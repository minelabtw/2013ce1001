#include <iostream>
using namespace std;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x=0;
    int y=0;

    while(x<=0)                                 //判斷x的值是否符合範圍
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
        if(x<=0)
        {
            cout<<"Out of range!\n";
        }
    }
    while(y<=0)                                 //判斷y的值是否符合範圍
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
        if(y<=0)
        {
            cout<<"Out of range!\n";
        }
    }
    cout<<"First time calculation\n";
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    cout<<"After change function...\n";
    cout<<"Second time calculation\n";
    Change(x,y);                                   //呼叫函式將x,y互換
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y);

    return 0;
}

int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)
{
    int a;
    int b;
    a=x;                        //將x的值帶入a
    b=y;                        //將y的值帶入b
    x=b;                        //使x變為y
    y=a;                        //使y變為x
}
