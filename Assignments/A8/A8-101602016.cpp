#include<iostream>
using namespace std;

int x=0,y=0;//宣告x,y變數
int Plus(int x,int y)//加法運算
{
    return x+y;
}

int Minus(int x,int y)//減法運算
{
    return x-y;
}

int Multiply(int x,int y)//乘法運算
{
    return x*y;
}

float Divide(float x,float y)//除法運算
{
    return x/y;
}

void Change(int &x , int &y)//變數交換
{
    int z=0;
    z=x;
    x=y;
    y=z;
}

int main()
{
    do//請使用者輸入x並大於零
    {
        cout<<"Please enter x ( >0 ):";
        cin>>x;
        if(x<=0)
            cout<<"Out of range!"<<endl;
    }
    while(x<=0);

    do//請使用者輸入y並大於零
    {
        cout<<"Please enter y ( >0 ):";
        cin>>y;
        if(y<=0)
            cout<<"Out of range!"<<endl;
    }
    while(y<=0);

    //顯示計算結果
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    Change(x,y);//變數交換

    //顯示交換變數後的計算結果
    cout<<"After change function..."<<endl
        <<"Second time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    return 0;
}
