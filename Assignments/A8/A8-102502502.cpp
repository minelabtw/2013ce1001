#include<iostream>
using namespace std;

int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)
{
    int t;
    t=x;
    x=y;
    y=t;
}

int main()
{
    int x,y;
    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while (x < 1)
    {
        cout << "Out of range!\n" << "Please enter x ( >0 ): ";
        cin >> x;
    }
    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while (y < 1)
    {
        cout << "Out of range!\n" << "Please enter y ( >0 ): ";
        cin >> y;
    }
    cout << "First time calculation\n";
    cout << "x + y =" << Plus(x,y) << endl;
    cout << "x - y =" << Minus(x,y) << endl;
    cout << "x * y =" << Multiply(x,y) << endl;
    cout << "x / y =" << Divide(x,y) << endl;
    cout << "After change function...\n";
    cout << "Second time calculation\n";
    Change(x,y);
    cout << "x + y =" << Plus(x,y) << endl;
    cout << "x - y =" << Minus(x,y) << endl;
    cout << "x * y =" << Multiply(x,y) << endl;
    cout << "x / y =" << Divide(x,y) << endl;

    return 0;
}
