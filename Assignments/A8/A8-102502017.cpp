#include<iostream>
using namespace std;
int Plus(int x,int y)
{
    return x+y;                 //傳回x+y
}
int Minus(int x,int y)
{
    return x-y;                 //傳回x-y
}
int Multiply(int x,int y)
{
    return x*y;                 //傳回x*y
}
float Divide(float x,float y)
{
    return x/y;                 //傳回x/y
}
void Change(int &x,int &y)
{
    int i=x;                    //紀錄x的數值
    x=y;
    y=i;
}
int main(void)
{
    int input[2]= {};
    for(int i=0;i<2;i=i)
    {
        if(i==0)cout << "Please enter x( >0 ): ";
        else cout << "Please enter y ( >0 ): ";
        cin >> input[i];
        if(input[i]<=0)cout << "Out of range!\n";
        else i++;
    }
    for(int i=0;i<2;i++)
    {
        if(i==0)cout << "First time calculation\n";
        else cout << "After change function..." << endl << "Second time calculation\n";
        cout << "x + y = " << Plus(input[0],input[1]) << endl;
        cout << "x - y = " << Minus(input[0],input[1]) << endl;
        cout << "x * y = " << Multiply(input[0],input[1]) << endl;
        cout << "x / y = " << Divide(input[0],input[1]);
        if(i==0)cout << endl;
        Change(input[0],input[1]);
    }
    return 0;
}
