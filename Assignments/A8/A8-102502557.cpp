#include <iostream>
using namespace std;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y , int z);

int main()
{
    int x,y,z;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)
    {
        cout<<"Out of range!";
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)
    {
        cout<<"Out of range!";
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    
    Change(x,y,z=0);//自己再多定一個z給x,y存，不能直接互換的
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation";
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    
    
}

int Plus(int i,int j)//因為題目規定要用int, 所以只好把cout<<"x+y"放在maon函數內，int自定函數，只能return一個值
{
    return i+j;
}

int Minus(int i , int j)
{
    return i-j;
}
int Multiply(int i, int j)
{
    return i*j;
}
float Divide(float i , float j)
{
    return i/j;
}
void Change(int &x , int &y ,int z)
{
    z=x,x=y,y=z;//把x的值存入z,y的值存入x,把z的值存入y
}


