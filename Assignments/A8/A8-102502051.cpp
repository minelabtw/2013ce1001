#include<iostream>
using namespace std;


int Plus(int x , int y) //加法
{
    int b=0;
    b= x + y;
}

int Minus(int x , int y) //減法
{
    int a=0;
    a=x-y;
}

int Multiply(int x , int y) // 乘法
{
    int c=0;
    c=x*y;
}

void Divide(float x , float y) // 除法
{
    int d=0;
    d=x/y;
}
void Change(float &x , float &y) //將X,Y相反
{
    int temp=x;
    x=y;
    y=temp;
}


int main()
{
    float x,y; //宣告變數

    cout << "Please enter x ( >0 ): ";
    cin >>x;
    while( x <= 0 )//判斷數字是否>0，若<=0則重新輸入，並告知數字不在範圍內
    {
        cout << "Out of range!!" << endl;
        cout << "Please enter x ( >0 ):";
        cin >> x;
    }
    cout << "Please enter y ( >0 ): ";
    cin >>y;
    while( y <= 0 )//判斷數字是否>0，若<=0則重新輸入，並告知數字不在範圍內
    {
        cout << "Out of range!!" << endl;
        cout << "Please enter y ( >0 ):";
        cin >> y;
    }

    cout << "First time calculation "<<endl; //開始計算
    Plus(x,y);
    cout << "x + y = "<<Plus(x,y)<<endl;
    Minus(x,y);
    cout<<"x - y = "<< Minus(x,y)<<endl;
    Multiply(x,y);
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    Divide(x,y);
    cout<<"x / y = "<< x/y<<endl;
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl; //開始將X,Y 轉換計算
    Change(x,y);
    cout << "x + y = "<<Plus(x,y)<<endl;
    Minus(x,y);
    cout<<"x - y = "<< Minus(x,y)<<endl;
    Multiply(x,y);
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    Divide(x,y);
    cout<<"x / y = "<< x/y<<endl;

    return 0;
}
