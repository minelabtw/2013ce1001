#include<iostream>
using namespace std ;
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);
int main()
{
    int x ;
    int y ;
    do
    {
        cout << "Please enter x( >0 ): " ;
        cin >> x ;
        if(x<=0)
        cout <<"Out of range!\n" ;
    }while(x<=0);
    //in put x
    do
    {
        cout << "Please enter y( >0 ): " ;
        cin >> y ;
        if(y<=0)
        cout <<"Out of range!\n" ;
    }while(y<=0);
    //in put y
    cout << "First time calculation\n" ;
    cout << "x + y = " <<Plus(x,y) << endl ;
    cout << "x - y = " <<Minus(x,y) << endl ;
    cout << "x * y = " <<Multiply(x,y) << endl ;
    cout << "x / y = " <<Divide((float)x,(float)y) << endl ;
    //first calculation
    cout << "After change function...\nSecond time calculation\n" ;

    Change(x,y) ;
    //change x to y

    cout << "x + y = " <<Plus(x,y) << endl ;
    cout << "x - y = " <<Minus(x,y) << endl ;
    cout << "x * y = " <<Multiply(x,y) << endl ;
    cout << "x / y = " <<Divide((float)x,(float)y) << endl ;
    //secend calculation
    return 0 ;
}
int Plus(int x , int y)
{
    return x+y ;
}
int Minus(int x , int y)
{
    return x-y ;
}
int Multiply(int x , int y)
{
    return x*y ;
}
float Divide(float x , float y)
{
    return x/y ;
}
void Change(int &x , int &y)
{
    int temp ;
    temp=x ;
    x=y ;
    y=temp ;
}
