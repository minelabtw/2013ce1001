#include <iostream>
using namespace std;
// 宣告函式
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void change(int &x , int &y);

int main()
{
    int fn, sn;
    do
    {
        cout << "Please enter x ( >0 ): ";
        cin >> fn;
        if (fn <= 0)
            cout << "Out of range!\n";
    }
    while (fn <= 0);

    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> sn;
        if (sn <= 0)
            cout << "Out of range!\n";
    }
    while (sn <= 0);

    cout << "First time calculation\n";
    cout << "x + y = " << Plus(fn ,sn) <<endl;
    cout << "x - y = " << Minus(fn , sn) <<endl;
    cout << "x * y = " << Multiply(fn , sn) << endl;
    cout << "x / y = " << Divide(fn , sn) << endl;

    cout << "After change function...\nSecond time calculation\n";

    change(fn , sn); // 叫出reference 函式
    // 兩數已交換
    cout << "x + y = " << Plus(fn ,sn) <<endl;
    cout << "x - y = " << Minus(fn , sn) <<endl;
    cout << "x * y = " << Multiply(fn , sn) << endl;
    cout << "x / y = " << Divide(fn , sn) << endl;

    return 0;
}

int Plus(int x , int y)
{
    return x + y;
}

int Minus(int x , int y)
{
    return x - y;
}

int Multiply(int x , int y)
{
    return x * y;
}

float Divide(float x , float y)
{
    return x / y;
}
void change(int &x , int &y)
{
    int z; // 輸入相異變數使得能轉換
    z = x;
    x = y;
    y = z;
}
