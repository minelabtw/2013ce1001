#include<iostream>
using namespace std;

int Plus(int x , int y);                      //宣告4個函式計算加減乘除
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);                 //讓輸入直對調的函示

int main()
{
    int a;
    int b;

    cout<<"Please enter x ( >0 ): ";
    cin>>a;
    while(a<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>a;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>b;
    while(b<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>b;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(a,b)<<endl;
    cout<<"x - y = "<<Minus(a,b)<<endl;
    cout<<"x * y = "<<Multiply(a,b)<<endl;
    cout<<"x / y = "<<Divide(a,b)<<endl;
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    Change(a,b);                                        //運用call by reference 呼叫Change使a,b對調
    cout<<"x + y = "<<Plus(a,b)<<endl;
    cout<<"x - y = "<<Minus(a,b)<<endl;
    cout<<"x * y = "<<Multiply(a,b)<<endl;
    cout<<"x / y = "<<Divide(a,b)<<endl;

    return 0;
}

int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x,int &y)
{
    int z;                                              //z為中間變數
    z=x;
    x=y;
    y=z;
}
