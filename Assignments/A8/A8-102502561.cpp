#include<iostream>
using namespace std;

int main()
{
    int add(int&,int&);
    int subtract(int&,int&);
    int times(int&,int&);
    int divide(int&,int&);
    void change(int&,int&);
    int x = 0;
    int y = 0;
    do
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if(x<=0)cout << "Out of range!\n";
    }
    while(x<=0);                            //確定x符合範圍
    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if(y<=0)cout << "Out of range!\n";
    }
    while(y<=0);                            //確定y符合範圍

    cout << "First time calculation\n";     //第一次
    add(x,y);
    subtract(x,y);
    times(x,y);
    divide(x,y);
    cout << "After change function...\n";
    cout << "Second time calculation\n";    //第二次
    change(x,y);
    add(x,y);
    subtract(x,y);
    times(x,y);
    divide(x,y);
    return 0;
}
void add(int &x,int &y)
{
    cout << "x + y = " << x+y << "\n";
}
void subtract(int &x,int &y)
{
    cout << "x - y = " << x-y << "\n";
}
void times(int &x,int &y)
{
    cout << "x * y = " << x*y << "\n";
}
void divide(int &x,int &y)
{
    float a = x;
    float b = y;
    cout << "x / y = " << a/b << "\n";  //先把x,y轉成浮點數,以免除玩結果無法正確顯示

}
void change(int &x,int &y)
{
    x^=y;
    y^=x;
    x^=y;
    //cout << "x : " << x << "\ny : " << y << "\n";
}
