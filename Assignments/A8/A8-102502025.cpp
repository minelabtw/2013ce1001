#include <iostream>
using namespace std;

double Plus(double x,double y);         //int加法function
double Minus(double x,double y);        //int減法function
double Multiply(double x,double y);     //int乘法function
float Divide(float x, float y);         //int除法function
void Change(double &x ,double &y);      //intxy互換function

int main()
{
    double x=0;                         //令x
    double y=0;                         //令y

    while(x<=0)                         //假如小於等於0進行迴圈
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if(x<=0)                        //假如等於0，則顯示
        {
            cout << "Out of range!\n";
        }
    }

     while(y<=0)
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if(y<=0)
        {
            cout << "Out of range!\n";
        }
    }

    cout << "First time calculation\n";
    cout << x << " + " << y << " = " << Plus(x,y) << endl ;//plus()為進行function
    cout << x << " - " << y << " = " << Minus(x,y) << endl ;//minus()為進行function
    cout << x << " * " << y << " = " << Multiply(x,y) << endl ;//multiply()為進行function
    cout << x << " / " << y << " = " << Divide(x,y) << endl ;//divide()為進行function

    cout << "After change funtion...\n";
    Change(x,y);//change()為進行function
    cout << "Second time calculation\n";
    cout << x << " + " << y << " = " << Plus(x,y) << endl ;
    cout << x << " - " << y << " = " << Minus(x,y) << endl ;
    cout << x << " * " << y << " = " << Multiply(x,y) << endl ;
    cout << x << " / " << y << " = " << Divide(x,y) << endl ;

    return 0;
}

double Plus(double x,double y)
{
    return x+y ;
}

double Minus(double x,double y)
{
    return x-y;
}

double Multiply(double x,double y)
{
    return x*y;
}

float Divide(float x,float y)
{
    return x/y ;
}

void Change(double &x,double &y)
{
    int a=x;
    int b=y;
    x=b;
    y=a;
}
