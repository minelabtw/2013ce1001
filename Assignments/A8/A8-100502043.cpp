#include<iostream>

using namespace std;

int Plus(int x,int y)
{
    return x+y;
}
int Minus(int x,int y)
{
    return x-y;
}
int Multiply(int x,int y)
{
    return x*y;
}
float Divide(float x,float y)
{
    return x/y;
}
void Change(int &x,int &y)  //�NX,Y�Ȥ���
{
    int tem;
    tem = x;
    x = y;
    y = tem;
}

int main()
{
    int num1=0,num2=0;

    while (num1<1)
    {
        cout << "Please enter x ( >0 ): ";
        cin >> num1;
        if (num1<1)
            cout << "Out of range!" << endl;
    }
    while (num2<1)
    {
        cout << "Please enter y ( >0 ): ";
        cin >> num2;
        if (num2<1)
            cout << "Out of range!" << endl;
    }

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(num1,num2) << endl;
    cout << "x - y = " << Minus(num1,num2) << endl;
    cout << "x * y = " << Multiply(num1,num2) << endl;
    cout << "x / y = " << Divide(num1,num2) << endl;

    Change(num1,num2);  //�I�s�����禡

    cout << "After change function..." << endl << "Second time calculation" << endl;
    cout << "x + y = " << Plus(num1,num2) << endl;
    cout << "x - y = " << Minus(num1,num2) << endl;
    cout << "x * y = " << Multiply(num1,num2) << endl;
    cout << "x / y = " << Divide(num1,num2) << endl;

    return 0;
}


