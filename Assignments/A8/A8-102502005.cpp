#include<iostream>
using namespace std;

int Plus(int x , int y);                            //ㄧ计prototype
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()

{
    int x,y;                                        //跑计

    cout << "Please enter x( >0 ): " ;              //璶―ㄏノ块才絛瞅计璝ぃ才玥璶―穝块
    cin >> x ;
    while(x<=0)
    {
        cout << "Out of range!" << endl ;
        cout << "Please enter x( >0 ): ";
        cin >> x;
    }

    cout << "Please enter y ( >0 ): " ;
    cin >> y ;
    while(y<=0)
    {
        cout << "Out of range!" << endl ;
        cout << "Please enter y ( >0 ): " ;
        cin >> y ;
    }

    cout << "First time calculation" << endl ;      //㊣ㄧ计挡狦
    cout << "x + y = " << Plus(x,y) << endl ;
    cout << "x - y = " << Minus(x,y) << endl ;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl ;

    Change(x,y);                                    //ノノChangeㄧ计рx㎝yユ传

    cout << "After change function..." << endl ;    //㊣ㄧ计挡狦
    cout << "Second time calculation" << endl ;
    cout << "x + y = " << Plus(x,y) << endl ;
    cout << "x - y = " << Minus(x,y) << endl ;
    cout << "x * y = " << Multiply(x,y) << endl ;
    cout << "x / y = " << Divide(x,y) << endl;

    return 0;

}

int Plus(int x , int y)
{
    int c;
    c=x+y;
    return c;
}

int Minus(int x , int y)
{
    int c;
    c=x-y;
    return c;
}

int Multiply(int x , int y)
{
    int c;
    c=x*y;
    return c ;
}

float Divide(float x , float y)
{
    float c;
    c=x/y;
    return c ;
}

void Change(int &x , int &y)
{
    int c;
    c = x;
    x = y;
    y = c;
}


