#include <iostream>

using namespace std;

int Plus(int x , int y);  //宣告函數型式
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main(){
    int x;  //宣告變數
    int y;
    float x2;
    float y2;

    do{  //輸入x並判斷有無超出範圍
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if(x <= 0){
            cout << "Out of range!" << endl;
        }
    }while(x <= 0);

    do{  //輸入y並判斷有無超出範圍
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if(y <= 0){
            cout << "Out of range!" << endl;
        }
    }while(y <= 0);

    x2 = x;  //把整數型態存成浮點數
    y2 = y;

    cout << "First time calculation" << endl;  //輸出加減乘除結果
    cout << "x + y = " << Plus(x , y) << endl;
    cout << "x - y = " << Minus(x , y) << endl;
    cout << "x * y = " << Multiply(x , y) << endl;
    cout << "x / y = " << Divide(x2 , y2) << endl;

    Change(x , y);  //交換xy
    x2 = x;  //把整數型態存成浮點數
    y2 = y;

    cout << "After change function..." << endl;  //輸出變換後的加減乘除結果
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(x , y) << endl;
    cout << "x - y = " << Minus(x , y) << endl;
    cout << "x * y = " << Multiply(x , y) << endl;
    cout << "x / y = " << Divide(x2 , y2) << endl;

    return 0;
}

int Plus(int x , int y){  //加函式
    return x + y;
}
int Minus(int x , int y){  //減函式
    return x - y;
}
int Multiply(int x , int y){  //乘函式
    return x * y;
}
float Divide(float x ,float y){  //除函式
    return x / y;
}
void Change(int &x , int &y){  //交換函式
    int temp = x;
    x = y;
    y = temp;
}
