#include <iostream>

using namespace std ;

int Plus ( int x , int y ) ;
int Minus ( int x , int y ) ;
int Multiply ( int x , int y ) ;
float Divide ( float x , float y ) ;  //宣告四函式。

int main ()
{
    int x ;
    int y ;  //宣告兩變數。

    cout << "Please enter x ( >0 ): " ;
    cin >> x ;  //使輸入x值。

    while ( x <= 0 )
    {
        cout << "Out of range!" << endl << "Please enter x ( >0 ): " ;
        cin >> x ;
    }  //判斷是否大於零，若不是就重新輸入。

    cout << "Please enter y ( >0 ): " ;
    cin >> y ;  //使輸入y值。

    while ( y <= 0 )
    {
        cout << "Out of range!" << endl << "Please enter y ( >0 ): " ;
        cin >> y ;
    }  //判斷是否大於零，若不是就重新輸入。

    cout << "First time calculation" << endl << "x + y = " << Plus ( x , y ) << endl ;
    cout << "x - y = " << Minus ( x , y ) << endl ;
    cout << "x * y = " << Multiply ( x , y ) << endl ;
    cout << "x / y = " << Divide ( x , y ) << endl ;  //作四則運算，並輸出算式及結果。

    cout << "After change function ..." << endl << "Second time calculation" << endl ;
    cout << "x + y = " << Plus ( y , x ) << endl ;
    cout << "x - y = " << Minus ( y , x ) << endl ;
    cout << "x * y = " << Multiply ( y , x ) << endl ;
    cout << "x / y = " << Divide ( y , x ) << endl ;  //將兩數數值交換，再做一次四則運算，輸出算式及數值。

    return 0 ;
}

int Plus ( int x , int y )
{
return x + y ;
}  //定義加法函式。

int Minus ( int x , int y )
{
return x - y ;
}  //定義減法函式。

int Multiply ( int x , int y )
{
return x * y ;
}  //定義乘法函式。

float Divide ( float x , float y )
{
return x / y ;
}  //定義除法函式。
