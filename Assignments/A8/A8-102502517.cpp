#include <iostream>

using namespace std;

int Plus(int x , int y) //加法函式
{
    return x+y;
}

int Minus(int x , int y) //減法函式
{
    return x-y;
}

int Multiply(int x , int y) //乘法函式
{
    return x*y;
}

float Divide(float x , float y) //除法函式
{
    return x/y;
}

void Change(int &x , int &y) //對調函式
{
    int z = x;
    x = y;
    y = z;
}

int main()
{
    int x = 0; //宣告變數
    int y = 0; //宣告變數

    do //輸入變數
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if (x<=0)
            cout << "Out of range!" << endl;
    }
    while (x<=0);


    do //輸入變數
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if (y<=0)
            cout << "Out of range!" << endl;
    }
    while (y<=0);

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl; //輸出第一次結果
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    Change(x,y); //對調變數
    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl; //輸出第二次結果
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    return 0;
}
