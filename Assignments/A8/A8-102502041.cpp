#include<iostream>
using namespace std;
int Plus(int x, int y)
{
    return x+y;
}
int Minus(int x, int y)
{
    return x-y;
}
int Multiply(int x, int y)
{
    return x*y;
}
float Divide(float x, float y)
{
    return x/y;
}
void Calculate(int x, int y)                //使用此函數來顯示各種運算結果
{
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
}
void Change(int& x, int& y)                 //依照題意使用Call by reference
{
    int b;
    b=x;
    x=y;
    y=b;
    cout<<"After change function..."<<endl;
}
int main()
{
    int x,y;
    while(1)
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
        if(x<=0)
            cout<<"Out of range!"<<endl;
        else
            break;
    }
    while(1)
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
        if(y<=0)
            cout<<"Out of range!"<<endl;
        else
            break;
    }
    cout<<"First time calculation"<<endl;
    Calculate(x,y);
    Change(x,y);
    cout<<"Second time calculation"<<endl;
    Calculate(x,y);

    return 0;
}
