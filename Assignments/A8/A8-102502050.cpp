#include<iostream>
using namespace std;
int Plus(int  , int );//回傳 x+y
int Minus(int  , int );//回傳 x-y
int Multiply(int  , int );//回傳 x*y
float Divide(float  , float );//回傳 x/y
void Change(int & , int &);//x 和 y 互換
int main()
{
    int x,y;
    int answer1,answer2,answer3;
    float answer4;                  //儲存function計算的結果
    do
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }
    while(x<=0 && cout << "Out of range!\n");
    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }
    while(y<=0 && cout << "Out of range!\n");
    //輸入並檢查

    cout << "First time calculation\n";
    for(int i=0;i<2;i++)
    {
        answer1=Plus(x,y);//計算x+y
        answer2=Minus(x,y);//計算x-y
        answer3=Multiply(x,y);//計算x*y
        answer4=Divide((float)x,(float)y);//計算x/y
        cout << "x + y = " << answer1 << endl;
        cout << "x - y = " << answer2 << endl;
        cout << "x * y = " << answer3 << endl;
        cout << "x / y = " << answer4 << endl;
        //輸出結果

        if(!i)
            cout << "After change function...\n" << "Second time calculation\n";
        Change(x,y);

    }
    return 0;
}
int Plus(int x , int y)
{
    return (x+y);
}
 int Minus(int x , int y)
{
    return (x-y);
}
 int Multiply(int x , int y)
{
    return (x*y);
}
 float Divide(float x , float y)
{
    return (x/y);
}
 void Change(int &x , int &y)
{
    int temp=x;
    x=y;
    y=temp;
}
