#include<iostream>
using namespace std;

int Plus(int x,int y)//宣告一涵式計算x+y並輸出結果
{
    cout << "x + y = ";
    return x+y;
}

int Minus(int x,int y)//宣告一涵式計算x-y並輸出結果
{
    cout << "x - y = ";
    return x-y;
}

int Multiply(int x,int y)//宣告一涵式計算x*y並輸出結果
{
    cout << "x * y = ";
    return x*y;
}

float Divide(float x, float y)//宣告一涵式計算x/y並輸出結果
{
    cout << "x / y = ";
    return x/y;
}

void Change(int &x, int &y)//宣告一可變數涵式將x,y互換
{
    int p=x;
    x=y;
    y=p;
}

int main()
{
    int x=0,y=0;//宣告變數x,y初始為0來當作之後運算的兩數字

    while(x<=0)//當為第一次輸入或不符條件要求時
    {
        cout << "Please enter x ( >0 ): ";//輸出字串要求輸入x
        cin >> x;//將數字宣告給x
        if(x<=0)//若不符條件則輸出字串要求重新輸入
            cout << "Out of range!\n";
    }
    while(y<=0)//當為第一次輸入或不符條件要求時
    {
        cout << "Please enter y ( >0 ): ";//輸出字串要求輸入y
        cin >> y;//將數字宣告給y
        if(y<=0)//若不符條件則輸出字串要求重新輸入
            cout << "Out of range!\n";
    }
    cout << "First time calculation\n";//輸出字串"First time calculation"
    cout << Plus(x,y) << "\n";//輸出x+y的計算結果
    cout << Minus(x,y) << "\n";//輸出x-y的計算結果
    cout << Multiply(x,y) << "\n";//輸出x*y的計算結果
    cout << Divide(x,y) << "\n";//輸出x/y的計算結果
    cout << "After change function...\n" << "Second time calculation\n";//輸出字串"After change function..."換行後再輸出字串"Second time calculation"
    Change(x,y);//將x,y互換
    cout << Plus(x,y) << "\n";//輸出x+y的計算結果
    cout << Minus(x,y) << "\n";//輸出x-y的計算結果
    cout << Multiply(x,y) << "\n";//輸出x*y的計算結果
    cout << Divide(x,y);//輸出x/y的計算結果

    return 0;
}


