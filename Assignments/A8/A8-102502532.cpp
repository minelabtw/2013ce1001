#include<iostream>
using namespace std;

int Plus(int , int);                          //int Plus(int x , int y);  都可
int Minus(int , int);
int Multiply(int , int);
float Divide(float , float);
void Change(int & , int &);

int main()
{
    int x =0;
    int y =0;

    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while (x <= 0)                                 //正整數
    {
        cout<<"Out of range!\nPlease enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while (y <= 0)                                  //正整數
    {
        cout<<"Out of range!\nPlease enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation\nx + y = "<<Plus(x , y)<<"\nx - y = "<<Minus(x , y)<<"\nx * y = "<<Multiply(x , y)<<"\nx / y = "<<Divide(x , y);
    Change(x , y);
    cout<<"\nAfter change function...\nSecond time calculation\nx + y = "<<Plus(x , y)<<"\nx - y = "<<Minus(x , y)<<"\nx * y = "<<Multiply(x , y)<<"\nx / y = "<<Divide(x , y);

    return 0;
}

int Plus(int x , int y)                           //加 function
{
    return x + y;
}
int Minus(int x , int y)                        //減 function
{
    return x - y;
}
int Multiply(int x , int y)
{
    return x * y;
}
float Divide(float x , float y)                       //除
{
    return x / y;
}
void Change(int &x , int &y)                       //call by reference
{
    int X =0;                                       //再設 兩個變數
    int Y =0;

    X = x;                                    //保留 x y 的值
    Y = y;
    x = Y;                                         //不能 x=y y=x   第二行變y=y
    y = X;
}

