#include<iostream>

using namespace std;

double number(int a,char o); //宣告number函式
int Plus(int x , int y); //宣告Plus函式
int Minus(int x , int y); //宣告Minus函式
int Multiply(int x , int y); //宣告Multiply 函式
float Divide(float x , float y); //宣告Divide 函式
void Change(int &x , int &y); //宣告Change 函式

int main()
{
    int x=0,y=0;
    x=number(x,'x');
    y=number(y,'y');
    cout << "First time calculation" << endl;
    cout << "x + y = "<<Plus(x,y)<< endl;
    cout << "x - y = "<<Minus(x,y)<< endl;
    cout << "x * y = "<<Multiply(x,y)<< endl;
    cout << "x / y = "<<Divide(x,y)<< endl;
    cout << "After change function..."<< endl;
    cout << "Second time calculation"<<endl;
    Change(x,y);
    cout << "x + y = "<<Plus(x,y)<< endl;
    cout << "x - y = "<<Minus(x,y)<< endl;
    cout << "x * y = "<<Multiply(x,y)<< endl;
    cout << "x / y = "<<Divide(x,y)<< endl;

    return 0;
}

double number(int a,char o) //(用來判斷輸入值是否為正整數)
{
    cout<<"Please enter "<<o<<" ( >0 ):";
    cin >> a;
    while(a<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter "<<o<<" ( >0 ):";
        cin >> a;
    }
    return a;
}

int Plus(int x , int y) //(進行加法運算)
{
    return x+y;
}

int Minus(int x , int y) // (進行減法運算)
{
    return x-y;
}

int Multiply(int x , int y)//(進行乘法運算)
{
    return x*y;
}

float Divide(float x , float y)//(進行除法運算)
{
    return x/y;
}

void Change(int &x , int &y) //(讓x y交換)
{
    int q;
    q=x;
    x=y;
    y=q;
}
