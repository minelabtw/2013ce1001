//============================================================================
// Name        : A8-102502038.cpp
// Author      : catLee
// Version     : 0.1
// Description : NCU CE1001 A8
//============================================================================

#include <iostream>
using namespace std;

int Plus(unsigned int,unsigned int);
int miuns(unsigned int,unsigned int);
int multiply(unsigned int,unsigned int);
float divide (float,float);
void change(unsigned int &,unsigned int &);

int main(){
  unsigned int x,y;
  int preX,preY;
  while(1){  //input loop of x
    cout << "Please enter x( >0 ): ";
    cin >> preX;
    if(preX > 0){
      x = preX;
      break;
    }
    cout << "Out of range!";
  }
  while(1){  //input loop of y
    cout << "Please enter y( >0 ): ";
    cin >> preY;
    if(preY > 0){
      y = preY;
      break;
    }
    cout << "Out of range!";
  }
  cout << "First time calculation\n";
  cout << "x + y = " << Plus(x,y) << "\n";
  cout << "x - y = " << miuns(x,y) << "\n";
  cout << "x * y = " << multiply(x,y) << "\n";
  cout << "x / y = " << divide(x,y) << "\n";
  cout << "After change function...\n";
  cout << "Second time calculation\n";
  change(x,y);  //change!
  cout << "x + y = " << Plus(x,y) << "\n";  //too lazy to use loop
  cout << "x - y = " << miuns(x,y) << "\n";
  cout << "x * y = " << multiply(x,y) << "\n";
  cout << "x / y = " << divide(x,y) << "\n";
}

int Plus(unsigned int a,unsigned int b){  //Plus function,without "+" !!
  unsigned int uncarry,carry;
  while(1){
    carry = ((a & b) << 1);
    uncarry = (a xor b);
    if((((a & b) << 1)&(a xor b))==0){
      a = (carry xor uncarry);
      break;
    }else{
      a = carry;
      b = uncarry;
    }
  }
  return a;
}

int miuns(unsigned int a,unsigned int b){  //miuns function,without "-" !!
  return Plus(Plus(~b,1),a);
}
int multiply(unsigned int a,unsigned int b){  //multiply function,without "*" !!
  unsigned int res = 0;
  if(b == 0){
    return 0;
  }
  while(1){
    b = miuns(b,1);
    res = Plus(res,a);
    if(b <= 0){
      break;
    }
  }
  return res;
}
float divide (float a,float b){
  return a/b;  //divide function....I give up (´・ω・)
}
void change(unsigned int & a,unsigned int & b){  //change(or swap) function
  int c = a;
  a = b;
  b = c;
}
