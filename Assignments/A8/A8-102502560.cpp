#include <iostream>
using namespace std;

//prototype
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
	int x,y;
	while(1){
		cout << "Please enter x ( >0 ): "; cin >> x;
		if(x<=0){cout << "Out of range!\n"; continue;}
		break;
	}
	while(1){
		cout << "Please enter y ( >0 ): "; cin >> y;
		if(y<=0){cout << "Out of range!\n"; continue;}
		break;
	}

	cout << "First time calculation\n";

	cout << "x + y = " << Plus(x,y) << "\n"
		<< "x - y = " << Minus(x,y) << "\n"
		<< "x * y = " << Multiply(x,y) << "\n"
		<< "x / y = " << Divide(x,y) << "\n";

	Change(x,y);	//swap the values of x and y

	cout << "After change function...\n"
		<< "Second time calculation\n";

	cout << "x + y = " << Plus(x,y) << "\n"
		<< "x - y = " << Minus(x,y) << "\n"
		<< "x * y = " << Multiply(x,y) << "\n"
		<< "x / y = " << Divide(x,y) << "\n";

	return 0;
}

int Plus(int x , int y){return x+y;}
int Minus(int x , int y){return x-y;}
int Multiply(int x , int y){return x*y;}
float Divide(float x , float y){return x/y;}
void Change(int &x , int &y){int z=x; x=y; y=z;}
