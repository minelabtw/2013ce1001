#include<iostream>

using namespace std;

int Plus(int x , int y);                              //宣告函數
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
  int x,y;
  int a,b,c;
  float d;
  cout << "Please enter x ( >0 ): ";
  cin >> x;
  while(x<=0)                                  //當x小於0時進入迴圈
  {
     cout << "Out of range!!" << endl;
     cout << "Please enter x ( >0 ): ";
     cin >> x;
  }
  cout << "Please enter y ( >0 ): ";
  cin >> y;
  while(y<=0)                                   //當x小於0時進入迴圈
  {
     cout << "Out of range!!" << endl;
     cout << "Please enter y ( >0 ): ";
     cin >> y;

  }
  a=Plus(x,y);                                //呼叫函數
  b=Minus(x,y);
  c=Multiply(x,y);
  d=Divide(x,y);
  cout << "First time calculation"<< endl;
  cout << "x + y = " << a << endl << "x - y = " << b << endl << "x * y = " << c << endl << "x / y = " << d << endl;
  cout << "After change function..." << endl;
  Change(x,y);                              //呼叫函數Change
  a=Plus(x,y);
  b=Minus(x,y);
  c=Multiply(x,y);
  d=Divide(x,y);
  cout << "x + y = " << a << endl << "x - y = " << b << endl << "x * y = " << c << endl << "x / y = " << d << endl;
}


int Plus(int x , int y)
{
    int a;
    a= x + y;
    return a;
}

int Minus(int x , int y)
{
    int b;
    b=x - y;
    return b;
}

int Multiply(int x , int y)
{
    int c;
    c= x * y;
    return c;
}

float Divide(float x , float y)
{
    float d;
    d= x / y ;
    return d;
}

void Change(int &x , int &y)
{
    int k;
    k=x;
    x=y;
    y=k;
}
