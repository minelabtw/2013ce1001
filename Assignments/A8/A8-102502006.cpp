#include <iostream>

using namespace std;

int Plus(int x, int y) // 加法
{
    return (x+y);
}
int Minus(int x ,int y) // 減法
{
    return (x-y);
}
int Multiply(int x, int y) // 乘法
{
    return (x*y);
}
float Divide(float x, float y) // 除法
{
    return (x/y);
}
void Change(int &x, int &y) // 互換
{
    int z;
    z = x;
    x = y;
    y = z;
}

int main()
{

    int x=0;
    int y=0;

    while(x<1) // 判斷書入
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if(x<1)cout << "Out of range!\n";
    }

    while(y<1) // 判斷輸入
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if(y<1)cout << "Out of range!\n";
    }

    cout << "First time calculation\n" << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    Change(x,y); // 互換

    cout << "After change function...\nSecond time calculation\n" << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    return 0;
}
