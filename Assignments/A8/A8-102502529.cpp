#include<iostream>

using namespace std;

int Plus(int x , int y);                            //function prototype
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x=0,y=0 ;
    cout<<"Please enter x ( >0 ): ";
    cin>>x ;
    while(x<0||x==0)                                            //判斷x 是否正確
    {
        cout<<"Out of range!\n";                                //錯誤重新輸入
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y ;
    while(y<0||y==0)                                            //y亦同
    {
        cout<<"Out of range!\n";
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation\n";                           //接下來皆為function 運算
    Plus(x,y);
    Minus(x,y);
    Multiply(x,y);
    Divide(x,y);
    Change(x,y);
    cout<<"After change function...\n"<<"Second time calculation\n";
    Plus(x,y);
    Minus(x,y);
    Multiply(x,y);
    Divide(x,y);
}
int Plus(int x , int y)
{
    cout<<"x"<<" + "<<"y"<<" = "<<x+y<<endl;
}
int Minus(int x,int y)
{
    cout<<"x"<<" - "<<"y"<<" = "<<x-y<<endl;
}
int Multiply(int x,int y)
{
    cout<<"x"<<" * "<<"y"<<" = "<<x*y<<endl;
}
float Divide(float x,float y)
{
    cout<<"x"<<" / "<<"y"<<" = "<<x/y<<endl;
}
void Change(int &x , int &y)
{
    int a=0,b=0;
    a=x,b=y;                                                //利用a b 儲存x y 的值
    x=b,y=a;                                                //在互換 x變y y變x
}
