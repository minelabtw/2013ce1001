#include <iostream>

using namespace std ;

int Plus(int x , int y);         //function prototype
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y , int &a , int &b , int &c , float &d);
int main ()
{
    int x = 0 ;            //宣告變數
    int y = 0 ;
    int a = 0 ;
    int b = 0 ;
    int c = 0 ;
    float d = 0 ;
    do                                                           //輸入xy
    {
        cout << "Please enter x ( >0 ): " ;
        cin >> x ;
    } while (x <= 0 && cout << "Out of range!" << endl) ;

    do
    {
        cout << "Please enter y ( >0 ): " ;
        cin >> y ;
    } while (y <= 0 && cout << "Out of range!" << endl) ;

    cout << "First time calculation" << endl ;                 //運算並輸出結果
    cout << "x + y = " << Plus (x,y)  << endl ;
    cout << "x - y = " << Minus (x,y) << endl ;
    cout << "x * y = " << Multiply (x,y) << endl ;
    cout << "x / y = " << Divide (x,y)  << endl ;
    cout << "After change function..." << endl << "Second time calculation" << endl ;
    Change(x , y , a , b , c , d) ;                            //轉換後再運算並輸出結果
    cout << "x + y = " << a << endl ;
    cout << "x - y = " << b << endl ;
    cout << "x * y = " << c << endl ;
    cout << "x / y = " << d << endl ;

    return 0 ;
}

int Plus(int x , int y)
{
    int z = 0 ;
    z = x + y ;
    return z ;
}

int Minus(int x , int y)       //函式內容
{
    int z = 0 ;
    z = x - y ;
    return z ;
}

int Multiply(int x , int y)         //函式內容
{
    int z = 0 ;
    z = x*y ;
    return z ;
}

float Divide(float x , float y)          //函式內容
{
    float z = 0 ;
    z = x / y ;
    return z ;
}

void Change(int &x , int &y , int &a , int &b , int &c , float &d)         //函式內容
{
    a = x + y ;
    b = y - x ;
    c = x*y ;
    d = (float)y/(float)x ;
}
