#include<iostream>
using namespace std;

int Plus(int x , int y);//加法function prototype
int Minus(int x , int y);//減法function prototype
int Multiply(int x , int y);//乘法function prototype
float Divide(float x , float y);//除法function prototype
void Change(int &x , int &y);//交換值function prototype

int main()
{
    int x;
    int y;

    while(1)//x要>0
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
        if(x<=0)
            cout<<"Out of range!"<<endl;
        else
            break;
    }

    while(1)//y要>0
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
        if(y<=0)
            cout<<"Out of range!"<<endl;
        else
            break;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x"<<" + "<<"y"<<" = "<<Plus(x,y)<<endl;
    cout<<"x"<<" - "<<"y"<<" = "<<Minus(x,y)<<endl;
    cout<<"x"<<" * "<<"y"<<" = "<<Multiply(x,y)<<endl;
    cout<<"x"<<" / "<<"y"<<" = "<<Divide(x,y)<<endl;

    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;

    Change(x,y);//call by reference function，交換後的x,y二值
    cout<<"x"<<" + "<<"y"<<" = "<<Plus(x,y)<<endl;
    cout<<"x"<<" - "<<"y"<<" = "<<Minus(x,y)<<endl;
    cout<<"x"<<" * "<<"y"<<" = "<<Multiply(x,y)<<endl;
    cout<<"x"<<" / "<<"y"<<" = "<<Divide(x,y)<<endl;

    return 0;
}

int Plus(int x , int y)//加法
{
    return (x+y);
}
int Minus(int x , int y)//減法
{
    return (x-y);
}
int Multiply(int x , int y)//乘法
{
    return (x*y);
}
float Divide(float x , float y)//除法
{
    return (x/y);
}
void Change(int &x , int &y)//交換二值
{
    int c=x;
    x=y;
    y=c;
}
