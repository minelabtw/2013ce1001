#include <iostream>

using namespace std;
int Plus(int x , int y);                 //宣告方程
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);            //宣告指令

int main()
{
    int x,y=0;
    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while (x<0)
    {
        cout << "Out of range!";
        cin >> x;
    }
    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while (y<0)
    {
        cout << "Out of range!";
        cin >> y;
    }
    cout << "x + y = " << Plus(x,y);
    cout << endl << "x - y = " << Minus(x,y);
    cout << endl << "x * y = " << Multiply(x,y);
    cout << endl << "x / y = " << Divide(x,y);

    cout << endl << "After change function..." << endl << "Second time calculation" << endl;
    Change(x,y);                        //用指令互換變數
    cout << "x + y = " << Plus(x,y);
    cout << endl << "x - y = " << Minus(x,y);
    cout << endl << "x * y = " << Multiply(x,y);
    cout << endl << "x / y = " << Divide(x,y);

    return 0;
}
int Plus(int x , int y)                             //加方程
{
    int sum;
    sum = x + y ;
    return sum;
}
int Minus(int x , int y)                            //減方程
{
    int sum;
    sum = x - y ;
    return sum;
}
int Multiply(int x , int y)                         //乘方程
{
    int sum;
    sum = x * y ;
    return sum;
}
float Divide(float x , float y)                    //除方程
{
    float sum;
    sum = x / y ;
    return sum;
}
void Change(int &x , int &y)                       //互換變數的指令
{
    int a=x;
    x=y;
    y=a;
}
