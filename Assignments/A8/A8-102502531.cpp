#include <iostream>

using namespace std;
int a;
int b;
int Plus(int x , int y)  //加函式
{
    return x+y;
}
int Minus(int x , int y) //減函式
{
    return x-y;
}
int Multiply(int x , int y) //乘函式
{
    return x*y;
}
float Divide(float x , float y) //除函式
{
    return x/y;
}
void Change(int &x , int &y) //轉換函式
{
    int c;
    c=x;
    x=y;
    y=c;
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    cout<<"x + y = "<<Plus(a,b)<<endl;
    cout<<"x - y = "<<Minus(a,b)<<endl;
    cout<<"x * y = "<<Multiply(a,b)<<endl;
    cout<<"x / y = "<<Divide(a,b)<<endl;
}
int main()
{
    do //輸入x值
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>a;
        if(a<=0)
            cout<<"Out of range!"<<endl;
    }
    while(a<=0);
    do //輸入y值
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>b;
        if(b<=0)
            cout<<"Out of range!"<<endl;
    }
    while(b<=0); //輸出第一次答案
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(a,b)<<endl;
    cout<<"x - y = "<<Minus(a,b)<<endl;
    cout<<"x * y = "<<Multiply(a,b)<<endl;
    cout<<"x / y = "<<Divide(a,b)<<endl;
    Change(a,b); //轉換後輸出
}
