#include<iostream>
using namespace std ;
int Plus(int x , int y)  //加法函式
{
    return x+y ;
}
int Minus(int x , int y)   //減法函式
{
    return x-y ;
}
int Multiply(int x , int y)   //乘法函式
{
    return x*y ;
}
float Divide(float x , float y)  //除法函式
{
    return x/y ;
}
void Change(int &x , int &y ,int z )   //變數變換
{
    z=x ;
    x=y ;
    y=z ;
}
int main()
{
    int x,y ;
    cout<<"Please enter x ( >0 ): ";
    cin >>x ;
    while(x<=0)
    {
        cout<<"Out of range!\n" ;
        cout<<"Please enter x ( >0 ): ";
        cin >>x ;
    }
    cout<<"Please enter y ( >0 ): ";
    cin >>y ;
    while(y<=0)
    {
        cout<<"Out of range!\n" ;
        cout<<"Please enter y ( >0 ): ";
        cin >>y ;
    }

    cout <<"First time calculation\n" ;
    cout<<"x + y = "<<Plus(x,y)<<endl ;
    cout<<"x - y = "<<Minus(x,y)<<endl ;
    cout<<"x * y = "<<Multiply(x,y)<<endl ;
    cout<<"x / y = "<<Divide(x,y)<<endl ;
    cout<<"After change function...\n" ;
    cout <<"Second time calculation\n" ;
    int z ;
    Change(x,y,z) ;
    cout<<"x + y = "<<Plus(x,y)<<endl ;
    cout<<"x - y = "<<Minus(x,y)<<endl ;
    cout<<"x * y = "<<Multiply(x,y)<<endl ;
    cout<<"x / y = "<<Divide(x,y)<<endl ;
    return 0 ;
}
