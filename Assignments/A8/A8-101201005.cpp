#include<iostream>
using namespace std;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x,y;
    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while (x<=0)
    {
        cout << "Out of range!\nPlease enter x ( >0 ): ";
        cin >> x;
    }
    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while (y<=0)
    {
        cout << "Out of range!\nPlease enter y ( >0 ): ";
        cin >> y;
    }
    cout << "First time calculation\nx + y = " << Plus(x,y) << "\nx - y = " << Minus(x,y) << "\nx * y = " << Multiply(x,y) << "\nx / y = " << Divide(x,y) << "\n" ; //第一次運算結果
    cout << "After change function...\n" ;
    Change(x,y); //chande x into y,y into x
    cout << "Second time calculation\nx + y = " << Plus(x,y) << "\nx - y = " << Minus(x,y) << "\nx * y = " << Multiply(x,y) << "\nx / y = " << Divide(x,y); //第二次運算結果

return 0;
}
int Plus(int x , int y) //相加
{
    return x+y;
}
int Minus(int x , int y) //相減
{
    return x-y;
}
int Multiply(int x , int y) //相乘
{
    return x*y;
}
float Divide(float x , float y) //相除
{
    return x/y;
}
void Change(int &x , int &y) //將兩變數值互換
{
    int tmp;
    tmp = x;
    x = y;
    y = tmp;
}
