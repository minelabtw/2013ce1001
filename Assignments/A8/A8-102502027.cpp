#include <iostream>
using namespace std;
int Plus(int x , int y);//宣告原型
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x,y,X;
    do //執行
    {
        cout<<"Please enter x ( >0 ): ";//輸出
        cin>>x;//輸入
    }
    while(x<=0 && cout<<"Out of range!\n");
    do
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    while(y<=0 && cout<<"Out of range!\n");
    cout<<"First time calculation"<<endl<<"x + y = "<<Plus(x,y)<<endl; //呼叫
    cout<<"x - y = "<<Minus(x,y)<<endl<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl<<"After change function..."<<endl<<"Second time calculation"<<endl;
    Change(x,y); //呼叫
    cout<<"x + y = "<<x + y<<endl;
    cout<<"x - y = "<<x - y <<endl<<"x * y = "<<x * y<<endl;
    cout<<"x / y = "<<x / y;
    return 0;
}
int Plus(int x , int y)
{
    return x + y ; //回傳
}
int Minus(int x , int y)
{
    return x - y;
}
int Multiply(int x , int y)
{
    return x * y;
}
float Divide(float x , float y)
{
    return x / y ;
}
void Change(int &x , int &y) //x值和y值互換
{   int X;
    X=x;
    x=y;
    y=X;
}
