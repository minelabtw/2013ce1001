#include<iostream>
using namespace std;

int Plus(int x , int y) //+的函式
{
    return x+y;

}
int Minus(int x , int y) //-的函式
{
    return x-y;
}
int Multiply(int x , int y) //*的函式
{
    return x*y;
}
float Divide(float x , float y) // /的函式
{
    return x/y;
}
void Change(int &x , int &y ,int z) // X Y 交換的函式
{
    z=x;
    x=y;
    y=z;
}

int main()
{
    int x=0;
    int y=0;
    int z=0;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0) // 確認X>0
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }

    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0) // 確認Y>0
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }


    cout<<"First time calculation"<<endl; //顯示出來 運算結果
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    Change(x,y,z);
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    return 0;
}
