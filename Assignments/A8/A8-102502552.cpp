#include<iostream>
#include<iomanip>
using namespace std;

int Plus(double x,double y);//呼叫加法函式
int Minus(double x,double y);//呼叫減法函式
int multiply(double x,double y);//呼叫乘法函式
double divide(double x,double y);//呼叫除法函式
void change(double &x,double &y);//呼叫轉換函式

int main()
{
   double x,y;//宣告兩個變數

   do{
   cout << "Please enter x( >0 ):";
   cin >> x;
   if(x <= 0)
    cout << "Out of range!" << endl;
   }while(x <= 0);//輸入第一個變數,並檢查是否符合要求

   do{
   cout << "Please enter y( >0 ):";
   cin >> y;
   if(y <= 0)
    cout << "Out of range!" << endl;
   }while(y <= 0);//輸入第二個變數,並檢查是否符合要求

   cout << "First time calculation" << endl << "x+y=" << Plus(x,y) << endl << "x-y=" << Minus(x,y) << endl << "x*y=" << multiply(x,y) << endl << "x/y=" << divide(x,y) << endl << "After change function..." << endl;//將兩個變數加減乘除,並提示即將轉換

   change(x,y);//轉換兩變數

   cout << "Second time calculation" << endl << "x+y=" << Plus(x,y) << endl << "x-y=" << Minus(x,y) << endl << "x*y=" << multiply(x,y) << endl << "x/y=" << divide(x,y);//將兩個變數加減乘除

   return 0;//回傳
}
int Plus(double x,double y)
{
    return x + y;
}//定義加法函式
int Minus(double x,double y)
{
    return x - y;
}//定義減法函式
int multiply(double x,double y)
{
    return x * y;
}//定義乘法函式
double divide(double x,double y)
{
    return x / y;
}//定義除法函式
void change(double &x,double &y)
{
    int sum;
    sum = x + y;
    x = y;
    y = sum - x;
}//定義轉換函式
