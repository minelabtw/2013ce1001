#include<iostream>

using namespace std;
//do Plus
int Plus(int x , int y)
{
    return x+y; //return result (x+y)
}
//do Minus
int Minus(int x , int y)
{
    return x-y; //return result
}
//do Multiply
int Multiply(int x , int y)
{
    return x*y; //return
}
//do Divide
float Divide(float x , float y)
{
    return x/y; //return
}
//Change two value
void Change(int &x , int &y) //x & y call by reference
{
    int tmp = x; //temporary variable storing x
    x = y;
    y = tmp; //swaping finish
}

int main()
{
    int x,y;
    while(1)
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if(x>0) //break if input is legal
            break;
        cout << "Out of range!" << endl; //error
    }

    while(1)
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if(y>0) //break if input is legal
            break;
        cout << "Out of range!" << endl; //error
    }

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl; //call function Plus(int x, int y)
    cout << "x - y = " << Minus(x,y) << endl; //call funcrion
    cout << "x * y = " << Multiply(x,y) << endl; //call function
    cout << "x / y = " << Divide((float)x,(float)y) << endl; //change data type and then call function
    cout << "After change function..." << endl;
    Change(x,y); //call function to change two value
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl; //call func
    cout << "x - y = " << Minus(x,y) << endl; //call func
    cout << "x * y = " << Multiply(x,y) << endl; //call func
    cout << "x / y = " << Divide((float)x,(float)y) << endl; //change data type and then call function

    return 0;
}
