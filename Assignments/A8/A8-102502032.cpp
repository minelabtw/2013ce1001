#include <iostream>
using namespace std;

//function prototype
int Plus( int, int );
int Minus( int, int );
int Multiply( int, int );
float Divide( float, float );
void Change( int &, int & );

int main()
{
    //decalaration and initialization
    int x = 0;
    int y = 0;

    //ask for first number ( x )
    do
    {
        cout << "Please enter x( >0 ): ";
        cin >> x;
        if ( x <= 0 )
            cout << "Out of range!" << endl;
    }
    while ( x <= 0 );

    //ask for second number ( y )
    do
    {
        cout << "Please enter y( >0 ): ";
        cin >> y;
        if ( y <= 0 )
            cout << "Out of range!" << endl;
    }
    while ( y <= 0 );

    //output First time calculation
    cout << "First time calculation" << endl
         << "x + y = " << Plus( x, y ) << endl
         << "x - y = " << Minus( x, y ) << endl
         << "x * y = " << Multiply( x, y ) << endl
         << "x / y = " << Divide( x, y ) << endl;

    //exchange x and y
    Change( x, y );

    //Second time calculation
    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl
         << "x + y = " << Plus( x, y ) << endl
         << "x - y = " << Minus( x, y ) << endl
         << "x * y = " << Multiply( x, y ) << endl
         << "x / y = " << Divide( x, y ) << endl;

    return 0;
}

//caculate x + y
int Plus( int x, int y )
{
    return x + y;
}

//caculate x - y
int Minus( int x, int y )
{
    return x - y;
}

//caculate x * y
int Multiply( int x, int y )
{
    return x * y;
}

//caculate x / y
float Divide( float x, float y )
{
    return x / y;
}

//exchange x and y
void Change( int &x, int &y )
{
    //decalaration and initialization
    int temp = 0;  //template

    //exchange x and y
    temp = x;
    x = y;
    y = temp;
}
