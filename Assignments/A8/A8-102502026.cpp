//A8-102502026

#include<iostream>
using namespace std;

int Plus(int x, int y);         //use as prototype
int Minus(int x , int y);       //use as prototype
int Multiply(int x , int y);     //use as prototype
float Divide(float x , float y);     //use as prototype
void Change(int &x , int &y);    //use as prototype

int main()
{
    int x=0;    //define x
    int y=0;    //define y
    cout<<"Please enter x ( >0 ):"; //ask x
    cin>>x;
    while (x<0) //if x<0
    {
        cout<<"Out of range!\n";    //wrong
        cout<<"Please enter x ( >0 ):"; //ask again
        cin>>x;
    }
    cout<<"Please enter y ( >0 ):"; //ask y
    cin>>y;
    while (y<0) //if y<0
    {
        cout<<"Out of range!\n";    //wrong
        cout<<"Please enter x ( >0 ):"; //ask again
        cin>>y;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;  //function plus
    cout<<"x - y = "<<Minus(x,y)<<endl; //function minus
    cout<<"x * y = "<<Multiply(x,y)<<endl;  //function multiply
    cout<<"x / y = "<<Divide(x,y)<<endl;    //function divide
    Change(x,y);    //function that change x to y and y to x
    cout<<"After change function...\n";
    cout<<"Second time calculation\n";  //x=y and y=x
    cout<<"x + y = "<<Plus(x,y)<<endl;  //function plus
    cout<<"x - y = "<<Minus(x,y)<<endl; //function minus
    cout<<"x * y = "<<Multiply(x,y)<<endl;  //function multiply
    cout<<"x / y = "<<Divide(x,y)<<endl;    //function divide
    return 0;
}
int Plus(int x , int y) //function plus formula
{
    return x+y;
}
int Minus(int x , int y)    //function minus formula
{
    return x-y;
}
int Multiply(int x , int y) //function multiply formula
{
    return x*y;
}
float Divide(float x , float y) //function divide formula
{
    return x/y;
}
void Change(int&x, int &y)  //function for change formula
{
    int a=x;
    int b=y;
    x=b;
    y=a;
}
