#include <iostream>
using namespace std;

int x,y ,c;

void Change(int &x , int &y)   //交換數值
{
    c=x;
    x=y;
    y=c;
}

int Plus(int x , int y)   //加
{
    return (x+y) ;
}
int Minus(int x , int y) //減
{
    return (x-y) ;
}
int Multiply(int x , int y)  //乘
{
    return x*y ;
}
float Divide(float x , float y) // 除
{
    return x/y;
}
int main()
{
    cout <<"Please enter x ( >0 ): ";   //輸入變數
    cin >> x ;
    while (x<=0)
    {
        cout << "Out of range!" <<endl ;
        cout <<"Please enter x ( >0 ): ";
        cin >> x ;
    }
    cout <<"Please enter y ( >0 ): ";
    cin >> y ;
    while (y<=0)
    {
        cout << "Out of range!" <<endl ;
        cout <<"Please enter y` ( >0 ): ";
        cin >> y ;
    }
    cout <<"First time calculation" <<endl;       //運算成果
    cout << "x + y = " <<  Plus (x,y) <<endl  ;
    cout << "x - y = " << Minus( x , y) <<endl ;
    cout << "x * y = " << Multiply (x ,y ) << endl ;
    cout << "x / y = " << Divide( x,y ) <<endl ;

    cout <<"After change function..."<<endl ;    //運算成果
    cout <<"Second time calculation" <<endl;
    Change (x,y) ;
    cout << "x + y = " <<  Plus (x,y) <<endl  ;
    cout << "x - y = " << Minus( x , y) <<endl ;
    cout << "x * y = " << Multiply (x ,y ) << endl ;
    cout << "x / y = " << Divide( x,y ) <<endl ;

    return 0 ;
}
