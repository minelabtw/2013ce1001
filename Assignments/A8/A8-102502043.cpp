#include <iostream>

using namespace std;


int Plus(int x , int y)                   //方程式加
{
    return x+y;
}
int Minus(int x , int y)                  //方程式減
{
    return x-y;
}
int Multiply(int x , int y)               //函式乘
{
    return x*y;
}
float Divide(float x , float y)           //函式除
{
    return x/y;
}
void Change(int &x , int &y)              //函式變換XY值
{
    int i;
    i=y;
    y=x;
    x=i;
}
int main()
{
    int x;                                //宣告變數
    int y;
    int i;

    cout<<"Please enter x ( >0 ): ";      //顯示X值
    cin>>x;
    while(x<0)                            //限定X範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";      //顯示Y值
    cin>>y;
    while(y<0)                            //限定Y範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation"<<endl;   //顯示X Y運算過程
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    cout<<"After change function...";
    cout<<"Second time calculation"<<endl;
    Change(x,y);                           //進入函式Change
    cout<<"x + y = "<<Plus(x,y)<<endl;     //計算XY值
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    return 0;
}
