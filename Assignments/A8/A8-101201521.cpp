#include <iostream>
using namespace std;
//functions and prototypes
int Plus(int x, int y)//加
{
    return x+y;
}
int Minus(int x, int y)//減
{
    return x-y;
}
int Multiply(int x, int y)//乘
{
    return x*y;
}
float Divide(float x, float y)//除
{
    return x/y;
}
void Change(int &x, int &y)//x, y互換
{
    int tmp;
    tmp=x;
    x=y;
    y=tmp;
}
int main()
{
    int x=0, y=0;//宣告變數為整數
    do
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }
    while(x<=0 && cout << "Out of range!\n");
    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }
    while(y<=0 && cout << "Out of range!\n");
    for(int i=0; i<2; i++)//執行第一次動作後，變數互換，並重複執行
    {
        cout << (i==0 ? "First time calculation\n" : "After change function...\nSecond time calculation\n")
             << "x + y = " << Plus(x, y) << endl
             << "x - y = " << Minus(x, y) << endl
             << "x * y = " << Multiply(x, y) << endl
             << "x / y = " << Divide(x, y) << endl;
        if(i==0)
            Change(x, y);
    }
    return 0;
}
