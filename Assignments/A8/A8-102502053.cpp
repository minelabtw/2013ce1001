#include <iostream>
using namespace std;

//function prototype
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

//start of the program
int main()
{
    //call integers
    int x=0;
    int y=0;

    //intput for the first number
    while(x<=0)
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
        if(x<=0)//data validation
        {
            cout<<"Out of range!"<<endl;
        }
    }

    //input for the second number
    while(y<=0)
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
        if(y<=0)//data validation
        {
            cout<<"Out of range!"<<endl;
        }
    }

    //display the outputs
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    cout<<"After change function..."<<endl;
    Change(x,y); // flip x and y
    cout<<"Second time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    return 0;
}


int Plus(int x , int y) //function for addition
{
    return x+y;
}

int Minus(int x , int y) //function for subtraction
{
    return x-y;
}

int Multiply(int x , int y) //function for multiplication
{
    return x*y;
}

float Divide(float x , float y) //function for division
{
    return x/y;
}

void Change(int &x , int &y) //flip the numbers
{
    int a=x;
    x=y;
    y=a;
}
