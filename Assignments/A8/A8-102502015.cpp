#include <iostream>
using namespace std;
int Plus(int x , int y)                 //傳入 整數 x y 回傳 整數x+y
{
    return x+y;
}
int Minus(int x , int y)                //傳入 整數 x y 回傳 整數x-y
{
    return x-y;
}
int Multiply(int x , int y)             //傳入 整數 x y 回傳 整數x*y
{
    return x*y;
}
float Divide(float x , float y)         //傳入 整數 x y 回傳 浮點數x/y
{
    return x/y;
}
void Change(int &x , int &y)            //傳入參照 x y
{
    int tmp;                            //宣告暫存
    tmp=x;                              //存x
    x=y;                                //x給y值
    y=tmp;                              //y給暫存x值
}
int main()
{
    int x;
    int y;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)                             //超出範圍則重輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)                             //超出範圍則重輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;              //各函數運算
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    Change(x,y);                                    //x y 互換
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;              //各函數再運算
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y);
    return 0;
}
