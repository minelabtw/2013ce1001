#include <iostream>
using namespace std;
int Plus(int x, int y);//計算x+y
int Minus(int x, int y);//計算x-y
int Multiply(int x, int y);//計算x*y
float Divide(float x, float y);//計算x/y
void Change(int &x, int &y);//x和y值交換

int main(){
    int x = 1;//宣告型態為int的x，並初始化為1
    int y = 1;//宣告型態為int的y，並初始化為1

    while(1){
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if(x <= 0){
            cout << "Out of range!" << endl;
        }
        else{
            break;
        }
    }
    while(1){
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if(y <= 0){
            cout << "Out of range!" << endl;
        }
        else{
            break;
        }
    }
    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(x, y) << endl;
    cout << "x - y = " << Minus(x, y) << endl;
    cout << "x * y = " << Multiply(x, y) << endl;
    cout << "x / y = " << Divide((float)x, (float)y) << endl;
    cout << "After change function..." << endl;
    Change(x, y);
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(x, y) << endl;
    cout << "x - y = " << Minus(x, y) << endl;
    cout << "x * y = " << Multiply(x, y) << endl;
    cout << "x / y = " << Divide((float)x, (float)y) << endl;

    return 0;
}

int Plus(int x, int y){
    //計算x+y
    return x + y;
}
int Minus(int x, int y){
    //計算x-y
    return x - y;
}
int Multiply(int x, int y){
    //計算x*y
    return x * y;
}
float Divide(float x, float y){
    //計算x/y
    return x / y;
}
void Change(int &x, int &y){
    //x和y值交換
    int temp = x;//宣告型態為int的暫存變數，並初始化為x
    x = y;
    y = temp;
}
