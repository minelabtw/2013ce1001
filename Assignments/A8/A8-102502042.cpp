#include <iostream>
using namespace std;
int Plus(int, int );
int Minus(int, int );
int Multiply(int, int );
float Divide(float, float );
void Change(int &, int &);
int main()
{
    int x, y;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide((float)(x),(float)(y))<<endl;
    cout<<"After change function..."<<endl;
    Change(x,y);
    cout<<"Second time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide((float)(x),(float)(y))<<endl;
    return 0;
}
int Plus(int x, int y)
{
    return x+y;
}
int Minus(int x, int y)
{
    return x-y;
}
int Multiply(int x, int y)
{
    return x*y;
}
float Divide(float x, float y)
{
    return x/y;
}
void Change(int &x, int &y)
{
    int temp=x;
    x=y;
    y=temp;
}
