#include<iostream>
#include<cstdlib>

using namespace std;

int Plus(int x , int y);//add y to x.
int Minus(int x , int y);//subtract y from x.
int Multiply(int x , int y);//multiply x by y.
float Divide(float x , float y);//divide x by y.
void Change(int &x , int &y);//change x to y, y to x.

int main()
{
    int x, y;
    do//prompt user to enter a positive integer
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }
    while(x <= 0 && cout << "Out of range!\n");

    do//prompt user to enter a positive integer
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }
    while(y <= 0 && cout << "Out of range!\n");

    cout << "First time calculation\n";//perform the first time calculation.
    cout << "x + y = " << Plus(x, y) << "\n" << "x - y = " << Minus(x, y) << "\n" << "x * y = " << Multiply(x, y) << "\n" << "x / y = " << Divide (x, y) << "\n";

    cout << "After change function...\n" << "Second time calculation\n";//perform the second time calculation.
    Change(x, y);//exchange x and y.
    cout << "x + y = " << Plus(x, y) << "\n" << "x - y = " << Minus(x, y) << "\n" << "x * y = " << Multiply(x, y) << "\n" << "x / y = " << Divide (x, y) << "\n";

    system("pause");
    return 0;
}

int Plus(int x , int y)//add y to x.
{
    return x + y;
}
int Minus(int x , int y)//subtract y from x.
{
    return x - y;
}
int Multiply(int x , int y)//multiply x by y.
{
    return x * y;
}
float Divide(float x , float y)//divide x by y.
{
    return x / y;
}
void Change(int &x , int &y)//change x to y, y to x.
{
    int a;
    a = x;
    x = y;
    y = a;
}



