#include<iostream>
using namespace std;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y); //prototype

int main()
{
    int plus1;
    int minus1;
    int multiply1;
    float divide1;
    int x;
    int y; //宣告+-*/與兩數

    do
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }
    while (x<=0 && cout << "Out of range!" <<endl);

    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }
    while (y<=0 && cout << "Out of range!" <<endl); //要求輸入x.y

    cout << "First time calculation" <<endl;
    plus1=Plus(x , y);
    cout << "x + y = " << plus1 << endl;
    minus1=Minus(x ,y);
    cout << "x - y = " << minus1 << endl;
    multiply1=Multiply(x ,y);
    cout << "x * y = " << multiply1 << endl;
    divide1=Divide(x ,y);
    cout << "x / y = " << divide1 << endl; //進入函式運算是顯示答案

    cout << "After change function..." << endl;
    Change(x ,y); //變換x.y值

    cout << "Second time calculation" <<endl;
    plus1=Plus(x , y);
    cout << "x + y = " << plus1 << endl;
    minus1=Minus(x ,y);
    cout << "x - y = " << minus1 << endl;
    multiply1=Multiply(x ,y);
    cout << "x * y = " << multiply1 << endl;
    divide1=Divide(x ,y);
    cout << "x / y = " << divide1; //顯示第二次結果

    return 0;
}

int Plus(int x , int y) //加
{
    return (x+y);
}
int Minus(int x , int y) //減
{
    return (x-y);
}
int Multiply(int x , int y) //乘
{
    return (x*y);
}
float Divide(float x , float y) //除
{
    return (x/y);
}
void Change(int &x , int &y) //交換
{
    int c;
    c=x;
    x=y;
    y=c;
}
