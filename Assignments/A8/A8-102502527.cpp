#include <iostream>

using namespace std;

int Plus(int x,int y)//設定函數
{
    return x + y;
}

int Minus(int x,int y)
{
    return x - y;
}

int Multiply(int x,int y)
{
    return x * y;
}

float Divide(float x,float y)
{
    return x / y;
}

void Change(int &x,int &y)
{
    int a;
    a = x;
    x = y;
    y = a;
}

int main()
{
    int x,y;

    cout << "Please enter x ( >0 ): ";//顯示字彙並設定範圍
    cin >> x;
    while ( x <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }

    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while ( y <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }

    cout << "First time calculation" << endl;

    Plus(x,y);//叫出函數

    cout << "x + y = " << Plus(x,y) << endl;

    Minus(x,y);

    cout << "x - y = " << Minus(x,y) << endl;

    Multiply(x,y);

    cout << "x * y = " << Multiply(x,y) << endl;

    Divide(x,y);

    cout << "x / y = " << Divide(x,y) << endl;

    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl;

    Change(x,y);

    Plus(x,y);

    cout << "x + y = " << Plus(x,y) << endl;

    Minus(x,y);

    cout << "x - y = " << Minus(x,y) << endl;

    Multiply(x,y);

    cout << "x * y = " << Multiply(x,y) << endl;

    Divide(x,y);

    cout << "x / y = " << Divide(x,y) << endl;

    return 0;
}
