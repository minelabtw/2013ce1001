#include <iostream>

using namespace std;

int Plus(int x , int y);//函式
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x;
    int y;

    cout<<"Please enter x( >0 ): ";
    cin>>x;
    while (x<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x( >0 ): ";
        cin>>x;
    }

    cout<<"Please enter y( >0 ): ";
    cin>>y;
    while (y<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation"<<endl;
    Plus(x,y);//引用函式
    Minus(x,y);
    Multiply(x,y);
    Divide(x,y);
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    Change(x,y);
    Plus(x,y);
    Minus(x,y);
    Multiply(x,y);
    Divide(x,y);
    return 0;
}

int Plus(int x , int y)
{
    cout<<"x + y = "<<x+y<<endl;
}
int Minus(int x , int y)
{
    cout<<"x - y = "<<x-y<<endl;
}
int Multiply(int x , int y)
{
    cout<<"x * y = "<<x*y<<endl;
}
float Divide(float x , float y)
{
    cout<<"x / y = "<<x/y<<endl;
}
void Change(int &x , int &y)//把x和y交換(用第三者t)
{
    int t;
    t=x;
    x=y;
    y=t;
}
