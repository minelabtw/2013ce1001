#include <iostream>
using namespace std;
int Plus(int,int); //宣告函式
int Minus(int,int);
int Multiply(int,int);
float Divide(float,float);
void Change(int &, int &);

int main()
{
    int x=0; //宣告變數
    int y=0;

    do
    {
        cout << "Please enter x ( >0 ): "; //輸出
        cin >> x; //輸入
        if ( x<=0 )
            cout << "Out of range!" << endl;
    }

    while( x<=0 ); //當x小於等於0時迴圈
    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if ( y<=0 )
            cout << "Out of range!" << endl;
    }

    while( y<=0 );
    cout << "First time calculation"<<endl; //輸出第一次計算
    cout << "x + y = " << Plus(x,y )<< endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
    cout << "After change function..." << endl << "Second time calculation" << endl; //輸出第二次計算
    Change(x,y); //x跟y的值互換
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y);

    return 0;
}
int Plus(int x,int y) //加法函式
{
    x=x+y;
    return x;
}
int Minus(int x,int y) //減法函式
{
    x=x-y;
    return x;
}
int Multiply(int x,int y) //乘法函式
{
    x=x*y;
    return x;
}
float Divide(float x,float y) //除法函式
{
    x=x/y;
    return x;
}
void Change (int &x,int &y) //x跟y的值互換
{
    int a=x;
    int b=y;
    x=b;
    y=a;
}
