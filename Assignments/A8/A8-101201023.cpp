#include <iostream>

using namespace std;

int Plus(int x , int y);                                //宣告方程
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int Plus(int x , int y)                                 //運算兩變數相加
{
    return x+y;
}

int Minus(int x , int y)                                //運算兩變數相減
{
    return x-y;
}

int Multiply(int x , int y)                             //運算兩變數相乘
{
    return x*y;
}

float Divide(float x , float y)                         //運算兩變數相除
{
    return x/y;
}

void Change(int &x , int &y)                            //互換輸入的兩變數的值
{
    int X=0;
    int Y=0;
    X=x;                                                //固定原輸入的變數值
    Y=y;
    x=Y;
    y=X;
}

int main()
{
    int x=0;
    int y=0;

    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while(x<=0)                                                      //當值不符合範圍時，持續重新執行
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }

    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while(y<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;                       //輸出 x + y = 並將帶入方程的值輸出
    cout << "x - y = " << Minus(x,y) << endl;                      //輸出 x - y = 並將帶入方程的值輸出
    cout << "x * y = " << Multiply(x,y) << endl;                   //輸出 x * y = 並將帶入方程的值輸出
    cout << "x / y = " << Divide(x,y) << endl;                     //輸出 x / y = 並將帶入方程的值輸出

    Change(x,y);                                                   //將兩變數的值互換

    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y);

    return 0;
}
