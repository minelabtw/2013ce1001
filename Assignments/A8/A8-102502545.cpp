#include <iostream>
using namespace std;
int Plus(int x , int y);//宣告函式Plus
int Minus(int x , int y);//宣告函式Minus
int Multiply(int x , int y);//宣告函式Multiply
float Divide(float x , float y);//宣告函式Divide
void Change(int &x , int &y);//宣告函式Change
int main()
{
    int a=0,b=0,i,j,k;
    float l;
    do
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>a;
        if(a<1)
            cout<<"Out of range!\n";
    }
    while(a<1);//輸入a值，如果a小於1時輸出"Out of range!"並重新輸入a值
    do
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>b;
        if(b<1)
            cout<<"Out of range!\n";
    }
    while(b<1);//輸入b值，如果b小於1時輸出"Out of range!"並重新輸入b值
    cout<<"First time calculation\n";
    cout<<"x + y = "<<Plus(a,b)<<endl;
    cout<<"x - y = "<<Minus(a,b)<<endl;
    cout<<"x * y = "<<Multiply(a,b)<<endl;
    cout<<"x / y = "<<Divide(a,b)<<endl;

    cout<<"After change function...\nSecond time calculation\n";
    Change(a,b);
    cout<<"x + y = "<<Plus(a,b)<<endl;
    cout<<"x - y = "<<Minus(a,b)<<endl;
    cout<<"x * y = "<<Multiply(a,b)<<endl;
    cout<<"x / y = "<<Divide(a,b)<<endl;
    return 0;
}
int Plus(int x , int y)//宣告函式Plus
{
    int i=x+y;
    return i;
}
int Minus(int x , int y)//宣告函式Minus
{
    int j=x-y;
    return j;
}
int Multiply(int x , int y)//宣告函式Multiply
{
    int k=x*y;
    return k;
}
float Divide(float x , float y)//宣告函式Divide
{
    float l=x/y;
    return l;
}
void Change(int &x , int &y)//宣告函式Change
{
    int r=x;
    x=y;
    y=r;
}
