#include <iostream>

using namespace std;

int Plus(int x , int y);     //宣告函式
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{

    int x=0;    //宣告變數
    int y=0;

    cout<< "Please enter x ( >0 ): ";
    cin>> x;
    while(x<=0)    //迴圈
    {
        cout<< "Out of range!\n""Please enter x ( >0 ): ";
        cin>> x;
    }

    cout<< "Please enter y ( >0 ): ";
    cin>> y;
    while(y<=0)    //迴圈
    {
        cout<< "Out of range!\n""Please enter y ( >0 ): ";
        cin>> y;
    }

    cout<< "First time calculation"<< endl;
    cout<< "x + y = "<< Plus(x , y)<< endl;
    cout<< "x - y = "<< Minus( x , y)<< endl;
    cout<< "x * y = "<< Multiply( x , y)<< endl;
    cout<< "x / y = "<< Divide( x , y)<< endl;
    cout<< "After change function...\n""Second time calculation"<< endl;
    Change(x,y);                                                 //宣告函數
    cout<< "x + y = "<< Plus(x , y)<< endl;                  //丟入函數進行運算
    cout<< "x - y = "<< Minus( x , y)<< endl;
    cout<< "x * y = "<< Multiply( x , y)<< endl;
    cout<< "x / y = "<< Divide( x , y)<< endl;

    return 0;
}

int Plus(int x , int y)  //宣告函數以及x,y兩個變數
{
    return x + y;      //回傳值
}

int Minus(int x , int y)
{
    return x - y;
}

int Multiply(int x , int y)
{
    return x*y;
}

float Divide(float x , float y)
{
    return x/y;
}

void Change(int &x , int &y)
{
    int temp;    //宣告一個名稱為temp的暫時變數
    temp=x;
    x=y;
    y=temp;
}



