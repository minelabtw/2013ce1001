#include <iostream>

using namespace std;
int Plus(int x , int y);                                          //函數原型
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);
int main()
{
    int x,y;
    do{                                                           //重複輸入直到在正確範圍內
       cout<<"Please enter x ( >0 ): ";
       cin>>x;
    }while(x<=0 && cout<<"Out of range!"<<endl);
    do{
       cout<<"Please enter y ( >0 ): ";
       cin>>y;
    }while(y<=0 && cout<<"Out of range!"<<endl);

    cout<<"First time calculation"<<endl;                          //印出第一次結果

    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    cout<<"Second time calculation"<<endl;                         //印出第二次結果

    Change(x,y);
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    return 0;
}
int Plus(int x,int y)                                                    //函數內容
{
    return x+y;
}
int Minus(int x,int y)
{
    return x-y;
}
int Multiply(int x,int y)
{
    return x*y;
}
float Divide(float x,float y)
{
    return x/y;
}
void Change(int &x,int &y)
{
    int temp;
    temp=x;
    x=y;
    y=temp;
}
