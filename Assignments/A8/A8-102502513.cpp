#include <iostream>
using namespace std;
int Plus(int x , int y);  //加入函式原型宣告
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x=0, y=0;
    double Plus1=0, Minus1=0, Multiply1=0, Divide1=0;

    while(x<=0)
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;

        if(x<=0)
            cout << "Out of range!\n";
    }

    while(y<=0)
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;

        if(y<=0)
            cout << "Out of range!\n";
    }

    cout << "First time calculation\n";

    Plus1=Plus(x,y);  //呼叫函式
    cout << "x + y = " << Plus1 << endl;

    Minus1=Minus(x,y);
    cout << "x - y = " << Minus1 << endl;

    Multiply1=Multiply(x,y);
    cout << "x * y = " << Multiply1 << endl;

    Divide1=Divide(x,y);
    cout << "x / y = " << Divide1 << endl;

    cout << "After change function..." << endl << "Second time calculation" << endl;

    Change(x,y);
    Plus1=Plus(x,y);
    cout << "x + y = " << Plus1 << endl;

    Minus1=Minus(x,y);
    cout << "x - y = " << Minus1 << endl;

    Multiply1=Multiply(x,y);
    cout << "x * y = " << Multiply1 << endl;

    Divide1=Divide(x,y);
    cout << "x / y = " << Divide1 << endl;

    return 0;
}

int Plus(int x , int y)  //建立函式
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)
{
    int x2=0, y2=0;  //進行 x & y 的互換
    x2=y, y2=x;
    x=x2, y=y2;
}
