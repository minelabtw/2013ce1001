#include <iostream>
using namespace std;

int Plus(int x , int y);//宣告Plus函數
int Minus(int x , int y);//宣告Minus函數
int Multiply(int x , int y);//宣告Multiply函數
float Divide(float x , float y);//宣告Divide函數
void Change(int &x , int &y)//把x和y互換
{
    int a;//宣告型別為整數的a暫存x的值
    a = x;
    x = y;
    y = a;
}
int main()
{
    int x,y;//宣告型別為整數的x,y

    do//輸入大於0的整數x
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if(x<=0)
            cout << "Out of range!\n";
    }
    while(x<=0);
    do//輸入大於0的整數y
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if(y<=0)
            cout << "Out of range!\n";
    }
    while(y<=0);

    cout << "First time calculation\n";
    cout << "x + y = " << Plus(x,y) << endl;//呼叫Plus(x,y)並輸出結果
    cout << "x - y = " << Minus(x,y) << endl;//呼叫Minus(x,y)並輸出結果
    cout << "x * y = " << Multiply(x,y) << endl;//呼叫Multiply(x,y)並輸出結果
    cout << "x / y = " << Divide(x,y) << endl;//呼叫Divide(x,y)並輸出結果
    cout << "After change function...\n";
    Change(x,y);//呼叫Change(x,y)互換x,y
    cout << "Second time calculation\n";
    cout << "x + y = " << Plus(x,y) << endl;//呼叫Plus(x,y)並輸出結果
    cout << "x - y = " << Minus(x,y) << endl;//呼叫Minus(x,y)並輸出結果
    cout << "x * y = " << Multiply(x,y) << endl;//呼叫Multiply(x,y)並輸出結果
    cout << "x / y = " << Divide(x,y) << endl;//呼叫Divide(x,y)並輸出結果

    return 0;
}
int Plus(int x , int y)//計算x+y
{
    return x + y;
}
int Minus(int x , int y)//計算x-y
{
    return x - y;
}
int Multiply(int x , int y)//計算x*y
{
    return x * y;
}
float Divide(float x , float y)//計算x/y
{
    return x / y;
}
