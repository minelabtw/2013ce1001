#include<iostream>
using namespace std;
double Plus(int x,int y);//宣告加法函式
double Minus(int x,int y);//宣告減法函式
double Multiply(int x,int y);//宣告乘法函式
double Divide(double x,double y);//宣告除法函式
void Change(int &x,int &y);//宣告互換變數函式
int main()
{
    int x=0;
    int y=0;

    do
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
        if(x<=0)
            cout<<"Out of range!"<<endl;
    }
    while(x<=0);

    do
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
        if(y<=0)
            cout<<"Out of range!"<<endl;
    }
    while(y<=0);

    cout<<"First time calculation";
    cout<<"\nx + y = "<<Plus(x,y);//呼叫加法函式
    cout<<"\nx - y = "<<Minus(x,y);//呼叫減法函式
    cout<<"\nx * y = "<<Multiply(x,y);//呼叫乘法函式
    cout<<"\nx / y = "<<Divide(x,y);//呼叫除法函式
    cout<<"\nAfter change function..."<<"\nSecond time calculation";
    Change(x,y);//呼叫互換變數函式
    cout<<"\nx + y = "<<Plus(x,y);
    cout<<"\nx - y = "<<Minus(x,y);
    cout<<"\nx * y = "<<Multiply(x,y);
    cout<<"\nx / y = "<<Divide(x,y);

    return 0;
}
double Plus(int x,int y)//執行加法函式
{
    return x+y;
}
double Minus(int x,int y)//執行減法函式
{
    return x-y;
}
double Multiply(int x,int y)//執行乘法函式
{
    return x*y;
}
double Divide(double x,double y)//執行除法函式
{
    return x/y;
}
void Change(int &x,int &y)//執行互換變數函式
{
    int a=0;
    a=x;
    x=y;
    y=a;
}
