#include <iostream>
using namespace std;
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y); //宣告5個函式

int main()
{
    int a,b; //宣告2個整數變數
    do
    {
        cout << "Please enter x ( >0 ): ";
        cin >> a;
        if(a<1)
            cout << "Out of range!\n";
    }
    while(a<1); //輸入a值，a非正整數時輸出字串並重新輸入
    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> b;
        if(b<1)
            cout << "Out of range!\n";
    }
    while(b<1); //輸入b值，b非正整數時輸出字串並重新輸入

    cout << "First time calculation\nx + y = " << Plus(a,b) << "\nx - y = " << Minus(a,b) << "\nx * y = " << Multiply(a,b) << "\nx / y = " << Divide(a,b);
    cout << "\nAfter change function...\n";
    Change(a,b); //執行函式Change，代入a與b
    cout << "Second time calculation\nx + y = " << Plus(a,b) << "\nx - y = " << Minus(a,b) << "\nx * y = " << Multiply(a,b) << "\nx / y = " << Divide(a,b);

    return 0;
}

int Plus(int x , int y)
{
    return x+y; //回傳x+y值
}
int Minus(int x , int y)
{
    return x-y; //回傳x-y值
}
int Multiply(int x , int y)
{
    return x*y; //回傳x*y值
}
float Divide(float x , float y)
{
    return x/y; //回傳x/y值
}
void Change(int &x , int &y)
{
    int a=x;
    x=y;
    y=a; //交換x與y值
}
