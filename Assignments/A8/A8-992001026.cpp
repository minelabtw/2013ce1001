#include <iostream>

using namespace std;


// 相加函示
int Plus(int x , int y)
{
    return x+y ;
}
// 相減
int Minus(int x , int y)
{
    return x-y;
}
// 相乘
int Multiply(int x , int y)
{
    return x*y ;
}
//相除
float Divide(float x , float y)
{
    return x/y ;
}
// 兩者交換
void Change(int &x , int &y)
{
    int k ;
    k = x ;
    x = y ;
    y = k ;

}

int main()
{

    int x , y ;
    int plus , minus , mul ;
    float div ;

    cout << "Please enter x ( >0 ): " ;
    cin >> x ;

    while ( x < 1)
    {
        cout << "Out of range!" << endl ;
        cout << "Please enter x ( >0 ): " ;
        cin >> x ;
    }

    cout << "Please enter y ( >0 ): " ;
    cin >> y ;

    while ( y < 1)
    {
        cout << "Out of range!" << endl ;
        cout << "Please enter y ( >0 ): " ;
        cin >> y ;
    }

    plus = Plus ( x , y ) ;
    minus = Minus ( x , y ) ;
    mul = Multiply ( x , y ) ;
    div = Divide ( x , y ) ;
    // 第一次輸出
    cout << "First time calculation" << endl ;
    cout << "x + y = " << plus << endl ;
    cout << "x - y = " << minus << endl ;
    cout << "x * y = " << mul<< endl ;
    cout << "x / y = " << div << endl ;

    //交換 x y
    Change(x, y);

    plus = Plus ( x , y ) ;
    minus = Minus ( x , y ) ;
    mul = Multiply ( x , y ) ;
    div = Divide ( x , y ) ;

    // 第二次輸出
    cout << "After change function..." << endl ;
    cout << "Second time calculation " << endl ;
    cout << "x + y = " << plus << endl ;
    cout << "x - y = " << minus << endl ;
    cout << "x * y = " << mul<< endl ;
    cout << "x / y = " << div << endl ;


    return 0 ;
}
