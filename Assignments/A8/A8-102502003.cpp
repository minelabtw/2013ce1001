#include<iostream>
#include<cmath>
using namespace std;

int Plus(int x, int y);
int Minus(int x, int y);
int Multiply(int x, int y);
float Divide(float x, float y);
void Change(int &x, int &y);

int main()
{
    int a=0;
    int b=0;

    while(a<=0)  //使輸入整數恆正
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>a;
        if(a<=0)
            cout<<"Out of range!"<<endl;
    }

    while(b<=0)
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>b;
        if(b<=0)
            cout<<"Out of range!"<<endl;
    }

    cout<<"First time calculation"<<endl;  //第一次計算加減乘除
    cout<<"x + y = "<<Plus(a,b)<<endl;
    cout<<"x - y = "<<Minus(a,b)<<endl;
    cout<<"x * y = "<<Multiply(a,b)<<endl;
    cout<<"x / y = "<<Divide(a,b)<<endl;

    cout<<"After change function..."<<endl<<"Second time calculation"<<endl;

    Change(a,b); //互換變數
    cout<<"x + y = "<<Plus(a,b)<<endl;  //第二次計算加減乘除
    cout<<"x - y = "<<Minus(a,b)<<endl;
    cout<<"x * y = "<<Multiply(a,b)<<endl;
    cout<<"x / y = "<<Divide(a,b)<<endl;


    return 0;

}

int Plus(int x, int y)  //加法
{
    int z=0;
    z=x+y;

    return z;  //回傳值z
}

int Minus(int x, int y)  //減法
{
    int z=0;
    z=x-y;

    return z;
}

int Multiply(int x, int y)  //乘法
{
    int z=0;
    z=x*y;

    return z;
}

float Divide(float x, float y)  //除法
{
    float z=0;
    z=x/y;

    return z;
}

void Change(int &x, int &y)  //變數互換
{
    int z=x;
    x=y;
    y=z;
}
