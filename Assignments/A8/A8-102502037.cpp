#include <iostream>
#include <iomanip>
using namespace std;
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);
int main()
{
    int x,y,a,b,c;//輸入值
    float d;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)
    {cout<<"Out of range!"<<endl;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    }

    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)
    {cout<<"Out of range!"<<endl;
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    }
    a=Plus(x,y);
    b=Minus(x,y);
    c=Multiply(x,y);
    d=Divide(x,y);
    cout<<"x + y = "<<a<<endl;
    cout<<"x - y = "<<b<<endl;
    cout<<"x * y = "<<c<<endl;
    cout<<"x / y = "<<d<<endl;
    cout<<"After change function..."<<endl<<"Second time calculation"<<endl;
    Change(x,y);
    a=Plus(x,y);
    b=Minus(x,y);
    c=Multiply(x,y);
    d=Divide(x,y);
    cout<<"x + y = "<<a<<endl;
    cout<<"x - y = "<<b<<endl;
    cout<<"x * y = "<<c<<endl;
    cout<<"x / y = "<<d<<endl;
}
int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)
{
    int a,b;
    a=x;
    x=y;
    y=a;
}
