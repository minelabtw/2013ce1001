#include <iostream>
using namespace std;

int Plus(int x,int y){//計算x+y的函式 
    return x+y;
}

int Minus(int x,int y){//計算x-y的函式
    return x-y;
}

int Multiply(int x,int y){//計算x*y的函式
    return x*y;
}

float Divide(float x,float y){//計算x/y的函式
    return x/y;
}

void Change(int &x,int &y){//將x,y值交換的函式 
    swap(x,y);
}

int main(){
    int num1,num2;//宣告整數,num1為輸入的x,num2為輸入的y 
    do{
        cout << "Please enter x ( >0 ): ";
        cin >> num1;
    }while(num1<=0 && cout << "Out of range!\n");//處理輸入的x,超出範圍則要求重新輸入 
    do{
        cout << "Please enter y ( >0 ): ";
        cin >> num2;
    }while(num2<=0 && cout << "Out of range!\n");//處理輸入的y,超出範圍則要求重新輸入
    cout << "First time calculation\n"
         << "x + y = " << Plus(num1,num2) << endl
         << "x - y = " << Minus(num1,num2) << endl
         << "x * y = " << Multiply(num1,num2) << endl
         << "x / y = " << Divide(num1,num2) << endl;//利用函式,並且輸出結果 
    Change(num1,num2);//交換兩數之值 
    cout << "After change function...\n"
         << "Second time calculation\n"
         << "x + y = " << Plus(num1,num2) << endl
         << "x - y = " << Minus(num1,num2) << endl
         << "x * y = " << Multiply(num1,num2) << endl
         << "x / y = " << Divide(num1,num2) << endl;//利用函式,並且輸出結果 
    return 0;
}
