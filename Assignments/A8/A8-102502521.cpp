#include<iostream>

using namespace std;

int Plus(int x , int y);    //function prototype
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x;    //宣告變數
    int y;

    do    //判斷是否為合理值
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
        if(x<=0)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(x<=0);

    do
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
        if(y<=0)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(y<=0);

    cout<<"First time calculation"<<endl;

    cout<<"x + y = "<<Plus(x,y)<<endl;    //呼叫函數
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;

    Change(x,y);    //呼叫函數

    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;

    return 0;
}

int Plus(int x , int y)    //定義函數
{
    return x+y;
}

int Minus(int x , int y)
{
    return x-y;
}

int Multiply(int x , int y)
{
    return x*y;
}

float Divide(float x , float y)
{
    return x/y;
}

void Change(int &x , int &y)
{
    int z;

    z=x;
    x=y;
    y=z;
}

