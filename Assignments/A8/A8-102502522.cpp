#include<iostream>
#include<math.h>
using namespace std;

int Plus(int x , int y)//相加方程式
{
    return x+y;
}
 int Minus(int x , int y)//相減方程式
 {
     return x-y;

 }
 int Multiply(int x , int y)//相乘方程式
 {
    return x*y;
 }
 float Divide(float x , float y)//相除方程式
 {
     return x/y;
 }
 void Change(int &x , int &y)//x y交換的方程式
 {
     int z;
     z=y;
     y=x;
     x=z;
 }
main()
{
    int x;
    int y;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"x+y="<<Plus(x,y)<<endl;
    cout<<"x-y="<<Minus(x,y)<<endl;
    cout<<"x*y="<<Multiply(x,y)<<endl;
    cout<<"x/y="<<Divide(x,y)<<endl;

    Change(x,y);//交換
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    cout<<"x+y="<<Plus(x,y)<<endl;
    cout<<"x-y="<<Minus(x,y)<<endl;
    cout<<"x*y="<<Multiply(x,y)<<endl;
    cout<<"x/y="<<Divide(x,y);

    return 0;
}
