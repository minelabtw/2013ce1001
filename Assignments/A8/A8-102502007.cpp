#include<iostream>
using namespace std;
int Plus(int x , int y)
{
    cout<<"x + y = ";
    return x+y;
}//set up a function to compute the result of sum
int Minus(int x , int y)
{
    cout<<"x - y = ";
    return x-y;
}//set up a function to compute the result of subtraction
int Multiply(int x , int y)
{
    cout<<"x * y = ";
    return x*y;
}//set up a function to compute the result of multiplication
float Divide(float x , float y)
{
    cout<<"x / y = ";
    return x/y;
}//set up a function to compute the result of division
void Change(int &x , int &y)
{
    swap(x,y);
}//set up a function to exchange the value of x and y
int main()
{
    int x,y;
    do
    {
        cout<<"Please enter x( >0 ): ";
        cin>>x;
        if(x<=0)
            cout<<"Out of range!"<<endl;
    }
    while(x<=0);//loop until the correct value is input
    do
    {
        cout<<"Please enter y( >0 ): ";
        cin>>y;
        if(y<=0)
            cout<<"Out of range!"<<endl;
    }
    while(y<=0);//loop until the correct value is input
    cout<<"First time calculation"<<endl;
    cout<<Plus(x,y)<<endl;
    cout<<Minus(x,y)<<endl;
    cout<<Multiply(x,y)<<endl;
    cout<<Divide(x,y)<<endl;
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    Change(x,y);//exchange the value of x and y
    cout<<Plus(x,y)<<endl;
    cout<<Minus(x,y)<<endl;
    cout<<Multiply(x,y)<<endl;
    cout<<Divide(x,y)<<endl;
    cout<<x;
    cout<<y;
    return 0;
}
