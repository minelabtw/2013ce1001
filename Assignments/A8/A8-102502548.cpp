#include<iostream>

using namespace std ;

int Plus (int x,int y)//宣告函式
{
    return x+y ;//回傳x*y的值
}

int Minus (int x, int y )
{
    return x-y ;
}

int Multiply (int x, int y)
{
    return x*y ;
}

float Divide (float x, float y)//用浮點數做宣告
{
    return x/y ;
}

void Change(int &x, int &y, int c) ;//宣告函式，並將丟入的參數值命名為x,y,c

int main ()
{
    int x=0,y=0, c=0 ;

    while (1)//使用迴圈
    {
        cout << "Please enter x ( >0 ): " ;

        cin >> x ;

        if (x<=0)
        {
            cout << "Out of range!\n" ;
        }

        else break ;
    }

    while (1)
    {
        cout << "Please enter y ( >0 ): " ;

        cin >> y ;

        if (y<=0)
        {
            cout << "Out of range!\n" ;
        }

        else break ;
    }

    cout << "First time calculation\n" ;//在螢幕上輸出

    cout << "x + y = " << Plus(x,y) << endl ;//呼叫函式，並用endl換行

    cout << "x - y = " << Minus(x,y) << endl ;

    cout << "x * y = " << Multiply(x,y) << endl ;

    cout << "x / y = " << Divide(x,y) << endl ;

    cout << "After change function...\n" << "Second time calculation\n" ;

    Change(x,y,c) ;

    cout << "x + y = " << Plus(x,y) << endl ;

    cout << "x - y = " << Minus(x,y) << endl ;

    cout << "x * y = " << Multiply(x,y) << endl ;

    cout << "x / y = " << Divide(x,y) << endl ;
}

void Change(int &x, int &y, int c)//函式的內容
{
    c=x ;

    x=y ;

    y=c ;
}
