#include <iostream>
using namespace std;

int Plus ( int x , int y ); //function prototype
int Minus ( int x , int y );
int Multiply ( int x , int y );
float Divide ( int x , int y );
void Change ( int &x , int &y );

int main ()
{
    int x = 0; //宣告型別為 整數(int) 的第一個(x)和第二個變數(y)，並初始化其數值為0。
    int y = 0;
    cout << "Please enter x ( >0 ): " ; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cin >> x ;
    while ( x <= 0 ) //用while迴圈檢驗使用者的輸入是否合乎標準，若不符，則要求其重新輸入。
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): " ;
        cin >> x ;
    }
    cout << "Please enter y ( >0 ): " ;
    cin >> y ;
    while ( y <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): " ;
        cin >> y ;
    }
    cout << "First time calculation" << endl;
    cout << "x" << " + " << "y" << " = " << Plus( x , y ) << endl; //呼叫加法函數
    cout << "x" << " - " << "y" << " = " << Minus( x , y ) << endl; //呼叫減法函數
    cout << "x" << " * " << "y" << " = " << Multiply( x , y ) << endl; //呼叫乘法函數
    cout << "x" << " / " << "y" << " = " << Divide( x , y ) << endl; //呼叫除法函數
    cout << "After change function..." << endl;
    Change( x , y ); //讓 x 和 y 的值交換
    cout << "Second time calculation" << endl;
    cout << "x" << " + " << "y" << " = " << Plus( x , y ) << endl;
    cout << "x" << " - " << "y" << " = " << Minus( x , y ) << endl;
    cout << "x" << " * " << "y" << " = " << Multiply( x , y ) << endl;
    cout << "x" << " / " << "y" << " = " << Divide( x , y ) << endl;

    return 0;
}

int Plus ( int x , int y ) //加法函數
{
    return x + y;
}

int Minus ( int x , int y ) //減法函數
{
    return x - y;
}

int Multiply ( int x , int y ) //乘法函數
{
    return x * y;
}

float Divide ( int x , int y ) //除法函數，但要記得先將x和y的型別轉換為float
{
    return ( float )x / ( float )y;
}

void Change ( int &x , int &y ) //交換函數
{
    int z = 0; //宣告型別為 整數(int) 的變數(z)，並初始化其數值為0，用來暫存x原本的值。
    z = x;
    x = y;
    y = z;
}


