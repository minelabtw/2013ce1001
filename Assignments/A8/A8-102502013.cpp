#include <iostream>
#include<iomanip>
using namespace std;

int Plus(int x , int y)//相加函式
{
    return x + y;
}
int Minus(int x , int y)//相減函式
{
    return x - y;
}
int Multiply(int x , int y)//相乘函式
{
    return x * y;
}
float Divide(float x , float y)//相除函式
{
    x = static_cast< float >(x);//轉換型別
    y = static_cast< float >(y);//轉換型別
    return x / y;
}
void Change(int &x , int &y)//交換x,y的值
{
    int a = x;
    x = y;
    y = a;
}
int main()
{
    int x = 0;
    int y = 0;
    int a = 0;
    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while (x<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }
    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while (y<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }
    cout << "First time calculation" << endl;
    cout << Plus(x,y) << endl;//呼叫相加函式
    cout << Minus(x,y) << endl;//呼叫相減函式
    cout << Multiply(x,y) << endl;//呼叫相乘函式
    cout << Divide(x,y) << endl;//呼叫相除函式
    cout << "After change function..." << endl;
    Change(x,y);//交換x,y的值
    cout << "Second time calculation" << endl;
    cout << Plus(x,y) << endl;//呼叫相加函式
    cout << Minus(x,y) << endl;//呼叫相減函式
    cout << Multiply(x,y) << endl;//呼叫相乘函式
    cout << Divide(x,y) << endl;//呼叫相除函式
    return 0;
}
