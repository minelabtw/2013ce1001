#include <iostream>
using namespace std;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main(){

    int x, y = 0;

    do{
        cout << "Please enter x( >0 ): ";
        cin >> x;
        if(x<=0)
            cout << "Out of range!" << endl;
    }while(x<=0);

    do{
        cout << "Please enter y( >0 ): ";
        cin >> y;
        if(y<=0)
            cout << "Out of range!" << endl;
    }while(y<=0);

    cout << "First time calculation" << endl;

    Plus(x,y);
    Minus(x,y);
    Multiply(x,y);
    Divide(x,y);

    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl;

    Change(x,y);
    Plus(x,y);
    Minus(x,y);
    Multiply(x,y);
    Divide(x,y);

    return 0;
}

int Plus(int x , int y){
    cout << "x + y = " << x+y << endl;
}
int Minus(int x , int y){
    cout << "x - y = " << x-y << endl;
}
int Multiply(int x , int y){
    cout << "x * y = " << x*y << endl;
}
float Divide(float x , float y){
    cout << "x / y = " << x/y << endl;
}
void Change(int &x , int &y){
    int temp = x;
    x = y;
    y = temp;
}
