#include <iostream>
using namespace std;

int Plus(int x , int y); //命名函式
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x;
    int y;

    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while(x<=0) //刪去不符合範圍的數
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }

    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while(y<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl;
    Change (x,y); //用Change函式使得x和y數值交換
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
}

int Plus(int x , int y) //定義Plus函式
{
    return x+y; //回傳為x+y
}

int Minus(int x , int y) //定義Minus函式
{
    return x-y;
}

int Multiply(int x , int y) //定義Multiply函式
{
    return x*y;
}
float Divide(float x , float y) //定義Divide函式
{
    return x/y;
}

void Change(int &x , int &y)
{
    int z; //設計一個新變數z
    z = x; //讓z=x儲存原本x的值
    x = y; //讓x變成y的值
    y = z; //讓y變成z的值 即是原本x的值
}
