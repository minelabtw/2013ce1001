#include <iostream>
using namespace std;

int Plus(int x , int y)           //計算x,y相加的函式
{
    return x+y;
}
int Minus(int x , int y)          //計算x,y相減的函式
{
    return x-y;
}
int Multiply(int x , int y)       //計算x,y相乘的函式
{
    return x*y;
}
float Divide(float x , float y)   //計算x,y相除的函式
{
    return x/y;
}
void Change(int &x , int &y)      //將x,y值互相交換的函式
{
    int cx=x;
    int cy=y;
    y=cx;
    x=cy;
}
void Output(int &x, int &y)       //要輸出的四個等式
{
    cout << "x + y = " << Plus(x,y) << endl;              //輸出字串，叫出Plus函式，運算相加結果
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
}

int main()
{
    int x=0;                 //宣告一變數x，令其初始值0
    int y=0;                 //宣告一變數y，令其初始值0

    cout << "Please enter x ( >0 ): ";                 //輸出字串
    cin >> x;                                          //輸入x
    while (x<=0)                              //while迴圈，當x小於等於0，輸出Out of range，並重新輸入x
    {
        cout << "Out of range!\n" << "Please enter x ( >0 ): ";
        cin >> x;
    }

    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while (y<=0)
    {
        cout << "Out of range!\n" << "Please enter y ( >0 ): ";
        cin >> y;
    }

    cout << "First time calculation\n";                  //第一次的運算
    Output(x,y);                                         //呼叫輸出函式
    cout << "Second time calculation\n";                 //第二次的運算
    Change(x,y);                                         //呼叫x,y互換的函式
    Output(x,y);                                         //輸出第二次運算結果

    return 0;
}
