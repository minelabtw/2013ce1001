#include <iostream>
using namespace std;
int Plus (int x , int y);//宣告Plus函式
int Minus (int x , int y);//宣告Minus函式
int Multiply (int x , int y);//宣告Multiply函式
float Divide (float x , float y);//宣告Divide函式
void Change(int &x , int &y);//宣告Change函式
int main ()
{
    int x =0;//宣告變數並=0
    int y =0;//宣告變數並=0
    cout << "Please enter x ( >0 ): ";//提示輸入
    cin >> x;//輸入
    while (x<=0)//判斷條件
    {
        cout << "Out of range!" << endl << "Please enter x ( >0 ): ";//提示輸入
        cin >> x;//輸入
    }
    cout << "Please enter y ( >0 ): ";//提示輸入
    cin >> y;//輸入
    while (y<=0)//判斷條件
    {
        cout << "Out of range!" << endl << "Please enter y ( >0 ): ";//提示輸入
        cin >> y;//輸入
    }
    cout << "First time calculation" << endl;//輸出題目要求
    cout << "x + y = " << Plus (x ,y) << endl;//輸出加
    cout << "x - y = " << Minus (x ,y) << endl;//輸出減
    cout << "x * y = " << Multiply (x ,y) << endl;//輸出乘
    cout << "x / y = " << Divide (x ,y) << endl;//輸出除
    Change(x ,y);//變換
    cout << "After change function..." << endl << "Second time calculation" << endl;//輸出題目
    cout << "x + y = " << Plus (x ,y) << endl;//輸出加
    cout << "x - y = " << Minus (x ,y) << endl;//輸出減
    cout << "x * y = " << Multiply (x ,y) << endl;//輸出乘
    cout << "x / y = " << Divide (x ,y) << endl;//輸出除
    return 0;
}
int Plus (int x , int y)//Plus函式
{
    return x+y;
}
int Minus (int x , int y)//Minus函式
{
    return x-y;
}
int Multiply (int x , int y)//Multiply函式
{
    return x*y;
}
float Divide (float x , float y)//Divide函式
{
    return x/y;
}
void Change(int &x , int &y)//Change函式
{
    int z =0;
    z =x;
    x =y;
    y =z;
}
