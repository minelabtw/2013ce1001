#include <iostream>

using namespace std;


int Plus(int x,int y);
int Minus(int x,int y);
int Multiply(int x,int y);
float Divide(float x,float y);
void Change(int &x,int &y);

int main()
{
    int inputX=0,inputY=0;

    do
    {
        cout << "Please enter x ( >0 ): " ;
        cin>>inputX;
        if(inputX<=0)
        {
            cout << "Out of range!" << endl;
        }

    }while(inputX<=0);

     do
    {
        cout << "Please enter y ( >0 ): " ;
        cin>>inputY;
        if(inputY<=0)
        {
            cout << "Out of range!" << endl;
        }

    }while(inputY<=0);
    //運算第一次
    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(inputX,inputY)<< endl;
    cout << "x - y = " << Minus(inputX,inputY)<< endl;
    cout << "x * y = " << Multiply(inputX,inputY)<< endl;
    cout << "x / y = " << Divide(inputX,inputY)<< endl;

    //交換X&Y
    cout << "After change function..." << endl;
    Change(inputX,inputY);
    //運算第二次
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(inputX,inputY)<< endl;
    cout << "x - y = " << Minus(inputX,inputY)<< endl;
    cout << "x * y = " << Multiply(inputX,inputY)<< endl;
    cout << "x / y = " << Divide(inputX,inputY)<< endl;



    return 0;
}

int Plus(int x,int y)
{
    int sum=0;
    sum=x+y;

    return sum;
}
int Minus(int x,int y)
{
    int sub=0;
    sub=x-y;
    return sub;
}
int Multiply(int x,int y)
{
    int mul=0;
    mul=x*y;
    return mul;
}
float Divide(float x,float y)
{
    double div=0;
    div=x/y;
    return div;
}
void Change(int &x,int &y)
{
    int temp;
    temp=x;
    x=y;
    y=temp;

}

