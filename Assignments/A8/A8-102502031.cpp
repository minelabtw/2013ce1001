#include <iostream>

using namespace std;

void Data_Input(string, float, int&);  //used to prompt user for data and write the value into data
int Plus(int, int);                    //calculate plus
int Minus(int, int);                   //calculate minus
int Multiply(int, int);                //calculate mutiply
float Divide(float, float);            //calculate divide
void Group (int, int);                 //output the result for all calculate once
void Change(int &, int &);             //change the value of two data

int main()
{
    int x=0;
    int y=0;
    float a=0;  //varible for check in function Data_input

    //prompt user for data and write the value into data
    Data_Input("x", a, x);
    Data_Input("y", a, y);

    //output the result
    cout << "First time calculation" << endl;
    Group(x, y);
    cout << "After change function..." << endl;
    Change(x, y);
    cout << "Second time calculation" <<endl;
    Group(x, y);

    return 0;
}

void Data_Input(string Data_Name, float Memory, int &Data)
{
    do
    {
        cout << "Please enter " << Data_Name << " ( >0 ): ";
        cin >> Memory;
        Data=Memory;
    }
    while((Data<=0||static_cast<float>(Data)!=Memory) && cout << "Out of range!" << endl);
}

//calcuate
int Plus(int Data_x, int Data_y)
{
    return Data_x+Data_y;
}
int Minus(int Data_x, int Data_y)
{
    return Data_x-Data_y;
}
int Multiply(int Data_x, int Data_y)
{
    return Data_x*Data_y;
}
float Divide(float Data_x, float Data_y)
{
    return Data_x/Data_y;
}

void Group(int Data_x, int Data_y)
{
    cout << "x + y = " << Plus(Data_x, Data_y) << endl;
    cout << "x - y = " << Minus(Data_x, Data_y) << endl;
    cout << "x * y = " << Multiply(Data_x, Data_y) << endl;
    cout << "x / y = " << Divide(static_cast<float>(Data_x), static_cast<float>(Data_y)) << endl;  //float divide float will be float
}

void Change(int &Data_x, int &Data_y)
{
    Data_x=Data_x+Data_y;  //store another data by their sum
    Data_y=Data_x-Data_y;
    Data_x=Data_x-Data_y;
}

