#include<iostream>
using namespace std;
int Plus(int, int );                                    //Line 3~7: Declare the functions.
int Minus(int, int );
int Multiply(int, int);
float Divide(float, float);
void Change(int &, int &);
int main()
{
    int x,y;
    int a1,a2,a3;
    float a4;
    do                                                  //Line 13~28: Let user input the x and y.
    {
        cout<<"Please enter x (>0): ";
        cin>>x;
        if(x<=0)
            cout<<"Out of range!\n";
    }
    while(x<=0);
    do
    {
        cout<<"Please enter y (>0): ";
        cin>>y;
        if(y<=0)
            cout<<"Out of range!\n";
    }
    while(y<=0);
    cout<<"Firsst time calculation\n";
    a1=Plus(x,y);                                         //Line 30~47: Let functions caculate and print the results.
    a2=Minus(x,y);
    a3=Multiply(x,y);
    a4=Divide(x,y);
    cout<<"x + y = "<<a1<<endl;
    cout<<"x - y = "<<a2<<endl;
    cout<<"x * y = "<<a3<<endl;
    cout<<"x / y = "<<a4<<endl;
    Change(x,y);
    cout<<"After change function...\n"<<"Second time calculation\n";
    a1=Plus(x,y);
    a2=Minus(x,y);
    a3=Multiply(x,y);
    a4=Divide(x,y);
    cout<<"x + y = "<<a1<<endl;
    cout<<"x - y = "<<a2<<endl;
    cout<<"x * y = "<<a3<<endl;
    cout<<"x / y = "<<a4<<endl;
    return 0;
}
int Plus(int x, int y)                                      //Line 50~end: Telling computer the means of functions.
{
    return x+y;
}
int Minus(int x, int y)
{
    return x-y;
}
int Multiply(int x, int y)
{
    return x*y;
}
float Divide(float x, float y)
{
    return x/y;
}
void Change(int &x,int &y)
{
    int control=x;
    x=y;
    y=control;
}
