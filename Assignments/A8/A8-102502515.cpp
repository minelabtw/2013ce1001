#include <iostream>

using namespace std;

int Plus(int x,int y)//function prototype of plus
{
    return x+y;//return the answer
}

int Minus(int x,int y)//function prototype of minus
{
    return x-y;//return the answer
}

int Multiply(int x,int y)//function prototype of multiply
{
    return x*y;//return the answer
}

float Divide(float x,float y)//function prototype of divide
{
    return x/y;//return the answer
}

void formula(int x ,int y)//use void to repeat the motion of cout
{
    cout << "x + y = " << Plus(x,y) << endl;//recall the function Plus and get the answer.
    cout << "x - y = " << Minus(x,y) << endl;//recall the function Minus and get the answer.
    cout << "x * y = " << Multiply(x,y) << endl;//recall the function Multiply and get the answer.
    cout << "x / y = " << Divide(x,y) << endl;//recall the function Divide and get the answer.
}

void Change(int &x,int &y)//function prototype of exchanging the two value
{
    int i=x;//store the primitive value of x
    x=y;
    y=i;
}

int main()
{
    int x;
    int y;

    cout << "Please enter x( >0 ): ";
    cin  >> x;
    while (x<=0)//make the input meet the demand of the assignment
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x( >0 ): ";
        cin  >> x;
    }

    cout << "Please enter y( >0 ): ";
    cin  >> y;
    while (y<=0)//same as x
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y( >0 ): ";
        cin  >> y;
    }

    cout << "First time calculation" << endl;
    formula(x,y);//operate the function

    cout << "After change function..." << endl;
    Change(x,y);//recall the function to exchange the values

    cout << "Second time calculation" << endl;
    formula(x,y);//operate the function

    return 0;
}
