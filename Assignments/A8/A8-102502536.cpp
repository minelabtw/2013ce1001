#include<iostream>

using namespace std;

int Plus(int x , int y)//宣告加法函式
{
    int temp=0;

    temp=x+y;

    return temp;
}

int Minus(int x , int y)//宣告減法函式
{
    int temp=0;

    temp=x-y;

    return temp;
}

int Multiply(int x , int y)//宣告乘法函式
{
    int temp=0;

    temp=x*y;

    return temp;
}

float Divide(float x , float y)//宣告除法函式
{
    float temp=0;

    temp=x/y;

    return temp;
}

void Change(int &x , int &y)//宣告函式交換變數
{
    int temp;

    temp=x;
    x=y;
    y=temp;
}

int main()
{
    int x=0,y=0;//宣告變數

    do
    {
        cout << "Please enter x ( >0 ): ";//輸出字串
        cin  >> x;//輸入值給變數x

        if(x<=0)//x<=0時輸出字串
            cout << "Out of range!" << endl;
    }
    while(x<=0);//x<=0時重複

    do
    {
        cout << "Please enter y ( >0 ): ";//輸出字串
        cin  >> y;//輸入值給變數x

        if(y<=0)//y<=0時輸出字串
            cout << "Out of range!" << endl;
    }
    while(y<=0);//y<=0時重複

    cout << "First time calculation" << endl;//輸出字串
    cout << "x + y = " << Plus(x , y) << endl;//輸出加法
    cout << "x - y = " << Minus(x , y) << endl;//輸出減法
    cout << "x * y = " << Multiply(x , y) << endl;//輸出乘法
    cout << "x / y = " << Divide(x , y) << endl;//輸出除法

    cout << "After change function...\nSecond time calculation" << endl;//輸出字串
    Change(x , y);//交換變數數值
    cout << "x + y = " << Plus(x , y) << endl;//輸出加法
    cout << "x - y = " << Minus(x , y) << endl;//輸出減法
    cout << "x * y = " << Multiply(x , y) << endl;//輸出乘法
    cout << "x / y = " << Divide(x , y) << endl;//輸出除法

    return 0;
}
