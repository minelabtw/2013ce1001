#include<iostream>

using namespace std;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x;                                      //輸入x
    cout << "Please enter x ( >0 ): ";
    cin >> x;

    while( x <= 0 )                             //判斷x>0
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }

    int y;                                      //輸入y
    cout << "Please enter y ( >0 ): ";
    cin >> y;

    while( y <= 0 )                             //判斷y>0
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }

    cout << "First time calculation" << endl;   //第一次運算
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
    cout << "After change function..." << endl;

    Change(x,y);                                //使用將x與y值互換的函式
    cout << "Second time calculation" << endl;  //第二次運算
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    return 0;
}

int Plus(int x , int y)                         //加法函式
{
    return x + y;
}

int Minus(int x , int y)                        //減法函式
{
    return x - y;
}

int Multiply(int x , int y)                     //乘法函式
{
    return x * y;
}

float Divide(float x , float y)                 //除法函式
{
    return x / y;
}

void Change(int &x , int &y)                    //兩值互換函式
{
    int z;
    z=x;
    x=y;
    y=z;
}
