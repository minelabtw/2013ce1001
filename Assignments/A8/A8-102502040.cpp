#include<iostream>
using namespace std;

int x=0;
int y=0;
int Plus(int , int );   //輸入4個funtion,還有一個交換數值的funtion
int Minus(int  , int );
int Multiply(int  , int );
float Divide(float  , float );
void Change(int &x , int &y);

int main()
{
    cout << "Please enter x ( >0 ): ";  //輸入兩整數,皆須大於零
    cin >> x;
    while (x<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }
    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while (y<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> y;
    }
    cout << "First time calculation"<< endl;    //輸出字元,計算依序加減乘除
    cout << "x + y = " ;
    cout << Plus(x,y) << endl;
    cout << "x - y = " ;
    cout << Minus(x,y) << endl;
    cout << "x * y = " ;
    cout << Multiply(x,y) << endl;
    cout << "x / y = " ;
    cout << Divide(x,y) << endl;
    cout << "After change function..." << endl;
    Change (x,y);   //交換數值
    cout << "Second time calculation" << endl;
    cout << "x + y = " ;
    cout << Plus(x,y) << endl;
    cout << "x - y = " ;
    cout << Minus(x,y) << endl;
    cout << "x * y = " ;
    cout << Multiply(x,y) << endl;
    cout << "x / y = " ;
    cout << Divide(x,y) << endl;

    return 0;
}
int Plus(int x , int y) //定義funtion
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)    //設定funtion數值交換
{
    int k;
    k=x;
    x=y;
    y=k;
}
