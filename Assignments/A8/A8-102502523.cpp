#include<iostream>
using namespace std;

double Plus(double x , double y);
double Minus(double x , double y);
double Multiply(double x , double y);
double Divide(double x , double y);
void Change(double &x , double &y);
int main(){
double x=0;
double y=0;
while(x<=0){
cout<<"Please enter x ( >0 ): ";
cin>>x;
if(x<0){cout<<"Out of range!\n";}
else {break;}
}
while(y<=0){
    cout<<"Please enter y ( >0 ): ";
 cin>>y;
 if(y<0){cout<<"Out of range!\n";}
else {break;}
 }
 cout<<"First time calculation"<<endl;
cout<<"x + y = "<<Plus(x,y)<<endl;
cout<<"x - y = "<<Minus(x,y)<<endl;
cout<<"x * y = "<<Multiply(x,y)<<endl;
cout<<"x / y = "<<Divide(x,y)<<endl;
cout<<"After change function..."<<endl<<"Second time calculation"<<endl;
Change(x,y);
cout<<"x + y = "<<Plus(x,y)<<endl;
cout<<"x - y = "<<Minus(x,y)<<endl;
cout<<"x * y = "<<Multiply(x,y)<<endl;
cout<<"x / y = "<<Divide(x,y)<<endl;
return 0;
}
double Plus(double x , double y){
double z=x+y;
return z;
}
double Minus(double x , double y){
double z=x-y;
return z;}
double Multiply(double x , double y){
double z=x*y;
return z;}
double Divide(double x , double y){
double z=x/y;
return z;}
void Change(double &x , double &y){
double a;
double b;
a=x;
b=y;
x=b;
y=a;
}
