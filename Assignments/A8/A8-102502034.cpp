#include<iostream>
using namespace std;
int Plus(int x , int y);//宣告函式
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);
int main()
{   int x , y;//宣告變數
    cout << "Please enter x ( >0 ): " ;//輸入輸出題目所需
    cin >> x ;
    while (x<=0)//當x<=0時
    {
        cout <<"Out of range!"<<endl;//輸出Out of range
        cout << "Please enter x ( >0 ): ";//輸出please enter x ( >0 ):
        cin >> x;//輸入x
    }
    cout << "Please enter y ( >0 ): " ;//輸出輸入題目所需
    cin >> y ;
    while (y<=0)//當y<=0時
    {
        cout <<"Out of range!"<<endl;//輸出out of range!
        cout << "Please enter y ( >0 ): ";//輸出Please enter y ( >0 ):
        cin >> y;//輸入y
    }
    cout << "First time calculation"<<endl ;//輸出First time caculation
    cout << "x + y = " << Plus(x , y) << endl;//輸出x + y = 多少
    cout <<"x - y = " << Minus(x , y) << endl;//輸出x-y=多少
    cout <<"x * y = " << Multiply(x,y) <<endl;//輸出x*y=多少
    cout <<"x / y = " << Divide(x,y)<<endl;//輸出x/y=多少
    cout <<"After change function..."<<endl;//輸出After change function...
    cout <<"Second time calculation"<<endl;//輸出Second time calculation
    Change(x,y);//呼叫Change此函式
    cout << "x + y = " << Plus(x , y) << endl;//輸出進行變換後的結果
    cout <<"x - y = " << Minus(x , y) << endl;
    cout <<"x * y = " << Multiply(x,y) <<endl;
    cout <<"x / y = " << Divide(x,y)<<endl;
    return 0;
}
int Plus(int x , int y)//計算x+y
{
    return x+y ;
}
int Minus(int x , int y)//計算x-y
{
    return x-y;
}
int Multiply(int x , int y)//計算x*y
{
    return x*y;
}
float Divide(float x , float y)//計算x/y
{
    return x/y ;
}
void Change(int &x , int &y)//變換x,y
{
    int a=x ;
    x=y;
    y=a ;
}
