#include<iostream>
using namespace std;

int Plus(int x , int y)   //設定需要的function
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)
{
    int a,b;
    a=x,b=y;
    x=b,y=a;
}

int main ()
{
    int x,y;
    int answer1,answer2,answer3;
    float answer4;

    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)//判定x的結果是否<0
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)//判定=y的結果是否<0
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }

    answer1=Plus(x,y);
    answer2=Minus(x,y);
    answer3=Multiply(x,y);
    answer4=Divide(x,y);

    cout<<"First time calculation"<<endl;//輸出運算結果
    cout<<"x + y = "<<answer1<<endl;
    cout<<"x - y = "<<answer2<<endl;
    cout<<"x * y = "<<answer3<<endl;
    cout<<"x / y = "<<answer4<<endl;

    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    Change(x,y);//轉換變數
    answer1=Plus(x,y);
    answer2=Minus(x,y);
    answer3=Multiply(x,y);
    answer4=Divide(x,y);

    cout<<"x + y = "<<answer1<<endl;//輸出轉換後的運算結果
    cout<<"x - y = "<<answer2<<endl;
    cout<<"x * y = "<<answer3<<endl;
    cout<<"x / y = "<<answer4;

    return 0;
}
