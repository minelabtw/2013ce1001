#include<iostream>
using namespace std;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x,y;
    cout << "Please enter x ( >0 ): ";             //輸入數值
    cin >> x;
    while(x<=0)
    {
        cout << "Out of range!" << endl << "Please enter x ( >0 ): ";
        cin >> x;
    }
    cout <<"Please enter y ( >0 ): ";
    cin >> y;
    while(y<=0)
    {
        cout << "Out of range!" << endl << "Please enter y ( >0 ): ";
        cin >> y;
    }
    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;       //呼叫函式
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
    Change(x,y);
    cout << "After change function..." << endl << "Second time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;             //呼叫函式
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
    return 0;
}

int Plus(int x , int y)            //計算相加
{
    return x + y;
}

int Minus(int x , int y)         //計算相減
{
    return x - y;
}

int Multiply(int x , int y)       //計算相乘
{
    return x * y;
}

float Divide(float x , float y)    //計算相除
{
    return x / y;
}

void Change(int &x , int &y)        //互換
{
    int a;
    a = x;
    x = y;
    y = a;
}
