#include<iostream>
using namespace std ;

int Plus(int , int) ; //宣告函式
int Minus(int , int) ; //同上
int Multiply(int , int) ; //同上
float Divide(float , float) ; //同上
void Change(int & , int &) ; //同上

int main()
{
    int x=0 ; //宣告變數
    int y=0 ; //宣告變數
    cout << "Please enter x ( >0 ): " ; //輸出
    cin >> x ;
    while (x<=0) //當x小於等於0進入以下迴圈
    {
        cout << "Out of range!" << endl << "Please enter x ( >0 ): " ;
        cin >> x ;
    }
    cout << "Please enter y ( >0 ): " ; //輸出
    cin >> y ;
    while (y<=0) //當y小於等於0進入以下迴圈
    {
        cout << "Out of range!" << endl << "Please enter y ( >0 ): " ;
        cin >> y ;
    }
    cout << "First time calculation " << endl ; //輸出 First time calculation
    cout << "x" << "+" << "y" << "= " <<  Plus(x,y) << endl ; //輸出x+y
    cout << "x" << "-" << "y" << "= " <<  Minus(x,y) << endl ; //輸出x-y
    cout << "x" << "*" << "y" << "= " << Multiply(x,y) << endl ; //輸出x*y
    cout << "x" << "/" << "y" << "= " << Divide(x,y) << endl ; //輸出x/y
    cout << "After change function..." << endl ; //輸出After change function...
    cout << "Second time calculation" << endl ; //輸出Second time calculation
    Change(x,y); //呼告函式
    cout << "x" << "+" << "y" << "= " << x+y << endl ; //同上
    cout << "x" << "-" << "y" << "= " << x-y << endl ; //同上
    cout << "x" << "*" << "y" << "= " << x*y << endl ; //同上
    cout << "x" << "/" << "y" << "= " << x/y ; //同上

    return 0 ;
}
int Plus (int x , int y) //函式x+y
{
    return x+y ;
}
int Minus (int x , int y) //函式x-y
{
    return x-y ;
}
int Multiply (int x , int y) //函式x*y
{
    return x*y ;
}
float Divide (float x , float y) //函式x/y
{
    return x/y ;
}
void Change (int &x , int &y) //函式 x值與y值互換
{
    int a=x ;
    x=y ;
    y=a ;
}
