#include<iostream>
using namespace std;
int Plus(int x , int y);//prototype
int Minus(int x , int y);//prototype
int Multiply(int x , int y);//prototype
float Divide(float x , float y);//prototype
void Change(int &x , int &y);//prototype
int main()
{
    int x=0,y=0;//宣告
    do//判斷有無大於0
    {
        cout<<"Please enter x( >0 ): ";//顯示字串
        cin>>x;//輸入
        if(x<=0)//如果小於0
            cout<<"Out of range!\n";//顯示字串
    }
    while(x<=0);//判斷
    do//判斷有無大於0
    {
        cout<<"Please enter y( >0 ): ";//顯示字串
        cin>>y;//輸入
        if(y<=0)//如果小於0
            cout<<"Out of range!\n";//顯示字串
    }
    while(y<=0);//判斷
    cout<<"First time calculation\n";//顯示字串 函數
    cout<<"x + y = "<<Plus(x,y)<<endl;//顯示字串 函數
    cout<<"x - y = "<<Minus(x,y)<<endl;//顯示字串 函數
    cout<<"x * y = "<<Multiply(x,y)<<endl;//顯示字串 函數
    cout<<"x / y = "<<Divide(x,y)<<endl;//顯示字串 函數

    Change(x,y);
    cout<<"After change function...\nSecond time calculation\n";//顯示字串
    cout<<"x + y = "<<Plus(x,y)<<endl;//顯示字串 函數
    cout<<"x - y = "<<Minus(x,y)<<endl;//顯示字串 函數
    cout<<"x * y = "<<Multiply(x,y)<<endl;//顯示字串 函數
    cout<<"x / y = "<<Divide(x,y)<<endl;//顯示字串 函數
    return 0;
}
int Plus(int x , int y)
{
    return x+y;//回傳值
}
int Minus(int x , int y)
{
    return x-y;//回傳值
}
int Multiply(int x , int y)
{
    return x*y;//回傳值
}
float Divide(float x , float y)
{
    return x/y;//回傳值
}
void Change(int &x , int &y)
{
    int z=0;//x y互換
    z=x;//x y互換
    x=y;//x y互換
    y=z;//x y互換
}
