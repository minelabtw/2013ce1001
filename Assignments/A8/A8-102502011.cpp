#include<iostream>
#include <math.h>
using namespace std;

int Plus(int x , int y);
int Minus(int x , int y); //宣告要使用的函式
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x = 0, y = 0 ; //宣告變數

    do
    {
        cout << "Please enter x ( >0 ): " ;
        cin >> x ;
        if (x <= 0 )
            cout << "Out of range!" << endl ;
    }
    while (x <= 0 ) ;

    do
    {
        cout << "Please enter y ( >0 ): " ;
        cin >> y ;
        if ( y <= 0 )
            cout << "Out of range!" << endl ;
    }
    while ( y <= 0 ) ;

    cout << "First time calculation" << endl ; //第一次運算
    cout << "x + y = " << Plus(x,y) << endl ;
    cout << "x - y = " << Minus(x,y) << endl ;
    cout << "x * y = " << Multiply(x,y) << endl ;
    cout << "x / y = " << Divide(x,y) << endl ;

    cout << "After change function..." << endl ; //第二次運算
    cout << "Second time calculation" << endl ;

    Change(x,y) ;







    return 0;
}


int Plus(int x , int y)
{
    return x + y ;
}

int Minus(int x , int y)
{
    return x - y ;
}

int Multiply(int x , int y)
{
    return x * y ;
}

float Divide(float x , float y)
{
    return x / y ;
}

void Change(int &x , int &y)
{
   int tmp = 0 ;

   tmp = x ; //兩數互換的方法: 另外宣告一個變數tmp,接著讓tmp儲存x,x儲存為y,再讓y儲存數值為x的tmp,即可將兩數互換
   x = y ;
   y = tmp ;


    cout << "x + y = " << Plus(x,y) << endl ;
    cout << "x - y = " << Minus(x,y) << endl ;
    cout << "x * y = " << Multiply(x,y) << endl ;
    cout << "x / y = " << Divide(x,y) ;
}
