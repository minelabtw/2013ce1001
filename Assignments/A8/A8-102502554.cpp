#include <iostream>
using namespace std;

int Plus (int , int);
int Minus (int , int);
int Multiply (int , int);
float Divide (float , float);
void Change (int &, int &);//宣告所需使用的function

int main ()
{
    int x,y;//宣告變數x,y

    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while ( x <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }

    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while ( y <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }//輸入x,y並在輸入錯誤時可重新輸入

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus ( x , y ) << endl;
    cout << "x - y = " << Minus ( x , y ) << endl;
    cout << "x * y = " << Multiply ( x , y ) << endl;
    cout << "x / y = " << Divide ( x , y ) << endl;//輸出計算結果

    cout << "After change function..." << endl << "Second time calculation" << endl;

    Change (x , y);//交換x,y之值

    cout << "x + y = " << Plus ( x , y ) << endl;
    cout << "x - y = " << Minus ( x , y ) << endl;
    cout << "x * y = " << Multiply ( x , y ) << endl;
    cout << "x / y = " << Divide ( x , y ) << endl;//再次輸出計算結果

    return 0;
}

int Plus (int x , int y)
{
    return x + y;
}//計算x加y

int Minus (int x , int y)
{
    return x - y;
}//計算x減y

int Multiply (int x , int y)
{
    return x * y;
}//計算x乘y

float Divide (float x , float y)
{
    return x / y;
}//計算x除以y

void Change (int &x , int &y)
{
    int temp = x;//先宣告一變數temp儲存x之值
    x = y;//將y之值存入x
    y = temp;//將temp中所存的x值放入y中
}//交換x,y
