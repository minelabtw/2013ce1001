#include<iostream>

int Plus(int x,int y);  //Prototype
int Minus(int x,int y);
int Multiply(int x,int y);
float Divide(float x,float y);
void Change(int &x,int &y);

int main()
{
   int x,y;    //declaration
   
   do    //input
   {
      std::cout<<"Please enter x ( >0 ): ";
      std::cin>>x;
      if(x<=0)
         std::cout<<"Out of range!\n";
   }while(x<=0);
   do
   {
      std::cout<<"Please enter y ( >0 ): ";
      std::cin>>y;
      if(y<=0)
         std::cout<<"Out of range!\n";
   }while(y<=0);

   std::cout<<"First time calculation\n"<<   //output
              "x + y = "<<Plus(x,y)<<'\n'<<
              "x - y = "<<Minus(x,y)<<'\n'<<
              "x * y = "<<Multiply(x,y)<<'\n'<<
              "x / y = "<<Divide(x,y)<<'\n';
   Change(x,y);
   std::cout<<"After change function...\n"<<
              "Second time calculation\n"<<
              "x + y = "<<Plus(x,y)<<'\n'<<
              "x - y = "<<Minus(x,y)<<'\n'<<
              "x * y = "<<Multiply(x,y)<<'\n'<<
              "x / y = "<<Divide(x,y)<<'\n';

   return 0;
}
int Plus(int x,int y){return x+y;}  //Plus
int Minus(int x,int y){return x-y;}    //Minus
int Multiply(int x,int y){return x*y;}    //Multyply
float Divide(float x,float y){return x/y;}   //Divide
void Change(int &x,int &y){x^=y^=x^=y;}   //Swap
