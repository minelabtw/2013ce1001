#include<iostream>
using namespace std;

int Plus(int x,int y);
int Minus(int x,int y);
int Multiply(int x,int y);
float Divide(float x,float y);
void Change(int &,int &);
int main()
{
    int num1=0,num2=0;

    while(num1<=0)    //使num1大於0
    {
        cout << "Please enter x ( >0 ): ";
        cin >> num1;
        if(num1<=0)
            cout << "Out of range!" << endl;
    }

    while(num2<=0)    //使num2大於0
    {
        cout << "Please enter y ( >0 ): ";
        cin >> num2;
        if(num2<=0)
            cout << "Out of range!" << endl;
    }

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(num1,num2) << endl;
    cout << "x - y = " << Minus(num1,num2) << endl;
    cout << "x * y = " << Multiply(num1,num2) << endl;
    cout << "x / y = " << Divide(num1,num2) << endl;

    Change(num1,num2);

    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(num1,num2) << endl;
    cout << "x - y = " << Minus(num1,num2) << endl;
    cout << "x * y = " << Multiply(num1,num2) << endl;
    cout << "x / y = " << Divide(num1,num2) << endl;
}
int Plus(int x,int y)    //加法
{
    return x + y;
}
int Minus(int x,int y)    //減法
{
    return x - y;
}
int Multiply(int x,int y)    //乘法
{
    return x * y;
}
float Divide(float x,float y)    //除法
{
    return x / y;
}
void Change(int &x,int &y)    //轉換
{
    int i=0;
    i = x;
    x = y;
    y = i;
}
