#include<iostream>
using namespace std;

int Plus(int,int);                                  //宣告會使用之函式
int Minus(int,int);
int Multiply(int,int);
float Divide(float,float);
void Change(int &,int &);

int main()
{
    int x = 0;
    int y = 0;

    cout << "Please enter x ( >0 ): ";              //輸入並判斷x是否符合範圍
    while(cin >> x)
    {
        if(x<=0)
        {
            cout << "Out of range!" << endl;
            cout << "Please enter x ( >0 ): ";
        }
        else
            break;
    }

    cout << "Please enter y ( >0 ): ";              //輸入並判斷y是否符合範圍
    while(cin >> y)
    {
        if(y<=0)
        {
            cout << "Out of range!" << endl;
            cout << "Please enter y ( >0 ): ";
        }
        else
            break;
    }

    cout << "First time calculation" << endl;       //輸出第一次結果
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
    cout << "After change function..." << endl;
    Change(x,y);                                    //變數互換
    cout << "Second time calculation" << endl;      //輸出第二次結果
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    return 0;
}

int Plus(int x,int y)                               //加法
{
    return x+y;
}

int Minus(int x,int y)                              //減法
{
    return x-y;
}

int Multiply(int x,int y)                           //乘法
{
    return x*y;
}

float Divide(float x,float y)                       //除法
{
    return x/y;
}

void Change(int &x,int &y)                          //互換變數
{
    int a = 0;
    a=x;
    x=y;
    y=a;
}
