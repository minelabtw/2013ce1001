#include<iostream>

using namespace std;

int Plus(int x,int y)
{
    return x+y;
}
//
int Minus(int x,int y)
{
    return x-y;
}
int Multiply(int x,int y)
{
    return x*y;
}
float Divide(float x,float y)
{
    return x/y;
}
//
void Change(int &x,int &y)
{
    float ch=0; //for change
    ch=x;
    x=y;
    y=ch;
}

int main()
{
    int x,y;
    //input x
    while(true)
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if(x<=0)
            cout << "Out of range!" << endl;
        else
            break;
    }
    //input y
    while(true)
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if(y<=0)
            cout << "Out of range!" << endl;
        else
            break;
    }
    //first cal
    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide((float)x,(float)y) << endl;
    //and change x,y;
    Change(x,y);
    //second cal
    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide((float)x,(float)y) << endl;

    return 0;

}
