#include <iostream>
#include <string>

using namespace std;

int Plus(int x , int y); // function declare
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
	int x , y;

	while(true) // input x
	{
		cout << "Please enter x ( >0 ): ";
		cin >> x;
		if(x > 0)
			break;
		cout << "Out of range!" << endl;
	}

	while(true) // inpput y
	{
		cout << "Please enter y ( >0 ): ";
		cin >> y;
		if(y > 0)
			break;
		cout << "Out of range!" << endl;
	}

	cout << "First time calculation" << endl; // first time
	cout << "x + y = " << Plus(x , y) << endl; // plus
	cout << "x - y = " << Minus(x , y) << endl; // minus
	cout << "x * y = " << Multiply((float)x , (float)y) << endl; // multiply
	cout << "x / y = " << Divide(x , y) << endl; // divide

	Change(x , y); // change x & y
	cout << "After change function..." << endl;

	cout << "Second time calculation" << endl; // second time
	cout << "x + y = " << Plus(x , y) << endl; // plus
	cout << "x - y = " << Minus(x , y) << endl; // minus
	cout << "x * y = " << Multiply((float)x , (float)y) << endl; // multiply
	cout << "x / y = " << Divide(x , y) << endl; // divide

	return 0;
}

// function implement
int Plus(int x , int y)
{
	return x + y;
}

int Minus(int x , int y)
{
	return x - y;
}

int Multiply(int x , int y)
{
	return x * y;
}

float Divide(float x , float y)
{
	return x / y;
}

void Change(int &x , int &y)
{
	int temp = x;
	x = y;
	y = temp;
}
