#include<iostream>
using namespace std;

int temp=0;

int Plus(int x , int y)
{
    cout<<"x + y = "<<x+y<<endl; //加法
}
int Minus(int x , int y)
{
    cout<<"x - y = "<<x-y<<endl; //減法
}
int Multiply(int x , int y)
{
    cout<<"x * y = "<<x*y<<endl; //乘法
}
float Divide(float x , float y)
{
    cout<<"x / y = "<<x/y; //除法
}
void Change(int &x , int &y)
{
    temp=x; //交換數字
    x=y;
    y=temp;
}

int main()
{
    int x = 0;
    int y = 0;

    cout<<"Please enter x ( >0 ): "; //輸入
    cin>>x;
    while(x<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }

    cout<<"Please enter y ( >0 ): "; //輸入
    cin>>y;
    while(y<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }

    cout<<"First time calculation"<<endl;
    Plus(x , y); //叫出函式
    Minus(x , y);
    Multiply(x , y);
    Divide(x , y);
    cout<<endl;

    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    Change(x , y); //叫出函式
    Plus(x , y);
    Minus(x , y);
    Multiply(x , y);
    Divide(x , y);

    return 0;
}

