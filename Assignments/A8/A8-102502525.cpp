#include<iostream>
using namespace std;
int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)
{
    int inx;
    inx=x;
    x=y;
    y=inx;
}
int main()
{
int x,y;
do{
    cout <<"Please enter x ( >0 ): ";
    cin >>x;
    if (x<=0)
    cout <<"Out of range!"<<endl;
}while(x<=0);
do{
    cout <<"Please enter y ( >0 ): ";
    cin >>y;
    if (y<=0)
    cout <<"Out of range!"<<endl;
}while(y<=0);
    cout <<"First time calculation"<<endl;
    cout <<"x + y = "<<Plus(x,y)<<endl;
    cout <<"x - y = "<<Minus(x,y)<<endl;
    cout <<"x * y = "<<Multiply(x,y)<<endl;
    cout <<"x / y = "<<Divide(x,y)<<endl;
    cout <<"After change function..."<<endl;
    cout <<"Second time calculation"<<endl;
    Change(x,y);
    cout <<"x + y = "<<Plus(x,y)<<endl;
    cout <<"x - y = "<<Minus(x,y)<<endl;
    cout <<"x * y = "<<Multiply(x,y)<<endl;
    cout <<"x / y = "<<Divide(x,y)<<endl;

    return 0;
}
