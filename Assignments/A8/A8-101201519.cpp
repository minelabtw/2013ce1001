/*先讓使用者輸入兩個正整數(>0)，
若使用者輸入非合法範圍則要輸出"Out of range!"，
再把這兩個正整數分別傳入加、減、乘、除四個function中，
然後輸出第一次這兩個正整數經過4個funtion運算的結果，
接下來呼叫一個call by reference function把輸入的兩個變數值互換後，
以第一次傳入變數的順序傳入function，
最後輸出第二次運算結果。*/
#include<iostream>
using namespace std;

int Plus(int x , int y)//加法函式
{
    return x+y;
}
int Minus(int x , int y)//減法函式
{
    return x-y;
}
int Multiply(int x , int y)//乘法函式
{
    return x*y;
}
float Divide(float x , float y)//除法函式
{
    return x/y;
}
void Change(int &x , int &y)//交換函式
{
    int z;
    z=y,y=x,x=z;
}
int main ()
{
    int x=0,y=0;
    while(x<=0)
    {
        cout << "Please enter x ( >0 ): ";
        cin >> x;
        if (x<=0)
            cout << "Out of range!\n";
    }
    while(y<=0)
    {
        cout << "Please enter y ( >0 ): ";
        cin >> y;
        if (y<=0)
            cout << "Out of range!\n";
    }
    cout << "First time calculation\n";
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
    cout << "After change function...\n" << "Second time calculation\n";
    Change(x,y);//引入交換函式將兩數交換
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    return 0;

}
