#include <iostream>
#include <math.h>
using namespace std;
int Plus (int x , int y );//定義一些東西，之後使用
int Minus (int x , int y );
int Multiply (int x , int y );
float Divide (float x ,float y );
void Change (int &x , int &y );
int p;
int mi;
int mu;
float di;
int main()
{
    int x;
    int y;
    do
    {
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
        if (x <= 0)//檢驗輸入的數字要大於零
            cout<<"Out of range!\n";
    }
    while (x<=0);

    do
    {
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
        if (y <= 0)//檢驗輸入的數字要大於零
            cout<<"Out of range!\n";
    }
    while (y<=0);

    cout<<"First time calculation\n";//輸出
    Plus(x,y);//呼叫函式
    Minus(x,y);
    Multiply (x,y);
    Divide (x,y);
    cout<<"x + y ="<<p<<endl;//輸出結果
    cout<<"x - y ="<<mi<<endl;
    cout<<"x * y ="<<mu<<endl;
    cout<<"x / y ="<<di<<endl;

    cout<<"After change function...\n";//輸出
    cout<<"Second time calculation\n";
    Change(x,y);//呼叫函式
    Plus(x,y);
    Minus(x,y);
    Multiply (x,y);
    Divide (x,y);
    cout<<"x + y ="<<p<<endl;//輸出結果
    cout<<"x - y ="<<mi<<endl;
    cout<<"x * y ="<<mu<<endl;
    cout<<"x / y ="<<di<<endl;

    return 0;
}

int Plus (int x , int y )//加函式
{
    p = x + y;
}
int Minus (int x , int y )//減函式
{
    mi = x - y;
}
int Multiply (int x , int y )//乘函式
{
    mu = x * y;
}
float Divide (float x , float y )//除函式
{
    di = x / y;
}
void Change(int &x , int &y)//將變數x,y位置交換
{
    int c;
    c = y;
    y = x;
    x = c;
}
