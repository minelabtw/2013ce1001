#include <iostream>
using namespace std;

int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)                               //���潻�Q׃��ӡ���Ӝp�˳��ĺ���
{
    cout <<"x + y = "<<Plus(y,x)<<endl;
    cout <<"x - y = "<<Minus(y,x)<<endl;
    cout <<"x * y = "<<Multiply(y,x)<<endl;
    cout <<"x / y = "<<Divide(y,x);
}

int main()
{
    int x,y;

    cout <<"Please enter x ( >0 ): ";
    cin >>x;
    while (x<=0)
    {
        cout <<"Out of range!\nPlease enter x ( >0 ): ";
        cin >>x;
    }
    cout <<"Please enter y ( >0 ): ";
    cin >>y;
    while (y<=0)
    {
        cout <<"Out of range!\nPlease enter y ( >0 ): ";
        cin >>y;
    }
    cout <<"First time calculation\n";
    cout <<"x + y = "<<Plus(x,y)<<endl;                    //���мӷ�����
    cout <<"x - y = "<<Minus(x,y)<<endl;                   //���Мp������
    cout <<"x * y = "<<Multiply(x,y)<<endl;                //���г˷�����
    cout <<"x / y = "<<Divide(x,y)<<endl;                  //���г�������
    cout <<"After change function..."<<endl;
    cout <<"Second time calculation"<<endl;
    Change(x,y);                                           //���н��Q׃����ӡ���Ӝp�˳��ĺ���

    return 0;
}


