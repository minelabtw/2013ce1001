#include<iostream>
using namespace std;
#include<cmath>

int Plus(int x , int y); //prototype , function call by value
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y); //prototype , function call by reference

int main()
{
    int x,y;
    cout << "Please enter x( >0 ): "; //第一次輸入x
    cin >> x;
    while (x<=0)  //輸入<=0要輸出"Out of range!"
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x( >0 ): ";  //並重複輸入
        cin >> x;
    }
    cout << "Please enter y ( >0 ): "; //第二次輸入y
    cin >> y;
    while (y<=0)  //輸入<=0要輸出"Out of range!"
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";  //並重複輸入
        cin >> y;
    }
    cout << "x + y = " << Plus(x,y) << endl; //輸出第一次這兩個正整數經過4個funtion運算的結果
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
    cout << "After change function...\n" << "Second time calculation\n";
    Change(x,y); //把輸入的兩個變數值互換
    cout << "x + y = " << Plus(x,y) << endl; //以第一次傳入變數的順序傳入function，最後輸出第二次運算結果。
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
    return 0;
}

int Plus(int x , int y) //以第一次傳入變數的順序傳入function，最後輸出第二次運算結果。
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)
{
    int z;
    z = x;
    x = y;
    y = z;
}
