/* 先讓使用者輸入兩個正整數(>0)，若使用者輸入非合法範圍則要輸出"Out of range!"，
再把這兩個正整數分別傳入加、減、乘、除四個function中，然後輸出第一次這兩個正整數經過4個funtion運算的結果，
接下來呼叫一個call by reference function把輸入的兩個變數值互換後，以第一次傳入變數的順序傳入function，最後輸出第二次運算結果。*/

#include<iostream>
using namespace std;

int Plus(int x, int y) //加法的fumcyion
{
    return x+y;
}

int Minus(int x, int y) //減法的function
{
    return x-y;
}

int Mutiply(int x, int y) //乘法的function
{
    return x*y;
}

float Divide(float x, float y) //除法的function
{
    return x/y;
}
void Change(int &x, int &y) //把兩個變數交換
{
    int tmp=x;
    x=y;
    y=tmp;
}

int main()
{
    int x=0,y=0;
    do                      //輸入數字, 如果小於0就會顯示Out of range而且重輸
    {
        cout << "Please enter x (>0): ";
        cin >> x;
        if(x<0)
            cout << "Out of range!!" << endl;
    }
    while (x<0);

    do                      //輸入數字, 如果小於0就會顯示Out of range而且重輸
    {
        cout << "Please enter y (>0): ";
        cin >> y;
        if(y<0)
            cout << "Out of range!!" << endl;
    }
    while (y<0);

    cout << "First timr calculation" << endl; //輸出第一次運算的加,減,乘,除結果
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Mutiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;

    Change(x,y);
    cout << "after change function..." << endl; //兩個number交換後的結果
    cout << "Second time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Mutiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;


    return 0;

}
