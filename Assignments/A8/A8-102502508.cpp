#include<iostream>
using namespace std ;
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(double &x , double &y);
int main()//本次作業目的在於運用函式中的不同方法來進行四則運算中變數的互換
{
    float x=0 ;
    float y=0 ;

    while(x<=0)
    {
        cout<<"Please enter x ( >0 ):" ;
        cin>>x ;
        if(x<=0)
        {
            cout<<"Out of range !"<<endl ;
        }
    }
    while(y<=0)
    {
        cout<<"Please enter y ( >0 ):" ;
        cin>>y ;
        if(y<=0)
        {
            cout<<"Out of range !"<<endl ;
        }
    }

    cout<<"First time calculation"<<endl ;     //第一次的運算是利用擁有回傳值的函式將答案算出並傳回主程式
    cout<<"x + y = "<<x+y<<endl ;
    cout<<"x - y = "<<x-y<<endl ;
    cout<<"x * y = "<<x*y<<endl ;
    cout<<"x / y = "<<x/y<<endl ;
    cout<<"After change function..."<<endl ;
    cout<<"Second time calculation"<<endl ;
    void Change(double &x , double &y) ;     //第二次的運算是利用call by reference中的取址符號將x,y的值傳入副程式中運算並傳回主程式
    cout<<"x + y = "<<y+x<<endl ;
    cout<<"x - y = "<<y-x<<endl ;
    cout<<"x * y = "<<y*x<<endl ;
    cout<<"x / y = "<<y/x<<endl ;
    return 0 ;


}
int Plus(int x , int y)
{
    return x+y ;
}
int Minus(int x , int y)
{
    return x-y ;
}
int Multiply(int x , int y)
{
    return x*y ;
}
float Divide(float x , float y)
{
    return x/y ;
}
void Change(double &x , double &y)
{
    y+x ;
    y-x ;
    y*x ;
    y/x ;


}

