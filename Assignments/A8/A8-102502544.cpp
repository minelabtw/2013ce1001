#include<iostream>
using namespace std;
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);
int main()
{
    int x=0;//跑计俱计
    int y=0;//跑计俱计

    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)//砞﹚讽x单0
    {
        cout<<"Out of range!\nPlease enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)//砞﹚讽y单0
    {
        cout<<"Out of range!\nPlease enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x+y= "<<Plus(x,y)<<endl;
    cout<<"x-y= "<<Minus(x,y)<<endl;
    cout<<"x*y= "<<Multiply(x,y)<<endl;
    cout<<"x/y= "<<Divide(x,y)<<endl;
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    Change(x,y);
    cout<<"x+y= "<<Plus(x,y)<<endl;
    cout<<"x-y= "<<Minus(x,y)<<endl;
    cout<<"x*y= "<<Multiply(x,y)<<endl;
    cout<<"x/y= "<<Divide(x,y)<<endl;

    return 0;
}
int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)
{
    int a=x;
    x=y;
    y=a;
}
