#include<iostream>

using namespace std;
int ps(int x , int y);//prototype
int ms(int x , int y);
int my(int x , int y);
float de(float x , float y);
void change(int &x , int &y);

main()
{
    int x;
    int y;
    cout << "Please enter x ( >0 ): ";
    cin  >> x;
    while(x<=0)//判定X要大於零的迴圈
    {
        cout << "Out of range!\n";
        cout << "Please enter x ( >0 ): ";
        cin  >> x;
    }

    cout << "Please enter y ( >0 ): ";
    cin  >> y;
    while(y<=0)//判定Y要大於零的迴圈
    {
        cout << "Out of range!\n";
        cout << "Please enter y ( >0 ): ";
        cin  >> y;
    }

    cout << "First time calculation\n";
    cout << "x" << " + " << "y" << " = " << ps(x,y) << "\n";//輸出 帶入含式的值
    cout << "x" << " - " << "y" << " = " << ms(x,y) << "\n";
    cout << "x" << " * " << "y" << " = " << my(x,y) << "\n";
    cout << "x" << " / " << "y" << " = " << de(x,y) << "\n";

    cout << "After change function...\n" << "Second time calculation\n";
    change(x,y);
    cout << "x" << " + " << "y" << " = " << ps(x,y) << "\n";
    cout << "x" << " - " << "y" << " = " << ms(x,y) << "\n";
    cout << "x" << " * " << "y" << " = " << my(x,y) << "\n";
    cout << "x" << " / " << "y" << " = " << de(x,y) << "\n";



    return 0;
}

int ps(int x,int y)
{
    return x+y;
}
int ms(int x,int y)
{
    return x-y;
}
int my(int x,int y)
{
    return x*y;
}
float de(float x,float y)
{
    return x/y;
}
void change(int &x,int &y)//改變XY數值
{
    int save;//存取X值
    save=x;
    x=y;
    y=save;
}





