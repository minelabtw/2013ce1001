#include <iostream>

using namespace std;

int Plus(int x , int y)  //�ۥ[
{
    return x+y;
}

int Minus(int x , int y)  //�۴�
{
    return x-y;
}

int Multiply(int x , int y)  //�ۭ�
{
    return x*y;
}

float Divide(float x , float y)  //�۰�
{
    return x/y;
}

void Change(int &x , int &y)  //�Ʀr����
{
    int box;  //�x�s��
    box=x;
    x=y;
    y=box;
}

int main()
{
    int x,y;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)  //�P�_�O�_�W�X�d��
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)  //�P�_�O�_�W�X�d��
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    cout<<"After change function..."<<endl;
    cout<<"Second time calculation"<<endl;
    Change(x,y);
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    return 0;
}
