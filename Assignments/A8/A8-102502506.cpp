#include<iostream>
using namespace std;

int Plus(int x , int y)  //加法的function
{
    int P;
    P = x + y;
    return P;
}
int Minus(int x , int y)  //減法的function
{
    int M;
    M = x - y;
    return M;
}
int Multiply(int x , int y)  //乘法的function
{
    int M;
    M = x * y;
    return M;
}
float Divide(float x , float y)  //除法的function
{
    float D;
    D = x / y;
    return D;
}
void Change(int &x ,int &y)  //將兩變數調換的function,先設一個x1將y值存入,然後再把x值丟入y,再將x1值丟入x
{
    int x1 = y;
    y = x;
    x = x1;
}

int main ()
{
    int x,y;  //設兩變數
    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while ( x < 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;
    }
    cout << "Please enter y ( >0 ): ";
    cin >> y;
    while ( y < 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }
    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus( x,y ) << endl;
    cout << "x - y = " << Minus( x,y ) << endl;
    cout << "x * y = " << Multiply( x,y ) << endl;
    cout << "x / y = " << Divide( x,y ) << endl;
    cout << "After change function..." << endl << "Second time calculation" << endl;
    Change( x,y );  //兩樹調換
    cout << "x + y = " << Plus( x,y ) << endl;
    cout << "x - y = " << Minus( x,y ) << endl;
    cout << "x * y = " << Multiply( x,y ) << endl;
    cout << "x / y = " << Divide( x,y ) << endl;
}
