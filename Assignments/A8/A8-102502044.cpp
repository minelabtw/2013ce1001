/* IDE: Visual Studio */
#include <stdio.h>

/* + - * / operator */
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);

/* swap x y */
void Change(int &x , int &y);

/* function to input */
void input(const char *str, int *tmp);

/* function to output */
void output(int x, int y);

int main()
{
	int x, y;

	/* input x y */
	input("Please enter x ( >0 ): ", &x);
	input("Please enter y ( >0 ): ", &y);

	/* ui message */
	puts("First time calculation");

	/* output x y */
	output(x, y);

	/* swap x y */
	Change(x, y);

	/* ui message */
	puts("After change function...");
	puts("Second time calculation");

	/* output x y */
	output(x, y);

	return 0;
}

/* swap x y */
void Change(int &x , int &y)
{
	int t = x;
	x = y;
	y = t;
}

/* output x {+,-,*,/} y */
void output(int x, int y)
{
	/* x + y */
	printf("x + y = %d\n", Plus(x, y));

	/* x - y */
	printf("x + y = %d\n", Minus(x, y));

	/* x * y */
	printf("x + y = %d\n", Multiply(x, y));

	/* x / y */
	printf("x + y = %f\n", Divide((float)x ,(float)y));
}

/* x + y */
int Plus(int x , int y){return x + y;}

/* x - y */
int Minus(int x , int y){return x - y;}

/* x * y */
int Multiply(int x , int y){return x * y;}

/* x / y */
float Divide(float x , float y){return x / y;}

/* input *tmp */
/* *str is ui message */
void input(const char *str, int *tmp)
{
	for(;;)
	{
		/* ui message */
		printf(str);

		/* input *tmp */
		scanf("%d", tmp);

		/* check *tmp is in range(0, unlimit) */
		if(*tmp > 0)
			break; //break the loop
		else
			puts("Out of range!"); //error ui message
	}
}
