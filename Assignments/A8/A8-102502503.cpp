#include <iostream>
using namespace std;
int Plus(int x , int y);  //宣告加法函式
int Minus(int x , int y);  //宣告減法函式
int Multiply(int x , int y);  //宣告乘法函式
float Divide(float x , float y);  //宣告除法函式
void Change(int &x , int &y);  //宣告改變變數順序函式
int main()
{
    int x,y;  //宣告兩整數變數
    cout << "Please enter x ( >0 ): ";
    cin >> x;
    while(x<=0)  //使用while迴圈判斷x是否為正整數
    {
        cout << "Out of range!" << endl << "Please enter x ( >0 ): ";
        cin >> x;
    }
    cout << "Please enter y ( >0 ): ";
    cin >> y;
     while(y<=0)  //使用while迴圈判斷y是否為正整數
    {
        cout << "Out of range!" << endl << "Please enter y ( >0 ): ";
        cin >> y;
    }

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus(x,y) << endl;  //呼叫加法函式
    cout << "x - y = " << Minus(x,y) << endl;  //呼叫減法函式
    cout << "x * y = " << Multiply(x,y) << endl;  //呼叫乘法函式
    cout << "x / y = " << Divide(x,y) << endl;  //呼叫除法函式
    cout << "After change function..." << endl << "Second time calculation" << endl;
    Change(x,y);  //改變變數順序
    cout << "x + y = " << Plus(x,y) << endl;  //呼叫加法函式
    cout << "x - y = " << Minus(x,y) << endl;  //呼叫減法函式
    cout << "x * y = " << Multiply(x,y) << endl;  //呼叫乘法函式
    cout << "x / y = " << Divide(x,y);  //呼叫除法函式
}

int Plus(int x , int y)  //定義加法函式
{
    return x+y;
}
int Minus(int x , int y)  //定義減法函式
{
    return x-y;
}
int Multiply(int x , int y)  //定義乘法函式
{
    return x*y;
}
float Divide(float x , float y)  //定義除法函式
{
    return x/y;
}
void Change(int &x , int &y)  //定義改變變數順序函式
{
    int a=x;
    x=y;
    y=a;
}
