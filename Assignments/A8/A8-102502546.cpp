#include <iostream>

using namespace std ;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);//宣告函式

int main()
{
    int x , y ;//宣告變數

    cout << "Please enter x ( >0 ): " ;//輸出 需要x
    cin >> x ;//輸入x值
    while(x<=0)//x<=0時重新輸入
    {
        cout << "Out of range!\nPlease enter x ( >0 ): " ;
        cin >> x ;
    }

    cout << "Please enter y ( >0 ): " ;
    cin >> y ;
    while(y<=0)
    {
        cout << "Out of range!\nPlease enter y ( >0 ): " ;
        cin >> y ;
    }
    cout << "First time calculation" << endl ;
    cout << "x + y = " << Plus(x,y) << endl ;//輸出含式計算x+y
    cout << "x - y = " << Minus(x,y) << endl ;//x-y
    cout << "x * y = " << Multiply(x,y) << endl ;//x*y
    cout << "x / y = " << Divide(x,y) << endl ;//x/Y

    cout << "After change function...\nSecond time calculation\n" ;

    Change(x,y) ;//x.y交換

    cout << "x + y = " << Plus(x,y) << endl ;
    cout << "x - y = " << Minus(x,y) << endl ;
    cout << "x * y = " << Multiply(x,y) << endl ;
    cout << "x / y = " << Divide(x,y) << endl ;//再次運算

    return 0 ;
}

int Plus(int x , int y)
{
    return x+y ;
}
int Minus(int x , int y)
{
    return x-y ;
}
int Multiply(int x , int y)
{
    return x*y ;
}
float Divide(float x , float y)
{
    return x/y ;
}
void Change(int &x , int &y)
{
    int temp ;
    temp = x ;
    x = y ;
    y = temp ;
}
