#include <iostream>
using namespace std;

int Plus(int x , int y); // function prototype for function Plus
int Minus(int x , int y); // function prototype for function Minus
int Multiply(int x , int y); // function prototype for function Multiply
float Divide(float x , float y); // function prototype for function Divide
void Change(int &x , int &y); // function prototype for function Change (reference pass)

int main()
{
    int a = 0,b = 0; // initialize integer a and b
    cout << "Please enter x ( >0 ): " ;
    cin >> a;

    while ( a <= 0) // while loop to check a is on demand
    {
        cout << "Out of range!" << endl;
        cout << "Please enter x ( >0 ): " ;
        cin >> a;
    }

    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> b;
        if ( b <= 0)
        cout << "Out of range!" << endl;

    }
    while ( b <= 0); // do... while loop to check b is on demand
    cout << "First time calculation\n"
         << "x + y = " <<  Plus( a, b) << endl
         << "x - y = " <<  Minus( a, b) << endl
         << "x * y = " <<  Multiply( a, b) << endl
         << "x / y = " <<  Divide( a, b) << endl;

    Change( a, b); // use function Change to alter the value of a and b

    cout << "After change function...\n" << "Second time calculation\n"
         << "x + y = " <<  Plus( a, b) << endl
         << "x - y = " <<  Minus( a, b) << endl
         << "x * y = " <<  Multiply( a, b) << endl
         << "x / y = " <<  Divide( a, b) << endl;

    return 0;
}

int Plus(int x , int y)
{
    return x + y;
}

int Minus(int x , int y)
{
    return x - y;
}

int Multiply(int x , int y)
{
    return x * y;
}

float Divide(float x , float y)
{
    return x / y;
}

void Change(int &x , int &y)
{
    int x1 = x;
    x = y;
    y = x1;
}
