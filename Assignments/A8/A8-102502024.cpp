#include<iostream>
using namespace std;
int Plus(int x , int y );  //宣告function
int Minus(int x , int y);  //宣告function
int Multiply(int x , int y);  //宣告function
float Divide(float x , float y);  //宣告function
void Change(int &x , int &y);  //宣告function
int x1=0;
int main()
{
    int x=0,y=0;  //宣告變數
    cout<<"Please enter x ( >0 ): ";  //輸出題目
    cin>>x;  //輸入數字
    while(x<=0)  //判斷是否符合條件
    {
        cout<<"Out of range!\n";
        cout<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";  //輸出題目
    cin>>y;  //輸入數字
    while(y<=0)  //判斷是否符合條件
    {
        cout<<"Out of range!\n";
        cout<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation\n";
    Plus(x,y);  //呼叫function
    Minus(x,y);  //呼叫function
    Multiply(x,y);  //呼叫function
    Divide(x,y);  //呼叫function
    cout<<"x + y = "<<Plus(x,y)<<endl;  //輸出結果
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    cout<<"After change function...\n";
    cout<<"Second time calculation\n";
    x1=x;
    Change(x,y);  //呼叫function
    cout<<"x + y = "<<Plus(x,y)<<endl;  //輸出結果
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    return 0;
}
int Plus(int x , int y )  //計算答案
{
    return x+y;
}
int Minus(int x , int y)  //計算答案
{
    return x-y;
}
int Multiply(int x , int y)  //計算答案
{
    return x*y;
}
float Divide(float x , float y)  //計算答案
{
    return x/y;
}
void Change(int &x , int &y)  //交換變數
{
    x=y;
    y=x1;
}
