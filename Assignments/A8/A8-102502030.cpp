#include <iostream>
using namespace std;
int Plus( int x , int y );
int Minus( int x , int y );
int Multiply( int x , int y );
float Divide( float x , float y );
void Change( int &x , int &y );
main()
{
    int num1;  //設變數
    int num2;

    do  //輸入變數並檢查合理性
    {
        cout << "Please enter x ( >0 ): ";
        cin >> num1;
    }
    while( num1<=0 && cout << "Out of range!\n");
    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> num2;
    }
    while( num2<=0 && cout << "Out of range!\n");

    cout << "First time calculation\n";  //輸出四則運算結果
    cout << "x + y = " << Plus( num1 , num2 ) << endl;
    cout << "x - y = " << Minus( num1 , num2 ) << endl;
    cout << "x * y = " << Multiply( num1 , num2 ) << endl;
    cout << "x / y = " << Divide( num1 , num2 ) << endl;

    cout << "After change function...\n";
    Change( num1 , num2 );  //數字交換
    cout << "Second time calculation\n";  //輸出交換順序後的四則運算結果
    cout << "x + y = " << Plus( num1 , num2 ) << endl;
    cout << "x - y = " << Minus( num1 , num2 ) << endl;
    cout << "x * y = " << Multiply( num1 , num2 ) << endl;
    cout << "x / y = " << Divide( num1 , num2 ) << endl;

    return 0;
}
int Plus( int x , int y )  //相加
{
    return x+y;
}
int Minus( int x , int y )  //相減
{
    return x-y;
}
int Multiply( int x , int y )  //相乘
{
    return x*y;
}
float Divide( float x , float y )  //相除
{
    return x/y;
}
void Change( int &x , int &y )  //調換順序
{
    int z=x;
    x=y;
    y=z;
}
