#include<iostream>
using namespace std;
int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)//定義一個函式 參數為x的位置 y的位置
{
  int z = x;//宣告一個z儲存x的值
  x = y;//讓x和y互換
  y = z;
}
int main()
{
   int x = 0;
   int y = 0;
   do
   {
       cout << "Please enter x ( >0 ): ";
       cin >> x;
       if(x<=0)
        cout << "Out of range!" << endl;
   }while(x<=0);
   do
   {
       cout << "Please enter y ( >0 ): ";
       cin >> y;
       if(y<=0)
        cout << "Out of range!" << endl;
   }while(y<=0);
   cout << "First time calculation" << endl;
   cout << "x + y = " << Plus(x,y) << endl;
   cout << "x - y = " << Minus(x,y) << endl;
   cout << "x * y = " << Multiply(x,y) << endl;
   cout << "x / y = " << Divide(x,y) << endl;
   Change(x,y);//呼叫函式讓x和y的值互換
   cout << "After change function..." << endl << "Second time calculation" << endl;
   cout << "x + y = " << Plus(x,y) << endl;
   cout << "x - y = " << Minus(x,y) << endl;
   cout << "x * y = " << Multiply(x,y) << endl;
   cout << "x / y = " << Divide(x,y);
   return 0;

}
