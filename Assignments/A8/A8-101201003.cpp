#include<iostream>
using namespace std;
int x=0;
int y=0;

int Plus(int x , int y)
{
    return x+y;
}                                           //加法的函數
int Minus(int x , int y)
{
    return x-y;
}                                           //減法的函數
int Multiply(int x , int y)
{
    return x*y;
}                                           //乘法的函數
float Divide(float x , float y)
{
    return x/y;
}                                           //除法的函數
void Change(int &x , int &y)
{
    int z=0;
    z=y;
    y=x;
    x=z;
}                                           //將x、y值交換
int main()
{
    cout<<"Please enter x ( >0 ): ";        //輸入x
    cin>>x;
    while(x<=0)
    {
        cout<<"Out of range!"<<endl<<"Please enter x ( >0 ): ";
        cin>>x;
    }                                       //要求>0
    cout<<"Please enter y ( >0 ): ";        //輸入y
    cin>>y;
    while(y<=0)
    {
        cout<<"Out of range!"<<endl<<"Please enter y ( >0 ): ";
        cin>>y;
    }                                       //要求>0
    cout<<"First time calculation"<<endl
        <<"x + y = "<<Plus(x ,y)<<endl
        <<"x - y = "<<Minus(x ,y)<<endl
        <<"x * y = "<<Multiply(x,y)<<endl
        <<"x / y = "<<Divide(x,y)<<endl;    //輸出計算結果
    cout<<"After change function..."<<endl<<"Second time calculation"<<endl;
    Change(x,y);
    cout<<"x + y = "<<Plus(x ,y)<<endl
        <<"x - y = "<<Minus(x ,y)<<endl
        <<"x * y = "<<Multiply(x,y)<<endl
        <<"x / y = "<<Divide(x,y)<<endl;    //交換值後輸出結果

    return 0;
}
