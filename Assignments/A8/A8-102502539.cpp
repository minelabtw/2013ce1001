#include <iostream>

using namespace std;

int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main()
{
    int x;
    int y;

    do
    {
        cout << "Please enter x ( >0 ): " ;
        cin >> x ;
        if ( x < 1 )
            cout << "Out of range!" <<endl;
    } while ( x < 1 );

    do
    {
        cout << "Please enter y ( >0 ): " ;
        cin >> y ;
        if ( y < 1 )
            cout << "Out of range!" << endl;
    } while ( y < 1 );

    cout << "First time calculation" << endl;
    cout << "x + y = " << Plus (x , y) << endl;
    cout << "x - y = " << Minus (x , y) << endl;
    cout << "x * y = " << Multiply (x , y) << endl;
    cout << "x / y = " << Divide (x , y) << endl;

    Change (x , y);

    cout << "After change function...\n" << "Second time calculation" << endl;
    cout << "x + y = " << Plus (x , y) << endl;
    cout << "x - y = " << Minus (x , y) << endl;
    cout << "x * y = " << Multiply (x , y) << endl;
    cout << "x / y = " << Divide (x , y) << endl;

    return 0;
}

int Plus(int x , int y)     //加
{
    return x+y;
}

int Minus(int x , int y)     //減
{
    return x-y;
}

int Multiply(int x , int y)    //乘
{
    return x*y;
}

float Divide(float x , float y)     //除
{
    return x/y;
}

void Change(int &x , int &y)     //變數調換
{
    int i;
    i = y;
    y = x;
    x = i;
}
