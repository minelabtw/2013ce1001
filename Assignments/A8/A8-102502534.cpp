#include<iostream>
using namespace std;
//function prototype
int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
void Change(int &x , int &y)//將兩變數值互換
{
    int a=0;
    a=x;
    x=y;
    y=a;
}
int main()
{
    int x=0;//宣告變數
    int y=0;
    cout<<"Please enter x ( >0 ): ";
    cin>>x;
    while(x<=0)//x必須大於0
    {
        cout<<"Out of range!"<<endl<<"Please enter x ( >0 ): ";
        cin>>x;
    }
    cout<<"Please enter y ( >0 ): ";
    cin>>y;
    while(y<=0)//y必須小於0
    {
        cout<<"Out of range!"<<endl<<"Please enter y ( >0 ): ";
        cin>>y;
    }
    cout<<"First time calculation"<<endl;
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    cout<<"After change function..."<<endl<<"Second time calculation"<<endl;
    Change(x,y);
    cout<<"x + y = "<<Plus(x,y)<<endl;
    cout<<"x - y = "<<Minus(x,y)<<endl;
    cout<<"x * y = "<<Multiply(x,y)<<endl;
    cout<<"x / y = "<<Divide(x,y)<<endl;
    return 0;
}
