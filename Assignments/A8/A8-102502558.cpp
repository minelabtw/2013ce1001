#include <iostream>

using namespace std;

// prototype
int Plus(int x , int y);
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

// 輸入用的函數
void input(const char *prompt, const char *error, int &n)
{
    do
    {
        cout << prompt;
        cin >> n;
    } while(n <= 0 && cout << error << endl);
}

// 呼叫計算用的函數
void calc(int x,int y)
{
    cout << "x + y = " << Plus(x,y) << endl;
    cout << "x - y = " << Minus(x,y) << endl;
    cout << "x * y = " << Multiply(x,y) << endl;
    cout << "x / y = " << Divide(x,y) << endl;
}

int main()
{
    int x = 0,y = 0;
    // 重複輸入
    input("Please enter x ( >0 ): ","Out of range!",x);
    input("Please enter y ( >0 ): ","Out of range!",y);
    cout << "First time calculation" << endl;
    calc(x,y);
    Change(x,y);
    cout << "After change function..." << endl;
    cout << "Second time calculation" << endl;
    calc(x,y);
    return 0;
}
// 以下是計算用的函數
int Plus(int x , int y)
{
    return x+y;
}
int Minus(int x , int y)
{
    return x-y;
}
int Multiply(int x , int y)
{
    return x*y;
}
float Divide(float x , float y)
{
    return x/y;
}
// 交換兩個變數
void Change(int &x , int &y)
{
    int t = x;
    x = y;
    y = t;
}
