#include<iostream>
using namespace std;

int Plus(int x , int y);            //宣告函數
int Minus(int x , int y);
int Multiply(int x , int y);
float Divide(float x , float y);
void Change(int &x , int &y);

int main ()
{
    int x=0;
    int y=0;
    cout << "Please enter x ( >0 ): " ;         //輸入第一個數字
    cin >> x ;
    while ( x<=0 )
    {
        cout << "Out of range!" << endl ;
        cout << "Please enter x ( >0 ): " ;
        cin >> x ;
    }

    cout << "Please enter y ( >0 ): " ;         //輸入第二個數字
    cin >> y ;
    while ( y<=0 )
    {
        cout << "Out of range!" << endl ;
        cout << "Please enter y ( >0 ): " ;
        cin >> y ;
    }
    cout << "First time calculation" << endl ;      //印出第一次計算結果
    cout << "x + y = " << Plus( x,y ) << endl ;
    cout << "x - y = " << Minus( x,y ) << endl ;
    cout << "x * y = " << Multiply( x,y ) << endl ;
    cout << "x / y = " << Divide( x,y ) << endl ;
    cout << "After change function..." << endl ;
    Change( x , y );                                //使第一個和第二個數字交換
    cout << "Second time calculation" << endl ;
    cout << "x + y = " << Plus( x,y ) << endl ;
    cout << "x - y = " << Minus( x,y ) << endl ;
    cout << "x * y = " << Multiply( x,y ) << endl ;
    cout << "x / y = " << Divide( x,y ) << endl ;

    return 0 ;
}
int Plus(int x , int y)         //加法函數
{
    return x+y ;
}
int Minus(int x , int y)        //減法函數
{
    return x-y ;
}
int Multiply(int x , int y)     //乘法函數
{
    return x*y ;
}
float Divide(float x , float y) //除法函數
{
    return (float)x/y ;
}
void Change(int &x , int &y)    //數字交換函數
{
    int s=0;
    s = x ;
    x = y ;
    y = s ;
}
