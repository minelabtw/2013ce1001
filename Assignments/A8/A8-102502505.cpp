#include <iostream>
using namespace std;

float x;//定義變數x
float y;//定義變數y
int a;
int b;

int Plus()//定義函式1
{
    return x+y;//x與y相加
}
int Minus()//定義函式2
{
    return x-y;
}
int Multiply()//定義函式3
{
    return x*y;
}
float Divide()//定義函式4，並把變數準確值提高，使相除有較精確值
{
    return x/y;
}
void Change(float &x , float &y)//定義x與y變換的函式
{
    a=x;//先將a變為x
    b=y;//將b變為y
    x=b;//將x等於b，即可不變y值的情況下，變換x值
    y=a;//將y等於a，即可完成互換
}

int main()
{
    cout << "Please enter x ( >0 ): ";//輸出字串
    cin >> x;//輸入變數x

    while ( x<=0 )//加入條件使x大於零
    {
        cout << "Out of range!" <<endl;
        cout << "Please enter x ( >0 ): ";
        cin >> x;//重新輸入x值
    }

    cout << "Please enter y ( >0 ): ";
    cin >> y;

    while ( y<=0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter y ( >0 ): ";
        cin >> y;
    }

    cout << "First time calculation" << endl;//輸出字串
    cout << "x + y = " << Plus() << endl;//依序呼叫函式
    cout << "x - y = " << Minus() << endl;
    cout << "x * y = " << Multiply() << endl;
    cout << "x / y = " << Divide() << endl;

    cout << "After change function..." << endl;//輸出字串
    cout << "Second time calculation" << endl;

    Change(x,y);//呼叫變換函式，利用call by reference把xy值對調，並記錄

    cout << "x + y = " << Plus() << endl;//呼叫各函式，並輸出對調後的各值
    cout << "x - y = " << Minus() << endl;
    cout << "x * y = " << Multiply() << endl;
    cout << "x / y = " << Divide() << endl;

    return 0;//返回初始值

}










