#include <iostream>

using namespace std;

int Plus(int x , int y)             //宣告加法的函式
{
    return x + y;
}
int Minus(int x , int y)            //宣告減法的函式
{
    return x - y;
}
int Multiply(int x , int y)         //宣告乘法的函式
{
    return x * y;
}
float Divide(float x , float y)     //宣告除法的函式
{
    return x / y;
}
void Change(int &x , int &y)        //宣告調換變數的函式
{
    int z;
    z=x;
    x=y;
    y=z;
}

int main()
{
    int number1=0;                                               //宣告變數
    int number2=0;
    do
    {
        cout << "Please enter x ( >0 ): ";
        cin >> number1;
        if(number1<=0)
            cout << "Out of range!" << endl;
    }
    while(number1<=0);                                           //當number1不大於0時，再執行一次
    do
    {
        cout << "Please enter y ( >0 ): ";
        cin >> number2;
        if(number2<=0)
            cout << "Out of range!" << endl;
    }
    while(number2<=0);                                           //當number2不大於0時，再執行一次

    cout << "First time calculation" << endl
         << "x + y = " << Plus(number1 , number2) << endl        //呼叫函式
         << "x - y = " << Minus(number1 , number2) << endl
         << "x * y = " << Multiply(number1 , number2) << endl
         << "x / y = " << Divide(number1 , number2) << endl
         << "After change function..." << endl;
         Change(number1 , number2);                              //呼叫更換變數的函式
    cout << "Second time calculation" << endl
         << "x + y = " << Plus(number1 , number2) << endl
         << "x - y = " << Minus(number1 , number2) << endl
         << "x * y = " << Multiply(number1 , number2) << endl
         << "x / y = " << Divide(number1 , number2) << endl;

    return 0;
}
