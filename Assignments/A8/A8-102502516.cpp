#include <iostream>
#include <limits>

using namespace std;

int main()
{
    int x , y;      //宣告x,y兩變數
    void inputLegal( int &x , char y);
    int Plus(int x , int y);
    int Minus(int x , int y);
    int Multiply(int x , int y);
    float Divide(float x , float y);
    void Change(int &x , int &y);       //一堆prototype

    inputLegal ( x , 'x');
    inputLegal( y , 'y');           //向使用者要求x與y的值
    cout << "First time calculation" << endl ;      //第一次運算
    for ( int counter = 0 ; counter <= 1 ; counter ++ )
    {
        cout << "x + y = " << Plus ( x, y ) << endl <<
             "x - y = " << Minus ( x, y ) << endl <<
             "x * y = " << Multiply ( x, y ) << endl <<
             "x / y = " << Divide ( float(x), float(y) ) << endl ;      //呼叫四個函式執行四則運算
        if (counter == 0)
        {
            Change ( x , y );       //第二次運算前先交換x,y之值
            cout << "After change function..." << endl <<
                 "Second time calculation" << endl ;        //第二次運算
        }
    }
    return 0;
}

void inputLegal (int &input , char variable )       //判斷使用者輸入是否合法的函式
{
    do
    {
        cout << "Please enter " << variable <<" ( >0 ): ";
        cin >> input;
        if ( input <= 0 || !(cin) )     //限定"正""整數"
        {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Out of range!\n";
        }
    }
    while ( input <= 0 || !(cin) );
}
int Plus ( int x , int y)           //兩數相加
{
    return x + y;
}
int Minus ( int x , int y)          //兩數相減
{
    return x - y;
}
int Multiply ( int x , int y)       //兩數相乘
{
    return x * y;
}
float Divide ( float x , float y)   //兩數相除
{
    return x / y;
}
void Change ( int &x , int &y)      //交換兩數
{
    x -= y;         //x儲存兩數之差
    y += x;         //把y加上兩數之差（y值交換完成)
    x = y - x;      //新的x為y減掉兩數之差 （x值交換完成）
}
