#include<iostream>
using namespace std;
int Plus(int x, int y){
	return x + y;
}
int Minus(int x, int y){
	return x - y;
}
int Multiply(int x, int y){
	return x * y;
}
float Divide(float x, float y){
	return x / y;
}
void Change(int &x, int &y){
	int tmp = x;
	x = y;
	y = tmp;
}
void PrintResult(int x,int y){ // print four results
	cout << "x + y = " << Plus(x, y) << endl;
	cout << "x - y = " << Minus(x, y) << endl;
	cout << "x * y = " << Multiply(x, y) << endl;
	cout << "x / y = " << Divide((float)x, (float)y) << endl;
}
int main(){
	int x, y;
	do{
		cout << "Please enter x(>0) : ";
		cin >> x;
	} while (x <= 0 && cout << "Out of range!" << endl);
	do{
		cout << "Please enter y(>0) : ";
		cin >> y;
	} while (y <= 0 && cout << "Out of range!" << endl);
	cout << "First time calculation" << endl;
	PrintResult(x, y);
	cout << "After change function..." << endl;
	Change(x, y);
	cout << "Second time calculation" << endl;
	PrintResult(x, y);
	return 0;
}