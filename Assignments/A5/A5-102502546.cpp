#include <iostream>
#include<iomanip>
using namespace std;

int main()
{
    int a,x,y=0;//使三變數初始值為0

    cin >>a;//輸入a值

    switch(a)//a值將決定case1 or 2 or default
    {
    case 1:
        for(int x=1; x<10; x++)//迴圈
        {
            for(int y=1; y<10; y++)//迴圈
            {
                cout <<x<<"*"<<y<<"="<<setw(2)<<x*y<<" ";//輸出為x*y=  的模式 且後面貼齊
            }
            cout <<endl;//每九個就換行
        }
        break;//case1跑完即結束
    case 2:
        for(int y=1; y<10; y++)
        {
            for(int x=1; x<10; x++)
            {
                cout <<x<<"*"<<y<<"="<<setw(2)<<x*y<<" ";
            }
            cout <<endl;
        }
        break;
    default://當輸入數字不合，即顯示錯誤 且結束
        cout <<"Error!";
        break;
    }
    return 0;
}
