#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    ios::sync_with_stdio(0);
    int number; //宣告一整數變數
    cin>>number;
    switch(number)  //switch判斷輸入數字
    {
        case 1: //當等於1的情形
            for(int i=1; i<=9; ++i)
            {
                for(int j=1; j<=9; ++j)
                {
                    cout<<i<<'*'<<j<<'='<<setw(2)<<i*j<<" ";
                }
                cout<<endl;
            }
        break;
        case 2: //當等於2的情形
            for(int i=1; i<=9; ++i)
            {
                for(int j=1; j<=9; ++j)
                {
                    cout<<j<<'*'<<i<<'='<<setw(2)<<i*j<<" ";
                }
                cout<<endl;
            }
        break;
        default:  //其他的情形
            cout<<"Error!"<<endl;
        break;
    }
    return 0;
}
