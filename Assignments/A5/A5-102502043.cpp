#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a,b,c;                                                   //宣告變數
    cin>>c;                                                      //輸入數字
    switch(c)                                                    //進入switch
    {
    case 1:                                                      //當輸入數字為1
        for(a=1; a<=9; a++)                                      //進入for迴圈
        {
            for(b=1; b<=9; b++)                                  //進入for迴圈
            {
                cout<<a<<"*"<<b<<"="<<setw(2)<<a*b<<" ";         //輸出字串1
            }
            cout<<endl;
        }
        break;                                                   //跳出switch
    case 2:                                                      //當輸入數字2
        for(b=1; b<=9; b++)                                      //進入for迴圈
        {
            for(a=1; a<=9; a++)                                  //進入for迴圈
            {
                cout<<a<<"*"<<b<<"="<<setw(2)<<a*b<<" ";         //輸出字串
            }
            cout<<endl;
        }
        break;                                                   //跳出sitch
    default:                                                     //預設值
    {
        cout<<"Error!";                                          //顯示字串
    }
    break;                                                       //跳出switch
    }
    return 0;
}
