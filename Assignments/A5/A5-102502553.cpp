#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int a = 0; //令一個整數變數初始化=0

    cin >> a; //輸入字串

    switch ( a ) //選擇狀況
    {
    case 1: //輸入為1的狀況
        for ( int b = 1; b <= 9; b++ ) //令整數變數初始值=1，做到9停止，每做一次b+1
        {
            for ( int c = 1; c <= 9; c++ )
            {
                cout << b << "*" << c << "=" << setw ( 2 ) << b * c << " "; //顯示字串
            }
            cout << endl;
        }
        break; //跳出選擇

    case 2:
        for ( int b = 1; b <= 9; b++ )
        {
            for ( int c = 1; c <= 9; c++ )
            {
                cout << c << "*" << b << "=" << setw ( 2 ) << b * c << " ";
            }
            cout << endl;
        }
        break;

    default: //其他狀況
        cout << "Error!";
        break;
    } //結束選擇
    return 0; //結束
}
