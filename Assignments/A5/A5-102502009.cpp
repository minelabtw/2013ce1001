#include<iostream>
#include<iomanip>
using namespace std;

int main()
{

    int type=0; // set variable;

    cin>>type; // intput the type

    switch(type)
    {
    case 1: // if type=1, output the multiplication table
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";
            }

            cout<<endl;
        }
        break;

    case 2: // if type=2, output the multiplication table
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
            {
                cout<<j<<"*"<<i<<"="<<setw(2)<<i*j<<" ";
            }

            cout<<endl;
        }
        break;

    default: // if type!=1 or type!=2, output "Error!"
        cout<<"Error!"<<endl;
        break;
    }

    return 0;

}
