#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int number;//宣告各個整數,a跟b的初始值為1
    int a = 1;
    int b = 1;

    number = cin.get();//輸入進去的值給switch做判斷

    switch ( number )
    {
    case '1'://當值為1時做以下動作並離開迴圈
        for ( a = 1; a <= 9; a++ )
        {
            for ( b = 1; b <= 9; b++ )
            {
                cout << a << "*" << b << "=" << setw(2) << a * b << " ";
            }
            cout << endl;
        }
        break;

    case '2'://當值為2時做以下動作並離開迴圈
        for ( a = 1; a <= 9; a++ )
        {
            for ( b = 1; b <= 9; b++ )
            {
                cout << b << "*" << a << "=" << setw(2) << b * a << " ";
            }
            cout << endl;
        }
        break;

    default:
        cout << "Error!";
        break;
    }

    return 0;
}
