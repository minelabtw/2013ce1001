#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int q = 0;//宣告整數q
    cin >> q;//輸入q
    for(int a=1; a<=9; a++)//迴圈 當a等於1 a小於等於9 a遞增
    {
        for(int b=1; b<=9; b++)//迴圈 當b等於1 b小於等於9 b遞增
        {
            switch(q)//依q判斷
            {
            case 1://q=1時
                cout << a << "*"  << b << "=" << setw(2) << a*b << " ";//輸出a*b=積(對齊=後兩格) 空格
                break;//結束判斷
            case 2://b=1時
                cout << b << "*"  << a << "=" << setw(2) << a*b << " ";//輸出b*a=積(對齊=後兩格) 空格
                break;//結束判斷
            }
        }
        switch(q)//依q判斷
        {
        case 1://q=1時
        case 2://q=2時
            cout << endl;//輸出 換行
        }
    }
    switch(q)//依q判斷
    {
    case 1://q=1時
    case 2://q=2時
        break;//結束
    default://其餘
        cout << "Error!";//輸出Error!
    }
    return 0;
}
