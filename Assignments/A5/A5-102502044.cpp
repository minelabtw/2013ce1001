/*************************************************************************
    > File Name: A5-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年10月24日 (週四) 17時41分24秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

int main()
{
	/* get the first char */
	char input = getchar();

	/* if input 1, then output 9*9 multi table by mode 1 */
	/* if input 2, then output 9*9 multi table by mode 2 */
	/*     else  , the output error message              */
	switch(input)
	{
		case '1': // mode 1
			for(int i=1 ; i<=9 ; putchar('\n'), i++)
				for(int j=1 ; j<=9 ; j++)
					printf("%d*%d=%2d ", i, j, i*j);
			break;

		case '2': // mode 2
			for(int i=1 ; i<=9 ; putchar('\n'), i++)
				for(int j=1 ; j<=9 ; j++)
					printf("%d*%d=%2d ", j, i, i*j);
			break;

		default: // error
			puts("Error!");
	}

	return 0;
}

