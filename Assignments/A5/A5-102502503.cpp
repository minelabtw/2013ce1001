#include <iostream>
using namespace std;
int main()
{
    int a;  //宣告整數變數a
    int b=1;  //宣告整數變數b,並初始化其值為1
    int c=1;  //宣告整數變數c,並初始化其值為1

    cin >>  a;  //將輸入的值給a

    switch (a)  //使用swith判斷a值
    {
    case 1:  //若a值為1則進行下列程式
        for (b=1; b<=9; b++)  //使用for迴圈使b值從1跑到9
        {
            for (c=1; c<=9; c++)  //使用for迴圈使c值從1跑到9
            {
                switch (b*c)
                {
                case 1:  //若b*c值為1
                case 2:  //或2
                case 3:  //或3
                case 4:  //或4
                case 5:  //或5
                case 6:  //或6
                case 7:  //或7
                case 8:  //或8
                case 9:  //或9
                    cout << b << "*" << c << "= " << b*c << " ";
                    break;  //停止switch

                default:  //若b*c值不為個位數
                    cout << b << "*" << c << "=" << b*c << " ";
                    break;  //停止switch
                }
            }
            cout << endl;
        }
        break;  //停止switch

    case 2:  //若a值為2則進行下列程式
        for (c=1; c<=9; c++)  //使用for迴圈使c值從1跑到9
        {
            for (b=1; b<=9; b++)  //使用for迴圈使b值從1跑到9
            {
                switch (b*c)
                {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    cout << b << "*" << c << "= " << b*c << " ";
                    break;

                default:
                    cout << b << "*" << c << "=" << b*c << " ";
                    break;
                }
            }
            cout << endl;
        }
        break;  //停止switch

    default:  //若a不為1或2則進行下列程式
        cout << "Error!";
        break;  //停止switch
    }

    return 0;
}
