#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int a=0;//宣告變數a,初始值為0
    int i,j;//宣告兩個變數i,j

    cin >> a;//輸入值給變數a

    switch(a)//根據a的值進入標示所對應值的標籤處
    {
        case 1://a值為1時
            for(j=1;j<10;j++)//initialization;test-expression;update-expression
            {
                for(i=1;i<10;i++)//initialization;test-expression;update-expression
                {
                    cout << j << "*" << i << "=" << setw(2) << i*j << " ";//輸出i,j二值相乘結果
                }
                cout << endl;//換行
                i=1;//i的初始值回到1
            }
            break;//跳出switch

        case 2://a值為2時
            for(j=1;j<10;j++)//initialization;test-expression;update-expression
            {
                for(i=1;i<10;i++)//initialization;test-expression;update-expression
                {
                    cout << i << "*" << j << "=" << setw(2) << i*j << " ";//輸出i,j二值相乘結果
                }
                cout << endl;//換行
                i=1;//i的初始值回到1
            }
            break;//跳出switch

        default ://沒有標籤符合時,跳至default敘述
            cout << "Error!" << endl;//輸出字串並換行
            break;//跳出switch
    }

    return 0;
}
