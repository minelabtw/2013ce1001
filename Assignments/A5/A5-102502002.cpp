#include <iostream>
#include <iomanip>      // std::setw
using namespace std;

int main()
{
    int type=0;         //宣告一變數type，並令其初始值為零。

    cin >> type;        //輸入type。
    switch ( type )
    {
    case 1:             //當type為1
        for( int n1=1; n1<=9; n1++)              //對於n1等於1到9
        {
            for ( int n2=1; n2<=9; n2++)         //對於n2等於1到9
            {
                cout << n1 << "*" << n2 << "=" << setw(2) << n1*n2 << " ";   //輸出n1乘n2的等式，並使其向右靠齊。
            }
            cout << endl;                        //換行。
        }
        break;          //跳出switch。

    case 2:             //當type為2
        for( int n1=1; n1<=9; n1++)              //對於n1等於1到9
        {
            for ( int n2=1; n2<=9; n2++)         //對於n2等於1到9
            {
                cout << n2 << "*" << n1 << "=" << setw(2) << n1*n2 << " ";   //輸出n2乘n1的等式，並使其向右靠齊。
            }
            cout << endl;                        //換行。
        }
        break;          //跳出switch。

    default:            //除了1和2以外的輸入
        cout << "Error!";                        //輸出Error!
        break;          //跳出switch。
    }

    return 0;
}
