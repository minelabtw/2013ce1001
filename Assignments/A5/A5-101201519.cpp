/*
本題禁止使用while、if else。
請使用for迴圈來輸出一個九九乘法表，每個式子相乘的結果必須靠右對齊。

一開始可以輸入一個數字(1或2)來選擇排版的模式(共有兩個九九乘法表排版的模式)，而輸入其他字元或數字則輸出”Error!”。輸入一次即可，不需要重複輸入。

可以參考課本第四章的switch、setw、for的用法。
*/
#include <iostream>
#include <iomanip>//引入函式setw
using namespace std;

int main()
{
    char mode;

    cin >> mode;
    switch(mode)//兩種九九乘法表模式的轉換
    {
    case '1'://在數字1的情況下
    {
        for(int a=1; a<=9; a++)
        {
            for(int b=1; b<=9; b++)
            {
                cout << a << "*" << b << "=" << setw(2) /*對齊右邊兩格*/ << a*b << " ";
            }
            cout << endl;
        }
    }
    break;//跳出第一個case

    case '2'://在數字2的情況下
    {
        for(int a=1; a<=9; a++)
        {
            for(int b=1; b<=9; b++)
            {
                cout << b << "*" << a << "=" << setw(2) << a*b << " ";
            }
            cout << endl;
        }
    }
    break;//跳出第二個case

    default://排除除一和二模式之外
        cout << "Error!";
        break;//跳出default

    }
    return 0;
}
