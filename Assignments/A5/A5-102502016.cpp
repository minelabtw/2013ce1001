#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int a;//設變數a

    cin >>a;
    switch (a)
    {
    case 1://第一種情況
        for (int b=1; b<=9; b++)//列出第一種99乘法表
        {
            for (int c=1; c<=9; c++)
            {
                cout <<b<<"*"<<c<<"="<<setw(2)<<(b*c)<<" ";
            }
            cout <<endl;
        }
        break;

    case 2://第二種情況
        for (int b=1; b<=9; b++)//列出第2種99乘法表
        {
            for (int c=1; c<=9; c++)
            {
                cout <<c<<"*"<<b<<"="<<setw(2)<<(b*c)<<" ";
            }
            cout <<endl;
        }
        break;

    default://列輸入的不是1或2,則直接到這
        cout << "Error!";
        break;
    }

    return 0;
}
