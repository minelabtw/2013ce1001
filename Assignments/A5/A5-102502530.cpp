#include<iostream>
#include<iomanip>
int main()
{
    char num;   //宣告

    num=std::cin.get(); //輸入
    for(;std::cin.get()!='\n';num=0);

    switch(num)
    {
    case '1':   //輸出模式一
        for(int i=0;i!=81;i++)
            std::cout<<i/9+1<<'*'<<i%9+1<<'='<<std::setw(2)<<(i%9+1)*(i/9+1)<<((i+1)%9?" ":" \n");
        break;
    case '2':   //輸出模式二
        for(int i=0;i!=81;i++)
            std::cout<<i%9+1<<'*'<<i/9+1<<'='<<std::setw(2)<<(i%9+1)*(i/9+1)<<((i+1)%9?" ":" \n");
        break;
    default:    //輸出Error!
        std::cout<<"Error!\n";
    }

    return 0;
}
