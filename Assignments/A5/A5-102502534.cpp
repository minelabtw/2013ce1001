#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int a=0;//宣告變數a
    cin>>a;//輸入變數a

    switch(a)
    {
    case 1://輸入的變數為1時
        for(int x=1; x<=9; x++)
        {
            for(int y=1; y<=9; y++)
            {
                cout<<x<<"*"<<y<<"="<<setw(2)<<x*y<<" ";
            }
            cout<<endl;
        }
        break;

    case 2://輸入的變數為2時
        for(int x=1; x<=9; x++)
        {
            for(int y=1; y<=9; y++)
            {
                cout<<y<<"*"<<x<<"="<<setw(2)<<y*x<<" ";
            }
            cout<<endl;
        }
        break;

    default://輸入的變數不為1&2時
        cout<<"Error!";
    }

    return 0;
}
