#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int a; //設一變數為a
    int b; //設一變數為b
    int c; //設一變數為c

    cin>> c; //輸入c數

    switch(c)
    {
    case 1: //當c符合case中的數字時，執行下列動作
    {
        for(int a=1; a<=9; a++) //設a初始值為1，持續遞增a直到a不符合條件為止
        {
            for(int b=1; b<=9; b++) //設b初始值為1，持續遞增b直到b大於等於9
            {
                cout << a << "*" << b << "="<<setw (2) << a*b << " "; //使用setw將答案控制為兩格
            }
            cout << endl;
        }
        break; //當a大於等於9時，跳出for迴圈
    }
    case 2: //當c符合case中的數字時，執行下列動作
    {
        for(int a=1; a<=9; a++) //設a初始值為1，持續遞增a直到a不符合條件為止
        {
            for(int b=1; b<=9; b++) //設b初始值為1，持續遞增b直到b大於等於9
            {
                cout << b << "*" << a << "="<<setw (2) << a*b << " "; //使用setw將答案控制為兩格
            }
            cout << endl;
        }
        break; //當a大於等於9時，跳出for迴圈
    }
    default: //當c不符合上面case中的東西時，執行下列動作
        cout << "Error!"<<endl;
        break; //執行完後，跳出
    }
    return 0;
}




