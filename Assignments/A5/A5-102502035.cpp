#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int form =0;//宣告變數並=0
    int i =0;
    int j =0;
    cin >> form;//輸入格式
    switch(form)//選擇格式
    {
    case 1://選擇1
        for(i=1; i<=9; i++)//迴圈行
        {
            for(j=1; j<=9; j++)//迴圈列
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";//輸出乘法表
            }
            cout << endl;//換行
        }
        break;//終止switch
    case 2:
        for(i=1; i<=9; i++)//迴圈行
        {
            for(j=1; j<=9; j++)//迴圈列
            {
                cout << j << "*" << i << "=" << setw(2) <<  i*j << " ";//輸出乘法表
            }
            cout << endl;//換行
        }
        break;//終止switch
    default://抓出其它值
        cout << "Error!";//提示不符合
        break;//終止switch
    }
    return 0;
}
