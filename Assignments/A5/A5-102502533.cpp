#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int a=0;//宣告整數a


    cin>>a;

    switch(a)//判斷a
    {
    case 1://出現a,就跑橫排的九九九乘法
        for( int a=1; a<=9; a++)
        {

            for( int b=1; b<=9; b++)
            {
                cout <<a<<"*"<<b<<"="<<setw( 2 )<<a*b<<" ";
            }
            cout<<endl;
        }
        break;

    case 2://出現b,就跑直排的九九乘法
        for( int a=1; a<=9; a++)
        {
            for( int b=1; b<=9; b++)
            {
                cout<<b<<"*"<<a<<"="<<setw( 2 )<<a*b<<" ";
            }
            cout<<endl;
        }
        break;

    default://如果不是1或2,就跑出Error!
        cout<<"Error!"<<endl;
        break;

    }


    return 0;
}
