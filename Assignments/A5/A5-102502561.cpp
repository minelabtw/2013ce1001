#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int x;
    int y;
    int number;
    cin >> number;

    switch(number)
    {
    case 1:
        for(x=1; x<=9; x++)
        {
            for(y=1; y<=9; y++)
            {
                cout << x << "*" << y  << "= " << setw( 2 ) << x*y << "\t";
            }
            cout << "\n";
        }
        break;

    case 2:
        for(y=1; y<=9; y++)
        {
            for(x=1; x<=9; x++)
            {
                cout << x << "*" << y  << "= " << setw( 2 ) << x*y << "\t";
            }
            cout << "\n";
        }
        break;

    default :
        cout << "Error!";
    }
    return 0;
}

