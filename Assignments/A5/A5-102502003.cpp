#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int i=1;
    int j=1;
    int form=0;
    int sum=0;


    cin>>form;  //輸入一個數、選擇排版模式
    switch(form)  //選擇輸出的排版模式
    {
    case 1:
        for(i=1; i<=9; i++)  //輸出九九乘法表
        {
            for(j=1; j<=9; j++)
            {
                sum=i*j; //相乘
                cout<<i<<"*"<<j<<"="<<setw(2)<<sum<<" ";  //setw對齊格式
            }
            cout<<endl;

        }
        break;

    case 2:
        for(i=1; i<=9; i++)  //輸出九九乘法表
        {
            for(j=1; j<=9; j++)
            {
                sum=i*j;
                cout<<j<<"*"<<i<<"="<<setw(2)<<sum<<" ";  //setw對齊格式
            }
            cout<<endl;
        }
        break;

    default:  //除了1、2之外的情況
        cout<<"Error!"<<endl;
        break;

    }


}
