#include<iostream>
#include<iomanip>
using namespace std;


int main()
{
    int number1=0;    //宣告一變數number1=0
    int i;    //宣告一變數i
    int j;    //宣告一變數j

    cout << "";    //輸出至螢幕
    cin >> number1;    //輸入至number1

    switch(number1)    //switch條件式
    {
    case 1 :
        for(i=1 ; i<=9 ; ++i)
        {
            for(j=1 ; j<=9 ; ++j)
            {
                cout << i << "*" << j << "=" << setw(2) << i*j  << " ";
            }
            j=1;
            cout << endl;
        }
        break;
    case 2 :
        for(i=1 ; i<=9 ; ++i)
        {
            for(j=1 ; j<=9 ; ++j)
            {
                cout << j << "*" << i << "=" << setw(2) << i*j  << " ";
            }
            j=1;
            cout << endl;
        }
        break;
    default :
        cout << "Error!";
        break;
    }
    return 0;
}
