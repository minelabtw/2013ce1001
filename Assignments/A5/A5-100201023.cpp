#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
	char input;

	cin >> input;

	switch(input)
	{
	case '1':
		for(int i = 1 ; i < 10 ; ++i)
		{
			for(int j = 1 ; j < 10 ; ++j)
				cout << i << "*" << j << "=" << setw(2) << i*j << " ";

			cout << endl;
		}
		break;

	case '2':
		for(int i = 1 ; i < 10 ; ++i)
		{
			for(int j = 1 ; j < 10 ; ++j)
				cout << j << "*" << i << "=" << setw(2) << i*j << " ";

			cout << endl;
		}
		break;

	default:
		cout << "Error!" << endl;
	}

	return 0;
}
