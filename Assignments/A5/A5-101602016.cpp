#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int selection=0;//給使用者選擇模式
    int i=0,j=0;//給for迴圈使用
    int product=0;//乘積

    cin>>selection;//選擇模式

    switch (selection)
    {
        case 1://模式1
            for(i=1;i<=9;i++)
            {
                for(j=1;j<=9;j++)
                {
                    product=i*j;//將i,j相乘
                    cout<<i<<"*"<<j<<"="<<setw(2)<<product<<" ";
                    //輸出乘法表，並用setw語法將字數訂定為2
                }
                cout<<endl;//換行
            }
            break;//跳出模式1

        case 2://模式2
            for(i=1;i<=9;i++)
            {
                for(j=1;j<=9;j++)
                {
                    product=i*j;//將i,j相乘
                    cout<<j<<"*"<<i<<"="<<setw(2)<<product<<" ";
                    //輸出乘法表，並用setw語法將字數訂定為2
                }
                cout<<endl;//換行
            }
            break;//跳出模式2

        default:
            cout<<"Error!";//輸入其餘數字皆為錯誤
    }
    return 0;
}
