#include<iostream>
#include<iomanip>

using namespace std;
int main()
{
    int value=0;  //設定變數
    cin >> value;  //輸入變數
    switch( value )
    {
    case 1:  //若輸入的值是1就執行下列迴圈
        for( int i = 1; i <= 9; i++ )
        {
            for( int j = 1; j <= 9; j++ )
            {
                cout << i << "*" << j << "=" << setw(2) << i * j << " ";
            }

            cout << endl;
        }
        break;

    case 2:
        for( int i = 1; i <= 9; i++ )
        {
            for( int j = 1; j <= 9; j++ )
            {
                cout << j << "*" << i << "=" << setw(2) << i * j << " ";
            }

            cout<<endl;
        }

    default:  //若輸入的值不是1或2就全部輸出Error!
        cout<<"Error!"<<endl;
        break;

    }
    return 0;
}
