#include <iostream>
#include <iomanip>
using namespace std ;

int main()
{
    int number ; //宣告變數number
    cin >> number ; //輸入number
    switch ( number ) // 進入下列case
    {

    case 1 : //條件 1
        for ( int a=1 ; a<=9 ; a++ ) //a=1 執行下列迴圈 直到a=9
        {
            for ( int b=1 ; b<=9 ; b++ ) //b=1 執行下列迴圈 直到b=9
            {
                cout << a << "*" << b << "=" << setw( 2 ) << a*b << " " ; //輸出a*b的值
            }

            cout << "\n" ; //換行
        }
        break ; //離開switch
    case 2 : // 條件2
        for ( int a=1 ; a<=9 ; a++ ) //同上
        {
            for ( int b=1 ; b<=9 ; b++ ) //同上
            {
                cout << b << "*" << a << "=" << setw( 2 ) << b*a << " " ; //輸出b*a的值
            }
            cout << "\n" ; //換行
        }
        break ; //離開switch

    default : //當條件不是上述時
        cout << "Error!" ; //輸出Error!
        break ; //離開switch


        return 0 ;
    }
