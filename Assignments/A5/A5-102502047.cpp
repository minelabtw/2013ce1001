#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int a=0;//宣告a,初始值為0
    cin>>a;
    switch(a)//判斷a為何數
    {
    case 1://若a=1
        for(int y=1; y<=9; y++)//列出九九乘法表
        {
            for(int x=1; x<=9; x++)
            {
                cout<<y<<"*"<<x<<"=";
                int i=x*y;
                cout<<setw(2)<<i<<" ";//提供2個空位給i
            }
            cout<<endl;
        }
        break;//跳出switch
    case 2://若a=2
        for(int y=1; y<=9; y++)
        {
            for(int x=1; x<=9; x++)
            {
                cout<<x<<"*"<<y<<"=";
                int i=x*y;
                cout<<setw(2)<<i<<" ";
            }
            cout<<endl;
        }
        break;
    default://若a!=1 or a!=2
        cout<<"Error!";
        break;
    }
    return 0;
}
