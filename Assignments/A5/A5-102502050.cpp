#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int i,j;
    int input;                                   //輸入
    cin >> input ;

    switch(input)
    {
    case 1:                                      //輸出第一種乘法表
        for(i=1; i<10; i++)
        {
            for(j=1; j<10; j++)
            {
                cout << i << "*" << j << "=" ;

                cout << setw(2) <<i*j << " " ;   // i*j佔兩格
            }
            cout << endl;
        }
        break;
    case 2:                                      //輸出第二種乘法表
        for(i=1; i<10; i++)
        {
            for(j=1; j<10; j++)
            {
                cout << j << "*" << i << "=" ;

                cout << setw(2) <<i*j << " " ;
            }
            cout << endl;
        }
        break;
    default :                                    //輸入錯誤
        cout << "Error!\n";
    }
    return 0;
}
