#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    // 用string接受輸入
    // 因為會輸入s1d 2f3#@這種奇怪的東西
    string s;
    cin >> s;

    /*
        如果size不等於1 這全部的數值就會變成0 否則就是 第一個字的ascii-'0'
    */
    switch( (s[0]-'0') * (int)(s.size() == 1))
    {
    case 1: // mode 1
        for (int i=1;i<=9;i++)
        {
            // 印出一項後 + 空白
            for (int j=2;j<=9;j++)
            {
                cout << i << '*' << j << '=' << setw(2) << i*j << ' ';
            }
            cout << endl;
        }
        break;
    case 2: // mode 2
        // 同上只是順序改了
        for (int i=1;i<=9;i++)
        {
            cout << 1 << '*' << i << '=' << setw(2) << i;
            for (int j=2;j<=9;j++)
            {
                cout << j << '*' << i << '=' << setw(2) << i*j << ' ';
            }
            cout << endl;
        }
        break;
    default:
        // 輸出錯誤
        cout << "Error!" << endl;
        break;
    }
    return 0;
}
