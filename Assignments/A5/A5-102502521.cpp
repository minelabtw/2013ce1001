#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int a;    //宣告變數
    int b;
    int c;

    a=cin.get();  //讀入a

    switch(a)    //利用switch判斷所輸入a之值對應的顯示是什麼
    {
    case'1':
        for(b=1; b<=9; b++)    //利用for迴圈印出九九乘法表
        {
            for(c=1; c<=9; c++)
            {
                cout<<b<<"*"<<c<<"="<<setw(2)<<b*c<<" ";  //印出算式，再利用setw排版
            }
            cout<<endl;
        }
        break;    //跳出switch

    case'2':
        for(c=1; c<=9; c++)
        {
            for(b=1; b<=9; b++)
            {
                cout<<b<<"*"<<c<<"="<<setw(2)<<b*c<<" ";
            }
            cout<<endl;
        }
        break;

    default:
        cout<<"Error!";
        break;
    }
    return 0;
}
