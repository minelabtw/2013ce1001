#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int n;//宣告變數n來接下來輸入的字
    n=cin.get();
    switch( n )//檢查輸入的字為何
    {
        case '1'://若為1則輸出一號版的成法表
            for(int i=1 ; i<=9 ; i++)//設前面的數為i從一到九
            {
                for(int j=1 ; j<=9 ; j++)//設後面的數為j從一到九
                {
                    cout << i << "*" << j << "=" << setw(2) << i*j << " ";//輸出字串i*j=ij並空一格
                }
                cout << "\n";//完成一排的輸出後(i從一到九)換行繼續輸出
            }
            break;
        case '2'://若為2則輸出二號版的成法表
            for(int j=1 ; j<=9 ; j++)//同上的輸出方式但是i,j(前後的數字)位置互換，輸出時也互換(列變行，行變列)
            {
                for(int i=1 ; i<=9 ; i++)
                {
                    cout << i << "*" << j << "=" << setw(2) << i*j << " ";
                }
                cout << "\n";
            }
            break;
        default://若輸入的字為1,2之外則輸出字串Error!
            cout << "Error!";
            break;
    }

    return 0;
}
