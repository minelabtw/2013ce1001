﻿#include <iostream>
#include <iomanip>

using namespace std;

int main(){
    int input;  //變數 控制模式一或二

    cin >> input;  //輸入變數

    switch( input ){  //輸出九九乘法表
        case 1:  //模式一
            for(int i = 1 ; i < 10 ; i++){
                for(int j = 1 ; j < 10 ; j++){
                    cout << i << "*" << j << "=" << setw(2) << i * j << " ";
                }
                cout << endl;
            }
            break;

        case 2:  //模式二
            for(int i = 1 ; i < 10 ; i++){
                for(int j = 1 ; j < 10 ; j++){
                    cout << j << "*" << i << "=" << setw(2) << j * i << " ";
                }
                cout << endl;
            }
            break;

        default:  //其他狀況輸出錯誤
            cout << "Error!";
            break;

    }
    return 0;
}
