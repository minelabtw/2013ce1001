#include<iostream>
#include<iomanip>
using namespace std ;
int main()
{
    int modle ; //modle
    cin >> modle ;
    switch (modle)
    {
    case 1:                         //modle=1
        for(int i=1; i<=9; i++) //列
        {
            for(int j=1; j<=9; j++) //行
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " " ; //列*行=(靠右)
            }
            cout << endl ; //換列
        }
        break; //跳出switch
    case 2:                        //modle=2
        for(int i=1; i<=9; i++) //列
        {
            for(int j=1; j<=9; j++) //行
            {
                cout << j << "*" << i << "=" << setw(2) << i*j << " " ; //行*列=(靠右)
            }
            cout << endl ; //換行
        }
        break ; //跳出switch
    default : //modle not 1 or 2
        cout << "Error!\n" ;
        break ; // 跳出switch
    }
    return 0;
}
