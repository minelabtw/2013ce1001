#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int way = 0;//宣告決定變數
    int num1 = 1;//宣告九九乘法表一號數變數
    int num2 = 2;//宣告九九乘法表二號數變數

    cin >> way;

    switch ( way )//switch迴圈進入的變數為決定變數
    {
    case 1://當決定變數為1
        for ( num1 = 1; num1 <= 9; num1++)//九九乘法表一號數變數開始跑
        {
            for ( num2 = 1; num2 <= 9; num2++)//九九乘法表二號數變數再跑
            {
                cout << num1 << "*" << num2 << "=" << setw( 2 ) << num1 * num2 << " ";//輸出九九乘法表1型并向右對齊
            }cout << endl;//九九乘法表一號數變數跑完後換行
        }break;//直接讀迴圈最後
    case 2://決定變數為2
        for ( num2 = 1; num2 <= 9; num2++)//九九乘法表二號數變數開始跑
        {
            for ( num1 = 1; num1 <= 9; num1++)//九九乘法表一號數變數開始跑
            {
                cout << num1 << "*" << num2 << "=" << setw( 2 ) << num1 * num2 << " ";//輸出九九乘法表2型并向右對齊
            }cout << endl;//九九乘法表二號數變數跑完後換行
        }break;//直接讀迴圈最後
    default://當決定變數為其他
        cout << "Error!" << endl;//直接輸出錯誤
        break;//直接讀迴圈最後
    }//結束switch迴圈
return 0;//迴歸到0
}
