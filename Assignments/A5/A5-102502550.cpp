#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a=0;

    cin>>a;                                                                  //輸入a
    switch (a)
    {
        case 1:                                                              //若輸入為1
           for(int i=1;i<=9;i++)
           {
              for(int j=1;j<=9;j++)
               {
                    cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";
               }
               cout<<endl;
           }
           break;
        case 2:                                                              //若輸入為2
           for(int i=1;i<=9;i++)
           {
              for(int j=1;j<=9;j++)
               {
                    cout<<j<<"*"<<i<<"="<<setw(2)<<i*j<<" ";
               }
               cout<<endl;
           }
           break;
        default :                                                            //若皆不是則輸出Error!
            cout<<"Error!";

    }
    return 0;
}
