#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int number=0;                                              //宣告變數

    cin >>number;                                              //輸出字元

    switch ( number )                                          //判斷執行動作1或是動作2
    {
    case 1:
    {
        for (int y=1; y<=9; y=y+1)
        {
            for (int x=1; x<=9; x=x+1)
            {
                cout <<y <<"*"<<x <<"="<<setw(2)<<y*x<<" ";    //顯示九九乘法表並且答案是靠右對齊兩格
            }
            cout <<endl;
        }
    }
    break;
    case 2:
    {
        for (int y=1; y<=9; y=y+1)
        {
            for (int x=1; x<=9; x=x+1)
            {
                cout <<x <<"*"<<y <<"="<<setw(2)<<x*y<<" ";
            }
            cout <<endl;
        }
    }
    break;
    case '\n':
    case '\t':
    case ' ':
        break;
    default:                                                    //若皆不是則顯示Error!
        cout <<"Error!";
        break;
    }
}
