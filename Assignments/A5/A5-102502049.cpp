#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int number=0; //宣告一個整數作為選擇型態用

    cin >> number; //輸入1或2選擇型態
    for (; number != 1 && number != 2;) //當輸入值不為1或2時顯示錯誤並要求重新輸入
    {
        cout << "Error!" << endl;
        cin >> number;
    }

    switch (number) //選擇型態
    {
    case 1:
        for ( int x=1 ; x<=9 ; x++ ) //輸出型態1的乘法表
        {
            for ( int y=1 ; y<=9 ; y++ )
            {
                cout << x << "*" << y << "=" << setw(2) << x*y << " ";
            }
            cout <<endl;
        }
        break;

    case 2:
        for ( int x=1 ; x<=9 ; x++ ) //輸出型態2的乘法表
        {
            for ( int y=1 ; y<=9 ; y++ )
            {
                cout << y << "*" << x << "=" << setw(2) << x*y << " ";
            }
            cout <<endl;
        }
        break;

    }


    return 0;
}
