#include<iostream>
#include<iomanip> //we need setw()

using namespace std;

int main()
{
    char format; //storing the input
    int i, j;

    cin >> format; //input the format (only the first charactor)

    switch(format)  //such like multiple if else
    {
    case '1':
        //if format == '1' then output following
        for(i=1; i<=9; i++) //row major
        {
            for(j=1; j<10; j++) //column
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";
                //output in i*j=k form
                //use setw(2) to output in two digits
            }
            cout << endl; //change line at the end
        }
        break; //end switch
    case '2':
        //if format == '2' then output following
        for(j=1; j<=9; j++) //column major
        {
            for(i=1; i<10; i++) //row
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";
                //output in j*i=k form
                //use setw(2) to output in two digits
            }
            cout << endl; //change line at the end
        }
        break; //end switch
    default:
        //if fail then error
        cout << "Error!";
    }
    return 0;
}
