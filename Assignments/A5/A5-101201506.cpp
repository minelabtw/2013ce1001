#include <iostream>
#include <iomanip>

using namespace std;

int main()
{

    int inputInteger=0; //宣告要輸入的整數

    cin >> inputInteger ;

    switch (inputInteger)  //判斷1.2兩種case 然後列出2種乘法表
    {
    case 1:
        for (int i=1 ; i<=9 ; i++)
        {
            for (int j=1 ; j<=9; j++)
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";
            }
            cout << endl;
        }
        break;
    case 2:
        for (int i=1 ; i<=9; i++)
        {
            for (int j=1 ; j<=9 ; j++)
            {
                cout << j << "*" << i << "=" << setw(2) <<i*j << " ";
            }
            cout << endl;
        }
        break;
    default:
        cout << "Error!";
        break;
    }

    return 0;
}
