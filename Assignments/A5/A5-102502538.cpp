#include<iostream>
#include <iomanip>
using namespace std;

int main()
{
    int x;
    int counter =1;

    cin >>x;
    switch ( x )
    {
    case 1:                                                                  //設定輸入1產生的迴圈
        for (int x=1; x<=9; x++)
        {
            for(int counter=1; counter<=9; counter++)
            {
                cout <<x <<"*"<< counter <<"="<<setw(2)<<x*counter<<" ";     //設定最小位數
            }
            cout <<endl;
        }
        break;
    case 2:                                                                  //設定輸入2產生的迴圈
        for (int x=1; x<=9; x++)
        {
            for(int counter=1; counter<=9; counter++)
            {
                cout << counter <<"*"<< x <<"="<<setw(2)<<x*counter<<" ";
            }
            cout <<endl;
        }
        break;
    default:                                                                 //除此之外都輸出Error!
        cout <<"Error!";
    }
    return 0 ;
}
