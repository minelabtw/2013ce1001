#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int number;//declare a variable to get input
    cin>>number;
    switch(number)
    {
    case 1: //the input number is 1
    {
        for(int j=1; j<=9; j++)
        {
            for(int i=1; i<=9; i++)
                cout<<j<<"*"<<i<<"="<<setw(2)<<j*i<<" ";//print 9*9 multiple table with the latter figure changing first, and align the result
            cout<<endl;//the first outer loop has accomplished and then jump to the next line
        }
        break;//escape from the loop if the foregoing condition happen
    }
    case 2:
    {
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
                cout<<j<<"*"<<i<<"="<<setw(2)<<j*i<<" ";//print 9*9 multiple table with the former figure changing first, and align the result
            cout<<endl;//the first outer loop has accomplished and then jump to the next line
        }
        break;//escape from the loop if the foregoing condition happen
    }
    default:
    {
        cout<<"Error!";//if the input number is not either 1 or 2 , then print error.
        break;//escape from the loop if the foregoing condition happen
    }
    }
    return 0;
}
