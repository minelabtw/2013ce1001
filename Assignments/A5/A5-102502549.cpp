#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    char n;//宣告選項

    cin>>n;

    switch(n)//判斷情況
    {

        //第一種情況
    case '1':
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";
            }
            cout<<endl;
        }
        break;

        //第二種情況
    case '2':
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
            {
                cout<<j<<"*"<<i<<"="<<setw(2)<<j*i<<" ";
            }
            cout<<endl;
        }
        break;

        //預設情況
    default:
        cout<<"Error!";
        break;
    }
    return 0;
}
