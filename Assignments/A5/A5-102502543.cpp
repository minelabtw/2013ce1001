#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int c=0;//宣告變數c
    cin >> c; //輸入
    switch(c)
    {
    case 1: //輸入1時進入
        for (int a=1; a<=9; a++) //輸出九九乘法表
        {
            for (int b=1; b<=9; b++)
            {
                cout <<a<<"*"<<b<<"="<<setw(2)<<a*b<<" ";
            }
            cout <<endl;
        }
        break; //跳出switch
    case 2: //輸入2時進入
        for (int a=1; a<=9; a++) //輸出九九乘法表
        {
            for (int b=1; b<=9; b++)
            {
                cout <<b<<"*"<<a<<"="<<setw(2)<<a*b<<" ";
            }
            cout <<endl;
        }
        break; //跳出switch
    default: //輸入其他字元時進入
        cout<<"Error!";
        break; //跳出switch
    }
    return 0;
}
