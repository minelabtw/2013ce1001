
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int a ;
    cin >> a ; //輸入變數a
    switch(a)  //當a等於1跟2時的兩種情況
    {
       int i,j ;
       case 1 :
          for(i=1; i<=9; ++i)  //i的初始值為1,如果i小於9,進入迴圈
          {
             for(j=1; j<=9; ++j)  //j的初始值為1,如果j小於9,進入迴圈
             {
               cout << i << "*" << j << "=" << std::setw(2) << i*j<<" "<< "\t" ;  //輸出i*j,並在等號後空兩個字元
             }
               cout << endl;
          }
        break;

       case 2:
          for(i=1; i<=9; ++i)  //i的初始值為1,如果i小於9,進入迴圈
          {
              for(j=1; j<=9; ++j)  //j的初始值為1,如果j小於9,進入迴圈
              {
                cout << j << "*" << i << "=" << std::setw(2) << i*j<<" "<< "\t" ;   //輸出j*i,並在等號後空兩個字元
              }
                cout << endl;
          }
        break;

       default:
       cout << "Error!";


    }


}
