#include<iostream>
#include<iomanip>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number ;
    cin >> number ;
    switch (number)
    {
    case 1:
        for (int i=1;i<=9;++i)
        {
            for (int j=1;j<=9;++j)
                cout << i << "*" << j << "=" <<setw(2) << i*j << " " ;
            cout << endl ;
        }
        break ;
    case 2:
        for (int i=1;i<=9;++i)
        {
            for (int j=1;j<=9;++j)
                cout << j << "*" << i << "=" <<setw(2) << i*j << " " ;
        cout << endl ;
        }
        break ;
    default:
        cout << "Error!" ;
        break ;
    }
    return 0;
}
