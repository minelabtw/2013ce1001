#include <iostream>

using namespace std ;

int main()
{
    int a = 0 ;  //設定一變數為a，並設其初始值為0。

    cin >> a ;  //讓操作者輸入，並將輸入值丟給a。

    switch ( a )  //開始對a的輸入去做判斷。
    {
    case 2 :  //若a為2，進行以下指令。
        for( int j = 1 ; j < 10 ; j ++ )  //for迴圈開始：設一初使值為1的變數為j，當j小於10就進行迴圈內指令，並在做完後將其值加1。
        {
            for( int i = 1 ; i < 10 ; i ++ )  //for迴圈開始：設一初使值為1的變數為i，當i小於10就進行迴圈內指令，並在做完後將其值加1。
            {
                cout << i << "*" << j ;  //輸出兩數相乘的算式。
                if( i * j >= 10 )
                    cout << "=" << i * j ;  //當ij相乘為二位數時，輸出"="及結果。
                else
                    cout << "= " << i * j ;  //若ij相乘不為二位數，輸出"= "及結果。
                cout << " " ;  //在表尾輸出空格。
            }
            cout << endl ;
        }
        break ;  //跳出條件判斷。

    case 1 :  //若a為1，進行以下指令。
        for( int i = 1 ; i < 10 ; i ++ )
        {
            for( int j = 1 ; j < 10 ; j ++ )
            {
                cout << i << "*" << j ;
                if( i * j >= 10 )
                    cout << "=" << i * j ;
                else
                    cout << "= " << i * j ;
                cout << " " ;
            }
            cout << endl ;
        }  //與上個雙重for迴圈類似，只是輸出格式有所不同。
        break ;  //跳出條件判斷。

    default :
        cout << "Error!" ;  //若不為二且不為一，就輸出"Error!"。
    } ;

    return 0 ;
}

