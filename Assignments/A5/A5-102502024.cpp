#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int  number=0,total=0,x=1,y=1;  //宣告形式為整數的變數跟總和
    cout<<"Please enter 1 or 2:";  //輸出要求
    cin>>number;  //輸入數字
    switch(number)  //產生乘法表
    {
    case 1:
        for(int y=1;y<=9; y++)
        {
            for(int x=1;x<=9; x++)
            {
                total=x*y;
                cout<<y<<"*"<<x<<"="<<setw(2)<<total<<" ";
            }
            cout<<endl;
            x=1;
        }
        break;
    case 2:
        for(int y=1;y<=9; y++)
        {
            for(int x=1;x<=9; x++)
            {
                total=x*y;
                cout<<x<<"*"<<y<<"="<<setw(2)<<total<<" ";
            }
            cout<<endl;
            x=1;
        }
        break;
    default:  //檢驗是否符合條件
        cout<<"Error!";
        break;

    }
    return 0;
}
