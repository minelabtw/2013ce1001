#include<iostream>

#include<iomanip>

using namespace std ;

int main ()
{
    int number=0  ;//宣告變數

    cin >> number ;

    switch (number)//用swith判斷，當輸入1,2的時候要跑哪個loop
    {
    case 1:
        for (int a=1;a<=9;a++)//用for迴圈
        {
           for (int b=1;b<=9;b++)
           {
              cout << a << "*" << b << "=" ;

              cout << setw(2) <<a*b ;

              cout << " " ;
           }

           cout << "\n" ;
        }
        break ;
    case 2:
        for (int b=1;b<=9;b++)
        {
            for (int a=1;a<=9;a++)
            {
              cout << a << "*" << b << "=" ;

              cout << setw(2) <<a*b ;

              cout << " " ;
            }

            cout << "\n" ;
        }
        break ;
    default:
        cout << "Error!" ;

        break ;
    }
    return 0 ;
}
