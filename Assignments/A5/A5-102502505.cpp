#include <iostream>
#include <iomanip>//使檔案可使用setw

using namespace std;

int main()
{
    int integer;//宣告變數

    cin >> integer;//輸入變數

    for ( int y = 1 ; integer == 1 && y <= 9 ; y++ )//使用for迴圈，並加入條件:變數等於1，縱行
    {
        for ( int x = 1 ; x <= 9 ; x++ )
        {
            cout << y << "*" << x << "=" << setw(2) << y*x << " ";//輸出九九乘法的字串:橫行，並使用setw來控制字元寬度
        }
        cout << endl;//在橫行顯示完後換行
    }

    for ( int y = 1 ; integer == 2 && y<= 9 ; y++ )//條件為變數等於2，縱行
    {
        for ( int x = 1 ; x <= 9 ; x++ )//橫行
        {
            cout << x << "*" << y << "=" << setw(2) << y*x << " ";//x與y位置互換，使輸出的九九乘法表不同
        }
        cout << endl;//在橫行顯示完後換行繼續顯示下一行
    }

    for ( int y = 1 ; integer !=1 && integer !=2 && y == 1 ; y++)//條件為變數不為1也不為2
    {
        cout << "Error!";//輸出字串
    }

    return 0;//返回初始值

}
