#include <iostream> //for input and output
#include <iomanip> //for setw

using std::cout; //standard the output
using std::cin; //standard the input
using std::setw; //standard the setw

int main () //main function
{
    int tmp = 0; //int the tmp
    int k = 0; // int the input

    cin >>k; //input the number

    switch (k) //determine the number
    {
    case 1: //if k is 1
        for (int j=1; j<10; j++) //output the data from up to down
        {
            for (int i=1; i<10; i++) //output the data from left to right
            {
                tmp = i * j; //the condition
                cout <<j<<"*"<<i; //print the words
                cout<<"="<<setw(2)<<tmp; //print the space
                cout<<"\t"; //print the space
            }
            cout <<"\n"; //change line
        }
        break; //end the case
    case 2: // if k is 2
        for (int j=1; j<10; j++) //output the data from up to down
        {
            for (int i=1; i<10; i++) //output the data from left to right
            {
                tmp = i * j; //the condition
                cout <<i<<"*"<<j; //print the words
                cout<<"="<<setw(2)<<tmp; //print the space
                cout<<"\t"; //print the space
            }
            cout <<"\n"; //change line
        }
        break; //end the case
    default: //if k is not nither 1 or 2
        cout <<"Error!\n"; //print error
        break; //end default
    }

    return 0; //indicate that program ended successfully
} // end the main function
