#include <iostream>
#include <iomanip>

using namespace std ;
int main ()
{
    int a=0 ;                                                   //宣告變數a並設定初始值為0
    cin>>a ;                                                    //輸入a值
    switch(a)                                                   //對a進行判斷
    {
    case 1:                                                     //當a值=1時
        for(int i1=1; i1<=9; i1++)                              //宣告i1=1 當i1<=9時 i1加1
        {
            for(int i2=1; i2<=9; i2++)                          //宣告i2=1 當i2<=2時 i2加1
            {
                cout<<i1<<"*"<<i2<<"="<<setw(2)<<i1*i2<<" ";    //先從i2(右邊)開始變大 且預留2格 由左而右
            }
            cout<<endl;                                         //跳出迴圈後換行
        }
        break;                                                  //跳出case
    case 2:                                                     //當a值=2
    for(int i1=1; i1<=9; i1++)                                  //宣告i1=1 當i1<=9時 i1加
        {
            for(int i2=1; i2<=9; i2++)                          //宣告i2=1 當i2<=2時 i2加1
            {
                cout<<i2<<"*"<<i1<<"="<<setw(2)<<i1*i2<<" " ;   //先從i2(左邊)開始變大 且預留2格 由左而右
            }
            cout<<endl;                                         //跳出迴圈後換行
        }
        break;                                                  //跳出case
    default:                                                    //其餘
        cout<<"Error!" ;                                        //顯示error
        break;                                                  //跳出迴圈
    }
}
