#include <iostream>
#include<iomanip>

using namespace std ;

int main()

{
    int a = 1 ;               //宣告整數變數a的初始值=1

    cin >> a ;                //輸入a

    switch ( a )              //轉換a變成3種結果
    {

    case 1 :                                          //第一種:a=1時 輸出第一種格式的九九乘法表
        for (int x = 1 ; x <= 9 ; x++)                //2個for迴圈製作九九乘法表
        {
            for(int y = 1 ; y <= 9 ; y ++)
            {
                cout << x << "*" << y << "="  << setw(2) << x*y << " " ;
            }
            cout << endl ;
        }
        break ;                //結束轉換


    case 2 :                                          //第二種:a=2時 輸出第二種格式的九九乘法表
        for (int y = 1 ; y <= 9 ; y++)                //2個for迴圈製作九九乘法表
        {
            for(int x = 1 ; x <= 9 ; x ++)
            {
                cout << x << "*" << y << "="  << setw(2) << x*y << " " ;
            }
            cout << endl ;
        }
        break ;                //結束轉換


    default:                                          //第三種:a不等於1也不等於2時輸出error並重新輸入
        cout << "Error!" << endl ;
        cin >> a ;
        break ;                //轉換

    }


    return 0 ;
}

