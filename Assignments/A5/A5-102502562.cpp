#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a=0;                                                            //宣告型別為整數的a 並初始化其值為0
    int i=1,j=1;                                                        //宣告型別為整數的i,j 並初始化其值為1

    cin >> a;                                                           //輸入一個值給a
    switch(a)                                                           //判定a的值
    {
    case 1:                                                             //判定a的值是否為1
        for(i=1; i<=9; i++)                                             //重複詢問並累加直到i<=9
        {
            for(j=1; j<=9; j++)                                         //重複詢問並累加直到j<=9
            {
                cout << i << "*" << j << "=" << setw( 2 ) << i*j;       //輸出"i的值*j的值=i*j的值"而i*j的值用兩格表示
                cout << " ";                                            //輸出空一格
            }
            cout << endl;                                               //輸出換行
        }
        break;                                                          //跳出switch到最下面

    case 2:                                                             //判定a的值是否為2
        for(i=1; i<=9; i++)                                             //重複詢問並累加直到i<=9
        {
            for(j=1; j<=9; j++)                                         //重複詢問並累加直到j<=9
            {
                cout << j << "*" << i << "=" << setw( 2 ) << i*j;       //輸出"j的值*i的值=i*j的值"而i*j的值用兩格表示
                cout << " ";                                            //輸出空一格
            }
            cout << endl;                                               //輸出換行
        }
        break;                                                          //跳出switch到最下面

    default:                                                            //判斷a是否為1或2的其他情形
        cout << "Error!";                                               //輸出"Error!"
        cout << endl;                                                   //輸出換行
        break;                                                          //跳出switch到最下面
    }

    return 0;
}
