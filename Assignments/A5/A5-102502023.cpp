#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    long int a; // initialize long integer a
    short int i,j; // initialize short integer i and j
    cin >> a;

    switch (a) // switch statement to check a is on demand
    {
    case 1: // if a is 1
        for (i=1; i<=9; i++) // outer for loop to print out the first number
        {
            for (j=1; j<=9; j++) // inner for loop to print out the second number
            {
                cout << i << "*" << j << "=" << setw( 2 ) << i*j << "\t"; // to print out the multipitation of i and j
            }
            cout << endl; // whenever finish the inner loop, print a new line
        }
        break; // exit switch

    case 2: // if a is 2
        for (j=1; j<=9; j++) // outer for loop to print out the first number
        {
            for (i=1; i<=9; i++) // inner for loop to print out the second number
            {
                cout << i << "*" << j << "=" << setw( 2 ) << j*i << "\t"; // to print out the multipitation of i and j
            }
            cout << endl; // whenever finish the inner loop, print a new line
        }
        break; // exit switch

    default: // if a is neither 1 nor 2
        cout << "Error!";
        break; // exit switch
    }

    return 0;
}
