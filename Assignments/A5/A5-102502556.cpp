#include <iostream>
#include <iomanip>
using namespace std;

int main ()
{
    int a = 0; //宣告型別為 整數(int) 的第一個變數以決定模式，並初始化其數值為0。
    int x = 0;
    int y = 0;
    cin >> a ;
    switch ( a ) //使用switch迴圈來針對不同的輸入值，能夠做不同的運算。
    {
    case 1:
        for ( x = 1; x <= 9; x++ ) //用for迴圈來做重複計算，列出完整的九九乘法表。
        {
            for ( y = 1; y <= 9; y++ )
            {
                cout << x << "*" << y << "=" << setw( 2 ) << x * y << " " ; //使用 setw 來做置右對齊的動作。
            }
            cout << endl;
        }
        break;

    case 2:
        for ( y = 1; y <= 9; y++ )
        {
            for ( x = 1; x <= 9; x++ )
            {
                cout << x << "*" << y << "=" << setw( 2 ) << x * y << " " ;
            }
            cout << endl;
        }
        break;

    default:
        cout << "Error!" ;
        break;
    }
    return 0;
}
