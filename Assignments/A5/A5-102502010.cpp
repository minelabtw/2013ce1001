#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    char number; //字元
    int i,j;
    cin.get(number);  //輸入字元
    switch(number)
    {
    case '1':  //判斷字元
        for(i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++)
            {
                cout<<i<<'*'<<j<<'='<<setw(2)<<i*j<<' ';  //跑出乘法表
            }
            cout<<endl;
        }
        break;

    case '2':  //判斷字元
        for(i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++)
            {
                cout<<j<<'*'<<i<<'='<<setw(2)<<i*j<<' ';  //跑出乘法表
            }
            cout<<endl;
        }
        break;

        default:  //其他字元
        cout<<"Error!"<<endl;
        break;
    }
    return 0;
}
