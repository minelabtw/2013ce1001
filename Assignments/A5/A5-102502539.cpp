#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int i;  //宣告變數
    int a;
    int b;

    switch ( i = cin.get() )    //輸入並判別
    {
        case '1':
            for ( a = 1 ; a < 10 ; a++ )    //九九乘法表
            {
                for ( b=1; b<10;b++)
                {
                    cout << a << "*" << b << "=" << setw( 2 ) << a*b << " " ;
                }
                cout << endl;
            }
            break;

        case '2':
            for ( a = 1 ; a < 10 ; a++ )    //九九乘法表
            {
                for ( b=1; b<10;b++)
                {
                    cout << b << "*" << a << "=" << setw( 2 ) << a*b << " " ;
                }
                cout << endl;
            }
            break;

        default:
            cout << "Error!" ;
    }

    return 0;
}
