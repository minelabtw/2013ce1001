#include <iostream>
#include <iomanip>
using namespace std;

int main ()
{

    int a=1; //設定三個變數
    int b=1;
    int c;
    cin >> c ; //輸入計算方式

    switch (c)  //計算方式
    {
    case 1 :  //方式1
        for (a=1; a<=9; a++ && cout << endl )  //在第一迴圈結束時換行
        {
            for(b=1; b<=9; b++ )
            {
                cout <<a<<"*"<<b<<"="<< setw(2) << a*b << " " ;
            }
        }
        break;

    case 2 :  //方式2
        for (b=1; b<=9; b++ && cout << endl )  //在第一迴圈結束時換行
        {
            for(a=1; a<=9; a++ )
            {
                cout <<a<<"*"<<b<<"="<< setw(2) << a*b  << " " ;
            }
        }
        break;

    default: //不以上方式內的
        cout << "Error!" ;
    }
    return 0 ;
}
