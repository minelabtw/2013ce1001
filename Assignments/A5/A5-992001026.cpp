#include<iostream>
#include<iomanip>
using namespace std ;

int main ()
{//宣告變數 &輸入變數
    int n ;
    cin >> n ;

// 選擇case 然後執行下面的東西 ,需要break 跳出
    switch ( n )
    {
    case 1 :

        for ( int i = 1 ; i < 10 ; ++i )
        {
            for ( int j = 1 ; j < 10 ; ++j )
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";
            }
            cout << endl ;
        }
        break;

    case 2 :

        for ( int i = 1 ; i < 10 ; ++i )
        {
            for ( int j = 1 ; j < 10 ; ++j )
            {
                cout << j << "*" << i << "=" << setw(2) << i*j << " ";
            }
            cout << endl ;
        }
        break ;
//非上面的case者執行
    default :
        cout << "Error!" ;
        break ;

    }

    return 0 ;
}

