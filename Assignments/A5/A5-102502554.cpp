#include <iostream>
#include <iomanip>
using namespace std;
int main ()
{
    int a;//宣告變數a
    cin >> a;//輸入a

    switch ( a )//設立switch條件判斷式
    {
    case 1://當a=1執行以下內容
        for(int i = 1; i < 10; i++)//設立for迴圈
        {
            for(int j = 1; j < 10; j++)//設立for迴圈
            {
                int k = i * j ;//宣告變數k=i*j
                cout << i << "*" << j << "=";
                cout << setw(2) << k;//輸出i*j=k並固定k的輸出格式為2字元
                cout << " ";//輸出1個等式後再輸出空格
            }
            cout << endl;//換行
        }
        break;//結束區塊的執行

    case 2://當a=2執行以下內容
        for(int j = 1; j < 10; j++)//設立for迴圈
        {
            for(int i = 1; i < 10; i++)//設立for迴圈
            {
                int k = i * j;//宣告變數k=i*j
                cout << i << "*" << j << "=";
                cout << setw(2) << k;//輸出i*j=k並固定k的輸出格式為2字元
                cout << " ";//輸出1個等式後再輸出空格
            }
            cout << endl;//換行
        }
        break;//結束區塊的執行

    default://當a不等於1或2時
        cout << "Error!";//輸出Error!
        break;//結束區塊的執行
    }
    return 0;
}
