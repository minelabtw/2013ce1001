#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a;                                                     //a=判斷輸入1 OR 2 d= 乘法表的值
    int d;
    cin>>a;
    switch(a)
    {
    case 1:                                                    //當a=1 時執行
        for(int b=1; b<=9; b++)                                //從1開始迴圈 迴圈到9
        {
            for(int c=1; c<=9; c++)                            //從1開始迴圈 迴圈到9
            {
                d=b*c;                                         //計算結果
                cout<<b<<"*"<<c<<"="<<setw(2)<<d<<" ";         //d蘭寬2
            }
            cout<<endl;
        }
        break;                                                 //結束
    case 2:                                                    //當a=2 執行
        for(int b=1; b<=9; b++)                                //從1開始迴圈 迴圈到9
        {
            for(int c=1; c<=9; c++)                            //從1開始迴圈 迴圈到9
            {
                d=b*c;                                         //計算結果
                cout<<c<<"*"<<b<<"="<<setw(2)<<d<<" ";         //d蘭寬2 c b 位置交換
            }
            cout<<endl;
        }
        break;                                                  //結束
    default:                                                    //a=2 1 以外的 執行
        cout<<"Error!";
        break;                                                  //結束
    }
    return 0;
}
