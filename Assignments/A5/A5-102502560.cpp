#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	int mode;
	cin >> mode;

	switch(mode)	//switch depended on condition
	{
		case 1:		//if mode is 1

		for(int n1=1 ; n1<=9 ; n1++){										//for first number 1~9
			for(int n2=1 ; n2<=9 ; n2++){									//for second number 1~9
				cout << n1 << "*" << n2 << "=" << setw(2) << n1*n2 << " ";	//print the multiplication table
			}
			cout << "\n";
		}
		break;

		case 2:		//if mode is 2

		for(int n1=1 ; n1<=9 ; n1++){										//for second number 1~9
			for(int n2=1 ; n2<=9 ; n2++){									//for first number 1~9
				cout << n2 << "*" << n1 << "=" << setw(2) << n1*n2 << " ";	//print the multiplication table
			}
			cout << "\n";
		}
		break;

		default:	//if mode is neither

		cout << "Error!";
		//break;
	}

	return 0;
}
