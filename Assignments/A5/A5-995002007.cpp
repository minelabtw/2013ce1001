/* 請使用for迴圈來輸出一個九九乘法表，每個式子相乘的結果必須靠右對齊。

一開始可以輸入一個數字(1或2)來選擇排版的模式(共有兩個九九乘法表排版的模式)，而輸入其他字元或數字則輸出”Error!”。輸入一次即可，不需要重複輸入。

可以參考課本第四章的switch、setw、for的用法。*/

#include<iostream>
using namespace std;

#include<iomanip>
using std::setw;


int main()
{

    int a;
    int sum=0;

    cout << "" ;
    cin >> a;

    switch(a)
    {
    case 1:                             //如果是1 就第一個模式
        for(int i=1; i<=9; i++)              // 重複詢問並把i 遞增
        {
            for(int j=1; j<=9; j++)
            {
                sum=i*j;                        //將兩個數字相加並把結果給sum
                cout << i << "*" << j << "="  << setw(2) << sum << " ";     // 將結果印出來並固定寬度為2
            }
            cout << endl;
        }
        break;          // 跳出去, 如果沒有break, 他會繼續印出case2

    case 2:
        for(int i=1; i<=9; i++)              // 重複詢問並把i 遞增
        {
            for(int j=1; j<=9; j++)
            {
                sum=i*j;                        //將兩個數字相加並把結果給sum
                cout << j << "*" << i << "="  << setw(2) << sum << " ";
            }
            cout << endl;
        }
        break;

    default:                            //如果不是上面的case就印出 error
        cout << "Error !!" << endl;

    }

    return 0;
}

