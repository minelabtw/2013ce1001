#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    string a;       //宣告一個字串a
    cin >> a;       //輸入字串a

    switch(a[0]-'0' * (int)(a.size()==1))       //計算該進入哪個case
    {
    case 1:
        for (int i=1 ; i<=9 ; i++)
        {
            for (int j=1 ; j<=9 ; j++)
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " " ;     //印出九九乘法表
            }
                 cout << endl;
        }
        break;                  //跳出switch

    case 2:
        for (int i=1 ; i<=9 ; i++)
        {
            for (int j=1 ; j<=9 ; j++)
            {
                cout << j << "*" << i << "=" << setw(2) << i*j << " " ;     //印出九九乘法表
            }
            cout << endl;
        }
        break;                  //跳出switch

    default:                    //其他case
        cout << "Error!";
        break;
    }
    return 0;
    }

