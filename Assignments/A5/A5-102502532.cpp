#include<iostream>
#include<iomanip>                           //使用setw
#include<cmath>
using namespace std;

int main()
{
    int input =0;

    cin>>input;

    switch  (input)                       //使用switch迴圈 判斷輸入的值
    {
    case 1 :                                   //數字不要用 ' '
        for ( int a =1 ; a <=9 ; a++ )                   //初始值 condition ++
        {
            for ( int b=1 ; b <=9 ; b++)
                cout<<a<<"*"<<b<<"="<<setw(2)<<a*b<<" ";
            cout<<endl;
        }
        break;                                           //結束

    case 2 :                                    //數字不要用 ' '
        for ( int a =1 ; a <=9 ; a++ )                     //初始值 condition ++
        {
            for ( int b=1 ; b <=9 ; b++)
                cout<<b<<"*"<<a<<"="<<setw(2)<<a*b<<" ";
            cout<<endl;
        }
        break;

    default:                                     //不是1或2的
        cout<<"Error!";
        break;
    }


    return 0;
}
