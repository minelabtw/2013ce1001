#include <iostream>
#include <iomanip>  // std::setw
using namespace std;

int main(){
    int input = 1;//宣告型態為int的輸入變數，並初始化為1

    cin >> input;//輸入input
    switch(input){
        case 1://乘數先遞增
            for(int i = 1; i <= 9; i++){
                for(int j = 1; j <= 9; j++){
                    cout << i << "*" << j << "=" << setw(2) << i * j << setw(1) << " ";
                }
                cout << endl;
            }
            break;
        case 2://被乘數先遞增
            for(int i = 1; i <= 9; i++){
                for(int j = 1; j <= 9; j++){
                    cout << j << "*" << i << "=" << setw(2) << i * j << setw(1) << " ";
                }
                cout << endl;
            }
            break;
        default://其他亂七八糟的輸入值
            cout << "Error!" << endl;
            break;
    }
    return 0;
}
