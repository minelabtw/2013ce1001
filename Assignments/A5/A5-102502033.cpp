#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int x;
    cin >> x;
    switch(x)
    {
    case 1:
        for(int a=1; a<=9; a++)
        {
            for(int b=1; b<=9; b++)
            {
                cout << a << "*" << b << "=" << setw(2) << a*b << " ";
            }
            cout << "\n";
        }
        break;

    case 2:
        for(int a=1; a<=9; a++)
        {
            for(int b=1; b<=9; b++)
            {
                cout << b << "*" << a << "=" << setw(2) << a*b << " ";
            }
            cout << "\n";
        }
        break;

    default:
        cout << "Error!";
        break;
    }
}
