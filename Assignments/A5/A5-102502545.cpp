#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a;                          //宣告一變數a
    a=cin.get();                    //使a的值可以代進switch
    switch(a)                       //使用switch做一迴圈,指其產生所需結果
    {
    case'1':
        int x,y;                     //宣告兩變數x,y
        for(x=1; x<10; x++)          //使用for做出兩個迴圈來完成99乘法表
        {
            for(y=1; y<10; y++)
            {
                cout<<x<<"*"<<y<<"="<<setw(2)<<x*y<<" ";
            }
            cout<<endl;
        }
        break;
    case'2':
        int g,m;                        //宣告兩變數g,m
        for(g=1; g<10; g++)            //使用for做出兩個迴圈來完成99乘法表
        {
            for(m=1; m<10; m++)
            {
                cout<<m<<"*"<<g<<"="<<setw(2)<<g*m<<" ";
            }
            cout<<endl;
        }
        break;
    default:
        cout<<"Error!";
        break;
    }
    return 0;
}
