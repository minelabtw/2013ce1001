#include<iostream>
#include <iomanip>
using namespace std ;
int main ()
{
    int type ; //宣告變數
    int i ,j ;
    cin >> type ; //輸入type
    {
        switch(type) //選擇type
        {
        case 1 : //當輸入1時
            for(i=1; i<=9; i++) //迴圈 i=1 在i<=9時執行 ,每執行一次i值+1
            {
                for (j=1; j<=9; j++)//迴圈 j=1 在j<=9時執行 ,每執行一次j值+1
                {
                    cout << i << "*" << j << "="<<setw(2) << i*j <<" " ;//輸出i * j = 值
                }
                cout << endl ; //換行

            }
            break; //離開case 1
        case 2 : //當輸入2時
            for(i=1; i<=9; i++) //同上
            {
                for (j=1; j<=9; j++) //同上
                {
                    cout << j << "*" << i << "="<<setw(2) << i*j <<" " ; //輸出j * i =值
                }
                cout << endl ; //換行
            }
            break; //離開case2
        default: //其他case
            cout << "Error!" ; //輸出error
            break;//離開default
        }
    }
    return 0;
}
