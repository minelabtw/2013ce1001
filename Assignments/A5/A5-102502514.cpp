#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int number;

    cin >>number;
    switch (number)
    {
    case 1: //方案1就是先讓y由1(左)往9(右)跑,再讓x由1(上)往9(下)跑,依序印出
    {
        for (int x=1; x<10; x++)
        {
            for (int y=1; y<10; y++)
            {
                cout<<x<<"*"<<y<<"="<<setw(2)<<x*y<<" ";
            }
            cout <<endl;
        }
    }
    break;
    case 2: //方案二就是讓x先由1(左)往9(右)跑,再讓y由1(上)往9(下)跑,依序印出
    {
        for (int y=1; y<10; y++)
        {
            for (int x=1; x<10; x++)
            {
                cout <<x<<"*"<<y<<"="<<setw(2)<<x*y<<" ";
            }
            cout <<endl;
        }
    }
    break;
    default:
        cout <<"Error!";
    }
    return 0;
}
