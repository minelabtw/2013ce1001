#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a = 0;

    cin >> a;
    switch(a)                                                           //判斷輸入的數值
    {
    case 1:                                                             //若為1，則輸出第一種九九乘法表
        for (int x=1;x<=9;x++)
        {
            for (int y=1;y<=9;y++)
            {
                cout << x << "*" << y << "=" << setw(2) << x*y << " ";
            }
            cout << endl;
        }
        break;

    case 2:                                                             //若為2，則輸出第二種九九乘法表
        for (int x=1;x<=9;x++)
        {
            for (int y=1;y<=9;y++)
            {
                cout << y << "*" << x << "=" << setw(2) << y*x << " ";
            }
            cout << endl;
        }
        break;

    default:                                                            //其他數則輸出錯誤
        cout << "Error!" << endl;
        break;
    }

    return 0;
}
