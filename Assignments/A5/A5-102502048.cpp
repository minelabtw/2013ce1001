#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int i=1,j=1,z=0;//宣告變數
    cin>>z;//輸入數值
    switch(z)//判斷z值
    {
    case 1://如果z=1
    {
        for(i=1; i<=9; i++)//用來輸出乘法表
        {
            for(j=1; j<=9; j++)//用來輸出乘法表
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";//輸出字串
            }
            cout<<endl;//換行
        }
        break;//break
    }
    case 2://如果z=2
    {
        for(j=1; j<=9; j++)//用來輸出乘法表
        {
            for(i=1; i<=9; i++)//用來輸出乘法表
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";//輸出字串
            }
            cout<<endl;//換行
        }
        break;//break
    }
    default://其餘的
        cout<<"Error!";//顯示字串
    }
    return 0;
}
