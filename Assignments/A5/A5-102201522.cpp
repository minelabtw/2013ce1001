#include<iostream>
#include<iomanip>  //Header providing parametric manipulators, ex: setw
using namespace std;

int main()
{
    int type,i,j;
    cin >> type; //輸入type值
    switch(type) //用switch判斷type,決定輸出九九乘法表的形式,或者輸出"Error!"
    {
    case 1: //如果輸入的type是1
        for(i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++) //每次乘數加一
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " "; //同一行被乘數不變
            }
            cout << endl; //換行
        }
        break;
    case 2: //如果輸入的type是2
        for(i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++) //每次被乘數加一
            {
                cout << j << "*" << i << "=" << setw(2) << i*j << " "; //同一行乘數不變
            }
            cout << endl; //換行
        }
        break;
    default:
        cout << "Error!";
        break;
    }
    return 0;
}
