#include<iostream>
#include<cstdlib>
#include<iomanip>
using namespace std;

int main()
{
    int a,i,j;
    char b;

    cin >> b;    //輸入數值
    switch (b)
    {
    case '1':    //當輸入為一時所作之輸出
        for(i=1; i<=9; i++)    //輸出橫向之99乘法表
        {
            for(j=1; j<=9; j++)
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";
            cout << endl;
        }
        break;
    case '2':    //當輸入為二時所作之輸出
        for(i=1; i<=9; i++)    //輸出直向之99乘法表
        {
            for(j=1; j<=9; j++)
                cout << j << "*" << i << "=" << setw(2) << i*j << " ";
            cout << endl;
        }
        break;
    default:    //出入其他東西時,顯示Error!
        cout << "Error!"<<endl;
    }

    return 0;
}
