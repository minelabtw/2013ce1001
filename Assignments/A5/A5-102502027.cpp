#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int number; //宣告number為整數
    cin>>number; //輸入number
    switch(number) //switch statement nested in while
    {
    case 1:// number是1
        for(int i=1; i<=9; i++) //宣告整數i的初始值為1，在i小於等於9的情況下，i持續加1，直到跳出回圈
        {
            for(int j=1; j<=9; j++) //宣告整數j的初始值為1，在j小於等於9的情況下，j持續加1，直到跳出回圈
                    cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";//輸出i*j=
                    cout<<endl;//跳下一行
        }
        break; //離開switch

    case 2:

        for(int j=1; j<=9; j++)  //宣告整數j的初始值為1，在j小於等於9的情況下，j持續加1，直到跳出回圈
        {
            for(int i=1; i<=9; i++)//宣告整數i的初始值為1，在i小於等於9的情況下，i持續加1，直到跳出回圈

                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";
                cout<<endl;
        }
        break;

    default: //捕捉其他的輸入值
        cout<<"Error!";
        break;
    }



    return 0;
}
