#include <iostream>
#include <iomanip>
using namespace std ;
int main()
{
    int a ;
    cin >> a ;      //輸入一整數
    switch(a)       //switch條件判斷
    {

    case 1 :      //方案一
        int i ;
        int j ;
        for(i=1; i<=9; ++i)       //i=1跑到i=9為止
        {
            for(j=1; j<=9; ++j)     //j=1跑到j=9為止
            {
                cout <<i<<"*"<<j<<"="<<std::setw(2)<<i*j<<"\t" ;
            }
            cout <<endl ;
        }
        break ;             //跑完case 1跳出for迴圈
    case 2:               //方案二
        int k ;
        int h ;
        for(k=1; k<=9; k++)       //k=1跑到k=9為止
        {
            for(h=1; h<=9; h++)          //h=1跑到h=9為止
            {
                cout <<h<<"*"<<k<<"="<<std::setw(2)<<h*k<<"\t" ;
            }
            cout <<endl ;
        }

        break ;                 //跑完case 2跳出for迴圈
    default:          //其餘的狀況
        cout <<"Error!" ;
        break ;         //跑完default跳出for迴圈
    }

    return 0 ;
}
