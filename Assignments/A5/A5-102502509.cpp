#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a;
    int x;
    int y;

    cin >> a; // input an number or character to sense.

    switch (a) // sense the number.
    {
    case 1: // the fifrst case.
    {
        for (x = 1; x <= 9; x++) // the x limit.
        {
            for (y = 1; y <= 9; y++) // the y limit.
                cout << x << "*" << y << "=" << setw(2) << x*y << " "; // output the chart.
            cout << endl; // to amke it clear.
        }
    }
    break; // exit the switch.
    case 2:

    {
        for (x = 1; x <= 9; x++)
        {
            for (y = 1; y <= 9; y++)
                cout << y << "*" << x << "=" << setw(2) << x*y << " "; // the other case.
            cout << endl;
        }
    }
    break;

    default: // the case of wrong answer.
        cout << "\aError!\n";
        break;
    }

    return 0;
}
