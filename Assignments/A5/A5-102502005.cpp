#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int c,i,j;                          //宣告三個變數
    cin >> c;                           //要求使用者輸入乘法表代號來決定要輸出哪種乘法表

    switch (c)                          //用switch來判斷要輸出哪種乘法表
    {
    case 1:                             //若c值為1,則輸出第一種乘法表
        for (i=1; i<=9; i=i+1)          //印出第一種乘法表
        {
            for (j=1; j<=9; j=j+1)
            {
                cout <<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";
            }
            cout <<endl;
        }
        break;

    case 2:                             //若c值為2,則輸出第二種乘法表
        for (i=1; i<=9; i=i+1)          //印出第二種乘法表
        {
            for (j=1; j<=9; j=j+1)
            {
                cout <<j<<"*"<<i<<"="<<setw(2)<<i*j<<" ";
            }
            cout <<endl;
        }
        break;

    default:                           //若為其他情況,則輸出error
        cout << "Error!";
        break;
    }
    return 0;
}
