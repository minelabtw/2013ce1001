#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int product=0;
    int A=0;

    cin >> A;

    switch(A)                                        //Use the "switch" selection statement to divide two parts of multiplication tables.
    {
    case 1:
    {
        for(int x=1; x<=9; x++)                      //One of the multiplication table.
        {
            for(int y=1; y<=9; y++)
            {
                product=x*y;
                cout << x << "*" << y << "=" << setw(2) << product << " ";
            }
            cout << endl;
        }
        break;
    }

    case 2:
    {
        for(int x=1; x<=9; x++)                     //The other of the multiplication table.
        {
            for(int y=1; y<=9; y++)
            {
                product=x*y;
                cout << y << "*" << x << "=" << setw(2) << product << " ";
            }
            cout << endl;
        }
        break;
    }

    default:
        cout << "Error!\n";
    }

    return 0;
}






















