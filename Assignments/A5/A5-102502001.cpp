#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int num1;  //宣告一整數變數
    int x=1;   //宣告整數變數,其初始值為0

    cin>>num1;   //輸入num1
    switch(num1) //若輸入值為1或2,顯示出九九乘法表;若否,則顯示"Error!"
    {
    case 1:
        for(int y=1; y<=9; y++)  //用for迴圈,由左到右為1*1= 1 1*2=2...
        {
            for(int x=1; x<=9; x++)
            {
                cout<<y<<"*"<<x<<"="<<setw(2)<<x*y<<" "; //靠右對齊,若十位數為0則留白
            }
            cout<<endl;
            x=1;
        }
        break;   //中斷switch
    case 2:
        for(int y=1; y<=9; y++) //用for迴圈,由左至右為1*1=1 2*1=2...
        {
            for(int x=1; x<=9; x++)
            {
                cout<<x<<"*"<<y<<"="<<setw(2)<<x*y<<" ";  //靠右對齊,若十位數為0則留白
            }
            cout<<endl;
            x=1;
        }
        break;           //中斷switch
    default:             //若條件未符合以上數值及執行此
        cout<<"Error!";  //顯示"Error!"
    }
    return 0;
}
