#include <iostream>
#define CHECK_TEXT_NUM (int)(type[0] - 48)&!((type[0]-48)&(type[0]-49))*3       //get num of input by ascii code
#define CHECK_TEXT_LENGTH !(sizeof(type)/sizeof(char) - 2)*3                    //check input length == 1
using namespace std;

/*-----------------init area 1-------------------*/
void dummyAlpha(int,int){};
void dummyBeta(int,int,int){};
//dummy for break recorsion
void recursion_LayerAlpha(int,int);
void recursion_LayerBeta(int,int,int);
//dual layer recursion,for echo text
void echoTypeA(int,int);
void echoTypeB(int,int);
string equa[] = {"=","= "};
/*-----------------init area 1-------------------*/

/*-----------------init area 2-------------------*/

/*
    FUNCTION ARRAY
    --replace "switch"

    ┌-void textEchor_TypeA(void)
    |    called when input error,echo "Error!"
    |
    └-┬void textEchor_TypeB(void)
      |     called when input "1"
      |
      └void textEchor_TypeC(void)
            called when input "2"
*/
void textEchor_TypeA(){
    cout << "Error!\n";
    return;
}
void textEchor_TypeB(){
    recursion_LayerAlpha(1,0);
}
void textEchor_TypeC(){
    recursion_LayerAlpha(1,1);
}

/*
    DUAL LAYER RECURSION
    --working as dual "for loop"
*/
void recursion_LayerAlpha(int x,int type){
    void (*arr[2])(int,int) = {dummyAlpha,recursion_LayerAlpha};    //function array
    recursion_LayerBeta(x,1,type);
    cout << "\n";
    (*arr[-1*((x-9) >> (sizeof(int)))])(x+1,type);                  //call x<9? self():dummy()
}
void recursion_LayerBeta(int x,int y,int type){
    void (*arr[2])(int,int,int) = {dummyBeta,recursion_LayerBeta};  //function array
    void (*arrEcho[2])(int,int) = {echoTypeA,echoTypeB};            //same as ↑
    (*arrEcho[type])(x,y);                                          //call echoTypeA() or echoTypeB() by value of "type"
    (*arr[-1*((y-9) >> (sizeof(int)))])(x,y+1,type);                //call y<9? self:dummy
}

void echoTypeA(int x,int y){
    cout << x << '*' << y << equa[-1*((x*y-10) >> (sizeof(long long int)))] << x*y << ' ';
    //"1 >> (sizeof(int))" means transfer 10001(bin) into 00001(bin)
    //-1 = negative,0 = positive(include 0).
}
void echoTypeB(int x,int y){
    cout << y << '*' << x << equa[-1*((x*y-10) >> (sizeof(long long int)))] << x*y << ' ';
}
/*-----------------init area 2-------------------*/

int main(void){
    char type[2];   //restrict type in 2 char
    void (*arr[3])(void) = {textEchor_TypeA,textEchor_TypeB,textEchor_TypeC};   //function array,to call "FUNCTION ARRAY" in "init area 2"
    cin >> type;
    int check = (CHECK_TEXT_NUM)&CHECK_TEXT_LENGTH; //var check code by type
    (*arr[check])();   //call function array
    return 0;
}
