#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    char number;        //宣告型別為char的數number

    cin>>number;        //輸入number

    switch(number)
    {
    case '1':           //當輸入1時，執行第一種九九乘法表

        for (int a=1; a<=9; a++)
        {
            for(int b=1; b<=9; b++)
            {
                cout<<a<<"*"<<b<<"="<< setw(2) <<a*b<<" ";
            }
            cout<<endl;
        }
        break;

    case '2':           //當輸入2時，執行第二種九九乘法表
        for (int a=1; a<=9; a++)
        {
            for(int b=1; b<=9; b++)
            {
                cout<<b<<"*"<<a<<"="<< setw(2) <<a*b<<" ";
            }
            cout<<endl;
        }
        break;

    default:            //當輸入1與2以外的字元時，顯示Error!
        cout<<"Error!";
        break;
    }

    return 0;
}
