#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int i=0;                                                            //宣告整數變數i，令其初始值為0。
    i=cin.get();                                                        //把鍵盤輸入的值丟給i。

    switch(i)                                                           //用i的值來做判定
    {
    case '1':                                                           //輸入1的話，執行以下動作。
        for(int x=1; x<=9; x++)                                         //宣告整數變數x，並做10次迴圈。
        {
            for(int y=1; y<=9; y++)                                     //宣告整數變數y，並做10次迴圈。
            {
                cout<< x << "*" << y << "=" << setw(2) << x*y << " ";   //輸出字串，並令x,y的積靠右邊對齊。
            }
            cout << endl;                                               //換行
        }
        break;                                                          //離開switch

    case '2':                                                           //輸入2的話，執行以下動作。
        for(int a=1; a<=9; a++)                                         //宣告整數變數a，並做10次迴圈。
        {
            for(int b=1; b<=9; b++)                                     //宣告整數變數b，並做10次迴圈。
            {
                cout << b << "*" << a << "=" << setw(2) << b*a << " ";  //輸出字串，並令b,a的積靠右邊對齊。
            }
            cout << endl;
        }
        break;

    default:                                                            //輸入其他字串的話，執行以下動作。
        cout<<"Error!";                                                 //輸出字串
        break;
    }

    return 0;
}
