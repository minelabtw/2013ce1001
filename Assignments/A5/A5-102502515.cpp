#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int a ;                                                                 //宣告整數變數
    cin >> a ;                                                              //將輸入的值給a

    switch ( a )                                                            //用switch判斷應執行哪個動作
    {
    case 1:                                                                 //在a=1的情況下，執行此動作
        for (int j=1 ; j <=9 ; j++)                                         //for迴圈製造不同行的式子
        {
            for (int i=1 ; i <=9 ; i++)                                     //for迴圈製造同行的式子
            {
                cout << j << "*" << i << "=" << setw(2) << i*j << " ";      //顯示出式子和利用setw向右靠齊
            }
            cout << endl ;
        }
        break;                                                              //跳出switch

    case 2:                                                                 //利用switch判斷應執行何者動作
        for (int j=1 ; j <=9 ; j++)                                         //利用for製造不同行的式子
        {
            for (int i=1 ; i <=9 ; i++)                                     //利用for製造同行式子
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";      //顯示出式子，並利用setw向右靠齊
            }
            cout << endl ;
        }
        break;                                                              //跳出switch

    default:                                                                //以上都不符合時，執行此
    {
        cout << "Error!" ;
    }
    }

    return 0;
}
