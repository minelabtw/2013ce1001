#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    switch(cin.get())//取輸入的第一個字元的ASCII碼
    {
    case '1'://看是否為'1'之ASCII碼
        for(int i=1; i<=9; i++)//九九乘法表模式1:第一行為1*1,1*2,...,1*9
        {
            for(int j=1; j<=9; j++)
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";
            }
            cout << endl;
        }
        break;//跳出switch
    case '2'://看是否為'2'之ASCII碼
        for(int i=1; i<=9; i++)//九九乘法表模式2:第一行為1*1,2*1,...,9*1
        {
            for(int j=1; j<=9; j++)
            {
                cout << j << "*" << i << "=" << setw(2) << i*j << " ";
            }
            cout << endl;
        }
        break;//跳出switch
    default://除了'1'或'2'以外之字元
        cout << "Error!\n";
        break;//跳出switch
    }
    return 0;
}
