#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a;
    int b;                                                       //宣告變數


    cin>>a;
    switch(a)                                                    //條件判斷該輸出哪一項
    {
    case 1:
        for(a=1; a<=9; a++)                                      //迴圈
        {
            for(b=1; b<=9; b++)
            {
                cout<<a<<"*"<<b<<"="<<setw(2)<<a*b<<" ";         //setw改變數字輸出的位置
            }
            cout<<endl;
            b=1;
        }
        break;
    case 2:
        for(a=1; a<=9; a++)
        {
            for(b=1; b<=9; b++)
            {
                cout<<b<<"*"<<a<<"="<<setw(2)<<a*b<<" ";
            }
            cout<<endl;
            b=1;
        }
        break;
    default:                                                     //不符合上述情況則輸出
        cout<<"Error!";
        break;
    }

    return 0;
}
