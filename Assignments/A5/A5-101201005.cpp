#include<sstream>
#include<iostream>
#include<iomanip>
using namespace std;

main()
{
    int a;
    cin >> a;
    switch(a)
    {
        int i,j;
    case 1: //a為1時輸出
        for (i=1; i<=9; ++i)
        {
            for (j=1; j<=9; ++j)
            {
                cout << i << "*" << j << "=" << std::setw(2) << i*j << " "<< "\t" ;
            }
            cout << endl ;
        }
        break; //若case1成立後跳脫switch
    case 2: //a為2時輸出
        for (i=1; i<=9; ++i)
        {
            for (j=1; j<=9; ++j)
            {
                cout << j << "*" << i << "=" << std::setw(2) << i*j << " " << "\t" ;
            }
            cout << endl ;
        }
        break; //若case2成立後跳脫case2
    default: //其他情況Error!
        cout << "Error!" ;
        break;
    }
    return 0;
}
