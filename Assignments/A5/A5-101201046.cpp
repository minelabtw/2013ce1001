#include <iostream>
#include <iomanip>

using namespace std;

/* construct a function for 9 x 9 table*/ 
int f(char ch) {

	for (int i = 1; i <= 9; i++, cout << "\n")
		for (int j = 1; j <= 9; j++)
			((ch == '1') && 
				(cout << i << "*" << j << "=" << setw(2) << i*j << " ")) ||
			((ch == '2') &&
				(cout << j << "*" << i << "=" << setw(2) << i*j << " "));
	
	return 1;
	}


int main(void) {
	char ch;

	ch = cin.get(); // input
	
	/* check input*/
	(((ch == '1') || (ch == '2')) && 
		f(ch)) ||
	(cout << "Error!\n");

	return 0;
	}
