#include<iostream>
#include<iomanip>
#define MAX 9
using namespace std;
int main()
{
    int mode;
    cin>>mode; // input
    switch( mode )
    {
    case 1: // mode 1
        for(int i=1; i<=MAX; i++)
        {
            for(int j=1; j<=MAX; j++)
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" "; // print 9*9
            }
            cout<<endl; // finish a line, change line
        }
        break;
    case 2: // mode 2
        for(int i=1; i<=MAX; i++)
        {
            for(int j=1; j<=MAX; j++)
            {
                cout<<j<<"*"<<i<<"="<<setw(2)<<i*j<<" "; // print 9*9
            }
            cout<<endl; // finish a line, change line
        }
        break;
    default:
        cout<<"Error!"<<endl;
        break;
    }
    return 0;
}
