#include<iostream>
#include<iomanip>  //以使用setw型別

using namespace std;

int main()
{
    int kind=0;
    int i=0,j=0;

    cin >> kind;
    switch (kind)  //用switch做多重選擇
    {
    case 1:  //選擇第一種排列時的作法
    {
        for (i=1;i<10;i++)
        {
            for (j=1;j<10;j++)
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";  //加入setw以對齊
            }
            cout << endl;
        }
    }
        break;  //跳出switch控制

    case 2:  //選擇第二種排列時的作法
    {
        for (i=1;i<10;i++)
        {
            for (j=1;j<10;j++)
            {
                cout << j << "*" << i << "=" << setw(2) << j*i << " ";
            }
            cout << endl;
        }
    }
        break;

    default:  //除輸入1和2之外，其餘的輸入均無效
            cout << "Error!" << endl;
        break;
    }

    return 0;
}
