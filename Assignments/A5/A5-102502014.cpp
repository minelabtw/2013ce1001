#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int a;                           //宣告變數
    int i;
    int j;
    cin>>a;
    switch(a)                        //用switch判斷a
    {
    case 1:                          //當a輸入1 便進入第一種for迴圈
        for(i=1; i<10; i++)
        {
            for(j=1; j<10; j++)
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";  //setw(2)使等號後方佔兩格輸出
            }
            cout<<endl;
        }
        break;
    case 2:                          //當a輸入2 便進入第二種for迴圈
        for(i=1; i<10; i++)
        {
            for(j=1; j<10; j++)
            {
                cout<<j<<"*"<<i<<"="<<setw(2)<<i*j<<" ";
            }
            cout<<endl;
        }
        break;
    default:                         //當a輸入的值不等於1也不等於2
        cout<<"Error!";              //列印"error!"
        break;
    }
    return 0;
}
