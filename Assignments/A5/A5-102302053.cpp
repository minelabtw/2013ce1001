//A5-102302053.cpp
#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

int main()
{
    char t;//use to decide what type of multiplication table to be output.
    int a;//use to create a multiplication table.
    int b;//use to create a multiplication table.

    cin >> t;//prompt user to give information for the output.

    switch (t)
    {
        case '1'://if user enter 1 , then output a horizontal multiplication table .
            for (a = 1;a <= 9;a++)
            {
                for(b = 1;b <= 9;b++)
                {
                    cout << a << "*" << b << "=" << setw (2) << a * b << " ";
                }
                cout << "\n";
            }
            cout << endl;
            break;
        case '2'://if user enter 2, then output a vertical multiplication table.
           for (a = 1;a <= 9;a++)
            {
                for(b = 1;b <= 9;b++)
                {
                    cout << b << "*" << a << "=" << setw (2) << a * b << " ";
                }
                cout << "\n";
            }
            cout << endl;
            break;
        default://if user enter other words or number, then show Error! to user.
            cout <<"Error!\n" << endl;

     }

     system ("pause");

     return 0;

}
