#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int input=0;                    //宣告型別為 整數(int) 的變數
    cin >> input;
    switch(input)                   //判斷變數值並輸出對應結果
    {
    case 1:
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
            {
                cout << i << "*" << j << "=" << setw(2) << (i*j) << " ";
            }
            cout << endl;
        }
        break;
    case 2:
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
            {
                cout << j << "*" << i << "=" << setw(2) << (i*j) << " ";
            }
            cout << endl;
        }
        break;
    default :
        cout << "Error!";
        break;
    }
    return 0;
}
