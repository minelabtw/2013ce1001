#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int a = 0; //宣告變數a其初始值為0
    int x = 0; //宣告變數x其初始值為0
    int y = 0; //宣告變數y其初始值為0

    cin >> a; //輸入a值

    switch( a ) //輸入a值選擇case1或case2
    {
    case 1 :

        for ( x = 1; x <= 9; x++ )
        {
            for ( y = 1; y <= 9; y++ )
            {
                cout << x << "*" << y << "=" << setw(2) << x*y << " ";
            }
            cout << endl;
        }
        break; //跳出swich
    case 2 :
        for ( x = 1; x <= 9; x++ )
        {
            for ( y = 1; y <= 9; y++ )
            {
                cout << y << "*" << x << "=" << setw(2) << x*y << " ";
            }
            cout << endl;
        }
        break;

        default : //其他
        cout << "Error!" << endl; //顯示Error!
        break;
    }
    return 0;
}
