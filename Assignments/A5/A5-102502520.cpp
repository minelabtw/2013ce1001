#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int sum;                                                                    //定義一整數為sum
    int number;                                                                 //定義一整數為number

    number = cin.get();                                                         //將輸入之數，帶入switch


    switch (number)
    {
    case'1':                                                                    //若輸入之數為1，使用以下for迴圈
        for (int y=1 ; y<=9 ; y++)                                              //九九乘法表
        {
            for(int x=1 ; x<=9 ; x++)
            {
                sum=x*y;
                cout<<y<<"*"<<x<<"="<<setw(2)<<sum<<"  ";
            }
            cout<<endl;
        }
        break;                                                                  //結束switch

    case'2':                                                                    //若輸入之數為2，使用以下for迴圈
        for (int y=1 ; y<=9 ; y++)                                              //九九乘法表
        {
            for(int x=1 ; x<=9 ; x++)
            {
                sum=x*y;
                cout<<x<<"*"<<y<<"="<<setw(2)<<sum<<"  ";
            }
            cout<<endl;
        }
        break;                                                                 //結束switch

    default:                                                                   //若不為1也不為2
        cout<<"Error!";                                                        //輸出Error!
        break;                                                                 //結束switch
    }




    return 0;
}
