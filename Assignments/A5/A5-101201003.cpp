#include<iomanip>
#include<iostream>
using namespace std;

int main()
{
    int input=0;                                                            //定義型態為int的input變數，並使其初始值為0
    cin >> input;                                                           //輸入input
    switch(input)                                                           //判斷
    {
        case 1:                                                             //如果輸入1，就執行下列結果
            for(int i=1;i<=9;i++)                                           //設定i初始值為1且小於等於9，完成迴圈後i每次要加一
            {
                for(int j=1;j<=9;j++)                                       //設定j初始值為1且小於等於9，完成迴圈後i每次要加一
                    cout << i << "*" << j << "=" << setw(2) << i*j << " ";  //輸出乘法表，輸出的數值要2字元寬度
                    cout << endl;                                           //換行
            }
        break;                                                              //跳出switch
        case 2:                                                             //如果輸入2，就執行下列結果
            for(int j=1;j<=9;j++)
            {
                for(int i=1;i<=9;i++)
                    cout << i << "*" << j << "=" << setw(2) << i*j << " ";
                    cout << endl;
            }                                                               //如case1，只是跑迴圈的順序不同
        break;                                                              //跳出switch
        default:                                                            //如果輸入1、2以外的字元或數字，就執行下列結果
            cout << "Error!";                                               //輸出"Error!"
    }
    return 0;
}
