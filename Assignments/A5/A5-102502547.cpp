#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    char a;
    cin >> a;
    switch(a) //switch-case
    {
    case '1': //a為1
        for(int i=1; i<10; i++) //迴圈，宣告整數i=1，i累加，執行9次
        {
            for(int j=1; j<10; j++) //迴圈，宣告整數j=1，j累加，執行9次
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " "; //輸出
            }
            cout<<endl;
        }
        break; //脫離switch-case

    case '2': //a為2
        for(int i=1; i<10; i++) //迴圈，宣告整數i=1，i累加，執行9次
        {
            for(int j=1; j<10; j++) //迴圈，宣告整數j=1，j累加，執行9次
            {
                cout << j << "*" << i << "=" << setw(2) << i*j << " "; //輸出
            }
            cout<<endl;
        }
        break; //脫離switch-case

    default: //a為其他
        cout << "Error!";
        break; //脫離switch-case
    }
    return 0;
}
