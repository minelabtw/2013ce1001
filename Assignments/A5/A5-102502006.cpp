#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int num =0; // 輸入的數字

    cin >> num; // 輸入規格

    switch(num)
    {

        // 印出第一種版本
    case 1:
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";
            }
            cout << endl;
        }
        break;

        // 印出第二種版本
    case 2:
        for(int i=1; i<=9; i++)
        {
            for(int j=1; j<=9; j++)
            {
                cout << j << "*" << i << "=" << setw(2) << i*j << " ";
            }
            cout << endl;
        }
        break;

        // 不符合的輸入
    default:
        cout << "Error!\n";
        break;
    }

    return 0;

}
