//A5-102502026
//9*9

#include<iostream>
#include<iomanip>
using namespace std;

int main()  //start program
{
    int a;  //define a
    int n1=0;   //define n1
    int n2=0;   //define n2
    int R=0;    //define result

    a=cin.get();    //ask a
    switch(a)       //with a
    {
    case '1':       //if a is 1
        for(n1=1; n1<=9; n1++)  //n1 is 1 and needs to go 9
        {
            for (n2=1; n2<=9; n2++) //n2 is 1 and needs to go 9
            {
                R=n1*n2;    //formula
                cout<<n1<<"*"<<n2<<"="<<setw(2)<<R<<setw(2);    //print
            }
            cout<<"\n"; //jump
            n2=1;   //return to 1 for the second for
        };   //end for first for
        break;  //end switch

    case'2':    //if a is 2
        for(n2=1; n2<=9; n2++)  //n2 is 1 and needs to go 9
        {
            for (n1=1; n1<=9; n1++) //n1 is 1 and needs to go 9
            {
                R=n1*n2;    //formula
                cout<<n1<<"*"<<n2<<"="<<setw(2)<<R<<setw(2);    //print
            }   //end for second for
            cout<<"\n"; //jump
            n1=1;   //return to 1 for the second for
        };  //end for first for
        break;//end switch

    default:    //if is another number or letter
        cout<<"Error!"<<"\n";   //print error
        break;  //end switch
    }   //end switch
    return 0;
}   //end program
