#include <iostream>
#include <iomanip>


using namespace std;


int main()
{
    int n=0;


    cin >> n;
    switch(n)                                              //歸類輸入不同n，輸出的種類
    {
        case 1:                                            //當n為1時
            for(int x=1 ;x<=9 ;x++ )                       //初始化x為1，每輸出完一回，x加1直到x等於9
            {
                for(int y=1; y<=9 ;y++ )
                {
                    cout << x << "*" << y << "=" << setw( 2 ) << x*y << " ";            //setw( 2 )為後面的輸出，在"="後第二格輸出
                }
                cout << endl;
            }
        break;                                                                          //跳離switch


        case 2:
            for(int y=1; y<=9 ;y++ )
            {
                for(int x=1 ;x<=9 ;x++ )
                {
                    cout << x << "*" << y << "=" << setw( 2 ) << x*y << " ";
                }
                cout << endl;

            }
            break;


        default:                                                  //當n不為1 or 2時
            cout << "Error!";
            break;
    }


    return 0;
}
