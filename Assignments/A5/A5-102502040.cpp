#include<iostream>
#include<iomanip>
using namespace std;

int main(void)
{
    int i=0;//設定輸入字元
    cin >> i;
    switch(i)//利用switch判斷輸入字元
    {
    case 1://若輸入1,則執行case1以下指令
        for(int x=1; x<=9; x++)//設定輸入1時,排版與數字增長
        {
            for(int y=1; y<=9; y++)
                cout << x << "*" << y << "= " << setw(2) << x*y << " ";//顯示乘法表,與設定字元位置
            cout << endl;
        }
        break;//指令結束
    case 2://若輸入2,則執行case2以下指令
        for(int y=1; y<=9; y++)
        {
            for(int x=1; x<=9; x++)//輸入2時,輸出為輸入1的另一種排版
                cout << x << "*" << y << "= " << setw(2) << x*y << " ";//顯示乘法表,與設定字元位置
            cout << endl;
        }
        break;
    default:
        cout << "Error!" <<endl;//顯示若輸入1.2以外的字,則輸出Error!
        break;
    }
    return 0;
}
