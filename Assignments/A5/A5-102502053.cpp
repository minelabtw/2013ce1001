#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int type; //call variable
    cin>>type; //input the type to be display

    //determine the type to be display
    switch(type)
    {
    case 1: //for choosing type one
        for(int x=1; x<=9; x++) //the first factor will be the same at the same line but +1 at the nest line
        {
            for(int y=1; y<=9; y++) //the 2nd factor will +1 every time as moving to next column
            {
                cout<<x<<"*"<<y<<"="<<setw(2)<<x*y<<" "; //calculate & display the output and set the product to be display in a field width of 2
            }
            cout<<endl;
        }
        break;

    case 2://for choosing type 2
        for(int y=1; y<=9; y++) //the first factor will be the same at the same line but +1 at the nest line
        {
            for(int x=1; x<=9; x++) //the 2nd factor will +1 every time as moving to next column
            {
                cout<<x<<"*"<<y<<"="<<setw(2)<<x*y<<" "; //calculate & display the output and set the product to be display in a field width of 2
            }
            cout<<endl;
        }
        break;

    default:
        cout<<"Error!"<<endl; //catch all other input
        break;
    }

    return 0;
}
