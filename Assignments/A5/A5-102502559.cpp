#include<iostream>
#include <iomanip>
using namespace std;

int main()
{
    int a,b=1,c=1; //宣告三變數
    cin>>a; //輸入直配給a
    switch(a) //以a的數值作條件判斷
    {
    case 1: //假如a的值為1
        for(b=1; b<=10; b++) //設b初始值為1,每次迴圈+1,超過10時停止迴圈
        {
            for(c=1; c<=10; c++)//設c初始值為1,每次迴圈+1,超過10時停止迴圈
            {
                cout<<b<<"*"<<c<<"="<<setw(2)<<b*c<<" "; //印出算式

            }
            cout<<endl;//換行
        }
        break; //停止,跳出條件判斷
    case 2://假如a的值為2
        for(b=1; b<=10; b++)//設b初始值為1,每次迴圈+1,超過10時停止迴圈
        {
            for(c=1; c<=10; c++)//設c初始值為1,每次迴圈+1,超過10時停止迴圈
            {
                cout<<c<<"*"<<b<<"="<<setw(2)<<b*c<<" "; //印出算式

            }
            cout<<endl;//換行
        }
        break;//停止,跳出條件判斷
    default: //其他狀況時
        cout<<"Error!"; //顯示"錯誤"
        break;//停止,跳出條件判斷
    }

}
