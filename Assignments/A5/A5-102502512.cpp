#include<iostream>
#include<iomanip>                                                 //To read iomanip file
using namespace std;
int main()
{
    int n;
    int x=9,y=9;
    cin>>n;
    switch (n)                                                     //Line 9~35: To deside that when user enter 1 or 2 or other things
    {                                                              // what will present to user.
    case 1:
        for(int i=1; i<=x; i++)                                    //Line 12~20: print out the 99 Multiplication table
        {
            for(int j=1; j<=y; j++)
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";
            }
            cout<<endl;
        }
        break;
    case 2:                                                        //Line 22~30: To print another 99 Multiplication table
        for(int i=1; i<=x; i++)
        {
            for(int j=1; j<=y; j++)
            {
                cout<<j<<"*"<<i<<"="<<setw(2)<<i*j<<" ";
            }
            cout<<endl;
        }
        break;
    default:                                                       //When user intput niether 1 nor 2 then print Error!
        cout<<"Error!";
        break;

    }
    return 0;
}
