#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int style=0;
    int a,b;

    cin >>style; //輸入型態

    switch(style)
    {
    case 1: //輸入1的時候執行下列for迴圈
        for(a=1; a<=9; a++) //先固定"被乘數"
        {
            for(b=1; b<=9; b++) //使已固定的"被乘數"，乘上"乘數(1~9)"，並一一顯示
            {
                cout <<a<<"x"<<b<<"="<<setw(2)<<a*b<<" ";
            }
            cout <<endl; //換行
        }
        break;

    case 2: //輸入2的時候執行下列for迴圈
        for(b=1; b<=9; b++) //先固定"乘數"
        {
            for(a=1; a<=9; a++) //使已固定的"乘數"，乘上"被乘數(1~9)"，並一一顯示
            {
                cout <<a<<"x"<<b<<"="<<setw(2)<<a*b<<" ";
            }
            cout <<endl; //換行
        }
        break;

    default: //輸入非1或2時，出現錯誤訊息
        cout <<"Error!";
        break;
    }
    return 0;
}
