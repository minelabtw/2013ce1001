#include<iostream>
#include<iomanip>

using namespace std;

int main()
{
    int a,b;
    int i = 1,j = 1;                          //初始化i和j的數值為1
    cin >> a;                                 //輸入a
    switch (a)
    {
    case 1:                                   //當a的數值=1時
        for (i=1; i<=9; i++)                  //當i的範圍在1到9之間
        {
            for (j=1; j<=9; j++)              //並且j的範圍在1到9之間時
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";      //輸出第一類型(i開頭同一橫排)的99乘法表並對齊
            }                                                               //給予積2個空格
            cout << endl;
        }
        break;                                //跳出迴圈

    case 2:                                   //當a的數值=2時
                                              //當j的範圍在1到9之間
        for (j=1; j<=9; j++)
        {
            for (i=1; i<=9; i++)              //並且i的範圍在1到9之間時
            {
                cout << i << "*" << j << "=" << setw(2) << i*j << " ";      //輸出第二類型(i開頭同一直排)的99乘法表並對齊
            }                                                               //給予積2個空格
            cout << endl;
        }
        break;                                //跳出迴圈

    default:                                  //當a為其他數值或符號
        cout << "Error" << endl;              //顯示錯誤
        break;                                //跳出迴圈
    }
    return 0;
}
