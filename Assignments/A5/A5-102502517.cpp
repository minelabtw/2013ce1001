#include <iostream>
#include <iomanip> //使用setw

using namespace std;

int main()
{
    switch (cin.get()) //以switch判斷輸入值
    {
    case '1':
        for (int x = 1; x<=9; x++) //輸出九九乘法表
        {
            for (int y = 1; y<=9; y++)
            {
                cout << x << "*" << y << "=" << setw(2) << x*y << " ";
            }
            cout << endl;
        }
        break;

    case '2':
        for (int y = 1; y<=9; y++) //輸出九九乘法表
        {
            for (int x = 1; x<=9; x++)
            {
                cout << x << "*" << y << "=" << setw(2) << x*y << " ";
            }
            cout << endl;
        }
        break;

    default: //輸入值非1或2則輸出Error
        cout << "Error!";
        break;
    }
    return 0;
}
