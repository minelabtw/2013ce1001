#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::setw;

int main()
{
    int x=1;                 //宣告一個整數，並初始化其值為1
    int y=1;
    int mode;                //宣告一個整數
    cin>>mode;
    switch(mode)
    {
    case 1:
        for(x=1; x<=9; x++)
        {
            for(y=1; y<=9; y++)            //讓y先從1持續+1直到9，換行，從x=2繼續
            {
                cout<<x<<'*'<<y<<'='<<setw(2)<<x*y<<" ";
            }
            cout<<endl;
        }
        break;                                 //結束這個迴圈，沒有的話連case2都會被輸出

    case 2:
        for(y=1; y<=9; y++)
        {
            for(x=1; x<=9; x++)            //讓x先從1持續+1到9，換行，從y=2繼續
            {
                cout<<x<<'*'<<y<<'='<<setw(2)<<x*y<<" ";
            }
            cout<<endl;
        }
    default:
        cout<<"Error!";                    //輸入除了1和2之外的東西就會輸出"default   "
    }
    return 0;
}
