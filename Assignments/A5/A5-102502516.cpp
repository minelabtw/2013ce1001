#include <iostream>
#include<iomanip>
using namespace std;

int main()
{
    char input;
    cin >> input;
    //宣告變數input以接收使用者的指令
    switch (input)
    {
    case '1':
    case '2':
    //當使用者輸入1或2時
        for (int i=1 ; i<=9 ; i++)
        {
            for (int j=1 ; j<=9 ; j++)
            {
                switch (input)
                {
                case '1':
                    cout << i << "×" << j;
                    break;
                case '2':
                    cout << j << "×" << i;
                    break;
                }
                //用switch達到題目要求的兩種九九乘法表
                cout << "=" << setw(2) << i*j << " ";
            }
            cout << endl; //寫完一列要換行
        }
        break;
        //利用雙重迴圈跑出九九乘法表
    default:
        cout << "Error!";
        break;
    //若不是1或2則顯示錯誤並跳出
    }
}
