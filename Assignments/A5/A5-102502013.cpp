#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int model = 0;//宣告一個名稱為model的整數變數，並初始化其值為0
    int x;//宣告一個名稱為x的整數變數
    int y;//宣告一個名稱為y的整數變數
    int sum = 0;//宣告一個名稱為sum的整數變數，並初始化其值為0
    cin >> model;//輸入model
    switch (model)//判斷
    {
    case 1://如果model=1
        for ( x=1; x<=9; x++)//當x<=9時x+1
        {
            for(y=1; y<=9; y++)//當y<=9時y+1
            {
                sum = x * y;//sum=x*y
                cout << x << "*" << y << "=" << setw(2) << sum << " ";//輸出字串x*y=結果向右2格對齊再空1格
            }
            cout << endl;//換行
        }
        break;//跳出迴圈
    case 2://如果model=2
        for ( y=1; y<=9; y++)//當y<=9時y+1
        {
            for(x=1; x<=9; x++)//當x<=9時x+1
            {
                sum = x * y;//sum=x*y
                cout << x << "*" << y << "=" << setw(2) << sum << " ";//輸出字串x*y=結果向右2格對齊再空1格
            }
            cout << endl;//換行
        }
        break;//跳出迴圈
    default://如果皆不是
        cout << "Error";//輸出字串Error
        break;//跳出迴圈
    }
    return 0;
}
