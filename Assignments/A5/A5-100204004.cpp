#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int mode;//宣告變數"模式"
    cin>>mode;//將輸入的數字傳給mode
    switch(mode)
    {
    case 1://模式1
        for(int i=1; i<=9; i++)//模式1排法的99乘法表
        {
            for(int j=1; j<=9; j++)
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";
            }
            cout<<endl;//每輸出一排99乘法表，就跳一行
        }
        break;

    case 2://模式2
        for(int i=1; i<=9; i++)//模式2排法的99乘法表
        {
            for(int j=1; j<=9; j++)
            {
                cout<<j<<"*"<<i<<"="<<setw(2)<<i*j<<" ";
            }
            cout<<endl;//每輸出一排99乘法表，就跳一行
        }
        break;

    default://若輸入非1or2，則輸出Error!
        cout<<"Error!"<<endl;
        break;
    }
    return 0;
}
