#include<iostream>
#include<iomanip>
using namespace std;

int main()
{

    int num=0; // 設定變數

    int i=0;
    int j=0;

    cin>>num; // 輸入num

    switch(num)
    {

    case 1: // 若num=1,則輸出乘法表
        for(i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++)
            {
                cout<<i<<"*"<<j<<"="<<setw(2)<<i*j<<" ";
            }

            cout<<endl;
        }
        break;

    case 2: // 若num=2,則輸出乘法表
        for(i=1; i<=9; i++)
        {
            for(j=1; j<=9; j++)
            {
                cout<<j<<"*"<<i<<"="<<setw(2)<<i*j<<" ";
            }

            cout<<endl;
        }
        break;

    default: // 若num為其他情形,則輸出"Error!"
        cout<<"Error!"<<endl;
        break;

    }

    return 0;

}
