#include <iostream>
#include<iomanip>       //用於setw指令。

using namespace std;

int main()
{
    int k=0;        //宣告變數整數。
    int l=0;
    int m=0;
    int input=0;

    input=cin.get();        //使他只可以輸入與switch相關的數字。

    switch (input)      //條件輸出
    {
    case '1':
        for(k=1; k<=9; k++)     //外圍迴圈
        {
            for(l=1; l<=9; l++)     //內圍迴圈
            {
                m = l * k;      //計算乘法
                cout << setw(2 )<< k << "*" << l << "=" << setw(2) << m << setw(2);     //顯示乘法，setw是整齊排列，（）內數字是佔位
            }

            cout << endl;
            m=0;        //使其還原進行下次迴圈。
            l=0;
        }

        break;

    case '2':
        for(k=1; k<=9; k++)
        {
            for(l=1; l<=9; l++)
            {
                m = l * k;
                cout << setw(2) << l << "*" << k << "=" << setw(2) << m << setw(2);
            }

            cout << endl;
            m=0;
            l=0;
        }

        break;

    default:
        cout << "Error!";       //除了條件內是值外都是設定為錯誤。
    }

    return 0;
}
