/*閏平年判斷
請寫一個程式可以供使用者輸入兩個整數，
還要可以判別使用者輸入的數字大於0，
且第一個整數小於第二個整數，若不滿足則要重新輸入，
最後輸出一開始輸入的兩個整數區間有哪些年是屬於閏年，
hint:用西元年去判斷。

1.西元年份除以400可整除，為閏年。

2.西元年份除以4可整除並且除以100不可整除，為閏年。

3.西元年份除以4不可整除，為平年。

4.西元年份除以100可整除並且除以400不可整除， 為平年。*/
#include<iostream>
using namespace std;

int main()
{
    int year1=0,year2=0;

    while(year1 >= year2)//讓year2一定要大於year1
    {
        while(year1<=0)
        {
            cout << "Please enter first number( >0 ):";
            cin >> year1;
            if (year1<=0)//讓year1>0
                cout << "First number is out of range!!" << endl;
        }
        while(year2<=0)
        {
            cout << "Please enter second number( >0 ):";
            cin >> year2;
            if (year2<=0)//讓year2>0
                cout << "Second number is out of range!!" << endl;
        }
        if (year1 >= year2)
        {
            cout << "The second number is not greater than the first number!!" << endl;
            year1=0,year2=0;
        }
    }

    int x=year1,y=year2;
    cout << "Leap years:";
    while(x <= y)//判斷閏年
    {
        if (x%4==0 && x%100!=0 || x%400==0)//給定閏年的條件
            cout << x << " ";//輸出閏年
        x++;
    }

    return 0;
}
