#include <iostream>

using namespace std;

int main ()
{
    int number_1=0;
    int number_2=0;
    int remainder_1=0;
    int remainder_2=0;
    int remainder_3=0;
    int remainder_4=0; //給定六個變數,使之初始值為0。


    do //先執行一遍
    {
        cout<<"Please enter first number( >0 ):";
        cin >> number_1;  //給輸入第一個數。

        while (number_1<=0) //判別有無超過指定範圍。
        {
            cout<<"First number is out of range!!"<<endl;
            cout<<"Please enter first number( >0 ):"; //超過的話重新輸入其值。
            cin>>number_1;
        }

        cout<<"Please enter second number( >0 ):";
        cin >> number_2;  //給輸入第二個數。
        while (number_2<=0) //判別有無超過指定範圍。
        {
            cout<<"Second number is out of range!!"<<endl;
            cout<<"Please enter second number( >0 ):"; //超過的話重新輸入其值。
            cin>>number_2;
        }
        while (number_2<=number_1&&number_2>0) //判別兩數的的大小,後面要比前面大。
        {
            cout<<"The second number is not greater than the first number!!"<<endl;
            break; //打破此一內迴圈
        }
    }
    while (number_2<=number_1&&number_2>0); //判別兩數是否輸入合理,之後再判別是否要繼續迴圈。


    cout<<"Leap years:"; //輸出結果。
    while(number_2>=number_1)
    {
        remainder_1=number_1%4;    //看number_1是否被4整除,有沒有餘數。
        remainder_2=number_1%100;  //看number_1是否被100整除,有沒有餘數。
        remainder_3=number_1%400;  //看number_1是否被400整除,有沒有餘數。
        remainder_4=number_1%4000; //看number_1是否被4000整除,有沒有餘數。
        if (remainder_1==0&&remainder_2==0&&remainder_3==0&&remainder_4!=0) //判別是否閨年。
            cout<<number_1<<" ";
        else if (remainder_1==0&&remainder_2!=0) //另一種閨年的判別。
            cout<<number_1<<" ";
        number_1++; //一個一個慢慢檢查。
    }
    return 0;
}

