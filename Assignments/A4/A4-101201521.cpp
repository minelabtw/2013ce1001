#include <iostream>
using namespace std;
int main()
{
    int flag=0, year1=0, year2=0, input=0;//宣告變數為整數型態並設定初始值為0。
    while(flag<2)//年份輸入完成則跳出while。
    {
        if (flag==0)//flag=0時，要求輸入year1。
            cout << "Please enter first number( >0 ):";
        else if (flag==1)//flag=1時，要求輸入year2。
            cout << "Please enter second number( >0 ):";
        cin >> input;
        if (flag==0)//使得輸入要求與輸入存取變數對應正確。
            year1=input;
        else
            year2=input;
        if (input > 0 )//輸入皆要大於0，否則發出警告，並要求重新輸入。
            flag++;
        else if (flag==0)
            cout << "First number is out of range!!\n";
        else if (flag==1)
            cout << "Second number is out of range!!\n";
        if (flag==2 && year1 >= year2)//若year1大於或等於year2則發出警告並從year1開始要求重新輸入。
        {
            cout << "The second number is not greater than the first number!!\n";
            flag=0;
        }
    }
    cout << "Leap years:";
    for(int i=year1, check=0; i<=year2; i++) //檢查年份範圍從year1到year2，check用來檢查是否為第一次輸出。
    {
        if(i%4 == 0 &&( i%400 == 0 || i%100 != 0))//檢查是否為潤年。
            if(!check)//若為第一次輸出前面不加空格，否則加空格。
                cout << i, check++;
            else
                cout << " " << i;
    }
    return 0;
}
