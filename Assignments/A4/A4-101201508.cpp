#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int i ;                                                  //幫助判斷現在要進哪一個
    int first ;                                              //存現在前面的數
    int second ;                                             //存現在後面的數
    int j ;                                                  //現在檢查到哪個數
    int T ;                                                  //紀錄判斷到目前為止是對還是錯的
    for (i=0; i<2; ++i)
    {
        if (i==0)
        {
            cout << "Please enter first number( >0 ):" ;
            cin >> first ;
        }
        else if (i==1)
        {
            cout << "Please enter second number( >0 ):" ;
            cin >> second ;
        }
        if (first<=0)
        {
            cout << "First number is out of range!!" << endl ;
            i=-1 ;
        }
        else if (second<=0)
        {
            cout << "Second number is out of range!!" << endl ;
            i=0 ;
        }
        else if (first >second)                  //如果第二個數小於第一個數的話就重新
        {
            cout << "The second number is not greater than the first number!!" << endl ;
            i=-1 ;
        }
    }
    cout << "Leap years:" ;
    for (j=first; j<=second; ++j)
    {
        T=0;                                     //假設一開始是不是閏年
        if (j%400==0)                            //判斷是閏年的話就改
            T=1 ;
        if (j%4==0 && j%100!=0)
            T=1 ;
        if (j%4!=0)
            T=0 ;
        if (j%100==0 && j%400!=0)
            T=0 ;
        if (T==1)                                //如果經過之前的判斷是閏年的話就輸出
            cout  << j << " " ;
    }
    return 0;
}
