#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int year1 = 0, year2 = 0 ; //設定兩個年份並數值化為0

    while(year2 <= year1)
    {
        cout << "Please enter first number( >0 ):" ; //輸入第一年份
        cin >> year1 ;

        while(year1 <= 0 ) //設定要大於零
        {
            cout << "First number is out of range!!" << endl ;
            cout << "Please enter first number( >0 ):" ;
            cin >> year1 ;
        }

        cout << "Please enter second number( >0 ):" ; //輸入第二年份
        cin >> year2 ;

        while(year2 <= 0 )
        {
            cout << "Second number is out of range!!" << endl ;
            cout << "Please enter second number( >0 ):" ;
            cin >> year2 ;
        }
        if(year2 <= year1) //如果第二年小於第一年,顯示錯誤並重新跑入迴圈
        {
            cout << "The second number is not greater than the first number!!" << endl ;
            continue ;
        }
        else //若正確,跳出迴圈
            break ;
    }

    cout << "Leap years:" ;

    int leap = year1 ; //設定閏年從第一年開始

    for(leap==year1; leap<=year2; leap++) //將範圍從第一年到第二年
    {
        if (leap%400==0 )  //被400整除就顯示閏年
            cout << leap << " " ;

        else if (leap%100==0)  //如果被100整除,不輸出直接讓年份增加
            leap++ ;

        else if (leap%4==0) //被4整除就顯示閏年
            cout << leap << " " ;
    }


    return 0 ;
}
