#include<iostream>
using namespace std;
int main()

{
    int year1, year2;                                                   //define the vairble that mean the starting year and ending year
    do                                                                  //Line 7~29: To let user input the year and make sure that the years is matching our requirement
    {

        do
        {
            cout<<"Please enter first number( >0 ):";
            cin>>year1;
            if(year1<=0)
                cout<<"First number is out of range!!\n";
        }
        while(year1<=0);
        do
        {
            cout<<"Please enter second number( >0 ):";
            cin>>year2;
            if(year2<=0)
                cout<<"Second number is out of range!!\n";
        }
        while(year2<=0);
        if(year2<=year1)
        {
            cout<<"The second number is not greater than the first number!!\n";
        }
    }
    while(year2<=year1);
    cout<<"leap years: ";
    for(int i=year1; i<=year2; i++)                                                             //Line 32~48:To show the leap years form the years range user given
    {

        if(year1%4==0)
        {
            //if the year can be divided without remainder by 4, and then judge if the year can be divided without remainder by 100
            if(year1%100!=0)
            {

                cout<<year1<<" ";
            }
            else if(year1%400==0&&year1%4000!=0)                                                //if the year can be divided without remainder by 400 and can't be divided without remainder by 4000
            {
                cout<<year1<<" ";                                                               //then that year also a leap year
            }

        }

        year1++;
    }
    return 0;
}
