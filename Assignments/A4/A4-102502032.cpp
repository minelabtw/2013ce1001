#include <iostream>
using namespace std;

int main ()
{
    //varible decalaration
    int y_start = 0;            //integer varible; initialize; start year
    int y_end = 0;              //integer varible; initialize; end year

    //Ask for the start year. It's must an integer and larger than zero.
    do
    {
        cout << "Please enter first number( >0 ):";
        cin >> y_start;

        if ( y_start <= 0 )     //y_start has to be larger than 0
            cout << "First number is out of range!!" << endl;
    }
    while ( y_start <= 0 );

    //Ask for the end year. It's must an integer, larger than zero and larger than start year.
    do
    {
        cout << "Please enter second number( >0 ):";
        cin >> y_end;

        if ( y_end <= 0 )       //y_end has to be larger than 0
            cout << "First number is out of range!!" << endl;
        //y_end has to be larger than y_start
        //if not, request y_start again
        else if ( y_end <= y_start )
        {
            cout << "The second number is not greater than the first number!!" << endl;
            do
            {
                cout << "Please enter first number( >0 ):";
                cin >> y_start;

                if ( y_start <= 0 )     //y_start has to be larger than 0
                    cout << "First number is out of range!!" << endl;
            }
            while ( y_start <= 0 );

            y_end = 0;      //reset y_end
        }
    }
    while ( y_start <= 0 or y_end <= y_start);

    cout << "Leap years:";
    //take the closest year which can be divided by 4 as new y_start
    //because all leap years are multiple of 4
    if ( y_start % 4 != 0)
        y_start = y_start + ( 4 - y_start % 4 );

    //if the new y_start is larger than y_end
    //it means that there's no leap year between original y_start and y_end
    if ( y_start > y_end )
        return 0;
    //leap years' condition: (1) multiple of 4 and not multiple of 100 and 4000 (2) multiple of 400
    //suitable to one of two conditions is that we want, then output
    for ( int i = y_start; i <= y_end; i += 4 )
        if ( i % 100 != 0 or i % 400 == 0 )
            if ( i % 4000 != 0 )
                cout << i << " ";
    return 0;
}
