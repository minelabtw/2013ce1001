#include<iostream>
using namespace std;
int main()
{
    int a=0,b=0,z=0;//宣告變數

    cout<<"Please enter first number( >0 ):";//顯示字串
    cin>>a;//輸入數值
    while(a<=0)//當a<=0時進入迴圈
    {
        cout<<"First number is out of range!!";//顯示字串
        cin>>a;//重新輸入數值
    }

    cout<<"Please enter second number( >0 ):";//顯示字串
    cin>>b;//輸入數值
    while(b<=0 && b<=a)//當b<=0 && b<=a時進入迴圈
    {
        if(b<=0)//如果b<=0顯示此字串
            cout<<"Second number is out of range!!";//顯示字串
        else//否則顯示此字串
            cout<<"The second number is not greater than the first number!!";//顯示字串
        cin>>b;//重新輸入數值
    }
    cout<<"Leap years:";//顯示字串
    z=a;//z=a
    while(z<=b)//當z小於a時進入迴圈
    {
        if(z%4==0)//如果可以被4整除
            if(z%100==0)//如果又可以被100整除
                if(z%400==0)//如果又可以被400整除
                    if(z%4000==0)//如果又可以被4000整除
                        cout<<"";//不顯示
                    else//否則顯示z值並空格
                        cout<<z<<" ";
                else//否則不顯示
                    cout<<"";//不顯示
            else//否則顯示z值並空格
                cout<<z<<" ";
        else//否則不顯示
            cout<<"";//不顯示
        z++;//z遞增
    }
    return 0;
}
