#include <iostream>
using namespace std;
bool isNotPositive(float number);//檢查number是否為正
bool isFloat(float number);//檢查number是否帶小數

int main(){
    float startYear = 0.0;//宣告型別為float的起始年，並初始化其數值為0.0
    float endYear = 1.0;//宣告型別為float的結束年，並初始化其數值為1.0
    int sourceType = 0;//宣告型別為int的輸入來源類別，並初始化其數值為0
    float* sourcePtr = 0;//宣告型別為指向float的來源指標，並初始化其數值為0(null)
    /**************載入startYear和endYear *************/
    while(sourceType < 2){
        if(sourceType == 0){//sourceType為startYear
            cout << "Please enter first number( >0 ):";
            sourcePtr = &startYear;
        }
        else{//sourceType為endYear
            cout << "Please enter second number( >0 ):";
            sourcePtr = &endYear;
        }
        cin >> *sourcePtr;

        if(isNotPositive(*sourcePtr) || isFloat(*sourcePtr)){
            //sourcePtr < 0 或是 不是整數
            //則重新輸入
            if(sourceType == 0){
                cout << "First";
            }
            else{
                cout << "Second";
            }
            cout << " number is out of range!!" << endl;
        }
        else{
            //目前為止startYear和endYear皆已輸入完畢
            //但我們接下來要檢查是否結束年小於開始年
            if(sourceType == 1 && startYear > endYear){
                //sourceType為1代表目前載入endYear
                //如果startYear > endYear則要重新輸入
                cout << "The second number is not greater than the first number!!" << endl;
                sourceType = 0;//從startYear開始重新載入
            }
            else{
                sourceType++;
            }
        }
    }
    /**********計算startYear和endYear間的閏年*********/
    cout << "Leap years:";
    for(int i = static_cast<int>(startYear); i <= static_cast<int>(endYear); i++){
        if(((i % 4000) && !(i % 400)) || (!(i % 4) && (i % 100))){
            //i除以400可整除且i不是4000的倍數 或是
            //i除以4可整除且除以100不可整除 為閏年
            cout << i << " ";
        }
    }
    /***********************結束**********************/
    return 0;
}

bool isNotPositive(float number){//檢查number是否為正
    if(number <= 0.0){
        return true;
    }
    else{
        return false;
    }
}

bool isFloat(float number){//檢查number是否帶小數
    float intNumber = (int)number;//intNumber放入number整數部分
    if(intNumber != number){//如果不相等則必帶小數點
        return true;
    }
    else{
        return false;
    }
}
