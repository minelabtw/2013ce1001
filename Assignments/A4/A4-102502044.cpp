/*************************************************************************
    > File Name: ker.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: Fri Oct 18 18:44:05 2013
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

void input(const char *str, const char *err, int *tmp);
int is_there_any_leap(int fir, int sec);
int check_leap(int x);

int main()
{
	/*****************************/
	/* x means the  first number */
	/* y means the second number */
	/*****************************/
	int fir_num, sec_num;
	int flag_years = 0;

	for(;;)
	{
		/* input all variable */
		input("Please enter first number( >0 ):", "First number is out of range!!", &fir_num);
		input("Please enter second number( >0 ):", "Second number is out of range!!", &sec_num);
	
		if(fir_num < sec_num) //check second number is greater than first number
			break;
		else
			puts("The second number is not greater than the first number!!");
	}

	flag_years = is_there_any_leap(fir_num, sec_num); //count how many leap years

	if(flag_years == 0)// the case is no leap year
	{
		puts("Leap year:None");
		return 0;
	}
	else
		flag_years--;

	printf("Leap year%s:", flag_years ? "s" : ""); //print the message (, with end 's')
	
	for(int i=fir_num ; i<=sec_num ; i++) // print all leap years
		if(check_leap(i))
			printf("%d ", i);
	putchar('\n');

	return 0;
}

/******************************************/
/*  str means an string for ui message	  */
/*  err means an string for error message */
/* *tmp means a point to save score		  */
/******************************************/
void input(const char *str, const char *err, int *tmp)
{
	for(;;)
	{
		printf(str); //input number
		scanf("%d", tmp);

		if(*tmp > 0) //check input is in range (0, unlimit)
			break;
		else
			puts(err);
	}
}

int is_there_any_leap(int fir, int sec)
{
	/*********************************/
	/* count how many leap years     */
	/* flag = 0 means 0 leap year    */
	/*      = 1 means 1 leap year    */
	/* = 2 means 2 or more leap year */
	/*********************************/
	int flag = 0;
	
	for(int i=fir ; i<=sec ; i++)
		if(check_leap(i))
		{
			if(!flag)
				flag = 1;
			else	
				return 2;
		}

	return flag;
}


/******************************************/
/* x means the year which will be checked */
/******************************************/
int check_leap(int x)
{
	if(!(x % 400))
		return 1;
	
	if(!(x % 100))
		return 0;

	if(!(x % 4))
		return 1;

	return 0;
}

