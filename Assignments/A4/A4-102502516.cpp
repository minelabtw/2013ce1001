#include <iostream>

using namespace std;

int main()
{
    int first_number = 0;
    int second_number = 0;
    //宣告初始年份和結尾的年份，賦值為零才可以進入迴圈

    while ( second_number <= first_number )
    {
        while ( first_number <= 0 )
        {
            cout << "Please enter first number ( >0 ): ";
            cin >> first_number;
            if (first_number <= 0)
                cout << "First number is out of range !!" << endl ;
        }
        //請使用者輸入第一個數字，並判斷是否>0

        while ( second_number <= 0 )
        {
            cout << "Please enter second number ( >0 ): ";
            cin >> second_number;
            if (second_number <= 0)
                cout << "Second number is out of range !!" << endl ;
        }
        //請使用者輸入第二個數字，並判斷是否>0

        if ( second_number <= first_number )
        {
            cout << "The second number is not greater than the first number!!" << endl ;
            first_number = 0;
            second_number = 0;
        }
        //檢測兩數字大小關係。若有不符合規則，則重新輸入並且重新將兩數值設為零
    }

    cout << endl << "Leap years:" ;
    //先印出一行標題
    while ( first_number <= second_number )
    {
        if ( first_number % 400 == 0)
            cout << first_number << " " ;
        else if ( first_number % 100 == 0);
        else if ( first_number % 4 == 0)
            cout << first_number << " " ;
        first_number ++ ;
    }
    //利用迴圈判斷閏年規則：每逢四閏，逢百不閏，四百又閏
    return 0;
}
