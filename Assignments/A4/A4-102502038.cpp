#include <iostream>
long long int a,b,i;
short int stat = 0;//stat code  abc(Binary)   a=AllDone b=InputNum c=RangeError
void input(void){
    std::cout << (stat==1?"First":"") << (stat==3?"Second":"") << (stat%2==1?" number is out of range!!\n":"") << "Please enter " << ((stat&2)?"second ":"first ") << "number( >0 ):";//Telling User what to do
    std::cin >> ((stat&2)?b:a);//Allow user to input
    std::cout << ((a>b&&(stat&2)&&(b>0))?"The second number is not greater than the first number!!\n":"");//Display if b<a
    stat = (((stat|(((stat&2)?b:a)>0?2:1))&(((stat&2)?b:a)>0?6:7))|(a>b||!((stat&2)&&(!(stat&1)))?0:4))&((a>b&&(stat&2)&&(b>0))?0:7);//Modify stat code
    if(!(stat&4))input();//Recursive call
};
void output(void){
    i=(i<a?a:i+1);
    std::cout << (i==a?"Leap years:":"");//Init display
    if((i%400==0)||((i%100!=0)&&(i%4==0)))std::cout << i <<" ";//Display leap years
    if(i<b)output();//Recursive call
};
int main(void){
    input();
    output();
};
