#include <iostream>
using namespace std;

int main(){

    int x, y ,temp = 0;

    ReInput:
    while(true){

        cout << "Please enter the first number( >0 ): ";
        cin >> temp;   //先將輸入存到暫存器

        if(temp<=0)    //如果輸入小於或等於0,則重新回到while迴圈
            cout << "First number is out of range!!" << endl;
        else{          //否則輸入正確,存入第一個變數,並中斷迴圈
            x = temp;
            break;
        }
    }

    while(true){       //同上

        cout << "Please enter the second number( >0 ): ";
        cin >> temp;

        if(temp<=0)
            cout << "Second number is out of range!!" << endl;
        else if(temp <= x){
            cout << "The second number is not greater than the first number!!" << endl;
            goto ReInput;
        }
        else{
            y = temp;
            break;
        }
    }

    while(x%4 != 0 && x< y){
        x++;
    }

    cout << "Leap years: ";

    while(x <= y){
        if(x%4000 = 0)
            ;
        else if(x%400 = 0)
            cout << x << " ";
        else if(x%100 = 0)
            ;
        else
            cout << x << " ";

        x += 4;
    }
    return 0;
}
