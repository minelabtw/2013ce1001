//A4-102302053.cpp
#include <iostream>
#include <cstdlib>//allow program to pause after the computing.

using namespace std;

int main()
{
    int number1;// use as the lowwer bound of the detection.
    int number2;//use as the upper bound of the detection.
    int number3;//use to be the ouptput of the detection.

    do // prompt user to give two integers that are greater than 0, and the second integer must be greater than the first one..
    {
        cout << "Please enter first number( >0 ):";
        cin >> number1;
        while (number1 < 0)
        {
            cout << "First number is out of range!!\n";
            cout << "Please enter first number( >0 ):";
            cin >> number1;
        }

        cout << "Please enter second number( >0 )";
        cin >> number2;
        while (number2 < 0 )
        {
            cout << "Second number is out of range!!\n";
            cout << "Please enter second number( >0 ):";
            cin >> number2;
        }
    }
    while (number2 < number1 && cout << "The second number is not greater than the first number!!\n");


    number3 = number1;//assign number1 to be the initial value for number3.

    cout << "Leap years:";

    while (number3 < number2)//output numbers which are leap yeas.
    {
        if ((number3 % 100 ==0)&&(number3 % 400 != 0))
        cout << "";
        else if ((number3 % 400 == 0) or (number3 % 4 == 0) or ((number3 % 4 == 0)&&(number3 %100 != 0)))
        cout << number3 << " ";
        else
        cout << "";

        number3++;
    }

    cout << "\n";

    system ("pause");
    return 0;
}
