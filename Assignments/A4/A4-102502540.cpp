#include <iostream>

using namespace std;

int main()
{
    int integer1 = 0; //宣告一個整數變數,並初始化其數值為0
    int integer2 = 0; //宣告一個整數變數,並初始化其數值為0

    cout << "Please enter first number( >0 ):"; //顯示Please enter first number( >0 ):
    cin >> integer1; //輸入的值給integer1

    while ( integer1 <= 0 ) //當integer1<=0時進入以下迴圈
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):"; //顯示First number is out of range!!換行顯示Please enter first number( >0 ):
        cin >> integer1;
    }

    cout << "Please enter second number( >0 ):"; //顯示Please enter second number( >0 ):
    cin >> integer2; ////輸入的值給integer2

    while ( integer2 <= 0 ) //當integer2<=0時進入以下迴圈
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):"; //顯示Second number is out of range!!換行顯示Please enter second number( >0 ):
        cin >> integer2;
    }

    while ( integer1 >= integer2 ) //當integer1>=integer2時進入以下迴圈
    {
        cout << "The second number is not greater than the first number!!" << endl << "Please enter first number( >0 ):";
        cin >> integer1;

        while ( integer1 <= 0 ) //當integer1<=0時進入以下迴圈
        {
            cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";
            cin >> integer1;
        }

        cout << "Please enter second number( >0 ):";
        cin >> integer2;

        while ( integer2 <= 0 ) //當integer2<=0時進入以下迴圈
        {
            cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):";
            cin >> integer2;
        }
    }
    cout << "Leap years:"; //顯示Leap years:

    while ( integer1 <= integer2 ) //當integer1<=integer2時進入以下迴圈
    {
        if ( integer1 % 400 == 0 ) //判斷當integer1除以顯示0餘數為0時
            cout << integer1 << " "; //顯示integer1並空一格
        else if ( integer1 % 4 == 0 and integer1 % 100 != 0 ) //判斷當integer1除以4餘數為0且integer1除以100餘數不為0時
            cout << integer1 << " "; //顯示integer1並空一格

        integer1++; //使integer1值+1
    }

    return 0;
}
