#include <iostream>
using namespace std;
int main ()
{
    int int1 =1;//宣告變數並=0
    int int2 =0;//宣告變數並=0
    int run =0;//宣告變數並=0
    while (int1>=int2)//判斷相對大小
    {
        if (run!=0)
            cout << "The second number is not greater than the first number!!" << endl;
        run++;
        cout << "Please enter first number( >0 ):";//提示輸入
        cin >> int1;//輸入
        while (int1<=0)//判斷輸入條件
        {

            cout << "First number is out of range!!" << endl;//提示不符合條件
            cout << "Please enter first number( >0 ):";//提示輸入
            cin >> int1;//輸入
        }
        cout << "Please enter second number( >0 ):";//提示輸入
        cin >> int2;//輸入
        while (int2<=0)//判斷輸入條件
        {
            cout << "Second number is out of range!!" << endl;//提示不符合條件
            cout << "Please enter second number( >0 ):";//提示輸入
            cin >> int2;//輸入
        }
    }
    run=int1;//從最小開始
    cout << "Leap years:";//題目條件
    while (int1<=run && run<=int2)//當在條件中迴圈
    {
        if (run%4000==0);//四千不閏
        else if (run%400==0)//四百又閏
        {
            cout << run << " ";
        }
        else if (run%100==0);//逢百不閏
        else if (run%4==0)//四年一閏
        {
            cout << run << " ";//輸出年分
        }

        run++;
    }

    return 0;
}
