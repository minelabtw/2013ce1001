#include<iostream>
using namespace std;

int main()
{
    int number1=0;    //宣告一變數number1，並使其初始值為0。
    int number2=0;    //宣告一變數number2，並使其初始值為0。

    cout << "Please enter first number( >0 ):";    //輸出Please enter first number( >0 ):至螢幕。
    cin >> number1;    //輸入至number1。

    while(number1 <= 0)    //while迴圈限制範圍在number1<=0。
    {
        cout << "First number is out of range!!\n";    //輸出First number is out of range!!至螢幕。
        cout << "Please enter first number( >0 ):";    //輸出Please enter first number( >0 ):至螢幕。
        cin >> number1;    //輸入至number1。
    }

    cout << "Please enter second number( >0 ):";    //輸出Please enter second number( >0 ):至螢幕。
    cin >> number2;    //輸入至number2。

    while(number2 <= 0)    //while迴圈限制範圍在number2<=0。
    {
        cout << "Second number is out of range!!\n";    //輸出Second number is out of range!!至螢幕。
        cout << "Please enter second number( >0 ):";    //輸出Please enter second number( >0 ):至螢幕。
        cin >> number2;    //輸入至number2。
    }

    while(number1 > number2)    //while迴圈限制範圍在number1>number2。
    {
        cout << "The second number is not greater than the first number!!\n";    //輸出Please enter first number( >0 ):至螢幕。

        cout << "Please enter first number( >0 ):";    //輸出Please enter first number( >0 ):至螢幕。
        cin >> number1;    //輸入至number1。
        while(number1 <= 0)    //while迴圈限制範圍在number1<=0。
        {
            cout << "First number is out of range!!\n";    //輸出First number is out of range!!至螢幕。
            cout << "Please enter first number( >0 ):";    //輸出Please enter first number( >0 ):至螢幕。
            cin >> number1;    //輸入至number1。
        }

        cout << "Please enter second number( >0 ):";    //輸出Please enter second number( >0 ):至螢幕。
        cin >> number2;    //輸入至number2。

        while(number2 <= 0)    //while迴圈限制範圍在number2<=0。
        {
            cout << "Second number is out of range!!\n";    //輸出Second number is out of range!!至螢幕。
            cout << "Please enter second number( >0 ):";    //輸出Please enter second number( >0 ):至螢幕。
            cin >> number2;    //輸入至number2。
        }
    }

    cout << "Leap years:";    //輸出Leap years:至螢幕。

    for(int i=number1 ; i <= number2 ; i++)    //for迴圈運算。
    {
        if(i % 4000 != 0 && i % 400 == 0)
            cout << i << " ";
        else if(i % 100 != 0 && i % 4 == 0)
            cout << i << " ";
    }

    return 0;
}
