#include <iostream>

using namespace std;

int main()
{

    int year1 = 0; // 第一個年份
    int year2 = 0; // 第二個年份

    // 輸入年份
    for(int i=0; i<1; i++)
    {
        // 判斷第一個年份
        while(year1<1)
        {
            cout << "Please enter first number( >0 ):";
            cin >> year1;
            if(year1<1)cout << "First number is out of range!!\n";
        }

        // 判斷第二個年份
        while(year2<1)
        {
            cout << "Please enter second number( >0 ):";
            cin >> year2;
            if(year2<1)cout << "Second number is out of range!!\n";
        }

        // 判斷第二個年份是否大於第一個年份
        while(year1>year2)
        {
            cout << "The second number is not greater than the first number!!\n";
            year1 = 0;
            year2 = 0;
            i--;
        }
    }

    // 輸出閏年
    cout << "Leap years:";
    for(int j=year1; j<=year2; j++)
    {
        if(j%400==0)cout << j << " ";
        else if (j%4 == 0 and j%100 !=0)cout << j << " ";
        else;

    }

    return 0;
}
