#include <iostream>

using namespace std;

int main()
{

    int a,b,year;//宣告整數變數a b代表範圍
    cout<<"Please enter first number( >0 ):";
    cin>>a;
    if (a<0)
    {
        cout<<"First number is out of range!!"<<endl;//但a必須要大於0
        cout<<"Please enter first number( >0 ):";
        cin>>a;
    }
    cout<<"Please enter second number( >0 ):";
    cin>>b;
    if (b<0)
    {
        cout<<"Second number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }
    while (a>=b)//而且b要大於a
    {
        cout<<"The second number is not greater than the first number!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>a;
        if (a<0)
        {
            cout<<"First number is out of range!!"<<endl;
            cout<<"Please enter first number( >0 ):";
            cin>>a;
        }
        cout<<"Please enter second number( >0 ):";
        cin>>b;
        if (b<0)
        {
            cout<<"Second number is out of range!!"<<endl;
            cout<<"Please enter second number( >0 ):";
            cin>>b;
        }
    }
    cout<<"Leap years:";
    for (year=a; a<=year&&year<=b; year++)
    {
        if(year%400==0&&year%4000!=0)//閏年必須要400的倍數不是4000的倍數
        {
            cout<<year<<" ";//每個閏年之間要空一格
        }
        else if (year%4==0&&year%100!=0)//閏年要是四的倍數但不是100的倍數
        {
            cout<<year<<" ";
        }
    }
    return 0;
}

