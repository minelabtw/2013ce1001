#include <iostream>
using namespace std;

int main()
{
    int year1; //call variable
    int year2; //call variable

    do
    {
        year1=0; //initialize variable to 0 for getting into the loop
        while(year1<=0)//data validation
        {
            cout<<"Please enter first number(>0):"; // output words
            cin>>year1; //input number
            if(year1<=0)
            {
                cout<<"First number is out of range!!"<<endl;
            }
        }

        year2=0; // initialize the variable to 0 for getting into the loop
        while(year2<=0)//data validation
        {
            cout<<"Please enter second number(>0):";// output words
            cin>>year2; // input words
            if(year2<=0)
            {
                cout<<"Second number is out of range!!"<<endl;
            }
        }

        if(year2<=year1) //validate to ensure the second input number is smaller than the first one
        {
            cout<<"The second number is not greater than the first number!!"<<endl;
        }
    }
    while(year2<=year1);

    cout<<"Leap years:"; // output words
    while(year1<=year2) //calculate for leap year within range year1 to year2
    {
        if(year1%400==0||year1%4==0&&year1%100>0) //calculate for whether it is a leap year
        {
            cout<<year1<<" "; //output a leap year
        }
        year1++; //plus one ever time to calculate the next year
    }

    return 0;
}
