#include <iostream>
using namespace std;
int main()
{
    int a = 0; //宣告變數a等於零
    int b = 0; //宣告變數b等於零

    cout <<"Please enter first number( >0 ):"; //輸出
    cin >> a; //輸入a
    while (a <= 0 or b <= 0 or b <= a) //進入迴圈
    {
        if (a > 0 and b == 0)
        {
            cout <<"Please enter second number( >0 ):";
            cin >> b; //輸入b
        }
        else if (a <= 0)
        {
            cout <<"First number is out of range!!"<< endl <<"Please enter first number( >0 ):";
            cin >> a; //輸入a
        }
        else if (b <= 0)
        {
            cout <<"Second number is out of range!!"<< endl <<"Please enter second number( >0 ):";
            cin >> b; //輸入b
        }
        else if (b <= a)
        {
            cout <<"The second number is not greater than the first number!!"<< endl <<"Please enter first number( >0 ):";
            b = 0; //使b值回到0
            cin >> a; //輸入a
        }
    }

    cout <<"Leap years:"; //輸出閏年的年份
    while(a <= b) //重複判斷a是否小於等於b
    {
        if (a%400==0 && a%4000!=0)
        {
            cout << a <<" ";
        }
        else if (a%4==0 && a%100!=0)
        {
            cout << a <<" ";
        }
        a++; //使a值加1
    }
    return 0;
}
