#include<iostream>
using namespace std;

int main()
{
    int firstnumber=0;//宣告型別為整數(int)的firstnumber，並初始化其數值為0。
    int secondnumber=0;//宣告型別為整數(int)的secondnumber，並初始化其數值為0。

    cout<<"Please enter first number( >0 ):";
    cin>>firstnumber;

    while(firstnumber<=0)//firstnumber>0
    {
        cout<<"First number is out of range!!"<<endl<<"Please enter first number( >0 ):";//firstnumber<=0顯示First number is out of range!!
        cin>>firstnumber;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>secondnumber;

    while(secondnumber<=0)//secondnumber>0
    {
        cout<<"Second number is out of range!!"<<endl<<"Please enter second number( >0 ):";//secondnumber<=0顯示Second number is out of range!!
        cin>>secondnumber;
    }

    while(secondnumber<=firstnumber)//firstnumber<=secondnumber
    {
        cout<<"The second number is not greater than the first number!!"<<endl<<"Please enter first number( >0 ):";//secondnumber<=firstnumber顯示The second number is not greater than the first number!!
        cin>>firstnumber;
        cout<<"Please enter second number( >0 ):";
        cin>>secondnumber;
    }

    cout<<"Leap years:";
    while(firstnumber<=secondnumber)
    {
        if(firstnumber%400==0 or firstnumber%4==0&&firstnumber%100!=0)
        {
            if(firstnumber%4000!=0)
            {
                cout<<firstnumber<<" ";
            }
        }
        else
        {
            cout<<"";
        }
        firstnumber++;
    }
    return 0;
}


