#include<iostream>
using namespace std;
int main()
{
    int a;//宣告變數a
    int b;//宣告變數b
    cout<<"Please enter first number( >0 ):";//將"Please enter first number( >0 ):"輸出到螢幕上
    cin>>a;//將輸入的值丟給a
    while(a<=0)//當a小於等於0進入迴圈
    {
        cout<<"First number is out of range!!\n";//將"First number is out of range!!"輸出到螢幕上
        cout<<"Please enter first number( >0 ):";//將"Please enter first number( >0 ):"輸出到螢幕上
        cin>>a;//重新給定一個a的值
    }
    cout<<"Please enter second number( >0 ):";//將"Please enter second number( >0 ):"輸出到螢幕上
    cin>>b;//將輸入的值丟給b
    while(b<=0)//b小於等於0進入迴圈
    {
        cout<<"Second number is out of range!!\n";//將"Second number is out of range!!"輸出到螢幕上
        cout<<"Please enter second number( >0 ):";//將"Please enter second number( >0 ):"輸出到螢幕上
        cin>>b;//將輸入的值丟給b
    }
    while(a>=b)//當a大於等於b進入迴圈
    {
        cout<<"The second number is not greater than the first number!!\n";//將"The second number is not greater than the first number!!"輸出到螢幕上
        cout<<"Please enter first number( >0 ):";//將"Please enter first number( >0 ):"輸出到螢幕上
        cin>>a;//將輸入的值丟給a
        while(a<=0)
        {
            cout<<"First number is out of range!!";//將"First number is out of range!!"輸出到螢幕上
            cout<<"Please enter first number( >0 ):";//將"Please enter first number( >0 ):"輸出到螢幕上
            cin>>a;//重新給定一個a的值
        }
        cout<<"Please enter second number( >0 ):";//將"Please enter second number( >0 ):"輸出到螢幕上
        cin>>b;//將輸入的值丟給b
        while(b<=0)//b小於等於0進入迴圈
        {
            cout<<"Second number is out of range!!";//將"Second number is out of range!!"輸出到螢幕上
            cout<<"Please enter second number( >0 ):";//將"Please enter second number( >0 ):"輸出到螢幕上
            cin>>b;//將輸入的值丟給b
        }
    }
    cout<<"Leap years:";//將"Leap years:"輸出到螢幕上
    for(int c=a; c<b; c=c+1)//宣告一個變數c並將初始值訂為a當c小於b時進入迴圈每次跑完迴圈都把c用c+1帶入
    {
        if(c%4000==0)//當c被4000整除時執行下列程式
        {
            continue;//重新判斷迴圈
        }
        else if(c%400==0)//當c被400整除時執行下列程式
        {
            cout<<c<<" ";//將c與空格輸出到螢幕上
        }
        else if(c%4==0&&c%100!=0)//當c被4整除又不被100整除時執行下列程式
        {
            cout<<c<<" ";//將c與空格輸出到螢幕上
        }
    }
return 0;
}
