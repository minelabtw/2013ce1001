#include<iostream>

using namespace std;

int main()
{
    int firstnumber = 0,secondnumber = 0;//宣告整數firstnumber以及secondnumber原數值為0

    cout << "Please enter first number( >0 ):";
    cin >> firstnumber;
    while( firstnumber <= 0 )
    {
        cout << "First number is out of range!!" << endl;//輸出字彙並換行重新輸入
        cout << "Please enter first number( >0 ):";
        cin >> firstnumber;
    }

    cout << "Please enter second number( >0 ):";
    cin >> secondnumber;
    while( secondnumber <= 0 )
    {
        cout << "Second number is out of range!!" << endl;//輸出字彙並換行重新輸入
        cout << "Please enter second number( >0 ):";
        cin >> secondnumber;
    }

    while( secondnumber <= firstnumber )
    {
        cout << "The second number is not greater than the first number!!" << endl;//輸出字彙並換行

        cout << "Please enter first number( >0 ):";
        cin >> firstnumber;
        while( firstnumber <= 0 )
        {
            cout << "First number is out of range!!" << endl;
            cout << "Please enter first number( >0 ):";
            cin >> firstnumber;
        }

        cout << "Please enter second number( >0 ):";
        cin >> secondnumber;
        while( secondnumber <= 0 )
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):";
            cin >> secondnumber;
        }
    }

    cout << "Leap years:";//輸出字彙並假設狀況且middle必須在範圍內持續增加
    for ( int middle = firstnumber ; middle <= secondnumber ; middle++ )
    {
        if ( ( middle % 4 ) == 0 && ( middle % 100 ) != 0 || ( middle % 400 ) == 0 && ( middle % 4000 ) != 0 )
            cout << middle << " ";
    }

    return 0;
}
