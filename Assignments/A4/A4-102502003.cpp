#include<iostream>
using namespace std;

int main()
{
    int num_st = 0;
    int num_nd = 0;

    while(num_st<=0)
    {
        cout<<"Please enter first number( >0 ):";
        cin>>num_st;
        if(num_st<=0)
            cout<<"First number is out of range!!"<<endl;
    }



    while(num_st>num_nd)
    {
        cout<<"Please enter second number( >0 ):";
        cin>>num_nd;
        if(num_nd<=0)
            cout<<"Second number is out of range!!"<<endl;
        else if(num_st>num_nd)
            cout<<"The second number is not greater than the first number!!"<<endl;
    }

    cout << "Leap years:";
    for(int year=num_st; year<=num_nd; year++)  //判斷是否為閏年 並輸出
    {
        if (year%4==0)//可被4除盡
        {
            if (year%100!=0) //不可被100除盡
            {
                if (year%400==0)  //可被400除盡
                {
                    cout << year ;
                }
            }
            cout << year << " ";
        }
    }

    return 0;

}






