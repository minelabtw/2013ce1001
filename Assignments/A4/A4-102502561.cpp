#include<iostream>
using namespace std;

int main()
{
    int number1;    //年份一
    int number2;    //年分二
    int year;       //迴圈用年分
    do
    {
        cout << "Please enter first number( >0 ):";
        cin >>  number1;
        while(number1<=0)
        {
            cout << "First number is out of range!!\n";
            cout << "Please enter first number( >0 ):";
            cin >>  number1;
        }                                               //^輸入年份一且確認符合範圍

        cout << "Please enter second number( >0 ):";
        cin >> number2;
        while(number2<=0)
        {
            cout << "Second number is out of range!!\n";
            cout << "Please enter second number( >0 ):";
            cin >>  number2;
        }                                               //^輸入年分二且確認年份二>0
        if(number2<=number1)                            //ˇ當年分二<=年分一時啟動
        {
            cout << "The second number is not greater than the first number!!\n";
        }
    }
    while(number1>=number2);
    cout << "leap years:";
    for(year=number1; year<=number2; year++)            //從年份一到年份二之間的所有年分
    {
        if(year%400==0||(year%4==0&&year%100!=0))       //如果符合條件則輸出
        {
            cout << year << " ";                        //輸出年分+空格
        }
    }
    return 0;
}
