#include <iostream>

using namespace std;

int main()
{
    int year1=0;        //宣告第一個年份變數整數。
    int year2=0;        //宣告第二個年份變數整數。

    cout << "Please enter first number(>0):";       //顯示需填入一個數字，並不可以小於等於0.
    cin >> year1;
    while (year1<=0)        //做一迴圈不可以小於等於0.
    {
        cout << "First number is out of range!!" << endl << "Please enter first number(>0):";
        cin >> year1;
    }

    cout << "Please enter second number(>0):";
    cin >> year2;
    while (year2<=0)
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number(>0):";
        cin >> year2;
    }

    while (year2<=year1)        //輸入的第二年份不可以小於等於第一年份，否者進入迴圈。

    {
        cout << "The second number is not greater than the first number!!" << endl ;
        cout << "Please enter first number(>0):";
        cin >> year1;
        while (year1<=0)
        {
            cout << "First number is out of range!!" << endl << "Please enter first number(>0):";
            cin >> year1;
        }

        cout << "Please enter second number(>0):";
        cin >> year2;
        while (year2<=0)
        {
            cout << "Second number is out of range!!" << endl << "Please enter second number(>0):";
            cin >> year2;
        }

    }

    cout << "Leap years:";      //顯示閏年。

    while ( year1<=year2 )      //進入迴圈，當可以被4及400正處，但不可以被100整除的就是閏年。
    {
        if(year1%400==0)
        {
            cout << year1 << " " ;
        }
        else if(year1%100==0)
        {
            cout << "" ;
        }

        else if(year1%4==0)
        {
            cout << year1 << " " ;
        }

        year1++;
    }
}
