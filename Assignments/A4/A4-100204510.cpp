#include <iostream> // allows program to input and output data to the screen

using namespace std; //program uses names from the std namespace

int main () // function main begins program execution
{
    int year1; //the fiest number of the year
    int year2; //the second number of the year
    int leapyear; //output of the leap years

    while (1) //determination of the entered-numbers
    {
        while (1) //determination of the first number
        {
            cout <<"Please enter first number ( >0 )"; //print the word
            cin >>year1; //input the number
            if (year1 <=0) //determine the number
                cout <<"First number is out of range!!\n"; //if the number does not fit the condition then print
            else if (year1 >=0) //if the number fits the condition
                break; //leave the while
        }
        if (year1 >=0) //if the inputed number fits the condition
            cout <<"Please enter second number ( >0 )"; //print the words
        cin >>year2; //input the second number
        if (year2 <=0) //determine the inputed number
            cout <<"Second number is out of range!!\n"; //print the error information
        else if (year2 <= year1) //if the second number does not fit the condition
            cout <<"The second number is not greater than the first number!!\n"; //print the words
        else if (year2 >= year1) //the second number fit the setted condition
            break; //leave the while
    }
    cout <<"Leap years:"; //print the data words
    for (leapyear = year1; year2 >= leapyear; leapyear++) //setting the condition to the leap years
        if ((leapyear%4==0 && leapyear%100!=0)||(leapyear%400==0 && leapyear%4000!=0)) //setting to determine the input numbers
            cout <<leapyear<<"\t"; //print the data
        else //if the number are not leap years
            cout <<""; //print the space

    return 0; //indicate that program ended successfully
}
