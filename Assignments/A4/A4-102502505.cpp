#include <iostream>

using namespace std;

int main()
{
    int integer1;//宣告變數
    int integer2;

    cout << "Please enter first number( >0 ):";//輸出字串
    cin >> integer1;

    if ( integer1 <= 0 )//加入變數一的條件，假如不符合則重新輸入
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> integer1;
    }

    cout << "Please enter second number( >0 ):";
    cin >> integer2;

    if ( integer2 <= 0 )
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> integer2;
    }

    while ( integer1 >= integer2 )//加入條件(使用迴圈)，使變數一要小於變數二，假如沒有則重新輸入變數一及變數二
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> integer1;

        if ( integer1 <= 0 )
        {
            cout << "First number is out of range!!" << endl;
            cout << "Please enter first number( >0 ):";
            cin >> integer1;
        }

        cout << "Please enter second number( >0 ):";
        cin >> integer2;

        if ( integer2 <= 0 )
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):";
            cin >> integer2;
        }

    }

    cout << "Leap year:";//輸出字串

    while ( integer1 <= integer2 )//當變數一小於變數二時，執行下面的指令
    {
        if ( integer1%400 == 0 and integer1%4000 != 0 )//假如變數一是400的倍數而且不是4000的倍數，則是閏年，所以輸出
        {
            cout << integer1 << " ";
            integer1 = integer1+1;
        }

        if ( integer1%4 == 0 and integer1%100 != 0)//假如變數一可被4整除，但不是100的倍數，則是閏年，所以輸出
        {
            cout << integer1 << " ";
            integer1 = integer1+1;
        }

        else//假如不符合上述兩個條件之一，則變數一加一，直至變數一大於變數二
        {
            integer1 = integer1+1;
        }
    }

    return 0;
}
