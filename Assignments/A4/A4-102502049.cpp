#include <iostream>
using namespace std;

int main()
{
    int number1 = 0;
    int number2 = 0;
    int leapyear = 0; //宣告三個整數分別儲存兩個年份及閏年

    while ( number1 >= number2 ) //要求後面的年分必大於前面的年份
    {
        number1 = 0;
        number2 = 0; //若後面年份小於前面的年份，初始化兩年份為0

        while ( number1 <= 0 ) //要求第一年份必須為大於0的整數
        {
            cout << "Please enter first number( >0 ):";
            cin >> number1;
            if( number1 <= 0 )
            {
                cout << "First number is out of range!!" << endl;
            }
        }

        while ( number2 <= 0 ) //要求第二年份必須為大於0的整數
        {
            cout << "Please enter second number( >0 ):";
            cin >> number2;
            if ( number2 <= 0 )
            {
                cout << "Second number is out of range!!" << endl;
            }
        }

        if ( number1 >= number2 ) //若第二年份小於第一年份顯示錯誤
        {
            cout << "The second number is not greater than the first number!!" << endl;
        }
    }

    cout << "Leap years:";

    while ( number1 <= number2 ) //顯示閏年，必須可以整除4
    {
        if ( number1%4 == 0 )
        {
            cout << number1 << " ";
        }
        number1++;
    }

    return 0;
}
