#include <iostream>
using namespace std;

int main()
{
    int num1;//宣告第一個數字
    int num2;//宣告第二個數字

    //判斷數字是否合理
    while(true)
    {
        while(true)
        {
            cout<<"Please enter first number( >0 ):";
            cin>>num1;

            if(num1<=0)
                cout<<"First number is out of range!!"<<endl;
            else
                break;
        }

        while(true)
        {
            cout<<"Please enter second number( >0 ):";
            cin>>num2;

            if(num2<=0)
                cout<<"Second number is out of range!!"<<endl;
            else
                break;
        }

        if(num2<=num1)
        {
            cout<<"The second number is not greater than the first number!!"<<endl;
        }
        else
            break;
    }

    cout<<"Leap years:";

    //判斷並印出閏年
    for(int i=num1; i<=num2; i++)
    {
        if(i%4==0&&i%100!=0||i%400==0&&i%4000!=0)
            cout<<i<<" ";
    }

    return 0;
}
