#include<iostream>
using namespace std;

int main()
{
    int first, second;

    while(1)
    {
        //a loop testing if second input is greater than first

        while(1)
        {
            //a loop testing if first input is illegal

            cout << "Please enter first number( >0 ):";
            cin >> first;
            if(first>0)
                break;
            cout << "First number is out of range!!" << endl;
        }
        while(1)
        {
            //a loop testing if second input is illegal

            cout << "Please enter second number( >0 ):";
            cin >> second;
            if(second>0)
                break;
            cout << "Second number is out of range!!" << endl;
        }
        if(second>first)
            break;
        cout << "The second number is not greater than the first number!!" << endl;
    }
    //output
    cout << "Leap years:";
    bool isLeapYear;
    for(int i=((first+3)>>2)<<2; i<=second ; i+=4)
    {
        isLeapYear = true;
        if(!(i%100))
            isLeapYear = !isLeapYear;
        if(!(i%400))
            isLeapYear = !isLeapYear;
        if(!(i%4000))
            isLeapYear = !isLeapYear;
        if(isLeapYear)
            cout << i << " ";
    }
    cout << endl;

    return 0;
}
