#include <iostream>

using namespace std;

int main()
{
    int number1;                                                                         //宣告變數
    int number2;

    while(1)                                                                             //進入迴圈
    {
        cout<<"Please enter first number( >0 ):";
        cin>>number1;
        while(number1<=0)                                                                //限制數字1範圍
        {
            cout<<"First number is out of range!!"<<endl;
            cout<<"Please enter first number( >0 ):";
            cin>>number1;
        }
        cout<<"Please enter second number( >0 ):";
        cin>>number2;
        while(number2<=0)                                                                //限制數字2範圍
        {
            cout<<"Second number is out of range!!"<<endl;
            cout<<"Please enter second number( >0 ):";
            cin>>number2;
        }
        if(number1<number2)                                                              //當數字2大於數字1跳出迴圈執行下一步
            break;
        else cout<<"The second number is not greater than the first number!!"<<endl;
    }
    cout<<"Leap years:";
    for(int i=number1; i<number2; ++i)                                                   //進入FOR迴圈判斷是否為閏年
    {
        if(i%4000==0)                                                                    //閏年條件
            cout<<"";
        else if(i%400==0)
            cout<<i<<" ";
        else if(i%100==0)
            cout<<"";
        else if(i%4==0)
            cout<<i<<" ";
        else
            cout<<"";

    }

    return 0;
}
