#include <iostream>
using namespace std;
int main()
{
    int a;  //宣告名稱為a的整數變數
    int b;  //宣告名稱為b的整數變數

    cout << "Please enter first number( >0 ):";  //輸出字串
    cin >> a;  //將輸入的值給a

    while (a<0)  //使用while迴圈，若a數值小於0就會一直重複該程式碼內的動作
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";  //輸出字串
        cin >> a;
    }

    cout << "Please enter second number( >0 ):";
    cin >> b;

    while (b<0)  //使用while迴圈，若b數值小於0就會一直重複該程式碼內的動作
    {
        cout << "Second number is out of range" << endl << "Please enter second number( >0 ):";
        cin >> b;
    }

    while (b<a)  //使用while迴圈，若b數值小於a就會一直重複該程式碼內的動作
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> a;  //重新輸入a值

        while (a<0)
        {
            cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";
            cin >> a;
        }

        cout << "Please enter second number( >0 ):";
        cin >> b;  //重新輸入b值

        while (b<0)
        {
            cout << "Second number is out of range" << endl << "Please enter second number( >0 ):";
            cin >> b;
        }

    }


    cout << "Leap years:";
    while (a<=b)  ////使用while迴圈，若a數值小於等於b就會一直重複該程式碼內的動作
    {
        if (a%400==0 and a%100!=0)  //如果a值整除400且不整除100則輸出該值
        {
            cout << a << " ";
            a++;
        }
        else if (a%4==0 and a%100!=0)  //如果a值整除4則輸出該值
        {
            cout << a << " ";
            a++;
        }
        else
            a++;  //a遞增
    }


    return 0;
}
