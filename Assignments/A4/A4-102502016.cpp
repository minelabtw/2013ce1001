#include <iostream>

using namespace std;

int main()
{
    int a=0;//設a,b為兩變數
    int b=0;

    while (a<=0)//當a<=0進入{}的內容
    {
        cout << "Please enter first number( >0 ):";//列出""的內容
        cin >> a;//輸入值給a
        b=0;//把b的值弄成0(因為後面的while若不成立,跳回來時要讓b回歸初始值)

        if (a<=0)//若a<=0則進入{}的內容
        {
            cout << "First number is out of range!!"<<endl;
        }
        else//若if不成立,則進入{}的內容
        {
            while (b<=0)//當b<=0時進入{}的內容
            {
                cout <<"Please enter second number( >0 ):";
                cin >> b;

                if (b<=0)
                    cout << "Second number is out of range!!"<<endl;

                else
                {
                    if (b<a)//假如b<a則進入{}的內容
                    {
                        cout << "The first number is not greater than the second number!!"<<endl;
                        a=0;//把a的值弄成0(使a能夠符合外面大while的條件,讓整個程式重來)
                    }
                }
            }
        }
    }
    cout <<"Leap years:";//列出""的內容
    for (int c=a; c<=b; c++)//重複做a~b次{}的內容
    {
        if ((c%4000)!=0)//假如C/4000的餘數不等於0,則進入下面{}的程式
        {
            if ((c%400)==0)//假如c/400的餘數=0,則列出C+空格
                cout <<c<<" ";

            else//若if不成立
            {
                if((c%100)==0)//假如c/100的餘數=0,不做任何事
                {   }
                else
                {
                    if((c%4)==0)//若C/4的餘數=0,則列出C+空格
                        cout <<c<<" ";
                }
            }
        }
    }
    return 0;
}
