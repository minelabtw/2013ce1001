#include <iostream>

int main()
{

    int endpoint[2] ;   //宣告區間邊界

    for ( int i = 0 ; i != 2 ; i++ )    //輸入
    {
        std::cout << "Please enter " << ( i == 0 ? "first" : "second" ) << " number( >0 ):" ;
        std::cin >> endpoint[i] ;
        if ( endpoint[i] <= 0 )
        {
            std::cout << ( i == 0 ? "First" : "Second" ) << " number is out of range!!" << std::endl ;
            i -- ;
        }
        else if ( i == 1 && endpoint[1] <= endpoint[0] )
        {
            std::cout << "The second number is not greater than the first number!!" << std::endl ;
            i -= 2 ;
        }
    }

    int now = endpoint[0] , flag = 1 ;   //宣告指針及旗標
    for ( int i = now ; i <= endpoint[1] ; i++ )    //尋找第一個閏年
        if ( i % 4 == 0 && i % 100 != 0 || i % 400 == 0 && i % 4000 != 0 ) //找到第一個閏年
        {
            now = i ;
            for ( int j = now + 1 ; j <= endpoint[1] ; j++ )    //尋找第二個閏年
                if ( j % 4 == 0 && j % 100 != 0 || j % 400 == 0 && j % 4000 != 0  )   //多個閏年的情形
                {
                    std::cout << "Leap years:" << i << ' ' << j ;
                    flag = 0 ;
                    now = j ;
                    break ;
                }
            if ( flag ) //單個閏年的情形
            {
                std::cout << "Leap year:" << i << std::endl ;
                return 0 ;
            }
            break ;
        }
    if ( flag )    //沒有任何閏年的情形
    {
        std::cout << "There is no leap year between the two numbers!!" << std::endl ;
        return 0 ;
    }
    for ( int i = now + 1 ; i <= endpoint[1] ; i++ )    //尋找其餘閏年
        if ( i % 4 == 0 && i % 100 != 0 || i % 400 == 0 && i % 4000 != 0  )
            std::cout << ' ' << i ;

    return 0 ;
}
