#include<iostream>
using namespace std;

int main()
{
    int a,b;
    bool flag1,flag2; // to record if it is legal
    cout<<"Please enter first number( >0 ):";
    while( cin>>a )
    {
        if( a>0 )  // valid input
        {
            flag1=true;
            cout<<"Please enter second number( >0 ):";
            while( cin>>b )
            {
                if( a<b )  // valid input.
                {
                    flag2=true;

                    bool first=true;
                    cout<<"Leap years:";
                    for(int i=a; i<=b; i++)
                        if( i%4==0&&(i%100!=0||i%400==0)&&i%4000!=0 )  // if it is a leap year
                        {
                            if( first ) // match format
                                cout<<i;
                            else
                                cout<<" "<<i;
                            first=false;
                        }
                    cout<<endl;
                }
                else
                    flag2=false;

                if( !flag2 )  // b is invalid
                {
                    if( b<=0 )  // in this situation,we need to input b again.
                    {
                        cout<<"Second number is out of range!!"<<endl;
                        cout<<"Please enter second number( >0 ):";
                        continue;
                    }
                    else if( a>=b )  // in this situation, we need to input a and b again.
                    {
                        cout<<"The second number is not greater than the first number!!"<<endl;
                        break;
                    }
                }
                else
                    break;
            }
        }
        else
            flag1=false;
        if( !flag1 )  // a is invalid.
        {
            cout<<"First number is out of range!!"<<endl;
            cout<<"Please enter first number( >0 ):";
        }
        else if( !flag2 ) // a is valid,b is invalid
            cout<<"Please enter first number( >0 ):";
        else
            break;
    }
    return 0;
}
