#include <iostream>

using namespace std;

int main ()
{
    int year1 =0 , year2 =0 ;                           //宣告兩個變數
    cout << "Please enter first number( >0 ):" << endl; //輸出第一個變數
    cin >> year1 ;                                      //輸入第二個變數
    while ( year1 < 0)                                  //當第一個變數小於0,跑回圈直到大於0
    {
        cout << "First number is out of range!!";
        cin >> year1;
    }
    cout << "Please enter second number( >0 ):" << endl;//輸出第一個變數
    cin >> year2 ;                                      //輸入第二個變數
    while ( year2 < 0 )                                 //當第二個變數小於0,跑回圈直到大於0
    {
        cout << "Second number is out of range!!";
        cin >> year2;
    }
    while ( year1 > year2 )                            //當第一個數大於第二個數,跑回圈直到一一個大於第二個
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( > 0):" << endl;
        cin >> year1;
        cout << "Please enter second number( > 0):" << endl;
        cin >> year2 ;
    }
    int x =year1;
    while ( x <= year2 )
    {
        if( x%100!=0 && x%400==0)                    //當變數除以100不等於0,且除以400等於0,則輸出
        {
            cout << x << " " ;
        }
        else if (x%4==0)                            //當變數除以4等於0則輸出
        {
            cout << x << " " ;
        }
        x++;
    }

    return 0 ;
}
