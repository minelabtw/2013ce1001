#include<iostream>
using namespace std;

int main()
{
    int year1=0,year2=0;                                          //宣告變數year1,year2來接收之後輸入的兩個年份起始值設為0以進入下一行的while

    while(year2<=year1)                                           //檢查第二個年份有沒有大於第一個年份，若否或尚未輸入過(year1=year2=0)則執行以下動作
    {
        cout << "Please enter first number( >0 ):";               //輸出字串"Please enter first number( >0 ):"
        cin >> year1;                                             //將輸入的數字宣告給year1
        while(year1<=0)
        {
            cout << "First number is out of range!!\n";           //若輸入數字無大於0則輸出字串"First number is out of range!!"並重新要求輸入新數字
            cout << "Please enter first number( >0 ):";
            cin >> year1;
        }
        cout << "Please enter second number( >0 ):";              //輸出字串"Please enter second number( >0 ):"
        cin >> year2;                                             //將輸入的數字宣告給year2
        while(year2<=0)
        {
            cout << "Second number is out of range!!\n";          //若輸入數字無大於0則輸出字串"Second number is out of range!!"並重新要求輸入新數字
            cout << "Please enter second number( >0 ):";
            cin >> year2;
        }
        if(year2<=year1)                                          //若year2不大於year1則輸出字串"The second number is not greater than the first number!!"從頭來過
            cout << "The second number is not greater than the first number!!\n";
    }

    cout << "Leap years:";                                        //輸出字串"Leap years:"
    for(int i=year1,check=0 ; i<=year2 ; i++)                     //設一變數i從year1到year2一個個做以下判斷
    {
        if(i%400==0 || (i%4==0 && i%100!=0))                      //若i是閏年則做以下判斷
            if(!check)                                            //若變數check是0的話則輸出年份i，check+1(再也不會是0)
            {
                cout << i;
                check++;
            }
            else                                                  //第一個輸出的數字後，顯示的年份i前再加一格空格
            cout << " " << i;
    }

    return 0;
}
