#include <iostream>

using namespace std;

int main()
{
    int year1=0;     //宣告名稱為year1的整數變數，並初始化其數值為0。
    int year2=0;     //宣告名稱為tear2的整數變數，並初始化其數值為0。
    int i=0;         //宣告名稱為i的整數變數，並初始化其數值為0。

    while(i==0)                                                 //用i的值進行判斷，當i等於0時執行以下動作。
    {
        cout << "Please enter first number( >0 ):";             //將Please enter first number( >0 ):輸出到螢幕上。
        cin >> year1;                                           //輸入整數year1
        if(year1<0)                                             //當year1小於0時，執行以下動作。
        {
            cout << "First number is out of range!!" << endl;   //將First number is out of range!!輸出到螢幕上。
            i=0;                                                //把0丟給i
        }
        else i=1;                                               //當不符合此情況時，把1丟給i。
        while(i==1)                                                       //當i等於1時執行以下動作。
        {
            cout << "Please enter second number( >0 ):";                  //將Please enter second number( >0 ):輸出到螢幕上。
            cin >> year2;                                                 //輸入整數year2
            if(year2<=year1)                                              //當year2小於等於year1時，執行以下動作。
            {
                if(year2<0)                                               //當year2小於0時，執行以下動作。
                {
                    cout << "Second number is out of range!!" << endl;    //將First number is out of range!!輸出到螢幕上。
                    i=1;                                                  //把1丟給i
                }
                else
                {
                    cout << "The second number is not greater than the first number!!" << endl;   //將The second number is not greater than the first number!!輸出到螢幕上。
                    i=0;                                                                          //把0丟給i
                }
            }
            else i=2;                                                                             //把2丟給i
        }
    }
    cout << "Leap years:";               //將Leap years:輸出到螢幕上。
    for(int x=year1; x<=year2; x++)      //在這個迴圈中，先宣告名稱x為整數變數，並初始化其數值為year1的值，然後逐一遞增並分別執行迴圈裡的動作，直到x的值等於year2的值。
        if(x%400==0)                     //如果x除以400餘數為0時，執行以下動作。
            cout << x << " ";            //將x 輸出到螢幕上。
        else if(x%100==0);               //除非如果x除以100餘數為0時，不須輸出。
        else if(x%4==0)                  //除非如果x除以4餘數為0時，執行以下動作。
            cout << x << " ";

    return 0;
}
