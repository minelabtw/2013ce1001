#include <iostream>
using namespace std;

int main()
{
    int a=0;                                //宣告一個整數變數a
    int b=0;                                //宣告一個整數變數b

    cout<<"Please enter first number( >0 ):";
    cin>>a;
    while(a<=0)                                      //設置一個迴圈來限定a的範圍使其運作
    {
        cout<<"First number is out of range!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>a;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>b;
    while(b<=0)                                    //設置一個迴圈來限定b的範圍使其運作
    {
        cout<<"Second number is out of range!!\n";
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }
    while(b<=a&&b>0)
    {
        cout<<"The second number is not greater than the first number!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>a;
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }
    cout<<"Leap years:";
    int i=a;                           //設置一個整數i
    while (i<=b)                       //設置一個迴圈並且使其進行運算後輸出
    {
        if(i%4==0&&i%100!=0||i%400==0)
            cout<<i<<" ";
        i++;
    }

    return 0;

}


