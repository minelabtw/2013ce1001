#include <iostream>

using namespace std;

int main()
{
    int a;     //宣告名稱為a的整數變數
    int b;     //宣告名稱為b的整數變數

    cout<<"Please enter first number( >0 ):"; //將字串"Please enter first number( >0 ):"輸出到螢幕上
    cin>>a;                                   //從鍵盤輸入，並將輸入的值給a
    while(a<=0)  //使用while迴圈，若a值小於等於0就會一直重複該程式碼內的動作
    {
        cout<<"First number is out of range!!"<<endl;  //將字串"First number is out of range!!"輸出到螢幕上並且換行
        cout<<"Please enter first number( >0 ):";      //將字串"Please enter first number( >0 ):"輸出到螢幕上
        cin>>a;                                        //從鍵盤輸入，並將輸入的值給a
    }

    cout<<"Please enter second number( >0 ):"; //將字串"Please enter second number( >0 ):"輸出到螢幕上
    cin>>b;                                    //從鍵盤輸入，並將輸入的值給b
    while(b<=0)  //使用while迴圈，若b值小於等於0就會一直重複該程式碼內的動作
    {
        cout<<"Second number is out of range!!"<<endl; //將字串"Second number is out of range!!"輸出到螢幕上並且換行
        cout<<"Please enter second number( >0 ):";     //將字串"Please enter second number( >0 ):"輸出到螢幕上
        cin>>b;                                        //從鍵盤輸入，並將輸入的值給b
    }

    while(a>=b)  //使用while迴圈，若a值大於等於b就會一直重複該程式碼內的動作
    {
        cout<<"The second number is not greater than the first number!!"<<endl; //將字串"The second number is not greater than the first number!!"輸出到螢幕上並且換行
        cout<<"Please enter first number( >0 ):";                               //將字串"Please enter first number( >0 ):"輸出到螢幕上
        cin>>a;                                                                 //從鍵盤輸入，並將輸入的值給a
        while(a<=0)  //使用while迴圈，若a值小於等於0就會一直重複該程式碼內的動作
        {
            cout<<"First number is out of range!!"<<endl;  //將字串"First number is out of range!!"輸出到螢幕上並且換行
            cout<<"Please enter first number( >0 ):";      //將字串"Please enter first number( >0 ):"輸出到螢幕上
            cin>>a;                                        //從鍵盤輸入，並將輸入的值給a
        }

        cout<<"Please enter second number( >0 ):";     //將字串"Please enter second number( >0 ):"輸出到螢幕上
        cin>>b;                                        //從鍵盤輸入，並將輸入的值給b
        while(b<=0)  //使用while迴圈，若b值小於等於0就會一直重複該程式碼內的動作
        {
            cout<<"Second number is out of range!!"<<endl; //將字串"Second number is out of range!!"輸出到螢幕上並且換行
            cout<<"Please enter second number( >0 ):";     //將字串"Please enter second number( >0 ):"輸出到螢幕上
            cin>>b;                                        //從鍵盤輸入，並將輸入的值給b
        }
    }

    cout<<"Leap years:";  //將字串"Leap years:"輸出到螢幕上
    while(a<=b)  //使用while迴圈，若a值小於等於b就會一直重複該程式碼內的動作
    {
        if(a%4==0&&a%4000!=0)  //使用if-else if判斷閏年
        {
            if(a%400==0)
            {
                cout<<a<<" ";  //將字串"a "輸出到螢幕上
            }
            else if(a%100!=0)
            {
                cout<<a<<" ";  //將字串"a "輸出到螢幕上
            }
        }
        a++;  //a遞增，公差1
    }
    return 0;
}
