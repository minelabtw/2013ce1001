#include <iostream>
#include <string>
#include <vector>
using namespace std;
//函式判斷是否為閏年
bool is_leap_year(int n)
{
    if(n%4000 == 0)
        return false;
    else if(n%400 == 0)
        return true;
    else if(n%4 == 0&&n%100 != 0)
        return true;
    return false;
}
int main()
{
    ios::sync_with_stdio(false);
    //宣告一字串陣列存放輸出的字串
    string str[6] = {"Please enter ",
                     "first number( >0 ):",
                     "First number is out of range!!",
                     "second number( >0 ):",
                     "Second number is out of range!!",
                     "The second number is not greater than the first number!!"
                    };
    int firstnumber;
    int secondnumber;
    //宣告一布林變數控制輸出的空格
    bool first = false;
    //用來存放答案的vector
    vector<int>leap;
    //迴圈處理兩年份的輸入
    do
    {
        cout<<str[0]<<str[1];
        cin>>firstnumber;
        while(firstnumber <= 0)
        {
            cout<<str[2]<<endl;
            cout<<str[0]<<str[1];
            cin>>firstnumber;
        }
        cout<<str[0]<<str[3];
        cin>>secondnumber;
        while(secondnumber <= 0)
        {
            cout<<str[4]<<endl;
            cout<<str[0]<<str[3];
            cin>>secondnumber;
        }
        if(firstnumber >= secondnumber)
            cout<<str[5]<<endl;
    }
    while(firstnumber >= secondnumber);
    //迴圈跑所有年份再判斷是否為閏年丟進leap中
    for(int i = firstnumber; i <= secondnumber; ++i)
        if(is_leap_year(i))
            leap.push_back(i);
    //由leap內元素數量決定輸出單複數
    if(leap.size()<=1)
        cout<<"Leap year:";
    else
        cout<<"Leap years:";
    for(size_t i=0; i<leap.size(); ++i)
    {
        if(first)
            cout<<" ";
        else
            first = true;
        cout<<leap[i];
    }
    return 0;
}
