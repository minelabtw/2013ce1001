/*閏平年判斷
請寫一個程式可以供使用者輸入兩個整數，
還要可以判別使用者輸入的數字大於0，
且第一個整數小於第二個整數，若不滿足則要重新輸入，
最後輸出一開始輸入的兩個整數區間有哪些年是屬於閏年，
hint:用西元年去判斷。
1.西元年份除以400可整除，為閏年。
2.西元年份除以4可整除並且除以100不可整除，為閏年。
3.西元年份除以4不可整除，為平年。
4.西元年份除以100可整除並且除以400不可整除， 為平年。
例如：公元1992、1996 年等為4的倍數，故為閏年；公元1800、1900、2100年為100的倍數，當年不閏；公元1600、2000、2400年為400的倍數，有閏；而公元4000、8000年為4000的倍數，則不閏。*/


#include<iostream>
using namespace std;

int main()
{
    int num1=0;         //宣告一個整數並把0給他
    int num2=0;
    int leapyears=0;

    while (num2<=num1)          //當 num2小於num1就會迴圈
    {
        num1=0;                 //再把0給num1 & num2, 讓 num1 & num2 可以重新從鍵盤輸入
        num2=0;

        while(num1<=0 )         // 當 num1小於等於0 就迴圈
        {
            cout << "please enter first number ( >0 ): ";     //將字串印出
            cin >> num1;                                //從鍵盤輸入

            if(num1<=0)                                 // 如果小於等於0 就輸出字串
            {
                cout << "First number is out of range" << endl;
            }
        }
        while (num2<=0)
        {
            cout << "please enter sencond number ( >0 ): " ;
            cin >> num2;

            if (num2<=0)
            {
                cout << "Sencond numbers is out of range" << endl;
            }
        }

        if(num2<=num1)
        {
            cout << "Sencond number greater than the first number" << endl;
        }

        cout << "Leap years:" ;

        for(int i=num1; i<=num2; i++)       // for迴圈, 宣告一個整數並把num1的直給他, 判斷從num1到num2, 並加1
        {
            if(i%400==0)                    //如果除以400可整除就
            {
                leapyears = i;              // 把那個數字給leapyears
                cout  << leapyears << " " ;
            }
            if(i%4==0 && i%100!=0)          // 如果除以4可整除並除以100不可整除 就
            {
                leapyears = i;              // 將那一年給leapyears
                cout << leapyears << " " ;
            }
            if (i%4!=0)                     // 如果除以4不可整除
            {
                continue;                   // 繼續對最近的迴圈
            }
            if (i%100==0 && i%400!=0)
            {
                continue;
            }

        }

    }
    return 0;
}
