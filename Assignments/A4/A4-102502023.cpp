#include <iostream>
using namespace std;

int main()
{
    int a=0,b=0; // initialize integer a and b
    cout << "Please enter first number( >0 ):";
    cin >> a;
    while ( a <= 0 ) // while loop to check whether a is in demand
    {
        cout << "First number is out of range!!\n";
        cout << "Please enter first number( >0 ):";
        cin >> a;
    }
    cout << "Please enter second number( >0 ):";
    cin >> b;
    while ( b <= 0 ) // while loop to check whether b is in demand
    {
        cout << "Second number is out of range!!\n";
        cout << "Please enter second number( >0 ):";
        cin >> b;
    }
    while ( a > b) // while loop to examine wehther a is bigger than b
    {
        cout << "The first number is greater than the second number!!\n";
        cout << "Please enter first number( >0 ):";
        cin >> a;
        while ( a <= 0 )
        {
            cout << "First number is out of range!!\n";
            cout << "Please enter first number( >0 ):";
            cin >> a;
        }
        cout << "Please enter second number( >0 ):";
        cin >> b;
        while ( b <= 0 )
        {
            cout << "Second number is out of range!!\n";
            cout << "Please enter second number( >0 ):";
            cin >> b;
        }
    }
    cout << "Leap years:";
    for ( int number = a; number <= b; number++) // for loop to out the the leap years
    {
        if (number%400==0)
        {
            cout << number << " ";
        }
        else if ( number%4==0 && number%100!=0)
        {
            cout << number << " ";
        }
        else
        {

        }
    }
    return 0;
}
