#include<iostream>

using namespace std;

int main()
{
    int a=0;//宣告第一個整數變數
    int b=0;//宣告第二個整數變數

    cout <<"Please enter first number( >0 ):";
    cin  >>a;
    while(a<=0)//當a小於等於零的時候 產生重新輸入的迴圈
    {
        cout <<"First number is out of range!!\n";
        cout <<"Please enter first number( >0 ):";
        cin  >>a;
    }


    cout <<"Please enter second number( >0 ):";
    cin  >>b;
    while(b<=0)//當b小於等於零的時候 產生重新輸入的迴圈
    {
        cout <<"second number is out of range!!\n";
        cout <<"Please enter second number( >0 ):";
        cin  >>b;
    }


    while(b<=a)//當b小於等於a的時候 產生全部重新輸入的迴圈
    {
        cout <<"The second number is not greater than the first number!!\n";
        cout <<"Please enter first number( >0 ):";
        cin  >>a;
        while(a<=0)
        {
            cout <<"First number is out of range!!\n";
            cout <<"Please enter first number( >0 ):";
            cin  >>a;
        }
        cout <<"Please enter second number( >0 ):";
        cin  >>b;
        while(b<=0)
        {
            cout <<"second number is out of range!!\n";
            cout <<"Please enter second number( >0 ):";
            cin  >>b;
        }

    }

    cout <<"Leap years:";
    for(int x=a; x<=b; x++)//(宣告整數變數x=a，當x小於等於b的時候，執行下面的動作，執行後x+1)的迴圈
        if((x%4==0 && x%100!=0) || (x%400==0 && x%4000!=0) )//4的倍數但不是100的倍數，X要印出;400的倍數但不是4000的倍倍數，x要印出
        {
            cout <<x<<" ";
        }

    return 0;
}
