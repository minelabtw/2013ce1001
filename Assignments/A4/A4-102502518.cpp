#include <iostream>

using namespace std;

int main()
{
    int first_number = 0;                                        //宣告型別為整數(int)的first_number，並初始化其數值為0。
    int second_number = 0;                                       //宣告型別為整數(int)的second_number，並初始化其數值為0。

    cout << "Please enter first number( >0 ):";                  //將字串"Please enter first number( >0 ):"輸出到螢幕上。
    cin >> first_number ;                                        //從鍵盤輸入，並將輸入的值給first_number。

    while ( first_number <= 0 )                                  //使用while迴圈(當first_number <= 0)。
    {
        cout << "First number is out of range!!" << endl;        //將字串"First number is out of range!!"輸出到螢幕上。
        cout << "Please enter first number( >0 ):";              //將字串"Please enter first number( >0 ):"輸出到螢幕上。
        cin >> first_number ;                                    //從鍵盤輸入，並將輸入的值給first_number。
    }

    cout << "Please enter second number( >0 ):";                 //將字串"Please enter second number( >0 ):"輸出到螢幕上。
    cin >> second_number ;                                       //從鍵盤輸入，並將輸入的值給second_number。

    while ( second_number <= 0 )                                 //使用while迴圈(當second_number <= 0)。
    {
        cout << "Second number is out of range!!" << endl;       //將字串"Second number is out of range!!"輸出到螢幕上。
        cout << "Please enter second number( >0 ):";             //將字串"Please enter second number( >0 ):"輸出到螢幕上。
        cin >> second_number ;                                   //從鍵盤輸入，並將輸入的值給second_number。
    }
    while ( second_number <= first_number )                      //使用while迴圈(當second_number <= first_number)。
    {
        cout << "The second number is not greater than the first number!!" << endl;    //將字串"The second number is not greater than the first number!!"輸出到螢幕上。

        cout << "Please enter first number( >0 ):";              //將字串"Please enter first number( >0 ):"輸出到螢幕上。
        cin >> first_number ;                                    //從鍵盤輸入，並將輸入的值給first_number。

        while ( first_number <= 0 )                              //使用while迴圈(當first_number <= 0)。
        {
            cout << "First number is out of range!!" << endl;    //將字串"First number is out of range!!"輸出到螢幕上。
            cout << "Please enter first number( >0 ):";          //將字串"Please enter first number( >0 ):"輸出到螢幕上。
            cin >> first_number ;                                //從鍵盤輸入，並將輸入的值給first_number。
        }

        cout << "Please enter second number( >0 ):";             //將字串"Please enter second number( >0 ):"輸出到螢幕上。
        cin >> second_number ;                                   //從鍵盤輸入，並將輸入的值給second_number。

        while ( second_number <= 0 )                             //使用while迴圈(當second_number <= 0)。
        {
            cout << "Second number is out of range!!" << endl;   //將字串"Second number is out of range!!"輸出到螢幕上。
            cout << "Please enter second number( >0 ):";         //將字串"Please enter second number( >0 ):"輸出到螢幕上。
            cin >> second_number ;                               //從鍵盤輸入，並將輸入的值給second_number。
        }
    }
    cout << "Leap years:";                                       //將字串"Leap years:"輸出到螢幕上。

    while (first_number <= second_number)                        //使用while迴圈(當first_number <= second_number)。
    {
        if (first_number%400 == 0)                               //假使first_number除以400之餘數為0。
            cout << first_number <<" ";                          //將first_number與空白輸出到螢幕上。

        else if (first_number%100 == 0)                          //假使first_number除以100之餘數為0。
            cout <<"";                                           //不顯示。

        else if (first_number%4 == 0)                            //假使first_number除以4之餘數為0。
            cout << first_number <<" ";                          //將first_number與空白輸出到螢幕上。

        first_number++;                                          //將first_number加一並存回first_number。
    }
    return 0;

}
