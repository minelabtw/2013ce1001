#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int fnum , snum;

	while(true)
	{
		static int check = 0;

		while(true)
		{
			cout << "Please enter first number( >0 ):";
			cin >> fnum;
			if(fnum > 0)
				break;
			cout << "First number is out of range!!" << endl;
		}

		while(true)
		{
			cout << "Please enter second number( >0 ):";
			cin >> snum;
			if(snum <= 0)
				cout << "Second number is out of range!!" << endl;
			else if(snum <= fnum)
			{
				cout << "The second number is not greater than the first number!!" << endl;
				break;
			}
			else
			{
				check = 1;
				break;
			}
		}

		if(check == 1)
			break;
	}

	cout << "Leap years:";
	for(int i = fnum ; i <= snum ; ++i) // for loop to judge if leap year
	{
		if(i % 4 == 0)
		{
			if(i % 100 ==0)
			{
				if(i % 400 == 0)
				{
					if(i % 4000 != 0)
						cout << i << " ";
				}
			}
			else
				cout << i << " ";
		}
	}

	return 0;
}
