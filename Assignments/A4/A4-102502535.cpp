#include <iostream>

using namespace std;

int main()
{
    int year1 ;  //設一整數變數為year1。
    int year2 ;  //設一整數變數為year2。

    cout << "Please enter first number( >0 ):" ;
    cin >> year1 ;  //使操作者輸入數值。

    while( year1 <= 0 )  //以while限制輸入之數值大於零；若不符規定，就要求重新輸入。
    {
        cout << "First number is out of range!!\n" << "Please enter first number( >0 ):" ;
        cin >> year1 ;
    }

    cout << "Please enter second number( >0 ):" ;
    cin >> year2 ;  //使操作者輸入數值。

    while ( year2 <= 0 )  //以while限制輸入之數值大於零；若不符規定，就要求重新輸入。
    {
        cout << "Second number is out of range!!\n" << "Please enter second number( >0 ):" ;
        cin >> year2 ;
    }
    while ( year2 <= year1 )   //以while限制輸入的第二個數字大於第一個；若不符規定，就要求重新輸入。
    {
        cout << "The second number is not greater than the first number!!\n" << "Please enter first number( >0 ):" ;
        cin >> year1 ;
        cout << "Please enter second number( >0 ):" ;
        cin >> year2 ;
    }

    int year = year1 ;  //設一變數，其初始值訂為year1。
    cout << "Leap years:" ;  //輸出"閏年年份："。

    while ( year <= year2 )  //當year小於等於year2時，執行下列指令。
    {
        if ( year %400 == 0 )
        {
            cout << year << " " ;
            year ++ ;  //若year除以400餘0時，將其數字輸出，並將year值加1，繼續其迴圈。
        }
        else if ( year %100 != 0 && year %4 == 0)
        {
            cout << year << " " ;
            year ++ ;  //若year除以100不餘0且除以4餘0時，將其數字輸出，並將year值加1，繼續其迴圈。
        }
        else
            year++ ;  //若不符合以上條件，就將year值加1，並繼續其迴圈。
    }

    return 0 ;
}
