#include <iostream>
using namespace std ;
int main()
{
    int num1 ;  //宣告一整數變數為num1
    int num2 ;  //宣告一整數變數為num2
    int x ;     //宣告一整數變數x,使其值介於num1和num2

    cout << "Please enter first number( >0 ):" ;   //將字串"Please enter first number( >0 ):"顯示於螢幕上
    cin >> num1 ;                                  //將num1輸入至電腦

    while(num1<0)                                  //當num1小於0時重新輸入num1
    {
        cout << "First number is out of range!!" << endl ;
        cout << "Please enter first number( >0 ):" ;
        cin >> num1 ;
    }

    cout << "Please enter second number( >0 ):" ;  //將字串"Please enter second number( >0 ):"顯示於螢幕上
    cin >> num2 ;

    while(num2<0)                                  //當num2小於0時重新輸入num2
    {
        cout << "second number is out of range!!" << endl ;
        cout << "Please enter second number( >0 ):" ;
        cin >> num2 ;
    }

    while(num2<=num1)                              //當num2小於num1時,從num1開始重新輸入
    {
        cout << "The second number is not greater than the first number!!" <<endl;
        cout << "Please enter first number( >0 ):" ;
        cin >> num1 ;

        while(num1<0)
        {
            cout << "First number is out of range!!" << endl ;
            cout << "Please enter first number( >0 ):" ;
            cin >> num1 ;
        }

        cout << "Please enter second number( >0 ):" ;
        cin >> num2 ;

        while(num2<0)
        {
            cout << "second number is out of range!!" ;
            cout << "Please enter second number( >0 ):" << endl ;
            cin >> num2 ;
        }
    }

    x=num1;                             //使x值等於num1
    cout << "Leap years:" ;             //將字串"Leap years:"顯示於螢幕上
    while ( x<=num2)                    //x由num1開始逐次加1,直到x等於num2時才停止
    {
        if (x%4000==0)                  //若x為4000的倍數,則不顯示
            cout << "" ;
        else if ( x%400 == 0 )          //若x為400的倍數,則將其值顯示於螢幕上
            cout << "" << x << " " ;
        else if ( x%100==0 )            //若x為100的倍數,則不顯示
            cout << "" ;
        else if(x%4==0)                 //若x為4的倍數,則將其值顯示於螢幕上
            cout << "" << x << " " ;
        x++;
    }
    return 0;
}
