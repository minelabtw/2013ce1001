#include <iostream>
using namespace std;
main()
{
    int a=0;
    int b=0; //宣告2整數a,b，將值定為0
    while(b<=a) //b<=a時，執行迴圈
    {
        a=0;
        b=0; //將a,b定為0，以將上次輸入值洗掉
        while(a<=0) //a<=0時，執行迴圈
        {
            cout << "Please enter first number( >0 ):"; //輸出文字
            cin >> a; //輸入a值
            if(a<=0) //如果a<=0時，執行
                cout << "First number is out of range!!\n"; //輸出文字
        }
        while(b<=0) //b<=0時，執行迴圈
        {
            cout << "Please enter second number( >0 ):"; //輸出文字
            cin >> b; //輸入b值
            if(b<=0) //如果b<=0時，執行
                cout << "Second number is out of range!!\n"; //輸出文字
        }
        if(b<=a) //如果b<=a時，執行
        cout << "The second number is not greater than the first number!!\n"; //輸出文字
    }
    cout << "Leap years:"; //輸出文字
    while(a<=b) //當a<=b時，執行迴圈
    {
        if((a%4000!=0 && a%400==0) || (a%100!=0 && a%4==0)) //如果"a除以4000的餘數不為0，且a除以400的餘數為0"或"a除以100的餘數不為0，且a除以4的餘數為0"時，執行
            cout << a << " "; //輸出a值與空格
        a++; //a值增加1
    }
    return 0;
}
