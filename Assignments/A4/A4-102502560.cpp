#include <iostream>
using namespace std;

int main()
{
	int startyear, endyear;

	while(1){
		cout << "Please enter first number( >0 ):"; cin >> startyear;
		while(startyear<=0){
			//warn the user and re-input if startyear is not in range
			cout << "First number is out of range!!\n";
			cout << "Please enter first number( >0 ):"; cin >> startyear;
		}

		cout << "Please enter second number( >0 ):"; cin >> endyear;
		while(endyear<=0){
			//warn the user and re-input if endyear is not in range
			cout << "Second number is out of range!!\n";
			cout << "Please enter second number( >0 ):"; cin >> endyear;
		}

		if(endyear<=startyear){
			//warn the user and redo the loop if endyear is not greater than firstyear
			cout << "The second number is not greater than the first number!!\n";
		}else{
			//jump out of the loop if the numbers are appropriate
			break;
		}
	}

	cout << "Leap years:";
	string space="";												//the first number doesn't need a space ahead
	for(int year=startyear ; year<=endyear ; year++){				//for each year from startyear to endyear:
		if(year%4){}												//not a leap year
		else if(year%4000==0){}										//not a leap year
		else if(year%400==0){cout << space << year; space=" ";}		//print the leap year, turn on the space
		else if(year%100==0){}										//not a leap year
		else if(year%4==0){cout << space << year; space=" ";}		//print the leap year, turn on the space
	}

	return 0;
}
