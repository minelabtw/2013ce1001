#include<iostream>
using namespace std;

int main()
{
    int num1=0,num2=0,x;

    while ( num1<=0 ) //利用while迴圈使num1的值大於0，若不在範圍內，則顯示First number is out of range!!，並要求重新輸入num1。
    {
        cout << "Please enter first number( >0 ):";
        cin >> num1;
        if ( num1<=0 )
            cout << "First number is out of range!!" << endl;
    }

    while ( num2<=0||num2<=num1 ) //利用while迴圈使num2的值大於0且num2>num1，若不在範圍內，則顯示"First number is out of range!!"/"The second number is not greater than the first number!!" ，並要求重新輸入num1。
    {
        cout << "Please enter second number( >0 ):";
        cin >> num2;
        if ( num2<=0 )
            cout << "Second number is out of range!!" << endl;
        if ( num2<=num1 )
            cout << "The second number is not greater than the first number!!" << endl;
    }

    cout<<"Leap years: "; //顯示Leap years:


    for ( x=num1; x<=num2; x++ ) //利用for迴圈設定起始值及終止值，並在每次重新執行迴圈時使x值+1
    {
        if ( x%400==0&&x%4000!=0 )  //當x值為400的倍數且不是4000的倍數為閏年(判斷大於400的數字)
        {
            cout << x << "  "; //輸出符合條件的x值，並使每個x值中間加入空白
        }
        else if ( x%4==0&&x%100!=0 ) //若不符合上述if的條件，則執行else if；當x值可被4整除，但不被100整除時為閏年(判斷小於400的數字)
        {
            cout << x << "  "; //輸出符合條件的x值，並使每個x值中間加入空白
        }
    }
    return 0;
}
