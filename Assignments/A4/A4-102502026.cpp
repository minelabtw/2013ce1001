//A4-102502026
//Leap years

#include <iostream>
using namespace std;

int main()  //start program
{
    int Fn=0;   //define the first number
    int Sn=0;   //define the second number
    int R=0;    //definde the residue of division

    cout<<"Please enter first number ( >0 ) :"; //ask the first number
    cin>>Fn;

    while (Fn<=0)    //do it until Fn is greater than 0
    {
        cout<<"First number is out of range!!\n";   //wrong
        cout<<"Please enter first number ( >0 ) :"; //ask again Fn
        cin>>Fn;
    }

    cout<<"Please enter second number ( >0 ) :";    //ask the second number
    cin>>Sn;

    while (Sn<=0 or Sn<=Fn)  //do it until Sn is greater than 0 or Sc is greater than Fn
    {
          if (Sn<=0) //if Sn is lower than 0
    {
        cout<<"Second number is out of range!!\n";  //wrong
        cout<<"Please enter second number ( >0 ) :";    //ask again Sn
        cin>>Sn;

    }
        else if (Sn<=Fn) //if Sn is lower than 0
        {
            cout<<"The second number is not greater than the first number!!\n"; //respond is not greater than Fn
            cout<<"Please enter first number ( >0 ) :"; //so return to ask first number
            cin>>Fn;

            while (Fn<=0)    //do it until Fn is greater than 0
            {
                cout<<"First number is out of range!!\n";   //wrong
                cout<<"Please enter first number ( >0 ) :"; //ask again Fn
                cin>>Fn;
            }

            cout<<"Please enter second number ( >0 ) :"; //ask again Sn
            cin>>Sn;
        }
    }

cout<<"Leap years: ";   //It says Leap Years:
    while (Fn<=Sn)      //Do it until the first number is equal to Sn
    {
        if(R=Fn % 400, R==0)    //If the Fn divided into 400 and has residue 0
            cout<<Fn<<" ";      //Print the number
        else if(R=Fn % 100==0)  //If the Fn divided into 100 and has residue 0
            cout<<"";           //Print nothing
        else if (R= Fn % 4==0)  //If the Fn divided into 4 and has residue 0
            cout<<Fn<<" ";      //Print the number
        else                    //If nothing above
            cout<"";            //Print nothing
        Fn++;                   //Fn= Fn+1
    }
    return 0;
}   //End of program
