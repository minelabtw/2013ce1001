#include <iostream>

using namespace std ;

int main ()
{
    int a = 1;                              //宣告整數變數a且初始值為1
    int b = 1;


    cout << "Please enter first number( >0 ):" ;                       //輸出要求
    cin >> a ;
    while (a <= 0)                                                     //迴圈製造例外時情況
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):" << endl ;
        cin >> a ;
    }


    cout << "Please enter second number( >0 ):" ;
    cin >> b ;
    while (b <= 0)
    {
        cout << "Second number is out of range!!" << endl ;
        cout << "Please enter second number( >0 ):" << endl;
        cin >> b ;
    }


    while (a >= b)            //第二種例外情況
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):" << endl ;
        cin >> a ;
        while (a <= 0)
        {
            cout << "First number is out of range!!"  <<endl ;
            cout << "Please enter first number( >0 ):" << endl ;
            cin >> a ;
        }

        cout << "Please enter second number( >0 ):" ;
        cin >> b ;
        while (b <= 0)
        {
            cout << "Second number is out of range!!" << endl ;
            cout << "Please enter second number( >0 ):" << endl ;
            cin >> b ;
        }

    }


    cout << "Leapyears:" ;
    for (int leap = a ; leap <= b ; leap++)                     //篩選條件符合的輸出
    {
        if (leap % 4 == 0 && leap % 100 != 0 || leap % 400 == 0 && leap % 4000 != 0 )
            cout<< leap << " " ;
    }







    return 0 ;
}
