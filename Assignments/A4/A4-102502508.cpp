#include<iostream>
using namespace std ;
int main()

{
    //本次的作業目的在於運用迴圈的概念及餘數的運算來判別出介於某兩年間的閏年
    int integer1=0 ;
    int integer2=0 ;

    cout<<"Please enter first number( >0 ):" ;
    cin >>integer1 ;

    while(integer1<=0)

    {
        cout<< "First number is out of range!!"<<endl ;
        cout<<"Please enter first number( >0 ):" ;
        cin >> integer1 ;
    }

    cout<<"Please enter second number( >0 ):" ;
    cin >>integer2 ;
    while(integer2<0 or integer2<=integer1)
    {
        if(integer2<=0)
        {
            cout<<"Second number is out of range!!"<<endl ;
            cout<<"Please enter second number( >0 ):" ;
            cin >>integer2 ;
        }
        else if(integer2<=integer1)
        {
            cout<<"The second number is not greater than the first number!!"<<endl ;
            cout<<"Please enter first number( >0 ):" ;
            cin >>integer1 ;

            while(integer1<=0)
            {
                cout<<"First number is out of range!!"<<endl ;
                cout<<"Please enter first number( >0 ):" ;     //這裡記得要多打一次指令提醒使用者integer1必須大於0
                cin >>integer1 ;
            }
            cout<<"Please enter second number( >0 ):" ;
            cin >>integer2 ;
        }
    }



    int x=integer1 ;
    int y=integer2 ;
    cout<<"Leap years:";
    for(x==integer1 ; x<=integer2 ; x=x+1)     //運用迴圈及餘數的概念找出兩數間的閏年

    {
        if(x%100==0)
            cout<<" " ;
        else  if(x%4==0 or x%400==0 )
            cout<<x<<" ";


    }

    return 0 ;

}

