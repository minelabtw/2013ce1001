#include<iostream>

using namespace std;

int main()
{
    int number1=0,number2=0;//宣告兩個變數

    cout << "Please enter first number( >0 ):";//輸出"Please enter first number( >0 ):"字串
    cin  >> number1;//輸入數字給number1

    while(number1<=0)//當number1小於等於0進入迴圈
    {
        cout << "First number is out of range!!" << endl;//輸出"First number is out of range!!"字串並換行
        cout << "Please enter first number( >0 ):";//輸出"Please enter first number( >0 ):"字串
        cin  >> number1;//輸入數字給number1
    }

    cout << "Please enter second number( >0 ):";//輸出"Please enter second number( >0 ):"字串
    cin  >> number2;//輸入數字給number2

    while(number2<=0)//當number2小於等於0進入迴圈
    {
        cout << "Second number is out of range!!" << endl;//輸出"Second number is out of range!!"字串並換行
        cout << "Please enter second number( >0 ):";//輸出"Please enter second number( >0 ):"字串
        cin  >> number2;//輸入數字給number2
    }

    while(number1>=number2)//當number大於等於number2時進入迴圈
    {
        cout << "The second number is not greater than the first number!!" << endl;//輸出字串"The second number is not greater than the first number!!"並換行
        cout << "Please enter first number( >0 ):";//輸出"Please enter first number( >0 ):"字串
        cin  >> number1;//輸入數字給number1

        while(number1<=0)
        {
            cout << "First number is out of range!!" << endl;//輸出"First number is out of range!!"字串並換行
            cout << "Please enter first number( >0 ):";//輸出"Please enter first number( >0 ):"字串
            cin  >> number1;//輸入數字給number1
        }

        cout << "Please enter second number( >0 ):";//輸出"Please enter second number( >0 ):"字串
        cin  >> number2;//輸入數字給number2

        while(number2<=0)
        {
            cout << "Second number is out of range!!" << endl;//輸出"Second number is out of range!!"字串並換行
            cout << "Please enter second number( >0 ):";//輸出"Please enter second number( >0 ):"字串
            cin  >> number2;//輸入數字給number2
        }
    }

    cout << "Leap years:";//輸出"Leap years:"字串

    while(number1<number2)//當number1小於number2時進入迴圈
    {
        if((number1%4==0 and number1%100!=0) or (number1%400==0))//當number1除以4餘0且除以100不餘0,或除以400餘0時
            cout << number1 << " ";//輸出number1的值和空格

        number1++;//把number1的值加一再次進入迴圈執行動作

    }


    return 0;
}
