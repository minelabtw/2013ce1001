#include <iostream>
using namespace std;

int main()
{
    int x;//給使用者輸入第一個年份 
    int y;//給使用者輸入第二個年份 
    
    cout << "Please enter first number( >0 ):";
	cin >> x;
	while(x < 0)//第一個年份不可小於零 
    {
		cout << "First number is out of range!!" << endl;
		cout << "Please enter first number( >0 ):";
		cin >> x;
	}
	
	cout << "Please enter second number( >0 ):";
	cin >> y;
    while(y < 0)//第二個年份不可小於零 
    {
		cout << "Second number is out of range!!" << endl;
		cout << "Please enter second number( >0 ):";
		cin >> y;
	}
	
	while(y < x)//第二個年份不可小於第一個年份 
	{
		cout << "The first number is not greater than the second number!!" << endl;
		cout << "Please enter first number( >0 ):";
		cin >> x;
		
		while(x < 0)
		{
			cout << "First number is out of range!!" << endl;
			cout << "Please enter first number( >0 ):";
			cin >> x;
		}
		
		cout << "Please enter second number( >0 ):";
		cin >> y;
		while(y < 0)
		{
			cout << "Second number is out of range!!" << endl;
			cout << "Please enter second number( >0 ):";
			cin >> y;
		}
	}
	
	cout << "Leap years:";//輸出閏年 
	while(x <= y)
	{
		if(x%4==0)//當年份可被4整除 
		{
            //再判定可不可以被100整除 
			if(x%100==0)//若可以被100整除 
			{
				if(x%400==0)//則判定可不可以被400整除，若可，則輸出年份 
					cout << x << " ";
			}
			else//若不可被100整除，則輸出年份
			{
			     cout << x << " ";
			}
		}
		x++;
	}
	return 0;
}
