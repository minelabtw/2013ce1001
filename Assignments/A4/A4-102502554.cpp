#include <iostream>
using namespace std;
int main()
{
    int first;//宣告變數first
    int second;//宣告變數second
    cout<<"Please enter first number( >0 ):";//輸出Please enter first number( >0 ):
    cin>>first;//輸入first
    while ( first < 0 )//設定條件first<0
    {
        cout<<"First number is out of range!!"<<endl<<"Please enter first number( >0 ):";//輸出First number is out of range!!並於換行後再輸出Please enter first number( >0 ):
        cin>>first;//再次輸入first
    }//符合條件時執行括號內的內容
    cout<<"Please enter second number( >0 ):";//輸出Please enter second number( >0 ):
    cin>>second;//輸入second
    while ( second < 0 )//設定條件second<0
    {
        cout<<"Second number is out of range!!"<<endl<<"Please enter second number( >0 ):";//輸出Second number is out of range!!並於換行後再輸出Please enter second number( >0 ):
        cin>>second;//再次輸入second
    }//符合條件時執行括號內之內容
    while ( second <= first )//設定條件second<=first
    {
        cout<<"The second number is not greater than the first number!!"<<endl<<"Please enter second number( >0 ):";//輸出The second number is not greater than the first number!!並於換行後再輸出Please enter second number( >0 ):
        cin>>second;//再次輸入second
    }//符合條件時執行括號內之內容
    cout<<"Leap years:";//輸出Leap years:
    for ( int i = first ; i <= second ; i++ ) //設置一個迴圈其變數i之初始值為first並會逐漸+1直到i=second
    {
        if ( i % 400 == 0 )//若i可被400整除
        {
            cout<<i<<" ";//輸出i並空格
        }
        else if ( i % 4== 0 && i % 100!= 0 )//若i可被4整除但不被100整除
        {
            cout<<i<<" ";//輸出i並空格
        }
        else
        {
            cout<<"";//其餘則不輸出
        }
    }
    return 0;
}
