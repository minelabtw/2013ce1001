#include <iostream>

using namespace std;
bool judge_leap(int year)//判斷閏年
{
    if(year%4000==0)
        return false;
    else if(year%400==0)
        return true;
    else if(year%4==0&&year%100!=0)
        return true;
    else if(year%100==0&&year%400!=0)
        return false;
        else
            return false;
}
int main()
{
    int first_int=0;
    int second_int=0;
    while(first_int>=second_int)
    {
        while(first_int<=0)//輸入數字1
        {
            cout << "Please enter first number( >0 ):";
            cin >> first_int;
            if(first_int<=0)
                cout << "First number is out of range!!" << endl;
        }
        while(second_int<=0||first_int>=second_int)//輸入數字2
        {
            cout << "Please enter second number( >0 ):";
            cin >> second_int;
            if(second_int<=0)
                cout << "Second number is out of range!!" << endl;
            else if(first_int>=second_int)
            {
                cout << "The second number is not greater than the first number!!" << endl;
                first_int=0;
                second_int=0;
                break;
            }
        }
    }
    while(first_int%4!=0)//判斷是否為4的倍數
    {
        ++first_int;
    }
    cout <<"Leap years:" << first_int;
    for(int i=first_int+4; i<=second_int; i+=4)//輸出閏年
    {
        if(judge_leap(i))
        {
            cout << " " << i;
        }
    }
    return 0;
}
