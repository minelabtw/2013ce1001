#include<iostream>

using namespace std ;

int main ()
{
    // 變數 a ,b 為整數
    int a, b ;

    cout << "Please enter first number( >0 ):" ;
    cin >> a ;
    //判斷 第一個數是否小於零
    while ( a < 0 )
    {
        cout <<"First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):" ;
        cin >> a ;
    }

    cout << "Please enter second number( >0 ):";
    cin >> b ;
    // 判斷 第二個數是否小於零
    while ( b < 0 )
    {
        cout <<"Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):" ;
        cin >> b ;
    }

    // 判斷第二個數是否大於第一個
    while ( b <= a )
    {
        cout <<"The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):" ;
        cin >> a ;
        cout << "Please enter second number( >0 ):" ;
        cin >> b ;
    }

    //印出
    cout <<"Leap years:" ;
    for ( int i = a  ; i < b +1 ; ++i )
    {
        if ( i%4000 != 0 && i% 400 ==0 )
        {
            cout << i ;
        }
        else if ( i%4 ==0 && i%100 !=0 )
        {
            cout << i ;
        }
        cout << " " ;
    }


    return 0 ;
}
