//請寫一個程式可以供使用者輸入兩個整數，
//還要可以判別使用者輸入的數字大於0，
//且第一個整數小於第二個整數，若不滿足則要重新輸入，
//最後輸出一開始輸入的兩個整數區間有哪些年是屬於閏年，
#include <iostream>
using namespace std;

int main()
{
    int a,b; //宣告兩個年份,形成區段

    do
    {
        cout <<"Please enter first number( >0 ):"; //如果年份a<=0則印出重新輸入的請求
        cin >>a;
        while ( a <= 0 )
        {
            cout << "First number is out of range!!\nPlease enter first number( >0 ):";
            cin >> a;
        }
        cout << "Please enter second number( >0 ):"; //如果年份b<=0則印出重新輸入的請求
        cin >> b;
        while ( b <= 0 )
        {
            cout << "Second number is out of range!!\nPlease enter second number( >0 ):";
            cin >> b;
        }
    }
    while (b<=a&&cout <<"The second number is not greater than the first number!!\n"); //後面的年份不能小於或等於前面的年份,否則重新輸入

    int x=a,leapyears; //宣告一個變數x,他的起始值是年份區段的最左邊a。並宣告閏年為leapyears

    cout <<"Leap years:";
    while (x<=b)
    {
        if((x%400==0&&x%4000!=0)||(x%4==0&&x%100!=0)) //西元年份除以400可整除or西元年份除以4可整除並且除以100不可整除or西元年份除以4000可整除者為閏年
            cout <<x<<" ";
        x++;
    }

    return 0;
}
