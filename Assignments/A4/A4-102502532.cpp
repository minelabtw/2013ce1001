#include<iostream>
using namespace std;

int main()
{
    int firstnumber =0;
    int secondnumber =0;


    cout<<"Please enter first number( >0 ):";
    cin>>firstnumber;
    while (firstnumber <= 0)                          //數字大於0
    {
        cout<<"First number is out of range!!\nPlease enter first number( >0 ):";
        cin>>firstnumber;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>secondnumber;
    while (secondnumber <= 0 )                         //數字大於0
    {
        cout<<"Second number is out of range!!\nPlease enter second number( >0 ):";
        cin>>secondnumber;
    }
    while (secondnumber <= firstnumber)                //第一個數 要小於 第二個
    {
        cout<<"The second number is not greater than the first number!!\nPlease enter first number( >0 ):";        //重新輸入
        cin>>firstnumber;
        while (firstnumber <= 0)                          //數字大於0   還要再判斷
        {
            cout<<"First number is out of range!!\nPlease enter first number( >0 ):";
            cin>>firstnumber;
        }

        cout<<"Please enter second number( >0 ):";
        cin>>secondnumber;
        while (secondnumber <= 0 )                         //數字大於0
        {
            cout<<"Second number is out of range!!\nPlease enter second number( >0 ):";
            cin>>secondnumber;
        }
    }

    cout<<"Leap years:";
    while (firstnumber <= secondnumber)           //包含輸入的兩個整數
    {
        if (firstnumber%4000 == 0)           //4000的倍數 不閏
            cout<<"";
        else if (firstnumber%400 == 0)            //400的倍數  除以400可整除  閏
            cout<<firstnumber<<" ";
        else if (firstnumber%4 == 0 and firstnumber%100 != 0 )        //除以4可整除 且 除以100不可整除  閏
            cout<<firstnumber<<" ";
        else
            cout<<"";                        //不閏
        firstnumber++;
    }

    return 0;
}
