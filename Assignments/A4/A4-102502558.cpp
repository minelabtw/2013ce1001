#include <iostream>

using namespace std;

/**************************************************/
/* *prompt means a string for information         */
/* *error means a string for shows error message  */
/* &n means to read a int without copy            */
/**************************************************/

void input(const char *prompt,const char *error,int &n)
{
    for (;;)
    {
        cout << prompt; // input an integer
        cin >> n;
        if (n > 0) // check input is valid
        {
            return;
        }
        else
        {
            cout << error << endl;
        }
    }
}

int main()
{
    int num_first=0, num_second=0; // int two num
    for(;;)
    {
        input("Please enter first number( >0 ):","First number is out of range!!",num_first);
        input("Please enter second number( >0 ):","Second number is out of range!!",num_second);

        if (num_first > num_second)
        {
            // if not valid then not break
            cout << "The second number is not greater than the first number!!" << endl;
        }
        else
        {
            // valid so we break
            break;
        }
    }

    cout << "Leap years:";
    for (int y=num_first; y<=num_second; y++) // loop year from firstnmu to secondnum
    {
        if(((y%4==0 && y%100!=0)||(y%400==0)) && y % 4000 != 0)
        {
            cout << y << " ";
        }
    }
    return 0;
}
