#include <iostream>
using namespace std;
int main()
{
    int year1 , year2;

    do                                                       //use the "do...while" loop that runs the program before testing the condition.
    {
        year1=-1 , year2=-2;

        while(year1<=0)
        {
            cout << "Please enter first number( >0 ):";
            cin >> year1;
            if(year1<=0)
                cout << "First number is out of range!!\n";
        }

        while(year2<=0)
        {
            cout << "Please enter second number( >0 ):";
            cin >> year2;
            if(year2<=0)
                cout << "Second number is out of range!!\n";
        }

        if(year1>=year2)
            cout << "The second number is not greater than the first number!!\n";
    }
    while(year1>=year2);

    cout << "Leap years:";

    for(int x=year1 ; x<=year2 ; x++)                        // use "for" loop and "if" order to output the leap years.
    {
        if(x%400==0)
            cout << x << " ";
        if(x%4==0&&x%100!=0)
            cout << x << " ";
    }

    return 0;
}
