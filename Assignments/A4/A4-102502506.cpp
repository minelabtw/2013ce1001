#include <iostream>

using namespace std;

int main()
{
    int num1;  //宣告變數
    int num2;

    cout << "Please enter first number( >0 ):";  //輸入第一個變數
    cin >> num1;

    while ( num1 <= 0 )  //變數若小於或等於0則執行下列迴圈,重新輸入變數
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";

        cin >> num1;
    }

    cout << "Please enter second number( >0 ):";  //輸入第二個變數
    cin >> num2;

    while ( num2 <= 0 )  //變數若小於或等於0則執行下列迴圈,重新輸入變數
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";

        cin >> num2;
    }

    while ( num2 <= num1 )  //判斷變數2若小於或等於變數1則執行下列迴圈,從新輸入變數一跟變數二
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> num1;

        while ( num1 <= 0 )
        {
            cout << "First number is out of range!!" << endl;
            cout << "Please enter first number( >0 ):";

            cin >> num1;
        }

        cout << "Please enter second number( >0 ):";
        cin >> num2;

        while ( num2 <= 0 )
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):";

            cin >> num2;
        }

    }
    while ( num1 <= num2 )  //若變數1小於或等於變數2則執行下列迴圈
    {
        if ( num1 % 400 ==  0 )  //判斷變數1到變數2中所有數值能被400整除的數
        {
            cout << num1 << " ";  //輸出以上判斷出的數值然後控一格
        }
        else if ( num1 % 4 == 0 && num1 % 100 != 0 )  //判斷變數1到變數2中所有數值能被4整除且不能被100整除的數
        {
            cout << num1 << " ";  //輸出以上判斷出的數值然後控一格
        }
        num1 ++;
    }
    return 0;
}

