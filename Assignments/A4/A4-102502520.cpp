#include <iostream>
using namespace std;

int main()
{
    int number1;
    int number2;

    cout<<"Please enter first number( >0 ):";
    cin>>number1;

    while (number1<=0)                                  //檢驗number1
    {
        cout<<"First number is out of range!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>number1;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>number2;

    while (number2<=0)                                 //檢驗number2
    {
        cout<<"second number is out of range!!\n";
        cout<<"Please enter second number( >0 ):";
        cin>>number2;
    }

    while (number1>=number2)                           //檢驗number1<number2
    {
        cout<<"The first number is not greater than the second number!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>number1;

        while (number1<=0)                             //檢驗重新輸入的number1
        {
            cout<<"First number is out of range!!\n";
            cout<<"Please enter First number( >0 ):";
            cin>>number1;
        }

        cout<<"Please enter second number( >0 ):";
        cin>>number2;

        while (number2<=0)                             //檢驗重新輸入的number2
        {
            cout<<"second number is out of range!!\n";
            cout<<"Please enter second number( >0 ):";
            cin>>number2;
        }
    }

    cout<<"Leap years:";                               //輸出"Leap years:"
    for(int y=number1; y<=number2; y++)                //for迴圈，重輸入之number1到輸入之number2
    {
        if (y%400==0&&y%4000!=0)                       //被400整除但不被4000整除
        {
            cout<<y<<" ";
        }
        if (y%4==0&&y%100!=0)                          //被4整除但不被100整除
        {
            cout<<y<<" ";
        }

    }

    return 0;
}
