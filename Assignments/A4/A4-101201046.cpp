#include <iostream>

using namespace std;

int main(void) {
    int n1, n2; //declare two number

    do {

        do {
            cout << "Please enter first number( >0 ):"; //input first number
            cin >> n1;
            }while(n1 <= 0 && cout << "First number is out of range!!\n");

        do {
            cout << "Please enter second number( >0 ):"; //input second number
            cin >> n2;
            }while(n2 <= 0 && cout << "Second number is out of range!!\n");

        }while(n2 <= n1 && cout << "The second number is not greater "
                                   "than the first number!!\n"); // n1 < n2

    cout << "Leap years:" ; //output leap year

    for (int i = ((n1+3)/4)*4; i <= n2; (((i%4000) && !(i%400)) || (i%100)) &&
                                        cout << i << " ", i+=4);
/*
    do the same thing:
    for (int i = ((n1+3)/4)*4; i <= n2; i+=4)
        if(((i%4000) && !(i%400)) || (i%100))cout << i << " ";
*/

    return 0;
    }
