#include <iostream>
using namespace std;
int main()
{
    int firstyear; //設firstyear為一變數

    cout << "Please enter first number( >0 ):";
    cin >> firstyear;

    while(firstyear<=0) //當firstyear小於等於0時，重複下列動作
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> firstyear;
    }

    int lastyear; //設lastyear為一變數

    cout << "Please enter second number( >0 ):";
    cin >> lastyear;

    while(lastyear<=firstyear) //當lastyear小於等於firstyear時，進入下列循環
    {
        while(lastyear<=0) //當lastyear小於等於0，進入下列動作中
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):";
            cin >> lastyear;
        }

        cout << "The first number is not greater than the second number!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >>firstyear;

        while(firstyear<=0) //當firstyear小於等於0時，重複下列動作
        {
            cout << "First number is out of range!!" << endl;
            cout << "Please enter first number( >0 ):";
            cin >> firstyear;
        }

        cout << "Please enter second number( >0 ):";
        cin >> lastyear;
    }

    int x=firstyear; //設變數x等於firstyear

    cout << "Leap years:"; //輸出Leap years:

    while(x<=lastyear) //當x小於等於lastyear時，執行下列動作
    {
        if(x%4==0&&x%100!=0||x%400==0&&x%4000!=0) //當x被4整除且不被100整除 或 x被400整除且不被4000整除時，執行下列舉動
            cout << x << " "; //當x符合上列條件時，輸出x和空格
        x++; //x持續遞增，直到x超過lastyear，跳出while
    }

    return 0;
}
