#include<iostream>
using namespace std;
int main()
{
    int number1=0;//declare a variable and initial it as 0
    int number2=0;//declare a variable and initial it as 0

    do
    {
        do
        {
            cout<<"Please enter first number( >0 ):";
            cin>>number1;
            if(number1<=0)
                cout<<"First number is out of range!!"<<endl;
        }
        while(number1<=0);//loop until the correct number is input

        do
        {
            cout<<"Please enter second number( >0 ):";
            cin>>number2;
            if(number2<=0)
                cout<<"Second number is out of range!!"<<endl;
            else if(number2<=number1)
                cout<<"The second number is not greater than the first number!!"<<endl;
        }
        while(number2<=0);//loop until the correct number is input
    }
    while(number2<=number1);
//if second number <= first number , then reinput both first and second number
    cout<<"Leap years:";
    for(int i=number1; i<=number2; i++)
    {
        if(i%4000==0)
            continue ;//if the year is a multiple of 4000 , then skip it and don't print it
        else if(i%400==0)
            cout<<i<<" ";//for i in this step must not be the multiple of 4000 , so if the year is a multiple of 400 , then print it
        else if(i%100==0)
            continue ;//for the same reason , if the year is a multiple of 100, then skip it and don't print
        else if(i%4==0)
            cout<<i<<" ";//for the same reason, if the year is a multiple of 4, then print it
        else ; // do nothing if the year is not the multiple of 4
    }//to caculate the leap year between the first and second number

    return 0;
}





