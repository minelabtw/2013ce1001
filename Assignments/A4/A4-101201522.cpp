#include <iostream>
using namespace std;

int main(){
    int counter = 0,num1,num2,year;//宣告為整數,counter處理第幾個輸入,num1、num2為第一和二的輸入,year為輸出的年 
    bool check = false;//宣告為布林,判斷是否為第一個輸出的閏年 
    
    while(counter<2){//counter為0處理第一個輸入,1處理第二個輸入 
        if(counter==0){
            cout << "Please enter first number( >0 ):";
            cin >> num1;
            if(num1<=0)//判斷是否超出範圍 
                cout << "First number is out of range!!\n";
            else
                counter++;
        }
        else{
            cout << "Please enter second number( >0 ):";
            cin >> num2;
            if(num2<=0)//判斷是否超出範圍
                cout << "Second number is out of range!!\n";
            else if(num2<=num1){//如果大小不合要求從第一個開始重新輸入 
                cout << "The second number is not greater than the first number!!\n";
                counter = 0;
            }
            else
                counter++;
        }
    }
    for(year=(num1+3)/4*4,cout << "Leap years:";year<=num2;year+=4){//輸出兩數字之間的閏年 
        if(!(year%400) || (year%100)){//判斷是否為閏年 
            if(check)//除了第一個閏年以外,其他以空格隔開 
                cout << " " <<year;
            else{//處理第一個閏年,並記錄 
                cout << year;
                check = true;
            }
        }
    }
    cout << endl;
    return 0;
}
