#include <iostream>

using namespace std;

int main()
{
    int number1=0,number2=0,x=0,year=0;                    //宣告變數

    while (x==0)                                           //迴圈(條件)
    {
        cout <<"Please enter first number( >0 ):";         //顯示字元
        cin >>number1;
        while (number1<0)                                  //迴圈(條件)
        {
            cout <<"First number is out of range!!\n" << "Please enter first number( >0 ):";
            cin >>number1;
        }
        cout <<"Please enter second number( >0 ):";
        cin >>number2;

        while (number2<0)                                  //while迴圈(條件)
        {
            cout <<"Second number is out of range!!\n" << "Please enter second number( >0 ):";
            cin >>number2;
        }
        if (number1>=number2)                              //if判別
        {
            cout <<"The second number is not greater than the first number!!\n";
        }
        else
        {
            x=1;
        }

    }
    cout<<"Leap years:";

    for ( int year=number1; year<=number2; year=year+1)    //for迴圈(條件)
    {
        if((year%4==0 && year%100 && year%400) || (year%4==0 && year%100!=0) )
        {
            cout <<year <<" ";

        }
    }
    cout <<endl;
    return 0;
}
