#include <iostream>

using namespace std;

int main ()
{
    //variable declartion
    int year1=0;                                                                 //the year user would like to start
    int year2=0;                                                                 //the year user would like to end

    //data year1
    cout<<"Please enter first number( >0 ):";                                    //prompt user for data
    cin>>year1;                                                                  //read the first interger from user into year1
    while(year1<=0)                                                              //while year1 is not positive interger
    {
        cout<<"First number is out of range!!\n";                                //tell user the data is out of range
        cout<<"Please enter first number( >0 ):";                                //prompt user for data again
        cin>>year1;                                                              //read the first interger from user into year1 again
    }

    //data year2
    cout<<"Please enter second number( >0 ):";                                   //prompt user for data
    cin>>year2;                                                                  //read the second interger from user into year2
    while(year2<= 0||year2<=year1)                                               //while year2 is not positive interger or year2 is not greater than year1
    {
        if(year2<=0)                                                             //if year2 is not positive interger
        {
            cout<<"Second number is out of range!!\n";                           //tell user the data is out of range
            cout<<"Please enter second number( >0 ):";                            //prompt user for data again
            cin>>year2;                                                          //read the second interger from user into year2 again
        }
        else                                                                     //else if year2 is not greater than year1
        {
            cout<<"The second number is not greater than the first number!!\n";  //tell user year2 must greater than year1
            //data year1 again
            cout<<"Please enter first number( >0 ):";                            //prompt user for data
            cin>>year1;                                                          //read the first interger from user into year1
            while(year1<=0)                                                      //while year1 is not positive interger
            {
                cout<<"First number is out of range!!\n";                        //tell user the data is out of range
                cout<<"Please enter first number( >0 ):";                        //prompt user for data again
                cin>>year1;                                                      //read the first interger from user into year1 again
            }
            //data year2 again
            cout<<"Please enter second number( >0 ):";                           //prompt user for data again
            cin>>year2;                                                          //read the second interger from user into year2 again
        }
    }

    cout<<"Leap years:";                                                         //start the result
    for(int a=year1; a<=year2; a=a+1)                                            //run the loop from year1 to year2
    {
        if(a%4000==0)                                                            //if a mod 4000 is 0, that is, a is not leap year
            a=a;                                                                 //no program should run
        else if(a%400==0)                                                        //else if a mod 400 is 0, that is, a is leap year
            cout<<a<<" ";                                                        //print the year and leave a blank
        else if(a%100==0)                                                        //else if a mod 100 is 0, that is, a is not leap year
            a=a;                                                                 //no program should run
        else if(a%4==0)                                                          //else if a mod 4 is 0, that is, a is leap year
            cout<<a<<" ";                                                        //print the year and leave a blank
        else                                                                     //else if a mod 4 is not 0, that is, a is not leap year
            a=a;                                                                 //no program should run
    }
    return 0;
}
