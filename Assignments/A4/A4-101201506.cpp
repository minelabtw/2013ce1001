#include <iostream>
using namespace std;

int main()
{
    int year1=0;                                                                //宣告整數year1，將要輸入的第一個西元年份
    int year2=0;                                                                //宣告整數year2，將要輸入的第二個西元年份

    cout<<"Please enter first number( >0 ):";                                   //輸入第一個西元年份
    cin>>year1;
    while(year1<=0)                                                             //確認year1不可以<0，若<0則重新輸入第一個年分
    {
        cout<<"First number is out of range!!"<<endl
            <<"Please enter first number( >0 ):";
        cin>>year1;
    }

    cout<<"Please enter second number( >0 ):";                                  //輸入第二個西元年份
    cin>>year2;

    while(year2<=0)                                                             //確認year2不可以<0，若<0則重新輸入第二個年份
    {
        cout<<"Second number is out of range!!"<<endl
            <<"Please enter second number( >0 ):";
        cin>>year2;
    }

    while(year1>year2)                                                          //確認year1>year2，如果沒有則重新輸入year1，year2
    {
        cout<<"The second number is not greater than the first number!!"<<endl  //再次輸入第一個西元年份
            <<"Please enter first number( >0 ):";
        cin>>year1;

        while(year1<=0)                                                         //再次確認year1不可以<0，若<0則重新輸入
        {
            cout<<"First number is out of range!!"<<endl
                <<"Please enter first number( >0 ):";
            cin>>year1;
        }

        cout<<"Please enter second number( >0 ):";                              //再次輸入第二個西元年份
        cin>>year2;

        while(year2<=0)                                                         //再次確認year2不可以<0，若<0則重新輸入
        {
            cout<<"Second number is out of range!!"<<endl
                <<"Please enter second number( >0 ):";
            cin>>year2;
        }
    }

    cout<<"Leap years:";
    for(int x=year1; x<=year2; x++)                                             //宣告整數x，起始質，條件式，更新質，判斷閏年並顯示出來
    {
        if(x%400==0)                                                            //1.西元年份除以400可整除，為閏年。
            cout<<x<<" ";

        else if(x%4==0 && x%100!=0)                                             //2.西元年份除以4可整除並且除以100不可整除，為閏年。剩下的就都是平年了
            cout<<x<<" ";
    }
    return 0;
}


