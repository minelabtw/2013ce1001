#include<iostream>
using namespace std;

int main()
{
    int a = 0;//宣告型別為int的變數a，並初始化其數值為0
    int b = 0;//宣告型別為int的變數b，並初始化其數值為0

    cout << "Please enter first number( >0 ):"; //顯示"Please enter first number( >0 ):"
    cin >> a;                                   //輸入a
    while(a < 0)                                //當a<0，進入迴圈
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";
        cin >> a;
    }   //顯示"First number is out of range!!"換行顯示"Please enter first number( >0 ):"，輸入a

    cout << "Please enter second number( >0 ):";//顯示"Please enter second number( >0 ):"
    cin >> b;                                   //數入b
    while(b < 0)                                //當b<0，進入迴圈
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):";
        cin >> b;
    }   //顯示"Second number is out of range!!"換行顯示"Please enter second number( >0 ):"，輸入b
    while(b < a)                                //當b<a，進入迴圈
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> a;
        while(a < 0)
        {
            cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";
            cin >> a;
        }

        cout << "Please enter second number( >0 ):";
        cin >> b;
        while(b < 0)
        {
            cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):";
            cin >> b;
        }
    }                               //要求重新輸入，條件和前面相同

    cout << "Leap years:";          //顯示"Leap years:"
    while(a <= b)                   //當a<=b，進入迴圈
    {
        if (a%400==0)               //如果a除以400可整除，顯示a
            cout << a << " ";
        else if (a%4==0&&a%100!=0   //如果a除以4可整除並且除以100不可整除，顯示a
            cout << a << " ";
        a++;                        //a的值一直+1
    }
    return 0;
}
