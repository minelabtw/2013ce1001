#include <iostream>

using namespace std;

int main()
{
    int a = 0; //宣告型別為int的a，並初始化其數值為0
    int b = 0; //宣告型別為int的b，並初始化其數值為0

    while (a<=0)
    {
        cout << "Please enter first number( >0 ):"; //輸入第一個數
        cin >> a;
        if (a<=0) //若不符條件則輸出First number is out of range!!
            cout << "First number is out of range!!" << endl;
    }

    while (b<=0 or b<=a)
    {
        cout << "Please enter second number( >0 ):"; //輸入第二個數
        cin >> b;
        if (b<=a and b>0) //若第二個數不大於第一個數則輸出The second number is not greater than the first number!!
            cout << "The second number is not greater than the first number!!" << endl;
        else if (b<=0) //若不符條件則輸出Second number is out of range!!
            cout << "Second number is out of range!!" << endl;
    }

    cout << "leap years:";

    while (a<=b)
    {
        if ((a%4==0 and a%100!=0) or (a%400==0)) //設定符合潤年條件的條件句
            cout << a << " ";
        a++;
    }
    return 0;
}
