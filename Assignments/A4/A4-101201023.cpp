#include <iostream>

using namespace std;

int main()
{
    int number1=0;
    int x=0;
    int number2=0;
    int y=0;
    int a=0;
    int i=0;


    cout << "Please enter first number( >0 ):";
    cin >> number1;
    while(number1<=0)                                                    //當number1不大於零時，重新輸入
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> number1;
    }
    x = number1;                                                         //當number1符合範圍時，宣告x為number1


    cout << "Please enter second number( >0 ):";
    cin >> number2;
    while(number2<=0 || number2<=x)
    {
        if(number2<=0)
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):";
            cin >> number2;
        }

        else if(number2<=x)                                              //當number2不大於x時，全部重新輸入
        {
            cout << "The second number is not greater than the first number!!" << endl;
            cout << "Please enter first number( >0 ):";
            cin >> number1;

            while(number1<=0)
            {
                cout << "First number is out of range!!" << endl;
                cout << "Please enter first number( >0 ):";
                cin >> number1;
            }
            x = number1;

            cout << "Please enter second number( >0 ):";
            cin >> number2;
        }
    }
    y = number2;


    cout << "Leap years:";
    while(x<=y)
    {
        if(x%400==i && x%4000!=i)                                        //當x為400的倍數且非4000的倍數時，輸出
        {
            cout << x << " ";
        }

        else if(x%4==i && x%100!=i)
        {
            cout << x << " ";
        }
        x++;
    }

    return 0;
}
