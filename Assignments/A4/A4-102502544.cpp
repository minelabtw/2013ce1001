#include<iostream>
using namespace std;

int main()
{
    int a=0;     //宣告型別為整數
    int b=0;     //宣告型別為整數

    cout<<"Please enter first number(>0):";
    cin>>a;
    while(a<=0)    //設定當a小於等於零時
    {
        cout<<"First number is out of range!!\nPlease enter first number(>0):";
        cin>>a;
    }
    cout<<"Please enter second number(>0):";
    cin>>b;
    while(b<=0)    //設定當b小於等於零時
    {
        cout<<"Second number is out of range!!\nPlease enter second number(>0):";
        cin>>b;
    }
    while(b<=a)    //設定當b小於等於a時
    {
        cout<<"The second number is not greater than the first number!!\nPlease enter first number(>0):";
        cin>>a;
        cout<<"Please enter second number(>0):";
        cin>>b;
    }

    cout<<"Leap years:";
    while(a<b)    //設定當a小於b時
    {
        if(a%4==0 && a%100!=0)
        {
            cout<<a<<" ";
        }
        else if(a%400==0)
        {
            cout<<a<<" ";
        }
        a++;
    }
    return 0;
}
