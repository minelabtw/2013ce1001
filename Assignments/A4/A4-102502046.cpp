#include <iostream>
using namespace std;

int main()
{
    int a=0;      //宣告整數a,且起始值為0
    int b=0;      //宣告整數b,且起始值為0

    cout << "Please enter first number( >0 ):";
    cin >> a;       //輸入整數a
    while (a<=0)    //當a<=0
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> a;   //再次輸入a
    }

    cout << "Please enter second number( >0 ):";
    cin >> b;       //輸入b
    while (b<=0)
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):" ;
        cin >> b;   //再次輸入b
    }

    while (a>=b)    //當a>=b，進入while迴圈
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> a;
        while (a<=0)
        {
            cout << "First number is out of range!!" << endl;
            cout << "Please enter first number( >0 ):" ;
            cin >> a;
        }

        cout << "Please enter second number( >0 ):";
        cin >> b;
        while (b<=0)
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):" ;
            cin >> b;
        }
    }

    cout << "Leap years:" ;     //印出"Leap years:"

    for (int i=a ; i<=b ; i++)
    {
        if ( ((i%4==0 && i%100!=0) || i%400==0)&& i%4000!=0 )  //判斷是否為閏年
            cout << i << " ";
    }
    return 0;
}
