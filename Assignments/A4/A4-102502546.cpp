#include <iostream>
using namespace std;

int main()
{
    int a=0,b=0;
    cout<<"Please enter first number( >0 ):";
    cin>>a;
    while(a<=0)//當a<=0時 再輸入一次a
    {
        cout<<"First number is out of range!!\nPlease enter first number( >0 ):";
        cin>>a;
    }
    cout<<"Please enter second number( >0 ):";
    cin>>b;
    while(b<=0)//同上
    {
        cout<<"Second number is out of range!!\nPlease enter second number( >0 ):";
        cin>>b;
    }
    while(b<=a)//此迴圈是當b沒有比a大時，全部重新輸入
    {
        cout<<"The second number is not greater than the first number!!\nPlease enter first number( >0 ):";
        cin>>a;
        while(a<=0)
        {
            cout<<"First number is out of range!!\nPlease enter first number( >0 ):";
            cin>>a;
        }
        cout<<"Please enter second number( >0 ):";
        cin>>b;
        while(b<=0)
        {
            cout << "Second number is out of range!!\nPlease enter second number( >0 ):";
            cin >> b;
        }
    }
    cout<<"Leap years:";//輸出年份
    for(int n=a ; n<=b ; n++)
    {
        if(n%4==0 && n%100!=0 || n%400 ==0 && n%4000!=0)//確定是否是潤年
        {
            cout<<n<<" ";
        }
    }
    cout<<endl;
    return 0;
}
