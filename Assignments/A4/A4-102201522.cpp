#include<iostream>
using namespace std;

int main()
{
    int f_n = 1; //先令first number(簡稱f_n)為1
    int s_n = 1; //以及second number(s_n)為一,使得第一個while判斷為True
    int year;

    while(f_n>=s_n) //若不符合題目條件(s_n<f_n),則輸入f_n,s_n二數
    {
        cout << "Please enter first number( >0 ):";
        cin >> f_n;
        while(f_n<=0) //判斷f_n是否符合條件
        {
            cout << "First number is out of range!!"<<endl;
            cout << "Please enter first number( >0 ):";
            cin >> f_n;
        }
        cout << "Please enter second number( >0 ):";
        cin >> s_n;
        while(s_n<=0) //判斷s_n是否符合條件
        {
            cout << "Second number is out of range!!"<<endl;
            cout << "Please enter second number( >0 ):";
            cin >> s_n;
        }
        if (f_n>=s_n) //若不符合題目條件(s_n<f_n),則要求重新輸入
            cout << "The second number is not greater than the first number!!"<<endl;
    }

    cout << "Leap years:";
    for (year=f_n;year<=s_n;year++) //檢查每個介於f_n和s_n的年份(year)
    {
        if (year%400==0 && year%4000!=0) //若整除於400,且不整除4000,則此年為閏年
            cout << year << " ";
        else if (year%4==0 && year%100!=0 && year%4000!=0) //若整除於4,且不整除100和4000,則此年為閏年
            cout << year << " ";
    }
    return 0;
}
