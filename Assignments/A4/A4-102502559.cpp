#include<iostream>
using namespace std;

int main()
{
    int a,b,c=1 ; //宣告三個變數
    cout<<"Please enter first number( >0 ):" ; //顯示Please enter first number( >0 ):
    cin>>a;//輸入的值配給變數a
    while(a<=0) //當a的值小於或等於0,進入迴圈
    {
        cout<<"First number is out of range!!" <<endl; //顯示First number is out of range!!
        cout<<"Please enter first number( >0 ):";//顯示Please enter first number( >0 ):
        cin>>a;
    }
    cout<<"Please enter second number( >0 ):";//顯示Please enter second number( >0 ):
    cin>>b;//將輸入的值配給變數b
    while(b<=a) //當b小於或等於a,進入迴圈
    {
        if(b<=0) //如果b小於或等於0
        {
            cout<<"Second number is out of range!!"<<endl; //顯示Second number is out of range!!
            cout<<"Please enter second number( >0 ):";//顯示Please enter second number( >0 ):
            cin>>b;//將輸入值配給b
        }
        else
        {
            cout<<"The second number is not greater than the first number!!"<<endl;//顯示The second number is not greater than the first number!!
            cout<<"Please enter second number( >0 ):";//顯示Please enter second number( >0 ):
            cin>>b;
        }
    }
    cout<<"Leap years:";//顯示Leap years:
    while(c<=b)//當c小於或等於b時,進入迴圈
    {
        if(c%400==0&&c<=b) //如果c除以400餘0和c小於或等於b
        {
            cout<<c<<" ";//顯示c數值和空格
        }
        else if(c%100!=0 && c%4==0&&c<=b)//如果c除以100餘數不等於0和除以4餘數等於0和c小於或等於b
        {
            cout<<c<<" ";//顯示c數值和空格
        }
        c++;//c值加一
    }
    return 0 ;
}

