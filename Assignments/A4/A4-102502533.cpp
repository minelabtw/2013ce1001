#include<iostream>
using namespace std;

int main()
{
    int first_number;//宣告第一個年數
    int second_number;//宣告第二個年數

    cout<<"Please enter first number( >0 ):";//要求輸入第一個數
    cin>>first_number;

    while( first_number<=0)//限制第一個數需大於0,否則重新輸入
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>first_number;
    }

    cout<<"Please enter second number( >0 ):";//要求輸入第二個數
    cin>>second_number;

    while( second_number<=0)//限制第二個數需大於0,否則重新輸入
    {
        cout<<"Second number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>second_number;
    }

    while(second_number<first_number)//限制第1數需小於第二數,否則全部重新輸入
    {
        cout<<"The second number is not greater than the first number!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>first_number;

        while( first_number<=0)
        {
            cout<<"First number is out of range!!"<<endl;
            cout<<"Please enter first number( >0 ):";
            cin>>first_number;
        }

        cout<<"Please enter second number( >0 ):";
        cin>>second_number;

        while( second_number<=0)
        {
            cout<<"Second number is out of range!!"<<endl;
            cout<<"Please enter second number( >0 ):";
            cin>>second_number;
        }
    }

    cout<<"leap year:";//顯示閏年

    while( first_number<=second_number)
    {

        if(first_number%4==0  && first_number%100!=0 || first_number%400==0 )//限制閏年的條件:整除4,不整除100,整除400
        {
            if(first_number%4000!=0)//不整除4000
            {


                cout<<first_number<<" ";
                first_number++;
            }
            first_number++;
        }
        else
        {
            first_number++;
        }
    }
    return 0;
}


