#include <iostream>

using namespace std;

int main()
{
    int number1;                                                            //宣告整數變數
    int number2;                                                            //宣告整數變數




    do                                                                      //執行下列動作
    {
        cout << "Please enter first number( >0 ):" ;                        //顯示字串
        cin  >> number1 ;                                                   //將輸入值給number1
        while (number1 <= 0 )                                               //設定迴圈(確認輸入值在題目範圍內)，符合條件時執行下列動作
        {
            cout << "First number is out of range!!" << endl;               //顯示字串
            cout << "Please enter first number( >0 ):" ;                    //顯示字串
            cin  >> number1 ;                                               //將輸入的值給number1
        }


        cout << "Please enter second number( >0 ):" ;                       //顯示字串
        cin  >> number2;                                                    //將輸入的值給number2

        while (number2 <=0)                                                 //設定迴圈(確認輸入值在題目範圍內)，並符合條件時執行下列動作
        {
            cout << "Second number is out of range!!" << endl;              //顯示字串
            cout << "Please enter second number( >0 ):" ;                   //顯示字串
            cin  >> number2 ;                                               //將輸入的值給number2
        }

        if (number2 <= number1)                                             //設定假設條件，符合時執行下列動作
        {
            cout << "The second number is not greater than the first number!!" << endl; //顯示字串
        }

    }
    while (number2 <= number1) ;                                            //設定迴圈(確認輸入值符合題目範圍內)，當條件符合時，執行do


    cout << "Leap years:" ;                                                 //顯示字串

    while (number1 <= number2)                                              //設定迴圈(讓值重複在某一範圍)
    {
        if (0 == number1 % 400 && 0 != number1 % 4000)                      //設定條件(閏年條件)
            cout << number1 << " " ;                                        //顯示數字
        else if (0 == number1 % 4 && 0 != number1 % 100)                    //設定條件(閏年條件)
            cout << number1 << " " ;                                        //顯示數字

        number1 = number1 +1;                                               //將輸入的值+1並重複執行動作，直到條件不符
    }


    return 0;
}
