#include <iostream>

using namespace std;

int main()
{
    int year1 ;
    int year2 = 0 ;

    cout << "Please enter first number( >0 ):" ;
    cin >> year1;

    while ( year1 < 1 || year2 <= year1)
    {
        if ( year1 < 1 )    // year1 範圍限制
        {
            cout << "First number is out of range!!\n" << "Please enter first number( >0 ):" ;
            cin >> year1;
        }

        cout << "Please enter second number( >0 ):" ;
        cin >> year2;

        while ( year2 <= year1 )    // year2 範圍限制
        {
            if ( year2 < 1)
            {
                cout << "Second number is out of range!!\n" << "Please enter second number( >0 ):" ;
                cin >> year2;
            }
            else
            {
                cout << "The second number is not greater than the first number!!\n" << "Please enter first number( >0 ):" ;
                year2 = 0;
                cin >> year1;
            }
        }

    }

    cout << "Leap years:" ;

    for ( int i = ( year1 + year1 % 4 ) ; i <= year2 ; i += 4)  //宣告變數 i 並辨別是否閏年
    {
        if ( i % 4 == 0 && i % 100 != 0 || i % 400 == 0 )
            cout << i << " " ;
    }

    return 0;
}
