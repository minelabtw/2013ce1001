#include<iostream>
using namespace std;

int main()
{
    int x=1,y=0;                                  //宣告兩個變數分別為年份的上限與下限

    cout << "Please enter first number( >0 ):";   //要求使用者輸入下限年分
    cin >> x;

    while (x<=0)                                  //當下限年分小於等於零時要求使用者重新輸入
    {
        cout << "First number is out of range!!" <<endl;
        cout << "Please enter first number( >0 ):";
        cin >> x;
    }

    cout << "Please enter second number( >0 ):";  //要求使用者輸入上限年分
    cin >> y;

    while (y<=0)                                  //當上限年分小於等於零時要求使用者重新輸入
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> y;
    }

    while(x>=y)                                   //當下限年分大於等於上限年份時要求使用者重新輸入
    {
        cout << "The first number is not greater than the second number!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> x;

        while (x<=0)
        {
            cout << "First number is out of range!!" <<endl;
            cout << "Please enter first number( >0 ):";
            cin >> x;
        }

        cout << "Please enter second number( >0 ):";
        cin >> y;

        while (y<=0)
        {
            cout << "Second number is out of range!!" <<endl;
            cout << "Please enter second number( >0 ):";
            cin >> y;
        }
    }
    cout << "Leap years:" ;                     //輸出字串

    for (; x<=y;)                               //逐一檢查年分,若符合條件則印出該年分
    {
        if (x%400==0)
        {
            cout << x << " " ;
            x=x+1;
        }

        if(x%4==0 && x%100!=0)
        {
            cout << x << " " ;
            x=x+1;
        }

        else
        {
            x=x+1;
        }

    }
    return 0;
}
