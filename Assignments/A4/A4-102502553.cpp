#include<iostream>
using namespace std;
int main ()
{
    int a = 0; //宣布一個整數變數並初始化 = 0
    int b = 0;
    do //先做括號裡的程式
    {
        do
        {
            cout << "Please enter first number( >0 ):"; //顯現字串
            cin >> a; //輸入數字
            if ( a <= 0 ) //如果a <= 0
                cout << "First number is out of range!!" << endl; //顯現字串並換行
        }
        while ( a <= 0 );  //當a <= 0 時，回去找最近的do重做一次
        do
        {
            cout << "Please enter second number( >0 ):";
            cin >> b;
            if ( b <= 0 )
                cout << "Second number is out of range!!" << endl;
        }
        while ( b <= 0 );
        if ( b <= a )
            cout << "The second number is not greater than the first number!!" << endl;
    }
    while ( b <= a );
    cout << "Leap years:";
    for ( int c = a; c <= b; c++ ) //令整數變數c = a，在c <= b時執行，執行完一次c+1
        if ( c % 4000 == 0 ) //如果c除4000餘數為0
            cout << ""; //不要顯示這個數字
        else if ( c % 400 == 0 ) //如果c除400餘數為0
            cout << c << " "; //顯示c，並空一格
        else if ( c % 100 == 0 )
            cout << "";
        else if ( c % 4 == 0 )
            cout << c << " ";
    return 0; //結束
}
