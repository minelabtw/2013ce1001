#include <iostream>

using namespace std;

int main()
{
    int firstyear=0;//宣告名稱為firstyear的整數變數
    int secondyear=0;//宣告名稱為secondyear的整數變數
    while (firstyear>=secondyear)//當firstyear>=secondyear
    {
        cout << "Please enter first number( >0 ):";//輸出字串Please enter first number( >0 ):
        cin >> firstyear;//輸入firstyear
        while (firstyear<=0)//當firstyear<=0
        {
            cout << "First number is out of range!!" << endl;//輸出字串First number is out of range!!
            cout << "Please enter first number( >0 ):";//輸出字串Please enter first number( >0 ):
            cin >> firstyear;//輸入firstyear
        }
        cout << "Please enter second number( >0 ):";//輸出字串Please enter second number( >0 ):
        cin >> secondyear;//輸入secondyear
        while (secondyear<=0||firstyear>=secondyear)//當secondyear<=0或firstyear>=secondyear
        {
            if (secondyear<=0)//判斷secondyear是否<=0
            {
                cout << "Second number is out of range!!" << endl;//輸出字串Second number is out of range!!
                cout << "Please enter second number( >0 ):";//輸出字串Please enter second number( >0 ):
                cin >> secondyear;//輸入secondyear
            }
            else if (secondyear<=firstyear)//判斷secondyear是否<=firstyear
            {
                cout << "The second number is not greater than the first number!!" << endl;//輸出字串The second number is not greater than the first number!!
                break;//跳出迴圈
            }
        }
    }
    cout << "Leap years:";//輸出字串Leap years:
    while (firstyear<=secondyear)//當firstyear<=secondyear
    {
        if (firstyear % 400==0)//判斷firstyear是否被4整除
            cout << firstyear << " ";//輸出字串firstyear
        else if (firstyear % 4==0&&firstyear % 100!=0)//判斷firstyear是否被4整除且firstyear不能被100整除
            cout << firstyear << " ";//輸出字串firstyear
        firstyear++;//firstyear+1

    }
    return 0;
}
