#include <iostream>
using namespace std;

int main()
{
    int firstyear;
    int secondyear;
    cout<<"Please enter first number( >0 ):";
    while(cin>>firstyear) //輸入第一個年分
    {
        if(firstyear<=0)  //判斷是否在範圍內
        {
            cout<<"First number is out of range!!"<<endl;
            cout<<"Please enter first number( >0 ):";
        }
        else
        {
            cout<<"Please enter second number( >0 ):";
            while(cin>>secondyear)  //輸入第二個年份
            {
                if(secondyear<=0)   //判斷是否在範圍內
                {
                    cout<<"Second number is out of range!!"<<endl;
                    cout<<"Please enter second number( >0 ):";
                }

                else
                    break;
            }
            if(firstyear>=secondyear)  //判段是否符合題意
            {
                cout<<"The second number is not greater than the first number!!"<<endl;
                cout<<"Please enter first number( >0 ):";
            }
            else
            {
                cout<<"Leap years:";
                while(firstyear<=secondyear)   //輸出閏年
                {
                    if(firstyear%4==0 && firstyear%100 || firstyear%400==0)
                        cout<<firstyear<<' ';
                    firstyear++;
                }
                break;
            }
        }
    }
    return 0;
}
