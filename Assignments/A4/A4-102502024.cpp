#include<iostream>
using namespace std;

int main()
{
    int year1,year2;  //宣告型別為整數的年份
    cout<<"Please enter first number( >0 ):";  //輸出要求
    cin>>year1; //輸入年分
       while (year1<0)  //判斷是否符合條件
    {
        cout<<"First number is out of range!!\n";
        cin>>year1;
    }
    cout<<"Please enter second number( >0 ):";  //輸出要求
    cin>>year2;  //輸入年分
    while (year2<0)  //判斷是否符合條件
    {
        cout<<"Second number is out of range!!\n";
        cin>>year2;
    }
    while (year2<year1)   //判斷是否符合條件
    {
        cout<<"The second number is not greater than the first number!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>year1;
        while (year1<0)
        {
            cout<<"First number is out of range!!\n";
            cin>>year1;
        }
        cout<<"Please enter second number( >0 ):";
        cin>>year2;
        while (year2<0)
        {
            cout<<"Second number is out of range!!\n";
            cin>>year2;
        }
    }
    int x=year1;  //宣告型別為整數的年份
    while (x<=year2)  //輸出符合條件的年份
    {

        if (x%400==0 && x%4000!=0)
        {
            cout<<x<<" ";

        }
        else if (x%4==0 && x%100!=0 && x%4000!=0)
        {
            cout<<x<<" ";

        }
        x++;
    }
    return 0;
}
