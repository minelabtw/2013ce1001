#include<iostream>
using namespace std;

int main()
{
    int first=0,second=-1;  //宣告型別為 整數(int) 的變數

    while(first>=second)    //輸入數值並判斷是否符合定義
    {
        while(first<=0)
        {
            cout << "Please enter first number( >0 ):";
            cin >> first;
            if(first<=0)cout<< "First number is out of range!!\n";
        }
        while(second<=0)
        {
            cout << "Please enter second number( >0 ):";
            cin >> second;
            if(second<=0)cout<< "Second number is out of range!!\n";
        }
        if(first>=second)
        {
            cout << "The second number is not greater than the first number!!\n";
            first=0;
            second=-1;
        }
    }
    cout << "Leap years:";
    for(int i=first; i<=second; i++)    //判斷並輸出結果
    {
        if(i%400==0 && i%4000!=0)cout << i << " ";
        else if(i%4==0 && i%100!=0)cout << i << " ";
    }
    return 0;
}
