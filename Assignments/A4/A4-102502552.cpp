#include <iostream>
using namespace std;

int main()//開始主程式
{
    int firstyear;//宣告第一個年份的變數
    int lastyear;//宣告第二個年份的變數
    int x = 0;//宣告一個跑動的年份變數

   do{
    cout << "Please enter first number( >0 ):";//提示輸入第一個年份
    cin >> firstyear;
    if ( firstyear <= 0)//判斷輸入的年份是否在要求內
        cout << "First number is out of range!!" << endl;//不符即提示輸入錯誤
   }while( firstyear <= 0);//迴圈檢查年份是否符合要求

   do{
    cout << "Please enter second number( >0 ):";//提示輸入第二年份
    cin >> lastyear;
    if (lastyear <= 0)//判斷輸入的年份是否在要求內
        cout << "Second number is out of range!!" << endl;//不符即提示輸入錯誤
        else if ( lastyear <= firstyear)//檢查第二年份是否大於第一年份
        cout << "The second number is not greater than the first number!!" << endl;//不符即提示輸入錯誤
   }while( lastyear <= 0 || lastyear <= firstyear);//迴圈檢查年份是否符合要求

   cout << "Leap years:";//提示結果為閏年

    for ( x = firstyear; x <= lastyear; x++)//建立迴圈，跑動的變數將由第一年份往第二年份遞增
    {if(x % 4000 == 0)//判斷能否被四千整除
    cout << "";//閏年不能被4000整除，符合者不顯示
      else if (x % 400 == 0)//判斷能否被400整除
        cout << x << " ";//可以的須被顯示
      else if ( x % 4 == 0 && x % 100 != 0)//判斷是否能被4整除但是不能被100整除
        cout << x << " ";//符合的顯示
    }
return 0;//迴歸至0
}//結束主程式
