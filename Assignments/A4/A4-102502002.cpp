#include<iostream>
using namespace std;

int main()
{
    int y1=1;              //宣告一整數變數y1，並令其初始值為1。
    int y2=1;              //宣告一整數變數y2，並令其初始值為1。
    int ly=1;              //宣告一整數變數ly，並令其初始值為1。

    cout << "Please enter first number( >0 ):";    //將"Please enter first number( >0 ):"輸出。
    cin >> y1;                                     //輸入y1。
    while (y1<=0)            //當y1小於等於0。
    {
        cout << "First number is out of range!!\n" << "Please enter first number( >0 ):";  //輸出"First number is out of range!!"，並換行重新輸入。
        cin >> y1;           //輸入y1。
    }

    cout << "Please enter second number( >0 ):";   //將"Please enter second number( >0 ):"輸出。
    cin >> y2;                                     //輸入y2。
    while (y2<=0)            //當y2小於等於0。
    {
        cout << "Second number is out of range!!\n" << "Please enter second number( >0 ):";  //輸出"Second number is out of range!!"，並換行重新輸入。
        cin >> y2;           //輸入y2。
    }

    while (y2<=y1)           //當y2小於等於y1。
    {
        cout << "The second number is not greater than the first number!!\n" << "Please enter first number( >0 ):";  //輸出"The second number is not greater than the first number!!"，並換行重新輸入。
        cin >> y1;           //輸入y1。
        while (y1<=0)        //當y1小於等於0，重複上述迴圈。
        {
            cout << "First number is out of range!!\n" << "Please enter first number( >0 ):"; //輸出out of range，並重新輸入y1的值。
            cin >> y1;
        }
        cout << "Please enter second number( >0 ):";     //重新輸入第二個數。
        cin >> y2;           //將值丟給y2。
        while (y2<=0)        //當y2小於等於0，重複上述迴圈。
        {
            cout << "Second number is out of range!!\n" << "Please enter second number( >0 ):"; //輸出out of range，並重新輸入其值。
            cin >> y2;
        }
    }

    cout << "Leap years:";           //輸出leap years。
    for (ly=y1; (ly<=y2); ly++)      //對於ly介於y1和y2之間，且ly遞增。
    {
        if (ly%400==0)               //判斷ly是否被400整除。
        {
            cout << ly << " ";       //若整除，輸出其值。
        }
        else if ((ly%100==0) && (ly/100!=0));    //判斷100是否被100整除，若整除獲商等於0，則不做任何動作。
        else if (ly%4==0)            //判斷ly是否被4整除。
        {
            cout << ly << " ";       //若整除，輸出ly。
        }
    }
    return 0;
}
