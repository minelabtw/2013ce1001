#include<iostream>
using namespace std;

int main(void)
{
    int y1=0,y2=0;//設定輸入的兩數初始值為0

    cout << "Please enter first number( >0 ):"   ;
    cin >> y1;
    while(y1<0)//輸入第一個數字,若為負數,則顯示字元並重新輸入第一個數字
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):"   ;
        cin >> y1;
    }
    cout << "Please enter second number( >0 ):"   ;
    cin >> y2;
    while(y2<0)//輸入第二個數字,若為負數,則顯示字元並重新輸入第二個數字
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):"   ;
        cin >> y2;
    }

    while(y2<=y1)//輸入的第二個正整數若比第一個正整數小,則重新輸入,直到條件符合
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):"   ;
        cin >> y1;
        while(y1<0)
        {
            cout << "First number is out of range!!" << endl;
            cout << "Please enter first number( >0 ):"   ;
            cin >> y1;
        }
        cout << "Please enter second number( >0 ):"   ;
        cin >> y2;
        while(y2<0)
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):"   ;
            cin >> y2;
        }
    }

    int x=y1;//設定x的最小值為第一個輸入數字
    cout << "Leap years:" ;
    while(x++ and x <= y2)//x的值漸增,且最大值為第二個輸入的數字
    {

        if(x%4000==0)//判斷年份是否為閏年
            cout << "";
        else if (x%400==0)
            cout << x << " ";
        else if (x%100==0)
            cout << "";
        else if (x%4==0)
            cout << x << " ";
    }
    return 0;
}
