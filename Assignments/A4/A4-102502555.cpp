#include<iostream>

using namespace std;

int main()
{
    int first_number;  //宣告變數一
    int second_number;  //宣告變數二

    do{
        cout << "Please enter first number( >0 ):";  //請使用者輸入第一個數字
        cin >> first_number;  //輸入數字一
        if(first_number <= 0){  //檢查數字一有無超出範圍
            cout << "First number is out of range!!" << endl;  //告知超出範圍
        }
    }while(first_number <= 0);  //如果超出範圍再執行一次
    do{
        cout << "Please enter second number( >0 ):";  //請使用者輸入第二個數字
        cin >> second_number;  //輸入數字二
        if(second_number <= 0){  //檢查有無超出範圍
            cout << "Second number is out of range!!" << endl;  //告知超出範圍
        }else if(second_number <= first_number){  //檢查數字二有無小於或等於數字一
            cout << "The second number is not greater than the first number!!" << endl;  //告知不合規定
        }
    }while(second_number <= 0 || second_number <= first_number);  //超出範圍或小於等於數字一再執行一次

    cout << "Leap years:";  //輸出閏年

    for(int i = first_number ; i <= second_number ; i++){  //輸出閏年
        if(i % 400 == 0){  //檢查是否可被400整除
            if(i % 4000 != 0){  //檢查是否可被4000整除
                cout << i << " ";  //輸出閏年
            }
        }else if(i % 4 == 0 && i % 100 != 0){  //檢查是否可被4整除且不被100整除
            cout << i << " ";  //輸出閏年
        }
    }

    return 0;

}
