#include <iostream>
using namespace std;
int main()
{
    int first;  //first year
    int second;  //second year

    cout<<"Please enter first number( >0 ):";
    cin>>first;
    while(first<=0)  //check if bigger than 0
    {
        cout<<"First number is out of range!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>first;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>second;
    while(second<=0)  //check if bigger than 0
    {
        cout<<"Second number is out of range!!\n";
        cout<<"Please enter second number( >0 ):";
        cin>>second;
    }
    while(second<=first)  //check if second year is bigger than first year
    {
        cout<<"The second number is not greater than the first number!!\n";
        cout<<"Please enter first number( >0 ):";  //enter first year again
        cin>>first;
        while(first<=0)  //check new first year
        {
            cout<<"First number is out of range!!\n";
            cout<<"Please enter first number( >0 ):";
            cin>>first;
        }
        cout<<"Please enter second number( >0 ):";
        cin>>second;
        while(second<=0)  //check new second year
        {
            cout<<"Second number is out of range!!\n";
            cout<<"Please enter second number( >0 ):";
            cin>>second;
        }
    }

    cout<<"Leap years:";  //output leap years
    while(first<=second)
    {
        if(first%400==0 && first%4000!=0)
            cout<<first<<" ";
        else if(first%4==0 && first%100!=0)
            cout<<first<<" ";
        first++;
    }


    return 0;

}
