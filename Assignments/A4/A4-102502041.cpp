#include<iostream>
using namespace std;
int main()
{
    int first_number=0,second_number=0;
    while(true)
    {
        while(true)
        {
            cout<<"Please enter first number( >0 ):";
            cin>>first_number;

            if(first_number<=0)
            {
                cout<<"First number is out of range!!"<<endl;
            }
            else                                                //若是first_number在符合的範圍內,則跳出迴圈
                break;
        }
        while(true)
        {
            cout<<"Please enter second number( >0 ):";
            cin>>second_number;

            if(second_number<=0)
            {
                cout<<"Second number is out of range!!"<<endl;
            }
            else                                                //若是second_number在符合的範圍內,則跳出迴圈
                break;
        }
        if(second_number<=first_number)
        {
            cout<<"The second number is not greater than the first number!!"<<endl;
            continue;
        }
        else                                                    //若是兩個變數都沒有問題,就跳出迴圈
            break;

    }
    cout<<"Leap years:";

    for(int x=0; x<=second_number-first_number; x++)
    {
        if((x+first_number)%400==0)                             //依照閏年判斷的先後順序,來設計迴圈
            cout<<x+first_number<<" ";
        else if((x+first_number)%100==0)
            continue;
        else if((x+first_number)%4==0)
            cout<<x+first_number<<" ";
    }
    return 0;
}



