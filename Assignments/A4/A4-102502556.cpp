#include <iostream>
using namespace std;

int main ()
{
    int a = 0; //宣告型別為 整數(int) 的第一個年份(a)，並初始化其數值為0。
    int b = 0; //宣告型別為 整數(int) 的第二個年份(b)，並初始化其數值為0。
    cout << "Please enter first number( >0 ):" ; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cin >> a;
    while ( a <= 0 ) //用while迴圈檢驗使用者的輸入是否符合標準，如果輸入的年份不大於0，則要求其重新輸入。
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):" ;
        cin >> a ;
    }
    cout << "Please enter second number( >0 ):" ;
    cin >> b ;
    while ( b <= 0 )
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):" ;
        cin >> b;
    }
    while ( b <= a && b > 0 ) //用while迴圈檢驗使用者的輸入是否符合標準，如果輸入的第二個年份不大於第一個，則要求其重新輸入。
    {
        cout << "The second number is not greater than the first number!!" << endl;
        cout << "Please enter first number( >0 ):" ;
        cin >> a;
        while ( a <= 0 )
        {
            cout << "First number is out of range!!" << endl;
            cout << "Please enter first number( >0 ):" ;
            cin >> a;
        }
        cout << "Please enter second number( >0 ):" ;
        cin >> b;
        while ( b <= 0 )
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):" ;
            cin >> b;
        }
    }
    cout << "Leap years:";
    for (int i = a; i <= b; i++) //使用for迴圈來檢驗每一個年份，並輸出屬於閏年的年份。
    {

        if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0 && i % 4000 != 0)
            cout << i << " ";
    }
    return 0;
}
