#include <iostream>
using namespace std;

int main()
{
    int first_number=0,second_number=0;                         //宣告型別為整數的first_number,second_number 並初始為其值為0
    int i=first_number;                                         //宣告型別為整數的i 並初始為其值為first_number

    while(1)                                                    //當second_number小於first_number 可以再次詢問first_number的迴圈
    {
        while(1)                                                //詢問first_number的迴圈
        {
            cout << "Please enter first number( >0 ):";         //輸出"Please enter first number( >0 ):"到螢幕上
            cin >> first_number;                                //輸入一個值給first_number
            if (first_number<=0)                                //判斷first_number是否小於等於0
            {
                cout << "First number is out of range!!\n";     //輸出"First number is out of range!!\n"到螢幕上
            }
            else
            {
                break;                                          //跳出迴圈
            }
        }
        while(1)                                                //詢問second_number的迴圈
        {
            cout << "Please enter second number( >0 ):";        //輸出"Please enter second number( >0 ):"到螢幕上
            cin >> second_number;                               //輸入一個值給second_number
            if (second_number<=0)                               //判斷second_number是否小於等於0
            {
                cout<< "Second number is out of range!!\n";     //輸出"Second number is out of range!!\n"到螢幕上
            }
            else if (second_number>0 && second_number<=first_number)                            //判斷second_number是否大於等於0和小於等於first_number
            {
                cout << "The second number is not greater than the first number!!\n";           //輸出"The second number is not greater than the first number!!\n"到螢幕上
                break;                                          //跳出迴圈
            }
            else
            {
                break;                                          //跳出迴圈
            }
        }
        if(first_number<second_number)                          //判斷first_number是否小於second_number
        {
            break;                                              //跳出迴圈
        }
    }
    cout << "Leap years:";                                      //輸出"Leap years:"到螢幕上
    for (i=first_number; i<=second_number; i++)                 //重複累加i 到i小於等於second_number
    {
        if(i%400==0)                                            //判斷i除400的餘數是否為0
        {
            cout << i << " ";                                   //輸出i" "到螢幕上
        }
        if(i%4==0 && i%100!=0)                                  //判斷i除4的餘數是否為0並且i除100的餘數是否不等於0
        {
            cout << i << " ";                                   //輸出i" "到螢幕上
        }
    }

    return 0;
}
