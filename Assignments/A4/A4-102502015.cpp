#include <iostream>
using namespace std;
int main()
{
    int a=1;                                                //a=第一個數   b=第二個數  c=年份從a判斷到b  d=c除以4的餘數  e=c除以100的餘數 f=c除以400的餘數
    int b=0;
    int c=0;
    int d=0;
    int e=0;
    int f=0;
    while(a>b)                                              //當A>B的時候 就會重新迴圈
    {
        cout<<"Please enter first number( >0 ):";
        cin>>a;
        while(a<=0)                                         //A小於等於0重新輸入
        {
            cout<<"First number is out of range!!"<<endl;
            cout<<"Please enter first number( >0 ):";
            cin>>a;
        }
        cout<<"Please enter second number( >0 ):";
        cin>>b;
        while(b<=0)                                         //B小於等於0重新輸入
        {
            cout<<"Second number is out of range!!"<<endl;
            cout<<"Please enter second number( >0 ):";
            cin>>b;
        }
        if (b<=a)                                           //a大於等於b的時候跳出訊息 並且 因為11行的WHILE  重新迴圈
        {
            cout<<"The first number is not greater than the second number!!"<<endl;
        }
    }
    cout<<"Leap years:";
    for(c=a; c<b; c++)                                      //從A開始迴圈判斷到B
    {
        d=c%4;                                              //計算分別的餘數
        e=c%100;
        f=c%400;
        if(d==0 && e!=0 ||(f==0))                           //當 被4整除 但不被100整除 或者是 被400 整除的數 就會輸出
        {
            cout<<c<<" ";
        }
    }
    return 0;
}
