#include <iostream>
using namespace std;

int main()
{
    int a = 0;                                                                //設定變數a,b
    int b = 0;

    cout << "Please enter first number( >0 ):";
    while (cin>>a)                                                           //以輸入第一個數為迴圈起始
    {
        while (a<=0)                                                          //判別a是否超出範圍
        {
            cout << "First number is out of range!!" << endl;
            cout << "Please enter first number( >0 ):";
            cin >> a;
        }

        cout << "Please enter second number( >0 ):";
        cin >> b;
        while (b<=0)                                                          //判別b是否超出範圍
        {
            cout << "Second number is out of range!!" << endl;
            cout << "Please enter second number( >0 ):";
            cin >> b;
        }
        if (a>b)                                                            //如果第一個數大於第二個數，則結束迴圈回到起始
        {
            cout << "The second number is not greater than the first number!!" << endl;
            cout << "Please enter first number( >0 ):";
        }
        else                                                                //否則開始找尋閏年
        {
            cout << "Leap years:";
            for (int i=a; i<=b; i++)
            {
                if( (i%4)==0 && (i%100)!=0 || (i%400)==0 && (i%4000)!=0)     //是4的倍數，不是100的倍數，是400的倍數，不是4000的倍數
                    cout << i << " ";                                        //依序輸出
            }
            break;
        }
    }
    return 0;
}
