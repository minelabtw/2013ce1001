#include<iostream>

using namespace std;

int main()
{
    int a = 0;//宣告整數a
    int b = 0;//宣告整數b
    while(a<= 0 || b<=0)//迴圈 當 a小於等於0 或 b小於等於0
    {
        while(a<=0)//迴圈  當a小於等於0
        {
            cout << "Please enter first number( >0 ):";//輸出Please enter first number( >0 ):
            cin >> a;//輸入變數a
            if(a<=0)//條件 當a小於等於0
                cout << "First number is out of range!!" << endl;//輸出First number is out of range!! 換行
		}
        cout << "Please enter second number( >0 ):";//輸出Please enter second number( >0 ):
        cin >> b;//輸入變數b
        if(b<=0)//條件一 當b小於等於0
            cout << "Second number is out of range!!" << endl;//輸出Second number is out of range!! 換行
        else if(b>0 && b<=a)//條件二 當b大於0 且 b小於等於a
        {
            cout << "The second number is not greater than the first number!!" << endl;//輸出The second number is not greater than the first number!! 換行
            a=0;//a恢復起始值0
        }
    }
    cout << "Leap years:";//輸出Leap years:
    for(int c=a; c<=b; c++)//迴圈 整數c=a c小於等於b c遞增
    {
        if(c%400==0 && c%4000!=0)//條件 當c除400餘0 且c除4000不等於0
            cout << c << " ";//輸出 c 空格
        else if(c%4==0 && c%100!=0)//條件 當c除4等於0 且c除100不等於0
            cout << c << " " ;//輸出 c 空格
    }
    return 0;
}
