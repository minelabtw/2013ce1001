#include<iostream>
using namespace std ;

int main ()
{
    int counter=0 ,y1=0,y2=0 ;                                  //宣告三個變數 並設定初始值為0

    while(counter<=1)                                           //counter 拿來計次
    {
        if(counter==0)                                          //因為已設定初始值為0
        {
            cout<<"Please enter first number( >0 ):" ;
            cin>>y1 ;
            if(y1<=0)                                           //不符合 則counter不加 所以會再跑一次 迴圈
            {
                cout<<"First number is out of range!!"<<endl ;
            }
            else
                counter++ ;                                     //符合則加1
        }
        if(counter==1)                                          //同上邏輯
        {
            cout<<"Please enter second number( >0 ):" ;
            cin>>y2 ;
            if(y2<=0)                                           //不符合再跑一次迴圈
            {
                cout<<"Second number is out of range!!"<<endl ;
            }
            else if(y2<y1)                                      //當第二次輸入的年份小於第一次
            {
                cout<<"The second number is not greater than the first number!!"<<endl ;
                counter-- ;                                     //counter 減1 所以會再跑一次上面的if迴圈
            }
            else
                counter++ ;                                     //符合counter 加1 跳出迴圈
        }
    }
    cout<<"Leap years:" ;                                       //用來做最後的輸出
    for (int year=y1;year<=y2;year++)                           //宣告新變數year其值為y1 假如year <=y2 則year +1
    {
        if(year%400==0)                                         //若可以被400整除 則 輸出
        {
            cout<<year<<" " ;
        }
        else if(year%4==0&&year%100!=0)                         //若可被4只除且不能被100整除 則輸出
        {
            cout<<year<<" ";
        }
    }
    return 0 ;
}
