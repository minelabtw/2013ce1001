#include <iostream>

using namespace std; //代表要使用std裡面的名稱如cin.cout.endl

int main()
{
    int quiz = 0;   //宣告四個整數變數，初始值為0
    int Midterm = 0;
    int FinalExam = 0;
    int Assignment = 0;

    cout << "quiz(20%):";
    cin >> quiz;    //輸入quiz的值

    while (quiz < 0 || quiz > 100) //如果quiz的值不在0~100，則執行下列迴圈，再次輸入quiz的值
    {
        cout << "Out of range!!" << endl << "quiz(20%):";
        cin >> quiz;
    }

    cout << "Midterm(20%):";
    cin >> Midterm;

    while (Midterm < 0 || Midterm > 100)      //假如Midterm的值不在0~100，則執行下列迴圈，再次輸入Midterm的值
    {
        cout << "Out of range!!" << endl << "Midterm(20%):";
        cin >> Midterm;
    }

    cout << "Final Exam(30%):";
    cin >> FinalExam;

    while(FinalExam < 0 || FinalExam > 100)    //假如FinalExam的值不在0~100，則執行下列迴圈，再次輸入FinalExam的值
    {
        cout << "Out of range!!" << endl << "FinalExam(30%):";
        cin >> FinalExam;
    }

    cout << "Assignment(30%):";
    cin >> Assignment;

    while (Assignment < 0 || Assignment > 100)  //假如Assignment的值不在0~100，則執行下列迴圈，再次輸入Assignment的值

    {
       cout << "Out of range!!" << endl << "Assignment(30%):";
       cin >> Assignment;
    }

    cout << "Final grade:" << quiz * 0.2 +Midterm * 0.2 +FinalExam * 0.3 +Assignment * 0.3 << "分" << endl; //輸出Final grade

    return 0;

}

