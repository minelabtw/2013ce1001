#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    double number1=0 ;//宣告型別為double的變數number1，並初始化其數值為0
    double number2=0 ;//宣告型別為double的變數number2，並初始化其數值為0
    double number3=0 ;//宣告型別為double的變數number3，並初始化其數值為0
    double number4=0 ;//宣告型別為double的變數number4，並初始化其數值為0
    double sum=0 ;    //宣告型別為double的變數sum，並初始化其數值為0


    cout << "quiz(20%):";                                           //在螢幕上顯示"quiz(20%):"
    while (cin >> number1)                                          //當輸入number1時進入迴圈
    {

        if ( number1 < 0 || number1 > 100 )                         //如果number1<0或number1>100
            cout << "Out of range!!" << endl << "quiz(20%):";       //顯示"Out of range!!"且換行並顯示"quiz(20%):"

        else                                                        //當不是number1<0或number1>100的情況時
        {
            cout << "Midterm(20%):" ;                               //輸出"Midterm(20%):"
            break;                                                  //跳出迴圈
        }
    }

    while (cin >> number2)                                          //當輸入number2時進入迴圈
    {
        if ( number2 < 0 || number2 > 100 )                         //如果number2<0或number2>100
            cout << "Out of range!!" << endl << "Midterm(20%):";    //顯示"Out of range!!"且換行並顯示"Midterm(20%):"
        else                                                        //當不是number2<0或number2>100的情況時
        {
            cout << "Final Exam(30%):" ;                            //輸出"Final Exam(30%):"
            break;                                                  //跳出迴圈
        }
    }

    while (cin >> number3)                                          //當輸入number3時進入迴圈
    {
        if ( number3 < 0 || number3 > 100 )                         //如果number3<0或number3>100
            cout << "Out of range!!" << endl << "Final Exam(30%):"; //顯示"Out of range!!"且換行並顯示 "Final Exam(30%):"
        else                                                        //當不是number3<0或number3>100的情況時
        {
            cout << "Assignment(30%):" ;                            //輸出"Assignment(30%):"
            break;                                                  //跳出迴圈
        }

    }

    while (cin >> number4)                                          //當輸入number4時進入迴圈
    {
        if ( number4 < 0 || number4 > 100 )                         //如果number4<0或number4>100
            cout << "Out of range!!" << endl << "Assignment(30%):"; //顯示"Out of range!!"且換行並顯示 "Assignment(30%):"
        else                                                        //當不是number4<0或number4>100的情況時
        {
            cout << "Final grade:" ;                                //輸出"Final grade:"
            break;                                                  //跳出迴圈
        }
    }

    sum = number1*0.2 + number2*0.2 + number3*0.3 + number4*0.3 ;   //將number1*0.2 + number2*0.2 + number3*0.3 + number4*0.3 後的值送到sum
    cout << sum << "分" ;                                           //輸出計算後的值 sum分


     return 0;



}

