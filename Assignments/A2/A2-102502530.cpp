#include<iostream>
int main()
{
   double score[4],per[4]={.2,.2,.3,.3};     //宣告存放分數用的變數及加權
   double ans=0;                             //宣告存放加權平均用的變數
   char output[5][20]={"quiz(20%):","Midterm(20%):","Final Exam(30%):","Assignment(30%):","Out of range!!\n"}; //宣告迴圈中輸出用的字串
   for(int i=0;i!=4;i++)                     //執行迴圈
   {
      std::cout<<output[i];                  //輸出輸入提示
      std::cin>>score[i];                    //輸入分數
      if(score[i]<0||score[i]>100)           //如果分數不是0~100
      {
         std::cout<<output[4];               //輸出"Out of range!!"
         i--;                                //重新執行本次迴圈
      }
      else                                   //如果分數是0~100
         ans+=score[i]*per[i];               //計算平均
   }
   std::cout<<"Final grade: "<<ans<<"分\n";  //輸出結果
}
