#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int quiz = 0,Midterm = 0,FinalExam = 0;                                        //宣告測驗成績(小考、期中、期末)，並初始化其數值為0。
    int Assignment = 0;                                                             //宣告作業成績，並初始化其數值為0。
    int Finalgrade = 0;                                                            //宣告總成績，並初始化其數值為0。

    cout << "quiz(20%):";                                                           //宣告小考成績，超出範圍者，重新輸入
    cin >> quiz;
        while(quiz < 0 || quiz >100)
            cout << "Out of Range!!" << "quiz(20%):";

    cout << "Midterm(20%):";                                                        //宣告期中成績，超出範圍者，重新輸入
    cin >> Midterm;
         while(Midterm < 0 || Midterm > 100)
            cout <<"Out of Range!!"<< "Midterm(20%):";

    cout << "FinalExam(30%):";                                                     //宣告期末成績，超出範圍者，重新輸入
    cin >> FinalExam;
        while(FinalExam < 0 || FinalExam > 100)
            cout << "Out of Range!!"<< "FinalExam(30%):";

    cout << "Assignment(30%):";                                                     //宣告作業成績，超出範圍者，重新輸入
    cin >> Assignment;
        while(Assignment < 0 || Assignment > 100)
            cout <<"Out of Range!!"<< "Assignment(30%):";

    Finalgrade = ( quiz + Midterm ) * 0.2 + ( FinalExam + Assignment ) * 0.3;    //結算總成績
    cout << "Finalgrade:" << Finalgrade << "分" << endl;

    return 0;
}
