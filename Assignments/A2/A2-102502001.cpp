#include <iostream>

using namespace std ;

int main()
{
    float grade1 = 0 ;    //宣告第一個變數,其初始值為0
    float grade2 = 0 ;    //宣告第二個變數,其初始值為0
    float grade3 = 0 ;    //宣告第三個變數,其初始值為0
    float grade4 = 0 ;    //宣告第四個變數,其初始值為0
    float average = 0 ;   //宣告average為平均值,其初始值為0

    cout << "quiz(20%)" ;              //將"quiz(20%)"顯示於螢幕上
    cin >> grade1 ;                    //將輸入值存放於grade1
    while (grade1 <0 or grade1 >100)   //判斷grade1之值是否介於0到100之間
    {
        cout <<"out of range!!" << endl ;   //若grade1之值不介於0到100之間,螢幕顯示"out of range"
        cout << "quiz(20%)" ;               //將"quiz(20%)顯示於螢幕上
        cin >> grade1 ;                     //將輸入值存放於grade1
    }

    cout <<"midterm(20%)" ;
    cin >> grade2 ;
    while (grade2 > 100 or grade2 < 0)
    {
        cout <<"out of range!!" << endl ;
        cout <<"midtern(20%)" ;
        cin >>grade2 ;
    }

    cout <<"final(30%)" ;
    cin >> grade3 ;
    while (grade3>100 or grade3<0)
    {
        cout <<"out of range!!" ;
        cout <<"final(30%)" ;
        cin >> grade3 ;
    }

    cout <<"assingnment(30%)" ;
    cin >> grade4 ;
    while (grade4>100 or grade4<0)
    {
        cout <<"out of range!!" ;
        cout <<"assingment(30%)" ;
        cin >>grade4 ;
    }

    average = grade1 * 0.2 + grade2 *0.2 + grade3 *0.3 + grade4 *0.3 ;   //計算平均並將其值存放於average
    cout << "final grade=" << average <<"分" ;                           //將"final grade="顯示於螢幕,並顯示平均值

    return 0 ;
}
