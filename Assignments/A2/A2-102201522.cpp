#include<iostream>
using namespace std;

int main()
{
    float a,b,c,d; //引入四個變數，為避免輸入的成績有小數點，使用float
CINA:              //在此加入第一個標誌CINA
    ;              //goto CINA時執行的空敘述
    cout << "quiz(20%):" ; //輸出"quiz(20%):"
    cin >> a;              //輸入變數a
    if (a>100|a<0)         //使用if-else判斷，若a不符合成績輸入條件(0-100分)
    {
        cout << "Out of range!!"<<endl; //輸出"Out of range!!"的警告
        goto CINA;         //跳回CINA
    }
    else                   //若a符合條件
    {
CINB:              //在此加入第二個標誌CINB
        ;
        cout <<"Midterm(20%):";
        cin >> b;
        if (b>100|b<0)
        {
            cout << "Out of range!!"<<endl;
            goto CINB;
        }
        else
        {
CINC:              //在此加入第三個標誌CINC
            ;
            cout <<"Final Exam(30%):";
            cin >> c;
            if (c>100|c<0)
            {
                cout << "Out of range!!"<<endl;
                goto CINC;
            }
            else
            {
                cout <<"Assignment(30%):";
                cin >> d;
                while (d>100|d<0)
                //使用while判斷d是否符合輸入成績的條件，若不符合則重複循環大括號內的敘述
                {
                    cout << "Out of range!!"<<endl;
                    cout <<"Assignment(30%):";
                    cin >> d;
                }
                cout << "Final grade: "<<(a+b)*0.2+(c+d)*0.3<<"分";
            }
        }
    }
}
