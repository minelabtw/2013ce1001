#include<iostream>
using std::cout;
using std::cin;

int main()
{
    int number1=0;      //宣告number1的變數，並使初始值為0。
    int number2=0;      //宣告number2的變數，並使初始值為0。
    int number3=0;      //宣告number3的變數，並使初始值為0。
    int number4=0;      //宣告number4的變數，並使初始值為0。
    int sum=0;          //宣告number5的變數，並使初始值為0。

    cout << "quiz(20%):";      //輸出"quiz(20%):"至螢幕。
    cin >> number1;            //輸入至number1。

    while(number1>100 || number1<0)                         //while迴圈限定執行範圍於number1>100或number1<0。
    {
        cout << "out of range！！\n" << "quiz(20%):";       //輸出"out of range！！"至螢幕，並再次輸出"quiz(20%):"至螢幕。
        cin >> number1;                                     //輸入至number1。
    }

    cout << "Midterm(20%):";      //輸出"Midterm(20%):"至螢幕。
    cin >> number2;               //輸入至number2。

    while(number2>100 || number2<0)                            //while迴圈限定執行範圍於number2>100或number2<0。
    {
        cout << "out of range！！\n" << "Midterm(20%):";       //輸出"out of range！！"至螢幕，並再次輸出"Midterm(20%):"至螢幕。
        cin >> number2;                                        //輸入至number2。
    }

    cout << "Final Exam(30%):";      //輸出"Final Exam(30%):"至螢幕。
    cin >> number3;                  //輸入至number3。

    while(number3>100 || number3<0)                               //while迴圈限定執行範圍於number3>100或number3<0。
    {
        cout << "out of range！！\n" << "Final Exam(30%):";       //輸出"out of range！！"至螢幕，並再次輸出"Final Exam(30%):"至螢幕。
        cin >> number3;                                           //輸入至number3。
    }

    cout << "Assignment(30%):";      //輸出"Assignment(30%):"至螢幕。
    cin >> number4;                  //輸入至number4。

    while(number4>100 || number4<0)                               //while迴圈限定執行範圍於number4>100或number4<0。
    {
        cout << "out of range！！\n" << "Assignment(30%):";       //輸出"out of range！！"至螢幕，並再次輸出"Assignment(30%):"至螢幕。
        cin >> number4;                                           //輸入至number4。
    }

    sum = (number1 * 1/5 + number2 * 1/5 + number3 * 3/10 + number4 * 3/10);      //運算後將值輸入至sum裡。
    cout << "Final grade:" << sum;      //輸出"Final grade:"="sum"至螢幕。

    return 0;
}
