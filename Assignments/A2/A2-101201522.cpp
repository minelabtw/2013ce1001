#include <iostream>
#include <string>
using namespace std;

int main(){
    double grade,average = 0;//grade為每次輸入的分數,average為總平均分數且初始為0
    double percent[4] = {0.2,0.2,0.3,0.3};//percent為每次分數占總成績的百分比 
    //宣告為雙精度浮點數，以便處理有小數的分數和總平均 
    string output[4] = {"quiz(20%):","Midterm(20%):","Final Exam(30%):","Assignment(30%):"};//output為每次詢問分數的字串 
    int times = 0;//times表示第幾次的分數,初始化為0,次數從0開始計算 
    
    while(times<=3){//如果四次成績輸入完畢即跳出迴圈 
        
        cout << output[times];//輸出詢問字串 
            
        cin >> grade;//輸入成績 
        
        if(grade < 0 || grade > 100)//如果成績輸入錯誤，輸出Out of range,回到一開始重新詢問 
            cout << "Out of range!!\n";
        else{//如果成績輸入正確，計算目前總平均，並繼續計算下一次分數 
            average += percent[times]*grade;
            times += 1;
        }
        
    }
    
    cout << "Final grade: " << average << "分\n";//輸出總平均結果 
    
    return 0;
}
