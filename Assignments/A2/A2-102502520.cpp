#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int quiz=0;                                               //宣告一個整數，作為小考的元素，並定義其為0
    int midterm=0;                                            //宣告一個整數，作為期中的元素，並定義其為0
    int finalexam=0;                                          //宣告一個整數，作為期末的元素，並定義其為0
    int assignment=0;                                         //宣告一個整數，作為作業的元素，並定義其為0
    int grade=0;                                              //宣告一個整數，作為最終成績的元素，並定義其為0

    cout<<"請輸入小考成績!\n";                                //輸出"請輸入小考成績!"
    cin>>quiz;                                                //輸入，在quiz的位置

    while                                                     //當quiz>100||quiz<0時
        (quiz>100||quiz<0)
        {
           cout<<"Out of rage!"<<endl;                        //輸出"Out of rage!"，並換行
           cout<<"再輸入一次!\n";                             //輸出"再輸入一次!"
           cin>>quiz;                                         //重新輸入，在quiz的位置
        }
        if (quiz>=0||quiz<=100)                               //若quiz介於0~100
            cout<<"quiz(20%):"<<quiz<<endl;                   //則顯示"quiz(20%):小考成績"

    cout<<"請輸入期中成績!\n";                                //輸出"請輸入期中成績!"
    cin>>midterm;                                             //輸入，在midterm的位置

    while                                                     //當midterm>100||midterm<0時
        (midterm>100||midterm<0)
        {
           cout<<"Out of rage!"<<endl;                        //輸出"Out of rage!"，並換行
           cout<<"再輸入一次!\n";                             //輸出"再輸入一次!"
           cin>>midterm;                                      //重新輸入，在midterm的位置
        }
        if (midterm>=0||midterm<=100)                         //若midterm介於0~100
            cout<<"midterm(20%):"<<midterm<<endl;             //則顯示"midterm(20%):期中成績"

    cout<<"請輸入期末成績!\n";                                //輸出"請輸入期末成績!"
    cin>>finalexam;                                           //輸入，在finalexam的位置

    while                                                     //當finalexam>100||finalexam<0時
        (finalexam>100||finalexam<0)
        {
           cout<<"Out of rage!"<<endl;                        //輸出"Out of rage!"，並換行
           cout<<"再輸入一次!\n";                             //輸出"再輸入一次!"
           cin>>finalexam;                                    //重新輸入，在finalexam的位置
        }
        if (finalexam>=0||finalexam<=100)                     //若finalexam介於0~100
            cout<<"finalexam(30%):"<<finalexam<<endl;         //則顯示"finalexam(30%):期末成績"

    cout<<"請輸入作業成績!\n";                                //輸出"請輸入作業成績!"
    cin>>assignment;                                          //輸入，在assignment的位置

    while                                                     //當assignment>100||assignment<0時
        (assignment>100||assignment<0)
        {
           cout<<"Out of rage!"<<endl;                        //輸出"Out of rage!"，並換行
           cout<<"再輸入一次!\n";                             //輸出"再輸入一次!"
           cin>>assignment;                                   //重新輸入，在assignment的位置
        }
        if (assignment>=0||assignment<=100)                   //若assignment介於0~100
            cout<<"assignment(30%):"<<assignment<<endl;       //則顯示"assignment(30%):作業成績"

    grade=(quiz*2+midterm*2+finalexam*3+assignment*3)/10;     //將所有成績按比例計算，並放到grade的位置
    cout<<"Final Grade:"<<grade<<endl;                        //輸出"Final Grade:最後成績"



    return 0;
}
