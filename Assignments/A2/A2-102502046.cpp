#include <iostream>
using namespace std;

int main()
{
    float a=0;    //宣告型別為 浮點數(int) 的a，並初始化其數值為0。
    float b=0;    //宣告型別為 浮點數(int) 的b，並初始化其數值為0。
    float c=0;    //宣告型別為 浮點數(int) 的c，並初始化其數值為0。
    float d=0;    //宣告型別為 浮點數(int) 的d，並初始化其數值為0。

    cout << "quiz(20%):";   //印出quiz(20%):
    cin >> a;               //輸入變數a
        while (a>100 || a<0)    //當a>100或a<0時，進入while迴圈
        {
            cout << "Out of range!!" << endl;   //印出Out of range!!
            cout << "quiz(20%):";               //印出quiz(20%):
            cin >> a;                           //再次輸入變數a
        }
    cout << "Midterm(20%):";
    cin >> b;
        while (b>100 || b<0)
        {
            cout << "Out of range!!" << endl;
            cout << "Midterm(20%):";
            cin >> b;
        }
    cout << "Final Exam(30%):";
    cin >> c;
        while (c>100 || c<0)
        {
            cout << "Out of range!!" << endl;
            cout << "Final Exam(30%):";
            cin >> c;
        }
    cout << "Assignment(30%):";
    cin >> d;
        while (d>100 || d<0)
        {
            cout << "Out of range!!" << endl;
            cout << "Assignment(30%):";
            cin >> d;
        }

    cout << "Final grade: " << a*0.2+b*0.2+c*0.3+d*0.3 << "分" ; //計算總成績，並輸出



    return 0;       //回傳整數0

}
