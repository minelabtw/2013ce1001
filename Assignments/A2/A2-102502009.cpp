#include <iostream>

using namespace std;

int main()
{
   int quiz=0; //宣告六個整數變數，且初始值為0
   int midterm=0;
   int finalexam=0;
   int assignment=0;
   int finalgrade=0;
   int grade=0;

   while(grade<=10) //設定一個迴圈，使輸入的值過高時，可以重新輸入
   {
       if(grade==0) //假設grade為初始值0，則輸入quiz的值
       {
           cout <<"quiz(20%):";
           cin >>quiz;

           if(quiz>100 || quiz<0) //假設輸入的quiz>100或<0，則輸出"Out of range!!"，且grade維持初始值0，重新輸入quiz的值
           {
                cout<<"Out of range!!"<<endl;
           }

           else
                grade=grade+1; //若輸入的quiz<=100，則grade的值+1
       }

       if(grade==1) //假設grade的值為1，則輸入midterm的值
       {
           cout <<"Midterm(20%):";
           cin >>midterm;

           if(midterm>100 || midterm<0)
           {
                cout<<"Out of range!!"<<endl; //假設輸入的midterm>100或<0，則輸出"Out of range!!"，且grade維持為1，重新輸入midterm的值
           }

           else
                grade=grade+1; //若輸入的midterm<=100，則grade的值+1
       }

       if(grade==2) //假設grade的值為2，則輸入finalexam的值
       {
           cout <<"Final Exam(30%):";
           cin >>finalexam;

           if(finalexam>100 || finalexam<0) //假設輸入的finexam>100或<0，則輸出"Out of range!!"，且grade維持為2，重新輸入finalexam的值
           {
                cout<<"Out of range!!"<<endl;
           }

           else
                grade=grade+1; //若輸入的finalexam<=100，則grade的值+1
       }

       if(grade==3) //假設grade的值為3，則輸入assignment的值
       {
           cout <<"Assignment(30%):";
           cin >>assignment;

           if(assignment>100 || assignment<0) //假設輸入的assignment>100或<0，則輸出"Out of range!!"，且grade維持為3，重新輸入assignment的值
           {
                cout<<"Out of range!!"<<endl;
           }

           else
                grade=grade+1; //若輸入的assignment<=100，則grade的值+1
       }

       if(grade==4) //假設grade的值為4，則輸出finalgrade，且跳出迴圈
       {
            finalgrade=quiz*0.2+midterm*0.2+finalexam*0.3+assignment*0.3;

            cout<<"Final grade: "<<finalgrade<<"分"<<endl;

            break;
       }
   }


    return 0;

}

