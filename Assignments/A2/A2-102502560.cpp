#include <iostream>
using namespace std;

int main()
{
	double quiz;						//declare a variable to store the score of "quiz"
	cout << "quiz(20%):" ;				//display message to tell the user to input a number
	cin >> quiz ;						//store user's input into the variable
	while( quiz<0 || quiz>100 ){		//if the input is out of range...
		cout << "Out of range!!\n" ;	//WARN the user that the input is out of range
		cout << "quiz(20%):" ;			//...and display message to tell the user to input a number again
		cin >> quiz ;					//store user's input into the variable again
	}

	double midterm;
	cout << "Midterm(20%):" ;
	cin >> midterm ;
	while( midterm<0 || midterm>100 ){
		cout << "Out of range!!\n" ;
		cout << "Midterm(20%):" ;
		cin >> midterm ;
	}

	double final_exam;
	cout << "Final Exam(30%):" ;
	cin >> final_exam ;
	while( final_exam<0 || final_exam>100 ){
		cout << "Out of range!!\n" ;
		cout << "Final Exam(30%):" ;
		cin >> final_exam ;
	}

	double assignment;
	cout << "Assignment(30%):" ;
	cin >> assignment ;
	while( assignment<0 || assignment>100 ){
		cout << "Out of range!!\n" ;
		cout << "Assignment(30%):" ;
		cin >> assignment ;
	}

	double total = quiz*0.2 + midterm*0.2 + final_exam*0.3 + assignment*0.3;
	//declare the variable "total" and calculate the final grade
	cout << "Final grade: " << total << "��" ;	//print the final result

	return 0;
}
