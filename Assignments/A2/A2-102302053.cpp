//A2-102302053.cpp
//calculator for final grades
#include <iostream>// allow program to perform  input and output
#include <cstdlib>//allow program to pause after the computing

using std::cout;
using std::cin;
using std::endl;

//function main begins program execution
int main()

{
    double number1;//for quiz(20%)
    double number2;//for Midterm(20%)
    double number3;//for Final Exam(30%)
    double number4;//for Assignment(30%)
    double answer;//for Final Grades

    cout << "quiz(20%):";//prompt user to give a value for quiz(20%).
    cin >> number1;

    while (0 > number1 or number1 > 100) //check the range of the value, if the value isnt in the range of 0 ~100, prompt the user to enter again.
    {
         cout << "Out of Range !\n";
         cout << "quiz(20%):";
         cin >> number1;

    }

    cout << "Midterm(20%):";//prompt user to give a value for Midterm(20%).
    cin >> number2;

    while (0 > number2 or number2 > 100)//check the range of the value, if the value isnt in the range of 0 ~100, prompt the user to enter again.
    {
         cout << "Out of Range !\n";
         cout << "Midterm(20%):";
         cin >> number2;
    }

    cout << "Final Exam(30%):";//prompt user to give a value for Final Exam(30%).
    cin >> number3;

    while (0 > number3 or number3 > 100)//check the range of the value, if the value isnt in the range of 0 ~100, prompt the user to enter again.
    {
         cout << "Out of Range !\n";
         cout << "Final Exam(30%):";
         cin >> number3;
    }

    cout << "Assignment(30%):";//prompt user to give a value for Assignment(30%).
    cin >> number4;

    while (0 > number4 or number4 > 100)//check the range of the value, if the value isnt in the range of 0 ~100, prompt the user to enter again.
    {
         cout << "Out of Range !\n";
         cout << "Assignment(30%):";
         cin >> number4;
    }


    answer = (number1 + number2)*0.2 + (number3 +number4)*0.3;//compute the Final Grades.
    cout << "Final Grades:" << answer << "分" << endl;//show the Final Grades to user.

    system ("pause");

    return 0 ;


}
