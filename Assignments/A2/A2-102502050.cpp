#include<iostream>
using namespace std ;
int main()
{
    string name[4]={"quiz(20%):","Midterm(20%):","Final Exam(30%):","Assignment(30%):"};
     //將要輸出的文字存入陣列
    int    percent[4]={20,20,30,30};
     //所佔的比例
    double grade = 0;   //用來輸入成績
    int sum = 0 ;    //用來計算最終成績
    int index = 0;
    for(;index<4;)
    {
        cout << name[index] ;
        cin >> grade ;
        if(grade<0 || grade>100)
            cout << "Out of range!!\n" ;                       //若超出範為則index不動
        else
        sum += grade * percent[index++];                       //依照比例加入總和，並讓index加一
    }

    if( sum % 100 == 0 )
        cout << "Final grade: " <<sum/ 100 << "分";                    //若計算結果為整數，則輸出整數
    else
        cout << "Final grade: " <<sum/ 100 << "." << sum% 100 << "分";  //若計算結果有小數，則輸出至小數點後第二位，其餘捨棄
    return 0 ;
}
