#include <iostream>
using namespace std;

int main()

{
    int quiz=0;//宣告型別為整數(int)的quiz，並初始化其數值為0
    int Midterm=0;//宣告型別為整數(int)的Midterm，並初始化其數值為0
    int FinalExam=0;//宣告型別為整數(int)的FinalExam，並初始化其數值為0
    int Assignment=0;//宣告型別為整數(int)的Assignment，並初始化其數值為0
    int Finalgrade=0;//宣告型別為整數(int)的Finalgrade，並初始化其數值為0

    cout<<"quiz(20%):"<<endl;
    cin>>quiz;
    while(quiz<0 or quiz>100)//分數需介於0~100之間
    {
        cout<<"Out of range"<<endl<<"quiz(20%):";//分數不是介於0~100之間'顯示Out of range
        cin>>quiz;
    }

    cout<<"Midterm(20%):"<<endl;
    cin>>Midterm;

    while(Midterm<0 or Midterm>100)//分數需介於0~100之間
    {
        cout<<"Out of range"<<endl<<"Midterm(20%):";//分數不是介於0~100之間'顯示Out of range
        cin>>Midterm;
    }

    cout<<"Final Exam(30%):"<<endl;
    cin>>FinalExam;

    while(FinalExam<0 or FinalExam>100)//分數需介於0~100之間
    {
        cout<<"Out of range"<<endl<<"Final Exam(30%):";//分數不是介於0~100之間'顯示Out of range
        cin>>FinalExam;
    }

    cout<<"Assignment(30%):"<<endl;
    cin>>Assignment;

    while(Assignment<0 or Assignment>100)//分數需介於0~100之間
    {
        cout<<"Out of range"<<endl<<"Assignment(30%):";//分數不是介於0~100之間'顯示Out of range
        cin>>Assignment;
    }

    Finalgrade=quiz*0.2+Midterm*0.2+FinalExam*0.3+Assignment*0.3;//計算Finalgrade
    cout<< "Final Grade:" << Finalgrade;

    return 0;
}
