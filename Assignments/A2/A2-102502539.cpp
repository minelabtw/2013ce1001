#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int quiz = 0 , midterm = 0 , final_exam = 0 , assignment = 0; //宣告四個變數

    cout << "Quiz(20%): " ;
    cin >> quiz;
    while ( quiz > 100 || quiz < 0 ) //迴圈
    {
        cout << "Out of range!\n" << "Quiz(20%): " ;
        cin >> quiz;
    }

    cout << "Midterm(20%): " ;
    cin >> midterm;
    while ( midterm > 100 || midterm < 0 )
    {
        cout << "Out of range!\n" << "Midterm(20%): " ;
        cin >> midterm;
    }


    cout << "Final Exam(30%): " ;
    cin >> final_exam;
    while ( final_exam > 100 || final_exam < 0 )
    {
        cout << "Out of range!\n" << "Final Exam(30%): " ;
        cin >> final_exam;
    }

    cout << "Assignment(30%): " ;
    cin >> assignment;
    while ( assignment > 100 || assignment < 0 )
    {
        cout << "Out of range!\n" << "Assignment(30%): " ;
        cin >> assignment;
    }

    system ("CLS"); //清除畫面

    cout << "Final Grade: " << (double) 0.2 * ( quiz + midterm ) + 0.3 * ( final_exam + assignment ) << "分" ; //運算並輸出

    return 0;
}
