#include <iostream>

using namespace std;

int main()
{
    int quiz = 0; //宣告型別為int，並初始化其數值為0
    int midterm = 0; //宣告型別為int，並初始化其數值為0
    int f_exam = 0; //宣告型別為int，並初始化其數值為0
    int assignment = 0; //宣告型別為int，並初始化其數值為0
    int sum = 0; //宣告型別為int，並初始化其數值為0

    cout << "quiz(20%):";
    cin >> quiz;

    while ( quiz<0 or quiz >100 ) //若quiz非0到100間的整數則輸出Out of range!!
        {cout << "Out of range!!" << endl;
        cout << "quiz(20%):";
        cin >> quiz;
        }

    cout << "Midterm(20%):";
    cin >> midterm;

    while ( midterm<0 or midterm >100 ) //若midterm非0到100間的整數則輸出Out of range!!
        {cout << "Out of range!!" << endl;
        cout << "Midterm(20%):";
        cin >> midterm;
        }

    cout << "Final Exam(30%):";
    cin >> f_exam;

    while ( f_exam<0 or f_exam>100 ) //若f_exam非0到100間的整數則輸出Out of range!!
        {cout << "Out of range!!" << endl;
        cout << "Midterm(20%):";
        cin >> midterm;
        }

    cout << "Assignment(30%):";
    cin >> assignment;

    while ( assignment<0 or assignment>100 ) //若assignment非0到100間的整數則輸出Out of range!!
        {cout << "Out of range!!" << endl;
        cout << "Assignment(30%):";
        cin >> assignment;
        }

    sum = (quiz+midterm)*0.2 + (f_exam+assignment)*0.3; //計算平均
    cout << "Final grade: " << sum << "分";

    return 0;
}
