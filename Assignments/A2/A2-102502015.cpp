#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    float quiz = 0;                                 //宣告各個成績 並定義為0
    float Midterm = 0;
    float FinalExam = 0;
    float Assignment = 0;
    float Finalgrade = 0;
    cout<<"quiz(20%):";                             //輸入quiz成績
    cin>>quiz;
    while (!(quiz>=0 && quiz <=100))                //判斷quiz 是否超過範圍 若超過範圍則進行迴圈
    {
        cout<<"Out of range!!"<<endl;
        cout<<"quiz(20%):";
        cin>>quiz;
    }
    cout<<"Midterm(20%):";                          //輸入Midterm成績
    cin>>Midterm;
    while (!(Midterm>=0 && Midterm <=100))          //判斷Midterm是否超過範圍 若超過範圍則進行迴圈
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Midterm(20%):";
        cin>>Midterm;
    }
    cout<<"FinalExam(30%):";                        //輸入FinalExam成績
    cin>>FinalExam;
    while (!(FinalExam>=0 && FinalExam <=100))      //判斷FinalExam是否超過範圍 若超過範圍則進行迴圈
    {
        cout<<"Out of range!!"<<endl;
        cout<<"FinalExam(30%):";
        cin>>FinalExam;
    }
    cout<<"Assignment(30%):";                       //輸入Assignment成績
    cin>>Assignment;
    while (!(Assignment>=0 && Assignment <=100))    //判斷Assignment是否超過範圍 若超過範圍則進行迴圈
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Assignment(30%):";
        cin>>Assignment;
    }
    Finalgrade = quiz * 0.2 + Midterm * 0.2 + FinalExam * 0.3 + Assignment * 0.3;   //計算最後總成績
    cout<<"Final grade: "<<Finalgrade<<"分";
    return 0 ;
}


dd

