#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int grade1=0;         //宣告名稱為grade1的變數
    int grade2=0;         //宣告名稱為grade2的變數
    int grade3=0;         //宣告名稱為grade3的變數
    int grade4=0;         //宣告名稱為grade4的變數
    int sum= 0;           //宣告名稱為sum變數

    cout << "quiz(20%):";
    cin >> grade1;
    while ( grade1 > 100 || grade1 < 0 )               //迴圈,條件為grade1>100或<0
    {
        cout << "Out of range!!\n""quiz(20%):";        //輸出字串"Out of range!!",並換行再次輸出quiz(20%):
        cin >> grade1;                                 //再次輸入grade1
    }

    cout << "Midterm(20%):";
    cin >> grade2;
    while ( grade2 > 100 || grade2 < 0 )               //迴圈,條件為grade2>100或<0
    {
        cout << "Out of range!!\n""Midterm(20%):";     //輸出字串"Out of range!!",並換行再次輸出Midterm(20%):
        cin >> grade2;                                 //再次輸入grade2
    }

    cout << "Final Exam(30%):";
    cin >> grade3;
    while ( grade3 > 100 || grade3 < 0 )               //迴圈,條件為grade3>100或<0
    {
        cout << "Out of range!!\n""Final Exam(30%):";  //輸出字串"Out of range!!",並換行再次輸出Final Exam(30%):
        cin >> grade3;                                 //再次輸入grade3
    }

    cout << "Assignment(30%):";
    cin >> grade4;
    while ( grade4 > 100 || grade4 < 0 )               //迴圈,條件為grade4>100或<0
    {
        cout << "Out of range!!\n""Assignment(30%):";  //輸出字串"Out of range!!",並換行再次輸出Assignment(30%):
        cin >> grade4;                                 //再次輸入grade4
    }

    sum = grade1*0.2 + grade2*0.2 + grade3*0.3 + grade4*0.3;   //將grade1和grade2乘上0.2,grade3和grade4乘上0.3,並將得到的四個結果加起來丟給Final grade
    cout << "Final grade:" << sum << endl;

    return 0;

}
