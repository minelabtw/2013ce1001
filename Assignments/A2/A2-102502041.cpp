#include <iostream>                         //將輸入/輸出函式庫含括進來
using namespace std;                        //指名使用std這個名稱空間
int main(void)
{
    float quiz;                             //宣告浮點數quiz
    float Midterm;                          //宣告浮點數Midterm
    float Final_Exam;                       //宣告浮點數Final_Exam
    float Assignment;                       //宣告浮點數Assignment
    float Final_grade;                      //宣告浮點數Final_grade

    cout <<"quiz(20%):";                    //輸出字串"quiz(20%):"
    cin >> quiz;                            //請求輸入一個值quiz
    while(quiz>100||quiz<0)                 //若quiz不在0~100之間,即執行迴圈內之指令
    {
        cout <<"Out of range!!"<<endl;      //輸出字串"Out of range!!"告知其輸入的quiz超出範圍
        cout <<"quiz(20%):";
        cin >>quiz;
    }

    cout <<"Midterm(20%):";                 //輸出字串"Midterm(20%):"
    cin >>Midterm;                          //請求輸入一個值Midterm
    while(Midterm>100||Midterm<0)           //若Midterm不在0~100之間,即執行迴圈內之指令
    {
        cout <<"Out of range!!"<<endl;      //輸出字串"Out of range!!"告知其輸入的Midterm超出範圍
        cout <<"Midterm(20%):";
        cin >>Midterm;
    }

    cout <<"Final Exam(30%):";              //輸出字串"Final Exam(30%):"
    cin >>Final_Exam;                       //請求輸入一個值Final_Exam
    while(Final_Exam>100||Final_Exam<0)     //若Final_Exam不在0~100之間,即執行迴圈內之指令
    {
        cout <<"Out of range!!"<<endl;      //輸出字串"Out of range!!"告知其輸入的Final_Exam超出範圍
        cout <<"Final Exam(30%):";
        cin >>Final_Exam;
    }

    cout <<"Assignment(30%):";              //輸出字串"Assignment(30%):"
    cin >>Assignment;                       //請求輸入一個值Assignment
    while(Assignment>100||Assignment<0)     //若Assignment不在0~100之間,即執行迴圈內之指令
    {
        cout <<"Out of range!!"<<endl;      //輸出字串"Out of range!!"告知其輸入的Assignment超出範圍
        cout <<"Assignment(30%):";
        cin >>Assignment;
    }

    Final_grade = quiz*0.2 + Midterm*0.2 + Final_Exam*0.3 + Assignment*0.3; //按照既定比例進行運算並將其值丟回給Final_grade

    cout <<"Final grade:"<<Final_grade<<"分";                               //輸出字串"Final grade"以及Final_grade以及"分"
    return 0;


}
