#include <iostream>

using namespace std;

int main()
{
    int quiz = 0;                                                                   //宣告名稱為quiz整數變數
    int Midterm = 0;                                                                //宣告名稱為Midterm整數變數
    int FinalExam = 0;                                                              //宣告名稱為inalExam整數變數
    int Assignment = 0;                                                             //宣告名稱為Assignment整數變數
    int sum = 0;                                                                    //宣告名稱為sum整數變數

    cout << "quiz(20%):";                                                           //輸出字串quiz(20%):
    cin >> quiz;                                                                    //輸入quiz
    while (quiz<0||quiz>100){                                                       //判斷quiz是否符合範圍
        cout << "Out of range!!" << endl;                                           //輸出字串Out of range!!
        cout << "quiz(20%):";                                                       //輸出字串quiz(20%):
        cin >> quiz;                                                                //輸入quiz
    }

    cout << "Midterm(20%):";                                                        //輸出字串Midterm(20%):
    cin >> Midterm;                                                                 //輸入Midterm
    while (Midterm<0||Midterm>100){                                                 //判斷Midterm是否符合範圍
        cout << "Out of range!!" << endl;                                           //輸出字串Out of range!!
        cout << "Midterm(20%):";                                                    //輸出字串Midterm(20%):
        cin >> Midterm;                                                             //輸入Midterm
    }

    cout << "Final Exam(30%):";                                                     //輸出字串Final Exam(30%):
    cin >> FinalExam;                                                               //輸入FinalExam
    while (FinalExam<0||FinalExam>100){                                             //判斷FinalExam是否符合範圍
        cout << "Out of range!!" << endl;                                           //輸出字串Out of range!!
        cout << "Final Exam(30%):";                                                 //輸出字串Final Exam(30%):
        cin >> FinalExam;                                                           //輸入FinalExam
    }

    cout << "Assignment(30%):";                                                     //輸出字串Assignment(30%):
    cin >> Assignment;                                                              //輸入Assignment
    while (Assignment<0||Assignment>100){                                           //判斷Assignment是否符合範圍
        cout << "Out of range!!" << endl;                                           //輸出字串Out of range!!
        cout << "Assignment(30%):";                                                 //輸出字串Assignment(30%):
        cin >> Assignment;                                                          //輸入Assignment
    }

    sum = (quiz * 0.2) + (Midterm * 0.2) + (FinalExam * 0.3) + (Assignment * 0.3);  //將quiz*0.2加上Midterm*0.2加上FinalExam*0.3加上Assignment*0.3再將結果放至sum
    cout << "Final grade:" << sum << "分" << endl;                                  //輸出Final grade為幾分

    return 0;
}
