#include <iostream>
using namespace std;

int main()
{
    int quiz;                  //宣告名稱為quiz的整數變數
    int Midterm;               //宣告名稱為Midterm的整數變數
    int Final;                 //宣告名稱為Final的整數變數
    int Assignment;            //宣告名稱為Assignment的整數變數
    int grade;                 //宣告名稱為grade的整數變數

    cout << "quiz(20%):";      //將字串"quiz(20%):"輸出到螢幕上
    cin >> quiz;               //從鍵盤輸入，並將輸入的值給quiz
    while(quiz<0||quiz>100)            //使用while迴圈，若quiz數值小於0或大於100就會一直重複該程式碼內的動作
    {
        cout << "Out of range!!\n";    //將字串"Out of range!!"輸出到螢幕上並換行
        cout << "quiz(20%):";          //將字串"quiz(20%):"輸出到螢幕上
        cin >> quiz;                   //從鍵盤輸入，並將輸入的值給quiz
    }

    cout << "Midterm(20%):";   //將字串"Midterm(20%):"輸出到螢幕上
    cin >> Midterm;            //從鍵盤輸入，並將輸入的值給Midterm
    while(Midterm<0||Midterm>100)      //使用while迴圈，若Midterm數值小於0或大於100就會一直重複該程式碼內的動作
    {
        cout << "Out of range!!\n";    //將字串"Out of range!!"輸出到螢幕上並換行
        cout << "Midterm(20%):";       //將字串"Midterm(20%):"輸出到螢幕上
        cin >> Midterm;                //從鍵盤輸入，並將輸入的值給Midterm
    }

    cout << "Final Exam(30%):";//將字串"Final Exam(30%):"輸出到螢幕上
    cin >> Final;              //從鍵盤輸入，並將輸入的值給Final
    while(Final<0||Final>100)          //使用while迴圈，若Final數值小於0或大於100就會一直重複該程式碼內的動作
    {
        cout << "Out of range!!\n";    //將字串"Out of range!!"輸出到螢幕上並換行
        cout << "Final Exam(30%):";    //將字串"Final Exam(30%):"輸出到螢幕上
        cin >> Final;                  //從鍵盤輸入，並將輸入的值給Final
    }

    cout << "Assignment(30%):";//將字串"Assignment(30%):"輸出到螢幕上
    cin >> Assignment;         //從鍵盤輸入，並將輸入的值給Assignment
    while(Assignment<0||Assignment>100)//使用while迴圈，若Assignment數值小於0或大於100就會一直重複該程式碼內的動作
    {
        cout << "Out of range!!\n";    //將字串"Out of range!!"輸出到螢幕上並換行
        cout << "Assignment(30%):";    //將字串"Assignment(30%):"輸出到螢幕上
        cin >> Assignment;             //從鍵盤輸入，並將輸入的值給Assignment
    }

    grade=quiz*0.2+Midterm*0.2+Final*0.3+Assignment*0.3; //求出grade之值
    cout << "Final grade:" << grade << "分";             //將字串"Final grade:grade分"輸出到螢幕上
    return 0;
}

