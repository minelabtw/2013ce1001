#include <iostream>

using namespace std;

int main ()

{
    int quiz = 0;
    int Midterm = 0;
    int FinalExam = 0;
    int Assignment = 0;
    int FinalGrade = 0;//以上為宣告所需使用的整數變數並歸零


    cout << "quiz(20%):" ;//顯示quiz(20%):
    cin >> quiz ;//輸入quiz之值
    while ( quiz < 0 || quiz > 100 )//設定條件quiz小於0或大於100
    {
        cout << "Out of range!" << endl ;//顯示Out of range!
        cout << "quiz(20%):" ;//再次顯示quiz(20%)
        cin >> quiz ;//再次輸入quiz之值
    }//若quiz符合條件能夠在顯示Out of range!之後重新輸入


    cout << "Midterm(20%):" ;//顯示Midterm(20%):
    cin >> Midterm ;//輸入Midterm之值
    while (Midterm < 0 || Midterm > 100)//設定條件Midterm小於0或大於100
    {
        cout << "Out of range!" << endl ;//顯示Out of range!
        cout << "Midterm(20%):" ;//再次顯示Midterm(20%)
        cin >> Midterm ;//再次輸入Midterm之值
    }//若Midterm符合條件能夠在顯示Out of range!之後重新輸入


    cout << "Final Exam(30%):" ;//顯示Final Exam(30%):
    cin >> FinalExam ;//輸入FinalExam之值
    while ( FinalExam < 0 || FinalExam > 100 )//設定條件FinalExam小於0或大於100
    {
        cout << "Out of range!" << endl ;//顯示Out of range!
        cout << "Final Exam(30%):" ;//再次顯示Final Exam(30%)
        cin >> FinalExam ;//再次輸入FinalExam之值
    }//若FinalExam符合條件能夠在顯示Out of range!之後重新輸入


    cout << "Assignment(30%):" ;//顯示Assignment(30%):
    cin >> Assignment ;//輸入Assignment之值
    while ( Assignment < 0 || Assignment > 100 )//設定條件Assignment小於0或大於100
    {
        cout << "Out of range!" << endl ;//顯示Out of range!
        cout << "Assignment(20%):" ;//再次顯示Assignment(30%)
        cin >> Assignment ;//再次輸入Assignment之值
    }//若Assignment符合條件能夠在顯示Out of range!之後重新輸入


    FinalGrade = ( quiz*0.2 ) + ( Midterm*0.2 ) + ( FinalExam*0.3 ) + ( Assignment*0.3 );//計算總成績
    cout << "Final Grade:" << FinalGrade << "分";//顯示Final Grade:並輸出總成績





    return 0;
}
