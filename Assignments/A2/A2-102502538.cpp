#include<iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    double x,y,z,w;
    double sum = 0;

    cout << "quiz(20%):";                    //輸入quiz成績
    cin >> x;                                //判別是否介於1~100
    while(x>100 || x<0)
    {
        cout <<"Out of Range!!"<<endl<<"quiz(20%):";
        cin >>x;
    }

    cout << "Midterm(20%):";                 //輸入Midterm成績
    cin >> y;
    while(y>100 || y<0)                      //判別是否介於1~100
    {
        cout <<"Out of Range!!"<<endl<<"Midterm(20%):";
        cin >>y;
    }

    cout << "Final Exam(30%):";              //輸入Final Exam成績
    cin >> z;
    while(z>100 || z<0)                      //判別是否介於1~100
    {
        cout <<"Out of Range!!"<<endl<<"Final Exam(30%):";
        cin >>z;
    }

    cout << "Assessment(30%):";              //輸入Assessment成績
    cin >>w;
    while(w>100 || w<0)                      //判別是否介於1~100
    {
        cout <<"Out of Range!!"<<endl<<"Assessment(30%):";
        cin >>w;
    }

    sum = x*0.2 + y*0.2 + z*0.3 + w*0.3;
    cout << "Final grade:" <<sum;            //輸出Final grade

    return 0;

}
