#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{

    double a,b,c,d,total; //宣告型別為倍精度浮點數(double)的a,b,c,d,total。
    total = 0; //初始化total的數值為0。


    cout << "quiz(20%):";
    cin >> a;
    while (a < 0 || a > 100) //loop.to check the data whether is on demand.
    {
        cout << "Out of range!" << endl << "quiz(20%):"  ;
        cin >> a;
    }


    cout << "Midterm(20%):";
    cin >> b;
    while (b < 0 || b > 100) //loop.to check the data whether is on demand.
    {
        cout << "Out of range!" << endl << "Midterm(20%):";
        cin >> b;
    }

    cout << "Final Exam(30%):";
    cin >> c;
    while (c < 0 || c > 100) //loop.to check the data whether is on demand.
    {
        cout << "Out of range!" << endl << "Final Exam(30%):";
        cin >> c;
    }

    cout << "Assignment(30%):";
    cin >> d;
    while (d < 0 || d > 100) //loop.to check the data whether is on demand.
    {
        cout << "Out of range!" << endl << "Assignment(30%):";
        cin >> d;
    }

    total = (a*0.2 + b*0.2 + c*0.3 + d*0.3); //to count total.



    cout << "Final grade:" << total << endl;
    return 0;

}

