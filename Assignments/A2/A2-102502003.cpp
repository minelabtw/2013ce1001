#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int grade1 = 0;  //宣告變數，其初始值為0。
    int grade2 = 0;  //宣告變數，其初始值為0。
    int grade3 = 0;  //宣告變數，其初始值為0。
    int grade4 = 0;  //宣告變數，其初始值為0。
    int sum = 0;  //宣告變數，其初始值為0。

    cout << "Quiz(20%):";
    cin >> grade1;

    while(1)  //while迴圈規範輸入字元。
    {
        if( grade1 <= 100 and grade1 >= 0 )  //if條件式判定數字是否合理
        {
           break;  //符合條件，結束迴圈。
        }

        else
        cout << "Out of range!" << endl;
        cout << "Quiz(20%):";
        cin >> grade1;  //超出範圍，重新再輸入。

    }

    cout << "Midterm(20%):";
    cin >> grade2;

    while(1)  //while迴圈規範輸入字元。
    {
        if( grade2 <= 100 and grade2 >= 0 ) //判定數字是否在範圍內。
        {
           break;  //條件符合，結束迴圈。
        }

        else
        cout << "Out of range!" << endl;
        cout << "Midterm(20%)";
        cin >> grade2;  //重新輸入。

    }

    cout << "Final Exam(30%):";
    cin >> grade3;

    while(1)  //while迴圈規範輸入字元。
    {
        if( grade3 <= 100 and grade3 >= 0 )  //判定數字是否合理
        {
           break;  //符合條件，結束迴圈。
        }

        else
        cout << "Out of range!" << endl;
        cout << "Final Exam(30%):";
        cin >> grade3;  //重新輸入。

    }

    cout << "Assignment(30%):";
    cin >> grade4;

    while(1)  //規範輸入字元。
    {
        if( grade4 <= 100 and grade4 >= 0 )  //判定數字是否合理
        {
           break;  //符合條件，結束迴圈。
        }

        else
        cout << "Out of range!" << endl;
        cout << "Assignment(30%):";
        cin >> grade4;  //重新輸入。

    }

    sum = grade1 * 0.2 + grade2 * 0.2 + grade3 * 0.3 + grade4 * 0.3; //按照比例計算總成績。
    cout << "Final Grade:" << sum << "分" << endl;

    return 0;

}
