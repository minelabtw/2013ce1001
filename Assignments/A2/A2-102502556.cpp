#include <iostream>
using namespace std;

int main()
{
    double num1 = 0; //宣告型別為 倍精度浮動小數點數型(double) 的第一個數字，並初始化其數值為0。
    cout << "quiz(20%):"; //使用cout指令使指定的文字輸出在螢幕上。
    cin >> num1; //使用鍵盤輸入第一個數字。
    while ( num1 < 0 || num1 > 100) //用while迴圈來判斷數字的合理性，如果不符，就要求使用者重新輸入。
    {
        cout << "Out of range!!" << endl;
        cout << "quiz(20%):";
        cin >> num1;
    }
    // 以下同上。
    double num2 = 0;
    cout << "Midterm(20%):";
    cin >> num2;
    while ( num2 < 0 || num2 > 100)
    {
        cout << "Out of range!!" << endl;
        cout << "Midterm(20%):";
        cin >> num2;
    }
    double num3 = 0;
    cout << "Final Exam(30%):";
    cin >> num3;
    while ( num3 < 0 || num3 > 100)
    {
        cout << "Out of range!!" << endl;
        cout << "Final Exam(30%):";
        cin >> num3;
    }
    double num4 = 0;
    cout << "Assignment(30%):";
    cin >> num4;
    while ( num4 < 0 || num4 > 100)
    {
        cout << "Out of range!!" << endl;
        cout << "Assignment(30%):";
        cin >> num4;
    }
    double num5 = 0;
    num5 = num1 * 0.2 + num2 * 0.2 + num3 * 0.3 + num4 * 0.3; //使用num5來儲存分數計算後的結果。
    cout << "Final grade:" << num5 << "分" << endl; //使最後的總成績輸出在螢幕上。

    return 0;
}
