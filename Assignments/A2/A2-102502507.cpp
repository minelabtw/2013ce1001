#include <iostream>
using namespace std;
int main()
{
    float quiz = 0;
    float Midterm = 0;
    float FinalExam = 0;
    float Assignment = 0;
    cout<<"quiz(20%):";//輸入第一個成績
    cin>>quiz;
    while (quiz<0 or quiz>100)//當第一個成績<0或>100時,執行以下步驟
    {
        cout<<"Out of range!!"<<endl<<"quiz(20%):";//迴圈定理
        cin>>quiz;
    }
    cout<<"Midterm(20%):";//輸入第二次成績
    cin>>Midterm;
    while (Midterm<0 or Midterm>100)//當第二個成績<0或>100時,執行以下步驟
    {
        cout<<"Out of range!!"<<endl<<"Midterm(20%):";//迴圈定理
        cin>>Midterm;
    }
    cout<<"Final Exam(30%):";//輸入第三次成績
    cin>>FinalExam;
    while(FinalExam<0 or FinalExam>100)//當第三個成績<0或>100時,執行以下步驟
    {
        cout<<"Out of range!!"<<endl<<"FinalExam(30%):";//迴圈定理
        cin>>FinalExam;
    }
    cout<<"Assignment(30%):";//輸入第四次成績
    cin>>Assignment;
    while (Assignment<0 or Assignment>100)//當第四個成績<0或>100時,執行以下步驟
    {
       cout<<"Out of range!!"<<endl<<"Assignment(30%):";//迴圈定理
       cin>>Assignment;
    }
    cout<<"Final grade:"<<quiz * 0.2 +Midterm * 0.2 +FinalExam * 0.3 +Assignment *0.3 << "分" <<endl;//最後總成績結算
    return 0;
}
