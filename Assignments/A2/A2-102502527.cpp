#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int quiz = 0,Midterm = 0,FinalExam = 0,Assignment = 0;//宣告型別為quiz,Midterm,FinalExam,Assignment並初始化其數值為0
    int sum = 0;//宣告型別sum,並初始化其數值為0

    cout << "quiz(20%):";
    cin >> quiz;

    while ( quiz<0 || quiz>100 )//辨別quiz的值是否在範圍內
    {
        cout<<"out of range"<<endl;//顯示字樣
        cin>>quiz;
    }

    cout << "Midterm(20%):";
    cin >> Midterm;

    while ( Midterm<0 || Midterm>100 )//辨別Midterm的值是否在範圍內
    {
        cout<<"out of range"<<endl;//顯示字樣
        cin>>quiz;
    }

    cout << "Final Exam(30%):";
    cin >> FinalExam;

    while ( FinalExam<0 || FinalExam>100 )//辨別FinalExam的值是否在範圍內
    {
        cout<<"out of range"<<endl;//顯示字樣
        cin>>FinalExam;
    }

    cout << "Assignment(30%):";
    cin >> Assignment;

    while ( Assignment<0 || Assignment>100 )//辨別Assignment的值是否在範圍內
    {
        cout<<"out of range"<<endl;//顯示字樣
        cin>>Assignment;
    }

    sum = quiz * 0.2 + Midterm * 0.2 + FinalExam * 0.3 + Assignment * 0.3;//計算總分
    cout << "Final grade=" << sum << "分";

    return 0;
}
