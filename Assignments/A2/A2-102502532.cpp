#include<iostream>
using namespace std;

int main()
{
    double quiz =0;      //宣告型別為 double，初始化其數值為0
    double Midterm =0;
    double Final =0;
    double Assignment =0;
    double grade =0;

    cout <<"Quiz(20%):";
    cin >> quiz;
    while ( quiz > 100 or quiz < 0)       //分數介於0~100之間
    {
        cout <<"Out of range!!"<<endl;    //分數不是介於0~100之間，顯示Out of range!!
        cout <<"Quiz(20%):";
        cin >> quiz;
    }


    cout <<"Midterm(20%):";
    cin >> Midterm;
    while ( Midterm > 100 or Midterm < 0 )   //分數介於0~100之間
    {
        cout <<"Out of range!!"<<endl;       //分數不是介於0~100之間，顯示Out of range!!
        cout <<"Midterm(20%)";
        cin >> Midterm;
    }

    cout <<"Final Exam(30%):";
    cin >> Final;
    while ( Final > 100 or Final < 0 )     //分數介於0~100之間
    {
        cout <<"Out of range!!"<<endl;     //分數不是介於0~100之間，顯示Out of range!!
        cout <<"Final Exam(30%):";
        cin >> Final;
    }


    cout <<"Assignment(30%):";
    cin >> Assignment;
    while ( Assignment > 100 or Assignment < 0 )    //分數介於0~100之間
    {
        cout <<"Out of range!!"<<endl;              //分數不是介於0~100之間，顯示Out of range!!
        cout <<"Assignment(30%):";
        cin >> Assignment;
    }

    grade = quiz*0.2+ Midterm*0.2+ Final*0.3+ Assignment*0.3;    //算出最後總成績
    cout <<"Final Grade: "<< grade <<"分";

    return 0;

}

