#include<iostream>
using namespace std ;

int main()
{
    float score=0 ; //define score is a real number
    float total=0 ; //total is sum of all total
    int time=0 ; //讀取的次數

    while(time<=3) //repeat write read and decise 4 times
    {
       if (time==0) //first time
          cout << "quiz(20%):" ;
       else if(time==1) //secend time
          cout << "Midterm(20%):" ;
       else if(time==2) //third time
          cout << "Final Exam(30%):" ;
       else //last time
          cout << "Assignment(30%):" ;

       cin >> score ; // read a number in score
       if(score>100 || score<0 ) //judge score is 0~100 ? if not
       {
           cout << "Out of range!!\n" ;
           continue ; // if not the fowlling are not run
       }
       // if score is 0~100 run foelling
       if (time==0)                     // first time
          total = total + score * 0.2 ; // score (20%)
       else if(time==1)                 // secend time
          total = total + score * 0.2 ; // score (20%)
       else if(time==2)                 // third time
          total = total + score * 0.3 ; // score (30%)
       else                             // last time
          total = total + score * 0.3 ; // score (30%)

       time++ ;                         //to next time
    }
    cout << "Final grade: " << total << "分\n" ; //print total score
    return 0;
}
