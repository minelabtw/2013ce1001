#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int quiz = 0,Midterm = 0,Final_exam =0 ,Assignment = 0 , Final_grade = 0;//宣告型別為整數的五個數,並初始化其值為零

    cout << "quiz(20%):";//顯示quiz的比例為20%
    cin >> quiz;
    if (quiz > 100 or quiz < 0 )//設定輸入的值域大於等於零或是小於等於一百,否則會出現Out of range之字樣
    {
        cout << "Out of range!"<<endl;//利用endl換行
        cout << "quiz(20%):";
        cin >> quiz;
    }


    cout << "Midterm(20%):";//顯示Midterm的比例為20%
    cin >> Midterm;
    if (Midterm > 100 or Midterm < 0 )
    {
        cout << "Out of range!"<<endl;
        cout << "Midterm(20%):";
        cin >> Midterm;
    }


    cout << "Final_exam(30%):";//顯示Final_exam的比例為30%
    cin >> Final_exam;
    if (Final_exam > 100 or Final_exam < 0)
    {
        cout << "Out of range!"<<endl;
        cout << "Final_exam(30%):";
        cin >> Final_exam;
    }

    cout << "Assignment(30%):";//顯示Assignment的比例為30%
    cin >> Assignment;
    if (Assignment > 100 or Assignment < 0 )
    {
        cout << "Out of range!"<<endl;
        cout << "Assignment(30%):";
        cin >> Assignment;
    }

    Final_grade = quiz * 0.2 + Midterm * 0.2 + Final_exam * 0.3 + Assignment * 0.3;//設定Final_grade的值為各分數比例相加
    cout << "Final_grade=" << Final_grade;

    return 0;

}
