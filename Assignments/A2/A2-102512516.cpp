#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int quiz;       //宣告小考的變數
    int midterm;    //宣告期中考變數
    int final_exam; //宣告期末考變數
    int assigment;  //宣告作業的分數
    int final_grade;//這是加權平均的總成績


    cout << "quiz(20%):";
    cin >> quiz;
    //請使用者輸入小考分數，並把分數存到quiz變數中
    while (quiz>100 or quiz < 0)
    {
        cout << "Out of range!!" << endl;
        cout << "quiz(20%):";
        cin >>quiz;
    }
    //並判斷使用者輸入的數值是否介於0~100中間。若不是，則顯示超過範圍的訊息，並讓使用者重新輸入


    cout << "Midterm(20%):";
    cin >> midterm;
    //讓使用者輸入其中考成績，並把成績存到midterm中
    while (midterm>100 or midterm < 0)
    {
        cout << "Out of range!!" << endl ;
        cout << "Midterm(20%):";
        cin >>midterm;
    }
    //判斷輸入資料是否在100~0中間，否則提示使用者錯誤並重新輸入


    cout << "Final Exam(30%):";
    cin >> final_exam;
    //讓使用者輸入期末考成績，並把它存到final_exam的變數中
    while (final_exam>100 or final_exam< 0)
    {
        cout << "Out of range!!" << endl;
        cout << "Final Exam(30%):";
        cin >>final_exam;
    }
    //判斷輸入的數值是否介於100~0，否則提示使用者錯誤並重新輸入



    cout << "Assigment(30%):";
    cin >> assigment;
    //最後讓使用者輸入作業的成績
    while (assigment>100 or assigment < 0)
    {
        cout << "Out of range!!" << endl;
        cout << "Assigment(30%):";
        cin >>assigment;
    }
    //如果輸入的成績大於100或小於0就顯示錯誤訊息並重新輸入


    final_grade = quiz*0.2+midterm*0.2+final_exam*0.3+assigment*0.3;
    //進行總成績的加權

    cout << endl << "Final grade:" << final_grade << " 分";
    //輸出總成績的分數


    return 0;



}
