#include <iostream>
using namespace std;

int main()
{
    double quiz, mid, final, assign, sum, check;

    cout << "quiz(20%):";
    cin >> quiz;//Giving initial, so that the while loop effect
    while (quiz > 100 || quiz < 0)//Check wether the score is in the range.
    {
        cout << "Out of range!!" << endl;//If it is not in the range, give a warnning.
        cout << "quiz(20%):";
        cin >> quiz;
    }
    //Same procedure as quiz
    cout << "Midterm(20%):";
    cin >> mid;
    while (mid > 100 || mid < 0)
    {
        cout << "Out of range!!" << endl;
        cout << "Midterm(20%):";
        cin >> mid;
    }
    //Same procedure as quiz
    cout << "Final Exam(30%):";
    cin >> final;
    while (final > 100 || final < 0)
    {
        cout << "Out of range!!" << endl;
        cout << "Final Exam(30%):";
        cin >> final;
    }
    //Same procedure as quiz
    cout << "Assignment(30%):";
    cin >> assign;
    while (assign > 100 || assign < 0)
    {
        cout << "Out of range!!" << endl;
        cout << "Assignment(30%)";
        cin >> assign;
    }
    cout << "Final grade: " << (quiz*20 + mid*20 + final*30 + assign*30)/100 << "分";
    return 0;
}
