#include <iostream>

using namespace std;

/*
Set class.
I shuold not use cin/cout in constructor,but it doesn't matter.
*/
class teachersTable
{
private:
    //No any reason to use private,just review how to use.
    string name;
    int rati;
    double score;
public:
    double finalCalc()//Function to calculate final score.
    {
        return score*rati/100;
    };
    teachersTable(string text,int num)//Get name and ratio of scores.
    {
        name = text;//Score name.
        rati = num;//Score ratio.
        /*
        Loop ctrl,idea form Linux/Unix init.d
        0 => break
        1 => init
        2 => continue
        */
        int loop = 1;
        while(loop)
        {
            if(loop == 2) cout << "Out of range!!\n";
            cout << text << "(" << rati << "):";
            cin >> score;
            loop = (score > 100 or score < 0)?2:0;
        };
    };
};
int main(void)
{
    teachersTable quiz("quiz",20);
    teachersTable midt("Midterm",20);
    teachersTable fina("Final Exam",30);
    teachersTable assi("Assignment",30);
    cout << "Final grade: "<< quiz.finalCalc() + midt.finalCalc() + fina.finalCalc() + assi.finalCalc() << "分";//Expert.
    return 0;
}
