#include <iostream>

using namespace std;

int main()
{
    int score1=0,score2=0,score3=0,score4=0;       //4個成績
    bool flag=true;                                //flag表示是輸入否有在範圍內

    do
    {
       cout<<"quiz(20%):";
       cin>>score1;                                //輸入score1
       if(score1<0 || score1>100)                  //若在範圍外
       {
           flag=false;                                //將flag設為false
           cout<<"Out of range!!"<<endl;              //印出提示

       }
       else                                        //否則
       {
           flag=true;                                 //將flag設為true
       }
    }
    while(!flag);

    do
    {
       cout<<"Midterm(20%):";
       cin>>score2;
       if(score2<0 || score2>100)
       {
           flag=false;
           cout<<"Out of range!!"<<endl;

       }
       else
       {
           flag=true;
       }
    }
    while(!flag);

    do
    {
       cout<<"Final Exam(30%):";
       cin>>score3;
       if(score3<0 || score3>100)
       {
           flag=false;
           cout<<"Out of range!!"<<endl;

       }
       else
       {
           flag=true;
       }
    }
    while(!flag);

    do
    {
       cout<<"Assignment(30%):";
       cin>>score4;
       if(score4<0 || score4>100)
       {
           flag=false;
           cout<<"Out of range!!"<<endl;

       }
       else
       {
           flag=true;
       }
    }
    while(!flag);

    cout<<"Final grade: "<<score1*0.2+score2*0.2+score3*0.3+score4*0.3<<"分";   //加總並顯示結果

        return 0;
}
