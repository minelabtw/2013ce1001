#include <iostream>
using namespace std;

int main()
{
    int qui = 0;                                     // 設定五個變數qui,mid,fin,ass,gra，初始直皆設為0
    int mid = 0;
    int fin = 0;
    int ass = 0;
    int gra = 0;

    cout << "quiz(20%):";
    cin >> qui;                                     // 輸入quiz成績
    while ( qui < 0 || qui > 100 )                  // 如果輸入的值不在0~100之間，則顯示Out of range!!，並重新輸入quiz成績
    {
        cout << "Out of range!!" << endl;
        cout << "quiz(20%):";
        cin >> qui;
    }

    cout << "Midterm(20%):";
    cin >> mid;                                     // 輸入Midterm成績
    while ( mid < 0 || mid > 100 )                  // 如果輸入的值不在0~100之間，則顯示Out of range!!，並重新輸入Midterm成績
    {
        cout << "Out of range!!" << endl;
        cout << "Midterm(20%):";
        cin >> mid;
    }

    cout << "Final Exam(30%):";
    cin >> fin;                                     // 輸入Final Exam成績
    while ( fin < 0 || fin > 100 )                  // 如果輸入的值不在0~100之間，則顯示Out of range!!，並重新輸入Final Exam成績
    {
        cout << "Out of range!!" << endl;
        cout << "Final Exam(30%):";
        cin >> fin;
    }


    cout << "Assignment(30%):";
    cin >> ass;                                     // 輸入Assignment成績
    while ( ass < 0 || ass > 100 )                  // 如果輸入的值不在0~100之間，則顯示Out of range!!，並重新輸入Assignment成績
    {
        cout << "Out of range!!" << endl;
        cout << "Assignment(30%):";
        cin >> ass;
    }
    gra = qui*0.2+mid*0.2+fin*0.3+ass*0.3;          // 計算總成績，並將值傳給變數gra
    cout << "Final grade:" << gra << "分" << endl;  // 輸出計算後的總分
}
