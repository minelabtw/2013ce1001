#include <iostream>

using  std::cout ;
using  std::cin ;
using  std::endl ;

int main()
{
    int quiz = 0 ;
    int Midterm = 0 ;
    int FinalExam = 0 ;
    int Assignment = 0 ;
    int FinalGrade = 0 ;   //宣告quiz Midterm FinalExam Assignment FinalGrade,並將其數值化為0

    cout << "quiz(20%):"  ; //輸入quiz之分數
    cin >> quiz ;

    while ( quiz > 100 || quiz < 0 )   //設定分數必須介於0~100之間,並且使用迴旋來設定
    {
        cout << "Out of range!!"  << endl ;   //為輸入超出範圍的結果,並要求重複輸入
        cout << "quiz(20%):"  ;
        cin >> quiz ;
    }

    cout  << "Midterm(20%):"  ;  //輸入Midterm之分數
    cin >> Midterm ;

    while ( Midterm > 100 || Midterm < 0)  //設定分數必須介於0~100之間,並且使用迴旋來設定
    {
        cout << "Out of range!!"  << endl ;   //為輸入超出範圍的結果,並要求重複輸入
        cout << "Midterm(20%):"  ;
        cin >> Midterm ;
    }

    cout << "Final Exam(30%):" ; //輸入FinalExam之分數
    cin >> FinalExam ;

    while ( FinalExam > 100 || FinalExam < 0 )  //設定分數必須介於0~100之間,並且使用迴旋來設定
    {
        cout << "Out of range!!"  << endl ;  //為輸入超出範圍的結果,並要求重複輸入
        cout << "Final Exam(30%):" ;
        cin >> FinalExam ;

    }

    cout << "Assignment(30%):" ; //輸入Midterm之分數
    cin >> Assignment ;

    while ( Assignment > 100 || Assignment < 0 )  //設定分數必須介於0~100之間,並且使用迴旋來設定
    {
        cout << "Out of range!!"  << endl ;  //為輸入超出範圍的結果,並要求重複輸入
        cout << "Assignment(30%)" ;
        cin >> Assignment ;
    }

    FinalGrade = quiz * 0.2 + Midterm * 0.2 + FinalExam * 0.3 + Assignment * 0.3 ; //期末成績算法 , 最後將輸入之數字運算為結果
    cout << "Final grade:" << FinalGrade ;







    return 0;
}
