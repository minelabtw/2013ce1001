/*************************************************************************
    > File Name: A2-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
	> Created Time: 西元2013年10月03日 (週四) 15時05分32秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

/************************************/
/*  str means an string for ui      */
/* *tmp means a point to save score */
/************************************/
void input(char *str, int *tmp)
{
	for(;;)
	{
		printf(str); //input score
		scanf("%d", tmp);

		if(0<=*tmp && *tmp<=100) //check input is in range [0, 100]
			break;
		else
			puts("Out of range!!");
	}
}

int main()
{
	/****************************/
	/* int qui means Quiz       */
	/* int mid means Midterm    */
	/* int fin means Final Exam */
	/* int ass means assignment */
	/****************************/
	int qui, mid, fin, ass;

	/****************************/
	/* int ans means final score*/
	/* init, set ans to 0       */
	/****************************/
	int ans = 0;

	/* input all variable */
	input("quiz(20%):",			&qui);
	input("Midterm(20%):",		&mid);
	input("Final Exam(30%):",	&fin);
	input("Assignment(30%):",	&ass);

	/* calc the final score */
	ans += qui * 20;
	ans += mid * 20;
	ans += fin * 30;
	ans += ass * 30;
	ans /= 100;
	
	//output final score
	printf("Final grade: %d分\n", ans); 

	return 0;
}

