#include<iostream>
using namespace std;

int main()
{
    double score1,score2,score3,score4;             //宣告變數score1,score2,score3,score4來接收接下來輸入的四個成績

    cout << "quiz(20%):";                           //將字串"quiz(20%):"輸出到螢幕上
    cin >> score1;                                  //將輸入分數的宣告給score1
    while(score1>100 | score1<0)                    //檢察分數是否在0~100之間，是則繼續程式，否則執行while中的處理辦法
    {
        cout << "Out of range!! \n";                //將"Out of range!!"輸出到螢幕上，並換行
        cout << "quiz(20%):";                       //再次將字串"quiz(20%):"輸出到螢幕上
        cin >> score1;                              //重新接收一次成績數據，如此不斷重複此動作直到數據在0~100之間
    }

    cout << "Midterm(20%):";                        //同上者執行方式改為"Midterm(20%):"，接收成績者變為score2
    cin >> score2;
    while(score2>100 | score2<0)
    {
        cout << "Out of range!! \n";
        cout << "Midterm(20%):";
        cin >> score2;
    }

    cout << "Final Exam(30%):";                     //同上者執行方式改為"Final Exam(30%):"，接收成績者變為score3
    cin >> score3;
    while(score3>100 | score3<0)
    {
        cout << "Out of range!! \n";
        cout << "Final Exam(30%):";
        cin >> score3;
    }

    cout << "Assignment(30%):";                     //同上者執行方式改為"Assignment(30%):"，接收成績者變為score4
    cin >> score4;
    while(score4>100 | score4<0)
    {
        cout << "Out of range!! \n";
        cout << "Assignment(30%):";
        cin >> score4;
    }


    cout << "Final grade: " << (score1*0.2)+(score2*0.2)+(score3*0.3)+(score4*0.3) << "分";
    //依照文字上的配分比例計算出平均成績，並將字串"Final grade: (計算結果)分"輸出到螢幕上


    return 0;
}



