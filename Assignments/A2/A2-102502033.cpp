#include <iostream>
 using namespace std;//讓STD::可省略
 int main()
 {
     int q1;//宣告整數變數代表小考
     int q2;//宣告整數變數代表期中考
     int q3;//宣告整數變數代表期末考
     int a1;//宣告整數變數代表作業
     int average;//宣告整數變數代表平均

     cout << "quiz(20%):";//輸出小考占20趴
     cin  >> q1;//輸入q1
     while(q1<0 || q1>100)//當q1小於0或q1大於100時
     {
       cout << "Out of range!!" << "\n";//代表你超過範圍並換行
       cout << "quiz(20%):";//輸出小考占20趴
       cin  >> q1;//輸入q1
     }


     cout << "Midterm(20%):";//輸出期中考占20趴
     cin  >> q2;//輸入q2
     while(q2<0 || q2>100)//當q2小於0或q2大於100時
     {
       cout << "Out of range!!" << "\n";//代表你超過範圍並換行
       cout << "Midterm(20%):";//輸出期中考占20趴
       cin  >> q2;//輸入q2
     }


     cout << "Final Exam(30%):";//輸出期末考占30趴
     cin  >> q3;//輸入q3
     while(q3<0 || q3>100)//當q3小於0或q3大於100時
     {
       cout << "Out of range!!" << "\n";//代表你超過範圍並換行
       cout << "Final Exam(30%):)";//輸出期末考占30趴
       cin  >> q3;//輸入q3
     }


     cout << "Assignment(30%):";//輸出作業占30趴
     cin  >> a1;//輸入a1
     while(a1<0 || a1>100)//當a1小於0或a1大於100時
     {
       cout << "Out of range!!" << "\n";//代表你超過範圍並換行
       cout << "Assignment(30%):";//輸出作業占30趴
       cin  >> a1;//輸入a1
     }


     average = q1*0.2+q2*0.2+q3*0.3+a1*0.3;//將算出的平均存入average
     cout << "Final Grade:" << average << "分" <<"\n";//輸出平均值

     return 0;

 }
