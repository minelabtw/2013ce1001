#include <iostream>
using namespace std ;

int main()
{
    float quiz=0 ; //宣告 quiz 變數
    float Midterm=0 ; //宣告 Midterm 變數
    float FinalExam=0 ; //宣告 FinalExam 變數
    float Assignment=0 ; //宣告 Assignment 變數

    cout << "quiz(20%):"  ; //輸出 quiz(20%): 到螢幕
    cin >> quiz ; //輸入quiz
    while ( quiz < 0 or quiz > 100 ) //迴圈( 當 quiz 小於0 或 quiz 大於100 )
    {
        cout << "Out of range !!" << endl << "quiz(20%):" ; //輸出 Out of range !! 換行 在輸出 quiz(20%):
        cin >> quiz ; //輸入 quiz
    }

    cout << "Midterm(20%):" ; //輸出 Midterm(20%): 到螢幕
    cin >> Midterm ; //輸入 Midterm
    while ( Midterm < 0 or Midterm > 100 ) //迴圈( 當 Midterm 小於0 或 Midterm 大於100 )
    {
        cout << "Out of range !!" << endl << "Midterm(20%:" ; //輸出 Out of range !! 換行 在輸出 Midterm(20%):
        cin >> Midterm ; //輸入 Midterm
    }
    cout << "FinalExam(30%):" ; //輸出 FinalExam(30%): 到螢幕
    cin >> FinalExam ; //輸入 FinalExam
    while ( FinalExam < 0 or FinalExam > 100 ) //迴圈( 當 FinalExam 小於0 或 FinalExam 大於100 )
    {
        cout << "Out of range !!" << endl << "FinalExam(30%):" ; //輸出 Out of range !! 換行 在輸出 FinalExam(30%):
        cin >> FinalExam ; //輸入 FinalExam
    }
    cout << "Assignment(30%):" ; //輸出 Assignment(30%): 到螢幕
    cin >> Assignment ; //輸入 Assignment
    while ( Assignment < 0 or Assignment > 100 ) //迴圈( 當 Assignment 小於0 或 Assignment 大於100 )
    {
        cout << "Out of range !!" << endl << "Assignment(30%):" ; //輸出 Out of range !! 換行 在輸出 Assignment(30%):
        cin >> Assignment ; //輸入 Assignment
    }
    cout << "Final grade: " << quiz * 0.2 + Midterm * 0.2 + FinalExam * 0.3 + Assignment * 0.3 << "分" ; //輸出 Final grade: 到螢幕

    return 0 ;


}
