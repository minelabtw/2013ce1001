#include <iostream>
using namespace std;

int main()
{
    int integer1 = 0;   //宣告型別為 整數(int) 的變數，並初始化其數值為0。
    int integer2 = 0;
    int integer3 = 0;
    int integer4 = 0;
    int value1 = 0;    //宣告型別為 整數(int) 的數值，並初始化其數值為0。


    cout << "quiz(20%):";
    cin >> integer1;
    while (integer1>100 or integer1<0) //若符合integer1>100 or integer1<0的條件則進行下列的程式碼，否則不進行。
    {
        cout << "Out of range!!" << endl << "quiz(20%):";
        cin >> integer1;
    }

    cout << "Midterm(20%):";
    cin >> integer2;
    while (integer2>100 or integer2<0)
    {

        cout << "Out of range!!" << endl << "Midterm(20%):";
        cin >> integer2;
    }


    cout << "Final Exam(30%):";
    cin >> integer3;
    while (integer3>100 or integer3<0)
    {
        cout << "Out of range!!" << endl << "Final Exam(30%):";
        cin >> integer3;
    }

    cout << "Assignment(30%):";
    cin >> integer4;
    while (integer4>100 or integer4<0)
    {
        cout << "Out of range!!" << endl << "Asignment(30%):";
        cin >> integer4;
    }


    value1 = (integer1 * 0.2) + (integer2 * 0.2) + (integer3 * 0.3) + (integer4 * 0.3);  //計算總成績平均
    cout << "Final grade:" << value1;  //輸出總成績平均值

    return 0;
}
