#include <iostream>
using namespace std;

int main()
{
    double number1 = 0;
    double number2 = 0;
    double number3 = 0;
    double number4 = 0;
    double number5 = 0; //宣告變數型別為浮點值的五個變數

    cout << "quiz(20%):"; //顯示出quiz(20%):
    cin >> number1; //輸入第一個成績

    while ( number1 > 100 || number1 < 0 ) //設定一個當number1>100或<0時進入迴圈
    {
      cout << "Out of range!!\nquiz(20%):"; //顯示Out of range!! 並在下一行顯示quiz(20%):
      cin >> number1; //重新輸入第一個成績
    }

    cout << "Midterm(20%):"; //顯示出Midterm(20%):
    cin >> number2; //輸入第二個成績

    while ( number2 > 100 || number2 < 0 ) //設定一個當number2>100或<0時進入迴圈
    {
      cout << "Out of range!!\nMidterm(20%):"; //顯示Out of range!! 並在下一行顯示Midterm(20%):
      cin >> number2; //重新輸入第二個成績
    }

    cout << "Final Exam(30%):"; //顯示出Final Exam(30%):
    cin >> number3; //輸入第三個成績

    while ( number3 > 100 || number3 < 0 ) //設定一個當number3>100或<0時進入迴圈
    {
      cout << "Out of range!!\nFinal Exam(30%):"; //顯示Out of range!! 並在下一行顯示Final Exam(30%):
      cin >> number3; //重新輸入第三個成績
    }

    cout << "Assignment(30%):"; //顯示出Assignment(30%):
    cin >> number4; //輸入第四個成績

    while ( number4 > 100 || number4 < 0 ) ////設定一個當number4>100或<0時進入迴圈
    {
      cout << "Out of range!!\nAssignment(30%):"; //顯示Out of range!! 並在下一行顯示Assignment(30%):
      cin >> number4; //重新輸入第四個成績
    }

    number5 = ( number1*2 + number2*2 + number3*3 + number4*3 ) / 10; //將四個成績呈上比例再相加，並將其值值給integer5

    cout << "Final grade: " << number5; //顯示出Final grade: integer5的值

    return 0;
}
