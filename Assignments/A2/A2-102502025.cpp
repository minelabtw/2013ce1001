#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()

    {
        double quiz=0;
        double midterm=0;
        double exam=0;
        double assignment=0;
        double grade=0;

        cout << "quiz(20%):";
        cin >> quiz;

        while (quiz>100||quiz<0)
        {
            cout << "Out of range!!" << endl << "quiz(20%):";
            cin >> quiz;
        }

        cout << "Midterm(20%):";
        cin >> midterm;

        while (midterm>100||midterm<0)
        {
            cout << "Out of range!!" << endl << "Midterm(20%):";
            cin >> midterm;
        }

        cout << "Final Exam(30%):";
        cin >> exam;

        while (exam>100||exam<0)
        {
            cout << "Out of range!!" << endl << "Final Exam(30%):";
            cin >> exam;
        }

        cout << "Assignment(30%):";
        cin >> assignment;

        while (assignment>100||assignment<0)
        {
            cout << "Out of range!!" << endl << "Assignment(30%)";
            cin >> assignment;
        }

        grade = ( quiz*20/100 ) + ( midterm*20/100 ) + ( exam*30/100 ) + ( assignment*30/100 );
        cout << "Final grade:" << grade;

    }
