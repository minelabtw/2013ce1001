#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int score1 = 0;       //宣告名稱為score1的整數變數，並初始化其數值為0。
    int score2 = 0;       //宣告名稱為score2的整數變數，並初始化其數值為0。
    int score3 = 0;       //宣告名稱為score3的整數變數，並初始化其數值為0。
    int score4 = 0;       //宣告名稱為score4的整數變數，並初始化其數值為0。
    int grade = 0;        //宣告名稱為grade的整數變數，並初始化其數值為0。

    cout << "quiz(20%):";                   //將"quiz(20%):"輸出到螢幕上。
    cin >> score1;                          //將鍵盤輸入的值給score1。
    while (score1 > 100 or score1 < 0)      //當score1>100或score1<0時，執行以下的動作。
    {
        cout << "Out of range!!" << endl;   //將"Out of range!!"輸出到螢幕上，並換行。
        cout << "quiz(20%):";
        cin >> score1;
    }

    cout << "Midterm(20%):";                //將"Midterm(20%):"輸出到螢幕上。
    cin >> score2;                          //將鍵盤輸入的值給score2。
    while (score2 > 100 or score2 < 0)      //當score2>100或score2<0時，執行以下的動作。
    {
        cout << "Out of range!!" << endl;
        cout << "Midterm(20%):";
        cin >> score2;
    }

    cout << "Final Exam(30%):";             //將"Final Exam(30%):"輸出到螢幕上。
    cin >> score3;                          //將鍵盤輸入的值給score3。
    while (score3 > 100 or score3 < 0)      //當score3>100或score3<0時，執行以下的動作。
    {
        cout << "Out of range!!" << endl;
        cout << "Final Exam(30%):";
        cin >> score3;
    }

    cout << "Assignment(30%):";             //將"Assignment(30%):"輸出到螢幕上。
    cin >> score4;                          //將鍵盤輸入的值給score4。
    while (score4 > 100 or score4 < 0)      //當score4>100或score4<0時，執行以下的動作。
    {
        cout << "Out of range!!" << endl;
        cout << "Assignment(30%):";
        cin >> score4;
    }

    grade = score1*0.2 + score2*0.2 + score3*0.3 + score4*0.3;    //將四個分數乘以各自所佔的百分比，再相加，然後將所得的值放在grade。
    cout << "Final grade:" << grade << "分";                      //將"Final grade:grade(上一行所算的值)分"輸出到螢幕上。

    return 0;

}
