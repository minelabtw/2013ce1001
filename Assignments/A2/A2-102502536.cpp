#include<iostream>
using namespace std;

int main()
{
    int a , b , c , d;   //宣告四個整數變數a,b,c,d

    cout << "quiz(20%):";//螢幕上輸出字串
    cin  >> a;           //輸入整數給a

    while (a<0 or a>100)//限制條件
    {
        cout << "Out of range!!" << endl;//符合條件時輸出字串
        cout << "quiz(20%):";            //重新輸出字串,進行輪迴
        cin  >> a;                       //重新輸入
    }

    cout << "Midterm(20%):";//不符合條件時執行下一個動作,輸出字串
    cin  >> b;              //輸入整數給b

    while (b<0 or b>100)//限制條件
    {
        cout << "Out of range!!" << endl;//符合條件時輸出字串
        cout << "Midterm(20%)";          //重新輸出字串,進行輪迴
        cin  >> b;                       //重新輸入
    }

    cout << "Final Exam(30%):";//不符合條件時執行下一個動作,輸出字串
    cin  >> c;                 //輸入整數給c

    while (c<0 or c>100)//限制條件
    {
        cout << "Out of range!!" << endl;//符合條件時輸出字串
        cout << "Final Exam(30%):";      //重新輸出字串,進行輪迴
        cin  >> c;                       //重新輸入
    }

    cout << "Assignment(30%):";//不符合條件時執行下一個動作,輸出字串
    cin  >> d;                 //輸入整數給d

    while (d<0 or d>100)//限制條件
    {
        cout << "Out of range!!" << endl;//符合條件時輸出字串
        cout << "Assignment(30%):";      //重新輸出字串,進行輪迴
        cin  >> d;                       //重新輸入
    }

    cout << "Final grade:" << a*0.2 + b*0.2 + c*0.3 + d*0.3 << "分" << endl;//不符合條件時執行下一個動作,進行運算並輸出結果



    return 0;
}
