#include <iostream>

using namespace std ;

int main ()
{
    int a1=0,a2=0,a3=0,a4=0 ;               //宣告4個變數,並初始化其值為0
    cout<<"quiz(20%):";                     //在螢幕上印出

    while (cin>>a1)                         //將其值輸入a1,並進入迴圈
    {
        if (a1>100||a1<0)                   //如果a1>100 或 a1<o 則執行以下命令
        {
            cout<<"out of range!!"<<endl;   //螢幕上印出 並換行
            cout<<"quiz(20%):";             //螢幕上輸出 並回到迴圈
        }
        else
            break ;                         //輸入值在範圍內即跳出迴圈,並記錄下a1
    }
    cout<<"Midterm(20%):" ;                 //在螢幕上印出
    while (cin>>a2)                         //將其值輸入a2,並進入迴圈
    {
        if (a2>100||a2<0)                   //如果a2>100 或 a2<o 則執行以下命令
        {
            cout<<"out of range!!"<<endl;   //螢幕上印出 並換行
            cout<<"Midterm(20%):" ;         //螢幕上輸出 並回到迴圈
        }
        else
            break ;                         //輸入值在範圍內即跳出迴圈,並記錄下a2
    }

    cout<<"Final Exam(30%):" ;              //在螢幕上印出
    while (cin>>a3)                         //將其值輸入a3,並進入迴圈
    {
        if (a3>100||a3<0)                   //如果a3>100 或 a3<o 則執行以下命令
        {
            cout<<"out of range!!"<<endl;   //螢幕上印出 並換行
            cout<<"Final Exam(30%):" ;      //螢幕上輸出 並回到迴圈
        }

        else
            break ;                         //輸入值在範圍內即跳出迴圈,並記錄下a3
    }
    cout<<"Assignment(30%):" ;              //在螢幕上印出
    while (cin>>a4)                         //將其值輸入a4,並進入迴圈
    {
        if (a4>100||a4<0)                   //如果a4>100 或 a4<o 則執行以下命令
        {
            cout<<"out of range!!"<<endl;   //螢幕上印出 並換行
            cout<<"Assignment(30%):" ;      //螢幕上輸出 並回到迴圈
        }

        else
            break ;                         //輸入值在範圍內即跳出迴圈,並記錄下a4
    }

    cout<<"Final grade:"<<a1*0.2+a2*0.2+a3*0.3+a4*0.3<<"分" ;  //將計算後分數輸出

    return 0 ;


}
