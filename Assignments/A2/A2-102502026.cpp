//A2-102502026
//Grades

#include <iostream>
using namespace std;

int main()//starts the program
{
    double Quiz; //defining Quiz
    double Midterm; //defining Midterm
    double Exam; //defening Exam
    double Assignment; //defining Assignment
    double FinalGrade; //defining FinalGrade
    FinalGrade=0;

    cout<<"Quiz(20%):"; //ask the grade of quiz
    cin>>Quiz;
    while (Quiz > 100) //loop until the grade is under or equal 100
    {
        cout<<"Out of Range!!!\n"; // the program says out of range
        cout<<"Quiz(20%):"; //ask again the grade of quizz
        cin>>Quiz;
    }
    cout<<"Midterm(20%):"; //ask the grade of midterm
    cin>>Midterm;
    while (Midterm > 100) //loop until the grade is under or equal 100
    {
        cout<<"Out of Range!!!\n"; //the program says out of range
        cout<<"Midterm(20%):"; //ask again the grade of midterm
        cin>>Midterm;
    }
    cout<<"Final Exam(30%):"; //ask the grade of Final Exam
    cin>>Exam;
    while (Exam > 100) //loop until the grade is under or equal 100
    {
        cout<<"Out of Range!!!\n"; //the program says out of range
        cout<<"Final Exam(30%):"; //ask again the grade of Final Exam
        cin>>Exam;
    }
    cout<<"Assignment(30%):"; //ask the grade of Assignment
    cin>>Assignment;
    while (Assignment > 100) //loop until the grade is under or equal 100
    {
        cout<<"Out of Range!!!\n"; //the program says out of range
        cout<<"Assignment(30%):"; //ask again the grade of Assignment
        cin>>Assignment;
    }

    FinalGrade= (Quiz*.20)+(Midterm*.20)+(Exam*.30)+(Assignment*.30); //Formula or the Final Grade
    cout<<"Final Grade is:"<<FinalGrade; //Result
    return 0;
} //End of the program
