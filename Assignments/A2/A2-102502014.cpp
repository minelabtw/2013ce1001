#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    float quiz=0;                         //將5個變數都宣告以浮點數的方式儲存 用以存取有小數點的分數
    float midterm=0;
    float exam=0;
    float assignment=0;
    float sum;

    cout <<"quiz(20%):";
    cin >> quiz;                          //將輸入的分數存入變數quiz
    while (quiz>100 || quiz<0)            //用一個while迴圈使輸入超過範圍的分數數值顯示"out of range !!"在螢幕上
        { cout <<"out of range !!"<<endl;
          cout <<"quiz(20%):";
          cin >> quiz;
        }

    cout <<"Midterm(20%):";
    cin >> midterm;                       //將輸入的分數存入變數midterm
    while (midterm>100 || midterm<0)      //用一個while迴圈使輸入超過範圍的分數數值顯示"out of range !!"在螢幕上
        {cout <<"out of range !!"<<endl;
         cout <<"Midterm(20%):";
         cin >> midterm;
        }

    cout <<"Final Exam(30%):";
    cin >> exam;                    //將輸入的分數存入變數exam
    while (exam>100 || exam<0)      //用一個while迴圈使輸入超過範圍的分數數值顯示"out of range !!"在螢幕上
        {cout <<"out of range !!"<<endl;
         cout <<"Final Exam(30%):";
         cin >> exam;
        }

    cout <<"Assignment(30%):";
    cin >> assignment;                       //將輸入的分數存入變數assignment
    while (assignment>100 || assignment<0)   //用一個while迴圈使輸入超過範圍的分數數值顯示"out of range !!"在螢幕上
        {cout <<"out of range !!"<<endl;
         cout <<"Assignment(30%):";
         cin >> assignment;
        }

    sum=quiz*0.2+midterm*0.2+exam*0.3+assignment*0.3;    //將四次成績分別乘以各自的比率再做總合 得到總成績

    cout <<"Final grade:"<<sum<<"分";                    //列印總成績在螢幕上

    return 0;
}
