#include <iostream>
using namespace std;

int main()
{
    float grade1=0;
    float grade2=0;
    float grade3=0;
    float grade4=0;                       //宣告4個變數,並使其初始值為0
    float sum=0;                          //宣告sum的初始值為0

    cout<<"quiz(20%):";
    cin >>grade1;
    while(grade1>100 || grade1<0)         //迴圈判斷分數是否符合0~100之間
    {
        cout<<"Out of range!!"<<endl;
        cout<<"quiz(20%):";
        cin >>grade1;
    }
    cout<<"Midterm(20%):";
    cin >>grade2;
    while(grade2>100 || grade2<0)
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Midterm(20%):";
        cin >>grade2;
    }
    cout<<"Final Exam(30%):";
    cin >>grade3;
    while(grade3>100 || grade3<0)
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Final Exam(30%):";
        cin >>grade3;
    }
    cout<<"Assignment(30%):";
    cin >>grade4;
    while(grade4>100 || grade4<0)
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Assignment(30%):";
        cin >>grade4;
    }
    sum=grade1*0.2+grade2*0.2+grade3*0.3+grade4*0.3;  //將所有分數依照其所占百分比相加
    cout<<"Final grade:"<<sum<<"分";


    return 0;
}
