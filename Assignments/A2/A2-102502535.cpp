#include <iostream>

using namespace std;

int main()

{
    int Q,M,FE,A;  //宣告四個變數。

    cout << "Quiz(20%):" ;  //使螢幕顯示Quiz(20%):。
    cin >> Q ;  //操作者輸入分數，並將其值丟給Q。
    while ( Q < 0  || Q>100 )  //以while限制操作者的輸入範圍:若輸入的數字小於0或大於100，即操作以下指示。
    {
        cout << "Out of range!\n" << "Quiz(20%):";  //若輸入錯誤，即顯示"超出範圍"。
        cin >> Q ;   //承上，輸入錯誤時，要求操作者再輸入一次;若還是錯誤，必須輸入到正確，才會繼續下一個動作。
    }  //結束while。以下亦同。

    cout << "Midterm(20%):" ;
    cin >> M ;
    while ( M < 0  || M > 100 )
    {
        cout << "Out of range!\n" << "Midterm(20%):";
        cin >> M ;
    }

    cout << "Final exam(30%):" ;
    cin >> FE ;
    while ( FE < 0  || FE > 100 )
    {
        cout << "Out of range!\n" << "Final exam(30%):";
        cin >> FE ;
    }

    cout << "Assignment(30%):" ;
    cin >> A ;
    while ( A < 0  || A > 100 )
    {
        cout << "Out of range!\n" << "Assignment(30%):";
        cin >> A ;
    }

    cout << "Final grade:" << Q*0.2+M*0.2+FE*0.3+A*0.3 ;

    return 0 ;
}
