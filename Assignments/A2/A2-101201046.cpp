#include <iostream>

//check grade whether out of 0~100
#define check(grade) ((grade > 100) || (grade < 0))


using namespace std;

typedef struct {
	string grade_type; //the type of grade
	int grade_points; //the percentage of final grade
	int grade; //grade number
	} Grade;


int main(void) {
	int final_grade = 0; //declare final_grade

	/*there are four grade quiz, Midterm, Final Exam, and Assignment
	 *  for input.And their percetage of final grade are 20,20,30,30.*/
	Grade list[4] = {{"quiz", 20, 0}, 
					 {"Midterm", 20, 0},
					 {"Final Exam", 30, 0},
					 {"Assignment", 30, 0}};
	
	/*start input the data*/
	for (int num = 0;num < 4;num++) {
		do {
			/*to show which type of grade*/
			cout << list[num].grade_type 
				 << '(' << list[num].grade_points << "%):";
			
			cin >> list[num].grade; //input 
			
			/*check grade if is between 0 and 100*/
			cout << (check(list[num].grade) ? "Out of range!!\n" : "");

			}while(check(list[num].grade));

		final_grade += list[num].grade * list[num].grade_points;
		}

	/*ouput final grade*/
	cout << "Final grade:";
	cout.width(3);
	cout << final_grade/100;
	cout << '.' << final_grade%100 << "分";

	return 0;
	}
