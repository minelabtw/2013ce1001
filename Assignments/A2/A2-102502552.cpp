#include <iostream>
using namespace std;
int main()//開始程式
{
    float score1 =0;//設定小考的變數
    float score2 =0;//設定期中考的變數
    float score3 =0;//設定期末考的變數
    float score4 =0;//設定作業的變數

    do{
    cout << "quiz(20%):";//提示輸入小考分數
    cin >> score1;//輸入小考分數
    if ( score1 >100 || score1 < 0 )//判斷分數是否在0-100的區間內
        cout << "Out of Range!!" <<endl;//若不是，將提示超出範圍
    }while ( score1 > 100 || score1 <0 );//設置一個判斷分數的迴圈以檢查分數是否在要求內

     do{
    cout << "Midterm(20%):";//提示輸入期中分數
    cin >> score2;//輸入小考分數
    if ( score2 >100 || score2 < 0 )//判斷分數是否在0-100的區間內
        cout << "Out of Range!!" <<endl;//若不是，將提示超出範圍
    }while ( score2 > 100 || score2 <0 );//設置一個判斷分數的迴圈以檢查分數是否在要求內

     do{
    cout << "Final Exam(30%):";//提示輸入期末考分數
    cin >> score3;//輸入小考分數
    if ( score3 >100 || score3 < 0 )//判斷分數是否在0-100的區間內
        cout << "Out of Range!!" <<endl;//若不是，將提示超出範圍
    }while ( score3 > 100 || score3 <0 );//設置一個判斷分數的迴圈以檢查分數是否在要求內

     do{
    cout << "Assignment(30%):";//提示輸入作業分數
    cin >> score4;//輸入小考分數
    if ( score4 >100 || score4 < 0 )//判斷分數是否在0-100的區間內
        cout << "Out of Range!!" <<endl;//若不是，將提示超出範圍
    }while ( score4 > 100 || score4 <0 );//設置一個判斷分數的迴圈以檢查分數是否在要求內

    cout << "Final Grade:" << score1 * 0.2 + score2 * 0.2 + score3 * 0.3 + score4 * 0.3 << "分";//顯示出最終加權分數

    return 0;//結束程式
}//結束主程式
