#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int quiz=0;                       //宣告一個整數quiz，並初始化其值為0
    int midterm=0;                    //宣告一個整數midterm，並初始化其值為0
    int FinalExam=0;                  //宣告一個整數FinalExam，並初始化其值為0
    int assignment=0;                 //宣告一個整數assignment，並初始化其值為0
    int FinalGrade=0;                 //宣告一個整數FinalGrade，並初始化其值為0，用以存放運算後的結果

        cout << "quiz:";
        cin >> quiz;
        while (quiz < 0 or quiz > 100)   //當quiz值小於0或是大於100時，輸出"out of range"與"quiz:"與至螢幕上，並可在輸入一次quiz的值
            {cout << "out of range" << endl;
            cout << "quiz:";
            cin >> quiz;}

        cout << "midterm:";
        cin >> midterm;
        while (midterm <0 or midterm >100)
            {cout << "out of range" << endl;
            cout << "midterm:";
            cin >> midterm;}

        cout << "FinalExam:";
        cin >> FinalExam;
        while (FinalExam <0 or FinalExam >100)
            {cout << "out of range" << endl;
            cout << "FinalExam:";
            cin >> FinalExam;}

        cout << "assignment:";
        cin >> assignment;
        while (assignment <0 or assignment >100)
           {cout << "out of range" << endl;
            cout << "assignment:";
            cin >> assignment;}


    cout << endl;

    FinalGrade = quiz*0.2 +midterm*0.2 + FinalExam*0.3 + assignment*0.3;  //將上面所輸入的值進行運算
    cout << "FinalGrade:" << FinalGrade;                                  //輸出最終結果

    return 0;
}
