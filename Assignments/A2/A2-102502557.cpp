#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    double num1,num2,num3,num4,num5;//宣告五個型別為倍精度浮動小數點數型的變數
 cout<<"quiz(20%):";               //將 quiz(20%): 印在螢幕上
 cin>>num1;                        //使用鍵盤輸入第一個數字
 while(num1>100||num1<0)           //用while迴圈來讓數字在規定的範圍內，如果不符，就要求使用者重新輸入。
    {
     cout<<"out of range"<<endl;   //在螢幕上印出out of range
     cout<<"quiz(20%):";           //在螢幕上印出quiz(20%):
     cin>>num1;                    //使用鍵盤輸入quiz的成績
    }
 cout<<"Midterm(20%):";            //以下概念類似上面
 cin>>num2;
 while(num2>100||num2<0)
    {
     cout<<"out of range"<<endl;
     cout<<"Midterm(20%):";
     cin>>num2;
    }
 cout<<"Final Exam(30%):";
 cin>>num3;
 while(num3>100||num3<0)
    {
     cout<<"out of range"<<endl;
     cout<<"Final Exam(30%):";
     cin>>num3;
    }
 cout<<"Assignment(30%):";
 cin>>num4;
 while(num4>100||num4<0)
    {
     cout<<"out of range"<<endl;
     cout<<"Assignment(30%):";
     cin>>num4;
    }
    num5 = num1 * 0.2 + num2 * 0.2 + num3 * 0.3 + num4 * 0.3; //使用num5來做加權平均
    cout << "Final grade:" << num5 << "分" << endl;           //並將其印出來
    return 0;

}
