#include <iostream>

using namespace std;

int main()
{
    //variable declaration
    int quiz=-1;       //the score of the quiz
    int midterm=-1;    //the score of Midterm
    int final=-1;      //the score of Final Exam
    int assignment=-1; //the score of the assignment

    while ( quiz > 100 || quiz < 0 )             //Check if the score is in the range or not. If not, promt for the data again. If yes, promt for the next data.
    {
        cout << "quiz(20%):";                    //promt user for data
        cin >> quiz;                             //read the first integer from user into quiz
        if ( quiz > 100 || quiz < 0 )
            cout << "Out of range!!\n";          //tell user the data is out of range
    }

    while ( midterm > 100 || midterm <0 )        //Check if the score is in the range or not. If not, promt for the data again. If yes, promt for the next data.
    {
        cout << "Midterm(20%):";                 //promt user for data
        cin >> midterm;                          //read the second integer from user into midterm
        if ( midterm > 100 || midterm <0 )
            cout << "Out of range!!\n";          //tell user the data is out of range
    }

    while ( final > 100 || final < 0 )           //Check if the score is in the range or not. If not, promt for the data again. If yes, promt for the next data.
    {
        cout << "Final Exam(30%):";              //promt user for data
        cin >> final;                            //read the third integer from user into final
        if ( final > 100 || final < 0 )
            cout << "Out of range!!\n";          //tell user the data is out of range
    }

    while ( assignment > 100 || assignment < 0 ) //Check if the score is in the range or not. If not, promt for the data again. If yes, calculate for result, and then show it at the next line.
    {
        cout << "Assignment(30%):";              //promt user for data
        cin >> assignment;                       //read the forth integer from user into assignment
        if ( assignment > 100 || assignment < 0 )
            cout << "Out of range!!\n";          //tell user the data is out of range
    }

    cout << "Final grade: " << ( ( quiz * 2 + midterm * 2 + final *3 + assignment * 3 ) / 10 ) << "��"; //display the result

    return 0;
}
