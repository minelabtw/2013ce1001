﻿#include<iostream>

using namespace std;

int main()
{
    int q = 0;  //宣告型別為整數(int)，並初始化其數值為0
    int M = 0;  //宣告型別為整數(int)，並初始化其數值為0
    int F = 0;  //宣告型別為整數(int)，並初始化其數值為0
    int A = 0;  //宣告型別為整數(int)，並初始化其數值為0
    int G = 0;  //宣告型別為整數(int)，並初始化其數值為0


    cout<<"enter quiz(20%)"<<endl;                       //顯示字串 輸入成績
    cin>>q;                                              //從鍵盤輸入，並將輸入的值給q
    while(q<0 || q>100)                                  //判斷q有沒有符合範圍
    {
           cout<<"Out of range!!"<<endl;                 //如不符合 顯示字串 超出範圍
           cin>>q;                                       //從鍵盤輸入，並將輸入的值給q
    }

    cout<<"enter Midterm(20%)"<<endl;                    //顯示字串 輸入成績
    cin>>M;                                              //從鍵盤輸入，並將輸入的值給M
    while(M<0 || M>100)                                  //判斷M有沒有符合範圍
    {
           cout<<"Out of range!!"<<endl;                 //如不符合 顯示字串 超出範圍
           cin>>M;                                       //從鍵盤輸入，並將輸入的值給M
    }

    cout<<"enter Final Exam(30%)"<<endl;                 //顯示字串 輸入成績
    cin>>F;                                              //從鍵盤輸入，並將輸入的值給F
    while(F<0 || F>100)                                  //判斷F有沒有符合範圍
    {
           cout<<"Out of range!!"<<endl;                 //如不符合 顯示字串 超出範圍
           cin>>F;                                       //從鍵盤輸入，並將輸入的值給F
    }

    cout<<"enter Assignment(30%)"<<endl;                 //顯示字串 輸入成績
    cin>>A;                                              //從鍵盤輸入，並將輸入的值給A
    while(A<0 || A>100)                                  //判斷A有沒有符合範圍
    {
           cout<<"Out of range!!"<<endl;                 //如不符合 顯示字串 超出範圍
           cin>>A;                                       //從鍵盤輸入，並將輸入的值給A
    }

    G=q*0.2+M*0.2+F*0.3+A*0.3;                           //計算最後成績

    cout<<"Final grade:"<<G<<"分";                       //顯示最後成績



    return 0;
}
