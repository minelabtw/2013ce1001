#include<iostream>

using namespace std;

int main()
{
    float quiz=0;           //宣告一個變數quiz，型態為浮點數
    float midterm=0;        //宣告一個變數midterm，型態為浮點數
    float final_exam=0;     //宣告一個變數final_exam，型態為浮點數
    float assignment=0;     //宣告一個變數assignment，型態為浮點數

    int correct=0;          //宣告一個變數correct來判斷迴圈是否結束

    while(correct!=1)
        {
        cout << "quiz(20%):";

        cin >> quiz;
        if(quiz>100 || quiz<0)          //判斷輸入的數值是否正確
            {
            cout << "out of range\n";   //輸入錯誤，print出指定字串
            }
        else correct=1;                 //輸入正確，跳出迴圈
        }
    correct=0;                          //將correct重置，在下個迴圈繼續使用

    while(correct!=1)
        {
        cout <<"Midterm(20%):";

        cin >> midterm;
        if(midterm>100 || midterm<0)    //判斷輸入的數值是否正確
            {
            cout << "out of range\n";   //輸入錯誤，print出指定字串
            }
        else correct=1;                 //輸入正確，跳出迴圈
        }
    correct=0;                          //將correct重置，在下個迴圈繼續使用

    while(correct!=1)
        {
        cout <<"Final Exam(30%):";

        cin >> final_exam;
        if(final_exam>100 || final_exam<0)  //判斷輸入的數值是否正確
            {
            cout << "out of range\n";       //輸入錯誤，print出指定字串
            }
        else correct=1;                     //輸入正確，跳出迴圈
        }
    correct=0;                              //將correct重置，在下個迴圈繼續使用

    while(correct!=1)
        {
        cout << "Assignment(30%):";

        cin >> assignment;
        if(assignment>100 || assignment<0)      //判斷輸入的數值是否正確
            {
            cout << "out of range\n";           //輸入錯誤，print出指定字串
            }
        else correct=1;                         //輸入正確，跳出迴圈
        }

    cout << "Final grade:" <<quiz*0.2 + midterm*0.2 + final_exam*0.3 + assignment*0.3 << "分";

        return 0;
}
