#include <iostream>
using namespace std;
int main()
{
    int quiz = 0; //宣告四個整數變數，並初始化為0
    int Midterm = 0;
    int FinalExam = 0;
    int Assignment = 0;

    cout<<"quiz(20%):";
    cin>>quiz;    //輸入quiz的值

    while (quiz<0 || quiz>100) //假如quiz的值不在0~100，則輸出Out of range!!，且重新輸入quiz的值
    {
        cout<<"Out of range!!"<<endl<<"quiz(20%):";
        cin>>quiz;
    }

    cout<<"Midterm(20%):";
    cin>>Midterm; //輸入Midterm的值

    while (Midterm<0 || Midterm>100) //假如Midterm的值不在0~100，則輸出Out of range!!，且重新輸入Midterm的值
    {
        cout<<"Out of range!!"<<endl<<"Midterm(20%):";
        cin>>Midterm;
    }

    cout<<"Final Exam(30%):";
    cin>>FinalExam; //輸入FinalExam的值

    while(FinalExam<0 || FinalExam>100) //假如FinalExam的值不在0~100，則輸出Out of range!!，且重新輸入FinalExam的值
    {
        cout<<"Out of range!!"<<endl<<"FinalExam(30%):";
        cin>>FinalExam;
    }

    cout<<"Assignment(30%):";
    cin>>Assignment; //輸入Assignment的值

    while (Assignment<0 || Assignment>100) //假如Assignment的值不在0~100，則輸出Out of range!!，且重新輸入Assignment的值

    {
       cout<<"Out of range!!"<<endl<<"Assignment(30%):";
       cin>>Assignment;
    }

    cout<<"Final grade:"<<quiz * 0.2 +Midterm * 0.2 +FinalExam * 0.3 +Assignment *0.3<<"分"<<endl; //輸出Final grade


    return 0;


}
