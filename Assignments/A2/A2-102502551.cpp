#include <iostream>

using namespace std;
using std::cin;
using std::endl;

int main()
{
   float quiz = 0,Midterm = 0,FinalExam = 0,Assignment = 0;     //宣告4個成績，並初始化其數值為0。
   float Finalgrade = 0;                                        //宣告最終成績，並初始化其數值為0。
   bool flag = true ;                                           //flag用來看是否在範圍內

   do
   {
    cout << "quiz(20%)：";
    cin >> quiz;
      if ( quiz > 100 || quiz < 0 )                              //當輸入數字不合範圍時,flag=false
      {
            flag = false;
            cout << "Out of range!!" <<endl;
      }
      else
      {
            flag=true;
      }
   }
   while(flag==false);                                            //當flag=false時,再重來
   do
   {
    cout << "Midterm(20%)：";
    cin >> Midterm;
         if ( Midterm > 100 || Midterm < 0 )
      {
            flag = false;
            cout << "Out of range!!" <<endl;
      }
      else
      {
            flag=true;
      }
   }
    while(flag==false);
    do
   {
    cout << "Final Exam(30%)：";
    cin >> FinalExam;
             if ( FinalExam > 100 || FinalExam < 0 )
      {
            flag = false;
            cout << "Out of range!!" <<endl;
      }
      else
      {
            flag=true;
      }
   }
    while(flag==false);
    do
   {
    cout << "Assignment(30%)：";
    cin >> Assignment;
         if ( Assignment > 100 || Assignment < 0 )
      {
            flag = false;
            cout << "Out of range!!" <<endl;
      }
      else
      {
            flag=true;
      }
   }
    while(flag==false);

    Finalgrade = ( quiz * 2 + Midterm * 2 + FinalExam * 3 + Assignment * 3 ) / 10;     //計算最終成績
    cout << "Final grade: " << Finalgrade << "分"<< endl;

    return 0;
}
