#include <iostream>
using namespace std;

int main()
{
    float a=0;
    float b=0;
    float c=0;
    float d=0;
    float grade=0;//宣告5個浮點數a,ab,c,d,grade,並將初始值訂為0
    cout << "quiz(20%):";
    cin >> a;
    while(a <0 || a > 100)//如果a小於0或大於100
    {
        cout << "Out of range!!\nquiz(20%):";
        cin >> a;
    }
    grade=grade+a*0.2;

    cout << "Midterm(20%):";
    cin >> b;
     while(b <0 || b > 100)//如果b小於0或大於100
    {
        cout << "Out of range!!\nMidterm(20%):";
        cin >> b;
    }
    grade=grade+b*0.2;

    cout << "Final Exam(30%):";
    cin >> c;
    while(c <0 || c > 100)//如果c小於0或大於100
    {
        cout << "Out of range!!\nFinal Exam(30%):";
        cin >> c;
    }
    grade=grade+c*0.3;

    cout << "Assignment(30%):";
    cin >> d;
   while(d <0 || d > 100)//如果d小於0或大於100
    {
        cout << "Out of range!!\nAssignment(30%):";
        cin >> d;
    }
    grade=grade+d*0.3;

    cout << "Final grade: " << grade << "分";
    return 0;
}
