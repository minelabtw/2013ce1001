#include <iostream>

using namespace std;

int main ()
{
    int grade_1=0; //宣告一個變數grade_1,並初始化其數值為0。
    int grade_2=0; //宣告一個變數grade_2,並初始化其數值為0。
    int grade_3=0; //宣告一個變數grade_3,並初始化其數值為0。
    int grade_4=0; //宣告一個變數grade_4,並初始化其數值為0。
    int sum=0;     //宣告一個變數sum,並初始化其數值為0。

    do //進入此一迴圈
    {
        cout << "quiz(20%):"; //將"quiz(20%):"儲存於cout,並印在銀幕上。
        cin>>grade_1; //輸入變數grade_1的值。
        if (grade_1>100||grade_1<0)
            cout << "Out of range!!"<<endl; //如果grade_1>100||grade_1<則將"Out of range"印出螢幕。
    }
    while(grade_1>100||grade_1<0); //判斷是否還需不需繼續迴圈。

    do //進入此一迴圈
    {
        cout << "Midterm(20%)"; //將"Midterm(20%)"儲存於cout,並印在銀幕上。
        cin>>grade_2; //輸入變數grade_2的值。
        if (grade_2>100||grade_2<0)
            cout << "Out of range!!"<<endl; //如果grade_2>100||grade_2<則將"Out of range"印出螢幕。
    }
    while(grade_2>100||grade_2<0); //判斷是否還需不需繼續迴圈。

    do //進入此一迴圈
    {
        cout << "Final Exam(30%):"; //將"Final Exam(30%):"儲存於cout,並印在銀幕上。
        cin>>grade_3; //輸入變數grade_3的值。
        if (grade_3>100||grade_3<0)
            cout << "Out of range!!"<<endl; //如果grade_3>100||grade_3<則將"Out of range"印出螢幕。
    }
    while(grade_3>100||grade_3<0); //判斷是否還需不需繼續迴圈。

    do //進入此一迴圈
    {
        cout << "Assignment(30%):";  //將"Assignment(30%):"儲存於cout,並印在銀幕上。
        cin>>grade_4; //輸入變數grade_4的值。
        if (grade_4>100||grade_4<0)
            cout << "Out of range!!"<<endl; //如果grade_4>100||grade_4<則將"Out of range"印出螢幕。
    }
    while(grade_4>100||grade_4<0); //判斷是否還需不需繼續迴圈。

    sum=(grade_1*.2+grade_2*.2+grade_3*.3+grade_4*.3); //將輸入的值乘與相對應的趴數,並儲存於sum裡。
    cout<<"Final grade:"<<sum<<"分"; //將sum的值輸出到螢幕上 。

    return 0 ;
}
