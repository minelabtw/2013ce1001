#include <iostream>

using namespace std;

int main()

{
    float number1=0,number2=0,number3=0,number4=0,number5=0;//宣告型別為(float) 的number1,2,3,4,5，並初始化其數值為0。

    cout << "quiz(20%):";                                   //顯示"    "
    cin >> number1;                                         //輸入變數

    while (number1<0 || number1>100)                        //迴圈 (條件)
    {
        cout << "Out of range!!\n"<< "quiz(20%):";
        cin >> number1;
    }

    cout << "Midterm(20%):";
    cin >> number2;
    while (number2<0 || number2>100)
    {
        cout << "Out of range!!\n"<< "Midterm(20%):";
        cin >> number2;
    }
    cout << "Final Exam(30%):";
    cin >> number3;
    while (number3<0 || number3>100)
    {
        cout << "Out of range!!\n" << "Final Exam(30%):";
        cin >> number3;
    }
    cout << "Assignment(30%)";
    cin >> number4;
    while (number4<0 || number4>100)
    {
        cout <<  "Out of range!!\n" << "Assignment(30%):";
        cin >> number4;
    }

    number5 = number1*0.2+number2*0.2+number3*0.3+number4*0.3; //計算

    cout << "Final grade:" << number5;

    return 0;
}
