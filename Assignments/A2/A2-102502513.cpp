#include <iostream>
using namespace std;
int main()
{
    double Quiz=-1, Midtern=-1 , Finalexam=-1 , Assignment=-1;  //declare variables of a variety of grades, and initialize to -1 to run the program
    double Finalgrade=0;                                        //declare variable of Finalgrade

    while(Quiz<0|Quiz>100)                                      //use the while loop to repeat input the value if the value is greater than 100, or is less than 0
    {
        cout << "Quiz(20%):";
        cin >> Quiz;

        if (Quiz<0|Quiz>100)                                    //if the value of Quiz is greater than 100, or is less than 0, the <if...else instruction> will output "out of range !!"
        {
            cout << "out of range !!\n";
        }
        else                                                    //if the value of Quiz is from 0 to 100, the <if...else instruction> will do nothing
        {
        }
    }

    while(Midtern<0|Midtern>100)
    {
        cout << "Midtern(20%):";
        cin >> Midtern;

        if (Midtern<0|Midtern>100)
        {
            cout << "out of range !!\n";
        }
    }

    while(Finalexam<0|Finalexam>100)
    {
        cout << "Final exam(30%):";
        cin >> Finalexam;

        if (Finalexam<0|Finalexam>100)
        {
            cout << "out of range !!\n";
        }
    }

    while(Assignment<0|Assignment>100)
    {
        cout << "Assignment(30%):";
        cin >> Assignment;

        if (Assignment<0|Assignment>100)
        {
            cout << "out of range !!\n";
        }
    }

    Finalgrade = Quiz*0.2+Midtern*0.2+Finalexam*0.3+Assignment*0.3; //let finalgrade store the value that is completed by above-mentioned grades which have respective proportion
    cout << "Final grade:" << Finalgrade << "分\n";

    return 0;
}
