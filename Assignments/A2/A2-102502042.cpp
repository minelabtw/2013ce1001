#include <iostream>
using namespace std;
int main()
{
    ios::sync_with_stdio(0);               //取消同步以提高輸入輸出效率
    double grade1;                         //宣告1個浮點數型別變數來存quiz成績
    double grade2;                         //宣告1個浮點數型別變數來存Midterm成績
    double grade3;                         //宣告1個浮點數型別變數來存Final Exam成績
    double grade4;                         //宣告1個浮點數型別變數來存Assignment成績
    cout<<"quiz(20%):";
    cin>>grade1;                           //輸入quiz成績存到grade1
    while(grade1>100||grade1<0)            //用while迴圈重複處理輸入超出範圍的情形
    {
        cout<<"Out of range!!"<<endl;
        cout<<"quiz(20%):";
        cin>>grade1;
    }
    cout<<"Midterm(20%):";
    cin>>grade2;                           //輸入Midterm成績存到grade2
    while(grade2>100||grade2<0)            //用while迴圈重複處理輸入超出範圍的情形
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Midterm(20%):";
        cin>>grade2;
    }
    cout<<"Final Exam(30%):";
    cin>>grade3;                           //輸入Final Exam成績存到grade3
    while(grade3>100||grade3<0)            //用while迴圈重複處理輸入超出範圍的情形
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Final Exam(30%):";
        cin>>grade3;
    }
    cout<<"Assignment(30%):";
    cin>>grade4;                           //輸入Assignment成績存到grade4
    while(grade4>100||grade4<0)            //用while迴圈重複處理輸入超出範圍的情形
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Assignment(30%):";
        cin>>grade4;
    }
    cout<<"Final grade: "<<0.2*grade1+0.2*grade2+0.3*grade3+0.3*grade4<<"分"<<endl; //輸出4次成績運算結果
    return 0;
}
