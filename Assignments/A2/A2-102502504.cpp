#include <iostream>

using namespace std;

int main()
{
    float quiz = 0; //宣告變數quiz，並初始化為0。
    float Midterm = 0;  //宣告變數Midterm，並初始化為0。
    float FinalExam = 0; //宣告變數FinalExam，並初始化為0。
    float Assignment = 0;  //宣告變數Assignment，並初始化為0。

    cout << "quiz(20%):"; //顯示quiz(20%):
    cin >> quiz; //輸入quiz的成績
    while( quiz<0 or quiz>100 ) //當quiz小於0或大於100時，執行下方{}內之內容
    {
        cout << "(Out of range!!)" << endl << "quiz(20%):"; //顯示Out of range!!，並換行顯示quiz(20%)
        cin >> quiz; //再次輸入quiz
    }

    cout << "Midterm(20%):"; //顯示Midterm(20%)
    cin >> Midterm; //輸入Midterm的成績
    while ( Midterm<0 or Midterm>100 ) //當Midterm小於0或大於100時，執行下方{}內之內容
    {
        cout << "(Out of range!!)" << endl << "Midterm(20%)"; //顯示Out of range!!，並換行顯示Midterm(20%)
        cin >> Midterm; //再次輸入Midterm的成績
    }

    cout << "Final Exam(30%):"; //顯示Final Exam(30%):
    cin >> FinalExam; //輸入Final Exam的成績

    while(  FinalExam<0 or FinalExam>100 ) //當FinalExam小於0或大於100時，執行下方{}內之內容
    {
        cout << "(Out of range!!)" << endl << "Final Exam(30%)"; //顯示Out of range!!，並換行顯示FinalExam(30%)
        cin >> FinalExam; //再次輸入FinalExam的成績
    }


    cout << "Assignment(30%):"; //顯示Assignment(30%)
    cin >> Assignment; //輸入Assignment的成績
    while (  Assignment<0 or Assignment>100 ) ////當Assignment小於0或大於100時，執行下方{}內之內容
    {
        cout << "(Out of range!!)" << endl << "Assignment(30%)"; //顯示Out of range!!，並換行顯示Assignment(30%)
        cin >> Assignment; //再次輸入Assignment的成績
    }

    cout << "Final grade:" << quiz * 0.2 + Midterm * 0.2 + FinalExam * 0.3 + Assignment * 0.3 << "分" ; //顯示Final grade:，並將quiz(20%)、Midterm(20%)、FinalExam(30%)、Assignment(30%)加權計算後的成績顯示

    return 0;
}

