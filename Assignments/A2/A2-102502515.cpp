#include <iostream>

using namespace std;

 int main()
 {
    double Quizgrade ;                                            //宣告實數變數
    double Midtermgrade ;                                         //宣告實數變數
    double Finalexamgrade ;                                       //宣告實數變數
    double Assignmentgrade ;                                      //宣告實數變數
    double Grade ;                                                //宣告實數變數的平均值

cout << "quiz(20%):" ;                                            //將字串顯示到螢幕
cin  >> Quizgrade ;                                               //將輸入的值給Quizgrade


    while (Quizgrade >100 || Quizgrade <0)                        //用while設定條件，當符合條件時執行下列動作，不符合條件時略過下列動作
        {cout << "out of range!!\nquiz(20%):" ;                   //顯示字串
         cin  >> Quizgrade ; }                                    //將再次輸入的數字給Quizgrade，並回到while條件


cout << "Midterm(20%):" ;                                         //顯示字串
cin  >> Midtermgrade ;                                            //將輸入的值給Midtermgrade

    while (Midtermgrade >100 || Midtermgrade <0)                  //用while設定條件，當符合條件時執行下列動作，不符合條件時略過下列動作
       {cout << "out of range!!\nMidterm(20%):";                  //顯示字串
        cin  >> Midtermgrade ; }                                  //將再次輸入的數字給Midtermgrade，並回到while條件

cout << "Final Exam(30%):" ;                                      //顯示字串
cin  >> Finalexamgrade ;                                          //將輸入的值給Finalexamgrade

    while (Finalexamgrade >100 || Finalexamgrade <0)              //用while設定條件，當符合條件時執行下列動作，不符合條件時略過下列動作
       {cout << "out of range!!\nFinal Exam(30%):" ;              //顯示字串
        cin  >> Finalexamgrade ; }                                //將再次輸入的數字給Finalexamgrade，並回到while條件

cout << "Assignment(30%):" ;                                      //顯示字串
cin  >> Assignmentgrade ;                                         //將輸入的值給Assignmentgrade

    while (Assignmentgrade >100 || Assignmentgrade <0)            //用while設定條件，當符合條件時執行下列動作，不符合條件時略過下列動作
       {cout << "out of range!!\nAssignment(30%):" ;              //顯示字串
        cin  >> Assignmentgrade ; }                               //將再次輸入的數字給Assignmentgrade，並回到while條件



Grade = Quizgrade * 0.2 + Midtermgrade*0.2 + Finalexamgrade * 0.3 + Assignmentgrade * 0.3;      //將所以數字依比率分配寫出方程式
    cout << "Final grade:" << Grade << "分" << endl;                                            //顯示字串與算出之平均值

return 0;
 }
