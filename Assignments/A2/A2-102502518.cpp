#include <iostream>

using namespace std;

int main()
{
    int quiz = 0;               //宣告型別為整數(int)的quiz，並初始化其數值為0。
    int Midterm = 0;            //宣告型別為整數(int)的Midterm，並初始化其數值為0。
    int Final_Exam = 0;         //宣告型別為整數(int)的Final_Exam，並初始化其數值為0。
    int Assignment = 0;         //宣告型別為整數(int)的Assignment，並初始化其數值為0。
    int Final_grade = 0;        //宣告型別為整數(int)的Final_grade，並初始化其數值為0。

    cout << " quiz(20%): ";          //將字串"quiz(20%):"輸出到螢幕上。
    cin >> quiz;                     //從鍵盤輸入，並將輸入的值給quiz。

    while ( quiz > 100 )             //使用while迴圈(當quiz>100)。
    {
       cout << " Out of range!! " << endl;     //將字串"Out of range!!"輸出到螢幕上,並換行。
       cout << " quiz(20%): ";       //將字串"quiz(20%):"輸出到螢幕上。
       cin >> quiz;                  //從鍵盤輸入，並將輸入的值給quiz。
    }

    while ( quiz < 0 )               //使用while迴圈(當quiz<0)。
    {
       cout << " Out of range!! " << endl;     //將字串"Out of range!!"輸出到螢幕上,並換行。
       cout << " quiz(20%): ";       //將字串"quiz(20%):"輸出到螢幕上。
       cin >> quiz;                  //從鍵盤輸入，並將輸入的值給quiz。
    }

    cout << " Midterm(20%): ";       //將字串"Midterm(20%):"輸出到螢幕上。
    cin >> Midterm;                  //從鍵盤輸入，並將輸入的值給Midterm。

    while ( Midterm > 100 )          //使用while迴圈(當Midterm>100)。
    {
       cout << " Out of range!! " << endl;     //將字串"Out of range!!"輸出到螢幕上,並換行。
       cout << " Midterm(20%): ";    //將字串"Midterm(20%):"輸出到螢幕上。
       cin >> Midterm;               //從鍵盤輸入，並將輸入的值給Midterm。
    }

    while ( Midterm < 0 )            //使用while迴圈(當Midterm<0)。
    {
       cout << " Out of range!! " << endl;     //將字串"Out of range!!"輸出到螢幕上,並換行。
       cout << " Midterm(20%): ";    //將字串"Midterm(20%):"輸出到螢幕上。
       cin >> Midterm;               //從鍵盤輸入，並將輸入的值給Midterm。
    }

    cout << " Final Exam(30%): ";    //將字串"Final Exam(30%):"輸出到螢幕上。
    cin >> Final_Exam;               //從鍵盤輸入，並將輸入的值給Final_Exam。

    while ( Final_Exam > 100 )       //使用while迴圈(當Final_Exam>100)。
    {
       cout << " Out of range!! " << endl;     //將字串"Out of range!!"輸出到螢幕上,並換行。
       cout << " Final Exam(30%): "; //將字串"Final Exam(30%):"輸出到螢幕上。
       cin >> Final_Exam;            //從鍵盤輸入，並將輸入的值給Final_Exam。
    }

    while ( Final_Exam < 0 )         //使用while迴圈(當Final_Exam<0)。
    {
       cout << " Out of range!! " << endl;     //將字串"Out of range!!"輸出到螢幕上,並換行。
       cout << " Final Exam(30%): "; //將字串"Final Exam(30%):"輸出到螢幕上。
       cin >> Final_Exam;            //從鍵盤輸入，並將輸入的值給Final_Exam。
    }

    cout << " Assignment(30%): ";    //將字串"Assignment(30%):"輸出到螢幕上。
    cin >> Assignment;               //從鍵盤輸入，並將輸入的值給Assignment。

    while ( Assignment > 100 )       //使用while迴圈(當Assignment>100)。
    {
       cout << " Out of range!! " << endl;     //將字串"Out of range!!"輸出到螢幕上,並換行。
       cout << " Assignment(30%): "; //將字串"Assignment(30%):"輸出到螢幕上。
       cin >> Assignment;            //從鍵盤輸入，並將輸入的值給Assignment。
    }

    while ( Assignment < 0 )         //使用while迴圈(當Assignment<0)。
    {
       cout << " Out of range!! " << endl;     //將字串"Out of range!!"輸出到螢幕上,並換行。
       cout << " Assignment(30%): "; //將字串"Assignment(30%):"輸出到螢幕上。
       cin >> Assignment;            //從鍵盤輸入，並將輸入的值給Assignment。
    }

    Final_grade = ( quiz * 20 + Midterm * 20 + Final_Exam * 30 + Assignment * 30 ) / 100;     //計算Final_grade。
    cout << " Final grade: " << Final_grade << " 分 " << endl;                                //將Final grade輸出到螢幕上。

    return 0;
}
