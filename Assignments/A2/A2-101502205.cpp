#include <iostream>

using namespace std;

// function of inputting a the score
double inputAScore(const char *request){    //const string, example:quiz(20%):
    double score;                       //declare a double float storing the score
    while(1){
        cout << request;                //output the question
        cin >> score;                   //input the score by keyboard
        if(score>=0 && score<=100)      //test if legal
            return score;               //if it's legal then return the value
        cout << "Out of range!!" << endl;   //error if failed
    }
}

int main() {
    double finalGrade = 0;              //declare a double float (initial: 0)
    finalGrade += 0.2*inputAScore("quiz(20%):");        //add a scaled score to the finalGrade
    finalGrade += 0.2*inputAScore("Midterm(20%):");     //add to the finalGrade
    finalGrade += 0.3*inputAScore("Final Exam(30%):");  //add the final score
    finalGrade += 0.3*inputAScore("Assignment(30%):");  //add the assignment score
    cout << "Final grade: " << finalGrade << "��" << endl; //output the result
    return 0;
}
