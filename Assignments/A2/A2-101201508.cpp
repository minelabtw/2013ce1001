#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    float quiz;                                                             //宣告變數，存小考分數
    float midterm;                                                          //宣告變數，存其中分數
    float Final;                                                            //宣告變數，存期末分數
    float assigment;                                                        //宣告變數，存作業分數
    int i=0;                                                                //算現在有幾個分數是已經輸入正確了，並做判斷是否重新輸入的機制
    for (i=0;i<4;++i){
        if (i==0){
            cout << "quiz(20%):";                                           //輸出文字
            cin >> quiz ;                                                   //輸入分數
            if (quiz>100 || quiz<0){                                        //判斷是否輸入錯誤
                i--;                                                        //錯誤則之後數字不能加，所以先減一
                cout << "Out of range!!" << endl;                           //輸出使用者輸入錯誤
            }
        }
        else if (i==1){                                                     //後面同理
            cout << "Midterm(20%):";
            cin >> midterm ;
            if (midterm>100 || midterm<0){
                i--;
                cout << "Out of range!!" << endl;
            }
        }
        else if (i==2){
            cout << "Final Exam(30%):";
            cin >> Final ;
            if (Final>100 || Final<0){
                i--;
                cout << "Out of range!!" << endl;
            }
        }
        else if (i==3){
            cout << "Assigment(30%):";
            cin >> assigment ;
            if (assigment>100 || assigment<0){
                i--;
                cout << "Out of range!!" << endl;
            }
        }
    }
    cout << "Final grade: " << quiz*0.2+midterm*0.2+Final*0.3+assigment*0.3 << "分";
    return 0;
}
