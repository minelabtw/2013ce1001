#include <iostream>

using namespace std;

int main()
{
    int Quiz,Final,Mid,Assign;  //設定四個即將輸入的變數
    double Grade;               //設定最後成績變數
    cout<<"Quiz(20%)";
    cin>>Quiz;                  //顯示並要求輸入QUIZ成績值
    while(Quiz>100 || Quiz<0)   //WHILE迴圈 如果不再範圍內顯示超出範圍並重新輸入
    {
        cout<<"Out of Range"<<endl;
        cout<<"Quiz(20%)";
        cin >> Quiz;
    }
    cout<<"Midterm(20%)";
    cin>>Mid;                   //顯示並要求輸入MIDTERM成績值
    while(Mid>100 || Mid<0)     //WHILE迴圈 如果不再範圍內顯示超出範圍並重新輸入
    {
        cout<<"Out of Range"<<endl;
        cout<<"Midterm(20%)";
        cin >> Mid;
    }
    cout<<"Final Exam(30%)";
    cin>>Final;                 //顯示並要求輸入FINAL EXAM成績值
    while(Final>100 || Final<0) //WHILE迴圈 如果不再範圍內顯示超出範圍並重新輸入
    {
        cout<<"Out of Range"<<endl;
        cout<<"Final Exam(30%)";
        cin >> Final;
    }
    cout<<"Assignment(30%)";
    cin>>Assign;                    //顯示並要求輸入Assignment成績值
    while(Assign>100 || Assign<0)   //WHILE迴圈 如果不再範圍內顯示超出範圍並重新輸入
    {
        cout<<"Out of Range"<<endl;
        cout<<"Assignment(30%)";
        cin >> Assign;
    }
    Grade=0.2*Quiz+0.2*Mid+0.3*Final+0.3*Assign; //計算GRADE之值
    cout<<"Final Grade;"<<Grade<<"分";           //顯示最後FINAL GRADE

    return 0;
}
