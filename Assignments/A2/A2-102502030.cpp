#include <iostream>
using namespace std;
int main()
{
    int quiz;  //設第一個變數
    int midterm;  //設第二個變數
    int finalexam;  //設第三個變數
    int assignment;  //設第四個變數

    cout << "quiz(20%):" ;
    cin >> quiz ;  //輸入第一個變數

    while ( quiz>100 || quiz<0)  //確認是否合理
          {cout << "Out of range!!\n";
           cout << "quiz(20%):" ;
           cin >> quiz;  //重新輸入
          }
    cout << "Midterm(20%):" ;
    cin >> midterm ;  //輸入第二個變數

    while ( midterm>100 || midterm<0)  //確認是否合理
          {cout << "Out of range!!\n";
           cout << "Midterm(20%):" ;
           cin >> midterm;  //重新輸入
          }
    cout << "Final Exam(30%):" ;
    cin >> finalexam ;    //輸入第三個變數

    while ( finalexam>100 || finalexam<0)  //確認是否合理
          {cout << "Out of range!!\n";
           cout << "Final Exam(30%):" ;
           cin >> finalexam;  //重新輸入
          }
    cout << "Assignment(30%):" ;
    cin >> assignment;  //輸入第四個變數

    while ( assignment>100 || assignment<0)  //確認是否合理
          {cout << "Out of range!!\n";
           cout << "Assignment(30%):" ;
           cin >> assignment;  //重新輸入
          }
    cout << "Final grade: " << (quiz*2+midterm*2+finalexam*3+assignment*3)/10 << "分" ; //計算結果



    return 0;
}
