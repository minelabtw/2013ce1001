#include<iostream>

using namespace std;

int main()
{
   int Q = 0,M = 0,F = 0,A = 0;             //宣告四個變數Q,M,F,A
   int FG = 0;                              //宣告一個變數作為總分

   flag1:
   cout << "quiz(20%):";
   cin >> Q;
      if(Q>=0 && Q<=100);                   //判別Q之值是否在0~100之間

      else                                  //若否，則執行以下動作
      {
          cout << "out of range" <<endl;
          goto flag1;                       //將程式拉至flag1處，重新執行動作
      }

   flag2:
   cout << "Midterm(20%):";
   cin >> M;
      if(M>=0 && M<=100);                   //判別M之值是否在0~100之間

      else                                  //若否，則執行以下動作
      {
          cout << "out of range" <<endl;
          goto flag2;                       //將程式拉至flag2處，重新執行動作
      }

   flag3:
   cout << "Final Exam(30%):";
   cin >> F;
      if(F>=0 && F<=100);                  //判別F之值是否在0~100之間

      else                                 //若否，則執行以下動作
      {
          cout << "out of range" <<endl;
          goto flag3;                      //將程式拉至flag3處，重新執行動作
      }

   flag4:
   cout << "Assignment(30%):";
   cin >> A;
      if(A>=0 && A<=100);                  //判別A之值是否在0~100之間

      else                                 //若否，則執行以下動作
      {
          cout << "out of range" <<endl;
          goto flag4;                      //將程式拉至flag4處，重新執行動作
      }

      FG=Q*.2 +M*.2+F*.3+A*.3;             //計算總成績
      cout <<"Final grade:"<< FG;

   return 0;
}
