//請輸入4次成績，分數介於0~100之間(需使用if else 判斷成績是否合理，介於0~100之間)，並算出最後總成績。

#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    float quiz;             //宣告實數
    float midterm;
    float exam;
    float assignment;
    float grace=0;

    cout << "請輸入成績" << endl;

    int quizflag=0;
    while(quizflag==0)          //當quizflag=0迴圈
      {
        cout << "Quiz(20%): ";
        cin >> quiz;

        if(quiz>=0&&quiz<=100)      //判斷條件如果(0<=quiz<=100)就
           {
            quizflag=1;             //將1給quizflag
            quiz=quiz*0.2;          //將quiz*0.2的結果給quiz
           }
        else                        //如果quiz>100 就把"out of range"輸出螢幕上
            cout << "Out of range" << endl;
      }

    int midtermflag=0;
    while(midtermflag==0)
      {
        cout << "Midterm(20%): ";
        cin >> midterm;
        if(midterm>=0&&midterm<=100)
           {
            midtermflag=1;
            midterm=midterm*0.2;
           }
        else
            cout << "Out of range" << endl;
      }

    int examflag=0;
    while(examflag==0)
      {
        cout << "Final Exam(30%): ";
        cin >> exam;
        if(exam>=0&&exam<=100)
           {
            examflag=1;
            exam=exam*30/100;
           }
        else
            cout << "Out of range" << endl;
      }

    int assigflag=0;
    while(assigflag==0)
      {
        cout << "Assignment(30%): ";
        cin >> assignment;
        if(assignment>=0&&assignment<=100)
           {
            assigflag=1;
            assignment=assignment*30/100;
           }
        else
            cout << "Out of range" << endl;
      }

    grace=quiz+midterm+exam+assignment;         //把quiz,midterm,exam,assignmen的計算結果相加然後給grace
    cout << "Final Grace: " << grace;           //把grace的結果輸出

    return 0;
    }
