#include<iostream>

using namespace std;

int main()
{
    int quiz=0;//宣告型別為整數(int)的quiz，並初始化其數值為0。
    int Midterm=0;//宣告型別為整數(int)的Midterm，並初始化其數值為0。
    int FinalExam=0;//宣告型別為整數(int)的FinalExam，並初始化其數值為0。
    int Assignment=0;//宣告型別為整數(int)的Assignment，並初始化其數值為0。
    int FinalGrade=0;//宣告型別為整數(int)的FinalGrade，並初始化其數值為0。

       cout<<"quiz(20%):"<<endl;//輸出quiz(20%):。
       cin>>quiz;//輸入小考成績。
    while ( quiz>100 || quiz<0 )//當輸入的小考成績>100或<0時
    {
       cout<<"Out of range!!"<<endl;//輸出Out of range!!。

       cout<<"quiz(20%)"<<endl;//否則輸出quiz(20%)。
       cin>>quiz;//輸入小考成績。
    }

       cout<<"Midterm(20%):"<<endl;//輸出Midterm(20%)。
       cin>>Midterm;//輸入期中考成績。
    while ( Midterm>100 || Midterm<0 )//當輸入的期中考成績>100或<0時
    {
       cout<<"Out of range!!"<<endl;//輸出Out of range!!。

       cout<<"Midterm(20%)"<<endl;//否則輸出Midterm(20%)。
       cin>>Midterm;//輸入期中考成績。
    }

       cout<<"Final Exam(30%):"<<endl;//輸出Final Exam。
       cin>>FinalExam;//輸入期末考成績。
    while ( FinalExam>100 || FinalExam<0 )//當輸入的期末考成績>100或<0時
    {
       cout<<"Out of range!!"<<endl;//輸出Out of range。

       cout<<"Final Exam(30%):"<<endl;//否則輸出Final Exam(30%)。
       cin>>FinalExam;//輸入期末考成績。
    }

       cout<<"Assignment(30%):"<<endl;//輸出Assignment(30%)。
       cin>>Assignment;//輸入作業成績。
    while ( Assignment>100 || Assignment<0)//當輸入的作業成績>100或<0時
   {
       cout<<"Out of range!!"<<endl;//輸出Out of range!!。

       cout<<"Assignment(30%):"<<endl;//否則輸出Assignment(30%)。
       cin>>Assignment;//輸入作業成績。
   }

    FinalGrade = (quiz+Midterm+FinalExam+Assignment)/4;//定義最終成績(FinalGrade)。
       cout<<"Final Grade:"<<FinalGrade<<endl;//輸出Final Grade。


       return 0;


}
