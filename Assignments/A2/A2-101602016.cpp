#include <iostream>
using std::cin;
using std::cout;
using std::endl;


int main ()
{
    double X=0;                               //X拿來暫存輸入的數字，並初始化其數值為0
    double sum=0;                             //宣告型別為 double 的和，並初始化其數值為0

    cout << "quiz(20%):";                     //在螢幕上顯示"quiz(20%):"

    while (cin >> X){                         //當輸入變數X時進入此迴圈
        if(X < 0 || 100 < X)                  //當X<0 or 100<X時
            cout << "Out of range!!" << endl  //顯示"Out of range!!"並換行
            << "quiz(20%):";                  //重新顯示"quiz(20%):"，並要求輸入數字
        else{                                 //當發生 X<0 or 100<X以外的情況時
            sum += X * 0.2;                   //將X的值乘以該成績百分比，累加到sum這個變數裡面
            break;                            //跳出迴圈
        }
    }

    cout << "Midterm(20%):";                  //在螢幕上顯示"Midterm(20%):"
    while (cin >> X){                         //當輸入變數X時進入此迴圈
        if(X < 0 || 100 < X)                  //當X<0 or 100<X時
            cout << "Out of range!!" << endl  //顯示"Out of range!!"並換行
            << "Midterm(20%):";               //重新顯示"Midterm(20%):"，並要求輸入數字
        else{                                 //當發生 X<0 or 100<X以外的情況時
            sum += X * 0.2;                   //將X的值乘以該成績百分比，累加到sum這個變數裡面
            break;                            //跳出迴圈
        }
    }

    cout << "Final Exam(30%):";               //在螢幕上顯示"Final Exam(30%):"
    while (cin >> X){                         //當輸入變數X時進入此迴圈
        if(X < 0 || 100 < X)                  //當X<0 or 100<X時
            cout << "Out of range!!" << endl  //顯示"Out of range!!"並換行
            << "Final Exam(30%):";            //重新顯示"Final Exam(30%):"，並要求輸入數字
        else{                                 //當發生 X<0 or 100<X以外的情況時
            sum += X * 0.3;                   //將X的值乘以該成績百分比，累加到sum這個變數裡面
            break;                            //跳出迴圈
        }
    }

    cout << "Assignment(30%):";               //在螢幕上顯示"Assignment(30%):"
    while (cin >> X){                         //當輸入變數X時進入此迴圈
        if(X < 0 || 100 < X)                  //當X<0 or 100<X時
            cout << "Out of range!!" << endl  //顯示"Out of range!!"並換行
            << "Assignment(30%):";            //重新顯示"Assignment(30%):"，並要求輸入數字
        else{                                 //當發生 X<0 or 100<X以外的情況時
            sum += X * 0.3;                   //將X的值乘以該成績百分比，累加到sum這個變數裡面
            break;                            //跳出迴圈
        }
    }

    cout << "Final grade: " << sum << "分";   //顯示最終成績


    return 0;
}
