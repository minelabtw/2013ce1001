#include<iostream>
using namespace std;

int main()
{
    int gradeA,gradeB,gradeC,gradeD,gradeE=0;  //宣告五個整數變數,並初始其值為0。

    cout<<"quiz(20%):";                        //將字串"quiz(20%):"輸出到螢幕。
    cin>>gradeA;                               //從鍵盤輸入，並將輸入的值給gradeA。

    while(gradeA >100||gradeA <0)              //將gradeA設定範圍並做成迴圈。
    {
        cout<<"Out of range!!\n";              //將字串"Out of range!!\n"輸出到螢幕。
        cout<<"quiz(20%):";                    //將字串"quiz(20%):"輸出到螢幕。
        cin>>gradeA;                           //從鍵盤輸入，並將輸入的值給gradeA。
    }


    cout<<"Midterm(20%):";                     //將字串"Midterm(20%):"輸出到螢幕。
    cin>>gradeB;                               //從鍵盤輸入，並將輸入的值給gradeB。


    while(gradeB >100||gradeB <0)              //將gradeB設定範圍並做成迴圈。
    {
        cout<<"Out of range!!\n";              //將字串"Out of range!!\n"輸出到螢幕。
        cout<<"Midterm(20%):";                 //將字串"Midterm(20%):"輸出到螢幕。
        cin>>gradeB;                           //從鍵盤輸入，並將輸入的值給gradeB。
    }

    cout<<"Final Exam(30%):";                  //將字串"Final Exam(30%):"輸出到螢幕。
    cin>>gradeC;                               //從鍵盤輸入，並將輸入的值給gradeC。


    while (gradeC>100||gradeC<0)               //將gradeC設定範圍並做成迴圈。
    {
        cout<<"Out of range!!\n";              //將字串"Out of range!!\n"輸出到螢幕。
        cout<<"Final Exam(30%):";              //將字串"Final Exam(30%):"輸出到螢幕。
        cin>>gradeC;                           //從鍵盤輸入，並將輸入的值給gradeC。

    }
    cout<<"Assignment(30%):";                  //將字串"Assignment(30%):"輸出到螢幕。
    cin>>gradeD;                               //從鍵盤輸入，並將輸入的值給gradeD。


    while (gradeD>100||gradeD<0)               //將gradeD設定範圍並做成迴圈。
    {
        cout<<"Out of range!!\n";              //將字串"Out of range!!\n"輸出到螢幕。
        cout<<"Assignment(30%):";              //將字串"Assignment(30%):"輸出到螢幕。
        cin>>gradeD;                           //從鍵盤輸入，並將輸入的值給gradeD。
    }
    gradeE=gradeA*0.2+gradeB*0.2+gradeC*0.3+gradeD*0.3;  //將四個整數做運算並輸出gradeE。
    cout<<"Final Grade:"<<gradeE;


    return 0;
}
