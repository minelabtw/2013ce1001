#include <iostream>
using namespace std;
int main() {
    double input = 0; // the number that users entered
    double sum   = 0; // a variable to store the final grade
    double percentage[4] = {0.2, 0.2, 0.3, 0.3}; // weight for the four scores
    char prompt[4][17] = {"quiz(20%):","Midterm(20%):","Final Exam(30%):","Assignment(30%):"};
    for (int i=0;i<4;i++) { // loop through all question
        cout << prompt[i];
        cin >> input; // first we let user input
        while(input > 100 || input < 0){ // check until the input is correct
            cout << "Out of range!!\n" << prompt[i]; // show error message
            cin >> input; // and then input again
        }
        sum += percentage[i] * input; // add to the final grade
    }
    cout << "Final grade: " << sum << "分"; // show the final grade
    return 0;
}
