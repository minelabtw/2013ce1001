#include <iostream>
using namespace std;

int main()
{
    int x=0;

    float total=0, input=0, weight=0; //宣告浮點數 分配比重

    while(x<4) //重覆詢問並累加成績來計算總成績
    {
        if (x==0)//判斷是否為詢問第 i 項目之成績
        {
            cout<< "Quiz(20%):"; //印出詢問第i項目之成績
            weight= 0.2; //定義第i科之成績比重
        }
        else if (x==1)
        {
            cout<< "Midterm(20%):";
            weight= 0.2;
        }
        else if (x==2)
        {
            cout<< "Final Exam(30%):";
            weight= 0.3;
        }
        else
        {
            cout<< "Assignment(30%):";
            weight= 0.3;
        }

        cin >> input; //輸入某項目之成績

        if (input>0 && input<100) //判斷是否符合範圍
        {
            total= total+ weight*input;
            x++; //如果此項輸入符合 就到下一項
        }
        else
        {
            cout<< "Out of range!!"<<endl;
        }

    }

    cout<< "Final Grade:" << total << "分" ;

    return 0 ;
}
