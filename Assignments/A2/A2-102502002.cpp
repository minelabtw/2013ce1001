#include <iostream>
using namespace std;

int main()
{
   float quiz =0;              //宣告名稱為quiz的變數，並初始化其數值為0。
   float mid =0;               //宣告名稱為mid的變數，並初始化其數值為0。
   float final =0;             //宣告名稱為final的變數，並初始化其數值為0。
   float assign =0;            //宣告名稱為assign的變數，並初始化其數值為0。
   float grade =0;             //宣告名稱為grade的變數，並初始化其數值為0。

   cout << "Quiz(20%):";               //將"Quiz(20%):"輸出到螢幕。
   cin >> quiz;                        //從鍵盤輸入，並將輸入的值丟給quiz。
      while ( quiz > 100 or quiz < 0 )      //設定quiz範圍 quiz>100 或 quiz<0。
       {
         cout << "Out of Range" << endl << "Quiz(20%):"; //當quiz符合上述範圍，將"Out of Range"輸出到螢幕，並換行顯示"Quiz(20%):"。
         cin >> quiz;                                    //重新輸入quiz的值。
       }

   cout << "Midterm(20%):";            //將"Midterm(20%):"輸出到螢幕。
   cin >> mid;                         //從鍵盤輸入，並將輸入的值丟給mid。
      while ( mid > 100 or mid < 0 )        //設定mid範圍 mid>100 或 mid<0。
       {
         cout << "Out of Range" << endl << "Midterm(20%):"; //當mid符合上述範圍，將"Out of Range"輸出到螢幕，並換行顯示"Midterm(20%):"。
         cin >> mid;                                        //重新輸入mid的值。
       }


   cout << "Final(30%):";              //將"Final(30%):"輸出到螢幕。
   cin >> final;                       //從鍵盤輸入，並將輸入的值丟給final。
      while ( final > 100 or final < 0 )    //設定final範圍 final>100 或 final<0。
       {
         cout << "Out of Range" << endl << "Final(30%):";   //當final符合上述範圍，將"Out of Range"輸出到螢幕，並換行顯示"Final(30%):"。
         cin >> final;                                      //重新輸入mid的值。
       }

   cout << "Assignment(30%):";         //將"Assignment(30%):"輸出到螢幕。
   cin >> assign;                      //從鍵盤輸入，並將輸入的值丟給assign。
      while ( assign > 100 or assign < 0 )   //設定assign範圍 assign>100 或 assign<0。
       {
         cout << "Out of Range" << endl << "Assignment(30%):";  //當assign符合上述範圍，將"Out of Range"輸出到螢幕，並換行顯示"Assignment(30%):"。
         cin >> assign;                                         //重新輸入assign的值。
       }

   grade = quiz*0.2 + mid*0.2 + final*0.3 + assign*0.3 ;       //計算最終成績。
   cout << "Final Grade:" << grade << "分";                    //輸出最終成績於螢幕上。

   return 0;
}
