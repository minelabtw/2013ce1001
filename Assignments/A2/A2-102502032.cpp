#include <iostream>
using namespace std;

//grade_processer is used to ask for the grade and determine whether the grade is valid or not
int grade_processer (string output_string)
{
    //varible decalaration
    int grade = 0;     //integer varible to memorize the input grade grade and initialize

    //ask for the grade and test whether the grade is valid or not
    //condition:isn't lager than 100 or smaller than 0
    do
    {
        //ask for the grade
        cout << output_string;
        cin >> grade;

        //determine wheather the grade is between 0 and 100 or not (condition)
        if ( grade < 0 or grade > 100 )
            cout << "Out of range!!\n";     //if the grade is invalid, output "Out of range!!"
    }
    while (grade < 0 or grade > 100);        //if the grade is invalid, ask again
    //so far, we should get a valid grade

    return grade;       //return a valid grade
}

//main
int main()
{
    //varible decalaration
    int quiz = 0;           //integer varible to memorize the quiz grade and initialize
    int midterm = 0;        //integer varible to memorize the midterm grade and initialize
    int final_exam = 0;     //integer varible to memorize the final exam grade and initialize
    int assignment = 0;     //integer varible to memorize the assignment grade and initialize
    float sum = 0;          //float varible to memorize the final grade and initialize

    //ask for grades by grade_processer( output string ), it should retuen an integer as valid grade
    quiz = grade_processer("quiz(20%):");               //ask for quiz grade
    midterm = grade_processer("Midterm(20%):");         //ask for midterm grade
    final_exam = grade_processer("Final Exam(30%):");   //ask for final exam grade
    assignment = grade_processer("Assignment(30%):");   //ask for assignment grade

    //comput the final grade
    //weight: quiz(20%), Midterm(20%), Final Exam(30%), Assignment(30%)
    sum = ( quiz * 0.2 ) + ( midterm * 0.2 ) +( final_exam * 0.3 ) + ( assignment * 0.3 );

    //output the final grade
    cout << "Final grade: " << sum << "��";

    return 0;
}
