#include <iostream>

using namespace std;

int main()
{
    double grade1;                                 //宣告變數1
    double grade2;                                 //宣告變數2
    double grade3;                                 //宣告變數3
    double grade4;                                 //宣告變數4
    double grade5;                                 //宣告變數5

    cout<<"quiz(20%):";                            //顯示小考成績
    cin>>grade1;                                   //輸入成績
    while(grade1>100||grade1<0)                    //當超過小考成績超過範圍
    {
        cout<<"Out of range!!"<<endl;              //顯示小考超出範圍
        cout<<"quiz(20%):";                        //顯示小考成績
        cin>>grade1;                               //輸入小考成績
    }

    cout<<"Midterm(20%):";                         //顯示期中考成績
    cin>>grade2;                                   //輸入期中考成績
    while(grade2>100||grade2<0)                    //當超過期中考成績超過範圍
    {
        cout<<"Out of range!!"<<endl;              //顯示超過期中考成績範圍
        cout<<"Midterm(20%):";                     //顯示期中考成績
        cin>>grade2;                               //輸入期中考成績
    }
    cout<<"Final Exam(30%):";                      //顯示期末考成績
    cin>>grade3;                                   //輸入期末考成績
    while(grade3>100||grade3<0)                    //當超過期末考成績範圍
    {
        cout<<"Out of range!!"<<endl;              //顯示超過期末考成績範圍
        cout<<"Final Exam(30%):";                  //顯示期末考成績
        cin>>grade3;                               //輸入期末考成績
    }
    cout<<"Assignment(30%):";                      //顯示作業成績
    cin>>grade4;                                   //輸入作業成績
    while(grade4>100|grade4<0)                     //當作業成績超過範圍
    {
        cout<<"Out of range!!"<<endl;              //顯示作業成績超過範圍
        cout<<"Assignment(30%):";                  //顯示作業成績
        cin>>grade4;                               //輸入作業成績
    }
    grade5=grade1*.2+grade2*.2+grade3*.3+grade4*.3;//總成績算法
    cout<<"Final grade:"<<grade5<<endl;            //顯示總成績

    return 0;
}
