#include <iostream>
using namespace std;

int main()
{
    int quiz=0;//宣告小考成績並初始為0
    int Midterm=0;//宣告期中考成績並初始為0
    int Final_Exam=0;//宣告期末考成績並初始為0
    int Assignment=0;//宣告作業成績並初始為0
    int Final_grade=0;//宣告總成績並初始為0

    while(true)//迴圈開始
    {
        cout<<"quiz(20%):";
        cin>>quiz;

        if(quiz<0||quiz>100)
            cout<<"Out of range!!"<<endl;//當成績小於0或大於100則輸出Out of range!!
        else
            break;//若符合範圍則跳出迴圈
    }

    while(true)
    {
        cout<<"Midterm(20%):";
        cin>>Midterm;

        if(Midterm<0||Midterm>100)
            cout<<"Out of range!!"<<endl;//當成績小於0或大於100則輸出Out of range!!
        else
            break;//若符合範圍則跳出迴圈
    }

    while(true)
    {
        cout<<"Final Exam(30%):";
        cin>>Final_Exam;

        if(Final_Exam<0||Final_Exam>100)
            cout<<"Out of range!!"<<endl;//當成績小於0或大於100則輸出Out of range!!
        else
            break;//若符合範圍則跳出迴圈
    }

    while(true)
    {
        cout<<"Assignment(30%):";
        cin>>Assignment;

        if(Assignment<0||Assignment>100)
            cout<<"Out of range!!"<<endl;//當成績小於0或大於100則輸出Out of range!!
        else
            break;//若符合範圍則跳出迴圈
    }

    Final_grade=quiz*0.2+Midterm*0.2+Final_Exam*0.3+Assignment*0.3;//計算總成績

    cout<<"Final grade: "<<Final_grade<<"分";

    return 0;
}
