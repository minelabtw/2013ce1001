#include <iostream>

using namespace std;

int main()
{
    int interger1,interger2,interger3,interger4;  //宣告四個變數
    int sum=0;                                    //宣告變數SUM,使其初始值為0

    cout << "Quiz(20%):";
    cin >> interger1;
    while (interger1<0,interger1>100)             //當interger1符合條件即進入迴圈
    {
        cout << "out of range!! " << endl << "Quiz(20%):";
        cin >> interger1;                         //重新輸入interger1
    }

    cout << "Midterm(20%):";
    cin >> interger2;
    while (interger2<0,interger2>100)             //當interger2符合條件即進入迴圈
    {
        cout << "out of range!!" << endl << "Midterm(20%):";
        cin >> interger2;                         //重新輸入interger2
    }

    cout << "Final Exam(30%):";
    cin >> interger3;
    while (interger3<0,interger3>100)             //當interger3符合條件即進入迴圈
    {
        cout << "out of range!!" << endl << "Final Exam(30%):";
        cin >> interger3;                         //重新輸入interger3
    }

    cout << "Assignment(30%):";
    cin >> interger4;
    while (interger4<0,interger4>100)              //當interger4符合條件即進入迴圈
    {
        cout << "out of range!!" << endl << "Assignment(30%):";
        cin >> interger4;                          //重新輸入interger4
    }

    sum = interger1*0.2+interger2*0.2+interger3*0.3+interger4*0.3;  //計算四個變數並存到sum
    cout << "Final Grade:" << sum;
return 0;
}
