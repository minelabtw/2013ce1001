#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    float quiz_score = 0;                                 //宣告型別為單精度浮點數的小考(quiz)成績，並初始化其數值為0。
    float Midterm_score = 0;                              //宣告型別為單精度浮點數的期中考(Midterm)成績，並初始化其數值為0。
    float Final_Exam_score = 0;                           //宣告型別為單精度浮點數的期末考(Final Exam)成績，並初始化其數值為0。
    float Assignment_score = 0;                           //宣告型別為單精度浮點數的作業(Assignment)成績，並初始化其數值為0。
    float Final_grade_score = 0;                          //宣告型別為單精度浮點數的總成績(Final grade)，並初始化其數值為0。

    cout << "quiz(20%):" ;                                //輸出字串至螢幕上。
    cin >> quiz_score;                                    //輸入小考(quiz)成績並儲存至quiz_score。
    if (quiz_score < 0 || quiz_score > 100 )              //當小考成績不符規定時，執行if區塊裡的敘述。
    {
        do                                                //當小考(quiz)成績不符規定時，執行do while區塊裡的敘述。
        {
            cout << "Out of range!! " << endl ;           //輸出字串至螢幕並換行。
            cout << "quiz(20%):" ;                        //輸出字串至螢幕上。
            cin >> quiz_score ;                           //輸入小考(quiz)成績並儲存至quiz_score。
        }
        while (quiz_score < 0 || quiz_score > 100 );
    }

    cout << "Midterm(20%):" ;                             //輸出字串至螢幕上。
    cin >> Midterm_score;                                 //輸入期中考(Midterm)成績並儲存至Midterm_score。
    if (Midterm_score < 0 || Midterm_score > 100 )        //當期中考(Midterm)成績不符規定時，執行if區塊裡的敘述。
    {
        do                                                //當期中考(Midterm)成績不符規定時，執行do while區塊裡的敘述。
        {
            cout << "Out of range!! " << endl ;           //輸出字串至螢幕並換行。
            cout << "Midterm(20%):" ;                     //輸出字串至螢幕上。
            cin >> Midterm_score ;                        //輸入期中考(Midterm)成績並儲存至Midterm_score。
        }
        while (Midterm_score < 0 || Midterm_score > 100 );
    }

    cout << "Final Exam(30%):" ;                          //輸出字串至螢幕上。
    cin >> Final_Exam_score;                              //輸入期末考(Final Exam)成績並儲存至Final_Exam_score。
    if ( Final_Exam_score < 0 || Final_Exam_score > 100 ) //當期末考(Final Exam)成績不符規定時，執行if區塊裡的敘述。
    {
        do                                                //當期末考(Finla Exam)成績不符規定時，執行do while區塊裡的敘述。
        {
            cout << "Out of range!! " << endl ;           //輸出字串至螢幕並換行。
            cout << "Final Exam(30%):" ;                  //輸出字串至螢幕上。
            cin >> Final_Exam_score ;                     //輸入期末考(Final Exam)成績並儲存至Final_Exam_score。
        }
        while (Final_Exam_score < 0 || Final_Exam_score > 100 );
    }

    cout << "Assignment(30%):" ;                          //輸出字串至螢幕上。
    cin >> Assignment_score;                              //輸入作業(Assignment)成績並儲存至Assignment_score。
    if ( Assignment_score < 0 || Assignment_score > 100 ) //當作業(Assignment)成績不符規定時，執行if區塊裡的敘述。
    {
        do                                                //當作業(Assignment)成績不符規定時，執行do while區塊裡的敘述。
        {
            cout << "Out of range!! " << endl ;           //輸出字串至螢幕並換行。
            cout << "Assignment(30%):" ;                  //輸出字串至螢幕上。
            cin >> Assignment_score ;                     //輸入作業(Assignment)成績並儲存至Assignment_score。
        }
        while ( Assignment_score < 0 || Assignment_score > 100 );
    }

    Final_grade_score = quiz_score * 0.2 + Midterm_score * 0.2 + Final_Exam_score * 0.3 + Assignment_score * 0.3 ; //利用之前儲存的數值計算最終成績。
    cout << "Final grade: " << Final_grade_score << "分" ;//輸出字串及數值至螢幕上

    return 0;
}
