#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    double quiz=-1; //因為要跑while迴圈,所以起始值必須在預設範圍之外才可發動迴圈,在這姑且設為-1
    double midterm=-1;
    double finalexam=-1;
    double assignment=-1;
    double finalgrade=0;

    while(quiz<0||quiz>100)
    {
        cout << "quiz(20%):" ;
        cin >> quiz;
        if(quiz>100||quiz<0) //如果成績超出範圍,則顯示Out of range!並重新輸入
            cout << "Out of range!\n" ;
    }

    while(midterm<0||midterm>100)
    {
        cout << "Midterm(20%):" ;
        cin >> midterm;
        if(midterm>100||midterm<0) //如果成績超出範圍,則顯示Out of range!並重新輸入
            cout << "Out of range!\n" ;
    }

    while(finalexam<0||finalexam>100)
    {
        cout << "Final Exam(30%):" ;
        cin >> finalexam;
        if(finalexam>100||finalexam<0) //如果成績超出範圍,則顯示Out of range!並重新輸入
            cout << "Out of range!\n" ;
    }

    while(assignment<0||assignment>100)
    {
        cout << "Assignment(30%):" ;
        cin >> assignment;
        if(assignment>100||assignment<0) //如果成績超出範圍,則顯示Out of range!並重新輸入
            cout << "Out of range!\n" ;
    }

    finalgrade=quiz*0.2+midterm*0.2+finalexam*0.3+assignment*0.3;
    cout << "Final Grade:" << finalgrade << "分";

    return 0;
}
