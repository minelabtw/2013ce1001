#include <iostream>

using namespace std;

int main()
{
    float quiz = 0; //宣告變數存quiz的分數
    float midterm = 0;  //宣告變數存midterm的分數
    float finalexam = 0;  //宣告變數存finalexam的分數
    float assignment = 0;  //宣告變數存assignment的分數

    do{
        cout << "quiz(20%):";  //請使用者輸入quiz成績
        cin >> quiz;  //輸入quiz成績
        if(quiz>100||quiz<0)  //如果quiz>100或是quiz<0
            cout << "Out of range!!" << endl;  //輸出超過範圍
    }while(quiz>100||quiz<0);  //檢查quiz有沒有>100或是<0
    do{
        cout << "Midterm(20%):";  //請使用者輸入midterm成績
        cin >> midterm;  //輸入成績
        if(midterm>100||midterm<0)  //如果midterm>100或是midterm<0
            cout << "Out of range!!" << endl;  //輸出超過範圍
    }while(midterm>100||midterm<0);  //檢查midterm有沒有>100或是<0
    do{
        cout << "Final Exam(30%):";  //請使用者輸入finalexam成績
        cin >> finalexam;  //輸入成績
        if(finalexam>100||finalexam<0)  //如果finalexam>100或是finalexam<0
            cout << "Out of range!!" << endl;  //輸出超過範圍
    }while(finalexam>100||finalexam<0);  //檢查finalexam有沒有>100或是<0
    do{
        cout << "Assignment(30%):";  //請使用者輸入assignment成績
        cin >> assignment;  //輸入成績
        if(assignment>100||assignment<0)  //如果assignment>100或是assignment<0
            cout << "Out of range!!" << endl;  //輸出超過範圍
    }while(assignment>100||assignment<0);  //檢查assignment有沒有>100或是<0

    cout << "Final grade: " << (assignment + finalexam)*0.3 + (quiz + midterm)*0.2 << "分";//輸出總分

    return 0;
}
