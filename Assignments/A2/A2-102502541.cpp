#include <iostream>

using namespace std;

int main()
{
    float number1 = 0;//宣告整數一
    float number2 = 0;//宣告整數二
    float number3 = 0;//宣告整數三
    float number4 = 0;//宣告整數四


    cout << "quiz(20%):";//輸出quiz(20%):
    cin >> number1;//輸入整數一
    while (number1 < 0 || number1 > 100)//迴圈 整數一小於零大於一百
    {
		cout << "Out of range!!"<< endl;//輸出Out of range!! 換行
		cout << "quiz(20%):" ; //輸出quiz(20%):
        cin >> number1;//輸入整數一
	}

    cout << "Midterm(20%):";//輸出Midtrem(20%):
    cin >> number2;//輸入整數二
    while (number2 < 0 || number2 > 100)//迴圈 整數一小於零大於一百
    {
        cout << "Out of range!!"<< endl;//輸出Out of range!! 換行
        cout << "Midterm(20%)" ; //輸出Midterm(20%)
        cin >> number2;//輸入整數二
	}
    cout << "Final Exam(30%):";//輸出Final Exam(30%):
    cin >> number3;//輸入整數三
    while (number3 < 0 || number3 > 100)//迴圈 整數一小於零大於一百
    {
        cout << "Out of range!!" << endl;//輸出Out of range!! 換行
        cout << "Final Exam(30%):" ; //輸出Final Exam(30%):
        cin >> number3;//輸入整數三
	}
    cout << "Assignment(30%):";//輸出Assignment(30%):
    cin >> number4;//輸入整數四
    while (number4 < 0 || number4 > 100)//迴圈 整數一小於零大於一百
    {
        cout << "Out of range!!" << endl;//輸出Out of range!! 換行
        cout << "Assignment(30%):" ;  //輸出Assignment(30%):
        cin >> number4;//輸入整數四
	}


    cout << "Final grade: " << number1*0.2 + number2*0.2 + number3*0.3 + number4*0.3 << "分";//輸出Final grade:

    return 0;
}
