#include<iostream>
using namespace std;
int main()
{
    float quiz = 0;        //宣告分數1
    float midterm = 0;     //宣告分數2
    float finalexam = 0;   //宣告分數3
    float assignment =0;   //宣告分數4

    cout << "quiz(20%):";  //顯示quiz(20%):
    cin >> quiz;           //輸入分數1
    while ( quiz < 0 or quiz > 100 )  //迴圈條件為分數1小於零或大於一百
    {
        cout << "Out of range!!"<< endl << "quiz(20%):";  //顯示Out of range!! 換行 quiz(20%):
        cin >> quiz;  //輸入分數1
    }

    cout << "Midterm(20%):";  //顯示Midterm(20%):
    cin >> midterm;           //輸入分數2
    while ( midterm < 0 or midterm > 100 )  //迴圈條件為分數2小於零或大於一百
    {
        cout << "Out of range!!" << endl << "Midterm(20%):";  //顯示Out of range!! 換行 Midterm(20%):
        cin >> midterm;  //輸入分數2
    }

    cout << "Final Exam(30%):";  //顯示Final Exam(30%):
    cin >> finalexam;            //輸入分數3
    while ( finalexam < 0 or finalexam > 100 )  //迴圈條件為分數3小於零或大於一百
    {
         cout << "Out of range!!" << endl << "Final Exam(30%):";  //顯示Out of range!! 換行 Final Exam(30%):
         cin >> finalexam;  //輸入分數3
    }

    cout << "Assignment(30%):";  //顯示Assignment(30%):
    cin >> assignment;  //輸入分數4
    while ( assignment < 0 or assignment > 100 )  //迴圈條件為分數4小於零或大於一百
    {
        cout << "Out of range!!" << endl << "Assignment(30%):";  //顯示Out of range!! 換行 Assignment(30%)::
         cin >> assignment; //輸入分數4
    }
    cout << "Final grade: " << quiz * 0.2 + midterm * 0.2 + finalexam * 0.3 + assignment * 0.3 << "分" << endl;  //計算後並顯示總成績

    return 0;
}
