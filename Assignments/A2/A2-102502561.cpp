#include <iostream>

using namespace std;

int main()
{
    double score1, score2, score3, score4; //宣告變數score1,score2,score3,score4。

    cout << "quiz(20%):";
    cin >> score1;
    while(score1 > 100||score1 < 0) //當score1值>100或<0的時候觸發。
    {
    cout << "out of range!!\n";
    cin >> score1;
    }

    cout << "midterm(20%):";
    cin >> score2;
    while(score2 > 100||score2 < 0) //當score2值>100或<0的時候觸發。
    {
    cout << "out of range!!\n";
    cin >> score2;
    }

    cout << "Final Exam(30%):";
    cin >> score3;
    while(score3 > 100||score3 < 0) //當score3值>100或<0的時候觸發。
    {
    cout << "out of range!!\n";
    cin >> score3;
    }

    cout << "Assignment(30%):";
    cin >> score4;
    while(score4 > 100||score4 < 0) //當score4值>100或<0的時候觸發。
    {
    cout << "out of range!!\n";
    cin >> score4;
    }

    cout << "Final grade " << score1*0.2 + score2*0.2 + score3*0.3 + score4*0.3 << " 分"; //將計算結果輸出到螢幕上。

    return 0;


}
