#include <iostream>
using namespace std;

int main()
{
    double quiz =0;            //宣告一個可以記錄小數的變數quiz,並初始化其值為0
    double Midterm =0;         //宣告一個可以記錄小數的變數Midterm,並初始化其值為0
    double Final_Exam =0;      //宣告一個可以記錄小數的變數Final_Exam,並初始化其值為0
    double Assignment =0;      //宣告一個可以記錄小數的變數Assignment,並初始化其值為0
    double Final_grade =0;     //宣告一個可以記錄小數的變數Final_grade,並初始化其值為0

    while (1)                                   //設定一個迴圈
    {
        cout << "quiz(20%):";                   //輸出"quiz(20%):"到螢幕上
        cin >> quiz;                            //輸入一個值給quiz

        if ( quiz <0 or quiz >100)              //假如quiz <0 or quiz >100則執行下一句並重跑一次while
            cout << "Out of range!!\n";         //輸出"Out of range!!\n"到螢幕上
        else                                    //如果不符合if則執行下一句
            break;                              //跳離這個迴圈
    }
    while (1)                                   //設定一個迴圈
    {
        cout << "Midterm(20%):";                //輸出"Midterm(20%):"到螢幕上
        cin >> Midterm;                         //輸入一個值給Midterm

        if ( Midterm <0 or Midterm >100)        //假如Midterm <0 or Midterm >100則執行下一句並重跑一次while
            cout << "Out of range!!\n";         //輸出"Out of range!!\n"到螢幕上
        else                                    //如果不符合if則執行下一句
            break;                              //跳離這個迴圈
    }
    while (1)                                   //設定一個迴圈
    {
        cout << "Final Exam(30%):";             //輸出"Final Exam(30%):"到螢幕上
        cin >> Final_Exam;                      //輸入一個值給Final_Exam

        if ( Final_Exam <0 or Final_Exam >100)  //假如Final_Exam <0 or Final_Exam >100則執行下一句並重跑一次while
            cout << "Out of range!!\n";         //輸出"Out of range!!\n"到螢幕上
        else                                    //如果不符合if則執行下一句
            break;                              //跳離這個迴圈
    }
    while (1)                                   //設定一個迴圈
    {
        cout << "Assignment(30%):";             //輸出"Assignment(30%):"到螢幕上
        cin >> Assignment;                      //輸入一個值給Assignment

        if ( Assignment <0 or Assignment >100)  //假如Assignment <0 or Assignment >100則執行下一句並重跑一次while
            cout << "Out of range!!\n";         //輸出"Out of range!!\n"到螢幕上
        else                                    //如果不符合if則執行下一句
            break;                              //跳離這個迴圈
    }

    Final_grade = quiz * 0.2 + Midterm *0.2 + Final_Exam *0.3 + Assignment*0.3;     //計算Final_grade的值並儲存進去

    cout << "Final grade:" << Final_grade << "分" ;                                 //輸出"Final grade:"+Final_grade的值+"分"到螢幕上

    return 0;
}
