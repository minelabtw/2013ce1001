#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int quiz;//宣告quiz變數
    int midterm;//宣告midterm變數
    int finalexam;//宣告finalexam變數
    int assignment;//宣告assignment變數
    int finalgrade;//宣告finalgrade變數

    cout << "quiz(20%):";//將字串輸出
    cin >> quiz;//將字串輸入quiz這個變數

    while( quiz > 100 or quiz < 0 )//利用while環圈，加入條件，quiz要於0和100之間
    {
        cout << "Out of range!!" << endl;//假如不符合條件，就跑出此字串
        cout << "quiz(20%):";//並重新輸入
        cin >> quiz;//將新輸入的變數覆蓋新的變數，直到條件符合
    }

    cout << "Midterm(20%):";//上面的while迴圈滿足後再接下去，輸出此字串
    cin >> midterm;

    while( midterm > 100 or midterm < 0 )
    {
        cout << "Out of range!!" << endl;
        cout << "Midterm(20%):";
        cin >> midterm;
    }

    cout << "Final Exam(30%):";
    cin >> finalexam;

    while( finalexam > 100 or finalexam < 0  )
    {
        cout << "Out of range!!" << endl;
        cout << "Final Exam(30%):";
        cin >> finalexam;
    }

    cout << "Assignment(30%):";
    cin >> assignment;

    while( assignment > 100 or assignment < 0 )
    {
        cout << "Out of range!!" << endl;
        cout << "Assignment(30%):";
        cin >> assignment;
    }

    finalgrade = quiz*0.2 + midterm*0.2 + finalexam*0.3 + assignment*0.3;//計算final grade的數值
    cout << "Final grade: " << finalgrade;//並輸出其數值

    return 0;
}

