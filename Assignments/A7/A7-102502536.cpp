#include<iostream>

using namespace std;

void c_f(double & x);//宣告函式
void c_k(double & x);
void f_c(double & x);
void f_k(double & x);
void k_c(double & x);
void k_f(double & x);

int main()
{
    double c1=0,f1=0,k1=0;//宣告變數並設初始值
    double c2=0,f2=0,k2=0;

    cout << "Please enter temperature C : ";//輸出字串
    cin  >> c1;//輸入值給變數
    c2=c1;

    cout << "Please enter temperature F : ";//輸出字串
    cin  >> f1;//輸入值給變數
    f2=f1;

    cout << "Please enter temperature K : ";//輸出字串
    cin  >> k1;//輸入值給變數
    k2=k1;

    cout << "First  Temperature is ";//輸出字串
    c_f(c1);
    cout << c1 << "F and ";//輸出新變數值
    c_k(c2);
    cout << c2 << "K" << endl;//輸出新變數值

    cout << "Second Temperature is ";//輸出字串
    f_c(f1);
    cout << f1 << "C and ";//輸出新變數值
    f_k(f2);
    cout << f2 << "K" << endl;//輸出新變數值

    cout << "Third  Temperature is ";//輸出字串
    k_c(k1);
    cout << k1 << "C and ";//輸出新變數值
    k_f(k2);
    cout << k2 << "F" << endl;//輸出新變數值

    return 0;
}

void c_f(double & x)//攝氏轉華氏
{
    x=x*1.8+32;
}

void c_k(double & x)//攝氏轉凱氏
{
    x=x+273.15;
}

void f_c(double & x)//華氏轉攝氏
{
    x=(x-32)*5/9;
}

void f_k(double & x)//華氏轉凱氏
{
    x=(x-32)*5/9+273.15;
}

void k_c(double & x)//凱氏轉攝氏
{
    x=x-273.15;
}

void k_f(double & x)//凱氏轉華氏
{
    x=(x-273.15)*1.8+32;
}
