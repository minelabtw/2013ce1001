#include<iostream>
using namespace std;

void CtoF (double &t);                          //各函數的protype。
void CtoK (double &t);
void FtoC (double &t);
void FtoK (double &t);
void KtoC (double &t);
void KtoF (double &t);

int main()
{
    double C1,C2,F1,F2,K1,K2;                   //宣告變數。

    cout << "Please enter temperature C : ";    //要求使用者輸入溫度。
    cin >> C1;
    C2=C1;

    cout << "Please enter temperature F : ";
    cin >> F1;
    F2=F1;

    cout << "Please enter temperature K : ";
    cin >> K1;
    K2=K1;

    CtoF(C1);                                   //利用宣告的函數計算結果並輸出。
    CtoK(C2);
    cout << "First  Temperature is " << C1 << "F and " << C2 << "K" << endl;

    FtoC(F1);
    FtoK(F2);
    cout << "Second Temperature is " << F1 << "C and " << F2 << "K" << endl;

    KtoC(K1);
    KtoF(K2);
    cout << "Third  Temperature is " << K1 << "C and " << K2 << "F" ;

    return 0;
}


void CtoF (double &t)                           //宣告各個換算單位的函數。
{
    t=t*9/5+32;
}

void CtoK (double &t)
{
    t=t+273.15;
}

void FtoC (double &t)
{
    t=(t-32)*5/9;
}

void FtoK (double &t)
{
    t=(t+459.67)*5/9;
}

void KtoC (double &t)
{
    t=t-273.15;
}

void KtoF (double &t)
{
    t=t*9/5-459.67;
}
