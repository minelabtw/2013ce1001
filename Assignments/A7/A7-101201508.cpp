#include<iostream>
#include<iomanip>
using std::cout;
using std::cin;
using std::endl;
void C_to_Other(double &C,double &CtoF,double &CtoK)  //攝氏轉其他
{
    CtoF=C*9/5+32;
    CtoK=C+273.15;
}
void F_to_Other(double &F,double &FtoC,double &FtoK)  //華氏轉其他
{
    FtoC=(F-32)*5/9;
    FtoK=F=FtoC+273.15;
}
void K_to_Other(double &K,double &KtoC,double &KtoF)  //克氏轉其他
{
    KtoC=K-273.15;
    KtoF=KtoC*9/5+32;
}
int main()
{
    double C,CtoF,CtoK,F,FtoC,FtoK,K,KtoC,KtoF ;
    cout <<"Please enter temperature C : " ;
    cin >> C ;
    cout << "Please enter temperature F : " ;
    cin >> F ;
    cout << "Please enter temperature K : " ;
    cin >> K ;
    C_to_Other(C,CtoF,CtoK);               //call C_to_Other
    F_to_Other(F,FtoC,FtoK);               //call F_to_Other
    K_to_Other(K,KtoC,KtoF);               //call K_to_Other
    cout << "First  Temperature is " << CtoF << "F and " << CtoK<<"K" << endl;
    cout << "Second Temperature is " << FtoC << "C and " << FtoK<<"K" << endl;
    cout << "Third  Temperature is " << KtoC << "C and " << KtoF<<"F";


    return 0;
}
