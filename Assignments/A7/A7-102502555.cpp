#include <iostream>

using namespace std;

void K_C(double &input);  //宣告溫度變換的函式
void F_C(double &input);
void C_F(double &input);
void C_K(double &input);

int main(){
    double c;  //宣告變數裝溫度
    double f;
    double k;

    cout << "Please enter temperature C : ";  //輸入溫度
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;

    C_F(c);  //轉換溫度
    F_C(f);
    K_C(k);

    cout << "First Temperature is " << c << "F and ";  //輸出轉換後的溫度
    F_C(c);
    C_K(c);
    cout << c << "K" << endl;
    cout << "Second Temperature is " << f << "C and ";
    C_K(f);
    cout << f << "K" << endl;
    cout << "Third Temperature is " << k << "C and ";
    C_F(k);
    cout << k << "F" << endl;

    return 0;
}

void K_C(double &input){  //轉換溫度的函式
    input = input - 273.15;
}

void F_C(double &input){
    input = (input - 32) * (5.0 / 9);
}

void C_F(double &input){
   input = input * (9 / 5.0) + 32;
}

void C_K(double &input){
    input = input + 273.15;
}
