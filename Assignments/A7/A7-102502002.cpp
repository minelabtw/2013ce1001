#include <iostream>
using namespace std;

void cf(double &c)                 //將攝氏換算為華氏的函式
{
    c=c*9/5+32;                    //換算攝氏為華氏
}

void ck(double &c)                 //將華氏換算為凱氏的函式
{
    c=(c-32)*5/9+273.15;
}

void fc(double &f)                 //將華氏換算為攝氏的函式
{
    f=(f-32)*5/9;
}

void fk(double &f)                 //將攝氏換算為凱氏的函式
{
    f=f+273.15;
}

void kc(double &k)                 //將凱氏換算為攝氏的函式
{
    k=k-273.15;
}

void kf(double &k)                 //將攝氏換算為華氏的函式
{
    k=k*9/5+32;
}

int main()
{
    double c=0;                     //宣告變數攝氏c 華氏f 凱氏k
    double f=0;
    double k=0;

    cout << "Please enter temperature C : ";              //輸入c
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;

    cout << "First  Temperature is ";                     //輸出字串
    cf(c);                                                //跑攝氏轉華氏的函式
    cout << c << "F and ";
    ck(c);                                                //因c已轉為華氏，故跑華氏轉凱氏的函式
    cout << c << "K" << endl;

    cout << "Second Temperature is ";                     //由華氏轉為攝氏及凱氏
    fc(f);
    cout << f << "C and ";
    fk(f);
    cout << f << "K" << endl;

    cout << "Third  Temperature is ";                     //由凱氏轉為攝氏及華氏
    kc(k);
    cout << k << "C and ";
    kf(k);
    cout << k << "F" << endl;

    return 0;
}
