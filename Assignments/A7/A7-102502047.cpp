#include <iostream>

using namespace std;

void CF(double &);//宣告6個單位換算函式
void CK(double &);
void FC(double &);
void FK(double &);
void KC(double &);
void KF(double &);
int main()
{
    double a=0,b=0,c=0,k=0,m=0,n=0;//宣告6個浮點數
    cout<<"Please enter temperature C : ";
    cin>>a;
    cout<<"Please enter temperature F : ";
    cin>>b;
    cout<<"Please enter temperature K : ";
    cin>>c;
    k=a;
    m=b;
    n=c;
    CF(a);//執行函式
    cout<<"First  Temperature is "<<a<<"F ";
    CK(k);
    cout<<" and "<<k<<"K"<<endl;
    FC(b);
    cout<<"Second Temperature is "<<b<<"C";
    FK(m);
    cout<<" and "<<m<<"K"<<endl;
    KC(c);
    cout<<"Third  Temperature is "<<c<<"C";
    KF(n);
    cout<<" and "<<n<<"F";


    return 0;
}

void CF( double &x )//定義6個單位換算函式
{
    x=x*9/5+32;
}
void CK( double &x )
{
    x=x+273.149994;
}
void FC( double &x )
{
    x=(x-32)*5/9;
}
void FK( double &x )
{
    x=(x-32)*5/9+273.149994;
}
void KC( double &x )
{
    x=x-273.149994;
}
void KF( double &x )
{
    x=(x-273.149994)*9/5+32;
}
