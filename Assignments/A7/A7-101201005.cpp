#include<iostream>

using namespace std;

void C_to_F(double &C,double &F) //function 溫度轉換
{
    F=C*9/5+32;
}
void F_to_C(double &F,double &C)
{
    C=(F-32)*5/9;
}
void K_to_C(double &K,double &C)
{
    C=K-273.15;
}
void C_to_K(double &C,double &K)
{
    K=C+273.15;
}
int main()
{
    double C,CtoF,CtoK,F,FtoC,FtoK,K,KtoC,KtoF ;
    cout <<"Please enter temperature C : " ;
    cin >> C ;
    cout << "Please enter temperature F : " ;
    cin >> F ;
    cout << "Please enter temperature K : " ;
    cin >> K ;
    C_to_F(C,CtoF); //可載入函式
    C_to_K(C,CtoK);
    F_to_C(F,FtoC);
    C_to_K(FtoC,FtoK);
    K_to_C(K,KtoC);
    C_to_F(KtoC,KtoF);
    cout << "First  Temperature is " << CtoF << "F and " << CtoK << "K" << endl; //輸出溫度
    cout << "Second Temperature is " << FtoC << "C and " << FtoK << "K" << endl;
    cout << "Third  Temperature is " << KtoC << "C and " << KtoF << "F";


    return 0;
}
