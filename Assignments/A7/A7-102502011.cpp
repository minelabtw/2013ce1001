#include<iostream>
#include <math.h>
using namespace std;

void ctof (double & , double) ;//宣告六個函示
void ctok (double & , double) ;
void ftoc (double & , double) ;
void ftok (double & , double) ;
void ktoc (double & , double) ;
void ktof (double & , double) ;

int main()
{
    double c = 0 , f = 0 , k = 0 , output = 0 ;

    cout << "Please enter temperature C : " ;
    cin >> c ;

    cout << "Please enter temperature F : " ;
    cin >> f ;

    cout << "Please enter temperature K : " ;
    cin >> k ;

    ctof (output,c) ;
    cout << "First  Temperature is " << output << "F and " ; //輸出函式計算的結果

    ctok (output,c) ;
    cout << output << "K" << endl ;

    ftoc (output,f) ;
    cout << "Second Temperature is " << output << "C and " ;

    ftok (output,f) ;
    cout << output << "K" << endl ;

    ktoc (output,k) ;
    cout << "Third  Temperature is " << output << "C and " ;

    ktof (output,k) ;
    cout << output << "F" ;
    return 0;
}

void ctof (double &output , double c ) //參考output和宣告C,讓CIN的值能夠帶入函式計算,並正常輸出
{
    output = c * 9/5 + 32 ;
}

void ctok (double &output , double c )
{
    output = c + 273.15 ;
}

void ftoc (double &output , double f )
{
    output = (f - 32) * 5/9 ;
}

void ftok (double &output , double f )
{
    output = (f - 32) * 5/9 + 273.15 ;
}

void ktoc (double &output , double k )
{
    output = k - 273.15 ;
}

void ktof (double &output , double k )
{
    output = ( k - 273.15 ) * 9/5 +32 ;
}

