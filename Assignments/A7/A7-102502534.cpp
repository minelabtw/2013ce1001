#include<iostream>
using namespace std;

void functionCF(double&);//function prototype
void functionCK(double&);
void functionFC(double&);
void functionFK(double&);
void functionKC(double&);
void functionKF(double&);

int main()
{
    double temperatureC=0;//宣告變數
    double temperatureF=0;
    double temperatureK=0;

    cout<<"Please enter temperature C : ";
    cin>>temperatureC;
    cout<<"Please enter temperature F : ";
    cin>>temperatureF;
    cout<<"Please enter temperature K : ";
    cin>>temperatureK;
    //將攝氏溫度轉為華氏和克氏溫度
    functionCF(temperatureC);
    cout<<"First  Temperature is "<<temperatureC;
    functionCK(temperatureC);
    cout<<"F and "<<temperatureC;
    cout<<"K"<<endl;
    //將華氏溫度轉為攝氏和克氏溫度
    functionFC(temperatureF);
    cout<<"Second Temperature is "<<temperatureF;
    functionFK(temperatureF);
    cout<<"C and "<<temperatureF;
    cout<<"K"<<endl;
    //將克氏溫度轉為攝氏和華氏溫度
    functionKC(temperatureK);
    cout<<"Third  Temperature is "<<temperatureK;
    functionKF(temperatureK);
    cout<<"C and "<<temperatureK;
    cout<<"F"<<endl;
    return 0;
}
//定義函式
void functionCF(double&c)
{
    c=c*1.8+32;
}
void functionCK(double&c)
{
    c=(c+459.67)/1.8;
}
void functionFC(double&f)
{
    f=(f-32)/1.8;
}
void functionFK(double&f)
{
    f=f+273.15;
}
void functionKC(double&k)
{
    k=k-273.15;
}
void functionKF(double&k)
{
    k=k*1.8+32;
}


