#include<iostream>

using namespace std;

//define a structure storing temperaturs(C,F,K)
struct FriedChicken
{
    double k,f,c; //Tasty!!
};

void convertK(struct FriedChicken &t)
{
    //converting K into C F
    t.c = t.k - 273.15;
    t.f = t.c*9/5+32;
}

void convertF(struct FriedChicken &t)
{
    //converting F into C K
    t.c = (t.f-32)*5/9;
    t.k = t.c + 273.15;
}

void convertC(struct FriedChicken &t)
{
    //converting C into F K
    t.f = t.c*9/5+32;
    t.k = t.c + 273.15;
}

int main()
{
    struct FriedChicken t[3]; //an array of KFC!! nice!
    int i;

    //inputs
    cout << "Please enter temperature C : ";
    cin >> t[0].c;
    convertC(t[0]);//do conversion (C into F K)

    cout << "Please enter temperature F : ";
    cin >> t[1].f;
    convertF(t[1]);// do conversion (F into C K)

    cout << "Please enter temperature K : ";
    cin >> t[2].k;
    convertK(t[2]);// do conversion (K into C F)

    //outputs
    cout << "First  Temperature is " << t[0].f << "F and " << t[0].k << "K" << endl;
    cout << "Second Temperature is " << t[1].c << "C and " << t[1].k << "K" << endl;
    cout << "Third  Temperature is " << t[2].c << "C and " << t[2].f << "F" << endl;

    return 0;
}
