#include <iostream>

using namespace std;


// 從wiki查到的公式
void CtoF(double &n)
{
    n = n*9.0/5.0 + 32.0;
}
void CtoK(double &n)
{
    n = n + 273.15;
}

void FtoC(double &n)
{
    n = (n - 32.0)*5.0/9.0;
}
void FtoK(double &n)
{
    n = (n + 459.67) * 5.0 / 9.0;
}

void KtoC(double &n)
{
    n = n - 273.15;
}
void KtoF(double &n)
{
    n = n * 9.0/5.0 - 459.67;
}

int main()
{
    double C = 0, F = 0, K = 0;
    // print ui
    cout << "Please enter temperature C : ";
    cin >> C;
    cout << "Please enter temperature F : ";
    cin >> F;
    cout << "Please enter temperature K : ";
    cin >> K;

    cout << "First  Temperature is ";
    CtoF(C); // 這時候 C 已經被轉成 F
    cout << C << "F and ";
    FtoK(C); // 所以呼叫FtoK
    cout << C << "K" << endl;

    // 以下同上
    cout << "Second Temperature is ";
    FtoC(F);
    cout << F << "C and ";
    CtoK(F);
    cout << F << "K" << endl;

    cout << "Third  Temperature is ";
    KtoC(K);
    cout << K << "C and ";
    CtoF(K);
    cout << K << "F" << endl;
    return 0;
}
