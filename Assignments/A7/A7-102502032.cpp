#include <iostream>
using namespace std;

/*
convert formula:
c=c             f=1.8c +32            k=c +273.15
c=f/1.8 -32/1.8 f=f                   k=f/1.8 -32/1.8+273.15
c=k -273.15     f=1.8k +32-273.15*1.8 k=k
*/
//temperature convert
void T_convert ( double data[][3] )
{
    //decalaration
    double temp[3][3];          //template array
    double coefficient[3][3];   //the varible's coefficient
    double constant[3][3];      //quantities of shift

    //initialization
    for ( int i = 0; i <= 2; i ++ )
        for ( int j = 0; j <= 2; j ++ )
        {
            temp[i][j] = 0;
            coefficient[i][j] = 0;
            constant[i][j] = 0;
        }

    //set coefficient array
    for ( int i = 0; i <= 2; i ++ )
        for ( int j = 0; j <= 2; j ++ )
            if ( i == j or i == 2 - j )
                coefficient[i][j] = 1;
            else if ( ( i == 0 or i == 2 ) and j == 1 )
                coefficient[i][j] = 1.8;
            else
                coefficient[i][j] = 1 / 1.8;

    //set constant array
    constant[0][0] = 0;
    constant[0][1] = 32.0;
    constant[0][2] = 273.15;
    constant[1][0] = -32 / 1.8;
    constant[1][1] = 0;
    constant[1][2] = 273.15 - 32 / 1.8;
    constant[2][0] = -273.15;
    constant[2][1] = 32 - 273.15 * 1.8;
    constant[2][2] = 0;

    //convert temperature
    for ( int i = 0; i <= 2; i ++ )
        for ( int j = 0; j <= 2; j ++ )
            for ( int k = 0; k <= 2; k ++ )
                temp[i][j] += data[i][k] * coefficient[k][j];

    //shift
    for ( int i = 0; i <= 2; i ++ )
        for ( int j = 0; j <= 2; j ++ )
            temp[i][j] += constant[i][j];

    //return data
    for ( int i = 0; i <= 2; i ++ )
        for ( int j = 0; j <= 2; j ++ )
            data[i][j] = temp [i][j];
}

int main ()
{
    //decalaration and initialization
    double temperature[3][3];
    for ( int i = 0; i <= 2; i ++ )
        for ( int j = 0; j <= 2; j ++ )
            temperature[i][j] = 0;

    //ask for data
    cout << "Please enter temperature C : ";
    cin >> temperature[0][0];
    cout << "Please enter temperature F : ";
    cin >> temperature[1][1];
    cout << "Please enter temperature K : ";
    cin >> temperature[2][2];

    //convert
    T_convert( temperature );

    cout << "First  Temperature is " << temperature[0][1] << "F and " << temperature[0][2] << "K" << endl
         << "Second Temperature is " << temperature[1][0] << "C and " << temperature[1][2] << "K" << endl
         << "Third  Temperature is " << temperature[2][0] << "C and " << temperature[2][1] << "F";

    reutrn 0;
}
