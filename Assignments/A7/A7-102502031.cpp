//A7-102502031
//conversion of units of temperature(Celsius, Fahrenheit, and Kelvin scale)
#include <iostream>

using namespace std;

//check if input data is legal or not
void Check (string DataName,double &data,double minimum)
{
    do
    {
        cout << "Please enter temperature " << DataName << " : ";
        cin >> data;
    }
    while (data < minimum && cout << "Illegal input!\n");
}

void conversion (int mode,double input,double &output1,double &output2)
{ //mode=1->input=c   mode=2->input=f   mode=3->input=k
    switch (mode)
    {
    case 1:
        output1=input*9/5+32;          //C->F
        output2=input+273.15;          //C->K
        break;
    case 2:
        output1=(input-32)*5/9;        //F->C
        output2=(input-32)*5/9+273.15; //F->K
        break;
    case 3:
        output1=input-273.15;           //K->C
        output2=(input-273.15)*9/5+32;  //K->F
        break;
    }
}

int main()
{
    double c=0;
    double f=0;
    double k=0;
    double output1=0;
    double output2=0;

    Check ("C",c,-273.15);//minimum in Celsius scale has calculated
    Check ("F",f,-459.67);//minimum in Fahrenheit scale has calculated
    Check ("K",k,0);      //0 is the minimum in Kelvin scale by difinition

    conversion (1,c,output1,output2);
    cout << "First  Temperature is " << output1 << "F and " << output2 << "K" << endl;
    conversion (2,f,output1,output2);
    cout << "Second Temperature is " << output1 << "C and " << output2 << "K" << endl;
    conversion (3,k,output1,output2);
    cout << "Third  Temperature is " << output1 << "C and " << output2 << "F";

    return 0;
}
