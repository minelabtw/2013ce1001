#include<iostream>
using namespace std;

void C1(float &a);//攝氏轉華氏
void C2(float &a);//攝氏轉凱氏
void F1(float &b);//華氏轉攝氏
void F2(float &a);//華氏轉凱氏
void K1(float &c);//凱氏轉攝氏
void K2(float &c);//與C1相同都是攝氏轉華氏
int main()
{
    float a=0,b=0,c=0;//宣告浮點數型態的變數
    cout<<"Please enter temperature C : ";
    cin>>a;
    cout<<"Please enter temperature F : ";
    cin>>b;
    cout<<"Please enter temperature K : ";
    cin>>c;
    C1(a);
    cout<<"First  Temperature is "<<a;
    F2(a);
    cout<<"F and "<<a<<"K\n";
    F1(b);
    cout<<"Second Temperature is "<<b;
    C2(b);
    cout<<"C and "<<b<<"K\n";
    K1(c);
    cout<<"Third  Temperature is "<<c;
    K2(c);
    cout<<"C and "<<c<<"F\n";
    return 0;
}

void C1(float &a)//宣告一個函數使其計算攝氏轉華氏
{
    a=(a*9)/5+32;
}
void F2(float &a)//宣告一個函數使其計算華氏轉凱氏
{
    a=(a-32)*5/9+273.15;
}
void F1(float &b)//宣告一個函數使其計算華氏轉攝氏
{
    b=(b-32)*5/9;
}
void C2(float &b)//宣告一個函數使其計算攝氏轉凱氏
{
    b=b+273.15;
}
void K1(float &c)//宣告一個函數使其計算凱氏轉攝氏
{
    c=c-273.15;
}
void K2(float &c)//宣告一個函數使其計算攝氏轉華氏
{
    c=(c*9)/5+32;
}
