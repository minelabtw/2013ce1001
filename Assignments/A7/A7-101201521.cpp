#include <iostream>
using namespace std;
//prtotype and functions
void CtoF(double &c)//convert temperature from celsius to fahrenheit
{
    c = c * 9./5 + 32;
}

void FtoK(double &f)//convert temperature from fahrenheit to kelvin
{
    f = (f + 459.67) * 5./9;
}
void KtoC(double &k)//convert temperature from kelvin to celsius
{
    k = k - 273.15;
}

int main()
{
    //declare and initialize variables
    //c, f, k is the first, second, third input
    double c=0, f=0, k=0;
//input
    cout << "Please enter temperature C : ";
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;
//output
    //first
    CtoF(c);
    cout << "First  Temperature is " << c << "F";
    FtoK(c);
    cout << " and " << c << "k\n";
    //second
    FtoK(f), KtoC(f);//FtoC
    cout << "Second Temperature is " << f << "C";
    CtoF(f), FtoK(f);//CtoK
    cout << " and " << f << "K\n";
    //third
    KtoC(k);
    cout << "Third  Temperature is " << k << "C";
    CtoF(k);
    cout << " and " << k << "F\n";

    return 0;
}
