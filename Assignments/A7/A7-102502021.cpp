#include <iostream>
using namespace std;

void CFK(double &c) //指標
{
    c= c*1.8 +32;

}
void CFK2(double &c) //指標
{
    c = (c-32)*5/9;  //還原c
    c = c+272.149994;
}
void FCK (double &f) //指標
{
    f = (f-32)*5/9;
}
void FCK2 (double &f) //指標
{
    f= f*1.8 +32; //還原f
    f = (f-32)*5/9+272.149994;
}
void KCF (double &k) //指標
{
    k = k-273.149994;
}
void KCF2 (double &k) //指標
{
    k = k+272.149994; //還原K
    k = (k-272.149994)*1.8 +32;
}
int main()
{
    double c=0,f=0,k=0;
    cout << "Please enter temperature C:";
    cin >> c;
    cout << "Please enter temperature F:";
    cin >> f;
    cout << "Please enter temperature K:";
    cin >> k;
    CFK(c);  //變
    FCK(f);  //變
    KCF(k);  //變

    cout << "First Temperature is" << c << "F and ";
    CFK2(c);                                       //變
    cout << c << "K";
    cout << endl << "First Temperature is" << f << "C and " ;
    FCK2(f);                                      //變
    cout << f << "K";
    cout << endl << "First Temperature is" << k << "C and ";
    KCF2(k);                                      //變
    cout << k << "F";
}


