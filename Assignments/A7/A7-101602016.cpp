#include<iostream>
#include <math.h>
using namespace std;

double C=0,F=0,K=0,x=0;//各種變數

void CtoF(double &x)//攝氏轉華氏
{
    x=C*9/5+32;
}
void CtoK(double&x)//攝氏轉凱氏
{
    x=C+273.15;
}

void FtoC(double&x)//華氏轉攝氏
{
    x=(F-32)*5/9;
}
void FtoK(double&x)//華氏轉凱氏
{
    x=(F+459.67)*5/9;
}

void KtoC(double&x)//凱氏轉攝氏
{
    x=K-273.15;
}
void KtoF(double&x)//凱氏轉華氏
{
    x=K*9/5-459.67;
}

int main()
{
    cout<<"Please enter temperature C : ";
    cin>>C;
    cout<<"Please enter temperature F : ";
    cin>>F;
    cout<<"Please enter temperature K : ";
    cin>>K;
    //請使用者輸入三種溫度

    //以下為，將x經由各個函數計算後輸出
    CtoF(x);
    cout<<"First  Temperature is "<<x;
    CtoK(x);
    cout<<"F and "<<x<<"K"<<endl;

    FtoC(x);
    cout<<"First  Temperature is "<<x;
    FtoK(x);
    cout<<"C and "<<x<<"K"<<endl;

    KtoC(x);
    cout<<"First  Temperature is "<<x;
    KtoF(x);
    cout<<"C and "<<x<<"F"<<endl;

    return 0;
}
