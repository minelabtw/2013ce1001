#include <iostream>
using namespace std;
// define the references; void(do not have return).
void C ( double &);
void B ( double &);
void A ( double &);

int main()
{
    double Fahrenheit;
    double Celsius;
    double Kelvin;


    cout << "Please enter temperature C : ";
    cin >> Celsius;
    cout << "Please enter temperature F : ";
    cin >> Fahrenheit;
    cout << "Please enter temperature K : ";
    cin >> Kelvin;
// call out the functions.
    C(Celsius);
    cout << endl;
    B(Fahrenheit);
    cout << endl;
    A(Kelvin);
    cout << endl;

    return 0;
}
void C ( double &Celsius)
{
    double fah;
    double kel;

    fah = Celsius *9/5 + 32;
    kel = Celsius + 273.15;
    cout << "First Temperature is " << fah << "F and " << kel << "K";
}
void B ( double &Fahrenheit) // & = change the initial numbers.
{
    double cel;
    double kel;

    cel = (Fahrenheit - 32) *5/9;
    kel = (Fahrenheit - 32) *5/9 + 273.15;
    cout << "Second Temperature is " << cel << "C and " << kel << "K";
}
void A ( double &Kelvin)
{
    double cel;
    double fah;

    cel = Kelvin - 273.15;
    fah = (Kelvin - 273.15) *9/5 + 32;
    cout << "Third Temperature is " << cel << "C and " << fah << "F";
}
