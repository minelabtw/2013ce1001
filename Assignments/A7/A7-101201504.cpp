#include<iostream>
using namespace std ;
void function1 (float &c,float &b,float &d ) //攝氏換成華氏和凱式
{
    b=1.8*c+32 ;
    d=c+273.149994;
}
void function2 (float &f,float &b,float &d) //華氏轉換成攝氏和凱式
{
    b=(f-32)*5/9 ;
    d=b+273.149994;
}
void function3 (float &k,float &b,float &d)  //凱式轉換成攝氏和華氏
{
    b=k-273.149994 ;
    d=1.8*b+32 ;
}
int main ()
{
    float c,f,k ;
    cout <<"Please enter temperature C : " ;
    cin >>c ;
    cout <<"Please enter temperature F : " ;
    cin >>f ;
    cout <<"Please enter temperature K : " ;
    cin >>k ;
    float b,d ;
    function1(c,b,d) ;
    cout <<"First  Temperature is " <<b<<"F "<<"and "<<d<<"K"<<endl ;
    function2 (f,b,d) ;
    cout <<"Second  Temperature is " <<b<<"C "<<"and "<<d<<"K"<<endl  ;
    function3 (k,b,d) ;
    cout <<"Third  Temperature is " <<b<<"C "<<"and "<<d<<"F"<<endl  ;
    return 0 ;
}
