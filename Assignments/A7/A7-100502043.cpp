#include<iostream>

using namespace std;

void tc_f(float &);  //宣告C轉F之函式
void tc_k(float &);  //宣告C轉K之函式
void tf_c(float &);  //宣告F轉C之函式
void tf_k(float &);  //宣告F轉K之函式
void tk_c(float &);  //宣告K轉C之函式
void tk_f(float &);  //宣告K轉F之函式

int main()
{
    float tc=0,tf=0,tk=0;
    float tcf=0,tck=0,tfc=0,tfk=0,tkc=0,tkf=0;

    cout << "Please enter temperature C : ";
    cin >> tc;
    cout << "Please enter temperature F : ";
    cin >> tf;
    cout << "Please enter temperature K : ";
    cin >> tk;

    tcf=tc;tck=tc;  //使欲轉換Tc之兩變數之值=Tc
    tfc=tf;tfk=tf;  //使欲轉換Tf之兩變數之值=Tf
    tkc=tk;tkf=tk;  //使欲轉換Tk之兩變數之值=Tk
    tc_f(tcf);  //呼叫C轉F之函式
    tc_k(tck);  //呼叫C轉K之函式
    tf_c(tfc);  //呼叫F轉C之函式
    tf_k(tfk);  //呼叫F轉K之函式
    tk_c(tkc);  //呼叫K轉C之函式
    tk_f(tkf);  //呼叫K轉F之函式
    cout << "First  Temperature is " << tcf << "F and " << tck << "K" << endl;
    cout << "Second  Temperature is " << tfc << "C and " << tfk << "K" << endl;
    cout << "Third  Temperature is " << tkc << "C and " << tkf << "F" << endl;

    return 0;
}

void tc_f(float &tcf)  //C轉F之函式運算
{
    tcf = tcf*9/5+32;
}
void tc_k(float &tck)  //C轉K之函式運算
{
    tck = tck+273.15;
}
void tf_c(float &tfc)  //F轉C之函式運算
{
    tfc = (tfc-32)*5/9;
}
void tf_k(float &tfk)  //F轉K之函式運算
{
    tfk = (tfk-32)*5/9+273.15;
}
void tk_c(float &tkc)  //K轉C之函式運算
{
    tkc = tkc-273.15;
}
void tk_f(float &tkf)  //K轉F之函式運算
{
    tkf = (tkf-273.15)*9/5+32;
}
