#include<iostream>
using namespace std ;
void temperatureC(double &,double &,double &) ;
void temperatureF(double &,double &,double &) ;     //宣告變數
void temperatureK(double &,double &,double &) ;
int main()
{
    //本次作業的目的在於運用指標去尋找所輸入的值並加以計算出各溫度制間的轉換
    double c=0 ;
    double a=0 ;
    double b=0 ;
    double f=0 ;
    double m=0 ;
    double n=0 ;
    double k=0 ;
    double p=0 ;
    double q=0 ;

    cout<<"Please enter temperature C : " ;
    cin>>c ;

    cout<<"Please enter temperature F : " ;
    cin>>f ;

    cout<<"Please enter temperature K : " ;
    cin>>k ;

    temperatureC(c,a,b) ;
    cout<<"First Temperature is "<<a<<"F"<<" and "<<b<<"K"<<endl ;    //經由副程式將式子運算完後便代回主程式

    temperatureF(f,m,n) ;
    cout<<"Second Temperature is "<<m<<"C"<<" and "<<n<<"K"<<endl ;

    temperatureK(k,p,q) ;
    cout<<"Third Temperature is "<<p<<"C"<<" and "<<q<<"F"<<endl ;


    return 0 ;


}//副程式
void temperatureC(double &c,double &a,double &b)      //加上&(取址符號)使前面的function能找到使用者所給定的數值並將其帶入運算
{
    a=c*9/5+32 ;
    b=c+273.15 ;

}
void temperatureF(double &f,double &m,double &n)
{


    m=(f-32)*5/9 ;
    n=(f+459.67)*5/9 ;
}
void temperatureK(double &k,double &p,double &q)
{


    p=k-273.15 ;
    q=k*1.8-459.67 ;
}
