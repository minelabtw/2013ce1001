#include<iostream>
void Celsius(double c,double &cf,double &ck)    //Celsiun to others
{
   cf=c*9/5+32;
   ck=c+273.15;
}
void Fahrenheit(double f,double &fc,double &fk)    //Fahrenheit to others
{
   fc=(f-32)*5/9;
   fk=(f+459.67)*5/9;
}
void Kelvin(double k,double &kc,double &kf)  //Kelvin to others
{
   kc=k-273.15;
   kf=k*9/5-459.67;
}
int main()
{
   double c,f,k,cf,ck,fc,fk,kc,kf;  //Declaration

   std::cout<<"Please enter temperature C : ";  //Input
   std::cin>>c;
   std::cout<<"Please enter temperature F : ";
   std::cin>>f;
   std::cout<<"Please enter temperature K : ";
   std::cin>>k;

   Celsius(c,cf,ck);    //Calc
   Fahrenheit(f,fc,fk);
   Kelvin(k,kc,kf);

   std::cout<<"First  Temperature is "<<cf<<"F and "<<ck<<"K\n";  //Output
   std::cout<<"Second Temperature is "<<fc<<"C and "<<fk<<"K\n";
   std::cout<<"Third  Temperature is "<<kc<<"C and "<<kf<<"F\n";

   return 0;
}
