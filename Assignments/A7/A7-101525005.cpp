#include <iostream>
#include <iomanip>
using namespace std;
//宣告prototype
void transCToF(double &tempC);
void transCToK(double &tempC);
void transFToC(double &tempF);
void transFToK(double &tempF);
void transKToC(double &tempK);
void transKToF(double &tempK);

int main()
{
    double tempK=0,tempF=0,tempC=0;



    cout << "Please enter temperature C : " ;
    cin >> tempC;
    cout << "Please enter temperature F : " ;
    cin>>tempF;
    cout << "Please enter temperature K : " ;
    cin>>tempK;
//about C
    transCToF(tempC);
    cout << "First Temperature is " << tempC<<"F and ";

    transFToK(tempC);
     cout <<  tempC<<"K" << endl;
//about F
    transFToC(tempF);
    cout << "Second Temperature is " << tempF<<"C and ";
    transCToK(tempF);
    cout << tempF<<"K" << endl;
//about K
transKToC(tempK);
    cout << "Third Temperature is " << tempK<<"C and ";
    transCToF(tempK);
    cout << tempK<<"F" << endl;
    return 0;
}
//將C轉換成K和F的函式
    void transCToF(double &tempC)
    {

        tempC=tempC*9/5+32;

    }
    void transCToK(double &tempC)
    {

        tempC=tempC+273.15;

    }
    //將F轉換成K和C的函式
    void transFToC(double &tempF)
    {

        tempF=(tempF-32)*5/9;

    }
    void transFToK(double &tempF)
    {

        tempF=(tempF-32)*5/9+273.15;

    }
    //將K轉換成F和C的函式
    void transKToC(double &tempK)
    {

        tempK=tempK-273.15;

    }
    void transKToF(double &tempK)
    {

        tempK=(tempK-273.15)*9/5+32;

    }
