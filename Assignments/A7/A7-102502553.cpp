#include<iostream>
using namespace std;

void CelsiusByReference(double &);//宣告一個攝氏函式，沒有回傳值且指標為double
void FahrenheitByReference(double &);//宣告一個華氏函式，沒有回傳值且指標為double
void KByReference(double &);//宣告一個絕對溫度函式，沒有回傳值且指標為double

int main()
{
    double c=0;
    double f=0;
    double k=0;

    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    cout<<"Please enter temperature K : ";
    cin>>k;

    cout<<"First  Temperature is ";
    FahrenheitByReference(c);//叫出華氏的值，且c的值也變成華氏
    cout<<c<<"F and ";
    KByReference(c);//叫出絕對溫度的值，且c的值也變成絕對溫度
    cout<<c<<"K";

    cout<<"\nSecond Temperature is ";
    KByReference(f);
    CelsiusByReference(f);//叫出攝氏的值，且f的值也變成攝氏
    cout<<f<<"C and ";
    FahrenheitByReference(f);
    KByReference(f);
    cout<<f<<"K";

    cout<<"\nThird  Temperature is ";
    CelsiusByReference(k);
    cout<<k<<"C and ";
    FahrenheitByReference(k);
    cout<<k<<"F";

    return 0;
}

void CelsiusByReference(double &Celsius)//計算絕對溫度變成攝氏
{
    Celsius=Celsius-273.15;
}

void FahrenheitByReference(double &Fahrenheit)//計算攝氏變成華氏
{
    Fahrenheit=Fahrenheit*9/5+32;
}

void KByReference(double &K)//計算華氏變成絕對溫度
{
    K=(K-32)*5/9+273.15;
}
