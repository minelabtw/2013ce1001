#include<iostream>
using namespace std;
void temperature_CF(double &C);//prototype
void temperature_CK(double &C);//prototype
void temperature_FC(double &F);//prototype
void temperature_FK(double &F);//prototype
void temperature_KC(double &K);//prototype
void temperature_KF(double &K);//prototype
int main()
{
    double C=0,c=0,F=0,f=0,K=0,k=0;//宣告
    cout<<"Please enter temperature C : ";//顯示字串
    cin>>C;//輸入
    c=C;//計算
    temperature_CF(C);//計算
    temperature_CK(c);//計算
    cout<<"Please enter temperature F : ";//顯示字串
    cin>>F;//輸入
    f=F;
    temperature_FC(F);
    temperature_FK(f);
    cout<<"Please enter temperature K : ";//顯示字串
    cin>>K;//輸入
    k=K;//計算
    temperature_KC(K);//計算
    temperature_KF(k);//計算
    cout<<"First  Temperature is "<<C<<"F and "<<c<<"K\n";//顯示字串
    cout<<"Second Temperature is "<<F<<"C and "<<f<<"K\n";//顯示字串
    cout<<"Third  Temperature is "<<K<<"C and "<<k<<"F\n";//顯示字串
    return 0;
}
void temperature_CF(double &C)//函數
{
    C=9*C/5+32;//計算
    return;
}
void temperature_CK(double &C)//函數
{
    C=C+273.149994;//計算
    return;
}
void temperature_FC(double &F)//函數
{
    F=(F-32)*5/9;//計算
    return;
}
void temperature_FK(double &F)//函數
{
    F=(F-32)*5/9+273.149994;//計算
    return;
}
void temperature_KC(double &K)//函數
{
    K=K-273.149994;//計算
    return;
}
void temperature_KF(double &K)//函數
{
    K=(K-273.149994)*9/5+32;//計算
    return;
}
