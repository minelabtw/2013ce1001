#include<iostream>
using namespace std;

double t,t1,t2;

void cf(double &t)          //宣告攝氏轉華氏的函式
{
    t=1.8*t+32;
}

void ck(double &t)          //宣告華氏轉克氏的函式
{
    t=(t-32)*5/9+273.15;
}

void fc(double &t1)         //宣告華氏轉攝氏的函式
{
    t1=(t1-32)*5/9;
}

void fk(double &t1)         //宣告攝氏轉克氏的函式
{
    t1=t1+273.15;
}

void kc(double &t2)         //宣告克氏轉攝氏的函式
{
    t2=t2-273.15;
}

void kf(double &t2)         //宣告攝氏轉華氏的函式
{
    t2=1.8*t2+32;
}

int main()
{
    cout << "Please enter temperature C : ";
    cin >> t;
    cout << "Please enter temperature F : ";
    cin >> t1;
    cout << "Please enter temperature K : ";
    cin >> t2;
    cf(t);
    cout << "First  Temperature is " << t << "F and ";
    ck(t);
    cout << t << "K\n";
    fc(t1);
    cout << "Second Temperature is " << t1 << "C and ";
    fk(t1);
    cout << t1 << "K\n";
    kc(t2);
    cout << "Third  Temperature is " << t2 << "C and ";
    kf(t2);
    cout << t2 << "F";
    return 0;
}
