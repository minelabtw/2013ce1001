#include <iostream>
using namespace std;

void temperatureC1(double &c)   //宣告函式將攝氏轉成華氏
{
    c=1.8*c+32;
}
void temperatureC2(double &c1)   //將攝氏轉為凱氏
{
    c1=c1+273.15;
}
void temperatureF1(double &f)   //將華氏轉成攝氏
{
    f=(f-32)/1.8;
}
void temperatureF2(double &f1)   //將華氏轉成凱氏
{
    f1=((f1-32)/1.8)+273.15;
}
void temperatureK1(double &k)   //將凱氏轉為攝氏
{
    k=k-273.15;
}
void temperatureK2(double &k1)   //將凱氏轉為華氏
{
    k1=1.8*(k1-273.15)+32;
}

int main()
{
    double c;                               //宣告變數
    double f;
    double k;
    double c1;
    double f1;
    double k1;
    cout<<"Please enter temperature C : "; //輸出字串
    cin>>c;                                //輸入c值
    c1=c;                                  //使c值不受到上一個函式的影響
    cout<<"Please enter temperature F : ";
    cin>>f;
    f1=f;
    cout<<"Please enter temperature K : ";
    cin>>k;
    k1=k;
    cout<<"First  Temperature is ";        //輸出字串
    temperatureC1(c);                      //呼叫函式
    cout<<c<<"F and ";                     //輸出函式計算結果
    temperatureC2(c1);                     //呼叫函式
    cout<<c1<<"K"<<endl;                   //使用c值進行下一函式的計算
    cout<<"Second Temperature is ";
    temperatureF1(f);
    cout<<f<<"C and ";
    temperatureF2(f1);
    cout<<f1<<"K"<<endl;
    cout<<"Third  Temperature is ";
    temperatureK1(k);
    cout<<k<<"C and ";
    temperatureK2(k1);
    cout<<k1<<"F";
    return 0;
}


