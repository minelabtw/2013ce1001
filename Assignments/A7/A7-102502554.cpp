#include <iostream>
using namespace std;

void CtoF (double &);
void CtoK (double &);
void FtoC (double &);
void FtoK (double &);
void KtoC (double &);
void KtoF (double &);//宣告下方需要用到的溫標轉換函式

int main ()
{
    double c;
    double f;
    double k;//宣告所要輸入之3個不同溫標的溫度

    cout << "Please enter temperature C : ";
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;//輸入3個不同溫標的溫度

    cout << "First  Temperature is ";
    CtoF (c);
    cout << c;//藉由函式進行溫標轉換後再輸出
    cout << "F and ";
    FtoK (c);//因為前面輸出後為F故使用FtoK
    cout << c;//藉由函式進行溫標轉換後再輸出
    cout << "K" << endl;
    cout << "Second  Temperature is ";
    FtoC (f);
    cout << f;//藉由函式進行溫標轉換後再輸出
    cout << "C and ";
    CtoK (f);//因為前面輸出後為C故使用CtoK
    cout << f;//藉由函式進行溫標轉換後再輸出
    cout << "K" << endl;
    cout << "Third  Temperature is ";
    KtoC (k);
    cout << k;//藉由函式進行溫標轉換後再輸出
    cout << "C and ";
    CtoF (k);//因為前面輸出後為C故使用CtoF
    cout << k;//藉由函式進行溫標轉換後再輸出
    cout << "F" << endl;

    return 0;
}

void CtoF (double &c)//C轉F
{
    c=c*9/5+32;
}

void CtoK (double &c)//C轉K
{
    c=c+273.15;
}

void FtoC (double &f)//F轉C
{
    f=(f-32)*5/9;
}

void FtoK (double &f)//F轉K
{
    f=(f+459.67)*5/9;
}

void KtoC (double &k)//K轉C
{
    k=k-273.15;
}

void KtoF (double &k)//K轉F
{
    k=k*9/5-459.67;
}
