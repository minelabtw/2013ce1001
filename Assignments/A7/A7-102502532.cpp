#include<iostream>
using namespace std;

void firstf( double & );              //function prototype
void firstfk( double & );                 //用&
void secondc( double & );
void secondck( double & );
void thirdc( double & );
void thirdcf( double & );

int main()
{
    double c =0;
    double f =0;
    double k =0;

    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    cout<<"Please enter temperature K : ";
    cin>>k;

    firstf( c );                                      //固定指向main的c
    cout<<"First  Temperature is "<<c<<"F and ";
    firstfk( c );                                        //固定指向main的c 但值有變
    cout<<c<<"K\n";
    secondc( f );                                           //固定指向main的f
    cout<<"Second Temperature is "<<f<<"C and ";
    secondck( f );
    cout<<f<<"K\n";
    thirdc( k );
    cout<<"Third  Temperature is "<<k<<"C and ";
    thirdcf( k );
    cout<<k<<"F";

    return 0;
}

void firstf( double &c )
{
    c = c *9/5 + 32;                   //c換成f
}
void firstfk( double &c )
{
    c = (c + 459.67) * 5/9;               //f換成k
}
void secondc( double &f )
{
    f = (f - 32) * 5/9;                      //f換c
}
void secondck( double &f )
{
    f = f + 273.15;                          //c k
}
void thirdc( double &k )
{
    k = k - 273.15;                         //k c
}
void thirdcf( double &k )
{
    k = k * 9/5 + 32;                         //c f
}
