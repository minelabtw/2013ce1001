#include<iostream>
using namespace std;

void CF(double &c)//設定一function將攝氏換算程華氏
{
    c=c*9/5+32;
}

void FK(double &f)//設定一function將華氏換算程凱氏
{
    f=(f-32)*5/9+273.15;
}

void KC(double &k)//設定一function將凱氏換算程攝氏
{
    k=k-273.15;
}

int main()
{
    double c,f,k;//宣告變數c,f,k來代表輸入的攝氏、華氏、凱氏
    cout << "Please enter temperature C : ";//輸出字串要求輸入溫度並將他宣告給相對應的變數
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;

    CF(c);//將輸入的c經過CF(c)換算成華氏並輸出字串
    cout << "First  Temperature is " << c;
    FK(c);//將運算後的c經過FK(c)換算成凱氏並輸出字串
    cout << "F and " << c << "K\n";
    FK(f);//將輸入的f經過FK(f)轉為凱氏在經過KC(c)轉為攝氏並輸出字串
    KC(f);
    cout << "Second Temperature is " << f;
    CF(f);//將運算成攝氏的f在經過CF(f)轉換回華氏在經過FK(f)轉換成凱氏並輸出字串
    FK(f);
    cout << "C and " << f << "K\n";
    KC(k);//將輸入的k經過KC(c)換算成攝氏並輸出字串
    cout << "Third  Temperature is " << k;
    CF(k);//將運算後的k經過CF(c)換算成華氏並輸出字串
    cout << "C and " << k << "F\n";

    return 0;//結束
}
