#include <iostream>
using namespace std;

void T1(double C,double &f,double &k); //C的值用的是輸入的值所以不須用替代變數，但f和k為一開始不知道的值，若不用&設為替代變數，輸出的數值將會是暴表，而不是計算出的答案
void T2(double F,double &c,double &k);
void T3(double K,double &c,double &f);

int main()
{
    double C;
    double F;
    double K;
    double c;
    double f;
    double k;

    cout << "Please enter temperature C : ";
    cin >> C;
    cout << "Please enter temperature F : ";
    cin >> F;
    cout << "Please enter temperature K : ";
    cin >> K;

    T1(C,f,k); //引述下段用的為T1所算的數值
    cout << "First  Temperature is " << f << "F" << " and " << k  << "K" << endl;
    T2(F,c,k);
    cout << "Second  Temperature is " << c << "C" << " and " << k << "K" << endl;
    T3(K,c,f);
    cout << "Third  Temperature is " << c << "C" << " and " << f << "F";

    return 0;
}

void T1(double C,double &f,double &k) //給void T1定義 f和k用替代變數 使得f和k輸出時為下列所運算的 而非未知的數值
{
    f=9*C/5+32;
    k=C+273.15;
}

void T2(double F,double &c,double &k) //給void T2定義
{
    c=(F-32)/9*5;
    k=(F+459.67)*5/9;
}

void T3(double K,double &c,double &f) //給void T3定義
{
    c=K-273.15;
    f=K*9/5-459.67;
}
