#include <iostream>

using namespace std;

void f1(double a,double &s,double &t);      //ㄧ计
void f2(double b,double &s,double &t);
void f3(double c,double &s,double &t);


int main()
{
    double a,b,c,s,t;
    cout << "Please enter temperature C: ";
    cin >> a;
    cout << "Please enter temperature F: ";
    cin >> b;
    cout << "Please enter temperature K: ";
    cin >> c;
    f1(a,s,t);                                    //㊣ㄧ计f1
    cout << "First Temperature is " << s << "F and " << t<< "K" <<endl;
    f2(b,s,t);                                    //㊣ㄧ计f2
    cout << "Second Temperature is " << s << "C and " << t << "K" << endl;
    f3(c,s,t);                                    //㊣ㄧ计f3
    cout << "Third Temperature is " << s << "C and " << t << "F"<< endl;

}




void f1(double a,double &s,double &t)      //盢C锣ΘF蛤Kㄧ计
{
    s= a * 9/5 + 32;
    t= a + 273.15;
}

void f2(double b,double &s,double &t)      //盢F锣ΘC蛤Kㄧ计
{
    s=(b - 32)*5/9;
    t= s + 273.15;
}

void f3(double c,double &s,double &t)      //盢K锣ΘC蛤Fㄧ计
{
    s= c - 273.15;
    t=(c-273.15) * 9/5  + 32;
}
