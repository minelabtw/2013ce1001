#include <iostream>

using namespace std ;

// 設定三個副函式
void ktrans( double&k ,double&c , double&f ) ;
void ctrans( double&c , double&f, double&k) ;
void ftrans( double&f , double&c  ,double&k) ;

int main ()
{
//大寫為使用輸入的變數 小寫是計算後輸出的答案
    double K, C, F;
    double k , f, c ;

    cout << "Please enter temperature C : " ;
    cin >> C;
    cout << "Please enter temperature F : "  ;
    cin >> F;
    cout << "Please enter temperature K : "  ;
    cin >> K;
//把變數帶入函式
    ctrans(C ,f, k);
    cout << "First  Temperature is " << f << "F and " <<  k << "K "  << endl ;
    ftrans( F ,c , k);
    cout << "Second Temperature is " << c <<"C and "<< k << " K " <<endl ;
    ktrans( K ,c, f) ;
    cout << "Third  Temperature is" << c << "C and "<< f <<"F" << endl ;

    return 0 ;
}
//使用參照
void ctrans( double&c , double&f, double&k)
{
    k = c +273.15 ;
    f = c*9/5 +32 ;

}

void ktrans( double&k ,double&c , double&f )
{

    c = k - 273.15 ;
    f = (k-273.15)*9/5 +32 ;
}

void ftrans ( double&f , double&c  ,double&k)
{
    c = (f-32)*5/9 ;
    k = (f-32)*5/9 +273.15 ;

}
