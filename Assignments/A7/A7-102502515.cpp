#include <iostream>

using namespace std ;

void FtoC(double &);                                        //function prototype
void CtoF(double &);
void CtoK(double &);
void KtoC(double &);

int main()
{
    double numberC ;                                        //宣告變數(攝氏)
    double numberF ;                                        //華氏
    double numberK ;                                        //凱氏
    double x ;                                              //另一變數接攝氏溫度

    cout << "Please enter temperature C : " ;
    cin  >> numberC ;
    cout << "Please enter temperature F : " ;
    cin  >> numberF ;
    cout << "Please enter temperature K : " ;
    cin  >> numberK ;

    x=numberC;                                              //將numberC給x，因為numberC要使用到兩次，故另設一變數x承載numberC
    cout << "First  Temperature is " ;
    CtoF(x) ;                                               //call CtoF，將x的值跳到CtoF
    cout << x << "F and " ;
    CtoK(numberC) ;                                         //call CtoK，將numberC的值跳到CtoK
    cout << numberC << "K" << endl ;

    cout << "Second Temperature is " ;
    FtoC(numberF) ;                                         //call FtoC
    cout << numberF << "C and " ;
    CtoK(numberF) ;                                         //call CtoK，因為直接從攝氏轉凱氏，就可以省下一條公式
    cout << numberF << "K" << endl ;

    cout << "Third Temperature is " ;
    KtoC(numberK) ;                                         //call KtoC
    cout << numberK << "C and " ;
    CtoF(numberK) ;                                         //call CtoF
    cout << numberK << "F" << endl ;

    return 0 ;
}


void CtoF(double &I)                                        //設定void中該執行的事(攝氏轉華氏)，並利用&回傳得出的值
{
    I=I*1.8+32;  //C→F
}

void FtoC(double &I)                                        //設定void中該執行的事(華氏轉攝氏)，並利用&回傳得出的值
{
    I=(I-32)*5/9;  //F→C
}

void CtoK(double &I)                                        //設定void中該執行的事(攝氏轉凱氏)，並利用&回傳得出的值
{
    I=I+273.15;  //C→K
}

void KtoC(double &I)                                        //設定void中該執行的事(凱氏轉攝氏)，並利用&回傳得出的值
{
    I=I-273.15; //K→C
}
