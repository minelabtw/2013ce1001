#include <iostream>

using namespace std;
void cTOfk(double i,double &j,double &k)//i=c,j=f
{
    j=9.0/5.0*i+32;
    k=i+273.15;
}
void fTOck(double i,double &j,double &k)//i=f,j=c
{
    j=(i-32)*5/9;
    k=(i+459.67)*5/9;
}
void kTOcf(double i,double &j,double &k)//i=k,j=c,k=f
{
    j=i-273.15;
    k=i*9/5-459.67;
}
int main()
{
    double input=0;
    double answer1=0;
    double answer2=0;
    cin >> input;
    cTOfk(input,answer1,answer2);
    cout << "First  Temperature is " << answer1 << "F and " << answer2 << "K" << endl;
    cin >> input;
    fTOck(input,answer1,answer2);
    cout << "Second Temperature is " << answer1 << "C and " << answer2 << "K" << endl;
    cin >> input;
    kTOcf(input,answer1,answer2);
    cout << "Third  Temperature is "<< answer1 << "C and " << answer2 << "F" << endl;
    return 0;
}
