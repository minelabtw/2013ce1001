#include<iostream>
using namespace std;
void ctokf(float &c,float &k,float &f);
void ftock(float &c,float &k,float &f);
void ktocf(float &c,float &k,float &f);
int main()
{
    float c,c2,c3=0;
    float k,k1,k2=0;
    float f,f1,f3=0;                                //定義型態為float的很多變數，並使其初始值為0
    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    cout<<"Please enter temperature K : ";
    cin>>k;                                          //輸入要換算的三種溫度值
    ctokf(c,k1,f1);                                  //將c,k1,f1丟到函式裡去運算
    cout<<"First  Temperature is "<<f1<<"F and "<<k1<<"K"<<endl;
    ftock(f,c2,k2);                                  //將f,c2,k2丟到函式裡去運算
    cout<<"Second Temperature is "<<c2<<"C and "<<k2<<"K"<<endl;
    ktocf(k,c3,f3);                                  //將k,c3,f3丟到函式裡去運算
    cout<<"Third  Temperature is "<<c3<<"C and "<<f3<<"F"<<endl;

    return 0;
}
void ctokf(float &c,float &k1,float &f1)             //C轉K和F
{
    k1=c+273.15;
    f1=c*1.8+32;
}
void ftock(float &f,float &c2,float &k2)             //F轉C和K
{
    c2=(f-32)*5/9;
    k2=(f+459.67)*5/9;
}
void ktocf(float &k,float &c3,float &f3)             //K轉C和F
{
    c3=k-273.15;
    f3=k*9/5-459.67;
}
