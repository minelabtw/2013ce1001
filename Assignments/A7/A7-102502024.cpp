#include<iostream>
using namespace std;
void ctof(float &temperaturec);  //ctof functionprototype
void ctok(float &temperaturec2);  //ctok functionprototype
void ftoc(float &temperaturef);  //ftoc functionprototype
void ftok(float &temperaturef2);  //ftok functionprototype
void ktoc(float &temperaturek);  //ktoc functionprototype
void ktof(float &temperaturek2);  //ktof functionprototype

int main()
{
    float temperaturec=0,temperaturef=0,temperaturek=0,temperaturec2=0,temperaturef2=0,temperaturek2=0;  //宣告型別可為小數的溫度
    cout<<"Please enter temperature C : ";  //輸出題目
    cin>>temperaturec;  //輸入溫度
    cout<<"Please enter temperature F : ";  //輸出題目
    cin>>temperaturef;  //輸入溫度
    cout<<"Please enter temperature K : ";  //輸出題目
    cin>>temperaturek;  //輸入溫度
    while(temperaturek<0)  //判斷範圍
    {
        cout<<"Out of range!\n";
        cout<<"Please enter temperature K : ";
        cin>>temperaturek;
    }
    temperaturec2=temperaturec;
    ctof(temperaturec);
    cout<<"First  Temperature is "<<temperaturec<<" and ";  //輸出溫度
    ctok(temperaturec2);
    cout<<temperaturec2<<endl;
    temperaturef2=temperaturef;
    ftoc(temperaturef);
    cout<<"Second Temperature is "<<temperaturef<<" and ";  //輸出溫度
    ftok(temperaturef2);
    cout<<temperaturef2<<endl;
    temperaturek2=temperaturek;
    ktoc(temperaturek);
    cout<<"Third  Temperature is "<<temperaturek<<" and ";  //輸出溫度
    ktof(temperaturek2);
    cout<<temperaturek2;
    return 0;
}
void ctof(float &temperaturec)  //計算溫度
{
    temperaturec=temperaturec*9/5+32;
}
void ctok(float &temperaturec2)  //計算溫度
{
    temperaturec2=temperaturec2+273.15;
}
void ftoc(float &temperaturef)  //計算溫度
{
    temperaturef=(temperaturef-32)*5/9;
}
void ftok(float &temperaturef2)  //計算溫度
{
    temperaturef2=(temperaturef2+459.67)*5/9;
}
void ktoc(float &temperaturek)  //計算溫度
{
    temperaturek=temperaturek-273.15;
}
void ktof(float &temperaturek2)
{
    temperaturek2=temperaturek2*9/5-459.67;
}

