#include <iostream>
using namespace std;
void C(double c , double &result , double &result2)
{
    //&result and &result2 are declared to build connection between the result in this function and that one in main block
    result=c*1.8+32;
    result2=c+273.15;
}//set up a function to transform C into F and K
void F(double f , double &result , double &result2)
{
    result=(f-32)/1.8;
    result2=result+273.15;
}//set up a function to transform F into C and K
void K(double k , double &result , double &result2)
{
    result=k-273.15;
    result2=result*1.8+32;
}//set up a function to transform K into C and F
int main()
{
    double c,f,k,result,result2;
    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    cout<<"Please enter temperature K : ";
    cin>>k;
    C(c,result,result2);//call the function and send result to build connection so that the value we need could be printed in main()
    cout<<"First  Temperature is "<<result<<"F and "<<result2<<"K"<<endl;
    F(f,result,result2);//in this case , the function we declare is void type , which means that there is no returning value
    cout<<"Second Temperature is "<<result<<"C and "<<result2<<"K"<<endl;
    K(k,result,result2);//so we have to make "call by reference" , so that we could print the result of the calculation.
    cout<<"Third  Temperature is "<<result<<"C and "<<result2<<"F"<<endl;
    return 0;
}
