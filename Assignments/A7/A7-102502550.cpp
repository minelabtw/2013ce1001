#include <iostream>

using namespace std;

void C(double&,double&,double&);                                                   //函數原型
void F(double&,double&,double&);
void K(double&,double&,double&);

int main()
{
    double temp[9]={};                                                             //暫存結果的變數

    cout<<"Please enter temperature C : ";
    cin>>temp[0];
    C(temp[0],temp[1],temp[2]);
    cout<<"Please enter temperature F : ";
    cin>>temp[4];
    F(temp[3],temp[4],temp[5]);
    cout<<"Please enter temperature K : ";
    cin>>temp[8];
    K(temp[6],temp[7],temp[8]);

    cout<<"First  Temperature is "<<temp[1]<<"F and "<<temp[2]<<"K"<<endl;         //輸出結果
    cout<<"Second  Temperature is "<<temp[3]<<"C and "<<temp[5]<<"K"<<endl;
    cout<<"Third  Temperature is "<<temp[6]<<"C and "<<temp[7]<<"F";


    return 0;
}
void C(double &c,double &f,double &k)                                               //函數內容
{
    f=c*9/5+32;
    k=c+273.15;

}
void F(double &c,double &f,double &k)
{
    c=(f-32)*5/9;
    k=(f+459.67)*5/9;
}
void K(double &c,double &f,double &k)
{
    c=k-273.15;
    f=k*9/5-459.67;
}

