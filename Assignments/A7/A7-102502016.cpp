#include <iostream>

using namespace std;

void temperature(float &c,float &f,float &k,float &q,float &w,float &e,float &r,float &t,float &y)//指標
{
    q=(c*9)/5+32;
    w=c+273.15;
    e=((f-32)*5)/9;
    r=((f-32)*5)/9+273.15;
    t=k-273.15;
    y=((k-273.15)*9)/5+32;
}
int main()
{
    float c;//c=攝氏,f=華氏,k=克氏
    float f;
    float k;
    float q;
    float w;
    float e;
    float r;
    float t;
    float y;

    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    cout<<"Please enter temperature K : ";
    cin>>k;
    while (k<0)//克氏最小為0
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter temperature K : ";
        cin>>k;
    }
    temperature(c,f,k,q,w,e,r,t,y);//引用指標
    cout<<"First  Temperature is "<<q<<"F and "<<w<<"K"<<endl;
    cout<<"Second Temperature is "<<e<<"C and "<<r<<"K"<<endl;
    cout<<"Third  Temperature is "<<t<<"C and "<<y<<"F";
    return 0;
}
