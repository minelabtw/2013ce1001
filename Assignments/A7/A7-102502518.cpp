#include <iostream>

using namespace std;

int main()
{
    double c=0;
    double f=0;
    double k=0;
    double ans1=0;
    double ans2=0;

    double c_fk(double,double &,double &);
    double f_ck(double,double &,double &);
    double k_cf(double,double &,double &);


    cout<<"Please enter temperature C : ";
    cin>>c;

    cout<<"Please enter temperature F : ";
    cin>>f;

    cout<<"Please enter temperature K : ";
    cin>>k;

    c_fk(c,ans1,ans2);
    cout<<"First  Temperature is "<<ans1<<"F"<<" and "<<ans2<<"K"<<endl;

    f_ck(f,ans1,ans2);
    cout<<"Second Temperature is "<<ans1<<"C"<<" and "<<ans2<<"K"<<endl;

    k_cf(k,ans1,ans2);
    cout<<"Third  Temperature is "<<ans1<<"C"<<" and "<<ans2<<"F"<<endl;

    return 0;
}

double c_fk(double c,double &f,double &k)
{
    f=c*9/5+32;
    k=c+273.15;
}

double f_ck(double f,double &c,double &k)
{
    c=(f-32)*5/9;
    k=c+273.15;
}

double k_cf(double k,double &c,double &f)
{
    c=k-273.15;
    f=c*9/5+32;
}
