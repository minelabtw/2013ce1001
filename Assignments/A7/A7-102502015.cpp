#include <iostream>
using namespace std;
void temperature(float &a,float&b,float&c,float&d,float&e,float&f)         //轉換函數 參照各個變數
{
    b=a+273.15;                                                                 //攝氏轉K
    a=(9*a/5)+32;                                                               //攝氏轉華
    d=(c-32)*5/9;                                                               //華氏轉攝氏
    c=((c-32)*5/9)+273.15;                                                      //華氏轉克氏
    f=e-273.15;                                                                 //K轉攝氏
    e=(9*(e-273.15)/5)+32;                                                      //K轉華氏
}
int main()
{
    float a;           //a= 輸入攝氏 =輸出F
    float b;           //b= C輸出k
    float c;           //c= 輸入華氏 =輸出K
    float d;           //d= F輸出C
    float e;           //e= 輸入K    =輸出F
    float f;           //f= 輸出C
    cout<<"Please enter temperature C : ";
    cin>>a;
    cout<<"Please enter temperature F : ";
    cin>>c;
    cout<<"Please enter temperature K : ";
    cin>>e;
    temperature(a,b,c,d,e,f);                                              //轉換
    cout<<"First  Temperature is "<<a<<"F and "<<b<<"K"<<endl;
    cout<<"Second Temperature is "<<d<<"C and "<<c<<"K"<<endl;
    cout<<"Third  Temperature is "<<f<<"C and "<<e<<"F";
    return 0;
}
