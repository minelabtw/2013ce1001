#include<iostream>
using namespace std;

void CtoF(double&);
void CtoK(double&);
void FtoC(double&);
void FtoK(double&);
void KtoC(double&);
void KtoF(double&);
int main()
{
    double c,f,k;
    cout << "Please enter temperature C : ";
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;
    CtoF(c);     //將C之值換成華氏度數
    cout << "First  Temperature is " << c << "F and ";
    CtoK(c);          //將c之值換成開氏度數
    cout << c << "K" <<endl;
    FtoC(f);     //將F之值換成攝氏度數
    cout <<"Second Temperature is " << f << "C and ";
    FtoK(f);     //將F之值換成開氏度數
    cout << f << "K"<<endl;
    KtoC(k);     //將K之值換成攝氏度數
    cout << "Third  Temperature is " << k << "C and ";
    KtoF(k);     //將K之值換成華氏度數
    cout << k << "F" << endl;
    return 0;
}
void CtoF(double& c)           //以下皆為換算
{
    c = c * 9/5 + 32;
}
void CtoK(double& c)
{
    c=(c+459.67)*5/9;
}
void FtoC(double& f)
{
    f = (f-32)*5/9;
}
void FtoK(double& f)
{
    f = f + 273.15;
}
void KtoC(double& k)
{
    k = k -273.15;
}
void KtoF(double& k)
{
    k = k * 9/5 +32;
}

