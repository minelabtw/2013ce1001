#include <iostream>
using namespace std;

void cf(double &c1);//宣告C換成F的方程,而&是main裡的指向，意思是提取main裡c1的值。
void ck(double &c2);//宣告C換成K的方程。
void fc(double &f1);//宣告F換成C的方程。
void fk(double &f2);//宣告F換成K的方程。
void kc(double &k1);//宣告K換成C的方程。
void kf(double &k2);//宣告K換成F的方程。

int main()
{
    double c1=0;//宣告變數。
    double c2=0;
    double f1=0;
    double f2=0;
    double k1=0;
    double k2=0;

    cout << "Please enter temperature C: ";//指示
    cin >> c1;//輸入
    c2=c1;//等於

    cout << "Please enter temperature F: ";
    cin >> f1;
    f2=f1;

    cout << "Please enter temperature K: ";
    cin >> k1;
    k2=k1;

    cf(c1);//進入方程運算，把c1代入方程
    ck(c2);
    fc(f1);
    fk(f2);
    kc(k1);
    kf(k2);
    cout << "First Temperature is " << c1 << "F" << " and " << c2 << "K" << endl;//顯示結果
    cout << "Second Temperature is " << f1 << "C" << " and " << f2 << "K" << endl;
    cout << "Third Temperature is " << k1 << "C" << " and " << k2 << "F" <<endl;

    return 0;
}

void cf(double &c1)//C換成F的方程，並宣告一個變數以便帶回
{
    c1 = c1 * 9/5 +32;//C>F換算方程,等於前方的c1是之後方程令的c1
}

void ck(double &c2)
{
    c2 = c2 + 273.15;//C>K
}

void fc(double &f1)
{
    f1 = (f1 - 32) * 5/9;//F>C
}

void fk(double &f2)
{
    f2 = (f2 + 459.67) * 5/9;//F>K
}

void kc(double &k1)
{
    k1 = k1 - 273.15;//K>C
}

void kf(double & k2)
{
    k2 = k2 * 9/5 - 459.67;//K>F
}
