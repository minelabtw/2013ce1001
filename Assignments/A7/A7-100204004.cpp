#include <iostream>
using namespace std;

void temperatureCf(double &);//攝氏溫度(C)轉華氏溫度(F)function的prototype
void temperatureCk(double &);//攝氏溫度(C)轉凱式溫度(K)function的prototype
void temperatureFc(double &);//華氏溫度(F)轉攝氏溫度(C)function的prototype
void temperatureFk(double &);//華氏溫度(F)轉凱式溫度(K)function的prototype
void temperatureKc(double &);//凱式溫度(K)轉攝氏溫度(C)function的prototype
void temperatureKf(double &);//凱式溫度(K)轉華氏溫度(F)function的prototype
int main()
{
    double C1,C2;//攝氏溫度(C)
    double F1,F2;//華氏溫度(F)
    double K1,K2;//凱式溫度(K)

    cout<<"Please enter temperature C : ";
    cin>>C1;
    C2=C1;

    cout<<"Please enter temperature F : ";
    cin>>F1;
    F2=F1;

    cout<<"Please enter temperature K : ";
    cin>>K1;
    K2=K1;

    temperatureFc(C1);//攝氏溫度(C)轉華氏溫度(F)
    cout<<"First  Temperature is "<<C1;
    temperatureKc(C2);//攝氏溫度(C)轉凱式溫度(K)
    cout<<"F and "<<C2<<"K "<<endl;

    temperatureCf(F1);//華氏溫度(F)轉攝氏溫度(C)
    cout<<"Second Temperature is "<<F1;
    temperatureKf(F2);//華氏溫度(F)轉凱式溫度(K)
    cout<<"C and "<<F2<<"K "<<endl;

    temperatureCk(K1);//凱式溫度(K)轉攝氏溫度(C)
    cout<<"Third  Temperature is "<<K1;
    temperatureFk(K2);//凱式溫度(K)轉華氏溫度(F)
    cout<<"C and "<<K2<<"F "<<endl;

    return 0;

}

void temperatureFc(double &C1)//攝氏溫度(C)轉華氏溫度(F)
{
    double Fc=C1*9/5+32;
    C1=Fc;
}
void temperatureKc(double &C2)//攝氏溫度(C)轉凱式溫度(K)
{
    double Kc=C2+273.15;
    C2=Kc;
}

void temperatureCf(double &F1)//華氏溫度(F)轉攝氏溫度(C)
{
    double Cf=(F1-32)*5/9;
    F1=Cf;
}
void temperatureKf(double &F2)//華氏溫度(F)轉凱式溫度(K)
{
    double Kf=(F2+459.67)*5/9;
    F2=Kf;
}

void temperatureCk(double &K1)//凱式溫度(K)轉攝氏溫度(C)
{
    double Ck=K1-273.15;
    K1=Ck;
}
void temperatureFk(double &K2)//凱式溫度(K)轉華氏溫度(F)
{
    double Fk=K2*9/5-459.67;
    K2=Fk;
}

