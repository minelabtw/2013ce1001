#include <iostream>

using namespace std;

void change (int a, double &b)
{
    switch(a)
    {
    case 1: // C->F
        b=b*1.8+32;
        break;
    case 2: // C->K
        b=b+273.1502;
        break;
    default:
        break;
    }
}

int main()
{

    double cc; // ��JC
    double fc; // ��JF
    double kc; // ��JK

    cout << "Please enter temperature C : ";
    cin >> cc;
    cout << "Please enter temperature F : ";
    cin >> fc;
    cout << "Please enter temperature K : ";
    cin >> kc;

    fc=(fc-32)*5/9; // F-> C
    kc=kc-273.1502; // K->C

    cout << "First  Temperature is " <<  cc*1.8+32; // C->F
    change(2,cc); // C-> K
    cout << "F and " << cc << "K\n";


    cout << "Second Temperature is " << fc; // F->C
    change(2,fc); // C->K
    cout << "C and " << fc << "K\n";

    cout << "Third  Temperature is " << kc; // K->C
    change(1,kc); // C->F
    cout << "C and " << kc << "F\n";

    return 0;
}
