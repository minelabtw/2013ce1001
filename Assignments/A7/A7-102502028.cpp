#include <iostream>

using namespace std ;

void tcf (double &temC) ;     // function prototype
void tck (double &temC) ;
void tfc (double &temF) ;
void tfk (double &temF) ;
void tkc (double &temK) ;
void tkf (double &temK) ;

int main ()
{
    double temC = 0 ;     //跑计
    double temF = 0 ;
    double temK = 0 ;

    cout << "Please enter temperature C : " ;           //块肈ヘ块璶―
    cin >> temC ;
    cout << "Please enter temperature F : " ;
    cin >> temF ;
    cout << "Please enter temperature K : " ;
    cin >> temK ;

    tcf (temC) ;  //尼ん传地ん
    cout << "First  Temperature is " << temC << "F" << " and " ;
    tck (temC) ;  //尼ん传惩ん
    cout << temC << "K" << endl ;

    tfc (temF) ;  //地ん传尼ん
    cout << "Second Temperature is " << temF << "C" << " and " ;
    tfk (temF) ;  //地ん传惩ん
    cout << temF << "K" << endl ;

    tkc (temK) ;  //惩ん传尼ん
    cout << "Third  Temperature is " << temK << "C" << " and " ;
    tkf (temK) ;  //惩ん传地ん
    cout << temK << "F" ;

    return 0 ;
}

void tcf (double &temC)                        //ㄧ计磅︽ず甧
{
    temC = temC*9/5 + 32 ;
}

void tck (double &temC)
{
    temC = (temC - 32)*5/9 + 273.15 ;
}

void tfc (double &temF)
{
    temF = (temF - 32)*5/9 ;
}

void tfk (double &temF)
{
    temF = temF + 273.15 ;
}

void tkc (double &temK)
{
    temK = temK - 273.15 ;
}

void tkf (double &temK)
{
    temK = temK*9/5 + 32 ;
}

