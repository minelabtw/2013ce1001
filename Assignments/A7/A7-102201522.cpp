#include<iostream>
using namespace std;

void C2FK(double &CTemp,double &C2F,double &C2K); //prototype , function call by reference
void F2CK(double &FTemp,double &F2C,double &F2K);
void K2CF(double &KTemp,double &K2C,double &K2F);

int main()
{
    double CTemp,C2F,C2K,FTemp,F2C,F2K,KTemp,K2C,K2F;
    cout << "Please enter temperature C : "; //第一次輸入攝氏溫度
    cin >> CTemp;
    cout << "Please enter temperature F : "; //第二次輸入華氏溫度
    cin >> FTemp;
    cout << "Please enter temperature K : "; //第三次輸入凱式溫度
    cin >> KTemp;
    C2FK(CTemp,C2F,C2K); //呼叫函式
    cout << "First  Temperature is " << C2F << "F and " << C2K << "K"; //輸出答案
    F2CK(FTemp,F2C,F2K);
    cout << "\nSecond Temperature is " << F2C << "C and " << F2K << "K";
    K2CF(KTemp,K2C,K2F);
    cout << "\nThird  Temperature is " << K2C << "C and " << K2F << "F";
    return 0;
}

void C2FK(double &CTemp,double &C2F,double &C2K) //攝氏溫度轉換成華氏溫度(F)和凱式溫度(K)
{
    C2F = CTemp/5*9+32;
    C2K = CTemp+273.15;
}
void F2CK(double &FTemp,double &F2C,double &F2K)
{
    F2C = (FTemp-32)*5/9;
    F2K = (FTemp+459.67)*5/9;
}
void K2CF(double &KTemp,double &K2C,double &K2F)
{
    K2C = KTemp-273.15;
    K2F = KTemp*9/5-459.67;
}
