#include <iostream>
using namespace std;
void CF(double &a);
void CK(double &a);
void FC(double &b);
void FK(double &b);
void KC(double &c);
void KF(double &c); //宣告6個函式

int main()
{
    double a,b,c,x,y,z;
    cout << "Please enter temperature C : ";
    cin >> a;
    cout << "Please enter temperature F : ";
    cin >> b;
    cout << "Please enter temperature K : ";
    cin >> c; //輸入

    x=a;
    y=b;
    z=c;
    CF(x);
    CK(a);
    FC(y);
    FK(b);
    KC(z);
    KF(c); //執行函式
    cout << "First  Temperature is " << x;
    cout << "F and " << a << "K\nSecond Temperature is " << y;
    cout << "C and " << b << "K\nThird  Temperature is " << z;
    cout << "C and " << c << "F"; //輸出
    return 0;
}

void CF(double &a) //攝氏轉華氏
{
    a=a*9/5+32;
}

void CK(double &a) //攝氏轉絕對
{
    a=a+273.15;
}

void FC(double &b) //華氏轉攝氏
{
    b=(b-32)*5/9;
}

void FK(double &b) //華氏轉絕對
{
    b=(b-32)*5/9+273.15;
}

void KC(double &c) //絕對轉攝氏
{
    c=c-273.15;
}

void KF(double &c) //絕對轉華氏
{
    c=(c-273.15)*9/5+32;
}
