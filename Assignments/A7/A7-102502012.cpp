#include<iostream>
using namespace std;
void changeC(double &C,double &F,double &K)
{
    F=C*9/5.0+32;
    K=C+273.15;
}
void changeF(double &F,double &C,double &K)
{
    C=(F-32)/9*5;
    K=C+273.15;
}
void changeK(double &K,double &C,double &F)
{
    C=K-273.15;
    F=C*9/5+32;
}
int main()
{
    double C,F,K;
    double a,b; // temp variable
    cout<<"Please enter temperature C : ";
    cin>>C;
    cout<<"Please enter temperature F : ";
    cin>>F;
    cout<<"Please enter temperature K : ";
    cin>>K;
    changeC(C,a,b); // a stores F,b stores K
    cout<<"First  Temperature is "<<a<<"F and "<<b<<"K"<<endl;
    changeF(F,a,b); // a stores C,b stores K
    cout<<"Second Temperature is "<<a<<"C and "<<b<<"K"<<endl;
    changeK(K,a,b); // a stores C,b stores F
    cout<<"Third  Temperature is "<<a<<"C and "<<b<<"F"<<endl;
    return 0;
}
