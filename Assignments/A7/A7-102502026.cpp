//A7-102502026
//Call by reference
#include <iostream>
using namespace std;

void RTcf(double &);    //function prototype
void RTck(double &);    //function prototype
void RTfc(double &);    //function prototype
void RTfk(double &);    //function prototype
void RTkc(double &);    //function prototype
void RTkf(double &);    //function prototype

int main()  //start the program
{
    double Tc=0;    //define Tc
    double Tc1=0;   //define Tc1
    double Tf=0;    //define Tf
    double Tf1=0;   //define Tf1
    double Tk=0;    //define Tk
    double Tk1=0;   //define Tk1
    cout<<"Please enter temperature C : ";  //ask Tc
    cin>>Tc;
    Tc1=Tc;
    cout<<"Please enter temperature F : ";  //ask Tf
    cin>>Tf;
    Tf1=Tf;
    cout<<"Please enter temperature K : ";  //ask Tk
    cin>>Tk;
    Tk1=Tk;
    RTcf(Tc);   //using by reference
    RTck(Tc1);
    cout<<"First Temperature is "<<Tc<<"F and "<<Tc1<<"K\n";    //print result
    RTfc(Tf);   //using by reference
    RTfk(Tf1);
    cout<<"Second Temperature is "<<Tf<<"C and "<<Tf1<<"K\n";   //print result
    RTkc(Tk);   //using by reference
    RTkf(Tk1);
    cout<<"Third Temperature is "<<Tk<<"C and "<<Tk1<<"F\n";    //print result
    return 0;
}   //end program

void RTcf(double &numberRef)    //formula for C to F
{
    numberRef=(numberRef*1.8)+32;
}
void RTck(double &numberRef)    //formula for C to K
{
    numberRef= numberRef+273.15;
}
void RTfc(double &numberRef)    //formula for F to C
{
    numberRef= (numberRef-32)*5/9;
}
void RTfk(double &numberRef)    //formula for F to K
{
    numberRef=(numberRef+459.67)*5/9;
}
void RTkc(double &numberRef)    //formula for K to C
{
    numberRef= numberRef-273.15;
}
void RTkf(double &numberRef)    //formula for K to F
{
    numberRef= (numberRef*1.8)-459.67;
}

