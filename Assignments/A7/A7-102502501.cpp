#include<iostream>
using namespace std;


void  ck( double &b);                   //函數單位
void  cf( double &a);                   //函數單位
void  fc( double &c);
void  fk( double &d);
void  kc( double &e);
void  kf( double &f);

int main()
{
    double a;                       //宣告一個整數變數
    double b;
    double c;
    double d;
    double e;
    double f;
    cout<<"Please enter temperature C : ";         //螢幕上顯示Please enter temperature C
    cin>>a;                                        //請輸入數字
    cout<<"Please enter temperature F : ";         //螢幕上顯示Please enter temperature F
    cin>>c;                                        //請輸入數字
    cout<<"Please enter temperature K : ";         //螢幕上顯示Please enter temperature K
    cin>>e;                                        //請輸入數字
    b=a;                                           //將宣告的數字b當成C的某個數字
    cf(a);                                         //攝氏溫度轉換成華氏溫度便會顯示修改過的a值
    cout<<"First  Temperature is "<<a<<"F and ";
    ck(b);                                         //攝氏溫度轉換成凱式溫度便會顯示修改過的b值
    cout<<b<<"K"<<endl;
    d=c;                                          //將宣告的數字d當成c的某個數字
    fc(c);                                        //華氏溫度轉換成攝氏溫度便會顯示修改過的c值
    cout<<"Second Temperature is "<<c<<"C and ";
    fk(d);                                        //華氏溫度轉換成凱式溫度便會顯示修改過的d值
    cout<<d<<"K"<<endl;
    f=e;                                          //將宣告的數字f當成e的某個數字
    kc(e);                                        //凱式溫度轉換成攝氏溫度便會顯示修改過的e值
    cout<<"Third  Temperature is "<<e<<"C and ";
    kf(f);                                        //凱式溫度轉換成華氏溫度便會顯示修改過的f值
    cout<<f<<"F"<<endl;
}

void cf(double &a)                              //配合剛開始宣告的數字並指標於a值
{
    a=a*9/5+32;

}
void ck(double &b)                             //配合剛開始宣告的數字並指標於b值
{
    b=b+273.15;

}
void fc(double &c)                             //配合剛開始宣告的數字並指標於c值
{
    c=(c-32)*5/9;
}
void fk(double &d)                             //配合剛開始宣告的數字並指標於d值
{
    d=(d+459.67)*5/9;
}
void kc(double &e)                             //配合剛開始宣告的數字並指標於e值
{
    e=e-273.15;
}
void kf(double &f)                            //配合剛開始宣告的數字並指標於f值
{
    f=f*9/5-459.67;
}




