#include <iostream>
using namespace std;
//各種函式
inline void C_to_F(double &CF)
{
    CF = CF*9/5+32;
}
inline void C_to_K(double &CK)
{
    CK += 273.15;
}
inline void F_to_C(double &FC)
{
    FC = (FC-32)*5/9;
}
inline void F_to_K(double &FK)
{
    FK =( FK-32)*5/9+273.15;
}
inline void K_to_C(double &KC)
{
    KC -= 273.15;
}
inline void K_to_F(double &KF)
{
    KF =( KF-273.15)*9/5+32;
}
int main()
{
    ios::sync_with_stdio(0);
    //各種變數
    double CF;
    double CK;
    double FC;
    double FK;
    double KC;
    double KF;
    cout << "Please enter temperature C : ";
    cin >> CF;
    cout << "Please enter temperature F : ";
    cin >> FC;
    cout << "Please enter temperature K : ";
    cin >> KC;
    CK = CF;
    FK = FC;
    KF = KC;
    C_to_F(CF);
    C_to_K(CK);
    F_to_C(FC);
    F_to_K(FK);
    K_to_C(KC);
    K_to_F(KF);
    //各種輸出
    cout << "First  Temperature is " << CF << "F and " << CK << "K" << endl;
    cout << "Second Temperature is " << FC << "C and " << FK << "K" << endl;
    cout << "Third  Temperature is " << KC << "C and " << KF << "F" << endl;
    return 0;
}
