#include <iostream>
using namespace std;

//function prototype
void celsius(double&, double&, double&);
void fahrenheit(double&, double&, double&);
void kelvin(double&, double&, double&);

int main()
{
    //call variable
    double c;
    double f;
    double k;
    double co; //for output
    double fo; // for output
    double ko;// for output

    //display words
    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    cout<<"Please enter temperature K : ";
    cin>>k;

    //display calculation
    celsius(c,fo,ko); //convert celsius
    cout<<"First  Temperature is "<<fo<<"F and "<<ko<<"K"<<endl;
    fahrenheit(co,f,ko); //convert fahrenheit
    cout<<"Second Temperature is "<<co<<"C and "<<ko<<"K"<<endl;
    kelvin(co,fo,k); //convert kelvin
    cout<<"Third  Temperature is "<<co<<"C and "<<fo<<"F";

    return 0;
}

//function for celsius convertion
void celsius(double& c,double& fo,double& ko)
{
    fo=((9*c)/5)+32;
    ko=c+273.15;
}//end function

//function for fahrenheit convertion
void fahrenheit(double& co,double& f,double& ko)
{
    co=(f-32)/1.8;
    ko=((f-32)/1.8)+273.15;
}//end function

//function for kelvin convertion
void kelvin(double& co,double& fo,double& k)
{
    co=k-273.15;
    fo=((k-273.15)*1.8)+32;
}//end function


