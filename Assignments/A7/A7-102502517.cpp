#include <iostream>

using namespace std;

void input (double &,string); //溫度輸入的prototype
void CtoF (double &); //單位換算的prototype
void FtoK (double &); //單位換算的prototype
void FtoC (double &); //單位換算的prototype
void CtoK (double &); //單位換算的prototype
void KtoC (double &); //單位換算的prototype

int main()
{
    double C = 0; //宣告變數
    double F = 0; //宣告變數
    double K = 0; //宣告變數

    input (C,"C");
    input (F,"F");
    input (K,"K");

    CtoF (C); //單位換算
    cout << "First  Temperature is " << C << "F and ";
    FtoK (C); //單位換算
    cout << C << "K" << endl;

    FtoC (F); //單位換算
    cout << "Second Temperature is " << F << "C and ";
    CtoK (F); //單位換算
    cout << F << "K" << endl;

    KtoC (K); //單位換算
    cout << "Thrid  Temperature is " << K << "C and ";
    CtoF (K); //單位換算
    cout << K << "F";
}

void input (double &X,string enter) //輸入溫度的函式
{
    cout << "Please enter temperature " << enter << " : ";
    cin >> X;
}

void CtoF (double &X) //單位換算
{
    X=X*1.8+32;
}

void FtoK (double &X) //單位換算
{
    X=(X+459.67)*5/9;
}

void FtoC (double &X) //單位換算
{
    X=(X-32)*5/9;
}

void CtoK (double &X) //單位換算
{
    X=X+273.15;
}

void KtoC (double &X) //單位換算
{
    X=X-273.15;
}
