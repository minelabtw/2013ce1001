#include <iostream>
using namespace std;

double C(double &i1,double &i2); //宣告C函數
double F(double &j1,double &j2); //宣告F函數
double K(double &l1,double &l2); //宣告K函數

int main()
{
    double c,i1,i2; //宣告變數
    cout <<"Please enter temperature C : "; //請求輸入攝氏溫度
    cin >>c;
    i1=c;
    i2=c;

    double f,j1,j2; //宣告變數
    cout <<"Please enter temperature F : "; //請求輸入華氏溫度
    cin >>f;
    j1=f;
    j2=f;

    double k,l1,l2; //宣告變數
    cout <<"Please enter temperature K : "; //請求輸入凱式溫度
    cin >>k;
    l1=k;
    l2=k;

    C(i1,i2);
    cout <<"First  Temperature is "<<i1<<"F and "<<i2<<"K"<<endl; //印出結果
    F(j1,j2);
    cout <<"Second Temperature is "<<j1<<"C and "<<j2<<"K"<<endl;
    K(l1,l2);
    cout <<"Third  Temperature is "<<l1<<"F and "<<l2<<"F";

    return 0;
}
double C(double &i1,double &i2) //攝氏函數計算式子
{
    i1=i1*9/5+32;
    i2=i2+273.15;
}

double F(double &j1,double &j2) //華氏函數計算式子
{
    j1=(j1-32)*5/9;
    j2=(j2-32)*5/9+273.15;
}
double K(double &l1,double &l2) //凱氏函數計算式子
{
    l1=l1-273.15;
    l2=(l2-273.15)*9/5+32;
}
