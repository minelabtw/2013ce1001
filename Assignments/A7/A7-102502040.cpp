#include<iostream>
using namespace std;

void fk(double&,double&,double&);//宣告三個能利用程式參數計算的方程式
void ck(double&,double&,double&);
void cf(double&,double&,double&);
double c1,c2,c3,f1,f2,f3,k1,k2,k3;//宣告計算出來結果的溫度

int main()
{
    cout << "Please enter temperature C : " ;//顯示字元
    cin >> c1;
    cout << "Please enter temperature F : " ;
    cin >> f1;
    cout << "Please enter temperature K : " ;
    cin >> k1;
    fk(c1,f2,k2);
    cout << "First  Temperature is "<< f2 << "F and " << k2 << "K" << endl;//計算溫度
    ck(f1,c2,k3);
    cout << "Second  Temperature is "<< c2 << "C and " << k3 << "K" << endl;
    cf(k1,c3,f3);
    cout << "Third  Temperature is "<< c3 << "C and " << f3 << "F" << endl;

    return 0;//結束
}
void fk(double&c1,double&f2,double&k2)//利用前面宣告的參數,作為副參數
    {
        f2=c1*9/5+32;
        k2=c1+273.15;
    }
void ck(double&f1,double&c2,double&k3)
    {
        c2=(f1-32)/9*5;
        k3=c2+273.15;
    }
void  cf(double&k1,double&c3,double&f3)
    {
        c3=k1-273.15;
        f3=c3*9/5+32;
    }
