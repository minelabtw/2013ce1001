/*溫度轉換
請輸入攝氏溫度C、華氏溫度F、凱式溫度K

第一次輸入攝氏溫度轉換成華氏溫度(F)和凱式溫度(K)
第二次輸入華氏溫度轉換成攝氏溫度(C)和凱式溫度(K)
第三次輸入凱式溫度轉換成攝氏溫度(C)和華氏溫度(F)

需要使用函式，且函式不能有return和直接答案輸出(使用Call by reference)，須在main裡面呼叫函式與輸出答案
參考教學網站:http://tw.tonytuan.org/2010/03/call-by-valuecall-by-pointercall-by.html*/
#include <iostream>
using namespace std;

void C_F(double &C ,double &C_F)//C轉F
{
    C_F=C*9/5+32;
}
void C_K(double &C ,double &C_K)//C轉K
{
    C_K=C+273.15;
}
void F_C(double &F ,double &F_C)//F轉C
{
    F_C=(F-32)*5/9;
}
void F_K(double &F ,double &F_K)//F轉K
{
    F_K=(F+459.67)*5/9;
}
void K_C(double &K ,double &K_C)//K轉C
{
    K_C=K-273.15;
}
void K_F(double &K ,double &K_F)//K轉F
{
    K_F=(K-273.15)*9/5+32;
}
int main()
{
    double C=0,F=0,K=0;
    double CtoF,CtoK,FtoC,FtoK,KtoC,KtoF;
    cout << "Please enter temperature C : ";
    cin >> C;
    cout << "Please enter temperature F : ";
    cin >> F;
    cout << "Please enter temperature K : ";
    cin >> K;
    C_F(C,CtoF);//將輸入的數代入函數
    C_K(C,CtoK);
    F_C(F,FtoC);
    F_K(F,FtoK);
    K_C(K,KtoC);
    K_F(K,KtoF);
    cout << "First  Temperature is " << CtoF << "F" << " and " << CtoK << "K" << "\n";//輸出答案
    cout << "Second  Temperature is " << FtoC << "C" << " and " << FtoK << "K" <<"\n";
    cout << "Third  Temperature is " << KtoC << "C" << " and " << KtoF << "F" <<"\n";

    return 0;
}
