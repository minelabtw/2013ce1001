#include <iostream>
using namespace std;
void C2F(float& temperature);//C轉換成F
void F2C(float& temperature);//F轉換成C
void K2C(float& temperature);//K轉換成C
void C2K(float& temperature);//C轉換成K
void F2K(float& temperature);//F轉換成K
void K2F(float& temperature);//K轉換成F

int main(){
    float temperature1 = 0.0;////宣告型態為float的溫度1，並初始化為0.0
    float temperature2 = 0.0;////宣告型態為float的溫度2，並初始化為0.0
    float temperature3 = 0.0;////宣告型態為float的溫度3，並初始化為0.0

    cout << "Please enter temperature C : ";
    cin >> temperature1;
    cout << "Please enter temperature F : ";
    cin >> temperature2;
    cout << "Please enter temperature K : ";
    cin >> temperature3;

    C2F(temperature1);
    cout << "First  Temperature is " << temperature1 << "F and ";
    F2K(temperature1);
    cout << temperature1 << "K" << endl;

    F2C(temperature2);
    cout << "Second Temperature is " << temperature2 << "C and ";
    C2K(temperature2);
    cout << temperature2 << "K" << endl;

    K2C(temperature3);
    cout << "Third  Temperature is " << temperature3 << "C and ";
    C2F(temperature3);
    cout << temperature3 << "F" << endl;

    return 0;
}

void C2F(float& temperature){
    //輸入參數為temperature的float參考，C轉換成F
    temperature = temperature * 1.8 + 32.0;
}

void F2C(float& temperature){
    //輸入參數為temperature的float參考，F轉換成C
    temperature = (temperature - 32.0) * 5.0 / 9.0;
}

void K2C(float& temperature){
    //輸入參數為temperature的float參考，K轉換成C
    temperature = temperature - 273.15;
}

void C2K(float& temperature){
    //輸入參數為temperature的float參考，C轉換成K
    temperature = temperature + 273.15;
}

void F2K(float& temperature){
    //輸入參數為temperature的float參考，F轉換成K
    temperature = (temperature - 32.0) * 5.0 / 9.0 + 273.15;
}

void K2F(float& temperature){
    //輸入參數為temperature的float參考，K轉換成F
    temperature = (temperature - 273.15) * 1.8 +32.0;
}
