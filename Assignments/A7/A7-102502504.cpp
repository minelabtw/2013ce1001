#include <iostream>
using namespace std;

void ans1_1(double &a1); //宣告副函式ans1_1
void ans1_2(double &a2); //宣告副函式ans1_2
void ans2_1(double &a3); //宣告副函式ans2_1
void ans2_2(double &a4); //宣告副函式ans2_2
void ans3_1(double &a5); //宣告副函式ans3_1
void ans3_2(double &a6); //宣告副函式ans3_2

int main()
{
    double a1,a2,a3,a4,a5,a6,C,F,K;//宣告主函式內的變數

    cout << "Please enter temperature C : "; //輸出"Please enter temperature C : "
    cin >> C; //輸入C
    cout << "Please enter temperature F : "; //輸出"Please enter temperature F : "
    cin >> F; //輸入F
    cout << "Please enter temperature K : "; //輸出"Please enter temperature K : "
    cin >> K; //輸入K

    a1=a2=C;//a1和a2是C的值
    a3=a4=F;//a3和a4是F的值
    a5=a6=K;//a5和a6是K的值

    ans1_1(a1);
    ans1_2(a2);
    ans2_1(a3);
    ans2_2(a4);
    ans3_1(a5);
    ans3_2(a6);//將a1~a6的值丟入ansX_X()函式

    cout << "First  Temperature is "<<a1<<"F"<<" and "<<a2<<"K"<<endl;//輸出攝氏轉華氏、克氏的值
    cout << "Second  Temperature is "<<a3<<"C"<<" and "<<a4<<"K"<< endl;//輸出華氏轉攝氏、克氏的值
    cout << "Third  Temperature is "<<a5<<"C"<<" and "<<a6<<"F"<< endl;//輸出克氏轉攝氏、華氏的值

    return 0;
}

void ans1_1(double &a1) //副函式ans1_1，接收主函式中a1在記憶體中的地址，不回傳值
{
    a1=(9/5.)*a1+32; //將攝氏轉為華氏
}

void ans1_2(double &a2) //副函式ans1_2，接收主函式中a2在記憶體中的地址，不回傳值
{
    a2=a2+273.149994; //將攝氏轉為克氏
}

void ans2_1(double &a3) //副函式ans2_1，接收主函式中a3在記憶體中的地址，不回傳值
{
    a3=(a3-32)*(5./9); //將華氏轉為攝氏
}

void ans2_2(double &a4) //副函式ans2_2，接收主函式中a4在記憶體中的地址，不回傳值
{
    a4=(a4-32)*(5./9)+273.149994; //將華氏轉為克氏
}

void ans3_1(double &a5) //副函式ans3_1，接收主函式中a5在記憶體中的地址，不回傳值
{
    a5=a5-273.149994; //將克氏轉為攝氏
}

void ans3_2(double &a6) //副函式ans3_2，接收主函式中a6在記憶體中的地址，不回傳值
{
    a6=(a6-273.149994)*(9./5)+32; //將克氏轉為華氏
}
