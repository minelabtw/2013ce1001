#include<iostream>
using namespace std;
void CtoF (double &);           //宣告副函式
void CtoK (double &);
void FtoC (double &);
void KtoC (double &);
void FtoK (double &);
int main ()
{
    double C=0;
    double F=0;
    double K=0;
    cout << "Please enter temperature C : " ;
    cin >> C ;                                      //輸入浮點數C
    cout << "Please enter temperature F : " ;
    cin >> F ;                                      //輸入浮點數F
    cout << "Please enter temperature K : " ;
    cin >> K ;                                      //輸入浮點數K

    cout << "First  Temperature is " ;              //轉換溫度單位並印出
    CtoF (C);
    cout << C << "F and " ;
    FtoK (C);
    cout << C << "K" << endl ;

    cout << "Second Temperature is " ;              //轉換溫度單位並印出
    FtoC (F) ;
    cout << F << "C and " ;
    CtoK (F) ;
    cout << F << "K" << endl ;

    cout << "Third  Temperature is " ;              //轉換溫度單位並印出
    KtoC (K) ;
    cout << K << "C and " ;
    CtoF (K) ;
    cout << K << "F" << endl ;
    return 0;
}
void CtoF (double &C)
{
    C = C*9/5+32 ;
}
void CtoK (double &C)
{
    C = C+273.15 ;
}
void FtoC (double &F)
{
    F = (F-32)*5/9 ;
}
void KtoC (double &K)
{
    K = K-273.15 ;
}
void FtoK (double &F)
{
    F = (F+459.67)*5/9;
}
