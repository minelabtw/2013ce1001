#include <iostream>
using namespace std ;

void cf(double &);//宣告函式
void fc(double &);//同上
void kc(double &);//同上
void ck(double &);//同上

int main()
{
    double c=0 ;//宣告變數
    double f=0 ;//同上
    double k=0 ;//同上

    cout << "Please enter temperature C : " ;//輸出
    cin >> c ;//輸入
    cout << "Please enter temperature F : " ;//輸出
    cin >> f ;//輸入
    cout << "Please enter temperature K : " ;//輸出
    cin >> k ;//輸入

    cf(c) ;//呼叫函式
    cout << "First  Temperature is " << c << "F" << " and " ; //輸出
    fc(c) ;//呼叫函式
    ck(c) ;//同上
    cout << c << "K" << endl ;//輸出

    fc(f) ;//呼叫函式
    cout << "Second Temperature is " << f << "C" << " and " ;//輸出
    ck(f) ;//呼叫函式
    cout << f << "K" << endl ;//輸出

    kc(k) ;//呼叫函式
    cout << "Third  Temperature is " << k << "C" << " and " ;//輸出
    cf(k) ;//呼叫函式
    cout << k << "F" ;//輸出

    return 0 ;
}
void cf(double&x)//計算攝氏轉華式
{
    x=x*9/5+32 ;
}
void fc(double&x)//計算華式轉攝氏
{
    x=5*(x-32)/9 ;
}
void ck(double&x)//計算攝氏轉凱式
{
    x=x+273.15 ;
}
void kc(double&x)//計算凱式轉攝氏
{
    x=x-273.15 ;
}
