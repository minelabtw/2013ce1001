#include<iostream>
using namespace std;
void K(int i,double &x)
{
    if(i==0)            //C --> K
        x += 273.15;
    else                //F --> C
    {
        x = (x-32)*5/9;
        K(0,x);         //C --> K
    }
}
int main()
{
    double input[3]= {};            //宣告三個input變數，型別為double
    int i=0;
    do
    {
        if(i==0)cout << "Please enter temperature C : ";
        else if(i==1)cout << "Please enter temperature F : ";
        else if(i==2)cout << "Please enter temperature K : ";
        cin >> input[i];
        if(i!=2)K(i,input[i]);     //將輸入都轉成凱式以便判斷是否符合常理
        if(input[i]<0)cout << "Illegal input!\n";
        else i++;
    }
    while(i!=3);
    cout << "First  Temperature is " << (input[0]-273.15)*9/5+32 << "F and " << input[0] << "K\n";
    cout << "Second Temperature is " << input[1]-273.15 << "C and " << input[1] << "K\n";
    cout << "Third  Temperature is " << input[2]-273.15 << "C and " << (input[2]-273.15)*9/5+32 << "F\n";
    return 0;
}
