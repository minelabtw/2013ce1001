#include<iostream>

using namespace std ;

void CtoK(double &a);                           //宣告函式的prototype
void CtoF(double &b);
void FtoC(double &c);
void FtoK(double &d);
void KtoC(double &e);

int main ()
{
    double c=0,f=0,k=0;
    double ft1=0,ft2=0,st1=0,st2=0,tt1=0,tt2=0 ;
    cout<<"Please enter temperature C : ";
    cin>>c;
    ft1=ft2=c ;                                 //將輸入的c值丟入ft1 ,ft2
    cout<<"Please enter temperature F : ";
    cin>>f;
    st1=st2=f;
    cout<<"Please enter temperature K : ";
    cin>>k ;
    tt1=tt2=k;
    CtoF(ft1);                                //將ft1放入CtoF() 執行
    CtoK(ft2);
    cout<<"First  Temperature is "<<ft1<<"F and "<<ft2<<"K"<<endl;
    FtoC(st1);
    FtoK(st2);
    cout<<"Second Temperature is "<<st1<<"C and "<<st2<<"K"<<endl ;
    KtoC(tt1);
    KtoC(tt2);CtoF(tt2);
    cout<<"Third  Temperature is "<<tt1<<"C and "<<tt2<<"F";
    return 0 ;
}

void CtoK(double &a)                //下列皆為溫標換算函式 不會回傳值
{
    a=a+273.15;
}
void CtoF(double &b)
{
    b=b*1.8+32 ;
}
void FtoC(double &c)
{
    c=(c-32)*5/9;
}
void FtoK(double &d)
{
     d=(d+459.67)*5/9 ;
}
void KtoC(double &e)
{
    e=e-273.15 ;
}
