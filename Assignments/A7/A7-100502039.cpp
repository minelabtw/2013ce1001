#include <iostream>
using namespace std;

void Transfer_C(double C, &x, &y);
void Transfer_F(double F, &x, &y);
void Transfer_K(double K, &x, &y);

int main(){

    double C, F, K = 0;

    cout << "Please enter the temperature C : ";
    cin >> C;

    while(C < -273.15){
        cout << "The temperature C is out of range." << endl;
        cout << "Please enter the temperature C : ";
        cin >> C;
    }

    cout << "Please enter the temperature F : ";
    cin >> F;

    while(F < -459.67){
        cout << "The temperature F is out of range." << endl;
        cout << "Please enter the temperature F : ";
        cin >> F;
    }

    cout << "Please enter the temperature K: ";
    cin >> K;

    while(K < 0){
        cout << "The temperature K is out of range." << endl;
        cout << "Please enter the temperature K : ";
        cin >> K;
    }

    double x, y = 0;

    Transfer_C(C, x, y)

    cout << "First  Temperature is " << x << "F and " << y << "K" << endl;

    Transfer_F(F, x, y)

    cout << "First  Temperature is " << x << "C and " << y << "K" << endl;

    Transfer_K(K, x, y)

    cout << "First  Temperature is " << x << "C and " << y << "F";

    return 0;
}

void Transfer_C(double c, &x, &y){    //transfer C to F, K
    x = c * 1.8 + 32;
    y = c + 273.15;
}
void Transfer_F(double f, &x, &y){    //transfer F to C, K
    x = (f - 32)/1.8;
    y = x + 273.15;
}
void Transfer_K(double k, &x, &y){    //transfer K to C, F
    x = k - 273.15;
    y = x * 1.8 + 32;
}
