#include <iostream>

using namespace std;

double Celsius(double &c,double &Ac);    //各種換算公式的prototype
double Fahrenheit(double &f,double &Af);
double Kelvin(double &k,double &Ak);

int main()
{
    double c;    //設定變數
    double Ac;
    double f;
    double Af;
    double k;
    double Ak;

    cout<<"Please enter temperature C : ";
    cin>>c;

    Ac=c;    //複製c的數值

    Celsius(c,Ac);

    cout<<"Please enter temperature F : ";
    cin>>f;

    Af=f;    //複製f的數值

    Fahrenheit(f,Af);

    cout<<"Please enter temperature K : ";
    cin>>k;

    Ak=k;    //複製k的數值

    Kelvin(k,Ak);

    cout<<"First  Temperature is "<<c<<"F and "<<Ac<<"K"<<endl;
    cout<<"Second Temperature is "<<f<<"C and "<<Af<<"K"<<endl;
    cout<<"Third  Temperature is "<<k<<"C and "<<Ak<<"F"<<endl;

    return 0;
}

double Celsius(double &c,double &Ac)    //各個公式運算
{
    c=c*9/5+32;
    Ac=Ac+273.15;
}

double Fahrenheit(double &f,double &Af)
{
    f=(f-32)*5/9;
    Af=(Af-32)*5/9+273.15;
}

double Kelvin(double &k,double &Ak)
{
    k=k-273.15;
    Ak=(Ak-273.15)*9/5+32;
}
