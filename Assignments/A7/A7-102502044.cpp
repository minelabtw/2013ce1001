/*************************************************************************
    > File Name: A7-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年11月07日 (週四) 13時12分08秒
 ************************************************************************/

#include <stdio.h>

/* function to change c f k temperature */
inline void c2f(double &x);
inline void c2k(double &x);
inline void f2c(double &x);
inline void f2k(double &x);
inline void k2c(double &x);
inline void k2f(double &x);

/* function to input number */
inline void input(const char *str, double *tmp);

/* function to input number */
inline void output(const char *str, double x, void (*fun1)(double &), void (*fun2)(double &));

int main()
{
	/* declare all variables */
	double c, f, k;	

	/* input all variable */
	input("Please enter temperature C : ", &c);
	input("Please enter temperature F : ", &f);
	input("Please enter temperature K : ", &k);

	/* output */
	output("First Temperature is %lfF and %lfK\n" , c, c2f, c2k);
	output("Second Temperature is %lfC and %lfK\n", f, f2c, f2k);
	output("Third Temperature is %lfC and %lfF\n" , k, k2c, k2f);

	return 0;
}

/**********************************/
/*   str means the ui message	  */
/*  *tmp means the point of input */
/**********************************/
inline void input(const char *str, double *tmp)
{
		printf("%s", str);	// print ui

		scanf("%lf", tmp);	// input number
}

/*************************************************/
/*  str means the output message	             */
/*    x means the inpit number will be calced    */
/* fun1 means the  first function to calc output */
/* fun2 means the second function to calc output */
/*************************************************/
inline void output(const char *str, double x, void (*fun1)(double &), void (*fun2)(double &))
{
	/* tmp1 means the output of fun1 */
	/* tmp2 means the output of fun2 */
	static double tmp1, tmp2;

	/* init vartiable */
	tmp1 = tmp2 = x;

	/* calculate output */
	fun1(tmp1);
	fun2(tmp2);

	/* output answer */
	printf(str, tmp1, tmp2);
}

/* x means the input number */
inline void c2f(double &x)
{
	/* change c to f temperature */
	x = x*1.8 + 32.0;
}

/* x means the input number */
inline void c2k(double &x)
{
	/* change c to k temperature */
	x += 273.15;
}

/* x means the input number */
inline void f2c(double &x)
{
	/* change f to c temperature */
	x = (x - 32.0)/1.8;
}

/* x means the input number */
inline void f2k(double &x)
{
	/* change f to k temperature */
	f2c(x);
	c2k(x);
}

/* x means the input number */
inline void k2c(double &x)
{
	/* change k to c temperature */
	x -= 273.15;
}

/* x means the input number */
inline void k2f(double &x)
{
	/* change k to f temperature */
	k2c(x);
	c2f(x);
}
