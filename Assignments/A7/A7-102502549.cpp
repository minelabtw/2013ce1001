#include <iostream>
using namespace std;

void CtoFK(double,double&,double&);//CtoFK原型
void FtoCK(double,double&,double&);//FtoCK原型
void KtoCF(double,double&,double&);//KtoCF原型


int main()
{
    double C;//攝氏
    double F;//華氏
    double K;//凱氏
    double Ctemp;//C會變，用這存原值
    double Ftemp;//F會變，用這存原值
    double Ktemp;//K會變，用這存原值

//以下眼看即懂
    cout<<"Please enter temperature C : ";
    cin>>C;
    Ctemp=C;

    cout<<"Please enter temperature F : ";
    cin>>F;
    Ftemp=F;

    cout<<"Please enter temperature K : ";
    cin>>K;
    Ktemp=K;

    CtoFK(C,F,K);
    cout<<"First  Temperature is "<<F<<"F"<<" and "<<K<<"K"<<endl;
    F=Ftemp;
    K=Ktemp;

    FtoCK(F,C,K);
    cout<<"Second Temperature is "<<C<<"C"<<" and "<<K<<"K"<<endl;
    C=Ctemp;
    K=Ktemp;

    KtoCF(K,C,F);
    cout<<"Third  Temperature is "<<C<<"C"<<" and "<<F<<"F";

    return 0;
}

//實作CtoFK
void CtoFK(double c,double &f,double &k)
{
    f=c*1.8+32;
    k=c+273.15;
}

//實作FtoCK
void FtoCK(double f,double &c,double &k)
{
    c=(f-32)/1.8;
    k=c+273.15;
}

//實作KtoCF
void KtoCF(double k,double &c,double &f)
{
    c=k-273.15;
    f=c*1.8+32;
}
