#include<iostream>
using namespace std;
void foo(double &c1)//將C轉換成F的fuction
{
    c1=(5*c1)/9+32;
}
void voo(double &c1)//將C轉換成K的fuction
{
    c1=((c1-32)*9)/5+273.15;
}
void aoo (double &f2)//將F轉換成C的fuction
{
    f2=((f2-32)*9)/5;
}
void boo (double &f2)//將F轉換成K的fuction
{
    f2=(5*f2)/9+32+273.15;
}
void coo (double &k3)//將K轉換成C的fuction
{
    k3=(k3-273.15);
}
void doo (double &k3)//將K轉換成F的fuction
{
    k3=(5*k3)/9+32;
}
int main()
{
    double c1,c2,c3,f1,f2,f3,k1,k2,k3;
    cout <<"Please enter temperature C : ";
    cin >>c1;
    cout <<"Please enter temperature F : ";
    cin >>f2;
    cout <<"Please enter temperature K : ";
    cin >>k3;
    foo(c1);
    f1=c1;
    voo(c1);
    k1=c1;
    cout <<"First  Temperature is "<<f1<<"F and "<<k1<<"K"<<endl;//顯示第一個溫度
    aoo(f2);
    c2=f2;
    boo(f2);
    k2=f2;
    cout <<"Second Temperature is "<<c2<<"C and "<<k2<<"K"<<endl;//顯示第二個溫度
    coo(k3);
    c3=k3;
    doo(k3);
    f3=k3;
    cout <<"Third  Temperature is "<<c3<<"C and "<<f3<<"F"<<endl;//顯示第三個溫度
    return 0;
}
