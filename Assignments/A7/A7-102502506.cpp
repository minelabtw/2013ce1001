#include<iostream>

using namespace std;

void CtoF( double & );//宣告函式
void CtoK( double & );
void FtoC( double & );
void KtoC( double & );

int main ()
{
    double c;  //宣告變數
    double f;
    double k;
    double C;
    double F;
    double K;

    cout << "Please enter temperature C : ";  //輸出and輸入題目要求
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;

    C = c;  //讓C=c
    CtoF( C ) ;  //將C代入CtoF函式
    cout << "First  Temperature is " << C  ;  //輸出華氏溫度

    CtoK( c );
    cout << "F and "  << c << "K" << endl;  //輸出凱氏溫度

    F = f;
    FtoC( F );
    cout << "Second Temperature is " << F  ;

    FtoC( f );
    CtoK( f );
    cout << "C and "  << f << "K" << endl;

    K = k;
    KtoC ( K );
    cout << "Third  Temperature is " << K  ;

    KtoC( k );
    CtoF( k );
    cout << "C and "  << k << "F" << endl;

    return 0;
}

void CtoF( double &x )  //將攝氏轉為華氏
{
    x = x * 9 / 5 + 32;  //公式內容
}
void CtoK( double &x )
{
    x = ( x + 273 );
}
void FtoC( double &x )
{
    x = ( x - 32 ) * 5 / 9;
}
void KtoC( double &x )
{
    x = x - 273;
}

