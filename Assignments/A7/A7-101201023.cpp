#include <iostream>

using namespace std;

void shift1(double *);
void shift2(double *);
void shift3(double *);
void shift4(double *);
void shift5(double *);
void shift6(double *);

void shift1(double *cPtr)                      //計算*cPtr轉為華氏溫度，修改在main裡的c值
{
    *cPtr=*cPtr*1.8+32;
}

void shift2(double *fPtr)                      //計算*fPtr轉為攝氏溫度，修改在main裡的f值
{
    *fPtr=(*fPtr-32)*5/9.0;
}

void shift3(double *cPtr)                      //計算*cPtr轉為凱式溫度，修改在main裡的c值
{
    *cPtr=*cPtr+273.15;
}

void shift4(double *kPtr)                      //計算*kPtr轉為攝氏溫度，修改在main裡的k值
{
    *kPtr=*kPtr-273.15;
}

void shift5(double *fPtr)                      //計算*fPtr轉為凱式溫度，修改在main裡的f值
{
    *fPtr=(*fPtr-32)*5/9.0+273.15;
}

void shift6(double *kPtr)                      //計算*kPtr轉為華氏溫度，修改在main裡的k值
{
    *kPtr=(*kPtr-273.15)*1.8+32;
}

int main()
{
    double c=0,C=0;
    double f=0,F=0;
    double k=0,K=0;

    cout << "Please enter temperature C : ";
    cin >> c;
    C=c;                                                       //設大寫c為原輸入c值
    cout << "Please enter temperature F : ";
    cin >> f;
    F=f;
    cout << "Please enter temperature K : ";
    cin >> k;
    K=k;

    shift1(&c);                                                //將c帶入shift1修改
    cout << "First  Temperature is " << c << "F";
    c=C;                                                       //回歸原c值
    shift3(&c);
    cout << " and " << c << "K" << endl;

    shift2(&f);
    cout << "Second Temperature is " << f << "C";
    f=F;
    shift5(&f);
    cout << " and " << f << "K" << endl;


    shift4(&k);
    cout << "Third  Temperature is " << k << "C";
    k=K;
    shift6(&k);
    cout << " and " << k << "F";

    return 0;
}

