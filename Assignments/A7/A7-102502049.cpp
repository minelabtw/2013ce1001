#include<iostream>
using namespace std;

void ctof (double &);
void ctok (double &);
void ftoc (double &);
void ftok (double &);
void ktoc (double &);
void ktof (double &); //宣告六個函數prototype

int main()
{
    double num1;
    double num2;
    double num3;
    double num4;
    double num5;
    double num6; //宣告六個值儲存轉換

    cout << "Please enter temperature C : ";
    cin >> num1;
    cout << "Please enter temperature F : ";
    cin >> num3;
    cout << "Please enter temperature K : ";
    cin >> num5; //顯示題目需求

    num2 = num1;
    num4 = num3;
    num6 = num5; //複製輸入值

    ctof(num1);
    ctok (num2);
    ftoc(num3);
    ftok (num4);
    ktoc(num5);
    ktof (num6); //運算答案

    cout << "First  Temperature is " << num1 << "F and " << num2 << "K" << endl;
    cout << "Second  Temperature is " << num3 << "C and " << num4 << "K" << endl;
    cout << "Third  Temperature is " << num5 << "C and " << num6 << "F" << endl; //顯示答案

    return 0;
}

void ctof ( double &x )
{
    x = x*9/5+32;
}
void ctok ( double &x )
{
    x = x+273.15;
}
void ftoc ( double &x )
{
    x = (x-32)*5/9;
}
void ftok ( double &x )
{
    x = (x-32)*5/9+273.15;
}
void ktoc ( double &x )
{
    x = x-273.15;
}
void ktof ( double &x ) //函式運算
{
    x = (x-273.15)*9/5+32;
}

