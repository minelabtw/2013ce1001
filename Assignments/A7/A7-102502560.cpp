#include <iostream>
using namespace std;

//prototype
void ConvertTempture(double &, char, char);

int main()
{
	double n1, n2, n3;
	cout << "Please enter temperature C : "; cin >> n1;
	cout << "Please enter temperature F : "; cin >> n2;
	cout << "Please enter temperature K : "; cin >> n3;

	ConvertTempture(n1,'C','F');						//Convert first input from C to F
	cout << "First  Temperature is " << n1 << "F ";
	ConvertTempture(n1,'F','K');						//Convert first input from F to K
	cout << "and " << n1 << "K" << "\n";

	ConvertTempture(n2,'F','C');
	cout << "Second Temperature is " << n2 << "C ";
	ConvertTempture(n2,'C','K');
	cout << "and " << n2 << "K" << "\n";

	ConvertTempture(n3,'K','C');
	cout << "Third  Temperature is " << n3 << "C ";
	ConvertTempture(n3,'C','F');
	cout << "and " << n3 << "F" << "\n";

	return 0;
}

void ConvertTempture(double &tempture, char from ,char to)	//a funtion convert tempture from one unit to another
{
	switch(from)	//convert input to degC
	{
		case 'F':
		((tempture-=32)*=5)/=9;
		break;

		case 'K':
		tempture-=273.15;
		break;
	}

	switch(to)		//then convert from degC to required unit
	{
		case 'F':
		((tempture*=9)/=5)+=32;
		break;

		case 'K':
		tempture+=273.15;
		break;
	}
}
