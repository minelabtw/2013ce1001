#include <iostream>
using namespace std;

int main()
{   void toc(double &);//呼叫換算成攝氏的函式
    void tok(double &);//呼叫換算成開氏的函式
    void tof(double &);//呼叫換算成華氏的函式

    double c,f,k =0;//宣告各溫度的變數

    cout << "Please enter temperature C :";
    cin >> c;
    cout << "Please enter temperature F :";
    cin >> f;
    cout << "Please enter temperature K :";
    cin >> k;//提示輸入各式樣溫度

    cout << "First  Temperature is ";//提示第一個換算
    tof(c);//攝氏換算到華氏
    cout << c << "F and ";//印出華氏
    tok(c);//已被改變成華氏後換算成開式
    cout << c << "K" << endl << "Second Temperature is ";//印出開式，提示第二個換算
    tok(f);//華氏換算到開式
    toc(f);//已被改變成開式後換算成攝氏
    cout << f << "C and ";//印出攝氏
    tof(f);//已被改變成攝氏後換算成華氏
    tok(f);//已被改變成華氏後換算成開式
    cout << f << "K" << endl << "Third Temperature is ";//印出開式，提示第三個換算
    toc(k);//開式換算到攝氏
    cout << k << "C and ";//印出攝氏
    tof(k);//已被改變成攝氏換算到華氏
    cout << k << "F";//印出華氏

    return 0;//回傳
}
void toc(double &c)
{
    c = c - 273.15;
}//定義換算成攝氏的函式（由開式算來）
void tof(double &f)
{
   f = f * 9 / 5 + 32;
}//定義換算華氏的函式（由攝氏算來）
void tok(double &k)
{
    k = (k - 32) * 5 / 9 + 273.15;
}//定義換算成開氏的函式（由華氏算來）
