#include <iostream>

using namespace std;
void tempc(double&,double&,double&);
void tempcb(double&,double&,double&);
void tempf(double&,double&,double&);
void tempfb(double&,double&,double&);
void tempk(double&,double&,double&);
int main()
{
    double x;                                                      //宣告變數
    double y;
    double z;
    cout<<"Please enter temperature C : ";                         //顯示文字
    cin>>x;                                                        //輸入文字
    cout<<"Please enter temperature F : ";
    cin>>y;
	cout<<"Please enter temperature K : ";
	cin>>z;
	tempc(x,y,z);                                                  //轉換輸入數字的值
    cout<<"First temperature is "<<y<<"F and "<<z<<"K"<<endl;
    tempcb(x,y,z);                                                 //傳送回本輸入的值
    tempf(x,y,z);
    cout<<"Seccond temperature is "<<x<<"C and "<<z<<"K"<<endl;
    tempfb(x,y,z);
    tempk(x,y,z);
    cout<<"Third temperature is "<<x<<"C and "<<y<<"F"<<endl;

    return 0;
}

void tempc(double&x,double&y,double&z)                              //回傳計算值
{
	y=x*9/5+32;
	x=x;
	z=x+273.15;
}
void tempcb(double&x,double&y,double&z)
{
	y=(y-32)*5/9;
	z=z-273.15;
}
void tempf(double&x,double&y,double&z)
{
	x=(y-32)*5/9;
	z=x+273.15;
}
void tempfb(double&x,double&y,double&z)
{
	x=x*9/5+32;
	z=(z-273.15)*9/5+32;
}
void tempk(double&x,double&y,double&z)
{
	x=z-273.15;
	y=x*9/5+32;
}
