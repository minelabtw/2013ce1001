#include<iostream>
using namespace std;
void F2C(double&);//宣告函式
void C2F(double&);
void C2K(double&);
void K2C(double&);

int main ()
{
    double c , f , k ,cc ,ff ,kk ;//宣告變數
    cout << "Please enter temperature C : " ;//輸入輸出題目所需
    cin >> c;
    cout <<"Please enter temperature F : " ;
    cin >> f;
    cout <<"Please enter temperature K : " ;
    cin >> k;
    cc=c;//令cc=c
    C2F(cc) ;//將cc代入函式
    cout << "First  Temperature is " << cc  ;//輸出華氏溫度
    C2K(c);//將c代入函數
    cout << "F and "  << c <<"K" <<endl;//輸出凱氏溫度
    ff=f;//令ff=f
    F2C(ff);//將ff代入函式
    cout << "Second Temperature is " << ff  ;//輸出攝氏溫度
    F2C(f);//將f代入函式
    C2K(f);//再將代入函式
    cout << "C and "  << f <<"K" <<endl;//輸出凱氏溫度
    kk=k;//令kk=k
    K2C(kk);//將kk代入函式
    cout << "Third  Temperature is " << kk  ;//輸出攝氏溫度
    K2C(k);//將k代入函式
    C2F(k);//將k代入函式
    cout << "C and "  << k <<"F" <<endl;//輸出凱氏溫度

    return 0;
}

void C2F(double& c)//將攝氏轉換為華氏
{
    c=c*9/5 +32 ;//轉換公式
}
void C2K(double& c)//將攝氏轉換為凱氏
{
    c=(c+273.15) ;//轉換公式
}
void F2C(double& c)//將華氏轉換為攝氏
{
    c=(c-32)*5/9;//轉換公式
}
void K2C(double& c)//將凱氏轉換為攝氏
{
    c=c-273.15 ;//轉換公式
}



