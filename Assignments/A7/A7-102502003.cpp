#include<iostream>
using namespace std;

void change1(double,double,double,double&,double&);
void change2(double,double,double,double&,double&);
void change3(double,double,double,double&,double&);

int main()
{
    double c=0;  //攝氏溫度
    double f=0;  //華氏溫度
    double k=0;  //凱氏溫度
    double v1=0;  //第一個轉換後的溫標
    double v2=0;  //第二個轉換後的溫標


    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    cout<<"Please enter temperature K : ";
    cin>>k;

    change1(c,f,k,v1,v2);  //轉換攝氏溫標
    cout<<"First  Temperature is "<<v1<<"F"<<" and "<<v2<<"K"<<endl;
    change2(c,f,k,v1,v2);  //轉換華氏溫標
    cout<<"Second Temperature is "<<v1<<"C"<<" and "<<v2<<"K"<<endl;
    change3(c,f,k,v1,v2);  //轉換凱氏溫標
    cout<<"Third  Temperature is "<<v1<<"C"<<" and "<<v2<<"F"<<endl;


    return 0;


}

void change1(double a,double b,double d,double& v1,double& v2)  //轉換攝氏
{
    v1=(a*9/5)+32;  //攝氏轉華氏
    v2=a+273.15;  //攝氏轉凱氏
}

void change2(double a,double b,double d,double& v1,double& v2)  //轉換華氏
{
    v1=(b-32)*5/9;  //華氏轉攝氏
    v2=((b-32)*5/9)+273.15;  //華氏轉凱氏
}

void change3(double a,double b, double d,double& v1,double& v2)  //轉換凱氏
{
    v1=d-273.15;  //凱氏轉攝氏
    v2=(d-273.5)*9/5+32;  //凱氏轉華氏
}

