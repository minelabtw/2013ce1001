#include<iostream>
using namespace std;

void C(double &,double &,double &);
void F(double &,double &,double &);
void K(double &,double &,double &);

int main()
{
    double num1=0,num2=0,num3=0,num4=0,num5=0,num6=0,num7=0,num8=0,num9=0;

    cout << "Please enter temperature C : ";
    cin >> num1;
    cout << "Please enter temperature F : ";
    cin >> num4;
    cout << "Please enter temperature K : ";
    cin >> num7;

    C(num1,num2,num3);    //呼叫函數C
    F(num4,num5,num6);    //呼叫函數F
    K(num7,num8,num9);    //呼叫函數K

    cout << "First  Temperature is " << num2 << "F and " << num3 << "K" << endl;
    cout << "Second  Temperature is " << num5 << "C and " << num6 << "K" << endl;
    cout << "Third  Temperature is " << num8 << "C and " << num9 << "F";

    return 0;
}

void C(double &num1,double &num2,double &num3)    //攝氏換算
{
    num2 = num1 * 9/5 + 32;
    num3 = num1 + 273.15;
}
void F(double &num4,double &num5,double &num6)    //華氏換算
{
    num5 = (num4 - 32) * 5/9 ;
    num6 = num5 + 273.15;
}
void K(double &num7,double &num8,double &num9)    //凱氏換算
{
    num8 = num7 - 273.15;
    num9 = num8 * 9/5 + 32;
}
