#include <iostream>
#include <math.h>
using namespace std;

double C1,C2,C3,F1,F2,F3,K1,K2,K3;
void foo1(double &C);
void foo2(double &F);
void foo3(double &K);
int main()
{

    cout<<"Please enter temperature C : ";
    cin>>C1;
    foo1(C1);


    cout<<"Please enter temperature F : ";
    cin>>F1;
    foo2(F1);

    cout<<"Please enter temperature K : ";
    cin>>K1;
    foo3(K1);

    cout<<"First  Temperature is "<<F2<<"F"<<" and "<<K2<<"K"<<endl;
    cout<<"Second Temperature is "<<C2<<"C"<<" and "<<K3<<"K"<<endl;
    cout<<"Third  Temperature is "<<C3<<"C"<<" and "<<F3<<"F"<<endl;
}

void foo1(double &C1)
{
    F2 = C1 * 1.8 + 32;
    K2 = C1 + 273.15;
}

void foo2(double &F1)
{
    C2 = (F1 - 32) * 5/9;
    K3 = (F1 + 459.67) * 5/9;
}

void foo3(double &K1)
{
   C3 = K1 - 273.15;
   F3 = K1 * 1.8 - 459.67;
}
