#include<iostream>
using namespace std;

int main()
{
    void CtF(double&);  //無回傳值
    void FtK(double&);  //無回傳值
    void KtC(double&);  //無回傳值
    void CtK(double&);  //無回傳值
    void FtC(double&);  //無回傳值
    void KtF(double&);  //無回傳值

    double Ctemp1;  //宣告變數  從C-->F
    double Ctemp2;  //宣告變數  從C-->K
    double Ftemp1;  //宣告變數  從F-->C
    double Ftemp2;  //宣告變數  從F-->K
    double Ktemp1;  //宣告變數  從K-->C
    double Ktemp2;  //宣告變數  從K-->F

    cout << "Please enter temperature C : ";
    cin >> Ctemp1;
    Ctemp2 = Ctemp1;
    cout << "Please enter temperature F : ";
    cin >> Ftemp1;
    Ftemp2 = Ftemp1;
    cout << "Please enter temperature K : ";
    cin >> Ktemp1;
    Ktemp2 = Ktemp1;
    CtF(Ctemp1);   //將Ctemp1代入CtF進行計算
    FtK(Ftemp2);   //將Ftemp1代入FtK進行計算
    KtC(Ktemp1);   //將Ktemp1代入KtC進行計算
    CtK(Ctemp2);   //將Ctemp1代入CtK進行計算
    FtC(Ftemp1);   //將Ftemp1代入FtC進行計算
    KtF(Ktemp2);   //將Ktemp1代入KtF進行計算

    cout << "First  Temperature is " << Ctemp1 << "F and " << Ctemp2 << "K\n";  //輸出結果
    cout << "Second Temperature is " << Ftemp1 << "C and " << Ftemp2 << "K\n";  //輸出結果
    cout << "Third  Temperature is " << Ktemp1 << "C and " << Ktemp2 << "F\n";  //輸出結果

    return 0;
}

void CtF(double &Ctemp1)    //&為存取地址
{
    Ctemp1 = Ctemp1*9/5+32;
}
void FtK(double &Ftemp2)
{
    Ftemp2 = (Ftemp2+459.67)*5/9;
}
void KtC(double &Ktemp1)
{
    Ktemp1 = Ktemp1-273.15;
}
void CtK(double &Ctemp2)
{
    Ctemp2 = Ctemp2+273.15;
}
void FtC(double &Ftemp1)
{
    Ftemp1 = (Ftemp1-32)*5/9;
}
void KtF(double &Ktemp2)
{
    Ktemp2 = Ktemp2*9/5-459.67;
}
