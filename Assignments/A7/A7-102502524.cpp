#include <iostream>
using namespace std;

double C(double &,double &);                                                    //宣告函式
double F(double &,double &);
double K(double &,double &);

int main()
{
    double c1,c2;                                                               //設定變數
    double f1,f2;
    double k1,k2;

    cout << "Please enter temperature C : ";                                    //輸入要轉換的溫度
    cin >> c1;
    c2=c1;
    cout << "Please enter temperature F : ";
    cin >> f1;
    f2=f1;
    cout << "Please enter temperature K : ";
    cin >> k1;
    k2=k1;

    C(c1,c2);
    F(f1,f2);
    K(k1,k2);

    cout << "First  Temperature is " << c1 << "F and " << c2 << "K" << endl;    //輸出結果
    cout << "Second Temperature is " << f1 << "C and " << f2 << "K" << endl;
    cout << "Third  Temperature is " << k1 << "C and " << k2 << "F" << endl;

    return 0;
}

double C(double &c1,double &c2)                                                 //計算所輸入的攝氏溫度
{
    c1=c1*9/5+32;
    c2=c2+273.15;
}

double F(double &f1,double &f2)                                                 //計算所輸入的華氏溫度
{
    f1=(f1-32)*5/9;
    f2=(f2+459.67)*5/9;
}

double K(double &k1,double &k2)                                                 //計算所輸入的凱氏溫度
{
    k1=k1-273.15;
    k2=k2*9/5-459.67;
}
