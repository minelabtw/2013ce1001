#include <iostream>

using namespace std ;

int main ()
{
    void funk1 ( float & ) ;
    void funf1 ( float & ) ;
    void func1 ( float & ) ;
    void funk2 ( float & ) ;
    void funf2 ( float & ) ;
    void func2 ( float & ) ;  //宣告6個函式，分別處理六個溫度之轉換。

    float c1 , c2 ;
    float f1 , f2 ;
    float k1 , k2 ;  //設6個變數。

    cout << "Please enter temperature C : " ;
    cin >> c1 ;
    cout << "Please enter temperature F : " ;
    cin >> f1 ;
    cout << "Please enter temperature K : " ;
    cin >> k1 ;  //使操作者輸入溫度。

    c2 = c1 ;
    f2 = f1 ;
    k2 = k1 ;  //

    funf1(c1) ;  //使輸入值帶入函式做運算。
    funk1(c2) ;  //使輸入值帶入函式做運算。
    cout << "First  Temperature is " << c1 << "F and " << c2 << "K" << endl ;  //輸出字串以及運算之結果。以下亦同。

    func1(f1) ;
    funk2(f2) ;
    cout << "Second Temperature is " << f1 << "C and " << f2 << "K" << endl ;

    func2(k1) ;
    funf2(k2) ;
    cout << "Third  Temperature is " << k1 << "C and " << k2 << "F" << endl ;

    return 0 ;
}

void funf1 ( float &c1 )
{
    c1 = c1 * 9 / 5 + 32 ;
}  //定義函式：轉換攝氏成華氏。

void funk1 ( float &c2 )
{
    c2 = c2 + 273.15 ;
}  //定義函式：轉換攝氏成凱氏。

void func1 ( float &f1 )
{
    f1 = ( f1 - 32 ) * 5 / 9 ;
}  //定義函式：轉換華氏成攝氏。

void funk2 ( float &f2 )
{
    f2 = ( f2 + 459.67 ) * 5 / 9 ;
}  //定義函式：轉換華氏成凱氏。

void func2 ( float &k1 )
{
    k1 = k1 - 273.15 ;
}  //定義函式：轉換凱氏成攝氏。

void funf2 ( float &k2 )
{
    k2 = k2 * 9 / 5 - 459.67 ;
}  //定義函式：轉換凱氏成華氏。
