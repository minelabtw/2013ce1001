#include <iostream>

using namespace std;

void ftoc(double& in , double& out); // define function change from f to c
void ctof(double& in , double& out); // define function change from c to f
void ctok(double& in , double& out); // define function change from c to k
void ktoc(double& in , double& out); // define function change from k to c

int main()
{
	double c , f , k;
	double ans1 , ans2;

	cout << "Please enter temperature C : "; // input c
	cin >> c;
	cout << "Please enter temperature F : "; // intput f
	cin >> f;
	cout << "Please enter temperature K : "; // intput k
	cin >> k;

	ctof(c , ans1);
	ctok(c , ans2);
	cout << "First  Temperature is " << ans1 << "F and " << ans2 << "K" << endl; //output first temperature
	
	ftoc(f , ans1);
	ctok(ans1 , ans2);
	cout << "Second Temperature is " << ans1 << "C and " << ans2 << "K" << endl; // output second temperature

	ktoc(k , ans1);
	ctof(ans1 , ans2);
	cout << "Third  Temperature is " << ans1 << "C and " << ans2 << "F" << endl; // output third tempperature

	return 0;

}

void ftoc(double& in , double& out) // implement function change from f to c
{
	out = (in - 32) * 5 / 9;
}

void ctof(double& in , double& out) // implement function change from c to f
{
	out = in * 9 / 5 + 32;
}

void ctok(double& in , double& out) // implement function change from c to k
{
	out = in + 273.15;
}

void ktoc(double& in , double& out) // implement function change from k to c
{
	out = in - 273.15;
}
