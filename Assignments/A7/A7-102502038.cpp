#include <iostream>
using namespace std;

/*
class temperature
  temperature(long double *initNum,int unsigned t)
  void toKel(void)  trans value into Kel scale
  void toCel(void)  trans value into Cel scale
  void toFah(void)  trans value into Fah scale
  
  long double value  save temp value
  unsigned int type  save temp type
*/
class temperature{
public:
  long double value;
  temperature(long double *initNum,int unsigned t){
    type = t;
    value = *initNum;  //Reference using here.
    //Really,I don't know why using reference in this assignment.
  }
  void toKel(void){
    switch(type){
    case 0:
      break;
    case 1:
      value += 273.15;
      break;
    case 2:
      value = (value + 459.67) * 5/9;
      break;
    default:
      return;
    }
    type = 0;
  }
  void toCel(void){
    switch(type){
    case 1:
      break;
    case 0:
      value -= 273.15;
      break;
    case 2:
      value = (value - 32) * 5/9;
      break;
    default:
      return;
    }
    type = 1;
  }
  void toFah(void){
    switch(type){
    case 2:
      break;
    case 0:
      value = (value*9/5) - 459.67;
      break;
    case 1:
      value = (value*9/5) + 32;
      break;
    default:
      return;
    }
    type = 2;
  }
private:
  int type;//type of temperature  0 = Kelvin  1 = Celsius  2 = Fahrenheit
};
int main(void){
  long double temp;
  cout << "Please enter temperature C : ";
  cin >> temp;
  temperature t1(&temp,1);
  cout << "Please enter temperature F : ";
  cin >> temp;
  temperature t2(&temp,2);
  cout << "Please enter temperature K : ";
  cin >> temp;
  temperature t3(&temp,0);
  //echo first temp
  cout << "First  Temperature is ";
  t1.toFah();
  cout << t1.value << "F and ";
  t1.toKel();
  cout << t1.value << "K\n";
  //echo second temp
  cout << "Second Temperature is ";
  t2.toCel();
  cout << t2.value << "C and ";
  t2.toKel();
  cout << t2.value << "K\n";
  //echo third temp
  cout << "Third  Temperature is ";
  t3.toCel();
  cout << t3.value << "C and ";
  t3.toFah();
  cout << t3.value << "F\n";
}
