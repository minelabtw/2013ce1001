#include<iostream>
#include <math.h>
using namespace std;
void foo(double&x); //函式

int main()
{
    double x = 0 ,y =0 , z = 0 ,c = 0 , d = 0 ,a = 0 ,b = 0; //宣告變數
    foo(x); // 不用加&
    y=x*(9.0/5)+32;  //各個單位轉換
    z=x + 273.15;
    c=(x - 32)*(5.0/9);
    d=c + 237.15;
    a=x -273.15;
    b=a*(9.0/5)+32;
cout<<"First  Temperature is "<<y<<"F and "<<z<<"K"<<endl;
cout<<"Second Temperature is "<<c<<"C and "<<d<<"K"<<endl;
cout<<"Third  Temperature is "<<a<<"C and "<<b<<"F"<<endl;
}

void foo(double &x )  //在函式進行運算
{
     cout<<"Please enter temperature C : ";
    cin>>x;
    cout<<"Please enter temperature F : ";
    cin>>x;
    cout<<"Please enter temperature K : ";
    cin>>x;
}
