#include <iostream>
using namespace std;

void CtoF(double &t){//尼ん放锣传Θ地ん放
    t = t*9/5+32;
}

void FtoK(double &t){//地ん放锣传Θ惩ん放
    t = (t-32)*5/9+273.15;
}

void KtoC(double &t){//惩ん放锣传Θ尼ん放
    t = t-273.15;
}

void FtoC(double &t){//地ん放锣传Θ尼ん放
    FtoK(t);
    KtoC(t);
}

void CtoK(double &t){//尼ん放锣传Θ惩ん放
    CtoF(t);
    FtoK(t);
}

int main(){
    double inputC,inputF,inputK;//double篈,だ块尼ん,地ん,惩ん放 
    cout << "Please enter temperature C : ";//块贺放 
    cin >> inputC;
    cout << "Please enter temperature F : ";
    cin >> inputF;
    cout << "Please enter temperature K : ";
    cin >> inputK;
    cout << "First  Temperature is ";//锣传尼ん放 
    CtoF(inputC);
    cout << inputC << "F and ";
    FtoK(inputC);
    cout << inputC << "K\n";
    cout << "Second Temperature is ";//锣传地ん放 
    FtoC(inputF);
    cout << inputF << "C and ";
    CtoK(inputF);
    cout << inputF << "K\n";
    cout << "Third  Temperature is ";//锣传惩ん放 
    KtoC(inputK);
    cout << inputK << "C and ";
    CtoF(inputK);
    cout << inputK << "F\n";
    return 0;
}
