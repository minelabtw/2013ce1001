#include <iostream>

using namespace std;

void temp_fc (double &);
void temp_kc (double &);
void temp_cf (double &);
void temp_ck (double &);     //宣告函式

int main()
{
    double c;
    double f;
    double k;

    cout << "Please enter temperature C : " ;
    cin >> c;

    cout << "Please enter temperature F : " ;
    cin >> f;

    cout << "Please enter temperature K : " ;
    cin >> k;

    temp_cf (c);
    cout << "First  Temperature is " << c << "F and " ;
    temp_fc (c);
    temp_ck (c);
    cout << c << "K" << endl;

    temp_fc (f);
    cout << "Second Temperature is " << f << "C and " ;
    temp_ck (f);
    cout << f << "K" << endl;

    temp_kc (k);
    cout << "Third  Temperature is " << k << "C and " ;
    temp_cf (k);
    cout << k << "F" ;



    return 0;
}

void temp_fc (double &c)     //華氏轉攝氏
{
    c =( c - 32 ) * 5 / 9 ;
}

void temp_kc (double &c)     //凱氏轉攝氏
{
    c = c - 273.15 ;
}

void temp_cf (double &f)     //攝氏轉華氏
{
    f = f * 9 / 5 + 32 ;
}

void temp_ck (double &k)     //攝氏轉凱氏
{
    k = k + 273.15 ;
}
