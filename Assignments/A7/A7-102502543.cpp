#include <iostream>
using namespace std;
void a(double &); //宣告函式a
void b(double &); //宣告函式b
void c(double &); //宣告函式c
void d(double &); //宣告函式d
int main()
{
    double x=0; //宣告變數x
    double y=0; //宣告變數y
    double z=0; //宣告變數z
    cout <<"Please enter temperature C : "; //輸出
    cin >> x; //輸入
    cout <<"Please enter temperature F : ";
    cin >> y;
    cout <<"Please enter temperature K : ";
    cin >> z;
    a(x);
    cout <<"First  Temperature is "<<x<<"F and ";
    c(x);
    b(x);
    cout <<x<<"K"<<endl;
    c(y);
    cout <<"Second Temperature is "<<y<<"C and ";
    b(y);
    cout <<y<<"K"<<endl;
    d(z);
    cout <<"Third  Temperature is "<<z<<"C and ";
    a(z);
    cout <<z<<"F";
    return 0;
}
void a(double &t) //函示a
{
    t=t*9/5+32;
}
void b(double &t) //函示b
{
    t=t+273.15;
}
void c(double &t) //函示c
{
    t=(t-32)*5/9;
}
void d(double &t) //函示d
{
    t=t-273.15;
}
