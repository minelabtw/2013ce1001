#include<iostream>
using namespace std;
void CtoKandF(double,double &,double &);
void FtoCandK(double,double &,double &);
void KtoCandF(double,double &,double &);
//temperature conversion
/*
CtoKandF is used to conver Celsius to Fahrenheit and Kelvin
FtoCandK is used to conver Fahrenheit to Celsius and Kelvin
KtoCandF is used to conver Kelvin to Celsius and Fahrenheit

*/
int main()
{
    double C;
    double F;
    double K;
    double answer1,answer2;
    cout << "Please enter temperature C : ";
    cin >> C;
    cout << "Please enter temperature F : ";
    cin >> F;
    cout << "Please enter temperature K : ";
    cin >> K;
    //input temperature

    CtoKandF(C,answer1,answer2);
    cout << "First Temperature is " << answer1 << "F and " << answer2 << "K\n";
    FtoCandK(F,answer1,answer2);
    cout << "Secnod Temperature is " << answer1 << "C and " << answer2 << "K\n";
    KtoCandF(K,answer1,answer2);
    cout << "Third Temperature is " << answer1 << "C and " << answer2 << "F\n";
    //output the answer
    return 0;
}
void CtoKandF(double Celsius,double &Fahrenheit,double &Kelvin)
{
    Fahrenheit=Celsius*9/5+32;
    Kelvin = Celsius+273.15;
}
void FtoCandK(double Fahrenheit,double &Celsius,double &Kelvin)
{
    Celsius=(Fahrenheit-32)*5/9;
    Kelvin=Celsius+273.15;
}
void KtoCandF(double Kelvin,double &Celsius,double &Fahrenheit)
{
    Celsius=Kelvin-273.15;
    Fahrenheit=Celsius*9/5+32;
}
