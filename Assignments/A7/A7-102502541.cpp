#include<iostream>
using namespace std;
void CF(double &x);//宣告無回傳值的函數CF 參數x的記憶體位置
void CK(double &x);//宣告無回傳值的函數CK 參數x的記憶體位置
void F(double &x);//宣告無回傳值的函數F 參數x的記憶體位置
void K(double &x);//宣告無回傳值的函數K 參數x的記憶體位置
int main()
{
    double t1 = 0;//宣告變數t1=0
    double t2 = 0;//宣告變數t3=0
    double t3 = 0;//宣告變數t3=0
    cout << "Please enter temperature C : ";//輸出Please enter temperature C :
    cin >> t1;//輸入t1
    cout << "Please enter temperature F : ";//輸出Please enter temperature F :
    cin >> t2;//輸入t2
    cout << "Please enter temperature K : ";//輸出Please enter temperature K :
    cin >> t3;//輸入t3
    F(t1);//呼叫函式F代入t1
    cout << "First  Temperature is " << t1 << "F " << "and ";//輸出First  Temperature is t1的值F and
    CF(t1);//呼叫函式CF代入t1
    K(t1);//呼叫函式K代入t1
    cout << t1  << "K " << endl;//輸出 t1的值K 換行
    CF(t2);//呼叫函式CF代入t2
    cout << "Second  Temperature is " << t2  << "C " << "and ";//輸出Second  Temperature is t2的值C and
    K(t2);//呼叫函式K代入t2
    cout << t2 << "K " << endl;//輸出 t2的值K 換行
    CK(t3);//呼叫函式CK代入t3
    cout << "Third  Temperature is " << t3  << "C " << "and ";//輸出Third  Temperature is t3的值C and
    F(t3);//呼叫函式F代入t3
    cout << t3 << "F ";//輸出 t3的值F 換行
    return 0;
}
void CF(double &x)//函式CF 參數為x的記憶體位置
{
    x=(x-32)*5/9;
}
void CK(double &x)//函式CK 參數為x的記憶體位置
{
    x=x-273.15;
}
void F(double &x)//函式F 參數為x的記憶體位置
{
    x=x*9/5+32;
}
void K(double &x)//函式K 參數為x的記憶體位置
{
    x=x+273.15;
}
