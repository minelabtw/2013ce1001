#include <iostream>
using namespace std;

void cTof(double &); // initialize function cTof
void cTok(double &); // initialize function cTok
void fToc(double &); // initialize function fToc
void fTok(double &); // initialize function fTok
void kToc(double &); // initialize function kToc
void kTof(double &); // initialize function kTof


int main()
{
    double  c,f,k,c1,f1,k1; // initialize double c,f,k,c1,f1,k1
    cout << "Please enter temperature C : " ;
    cin >> c;
    c1 = c; // although c changes, c1 is the original value of c
    cout << "Please enter temperature F : " ;
    cin >> f;
    f1 = f; // although f changes, f1 is the original value of f
    cout << "Please enter temperature K : " ;
    cin >> k ;
    k1 = k; // although k changes, k1 is the original value of k

    cTof( c );
    cout << "First  Temperature is " << c << "F";
    cTok( c1 );
    cout << " and " << c1  << "K";
    fToc( f );
    cout<< "\nSecond Temperature is " << f << "C";
    fTok(f1);
    cout  << " and " << f1 << "K";
    kToc(k);
    cout << "\nThird Temperature is " <<  k << "C";
    kTof(k1);
    cout  << " and " << k1 << "F";
    return 0;
}

void cTof(double &cf)
{
    cf = cf*1.8 + 32;
}

void cTok(double &ck)
{
    ck = ck + 273.15;
}

void fToc(double &fc)
{
    fc = (fc-32)*5/9;
}

void fTok(double &fk)
{
    fk = (fk+459.67)*5/9;
}

void kToc(double &kc)
{
    kc = kc-273.15;
}

void kTof(double &kf)
{
    kf = kf*(1.8)-459.67;
}
