#include<iostream>
#include<math.h>
using namespace std;

 double FtoC(double &F)//方程式FtoC指的是把華氏轉成攝氏的方程式
 {
     F=(F-32)*5/9;
 }
 double CtoF(double &C)//方程式FtoC指的是把攝氏轉成華氏的方程式
 {
     C=C*9/5+32;
 }
 double KtoC(double &K)//方程式FtoC指的是把華氏轉成攝氏的方程式
 {
     K=K-273.15;
 }
 double FtoK(double &F)//方程式FtoC指的是把華氏轉成攝氏的方程式
 {
     F=(F-32)*5/9+273.15;
 }
 double CtoK(double &C)//方程式FtoC指的是把攝氏轉成凱氏的方程式
 {
     C=C+273.15;
 }
 double KtoF(double &K)//方程式FtoC指的是把凱氏轉成華氏的方程式
 {
     K=(K-273.15)*9/5+32;
 }

 int main()
 {
     double C;//輸入的攝氏值
     double F;//輸入的華氏值
     double K;//輸入的凱氏值
     double ctof;//從攝氏轉成華氏的值
     double ctok;//從攝氏轉成凱氏的值
     double ftoc;//從華氏轉成攝氏的值
     double ftok;//從華氏轉成凱氏的值
     double ktoc;//從凱氏轉成攝氏的值
     double ktof;//從凱氏轉成華氏的值

     cout<<"Please enter temperature C : ";
     cin>>C;
     cout<<"Please enter temperature F : ";
     cin>>F;
     cout<<"Please enter temperature K : ";
     cin>>K;

     CtoF(C);
     ctof=C;
     FtoC(C);
     CtoK(C);
     ctok=C;
     FtoC(F);
     ftoc=F;
     CtoF(F);
     FtoK(F);
     ftok=F;
     KtoC(K);
     ktoc=K;
     CtoK(K);
     KtoF(K);
     ktof=K;

     cout<<"First  Temperature is "<<ctof<<"F"<<"and"<<ctok<<"K"<<endl;
     cout<<"Second  Temperature is "<<ftoc<<"C"<<"and"<<ftok<<"K"<<endl;
     cout<<"Third  Temperature is "<<ktoc<<"C"<<"and"<<ktof<<"F";

     return 0;
}
