#include<iostream>
using namespace std;
void C_to_F(double &);
void C_to_K(double &);
void F_to_C(double &);
void F_to_K(double &);
void K_to_C(double &);
void K_to_F(double &);

int main()
{
    double input1,input2,input3;
    cout<<"Please enter temperature C : ";
    cin>>input1;
    cout<<"Please enter temperature F : ";
    cin>>input2;
    cout<<"Please enter temperature K : ";
    cin>>input3;

    cout<<"First  Temperature is ";
    C_to_F(input1);                                 //將原本輸入的攝氏溫標轉變成華氏溫標
    cout<<input1<<"F and ";
    F_to_K(input1);                                 //經過第21行後,原先的input已經不再是攝氏溫標了,而是華氏溫標,所以此時要以華氏溫標轉成克氏溫標
    cout<<input1<<"K"<<endl;

    cout<<"Second Temperature is ";
    F_to_C(input2);
    cout<<input2<<"C and ";
    C_to_K(input2);
    cout<<input2<<"K"<<endl;

    cout<<"Third  Temperature is ";
    K_to_C(input3);
    cout<<input3<<"C and ";
    C_to_F(input3);
    cout<<input3<<"F"<<endl;

    return 0;

}

void C_to_F(double &x)
{
    x=9.0/5*x+32;
}

void C_to_K(double &x)
{
    x=x+273.15;
}

void F_to_C(double &x)
{
    x=5.0/9*(x-32);
}

void F_to_K(double &x)
{
    F_to_C(x);
    C_to_K(x);
}

void K_to_C(double &x)
{
    x=x-273.15;
}

void K_to_F(double &x)
{
    K_to_C(x);
    C_to_F(x);
}
