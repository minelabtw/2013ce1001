#include <iostream>

using namespace std;

double C(double ,double &,double &);//宣告形式
double F(double ,double &,double &);
double K(double ,double &,double &);

int main()
{
    double c,c1,c2;
    double f,f1,f2;
    double k,k1,k2;

    cout << "Please enter temperature C : ";
    cin >> c;

    cout << "Please enter temperature F : ";
    cin >> f;

    cout << "Please enter temperature K : ";
    cin >> k;

    C(c,c1,c2);//開始做函示
    F(f,f1,f2);
    K(k,k1,k2);

    cout << "First  Temperature is " << c1 << "F and " << c2 << "K" << endl;
    cout << "Second Temperature is " << f1 << "C and " << f2 << "K" << endl;
    cout << "Third  Temperature is " << k1 << "C and " << k2 << "F" << endl;

    return 0;
}

double C(double c,double &c1,double &c2)
{
    c1=c*9/5+32;
    c2=c+273.15;
}

double F(double f,double &f1,double &f2)
{
    f1=(f-32)*5/9;
    f2=(f+459.67)*5/9;
}

double K(double k,double &k1,double &k2)
{
    k1=k-273.15;
    k2=k*9/5-459.67;
}
