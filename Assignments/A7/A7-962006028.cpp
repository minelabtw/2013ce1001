#include<iostream>

using namespace std;

double c, f, k ;           //設定變數
double cf , ck , fc ,fk ,kc,kf ;

void tempcf (double &a)   //使用函數 Call by reference
{
    a = (a*(9.0/5.0))+32;
}
void tempck (double &b)
{
    b=b+273.15;
}
void tempkc (double &c)
{
    c=c-273.15;
}
void tempfc (double &d)
{
    d=(d-32)*5.0/9.0;
}
int main()
{
    cout << "Please enter temperature C :" ;  //輸入與抓取變數
    cin >> c ;
    cf=c;
    ck=c;
    tempcf (cf) ;      //放入適當函數
    tempck (ck) ;

    cout << "Please enter temperature F :" ;
    cin >> f ;
    fc=f;
    tempfc (fc);
    fk=fc;
    tempck (fk);

    cout << "Please enter temperature K :" ;
    cin >> k ;
    kc=k;
    tempkc (kc);
    kf=kc;
    tempcf (kf);

    cout << "First   Temperature is " << cf << "F" << " and " << ck << "K" << endl ;  //輸出索求資訊
    cout << "Second  Temperature is " << fc << "C" << " and " << fk << "K" << endl ;
    cout << "Third   Temperature is " << kc << "C" << " and " << kf << "F" << endl ;
    system("pause");
    return 0 ;
}
