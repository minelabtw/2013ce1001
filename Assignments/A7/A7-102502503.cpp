#include <iostream>
using namespace std;

void CTOF(double &ctof);  //宣告副函式CTOF
void CTOK(double &ctok);  //宣告副函式CTOK
void FTOC(double &ftoc);  //宣告副函式FTOC
void FTOK(double &ftok);  //宣告副函式FTOK
void KTOC(double &ktoc);  //宣告副函式KTOC
void KTOF(double &ktof);  //宣告副函式KTOF

int main()
{
    double c,f,k,ctof,ctok,ftoc,ftok,ktoc,ktof;  //宣告變數

    cout << "Please enter temperature C : ";
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;

    ctof=ctok=c;  //輸入的c值等於ctof和ctok
    ftoc=ftok=f;  //輸入的f值等於ftoc和ftok
    ktoc=ktof=k;  //輸入的k值等於ktoc和ktof

    CTOF(ctof);  //呼叫CTOF函式
    CTOK(ctok);  //呼叫CTOK函式
    FTOC(ftoc);  //呼叫FTOC函式
    FTOK(ftok);  //呼叫FTOK函式
    KTOC(ktoc);  //呼叫KTOC函式
    KTOF(ktof);  //呼叫KTOF函式

    cout << "First  Temperature is " << ctof << "F and " << ctok << "K" << endl;
    cout << "Second Temperature is " << ftoc << "C and " << ftok << "K" << endl;
    cout << "Third  Temperature is " << ktoc << "C and " << ktof << "F";

    return 0;
}

void CTOF(double &ctof)  //將攝氏轉為華氏
{
    ctof=ctof*1.8+32;
}

void CTOK(double &ctok)  //將攝氏轉為克氏
{
    ctok=ctok+273.15;
}

void FTOC(double &ftoc)  //將華氏轉為攝氏
{
    ftoc=(ftoc-32)*(5./9);
}

void FTOK(double &ftok)  //將華氏轉為克氏
{
    ftok=(ftok+459.67)*(5./9);
}

void KTOC(double &ktoc)  //將克氏轉為攝氏
{
    ktoc=ktoc-273.15;
}

void KTOF(double &ktof)  //將克氏轉為華氏
{
    ktof=ktof*1.8-459.67;
}
