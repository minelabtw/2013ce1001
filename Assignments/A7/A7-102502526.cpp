#include <iostream>
using namespace std;

double cf(double,double &),ck(double,double &),fk(double,double &),fc(double,double &),kf(double,double &),kc(double,double &);
int main()
{
    double c,f,k;     //代表將輸入的攝氏、華氏、凱氏
    double cfa,cka,fca,fka,kca,kfa=0;  //代表將輸出各種函數運算後的結果

    cout<<"Please enter temperature C: ";
    cin>>c;
    cout<<"Please enter temperature F: ";
    cin>>f;
    cout<<"Please enter temperature K: ";
    cin>>k;

    cf(c,cfa),ck(c,cka),fc(f,fca),fk(f,fka),kc(k,kca),kf(k,kfa);   //霹靂卡霹靂拉拉波波莉娜貝貝魯多，召喚函數!!!

    cout<<"First temperature is "<<cfa<<" and "<<cka<<endl;
    cout<<"Sencond temperature is "<<fca<<" and "<<fka<<endl;
    cout<<"Third temperature is "<<kca<<" and "<<kfa;    //輸出結果

    return 0;
}

double cf(double c,double &cfa)   //宣告函數cf()、輸入值c、替換值cfa都是double形式  (&的意義在宣告代換回主函數)
{
    cfa=9*c/5+32;
}

double ck(double c,double &cka)
{
    cka=c+273.15;
}

double fc(double f,double &fca)
{
    fca=(f-32)*5/9;
}

double fk(double f,double &fka)
{
    fka=(f-32)*5/9+273.15;
}

double kc(double k,double &kca)
{
    kca=k-273.15;
}

double kf(double k,double &kfa)
{
    kfa=(k-273.15)*9/5+32;
}
