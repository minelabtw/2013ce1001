#include<iostream>

using namespace std;

void cturnk (double & );                     //宣告新的函式
void cturnf (double & );
void fturnc (double & );
void fturnk (double & );
void kturnc (double & );
void kturnf (double & );

int main()
{
    double c=0,f=0,k=0;                      //宣告變數
    cout <<"Please enter temperature C : ";
    cin >>c;
    cout <<"Please enter temperature F : ";
    cin >>f;
    cout <<"Please enter temperature K : ";
    cin >>k;
    cturnf(c);                               //使用&改變其原始直
    cout <<"First  Temperature is "<<c<<"F";
    fturnk(c);
    cout <<" and "<<c<<"K"<<endl;
    fturnc(f);
    cout <<"Second Temperature is "<<f<<"C";
    cturnk(f);
    cout <<" and "<<f<<"K"<<endl;
    kturnc(k);
    cout <<"Third  Temperature is "<<k<<"C";
    cturnf(k);
    cout <<" and "<<k<<"F"<<endl;

    return 0;
}
void cturnf (double &i)                       //新函式內容
{
    i=i*9/5+32;
}
void cturnk (double &t)
{
    t=t+273.15;
}
void fturnc (double &o)
{
    o=(o-32)*5/9;
}
void fturnk (double &y)
{
    y=((y-32)*5/9)+273.15;
}
void kturnc (double &p)
{
    p=p-273.15;
}
void kturnf(double &u)
{
    u=(u-273.15)*9/5+32;
}
