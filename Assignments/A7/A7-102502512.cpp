#include<iostream>
using namespace std;
void degreechangecfk(double &, double &, double &);                                  //Line 3~5:define three function
void degreechangefck(double &, double &, double &);
void degreechangekcf(double &, double &, double &);
int main()
{
    double degc,degk,degf;
    double c,f,k;                                                //Line 10~15: Let user enter degree
    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    do
    {
    cout<<"Please enter temperature k : ";
    cin>>k;
    if (k<=0)
    {
        cout<<"Out of range!"<<endl;
    }
    }while(k<=0);
    degreechangecfk(c, degf,degk);
    cout<<"First  Temperature is "<<degf<<"F and "<<degk<<"K";                                           //Line 16~20:translate degree
    cout<<endl;
    degreechangefck(f, degc, degk);
    cout<<"First  Temperature is "<<degc<<"C and "<<degk<<"K";
    cout<<endl;
    degreechangekcf(k, degc, degf);
    cout<<"First  Temperature is "<<degc<<"C and "<<degf<<"F";
    return 0;
}
void degreechangecfk(double &degc, double &degf, double &degk )                                                            //Line 23~emd:Let computer know what the function means
{
    degf=degc*9/5+32;
    degk=degc+273.15;
}
void degreechangefck(double &degf, double &degc, double &degk )
{
    degc=(degf-32)*5/9;
    degk=(degf-32)*5/9+273.15;
}
void degreechangekcf(double &degk, double &degc, double &degf )
{
    degc=degk-273.15;
    degf=(degk-273.15)*9/5+32;
}

