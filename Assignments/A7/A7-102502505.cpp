#include <iostream>
using namespace std;

double cint;//定義變數cint
double fint;//定義變數fint
double kint;//定義變數kint
double x = 0;//定義運算值x，並使之可以有小數

void c_f(double& x)//設定函數，c轉f
{
    x = cint;//先將運算值重新設為與cint相同
    x = x*9/5+32;//再將x做運算，再輸出x
}
void c_k(double& x)//設定函數，c轉k
{
    x = cint;//先將x值重新設定為cint
    x = x+273.15;
}
void f_c(double& x)//設定函數，f轉c
{
    x = fint;
    x = (x-32)*5/9;
}
void f_k(double& x)//設定函數，f轉k
{
    x = fint;
    x = (x+459.67)*5/9;
}
void k_c(double& x)//設定函數，k轉c
{
    x = kint;
    x = x-273.15;
}
void k_f(double& x)//設定函數，k轉f
{
    x = kint;
    x = x*9/5-459.67;
}

int main()//主要程式
{
    cout << "Please enter temperature C : ";//輸出字串
    cin >> cint;

    cout << "Please enter temperature F : ";
    cin >> fint;

    cout << "Please enter temperature K : ";
    cin >> kint;

    c_f(x);//先呼叫第一個函式
    cout << "First  Temperature is " << x << "F and ";//輸出x1(x可做多次運算，並覆蓋其值)
    c_k(x);//呼叫第二個函式
    cout << x << "K" << endl;//輸出x2
    f_c(x);//下面以此類推
    cout << "Second Temperature is " << x << "C and ";//輸出x3
    f_k(x);
    cout << x << "K" << endl;//輸出x4
    k_c(x);
    cout << "Third  Temperature is " << x << "C and ";//輸出x5
    k_f(x);
    cout << x << "F" << endl;//輸出x6

    return 0;//返回初始值
}
