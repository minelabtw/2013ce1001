#include <iostream>
#include <limits>
using namespace std;

int main()
{
    double c, f, k , converted_1 , converted_2;     //c、f、k分別是使用者輸入的溫度，而converted_1與converted_2表示轉換成另外兩種溫標的兩個值
    void C_to_other( double , double & , double &);
    void F_to_other( double , double & , double &);
    void K_to_other( double , double & , double &);
    double inputDegree( double & , char );          //prototype四個自訂的函式

    inputDegree( c , 'C' );
    inputDegree( f , 'F' );
    inputDegree( k , 'K' );                         //利用inputDegree檢驗使用者輸入是否為數字

    C_to_other( c , converted_1 , converted_2);
    cout <<"First  Temperature is " << converted_1 << "F and " <<converted_2 << "K" << endl;

    F_to_other( f , converted_1 , converted_2);
    cout <<"Second Temperature is " << converted_1 << "C and " <<converted_2 << "K" << endl;

    K_to_other( k , converted_1 , converted_2);
    cout <<"Third  Temperature is " << converted_1 << "C and " <<converted_2 << "F" << endl;
                                                    //呼叫轉換溫標的函式，並輸出被refrence的兩個變數：converted_1與converted_2
    return 0;
}

double inputDegree ( double &degree , char type)
{
    int OutOfRangeBool = 0;                                     //錯誤時判斷用的變數
    do
    {
        OutOfRangeBool = 0;
        cout << "Please enter temperature " << type << " : " ;  //提示使用者輸入何種溫度
        cin >> degree;
        if ( !(cin) )                                           //若使用者輸入非整數導致cin錯誤時
        {
            cin.clear();                                        //先清除cin的錯誤狀態
            cin.ignore(numeric_limits<streamsize>::max(), '\n');//刪除緩衝區所有的資料
            cout << "Out of range!\n";                          //提示錯誤
            OutOfRangeBool = 1;
        }
    }
    while ( OutOfRangeBool == 1 );
}
void C_to_other ( double input , double &f , double &k )        //input為被轉換的溫標，f表華氏、k表凱式
{                                                               //其中f與k分別為converted1與converted2的記憶體位置
    f = input * 9 / 5 + 32;
    k = input + 273.15;
}
void F_to_other ( double input , double &c , double &k )        //input為被轉換的溫標F，c表攝氏、k表凱式
{                                                               //其中c與k分別為converted1與converted2的記憶體位置
    c = ( input -32 ) *  5 / 9 ;
    k = c + 273.15;
}
void K_to_other ( double input , double &c , double &f )        //input為被轉換的溫標k，c表攝氏、f表華氏
{                                                               //其中c與f分別為converted1與converted2的記憶體位置
    c = input - 273.15;
    f = c * 9 / 5 + 32;
}

