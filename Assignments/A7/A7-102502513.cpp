#include <iostream>
using namespace std;
double C(double &c1, double &f1, double &k1);  //加入函式原型宣告
double F(double &c2, double &f2, double &k2);
double K(double &c3, double &f3, double &k3);

int main()
{
    double c1, f1, k1;
    double c2, f2, k2;
    double c3, f3, k3;

    cout << "Please enter temperature C : ";
    cin >> c1;
    cout << "Please enter temperature F : ";
    cin >> f2;
    cout << "Please enter temperature K : ";
    cin >> k3;

    C(c1,f1,k1);  //呼叫函式
    cout << "First  Temperature is " << f1 <<"F" << " and " << k1 << "K" << endl;
    F(c2,f2,k2);
    cout << "Second Temperature is " << c2 << "C" << " and " << k2 << "K" << endl;
    K(c3,f3,k3);
    cout << "Third  Temperature is " << c3 << "C" << " and " << f3 << "F" << endl;

    return 0;
}



double C(double &c1, double &f1, double &k1) //建立函式
{
    f1=c1*9/5+32;  //溫標轉換
    k1=c1+273.15;
}
double F(double &c2, double &f2, double &k2)
{
    c2=(f2-32)*5/9;
    k2=(f2+459.67)*5/9;
}
double K(double &c3, double &f3, double &k3)
{
    c3=k3-273.15;
    f3=k3*9/5-459.67;
}

