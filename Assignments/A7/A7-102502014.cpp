#include<iostream>

using namespace std;

void temperatureC (float &a,float &b)             //計算攝氏轉華式與克式
{
    a=a*9/5+32;
    b=b+273.15;
}
void temperatureF (float &a,float &b)             //計算華式轉攝氏與克式
{
    a=(a-32)*5/9;
    b=(b-32)*5/9+273.15;
}
void temperatureK (float &a,float &b)             //計算克式轉攝氏與華視
{
    a=a-273.15;
    b=(b-273.15)*9/5+32;
}
int main()
{

    float c;                                      //攝氏
    float f;                                      //華式
    float k;                                      //克式
    float a;
    float b;

    cout<<"Please enter temperature C : ";
    cin>>c;
    cout<<"Please enter temperature F : ";
    cin>>f;
    cout<<"Please enter temperature K : ";
    cin>>k;

    a=c;
    b=c;
    temperatureC(a,b);                                          //輸入的c指定給a、b
    cout<<"First  Temperature is "<<a<<"F and "<<b<<"K"<<endl;
    a=f;
    b=f;
    temperatureF(a,b);                                          //輸入的f指定給a、b
    cout<<"Second Temperature is "<<a<<"C and "<<b<<"K"<<endl;
    a=k;
    b=k;
    temperatureK(a,b);                                          //輸入的k指定給a、b
    cout<<"Third  Temperature is "<<a<<"C and "<<b<<"F"<<endl;

    return 0;
}
