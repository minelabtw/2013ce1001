#include<iostream>
#include<cstdlib>
using namespace std ;
void change_temp(int ,double *,double *,double *) ;// 次數 攝氏 華氏 凱氏

int main()
{
    double c ; //攝氏
    double f ; //華氏
    double k ; //凱氏
    double out1 ,out2 ;
    cout << "Please enter temperature C : " ;
    cin >> c ;
    cout << "Please enter temperature F : " ;
    cin >> f ;
    cout << "Please enter temperature K : " ;
    cin >> k ;
    change_temp(1,&c,&out1,&out2) ; //第一次轉換
    cout << "First  Temperature is "<< out1 <<"F and " << out2 <<"K\n" ;
    change_temp(2,&f,&out1,&out2) ; //第二次轉換
    cout << "Second Temperature is "<< out1 <<"C and " << out2 <<"K\n" ;
    change_temp(3,&k,&out1,&out2) ; //第三次轉換
    cout << "Third  Temperature is "<< out1 <<"C and " << out2 <<"F\n" ;
    return 0;
}
void change_temp(int mode,double *in,double *out1,double *out2)
{
    switch (mode)
    {
    case 1 :
        *out1=*in*1.8+32 ; //攝氏轉華氏
        *out2=*in+273.15 ; //攝氏轉凱氏
        break;
    case 2 :
        *out1=(*in-32)*5/9 ; //華氏轉攝氏
        *out2=*out1+273.15 ; //攝氏轉凱氏
        break;
    case 3 :
        *out1=*in-273.15 ; //凱氏轉攝氏
        *out2=*out1*1.8+32 ; //攝氏轉華氏
        break;
    default :
        break ;
    }
}
