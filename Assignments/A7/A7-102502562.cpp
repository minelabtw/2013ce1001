#include <iostream>
using namespace std;

void CtoF(double &c1)//攝氏轉華氏
{
    c1=c1 * 9 / 5 + 32;
}
void CtoK(double &c2)//攝氏轉凱式
{
    c2=c2 + 273.15;
}
void FtoC(double &f1)//華氏轉攝氏
{
    f1=(f1 - 32)* 5 / 9;
}
void FtoK(double &f2)//華氏轉凱氏
{
    f2=(f2 + 459.67) * 5 / 9;
}
void KtoC(double &k1)//凱氏轉攝氏
{
    k1=k1 - 273.15;
}
void KtoF(double &k2)//凱氏轉華氏
{
    k2=k2 * 9 / 5 - 459.67;
}

int main()
{
    double c1,c2;//宣告型別為浮點數的c1,c2以儲存輸入的c
    double f1,f2;//宣告型別為浮點數的f1,f2以儲存輸入的f
    double k1,k2;//宣告型別為浮點數的k1,k2以儲存輸入的k

    cout << "Please enter temperature C : ";
    cin >> c1;
    cout << "Please enter temperature F : ";
    cin >> f1;
    cout << "Please enter temperature K : ";
    cin >> k1;

    c2=c1;//讓c2的值等於c1
    f2=f1;
    k2=k1;

    CtoF(c1);//帶入公式
    CtoK(c2);
    FtoC(f1);
    FtoK(f2);
    KtoC(k1);
    KtoF(k2);

    cout << "First  Temperature is " << c1 << "F and " << c2 << "K" << endl;
    cout << "Second Temperature is " << f1 << "C and " << f2 << "K" << endl;
    cout << "Third  Temperature is " << k1 << "C and " << k2 << "F" << endl;

    return 0;
}
