#include <iostream>
using namespace std;
void temperatureC( double & , double & );
void temperatureF( double & , double & );
void temperatureK( double & , double & );
main()
{
    double c;  //攝氏
    double f;  //華氏
    double k;  //克氏

    cout << "Please enter temperature C : ";  //輸入提示
    cin >> c;
    cout << "Please enter temperature F : ";
    cin >> f;
    cout << "Please enter temperature K : ";
    cin >> k;

    double cf=c;  //轉換用函數
    double ck=c;
    double fc=f;
    double fk=f;
    double kc=k;
    double kf=k;

    temperatureC( cf , ck );  //轉換用函式
    temperatureF( fc , fk );
    temperatureK( kc , kf );

    cout << "First  Temperature is " << cf << "F and " << ck << "K" << endl;  //輸出結果
    cout << "Second  Temperature is " << fc << "C and " << fk << "K" << endl;
    cout << "Third  Temperature is " << kc << "C and " << kf << "F" << endl;

    return 0;
}
void temperatureC( double &cf , double &ck )  //攝氏轉換
{
    cf=ck*9/5+32;
    ck=ck+273.15;
}
void temperatureF( double &fc , double &fk )  //華氏轉換
{
    fc=(fc-32)*5/9;
    fk=fc+273.15;
}
void temperatureK( double &kc , double &kf )  //克氏轉換
{
    kc=kc-273.15;
    kf=kc*9/5+32;
}
