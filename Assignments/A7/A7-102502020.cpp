#include <iostream>

using namespace std;

void Formula1(double &);                        //宣告函式
void Formula2(double &);
void Formula3(double &);
void Formula4(double &);
void Formula5(double &);
void Formula6(double &);

int main()
{
    double C=0;                                 //宣告變數
    double F=0;
    double K=0;
    double a=0;
    double b=0;
    double c=0;
    double d=0;
    double e=0;
    double f=0;

    cout << "Please enter temperature C : ";    //輸出字串
    cin >> C;                                   //輸入數值
    cout << "Please enter temperature F : ";
    cin >> F;
    cout << "Please enter temperature K : ";
    cin >> K;
    a=C;                                        //將C的值給a
    b=C;
    c=F;
    d=F;
    e=K;
    f=K;
    Formula1(a);                                //呼叫函式，並用a代入
    Formula2(b);
    Formula3(c);
    Formula4(d);
    Formula5(e);
    Formula6(f);
    cout << "First  Temperature is " << a << "F and " << b << "K" << endl
         << "Second Temperature is " << c << "C and " << d << "K" << endl
         << "Third  Temperature is " << e << "C and " << f << "F";

    return 0;
}
void Formula1(double &x)    //將&x指向a，可以更改a的值
{
    x=x*9/5+32;             //用a進行運算
}
void Formula2(double &x)
{
    x=x+273.15;             //用b進行運算
}
void Formula3(double &x)
{
    x=(x-32)*5/9;           //用c進行運算
}
void Formula4(double &x)
{
    x=(x+459.67)*5/9;       //用d進行運算
}
void Formula5(double &x)
{
    x=x-273.15;             //用e進行運算
}
void Formula6(double &x)
{
    x=x*9/5-459.67;         //用f進行運算
}
