#include<iostream>
using namespace std;
void fk(double&,double&,double&);//宣告函數型態
void ck(double&,double&,double&);
void cf(double&,double&,double&);
double c1,f1,k1,c2,f2,k2,c3,f3,k3;
int main()
{
    cout<<"Please enter temperature C : ";
    cin>>c1;
    cout<<"Please enter temperature F : ";
    cin>>f2;
    cout<<"Please enter temperature K : ";
    cin>>k3;
    fk(c1,f1,k1);//呼叫函數
    cout<<"First  Temperature is "<<f1<<"F"<<" and "<<k1<<"K"<<endl;
    ck(c2,f2,k2);
    cout<<"Second Temperature is "<<c2<<"C"<<" and "<<k2<<"K"<<endl;
    cf(c3,f3,k3);
    cout<<"Third  Temperature is "<<c3<<"C"<<" and "<<f3<<"F"<<endl;
}
void fk(double &c1,double &f1,double &k1)//定義函數內容
{
f1=c1*9/5+32;
k1=c1+273.15;
}
void ck(double &c2,double &f2,double &k2)
{
c2=(f2-32)*5/9;
k2=(f2-32)*5/9+273.15;
}
void cf(double &c3,double &f3,double &k3)
{
c3=k3-273.15;
f3=(k3-273.15)*9/5+32;
}
