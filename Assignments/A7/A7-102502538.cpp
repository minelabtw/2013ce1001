#include<iostream>
using namespace std;

void c2f(double&);                       //設定需要的函式
void c2k(double&);
void f2c(double&);
void f2k(double&);
void k2c(double&);
void k2f(double&);

void c2f(double &c)
{
    c=c*1.8+32;
}
void c2k(double &c)
{
    c=c+273.15;
}
void f2c(double &f)
{
    f=(f-32)*5/9;
}
void f2k(double &f)
{
    f=(f-32)*5/9+273.15;
}
void k2c(double &k)
{
    k=k-273.15;
}
void k2f(double &k)
{
    k=(k-273.15)*9/5+32;
}

int main()
{
    double a,b,d;

    cout <<"Please enter temperature C : ";
    cin>>a;
    cout <<"Please enter temperature F : ";
    cin>>b;
    cout <<"Please enter temperature K : ";
    cin>>d;

    double c=a,f=b,k=d;                                 //替換變數
    double c1=a,f1=b,k1=d;

    c2f(c);
    cout <<"First  Temperature is "<<c<<"F"<<" and ";   //輸出運算後的變數
    c2k(c1);
    cout <<c1<<"K"<<endl;
    f2c(f);
    cout <<"Second Temperature is "<<f<<"C"<<" and ";
    f2k(f1);
    cout <<f1<<"K"<<endl;
    k2c(k);
    cout <<"Third  Temperature is "<<k<<"C"<<" and ";
    k2f(k1);
    cout <<k1<<"F"<<endl;

    return 0;
}
