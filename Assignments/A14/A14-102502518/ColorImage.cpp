#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;
ColorImage::ColorImage()
{
}
ColorImage::~ColorImage()
{
}
void ColorImage::ReadImage(string filename)
{
    ifstream infile(filename.c_str(),ios::in);//將檔案讀入
    infile>>magicValue>>width>>height>>maxColor;
    for(int a=0;a<height;a++)//先是寬的迴圈再是高的迴圈
    {
        for(int b=0;b<width;b++)
        {
            infile>>pixels[b][a].R>>pixels[b][a].G>>pixels[b][a].B;
        }
    }
    infile.close();
}
void ColorImage::OutImage(string filename)
{
    ofstream outfile(filename.c_str(),ios::out);//將檔案輸出
    outfile<<magicValue<<endl;
    outfile<<width<<" "<<height<<endl;
    outfile<<maxColor;
    for(int a=0;a<height;a++)//先是寬的迴圈再是高的迴圈
    {
        for(int b=0;b<width;b++)
        {
            outfile<<" "<<pixels[b][a].R<<" "<<pixels[b][a].G<<" "<<pixels[b][a].B;
        }
    }
    outfile<<endl;
    outfile.close();
}
void ColorImage::Flip()
{
    for(int a=0;a<width;a++)//使用迴圈與Swap,進行交換
    {
        for(int b=0,c=height-1;c>b;b++,c--)
        {
            Swap(&pixels[a][b],&pixels[a][c]);
        }
    }
}
void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel temp=*p1;//進行交換
    *p1=*p2;
    *p2=temp;
}
