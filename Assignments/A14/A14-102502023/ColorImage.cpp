#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    fstream file; // initialize file as an object of fstream
    file.open( filename.c_str(), ios::in ); // open a file as read mode
    file >> magicValue >> width >> height >> maxColor; // read some file's data to these values

    for ( int j = 0; j < height; j++ ) // double for loop to read the values to Pixel pixels
    {
        for ( int i = 0; i < width; i++ )
        {
            file >> pixels[i][j].R;
            file >> pixels[i][j].G;
            file >> pixels[i][j].B;
        }
    }
    file.close(); // close the file
}

void ColorImage::OutImage(string filename)
{
    fstream f; // initialize f as an object of fstream
    f.open( filename.c_str(), ios::out ); // open a file as write mode
    f << magicValue << ' ' << width << ' ' << height << ' ' << maxColor << endl; // write these values into file

    for ( int j = 0; j < height; j++ ) // double for loop to write the values into file
    {
        for ( int i = 0; i < width; i++ )
        {
            f << pixels[i][j].R << ' '
             << pixels[i][j].G << ' '
             << pixels[i][j].B << ' ';
        }

    }
    f.close();// close the file
}


void ColorImage::Flip()
{
    for ( int i = 0; i < height/2; i++ ) // double for loop to flip the pixels
    {
        for ( int j = 0; j < width; j++ )
        {
            Swap( &pixels[j][i] , &pixels[j][height-1-i] );
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
   swap(*p1,*p2); // swap function to swap two pixels
}
