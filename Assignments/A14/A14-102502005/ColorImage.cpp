#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    char filename_char[30]; //宣告要用於ifstream的char陣列(ifstream的檔案名只吃char)。
    strcpy(filename_char, filename.c_str());//將string轉為char陣列。
    ifstream inClientFile(filename_char, ios::in);//開啟檔案。
    inClientFile >> magicValue >> width >> height >> maxColor;//先將檔案格式，尺寸，色素範圍儲存起來。
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
        {
            inClientFile >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;//接著依序把每一個像素的資料儲存起來。
        }
    }

}

void ColorImage::OutImage(string filename)
{
    char filename_char[30];//宣告要用於ifstream的char陣列(ifstream的檔案名只吃char)。
    strcpy(filename_char, filename.c_str());//將string轉為char陣列。
    ofstream outClientFile(filename_char, ios::out);//建立檔案。
    outClientFile << magicValue << endl << width  << " " << height  << endl << maxColor << endl;//先將檔案格式，尺寸，色素範圍寫入檔案。
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
        {
            outClientFile << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " ";//接著依序把每一個像素的資料及其中的空格寫入檔案。
        }
    }
}


void ColorImage::Flip()//將圖片翻轉過來的函式。
{
    for(int i=0; i<height/2; i++)
    {
        for(int j=0; j<width; j++)
        {
            Swap(&pixels[j][i],&pixels[j][height-i]);//第一列和倒數第一列交換像素資料，以此類推...。
        }
    }
}

void ColorImage::BlackandWhite()//將圖片轉為黑白圖片的函式。
{
    for(int i=0;i<height;i++)
    {
        for(int j=0;j<width;j++)
        {
            _swappixel.R = (pixels[j][i].R + pixels[j][i].G + pixels[j][i].B)/3;//每一個像素的R,G和B的值都變成該像素RGB的平均值。
            pixels[j][i].R = _swappixel.R;//依序更改像素資料。
            pixels[j][i].G = _swappixel.R;
            pixels[j][i].B = _swappixel.R;
        }
    }
}

void ColorImage::Invert()//將圖片轉換為負片的函式。
{
    for(int i=0;i<height;i++)
    {
        for(int j=0;j<width;j++)
        {
            pixels[j][i].R = 255 - pixels[j][i].R;//每一個像素的R,GB的值都變成255 - 原值。
            pixels[j][i].G = 255 - pixels[j][i].G;//依序更改資料。
            pixels[j][i].B = 255 - pixels[j][i].B;
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)//交換任意兩個像素的資料
{
    _swappixel = *p1;
    *p1 = *p2;
    *p2 = _swappixel;
}
