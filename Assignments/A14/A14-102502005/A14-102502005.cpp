#include<iostream>
#include "ColorImage.cpp"
using namespace std;

int main()
{
    string filename="gulf-flounder.ppm";//input file
    string outFilename="gulf-flounder_1.ppm";//output file
    ColorImage image;//image class

    image.ReadImage(filename);//讀取原檔。
    image.BlackandWhite();//轉換為黑白照片。
    image.OutImage("gulf-flounder_black_and_white.ppm");//輸出檔案。

    image.ReadImage(filename);//讀取原檔。
    image.Invert();//轉換為負片。
    image.OutImage("gulf-flounder_Invert.ppm");//輸出檔案。

    image.ReadImage(filename);//讀取原檔。
    image.Flip();//翻魚。
    image.OutImage(outFilename);//輸出檔案。

    return 0;
}
