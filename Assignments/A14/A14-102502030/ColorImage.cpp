#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage( string filename )
{
    ifstream imgin( filename.c_str(), ios::in );  //open field
    imgin >> magicValue;  //分別傳入資料
    imgin >> width >> height >> maxColor;
    for( int i=0; i<height; i++ )  //傳入pixel
    {
        for( int j=0; j<width; j++ )
        {
            imgin >> pixels[ j ][ i ].R;
            imgin >> pixels[ j ][ i ].G;
            imgin >> pixels[ j ][ i ].B;
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream imgout(filename.c_str(), ios::out);  //open fiedl
    imgout << magicValue << endl;  //傳出資料
    imgout << width << ' ' << height << endl;
    imgout << maxColor << endl;
    for( int k=height-1; k>=0; k-- )  //倒著傳出
    {
        for( int l=0; l<width; l++ )
        {
            imgout << pixels[ l ][ k ].R  << ' ' ;
            imgout << pixels[ l ][ k ].G  << ' ' ;
            imgout << pixels[ l ][ k ].B  << ' ' ;
        }
        imgout << endl;
    }
    imgout.close();
}


void ColorImage::Flip()
{
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
}
