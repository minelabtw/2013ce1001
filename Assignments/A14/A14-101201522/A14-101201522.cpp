#include<iostream>
#include "ColorImage.cpp"
using namespace std;

int main()
{
    string filename="gulf-flounder.ppm";//input file
    string outFilename="gulf-flounder_1.ppm";//output file,flip vertical
    string outFilename2="gulf-flounder_2.ppm";//flip horizontal
    string outFilename3="gulf-flounder_3.ppm";//rotate the image ninety degrees clockwise
    string outFilename4="gulf-flounder_4.ppm";//rotate the image ninety degrees counter-clockwise
    string outFilename5="gulf-flounder_5.ppm";//invert the image
    string outFilename6="gulf-flounder_6.ppm";//censor the image with a mosaic 
    string outFilename7="gulf-flounder_7.ppm";//gray scale
    ColorImage image;//image class
    image.ReadImage(filename);
    image.Flip();
    image.OutImage(outFilename);
    image.Flip();
    image.Flip_Horizontal();
    image.OutImage(outFilename2);
    image.Flip_Horizontal();
    image.Clockwise90();
    image.OutImage(outFilename3);
    image.CounterClockwise90();
    image.CounterClockwise90();
    image.OutImage(outFilename4);
    image.Clockwise90();
    image.Invert();
    image.OutImage(outFilename5);
    image.Invert();
    image.Mosaic(8,8);
    image.OutImage(outFilename6);
    image.ReadImage(filename);
    image.Gray();
    image.OutImage(outFilename7);
    return 0;
}
