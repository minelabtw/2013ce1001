#include <iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)//open and read image to pixels
{
    int i,j;
    ifstream fin;
    fin.open(filename.c_str());
    fin >> magicValue;
    fin >> width >> height;
    fin >> maxColor;
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            fin >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
        }
    }
    fin.close();
}

void ColorImage::OutImage(string filename)//output a image from pixels
{
    int i,j;
    ofstream fout;
    fout.open(filename.c_str());
    fout << magicValue << endl
         << width << " " << height << endl
         << maxColor << endl;
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            if(j)
                fout << " ";
            fout << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B;
        }
        fout << endl;
    }
    fout.close();
}

void ColorImage::Flip()//flip vertical
{
    int i,j;
    for(i=0;i<height/2;i++)
    {
        for(j=0;j<width;j++)
        {
            Swap(&pixels[i][j],&pixels[height-1-i][j]);
        }
    }
}

void ColorImage::Flip_Horizontal()//flip horizontal
{
    int i,j;
    for(i=0;i<height;i++)
    {
        for(j=0;j<width/2;j++)
        {
            Swap(&pixels[i][j],&pixels[i][width-1-j]);
        }
    }
}

void ColorImage::Clockwise90()//rotate the image ninety degrees clockwise
{
    int i,j;
    Pixel change[256][256];
    swap(height,width);
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            change[i][j] = pixels[width-1-j][i];
            pixels[width-1-j][i].R = 255;
            pixels[width-1-j][i].G = 255;
            pixels[width-1-j][i].B = 255;
        }
    }
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            pixels[i][j] = change[i][j];
        }
    }
}

void ColorImage::CounterClockwise90()//rotate the image ninety degrees counter-clockwise
{
    int i,j;
    Pixel change[256][256];
    swap(height,width);
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            change[i][j] = pixels[j][height-1-i];
            pixels[j][height-1-i].R = 255;
            pixels[j][height-1-i].G = 255;
            pixels[j][height-1-i].B = 255;
        }
    }
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            pixels[i][j] = change[i][j];
        }
    }
}

void ColorImage::Invert()//invert the image
{
    int i,j;
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            pixels[i][j].R = maxColor-pixels[i][j].R;
            pixels[i][j].G = maxColor-pixels[i][j].G;
            pixels[i][j].B = maxColor-pixels[i][j].B;
        }
    }
}

void ColorImage::Mosaic(int range1,int range2)//censor the image with a mosaic
{
    int i,j,x,y,range[2]={range1,range2};
    int newR,newG,newB;
    for(i=0;i<height;i+=range[0])
    {
        for(j=0;j<width;j+=range[1])
        {
            newR = newG = newB = 0;
            for(x=0;x<range[0]&&(i+x)<height;x++)
            {
                for(y=0;y<range[1]&&(j+y)<width;y++)
                {
                    newR += pixels[i+x][j+y].R;
                    newG += pixels[i+x][j+y].G;
                    newB += pixels[i+x][j+y].B;
                }
            }
            for(x=0;x<range[0]&&(i+x)<height;x++)
            {
                for(y=0;y<range[1]&&(j+y)<width;y++)
                {
                    pixels[i+x][j+y].R = newR/(range[0]*range[1]);
                    pixels[i+x][j+y].G = newG/(range[0]*range[1]);
                    pixels[i+x][j+y].B = newB/(range[0]*range[1]);
                }
            }
        }
    }
}

void ColorImage::Gray()//gray scale
{
    int i,j,average;
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            average = (pixels[i][j].R+pixels[i][j].G+pixels[i][j].B)/3;
            pixels[i][j].R = average;
            pixels[i][j].G = average;
            pixels[i][j].B = average;
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)//swap two pixels
{
    Pixel h;
    h = *p1;
    *p1 = *p2;
    *p2 = h;
}
