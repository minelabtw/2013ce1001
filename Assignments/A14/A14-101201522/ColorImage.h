#ifndef COLORIMAGE_H
#define COLORIMAGE_H
#include <string>
#include "Pixel.cpp"

class ColorImage
{
public:
    ColorImage();
    virtual ~ColorImage();
    void ReadImage(string filename);//open and read image to pixels
    void OutImage(string filename);//output a image from pixels
    void Flip();//flip vertical
    void Flip_Horizontal();//flip horizontal
    void Clockwise90();//rotate the image ninety degrees clockwise
    void CounterClockwise90();//rotate the image ninety degrees counter-clockwise
    void Invert();//invert the image
    void Mosaic(int range1, int range2);//censor the image with a mosaic
    void Gray();//gray scale
protected:
private:
    string magicValue;//format header
    int width,height,maxColor;//width,height,color depth
    Pixel pixels[256][256];//all pixels
    void Swap(Pixel *p1,Pixel *p2);//swap two pixels
};

#endif // COLORIMAGE_H
