#include<iostream>
#include "Pixel.h"
using namespace std;

Pixel::Pixel( unsigned int r, unsigned int g, unsigned int b)
{
    //default as white
    R=r;
    G=g;
    B=b;
}

void Pixel::setColor( unsigned int r, unsigned int g, unsigned int b )
{
    R=r;
    G=g;
    B=b;
}

unsigned int Pixel::getR() const
{
    return R;
}

unsigned int Pixel::getG() const
{
    return G;
}

unsigned int Pixel::getB() const
{
    return B;
}

Pixel::~Pixel()
{
}
