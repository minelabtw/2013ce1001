#ifndef PIXEL_H
#define PIXEL_H

class Pixel {
public:
    Pixel( unsigned int r = 255, unsigned int g = 255, unsigned int b = 255 );
    ~Pixel();
    void setColor( unsigned int r = 255, unsigned int g = 255, unsigned int b = 255 );
    unsigned int getR() const;
    unsigned int getG() const;
    unsigned int getB() const;
protected:
private:
    unsigned int R, G, B;  // Blue, Green, Red
};

#endif // PIXEL_H
