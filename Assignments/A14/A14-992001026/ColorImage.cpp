#include<iostream>
#include "ColorImage.h"
#include <fstream>
#include <sstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

// 讀取檔案
void ColorImage::ReadImage(string filename)
{
    ifstream fin( filename.c_str() );
    string line;
    getline( fin, line );
    magicValue = line;
    getline( fin, line );
    istringstream istr(line);
    istr >> width >> height;
    getline( fin, line );
    istr.clear();
    istr.str( line );
    istr >> maxColor;
    while( getline(fin, line) )
    {//增加一行
        pixels.push_back( vector<Pixel>() );
        istringstream istr2( line );
        for( int i=0 ; i<width ; ++i )
        {
            unsigned int r, g, b;
            istr2 >> r >> g >> b;
            //存入vector
            pixels.back().push_back( Pixel(r, g, b) );
        }
    }

}

//輸出檔案
void ColorImage::OutImage(string filename)
{
    ofstream fout( filename.c_str() );
    fout << magicValue << endl;
    fout << width << " " << height << endl;
    fout << maxColor << endl;
    for( int i=0 ; i<pixels.size() ; ++i )
    {
        for( int j=0 ; j<pixels[i].size() ; ++j )
        {
            fout << pixels[i][j].getR() << " " << pixels[i][j].getG() << " " << pixels[i][j].getB() << " ";
        }
        fout << endl;
    }
}

//轉換
void ColorImage::Flip()
{
    for( int i=0 ; i<pixels.size()/2 ; ++i )
    {
        for(int j=0 ; j<pixels[i].size() ; ++j )
        {
            swap( pixels[i][j], pixels[pixels.size()-1-i][j] );
        }
    }
}

void ColorImage::swap(Pixel& p1,Pixel& p2)
{
    Pixel tmp = p1;
    p1 = p2;
    p2 = tmp;
    //finish me
}
