#ifndef COLORIMAGE_H
#define COLORIMAGE_H
#include <string>
#include "Pixel.cpp"

class ColorImage
{
public:
    ColorImage();
    virtual ~ColorImage();
    void ReadImage(string filename);//open and read image to pixels
    void OutImage(string filename);//output a image from pixels
    void Flip();//flip(½��) vertical
protected:
private:
    string magicValue;//format header
    int width,height,maxColor;//width,height,color depth
    Pixel pixels[256][128];//all pixels
    void Swap(Pixel *p1,Pixel *p2);//exchange two pixels
};

#endif // COLORIMAGE_H
