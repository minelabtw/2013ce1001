#include<iostream>
#include "ColorImage.h"
#include <fstream>                                   //用來輸入和輸出檔案
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    const char *filename_2=filename.c_str();         //轉換型態
    ifstream infile(filename_2);                     //輸入filename_2
    infile>>magicValue;                              //丟進變數
    infile>>width;
    infile>>height;
    infile>>maxColor;
    for(int i=0; i<=127; i++)
    {
        for(int k=0; k<=255; k++)                    //取像素
        {
            infile>>pixels[k][i].R;                  //丟入class pixel array 的object R G B
            infile>>pixels[k][i].G;
            infile>>pixels[k][i].B;
        }
    }
    infile.close();
}

void ColorImage::OutImage(string filename)
{
    const char *filename_3=filename.c_str();         //轉換型態
    ofstream outfile(filename_3);
    outfile<<magicValue<<endl;                       //輸出檔案
    outfile<<width<<" ";
    outfile<<height<<endl;
    outfile<<maxColor<<endl;
    for(int i=0; i<=127; i++)
    {
        for(int j=0; j<=255; j++)
        {
            outfile<<pixels[j][i].R<<" ";
            outfile<<pixels[j][i].G<<" ";
            outfile<<pixels[j][i].B<<" ";
        }
        outfile<<endl;
    }
    outfile.close();
}


void ColorImage::Flip()
{
    for(int j=0; j<=255; j++)                         //換下一列
    {
        for(int i=0; i<=63; i++)                      //將一行的上面跟下面的像素對調
            Swap(pixels[j]+i,pixels[j]+127-i);
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)            //互換
{
    Pixel t;
    t=*p1;
    *p1=*p2;
    *p2=t;
}
