#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    //finish me
    ifstream file("gulf-flounder.ppm",ios::in);//讀檔
    file>>magicValue;
    file>>width;
    file>>height;
    file>>maxColor;
    for(int j=0; j<128; j++)
        for(int i=0; i<256; i++)
            file>>pixels[i][j].R>>pixels[i][j].G>>pixels[i][j].B;//陣列紀錄

}

void ColorImage::OutImage(string filename)
{
    //finish me
    ofstream file("gulf-flounder_1.ppm",ios::out);
    file<<magicValue<<endl;
    file<<width<<endl;
    file<<height<<endl;
    file<<maxColor<<endl;
    for(int j=0; j<128; j++)
        for(int i=0; i<256; i++)
            file<<pixels[i][j].R<<" "<<pixels[i][j].G<<" "<<pixels[i][j].B<<" ";//由陣列寫入
}


void ColorImage::Flip()
{
    //finish me
    for(int j=0; j<64; j++)
        for(int i=0; i<256; i++)
            swap(pixels[i][j],pixels[i][127-j]);//互換
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    //finish me
    Pixel temp;
    temp=*p1;
    *p1=*p2;
    *p2=temp;
}
