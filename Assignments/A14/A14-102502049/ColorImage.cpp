#include<iostream>
#include "ColorImage.h"
#include <fstream>
#include <string.h>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    char schange[50];
    strcpy(schange,filename.c_str()); //string to char
    ifstream file(schange, ios::in); //load file

    int i=0; //column
    int j=0; //row
    file >> magicValue; //format header
    file >> width >> height >> maxColor;
    while (!file.eof())
    {
        int color;
        file >> color;
        switch(i%3) //分散儲存R.G.B
        {
        case 0:
            pixels[i/3][j].R=color;
            break;
        case 1:
            pixels[i/3][j].G=color;
            break;
        case 2:
            pixels[i/3][j].B=color;
            break;
        }


        if (i==255*3+2) //換行
        {
            i=0;
            j++;
        }
        else
            i++;
    }
}

void ColorImage::OutImage(string filename)
{
    char schange[50];
    strcpy(schange,filename.c_str());
    ofstream file(schange,ios::out);

    file << magicValue << endl << width << " " << height << endl << maxColor << endl; //格式
    for (int j=0; j<128; j++)
    {
        for (int i=0; i<256*3; i++)
        {
            int color;
            switch(i%3) //輸出資料
            {
            case 0:
                color=pixels[i/3][j].R;
                break;
            case 1:
                color=pixels[i/3][j].G;
                break;
            case 2:
                color=pixels[i/3][j].B;
                break;
            }
            file << color << " "; //分隔R.G.B
        }

    }

}


void ColorImage::Flip()
{
    for (int j=0; j<(128/2); j++)
    {
        for (int i=0; i<256; i++)
        {
            Swap(&pixels[i][127-j],&pixels[i][j]); //傳遞pixels
        }

    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2) //換pixels
{
    Pixel hold;

    hold=*p1;
    *p1=*p2;
    *p2=hold;
}
