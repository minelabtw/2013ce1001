#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream file("gulf-flounder.ppm",ios::in);
    file >> magicValue >> width >> height >> maxColor;              //傳入

    for ( int j = 0; j < 128; j++ )
    {
        for ( int i = 0; i < 256; i++ )
        {
            file >> pixels[i][j].R;            //裡面的R變數
            file >> pixels[i][j].G;
            file >> pixels[i][j].B;
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream outfile("gulf-flounder_1.ppm",ios::out);
    outfile << magicValue << ' ' << width << ' ' << height << ' ' << maxColor << endl;       //傳入

    for ( int j = 0; j < 128; j++ )
    {
        for ( int i = 0; i < 256; i++ )
        {
            outfile << pixels[i][j].R << ' '
                    << pixels[i][j].G << ' '
                    << pixels[i][j].B << ' ';
        }
    }
}

void ColorImage::Flip()
{
    for (int j=0; j<64; j++)               //翻轉
    {
        for (int i=0; i<256; i++)
            Swap(&pixels[i][j],&pixels[i][127-j]);       //不用& *也可
    }

    /*for (int x = 0; x < 256; x++)
    {
        for (int y = 0; y < 128/2; y++)
        {
            Swap( &pixels[y][x] , &pixels[128-y][x] );
        }
    }*/
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    swap(*p1,*p2);
}
