#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    fstream image;  //設一個檔名
    image.open( filename.c_str(), ios::in );    //讀出圖檔
    image >> magicValue >> width >> height >> maxColor;     //讀檔的結構

    for ( int k = 0; k < height; k++ )      //大小設定
    {
        for ( int l = 0; l < width; l++ )
        {
            image >> pixels[l][k].R;
            image >> pixels[l][k].G;
            image >> pixels[l][k].B;
        }
    }
    image.close();  //退出擋
}

void ColorImage::OutImage(string filename)
{
    fstream image;
    image.open( filename.c_str(), ios::out );
   image << magicValue << ' ' << width << ' ' << height << ' ' << maxColor << endl;

    for ( int j = 0; j < height; j++ )
    {
        for ( int i = 0; i < width; i++ )
        {
            image << pixels[i][j].R << ' '
             << pixels[i][j].G << ' '
             << pixels[i][j].B << ' ';
        }

    }
    image.close();
}


void ColorImage::Flip()
{
    for ( int i = 0; i < height/2; i++ )      //大小對調
    {
        for ( int j = 0; j < width; j++ )
        {
            Swap( &pixels[j][i] , &pixels[j][height-1-i] );
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
   swap(*p1,*p2);      //換圖
}
