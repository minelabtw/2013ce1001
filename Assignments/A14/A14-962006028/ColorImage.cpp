#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream putimage(&filename[0],ios::in);
    putimage >>magicValue >>width>>height>>maxColor;

    for (int j=0; j<height ; j++)
    {
        for(int i=0; i<width; i++)
        {
            putimage >> pixels[i][j].R >> pixels[i][j].G>> pixels[i][j].B ;
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream putimage(filename.c_str(),ios::out);
    putimage <<magicValue <<endl <<width<<" "<<height<<endl<<maxColor<<endl;

    for (int j=0; j<height ; j++)
    {
        for(int i=0; i<width; i++)
        {
            putimage << pixels[i][j].R <<" "<< pixels[i][j].G << " "<<pixels[i][j].B <<" ";
        }
        putimage <<endl;
    }
}


void ColorImage::Flip()
{
    for (int j=0; j< (height/2) ; j++)
    {
        for(int i=0; i<width; i++)
        {
            Swap (&pixels[i][j] , &pixels [i][height-j-1] ) ;
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{

    Pixel temp;
    temp = *p1;
    *p1 = *p2;
    *p2 = temp;

}
