#include <iostream>
#include "ColorImage.h"
#include <fstream>
#include <string>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)  //讀取檔案
{

    ifstream inClientFile(filename.c_str(), ios::in );  //把filename這個檔案讀取
    inClientFile >> magicValue >> width >> height >> maxColor;  //magicValue:P3, width:256, height:125, Color:255

    for ( int j = 0; j <  height; j++ )     //然後將圖案的像素讀出來
    {
        for ( int i = 0; i < width; i++ )
        {
            inClientFile >> pixels[i][j].R;
            inClientFile >> pixels[i][j].G;
            inClientFile >> pixels[i][j].B;
        }
    }
}

void ColorImage::OutImage(string filename) //
{
    ofstream outClientFile( filename.c_str(), ios::out); //將檔案清空然後讀取
    outClientFile << magicValue << endl << width << ' ' << height  << endl << maxColor << endl;

    for ( int j = 0; j <  height; j++ )
    {
        for ( int i = 0; i < width; i++ )
        {
            outClientFile << pixels[i][j].R << ' '
             << pixels[i][j].G << ' '
             << pixels[i][j].B << ' ';
        }
        outClientFile << endl;
    }
}


void ColorImage::Flip()  //把相片的魚翻生
{
    for ( int i = 0; i < width; i++ )
    {
        for ( int j = 0; j < height/2; j++ )// 因為上下翻
        {
             swap(pixels[i][j].R , pixels[i][height-j-1].R);

             swap(pixels[i][j].G , pixels[i][height-j-1].G);

            swap(pixels[i][j].B, pixels[i][height-j-1].B);

        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2) //掉換
{
    swap(p1,p2);
}
