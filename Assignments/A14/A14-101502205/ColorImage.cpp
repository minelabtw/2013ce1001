#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream file(filename.c_str(), ios::in);   //openfile
    file >> magicValue >> width >> height >> maxColor;  //read info
    for(int i=0; i<128; i++){
        for(int j=0; j<256; j++){
            file >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B; //pixel[width][height]
        }
    }
    file.close();   //close file
    return;
}

void ColorImage::OutImage(string filename)
{
    ofstream file(filename.c_str(), ios::out);  //openfile
    file << magicValue << " " << width << " " << height << " " << maxColor << endl; //write info
    for(int i=0; i<128; i++){
        for(int j=0; j<255; j++){
            file << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " ";    //output
        }
        file << pixels[256][i].R << " " << pixels[256][i].G << " " << pixels[256][i].B << endl; //last element with '\n'
    }
    file.close();   //close file
    return;
}


void ColorImage::Flip()
{
    for(int i=0; i<256; i++){
        for(int j=0; j<64; j++){
            Swap(&pixels[i][j], &pixels[i][127-j]); //swap 0 and 127, 1 and 126, ......
        }
    }
    return;
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel tmp;
    tmp = *p1;  //default copy constructor: bit by bit
    *p1 = *p2;
    *p2 = tmp;
    return;
}
