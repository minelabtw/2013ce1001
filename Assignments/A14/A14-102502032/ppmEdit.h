#ifndef PPMEDIT_H_INCLUDED
#define PPMEDIT_H_INCLUDED
#include <string>

//edit .ppm file
class ppmEdit
{
public:
    ppmEdit();                           //constructor
    void load(std::string);              //lode image
    void save(std::string);              //save image
    void flipV();                        //flip vertically
    void flipH();                        //flip horizonally
    void rotate90();                     //rotate 90 degrees to right
    ~ppmEdit();                          //destructor
private:
    std::string version;                 //ppm version
    int length;                          //image length
    int height;                          //image height
    int bpp;                             //image color: bits per pixel
    pixel **IMGorig;                     //original image array (dynamic)
    pixel **IMGedit;                     //edited image array (dynamic)
    int IMGeditH;                        //height of IMGedit
    int IMGeditL;                        //length of IMGedit
    void renewIMGedit(int, int);         //buile a new dynamic array named
    void Del(pixel **, int);             //delete dynamic array
    void substitute();                   //set IMGedit as oriaonal image

};

void askAndDo(ppmEdit *, string *);            //ask for function and do it

#endif // PPMEDIT_H_INCLUDED
