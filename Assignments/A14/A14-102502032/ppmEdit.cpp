#include <iostream>
#include <fstream>
#include <cstdlib>
#include "ppmEdit.h"

using namespace std;

//ask for function and do it
void askAndDo(ppmEdit &img, string *str)
{
    int type = -1;

    while (0 != type)
    {
        system("cls");
        cout << "File name: " << *str << endl
             << "Enter the number to edit the picture" << endl
             << "** Edited image won't show in file until exit program **" << endl
             << "number     function" << endl
             << "  0        exit program" << endl
             << "  1        flip vertically" << endl
             << "  2        flip horizonally" << endl
             << "  3        rotate 90 degrees to right" << endl
             << "  4        rotate 180 degrees to right" << endl
             << "  5        rotate 90 degrees to left" << endl
             << "  6        rotate 180 degrees to left" << endl
             << "Enter a number: ";

        cin >> type;

        switch (type)
        {
        case 0:
            break;
        case 1:
            img.flipV();
            break;
        case 2:
            img.flipH();
            break;
        case 3:
            img.rotate90();
            break;
        case 4:
            img.rotate90();
            img.rotate90();
            break;
        case 5:
            img.rotate90();
            img.rotate90();
            img.rotate90();
            break;
        case 6:
            img.rotate90();
            img.rotate90();
            break;
        default:
            cout << "Out of range!!" << endl;
            system("pause");
            break;
        }
    }
}

ppmEdit::ppmEdit()          //constructor
{
    version = "\0";
    height = 0;
    length = 0;
    bpp = 0;
    IMGeditH = 0;
    IMGeditL = 0;
}

void ppmEdit::load(string loadFileName)  //lode imagine
{
    //decalare ifstream
    ifstream inFile(loadFileName.c_str(), ios::in);

    //exit program if file could not be open
    if (!inFile)
    {
        cerr << "File could not be open!";
        exit(1);
    }

    inFile >> version >> length >> height >> bpp;    //get image info

    //set image array's size
    IMGorig = new pixel *[height];
    for (int i = 0; i < height; i ++)
        IMGorig[i] = new pixel [length];

    //load color data and save into image array
    for (int i = 0; i < height; i ++)
        for (int j = 0; j < length; j ++)
            inFile >> IMGorig[i][j].R >> IMGorig[i][j].G >> IMGorig[i][j].B;

    inFile.close();     //loading finished, close ifstream
}

void ppmEdit::save(string saveFileName)  //save imagine
{
    //decalare ofstream
    ofstream outFile(saveFileName.c_str(), ios::out);

    //exit program if file could not be open
    if (!outFile)
    {
        cerr << "File could not be open!";
        exit(1);
    }

    //output image info
    outFile << version << endl
            << IMGeditL << " " << IMGeditH << endl
            << bpp << endl;

    //output edited image data
    for (int i = 0; i < IMGeditH; i ++)
        for (int j = 0; j < IMGeditL; j ++)
        {
            outFile << IMGedit[i][j].R << " "
                    << IMGedit[i][j].G << " "
                    << IMGedit[i][j].B << " ";
        }

    outFile.close();        //saving finished, close ofstream

    cout << "Save complete." << endl
         << "File name: " << saveFileName << endl;
}

void ppmEdit::flipV()       //flip vertically
{
    //set edited image's data array and its size
    renewIMGedit(length, height);

    //fliping
    for (int i = 0; i < IMGeditH; i ++)
        for (int j = 0; j < IMGeditL; j ++)
            IMGedit[i][j] = IMGorig[height - i - 1][j];
    substitute();
}

void ppmEdit::flipH()       //flip horizonally
{
    //set edited image's data array and its size
    renewIMGedit(length, height);

    //fliping
    for (int i = 0; i < IMGeditH; i ++)
        for (int j = 0; j < IMGeditL; j ++)
            IMGedit[i][j] = IMGorig[i][length - 1 - j];

    substitute();
}

void ppmEdit::rotate90()    //rotate 90 degrees
{
    //set edited image's data array and its size
    renewIMGedit(height, length);

    //rotating
    for (int i = 0; i < IMGeditH; i ++)
        for (int j = 0; j < IMGeditL; j ++)
            IMGedit[i][j] = IMGorig[height - 1 - j][i];

    substitute();
}

ppmEdit::~ppmEdit()         //destructor
{
    Del(IMGorig, height);
    Del(IMGedit, IMGeditH);
}

//build a new dynamic array
void ppmEdit::renewIMGedit(int di1, int di2)  //di: dimention
{
    IMGeditL = di1;
    IMGeditH = di2;

    IMGedit = new pixel *[IMGeditH];
    for (int i = 0; i < IMGeditH; i ++)
        IMGedit[i] = new pixel [IMGeditL];
}

//delete dynamic array
void ppmEdit::Del(pixel **dyArr, int di1)
{
    for (int i = 0; i < di1; i ++)
        delete [] dyArr[i];
    delete [] dyArr;
}

//redefine IMGorig and copy data from IMGedit
//for continuelly edit
void ppmEdit::substitute()
{
    length = IMGeditL;
    height = IMGeditH;

    IMGorig = new pixel *[height];
    for (int i = 0; i < height; i ++)
        IMGorig[i] = new pixel [length];

    for (int i = 0; i < height; i ++)
        for (int j = 0; j < length; j ++)
            IMGorig[i][j] = IMGedit[i][j];
}
