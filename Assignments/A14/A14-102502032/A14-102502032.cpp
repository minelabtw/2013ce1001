#include <iostream>
#include <fstream>
#include "Pixel.cpp"
#include "ppmEdit.cpp"
using namespace std;

string filename(char);

int main()
{
    //declaration
    string inFile;
    string outFile;
    ppmEdit img;

    inFile = filename('L');
    img.load(inFile);
    askAndDo(img, &inFile);
    outFile = filename('S');
    img.save(outFile);

    return 0;
}

//ask file name for loading or saving
string filename(char ch)
{
    string name;

    cout << "Enter ""0"" to use default setting" << endl
         << "Enter the file's name you want to ";

    switch (ch)
    {
    case 'L':
        cout << "load: ";
        cin >> name;

        if (0 == name.compare("0"))
            name = "gulf-flounder.ppm";

        break;
    case 'S':
        cout << "save: ";
        cin >> name;

        if (0 == name.compare("0"))
            name = "gulf-flounder_1.ppm";

        break;
    default:
        break;
    }

    return name;
}
