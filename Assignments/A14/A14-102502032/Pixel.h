#ifndef PIXEL_H
#define PIXEL_H

//image's pixel data
struct pixel
{
public:
    pixel();        //constructor
    int R;          //red
    int G;          //green
    int B;          //blue
};

#endif // PIXEL_H
