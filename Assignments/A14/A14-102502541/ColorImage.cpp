#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    const char *name =filename.c_str();//將string 轉const char*
    ifstream image(name , ios::in);//將 filename 讀入
    image >> magicValue >> width >> height >> maxColor;
    for(int i=0; i<128; i++)
    {
        for(int j=0; j<256; j++)
        {
           image >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;
        }
    }
}

void ColorImage::OutImage(string filename)
{
    const char *name =filename.c_str();
    ofstream image( name, ios::out);
    image << magicValue << " " << width << " " << height << " " << maxColor << endl;
    for(int i=0; i<128; i++)
    {
        for(int j=0; j<256; j++)
        {
           image << pixels[j][i].R << " " << pixels[j][i].G  << " " << pixels[j][i].B << " ";
        }
    }
}
void ColorImage::Flip()
{
    for(int i=0; i<64; i++)
    {
        for(int j=0; j<256; j++)
        {
            Swap(&pixels[j][i] , &pixels[j][127-i]);
        }
    }
}
void ColorImage::Swap(Pixel *p1,Pixel *p2)//將p1 p2的內容交換
{
    int temp = p1->R;
        p1->R = p2->R;
        p2->R = temp;
        temp = p1->G;
        p1->G = p2->G;
        p2->G = temp;
        temp = p1->B;
        p1->B = p2->B;
        p2->B = temp;
}


