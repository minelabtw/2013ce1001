#include<iostream>
#include "ColorImage.cpp"
using namespace std;
int main()
{
    string filename="gulf-flounder.ppm";//input file
    string outFilename="gulf-flounder_1.ppm";//output file
    ColorImage image;//image class
    image.ReadImage(filename);//先讀檔並記錄
    image.Flip();//翻轉
    image.OutImage(outFilename);//輸出
    return 0;
}
