#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    fstream file;//建立檔案輸入輸出物件
    file.open("gulf-flounder.ppm",ios::in);//開啟檔案為讀取狀態
    if(file.is_open())
    {
        //依次寫入P3,寬,高,最大色素
        file >> magicValue >> width >> height >> maxColor ;
        //開始寫入檔案內容至Pixels陣列
        for(int i=0; i<height; i++)
            //總共有height行
            for(int j=0; j<width; j++)
            {
                //三個數字為1組，依序讀紅綠藍，1行總共有width組數字
                file >> pixels[i][j].R;
                file >> pixels[i][j].G;
                file >> pixels[i][j].B;
            }
    }
    file.close();
}

void ColorImage::OutImage(string filename)
{
    fstream file;//建立檔案輸入輸出物件
    file.open("gulf-flounder_1.ppm",ios::out);//檔案開啟為寫入狀態
    if(file.is_open())
    {
        file << magicValue << width << " " << height << endl << maxColor << endl;
        //複製P3,寬,高,最大色素
        for(int i=0; i<height; i++)//將翻轉後的內容寫入檔案
            for(int j=0; j<width; j++)
            {
                file << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
            }
            file.close();
    }
}


void ColorImage::Flip()
{
    //總共有height(128)行，
    //以中間為對稱軸，第一行(array[0])和最後一行(array[127])交換，
    //第二行和倒數第二行交換，依此類推。
    for(int i=0; i<=63; i++)
        for(int j=0; j<width; j++)
        {
            Swap(&pixels[i][j],&pixels[127-i][j]);
        }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    //顏色交換
    swap((*p1).R,(*p2).R);
    swap((*p1).B,(*p2).B);
    swap((*p1).G,(*p2).G);
}
