#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "ColorImage.h"

using namespace std;

ColorImage::ColorImage(){/* empty */}
ColorImage::~ColorImage(){/* empty */}

void ColorImage::ReadImage(string filename)
{
    //finish me
    //read image
    fstream fin(filename.c_str(), ios::in);
    fin >> magicValue >> width >> height >> maxColor; // read file info
    for (int j=0;j<height;j++)
        for (int i=0;i<width;i++)
            fin >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B; //read pixel info
    fin.close();
}

void ColorImage::OutImage(string filename)
{
    //finish me
    fstream fout(filename.c_str(), ios::out);
    // output file info
    fout << magicValue << endl;
    fout << width << " " << height << endl;
    fout << maxColor << endl;
    // output pixel info
    for (int j=0;j<height;j++)
    {
        for (int i=0;i<width;i++)
            fout << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
        fout << endl;
    }
    fout.close();
}


void ColorImage::Flip()
{
    //finish me
    // upside down
    for (int i=0;i<width;i++)
        for (int j=0;j<height-j;j++)
            Swap(&pixels[i][j], &pixels[i][height-j-1]);
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    //finish me
    // swap
    tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}
