#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream inFile( "gulf-flounder.ppm", ios::in );  //讀入檔案
    if(inFile.is_open())  //開啟檔案
    {
        inFile >> magicValue >> width >> height >> maxColor;  //將ppm檔的標頭 寬 高 像素讀入

        for (int j=0; j<128; j++)
        {
            for (int i=0; i<256; i++)
            {
                inFile >> pixels[i][j].R;  //分別將紅綠藍的像素讀入
                inFile >> pixels[i][j].G;
                inFile >> pixels[i][j].B;
            }
        }
    }
    inFile.close();  //關閉檔案
}
void ColorImage::OutImage(string filename)
{
    ofstream outFile( "gulf-flounder_1.ppm", ios::out );  //輸出檔案
    outFile << magicValue << endl << width << " " << height << endl << maxColor << endl;  //將標頭 寬 高 像素輸出到新檔案
    if(outFile.is_open())
    {
        for ( int j = 0 ; j < 128 ; j++ )
        {
            for ( int i = 0; i < 256; i++ )
            {
                outFile << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";  //將紅綠藍像素輸出到新檔案
            }
        }
    }

    outFile.close();  //關閉檔案
}
void ColorImage::Flip()
{
    for (int j=0; j<64; j++)
    {
        for (int i=0; i<256; i++)
            Swap(&pixels[i][j],&pixels[i][127-j]);  //使用Swap函式翻轉
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    swap((*p1).R,(*p2).R);  //交換紅色像素
    swap((*p1).G,(*p2).G);  //交換綠色像素
    swap((*p1).B,(*p2).B);  //交換藍色像素
}
