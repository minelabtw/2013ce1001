#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    //finish me
    fstream file("gulf-flounder.ppm",ios::in);
    file >> magicValue;
    file >> width;
    file >> height;
    file >> maxColor;
    for(int j=0;j<128;j++){
            for(int i=0;i<256;i++){
                file >> pixels[i][j].R;
                file >> pixels[i][j].G;
                file >> pixels[i][j].B;
            }
    }
    file.close();
}

void ColorImage::OutImage(string filename)
{
    //finish me
    fstream file("gulf-flounder_1.ppm",ios::out);
    file << magicValue << endl;
    file << width << " ";
    file << height << " ";
    file << maxColor << endl;
    for(int j=0;j<128;j++){
            for(int i=0;i<256;i++){
                file << pixels[i][j].R << " ";
                file << pixels[i][j].G << " ";
                file << pixels[i][j].B;
                if(i==255)file << endl;
                else file << " ";
            }
    }
    file.close();
}


void ColorImage::Flip()
{
    //finish me
    Pixel p2[256][128];
    for(int i=0;i<256;i++)Swap(pixels[i],p2[i]);
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    //finish me
    for(int j=0;j<64;j++){
        p2[j] = p1[j];
        p1[j]=p1[127-j];
        p1[127-j] = p2[j];
    }
}
