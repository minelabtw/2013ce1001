#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "ColorImage.h"

using namespace std;

ColorImage::ColorImage(){}
ColorImage::~ColorImage(){}

void ColorImage::ReadImage(string filename) 	//將檔案讀進來
{
    fstream filein(filename.c_str(), ios::in);
    filein >> magicValue >> width >> height >> maxColor;
    for (int j=0;j<height;j++)
        for (int i=0;i<width;i++)
            filein >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
    filein.close();
}
void ColorImage::OutImage(string filename)		//輸出檔案
{
    fstream fileout(filename.c_str(), ios::out);
    fileout << magicValue << endl
            << width << " " << height << endl
		    << maxColor << endl;
    for (int j=0;j<height;j++)
    {
        for (int i=0;i<width;i++)
            fileout << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";

        fileout << endl;
    }
    fileout.close();
}
void ColorImage::Flip()							//翻轉
{
    for (int i=0;i<width;i++)
        for (int j=0;j<height-j;j++)
            Swap(&pixels[i][j], &pixels[i][height-j-1]);
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)		//交換
{
    Pixel tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}
