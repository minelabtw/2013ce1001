#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream infish("gulf-flounder.ppm",ios::in);
    if(infish.is_open())
    {
        infish >> magicValue >> width >> height >> maxColor; //輸入

        for(int i=0; i<height; i++)     //存入陣列
            for(int j=0; j<width; j++)
            {
                infish >> pixels[i][j].R ;
                infish >> pixels[i][j].G ;
                infish >> pixels[i][j].B ;
            }
    }
    infish.close();
}

void ColorImage::OutImage(string filename)
{
    ofstream outfish("gulf-flounder_1.ppm",ios::out);
    if(outfish.is_open())
    {
        outfish << magicValue << endl ; //輸出
        outfish << width << " " ;
        outfish << height << endl ;
        outfish << maxColor << endl ;

        for(int i=0; i<height; i++) //輸出顏色
        {
            for(int j=0; j<width; j++)
            {
                outfish << pixels[i][j].R <<" " ;
                outfish << pixels[i][j].G <<" " ;
                outfish << pixels[i][j].B <<" " ;
            }

        }
    }
    outfish.close();
}


void ColorImage::Flip()
{
    for(int i=0; i<63; i++)
        for(int j=0; j<256; j++)
        {
            Swap(&pixels[i][j],&pixels[127-i][j]) ; //上下對稱
        }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    swap((*p1).R,(*p2).R); //交換顏色
    swap((*p1).G,(*p2).G);
    swap((*p1).B,(*p2).B);
}
