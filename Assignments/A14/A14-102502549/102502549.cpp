#include<iostream>
#include "ColorImage.cpp"
using namespace std;

int main()
{
    int num;
    string filename="gulf-flounder.ppm";//input file
    string outFilename="gulf-flounder_1.ppm";//output file
    ColorImage image;//image class
    image.ReadImage(filename);

//選擇功能
    while(true)
    {
        cout<<"functions:"<<endl;
        cout<<"1.flip vertical"<<endl;
        cout<<"2.flip horizontal"<<endl;
        cout<<"3.exit"<<endl;
        cout<<"? ";

        cin>>num;

        switch(num)
        {
        case 1:
            image.Flip();
            image.OutImage(outFilename);
            return 0;
        case 2:
            image.FlipH();
            image.OutImage(outFilename);
            return 0;
        case 3:
            return 0;
        default:
            cout<<"Out of range!"<<endl;
        }
    }
}
