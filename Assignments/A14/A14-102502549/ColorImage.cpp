#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream in(filename.c_str(),ios::in);

//將基本的那三行資訊讀入
    in>>magicValue;
    in>>width>>height;
    in>>maxColor;

//從左到右，從上到下讀入
    for(int i=0; i<128; i++)
    {
        for(int j=0; j<256; j++)
        {
            in>>pixels[i][j].R>>pixels[i][j].G>>pixels[i][j].B;
        }
    }

    in.close();

}

void ColorImage::OutImage(string filename)
{
    ofstream out(filename.c_str(),ios::out);

//將基本那三行輸出
    out<<magicValue<<" "<<endl;
    out<<width<<" "<<height<<" "<<endl;
    out<<maxColor<<" "<<endl;

//由左到右，由上到下輸出
    for(int i=0; i<128; i++)
    {
        for(int j=0; j<256; j++)
        {
            out<<pixels[i][j].R<<" "<<pixels[i][j].G<<" "<<pixels[i][j].B<<" ";
        }
    }

    out.close();
}

void ColorImage::Flip()
{
    //將每一行依序對調(第1行換第128行，第2行換第127行，以此類推)
    for(int i=0; i<64; i++)
    {
        for(int j=0; j<256; j++)
        {
            Swap(&pixels[i][j],&pixels[127-i][j]);
        }
    }
}

void ColorImage::FlipH()
{
    //將每一列依序對調(第1列換第256列，第2列換第255列，以此類推)
    for(int j=0;j<128;j++)
    {
        for(int i=0;i<128;i++)
        {
            Swap(&pixels[i][j],&pixels[i][255-j]);
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    int temp1,temp2,temp3;

    temp1=p1->R;
    temp2=p1->G;
    temp3=p1->B;

    p1->R=p2->R;
    p1->G=p2->G;
    p1->B=p2->B;

    p2->R=temp1;
    p2->G=temp2;
    p2->B=temp3;
}
