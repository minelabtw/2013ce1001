#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream in(filename.c_str(),ios::in);

    in>>magicValue;
    in>>width>>height;
    in>>maxColor;       //讀入前三行資訊

    for(int i=0; i<128; i++)
    {
        for(int j=0; j<256; j++)
        {
            in>>pixels[i][j].R>>pixels[i][j].G>>pixels[i][j].B;
        }
    }                   //讀入RGB

    in.close();

}

void ColorImage::OutImage(string filename)
{
    ofstream out(filename.c_str(),ios::out);

    out<<magicValue<<" "<<endl;
    out<<width<<" "<<height<<" "<<endl;
    out<<maxColor<<" "<<endl;   //將上面三行輸出


    for(int i=0; i<128; i++)
    {
        for(int j=0; j<256; j++)
        {
            out<<pixels[i][j].R<<" "<<pixels[i][j].G<<" "<<pixels[i][j].B<<" ";
        }
    }                           //RGB分別輸出 左到右 上到下

    out.close();
}


void ColorImage::Flip()
{

    for(int i=0; i<64; i++)
    {
        for(int j=0; j<256; j++)
        {
            Swap(&pixels[i][j],&pixels[127-i][j]);  //line 1 對應 line 128, line 2 對應 line 127
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    int temp1,temp2,temp3;

    temp1=p1->R;
    temp2=p1->G;
    temp3=p1->B;

    p1->R=p2->R;
    p1->G=p2->G;
    p1->B=p2->B;

    p2->R=temp1;
    p2->G=temp2;
    p2->B=temp3;
}
