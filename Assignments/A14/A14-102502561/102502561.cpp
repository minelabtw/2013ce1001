#include<iostream>
#include "ColorImage.cpp"
using namespace std;

int main()
{
    string filename="gulf-flounder.ppm";//input file
    string outFilename="gulf-flounder_1.ppm";//output file
    ColorImage image;//image class
    image.ReadImage(filename);

    cout << "this will flip the picture vertical" << endl;

    image.Flip();
    image.OutImage(outFilename);

    return 0;
}
