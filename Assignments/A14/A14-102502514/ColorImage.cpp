#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
   //finish me
   ifstream inClientFile( filename.c_str(), ios::in );  //ifstream第一個參數需輸入char,用string.c_str()做轉型
   inClientFile >> magicValue >> width >> height >> maxColor;  //依序輸入檔案內容
   for (int y=0; y<height; y++)
   {
       for (int x=0; x<width; x++)
       {
           inClientFile >> pixels[x][y].R >> pixels[x][y].G >> pixels[x][y].B;  //由左自右,由上自下,pixels[x][y]是"class PIXEL"的object,將檔案內容一組一組數字(ppm檔)依序輸入為R,G,B三原色
       }
   }
   inClientFile.close();  //關閉檔案
}

void ColorImage::OutImage(string filename)
{
    //finish me
    ofstream outClientFile( filename.c_str(), ios::out );  //ofstream第一個參數需輸入char,用string.c_str()做轉型
    outClientFile << magicValue << endl;  //輸出檔案需要依照實際格式,該空格就要加空格,該換行就要換行
    outClientFile << width << " " << height << endl;
    outClientFile << maxColor <<endl;
    for (int y=0; y<height; y++)
    {
        for (int x=0; x<width; x++)
        {
            outClientFile << pixels[x][y].R << " " << pixels[x][y].G << " " << pixels[x][y].B << " ";  //由左自右,由上自下,pixels[x][y]是class(類別) "Pixel"的object(物件),將檔案內容R,G,B三原色依序輸出為一組一組數字(ppm檔)
        }
    }
    outClientFile.close();  //關閉檔案
}


void ColorImage::Flip()
{
    //finish me
    for (int x=0; x<width; x++)
    {
        for (int y=0; y<height/2; y++)
        {
            Swap( &pixels[x][y], &pixels[x][height-1-y] );  //呼叫Swap函式,讓class(類別) "Pixel"底下的object(物件)做兩兩互換,也就是讓兩個二維陣列互換
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    //finish me
    Pixel hold;  //讓class(類別) "Pixel"底下的object(物件)做兩兩互換
    hold = *p1;
    *p1 = *p2;
    *p2 = hold;
}
