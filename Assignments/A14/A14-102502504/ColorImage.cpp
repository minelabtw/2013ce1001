#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)  //open and read image to pixels
{
    ifstream inClientFile( "gulf-flounder.ppm", ios::in ); //讀取一個名字叫"igulf-flounder.ppm"的檔案
    if(inClientFile.is_open()) //若inClientFile成功開啟，執行下列程式
    {
        inClientFile >> magicValue >> width >> height >> maxColor; //讀入p3 256 128 255

        for ( int j = 0; j < 128; j++ ) //將ppm的三個數值依序讀入pixel陣列
        {
            for ( int i = 0; i < 256; i++ )
            {
                inClientFile >> pixels[i][j].R;
                inClientFile >> pixels[i][j].G;
                inClientFile >> pixels[i][j].B;
            }
        }
    }
    inClientFile.close(); //關閉程式
}

void ColorImage::OutImage(string filename)  //output a image from pixels
{
    ofstream outClientFile( "gulf-flounder_1.ppm", ios::out ); //開啟一個名為"gulf-flounder_1.ppm"，可寫入的檔案
    outClientFile << magicValue << endl << width << " " << height << endl << maxColor << endl; //寫入p3 256 128 255

    if(outClientFile.is_open()) //若inClientFile成功開啟，執行下列程式
    {
        for ( int j = 0 ; j < 128 ; j++ ) //依序將pixel陣列寫入檔案
        {
            for ( int i = 0; i < 256; i++ )
            {
                outClientFile << pixels[i][j].R << " "
                              << pixels[i][j].G << " "
                              << pixels[i][j].B << " ";
            }
        }
    }

    outClientFile.close(); //關閉檔案
}


void ColorImage::Flip() //flip vertical
{
    for(int j=0; j<64; j++)
    {
        for(int i=0; i<256; i++)
        {
            Swap(&pixels[i][j],&pixels[i][127-j]) ; //將[0][0]和[0][127]、[1][0]和[1][126](以此類推)交換位置
        }
    }
}


void ColorImage::Swap(Pixel *p1,Pixel *p2) //swap two pixels
{
    swap((*p1).R,(*p2).R);
    swap((*p1).G,(*p2).G);
    swap((*p1).B,(*p2).B);
}
