#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream fin(filename); // open file

	fin >> magicValue; // read format version
	fin >> width >> height; // read width and height
	fin >> maxColor; // read deep

	for(int i = 0 ; i < height ; ++i)
	{
		for(int j = 0 ; j < width ; ++j)
			fin >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B; // read R , G , B for every pixel
	}
}

void ColorImage::OutImage(string filename)
{
    ofstream fout(filename); // open file

	fout << magicValue << endl; // write format version
	fout << width << " " << height << endl; // write width and height
	fout << maxColor << endl;// write deep

	for(int i = 0 ; i < height ; ++i)
	{
		for(int j = 0 ; j < width ; ++j)
			fout << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " "; // write R , G , B for every pixel
		fout << endl;
	}
}


void ColorImage::Flip()
{
    for(int i = 0 ; i < height / 2 ; ++i)
	{
		for(int j = 0 ; j < width ; ++j)
			Swap(&pixels[j][i] , &pixels[j][height - i - 1]);
	}
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    int r = p2 -> R;
	int g = p2 -> G;
	int b = p2 -> B;

	// swap p1 to p2
	p2 -> R = p1 -> R;
	p2 -> G = p1 -> G;
	p2 -> B = p1 -> B;

	// swap p2 to p1
	p1 -> R = r;
	p1 -> G = g;
	p1 -> B = b;
}
