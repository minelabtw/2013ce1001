#include<iostream>
#include "ColorImage.h"
#include <fstream>

using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream fin(filename.c_str(),ios::in);//讀取檔案
    fin >> magicValue >> width >> height >> maxColor;//儲存前面的參數
    for(int i=0;i<height;i++)//儲存各個像素的R,G,B值
    {
        for(int j=0;j<width;j++)
        {
            fin >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;
        }
    }
    fin.close();
}

void ColorImage::OutImage(string filename)
{
    ofstream fout(filename.c_str(),ios::out);//輸出檔案
    fout << magicValue << endl;//輸出前面的參數
    fout << width << " " << height << endl;
    fout << maxColor << endl;
    for(int i=0;i<height;i++)//輸出各個像數的R,G,B的值
    {
        for(int j=0;j<width;j++)
        {
            fout << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " ";
        }
        fout << endl;
    }
    fout.close();
}

void ColorImage::Flip()
{
    for (int i=0;i<height-i;i++)//讓第一行與最後一行交換在換第二行與倒數第二行交換以此類推
    {
        for (int j=0;j<width;j++)
        {
            Swap(&pixels[j][i],&pixels[j][height-i-1]);
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel p3 = *p1;//先用p3暫存p1再把p1和p2交換
    *p1 = *p2;
    *p2 = p3;
}
