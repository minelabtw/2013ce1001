#include<iostream>
#include "ColorImage.h"
#include<fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename) //讀圖
{
    ifstream in(filename.c_str(), ios::in);
    in >> magicValue;
    in >> width >> height >> maxColor;
    for(int i=0; i<height; i++) //對每一點的顏色做分析
    {
        for(int j=0; j<width; j++)
            in >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
    }
}

void ColorImage::OutImage(string filename) //輸出一個新圖檔
{
    ofstream Out(filename.c_str(), ios::out);
    Out << magicValue << endl; //格式
    Out << width << " " << height << endl; //長寬
    Out << maxColor << endl;  //顏色
    for (int i=0; i<height; i++) //對每一點顏色分析
    {
        for(int j=0; j<width; j++)
            Out << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";

        Out << endl;
    }
}

void ColorImage::Flip() //將圖做翻轉
{
    for (int j=0; j<width; j++)
    {
        for (int i=0; i<height/2; i++)
            Swap(&pixels[i][j], &pixels[height-i-1][j]);
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    int change1=(*p1).R;
    int change2=(*p1).G;
    int change3=(*p1).B;

    (*p1).R=(*p2).R;
    (*p1).G=(*p2).G;
    (*p1).B=(*p2).B;

    (*p2).R=change1;
    (*p2).G=change2;
    (*p2).B=change3;
}
