#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream inFile( "gulf-flounder.ppm", ios::in );
    if(inFile.is_open())
    {
        inFile >> magicValue >> width >> height >> maxColor;

        for (int j=0; j<128; j++)
        {
            for (int i=0; i<256; i++)
            {
                inFile >> pixels[i][j].R;
                inFile >> pixels[i][j].G;
                inFile >> pixels[i][j].B;
            }
        }
    }
    inFile.close();
}
void ColorImage::OutImage(string filename)
{
    ofstream outFile( "gulf-flounder_1.ppm", ios::out );
    outFile << magicValue << endl << width << " " << height << endl << maxColor << endl;
    if(outFile.is_open())
    {
        for ( int j = 0 ; j < 128 ; j++ )
        {
            for ( int i = 0; i < 256; i++ )
            {
                outFile << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
            }
        }
    }

    outFile.close();
}
void ColorImage::Flip()
{
    for (int j=0; j<64; j++)
    {
        for (int i=0; i<256; i++)
            Swap(&pixels[i][j],&pixels[i][127-j]);
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    swap((*p1).R,(*p2).R);
    swap((*p1).B,(*p2).B);
    swap((*p1).G,(*p2).G);
}
