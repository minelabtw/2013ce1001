#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(char* filename)
{
    ifstream read_fish(filename,ios::in);//設定讀檔名稱

    read_fish>>magicValue>>width>>height>>maxColor;//存取特殊數字

    while(read_fish)//將整條魚吃進來
    {
        for (int i=0; i<height; i++)
        {
            for(int j=0; j<width; j++)
            {
                read_fish>>pixels[j][i].R >> pixels[j][i].G>> pixels[j][i].B ;
            }
        }
    }
}

void ColorImage::OutImage(char* filename)
{
    ofstream output_fish(filename,ios::out);//設定寫檔名稱

    output_fish<<magicValue<<" "<<width<<" "<<height<<" "<<maxColor<<" ";//輸出格式

    for (int i=0; i<height; i++)//將魚畫出來
    {
        for(int j=0; j<width; j++)
        {
            output_fish<<pixels[j][i].R<<" "<< pixels[j][i].G<<" "<<pixels[j][i].B<<" " ;
        }
    }
}


void ColorImage::Flip()
{
    int k =127;
    for (int i=0; i<height/2; i++,k--)//把魚翻過來
    {
        for (int j=0 ; j<width ; j++)
        {
            Swap(&pixels[j][i],&pixels[j][k]);
        }
    }
}


void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    swap(*p1,*p2);//交換像素
}
