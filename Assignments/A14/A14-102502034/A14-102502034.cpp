#include<iostream>
#include "ColorImage.cpp"
using namespace std;

int main()
{
    char *filename="gulf-flounder.ppm";//input file
    char *outFilename="gulf-flounder_1.ppm";//output file
    ColorImage image;//image class
    image.ReadImage(filename);
    image.Flip();
    image.OutImage(outFilename);

    return 0;
}
