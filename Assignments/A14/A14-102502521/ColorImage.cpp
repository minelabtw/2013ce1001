#include<iostream>
#include "ColorImage.h"
#include <fstream>
#include <cstdlib>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)    //讀圖片進陣列中
{
    ifstream rimg(filename.c_str(),ios::in);
    if(!rimg)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }

    rimg>>magicValue>>width>>height>>maxColor;

    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            rimg>>pixels[w][h].R>>pixels[w][h].G>>pixels[w][h].B;
        }
    }
}

void ColorImage::OutImage(string filename)    //從陣列存取圖片
{
    ofstream oimg(filename.c_str(),ios::out);
    if(!oimg)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }

    oimg<<magicValue<<endl;
    oimg<<width<<" "<<height<<endl;
    oimg<<maxColor<<endl;

    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            oimg<<pixels[w][h].R<<" "<<pixels[w][h].G<<" "<<pixels[w][h].B<<" ";
        }
        oimg<<endl;
    }
}

void ColorImage::Flip()    //翻轉圖片
{
    for(int w=0;w<width;w++)
    {
        int lasth=height-1;
        for(int h=0;lasth>h;h++)
        {
            Swap(&pixels[w][h],&pixels[w][lasth]);
            lasth--;
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)    //上下交換
{
    Pixel x;
    x=*p2;
    *p2=*p1;
    *p1=x;
}
