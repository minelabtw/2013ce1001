#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    //finish me
    const char *ch = filename.c_str();//先轉存char
    int i=0;//宣告變數並=0
    int j=0;
    ifstream inClientFile (ch,ios::in);//讀入檔案
    if (inClientFile >> magicValue >> width >> height >> maxColor)
    {
        while (i <height)//重複行
        {
            while (j <width)//重複列
            {
                inClientFile >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;
                j++;
            }
            j=0;
            i++;
        }
    }
    else
    {
        cerr << "File could not be open";
    }
}

void ColorImage::OutImage(string filename)
{
    //finish me
    const char *outch = filename.c_str();//先轉存char
    int i =0;//宣告變數並=0
    int j =0;
    ofstream outClientFile (outch,ios::out);//輸出檔案
    outClientFile << magicValue << endl << width << " " << height << endl << maxColor << endl;//輸出檔案格式
    while (i <height)//重複行
    {
        while (j <width)//重複列
        {
            outClientFile << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " ";//輸出顏色
            j++;
        }
        j=0;
        i++;
    }
}


void ColorImage::Flip()
{
    //finish me
    for (int i =0; i <(height /2); i++)
    {
        for (int j =0; j <width; j++)
        {
            Swap(&pixels[j][i],&pixels[j][height -i -1]);//兩兩翻轉
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    //finish me
    Pixel temp;
    temp =*p1;//先轉存
    *p1 =*p2;//再覆蓋
    *p2 =temp;//再回來
}
