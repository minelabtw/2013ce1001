#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    int i,j;
    ifstream in ( filename.c_str() );//ifstream不能讀入string ，所以要用.c_str變成陣列
    in >> magicValue >> width >> height >> maxColor;//in是一個物件
    for (j=0; j<128; j++)
    {
        for (i=0; i<256; i++)
        {
         in >> pixels[i][j].R >> pixels[i][j].G >>  pixels[i][j].B ;//要去ColorImage找到Pixel pixels[256][128]，再去Pixel.cpp找R,G或B
        }
    }
}

void ColorImage::OutImage(string filename)
{
    int i,j;
    ofstream in ( filename.c_str() );//ifstream不能讀入string ，所以要用.c_str變成陣列
    in << magicValue << endl <<  width << " " << height << endl << maxColor << endl;//記
    for (j=0; j<128; j++)
    {
        for (i=0; i<256; i++)
        {
            in << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";//要去ColorImage找到Pixel pixels[256][128]，再去Pixel.cpp找R,G或B，還有 別忘了空格!!否則格式會不對
        }
    }
}
void ColorImage::Flip()
{
    int j,x,y,t;
    for (x=0,y=255; x+y==255&&x<128; x++,y--)
    {
        for (j=0; j<128; j++)
        {
            Pixel px;//一個pixel.h檔內的變數，作用是作為互換的暫存
        px=pixels[x][j];
        pixels[x][j]=pixels[y][j];
        pixels[y][j]=px;
        }
    }

}
