#include<iostream>
#include "ColorImage.cpp"
using namespace std;

int main()
{
    string filename="gulf-flounder.ppm";//input file
    string outFilename="gulf-flounder_1.ppm";//output file
    ColorImage image;//image class
    image.ReadImage(filename);//這個資料夾都是一個專案 所以可以這樣用＝＝
    image.Flip();
    image.OutImage(outFilename);
    return 0;
}
