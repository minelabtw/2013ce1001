#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    fstream op(filename.c_str(),ios::in);
    op>>magicValue>>width>>height>>maxColor;  //讀出基本資料
    for(int y=0; y<height; y++)
    {
        for(int x=0; x<width; x++)
        {
            op>>pixels[x][y].R>>pixels[x][y].G>>pixels[x][y].B;  //讀出圖案
        }
    }
    op.close();  //關閉檔案
}

void ColorImage::OutImage(string filename)
{
    fstream file(filename.c_str(),ios::out);  //打開檔案
    file<<magicValue<<endl;  //輸出開頭檔
    file<<width<<" "<<height<<endl;  //輸出寬 高
    file<<maxColor<<endl;  //輸出最顏色深度
    for(int y=0; y<height; y++) //輸出圖案
    {
        for(int x=0; x<width; x++)
        {
            file<<pixels[x][y].R<<" "<<pixels[x][y].G<<" "<<pixels[x][y].B<<" ";
        }
    }
    file.close();  //關閉檔案
}


void ColorImage::Flip()
{
    int z=height-1;
    for(int y=0; y<height/2; y++) //顛倒圖片
    {
        for(int x=0; x<width; x++)
        {
            Swap(&pixels[x][y],&pixels[x][z]);
        }
        z--;
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{

    swap(*p1,*p2);  //交換
}
