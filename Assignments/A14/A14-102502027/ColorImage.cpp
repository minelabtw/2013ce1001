#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}
void ColorImage::ReadImage(string filename) //讀取圖片
{
	ifstream fin;
	fin.open(filename.c_str(),ios::in);
	fin >> magicValue >> width >> height >> maxColor;
	for(int i = 0 ; i < width ; i++){
		for(int j = 0 ; j < height ; j++){
			fin >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
		}
	}
	fin.close();

}

void ColorImage::OutImage(string filename) //輸出圖片
{
	ofstream fout;
	fout.open(filename.c_str());
	fout << magicValue << endl << width << " " << height << endl << maxColor << endl;
	for(int i = 0 ; i < width ; i++){
		for(int j = 0 ; j < height ; j++){
			fout << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
		}
		fout << "\n";
	}
	fout.close();
}

void ColorImage::Flip() // 上下顛倒
{
	for(int i = 0 ; i < width ; i++){
			for(int j = 0 ; j < height/2 ; j++){
					Swap(&pixels[i][j],&pixels[width-i-1][height-j-1]);
			}
		}

}

void ColorImage::Swap(Pixel *p1,Pixel *p2) //互換P1,P2位置
{
	Pixel temp;
	temp = *p1;
	*p1 = *p2;
	*p2=temp;

}
