#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename) //讀檔
{
    ifstream read (filename.c_str());
    read>>magicValue;
    read>>width>>height>>maxColor;
    int i=0;
    int j=0;
    for(i=0; i<height; i++)
    {
        for(j=0; j<width; j++)
        {
            read>>pixels[i][j].R>>pixels[i][j].G>>pixels[i][j].B;
        }
    }
}

void ColorImage::OutImage(string filename) //輸出
{
    ofstream write (filename.c_str());
    write<<magicValue<<endl;
    write<<width<<" "<<height<<endl;
    write<<maxColor<<endl;
    int i=0;
    int j=0;
    for(i=0; i<height ; i++)
    {
        for(j=0; j<width; j++)
        {
            write<<pixels[i][j].R<<" "<<pixels[i][j].G<<" "<<pixels[i][j].B<<" ";
        }
        write<<endl;
    }
}

void ColorImage::Flip() //從新排序矩陣
{
    int i=0;
    int j=0;
    for(i=0; i<height/2; i++)
    {
        for(j=0; j<width; j++)
        {
            Swap(&pixels[i][j],&pixels[height-1-i][j]);
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2) //交換上下的顏色
{
    Pixel tmp;
    tmp.R=(*p1).R;
    tmp.G=(*p1).G;
    tmp.B=(*p1).B;
    (*p1).R=(*p2).R;
    (*p1).G=(*p2).G;
    (*p1).B=(*p2).B;
    (*p2).R=tmp.R;
    (*p2).G=tmp.G;
    (*p2).B=tmp.B;
}
