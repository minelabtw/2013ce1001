#include<iostream>
#include "ColorImage.h"
#include <fstream>
#include <cstdlib>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)  //檔案存入
{
    ifstream inClientFile( filename.c_str(), ios::in);

    if (!inClientFile)
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }

    inClientFile >> magicValue >> width >> height >> maxColor ;
    for (int x=0; x<256 ; x++)  //用二維陣列存入色彩數字
    {
        for (int y=0; y<128; y++)
        {
            inClientFile >> pixels[x][y].R;
            inClientFile >> pixels[x][y].G;
            inClientFile >> pixels[x][y].B;
        }
    }
    inClientFile.close();
}

void ColorImage::OutImage(string outfilename)  //檔案輸出
{
    ofstream outClientFile( outfilename.c_str(), ios::out);
    outClientFile << magicValue << endl;
    outClientFile << width << " " << height << endl;
    outClientFile << maxColor << endl;

    for (int x=0; x<256; x++)
    {
        for (int y=0; y<128; y++)
        {
            outClientFile << pixels[x][y].R << " ";
            outClientFile << pixels[x][y].G << " ";
            outClientFile << pixels[x][y].B << " ";
        }
    }
    outClientFile.close();
}


void ColorImage::Flip()  //找出應相互調的色彩位置(圖形對稱x軸)
{
    for (int x=0; x<128; x++)
    {
        for (int y=0; y<64; y++)
        {
            Swap(&pixels[y][x],&pixels[128-y][x]);
        }
    }
}

void ColorImage::Swap(Pixel *s1,Pixel *s2)  //交換所相應的位置
{
    Pixel hold;
    hold=*s1;
    *s1=*s2;
    *s2=hold;
}
