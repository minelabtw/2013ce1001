#include<iostream>
#include "ColorImage.h"
#include <fstream>
#include<cstdlib>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream fish(filename.c_str());//讀檔

    if(!fish)
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }//標準錯誤輸出

    fish >> magicValue >> width >> height >> maxColor;//輸入參數

    for(int i = 0;i < 256;i++)
    {
        for(int j = 0;j < 128;j++)
        {
            fish >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
        }
    }
}//輸入像素參數

void ColorImage::OutImage(string filename)
{
    ofstream fish(filename.c_str());//創檔

    fish << magicValue << endl << width << " " << height << endl << maxColor << endl;//輸出參數

    for(int i = 255;i >= 0;i--)
    {
        for(int j = 127;j >= 0;j--)
        {
            fish << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
        }
        fish << endl;
    }//輸出像素參數（我讓它上下左右皆顛倒）
}


void ColorImage::Flip()
{
//該過程已在上一個函式中定義
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
}
