#include<iostream>
#include "ColorImage.cpp"
using namespace std;

int main()
{
    string filename="gulf-flounder.ppm";//input file
    string outFilename="gulf-flounder_1.ppm";//output file
    ColorImage image;//image class
//flip vertically
    image.ReadImage(filename);
    image.Flip();
    image.OutImage(outFilename);
//flip hrizontally
    image.ReadImage(filename);
    image.hFlip();
    image.OutImage("gulf-flounder_2.ppm");
//negative effect
    image.ReadImage(filename);
    image.Negative();
    image.OutImage("gulf-flounder_3.ppm");
//change bright, in this case, darker
    image.ReadImage(filename);
    image.Bright(-100);
    image.OutImage("gulf-flounder_4.ppm");
//black and white effect
    image.ReadImage(filename);
    image.BnW();
    image.OutImage("gulf-flounder_5.ppm");
//rotate, 90=-270, 180=-180 or 270=-90 degree
    image.ReadImage(filename);
    image.Rotate(180);
    image.OutImage("gulf-flounder_6.ppm");
    return 0;
}
