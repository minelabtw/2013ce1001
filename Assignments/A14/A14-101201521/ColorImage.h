#ifndef COLORIMAGE_H
#define COLORIMAGE_H
#include <string>
#include "Pixel.cpp"
void mode0f(const int, const int, const int, const int, int []);//mode0 for rotate 90=-270 degree
void mode1f(const int, const int, const int, const int, int []);//mode1 for rotate 180=-180 degree
void mode2f(const int, const int, const int, const int, int []);//mode2 for rotate 270=-90 degree
class ColorImage
{
public:
    ColorImage();
    virtual ~ColorImage();
    void ReadImage(string filename);//open and read image to pixels
    void OutImage(string filename);//output a image from pixels
    void Flip();//flip vertical
    void hFlip();//flip horizontal
    void Negative();//negative film
    void Bright(int);//chag
    void BnW();//black and white
    void Rotate(int);//rotate 90=-270, 180=-180 or 270=-90

protected:
private:
    string magicValue;//format header
    int width,height,maxColor;//width,height,color depth
    Pixel pixels[256][256];//all pixels
    void Swap(Pixel *p1,Pixel *p2);//swap two pixels
};

#endif // COLORIMAGE_H
