#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
    //initializing
    magicValue = "";
    width = height = maxColor = 0;
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream file(filename.c_str(), ios::in);
    //get in the data in the file in the format of .ppm
    file >> magicValue >> width >> height >> maxColor;
    for(int i=0; i<height; i++)
        for(int j=0; j<width; j++)
            file >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;
}

void ColorImage::OutImage(string filename)
{
    ofstream file(filename.c_str(), ios::out);
    //out put the data to the file in the format of .ppm
    file << magicValue << endl;
    file << width << " " << height << endl;
    file << maxColor << endl;
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
        {
            file << pixels[j][i].R << " "
                << pixels[j][i].G << " "
                << pixels[j][i].B << " ";
        }
        file << endl;
    }
}


void ColorImage::Flip()
{
    int mid=(height)/2;// calculate the upper bound of height to swap, which is close to the midern of height
    for(int i=0; i<mid; i++)
        for(int j=0; j<width; j++)
            Swap(&pixels[j][i], &pixels[j][height-1-i]);
}

void ColorImage::hFlip()
{
    int mid=(width)/2;// calculate the upper bound of width to swap, which is close to the midern of width
    for(int i=0; i<height; i++)
        for(int j=0; j<mid; j++)
            Swap(&pixels[j][i], &pixels[width-1-j][i]);
}


void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel hold;// to store *p1 temporately
    hold=*p1;
    *p1=*p2;
    *p2=hold;
}

void ColorImage::Negative()
{
    for(int i=0; i<height; i++)
        for(int j=0; j<width; j++)
        {
            //change colors to the invers colors respectively
            pixels[j][i].R=maxColor-pixels[j][i].R;
            pixels[j][i].G=maxColor-pixels[j][i].G;
            pixels[j][i].B=maxColor-pixels[j][i].B;
        }
}

void ColorImage::Bright(int level)
{
    for(int i=0; i<height; i++)
        for(int j=0; j<width; j++)
        {
            //R, G and B adding an signed integer at the same time, and check whether the color is out of range
            //if the color is (higher / lower) than maxcolor, than assing the (max color in the range / 0) to it
            pixels[j][i].R=(pixels[j][i].R+level>maxColor ? maxColor : (pixels[j][i].R+level<0 ? 0 : pixels[j][i].R+level));
            pixels[j][i].G=(pixels[j][i].G+level>maxColor ? maxColor : (pixels[j][i].G+level<0 ? 0 : pixels[j][i].G+level));
            pixels[j][i].B=(pixels[j][i].B+level>maxColor ? maxColor : (pixels[j][i].B+level<0 ? 0 : pixels[j][i].B+level));
        }
}

void ColorImage::BnW()
{
    for(int i=0; i<height; i++)
        for(int j=0; j<width; j++)
            pixels[j][i].R = pixels[j][i].G = pixels[j][i].B = (pixels[j][i].R+pixels[j][i].G+pixels[j][i].B)/3;
            //calculate the average of R, G and B of the current pixel, and assign it back to R, G and B
}

void ColorImage::Rotate(int degree)
{
    void (*modef)(const int, const int, const int, const int, int []);//function pointer pointing to mode functions
    Pixel newpixels[256][256];//pixels of new image
    int mode=0, newheight=0, newwidth=0;// mode number, new height, new width
    int hw[2]={};//{mode width, mode height}
//setting modes
    if(degree==90 || degree==-270)
        mode=0;
    else if(degree==180 || degree==-180)
        mode=1;
    else if(degree==270 || degree==-90)
        mode=2;
    else
    {
        cerr << "Out of range!" << endl;
        return;
    }
//assigning mode function and new height and width
    if(mode==0)
    {
        modef=mode0f;
        newheight=width, newwidth=height;
    }
    else if(mode==2)
    {
        modef=mode2f;
        newheight=width, newwidth=height;
    }
    else
    {
        modef=mode1f;
        newheight=height, newwidth=width;
    }
//storing new pixels
    for(int i=0; i<height; i++)
        for(int j=0; j<width; j++)
        {
            (*modef)(height, width, i, j, hw);
            newpixels[hw[0]][hw[1]]=pixels[j][i];
        }
//assigning new pixels to the pixels of the object
    for(int i=0; i<newheight; i++)
        for(int j=0; j<newwidth; j++)
            pixels[j][i]=newpixels[j][i];
    width=newwidth, height=newheight;
}
void mode0f(const int h, const int w, const int i, const int j, int hw[])
{
    hw[0]=h-1-i, hw[1]=j;//change the index of newpixels
}
void mode2f(const int h, const int w, const int i, const int j, int hw[])
{
    hw[0]=i, hw[1]=w-1-j;//change the index of newpixels
}
void mode1f(const int h, const int w, const int i, const int j, int hw[])
{
    hw[0]=w-1-j, hw[1]=h-1-i;//change the index of newpixels
}
