#ifndef PIXEL_H
#define PIXEL_H

class Pixel
{
public:
    Pixel();
    ~Pixel();
    int R, G, B;  // Blue, Green, Red
protected:
private:
};

#endif // PIXEL_H
