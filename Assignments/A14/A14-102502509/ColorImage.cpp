#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream infile( filename.c_str(), ios::in ); // 字串轉換(c_str)
    infile >> magicValue >> width >> height >> maxColor;

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++) // 注意讀取方向
        {
            infile >> pixels[j][i].R;
            infile >> pixels[j][i].G;
            infile >> pixels[j][i].B;
        }
    }
    infile.close();
}

void ColorImage::OutImage(string filename)
{
    ofstream outfile( filename.c_str(), ios::out );
    outfile << magicValue << endl
            << width << " " << height << endl
            << maxColor << endl;

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            outfile << pixels[j][i].R << " ";
            outfile << pixels[j][i].G << " ";
            outfile << pixels[j][i].B << " ";
        }
    }
    outfile.close();
}


void ColorImage::Flip()
{
    for (int i = 0; i < height/2; i++)// 上下作對稱
    {
        for (int j = 0; j < width; j++)
        {
            Swap (&pixels[j][i] , &pixels[j][height-1-i]); // 位置
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)// 轉換座標
{
    Pixel hold;
    hold = *p1;
    *p1 = *p2;
    *p2 = hold;
}
