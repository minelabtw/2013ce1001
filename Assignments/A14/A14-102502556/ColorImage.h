#ifndef COLORIMAGE_H
#define COLORIMAGE_H
#include <string>
#include "Pixel.cpp"

class ColorImage
{
public:
    ColorImage();
    virtual ~ColorImage();
    void ReadImage(string filename);//open and read image to pixels
    void OutImage(string filename);//output a image from pixels
    void Flip();//flip vertical
protected:
private:
    string magicValue;//代表格式的版本
    int width,height,maxColor;//寬,高,位元深度
    Pixel pixels[256][128];//全部的像素點
    void Swap(Pixel *p1,Pixel *p2);//將兩個像素點互換
};

#endif // COLORIMAGE_H
