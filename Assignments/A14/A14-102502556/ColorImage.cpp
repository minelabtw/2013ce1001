#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    //finish me
    ifstream in ( filename.c_str() ); //用 ifstream 把原始的圖片檔讀入
    in >> magicValue >> width >> height >> maxColor; //讀取檔案格式、寬、高、位元深度的值
    for ( int y = 0 ; y < 128 ; y++ ) //使用for迴圈讀取圖檔中每個像素點的值(R,G,B)
    {
        for ( int x = 0 ; x < 256 ; x++ )
        {
            in >> pixels[x][y].R >> pixels[x][y].G >> pixels[x][y].B;
        }
    }
    in.close(); //關閉檔案
}

void ColorImage::OutImage(string filename)
{
    //finish me
    ofstream out ( filename.c_str() ); //用 ofstream 把翻轉後的結果寫入新的圖片檔
    out << magicValue << endl; //寫入檔案格式
    out << width << " " << height << endl; //寫入寬、高
    out << maxColor << endl; //寫入位元深度
    for ( int y = 0 ; y < 128 ; y++ ) //用for迴圈寫入新的圖檔中每個像素點的值(R,G,B)
    {
        for ( int x = 0 ; x < 256 ; x++ )
        {
            out << pixels[x][y].R << " " << pixels[x][y].G << " " << pixels[x][y].B << " ";
        }
        out << endl;
    }
    out.close(); //關閉檔案
}


void ColorImage::Flip() //使用for迴圈進行垂直翻轉的動作
{
    //finish me
    for ( int i = 0 ; i < 64 ; i++ )
    {
        for ( int j = 0 ; j < 256 ; j++ )
        {
            swap ( pixels[j][i] , pixels[j][127-i] );
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2) //將像素點的值作交換
{
    //finish me
    Pixel *pt;
    pt = p2;
    p2 = p1;
    p1 = pt;
}
