#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream Read("gulf-flounder.ppm",ios::in);//讀gulf-flounder.ppm
    Read>>magicValue>>width>>height>>maxColor;//讀取格式、寬,高,位元深度
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
        {
            Read>>pixels[j][i].R>>pixels[j][i].G>>pixels[j][i].B;//讀像素
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream Out("gulf-flounder_1.ppm",ios::out);//輸出gulf-flounder_1.ppm
    Out<<magicValue<<' '<<endl<<width<<' '<<height<<' '<<endl<<maxColor<<' '<<endl;//輸出格式、寬,高,位元深度
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
        {
            Out<<pixels[j][i].R<<' '<<pixels[j][i].G<<' '<<pixels[j][i].B<<' ';//輸出像素
        }
    }
}

void ColorImage::Flip()//翻轉
{
    for ( int j = 0; j < width; j++ )
    {
        for ( int i = 0; i < height/2; i++ )
        {
            Swap( &pixels[j][i] , &pixels[j][height-1-i] );
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    swap(*p1,*p2);////將兩個像素點互換
}
