#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream inclientfile(filename.c_str()) ; //讀檔
    inclientfile >> magicValue >> width >> height >> maxColor ;

    for (int j=0;j<128;j++)
    {
        for (int i=0;i<256;i++)
        {
            inclientfile >> pixels[i][j].R >>  pixels[i][j].G >> pixels[i][j].B ;
        }
    }
    inclientfile.close();  //關閉檔案
}

void ColorImage::OutImage(string filename)
{
    ofstream outclientfile(filename.c_str()) ; //將檔案輸出出來

    outclientfile << magicValue << endl << width << " " << height << endl << maxColor << endl ;

    for (int j=0;j<128;j++)
    {
        for (int i=0;i<256;i++)
        {
            outclientfile << pixels[i][j].R << " " <<  pixels[i][j].G << " " << pixels[i][j].B << " " ;
        }
    }
    outclientfile.close() ;
}


void ColorImage::Flip() //翻轉他
{
    for (int y=0;y<64;y++)
    {
        for (int x=0;x<256;x++)
        {
            Swap(&pixels[x][y],&pixels[x][127-y]) ;
        }
    }

}

void ColorImage::Swap(Pixel *p1,Pixel *p2) //將兩數交換
{
    Pixel hello = *p1 ;
    *p1 = *p2 ;
    *p2 = hello ;

}
