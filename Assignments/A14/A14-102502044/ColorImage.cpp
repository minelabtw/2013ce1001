#include <stdio.h>
#include "ColorImage.h"

ColorImage::ColorImage(){}

ColorImage::~ColorImage(){}

void ColorImage::ReadImage(const char *filename)
{
	/* input file point */
	static FILE *fin = fopen(filename, "r");
	
	/* input information */
	fscanf(fin, "%s%d%d%d", magicValue, &width, &height, &maxColor);

	/* input photo */
	for(int i=0 ; i<height ; i++)
		for(int j=0 ; j<width ; j++)
			/* input RGB in pixels[i][j] */
			fscanf(fin, "%d%d%d", &pixels[i][j].R, &pixels[i][j].G, &pixels[i][j].B);

	/* close file */
	fclose(fin);
}

void ColorImage::OutImage(const char *filename)
{
	/* output file point */
	static FILE *fout = fopen(filename, "w+");

	/* output information */
	fprintf(fout, "%s\n%d %d\n%d\n", magicValue, width, height, maxColor);

	/* output photo */
	for(int i=0 ; i<height ; i++)
	{
		for(int j=0 ; j<width ; j++)
			/* output RGB in pixels[i][j] */
				fprintf(fout, "%d %d %d ", pixels[i][j].R, pixels[i][j].G, pixels[i][j].B);
		fprintf(fout, "\n");
	}

	/* close file */		
	fclose(fout);
}


void ColorImage::Flip()
{
	for(int i=0 ; i<height-i ; i++)
		for(int j=0 ; j<width ; j++)
			/* swap pixel */
			Swap(&pixels[i][j], &pixels[height-i-1][j]);
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
	/* swap value in p1 p2 */
	Pixel tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}
