#include <stdio.h>
#include "ColorImage.cpp"

int main()
{
    ColorImage image;//image class
    image.ReadImage("gulf-flounder.ppm");//input file
    image.Flip();
    image.OutImage("gulf-flounder_1.ppm");//output file
    return 0;
}
