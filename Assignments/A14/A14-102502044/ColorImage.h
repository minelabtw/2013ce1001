#ifndef COLORIMAGE_H
#define COLORIMAGE_H
#include "Pixel.cpp"

class ColorImage
{
public:
    ColorImage();
    virtual ~ColorImage();
    void ReadImage(const char *filename);//open and read image to pixels
    void OutImage(const char *filename);//output a image from pixels
    void Flip();//flip vertical
protected:
private:
    char magicValue[10];//version of format
    int width,height,maxColor;
    Pixel pixels[128][256];//save pixels in a 2D array
    void Swap(Pixel *p1,Pixel *p2);//swap p1 p2
};

#endif // COLORIMAGE_H
