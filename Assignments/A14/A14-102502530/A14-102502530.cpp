#include<iostream>
#include<fstream>
#include<string>
class pixel
{
public:
   int r,g,b;
   void set(int,int,int);
};
void pixel::set(int R,int G,int B)
{
   r=R;
   g=G;
   b=B;
}
int main()
{
   std::string mode;
   int height,width,depth,R,G,B;
   pixel image[200][300];

   std::fstream in("gulf-flounder.ppm",std::fstream::in);
   std::fstream out("gulf-flounder_1.ppm",std::fstream::out);

   in>>mode>>width>>height>>depth;
   for(int i=0;i!=height;i++)
      for(int j=0;j!=width;j++)
      {
         in>>R>>G>>B;
         image[i][j].set(R,G,B);
      }

   in.close();

   out<<mode<<std::endl<<width<<' '<<height<<std::endl<<depth<<std::endl;
   for(int i=height-1;i>=0;i--)
      for(int j=0;j!=width;j++)
         out<<image[i][j].r<<' '<<image[i][j].g<<' '<<image[i][j].b<<' ';

   out.close();

   return 0;
}
