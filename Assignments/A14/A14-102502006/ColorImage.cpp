#include<iostream>
#include <cstdlib>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream input("gulf-flounder.ppm",ios::in); // 開啟檔案
    if(!input)
    {
        cout << "Error!" << endl;
        exit(1);
    }

    input >> magicValue >> width >> height >> maxColor;
    for(int i=0; i<height; i++)for(int j=0; j<width; j++)input >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B; // 存入RGB

    input.close();
}

void ColorImage::OutImage(string filename)
{
    ofstream output("gulf-flounder_1.ppm", ios::out);
    if(!output)
    {
        cout << "Error!" << endl;
        exit(1);
    }

    output << magicValue <<endl<< width <<" "<<height << endl  <<maxColor  <<endl;//255
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)output << pixels[i][j].R <<" " << pixels[i][j].G <<" " << pixels[i][j].B <<" "; // 輸出RGB
        //output << endl;
    }

    output.close();
}

void ColorImage::Flip()
{
    for(int i=0; i<(height/2); i++)for(int j=0; j<width; j++)ColorImage::Swap(&pixels[i][j],&pixels[height-1-i][j]); // 反轉像素
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    swap((*p1).R,(*p2).R);
    swap((*p1).G,(*p2).G);
    swap((*p1).B,(*p2).B);
}
