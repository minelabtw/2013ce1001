#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream inClientFile( "gulf-flounder.ppm", ios::in );
    inClientFile >> magicValue >> width >> height >> maxColor;

    for ( int i = 0; i < 256; i++ )
    {
        for ( int j = 0; j < 128; j++ )
        {
            inClientFile>>pixels[i][j].R;
            inClientFile>>pixels[i][j].G;
            inClientFile>>pixels[i][j].B;
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream outClientFile( "gulf-flounder_1.ppm", ios::out );
    outClientFile << magicValue << ' ' << width << ' ' << height << ' ' << maxColor << endl;

    for ( int i = 0; i < 256; i++ )
    {
        for ( int j = 0; j < 128; j++ )
        {
            outClientFile << pixels[i][j].R << ' '
                          << pixels[i][j].G << ' '
                          << pixels[i][j].B << ' ';
        }
    }
}

void ColorImage::Flip()
{
    for ( int j = 0; j < 256; j++ )
    {
        for ( int i = 0; i < 128/2; i++ )
        {
            Swap( &pixels[j][i] , &pixels[j][128-i] );
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    swap(*p1,*p2);
}
