#include<iostream>
#include "ColorImage.h"
#include <fstream>
#include <cstdlib>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    int i=0;
    ifstream read_1("gulf-flounder.ppm",ios::in); //把檔案讀進來
    if(!read_1) //檢查有無讀進來
    {
        cerr <<"File could not be opened"<<endl;
        exit(1);
    }
    read_1 >> magicValue >> width >> height >> maxColor ; //把前面的存進
    do //存後面的顏色
    {
        for(int y=0; y<width; y++)
        {
            read_1 >>pixels[i][y].R>>pixels[i][y].G>>pixels[i][y].B;
        }
        i++;
    }
    while(i<height);
    read_1.close();
}

void ColorImage::OutImage(string filename) //印出檔案
{
    int i=0;
    ofstream cout_1("gulf-flounder_1.ppm",ios::out);
    if(!cout_1)
    {
        cerr <<"File could not be opened"<<endl;
        exit(1);
    }
    cout_1 << magicValue << endl << width << " " << height << endl <<maxColor<<endl; //存入前面的東西
    do
    {
        for(int y=0; y<width; y++)
        {
            cout_1 <<pixels[i][y].R<<" "<<pixels[i][y].G<<" "<<pixels[i][y].B<<" ";
        }
        i++;
    }
    while(i<height);
    cout_1.close();

}


void ColorImage::Flip() //變換
{
    int i=0;
    int y=0;
    do
    {
        for(; y<256; y++)
        {
            Swap(&pixels[i][y],&pixels[127-i][y]) ;
        }
        y=0;
        i++;
    }
    while(i<63);
}

void ColorImage::Swap(Pixel *p1,Pixel *p2) //交換
{
    swap((*p1).R,(*p2).R);
    swap((*p1).G,(*p2).G);
    swap((*p1).B,(*p2).B);
}
