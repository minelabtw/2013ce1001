#include<iostream>
#include "ColorImage.h"
#include <fstream>
#include <cstdlib>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream inClientFile( filename.c_str(), ios::in);//開啟檔案

    if (!inClientFile)//假如無檔案，則顯示無法打開
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }

    inClientFile >> magicValue >> width >> height >> maxColor ;//將檔案前四個數字存入
    for (int i=0; i<256 ; i++)//剩下的色彩數字用for迴圈存入二維陣列
    {
        for (int j=0; j<128; j++)
        {
            inClientFile >> pixels[i][j].R;//存入R
            inClientFile >> pixels[i][j].G;//存入G
            inClientFile >> pixels[i][j].B;//存入B
        }
    }
    inClientFile.close();//關掉檔案
}

void ColorImage::OutImage(string outfilename)//輸出檔案
{
    ofstream outClientFile( outfilename.c_str(), ios::out);//檔案名稱
    outClientFile << magicValue << endl << width << " " << height << endl;
    outClientFile << maxColor << endl;//先將前四個數字輸出

    for (int i=0; i<256 ; i++)//剩下數字用迴圈輸出
    {
        for (int j=0; j<128; j++)
        {
            outClientFile << pixels[i][j].R << " ";
            outClientFile << pixels[i][j].G << " ";
            outClientFile << pixels[i][j].B << " ";
        }
    }
    outClientFile.close();//關掉檔案
}


void ColorImage::Flip()//找到兩個應該交換的位置
{
    for (int i=0; i<64 ; i++)
    {
        for (int j=0; j<128; j++)
        {
            Swap(&pixels[i][j],&pixels[128-i][j]);
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)//兩個數字做交換
{
    Pixel hold;
    hold=*p1;
    *p1=*p2;
    *p2=hold;
}
