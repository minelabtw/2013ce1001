#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename) //open and read image to pixels
{
    ifstream inClientFile( "gulf-flounder.ppm", ios::in );
    if(inClientFile.is_open())
    {
        inClientFile >> magicValue >> width >> height >> maxColor;

        for ( int A = 0; A < 128; A++ )
        {
            for ( int B = 0; B < 256; B++ )
            {
                inClientFile >> pixels[B][A].R;
                inClientFile >> pixels[B][A].G;
                inClientFile >> pixels[B][A].B;
            }
        }
    }
    inClientFile.close();
}

void ColorImage::OutImage(string filename)  //output a image from pixels
{
    ofstream outClientFile( "gulf-flounder_1.ppm", ios::out );
    outClientFile << magicValue << endl << width << " " << height << endl << maxColor << endl;
    if(outClientFile.is_open())
    {
        for ( int A = 0 ; A < 128 ; A++ )
        {
            for ( int B = 0; B < 256; B++ )
            {
                outClientFile << pixels[B][A].R << " "
                              << pixels[B][A].G << " "
                              << pixels[B][A].B << " ";
            }
        }
    }

    outClientFile.close();
}


void ColorImage::Flip() //flip vertical
{
    for(int A=0; A<64; A++)//初始值0，長從0至127
    {
        for(int B=0; B<256; B++)//初始值0，0~254
        {
            Swap(&pixels[B][A],&pixels[B][127-A]) ;//結果:0跟127換
        }
    }
}


void ColorImage::Swap(Pixel *p1,Pixel *p2)  //swap two pixels
{
    swap((*p1).R,(*p2).R);
    swap((*p1).G,(*p2).G);
    swap((*p1).B,(*p2).B);
}
