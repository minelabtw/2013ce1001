#include<iostream>
#include "ColorImage.h"
#include <fstream>
#include <string.h>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
  ifstream FILE (filename.c_str(), ifstream::in);
  getline(FILE,magicValue);
  FILE >> width;
  FILE >> height;
  FILE >> maxColor;
  int pix[3];
  string line;
  for(int y=0;y<height;y++){
    for(int x=0;x<width;x++){
      FILE >> pixels[x][y].R;
      FILE >> pixels[x][y].G;
      FILE >> pixels[x][y].B;
    }
  }
  FILE.close();
}

void ColorImage::OutImage(string filename)
{
  ofstream FILE(filename.c_str(),ofstream::out);
  FILE << magicValue << endl;
  FILE << width << " " << height << endl;
  FILE << maxColor << endl;
  for(int y=0;y<height;y++){
    for(int x=0;x<width;x++){
      FILE << pixels[x][y].R << " ";
      FILE << pixels[x][y].G << " ";
      FILE << pixels[x][y].B << " ";
    }
    FILE << endl;
  }
}


void ColorImage::Flip()
{
  for(int y=0;y<height/2;y++){
    for(int x=0;x<width;x++){
      Swap(&(pixels[x][y]),&(pixels[x][height-1-y]));
    }
  }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
  Pixel Temp;
  Temp = *p1;
  *p1 = *p2;
  *p2 = Temp;
}
