#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    fstream infile(filename.c_str(),ios::in);      //讀取檔案
    infile>>magicValue>>width>>height>>maxColor;
    for(int x=0;x<height;x++)     //for迴圈
    {
        for(int y=0;y<width;y++)
        {
            infile>>pixels[y][x].R>>pixels[y][x].G>>pixels[y][x].B;      //先跑寬再跑高
        }
    }
    infile.close();     //關閉
}

void ColorImage::OutImage(string filename)
{
    fstream outfile(filename.c_str(),ios::out);     //輸出檔案
    outfile<<magicValue<<endl;
    outfile<<width<<" "<<height<<endl;
    outfile<<maxColor;
    for(int m=0;m<height;m++)     //for迴圈
    {
        for(int n=0;n<width;n++)
        {
            outfile<<" "<<pixels[n][m].R<<" "<<pixels[n][m].G<<" "<<pixels[n][m].B;     //先跑寬再跑高
        }
    }
    outfile<<endl;
    outfile.close();    //關閉
}


void ColorImage::Flip()
{
    for(int a=0;a<width;a++)
    {
        for(int b=0,c=height-1;c>b;b++,c--)
        {
            Swap(&pixels[a][b],&pixels[a][c]);    //交換並呼叫下面的函式
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)     //
{
    Pixel temp=*p1;           //一個變數,交換的動作
    *p1=*p2;
    *p2=temp;
}
