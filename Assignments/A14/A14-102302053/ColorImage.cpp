#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    const char *ch=filename.c_str();
    fstream inpic;
    inpic.open(ch, ios::in);
    inpic >> magicValue >> width >> height >> maxColor;
    for(int h = 0; h < 128; h++)
    {
        for(int w = 0; w < 256; w++)
        {
            inpic >> pixels[w][h].R >> pixels[w][h].G >> pixels[w][h].B;
        }
    }
}


void ColorImage::OutImage(string filename)
{
    const char *ch=filename.c_str();
    fstream outpic;
    outpic.open(ch, ios::out);
    outpic <<  magicValue << "\n" << width << " " << height << "\n" << maxColor;
     for(int h = 0; h < 128; h++)
    {
        for(int w = 0; w < 256; w++)
        {
            outpic << pixels[w][h].R <<" " << pixels[w][h].G << " " << pixels[w][h].B << " ";
        }
    }
}


void ColorImage::Flip()
{
     for(int h = 0; h < 128; h++)
    {
        for(int w = 0; w < 256; w++)
        {
            Swap(&pixels[w][h], &pixels[w][(128 - h)]);
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    int tempR, tempG, tempB;
    tempR = (*p1).R;
    tempG = (*p1).G;
    tempB = (*p1).B;
    (*p1).R = (*p2).R;
    (*p1).G = (*p2).G;
    (*p1).B = (*p2).B;
    (*p2).R = tempR;
    (*p2).G = tempG;
    (*p2).B = tempB;
}
