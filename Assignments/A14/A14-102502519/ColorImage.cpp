#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)    //讀取gulf-flounder.ppm
{
    fstream f;
    f.open("gulf-flounder.ppm",ios::in);
    f>>magicValue;
    f>>width;
    f>>height;
    f>>maxColor;
    for(int j=0; j<128; ++j)
    {
        for(int i=0; i<256; ++i)
        {
            f >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
        }
    }
    f.close();
}

void ColorImage::OutImage(string filename)    //寫入gulf-flounder_1.ppm
{
    fstream f;
    f.open("gulf-flounder_1.ppm",ios::out);
    f<<magicValue<<endl;
    f<<width<<" ";
    f<<height<<endl;
    f<<maxColor<<endl;
    for(int j=0; j<128; ++j)
    {
        for(int i=0; i<256; ++i)
        {
            f << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
        }
    }
    f.close();
}


void ColorImage::Flip()    //倒轉圖片
{
    for(int i=0; i<64; i++)
    {
        for(int j=0; j<256; j++)
        {
            Swap(&pixels[j][i],&pixels[j][127-i]);
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)    //交換
{
    swap(*p1,*p2);
}
