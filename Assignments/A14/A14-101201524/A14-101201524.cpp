#include<iostream>
#include "ColorImage.cpp"
using namespace std;

int main()
{
    string filename="gulf-flounder.ppm";//設定數入檔名
    string outFilename="gulf-flounder_1.ppm";//設定輸出檔名
    ColorImage image;//宣告一class
    image.ReadImage(filename);//用class的涵式讀檔
    image.Flip();//用class的涵式翻轉圖片
    image.OutImage(outFilename);//用class的涵式將翻轉後的圖片輸出為另一個檔案
    return 0;
}
