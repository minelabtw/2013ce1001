#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream inClientFile(filename.c_str(),ios::in);//讀檔
    inClientFile >> magicValue >> width >> height >> maxColor;//將內容依序存為檔頭、寬、高、顏色最大值
    for(int i=0;i<128;i++)
    {
        for(int j=0;j<256;j++)
        {
            inClientFile >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;//將剩下的數字存為R、G、B並將每組存為一陣列
        }
    }
    inClientFile.close();//關檔
}

void ColorImage::OutImage(string filename)
{
    ofstream outClientFile(filename.c_str(),ios::out);//寫檔
    outClientFile << magicValue << endl << width << " " << height << endl << maxColor << endl;//輸入檔頭、寬、高、顏色最大值
    for(int i=0;i<128;i++)//依序將各點的RGB數據謝入
    {
        for(int j=0;j<256;j++)
            outClientFile << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " ";
    }
    outClientFile.close();//關檔
}

void ColorImage::Flip()
{
    for(int i=0;i<64;i++)
    {
        for(int j=0;j<256;j++)
            Swap(&pixels[j][i],&pixels[j][127-i]);//依序將上半部的圖與下半部的圖做交換
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel hold=*p1;//宣告一hold來暫存p1的內容
    *p1=*p2;//將p2的內容寫入p1
    *p2=hold;//將p1原本的內容寫入p2
}
