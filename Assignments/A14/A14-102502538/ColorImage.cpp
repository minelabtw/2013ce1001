#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{

    const char *ch = filename.c_str();
    ifstream inClientFile("gulf-flounder.ppm", ios::in );
    inClientFile >> magicValue >> width >> height >> maxColor;
    for ( int i = 0; i < 128; i++ )
    {
        for ( int j = 0; j < 256; j++ )
        {
            inClientFile>>pixels[j][i].R;
            inClientFile>>pixels[j][i].G;
            inClientFile>>pixels[j][i].B;
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream outClientFile( "gulf-flounder_1.ppm", ios::out );
    outClientFile << magicValue << ' ' << width << ' ' << height << ' ' << maxColor << endl;
    for ( int i = 0; i < 128; i++ )
    {
        for ( int j = 0; j < 256; j++ )
        {
            outClientFile<<pixels[j][i].R << ' ';
            outClientFile<<pixels[j][i].G << ' ';
            outClientFile<<pixels[j][i].B << ' ';
        }
    }
}


void ColorImage::Flip()
{
    Pixel array[256][128];
    for ( int i = 0; i < 128; i++ )
    {
        for ( int j = 0; j < 256; j++ )
        {
            array[j][i] = pixels[j][i];
        }
    }
    for ( int i = 0; i < 64; i++ )
    {
        for ( int j = 0; j < 256; j++ )
        {
            Swap(&pixels[j][i],&pixels[j][128-i]);
            pixels[j][i] = pixels[j][128-i];
        }
    }
    for ( int i = 64; i < 128; i++ )
    {
        for ( int j = 0; j < 256; j++ )
        {
            Swap(&pixels[j][i],&array[j][128-i]);
            pixels[j][i] = array[j][128-i];
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    p1 = p2;
}
