#ifndef PIXEL_H
#define PIXEL_H

class Pixel
{
public:
    Pixel();
    ~Pixel();
    unsigned int R, G, B;  // Blue, Green, Red
protected:
private:
};

#endif // PIXEL_H
