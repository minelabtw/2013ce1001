#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage (string filename)//讀取圖片
{
    fstream Input (filename.c_str(),ios::in);
    Input >> magicValue >> width >> height >> maxColor;//依序讀取magicValue、寬、高、和色碼最大值
    for ( int j = 0 ; j < height ; j++)
    {
        for ( int i = 0 ; i < width ; i++)
        {
            Input >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
        }
    }//再一一讀取各色碼
    Input.close();//關閉
}

void ColorImage::OutImage (string filename)//輸出圖片
{
    ofstream Output (filename.c_str(),ios::out);
    Output << magicValue << endl;
    Output << width << " " << height << endl;
    Output << maxColor << endl;//依序寫入magicValue、寬、高、和色碼最大值
    for ( int j = 0 ; j < height ; j++)
    {
        for ( int i = 0 ; i < width ; i++)
        {
            Output << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
        }
        Output << endl;
    }//依序寫入重新排過的色碼
    Output.close();//關閉
}

void ColorImage::Flip()//翻轉
{
    for ( int i = 0 ; i < width ; i++)
    {
        for ( int j = 0 ; j < height - j ; j++)//交換到高的一半即可，因為另一半也一起交換了
        {
            Swap( &pixels[i][j] , &pixels[i][height-j-1] );
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)//數值交換
{
    Pixel tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}
