#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream inFile("gulf-flounder.ppm",ios::in);//讀取gulf-flounder.ppm檔案
    inFile>>magicValue>>width>>height>>maxColor;//將圖片的格式版本,寬,高,位元深度讀取出來
    for(int j=0; j<128; j++)//長
    {
        for(int i=0; i<256; i++)//寬
        {
            inFile>>pixels[i][j].R>>pixels[i][j].G>>pixels[i][j].B;//讀取圖檔的RGB位元
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream outFile("gulf-flounder_1.ppm",ios::out);//輸出gulf-flounder_1.ppm檔案
    outFile<<magicValue<<' '<<width<<' '<<height<<' '<<maxColor<<endl;//輸出格式版本,寬,高,位元深度

    for(int j=0; j<128; j++)//長
    {
        for(int i=0; i<256; i++)//寬
        {
            outFile<<pixels[i][j].R<<' '<<pixels[i][j].G <<' '<< pixels[i][j].B<<' ';//輸出圖檔的RGB
        }
    }
}
void ColorImage::Flip()
{
    for(int j=0; j<128/2; j++)//長的一半
    {
        for(int i=0; i<256; i++)//寬
        {
            Swap(&pixels[i][j],&pixels[i][127-j]);//將對應位置的位元交換
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)//將p1,p2交換
{
    Pixel temp;
    temp=*p1;
    *p1=*p2;
    *p2=temp;
}
