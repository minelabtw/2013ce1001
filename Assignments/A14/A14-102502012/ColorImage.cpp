#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(char* filename)
{
	fstream file;
	file.open(filename, ios::in);
	if (file.good()){
		Pixel tmp;
		file >> magicValue;
		file >> width >> height;
		file >> maxColor;
		for (int i = 0; i < height; i++){
			for (int j = 0; j < width; j++){
				file >> tmp.R >> tmp.G >> tmp.B; // read a pixel
				pixels[j][i] = tmp;
			}
		}
	}
}

void ColorImage::OutImage(char* filename)
{
	fstream file;
	file.open(filename, ios::trunc | ios::out);
	if (file.good()){
		file << magicValue << endl;
		file << width << " " << height << endl;
		file << maxColor << endl;
		int count = 0; // a line only contains letters which is not longer than 70
		for (int i = 0; i < height; i++){
			for (int j = 0; j < width; j++){
				if (count == 5)
					cout << endl, count = 0;
				file << " " << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B;
				count++;
			}
		}
	}
}

void ColorImage::Flip()
{
	for (int i = 0, j = height - 1; i < j; i++, j--){
		for (int k = 0; k < width; k++)
			Swap(&pixels[k][i], &pixels[k][j]);
	}
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
	Pixel tmp = (*p1);
	(*p1) = (*p2);
	(*p2) = tmp;
}
