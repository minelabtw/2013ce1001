#include <iostream>
#include <stdlib.h>
#include <fstream>
#include "ColorImage.h"
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)                                 //將原圖的數值讀出並存入陣列中
{
    fstream filein(filename.c_str(),ios::in);
    filein >> magicValue >> width >> height >> maxColor;
    for (int j=0;j<height;j++)
    {
        for(int i=0;i<width;i++)
        {
            filein >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
        }
    }
    filein.close();
}

void ColorImage::OutImage(string filename)                                  //將轉換後的數值寫入新的檔案中
{
    fstream fileout(filename.c_str(),ios::out);
    fileout << magicValue << endl;
    fileout << width << " " << height << endl;
    fileout << maxColor << endl;
    for (int j=0;j<height;j++)
    {
        for(int i=0;i<width;i++)
        {
            fileout << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
        }
        fileout << endl;
    }
    fileout.close();
}


void ColorImage::Flip()                                                     //將第一行和倒數第一行進行交換(鏡像翻轉)
{
    for (int j=0;j<width;j++)
        for (int i=0;i<height-i;i++)
            Swap(&pixels[j][i], &pixels[j][height-i-1]);
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)                                  //交換
{
    Pixel tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}
