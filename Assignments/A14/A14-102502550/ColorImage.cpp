#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
   int w,h,m,r,g,b;
   char waste;

   ifstream file;

   file.open(filename.c_str());

   file>>waste>>waste>>w>>h>>m;                         //輸入圖形
   set_w_h_m(w,h,m);

   for(int i=0;i<h;i++)
   {
      for(int j=0;j<w;j++)
      {
          file>>r>>g>>b;
          set_pixel(r,g,b,i,j);                         //放入pixels

      }
   }
    file.close();
}

void ColorImage::OutImage(string filename)
{
    int w,h,m;
    Pixel temp;
    ofstream file;

    w=get_w();
    h=get_h();
    m=get_m();

    file.open(filename.c_str());

    file<<"P3"<<endl<<w<<" "<<h<<endl<<m<<endl;               //輸出最後結果

    for(int i=0;i<h;i++)
    {
      for(int j=0;j<w;j++)
      {
         temp=get_pixel(i,j);
         file<<temp.R<<" "<<temp.G<<" "<<temp.B<<" ";

      }
      file<<endl;
    }
   file.close();

}


void ColorImage::Flip()
{
    int h1=height/2;
    for(int i=0;i<h1;i++)
    {
        for(int j=0;j<width;j++)
        {
            Swap(&pixels[i][j],&pixels[height-i-1][j]);    //呼叫swap轉換
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel temp;
    temp=(*p1);
    (*p1)=(*p2);
    (*p2)=temp;

}

void ColorImage::set_w_h_m(int w,int h,int m) //以下為多餘
{
    this->width=w;
    this->height=h;
    this->maxColor=m;
}

void ColorImage::set_pixel(int r,int g,int b,int x,int y)
{
    this->pixels[x][y].R=r;
    this->pixels[x][y].G=g;
    this->pixels[x][y].B=b;

}

int ColorImage::get_w()
{
    return this->width;
}

int ColorImage::get_h()
{
    return this->height;
}

int ColorImage::get_m()
{
    return this->maxColor;
}

Pixel ColorImage::get_pixel(int x,int y)
{
    return this->pixels[x][y];
}


