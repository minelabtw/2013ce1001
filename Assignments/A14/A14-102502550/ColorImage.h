#ifndef COLORIMAGE_H
#define COLORIMAGE_H
#include <string>
#include "Pixel.cpp"

class ColorImage
{
public:
    ColorImage();
    virtual ~ColorImage();
    void ReadImage(string filename);//open and read image to pixels
    void OutImage(string filename);//output a image from pixels
    void Flip();//flip vertical

protected:
private:
    string magicValue;//format header
    int width,height,maxColor;//width,height,color depth
    Pixel pixels[128][256];//all pixels
    void Swap(Pixel *p1,Pixel *p2);//swap two pixels

    void set_w_h_m(int,int,int);//�H�U���h�l
    void set_pixel(int,int,int,int,int);
    int get_w();
    int get_h();
    int get_m();
    Pixel get_pixel(int,int);
};

#endif // COLORIMAGE_H
