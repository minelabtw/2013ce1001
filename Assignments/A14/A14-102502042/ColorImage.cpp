#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename) //input file
{
    ifstream fin(filename.c_str());
    fin>>magicValue>>width>>height>>maxColor;
    for(int i=0;i<height;++i)
        for(int j=0;j<width;++j)
            fin>>pixels[j][i].R>>pixels[j][i].G>>pixels[j][i].B;
}

void ColorImage::OutImage(string filename)  //output file
{
    ofstream fout(filename.c_str());
    fout<<magicValue<<endl;
    fout<<width<<" "<<height<<endl;
    fout<<maxColor<<endl;
    for(int i=0;i<height;++i)
    {
        for(int j=0;j<width;++j)
            fout<<pixels[j][i].R<<" "<<pixels[j][i].G<<" "<<pixels[j][i].B<<" ";
        fout<<endl;
    }
}


void ColorImage::Flip()
{
    int chk[256][128]={};   //check whether it had fliped or not
    for(int i=0;i<height;++i)
        for(int j=0;j<width;++j)
            if(!chk[j][i])Swap(&pixels[j][height-i-1],&pixels[j][i]),chk[j][height-i-1]=1;
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel tmp;
    tmp.R=(*p1).R;
    tmp.G=(*p1).G;
    tmp.B=(*p1).B;
    (*p1).R=(*p2).R;
    (*p1).G=(*p2).G;
    (*p1).B=(*p2).B;
    (*p2).R=tmp.R;
    (*p2).G=tmp.G;
    (*p2).B=tmp.B;
}
