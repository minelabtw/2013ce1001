#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
    magicValue = "";
    width = height = maxColor = 0;
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream file(filename.c_str(), ios::in);
    file >> magicValue >> width >> height >> maxColor;
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width ;j++)
        {
            file >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream file(filename.c_str(), ios::out);
    file << magicValue << endl;
    file << width << " " << height << endl;
    file << maxColor << endl;
    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
        {
            file << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " ";
        }
        file << endl;
    }
}


void ColorImage::Flip()
{
    for(int i=0; i<height/2; i++)
    {
        for(int j=0; j<width; j++)
        {
            Swap(&pixels[j][i], &pixels[j][height-i-1]);
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel temp;
    temp=*p1;
    *p1=*p2;
    *p2=temp;
}
