#include <iostream>
#include "ColorImage.h"
#include <fstream>
#include <cstdlib>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream fin(filename.c_str() , ios::in);

    if(!fin){
        cerr << "file could not open!" << endl;
        exit(1);
    }

    fin >> magicValue >> width >> height >> maxColor;

    for(int i = 0 ; i < height ; i++){
        for(int j = 0 ; j < width ; j++){
            fin >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
        }
    }

    fin.close();

    //finish me
}

void ColorImage::OutImage(string filename)
{
    ofstream fout(filename.c_str() , ios::out);

    if(!fout){
        cerr << "file could not open!" << endl;
        exit(1);
    }

    fout << magicValue << endl;
    fout << width << ' ' << height << endl;
    fout << maxColor << endl;

    for(int i = 0 ; i < height ; i++){
        for(int j = 0 ; j < width ; j++){
            fout << pixels[i][j].R << ' ' << pixels[i][j].G << ' ' << pixels[i][j].B << ' ';
        }
        fout << endl;
    }

    fout.close();

    //finish me
}


void ColorImage::Flip()  //翻轉
{
    for(int i = 0 ; i < height / 2 ; i++){
        for(int j = 0 ; j < width ; j++){  //上下交換
            Pixel *p1 = &pixels[i][j];
            Pixel *p2 = &pixels[height - i - 1][j];
            Swap(p1 , p2);
        }
    }

    //finish me
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)  //像素交換
{
    int temp = p1->R;
    p1->R = p2->R;
    p2->R = temp;

    temp = p1->G;
    p1->G = p2->G;
    p2->G = temp;

    temp = p1->B;
    p1->B = p2->B;
    p2->B = temp;
    //finish me
}
