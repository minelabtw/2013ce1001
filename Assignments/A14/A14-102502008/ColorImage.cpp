#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream input("gulf-flounder.ppm",ios::in);
    if(input.is_open())
    {
         input >> magicValue ;//p3
        input >> width ;//256
        input >>height ;//128
        input >>maxColor ;//255

        for(int i=0;i<height;i++)       //每三個一組 存進陣列裡
            for(int j=0;j<width;j++)
            {
                input >> pixels[i][j].R ;
                input >> pixels[i][j].G ;
                input >> pixels[i][j].B ;
            }
    }
    input.close();
}

void ColorImage::OutImage(string filename)
{
    ofstream output("gulf-flounder_1.ppm",ios::out);
    if(output.is_open())
    {
        output << magicValue <<endl;//p3
        output << width <<" ";//256
        output <<height << endl ;//128
        output <<maxColor  <<endl;//255
        //每個陣列裡的一組rgb輸出
        for(int i=0;i<height;i++)
        {
            for(int j=0;j<width;j++)
            {
                output << pixels[i][j].R <<" " << pixels[i][j].G <<" " << pixels[i][j].B <<" ";
            }

        }
    }
    output.close();
}


void ColorImage::Flip()
{
    //進行上下對稱
    for(int i=0;i<=63;i++)
        for(int j=0;j<256;j++)
    {
        Swap(&pixels[i][j],&pixels[127-i][j]) ;
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    //每組陣列的rgb交換
    swap((*p1).R,(*p2).R);
    swap((*p1).G,(*p2).G);
    swap((*p1).B,(*p2).B);
}
