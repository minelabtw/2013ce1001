using namespace std;
#ifndef PIXEL_H
#define PIXEL_H
class Pixel
{
public:
	Pixel(short,short,short);	//constructor
	void ChangeColor(short,short,short);	//ChangeColor of this pixel
	void Blightness(double);	//adjust Blightness of pixel
	void Colorfulness(double);	//adjust Colorfulness of pixel
	double Blightness();		//return Blightness of pixel
	double Colorfulness();		//return Colorfulness of pixel
	short maxcolor();			//get max value of (r,g,b)
	short mincolor();			//get min value of (r,g,b)
	short R, G, B;
private:
};

Pixel::Pixel(short r=255,short g=255,short b=255)
{
	ChangeColor(r,g,b);
}

double Pixel::Blightness()
{
	double mymax=maxcolor();
	return mymax/255*100;
}

double Pixel::Colorfulness()
{
	double mymax=maxcolor();
	double mymin=mincolor();
	if(mymax==0)return 0;
	return (mymax-mymin)/mymax*100;
}

void Pixel::Blightness(double perc)
{
	(perc<0) && (perc=0);
	(perc>100) && (perc=100);
	double mymax=maxcolor();
	bool black=0;
	if(mymax==0)black=1,mymax=1;
	double r=(R+black)/(mymax/255)*(perc/100);
	double g=(G+black)/(mymax/255)*(perc/100);
	double b=(B+black)/(mymax/255)*(perc/100);
	R=r;G=g;B=b;
}

void Pixel::Colorfulness(double perc)
{
	(perc<0) && (perc=0);
	(perc>100) && (perc=100);
	double mymax=maxcolor();
	double mymin=mincolor();
	if(mymax==mymin)return;
	double _r=(mymax-R)/(mymax-mymin)*mymax*(perc/100);
	double _g=(mymax-G)/(mymax-mymin)*mymax*(perc/100);
	double _b=(mymax-B)/(mymax-mymin)*mymax*(perc/100);
	R=mymax-_r;G=mymax-_g;B=mymax-_b;
}

short Pixel::maxcolor()
{
	short mymax=0;
	(mymax<R) && (mymax=R);
	(mymax<G) && (mymax=G);
	(mymax<B) && (mymax=B);
	return mymax;
}

short Pixel::mincolor()
{
	short mymin=255;
	(mymin>R) && (mymin=R);
	(mymin>G) && (mymin=G);
	(mymin>B) && (mymin=B);
	return mymin;
}

void Pixel::ChangeColor(short r=255,short g=255,short b=255)
{
	R=r;G=g;B=b;
}

#endif // PIXEL_H
