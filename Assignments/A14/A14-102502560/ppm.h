#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <string>
#include "pixel.h"
#include "martix.h"
using namespace std;

#ifndef PPM_H
#define PPM_H

class Picture
{
public:
	Picture(string);			//create from file
	Picture(int=800,int=600);	//open new empty canvas

	void changecolor(int=0, int=0, short=255,short=255,short=255);	//change a pixel at (x,y) with color(r,g,b)
	void DrawRect(int=0,int=0,int=1,int=1,short=0,short=0,short=0);	//draw a Rectangle from (x,y) with size(w,h) with color(r,g,b)

	void blightness(double);	//adjust the blightness
	void colorfulness(double);	//adjust the colorfulness

	void FlipLR();			//flip left-to-right
	void FlipUD();			//flip up-side-down
	void FlipXY();			//flip along the line x=y
	void Rotate_c90();		//rotate the whole picture counter-clockwise 90 deg
	void Rotate_90();		//rotate the whole picture clockwise 90 deg

	void printout();			//print picture in hexcode on the screen
	void printoutfile(string);	//output to a file

	int GetWid();	//get width
	int GetHei();	//get height

private:
	void CopyBack(Picture);		//copy <Pixel, Width, Height> from another Picture object
	string version;				//PPM's version
	int depth;					//bit depth
	int width;
	int height;
	vector <vector<Pixel> > pix;	//2-dimension vector for storing pixels
};

Picture::Picture(string filename)
{
	ifstream IN;
	IN.open(filename.c_str());

	IN >> version >> width >> height >> depth;

	pix.resize(height);									//set height
	for(int i=0;i<height;i++)pix[i].resize(width);		//set width

	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			IN >> pix[h][w].R >> pix[h][w].G >> pix[h][w].B;	//fill colors
		}
	}

	IN.close();
}

Picture::Picture(int w, int h)
:	width(w),height(h),version("P3"),depth(255)
{
	pix.resize(height);									//set height
	for(int i=0;i<height;i++)pix[i].resize(width);		//set width

	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			pix[h][w].ChangeColor();	//clear the whole canvas with white(255,255,255)
		}
	}
}

void Picture::changecolor(int w, int h, short r,short g,short b)
{
	pix[h][w].ChangeColor(r,g,b);
}

void Picture::DrawRect(int x,int y,int w,int h,short r,short g,short b)
{
	for(int row=y;row<y+h;row++){
		for(int col=x;col<x+w;col++){
			pix[row][col].ChangeColor(r,g,b);
		}
	}
}

void Picture::blightness(double ud)
{
	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			double nowpc=pix[h][w].Blightness();
			pix[h][w].Blightness(nowpc+ud);		//increase blightness by ud
		}
	}
}

void Picture::colorfulness(double ud)
{
	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			double nowpc=pix[h][w].Colorfulness();
			pix[h][w].Colorfulness(nowpc*(100+ud)/100);		//increase colorfulness by ud
		}
	}
}

void Picture::FlipLR()
{
	Picture tmp(width,height);
	Martix A(-1,0,0,1);
	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			Martix xy(w,h);
			xy.MartixTran(A);
			xy.x+=width-1;
			tmp.changecolor(xy.x,xy.y,pix[h][w].R,pix[h][w].G,pix[h][w].B);
		}
	}
	CopyBack(tmp);
}

void Picture::FlipUD()
{
	Picture tmp(width,height);
	Martix A(1,0,0,-1);
	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			Martix xy(w,h);
			xy.MartixTran(A);
			xy.y+=height-1;
			tmp.changecolor(xy.x,xy.y,pix[h][w].R,pix[h][w].G,pix[h][w].B);
		}
	}
	CopyBack(tmp);
}

void Picture::FlipXY()
{
	Picture tmp(height,width);
	Martix A(0,1,1,0);
	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			Martix xy(w,h);
			xy.MartixTran(A);
			tmp.changecolor(xy.x,xy.y,pix[h][w].R,pix[h][w].G,pix[h][w].B);
		}
	}
	CopyBack(tmp);
}

void Picture::Rotate_c90()
{
	Picture tmp(height,width);
	Martix A(0,-1,1,0);
	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			Martix xy(w,h);
			xy.MartixTran(A);
			xy.x+=height-1;
			tmp.changecolor(xy.x,xy.y,pix[h][w].R,pix[h][w].G,pix[h][w].B);
		}
	}
	CopyBack(tmp);
}

void Picture::Rotate_90()
{
	Picture tmp(height,width);
	Martix A(0,1,-1,0);
	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			Martix xy(w,h);
			xy.MartixTran(A);
			xy.y+=width-1;
			tmp.changecolor(xy.x,xy.y,pix[h][w].R,pix[h][w].G,pix[h][w].B);
		}
	}
	CopyBack(tmp);
}

void Picture::printout()
{
	cout << setbase(16) << setfill('0');

	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			cout << setw(2) << pix[h][w].R
				<< setw(2) << pix[h][w].G
				<< setw(2) << pix[h][w].B
				<< " ";
		}
		cout << endl;
	}

}

void Picture::printoutfile(string filename)
{
	ofstream OUT;
	OUT.open(filename.c_str());
	OUT << version << endl
		<< width << " " << height << endl
		<< depth << endl;

	for(int h=0;h<height;h++){
		for(int w=0;w<width;w++){
			OUT << pix[h][w].R << " "
				<< pix[h][w].G << " "
				<< pix[h][w].B << " ";
			if(w<width-1)OUT << " ";
		}
		OUT << endl;
	}

	OUT.close();
}

int Picture::GetWid()
{
	return width;
}

int Picture::GetHei()
{
	return height;
}

void Picture::CopyBack(Picture P)
{
	width=P.width;
	height=P.height;
	pix=P.pix;
}

#endif // PPM_H
