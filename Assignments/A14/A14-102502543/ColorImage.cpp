#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    int a=1;
    int b=1;
    int z;
    ifstream file (&filename[0]); //讀入黨案
    while(file >> magicValue)
    {
        while(a>2 and file >>z)
        {
            for(height=0; height<=127; height++)
            {
                for(width=0; width<=255;)
                {
                    if (b==1 and file >>z)
                        pixels[width][height].R=z; //將像素存入變數
                    if (b==2 and file >>z)
                        pixels[width][height].G=z;
                    if (b==3 and file >>z)
                    {
                        pixels[width][height].B=z;
                        b=0;
                        width++;
                    }
                    b++;
                }
            }
        }
        a++;
    }
}

void ColorImage::OutImage(string filename)
{
    int b=1;
    ofstream file (&filename[0]); //輸出檔案
    file <<"P3"<<endl<<"256 128"<<endl<<"255"<<endl;
    for(height=0; height<=127; height++)
    {
        for(width=0; width<=255;)
        {
            if (b==1)
                file<<pixels[width][height].R<<" ";
            if (b==2)
                file<<pixels[width][height].G<<" ";
            if (b==3)
            {
                file<<pixels[width][height].B<<" ";
                b=0;
                width++;
            }
            b++;
        }
    }
}

void ColorImage::Flip()
{
    for (int a=0;a<=255;a++)
        Swap(pixels[a],pixels[a]);
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    int t;
    for(int a=0;a<=63;a++) //將像素上下交換
    {
        t=p1[a].R;
        p1[a].R=p1[127-a].R;
        p1[127-a].R=t;
        t=p1[a].G;
        p1[a].G=p1[127-a].G;
        p1[127-a].G=t;
        t=p1[a].B;
        p1[a].B=p1[127-a].B;
        p1[127-a].B=t;
    }
}
