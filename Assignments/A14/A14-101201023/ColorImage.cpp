#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream fish(filename.c_str());                                      //開檔

    fish >> magicValue;                                                   //讀取格式版本
	fish >> width >> height;                                              //讀取寬、長
	fish >> maxColor;                                                     //讀取顏色總數

	for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
            fish >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;   //將每三個數子分別讀取到pixel的R、G、B
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream inverseF(filename.c_str());                                   //開檔

    inverseF << magicValue << endl;                                        //輸入格式版本
    inverseF << width << " " << height << endl;                            //輸入寬、長
    inverseF << maxColor << endl;                                          //輸入顏色總數

    for(int i=0; i<height; i++)
    {
        for(int j=0; j<width; j++)
            inverseF << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";     //輸入圖檔顏色

        inverseF << endl;
    }
}


void ColorImage::Flip()
{
    for(int j=0; j<width; j++)
    {
        for(int i=0; i<height/2; i++)
            Swap(&pixels[i][j] , &pixels[height-1-i][j]);           //做上下對稱互換
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    int r=(*p1).R;
    int g=(*p1).G;
    int b=(*p1).B;

    (*p1).R=(*p2).R;
    (*p1).G=(*p2).G;
    (*p1).B=(*p2).B;

    (*p2).R=r;
    (*p2).G=g;
    (*p2).B=b;
}
