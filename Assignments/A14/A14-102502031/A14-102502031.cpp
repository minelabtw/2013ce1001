#include<iostream>
#include "ColorImage.cpp"

using namespace std;

int main()
{
    string Filename="gulf-flounder.ppm";    //input file
    string outputFilename="gulf-flounder_1.ppm";    //output file
    ColorImage image;    //image class
    image.ReadImage(Filename);
    image.VerticalFlip();
    image.WriteImage(outputFilename);

    return 0;
}
