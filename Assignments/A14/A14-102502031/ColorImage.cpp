#include <iostream>
#include <fstream>
#include "ColorImage.h"

using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream read(filename.c_str(), ios::in);
    read >> magicValue;
    read >> width >> height;
    read >> FullColor;
    for (int j=0; j<height; j++)
    {
        for (int i=0; i<width; i++)
        {
            read >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;
        }
    }
}

void ColorImage::WriteImage(string filename)
{
    ofstream write(filename.c_str(), ios::out);

    write << magicValue << '\n';
    write << width << ' ' << height << '\n';
    write << FullColor << '\n';
    for (int j=0; j<height; j++)
    {
        for (int i=0; i<width; i++)
        {
            write << pixels[i][j].R << ' ' << pixels[i][j].G << ' ' << pixels[i][j].B;
            if (i!=width-1)
                write << ' ';
        }
        write << '\n';
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel Temporary;
    Temporary=*p1;
    *p1=*p2;
    *p2=Temporary;
}

void ColorImage::VerticalFlip()
{
    for (int j=0; j<height/2; j++)
    {
        for (int i=0; i<width; i++)
        {
            Swap(&pixels[i][j], &pixels[i][height-1-j]);
        }
    }
}
