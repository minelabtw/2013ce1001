#ifndef PIXEL_H
#define PIXEL_H

class Pixel
{
public:
    Pixel();
    ~Pixel();
    unsigned int R;    //Red
    unsigned int G;    //Green
    unsigned int B;    //Blue
protected:
private:
};

#endif    //PIXEL_H
