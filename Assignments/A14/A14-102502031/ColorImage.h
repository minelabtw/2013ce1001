#ifndef COLORIMAGE_H
#define COLORIMAGE_H

#include "Pixel.cpp"

using namespace std;

class ColorImage
{
public:
    ColorImage();
    virtual ~ColorImage();
    void ReadImage(string filename);    //open and read image
    void WriteImage(string filename);    //output a image
    void VerticalFlip();    //flip vertically
protected:
private:
    string magicValue;    //format header
    int width;
    int height;
    int FullColor;    //color depth
    Pixel pixels[256][128];    //all pixels
    void Swap(Pixel *p1,Pixel *p2);    //swap two pixels
};

#endif    //COLORIMAGE_H
