#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    const char *filename2=filename.c_str();  //轉換型態
    ifstream infile(filename2);  //輸入檔案
    infile >> magicValue;  //丟入變數
    infile >> width;
    infile >> height;
    infile >> maxColor;
    for(int i = 0; i <= 127; i++)
    {
        for(int k = 0; k <= 255; k++)  //像素資料
        {
            infile >> pixels[k][i].R;  //分別取到class pixels 陣列物件的 RGB裡面
            infile >> pixels[k][i].G;
            infile >> pixels[k][i].B;
        }
    }
    infile.close();
}

void ColorImage::OutImage(string filename)
{
    const char *filename3=filename.c_str();  //轉換型態
    ofstream outfile(filename3);
    outfile << magicValue << endl;  //各種輸出
    outfile << width << " ";
    outfile << height << endl;
    outfile << maxColor << endl;
    for(int i = 0; i <= 127; i++)
    {
        for(int k = 0; k <= 255; k++)
        {
            outfile << pixels[k][i].R << " ";
            outfile << pixels[k][i].G << " ";
            outfile << pixels[k][i].B << " ";
        }
        outfile << endl;
    }
    outfile.close();
}


void ColorImage::Filp()
{
    for(int k = 0; k <= 255; k++)  //從第一行的第一跟最後一個互換然後中間互換再換下一列
    {
        for(int i = 0; i <= 63; i++)
            Swap(pixels[k]+i,pixels[k]+127-i);
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel temp;  //暫存
    temp = *p1;  //p1 p2 互換
    *p1 = *p2;
    *p2 = temp;
}
