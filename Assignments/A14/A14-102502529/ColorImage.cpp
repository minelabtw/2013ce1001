#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)                         //讀取圖片資料
{
    fstream in(filename.c_str(),ios::in);
    in>>magicValue>>width>>height>>maxColor;
    for(int y=0; y<height; y++)
    {
        for(int x=0; x<width; x++)
        {
            in>>pixels[x][y].R>>pixels[x][y].G>>pixels[x][y].B;
        }
    }
    in.close();
}

void ColorImage::OutImage(string filename)                         //輸出圖片
{
    fstream out(filename.c_str(),ios::out);
    out<<magicValue<<endl
       <<width<<" "
       <<height<<endl
       <<maxColor<<endl;
    for(int y=0; y<height; y++)
    {
        for(int x=0; x<width; x++)
        {
            out<<pixels[x][y].R<<" "<<pixels[x][y].G<<" "<<pixels[x][y].B<<" ";
        }
        out<<endl;
    }
    out.close();

}


void ColorImage::Flip()                                     //跟Swap 進行翻轉
{
    for(int x=0; x<width; x++)
    {
        for(int y=0; y<height/2; y++)                       //height 只能有一半的高度 才能反轉
        {
            Swap(&pixels[x][y],&pixels[x][height-y-1]);
        }
    }

}

void ColorImage::Swap(Pixel *p1,Pixel *p2)                  //座標調換
{
    Pixel tmp=*p1;
    *p1=*p2;
    *p2=tmp;

}
