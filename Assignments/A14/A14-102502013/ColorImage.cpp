#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)//讀入原圖檔
{
    ifstream inclientfile(filename.c_str(), ios::in);
    inclientfile >> magicValue;//ppm的格式
    inclientfile >> width >> height >> maxColor;
    for (int i=0; i<128; i++)
    {
        for (int j=0; j<256; j++)
        {
            inclientfile >>  pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;//依序讀入pixels
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream outclientfile(filename.c_str() , ios::out);
    outclientfile << magicValue << endl;
    outclientfile << width << " " << height << endl << maxColor << endl;
    for (int i=0; i<128; i++)
    {
        for (int j=0; j<256; j++)
        {
            outclientfile << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " ";//依序輸出pixels
        }
        outclientfile << endl;
    }
}


void ColorImage::Flip()//垂直翻轉
{
    for (int i=0;i<64;i++)
    {
        for (int j=0;j<256;j++)
        {
            Swap(&pixels[j][i], &pixels[j][127-i]);//將第1個pixel與最後1個pixel交換依序向內交換
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)//交換對應的pixel
{
    Pixel tmp;
    tmp=*p1;
    *p1=*p2;
    *p2=tmp;
}
