#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream infile ( "gulf-flounder.ppm" );
    infile >> magicValue;
    infile >> width;
    infile >> height;
    infile >> maxColor;

    for ( int h = 0 ; h < 128 ; h++ )
    {
        for ( int w = 0 ; w < 256 ; w++ )
        {
            infile >> pixels[w][h].R;
            infile >> pixels[w][h].G;
            infile >> pixels[w][h].B;
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream outfile ( "gulf-flounder_1.ppm" );
    outfile << magicValue << endl;
    outfile << width << endl;
    outfile << height << endl;
    outfile << maxColor << endl;

    for ( int h = 0 ; h < 128 ; h++ )
    {
        for ( int w = 0 ; w < 256 ; w++ )
        {
            outfile << pixels[w][h].R << " " << pixels[w][h].G << " " << pixels[w][h].B << " " ;
        }
    }
}

void ColorImage::Flip()
{
    for ( int j = 1 ; j <= 64 ; j++ )   //平分成兩部分互換
    {
        for ( int i = 0 ; i < 256 ; i++ )
            Swap( &pixels[i][j] , &pixels[i][128-j] );

    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)  //頂層 . 底層互換
{
    int hold[3];

    hold[0] = (*p1).R;
    (*p1).R = (*p2).R;
    (*p2).R = hold[0];

    hold[1] = (*p1).G;
    (*p1).G = (*p2).G;
    (*p2).G = hold[1];

    hold[2] = (*p1).B;
    (*p1).B = (*p2).B;
    (*p2).B = hold[2];
}


