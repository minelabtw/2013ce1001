#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream fin(filename); // 開檔案

	fin >> magicValue;
	fin >> width >> height>>maxColor; // 長寬高
	for(int i = 0 ; i < height ; ++i)    //針對每一點
	{
		for(int j = 0 ; j < width ; ++j)
			fin >> pixels[j][i].R >> pixels[j][i].G >> pixels[j][i].B;
	}

}

void ColorImage::OutImage(string filename)
{
   ofstream fout(filename); //開檔案
out << magicValue << endl;
	fout << width << " " << height << endl;  // 寬與長
	fout << maxColor << endl ;   //顏色

	for(int i = 0 ; i < height ; ++i)  //針對每一點
	{
		for(int j = 0 ; j < width ; ++j)
			fout << pixels[j][i].R << " " << pixels[j][i].G << " " << pixels[j][i].B << " "; // write R , G , B for every pixel

		fout << endl ;
	}
}


void ColorImage::Flip()   //將圖做翻轉
{
for(int i = 0 ; i < height / 2 ; ++i)
	{
		for(int j = 0 ; j < width ; ++j)
			Swap(&pixels[j][i] , &pixels[j][height - i - 1]) ;
	}
}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    int a=(*p1).R;
    int b=(*p1).G;
    int c=(*p1).B;

    (*p1).R=(*p2).R;
    (*p1).G=(*p2).G;
    (*p1).B=(*p2).B;

    (*p2).R=a;
    (*p2).G=b;
    (*p2).B=c;
}
