#include<iostream>
#include "ColorImage.h"
#include <fstream>
using namespace std;

ColorImage::ColorImage()
{
}

ColorImage::~ColorImage()
{
}

void ColorImage::ReadImage(string filename)
{
    ifstream input(filename.c_str());         //開啟檔案
    input >> magicValue;                      //讀格式版本
    input >> width >> height >> maxColor;     //讀寬度跟高度跟深度
    int i,j;
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            input >> pixels[i][j].R >> pixels[i][j].G >> pixels[i][j].B;  //把它存進一個陣列
        }
    }
}

void ColorImage::OutImage(string filename)
{
    ofstream output(filename.c_str());               //輸出檔案
    output << magicValue << endl;                    //輸出格式版本
    output << width << " " << height << endl;        //輸出寬度跟高度
    output << maxColor << endl;                      //輸出深度
    int i,j;
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            output << pixels[i][j].R << " " << pixels[i][j].G << " " << pixels[i][j].B << " ";
        }
        output << endl;
    }

}


void ColorImage::Flip()
{
    int i,j;
    for(i=0;i<height/2;i++)
    {
        for(j=0;j<width;j++)
        {
            Swap(&pixels[i][j],&pixels[height-1-i][j]);
        }
    }


}

void ColorImage::Swap(Pixel *p1,Pixel *p2)
{
    Pixel p3;
    p3.R = (*p1).R;
    p3.G = (*p1).G;
    p3.B = (*p1).B;
    (*p1).R = (*p2).R;
    (*p1).G = (*p2).G;
    (*p1).B = (*p2).B;
    (*p2).R = p3.R;
    (*p2).G = p3.G;
    (*p2).B = p3.B;

}
