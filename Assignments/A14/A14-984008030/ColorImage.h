#ifndef COLORIMAGE_H
#define COLORIMAGE_H
#include "Pixel.cpp"
#include <string>
using namespace std;

class ColorImage{
public:
    ColorImage();
    virtual ~ColorImage();
    void ReadImage(string filename);//open and read image to pixels
    void OutImage(string filename);//output a image from pixels
    void Flip();//flip vertical
protected:
private:
    string magicValue;
    int width,height,maxColor;
    Pixel pixels[256][128];
    void Swap(Pixel *p1,Pixel *p2);
};

#endif // COLORIMAGE_H
