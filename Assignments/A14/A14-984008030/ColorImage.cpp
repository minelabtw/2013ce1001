#include "ColorImage.h"
#include <cstdlib>
#include <iostream>
#include <fstream>

ColorImage::ColorImage(){
    //初始化為一個白色圖片
    this->magicValue = "P3";
    this->width = 256;
    this->height = 128;
    this->maxColor = 255;
}

ColorImage::~ColorImage(){
}

void ColorImage::ReadImage(string filename){
    string buffer;//做為一個從string轉換成數字的緩衝
    ifstream fin(filename.c_str());//載入檔案
    getline(fin, this->magicValue);//取得magicValue
    getline(fin, buffer, ' ');//取得圖片長
    this->width = atoi(buffer.c_str());
    getline(fin, buffer);//取得圖片寬
    this->height = atoi(buffer.c_str());
    getline(fin, buffer);//取得圖片最大色彩
    this->maxColor = atoi(buffer.c_str());
    for(int i = 0; i < this->height; i++){//載入各個pixel的顏色
        for(int j = 0; j < this->width; j++){
            getline(fin, buffer, ' ');
            this->pixels[j][i].R = atoi(buffer.c_str());
            getline(fin, buffer, ' ');
            this->pixels[j][i].G = atoi(buffer.c_str());
            if(j == (this->width - 1)){//在最右邊時會換行 需要判斷是以' '還是'\n'結束
                getline(fin, buffer, '\n');
            }
            else{
                getline(fin, buffer, ' ');
            }
            this->pixels[j][i].B = atoi(buffer.c_str());
        }
    }
    fin.close();
}

void ColorImage::OutImage(string filename){
    ofstream fout(filename.c_str());
    fout << this->magicValue << endl \
         << this->width << " " << this->height << endl \
         << this->maxColor << endl;
    for(int i = 0; i < this->height; i++){
        for(int j = 0; j < this->width; j++){
            fout << this->pixels[j][i].R << " " \
                 << this->pixels[j][i].G << " " \
                 << this->pixels[j][i].B << " ";
        }
    }
    fout.close();
}


void ColorImage::Flip(){
    for(int i = 0; i < this->width; i++){
        int j = 0;
        int k = this->height - 1;
        while(j < k){//頭尾兩個pixel交換
            this->Swap(&(this->pixels[i][j]), &(this->pixels[i][k]));
            j++;
            k--;
        }
    }
}

void ColorImage::Swap(Pixel *p1,Pixel *p2){
    Pixel temp;
    temp.R = p1->R;
    temp.G = p1->G;
    temp.B = p1->B;
    p1->R = p2->R;
    p1->G = p2->G;
    p1->B = p2->B;
    p2->R = temp.R;
    p2->G = temp.G;
    p2->B = temp.B;
}
