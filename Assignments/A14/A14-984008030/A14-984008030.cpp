#include "ColorImage.cpp"
#include<iostream>
using namespace std;

int main()
{
    string filename="gulf-flounder.ppm";//宣告型態為字串的圖檔名稱，並初始化為gulf-flounder.ppm
    string outFilename="gulf-flounder_1.ppm";//宣告型態為字串的輸出圖檔名稱，並初始化為gulf-flounder_1.ppm
    ColorImage image;//宣告型態為ColorImage的圖片
    image.ReadImage(filename);//載入圖片
    image.Flip();//翻轉圖片
    image.OutImage(outFilename);//輸出圖片到檔案
    return 0;
}
