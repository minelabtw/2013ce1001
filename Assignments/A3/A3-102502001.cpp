#include <iostream>

using namespace std ;

int main()
{
    int num1;   //宣告一整數變數為num1
    int num2;   //宣告另一整數變數為num2
    int x=1;    //宣告整數變數x,其初始值為1
    int y=1;    //宣告整數變數y,其初始值為1

    cout << "Draw a Rectangle." <<endl;         //將字串"Draw a Rectangle."輸出至螢幕上
    cout << "Enter width and height." <<endl;   //將字串"Enter width and height."輸出至螢幕上
    cout << "width:";                           //將字串"width"輸出至螢幕上
    cin >> num1;                                //將num1輸入至電腦

    while (num1<=0)                             //用while迴圈,當num1小於0時
    {
        cout << "illegal input!"<<endl;         //將字串"illegal input"輸出至螢幕上並換行
        cout << "width:";                       //將字串"width:"輸出至螢幕上
        cin >> num1;                            //將num1輸入至電腦
    }

    cout << "height:";                          //將字串"height:"輸出至螢幕上
    cin >> num2 ;                               //將num2輸入至電腦

    while (num2<=0)                             //用while迴圈,當num2小於0時
    {
        cout << "illegal input!"<<endl;         //將字串"illegal input"輸出至螢幕上並換行
        cout << "height:";                      //將字串"height:"輸出至螢幕上
        cin >> num2;                            //將num2輸入至電腦
    }

    while (y<=num2)                             //用while迴圈,條件為"當y小於num2時"
    {
        while (x<=num1)                         //用while迴圈,條件為"當x小於num1時"
        {
            if (y==1 or y==num2)                //若y=1或y=num2時
            cout <<"*";                         //將"*"顯示於螢幕上
            else if(x==1 or x==num1)            //若x=1或x=num1
            cout<<"*";                          //將"*"顯示於螢幕上
            else                                //若y!=1,num2且x!=1,num1
            cout<<" ";                          //將" "顯示於螢幕上
            x++;                                //x=x+1
        }
        cout << endl ;                          //顯示下一行
        x=1;                                    //x初始值回到1
        y++;                                    //y=y+1
    }
    return 0;
}
