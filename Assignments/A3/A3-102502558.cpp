#include <iostream>

using namespace std;

int main()
{
    int h,w; // h for height, w for width
    // show message
    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    // input width
    cout << "width: ";cin >> w;
    while(w<=0)
    {
        cout << "illegal input!" << endl;
        cout << "width: ";cin >> w;
    }

    // input height
    cout << "height: ";cin >> h;
    while(h<=0)
    {
        cout << "illegal input!" << endl;
        cout << "height: ";cin >> h;
    }

    for (int y=0;y<h;y++)
    {
        for (int x=0;x<w;x++)
            if (x == 0 || y == 0 || x == w-1 || y == h-1)
                cout << '*'; // if x or y on the border then print star sign
            else
                cout << ' '; // else we just print a space
        cout << endl;  // next line
    }

    return 0;
}
