#include<iostream>
using namespace std;
int main()
{
    cout<<"Draw a Rectangle.\n";                        //將"Draw a Rectangle."輸出到螢幕上
    cout<<"Enter width and height.\n";                  //將"Enter width and height."輸出到螢幕上
    int width;                                          //宣告變數width
    int height;                                         //宣告變數height

    cout<<"width:";                                     //將"width:"輸出到螢幕上
    cin>>width;                                         //將輸入的值丟給width
    while(1)                                            //進入迴圈
    {
        if(width<=0)                                    //若符合if的範圍則進行以下程式
        {
            cout<<"illegal input!\n";                   //將"illegal input!"輸出到螢幕上
            cout<<"width:";                             //將"width:"輸出到螢幕上
            cin>>width;                                 //重新給一個width的值
        }
        else break;                                     //跳出迴圈
    }
    cout<<"height:";                                    //將"height:"輸出到螢幕上
    cin>>height;                                        //將輸入的值丟給height
    while(1)                                            //進入迴圈
    {
        if(height<=0)                                   //若符合if的範圍則進行以下程式
        {
            cout<<"illegal input!\n";                   //將"illegal input!"輸出到螢幕上
            cout<<"height:";                            //將"height:"輸出到螢幕上
            cin>>height;                                //重新給一個height的值
        }
        else break;                                     //跳出迴圈
    }
    for(int a=height; a>0; a--)                         //宣告一個a且把height的值給a然後進入迴圈
    {
        for(int b=width; b>0; b--)                      //宣告一個b且把width的值給b然後進入迴圈
        {
            if (a==height||a==1)
            {
                cout<<"*";                              //如果a=height或a=1則輸出*到螢幕上
            }
            else                                        //若非以上條件則進行以下程式
            {
                if(b==1||b==width)
                {
                    cout<<"*";                          //若b=1或b=width則將"*"輸出到螢幕上
                }
                else
                {
                    cout<<" ";                          //若非則輸出空格到螢幕上
                }
            }

        }
        cout<<"\n";                                      //換行
    }









}
