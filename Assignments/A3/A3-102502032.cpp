#include <iostream>
using namespace std;

int main ()
{
    int width = 0;                 //an integer varible: save the width
    int length = 0;                //an integer varible: save the length

    //intro
    cout << "Draw a Rectangle.\nEnter width and height.\n";

    //ask for the width
    do
    {
        cout << "width: ";
        cin >> width;
        //width must have to be larger than 0
        if ( width <= 0 )
            cout << "illegal input!" << endl;
    }
    while ( width <= 0 );

    //ask for the length and save into varible length
    do
    {
        cout << "length: ";
        cin >> length;
        //length must have to be larger than 0
        if ( length <= 0 )
            cout << "illegal input!" << endl;
    }
    while ( length <= 0 );

    //ask for the width and save into varible width
    for ( int i = 1; i <= length; i ++ )
    {
        for ( int j = 1; j <= width; j ++)
            {
                //inside block is space
                if ( (i != length and i!=1) and (j != width and j != 1) )
                    cout << " ";
                //the perimeter is *
                else
                    cout << "*";
            }
        cout << endl;
    }
}

