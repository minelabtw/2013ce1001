#include<iostream>
using namespace std;

int main()
{
    int width=0;//宣告整數變數長方形的寬
    int height=0;//宣告整數變數長方形的高
    int x=1;//宣告整數變數(X,Y)點
    int y=1;


    cout<<"Draw a Rectangle."<<"\n";
    cout<<"Enter width and height."<<"\n";
    cout<<"width:";
    cin >> width;

    while(width<=0)//當寬小於等於零的時候輸出錯誤訊息的迴圈
    {
        cout<<"illegal input!"<<"\n";
        cout<<"width:";
        cin >> width;
    }
    cout<<"height:";
    cin >> height;

    while(height<=0)//當高小於等於零的時候輸出錯誤訊息的迴圈
    {
        cout<<"illegal input!"<<"\n";
        cout<<"height:";
        cin >> height;
    }

    while(y<=height)//從第一列開始一個點一個點判定  從左到底再換列的迴圈
    {
        while(x<=width)
        {
            if(x==1)//第一行都打星號
            {
                cout<<"*";
            }
            else if(y==1)//第一列都打星號
            {
                cout<<"*";
            }
            else if(x==width)//點的X座標等於寬的時候打星號
            {
                cout<<"*";
            }
            else if(y==height)//點的y座標等於高的時候打星號
            {
                cout<<"*";
            }
            else//剩下打空白
            {
                cout<<" ";
            }
            x++;//一個X點叛定完換下一個，所以要+1
        }
        cout<<"\n";//換列
        x=1;//因為x已經加到最大值了，所以要歸零
        y++;//因為換列所以y要+1

    }
return 0;

}
