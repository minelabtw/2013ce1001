
#include <iostream>
using namespace std;
int main()
{
    int x=1,y=1,width,height;                   //設定初始點
    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;
    cout<<"width:";
    cin>>width;
    for (;width<=0;)                            //限定寬跟高的範圍
    {
        cout<<"illegal input!"<<endl;
        cout<<"width:";
        cin>>width;
    }
    
    cout<<"height:";
    cin>>height;
    
    for (;height<=0;)                            //限定寬跟高的範圍
    {
        cout<<"illegal input!"<<endl;
        cout<<"height:";
        cin>>height;
    }
    while(y<=height)                              //此程式要判斷的y不能大於高度
    {
        while (x<=width)                          //x不能大於寬度
        {
         if (y==1||y==height||x==1||x==width)     //當x y在邊界時
            cout<<"*";                            //輸出＊
         else
            cout<<" ";                            //其他的 輸出空白字元
            x++;
        }
        x=1;
        cout<<endl;
        y++;
    }
    return 0;
}

