#include <iostream>
using namespace std;

int main()
{
    int x;
    int y;
    int a=1;
    int b=1;                                                      //宣告變數x,y,a,b

    cout<<"Draw a Rectangle.\nEnter width and height.\nwidth:";
    cin>>x;
    while(x<=0)                                                   //判斷X的值是否符合範圍
    {
        cout<<"illegal input!\nwidth:";
        cin>>x;
    }
    cout<<"height:";
    cin>>y;
    while(y<=0)                                                   //判斷y的值是否符合範圍
    {
        cout<<"illegal input!\nheight:";
        cin>>y;
    }
    while(a<=y)                                                   //畫出圖形
    {
        while(b<=x)
        {
            if(a==1 && b<=x)
            {
                cout<<"*";
            }
            else if(a<y && b==1)
            {
                cout<<"*";
            }
            else if(a<y && b<x)
            {
                cout<<" ";
            }
            else if(a<y && b==x)
            {
                cout<<"*";
            }
            else
            {
                cout<<"*";
            }
            b++;
        }
        cout<<endl;
        b=1;                   //使b的值變回1
        a++;
    }

    return 0;
}
