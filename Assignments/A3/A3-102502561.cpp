#include <iostream>
using namespace std;

int main()
{
    int width;
    int height;
    int wtimes;
    int htimes;

    cout << "Draw a Rectangle.\nEnter width and height.\n"; //題目

    cout << "width:";                                       //ˇ讀取正確範圍的"width"
    cin >> width;
    while(width<=0)
    {
        cout << "illegal input!\n";
        cout << "width:";
        cin >> width;                                       //^讀取正確範圍的"width"
    }

    cout << "height:";                                      //ˇ讀取正確範圍的"height"
    cin >> height;
    while(height<=0)
    {
        cout << "illegal input!\n";
        cout << "height:";
        cin >> height;                                      //^讀取正確範圍的"height"
    }

    for(wtimes=0;wtimes<=(width-1);wtimes++)                       //第一個for接出長方形的邊寬(EX:*****)
    {
        cout << "*";
    }

    if(height>1)                                                   //可以省略考慮height=1時的情況
    {

    for(htimes=0;htimes<=(height-3);htimes++)                      //第二個for接出長方形空心部分的高度(EX:*   *)
        {
        cout << "\n*";
        for(wtimes=0;wtimes<=(width-3);wtimes++)                   //第三個for會根據"width"空出相對印的" "
            {
                cout << " ";
            }
        cout << "*";
        }

        cout << "\n";
                                                                  //長方形空心部分用完後(*   *)的換行
    for(wtimes=0;wtimes<=(width-1);wtimes++)                       //第四個for接出長方形的邊寬(EX:*****)
    {
            cout << "*";
    }
    }
    return 0;
}
