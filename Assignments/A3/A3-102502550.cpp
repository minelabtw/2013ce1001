#include <iostream>

using namespace std;

main()
{
    int width,height;                                                         //宣告長與寬 
    
    cout<<"Draw a Rectangle."<<endl<<"Enter width and height."<<endl; 
    
    cout<<"width:";                                                           //輸入長寬直到在範圍內 
    cin>>width;
    while(width<0)
    {          
        cout<<"illegal input\nwidth:";
        cin>>width;
    }
    cout<<"height:";                          
    cin>>height;
    while(height<0)
    {          
        cout<<"illegal input\nheight:";
        cin>>height;
    }
    
    for(int i=1;i<=height;i++)                                                //畫出圖形 
    {
        if(i==1 || i==height)    
            for(int j=1;j<=width;j++)
            {
                  cout<<'*';  
            }
        else
        {
            for(int j=1;j<=width;j++)
            {
                  if(j==1 || j==width)
                      cout<<'*';
                  else
                      cout<<" ";      
            }
        }    
        cout<<endl;  
            
    }
    system("pause");
    return 0;
}
