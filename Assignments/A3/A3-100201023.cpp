#include <iostream>

using namespace std;

int main()
{
	int width , height; // declare input varience

	cout << "Draw a Rectangle." << endl << "Enter width and height." << endl; // output message

	while(true) // read width
	{
		cout << "width: ";
		cin >> width;
		if(width > 0)
			break;
		cout << "illegal input!" << endl;
	}

	while(true) // read height
	{
		cout << "height: ";
		cin >> height;
		if(height > 0)
			break;
		cout << "illegal input!" << endl;
	}

	for(int i = 0 ; i < height ; ++i) // ouput height
	{
		for(int j = 0 ; j < width ; ++j) // output width
		{
			if(i == 0 || i == height - 1 || j == 0 || j == width - 1) // determine output "*" or " "
				cout << "*";
			else
				cout << " ";
		}
		cout << endl;
	}

	return 0;
}
