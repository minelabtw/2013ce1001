#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int width,height;
    cout << "Draw a Rectangle." << endl << "Enter width and height." << endl << "width:";
    cin >> width;
        while (width <= 0)
        {
            cout << "illegal input!" << endl << "width:";
            cin >> width;
        }
    cout << "height:";
    cin >> height;
        while (height <= 0)
        {
            cout << "illegal input!" << endl << "height:";
             cin >> height;
        }
    cout << endl;

    int i,j;
	for(i=1;i<=height;i++)
    {
        for(j=1;j<=width;j++)
        {
            if(i==1 || j==1 || j==width || i==height)
                cout<<'*';
            else
                cout<<' ';
        }
         cout << endl;
    }
    return 0;

}
