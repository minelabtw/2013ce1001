#include<iostream>
using namespace std;

int main()
{
    int a=0;   //宣告型別為整數,並初始化
    int b=0;   //宣告型別為整數,並初始化

    cout<<"Draw a Rectangle.\nEnter width and height.\nwidth:";
    cin>>a;
    while(a<=0)    //重覆循環並設定範圍
    {
        cout<<"illegal input!\nwidth:";
        cin>>a;
    }
    cout<<"height:";
    cin>>b;
    while(b<=0)    //重覆循環並設定範圍
    {
        cout<<"illegal input!\nheight:";
        cin>>b;
    }

    int xMAX=a-1;        //宣告型別為整數
    int yMAX=-b+1;       //宣告型別為整數
    int x=0;             //宣告型別為整數
    int y=0;             //宣告型別為整數

    while(y>=yMAX)       //重覆循環並設定範圍
    {
        while(x<=xMAX)   //重覆循環並設定範圍
        {
            if(y==0 ||y==-b+1)
            {
                cout<<"*";
            }
            else if(x==0 || x==a-1)
            {
                cout<<"*";
            }
            else if(x>0 && x<a)
            {
                cout<<" ";
            }
            x++;
        }
        cout<<endl;
        x=0;
        y--;
    }

    return 0;

}
