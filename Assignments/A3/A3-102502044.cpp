/*************************************************************************
    > File Name: A3-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年10月11日 (週五) 21時18分44秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

/************************************/
/*  str means an string for ui      */
/* *tmp means a point to save score */
/************************************/
void input(char *str, int *tmp)
{
	for(;;)
	{
		printf(str); //input score
		scanf("%d", tmp);

		if(*tmp > 0) //check input is in range (0, unlimit)
			break;
		else
			puts("illegal input!");
	}
}



int main()
{
	int w, h;

	/* print ui */
	puts("Draw a Rectangle.");
	puts("Enter width and height.");
	
	/* input all variable */
	input("width: " , &w);
	input("height: ", &h);

	/* print the rectangle */
	for(int i=0 ; i<h ; putchar('\n'), i++)
		for(int j=0 ; j<w ; j++)
		{
			/* check the cell must be star */
			if(i*j==0 || i==h-1 || j==w-1)
				putchar('*');
			else
				putchar(' ');
		}
	
	return 0;
}

