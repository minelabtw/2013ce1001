#include<iostream>
using namespace std;

int main ()
{
    int x = 1;                 //x,y初始值設定為1
    int y = 1;

    int width = 0;
    int height = 0;

    cout <<"Draw a Rectangle.\nEnter width and height.\nwidth: ";
    cin >>width;
    while ( width <= 0)                             //長、寬大於0
    {
        cout <<"illegal input!\nwidth: ";
        cin >>width;
    }

    cout <<"height: ";                                    //長、寬大於0
    cin >>height;
    while ( height <= 0)
    {
        cout <<"illegal input!\nheight: ";
        cin >>height;
    }

    while (y<= height)
    {
        while (x<= width)
        {

            if (x == 1)                       //如果x=1 或 x=width 或 y=1 或  y=height 出現* (中空)
                cout<<"*";
            else if (x == width )
                cout<<"*";
            else if (y == 1)
                cout<<"*";
            else if (y == height )
                cout<<"*";
            else
                cout<<" ";
            x++;                                 //x+1
        }

        cout<< endl;
        x = 1;                                    //x回到初始值
        y++;                                      //y+1
    }

    return 0;
}
