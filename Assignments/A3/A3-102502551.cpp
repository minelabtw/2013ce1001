#include <iostream>

using namespace std;

int main()
{
    int width,height;


    cout<<"Draw a Rectangle."<<endl<<"Enter width and height."<<endl<<"width: ";
    cin>>width;
    while(width<=0)                                                                          //檢查數字是否在範圍內
        {
            cout<<"illegal input!"<<endl;
            cout<<"width: ";
            cin>>width;
        }
    cout<<"height: ";
    cin>>height;
    while(height<=0)
        {
            cout<<"illegal input!"<<endl;
            cout<<"height: ";
            cin>>height;
        }
    for(int y=1;y<=height;y++)                                                              //畫出圖形
        {
            for(int x=1;x<=width;x++)
            {
                if (y==1)
                {
                    cout<<"*";
                }
                else if (x==1)
                {
                    cout<<"*";
                }
                else if (x==width)
                {
                    cout<<"*";
                }
                else if (y==height)
                {
                    cout<<"*";
                }
                else
                {
                    cout<<" ";
                }
            }
            cout<<endl;

        }
    return 0;
}
