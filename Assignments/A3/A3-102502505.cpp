#include <iostream>

using namespace std;

int main()
{
    int width;//宣告變數
    int height;
    int x = 1;//宣告變數x並將其初始值設為1
    int y = 1;

    cout << "Draw a Rectangle." << endl;//輸出字串
    cout << "Enter width and height." << endl;

    cout << "width: ";
    cin >> width;//輸入width變數
    while ( width <= 0 )//限制width的條件-不能小於零
    {
        cout << "illegal input!" << endl;//假如變數範圍錯誤，輸出此字串
        cout << "width: ";//重新輸入變數
        cin >> width;
    }

    cout << "height: ";
    cin >> height;
    while ( height <= 0 )
    {
        cout << "illegal input!" << endl;
        cout << "height: ";
        cin >> height;
    }


    if ( y = 1 )//顯示第一行星星
    {
        while (x <= width )//使用迴圈，使星星數和輸入的width變數數目相同
        {
            cout << "*";
            x++;
        }
        cout << endl;
        y++;
    }

    x = 1;//使x回歸初始值
    while ( y < height )//使用迴圈做出中間部分
    {
        while ( x <= width)//x等於一或width時出現星星，反之則出現空格
        {
            if ( x == 1 )
            {
            cout << "*";
            x++;
            }
            if ( x == width )
            {
            cout << "*";
            x++;
            }
            else
            {
            cout << " ";
            x++;
            }
        }

        cout << endl;
        x = 1;//使x值初始化
        y++;//y值加一，往下個條件前進
    }

    if ( y = height )//做出最後一行星星
    {
        while (x <= width )//使用迴圈使星星數和width變數相同
        {
            cout << "*";
            x++;
        }
        cout << endl;
        y++;
    }

    return 0;

}
