#include <iostream>
using namespace std;


int main()
{
    int number1;                                                                          //設定一個整數變數，初始化其數值為0。
    int number2;                                                                          //設定一個整數變數，初始化其數值為0。
    int x=1;                                                                              //設定一個整數變數為x，初始化其數值為1。
    int y=1;                                                                              //設定一個整數變數為y，初始化其數值為1。

   cout<<" Draw a Rectangle."<<endl<<"Enter width and height."<<endl<<"width: ";          //在螢幕上顯示Draw a Rectangle.換行之後顯示Enter width and height.在換行顯示width:
   cin>>number1;                                                                          //請輸入一個數字
   while (number1<0)                                                                      //當整數變數大於零時
   {
       cout<<"illegal input!"<<endl<<"width:";                                           //在螢幕上顯示"illegal input!"換行後顯示width:(長度)
       cin>>number1;                                                                     //請輸入一個數字

   }
   cout<<"height:";                                                                     //在螢幕上顯示height:(寬度)
   cin>>number2;                                                                        //請輸入一個數字
   while (number2<0)                                                                    //當整數變數大於零時
    {
        cout<<"illegal input!"<<endl<<"height:";                                        //在螢幕上顯示"illegal input!"換行後顯示height:(寬度)
        cin>>number2;                                                                   //請輸入一個數字

    }
    while (x<=number1)                                                                 //宣告x大於等於整數變數1時
    {
        while (y<=number2)                                                             //宣告Y大於等於整數變數2時
        {
            if (x==1)                                                                  //當X等於1
            cout<<"*";                                                                //在螢幕上出現*號
            else if (x==number1)                                                      //執行條件 X等於整數變數1時
            cout<<"*";                                                                //在螢幕上顯示*號
            else if  (y==1)                                                          //執行條件y等於1
            cout<<"*";                                                               //在螢幕上顯示*號
            else if (y==number2)                                                     //執行條件Y等於整數變數2時
            cout<<"*";                                                              //在螢幕上顯示*號
            else
            cout<<" ";                                                              //在螢幕上顯示空白
            y++;
        }
        x++;
        cout<<endl;
        y=1;

    }
return 0;


}
