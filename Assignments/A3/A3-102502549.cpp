#include <iostream>
using namespace std;

int main()
{
    //宣告長度和寬度
    int height=0;
    int width=0;

    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;

    //檢驗長是否合法
    while(true)
    {
        cout<<"width: ";
        cin>>width;

        if(width<=0)
            cout<<"illegal input!"<<endl;
        else
            break;
    }

    //檢驗高是否合法
    while(true)
    {
        cout<<"height: ";
        cin>>height;

        if(height<=0)
            cout<<"illegal input!"<<endl;
        else
            break;
    }

    //畫圖
    for(int y=1; y<=height; y++)
    {
        for(int x=1; x<=width; x++)
        {
            if(y==1||y==height||x==1||x==width)
                cout<<"*";
            else
                cout<<" ";
        }
        cout<<endl;
    }

    return 0;
}
