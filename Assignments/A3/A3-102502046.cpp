#include<iostream>
using namespace std;

int main ()
{
    cout << "Draw a Rectangle." << endl;            //印出"Draw a Rectangle."
    cout << "Enter width and height." << endl;      //"Enter width and height."

    int a=0;                //宣告一整數a,起始值為0
    int b=0;                //宣告一整數b,起始值為0

    cout << "width: " ;
    cin >> a;               //鍵入a值
    while (a<=0)            //當a<=0時，進入while迴圈
    {
        cout << "illegal input!" <<endl;
        cout << "width: " ;
        cin >> a;           //重新輸入b值
    }

    cout << "height: " ;
    cin >> b;               //鍵入b值
    while (b<=0)
    {
        cout << "illegal input!" <<endl;
        cout << "width: " ;
        cin >> b;           //重新輸入b值
    }

    int x=0;
    int y=0;

    while (y>=(-b)+1)       //當y>=(-b)+1時進入while迴圈
    {
        while (x<=a-1)      //當x<=a-1時進入while迴圈
        {
            if ( x==0 || y==0 || x==a-1 || y==-b+1 )    //如果x=0 或 y=0 或 x=a-1 或 y=-b+1 時。輸出"*"
                cout << "*" ;
            else                                        //其他輸出" "
                cout << " " ;

            x++;                  //x+1
        }
        cout << endl;
        x=0;                      //x回復為0
        y--;                      //y-1
    }

return 0;

}
