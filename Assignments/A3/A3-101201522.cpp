#include <iostream>
using namespace std;

int main(){
    int width,height,i,j;//宣告整數width為寬,height為高,i、j為迴圈用的變數 
    
    cout << "Draw a Rectangle.\n";
    cout << "Enter width and height.\n";
    
    while(1){//輸入寬,如果輸入錯誤,則輸出錯誤輸入,並重複詢問 
        cout << "width: ";
        cin >> width;
        if(width<=0)
            cout << "illegal input!\n";
        else
            break;
    }
    
    while(1){//輸入高,如果輸入錯誤,則輸出錯誤輸入,並重複詢問 
        cout << "height: ";
        cin >> height;
        if(height<=0)
            cout << "illegal input!\n";
        else
            break;
    }
    
    for(i=0;i<height;i++){//i控制高度的位置從0到height-1 
        for(j=0;j<width;j++){//j控制寬度的位置從0到width-1
            if(!(i%(height-1)) || !(j%(width-1)))//如果為矩形邊界,輸出*;反之輸出空白 
                cout << "*";
            else
                cout << " ";
        }
        cout << "\n";//改變高度位置前先換行 
    }
    
    return 0;
}
