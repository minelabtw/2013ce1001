#include <iostream>

using namespace std;

int main()
{
    int width;  //變數長方形的寬
    int height;  //變數長方形的長

    cout << "Draw a Rectangle." << endl;  //畫長方形
    cout << "Enter width and height." << endl;  //請使用者輸入長和寬

    do{
        cout << "width: ";  //請使用者輸入寬
        cin >> width;  //輸入寬
        if(width<=0){  //檢查有無超過範圍
            cout << "illegal input!" << endl;  //告知超出範圍
        }
    }while(width<=0);  //當寬超出範圍再做一次
    do{
        cout << "height: ";  //請使用者輸入長
        cin >> height;  //輸入長
        if(height<=0){  //檢查有無超過範圍
            cout << "illegal input!" << endl;  //告知超出範圍
        }
    }while(height<=0);  //當寬超出範圍再做一次

    int x;  //設變數x
    int y;  //設變數y
    for(y = 0;y < height;y++){  //輸出長方形
        for(x = 0;x < width;x++){  //輸出列
            if(y == 0||y == height - 1){  //當y等於零或是高減一
                cout << "*";  //輸出*
            }else if(x == 0||x == width - 1){  //當x等於零或是寬減一
                cout << "*";  //輸出*
            }else{
                cout << " ";  //都不是就輸出空白
            }
            if(x == width - 1){  //當x等於寬減一時換行
                cout << endl;
            }
        }
    }

    return 0;

}
