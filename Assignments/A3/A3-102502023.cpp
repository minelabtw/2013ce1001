#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int width,height; //initialize integer width and height
    cout << "Draw a Rectangle.\n" << "Enter width and height.\n" << "width:";
    cin >> width;
    while(width <= 0) //loop. to check whether width is on demand
    {
        cout << "illegal input!\n" << "width:";
        cin >> width;
    }
    cout << "height:";
    cin >> height;
    while(height <= 0) //loop. to check whether width is on demand
    {
        cout << "illegal input!\n" << "height:";
        cin >> height;
    }

    int xMax=width; //initialize the integers for the following loop
    int yMax=height;
    int x=1;
    int y=1;

    while(y<=yMax) //loop to draw the hollow rectangle
    {
        while(x<=xMax)
        {
            if(x==1 || x==xMax || y==1 || y==yMax)
            {
                cout << "*";
            }
            else
            {
                cout << " ";
            }
            x++;
        }
        cout<<endl;
        x=1;
        y++;
    }
    return 0;
    return 0;
}
