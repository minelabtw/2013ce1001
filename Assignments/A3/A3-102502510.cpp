#include <iostream>

using namespace std;

int main()
{
    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;
    int width=0;
    int height=0;
    cout << "width: ";
    while(cin >> width)//輸入寬
    {
        if(width>0)//判斷合理性
            break;
        cout << "illegal input!" << endl;
        cout << "width: ";
    }
    cout << "height: ";
    while(cin >> height)//輸入高
    {
        if(height>0)//判斷合理性
            break;
        cout << "illegal input!" << endl;

        cout << "height: ";
    }
    for(int i=0; i<width; ++i)//第一行
    {
        cout << "*";
    }
    cout << endl;
    for(int i=0; i<height-2; ++i)//中間
    {
        cout << "*";
        for(int j=0; j<width-2; ++j)
        {
            cout << " ";
        }
        if(width-2>=0)
        {
            cout << "*" ;
        }
        cout << endl;
    }
    if(height>1)//最後一行
    {
        for(int i=0; i<width; ++i)
        {
            cout << "*";
        }
        cout << endl;
    }

    return 0;
}
