#include <string>
#include <iostream>

using namespace std;

int main()
{
    int WIDTH;
    int HEIGHT;
    int i;  //宣告i來表示動作

    cout << "Draw a Rectangle." << endl << "Enter width and height." << endl;

    for (i=0; i<2; i++)  //用for來重覆詢問
    {
        if (i==0)
        {
             cout << "Width: ";
             cin >> WIDTH;
        }
        else
        {
             cout << "height: ";
             cin >> HEIGHT;
        }

        if (WIDTH<1)  //用if來確定長度為大於0的整數
        {
            cout << "illegal input!" << endl;
            i=i-1;  //若非大於0之整數，則回到上一動作
        }

        if (HEIGHT<1)  //用if來確定高度為大於0的整數
        {
            cout << "illegal input!" << endl;
            i=i-1;
        }

    }

    if (WIDTH>2&&HEIGHT>2)  //用if區別長寬大於2和小於2的做法
    {
       string border(WIDTH-2,'*');  //宣告邊長，並使用"*"來作圖
       string spaces(WIDTH-2,' ');  //宣告空白部分
       cout << '*' << border << '*' << endl;
       for (i=1; i<HEIGHT-1; ++i)
       {
           cout << '*' << spaces << '*' << endl;
       }
       cout << '*' << border << '*' << endl;
    }
    else
    {
       string border(WIDTH,'*');
       cout << border << endl;
       for (i=1; i<HEIGHT; ++i)
       {
       cout << border << endl;
       }
    }

    return 0;
}
