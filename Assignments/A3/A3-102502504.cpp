#include <iostream>

using namespace std;

int main()
{
    int width;
    int height; 
    int x;
    int y = 1;
    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." <<endl;

    cout << "width:";
    cin >> width ;
    while( width<=0 ) //運用while迴圈，限制width只能輸入自然數 
    {
        cout << "illegal input!" << endl << "width:" ; //若超出指定範圍則顯示illegal input!
        cin >> width ; //重新輸入width值 
    }

    cout << "height:";
    cin >> height;
    while( height<=0 ) //運用while迴圈，限制height只能輸入自然數  
    {
        cout << "illegal input!" << endl << "height:" ; //若超出指定範圍則顯示illegal input!
        cin >> height ; //重新輸入height值  
    }

    while ( y<=height ) //運用while迴圈，選定"列" 
    {
    	x=1; //初始化x值為1 
        while ( x<=width ) //運用while迴圈，選定"行" 
        {
            if (y==1 or y==height ) //畫出圖形的上下邊 
			cout << "*"; 
            else if ( y!=1 && x==1 ) //畫出圖形的左界 
            cout << "*"; 
            else if ( y!=1 && x==width ) //畫出圖形的右界 
            cout << "*";
            else //其餘空間為空白 
            cout << " ";  
            x++; //換行 
            
        }
        cout<<endl;
        y++; //換列 
    }

    return 0;
}
