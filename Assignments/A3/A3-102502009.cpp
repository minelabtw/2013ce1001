#include<iostream>
using namespace std;

int main()
{
    int width=0;
    int height=0;
    int squarex=0;
    int squarey=0;

    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;

    cout<<"width: "; //輸入寬度
    cin>>width;

    while(width<=0) //若寬度小於或等於零，則重新輸入
    {
        cout<<"illegal input!"<<endl;

        cout<<"width: ";
        cin>>width;
    }

    cout<<"height: "; //輸入高度
    cin>>height;

    while(height<=0) //若高度小於或等於零，則重新輸入
    {
        cout<<"illegal input!"<<endl;

        cout<<"height: ";
        cin>>height;
    }

    for(squarey=0;squarey<height;squarey++) //建立y座標，大小等於長度
    {
        for(squarex=0;squarex<width;squarex++) //建立x座標，大小等於寬度
        {
            if(squarex==0) //x座標=0時，輸出*
                cout<<"*";
            else if(squarex==width-1) //x座標=寬度-1時，輸出*
                cout<<"*";
            else if(squarey==0) //y座標=0時，輸出*
                cout<<"*";
            else if(squarey==height-1) //y座標=高度-1時，輸出*
                cout<<"*";
            else  //其餘輸出空白
                cout<<" ";
        }
        cout<<endl;
    }

    return 0;

}
