#include<iostream>
using namespace std;

int main()
{
    cout << "Draw a Rectangle.\nEnter width and height." << endl;    //將Draw a Rectangle.\nEnter width and height.輸出至螢幕。

    int width=0;     //宣告一變數width，並使其值為0。
    int height=0;    //宣告一變數height，並使其值為0。

    cout << "width=";    //將width=輸出至螢幕。
    cin >> width;        //將值輸入至width。

    while(width <= 0)    //while迴圈限制範圍在width<=0。
    {
        cout << "illegal input!" << endl;    //將illegal input!輸出至螢幕。
        cout << "width=";    //將width=輸出至螢幕。
        cin >> width;        //將值輸入至width。
    }

    cout << "height=";    //將height=輸出至螢幕。
    cin >> height;        //將值輸入至height。

    while(height <= 0)
    {
        cout << "illegal input!" << endl;    //將illegal input!輸出至螢幕。
        cout << "height";     //將height=輸出至螢幕。
        cin >> height;        //將值輸入至height。
    }

    int x=1;    //宣告一變數x，並使其值為1。
    int y=1;    //宣告一變數y，並使其值為1。

    while(y <= height)    //while迴圈限制範圍在y<=height。
    {
        while(x <= width)    //while迴圈限制範圍在x<=width。
        {
            if(y == 1 || x == 1 || y == height || x == width)    //運算
                cout << "*";
            else
                cout << " ";
            ++x;
        }
        cout << endl;     //換行
        x=1;
        y++;
    }
    return 0;
}
