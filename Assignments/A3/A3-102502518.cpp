#include <iostream>

using namespace std;

int main()
{
    int width = 0;                              //宣告型別為整數(int)的width，並初始化其數值為0。
    int height = 0;                             //宣告型別為整數(int)的height，並初始化其數值為0。
    int width_variation = 1;                    //宣告型別為整數(int)的width_variation，並初始化其數值為0。
    int height_variation = 1;                   //宣告型別為整數(int)的height_variation，並初始化其數值為0。

    cout << "Draw a Rectangle." << endl;        //將字串"Draw a Rectangle."輸出到螢幕上。
    cout << "Enter width and height." << endl;  //將字串"Enter width and height."輸出到螢幕上。

    cout << "width:";                           //將字串"width:"輸出到螢幕上。
    cin >> width ;                              //從鍵盤輸入，並將輸入的值給width。

    while ( width <= 0 )                        //使用while迴圈(當width <= 0)。
      {
      cout << "illegal input!" << endl;        //將字串"illegal input!"輸出到螢幕上。
      cout << "width:";                         //將字串"width:"輸出到螢幕上。
      cin >> width ;                            //從鍵盤輸入，並將輸入的值給width。
      }

    cout << "height:";                          //將字串"height:"輸出到螢幕上。
    cin >> height ;                             //從鍵盤輸入，並將輸入的值給height。

    while ( height <= 0 )                       //使用while迴圈(當height <= 0)。
      {
      cout << "illegal input!" << endl;        //將字串"illegal input!"輸出到螢幕上。
      cout << "height:";                        //將字串"height:"輸出到螢幕上。
      cin >> height ;                           //從鍵盤輸入，並將輸入的值給height。
      }
    while ( height_variation <= height )        //使用while迴圈(當height_variation <= height)。
    {

     while ( width_variation <= width )         //使用while迴圈(當width_variation <= width)。
     {

       if ( width_variation == 1 or height_variation == 1 or width_variation == width or height_variation == height ) //使用if(如果width_variation == 1 or height_variation == 1 or width_variation == width or height_variation == height)
       {
       cout << "*";                             //將"*"輸出到螢幕上。
       width_variation++;                       //將width_variation加一並存回width_variation。
       }

       else                                     //其餘未判斷的。
       {
       cout << " ";                             //將" "輸出到螢幕上。
       width_variation++;                       //將width_variation加一並存回width_variation。
       }

     }
     cout << endl;                              //換行。
     width_variation=1;                         //將width_variation的值初始化為一。
     height_variation++;                        //將height_variation加一並存回height_variation。

    }
    return 0;
}
