#include <iostream>
using namespace std;

int main()
{
    int w = 0;                                      //設定w,h為長寬所需變數
    int h = 0;
    int x = 1;                                      //設定x,y為繪圖所需變數
    int y = 1;

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    cout << "width:";
    cin >> w;
    while (w<=0)                                    //判斷w是否符合所需範圍
    {
        cout << "illegal input!" << endl;
        cout << "width:";
        cin >> w;
    }

    cout << "height:";
    cin >> h;
    while (h<=0)                                    //判斷h是否符合所需範圍
    {
        cout << "illegal input!" << endl;
        cout << "height:";
        cin >> h;
    }

    while (y<=h)
    {
        while (x<=w)
        {
            if (y==1 || y==h)                       //如果該點為上下兩邊，則輸出*
                cout << "*";
            else if (x==1 || x==w)                  //如果該點為左右兩邊，則輸出*
                cout << "*";
            else                                    //如果該點不為邊線，則顯示空白
                cout << " ";
            x++;                                    //重複累加直到x等於所需的寬度
        }
        cout << endl;                               //換行
        x=1;                                        //重置x回1
        y++;                                        //重複累加直到y等於所需的長度
    }
    return 0;
}
