#include<iostream>
using namespace std;

int main()
{
    int width=0,height=0;           //宣告型別為 整數(int) 的變數 長與寬
    int x=1,y=1;

    cout << "Draw a Rectangle.\n";
    cout << "Enter width and height.\n";
    while(width<=0)                             //重覆詢問並輸入變數直到正確
        {
        cout << "width: ";cin >> width;
        if(width<=0)cout << "illegal input!\n";
        }
    while(height<=0)
        {
        cout << "height: ";cin >>height;
        if(height<=0)cout << "illegal input!\n";
        }
    while(y<=height)                                    //畫出圖形
        {
        while(x<=width)
            {
            if(y==1 || y==height || x==1 || x==width)   //判斷是否為邊
                {
                cout << "*";
                }
            else
                {
                cout << " ";
                }
            x++;
            }
        cout << endl;
        x=1;
        y++;
        }
    return 0;
}
