#include <iostream>
using namespace std;
int main()
{
    int width ;//宣告變數
    int height ;
    int i ;
    int j ;
    cout<<"Draw a Rectangle."<<endl ;
    cout<<"Enter width and height."<<endl ;
    cout<<"width:" ;
    cin>>width;
    while(width<0)//當此情形發生時,進入迴圈
    {
        cout<<"illegal input!"<<endl<<"width:";//輸出illegal input!,換行跑出a:
        cin>>width;//輸入width
    }
    cout<<"height:" ;
    cin>>height;
    while(height<0)//當此情形發生時,進入迴圈
    {
        cout<<"illegal input!"<<endl<<"height:";//輸出illegal input!,換行跑出a:
        cin>>height;//輸入height
    }

    j=height-1 ; //給width一個初始值
    while(j>=0)
    {
        i=1 ;//給i一個初始值為0
        while(i<=width)
        {

            if(j==height-1)  //印出最上面那一排*
                cout<<"*" ;
            else if (j!=height&&i==1)//印出除了最左上角*以外其他左邊整排*
                cout<<"*" ;
            else if (j!=height&&i==width)//印出除了最右上角*以外其他右邊整排*
                cout<<"*" ;
            else if (j==0&&i>0&&i<width)//印出最下面那一排*除了第一個跟最後一個
                cout<<"*" ;
            else
                cout<<" " ;//剩下印空白
            i++ ;
        }

        j-- ;
        cout<<endl ;
    }


    return 0 ;


}
