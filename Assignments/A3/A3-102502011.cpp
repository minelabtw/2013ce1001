#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    cout << "Draw a Rectangle." << endl ;  //輸出Draw a Rectangle.
    cout << "Enter width and height." << endl ; //輸出Enter width and height.

    int width = 0 , height = 0 ; //宣告長和寬兩個整數 並將其數值化為零

    cout << "width: " ;  //輸入width之值
    cin >> width ;

    while (width <= 0 )
    {
        cout << "illegal input!" << endl ;  //將條件設定為數值必須為正整數,若錯誤則跳出illegal input!並要求重新輸入
        cout << "width: " ;
        cin >> width ;
    }

    cout << "height: " ; //輸入width之值
    cin >> height ;

    while (height <= 0 )
    {
        cout << "illegal input!" <<endl ;
        cout << "height: " ;
        cin >> height ;
    }

    int xMax = width ;   //宣告x=1,y=-1和xMax=width,yMax=-height
    int yMax = -height ;
    int x = 1 ;
    int y = -1 ;

    while (y >= yMax )
    {
        while(x <= xMax )  //二次迴圈,才能跑出XY兩軸的坐標系
        {
            if (x == 1)
                cout << "*" ;   //矩形最左方的邊
            else if (x == xMax )
                cout << "*" ; //矩形最右方的邊
            else if (y == -1)
                cout << "*" ; //矩形最上方的邊
            else if (y == yMax )
                cout << "*" ; //矩形最下方的邊
            else
                cout << " " ; //其餘為空白
            x++ ;
        }
        cout << endl ;
        x = 1 ; //當x跑完一行,在下一行重新以x=1開始
        y-- ; //因為y上為正,下為負,所以下一行必須讓y減一
    }
    return 0 ;
}
