#include <iostream>
using namespace std;
int main()
{
    int a = 0;     //宣告變數a
    int b = 0;     //宣告變數b

    cout <<"Draw a Rectangle."<< endl <<"Enter width and height."<< endl <<"width: "; //輸出文字
    cin >> a;      //輸入變數a
    while (a <= 0) //重複判斷a是否小於等於零
    {
        cout <<"illegal input!"<< endl <<"width: ";
        cin >> a;
    }

    cout << "height: "; //輸出文字
    cin >> b;           //輸入變數b
    while (b <= 0)      //重複判斷b是否小於等於零
    {
        cout <<"illegal input!"<< endl <<"height: ";
        cin >> b;
    }

    int x = 1;         //宣告x的起始值為1
    int y = 1;         //宣告y的起始值為1

    while (y <= b)     //重複判斷y是否小於等於b
    {
        while (x <= a) //重複判斷x是否小於等於a
        {
            if(x == 1 || x == a || y == 1 || y == b) //判斷是否在矩形邊長的位置
            {
                cout <<"*";
            }
            else
            {
                cout <<" ";
            }
            x++; //x值跳至x+1值
        }
        x = 1;   //使x值回到1
        cout << endl; //換行
        y++;     //y值跳至y+1值
    }
    return 0;
}
