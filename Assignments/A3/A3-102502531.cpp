#include <iostream>

using namespace std;

int main ()
{
    int a=0; //宣告一個變數a,並初始化其數值為0。
    int b=0; //宣告一個變數b,並初始化其數值為0。
    int amini=0; //宣告一個數xmax,並初始化其數值為0。
    int bmini=0; //宣告一個數ymax,初始化其數值為0。
    int x=0;  //宣告一個變數x,初始化其數值為0。
    int y=0;  //宣告一個變數y,初始化其數值為0。


    cout<<"Draw a Rectangle."<<endl; //將"Draw a Rectangle."儲存於cout,並印在銀幕上。
    cout<<"Enter width and height."<<endl;   //將"Enter width and height."儲存於cout,並印在銀幕上。
    do //進入此一迴圈
    {
        cout << "width:"; //將"width:"儲存於cout,並印在銀幕上。
        cin>>a;//輸入變數a的值。
        x=a; //將a的值放進b裡。
        if (a<=0)
            cout << "illegal input!"<<endl; //如果a<0則將"illegal input!"印出螢幕。
    }
    while(a<=0);  //判斷是否還需不需繼續迴圈。

    do //進入此一迴圈
    {
        cout << "height:"; //將"height:"儲存於cout,並印在銀幕上。
        cin>>b; //輸入變數b的值。
        y=b; //將b的值放進y裡。
        if (b<=0)
            cout << "illegal input!"<<endl; //如果b<0則將"illegal input!"印出螢幕。
    }
    while(b<=0);   //判斷是否還需不需繼續迴圈。

    while(b>amini) //進入此一迴圈(第一層)。
    {
        while(a>bmini) //進入此一迴圈(第二層)。
        {
            if(a==1||a==x||b==1||b==y) //用來製造外框的星星,當a==1||a==x||b==1||b==y時則印出"*"。
                cout<<"*";
            else
                cout<<" ";
            a--; //每次a都向下減1。
        }
        cout<<endl; //換到下一行。
        a=x; //使a重新回到x的值。
        b--; //每次b都向下減1。

    }

}


