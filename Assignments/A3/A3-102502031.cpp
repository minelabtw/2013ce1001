#include <iostream>

using namespace std;

int main()
{
//varible declartion
    int x=1;
    int y=1;
    int width=0;
    int height=0;

    cout << "Draw a Rectangle.\n";          //tell user the purpose of the program
    cout << "Enter width and height.\n";    //prompt user for needing data

    cout << "width: ";                      //prompt user for data
    cin >> width;                           //read the first integer into width
    while ( width <= 0 )                    //Check if data form user is in the range. If yes, prompt for the next data. If no, ask for width again.
    {
        cout << "illegal input!\n";         //tell user the data is out of range
        cout << "width: ";                  //prompt for data again
        cin >> width;                       //read the first integer into width again
    }

    cout << "height: ";                     //prompt user for data
    cin >> height;                          //read the second integer into height
    while ( height <= 0 )                   //Check if data form user is in the range. If yes, calculate for graph, and show it. If no, ask for hieght again.
    {
        cout << "illegal input!\n";         //tell user the data is out of range
        cout << "height: ";                 //prompt for data again
        cin >> height;                      //read the second integer into hieght again
    }

    while ( y >= 1 && y <= height )         //graph the rectangle
    {
        if ( y ==1 || y == height )         //graph the side of rectangle
        {
            while ( x >= 1 && x <= width )  //graph "*" at the side of rectangle
            {
                cout << "*";
                x=x+1;
            }
            cout << endl;
            x=1;
            y=y+1;
        }
        else                                //graph " " inside of rectangle and graph "*" at the side of rectangle
        {
            while ( x >= 1 && x <= width )
            {
                if ( x == 1 || x == width ) //graph "*" at the side of rectangle
                {
                    cout << "*";
                    x=x+1;
                }
                else
                {
                    cout << " ";            //graph " " inside of rectangle
                    x=x+1;
                }
            }
              cout << endl;
            x=1;
            y=y+1;
        }
    }
}
