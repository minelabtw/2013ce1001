#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int width=0 ;                                               //宣告型別為 整數(int) 的寬，並初始化其數值為0。
    int height=0 ;                                              //宣告型別為 整數(int) 的高，並初始化其數值為0。
    int a=0 ;                                                   //宣告型別為 整數(int) 的作圖時的列，並初始化其數值為0。
    int b=0 ;                                                   //宣告型別為 整數(int) 的作圖時的行，並初始化其數值為0。

    cout << "Draw a Rectangle."<<"\n"<<"Enter width and height."<<"\n" ;    //顯示Draw a Rectangle.  Enter width and height.
    cout << "width:" ;                                          //顯示width:
    cin >> width ;                                              //輸入寬
    while (width<=0)                                            //數值錯誤要求重新輸入
    {
        cout << "illegal input!\n" ;
        cout << "width:" ;
        cin >> width ;
    }
    cout << "height:" ;                                         //顯示height:
    cin >> height ;                                             //輸入高
    while (height<=0)                                           //數值錯誤要求重新輸入
    {
        cout << "illegal input!\n" ;
        cout << "height:" ;
        cin >> height ;
    }
    for (a=0;a<height;++a)
    {
        for (b=0;b<width;++b)
        {
            if (a==0 || a==height-1 || b==0 || b==width-1)      //用*畫出邊框
                cout << "*" ;
            else                                                //其餘都是"  "
                cout << " " ;
        }
        cout << endl ;                                          //每行結束時換行
    }
    return 0;
}
