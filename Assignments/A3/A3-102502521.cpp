#include <iostream>

using namespace std;

int main()
{
    int w;                    //宣告名稱為w的整數變數
    int h;                    //宣告名稱為h的整數變數
    int wc=1;                 //宣告名稱為wc的整數,將其值設為1
    int hc=1;                 //宣告名稱為hc的整數,將其值設為1

    cout<<"Draw a Rectangle."<<endl<<"Enter width and height."<<endl;  //將字串"Draw a Rectangle."換行"Enter width and height."換行輸出到螢幕上

    cout<<"width: ";    //將字串"width: "輸出到螢幕上
    cin>>w;             //從鍵盤輸入，並將輸入的值給w
    while(w<=0)         //使用while迴圈，若w數值小於等於0就會一直重複該程式碼內的動作
    {
        cout<<"illegal input!"<<endl;   //將字串"illegal input!"輸出到螢幕上並換行
        cout<<"width: ";                //將字串"width: "輸出到螢幕上
        cin>>w;                         //從鍵盤輸入，並將輸入的值給w
    }
    cout<<"height: ";   //將字串"height: "輸出到螢幕上
    cin>>h;             //從鍵盤輸入，並將輸入的值給h
    while(h<=0)         //使用while迴圈，若h數值小於等於0就會一直重複該程式碼內的動作
    {
        cout<<"illegal input!"<<endl;   //將字串"illegal input!"輸出到螢幕上並換行
        cout<<"height: ";               //將字串"height: "輸出到螢幕上
        cin>>h;                         //從鍵盤輸入，並將輸入的值給h
    }

    while(hc<=h)        //使用while迴圈，若hc數值小於等於h就會一直重複該程式碼內的動作
    {
        while(wc<=w)    //使用while迴圈，若wc數值小於等於w就會一直重複該程式碼內的動作
        {
            if(hc==1||hc==h)        //利用if,else if,else判斷什麼地方應該放什麼符號
            {
                cout<<"*";          //將字串"*"輸出到螢幕上
            }
            else if(wc==1||wc==w)
            {
                cout<<"*";          //將字串"*"輸出到螢幕上
            }
            else
            {
                cout<<" ";          //將字串" "輸出到螢幕上
            }
            wc++;                   //讓wc之值遞增,公差為1
        }
        cout<<endl;                 //換行
        wc=1;                       //重新設定wc為1
        hc++;                       //讓hc之值遞增,公差為1
    }

    return 0;
}
