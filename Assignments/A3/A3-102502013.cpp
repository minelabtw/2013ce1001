#include <iostream>

using namespace std;

int main()
{
    int width;                                    //宣告一個名稱為width整數變數
    int height;                                   //宣告一個名稱為height整數變數
    int x = 1;                                    //將整數變數的值初始化為1
    int y;                                        //宣告一個名稱為y整數變數
        cout << "Draw a Rectangle." << endl;      //輸出字串Draw a Rectangle.
        cout << "Enter width and height." << endl;//輸出字串Enter width and height.
        cout << "width: ";                        //輸出字串width:
        cin >> width;                             //輸入width
    while (width<=0){                             //當width<=0
        cout << "illegal input!" << endl;         //輸出字串illegal input!
        cout << "width: ";                        //輸出字串width:
        cin >> width;                             //輸入width
    }
        cout << "height: ";                       //輸出字串height:
        cin >> height;                            //輸入height
    while (height<=0){                            //當height<=0
        cout << "illegal input!" << endl;         //輸出字串illegal input!
        cout << "height: ";                       //輸出字串height:
        cin >> height;                            //輸入height
    }
    y = height;                                   //將height的值置於y
    while (y>0){                                  //當y>0
        while (x<=width){                         //當x<=width
            if (y==height||x==1)                  //判斷y=height或x=1
                cout << "*";                      //輸出字串*
            else if(x==width||y==1)               //判斷x=width或y=1
                cout << "*";                      //輸出字串*
            else
                cout << " ";                      //輸出字串
                x++;                              //x+1
        }
        cout << endl;
        x = 1;                                    //將x拉回1
        y--;                                      //y-1
    }
    return 0;
}
