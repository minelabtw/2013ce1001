#include <iostream>
using namespace std;

int main()
{
    int a;  //宣告名稱為a的整數變數
    int b;  //宣告名稱為b的整數變數
    int x=0;  //宣告名稱為x的整數變數,將其值設為0
    int y=0;  //宣告名稱為y的整數變數,將其值設為0


    cout << "Draw a Rectangle." << endl << "Enter width and height." << endl <<"width:";  //輸出字串
    cin >> a;  //將輸入的值給a

    while(a<0) //使用while迴圈，若a數值小於0就會一直重複該程式碼內的動作
    {
        cout <<"illegal input!" <<endl <<"width:";  //將字串"illegal input!"輸出並換行輸出字串"width:"
        cin >> a;
    }
    cout << "height:";
    cin >> b;  //將輸入的值給b

    while (b<0) //使用while迴圈，若b數值小於0就會一直重複該程式碼內的動作
    {
        cout << "illegal input!" << endl << "height:";
        cin >> b;
    }


    while (y<=b) //使用while迴圈，若y數值小於等於b就會一直重複該程式碼內的動作
    {
        while (x<=a)  //使用while迴圈，若x數值小於等於a就會一直重複該程式碼內的動作
        {
            if (x==0)
                cout << "*";
            else if (x==a)
                cout << "*";
            else if (y==0)
                cout << "*";
            else if (y==b)
                cout << "*";
            else
                cout << " ";  //中空

            x++;  //x遞增
        }
        y++;  //y遞增
        cout <<endl;
        x=0;  //將x重新設定為0
    }
    return 0;

}

