#include <iostream>

using namespace std;

int main()
{
    int width = 0;      //宣告名稱為width的整數，並初始化其數值為0。
    int height = 0;     //宣告名稱為height的整數，並初始化其數值為0。
    int x = 0;          //宣告名稱為x的整數，並初始化其數值為0。
    int y = 0;          //宣告名稱為y的整數，並初始化其數值為0。

    cout << "Draw a Rectangle." << endl;           //將"Draw a Rectangle."輸出到螢幕上，並換行。
    cout << "Enter width and height." << endl;     //將"Enter width and height."輸出到螢幕上，並換行。
    cout << "width:";                              //將"width:"輸出到螢幕上。
    cin >> width;                                  //輸入整數width。

    while(width<=0)                                //當width小於等於0時，執行以下動作。
    {
        cout << "illegal input!" << endl;          //將"illegal input!"輸出到螢幕上，並換行。
        cout << "width:";
        cin >> width;
    }

    cout << "height:";       //將"height:"輸出到螢幕上。
    cin >> height;           //輸入整數height。
    while(height<=0)         //當height小於等於0時，執行以下動作。
    {
        cout << "illegal input!" << endl;
        cout << "height:";
        cin >> height;
    }

    x = width;               //把width的值丟給x。
    y = height;              //把height的值丟給y。
    while(y>0)               //當y大於0時，執行以下動作。
    {
        while(x>0)           //當大於0時，執行以下動作。
        {

            if(x==width or x==1 or y==height or y==1)     //如果x等於width的值或1或y等於height的值或1時，執行以下動作。
                cout << "*";                              //將"*"輸出到螢幕上。
            else                                          //除非x或y不符合if的情形時，執行以下動作。
                cout << " ";                              //將" "輸出到螢幕上。
            x--;                                          //x從起始值開始逐1往下遞減。
        }
        cout << endl;                                     //當x<=0時換行。
        x = width;                                        //把width的值丟給x。
        y--;                                              //y從起始值開始逐1往下遞減。
    }

    return 0;
}
