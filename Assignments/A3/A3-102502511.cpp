#include <iostream>
using namespace std;

int main()
{
    int width = 1; //設width的初始值為1,使其符合條件以進行運作
    int height = 1; //設height的初始值為1,使其符合條件以進行運作


    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    cout << "width:";
    cin >> width;

    if(width>0) //當width大於0時,不做任何反應
        cout << "";
    else
        while(width<=0) //當width小於等於0時,進行重複動作,直到width大於0為止
        {
            cout << "Illegal input!" << endl;
            cout << "width:";
            cin >> width;
        }

    cout << "height:";
    cin >> height;

    if(height>0) //當height大於0時,不做任何反應
        cout << "";
    else
        while(height<=0) //當height小於等於0時,進行重複動作,直到height大於0為止
        {
            cout << "Illegal input!" << endl;
            cout << "height:";
            cin >> height;
        }
    int x=1; //設x初始值為1,以符合width的最小值
    int y=height; //設y的初始值為上列儲存的height,使y不會超過height

    while(y>=1) //當y大於等於height的最小值1時,做下列的重覆反應
    {
        while(x<=width) //當x小於等於width(即width的最大值)時,做出下列的動作
        {
            if(x==1) //當x為1(即width的最小值)時,顯現*
                cout << "*";
            else if(x==width) //當x為width(即width的最大值)時,顯示*
                cout << "*";
            else if(y==height) //當y為heigth(即height的最大值)時,顯示*
                cout << "*";
            else if(y==1) //當y為1(即height的最小值)時,顯現*
                cout << "*";
            else //其餘部分顯示空白
                cout << " ";
            x++; //x持續遞增值到不符合while的條件
        }
        cout << endl;
        x=1; //使x恢復為1,即回到width的最小值(圖的最左方)
        y--; //y持續遞減值到不符合while的條件
    }

    return 0;
}
