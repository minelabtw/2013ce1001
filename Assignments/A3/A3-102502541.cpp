#include <iostream>

using namespace std;

int main()
{
    cout << "Draw a Rectangle." << endl << "Enter width and height." << endl;//輸出Draw a Rectangle. 換行 輸出Enter width and height. 換行

    int a = 0;//宣告變數 a=0
    int b = 0;//宣告變數 b=0

    cout << "width:";//輸出width:
    cin >> a;//輸入變數a
    while(a <= 0)//迴圈 當 a小於等於 0
    {
        cout << "illegal input!" << endl << "width:";//輸出illegal input! 換行 輸出width:
        cin >> a;//輸入變數a
    }
    cout << "height:";//輸出height:
    cin >> b;//輸入變數b
    while(b <= 0)//迴圈 當 b小於等於 0
    {
        cout << "illegal input!" << endl << "height:";//輸出illegal input! 換行 輸出 height:
        cin >> b;//輸入變數b
    }

    for (int y = 1; y <=b; y++)//迴圈 起點y=1 終點y小於等於b y遞增
    {
        for (int x = 1; x <=a; x++)//迴圈 起點x=1 終點x小於等於a x遞增
        {
            if(x==1 || y==1 || y==b || x==a)//條件 x=1或 y=1 或 y=b 或 x=a
                cout << "*";//輸出*
            else//其餘
                cout << " ";//輸出空白

        }
        cout << endl;//輸出換行
    }
    return 0;

}
