#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int width=0 ;                                             //宣告變數"寬"
    int height=0 ;                                            //宣告變數"高"
    int i ;                                                   //繪圖時使用的列
    int j ;                                                   //繪圖時使用的行

    cout << "Draw a Rectangle." << endl << "Enter width and height." << endl ;
    cout << "width:" ;
    cin >> width ;                                            //要求輸入寬
    while (width<=0)                                          //如超出定意範圍則要求重新輸入
    {
        cout << "illegal input!" << endl ;
        cout << "width:" ;
        cin >> width ;
    }
    cout << "height:" ;
    cin >> height ;                                           //要求輸入寬
    while (height<=0)                                         //如超出定意範圍則要求重新輸入
    {
        cout << "illegal input!" << endl ;
        cout << "height:" ;
        cin >> height ;
    }
    for (i=0;i<height;++i)
    {
        for (j=0;j<width;++j)
        {
            if (i==0 || i==height-1 || j==0 || j==width-1)    //以星號畫出邊框
                cout << "*" ;
            else                                              //以空格填滿其他空間
                cout << " " ;
        }
        cout << endl ;                                        //每行結束時換行
    }
    return 0;
}
