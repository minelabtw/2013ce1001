#include<iostream>
using namespace std;

int main()
{
    int width,height,x,y; //宣告整數型態的矩形寬度,矩形高度,x,y距b
    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;
    cout<<"width: ";
    cin>>width;
    while(width<=0) //當width不符合條件，也就是小於等於0
    {
        cout<<"illegal input!"<<endl;
        cout<<"width: ";
        cin>>width;
    }
    cout<<"height: ";
    cin>>height;
    while(height<=0)//當height不符合條件，也就是小於等於0
    {
        cout<<"illegal input!"<<endl;
        cout<<"height: ";
        cin>>height;
    }

    for(x=1; x<=width; x++) //依寬度重覆輸出邊上"*"x次
    {
        cout<<"*";
    }
    cout<<endl;
    for(y=1; y<=(height-2); y++) //依高度重覆輸出y-2行
    {
        cout<<"*";                //輸出邊上"*"
        for(x=1; x<=(width-2); x++) //依寬度重覆輸出" "x-2次
        {
            cout<<" ";
        }
        cout<<"*"<<endl;          //輸出邊上"*"
    }
    for(x=1; x<=width; x++)
    {
        cout<<"*";
    }
}
