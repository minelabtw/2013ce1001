#include<iostream>
using namespace std;
int main()
{
    int width=0;
    int height=0;
    int boxx=0;
    int boxy=0;
    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;
    cout<<"width: "; //set width
    cin>>width;
    while(width<=0) //if width<=0,reset width
    {
        cout<<"illegal input!"<<endl;
        cout<<"width: ";
        cin>>width;
    }
    cout<<"height: "; //set height
    cin>>height;
    while(height<=0) //if width<=0,reset height
    {
        cout<<"illegal input!"<<endl;
        cout<<"height: ";
        cin>>height;
    }
    for(boxy=0;boxy<height;boxy++) //boxy from 0 to height-1,loops height times
    {
        for(boxx=0;boxx<width;boxx++) //boxx from 0 to width-1,loops width times
        {
            if(boxx==0) //when boxx=0,print*
                cout<<"*";
            else if(boxx==width-1) //when boxx=width-1,print*
                cout<<"*";
            else if(boxy==0) //when boxy=0,print*
                cout<<"*";
            else if(boxy==height-1) //when boxy=height-1,print*
                cout<<"*";
            else  //others,print space
                cout<<" ";
        }
        cout<<endl;
    }
    return 0;
}
