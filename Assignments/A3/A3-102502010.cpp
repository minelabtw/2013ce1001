#include <iostream>
using namespace std;

int main()
{
    int width,height; //寬,高
    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;
    cout<<"width: ";
    cin>>width;  //輸入寬
    while(width<=0)  //判斷寬是否在範圍內
    {
        cout<<"illegal input!"<<endl<<"width: ";
        cin>>width;
    }
    cout<<"height: ";
    cin>>height;  //輸入高
    while(height<=0)  //判斷高是否在範圍內
    {
        cout<<"illegal input!"<<endl<<"height: ";
        cin>>height;
    }
    int reback=width;  //返回原處
    int border=height; //邊界
    while(height>0)
    {
        while(width>0)
        {
            if(border==height || height==1 || reback==width || width==1)  //輸出圖形
                cout<<'*';
            else
                cout<<' ';
            width--;
        }
        cout<<endl;
        width=reback;
        height--;
    }
    return 0;
}
