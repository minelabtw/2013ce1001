#include <iostream>

using namespace std;
main(){
    int w=0,h=0;    //width & height
    cout << "Draw a Rectangle.\n"<< "Enter width and height" << endl;
    //input width and debug
    while(w<=0){
        cout << "width:";
        cin >> w;
        if(w<=0)
            cout << "illegal input!" << endl;
    }
    //input height and debug
        while(h<=0){
        cout << "height:";
        cin >> h;
        if(h<=0)
            cout << "illegal input!" << endl;
    }
    //output ans:
    //first row
    for(int j=0;j<w;j++){
        cout << "*";
    }
    //mid
    cout << endl;
    for(int i=1;i<h-1;i++){
        for(int j=0;j<w;j++){
            if(j==0 || j==w-1)
                cout << "*";
            else
                cout << " ";
        }
        cout << endl;
    }
    //last row
    for(int j=0;j<w;j++){
        cout << "*";
    }
    cout << endl;


    return 0;
}
