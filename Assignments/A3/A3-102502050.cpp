#include<iostream>
using namespace std;
int main()
{
    int height;
    int width;  //長和寬

    int x;
    int y;      //輸出時所需的index
    cout << "Draw a Rectangle.\n" << "Enter width and height.\n" ;
    cout << "width: ";
    cin >> width ;
    while(width <= 0)   //檢查輸入(寬)
    {
        cout << "illegal input!\n" <<"width: ";
        cin >> width ;
    }
    cout << "height: ";
    cin >> height ;
    while(height <= 0)  //檢查輸入(長)
    {
        cout << "illegal input!\n" <<"height: ";
        cin >> height ;
    }
    width --;
    height--;//調整數值，方便判斷

    for(y=0;y<=height; y++)
    {
        for (x=0; x<=width;x++)
                if( (width - x)%width && (height - y)%height )cout << " " ;  //若不在邊界時
                else                                          cout << "*" ;
        cout << endl ;
    }
    /*
     width - x 用於判斷    x==width
    (width - x)%width 判斷 x==0
    */

    return 0;
}
