#include <iostream>
using namespace std;
bool isNotPositive(float number);//檢查number是否為正
bool isFloat(float number);//檢查number是否帶小數

int main(){
    float width = 1.0;//宣告型別為float的width，並初始化其數值為1.0
    float height = 1.0;//宣告型別為float的height，並初始化其數值為1.0
    int sourceType = 0;//宣告型別為int的輸入來源類別，並初始化其數值為0.0
    float* sourcePtr = 0;//宣告型別為指向float的來源指標，並初始化其數值為0(null)

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;
    while(sourceType < 2){
        if(sourceType == 0){//sourceType為width
            cout << "width: ";
            sourcePtr = &width;
        }
        else if(sourceType == 1){//sourceType為height
            cout << "height: ";
            sourcePtr = &height;
        }
        else{
            //不做任何事
        }

        cin >> (*sourcePtr);
        if(isNotPositive(*sourcePtr) || isFloat(*sourcePtr)){
            //sourcePtr < 0 或是 不是整數
            //則重新輸入
            cout << "illegal input!" << endl;
        }
        else{//輸入下一個值
            sourceType++;
        }
    }
    for(int y = 1; y <= (int)height; y++){//從y=1畫到height
        for(int x = 1; x <= (int)width; x++){//從x=1畫到width
            if(x == 1 || x == (int)width || y == 1 || y == (int)height){
                //當在(1,y), (width,y), (x, 1), (x, height)
                //時就要輸出星號
                cout << "*";
            }
            else{//否則就是輸出長方行內部的空白
                cout << " ";
            }
        }
        cout << endl;//從下一行開始輸出
    }
    return 0;
}

bool isNotPositive(float number){//檢查number是否為正
    if(number <= 0.0)
        return true;
    else
        return false;
}

bool isFloat(float number){//檢查number是否帶小數
    float intNumber = (int)number;//intNumber放入number整數部分
    if(intNumber != number)//如果不相等則必帶小數點
        return true;
    else
        return false;
}
