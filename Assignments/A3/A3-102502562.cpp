#include <iostream>
using namespace std;

int main()
{
    int width,height;                           //宣告型別為整數的width,height
    int x=1,y=1;                                //宣告型別為整數的x,y 並初始化其值為1

    cout << "Draw a Rectangle.\n";              //輸出"Draw a Rectangle.\n"到螢幕上
    cout << "Enter width and height.\n";        //輸出"Enter width and height.\n"到螢幕上

    while (1)                                   //詢問width的迴圈
    {
        cout << "width:";                       //輸出"width:"到螢幕上
        cin >> width;                           //輸入一個值給width
        if (width<=0)                           //判斷width是否小於等於0
        {
            cout << "illegal input!\n";         //輸出"illegal input!\n"到螢幕上
        }
        else
        {
            break;                              //跳出迴圈
        }
    }
    while (1)                                   //詢問height的迴圈
    {
        cout << "height:";                      //輸出"height:"到螢幕上
        cin >> height;                          //輸入一個值給height
        if (height<=0)                          //判斷height是否小於等於0
        {
            cout << "illegal input!\n";         //輸出"illegal input!\n"到螢幕上
        }
        else
        {
            break;                              //跳出迴圈
        }
    }

    for (y=1;y<=height;y++)                     //y重複累加到<=height並執行
    {
        for (x=1;x<=width;x++)                  //x重複累加到<=width並執行
        {
            if (y==1)                           //判斷y是否等於1
            {
                cout << "*";                    //輸出"*"到螢幕上
            }
            else if (x==1)                      //判斷x是否等於1
            {
                cout << "*";                    //輸出"*"到螢幕上
            }
            else if (x==width)                  //判斷x是否等於width
            {
                cout << "*";                    //輸出"*"到螢幕上
            }
            else if (y==height)                 //判斷y是否等於height
            {
                cout << "*";                    //輸出"*"到螢幕上
            }
            else
            {
                cout << " ";                    //輸出" "到螢幕上
            }
        }
        cout << endl;                           //輸出換行
    }

    return 0;
}
