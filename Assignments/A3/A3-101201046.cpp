#include <iostream>
#include <iomanip>

using namespace std;

int main(void) {
    int height = 0, width = 0; //the width an heigjht of rectangle,
                               // set them zero
    cout << "Draw a Rectangle.\n"
            "Enter width and height.\n";
    do {
        cout << "width : ";
        cin >> width; //input width
        cout << ((width <= 0) ? "illegal input!\n":""); //check error
    }while (width <= 0);

    do {
        cout << "height : ";
        cin >> height; //input height
        cout << ((height <= 0) ? "illegal input!\n":""); //check error
    }while (height <= 0);

    cout << setfill('*') << setw(width +1) << '\n' << endl; //it just use setfill
    if (height > 1) {
        for (int i = 1; i < (height - 1); i++)
            cout << ((width > 1) ? "*" : "")                //it is doubtful.
                 << setfill(' ') << setw(width) << "*\n" << endl;

        cout << setfill('*') << setw(width +1) << '\n' << endl;
    }

    return 0;
    }
