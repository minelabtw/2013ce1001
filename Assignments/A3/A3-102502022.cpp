#include <iostream>

using namespace std;

int main()
{
    int integer1=0;      //宣告名稱為integer1的變數
    int integer2=0;      //宣告名稱為integer2的變數

    cout<< "Draw a Rectangle.\n""Enter width and height.\n";

    cout<< "width:";
    cin>> integer1;
    while(integer1<=0)   //迴圈'條件為integer1<=0
    {
        cout<< "illegal input!\n""width:";
        cin>> integer1;
    }

    cout<< "height:";
    cin>> integer2;
    while(integer2<=0)   //迴圈'條件為integer2<=0
    {
        cout<< "illegal input!\n""height:";
        cin>> integer2;
    }


    int x=0;     //宣告名稱為X的變數
    int y=0;     //宣告名稱為y的變數
    int xmax=integer1;    //宣告名稱為xmax的變數'值等於integer1
    int ymin=-integer2;   //宣告名稱為ymin的變數'值等於integer2
    while(y>=ymin)        //迴圈'條件為y>=ymin
    {
        while(x<=xmax)    //迴圈'條件為x<=xmax
        {
            if(x<=xmax && y==0)
                cout<< "*";
            else if(x==0 && y>=ymin)
                cout<< "*";
            else if(x==xmax && y>=ymin)
                cout<< "*";
            else if(x<=xmax && y==ymin)
                cout<< "*";
            else
                cout<< " ";
        x++;
        }

    cout<< endl;
    x=0;
    y--;

    }

    return 0;

}

