#include<iostream>
using namespace std;
int main()
{
    int w=1;//宣告寬度初始值為1
    int h=1;//宣告長度初始值為1
    int x=1;//宣告變數x初始值為1
    int y=1;//宣告變數y初始值為1
    cout <<"Draw a Rectangle."<<endl;
    cout <<"Enter width and height."<<endl;
    cout <<"width: ";//鍵入寬度
    cin >>w;
    while (w<=0)//做一個迴圈使得寬度大於0
    {
        cout <<"illegal input!"<<endl;
        cout <<"width: ";
        cin >>w;
    }
    cout <<"height: ";//鍵入長度
    cin >>h;
    while(h<=0)//做一個迴圈使得長度大於0
    {
        cout <<"illegal input!"<<endl;
        cout <<"height: ";
        cin >>h;
    }
    while (y<=h)//做出可畫出長方形的雙迴圈
    {
        while (x<=w)
        {
            if (x==1)//當x=1，印出*
                cout <<"*";
            else if (y==1)//當y=1，印出*
                cout <<"*";
            else if (x==w)
                cout <<"*";//當x=寬度，印出*，為長方形的寬
            else if (y==h)
                cout <<"*";//當y=長度，印出*，為長方形的長
            else
                cout <<" ";//其他部分則印出空白
            x++;//x加一
        }
        cout<<endl;//換行印
        x=1;
        y++;//y+1
    }
    return 0;
}
