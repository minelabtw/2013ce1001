#include <iostream>

using namespace std;

int main()
{
    int width=0;        //宣告變數整數。
    int height=0;

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    cout << "width:";       //顯示width。
    cin >> width;
    while (width<0 || width==0)     //迴轉條件，假如小於等於0的話
    {
        cout << "illegal input!" << endl << "width:";
        cin >> width;
    }

    cout << "height:";
    cin >> height;
    while (height<0 || height==0)
    {
        cout << "illegal input!" << endl << "height:";
        cin >> height;
    }

    int x=width;        //宣告寬度變數並使其等於寬度。
    int y=height;

    while (y>0)     //宣告迴轉條件使其向下移動。
    {
        while (x>0)     //宣告迴轉條件使其向右移動。
        {
            if(x==1||x==width)      //宣告假如=1和=width時會出現×的條件。
            {
                cout << "*";
            }
            else if(y==1||y==height)
            {
                cout << "*";
            }
            else
            {
                cout << " ";
            }
            x--;        //使他能在成功向右移動。
        }
        cout << endl;       //換行
        x=width;        //回歸本值以防時在多次迴轉是指停新的值在0
        y--;           //他能在成功向下移動。
    }
    return 0;
}
