#include <iostream>
using namespace std;

int main(){
    int width;
    int height;

    cout<<"Draw a Rectangle."<<endl;               //在螢幕上列印出"Draw a Rectangle."
    cout<<"Enter width and height."<<endl;         //在螢幕上列印出"Enter width and height."
    while(true){                                   //寫一個while迴圈判斷輸入的width與height值是否在設定的範圍內
         cout<<"width:";
         cin>>width;
         if(width<=0){
         cout<<"Illegal Input!"<<endl;             //若輸入的值超出設定的範圍 則在螢幕上列印出"Illegal Input!"
         continue;
         }
         cout<<"height:";
         cin>>height;
         if(height<=0){
         cout<<"Illegal Input!"<<endl;
         }else{
            break;
         }
    }
    for(int i=0;i<height;i++){                   //寫兩層for迴圈 使輸入的width跟height值能在螢幕上列印出對應的矩形
        if(i==0||i==height-1){                   //設定條件在第一行跟最後一行
            for(int j=0;j<width;j++){            //印出設定的寬度"*"
                cout<<"*";
            }
        }else{                                   //其他行
            for(int j=0;j<width;j++){
                if(j==0||j==width-1){            //每行的第一個與最後一個印出"*"
                    cout<<"*";
                }else{
                    cout<<" ";                   //印出中空
                }
            }
        }
        cout<<endl;
    }
    return 0;
}
