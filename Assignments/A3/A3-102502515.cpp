#include <iostream>

using namespace std;

int main()
{
    int width = 0;
    int height = 0;

    cout << "Draw a Rectangle.\nEnter width and height." << endl;
    cout << "width:" ;
    cin  >> width ;

    while (width <= 0)
    {
        cout << "illegal input!" << endl;
        cout << "width:" ;
        cin  >> width ;
    }

    cout << "height:" ;
    cin  >> height ;

    while (height <=0)
    {
        cout << "illegal input!" <<endl;
        cout << "height:" ;
        cin  >> height ;
    }


    int x = 1;
    int y = 1;


    while (y<=height)

    {

        while (x<=width)
        {
            if (y==1 || y==height || x==1 || x==width)
            {
                cout << "*" ;
                x++;
            }


            else
            {
                cout << " " ;
                x++;
            }

        }

        cout << endl;
        x=1;
        y++;


    }



    return 0;
}
