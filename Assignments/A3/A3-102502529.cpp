#include<iostream>

using namespace std ;

int main ()
{
    int a=0,b=0,t=0 ;                                       //宣告三數 將其值設定為0

    cout<<"Draw a Rectangle.\nEnter width and height.\n" ;  //輸出說明
    while(t<=1)                                             //t已設定為0 <=1 進入迴圈
    {
        if(t==0)
        {
            cout<<"width:" ;
            cin>>a ;
            if(a<=0)
                cout<<"illegal input!"<<endl;               //不符合就不t++ 所以會在跑一次
            else
                t++;
        }
        if(t==1)                                            //符合數值則t++ 進入if 方程式中
        {
            cout<<"height:" ;
            cin>>b ;
            if(b<=0)
                cout<<"illegal input!"<<endl;
            else
                t++;                                        //當兩數值符合後 t=2 所以跳出迴圈
        }
    }

    int x=-a,y=b ;                                         //宣告x,y 並將剛剛輸入的質宣告進去,x要由左至右 所以要加負號
    while(y>=1)                                        //先符合y的範圍 進入迴圈 ps.因y若到0 輸出的星星會多1
    {
        while(x<=-1)                                       //再來進入符合x的迴圈  一樣 x到0輸出的星星也是會多1
        {
            if(y==b||y==1)                                 //用來輸出長方形的上下並判定
            {
                cout<<"*" ;
            }
            else if (x==-a||x==-1)                        //用來輸出長方形的左右並判定
            {
                cout<<"*";
            }
            else                                          //其他地方為空白處
                cout<<" ";

            x++;                                         //跑一次則加一 直到-1跳出迴圈
        }
        cout<<endl ;                                     //拿來換行 才能繼續畫
        x=-a ;                                           //記得重新指定回-a 才能再進入迴圈做判斷
        y-- ;                                            //拿來計算縱軸長度 共b個
    }
}
