#include<iostream>
using namespace std ;
int main()
{
    int width=0;
    int height=0;
    int x=0;     //行
    int y=0;     //列

    cout << "Draw a Rectangle.\nEnter width and height.\n" ;
    while(1)    //cin width
    {
        cout << "width=";
        cin >> width ;
        if (width>0)    //cin is right
            break;
        cout << "illegal input!\n" ;    //again
    }
    while(1)    //cin height
    {
        cout << "height=";
        cin >> height ;
        if (height>0)   //cin is right
            break;
        cout << "illegal input!\n" ;    //again
    }

    for(y=height;y>0;y--)   //控制列
    {
        for(x=1;x<=width;x++)   //控制行
        {
            if (y==1||y==height||x==1||x==width)    //第一跟最後一行(列)cout *
                cout << "*" ;
            else
                cout <<" " ;                        //other is ' '
        }
        cout << endl ;                              //換行
    }

return 0;
}
