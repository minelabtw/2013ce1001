#include <iostream>

using namespace std;

int main()
{
    int number=0;
    int i=0;
    int x=0;
    int y=0;
    int number1=0,number2=0;

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    while(i<2)                                 //重覆詢問
    {
        if(i==0)                               //判斷是否詢問第i項數字
        {
            cout << "width:";                  //印出第i項數字為寬並詢問
        }

        else
        {
            cout << "height:";
        }

        cin >> number ;                        //輸入第i項數字

        if(number>0)                           //判斷第i項數字是否符合範圍
        {
            if(i==0)
            {
                number1 = number;              //宣告第i項數字代稱為number1
            }

            else
            {
                number2 = number;
            }
            i++;                               //跳至第i+1項數字
        }

        else
        {
            cout << "illegal input!" << endl;
        }
    }


    while(y<number2)                                //當y小於number2
    {
        while(x<number1)                            //當x小於number1
        {
            if(x==0)                                //判斷x或y為某值時，應當輸出甚麼符號
            {
                cout << "*";
            }

            else if(y==0)
            {
                cout << "*";
            }

            else if(x==number1-1)
            {
                cout << "*";
            }

            else if(y==number2-1)
            {
                cout << "*";
            }

            else
            {
                cout << " ";
            }

            x++;                                    //將x傳送到x+1(設為新的x)再跑一次
        }

        cout << endl;
        x=0;                                        //將x歸零
        y++;

    }

    return 0;
}
