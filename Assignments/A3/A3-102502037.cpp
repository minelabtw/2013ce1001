#include <iostream>

using namespace std;

int main()
{
    int w,h,x,y; //設定四個變數
    cout<<"Draw a Rectangle."<<endl<<"Enter width and height."<<endl; //輸出說明到螢幕上
    cout<<"width:";
    cin>>w; //輸入w.h
    while (w <= 0) //判別wh是否合格 否則重新輸入
    {
        cout << "illegal input!" << endl <<  "width:";
        cin >> w;
    }
    cout << "height:" ;
    cin >> h ;
    while (h <= 0)
    {
        cout << "illegal input!" << endl <<  "height:";
        cin >> h;
    }
 for(y=h ; y>=1 ; y--)  //輸出將由上至下所以y給最大值在減少
    {
        for(x=1 ; x<=w ; x++) //X由左至右所以遞增
        {   if(y==h || y==1) //判定該格需要輸出*或空格
                cout<<"*";
            else if(x==w || x==1)
                cout<<"*";
            else
                cout<<" ";
        }
        cout<<endl; //當一列X輸出完之後換行
    }

    return 0;
}
