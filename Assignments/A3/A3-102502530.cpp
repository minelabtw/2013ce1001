#include<iostream>
#include<cstdio>
int main()
{
    int width,height;                                           //宣告寬與高
    std::cout<<"Draw a Rectangle.\nEnter width and height.\n";  //輸出敘述
w:
    std::cout<<"width: ";                                       //輸出輸入提示
    std::cin>>width;                                            //輸入寬
    if(width<=0)                                                //如果寬不大於零
    {
        std::cout<<"illegal input!\n";                          //輸出"illegal input!"
        goto w;                                                 //重新輸入
    }
h:
    std::cout<<"height: ";                                      //輸出輸入提示
    std::cin>>height;                                           //輸入高
    if(height<=0)                                               //如果高不大於零
    {
        std::cout<<"illegal input!\n";                          //輸出"illegal input!"
        goto h;                                                 //重新輸入
    }
    for(int i=0;i<width;i++)                                    //輸出第一行
        putchar('*');
    puts("");
    for(int i=1;i<height-1;i++)                                 //輸出中間
    {
        putchar('*');
        for(int j=1;j<width-1;j++)
            putchar(' ');
        puts("*");
    }
    for(int i=0;i<width&&height!=1;i++)                         //輸出最後一行
        putchar('*');
    puts("");
}
