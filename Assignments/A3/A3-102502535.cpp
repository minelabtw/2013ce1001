#include <iostream>

using namespace std ;

int main()
{
    int width ;  //設定一整數變數width，定義其為矩形的寬。
    int height ;  //設定一整數變數height，定義其為矩形的長（高）。
    int x=1 ;  // 設定一對應width的整數變數為x，將其初始值定為1，因為寬的最小值為1。
    int y=1 ;  // 設定一對應height的整數變數為y，將其初始值定為1，因為寬的最小值為1。

    cout << "Draw a rectangle." << endl << "Enter width and height." << endl << "Width:" ;
    cin >> width ;
    while ( width <= 0 )   //以while限制操作者的輸入範圍:若輸入的數字小於0，即操作以下指示。
    {
        cout << "Illegal input!" << endl << "Width:" ;  //顯示"錯誤"。
        cin >> width ;  //使操作者重新輸入。
    }

    cout << "Height:" ;
    cin >> height ;
    while ( height <= 0 )  //同上面之while限制及操作。
    {
        cout << "Illegal input!" << endl << "Height:" ;
        cin >> height ;
    }

    while ( y<=height )  //當y小於等於操作者輸入之height，執行下列指示。
    {
        while ( x<=width )  //當x小於等於操作者輸入之width，執行下列指示。
        {
            if ( y == 1 || y == height )
            {
                cout << "*" ;  //當y=1或y=height時，輸出一個*。
                x++ ;  //執行完上列指示後，使x值加一並繼續執行此迴圈至不合條件為止。
            }
            else if ( x== 1 || x == width )
            {
                cout << "*" ;  //當x=1或x=width時，輸出一個*。
                x++ ;  //執行完上列指示後，使x值加一並繼續執行此迴圈至不合條件為止。
            }
            else
            {
                cout << " " ;  //當x跟y都不符合上列條件，即輸出一個空格。
                x++ ;  //執行完上列指示後，使x值加一並繼續執行此迴圈至不合條件為止。
            }
        }

        cout << endl ;
        x = 1 ;
        y++ ;  //執行完上列指示後，使y值加一並繼續執行此迴圈至不合條件為止。
    }

    return 0 ;
}

