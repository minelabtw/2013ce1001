#include <iostream>
using namespace std;

int main()
{
    int width ; //call variable for width
    int height ; //call variable for height
    int y =1; //call a variable

    cout << "Draw a Rectangle." << endl; //display words
    cout << "Enter width and height." << endl;//display words

    do
    {
        cout << "width: ";
        cin >>width; //input width
        if (width<=0) //data validation
        {
            cout << "illegal input!" << endl;//diplay "illegal input!" for improper input
        }
    }
         while (width <=0);

    do
    {
        cout << "height: ";
        cin >>height; // input height
        if (height<=0) //data validation
        {
            cout << "illegal input!" << endl;//diplay "illegal input!" for improper input

        }
    }
         while (height <=0);

    while (y<=height) //looping for numbers of lines to be display as the height
    {
        int x=1; //call variable with initial value
        while (x<=width) // looping for numbers of rows to be display as th width
        {
            if (y==1) //display all "*" as first line
            {
                cout << "*";
            }
            else if (x==1 || x==width) // display "*" as the first and last row
            {
                cout << "*";
            }
            else if (y==height) // display all "*" as the last line
            {
                cout << "*";
            }
            else if (x!=width && x!=1) // display " " as the empty space in the rectangle
            {
                cout << " ";
            }

            x++ ; //increase the value of x by 1 for looping to show the number of rows

        }
        y++; //increase the value of y by 1 for looping to show the number of lines
        cout << endl;
    }

    return 0;
}
