#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int width;
    int height;

    int x=1;
    int y=1;

    cout << "Draw a Rectangle." << endl << "Enter width and height."<< endl; //輸出字串

    cout << "width: ";                                                       //輸出字串
    cin >> width;                                                            //讓使用者輸入矩形的寬。

    while (width<=0)                                                         //當輸入之值不合規定時告知使用者並讓其重新輸入。
    {
        cout << "illegal input!" << endl << "width: ";
        cin >> width;
    }

    cout << "height: ";                                                      //輸出字串
    cin >> height;                                                           //讓使用者輸入矩形的寬。

    while (height<=0)                                                        //當輸入之值不合規定時告知使用者並讓其重新輸入。
    {
        cout << "illegal input!" << endl << "height: ";
        cin >> height;
    }


    while (y==1)                                                             //輸出第一行的邊長
    {

        do
        {
            cout << "*";
            x=x+1;
        }
        while (x<=width);
        x=1;
        y=y+1;
        cout << endl ;

    }


    while (y<height)                                                          //輸出第二行到倒數第二行的邊長
    {
        do
        {
            if (x==1)
            {
                x++;
                cout << "*";
            }
            else if (x<width)
            {
                x++;
                cout << " ";
            }
            else if (x==width)
            {
                x++;
                cout << "*";
            }

        }
        while (x<=width);
        y=y+1;
        x=1;
        cout << endl ;
    }


    while(y==height)                                                           //輸出最後一行的邊長
    {
        do
        {
            x=x+1;
            cout << "*";
        }
        while (x<=width);
        y=y+1;

    }

}
