#include <iostream>
using namespace std;
int main()
{
    int a;
    int b;
    int d;                                      //宣告a 寬度 b長度
    int e;
    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;
    cout<<"width: ";
    cin>>a;
    while(a<=0)
    {
        cout<<"illegal input!"<<endl;           //超過範圍就重新輸入
        cout<<"width: ";
        cin>>a;
    }
    cout<<"height: ";
    cin>>b;
    while(b<=0)
    {
        cout<<"illegal input!"<<endl;           //超過範圍就重新輸入
        cout<<"height: ";
        cin>>b;
    }
    for (int c=1; c<b+1; c++)                   //迴圈高度
    {
        if (c==1 || c==b)
        {
            for(d=1; d<a+1; d++)                //當是第一行跟最後一行時 實線
            {
                cout<<"*";
            }
            cout<<endl;
        }
        else                                    //中間則是空心
        {
            cout<<"*";
            for(e=1; e<a-1; e++)
            {
                cout<<" ";
            }
            cout<<"*";
            cout<<endl;
        }
    }
    return 0;
}

