#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int width ;                                               //給使用者輸入，寬
    int height ;                                              //給使用者輸入，高
    int i ;                                                   //作圖時的列
    int j ;                                                   //作圖時的行

    cout << "Draw a Rectangle.\nEnter width and height.\n" ;
    cout << "width:" ;
    cin >> width ;                                            //使用者輸入
    while (width<=0)                                          //錯了再重新輸入
    {
        cout << "illegal input!\n" ;
        cout << "width:" ;
        cin >> width ;
    }
    cout << "height:" ;
    cin >> height ;                                           //使用者輸入
    while (height<=0)                                         //錯了再重新輸入
    {
        cout << "illegal input!\n" ;
        cout << "height:" ;
        cin >> height ;
    }
    for (i=0;i<height;++i)
    {
        for (j=0;j<width;++j)
        {
            if (i==0 || i==height-1 || j==0 || j==width-1)    //如果是第一和做後一行和列的就是*
                cout << "*" ;
            else                                              //其餘都是"  "
                cout << " " ;
        }
        cout << endl ;                                        //每行結束時換行
    }
    return 0;
}
