#include<iostream>

using namespace std;

int main(void)
{
    int width = 0,height = 0;//定義高與寬的初始值為0

    cout << "Draw a Rectangle."<<endl;
    cout << "Enter width and height."<<endl;//顯示字元
    cout << "width:";//顯示所輸入之寬
    cin >> width;
    while(width==0||width<0)//定義寬的範圍
    {
        cout << "illegal input!"<<endl;
        cout << "width:";
        cin >> width;
    }
    cout << "height:";//顯示所輸入之高
    cin >> height;
    while(height==0||height<0)//定義高的範圍
    {
        cout << "illegal input!"<<endl;
        cout << "height:";
        cin >> height;
    }

    int y=height;//設定圖形之高為所輸入之值
    while (y>0)//高一定等於所輸入之整數值
    {
        int x=1;//迴圈之後,每次顯示字元回到第一行
        while(x<=width)
        {
            if (y==height || y==1)
            cout << "*";
            else if (x==1 || x==width)
            cout << "*";
            else
            cout<<" ";//設定高與寬的線條顯示為*,而其他字原為空白鍵
            x++;//x向右遞增
        }
        cout << endl;
        y--;//y向右遞減
    }
    return 0;
}
