//A3-102302053.cpp
#include <iostream>
#include <cstdlib>//allow program to pause after the computing

using namespace std;//allow program to use cin, cout , endl.

int main ()
{
    int length;//the length of the rectangle.
    int x = 1;//initial number for drawing length.
    int width;//the width of the rectangle
    int y = 1;//intial number for drawing width.

    cout << "Please enter length and width\n";

    cout << "Length:";//prompt user to give a value for length.
    cin >> length;
    while (length <= 0 )//check the range of the length, if it isn't a integer >= 0 ,prompt user to enter agaain.
    {
        cout << "illegal input!\n";
        cout << "Length:";
        cin >> length;
    }


    cout << "Width:";//prompt user to give a value for width.
    cin >> width;
    while (width <= 0 )//check the range of the width, if it isn't a integer >= 0 ,prompt user to enter agaain.
    {
        cout << "illegal input!\n";
        cout << "width:";
        cin >> width;
    }



    while (x <= length)//program start drawing the rectangle form this line, and will draw it form the top to the bottom.length and width was given by the user.
    {
        if (x == 1 or x == length)//the top and the bottom side of the rectangle.
        {
            while (y <= width)//output (width) * from left to right.
            {
                cout << "*";
                y++;
            }
        }
        else//the left and the right side of the rectangle.
        {   while (y <= width)//output * only at the left side and the right side of the rectangle.
            {
                if (y == 1)
                cout << "*";
                else if (y == width)
                cout << "*";
                else cout << " ";
                y++;
            }
        }
        cout << "\n";//finish drawing one line, keep drawing from the next line.
        y = 1;//reseting y to 1 enble program keep drawing the width from the next line.
        x++;


    }
    system ("pause");

    return 0;
}
