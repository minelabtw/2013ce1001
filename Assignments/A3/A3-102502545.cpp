  #include <iostream>

  using namespace std;

  int main()
  {
      int aMax=0;                             //宣告aMax為一變數。
      int bMax=0;                             //宣告bMax為一變數。

      cout<<"Draw a Rectangle.\n";
      cout<<"Enter width and height.\n";
      cout<<"width:";
      cin>>aMax;

      while(aMax<1)                          //設置一個迴圈來糾正錯誤的範圍。
      {
          cout<<"illegal input!\n";
          cout<<"width:";
          cin>>aMax;
      }

      cout<<"height:";
      cin>>bMax;

      while(bMax<1)                          //設置一個迴圈來糾正錯誤的範圍
      {
          cout<<"illegal input!\n";
          cout<<"height:";
          cin>>bMax;
      }

      int b=1;
      while(b<=bMax)                                 //設置兩個迴圈來產生*的分布。
      {
          int a=1;
          while(a<=aMax)
          {
              if (a==1||b==1||b==bMax||a==aMax)
              cout<<"*";
              else if(a!=1&&b!=1&&b!=bMax&&a!=aMax)
              cout<<" ";
              a++;
          }
          cout<<endl;
          b++;
      }




      return 0;
  }
