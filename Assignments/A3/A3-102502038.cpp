#include <iostream>

using namespace std;

int main(void){
    //init section
    short int widthCheck = 1;
    short int heightCheck = 1;
    short int walkingBool;
    long long int width,height,walkingWidth,walkingHeight;
    bool stop;
    cout << "Draw a Rectangle.\nEnter width and height.\n";
    while(widthCheck){
        //Width input loop
        cout << (widthCheck==2?"illegal input!\n":"") << "width: ";
        cin >> width;
        widthCheck = (width>0?0:2);
    };
    while(heightCheck){
        //Height input loop
        cout << (heightCheck==2?"illegal input!\n":"") << "height: ";
        cin >> height;
        heightCheck = (height>0?0:2);
    };
    //Start drawing.
    //Cursor
    walkingWidth = 1;
    walkingHeight = 1;
    while(!stop){
        //Cursor status code init
        walkingBool = 0;
        //Calc cursor status code
        walkingBool += (walkingWidth == 1||walkingHeight == 1||walkingWidth == width||walkingHeight == height)?4:0;
        walkingBool += (walkingWidth == width)?2:0;
        walkingBool += (walkingHeight == height)?1:0;
        switch(walkingBool){
        case 0://Inside Rectangle
            cout << " ";
            walkingWidth++;//看狀況，自。己。走。路。
            break;
        case 4://On Rectangle Edge
        case 5:
            cout << "*";
            walkingWidth++;
            break;
        case 6://On Rectangle Right Edge
            cout << "*\n";
            walkingWidth = 1;
            walkingHeight++;
            break;
        case 7://On Rectangle End Point
            cout << "*\n";
            stop = 1;
            break;
        default://For debug usage,usually not display.
            cout << walkingBool << ",Error!\n";
            stop = 1;
            break;
        };
    };
    return 0;
};
