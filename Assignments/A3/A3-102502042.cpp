#include <iostream>
using namespace std;
int main()
{
    ios::sync_with_stdio(0);                        //取消同步，優化輸入輸出效率

    int width;                                      //宣告一變數width存放寬
    int height;                                     //宣告一變數height存放高
    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;
    cout<<"width: ";
    cin>>width;                                     //輸入一整數放到width變數中
    while(width<=0)                                 //若該變數的值不符範圍則進入迴圈重新輸入
    {
        cout<<"illegal input!"<<endl;
        cout<<"width: ";
        cin>>width;
    }
    cout<<"height: ";
    cin>>height;                                    //輸入一整數放到height變數中
    while(height<=0)                                //若該變數的值不符範圍則進入迴圈重新輸入
    {
        cout<<"illegal input!"<<endl;
        cout<<"height: ";
        cin>>height;
    }
    for(int i=0;i<height;++i){                      //i代表高度 j代表寬度 兩層迴圈依序印出要求圖形
        for(int j=0;j<width;++j)
            if(i>0&&i<height-1&&j>0&&j<width-1)cout<<' ';
            else cout<<'*';
        cout<<endl;
    }
    return 0;                                       //若程式成功執行返回0
}
