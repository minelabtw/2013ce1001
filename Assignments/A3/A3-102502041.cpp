#include<iostream>
using namespace std;
int main()
{
    cout<<"Draw a Rectangle."<<endl;
    cout<<"Enter width and height."<<endl;

    int width=0;                        //宣告整數變數width
    int height=0;                       //宣告整數變數height
    cout<<"width:";
    cin>>width;
    while(width<=0)
    {
        cout<<"illegal input!"<<endl;
        cout<<"width:";
        cin>>width;
    }
    cout<<"height:";
    cin>>height;
    while(height<=0)
    {
        cout<<"illegal input!"<<endl;
        cout<<"height:";
        cin>>height;
    }

    int x=1;                            //將原點定義為(1,1)
    int y=1;
    while(y<=height)                    //使用while迴圈限制住圖形高度
    {
        int x=1;
        while(x<=width)                 //使用while迴圈限制住圖形寬度
        {
            if(y==1||y==height||x==1||x==width)         //將四個邊都填滿
                cout<<"*";
            else                        //剩下的就是空心部份了
                cout<<" ";
            x++;
        }
        cout<<endl;
        y++;
    }

    return 0;
}
