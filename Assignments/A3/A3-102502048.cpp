#include<iostream>
using namespace std;
int main()
{
    int w=0;//宣告變數
    int h=0;//宣告變數
    int x=1;//宣告變數
    int y=1;//宣告變數

    cout<<"Draw a Rectangle.";//顯示字串
    cout<<"Enter width and height."<<endl;//顯示字串

    cout<<"width:";//顯示字串
    cin>>w;//輸入數值
    while(w<=0)//判斷w是否<=0
    {
        cout<<"illegal input!"<<endl;//顯示字串
        cin>>w;//再次輸入數值
    }

    cout<<"height:";//顯示字串
    cin>>h;//輸入數值
    while(h<=0)//判斷h是否<=0
    {
        cout<<"illegal input!"<<endl;//顯示字串
        cin>>h;//再次輸入數值
    }

    while(y<=h)//當y<=h時進入迴圈
    {
        while(x<=w)//當x<=w時進入迴圈
        {
            if(y==1 || y==h)//判斷y是否等於1或等於h
                cout<<"*";//顯示符號
            else if(x==1 || x==w)//判斷y是否等於1或等於w
                cout<<"*";//顯示符號
            else
                cout<<" ";//顯示空白
            x++;//X值增加
        }
        x=1;//回復x值為1
        y++;//y值增加
        cout<<endl;//換行
    }
    return 0;
}
