#include<iostream>
using namespace std;
int main ()
{
    int width =0;//宣告寬並=0
    int height =0;//宣告高並=0
    int x =0;//宣告x並=0
    int y =0;//宣告y並=0
    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;//顯示題目要求
    cout << "width: ";//提示輸入寬
    cin >> width;//輸入寬
    while (width<=0)//判斷是否符合條件
    {
        cout << "illegal input!" << endl;//提示不符合條件
        cout << "width: ";//提示輸入寬
        cin >> width;//輸入寬
    }
    cout << "height: ";//提示輸入高
    cin >> height;//輸入高
    while (height<=0)//判斷是否符合條件
    {
        cout << "illegal input!" << endl;//提示不符合條件
        cout << "height: ";//提示輸入高
        cin >> height;//輸入高
    }
    x =1;//從右邊開始
    y =1;//從上面開始
    while (y<=height)//使y<高時迴圈
    {
        if (y==1 || y==height)//邊界時
        {
            while (x<=width)//使x<寬時迴圈
            {
                cout << "*";
                x++;//往右移
            }
            y++;//換行
            cout << endl ;//游標換行
            x =1;//回右邊
        }
        else//否則
        {
            while (x<=width)//使x<寬時迴圈
            {
                if (x==1 || x==width)//邊界時
                {
                    cout << "*";
                    x++;//往右移
                }
                else
                {
                    cout << " ";
                    x++;//往右移
                }
            }
            y++;//換行
            cout << endl ;//游標換行
            x =1;//回右邊
        }
    }

    return 0;
}
