#include <iostream>
using namespace std;

int main()
{
    int x = 0; //宣告型別為 整數(int) 的寬度(x)，並初始化其數值為0。
    int y = 0; //宣告型別為 整數(int) 的高度(y)，並初始化其數值為0。
    cout << "Draw a Rectangle." << endl; //使用cout指令來將欲顯示的文字輸出在螢幕上。
    cout << "Enter width and height." << endl;
    cout << "width:";
    cin >> x;
    while (x <= 0) //用while迴圈來判斷數字的合理性，否則要求使用者重新輸入。
    {
        cout << "illegal input!" << endl;
        cout << "width:";
        cin >> x;
    }
    cout << "height:";
    cin >> y;
    while (y <= 0)
    {
        cout << "illegal input!" << endl;
        cout << "height:";
        cin >> y;
    }
    int a = 1; //宣告型別為 整數(int) 的x變數(a)，並初始化其數值為1。
    int b = 1; //宣告型別為 整數(int) 的y變數(b)，並初始化其數值為1。
    for (b = 1; b <= y; b++) //用for迴圈來反覆計算，並設定計算的範圍大小。
    {
        for (a = 1; a <= x; a++)
        {
            if (a == 1) //用if和else來判斷位置，並使其輸出相對應的符號。
                cout << "*";
            else if (b == 1 || b == y)
                cout << "*";
            else if (a == x)
                cout << "*";
            else
                cout << " ";
        }
        a = 1; //將x變數(a)回歸到1的位置，使內部的for迴圈能夠重新做判斷。
        cout << endl;
    }
    return 0;
}
