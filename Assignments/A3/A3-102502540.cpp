#include <iostream>

using namespace std;

int main()
{
    int width = 0; //宣布寬度
    int height = 0;  //宣布長度

    cout << "Draw a Rectangle." << endl << "Enter width and height." << endl << "width:"; //顯示Draw a Rectangle. 換行 Enter width and height. 換行 width:
    cin >> width; //輸入寬度

    while ( width <= 0 ) //迴圈當width小於等於0
    {
        cout << "illegal input!" << endl << "width:"; //顯示illegal input! 換行 width:
        cin >> width; //輸入寬度
    }

    cout << "height:"; //顯示長度
    cin >> height; //輸入長度

    while ( height <= 0 ) //迴圈當height小於等於0
    {
        cout << "illegal input!" << endl << "height:"; //顯示illegal input! 換行 height:
        cin >> height; //輸入長度
    }

    int x = 1; //宣告x等於1
    int y = 1; //軒高y等於1

    while ( y <= height ) //迴圈當y小於等於height
    {
        while ( x <= width ) //迴圈當x小於等於width
        {
            if ( y == 1 || y == height || x == 1 || x == width ) //判斷當y等於1或y等於長度或x等於1或x等於寬度
            {
                cout << "*"; //顯示*
            }
            else //其餘
            {
                cout << " "; //顯示空白
            }
            x++; //使x值+1
        }
        y++; //使y值+!
        cout << endl; //換行
        x = 1; //使x等於1
    }
    return 0;
}

