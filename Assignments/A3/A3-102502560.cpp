#include <iostream>
using namespace std;

int main()
{
	cout << "Draw a Rectangle.\n" << "Enter width and height.\n";

	int width, height;	//declare two variables to store width and height

	cout << "width: "; cin >> width;
	while(width<=0)
	{
		//warn the user and re-input if width is not in range
		cout << "illegal input!\n";
		cout << "width: "; cin >> width;
	}

	cout << "height: "; cin >> height;
	while(height<=0)
	{
		//warn the user and re-input if height is not in range
		cout << "illegal input!\n";
		cout << "height: "; cin >> height;
	}

	for(int y=1;y<=height;y++){									//for every row
		for(int x=1;x<=width;x++){								//for every colomn
			if(x==1||x==width||y==1||y==height){cout << "*";}	//print a star if the point is at the edge
			else{cout << " ";}									//print a space otherwise
		}
		cout << "\n";											//endline
	}
	return 0;
}
