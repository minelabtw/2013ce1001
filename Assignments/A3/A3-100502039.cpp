#include <iostream>
using namespace std;

int main(){

    int x, width, height = 0;

    cout << "Please input width and height." << endl;

    while(true){

        cout << "width :" << endl;
        cin >> x;

        if(x<=0)
            cout << "Please input right width." << endl;
        else{
            width = x;
            break;
        }
    }

    while(true){

        cout << "height :" << endl;
        cin >> x;

        if(x<=0)
            cout << "Please input right height." << endl;
        else{
            height = x;
            break;
        }
    }

    for(int i=0; i<width; i++)
        cout << "*";

    cout << endl;

    for(int i=0; i<height-2; i++){
        cout << "*";

        for(int i=0; i<width-2; i++)
            cout << " ";

        cout << "*" << endl;
    }

    for(int i=0; i<width; i++)
        cout << "*";

}
