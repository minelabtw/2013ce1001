#include<iostream>

using namespace std;
int main()
{
    int width=0, height=0;                                        //To define the varibles width and height
    int x=1,y=1;                                                  //To define the varibles x and y
    cout<<"Draw a Rectangle.\n"<<"Enter width and height.\n";
    do                                                            //Line 9~28:To make sure user haven't input illegal number
    {
        cout<<"width:";
        cin>>width;
        if(width<=0)
        {
            cout<<"illegal input!\n";
        }
    }
    while(width<=0);
    do
    {
        cout<<"height:";
        cin>>height;
        if(height<=0)
        {
            cout<<"illegal input!\n";
        }
    }
    while(height<=0);


                                                                  //Line 32~46:print the graph of the rectangle
    while(y<=height)                                              //Let value of height doesn't exceed the value user input
    {
        while(x<=width)                                           //Let value of width doesn't exceed the value user input
        {
            if(x==1||x==width||y==1||y==height)                   //Print * on the periphery of jthe rectangle
                cout<<"*";
            else
                cout<<" ";                                        //Let the rectangle inside empty
            x++;
        }
        cout<<endl;
        x=1;
        y++;

    }

    return 0;

}
