#include <iostream>

using namespace std;

int main()
{
    int width = 0; //宣告型別為int的寬度，並初始化其數值為0
    int height = 0; //宣告型別為int的高度，並初始化其數值為0

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    cout << "width: ";
    cin >> width; //輸入寬度

    while (width<0) //當寬度小於0，輸出illegal input!
    {
        cout << "illegal input!" << endl;
        cout << "width: ";
        cin >> width;
    }

    cout << "height: "; //輸入高度
    cin >> height;

    while (height<0) //當高度小於0，輸出illegal input!
    {
        cout << "illegal input!" << endl;
        cout << "height: ";
        cin >> height;
    }

    int w_min = 1;
    int h_min = 1;

    while (h_min<=height) //輸出圖案
    {
        while (w_min<=width)
        {
            if (w_min==1 or w_min==width or h_min==1 or h_min==height)
            {
                cout << "*";
                w_min++;
            }
            else
            {
                cout << " ";
                w_min++;
            }
        }
        cout << endl;
        w_min = 1;
        h_min++;
    }
    return 0;
}
