#include <iostream>

using namespace std;

int main(){
    int width, height;         //declare the integers
    cout << "Draw a Rectangle." << endl;        //output
    cout << "Enter width and height." << endl;
    while(1){
        cout << "width:";       //output
        cin >> width;           //input the width
        if(width>0) break;      //if legal then break
        cout << "illegal input!" << endl;   //else error
    }
    while(1){
        cout << "height:";
        cin >> height;
        if(height>0) break;     //if legal then break
        cout << "illegal input!" << endl;   //else error
    }

    int i,j;    //counter
    for(i=1;i<=height;i++){     //for loop
        for(j=1;j<=width;j++){  //for loop inside a for loop
            if(i==1 || j==1 || i==height || j==width)   //test if it's on the side
                cout << "*";        //output '*' if it's on the side
            else
                cout << " ";        //output a space if it's in the middle
        }
        cout << endl;               //swtich to next line at the end of line
    }
    return 0;
}
