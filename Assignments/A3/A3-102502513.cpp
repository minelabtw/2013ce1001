#include <iostream>
using namespace std;
int main()
{
    int width=-1 , length=-1;
    int x=1 , y=-1;

    cout << "Draw a Rectangle.\n";
    cout << "Enter width and length.\n";

    while(width<=0)                          //use "while" loop to input the value of width again if it is out of range.
    {
        cout << "width: ";
        cin >> width;

        if(width<=0)
            cout << "illegal input!\n";
    }

    while(length<=0)
    {
        cout << "length:";
        cin >> length;

        if(length<=0)
            cout << "illegal input!\n";
    }

    while(y>=-length)                        //use "while" loop and make y run from up(-1) to down(-length).
    {
        while(x<=width)                      //use "while" loop and make x run from left(1) to right(width).
        {
            if (y==-1)                       //use this order to depict the up line of the rectangle.
                cout << "*";
            else if (y==-length)             //use this order to depict the down line of the rectangle.
                cout << "*";
            else if (x>1&&x<width)           //use this order to deal with the blank inside the rectangle.
                cout << " ";
            else if (x<=width)               //use this order to depict the both sides of the rectangle.
                cout << "*";
            x++;
        }
        x=1;                                 //initialize x=1 again to make x run from left(1) to right(width) again.
        y--;
        cout << endl;                        //use this order to let "*" jump to following line and output again.
    }

    return 0;
}
