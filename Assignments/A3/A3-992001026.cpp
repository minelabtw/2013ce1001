#include<iostream>

using namespace std ;

int main () {
// 宣告 四個變數 為整數
    int x , y , i ,j ;

// 輸出
    cout << "Draw a Rectangle.\nEnter width and height.\nwidth: " ;
    cin >> x ;

//判斷 輸入質是否正確 不能是負的或零 錯誤者再重新輸入一次
    while ( x < 1 ) {
        cout << "illegal input!" << endl ;
        cout << "width: " ;
        cin >> x ;
    }
    cout  << "height: " ;
    cin >> y ;
    while ( y < 1 ) {
        cout << "illegal input!" << endl ;
        cout << "height: " ;
        cin >> y ;
    }

// 印出 圖形
    for ( i = 0 ; i < y ; ++i ) {
        for ( j = 0 ; j < x ; ++j ) {
            cout << ( i == 0 || j == 0 || i == y-1 || j== x-1 ? "*" : " " );
        }
        cout << endl ;
    }
    return 0 ;
}

