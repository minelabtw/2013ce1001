#include <iostream>
using namespace std;

int main()
{
    int width =0;
    int height =0; //宣告變數型別為整數的高與寬，並初始化為0
    int widthfixed =0;
    int heightfixed =0; //宣告變數型別為整數的固定高與寬不隨迴圈改變值，並初始化為0

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;
    cout << "width: ";
    cin >> width; //顯示題目需求並要求輸入寬
    while (width <=0) //要求寬度必須>0
    {
        cout << "illegal input!" << endl;
        cout << "width: ";
        cin >> width;
    }

    cout << "height: "; //要求輸入高
    cin >> height;
    while (height <=0) //要求高度必須>0
    {
        cout << "illegal input!" << endl;
        cout << "height: ";
        cin >> height;
    }

    widthfixed = width;
    heightfixed = height; //寬與高的值給固定的寬與高

    while (height >0) //做一個高度的迴圈
    {
        while (width >0) //做一個寬度的迴圈
        {
            if (width == 1 || width == widthfixed)
            {
                cout << "*";
            }
            else if (height == 1 || height == heightfixed)
            {
                cout << "*";
            }
            else
            {
                cout << " ";
            } //做出一個中空的長方形
            width--; //寬度變數每次-1滿足寬度迴圈
        }
     cout << endl;
     width = widthfixed; //換行並將寬度變數值初始化為原始輸入值
     height--; //高度變數每次-1滿足高度迴圈
    }
    return 0;
}
