#include<iostream>

using namespace std;

int main()
{
    int width,height;
    int x = 1;
    int y = 1;

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    cout << "width: ";
    cin >> width ;

    while (width <= 0)
    {
        cout << "illegal input!" << endl <<  "width:";     //確認輸入的值是否在範圍內
        cin >> width;
    }
    cout << "height:" ;
    cin >> height ;
    while (height <= 0)
    {
        cout << "illegal input!" << endl <<  "height:";   //確認輸入的值是否在範圍內
        cin >> height;
    }

    while(y<=height)
    {
        while(x<=width)
        {
            if(y==1)                                     //第一列,輸出與width等值的*
            {
                cout << "*";
                x++;
            }
            else if(x==1 || x==width)                    //第二列開始,開頭跟結尾輸出*
            {
                cout <<"*";
                x++;
            }
            else if(y<height)                             //第二列開始,中間輸出空格
            {
                cout <<" ";
                x++;
            }
            else                                         //最後一列,輸出跟width等值的*
            {
                cout << "*";
                x++;
            }
        }
        cout << endl;
        x = 1;
        y ++;                                              //換列
    }
    return 0;
}
