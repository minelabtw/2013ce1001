/*  請寫出一個程式，能依據給定的長、寬在螢幕上畫出中空矩形
長、寬須為大於0的整數，有不合規定的輸入要提示"illegal input!“並要求重新輸入 */

#include<iostream>
using namespace std;

int main()
{
    int width;          //宣告整數
    int height;
    int a=0;
    int b=0;

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    do                                  //做
    {
        cout << "width: ";
        cin >> width;

        if(width<=0)                    //如果width < 0, 印出 "illegal input!"
        {
            cout << "illegal input!" << endl;
        }
    }
    while(width<=0);                //而且跑迴圈, 回到do

    do
    {
        cout << "height: ";
        cin >> height;

        if(height<=0)
        {
            cout << "illegal input!" << endl;
        }
    }
    while(height<=0);


    while(b<=height)            //當b小於等於我們輸入的height就一直迴圈
    {
        while (a<=width)        //當a小於等於我們輸入的width就一直迴圈
        {
            if(a==0 || b==0)        //先判斷a或者b等於0就輸出 "*"
            {
                cout << "*";
            }
            else if(a==width || b==height) //然後再看如果a等於width或者b等於height, 印出 "*"
            {
                cout << "*";
            }
            else                            //如果上面兩個都沒有就印出空白
            {
                cout << " ";
            }
            a++;
        }
        cout << endl;           //指到第二行
        a=0;                    //把0給a
        b++;
    }
    return 0;
}
