#include <iostream>
using namespace std;

int main()
{
    int height;                                 //定義一整數height，作為長方形的高
    int width;                                  //定義一整數width，作為長方形的寬

    cout<<"Draw a Rectangle.\n";                //輸出"Draw a Rectangle."並換行
    cout<<"Enter width and height.\n";          //輸出"Enter width and height."並換行
    cout<<"width:";                             //輸出"width:"
    cin>>width;                                 //輸入一整數，取代width的位置
    cout<<"height:";                            //輸出"height:"
    cin>>height;                                //輸入一整數，取代height的位置

    while (height==0||height<0)                 //若height等於0或小於0
    {
        cout<<"illegal input!\n";               //則輸出"illegal input!"並換行
        cout<<"Enter height.\n";                //重新輸入一次
        cin>>height;
    }

    while (width==0||width<0)                   //若width等於0或小於0
    {
        cout<<"illegal input!\n";               //則輸出"illegal input!"並換行
        cout<<"Enter width.\n";                 //重新輸入一次
        cin>>width;
    }



    for (int y=height;y>=1;y--)                 //定義一整數y其初始值為所輸入的height，往下檢驗，到其值大於等於1時停下
    {
        for (int x=1;x<=width;x++)              //定義一整數x其初始值為1，往上檢驗，到其值大於等於所輸入width時停下
        {
            if (y==height){cout<<"*";}          //當y等於height時，輸出*
            else if (y==1){cout<<"*";}          //當y等於1時，輸出*
            else if (x==width){cout<<"*";}      //當x等於width時，輸出*
            else if (x==1){cout<<"*";}          //當x等於1時，輸出*
            else {cout<<" ";}                   //其餘皆輸出空格
        }
        cout<<endl;                             //輸出結果換行
    }
















}
