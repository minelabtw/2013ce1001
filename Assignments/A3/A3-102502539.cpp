#include <iostream>

using namespace std;

int main()
{
    int width;
    int height;
    int x = 0;
    int y = 0;

    cout << "Draw a Rectangle.\n" << "Enter width and height.\n" ;

    cout << "width: " ;
    cin >> width;
    while ( width < 0 )     //寬度範圍限制
    {
        cout << "illegal input!\n" << "width: " ;
        cin >> width;
    }

    cout << "height: " ;
    cin >> height;
    while ( height < 0 )    //高度範圍限制
    {
        cout << "illegal input!\n" << "height: " ;
        cin >> height;
    }

    while ( y < height )
    {
        if ( x == width -1 )    //右邊邊框
        {
            cout << "*" << endl;
            y++;
            x = -1 ;
        }
        else if ( x == 0 )  //左邊邊框
            cout << "*" ;
        else
            cout << " " ;
        x++;
        while ( y == 0 || y == height -1 )  //上下邊框
        {
            cout << "*" ;
            x++;
            if ( x == width )
            {
                cout << endl;
                y++;
                x = 0;
            }
        }
    }

    return 0;
}
