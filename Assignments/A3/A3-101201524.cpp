#include<iostream>
using namespace std;

int main()
{
    int x=1,                                     //宣告x,ymin做為矩形排數和行數的變數起始值為1由左往右、由下往上算
        ymin=1,
        width,height;                            //宣告變數width,height來接收之後的輸入的寬和高

    cout << "Draw a Rectangle.\n";               //輸出字串"Draw a Rectangle."
    cout << "Enter width and height.\n";         //輸出字串"Enter width and height."
    cout << "width: ";                           //輸出字串"width: "
    cin >> width;                                //將輸入的數字傳給width

    while(width<=0)                              //當輸入的數字<=0時，輸出字串"illegal input!"，並重新要求輸入新數字
    {
        cout << "illegal input!\n";
        cout << "width: ";
        cin >> width;
    }

    cout << "height: ";                          //輸出字串"height: "
    cin >> height;                               //將輸入的數字傳給height
    while(height<=0)
    {
        cout << "illegal input!\n";              //當輸入的數字<=0時，輸出字串"illegal input!"，並重新要求輸入新數字
        cout << "height: ";
        cin >> height;
    }

    while(x<=width)                              //在第一行，當位置在寬度範圍之內時，輸出"*"並移到下一排繼續判斷
    {
        cout << "*";
        x++;
    }
    cout << "\n";                                //換到下一行，x回歸到1，height-1(代表下一行)
    height--;
    x=1;

    while(height>ymin)                           //當新的height(小於高)>=ymin(在最下面第二行之上)時，執行以下動作
    {
        while(x<=width)                          //當新的x(大於0)<=width(在最右邊一排之左)時，執行以下動作
        {
            if(x==1 || x==width)                 //當座標在左邊框或右邊框時，顯示"*"，中間則則留空" "
                cout << "*";
            else
                cout << " ";
            x++;                                 //移到右邊一個座標繼續做判斷
        }
        cout << "\n";                            //換到下一行，x回歸到1，height-1(代表下一行)
        height--;
        x=1;
    }

    while(x<=width)                              //到了最後一行則像第一行一樣，每個座標都顯示"*"
    {
        cout << "*";
        x++;
    }

    return 0;
}
