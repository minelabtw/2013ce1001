#include <iostream>

using namespace std;

int main()
{
    int width,height;//宣告整數width以及height
    int x=0;         //宣告x初始為0
    int y=0;         //宣告y初始為0

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    cout << "width: ";   //將width的值輸入
    cin >> width;
    while ( width <= 0 ) //當width小於等於0時
    {
        cout << "illegal input!" << endl << "width: ";//輸出字彙並換行重新輸入
        cin >> width;
    }

    cout << "height: ";  //將height的值輸入
    cin >> height;
    while ( height <= 0 )//當height小於等於0時
    {
        cout << "illegal input!" << endl << "height: ";//輸出字彙並換行重新輸入
        cin >> height;
    }

    while ( y >= -height + 1)          //當y大於等於-height加1
    {
        while ( x <= width - 1)        //當x小於等於width減1
        {
            if ( y == 0 )              //如果y=0時輸出符號*
                cout << "*";
            else if ( x == 0 )         //如果x=0時輸出符號*
                cout << "*";
            else if ( x == width -1 )  //如果x等於width減1時輸出符號*
                cout << "*";
            else if ( y == -height + 1)//如果y等於-height加1時輸出符號*
                cout << "*";
            else
                cout << " ";           //其他狀況則輸出空格
            x++;        //x作向下一位動作
        }
        cout << endl;
        x = 0;          //x歸於原數值
        y--;            //y作換行動作
    }

    return 0;
}
