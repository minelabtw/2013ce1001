#include <iostream> // allows prohram to output data to the screem

using namespace std; //program uses names from the std namespace

int main ()// function main begins program execution
{
    int i;//corresponds to height
    int j;//corresponds to width
    int width;//width
    int height;//height

    cout<<"Draw a Rectangle.\nEnter width and height.\n";

    while(1) // loop of width. If width>0,then break loop.
    {
        cout<<"width:";//prompt user for width
        cin>>width;
        if(width<=0)//Determine whether the value of the input width is greater than 0
            cout<<"illegal input!\n";
        else  if (width>0)
            break;
    }

    while(1)// loop of height. If height>0,then break loop.
    {
        cout<<"height:";//prompt user for height
        cin>>height;
        if(height<=0)//Determine whether the value of the input height is greater than 0
            cout<<"illegal input!\n";
        else if (height>0)
            break;
    }
    for(i=0; i<height; i++)//for statement header includes initialization(initial value =0), loop-continuation condition(for i < height , loop continue) and increment.

    {
        if(i==0||i==height-1)//Upper and lower ends of Rectangle
        {
            for(j=0; j<width; j++)//for statement header includes initialization(initial value =0), loop-continuation condition(for j< weight , loop continue) and increment.
                cout<<"*";
            cout<<endl;
        }
        else
            for(j=0; j<width; j++)//for statement header includes initialization, loop-continuation condition and increment.
            {
                if(j==0)
                    cout<<"*";
                else if (j==width-1)
                    cout<<"*"<<endl;
                else
                    cout<<" ";//Hollow Rectangle
            }
    }

    return 0;//indicate that program ended successfully

}

