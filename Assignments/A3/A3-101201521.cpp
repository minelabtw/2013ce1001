#include <iostream>
using namespace std;

int main()
{
    int width=0, height=0, x=0, y=0;    //宣告型別為整數，寬，長，x軸，y軸，並設定初始值為0。
    cout << "Draw a Rectangle.\n";
    cout << "Enter width and height.\n";
    while (1)   //讓while條件永遠對。
    {
        cout << "width: ";
        cin >> width;
        if(width<=0)
            cout << "illegal input!\n";     //若輸入非法值則輸出警告並重新詢問寬度
        else
            break;  //若輸入合法值則跳出while
    }
    while (1)
    {
        cout << "height: ";
        cin >> height;
        if(height<=0)
            cout << "illegal input!\n";     //若輸入非法值則輸出警告並重新詢問長度。
        else
            break;  //若輸入合法值則跳出while。
    }
    y=height;   //設定y為長度。
    x=width;    //設定x為寬度。
    while(y>0)  //長度界限。
    {
        while(x>0)  //寬度界限。
        {
            if (y==height || y==1|| x==width || x==1)
                cout << "*";   //當為矩形外圍時輸出*。
            else
                cout << " ";    //其餘輸出空白。
            x--;    //讓x改變。
        }
        x=width;    //初始化x值。
        cout << endl;
        y--;    //讓y改變。
    }
    return 0;
}
