#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a=0;       //宣告一個整數變數a，初始化其值為0，用以存放長值
    int b=0;       //宣告一個整數變數b，初始化其值為0，用以存放寬值
    int x=1;       //宣告一個整數變數x，初始化其值為1，作為繪圖所需之變數
    int y=1;       //宣告一個整數變數y，初始化其值為1，作為繪圖所需之變數

    cout<<"請輸入長值:";
    cin>>a;
    while(a<=0)                //當輸入長值小於等於0時，輸出"illegal input"至螢幕，並迴圈之
    {
        cout<<"illegal iput";
        cout<<endl;
        cin>>a;
    }

    cout<<"請輸入寬值:";       //當輸入寬值小於等於0時，輸出"illegal input"至螢幕，並迴圈之
    cin>>b;
    while(b<=0)
    {
        cout<<"illegal input";
        cout<<endl;
        cin>>b;
    }
    while(y <= a)
    {
        while(x<=b)                 //將繪製長寬值得變數x,y限制在我們所輸入的a,b值內
        {
            if(y==a || y==1)        //繪製出上下兩邊
                cout<<"*";
            else if(x==b || x==1)   //繪製出左右兩邊
                cout<<"*";
            else
                cout<<" ";          //繪製出中空部分
            x++;                    //重複將x從初始值1累加至上限b
        }
        cout<<endl;
        x=1;                        //將x值歸0
        y++;                        //重複將y從初始值1累加至上限a
    }
    return 0;
}
