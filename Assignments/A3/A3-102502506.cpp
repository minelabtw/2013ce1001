#include<iostream>
using namespace std;

int main()
{
    int width = 0;
    int height = 0;
    int x = 0;
    int y = 0;

    cout << "Draw a Rectangle." << endl;
    cout << "Enter width and height." << endl;

    cout << "width: ";
    cin >> width;  //輸入寬度

    while ( width <= 0 )  //若寬度小於或等於零，執行下列迴圈再次輸入寬度
    {
        cout << "illegal input!" << endl;

        cout << "width: ";
        cin >> width;
    }

    cout << "height: ";
    cin >> height;  //輸入高度

    while ( height <= 0 )  //若高度小於或等於零，執行下列迴圈再次輸入高度
    {
        cout << "illegal input!"<<endl;

        cout << "height: ";
        cin >> height;
    }

    for ( y = 0; y < height; y++ )  //建立y座標，大小等於高度
    {
        for ( x = 0; x < width; x++ )  //建立x座標，大小等於寬度
        {
            if ( x == 0 )  //x座標等於0時，輸出*
                cout << "*";
            else if ( x == width-1 )  //x座標等於寬度減1時，輸出*
                cout << "*";
            else if ( y == 0 )  //y座標等於0時，輸出*
                cout << "*";
            else if ( y == height-1 )  //y座標等於高度減1時，輸出*
                cout << "*";
            else  //其餘空白
                cout << " ";
        }
        cout << endl;
    }

    return 0;

}

