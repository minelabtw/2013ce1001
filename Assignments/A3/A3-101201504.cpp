#include<iostream>
using namespace std ;
int
main()
{
    int width ;
    int height ;
    int i ;
    int j ;

    cout << "Draw a Rectangle.\nEnter width and height.\n" ;

    cout <<"width:" ; //先在螢幕輸出width
    cin  >> width ;   //輸入width
    while(width<=0)
    {

        cout << "illegal input!\n" ;   //如果符合while()裡的要求則出現
        cout <<"width:" ;              //螢幕輸出width
        cin  >> width ;                //輸入width
    }
    cout << "height:" ;         //先在螢幕輸出height
    cin >> height ;             //輸入height
    while(height <=0)
    {
        cout <<"illegal input!\n" ;     //如果符合while()裡的要求則出現
        cout <<"height:" ;              //螢幕輸出height
        cin >>height ;                  //輸入height

    }

    for(i=1; i<=width; ++i)
    {
        for(j=1; j<=height; ++j)
        {
            if(i == 1 || i== width ||  j== 1 || j == height) //如果屬於if()中的入一條件
                cout <<"*" ;                                 //將會在螢幕上輸出*
            else                                             //其餘狀況
                cout <<" " ;                                 //將會在螢幕上輸出
        }
        cout << "\n" ;
    }

    return 0 ;
}
