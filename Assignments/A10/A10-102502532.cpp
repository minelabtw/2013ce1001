#include<iostream>
using namespace std;

const int ArraySize =20;                     //不能不打const
int grades[ ArraySize ]= {};          //宣告成績陣列    初始=0
int GetMax(int arr[ArraySize],int _size);           //回傳 最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);            //回傳 最小值
double GetAvg(int arr[ArraySize],int _size);            //回傳 平均值

int main()
{
    int grade =0;
    int _size =1;

    cout<<"Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades.\n";

    while (grade != -1)
    {
        cout<<"Enter grade?: ";
        cin>>grade;
        if (grade == -1)
            break;                   //跳出while迴圈
        while (grade <0 or grade >100)
        {
            cout<<"Out of range!\nEnter grade?: ";
            cin>>grade;
        }
        grades[_size] = grade;
        _size++;                    //最後一個分數 +1個
        if (_size-1 == 20)
            break;                      //20個
    }

    cout<<"The total number of grades is "<<_size-1<<".";            //多1
    cout<<"\nThe maximum of grades is "<<GetMax(grades,_size)<<".";         //傳入array name就可
    cout<<"\nThe minimum of grades is "<<GetMin(grades,_size)<<".";
    cout<<"\nThe average of grades is "<<GetAvg(grades,_size)<<".";

    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int Maxgrade =0;                        //注意 初始值
    for (int i =1; i<=_size; i++)
    {
        if (arr[i] > Maxgrade)
        {
            Maxgrade = arr[i];
        }
    }
    return Maxgrade;
}
int GetMin(int arr[ArraySize],int _size)
{
    int Mingrade =100;
    for (int i =1; i<_size; i++)               //最後一個值=0
    {
        if (arr[i] < Mingrade)
        {
            Mingrade = arr[i];
        }
    }
    return Mingrade;
}
double GetAvg(int arr[ArraySize],int _size)
{
    double total =0;                         //要另外宣告一個變數
    for (int i =1; i<=_size; i++)
    {
        total += arr[i];
    }
    return total/(_size-1);
}


