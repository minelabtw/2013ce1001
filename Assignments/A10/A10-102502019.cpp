#include<iostream>

using namespace std;

int maxn(int [], int );
int minn(int [], int );
double avg(int [], int );

int main()
{
    int x=0,counter=0,result[20]={},maxnumber=0,minnumber=0;
    cout <<"Please enter students grades.\n"<<"Maximum number of grades is 20.\n"<<"You can also enter '-1' to finish enter grades.\n";
    while(counter<=20)
    {
       while(x<0 || x>100)
       {
           cout <<"Out of range!\n"<<"Enter grade?: ";
           cin >>x;
       }
       cout <<"Enter grade?: ";
       cin >>x;
       result[counter]=x;
       if(x==-1)
       {
           break;
       }
       counter++;
    }
    cout <<"The total number of grades is "<<counter<<".\n";
    cout <<"The maximum of grades is "<<maxn(result,counter)<<".\n";
    cout <<"The minimum of grades is "<<minn(result,counter)<<".\n";
    cout <<"The average of grades is "<<avg(result,counter)<<".\n";

}
int maxn(int a[],int size1)
{
    int large=0;
    for(int k=1; k<size1 ;k++)
    {
        int number=0;
        number=a[k];
        while((k>0) && (a[k-1]>number))
        {
            a[k]=a[k-1];
            k--;
        }
        a[k]=number;
        large=a[k];
    }
       return large;
}
int minn(int b[],int size2)
{
    int least=0;
    for(int k=1; k<size2 ;k++)
    {
        int number=0;
        number=b[k];
        while((k>0) && (b[k-1]>number))
        {
            b[k]=b[k-1];
            k--;
        }
        b[k]=number;
        least=b[0];
    }
       return least;
}
double avg(int c[],int size3)
{
    double tatal=0;
    for (int j=0; j<size3; j++)
    {
        tatal=tatal+c[j];
    }
    return tatal/size3;
}
