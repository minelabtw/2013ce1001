#include<iostream>
#define ArraySize 20
using namespace std;

double average(double grade[ArraySize],int _size);//呼叫計算的各函式
double findmax(double grade[ArraySize],int _size);
double findmin(double grade[ArraySize],int _size);

int main()
{
    double grade[20] = {};//建立存放20個數的array
    double score;//宣告分數變數
    int counter = 0;//宣告次數變數

    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl << "You can also enter '-1' to finish enter grades." << endl;

    do{
        do{
            cout << "Enter grade?:";
            cin >> score;
            if(score < -1 || score > 100)
            cout << "Out of range" << endl;
        }while(score < -1 || score > 100);
        if(score != -1)//檢查範圍
        {
            grade[counter] = score;
            counter++;//範圍內將數存入array,并加記一次
        }
        else break;
        }while(counter < 20);//小於20次

    cout << "The total number of grades is " << counter << endl;//輸出各結果
    cout << "The maximum of grades is " << findmax(grade,counter) << endl;
    cout << "The minimum of grades is " << findmin(grade,counter) << endl;
    cout << "The average of grades is " << average(grade,counter) << endl;

return 0;
}

double average(double grade[ArraySize],int _size)
{
    double total = 0;
    for(int a = 0;a <= _size;a++)
    {
        total += grade[a];
    }
    return total / _size;
}//定義平均數函式

double findmax(double grade[ArraySize],int _size)
{
    double maxima = grade[0];
    for(int i = 1;i < _size;i++)
    {
        if(maxima < grade[i])
            maxima = grade[i];
    }
return maxima;
}//定義最大值函式

double findmin(double grade[ArraySize],int _size)
{
    double minima = grade[0];
    for(int j = 1;j < _size;j++)
    {
        if(minima > grade[j])
            minima = grade[j];
    }
return minima;
}//定義最小值函式
