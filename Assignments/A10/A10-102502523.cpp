#include<iostream>
#define ArraySize 20
using namespace std;
int GetMax(int arr[ArraySize],int _size);//宣告函式
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
int main()
{
    int arr[ArraySize]= {};//宣告陣列
    int grade;//宣告變數
    int a=0;
    int _size=0;
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    for(int time=0; time<20; time++)//輸入分數並判斷範圍及次數
    {
        cout<<"Enter grade?: ";
        cin>>grade;
        while(grade>100||grade<-1)
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter grade?: ";
            cin>>grade;
        }
        if (grade==-1)
        {
            break;//若輸入-1則跳出迴圈
        }
        arr[a]=grade;//將分數指定給陣列
        a++;
        _size++;
    }
    cout<<"The total number of grades is "<<_size<<"."<<endl;//輸出結果

    cout<<"The maximum of grades is "<<GetMax(arr,_size)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(arr,_size)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(arr,_size)<<"."<<endl;
    return 0;
}
int GetMax(int arr[ArraySize],int _size)//找出最大值
{
    int maxx=0;
    for(int c=0; c<_size; c++)
    {
        if(arr[c]>maxx)
        {
            if(arr[c]>arr[c+1])
            {
                maxx=arr[c];
            }
            else if(arr[c]<arr[c+1])
            {
                maxx=arr[c+1];
            }
        }
    }
    return maxx;
}
int GetMin(int arr[ArraySize],int _size)//找出最小值
{
    int minn=100;
    for(int c=0; c<_size; c++)
    {
        if(arr[c]<minn)
        {
            if(arr[c]>arr[c+1])
            {
                minn=arr[c+1];
            }
            else if(arr[c]<arr[c+1])
            {
                minn=arr[c];
            }
        }
    }
    return minn;
}
double GetAvg(int arr[ArraySize],int _size)//算出平均值
{
    double avg=0;
    for(int c=0; c<_size; c++)
    {
        avg=avg+arr[c];
    }
    avg=avg/_size;
    return avg;
}
