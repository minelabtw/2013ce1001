#include <iostream>
using namespace std;
#define ArraySize 20
int GetMax(int arr[ArraySize],int _size);
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
int main()
{
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    int grade[ArraySize],tmp_grade,idx=0,i;
    for(i=0; i<20; i++)
    {
        cout<<"Enter grade?: ";
        while(cin>>tmp_grade&&(tmp_grade>100||tmp_grade<0)&&tmp_grade!=-1)
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter grade?: ";
        }
        if(tmp_grade==-1)break; //finish enter grade
        grade[idx++]=tmp_grade; //put grade into array
    }
    //output the answer
    cout<<"The total number of grades is "<<i<<'.'<<endl;
    cout<<"The maximum of grades is "<<GetMax(grade,idx)<<'.'<<endl;
    cout<<"The minimum of grades is "<<GetMin(grade,idx)<<'.'<<endl;
    cout<<"The average of grades is "<<GetAvg(grade,idx)<<'.'<<endl;
    return 0;
}
int GetMax(int arr[ArraySize],int _size)    //get max
{
    int Mx=0;
    for(int i=0; i<_size; ++i)
        Mx=max(Mx,arr[i]);
    return Mx;
}
int GetMin(int arr[ArraySize],int _size)    //get min
{
    int Mn=100;
    for(int i=0; i<_size; ++i)
        Mn=min(Mn,arr[i]);
    return Mn;
}
double GetAvg(int arr[ArraySize],int _size) //get avg
{
    double total=0.0;
    for(int i=0; i<_size; ++i)
        total+=arr[i];
    return total/_size;
}
