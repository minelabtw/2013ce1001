#include<iostream>
#define AS 20

using namespace std;


int Max(int Grade[AS],int g);                    //宣告找最大值的變數
int Min(int Grade[AS],int g);                    //宣告找最小值的變數
double Avg(int Grade[AS],int g);                 //宣告算平均的變數


int main ()
{

    int Grade[AS]={0},g=0;
    int i=0;
    double a=0;
    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    for(g=0;g<20;g++)
    {
        cout << "Enter grade?: ";
        cin >> Grade[g];
        while(Grade[g]<-1 || Grade[g]>100)
        {
            cout << "Out of range!"<< endl<< "Enter grade?: ";
            cin >> Grade[g];
        }
        if(Grade[g]==-1)
        {
           break;
        }
    }

     if(g!=20)                                          //若輸入不滿20次,取g-1次成績
    {
        g-1;
    }

    cout << "The total number of grades is " << g << "." << endl;
    cout << "The maximum of grades is " << Max(Grade,g) << "." << endl;
    cout << "The minimum of grades is " << Min(Grade,g) << "." << endl;
    cout << "The average of grades is " << Avg(Grade,g) << "." << endl;

}


int Max(int Grade[AS],int g)                 //傳入陣列跟次數
{
   int m1;
   m1= Grade[0];
   for(g=1;g<20;g++)
   {
      if(Grade[g]>m1)                       //找第一和第二比較,取大的,再找第三的比較...
      {
          m1=Grade[g];
      }

   }
   return m1;                               //回傳最大值
}

int Min(int Grade[AS],int g)                 //傳入陣列和次數
{
    int m2,p;
    m2=Grade[0];
    for(p=1;p<g;p++)
    {
        if(Grade[p]<m2)
        {
            m2=Grade[p];
        }
    }
    return m2;
}

double Avg(int Grade[AS],int g)
{
    int t=0;
    double k=0;
    for(t=0;t<g;t++)
    {
        k = k + Grade[t];
    }
    return  k/g;
}
