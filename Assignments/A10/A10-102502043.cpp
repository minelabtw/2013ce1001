#include <iostream>
#define ArraySize 20
using namespace std;
int GetMax(int arr[ArraySize],int _size);					//宣告方程式
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
int main()
{
    int a[ArraySize];
    int x;
    int i;
    cout<<"Please enter students grades."<<endl;					//顯示文字
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    for(i=0; i<ArraySize; i++)
    {
        cout<<"Enter grade?: ";								//進入迴圈
        cin>>x;
        if(x==-1)											//跳出迴圈
        {
            break;
        }
        while(x<0||x>100)									//限制範圍
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter grade?: ";
            cin>>x;
        }
        a[i]=x;												//存值
    }


    cout<<"The total number of grades is "<<i<<"."<<endl;			//顯示計算結果
    cout<<"The maximum of grades is "<<GetMax(a,i)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(a,i)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(a,i)<<"."<<endl;

    return 0;
}
int GetMax(int arr[ArraySize],int _size)					//計算極大值
{
    int m=0;
    for(int p=0; p<_size; p++)
    {

        if(m<=arr[p])
        {
            m=arr[p];
        }

    }
    return m;
}
int GetMin(int arr[ArraySize],int _size)					//計算極小值
{
    int m=100;
    for(int p=0; p<_size; p++)
    {
        if(m>=arr[p])
        {
            m=arr[p];
        }
    }
    return m;
}
double GetAvg(int arr[ArraySize],int _size)					//計算平均值
{
    double c=0.0;
    int p;
    for(p=0; p<_size; p++)
    {
        c=c+arr[p];
    }
    c=c/_size;
    return c;
}
