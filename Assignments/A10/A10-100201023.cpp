#include <iostream>
#define ArraySize 20

using namespace std;

int GetMax(int arr[],int count);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[],int count);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[],int count);//回傳平均值 傳入陣列、成績總次數

int main()
{
	int score[ArraySize] , count = 0 , grade = 0;

	cout << "Please enter students grades." << endl;
	cout << "Maximum number of grades is 20." << endl;
	cout << "You can also enter '-1' to finish enter grades." << endl;

	while(count < 20)
	{
		cout << "Enter grade?: ";
		cin >> grade; // input grade

		if(grade < -1 || grade > 100) // determine out of range
			cout << "Out of range!" << endl;
		else if(grade == -1) // determine end of input
			break;
		else
		{
			score[count] = grade;
			++count;
		}
	}

	cout << "The total number of grades is " << count << ".\n";
	cout << "The maximum of grades is " << GetMax(score , count) << ".\n";
	cout << "The minimum of grades is " << GetMin(score , count) << ".\n";
	cout << "The average of grades is " << GetAvg(score , count) << ".\n";

	return 0;
}

int GetMax(int arr[],int count)//回傳最大值 傳入陣列、成績總次數
{
	int max = 0;

	for(int i = 0 ; i < count ; ++i)
		if(max < arr[i])
			max = arr[i];

	return max;
}

int GetMin(int arr[],int count)//回傳最小值 傳入陣列、成績總次數
{
	int min = 100;

	for(int i = 0 ; i < count ; ++i)
		if(min > arr[i])
			min = arr[i];

	return min;
}

double GetAvg(int arr[],int count)//回傳平均值 傳入陣列、成績總次數
{
	double total = 0;

	if(count == 0)
		return 0;

	for(int i = 0 ; i < count ; ++i)
		total += arr[i];

	return total / count;
}