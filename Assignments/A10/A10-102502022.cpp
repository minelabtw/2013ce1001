#include<iostream>
#define ArraySize 20
using namespace std;
int GetMax(int [],int );          //宣告函數
int GetMin(int [],int );
double GetAvg(int [],int );
int main()
{
    int g=0;   //宣告輸入的成績為變數
    int grades[ArraySize];    //宣告陣列
    int t=0;
    int x=1;
    cout<< "Please enter students grades.\n";
    cout<< "Maximum number of grades is 20.\n";
    cout<< "You can also enter '-1' to finish enter grades.\n";
    for(int z=0; z<20; z++)        //for迴圈 重複執行20次
    {
        cout<< "Enter grade?: ";
        cin>> g;
        while(g<-1 || g>100)      //while迴圈
        {
            cout<< "Out of range!\n";
            cout<< "Enter grade?: ";
            cin>> g;
        }
        if(g==-1)      //假設
            break;
        if(0<=g&&g<=100)
        {
            t++;
        }
        grades[x]=g;     //存入陣列
        x++;

    }
    cout<< "The total number of grades is "<< t<< endl;
    int Max=GetMax(grades,t);                          //將函數傳入變數在下面輸出
    cout<< "The maximum of grades is "<< Max << endl;
    int Min=GetMin(grades,t);
    cout<< "The minimum of grades is "<< Min << endl;
    double Ave=GetAvg(grades,t);
    cout<< "The average of grades is "<< Ave << endl;

    return 0;
}

int GetMax(int arr[ArraySize],int _size)       //函數
{
    for(int a=1;a<= _size;a++)      //for迴圈   執行輸入程式輸入的次數
    {
        for(int a1=a+1;a1<= _size;a1++)    //比較大小然後排列
        {
            if(arr[a]>arr[a1])
            {
                int temp1=arr[a];
                arr[a]=arr[a1];
                arr[a1]=temp1;
            }
        }
    }
    int Maxnum=arr[_size];
    return Maxnum;             //回傳值
}
int GetMin(int arr[ArraySize],int _size)
{
    for(int b=1;b<= _size;b++)
    {
        for(int b1=b+1;b1<= _size;b1++)
        {
            if(arr[b]>arr[b1])
            {
                int temp2=arr[b];
                arr[b]=arr[b1];
                arr[b1]=temp2;
            }
        }
    }
    int Minnum=arr[1];
    return Minnum;
}
double GetAvg(int arr[ArraySize],int _size)
{
    int sum=0;             //宣告變數
    for(int c=1;c<=_size;c++)      //for迴圈
    {
        sum=sum+arr[c];
    }
    double average= (double)sum/_size;
    return average;                         //回傳值
}







