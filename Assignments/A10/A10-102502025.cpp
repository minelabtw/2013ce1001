#include <iostream>
#define ArraySize 20        //設矩陣數為20
using namespace std;

int MAX(int,int []);        //最大值func
int MIN(int,int []);        //最小值func
double AVG(int,int []);     //均值func

int main()
{
    cout << "Please enter students grade.\n"
         << "Maximum number of grade is 20.\n"
         << "You can also enter '-1' to finish enter grade.\n";

    int input=-2;       //輸出值
    int total=0;        //總數
    int grade[ArraySize+1];     //grade矩陣

    for(int m=0; m<20; m++)     //20次輸入迴圈
    {
        while(input<-1||input>100)      //錯誤迴圈
        {
            cout << "Enter grade? :";
            cin >> input;
            if(input<-1||input>100)
            {
                cout << "Out of range!\n";
            }
        }
        if(input>-1&&input<=100)        //計算總數與加入矩陣
        {
            total++;
            grade[total]=input;
        }
        if(input==-1)       //跳出輸入grade
        {
            break;
        }
        else if(input>-1&&input<=100)       //恢復值
        {
            input=-2;
        }
    }

    cout << "The total number of grade is " << total << ".\n";
    cout << "The maximum of grade is " << MAX(total,grade) << ".\n";
    cout << "The minimum of grade is " << MIN(total,grade) << ".\n";
    cout << "The average of grades is " << AVG(total,grade) << ".\n";

    return 0;
}

int MAX(int total,int grade[])      //MAX func計算
{
    int low=0;
    for(int k=1; k<=total; k++)
    {
        if(grade[k]>=low)
        {
            low=grade[k];
        }
    }
    return low;
}
int MIN(int total,int grade[])      //MIN func計算
{
    int high=100;
    for(int k=1;k<=total;k++)
    {
        if(grade[k]<=high)
        {
            high=grade[k];
        }
    }
    return high;
}
double AVG(int total,int grade[])       //均值func計算
{
    double avg=0;
    double avgtotal=0;
    for(int j=1; j<=total; j++)
    {
        avgtotal=avgtotal+grade[j];
    }
    avg = avgtotal / total;
    return avg;
}
