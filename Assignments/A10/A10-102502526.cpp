#include <iostream>
#include <cstdlib>      //contain srand() and rand()
#include <ctime>        //contain time()

#define arraysise 20  //把arraysize定義成20
using namespace std;

int M(int g[20], int b)  //最大值函數
{
    int x=g[0];        //設定一個變數x，初始化其值為第一個陣列
    for(int i=0; i<b; i++)  //當x遇到比他大的陣列值，就把x代換成其陣列值
    {
        if (x < g[i])
            x = g[i];
    }
    return x;
}
int m(int g[20], int b)  //最小值函數
{
    int y=g[0];
    for(int i=0; i<b; i++)  //全與最大值一樣，不過是遇到比它小的才代換
    {
        if (y > g[i])
            y=g[i];
    }
    return y;
}

double avg(int g[20], int b)   //平均值函數
{
    double z=0;
    for(int i=0; i<b; i++)
    {
        z+=g[i];   //把所有陣列加起來丟到z
    }
    z=z/b;
    return z;
}

int main()
{
    int a;   //成績值
    int b = 0;   //輸入幾個成績
    int g[arraysise];   //陣列
    cout<<"Please enter students grades."<<endl<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    for(int blabla = 0; blabla<20; blabla++)
    {
        cout<<"Enter grade?: ";
        cin>>a;
        if(a==-1)
            break;
        while(a<0||a>100)
        {
            cout<<"Out of range!"<<endl<<"Enter grade?: ";
            cin>>a;
        }
        g[b]=a;
        b++;
    }
    for (int i=0; i<b; i++)     //把曾輸入過的成績照輸入順序輸出
    {
        cout << g[i] << endl;
    }

    cout<<"The total number of grades is "<< b <<"."<<endl;
    cout<<"The maximum of grades is "<<M(g, b)<<"."<<endl;
    cout<<"The minimum of grades is "<<m(g, b)<<"."<<endl;
    cout<<"The average of grades is "<<avg(g, b)<<"."<<endl;


return 0;
}
