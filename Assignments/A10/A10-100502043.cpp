#include<iostream>
#define ArraySize 20

using namespace std;

int GetMax(int arr[ArraySize],int _size)  //找出陣列最大元素之函式
{
    int maxgrade;
    maxgrade=arr[0];  //預設最大值為陣列第一項元素
    for (int j=1;j<_size;j++)  //再用for loop一一比較
    {
        if (maxgrade<arr[j])  //用if判斷較大值
            maxgrade=arr[j];
    }

    return maxgrade;  //回傳最大值
}
int GetMin(int arr[ArraySize],int _size)  //找出陣列最小元素之函式
{
    int mingrade;
    mingrade=arr[0];  //預設最小值為陣列第一項元素
    for (int j=1;j<_size;j++)  //並用for loop一一比較
    {
        if (mingrade>arr[j])  //用if判斷較小值
            mingrade=arr[j];
    }

    return mingrade;  //回傳最小值
}
double GetAvg(int arr[ArraySize],int _size)  //算出陣列元素平均值之函式
{
    double sum=0;  //宣告總和並預設初始值=0
    for (int j=0;j<_size;j++)  //用for loop一一相加陣列所有元素
        sum=sum+arr[j];

    return sum/_size;  //回傳總和除以陣列大小之值(平均值)
}

int main()
{
    int grade=-2;  //宣告輸入成績之變數，並使其初始值為-2
    int grades[ArraySize];  //宣告儲存成績之陣列
    int _size=0;  //宣告實際陣列大小之變數

    cout << "Please enter students grades.\nMaximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish entering grades." << endl;

    for (int i=0;i<20;i++)  //用for loop做重覆詢問
    {
        cout << "Enter grade?: ";
        cin >> grade;

        if (grade<-1||grade>100)  //if判斷成績是否介於0~100之間
        {
            cout << "Out of range!" << endl;
            i=i-1;  //若超過範圍則回復上一loop
        }

        grades[i]=grade;  //將輸入之成績傳入陣列
        _size=i;  //將輸入之次數傳入陣列大小的變數

        if (i==19)  //debug
            _size=20;
        if (grade==-1)  //輸入-1則跳出迴圈
            break;
    }

    cout << "The total number of grades is " << _size << "." << endl;  //印出成績之輸入次數、最大、最小值和平均值
    cout << "The maximum of grades is " << GetMax(grades,_size) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,_size) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,_size) << ".";

    return 0;
}
