#include<iostream>
#include<algorithm>
#define ArraySize 20
using namespace std;

int GetMax(int arr[ArraySize],int n)//回傳最大值 傳入陣列、成績總次數
{
    return *max_element(arr,arr+n);//利用內建函式找出最大值
}
int GetMin(int arr[ArraySize],int n)//回傳最小值 傳入陣列、成績總次數
{
    return *min_element(arr,arr+n);//利用內建函式找出最大值
}
double GetAvg(int arr[ArraySize],int n)//回傳平均值 傳入陣列、成績總次數
{
    double sum=0;
    for(int i=1; i<=n; i++)
    {
        sum+=arr[i-1];
    }
    return sum/n;
}
int main()
{
    int grades[ArraySize]= {0};
    int n=1,grade=0;
    cout<<"Please enter students grades"<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    for(int i=1; i<=ArraySize; i++)//使次數限制在20次內
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grade;
            if((grade<0||grade>100)&&grade!=-1)
                cout<<"Out of range!\n";
            else if(grade==-1)
                break;
            else
                grades[i-1]=grade;
        }
        while(grade<0||grade>100);//判斷grade的數
        n=i;
        if(grade==-1)
        {
            n=i-1;//輸入-1的不算次數
            break;
        }
    }
    cout<<"The total number of grades is "<<n<<"."<<"\n";
    cout<<"The maximum of grades is "<<GetMax(grades,n)<<"."<<endl
        <<"The minimum of grades is "<<GetMin(grades,n)<<"."<<endl
        <<"The average of grades is "<<GetAvg(grades,n)<<"."<<endl;
    return 0;
}
