#include <iostream>
#define ArraySize 20
#include <iomanip>
using namespace std;
int GetMax(int arr[ArraySize],int _size);
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
int main()
{
    int a,b=-1,_size,mx,mn;
    double ave;
    int arr[ArraySize];
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    cout<<"Enter grade?: ";
    while(cin>>a)
    {
        if(a==-1)
            break;
        else if(a<0 || a>100)
        {
            cout<<"out of range";
            cin>>a;
        }
        else
        {
            b++;
            arr[b]=a;
        }
        if(b==19)
        break;
        cout<<"Enter grade?: ";
    }
    _size=b+1;
    mx=GetMax(arr,_size);//回傳最大值 傳入陣列、成績總次數
    mn=GetMin(arr,_size);//回傳最小值 傳入陣列、成績總次數
    ave=GetAvg(arr,_size);//回傳平均值 傳入陣列、成績總次數
    cout<<"The total number of grades is "<<_size<<endl;
    cout<<"The maximum of grades is "<<mx<<endl;
    cout<<"The minimum of grades is "<<mn<<endl;
    cout<<"The average of grades is "<<ave;
    return 0;
}
int GetMax(int arr[ArraySize],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int c;
    int mx=0;
    for(c=0 ; c<=(_size-1) ; c++)
    {
        if(arr[c]>mx)
        mx=arr[c];
    }
    return mx;
}
int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int c;
    int mn=100;
    for(c=0 ; c<=(_size-1) ; c++)
    {
        if(arr[c]<mn)
        mn=arr[c];
    }
    return mn;
}
double GetAvg(int arr[ArraySize],int _size)//回傳平均值 傳入陣列、成績總次數
{
    int c;
    double ave,total=0;
    for(c=0 ; c<=(_size-1) ; c++)
    total=total+arr[c];
    ave=total/_size;
    return ave;
}
