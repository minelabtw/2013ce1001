#include <iostream>
#include <algorithm>
#include <numeric>
using namespace std;
#define ArraySize 20

int GetMax(int arr[ArraySize],int _size){
    return *max_element(arr,arr+_size);//利用內建函式找出最大值 
}

int GetMin(int arr[ArraySize],int _size){
    return *min_element(arr,arr+_size);//利用內建函式找出最小值
}

double GetAvg(int arr[ArraySize],int _size){
    return double(accumulate(arr,arr+_size,0))/_size;//利用內建函式找出平均值 
}

int main(){
    int grades[ArraySize],size=0;//宣告整數,分別為個別的分數陣列和人數 
    cout << "Please enter students grades.\n"
         << "Maximum number of grades is 20.\n"
         << "You can also enter '-1' to finish enter grades.\n";//輸出說明 
   do{//處理輸入成績,超過範圍則要求重新輸入,-1或超過20筆則結束輸入 
        cout << "Enter grade?: ";
        cin >> grades[size]; 
    }while(grades[size]!=-1 && ((grades[size]>=0 && grades[size]<=100 && ++size) || ((grades[size]<0 || grades[size]>100) && cout << "Out of range!\n")) && size<20);
    cout << "The total number of grades is " << size << ".\n"
         << "The maximum of grades is " << GetMax(grades,size) << ".\n"
         << "The minimum of grades is " << GetMin(grades,size) << ".\n"
         << "The average of grades is " << GetAvg(grades,size) << ".\n";//輸出結果
    return 0;
}
