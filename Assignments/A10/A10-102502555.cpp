#include<iostream>
#define ArraySize 20

using namespace std;

int GetMax(int arr[ArraySize] , int _size);  //宣告找最大的函式
int GetMin(int arr[ArraySize] , int _size);  //宣告找最小的函式
double GetAvg(int arr[ArraySize] , int _size);  //宣告算平均的函式

int main(){
    int input = 0;  //宣告輸出變數
    int counter = 0;  //宣告計數器變數
    int grades[ArraySize] = {};  //宣告成績陣列

    cout << "Please enter students grades." << endl;  //輸出說明事項
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    while(input != -1){  //輸入成績
        if(counter < ArraySize){
            do{
                cout << "Enter grade?: ";
                cin >> input;
                if(input != -1 && input < 0 || input > 100){
                    cout << "Out of range!" << endl;
                }
            }while(input != -1 && input < 0 || input > 100);
            if(input != -1){
                grades[counter] = input;
                counter++;
            }
        }else{
            input = -1;
        }
    }

    cout << "The total number of grades is " << counter << "." << endl;  //輸出有幾個成績
    cout << "The maximum of grades is " << GetMax(grades , counter) << "." << endl;  //輸出最大值
    cout << "The minimum of grades is " << GetMin(grades , counter) << "." << endl;  //輸出最小值
    cout << "The average of grades is " << GetAvg(grades , counter) << "." << endl;  //輸出平均值

    return 0;
}

int GetMax(int arr[ArraySize] , int _size){  //找最大值的函式
    int amax = -1;
    for(int i = 0 ; i < _size ; i++){
        if(arr[i] > amax){
            amax = arr[i];
        }
    }
    return amax;
}
int GetMin(int arr[ArraySize] , int _size){  //找最小值的函式
    int amin = 101;
    for(int i = 0 ; i < _size ; i++){
        if(arr[i] < amin){
            amin = arr[i];
        }
    }
    return amin;
}
double GetAvg(int arr[ArraySize] , int _size){  //算平均值的函式
    double sum = 0;
    for(int i = 0 ; i < _size ; i++){
        sum += arr[i];
    }
    return sum / _size;
}
