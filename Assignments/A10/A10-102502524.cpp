#include <iostream>
#define ArraySize 20
using namespace std;

int Max(int [],int);                                                    //宣告函式
int Min(int [],int);
double Avg(int [],int);

int main()
{
    int a = 0;                                                          //宣告變數
    int c = 0;
    int g[ArraySize];

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
    cout << "Enter grade?: ";
    while (cin >> a)                                                    //輸入數字
    {
        if (a==-1)                                                      //如果輸入-1，則結束輸入
            break;
        else if (a<0 || a>100)                                          //如果輸入超出0~100，則重新輸入
            cout << "Out of range!" << endl;
        else                                                            //如果在範圍內，則把成績放入陣列中
        {
            c++;
            g[c]=a;
        }
        if (c==20)                                                      //如果有效成績輸入達20筆，停止輸入
            break;
        cout << "Enter grade?: ";
    }
    cout << "The total number of grades is " << c << "." << endl;       //輸出有效輸入之次數
    cout << "The maximum of grades is " << Max(g,c) << "." << endl;     //找出有效輸入之最大值
    cout << "The minimum of grades is " << Min(g,c) << "." << endl;     //找出有效輸入之最小值
    cout << "The average of grades is " << Avg(g,c) << "." << endl;     //計算有效輸入的平均值

    return 0;
}

int Max(int g[ArraySize],int c)                                         //求最大值函式
{
    int m = 1;
    for (int i=1; i<=c; i++)
    {
        if (i==1)
            m=i;
        else
        {
            if (g[i]>g[m])
                m=i;
        }
    }
    return g[m];
}

int Min(int g[ArraySize],int c)                                         //求最小值函式
{
    int m = 1;
    for (int i=1; i<=c; i++)
    {
        if (i==1)
            m=i;
        else
        {
            if (g[i]<g[m])
                m=i;
        }
    }
    return g[m];
}

double Avg(int g[ArraySize],int c)                                      //計算平均值函式
{
    double m = 0;
    for (int i=1; i<=c; i++)
    {
            m+=g[i];
    }
    m=m/c;
    return m;
}
