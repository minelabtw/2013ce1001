#include <iostream>
using namespace std;
#define ArraySize 20  //設定陣列大小為20
int grades[ArraySize];  //宣告陣列
int GetMax( int arr[ArraySize],int _size )//回傳最大值 傳入陣列、成績總次數
{
    int Max = arr[0];
    for( int j = 0; j < _size; j++ )
    {
        if( Max < arr[j] )
            Max = arr[j];
    }
    return Max;
}
int GetMin( int arr[ArraySize],int _size )//回傳最小值 傳入陣列、成績總次數
{
    int Min = arr[0];
    for( int k = 0; k < _size; k++ )
    {
        if( Min > arr[k] )
            Min = arr[k];
    }
    return Min;
}
double GetAvg( int arr[ArraySize],int _size )//回傳平均值 傳入陣列、成績總次數
{
    double Sum = 0;
    for( int q = 0; q < _size; q++ )
    {
        Sum = Sum + arr[q];
    }
    return Sum/_size;
}
int main()
{
    int grade = 0,time = 0;  //設兩個整數變數一個用來cin成績用一個拿來當計數器
    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl << "You can also enter '-1' to finish enter grades." << endl;
    for ( time = 0; time < 20; time++)
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> grade;

            if ( grade == -1 )
                break;
            if ( grade < 0 || grade > 100 )
                cout << "Out of range!\n";
        }
        while( grade < 0 || grade > 100);

        if( grade == -1 )break;

        grades[ time ] = grade;
    }
    cout << "The total number of grades is " << time << "." << endl;
    cout << "The maximum of grades is " << GetMax(grades,time) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,time) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,time) << "." << endl;
}
