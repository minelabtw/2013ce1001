#include <iostream>

using namespace std;
#define ArraySize 20
int GetMax(int*,int);//回傳最大值 傳入陣列、成績總次數
int GetMin(int*,int);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int*,int);//回傳平均值 傳入陣列、成績總次數
int main()
{
    int grades[ArraySize],i=0;                 //宣告陣列

    cout<<"Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades.\n";

    for(i=0; i<ArraySize; i++)                 //不斷輸入數值,並要求在範圍內
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grades[i];
        }
        while((grades[i]>100 || grades[i]<-1) && cout<<"Out of range!\n");

        if(grades[i]==-1)                           //輸入-1時結束輸出
            break;
    }

    cout<<"The total number of grades is "<<i<<".\n";                        //呼叫函數,並印出結果
    cout<<"The maximum of grades is "<<GetMax(grades,i)<<".\n";
    cout<<"The minimum of grades is "<<GetMin(grades,i)<<".\n";
    cout<<"The average of grades is "<<GetAvg(grades,i)<<".\n";

    return 0;
}
int GetMax(int* arr,int _size)                           //以下為函數內容
{
    int Max=arr[0];

    for(int i=0; i<_size; i++)
        if(arr[i]>Max)
            Max=arr[i];

    return Max;
}
int GetMin(int* arr,int _size)
{
    int Min=arr[0];

    for(int i=0; i<_size; i++)
        if(arr[i]<Min)
            Min=arr[i];

    return Min;
}
double GetAvg(int* arr,int _size)
{
    int sum=0;

    for(int i=0; i<_size; i++)
        sum+=arr[i];

    return sum/_size;


}
