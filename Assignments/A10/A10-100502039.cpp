#include <iostream>
#define ArraySize 20         //定義陣列大小
using namespace std;

int GetMax(int arr[ArraySize],int _size);
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);

int main(){

    int grade[ArraySize];    //宣告成績陣列
    int count = 0;
    int score = 0;
    int Max, Min = 0;
    double Avg = 0.0;

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish entering grades." << endl;

    while(count < 20){

        cout << "Enter grade: ";
        cin >> score;

        if(score == -1)
            break;
        else if(score >= 0 && score <= 100){
            grade[count] = score;
            count++;
        }
        else
            cout << "Out of range!" << endl;
    }



    cout << "The total number of grades is " << count << "." << endl;
    cout << "The maximum of grades is " << GetMax(grade,count) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grade,count) << "." << endl;
    cout << "The average of grades is " << GetAvg(grade,count) << "." << endl;

    return 0;
}

int GetMax(int arr[ArraySize],int _size){
    int max = arr[0];
    for(int i = 1; i < _size; i++){
        if(arr[i] > max)
            max = arr[i];
    }
    return max;

}
int GetMin(int arr[ArraySize],int _size){
    int min = arr[0];
    for(int i = 1; i < _size; i++){
        if(arr[i] < min)
            min = arr[i];
    }
    return min;
}
double GetAvg(int arr[ArraySize],int _size){
    int sum = 0;
    for(int i = 0; i < _size; i++)
        sum += arr[i];
    return sum/(double)_size;
}
