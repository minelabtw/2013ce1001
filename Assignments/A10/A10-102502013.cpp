#include <iostream>

using namespace std;
#define ArraySize 20
int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數
int main()
{
    int grades[ArraySize]={}, i=0;
    int maximum=0, minimum=100;
    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
    do{
        cout << "Enter grade?: ";
        cin >> grades[i];
        if (grades[i]==-1)
            break;
        else if (grades[i]<0||grades[i]>100)
        {
            cout << "Out of range!" << endl;
            i--;
        }
        i++;
    }while(i<ArraySize);
    cout << "The total number of grades is " << i << "." << endl;//輸出成績總次數
    cout << "The maximum of grades is " << GetMax(grades,i) << "." << endl;//輸出最大值
    cout << "The minimum of grades is " << GetMin(grades,i) << "." << endl;//輸出最小值
    cout << "The average of grades is " << GetAvg(grades,i) << "." << endl;//輸出平均值
    return 0;
}
int GetMax(int arr[],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int maximum=0;
    for (int i=0;i<_size;i++)
    {
        if (maximum<arr[i])
            maximum=arr[i];
    }
    return maximum;
}
int GetMin(int arr[],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int minimum=100;
    for (int i=0;i<_size;i++)
    {
        if (minimum>arr[i])
            minimum=arr[i];
    }
    return minimum;
}
double GetAvg(int arr[],int _size)//回傳平均值 傳入陣列、成績總次數
{
    double avg=0;
    for (int i=0;i<_size;i++)
    {
        avg += arr[i];
    }
    avg /= _size;
    return avg;
}
