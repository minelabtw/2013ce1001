#include <iostream>
using namespace std ;
int GetMax(int[],int) ;//宣告函式
int GetMin(int[],int);
double GetAvg(int[],int) ;
int main()
{
    const int arraysize=20 ;//宣告陣列大小
    int grades[arraysize]= {} ;//陣列儲存
    cout << "Please enter students grades." << endl ;//輸出題目所需
    cout << "Maximum number of grades is 20." << endl ;
    cout << "You can also enter '-1' to finish enter grades." << endl ;
    int a=0 ;//宣告變數
    cout << "Enter grade?: " ;
    cin >> a ;
    while (a<-1 or a>100)//迴圈
    {
        cout << "Out of range!" << endl << "Enter grade?: " ;
        cin >> a ;
    }
    int c=0 ;
    while(a!=-1)//當a不等於-1進入迴圈
    {
        c++ ;//c值+1
        grades[c]=a;//將輸入的值依序儲存在陣列裡
        if (c==20)
            break ;//離開
        cout << "Enter grade?: " ;
        cin >> a ;
        if (a==-1)
            break ;
        while(a<-1 or a>100)
        {
            cout << "Out of range!" << endl << "Enter grade?: " ;
            cin >> a ;
        }
    }
    cout << "The total number of grades is " << c << endl ;//輸出
    cout << "The maximum of grades is " << GetMax(grades,c) << endl ;//將grades c 傳入函式並輸出
    cout << "The minimum of grades is " << GetMin(grades,c) << endl ;
    cout << "The average of grades is " << GetAvg(grades,c) << endl ;
    return 0 ;
}
int GetMax(int arr[],int _size)//函式判斷最大值
{
    int x=arr[1] ;
    for(int b=1; b<_size; b++)
        if (arr[b+1]>=x)
            x=arr[b+1] ;
        else if (arr[b+1]<x)
            x=x ;
    return x ;
}
int GetMin(int arr[],int _size)//函式判斷最小值
{
    int x=arr[1] ;
    for(int b=1; b<_size; b++)
        if (arr[b+1]<x)
            x=arr[b+1] ;
        else if (arr[b+1]>=x)
            x=x ;
    return x ;
}
double GetAvg(int arr[],int _size)//函式判斷平均值
{
    double total=0 ;
    for(int b=1; b<=_size; b++)
        total=total+arr[b] ;
    return (total/_size) ;
}
