#include<iostream>
#define ArraySize 20
using namespace std;

int GetMax(int arr[ArraySize],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int Max=0;
    for(int j=0; j<_size; j++)
        if(arr[j]>Max&&arr[j]!=-1)
            Max=arr[j];
    return Max;
}
int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    if(_size==0)
        return 0;
    else
    {
        int Min=100;
        for(int j=0; j<_size; j++)
        {
            if(arr[j]<Min&&arr[j]!=-1)
                Min=arr[j];
        }
        return Min;
    }
}
double GetAvg(int arr[ArraySize],int _size)//回傳平均值 傳入陣列、成績總次數
{
    if(_size==0)
        return 0;
    else
    {
        double Avg=0;
        for(int j=0; j<_size ; j++)
            if(arr[j]!=-1)
                Avg += arr[j];
        return Avg/_size;
    }
}

int main()
{
    int i,_size=0;
    int grades[ArraySize]= {};
    for(int j=0; j<20; j++)
        grades[j]=-1;
    cout<<"Please enter students grades.\n"<<"Maximum number of grades is 20.\n"
        <<"You can also enter '-1' to finish enter grades.\n"<<"Enter grade?: ";
    cin>>grades[0];
    for(i=1; i<=20; i++)
    {
        if(grades[i-1]!=-1)
        {
            while(grades[i-1]<-1||grades[i-1]>100)
            {
                cout<<"Out of range!\nEnter grade?: ";
                cin>>grades[i-1];
            }
            if(grades[i-1]!=-1)
                _size++; //_size=_size+1;
            if(grades[i-1]!=-1&&i!=20)
            {
                cout <<"Enter grade?: ";
                cin>>grades[i];
            }
        }
    }
    //if(grades[19]!=-1)
    //cout<<"The total number of grades is 20.\n";
    //else
    cout<<"The total number of grades is "<<_size<<".\n";
    cout<<"The maximum of grades is "<<GetMax(grades,_size)<<".\n";
    cout<<"The minimum of grades is "<<GetMin(grades,_size)<<".\n";
    cout<<"The average of grades is "<<GetAvg(grades,_size)<<".";

    return 0;
}
