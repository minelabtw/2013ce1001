#include <iostream>
using namespace std;
const int ArraySize=20;
int displaygrades[ArraySize]= {0}; //宣告函式
int GetMax(int arr[ArraySize],int _size);
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
int number;
int main()
{
    int grade;
    cout<<"Please enter students grades."<<endl<<"Maximum number of grades is 20."<<endl<<"You can also enter '-1' to finish enter grades."<<endl;
//輸出"Please enter students grades." "Maximum number of grades is 20." "You can also enter '-1' to finish enter grades."
    for(int counterN=0;counterN<ArraySize;counterN++)//進行運算和輸入
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grade;
            if(grade==-1)
                break;
            if(grade<0 || grade>100)
                cout<<"Out of range!"<<endl;
        }
        while(grade<0 || grade>100);
        if(grade==-1)
            break;
        displaygrades[counterN]=grade;
        number=counterN+1;
    }
    cout<<"The total number of grades is "<<number<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(displaygrades,number)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(displaygrades,number)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(displaygrades,number)<< ".";

    return 0;
}

int GetMax(int arr[ArraySize],int _size)//函式運算
{
    int Max=0;
    for(int i=0; i< _size; i++)
    {
        if(displaygrades[i]>Max)
            Max=displaygrades[i];
    }
    return Max;
}

int GetMin(int arr[ArraySize],int _size)
{
    int Min=100;
    for(int i=0; i<_size; i++)
    {
        if(displaygrades[i]<Min)
            Min=displaygrades[i];
    }
    return Min;
}

double GetAvg(int arr[ArraySize],int _size)
{
    double total=0;
    double average=0;
    for(int i=0; i < _size; i++)
    total=total+arr[i];
    average=total/_size;
    return average;
}
