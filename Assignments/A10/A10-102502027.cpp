#include <iostream>
#define ArraySize 20 //指定 ArraySize為20

using namespace std;
int GetMax(int [],int ); //prototype
int GetMin(int [],int );
double GetAvg(int [],int );

int main()
{
    int _size=0; //宣告
    int grade;
    int arr[ArraySize];//宣告成績陣列

    cout << "Please enter students grades.\n"<<"Maximum number of grades is 20.\n"<<
         "You can also enter '-1' to finish enter grades.\n"; //輸出
    do //執行
    {
        cout<<"Enter grade?: ";
        cin>>grade; //輸入
        if(grade==-1)
            break;
        else if(grade>100 || grade<0)
            cout<<"Out of range!\n";
        else
        {
            arr[_size]=grade;
            _size++;
        }
    }
    while(_size<ArraySize);
    cout<<"The total number of grades is "<<_size<<endl;
    cout<<"The maximum of grades is "<<GetMax(arr,_size)<<endl; //呼叫
    cout<<"The minimum of grades is "<<GetMin(arr,_size)<<endl;
    cout<<"The average of grades is "<<GetAvg(arr,_size);
    return 0;
}
int GetMax(int arr[],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int max=0;
    for(int j=0; j<_size; j++) //判斷最大值
    {
        if (arr[j]>=max)
            max=arr[j];
    }
    return max;// 回傳
}

int GetMin(int arr[],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int min=100;
    for(int a=0; a<_size; a++ )
    {
        if(arr[a]<min)
            min=arr[a];
    }
    return min;
}
double GetAvg(int arr[],int _size)//回傳平均值 傳入陣列、成績總次數
{
    double all =0;
    if(_size>0)
    {
        for(int k=0; k<_size; k++)
            all+=arr[k];
    }
    return  all/_size;
}


