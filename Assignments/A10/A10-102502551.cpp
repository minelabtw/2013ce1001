#include <iostream>
#define ArraySize 20

using namespace std;

int GetMax(int arr[ArraySize],int _size);
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
void work(int arr[ArraySize],int &stu);

int main()
{
    int grade[ArraySize];
    int stu;

    cout<<"Please enter students grades."<<endl
        <<"Maximum number of grades is 20."<<endl
        <<"You can also enter '-1' to finish enter grades."<<endl;

    work(grade,stu);

    cout<<"The total number of grades is "<<stu<<"."<<endl
        <<"The maximum of grades is "<<GetMax(grade,stu)<<"."<<endl
        <<"The minimum of grades is "<<GetMin(grade,stu)<<"."<<endl
        <<"The average of grades is "<<GetAvg(grade,stu)<<".";

    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int max=arr[0];

    for(int i=1; i<_size; i++)
    {
        if(arr[i]>max)                                                          //��̤j
            max=arr[i];
    }

    if(_size==0)
        return 0;
    else
        return max;
}

int GetMin(int arr[ArraySize],int _size)
{
    int min=arr[0];

    for(int i=1; i<_size; i++)
    {
        if(arr[i]<min)                                                          //��̤p
            min=arr[i];
    }

    if(_size==0)
        return 0;
    else
        return min;
}

double GetAvg(int arr[ArraySize],int _size)
{
    double sum=0;

    for(int i=0; i<_size; i++)
    {
        sum=sum+arr[i];
    }

    if(_size==0)
        return 0;
    else
        return sum/_size;
}

void work(int arr[ArraySize],int &stu)
{
    for(stu=0; stu<ArraySize; stu++)
    {
        cout<<"Enter grade?: ";
        cin>>arr[stu];

        while((arr[stu]<0 || arr[stu]>100) && arr[stu]!=-1)
        {
            cout<<"Out of range!"<<endl<<"Enter grade?: ";
            cin>>arr[stu];
        }
        if(arr[stu]==-1)
            break;
    }
}


