//A10-102502031.cpp
#include <iostream>
#include <iomanip>

using namespace std;

void Input(int _array[], int arraysize, int &number)
{
    for (int i=0; (i<arraysize && _array[i-1]!=-1); i=i+1)   //input=-1 is used to end
    {
        do
        {
            do
            {
                cout << "Enter grade?: ";
                cin >> _array[i];
            }
            while ((_array[i]>100 || (_array[i]<0 && _array[i]!=-1)) && cout << "Out of range!" << endl);

            if (_array[i]!=-1)
                number=number+1;
        }
        while (_array[0]==-1 && cout << "There is not any input!" << endl);
    }
}

int GetMaximum(int _array[], int arraysize)  //arraysize olny include legal input
{
    int x=0;                            //the maximum can be any interger above 0
    for (int i=0; i<arraysize; i=i+1)
    {
        if (_array[i]>x)
        {
            x=_array[i];
        }
    }
    return x;
}

int GetMinimum(int _array[], int arraysize)
{
    int x=100;                          //the minimum can be any interger beyond 100
    for (int i=0; i<arraysize; i=i+1)
    {
        if (_array[i]<x)
        {
            x=_array[i];
        }
    }
    return x;
}

double GetAverage(int _array[], int arraysize)
{
    double sum=0;
    for (int i=0; i<arraysize; i=i+1)
    {
        sum=sum+_array[i];
    }
    return sum/arraysize;
}

void Sort(int _array[], int _sort[], int arraysize)  //sort all the result for practicing after class
{
    cout << "After sorting: ";
    for (int i=0; i<arraysize; i=i+1)
        _sort[i]=_array[i];
    if (arraysize>1)
    {
        for (int i=0; i<arraysize-1; i=i+1)
            for (int j=i+1; j<arraysize; j=j+1)
                if (_sort[i]>_sort[j])           //change the value if the former is bigger than the latter
                {
                    _sort[i]=_sort[i]+_sort[j];  //use only two variable to change value
                    _sort[j]=_sort[i]-_sort[j];
                    _sort[i]=_sort[i]-_sort[j];
                }
    }
    for (int i=0; i<arraysize; i=i+1)
        cout << _sort[i] << " ";
}

int main()
{
    const int ArraySize=20;     //the constant initial value of array size can be changed by designer easily
    int grades[ArraySize]= {};  //array named grades is used as a place to store input from user
    int number=0;               //the number of data keyed in by user
    int _sort[ArraySize]= {};   //array named _sort is used to store the result of sorting the input data from user

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
    Input(grades, ArraySize, number);
    cout << "The total number of grades is " << number << "." << endl;
    cout << "The maximum of grades is " << GetMaximum(grades, number) << "." << endl;
    cout << "The minimum of grades is " << GetMinimum(grades, number) << "." << endl;
    cout << "The average of grades is " << GetAverage(grades, number) << "." << endl;
    Sort(grades, _sort, number);

    return 0;
}
