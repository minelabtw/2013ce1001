#include<iostream>
#define ArraySize 20
using namespace std;


int GetMax(int score[],int Size)//回傳最大值 傳入陣列、成績總次數
{

    int big=0;
    for(int i=0; i<=Size; i++)
    {
        if(big<score[i])
            big=score[i];
    }
    return big;
}
int GetMin(int score[],int Size)//回傳最小值 傳入陣列、成績總次數
{
    int small=100;
    for(int j=0; j<Size; j++)
    {
        if(small>score[j])
            small=score[j];
    }
    return small;
}
double GetAvg(int score[],int Size)//回傳平均值 傳入陣列、成績總次數
{
    double Average = 0.0;
    for (int i=0;i<Size;i++)
        Average += score[i];
    return Average / Size;

}
int main()
{
    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades."<< endl;
    int Size=0;
    int score[20]= {};
    while(1)
    {
        cout << "Enter grade?: ";
        cin >> score[Size];
        while(score[Size]>100 || score[Size]<-1)
        {
            cout << "Out of range!" << endl;
            cout << "Enter grade?: ";
            cin >> score[Size];
        }
        if (score[Size] == -1)
        {
            break;
        }
        Size++;
        if (Size==20)
        {
            break;
        }
    }
    cout << "The total number of grades is " << Size << "."<<endl;
    cout << "The maximum of grades is "<< GetMax(score,Size) << "."<<endl;
    cout << "The minimum of grades is "<< GetMin(score,Size) << "."<<endl;
    cout << "The average of grades is "<< GetAvg(score,Size) << ".";
}
