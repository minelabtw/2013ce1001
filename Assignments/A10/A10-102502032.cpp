#include <iostream>
using namespace std;

int GetMax(int arr[],int arr_size)//return the Max
{
    int Max = 0;
    for (int i = 1; i < arr_size; i ++)
        if (arr[i] > arr[Max])
            Max = i;
    return arr[Max];
}

int GetMin(int arr[],int arr_size)//return the min
{
    int minimum = 0;
    for (int i = 1; i < arr_size; i ++)
        if (arr[i] < arr[minimum])
            minimum = i;
    return arr[minimum];
}

double GetAvg(int arr[],int arr_size)//return the average
{
    double sum = 0;
    for ( int i = 1; i <= arr_size; i ++)
        sum += arr[i - 1];
    return sum / arr_size;
}

int main()
{
    //decalaration and intialization
    int ArraySize = 20;
    int grades[ArraySize];
    for (int i = 0; i < ArraySize; i ++)
        grades[i] = -2;

    //intro
    cout << "Please enter students grades." << endl
         << "Maximum number of grades is 20." << endl
         << "You can also enter '-1' to finish enter grades." << endl;


    for (int i = 1; i <= ArraySize; i ++)
    {
        while (grades[i - 1] < 0 or grades[i - 1] > 100)    //ask for grades
        {
            cout << "Enter grade?: ";
            cin >> grades[i - 1];

            if (grades[i - 1] < 0 or grades[i - 1] > 100)   //argu that whether input is leagle or not
            {
                if (grades[i - 1] == -1)
                    break;
                else
                    cout << "Out of range!" << endl;
            }
        }

        if (grades[i - 1] == -1 or i == ArraySize)  //input over, start to output results
        {
            if (grades[i - 1] == -1)
                i --;
            cout << "The total number of grades is " << i << "." << endl
                 << "The maximum of grades is " << GetMax(grades, i) << "." << endl
                 << "The minimum of grades is " << GetMin(grades, i) << "." << endl
                 << "The average of grades is " << GetAvg(grades, i) << ".";
            break;
        }
    }

    return 0;
}
