#include<iostream>
#define ArraySize 20 //replace ArraySize by 20 while compiling

using namespace std;

int GetMax(int arr[ArraySize],int _size); //return a max num
int GetMin(int arr[ArraySize],int _size); //return a min num
double GetAvg(int arr[ArraySize],int _size); //return average

int main(){
    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
    int i, arr[ArraySize]; //the array!!!
    for(i=0; i<20; i++){
        //ask grade
        while(1){
            cout << "Enter grade?: ";
            cin >> arr[i];
            if(arr[i]>=-1 && arr[i]<=100) //break if legal
                break;
            cout << "Out of range!" << endl; //error
        }
        if(arr[i]==-1) //terminate if input is -1
            break;
    }
    cout << "The total number of grades is " << i << "." << endl; //i is also a counter
    cout << "The maximum of grades is " << GetMax(arr, i) << "." << endl; //call function to get max
    cout << "The minimum of grades is " << GetMin(arr, i) << "." << endl; //call function to get min
    cout << "The average of grades is " << GetAvg(arr, i) << "." << endl; //call function to get avg
}

int GetMax(int arr[ArraySize],int _size){
    int maximum=0; //set max to a minmumnum number
    for(int i=0; i<_size; i++)
        if(arr[i]>maximum)
            maximum = arr[i]; //max replaced by a larger number
    return maximum; //return max
}

int GetMin(int arr[ArraySize],int _size){
    int minimum=100; //set max to a maximum number
    for(int i=0; i<_size; i++)
        if(arr[i]<minimum)
            minimum = arr[i];//min replaced by a smaller number
    return minimum; //return min
}

double GetAvg(int arr[ArraySize],int _size){
    double sum=0; //initialized
    for(int i=0; i<_size; i++)
        sum+=(double)arr[i]; //converted to double and added to sum
    return sum/_size; //return average num
}
