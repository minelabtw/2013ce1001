#include <iostream>
#define ArraySize 20

using namespace std;

int GetMax( int arr[ArraySize],int _size );
int GetMin( int arr[ArraySize],int _size );
double GetAvg( int arr[ArraySize],int _size );

int main()
{
    int score[ArraySize];
    int _size = 0 ;

    cout << "Please enter students grades." << endl
         << "Maximum number of grades is 20." << endl
         << "You can also enter '-1' to finish enter grades." << endl;

    do
    {
        cout << "Enter grade?: " ;
             cin >> score[_size];
        if ( score[_size] == -1 )
            break;
        else if ( score[_size] < 0 || score[_size] > 100  )
            cout << "Out of range!" << endl;
        else
            _size ++;
    } while ( _size < 20 ) ;

    cout << "The total number of grades is " << _size << "." << endl
         << "The maximum of grades is " << GetMax ( score, _size ) << "." << endl
         << "The minimum of grades is " << GetMin ( score, _size ) << "." << endl
         << "The average of grades is " << GetAvg ( score, _size ) << "." ;

    return 0;
}

int GetMax( int arr[],int _size )
{
    int hold = arr[0];
    for ( int i = 1; i < _size ; i++ )
    {
        if ( arr[i] > hold )
            hold = arr[i];
    }
    return hold;
}

int GetMin( int arr[],int _size )
{
    int hold = arr[0];
    for ( int i = 1; i < _size ; i++ )
    {
        if ( arr[i] < hold )
            hold = arr[i];
    }
    return hold;
}

double GetAvg(int arr[],int _size)
{
    double total = 0 ;
    for ( int i = 0; i < _size ; i++ )
        total += arr[i];
    total /= _size;
    return total ;
}
