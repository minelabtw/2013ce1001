#include <iostream>
using namespace std;
#define ArraySize 20
int GetMax(int arr[ArraySize],int size)
{
    int max=arr[0];                 //先給MAX 陣列第一個數字
    for (int j=1; j<size; j++)      //比較size-1次
    {
        if(max<arr[j])              //若比max 大 則 給max 值
        {
            max=arr[j];
        }
    }
    return max;                     //回傳max
}
int GetMin(int arr[ArraySize],int size)
{
    int min=arr[0];                 //先給MIN 陣列第一個數字
    for (int j=1; j<size; j++)      //比較size-1次
    {
        if(min>arr[j])              //若比min 小 則 給min 值
        {
            min=arr[j];
        }
    }
    return min;                     //回傳min
}
double GetAvg(int arr[ArraySize],int size)
{
    double sum=0;                   //定義總和
    for (int j=0; j<size; j++)      //迴圈size 次
    {
        sum=sum+arr[j];             //陣列每個都加進sum
    }
    return (sum/size);              //回傳sum/size
}
int main()
{
    int grades[ArraySize];          //成績陣列
    int grade;                      //輸入成績
    int counter=0;                  //計次數
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    do
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grade;
            if (grade<-1 || grade>100)          //超出範圍則重新輸入
            {
                cout<<"Out of range!"<<endl;
            }
        }while(grade<-1 || grade>100 );
        if(grade!=-1)                           //非-1 就記錄到陣列
        {
        counter++;                              //計數器+1
        grades[counter-1]=grade;                //儲存
        }
    }
    while(grade!=-1 && counter!=20);            //-1 或者 20次 就離開迴圈
    cout<<"The total number of grades is "<<counter<<"."<<endl;                     //輸入grade 陣列 之後依各函數運算 回傳值
    cout<<"The maximum of grades is "<<GetMax(grades,counter)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,counter)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,counter)<<".";
    return 0;
}
