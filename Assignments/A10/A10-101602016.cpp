#include<iostream>
#define ArraySize 20//定義ArraySize的值為20
using namespace::std;

void ForInput(int arr[ArraySize],int &_size);//給使用者輸入成績
int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數

int main()
{
    int grades[ArraySize];//宣告成績陣列
    int number=0;//用來計算輸入次數
    cout<<"Please enter students grades."<<endl
        <<"Maximum number of grades is 20."<<endl
        <<"You can also enter '-1' to finish enter grades."<<endl;
        //解釋程式
    ForInput(grades,number);//利用此函式輸入成績

    cout<<"The total number of grades is "<<number<<"."<<endl;//顯示輸入次數
    cout<<"The maximum of grades is "<<GetMax(grades,number)<<"."<<endl;//顯示最大值
    cout<<"The minimum of grades is "<<GetMin(grades,number)<<"."<<endl;//顯示最小值
    cout<<"The average of grades is "<<GetAvg(grades,number)<<"."<<endl;//顯示平均值

    return 0;
}

void ForInput(int arr[ArraySize],int &_size)
{
    int input=0;//給使用者輸入成績的變數
    do
    {
        cout<<"Enter grade?: ";
        cin>>input;
        while(input<-1||100<input)//判斷輸入值不超出範圍
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter grade?: ";
            cin>>input;
        }
        arr[_size]=input;//將輸入值送到陣列裡
        _size+=1;//輸入次數+1
    }while(input!=-1&&_size!=20);//當輸入-1或次數滿20次時跳出迴圈
    if(input==-1) _size-=1;//當輸入-1時_size的值會比正確的輸入次數多1，所以要減回來
}

int GetMax(int arr[ArraySize],int _size)//計算最大值
{
    int Max=0,i=0;
    for(i=0; i<_size; i++) if(Max<=arr[i]) Max=arr[i];//使得Max為成績的最大值
    return Max;//回傳最大值
}

int GetMin(int arr[ArraySize],int _size)//計算最小值
{
    int Min=100,i=0;
    for(i=0; i<_size; i++) if(Min>=arr[i]) Min=arr[i];//使得Min為成績的最小值
    return Min;//回傳最小值
}

double GetAvg(int arr[ArraySize],int _size)//計算平均
{
    double Sum=0;
    int i=0;
    for(i=0; i<_size; i++) Sum+=arr[i];//使得Sum為成績的總和
    return Sum/_size;//回傳總和除以輸入次數(平均值)
}

