#include <iostream>
#define ArraySize 20
using namespace std;
int input (int [],int&);//宣告函式庫
int stop (int [],int);
int GetMax (int [],int);
int GetMin (int [],int);
double GetAvg (int [],int);
int main ()//主程式
{
    int arraya [ArraySize] = {};//宣告變數並=0
    int times =0;
    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl
         << "You can also enter '-1' to finish entering grades." << endl;//輸出題目要求
    input ( arraya , times);//呼叫輸入程式
    return 0;//結束主程式
}
int input (int arr [],int &_size)
{
    cout << "Enter grade?: ";//提示輸入
    cin >> arr [_size];//輸入
    while (arr [_size] <-1 ||arr [_size] >100)//當輸入不符條件
    {
        cout << "Out of range!" << endl << "Enter grade?: ";//提示輸入
        cin >> arr [_size];//輸入
    }
    if (arr [_size] ==-1)//當停止
    {
        stop (arr , _size);
        return 0;//回主程式
    }
    _size++;
    if (_size >=20)//當輸入20次
    {
        stop (arr , _size);
        return 0;//回主程式
    }
    input (arr ,_size);//重複載入
}
int stop (int arr[],int allsize)//結尾函式
{
    cout << "The total number of grades is " << allsize << "." << endl;//輸出總次數
    cout << "The maximum of grades is " << GetMax(arr,allsize) << "." << endl;//輸出最大值
    cout << "The minimum of grades is " << GetMin(arr,allsize) << "." << endl;//輸出最小值
    cout << "The average of grades is " << GetAvg(arr,allsize) << ".";//輸出平均值
}
int GetMax (int arr [],int allsize)//最大值函式
{
    int maxvalue =0;
    for (int times =0; times <allsize; times++)//全部比一次
    {
        if (arr [times] >=maxvalue)
            maxvalue =arr [times];
    }
    return maxvalue;
}
int GetMin (int arr [],int allsize)//最小值函式
{

    int minvalue =100;
    for (int times =0; times <allsize; times++)//全部比一次
    {
        if (arr [times] <=minvalue)
            minvalue =arr [times];
    }
    return minvalue;
}
double GetAvg (int arr [],int allsize)//平均值函式
{

    double total =0;
    for (int times =0; times <allsize; times++)//全部加一次
    {
        total =arr [times] +total;
    }
    return total /allsize;
}
