#include <iostream>
#define ArraySize 20

using namespace std;

int GetMax(int arr[ArraySize],int _size);    //回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);    //回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);    //回傳平均值 傳入陣列、成績總次數

int main()
{
    int arr[ArraySize]={};    //設定變數和陣列
    int t=0;
    int grade;
    int _size;

    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;

    while(t<20&&grade!=-1)    //當滿20次或者grade輸入-1則彈出
    {
        do    //判斷是否為合理值
        {
            cout<<"Enter grade?: ";
            cin>>grade;

            if(grade<-1||grade>100)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(grade<-1||grade>100);

        if(grade!=-1)
        {
            arr[t]=grade;

            t++;

            _size=t;
        }

        else
        {
            _size=t;
        }
    }

    cout<<"The total number of grades is "<<_size<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(arr,_size)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(arr,_size)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(arr,_size)<<"."<<endl;

    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int rmax=arr[0];    //設定變數
    int maxt=1;

    while(maxt<_size)    //比大小
    {
        if(rmax<arr[maxt])
        {
            rmax=arr[maxt];
        }

        maxt++;
    }

    return rmax;
}

int GetMin(int arr[ArraySize],int _size)
{
    int rmin=arr[0];    //設定變數
    int mint=1;

    while(mint<_size)    //比大小
    {
        if(rmin>=arr[mint])
        {
            rmin=arr[mint];
        }

        mint++;
    }

    return rmin;
}

double GetAvg(int arr[ArraySize],int _size)
{
    double ravg;    //設定變數
    int avgt=0;
    double sum=0;

    while(avgt<_size)    //取平均
    {
        sum=sum+arr[avgt];
        avgt++;
    }

    ravg=sum/_size;

    return ravg;
}
