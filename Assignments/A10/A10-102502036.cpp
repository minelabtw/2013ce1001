#include<iostream>
using namespace std ;
#define ArraySize 20
int GetMax(int arr[],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[],int _size);//回傳平均值 傳入陣列、成績總次數

int main()
{
    int number=0 ;
    int grades[ArraySize]= {};
    cout<<"Please enter students grades."<<endl ;
    cout<<"Maximum number of grades is 20."<<endl ;
    cout<<"You can also enter '-1' to finish enter grades."<<endl ;

    while(number<ArraySize)
    {
        cout<<"Enter grade?: " ;
        cin>>grades[number] ;
        if(grades[number]==-1)
        {
            break ;
        }

        else if(grades[number]<0||grades[number]>100)
        {
            cout<<"Out of range!"<<endl ;
        }
       else
        number++ ;
    }

       cout<<"The total number of grades is "<<number<<endl;
       cout<<"The maximum of grades is "<<GetMax(grades,number)<<"."<<endl ;//回傳最大值
       cout<<"The minimum of grades is "<<GetMin(grades , number )<<"."<<endl ;//回傳最小值
       cout<<"The average of grades is "<<GetAvg(grades , number )<<"."<<endl ;//回傳平均值
        return 0 ;
      }
int GetMax(int arr[],int a)
{
    int maximum=arr[0] ;
    for(int b=1; b<a ; b++)
    {
        if(arr[b]>maximum)
        {
            maximum=arr[b] ;
        }
    }
    return maximum ;
}

int GetMin(int arr[],int a)
{
 int maximum=arr[0] ;
    for(int b=1; b<a ; b++)
    {
        if(arr[b]<maximum)
        {
            maximum=arr[b] ;
        }
    }
    return maximum ;
}
double GetAvg(int arr[],int a)
{
int total=0 ;
for(int b=0; b<a ; b++)
    {
       total+=arr[b] ;
    }
    return (total/a) ;
}
