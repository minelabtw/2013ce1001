#include<iostream>
#define ArraySize 20//define ArraySize is 20
using namespace std;

int GetMax(int arr[ArraySize],int _size);//回傳最大值,傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值,傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值,傳入陣列、成績總次數
void Grade(int [],int &);

int main()
{
    int arr[ArraySize]= {}; //存成績用陣列
    int Size=0;//計算次數
    cout<<"Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades."<<endl;
    Grade(arr,Size);
    cout<<"The total number of grades is "<<Size<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(arr,Size)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(arr,Size)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(arr,Size)<<"."<<endl;
    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int Max=0;
    for(int i=0; i<_size; i++)
    {
        if(arr[i]>=Max)
            Max=arr[i];
    }
    return Max;//回傳最大值
}

int GetMin(int arr[ArraySize],int _size)
{
    int Min=100;
    for(int i=0; i<_size; i++)
    {
        if(arr[i]<=Min)
            Min=arr[i];
    }
    return Min;//回傳最小值
}

double GetAvg(int arr[ArraySize],int _size)
{
    double Avg=0;//平均
    for(int i=0; i<_size; i++)
    {
        Avg+=arr[i];
    }
    Avg=Avg/_size;
    return Avg;//回傳平均
}

void Grade(int b[],int &_size)//判別輸入分數，改掉main的
{
    int grade;
    while(_size!=20&&grade!=-1)//輸入20次，或輸入-1跳出loop
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grade;
            if(grade<0&&grade!=-1||grade>100)
                cout<<"Out of range!"<<endl;
        }
        while(grade<0&&grade!=-1||grade>100);//分數不在0-100間(-1除外)，就無法跳出loop
        if(grade!=-1)
        {
            b[_size]=grade;//存分數到array
            _size++;//次數加1
        }
    }
}
