#include <iostream>
#include <cstdlib>
#define ArraySize 20
using namespace std ;
int Max(int *,int) ; //最大
int Min(int *,int) ; //最小
double Avg(int *,int) ; //平均
int main()
{
    int grade[ArraySize]={};   //成績
    int time=0 ;    //次數
    int input ;     //輸入
    cout << "Please enter students grades." << endl <<"Maximum number of grades is 20." <<endl <<"You can also enter '-1' to finish enter grades." <<endl ;

    while(time<20)  //最多20次
    {
        cout << "Enter grade?: " ;
        cin >> input ;
        if(input == -1)
        break ; //這次結束
        else if(input<-1 || input >100)
        {
            cout << "Out of range!" << endl ;
            continue;   // 重來一次
        }
        else
            grade[time] = input ;
        time++ ;    //成功輸入一次+1
    }
    cout << "The total number of grades is " << time << ".\n"
         << "The maximum of grades is " << Max(grade,time) << ".\n"
         << "The minimum of grades is " << Min(grade,time) << ".\n"
         << "The average of grades is " << Avg(grade,time) << ".\n" ;

    return 0;
}
int Max(int grade[],int time)
{
    int a = grade[0] ; //Max
    for(int i = 1 ; i < time ; i++)
        if(a < grade[i])
            a = grade[i] ;
    return a ;
}
int Min(int grade[],int time)
{
    int a = grade[0] ;  //Min
    for(int i = 1 ; i < time ; i++)
        if(a > grade[i])
            a = grade[i] ;
    return a ;
}
double Avg(int grade[],int time)
{
    double sum = 0 ;   //sum
    for(int i=0 ; i<time ; i++)
        sum += grade[i] ;
    sum /= time ;
    return sum ;
}
