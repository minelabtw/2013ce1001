#include<iostream>
#define Arraysize 20
using namespace std;
int GetMax(int [], int);//找出最大值
int GetMin(int [], int);//找出最小值
double GetAvg(int [], int);//計算平均值
int main()
{
    int grade[Arraysize];//儲存成績
    int i=0;//紀錄輸入次數
    cout << "Please enter students grades.\n";
    cout << "Maximum number of grades is 20.\n";
    cout << "You can also enter '-1' to finish enter grades.\n";
    do
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> grade[i];
            if((grade[i]<0 || grade[i]>100)&&grade[i]!=-1)
                cout << "Out of range!\n";
        }
        while( (grade[i]<0 || grade[i]>100)&&grade[i]!=-1 );
        //輸入並檢查，範圍:0~100  輸入-1跳出
    }
    while(grade[i]!=-1 && ++i<Arraysize);
    cout << "The total number of grade is " << i << endl;
    cout << "The maximum of grades is " << GetMax(grade,i) << endl;
    cout << "The minimum of grades is " << GetMin(grade,i) << endl;
    cout << "The average of grades is " << GetAvg(grade,i) << endl;
    return 0;
}
int GetMax(int grade[], int _size)
{
    int maximum=0;//預設最大值為0
    for(int i=0; i<_size; i++)
        if(maximum<grade[i])
            maximum=grade[i];
    return maximum;
}
int GetMin(int grade[], int _size)
{
    int minimum=100;//預設最小值為100
    for(int i=0; i<_size; i++)
        if(minimum>grade[i])
            minimum=grade[i];
    return minimum;
}
double GetAvg(int grade[], int _size)
{
    double sum=0;
    for(int i=0; i<_size; i++)
        sum+=grade[i];
    return (double)(sum/ (_size));
}
