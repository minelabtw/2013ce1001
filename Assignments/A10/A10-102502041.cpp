#include<iostream>
#define ArraySize 20
using namespace std;
inline int GetMax(int *grades,int _size)
{
    int M;
    for(int i=0; i<_size; i++)
        if(grades[i]>grades[M])M=i;        /*倘若第i項比原定的M還大,就把i定義成M*/
    return grades[M];
}

inline int GetMin(int *grades,int _size)
{
    int m;
    for(int i=0; i<_size; i++)
        if(grades[i]<grades[m])m=i;         /*倘若第i向比原定的m還小,就把i定義成m*/
    return grades[m];
}

inline double GetAvg(int *grades,int _size)
{
    float sum=0;
    for(int i=0; i<_size; i++)
        sum=sum+grades[i];
    return sum/_size;
}

inline void Enter_grade(int& counts,int *grades)          /*使用 Call/Pass by reference 來取代宣告 global variable*/
{
    int input;
    do
    {
        cout<<"Enter grade?: ";
        cin>>input;
        if((input<0||input>100)&&input!=-1)
        {
            cout<<"Out of range!"<<endl;
            continue;
        }
        else if(input==-1)break;
        grades[counts]=input;
        counts++;
    }
    while(counts<20);
}

int main()
{
    int counts=0,grades[ArraySize];
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    Enter_grade(counts,grades);
    cout<<"The total number of grades is "<<counts<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,counts)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,counts)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,counts)<<"."<<endl;
    return 0;
}
