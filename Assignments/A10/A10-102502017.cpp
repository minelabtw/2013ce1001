#include<iostream>
using namespace std;
#define ArraySize 20                                //定義Array大小為20

int GetMax(int arr[ArraySize],int _size){
    int x=arr[0];
    for(int i=0;i<_size;i++)if(arr[i]>x)x=arr[i];   //若下個數的值>當前的值，x = 下個數的值
    return x;
}
int GetMin(int arr[ArraySize],int _size){
    int x=arr[0];
    for(int i=0;i<_size;i++)if(arr[i]<x)x=arr[i];   //若下個數的值<當前的值，x = 下個數的值
    return x;
}
double GetAvg(int arr[ArraySize],int _size){
    double sum=0;
    for(int i=0;i<_size;i++)sum+=arr[i];
    return sum/_size;
}

int main(void){
    cout << "Please enter students grades.\n";
    cout << "Maximum number of grades is " << ArraySize << ".\n";    //Minimum number of grades is 1,please.
    cout << "You can also enter '-1' to finish enter grades.\n";

    int i=0;
    int grades[ArraySize]={};
    while(i<ArraySize){
        cout << "Enter grade?: ";
        cin >> grades[i];
        if(grades[i]==-1 && i!=0)break;             //在第一次輸入-1視為Out of range
        else if(grades[i]<0 || grades[i]>100)cout << "Out of range!\n";
        else i++;
    }
    cout << "The total number of grades is " << i << ".\n";
    cout << "The maximum of grades is " << GetMax(grades,i) << ".\n";
    cout << "The minimum of grades is " << GetMin(grades,i) << ".\n";
    cout << "The average of grades is " << GetAvg(grades,i) << ".";

    return 0;
}
