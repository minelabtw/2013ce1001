#include <iostream>
using namespace std;

const int ArraySize = 20 ; //使陣列裡最多為20個數
int GetMax(int arr[ArraySize],int _size); //設函式
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);

int main()
{
    int a;
    int grade[ArraySize];
    int Size = 0;

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    for(int number = 0 ; number < 20 ; number++)
    {
        cout << "Enter grade?: ";
        cin >> a;
        grade[Size] = a; //儲存該值到陣列中 保留

        while((grade[Size]) != -1 && (0 > (grade[Size]) || (grade[Size]) > 100))
        {
            cout << "Out of range!" << endl;
            cout << "Enter grade?: ";
            cin >> a;
            grade[Size] = a; //儲存該值到陣列中 保留
        }

        if((grade[Size]) == -1) //當數值等於-1時 跳出迴圈
            break;

        Size++;
    }
    cout << "The total number of grades is " << Size << endl;
    cout << "The maximum of grades is " << GetMax(grade,Size) << endl;
    cout << "The minimum of grades is " << GetMin(grade,Size) << endl;
    cout << "The average of grades is " << GetAvg(grade,Size) << endl;

    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int Max = arr[0];
    for (int b = 0 ; b < _size ; b++) //當其中一個數比Max大時，取代Max
    {
        if(Max < arr[b])
            Max = arr[b];
    }
    return Max; //回傳Max
}

int GetMin(int arr[ArraySize],int _size)
{
    int Min = arr[0];
    for (int c = 0 ; c < _size ; c++) //當其中一個數比Min大時，取代Min
    {
        if(Min > arr[c])
            Min = arr[c];
    }
    return Min;
}

double GetAvg(int arr[ArraySize],int _size)
{
    double total = 0; //此數必須設為double 不然會使double average還是讀出整數
    double average;

    for (int d = 0 ; d < _size ; d++)
    {
        total = total + arr[d];
    }

    average = total / _size;

    return average;
}







