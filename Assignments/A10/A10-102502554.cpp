#include<iostream>
using namespace std;
#define ArraySize 20//定義ArraySize為20

int GetMax(int [],int);
int GetMin(int [],int);
double GetAvg(int [],int);//宣告所需使用之函式

int main()
{
    int i,grade;
    int grades[ArraySize];

    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl << "You can also enter '-1' to finish enter grades." << endl;

    for(i=0; i<ArraySize; i++)
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> grade;

            if( grade == -1 )
                break;
            else if( grade < 0 || grade > 100 )
                cout << "Out of range!" << endl;
        }
        while( grade < 0 || grade > 100);

        if( grade == -1 )
            break;

        grades[i]=grade;
    }//一一輸入成績並將成積存於陣列中,輸入-1則跳出

    cout << "The total number of grades is " << i << "." << endl;
    cout << "The maximum of grades is " << GetMax(grades,i) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,i) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,i) << "." << endl;//輸出成績個數、最高、最低及平均

    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int Max=arr[0];
    for(int j=0; j<_size; j++)
    {
        if(arr[j]>Max)
            Max=arr[j];
    }
    return Max;
}//找最大

int GetMin(int arr[ArraySize],int _size)
{
    int Min=arr[0];
    for(int j=0; j<_size; j++)
    {
        if(arr[j]<Min)
            Min=arr[j];
    }
    return Min;
}//找最小

double GetAvg(int arr[ArraySize],int _size)
{
    double sum=0;//將總和存為double，計算結果才會有小數
    for(int j=0; j<_size; j++)
    {
        sum=sum+arr[j];
    }
    return sum/_size;
}//計算平均
