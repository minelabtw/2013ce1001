#include<iostream>
#define ArraySize 20
using namespace std;



int GetMax(int arr[ArraySize],int _size) //找出最大的grade
{
    int Max=0;
    for (int i; i< _size; i++)
    {
        if(arr[i]>Max)
            Max=arr[i];
    }

    return Max;

}

int GetMin(int arr[ArraySize],int _size) // //找出最小的grade
{
    int Min=arr[0];
    for (int j=0; j< _size; j++)
    {
        if(arr[j]<Min)
            Min=arr[j];
    }

    return Min;
}

double GetAvg(int arr[ArraySize],int _size) ////找出平均的grade
{
    int sum=0;
    double avg=0;
    for (int z=0; z< _size; z++)
    {
        sum =sum+arr[z];
    }
    avg=sum/_size;

    return avg;
}

int main()
{
    int grades[ArraySize]= {0};
    int input=0;

    cout << "Please enter students grades" << endl;
    cout << "Maximun number of the grades is 20" << endl;
    cout << "You can also enter '-1' to finish enter grades" << endl;

    int a=0;

    while(a<20)
    {

        cout << "Enter grades?: " ;
        cin >> input;
        if(input==-1)
            break;
        else if(input<0 || input>100)
            cout << "Out of range!" << endl;

        else
        {
            grades[a]=input; // 將輸入的grade存起來
            a++;

        }
    }

    cout << "The total number of grades is " << a << endl;
    cout << "The maximum of grades is " << GetMax(grades,a) << endl;
    cout << "The minimum of grades is " << GetMin(grades,a) << endl;
    cout << "The average of grades is " << GetAvg(grades,a) << endl;


    return 0;

}
