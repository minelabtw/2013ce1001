#include <iostream>
using namespace std;

const int ArraySize = 20; // initialize constant integer ArraySize
int grades[ ArraySize ] = {}; // initialize array grades and set all elemnets value to 0
int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數
int number; // initialize integer number


int main()
{

    int grade; // initialize integer grade
    cout << "Please enter students grades.\n"
         << "Maximum number of grades is 20.\n"
         << "You can also enter '-1' to finish enter grades.\n";

    for( int counter = 0; counter < ArraySize; counter++ ) // for loop to prompt user to key grades at most 20 times
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> grade;

            if( grade == -1 )
                break;
            if( grade < 0 || grade > 100 )
                cout << "Out of range!\n";
        }
        while( grade < 0 || grade > 100); // do... while loop to check whether grade is on demand


        if( grade == -1 )
            break;

        grades[ counter ] = grade;
        number = counter + 1;
    }

    cout << "The total number of grades is " << number
    << ".\nThe maximum of grades is " << GetMax( grades, number )
    << ".\nThe minimum of grades is " << GetMin( grades, number )
    << ".\nThe average of grades is " << GetAvg( grades, number ) << ".";

    return 0;
}

int GetMax(int arr[ArraySize], int _size)
{
    int highGrade = 0;

    for( int i = 0; i < _size; i++ )
    {
        if( arr[ i ] > highGrade )
            highGrade = arr[ i ];
    }

    return highGrade;
}

int GetMin(int arr[ArraySize], int _size)
{
    int lowGrade = 100;

    for( int i = 0; i < _size; i++ )
    {
        if( arr[ i ] < lowGrade )
            lowGrade = arr[ i ];
    }

    return lowGrade;
}

double GetAvg(int arr[ArraySize], int _size)
{
    double total = 0;

    for( int i = 0; i < _size; i++ )
        total += arr[ i ];

    return total / _size;
}
