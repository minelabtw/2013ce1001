#include <iostream>
using namespace std;
int GetMax(int[],int);
int GetMin(int[],int);
double GetAvg(int[],int);
int main()
{
    int grade;//宣告變數
    const int arraysize=20;//陣列數上限
    int marks[arraysize]= {};
    int no=0;
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish entering grades."<<endl;
    while(no!=20) //當輸入次數不到20時進入
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grade;
            if(grade>100||grade<-1)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(grade>100||grade<-1);

        if(grade==-1)//當輸入-1時跳出
        {
            break;
        }
        marks[no]=grade; //把數值存入陣列
        no++; //每次加1
    }
    cout<<"The total number of grades is "<<no<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(marks,no)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(marks,no)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(marks,no)<<"."<<endl;
    return 0;
}

int GetMax(int marks[],int no)//回傳最大值 傳入陣列、成績總次數
{
    int n=0;
    int a=marks[n];
    while(n<no-1)
    {
        int temp=marks[n+1];
        if(temp>a)
        {
            a=temp;
        }
        n++;
    }
    return a;
}

int GetMin(int marks[],int no) //回傳最小值 傳入陣列、成績總次數
{
    int n=0;
    int a=marks[n];
    while(n<no-1)
    {
        int temp=marks[n+1];
        if(temp<a)
        {
            a=temp;
        }
        n++;
    }
    return a;
}
double GetAvg(int marks[],int no)//回傳平均值 傳入陣列、成績總次數,要有小數點所以用DOUBLE
{
    double temp=0;
    for(int a=0; a<=no-1; a++)
    {
        temp=temp+marks[a];
    }
    temp=temp/no;
    return temp;
}
