#include<iostream>
#define ArraySize 20
using namespace std;
int GetMax(int arr[ArraySize],int _size);
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);

int main()
{
    int grade=0;//�ŧi�ܼ�
    int counter=0;
    int grades[ArraySize];//�ŧi�}�C

    cout<<"Please enter students grades.\n";
    cout<<"Maximum number of grades is 20.\n";
    cout<<"You can also enter '-1' to finish enter grades.\n";
    do//�j��
    {
        cout<<"Enter grade?: ";
        cin>>grade;
        if(grade==-1)
            break;
        else if(grade<0 || grade>100)
        {
            cout<<"Out of range!\n";
        }
        else
        {
            grades[counter]=grade;
            counter++;
        }
        if(counter==20)
            break;
    }
    while(grade!=-1);
    cout<<"The total number of grades is "<<counter<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,counter)<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,counter)<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,counter)<<endl;

    return 0;
}
int GetMax(int arr[ArraySize],int _size)
{
    int Max=arr[0];
    for(int i=0 ; i<_size ; i++)
    {
        if(Max<=arr[i])
            Max=arr[i];
    }
    return Max;
}
int GetMin(int arr[ArraySize],int _size)
{
    int Min=arr[0];
    for(int i=0 ; i<_size ; i++)
    {
        if(Min>=arr[i])
            Min=arr[i];
    }
    return Min;
}
double GetAvg(int arr[ArraySize],int _size)
{
    double total=0;
    for(int i=0 ; i<_size ; i++)
        total=total+arr[i];
    return (total/_size);
}
