#include <iostream>
#define ArraySize 20//將陣列長度設為20個元素
using namespace std;
int GetMax(int score[],int n)
{
    int a=0;
    for(int i=0; i<n; i++)
        if(score[i]>a)
            a=score[i];
    return a;//讓每個陣列元素都和a比，把比較大的代換進去
}//set up a function to find the biggest number of the grade

int GetMin(int score[],int n)
{
    int a=100;
    for(int i=0; i<n; i++)
        if(score[i]<a)
            a=score[i];
    return a;//讓每個陣列元素都和a比，把比較小的代換進去
}//set up a function to find the smallest number of the grade

double GetAvg(double total,int n)
{
    double avg=total/n;
    return avg;//compute the average value
}
void grade(int score[] , int *i , int *total , int *time)
{
    do
    {
        cout<<"Enter grade?: ";
        cin>>score[*i];
        if(score[*i]==-1)
            break;//if the user input -1 , then end this loop
        if(score[*i]<0 or score[*i]>100)
        {
            cout<<"Out of range!"<<endl;
            continue;//loop until the correct value is input
        }
        *total=*total+score[*i];//calculate the total grade
        *i += 1; //+1 each time for the next record
        if(*i==20) //at most 20 times
            break;
    }
    while(1);
}
int main()
{
    int score[ArraySize]= {};
    int i=0;
    int total=0;
    int time=1;
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish entering grades."<<endl;
    grade(score, &i , &total , &time);
    cout<<"The total number of grades is "<<i<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(score,i)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(score,i)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(total,i)<<"."<<endl;
    return 0;
}

