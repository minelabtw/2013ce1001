#include <iostream>
using namespace std;

#define ArraySize 20 //定義ArraySize值為20

int GetMax(int [],int );
int GetMin(int [],int );
double GetAvg(int [],int ); //prototype

int main()
{
    int grades[ArraySize]= {};
    int i=0;

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl; //題目

    do
    {
        cout << "Enter grade?: ";
        cin >> grades[i]; //輸入

        if (grades[i]==-1)
            break; //輸入值為-1結束
        else if(grades[i]>100 || grades[i]<0)
        {
            cout << "Out of range!" << endl;
            i--;
        } //錯誤顯示
        i++;
    }
    while ( grades[i]!=-1 && i<ArraySize );

    cout << "The total number of grades is " << i << endl;
    cout << "The maximum of grades is " << GetMax(grades,i) << endl;
    cout << "The minimum of grades is " << GetMin(grades,i) << endl;
    cout << "The average of grades is " << GetAvg(grades,i) << endl; //呼叫函式印出結果


    return 0;
}

int GetMax(int arr[ArraySize],int _size) //最大值函式
{
    int biggest=0; //最大值

    for(int i=0; i<_size; i++)
        if (biggest<arr[i])
        biggest=arr[i];

    return biggest;
}

int GetMin(int arr[ArraySize],int _size) //最小值函式
{
    int smallest=100; //最小值

    for(int i=0; i<_size; i++)
        if (smallest>arr[i])
        smallest=arr[i];

    return smallest;
}

double GetAvg(int arr[ArraySize],int _size) //平均
{
    double total=0;

    for (int i=0; i<_size; i++)
        total += arr[i];

    return (total/_size);
}
