#include<iostream>
#define ArraySize 20           //定義array有20筆資料
using namespace std ;
int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列,成績總次數

int main()
{
    int i=0 ;    //用來記數的
    int j=0 ;    //用來輸入使用者所輸入的成績
    int grades[ArraySize]= {}; //用陣列來記憶所輸入過的成績


    cout<<"Please enter students grades."<<endl ;
    cout<<"Maximum number of grades is 20."<<endl ;
    cout<<"You can also enter '-1' to finish  entering grades."<<endl ;

    do //後測試迴圈
    {

        cout<<"Enter grade?: " ;
        cin>>j ;


        if(j>=0 && j<=100 &&j!=-1)
        {
            grades[i]=j ;          //在有效地輸入下i會主動加1且將該數入的有效的數字傳入陣列
            i=i+1 ;

        }
        if(j==-1 || i==19)
        {
            cout<<"The total number of grades is "<<i<<endl ;
            break ;

        }
        else if(j<0 || j>100)
        {
            cout<<"Out of range!"<<endl ;
        }
    }
    while(i<20) ;

    cout<<"The maximum of grades is "<<GetMax(grades,i)<<endl ;
    cout<<"The minimum of grades is "<<GetMin(grades,i)<<endl ;
    cout<<"The average of grades is "<<GetAvg(grades,i)<<endl ;

    return 0 ;
}
int GetMax(int arr[ArraySize],int _size)
{
    int max=arr[0] ;
    for(int k=1 ; k<_size ; k=k+1)                //先在函式中運用選擇排序找出最大值並運用函是將其運算結果回傳到主程式中
    {
        if(max<arr[k])
        {
            max=arr[k] ;
        }
    }
    return max ;
}
int GetMin(int arr[ArraySize],int _size )
{
    int min=arr[0] ;
    for(int k=1 ; k<_size ; k=k+1)     //先在函式中運用選擇排序找出最小值並運用函是將其運算結果回傳到主程式中
    {
        if(min>arr[k])
        {
            min=arr[k] ;
        }

    }
    return min ;

}
double GetAvg(int arr[ArraySize],int _size)
{
    double sum=0 ;
    for(int k=0 ; k<=_size ; k=k+1)      //先在函式中運用迴圈的概念找出平均值並運用函是將其運算結果回傳到主程式中
    {
        sum=sum+arr[k] ;
    }

    return sum/_size  ;
}
