#include<iostream>
#define ArraySize 20
using namespace std;

int grades[ArraySize] = {}; //宣告成績陣列
int GetMax(int grades[ArraySize],int Size); //回傳最大值 傳入陣列、成績總次數
int GetMin(int grades[ArraySize],int Size); //回傳最小值 傳入陣列、成績總次數
double GetAvg(int grades[ArraySize],int Size); //回傳平均值 傳入陣列、成績總次數

int main()
{
    int grade,n = 0;
    cout << "Please enter students grades."<<endl
         << "Maximum number of grades is 20." << endl
         << "You can also enter '-1' to finish enter grades."<<endl;
    for(int i=0; i<ArraySize; i++)
    {
        cout << "Enter grade?: ";
        cin >> grade;
        while(grade<0||grade>100)
        {
            if( grade == -1 )
                break;
            cout << "Out of range!" << endl
                 << "Enter grade?: ";
            cin >> grade;
        }
        if( grade == -1 )
            break;
        grades[i] = grade;         //將grade值傳入陣列
        n = i+1 ;                 //紀錄次數
    }
    if(n==0)               //我想不到怎樣讓他最初就輸入-1時能正常輸出,只好土法煉鋼
    {
        cout << "The total number of grades is 0." << endl
             << "The maximum of grades is 0." << endl
             << "The minimum of grades is 0." << endl
             << "The average of grades is 0." << endl;
    }
    else             //傳值進函式
    {
        cout << "The total number of grades is " << n << "." << endl
             << "The maximum of grades is " << GetMax( grades, n ) << "." << endl
             << "The minimum of grades is " << GetMin( grades, n ) << "." << endl
             << "The average of grades is " << GetAvg( grades, n ) << "." << endl;
    }

    return 0;
}

int GetMax(int grades[ArraySize],int Size)
{
    int Max;
    for(int i=0; i<Size; i++)
    {
        if(Max<grades[i])
            Max = grades[i];
    }
    return Max;
}

int GetMin(int grades[ArraySize],int Size)
{
    int Min;
    for(int i=0; i<Size; i++)
    {
        if(Min>grades[i])
            Min = grades[i];
    }
    return Min;
}

double GetAvg(int grades[ArraySize],int Size)
{
    int total;
    for(int i=0; i<Size; i++)
    {
        total += grades[i];
    }
    return total / Size;
}
