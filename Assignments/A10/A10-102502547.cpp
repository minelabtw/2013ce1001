#include <iostream>
#define ArraySize 20
using namespace std;
int *GetArray(); //回傳陣列
int GetSize(int arr[]); //回傳成績總次數 傳入陣列
int GetMax(int arr[],int _size); //回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[],int _size); //回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[],int _size); //回傳平均值 傳入陣列、成績總次數

int main()
{
    int *a;
    a=GetArray();
    int b=GetSize(a);

    cout << "The total number of grades is " << b << ".";
    cout << "\nThe maximum of grades is " << GetMax(a,b) << ".";
    cout << "\nThe minimum of grades is " << GetMin(a,b) << ".";
    cout << "\nThe average of grades is " << GetAvg(a,b) << ".";

    return 0;
}

int *GetArray()
{
    static int arr[ArraySize]= {0}; //使用靜態陣列
    cout << "Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades.\n";
    for(int i=0; i<ArraySize; i++)
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> arr[i];
            if(arr[i]<-1 || arr[i]>100)
                cout << "Out of range!\n";
        }
        while(arr[i]<-1 || arr[i]>100); //輸入成績 範圍不符重新輸入

        if(arr[i]==-1) //如果輸入-1 結束輸入
            break;
    }
    return arr;
}

int GetSize(int arr[])
{
    int x=ArraySize;
    for(int i=0; i<ArraySize; i++)
    {
        if(arr[i]==-1)
        {
            x=i;
            i=ArraySize;
        }
    }
    return x;
}

int GetMax(int arr[],int _size)
{
    int x=arr[0];
    for(int i=1; i<_size; i++) //比大小 x等於最大值
    {
        if(x<arr[i])
            x=arr[i];
    }
    return x;
}

int GetMin(int arr[],int _size)
{
    int x=arr[0];
    for(int i=1; i<_size; i++) //比大小 x等於最小值
    {
        if(x>arr[i])
            x=arr[i];
    }
    return x;
}

double GetAvg(int arr[],int _size)
{
    double x=0;
    for(int i=0; i<_size; i++) //算平均
    {
        x=x+arr[i];
    }
    return x/_size;
}
