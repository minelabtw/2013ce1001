#include <iostream>
#include <limits>
#define ArraySize 20

using namespace std;

int main()
{
    int grades[ ArraySize ] = {}, number = ArraySize;
    void input( int (&grades)[ ArraySize ], int &number );  //取得正確的成績
    int GetMax( int arr[ ArraySize ], int _size );  //回傳最大值 傳入陣列、成績總次數
    int GetMin( int arr[ ArraySize ], int _size );  //回傳最小值 傳入陣列、成績總次數
    double GetAvg( int arr[ ArraySize ], int _size );  //回傳平均值 傳入陣列、成績總次數

    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl << "You can also enter '-1' to finish enter grades." << endl;
    input( grades, number );
    cout << "The total number of grades is " << number << ".\nThe maximum of grades is " << GetMax( grades, number )
         << ".\nThe minimum of grades is " << GetMin( grades, number ) << ".\nThe average of grades is " << GetAvg( grades, number ) << ".";
    return 0;
}

void input( int (&grades)[ ArraySize ], int &number )
{
    for( int i = 0; i < ArraySize; i++)  //可以跑20次
    {
        do
        {
            cout << "Enter grade?: ";
            cin  >> grades[ i ];
            if ( !(cin) )  //如果輸入非整數的話
            {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                grades[ i ] = -2;  //給他一個錯誤的輸入
            }
        }
        while ( ( ( grades[ i ] !=-1 && grades[ i ] < 0 ) || grades[ i ] > 100  ) && cout << "Out of range!\n" );  //不在範圍內的話就提是錯誤並重來
        if ( grades[ i ] == -1)  //結束輸入
        {
            number = i;  //代表總共有幾個成績
            i = ArraySize;  //給迴圈一個結束的假象
        }
    }
}
int GetMax( int arr[ArraySize], int _size )
{
    int Max = 0;
    for( int i = 0; i < _size; i++)  //每一個都比較
    {
        if( arr[ i ] > Max )
            Max = arr [ i ];  //如果遇到比他大的就把max改成那個成績
    }
    return Max;
}
int GetMin( int arr[ArraySize], int _size )
{
    int Min = 100;
    for( int i = 0; i < _size; i++)
    {
        if( arr[ i ] < Min )
            Min = arr [ i ];  //同理，找最小成績
    }
    if ( _size == 0 )
        Min = 0;
    return Min;
}
double GetAvg( int arr[ArraySize], int _size )
{
    double Avg = 0;
    for( int i = 0; i < _size; i++)
    {
        Avg += arr[ i ];  //累加
    }
    if ( _size == 0 )
        return 0;
    return Avg/_size;  //輸出總成績/成績數
}
