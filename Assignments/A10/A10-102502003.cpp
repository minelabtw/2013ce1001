#include<iostream>
#define ArraySize 20
using namespace std;

int GetMax(int[],int);//回傳最大值 傳入陣列、成績總次數
int GetMin(int[],int);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int[],int);//回傳平均值 傳入陣列、成績總次數

int main()
{
    int grade=0;
    int grades[20];
    int countN=0;

    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;

    while(countN!=20)
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grade;
            if(grade<-1||grade>100)
                cout<<"Out of range!"<<endl;
        }
        while(grade<-1||grade>100);

        grades[countN]=grade;
        countN++;

        if(grade==-1)
            break;

    }

       cout<<"The total number of grades is "<<countN<<endl;
       cout<<"The maximum of grades is "<<GetMax(grades,countN)<<endl;
       cout<<"The minimum of grades is "<<GetMin(grades,countN)<<endl;
       cout<<"The average of grades is "<<GetAvg(grades,countN)<<endl;




    return 0;

}


int GetMax(int arr[],int Size)
{
    int Max=0;

    for(int i=0; i<Size; i++)
    {
        if(arr[i]>Max)
            Max=arr[i];
    }

    return Max;
}

int GetMin(int arr[],int Size)
{
    int Min=arr[0];

    for(int i=0; i<Size; i++)
    {
        if(arr[i]<Min)
            Min=arr[i];
    }


    return Min;
}

double GetAvg(int arr[],int Size)
{
    double average=0;
    int total=0;

    for(int i=0; i<Size; i++)
        total=total+arr[i];

    average=total/Size;

    return average;
}





