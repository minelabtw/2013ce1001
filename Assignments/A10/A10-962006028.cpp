#include<iostream>
using namespace std ;

#define ArraySize 20

int GetMax(int arr[ArraySize],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int max=arr[0];
    for (int i=1; i<_size; i++)
    {
        if ( max < arr[i])
        {
            max=arr[i];
        }
    }
    return max;
}

int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int min=arr[0];
    for (int i=1; i<_size; i++)
    {
        if (min > arr[i])
        {
            min=arr[i];
        }
    }
    return min;
}

double GetAvg(int arr[ArraySize],int _size)//回傳平均值 傳入陣列、成績總次數
{
    double a=0;
    if (_size>0)
    {
        for (int i=0; i<_size; i++)
        {
            a+=arr[i];
        }
        a= a/ _size;
    }
    else
    {
        a=0;
    }
    return a ;
}

int size (int arr[ArraySize])  //紀錄數據 ，回傳次數
{
    int _size=0,n;
    do
    {
        cout<<"Enter grade?: " ;
        cin >> n ;
        if (n<101&&n>-1)
        {
            arr[_size]=n;
            _size++;
        }
        else if (n==-1)
        {
            break ;
        }
        else
        {
            cout <<"Out of range!"<<endl;
        }
    }
    while (_size<20&& n!=-1) ;
    return _size;
}

int main ()
{
    int _size=0;
    int grades[ArraySize];//宣告成績陣列
    cout<<"Please enter students grades." << endl;
    cout<<"Maximum number of grades is 20." <<endl;
    cout<<"You can also enter '-1' to finish enter grades." <<endl;
    _size= size (grades);

    cout <<"The total number of grades is " << _size<<endl;
    cout<< "The maximum of grades is " << GetMax(grades, _size)<<endl;
    cout<< "The minimum of grades is " << GetMin(grades, _size)<<endl;
    cout <<" The average of grades is " << GetAvg(grades, _size)<<endl;
    return 0;
}

