#include<iostream>

using namespace std;

const int ArraySize = 20;
int grades[ArraySize]= {}; //宣告成績陣列
int GetMax(int arr[ArraySize],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int highest = 0;
    for(int x=1; x<=_size; x++)
    {
        if (arr[x]>=highest)
            highest=grades[x];
    }
    return highest;
}

int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int lowest = 100;
    for(int x=1; x<=_size; x++)
    {
        if (arr[x]<=lowest)
            lowest=grades[x];
    }
    return lowest;
}
double GetAvg(int arr[ArraySize],int _size)//回傳平均值 傳入陣列、成績總次數
{
    double average = 0;
    for (int x=1; x<=_size; x++)
    {
        average = average+arr[x];
    }
    return average/_size;
}

int main()
{
    int input;
    int number;

    cout <<"Please enter students grades."<<endl;
    cout <<"Maximum number of grades is 20."<<endl;
    cout <<"You can also enter '-1' to finish enter grades."<<endl;

    for(int times=1; times<=20; times++)
    {
        cout <<"Enter grade?: ";
        cin >>input;
        if (input==-1)//當input=1的動作
        {
            break;
        }
        while(input<0||input>100)//判定input的範圍
        {
            cout <<"Out of range!"<<endl;
            cout <<"Enter grade?: ";
            cin >>input;
        }
        grades[times]=input;
        number=times;
    }

    cout <<"The total number of grades is "<<number<<"."<<endl;//輸出回傳的數
    cout <<"The maximum of grades is "<<GetMax(grades,number)<<"."<<endl;
    cout <<"The minimum of grades is "<<GetMin(grades,number)<<"."<<endl;
    cout <<"The average of grades is "<<GetAvg(grades,number)<<".";

    return 0;
}
