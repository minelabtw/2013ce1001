#include<iostream>
using namespace std;
#define ArraySize 20
int grades[ArraySize] = {};//宣告成績陣列
int GetMax(int arr[ArraySize],int _size );//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數
int main()
{
    int input=0;//宣告變數
    int i=0;

    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;

    while(input!=-1)//若input不等於1可以一直跑
    {
        cout<<"Enter grade?: ";
        cin>>input;
        if(input==-1)//若input等於1可以直接結束
        {
            break;
        }
        else if(input<0 or input>100)//若input不在0~100之間顯示Out of range!
        {
            cout<<"Out of range!"<<endl;
        }
        else if(input>=0&&input<=100)//若input在0~100之間,則會將input存入grades[i]中
        {
            grades[i] =input;
            i++;
        }
        if(i==20)//使用者最多只能輸入20次
        {
            break;
        }
    }
    cout<<"The total number of grades is " << i<<"."<<endl;
    cout<<"The maximum of grades is " <<GetMax(grades,i)<<"."<<endl;//呼叫函式
    cout<<"The minimum of grades is " <<GetMin(grades,i)<<"."<<endl;
    cout<<"The average of grades is " <<GetAvg(grades,i) << "."<<endl;

    return 0;
}
int GetMax(int arr[ArraySize],int _size)
{
    int highGrade=0;
    for(int i=0; i<_size; i++ )
    {
        if(arr[i]>highGrade)
            highGrade = arr[i];
    }
    return highGrade;
}
int GetMin(int arr[ArraySize],int _size)
{
    int lowGrade=100;
    for(int i=0; i<_size; i++)
    {
        if(arr[i] < lowGrade)
            lowGrade=arr[i];
    }
    return lowGrade;
}
double GetAvg(int arr[ArraySize],int _size)
{
    double total=0;
    for(int i =0; i<_size; i++ )
        total=total+arr[i];
    return total/_size;
}
