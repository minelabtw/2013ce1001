#include <iostream>
#define ArraySize 20

using namespace std;

int GetMax(int arr[ArraySize],int _size)
{
    // if number is greater than _max then _max is the number
    int _max = -1;
    for (int i=0; i<_size; i++)
        if (_max < arr[i]) _max = arr[i];
    return _size ? _max : 0;
}

int GetMin(int arr[ArraySize],int _size)
{
    // if number is smaller than _min then _min is the number
    int _min = 101;
    for (int i=0; i<_size; i++)
        if (_min > arr[i]) _min = arr[i];
    return _size ? _min : 0;
}

double GetAvg(int arr[ArraySize],int _size)
{
    // add all of the numbers and then divide by the size
    double avg = 0;
    for (int i=0; i<_size; i++)
        avg += arr[i];
    return _size ? avg / _size : 0;
}

int main()
{
    int input = 0, i = 0;
    int grades[ArraySize] = {}; // init the arr zero
    // print ui
    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
    for (i = 0; i<20; i++)
    {
        // repeat input
        do
        {
            cout << "Enter grade?: ";
            cin >> input;
        }
        while(((input < 0 || input > 100) && input != -1)&&cout << "Out of range!" << endl);
        // break when input is -1
        if (input == -1)
            break;
        grades[i] = input;
    }
    // print result
    cout << "The total number of grades is " << i << "." << endl;
    cout << "The maximum of grades is " << GetMax(grades, i) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades, i) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades, i) << "." << endl;
    return 0;
}
