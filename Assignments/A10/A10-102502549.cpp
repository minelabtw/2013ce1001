#include <iostream>
#define ArraySize 20
using namespace std;

//要使用函式，下為prototype：
int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數，平均值為double型態
void work(int arr[ArraySize],int &count);//輸入成績並檢查範圍，輸入-1跳出迴圈

int main()
{
    int grades[ArraySize];//宣告成績陣列
    int count;//儲存成績總數

    cout<<"Please enter students grades."<<endl
        <<"Maximum number of grades is 20."<<endl
        <<"You can also enter '-1' to finish enter grades."<<endl;

    work(grades,count);//呼叫神奇的work函式

    cout<<"The total number of grades is "<<count<<"."<<endl
        <<"The maximum of grades is "<<GetMax(grades,count)<<"."<<endl
        <<"The minimum of grades is "<<GetMin(grades,count)<<"."<<endl
        <<"The average of grades is "<<GetAvg(grades,count)<<".";

    return 0;
}

//實作GetMax
int GetMax(int arr[ArraySize],int _size)
{
    int max=arr[0];

    for(int i=1; i<_size; i++)
    {
        if(arr[i]>max)
            max=arr[i];
    }

    if(_size==0)
        return 0;
    else
        return max;
}

//實作GetMin
int GetMin(int arr[ArraySize],int _size)
{
    int min=arr[0];

    for(int i=1; i<_size; i++)
    {
        if(arr[i]<min)
            min=arr[i];
    }

    if(_size==0)
        return 0;
    else
        return min;
}

//實作GetAvg
double GetAvg(int arr[ArraySize],int _size)
{
    double sum=0;

    for(int i=0; i<_size; i++)
    {
        sum+=arr[i];
    }

    if(_size==0)
        return 0;
    else
        return sum/_size;
}

//實作work函式
void work(int arr[ArraySize],int &count)
{
    for(count=0; count<ArraySize; count++)
    {
        //輸入並檢查
        do
        {
            cout<<"Enter grade?: ";
            cin>>arr[count];

            if((arr[count]<0||arr[count]>100)&&arr[count]!=-1)
                cout<<"Out of range!"<<endl;
        }
        while((arr[count]<0||arr[count]>100)&&arr[count]!=-1);

        //當數入-1跳出迴圈
        if(arr[count]==-1)
            break;
    }
}


