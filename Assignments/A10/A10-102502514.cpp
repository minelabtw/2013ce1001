#include <iostream>
#include <cstdlib>
using namespace std;

const int ArraySize=20;
int grades[ArraySize];  //宣告陣列
int GetMax(int arr[ArraySize],int _size);  //宣告函式
int GetMin(int arr[ArraySize],int _size);  //宣告函式
double GetAvg(int arr[ArraySize],int _size);  //宣告函式

int main()
{
    int grade;
    int i=0;
    cout <<"Please enter students grades."<<endl;
    cout <<"Maximum number of grades is 20."<<endl;
    cout <<"You can also enter '-1' to finish entering grades."<<endl;

    while (true)
    {
        cout <<"Enter grade?: ";
        cin >>grade;
        if (grade<=100&&grade>=0)
        {
            grades[i]=grade;
            i++;
            if (i==20)
                break;
        }
        if (grade==-1)
            break;
        while (grade<0||grade>100)
        {
            cout <<"Out of range!\nEnter grade?: ";
            cin >>grade;
            if (grade<=100&&grade>=0)
            {
                grades[i]=grade;
                i++;
                if (i==20)
                    break;
            }
        }
    }
    cout <<"The total number of grades is "<<i<<"."<<endl;  //印出總共輸入幾次有效成績
    cout <<"The maximum of grades is "<<GetMax(grades,i)<<"."<<endl;  //回傳函式
    cout <<"The minimum of grades is "<<GetMin(grades,i)<<"."<<endl;  //回傳函式
    cout <<"The average of grades is "<<GetAvg(grades,i)<<".";  //回傳函式

    return 0;
}
int GetMax(int arr[ArraySize],int _size)
{
    int Max=0;
    for (int ArraySize=0; ArraySize<=_size; ArraySize++)  //注意:ArraySize<=_size
        if(arr[ArraySize]>Max)
            Max=arr[ArraySize];
    return Max;
}
int GetMin(int arr[ArraySize],int _size)
{
    int Min=100;
    for (int ArraySize=0; ArraySize<_size; ArraySize++)  //注意:ArraySize<_size
        if (arr[ArraySize]<Min)
            Min=arr[ArraySize];
    return Min;
}
double GetAvg(int arr[ArraySize],int _size)
{
    double total=0;
    for (int ArraySize=0; ArraySize<=_size; ArraySize++)  //注意:ArraySize<=_size
        total=total+arr[ArraySize];
    return total/_size;
}
