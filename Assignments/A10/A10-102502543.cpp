#include <iostream>
using namespace std;
int gmax(int [],int); //宣告函式
int gmin(int [],int);
double average(int [],int);
int main()
{
    const int a=20; //宣告陣列大小
    int g[a]= {}; //初始化陣列
    int e=0; //宣告變數
    cout <<"Please enter students grades."<<endl; //輸出
    cout <<"Maximum number of grades is 20."<<endl;
    cout <<"You can also enter '-1' to finish enter grades."<<endl;
    do
    {
        cout <<"Enter grade?: ";
        cin >>g[e];
        if (g[e]>=0 && g[e]<=100)
            e++;
        else if (g[e]!=-1)
            cout <<"Out of range!"<<endl;
    }
    while (g[e]!=-1 && e<20); //迴圈
    cout <<"The total number of grades is "<<e<<"."<<endl; //輸出成績
    cout <<"The maximum of grades is "<<gmax(g,e)<<"."<<endl;
    cout <<"The minimum of grades is "<<gmin(g,e)<<"."<<endl;
    cout <<"The average of grades is "<<average(g,e)<<".";
    return 0;
}
int gmax(int b[],int n) //最大值函式
{
    int x=0;
    for(int t=0; t<n; t++)
    {
        if (b[t]>x)
            x=b[t];
    }
    return x;
}
int gmin(int b[],int n) //最小值函式
{
    int x=b[0];
    for(int t=1; t<n; t++)
    {
        if (b[t]<x)
            x=b[t];
    }
    if (x==-1)
        x=0;
    return x;
}
double average(int b[],int n) //平均值函式
{
    for(int t=0; t<n-1; t++)
        b[t+1]+=b[t];
    double x=b[n-1];
    x/=n;
    if (n==0)
        x=0;
    return x;
}
