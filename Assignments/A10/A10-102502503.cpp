#include <iostream>
using namespace std;
#define ArraySize 20  //限定陣列大小
int GetMax(int arr[ArraySize],int _size);  //回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);  //回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);  //回傳平均值 傳入陣列、成績總次數
int main()
{
    int grade;
    int i=0;
    int counter=0;
    int arr[ArraySize];  //宣告ArraySize陣列
    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
    while (grade!=-1 && i!=ArraySize)  //若輸入值=-1或輸入滿20次則跳出迴圈
    {
        do  //判斷輸入值是否在範圍內
        {
            cout << "Enter grade?: ";
            cin >> grade;
            if (grade==-1 or i==ArraySize-1)
                break;
            if (grade>100 or grade<0)
                cout << "Out of range!" << endl;
        }
        while (grade>100 or grade<0);
        if (grade!=-1)  //若輸入值不等於-1則將其值存入陣列
        {
            arr[i]=grade;
            i++;
            counter++;
        }
    }
    cout << "The total number of grades is " << counter << "." << endl;
    cout << "The maximum of grades is " << GetMax(arr,counter) << "." << endl;  //呼叫GetMax函式
    cout << "The minimum of grades is " << GetMin(arr,counter) << "." << endl;  //呼叫GetMin函式
    cout << "The average of grades is " << GetAvg(arr,counter) << ".";  //呼叫GetAvg函式
    return 0;
}
int GetMax(int arr[ArraySize],int _size)
{
    int Max=0;  //初始Max為0
    for(int i=0; i<=_size-1; i++)  //取最大值
    {
        if (arr[i]>=Max)
            Max=arr[i];
    }
    return Max;  //回傳最大值
}
int GetMin(int arr[ArraySize],int _size)
{
    int Min=100;  //初始Min為100
    for(int i=0; i<=_size-1; i++)  //取最小值
    {
        if (arr[i]<=Min)
            Min=arr[i];
    }
    return Min;  //回傳最小值
}
double GetAvg(int arr[ArraySize],int _size)
{
    int sum=0;
    double average=0;
    for(int i=0; i<=_size-1; i++)  //計算總合
    {
        sum+=arr[i];
    }
    average=sum/_size;  //計算平均值
    return average;  //回傳平均值
}
