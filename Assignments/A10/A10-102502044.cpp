/*************************************************************************
    > File Name: A10-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年12月05日 (週四) 16時14分24秒
 ************************************************************************/

#include <stdio.h>
#define ArraySize 20

int GetMax(int arr[ArraySize],int _size);		// get max of arr
int GetMin(int arr[ArraySize],int _size);		// get min of arr
double GetAvg(int arr[ArraySize],int _size);	// get avg of arr

int input(int *tmp);	// get input

int main()
{
	int grades[ArraySize]; // array to save grades
	int num; // number to save times of input

	puts("Please enter students grades.");						// ui message
	printf("Maximum number of grades is %d.\n", ArraySize);		// ui message
	puts("You can also enter '-1' to finish enter grades.");	// ui message

	for(num = 0 ; num<ArraySize ; num++) // use for loop to get input
		if(input(grades + num))	// if input -1,
			break;				//		,then break the loop

	printf("The total number of grades is %d.\n", num);	// output times of input
	printf("The maximum of grades is %d.\n", GetMax(grades, num));	// output max
	printf("The minimum of grades is %d.\n", GetMin(grades, num));	// output min
	printf("The average of grades is %lf.\n", GetAvg(grades, num));	// output avg
	
	return 0; // exit program
}

int input(int *tmp)
{
	for(;;)
	{
		printf("Enter grade?: "); // ui message
		scanf("%d", tmp); // input number
	
		if(*tmp == -1) // if input -1
			return -1; //	,then exit this input, and return -1, mean input end
	
		if(0<=*tmp && *tmp<=100) // check *tmp is in range[0, 100]
			return 0; // exit this function, and return 0, mean seccus
		else
			puts("Out of range!"); // error message
	}
}

int GetMax(int arr[ArraySize],int _size)
{
	int ans = 0; // init max
	for(int i=0 ; i<_size ; i++) // find max in arr
		if(arr[i] > ans) // if arr[i] > max, now
			ans = arr[i];//		,then max must be arr[i]
	return ans; // return max
}

int GetMin(int arr[ArraySize],int _size)
{
	int ans = 100; // init min
	for(int i=0 ; i<_size ; i++) // find min in arr
		if(arr[i] < ans) // if arr[i] < min, now
			ans = arr[i];//		,then min must be arr[i]
	return ans; // return min
}

double GetAvg(int arr[ArraySize],int _size)
{
	double ans = 0.0; // init total
	for(int i=0 ; i<_size ; i++) // calc total of arr
			ans += arr[i]; // add arr[i] into total
	return ans/_size; // return total / size, mean avg
}
// this is the 80th line, my code is in range. XP
