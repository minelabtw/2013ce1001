#include <iostream>
using namespace std;
int GetMax( int [], int );
int GetMin( int [], int );
double GetAvg( int [], int );
int record( int [], int );
main()
{
    int grades[ 20 ]= {};  //設成績陣列
    int sum=0;  //設變數
    int Max;
    int Min;
    double avg;

    cout << "Please enter students grades.\n";  //題示
    cout << "Maximum number of grades is 20.\n";
    cout << "You can also enter '-1' to finish enter grades.\n";

    sum=record( grades, sum );  //輸入成績
    Max=grades[ 0 ];
    Max=GetMax( grades, Max );  //紀錄最大值
    Min=grades[ 0 ];
    Min=GetMin( grades, Min );  //紀錄最小值
    avg=sum;
    avg=GetAvg( grades, avg );  //紀錄平均值

    cout << "The total number of grades is " << sum << ".\n";  //輸出結果
    cout << "The maximum of grades is " << Max << ".\n";
    cout << "The minimum of grades is " << Min << ".\n";
    cout << "The average of grades is " << avg << ".";

    return 0;
}
int record( int arr[ 20 ], int _size )  //輸入
{
    for( int i=0; i<20; i++ )
    {
        do  //確認合理性
        {
            cout << "Enter grade?: ";
            cin >> arr[ i ];
            if( arr[ i ]==-1 )
                break;
        }
        while( arr[ i ]<0 && cout << "Out of range!\n" || arr[ i ]>100 && cout << "Out of range!\n" );
        if ( arr[ i ]==-1 )  //跳出
            break;
        _size++;
    }

    return _size;
}
int GetMax( int arr[ 20 ], int _size )  //取最大值
{
    for( int i=1; i<20; i++ )  //取大的
    {
        if( arr[ i ]==-1 )
            break;
        else if( _size<arr[ i ] )
            _size=arr[ i ];
    }

    return _size;
}
int GetMin( int arr[ 20 ], int _size )  //取最小值
{
    for( int i=1; i<20; i++ )  //取小的
    {
        if( arr[ i ]==-1 )
            break;
        else if( _size>arr[ i ] )
            _size=arr[ i ];
    }

    return _size;
}
double GetAvg( int arr[ 20 ], int _size )  //計算平均
{
    double x=0;
    for( int i=0; i<20; i++ )
    {
        if( arr[ i ]!=-1 )
        {
            x=x+arr[ i ];
        }
    }

    return x/_size;;
}
