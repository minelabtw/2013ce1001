#include <iostream>
#define ArraySize 20

using namespace std;

int grades[ArraySize]= {}; //宣告陣列

int GetMax(int arr[ArraySize],int _size) //找出陣列的最大值
{
    int Max_grade = 0;

    for (int i=0; i<_size; i++)
    {
        if (arr[i]>Max_grade)
            Max_grade = arr[i];
    }

    return Max_grade;
}

int GetMin(int arr[ArraySize],int _size) //找出陣列的最小值
{
    int Min_grade = 100;

    for (int i=0; i<_size; i++)
    {
        if (arr[i]<Min_grade)
            Min_grade = arr[i];
    }

    return Min_grade;
}

double GetAvg(int arr[ArraySize],int _size) //計算平均
{
    int total = 0;

    for (int i=0; i<_size; i++)
        total = total + arr[i];

    return total/_size;
}

int main()
{
    int grade = 0;
    int grade_count = 0;

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    do
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> grade;
            if (grade!=-1 and (grade<0 or grade>100))
                cout << "Out of range!" << endl;
        }
        while (grade!=-1 and (grade<0 or grade>100));

        if (grade==-1)
            break;
        else
        {
            grades[grade_count] = grade; //紀錄成績
            grade_count++;
        }
    }
    while (grade_count<20 and grade!=-1);

    cout << "The total number of grades is " << grade_count << "." << endl; //輸出
    cout << "The maximum of grades is " << GetMax(grades,grade_count) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,grade_count) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,grade_count) << ".";

    return 0;
}
