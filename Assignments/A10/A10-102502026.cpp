//A10-102502026
#include<iostream>
using namespace std;
#define AS 20   //use AS=20(ArraySize
int GetMax(int [],int );    //using as prototype
int GetMin(int [],int);     //using as prototype
double GetAvg(int [], int); //using as prototype

int main()
{
    int grade=0;    //define grade
    int C=0;        //define as counter
    int a[AS];      //define array
    cout<<"Please enter students grades.\n";    //print
    cout<<"Maximum number of grades is 20.\n";
    cout<<"You can also enter '-1' to finish enter grades.\n";

    while(grade!=-1 && C<AS)    //quit when grade is -1 and when counter is 20
    {
        cout<<"Enter grade?: "; //ask grade
        cin>>grade;
        if(grade>100 || grade<0 && grade!=-1)   //if grade <0 or >100 and cant be -1
            cout<<"Out of range!\n";
        if(grade>=0 && grade<=100)  //if grade is 0~100
        {
            a[C]=grade;
            C++;
        }
    }
    cout<<"The total number of grades is "<<C<<"."<<endl;   //print counter
    cout<<"The maximum of grades is "<<GetMax(a,C)<<"."<<endl;  //print max grade
    cout<<"The minimum of grades is "<<GetMin(a,C)<<"."<<endl;  //print min grade
    cout<<"The average of grades is "<<GetAvg(a,C)<<"."<<endl;  //print average
    return 0;
}

int GetMax(int a[], int C)
{
    int Max=0;  //assume max grade is 0
    for (int x=0; x<C; x++)
    {
        if(a[x]>Max)    //if current grade > Max
            Max=a[x];
    }
    return Max;
}
int GetMin(int a[], int C)
{
    int Min=100;    //assume min grade is 0
    for (int x=0; x<C; x++)
    {
        if(a[x]<Min)    //if current grade < Min
            Min=a[x];
    }
    return Min;
}
double GetAvg(int a[], int C)
{
    double total=0; //define total
    for(int x=0; x<C; x++)
        total= a[x]+total;  //sum all the grades
    total=total/C;  //all the sum grades divided to the counter
    return total;
}
