#include<iostream>
#include<algorithm>
#define ArraySize 20
using namespace std;

int GetMax(int arr[ArraySize],int _size)//回傳最大值 傳入陣列、成績總次數
{
    return *max_element(arr,arr+_size);//利用內建函式找出最大值
}
int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    return *min_element(arr,arr+_size);//利用內建函式找出最大值
}
double GetAvg(int arr[ArraySize],int _size)//回傳平均值 傳入陣列、成績總次數
{
    double total=0;
    for(int i=1; i<=_size; i++)
    {
        total+=arr[i-1];
    }
    return total/_size;
}
int main()
{
    int grades[ArraySize]= {0};
    int _size=1,input;//input用來判定輸入的數 在存進陣列中
    cout << "Please enter students grades.\n";
    cout << "Maximum number of grades is 20.\n";
    cout << "You can also enter '-1' to finish enter grades.\n";
    for(int i=1; i<=ArraySize; i++)//使輸入次數限制在20次內
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> input;
            if ((input<0 || input>100) && input!=-1)
                cout << "Out of range!\n";
            else if(input==-1)
                break;
            else
                grades[i-1]=input;
        }
        while(input<0 || input>100);//判斷input的數
        _size=i;
        if(input==-1)
        {
            _size=i-1;//輸入-1的那個input不算再_size裡
            break;
        }
    }
    cout << "The total number of grades is " << _size << "." << "\n";
    cout << "The maximum of grades is " << GetMax(grades,_size) << "." << "\n";
    cout << "The minimum of grades is " << GetMin(grades,_size) << "." << "\n";
    cout << "The average of grades is " << GetAvg(grades,_size) << "." << "\n";
    return 0;
}
