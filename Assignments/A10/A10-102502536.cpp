#include<iostream>
#define ArraySize 20

using namespace std;

int GetMax(int grades[ArraySize],int s);//回傳最大值 傳入陣列、成績總次數

int GetMin(int grades[ArraySize],int s);//回傳最小值 傳入陣列、成績總次數

double GetAvg(int grades[ArraySize],int s);//回傳平均值 傳入陣列、成績總次數

int main()
{
    int grades[ArraySize];//宣告成績陣列

    for(int i=0;i<ArraySize;i++)//初始值-1
    {
        grades[i]=-1;
    }

    cout << "Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades." << endl;//輸出字串

    for(int i=0;i<ArraySize;i++)//輸入成績到陣列
    {
        cout << "Enter grade?: ";
        cin  >> grades[i];
        while((grades[i]<0 || grades[i]>100) && grades[i]!=-1)
        {
            cout << "Out of range!" << endl << "Enter grade?: ";
            cin  >> grades[i];
        }
        if(grades[i]==-1)
            break;
    }

    int n=0;

    for(int i=0;i<ArraySize;i++)//求輸入項數
    {
        if(grades[i]!=-1)
        n=i+1;
    }

    cout << "The total number of grades is " << n << "." << endl;//輸出字串
    cout << "The maximum of grades is " << GetMax(grades,n) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,n) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,n) << "." << endl;

    return 0;
}

int GetMax(int grades[ArraySize],int s)//回傳最大值 傳入陣列、成績總次數
{
    int a=0;

    for(int i=0;i<s;i++)
    {
        if(grades[i]>=a)
            a=grades[i];
    }

    return a;
}

int GetMin(int grades[ArraySize],int s)//回傳最小值 傳入陣列、成績總次數
{
    int a=100;

    for(int i=0;i<s;i++)
    {
        if(grades[i]<=a)
            a=grades[i];
    }

    return a;
}

double GetAvg(int grades[ArraySize],int s)//回傳平均值 傳入陣列、成績總次數
{
    double Avg=0;
    double sum=0;

    for(int i=0;i<s;i++)
        sum+=grades[i];

    Avg=sum/s;

    return Avg;
}
