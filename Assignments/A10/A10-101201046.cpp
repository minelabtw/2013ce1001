#include <iostream>

/* NOT reinvent the wheel*/
/*for min max function*/
#include <algorithm>

/*for accumulate function*/
#include <numeric>

using namespace std;

#define ArraySize 20


int GetMax(int arr[ArraySize],int _size) {
    return *max_element(arr, arr + _size);
    }

int GetMin(int arr[ArraySize],int _size) {
    return *min_element(arr, arr + _size);
    }

double GetAvg(int arr[ArraySize],int _size) {
    return (double)accumulate(arr, arr + _size, 0) / _size;
    }

int main(void) {
    int grades[ArraySize] = {0}, tmp;
    int sz = 0;

    cout << "Please enter students grades.\n"
            "Maximum number of grades is 20.\n"
            "You can also enter '-1' to finish enter grades.\n";

    do {
        cout << "Enter grade?: ";
        cin >> tmp; // input the grade

        /*grade check*/
        if(tmp > 100 || tmp < -1)
            cout << "Out of range!\n";
        else if (tmp + 1)
            grades[sz++] = tmp;

        } while(sz < 20 && (tmp + 1));

    /* OUTPUT */
    cout << "The total number of grades is "
         << sz                 << ".\n"
         << "The maximum of grades is "
         << GetMax(grades, sz) << ".\n"
         << "The minimum of grades is "
         << GetMin(grades, sz) << ".\n"
         << "The average of grades is "
         << GetAvg(grades, sz) << ".\n";

    return 0;
    }
