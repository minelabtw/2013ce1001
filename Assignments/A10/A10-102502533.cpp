#include<iostream>
using namespace std;

const int ArraySize=20;
int GetMax(int arr[ArraySize],int );//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int );//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int );//回傳平均值 傳入陣列、成績總次數

int main()
{

    int arr[ArraySize]= {}; //建一個陣列存成績
    int i=0;//使成績一直跑
    int grade=0;//輸成績的變數


    cout<<"Please enter students grades."<<"\nMaximum number of grades is 20."
        <<"\nYou can also enter '-1' to finish enter grades.\n";//顯示輸入範圍與限制

    while(grade!=-1)//當輸入等於-1不能輸入
    {
            cout<<"Enter grade?: ";//要求輸入成績
            cin>>grade;
            if(grade==-1)
            {
                break;
            }
            if(grade<0 or grade>100 )//輸入超過範圍要求重新輸入
            {
                cout<<"Out of range!\n";
            }

            else if(grade<=100 &&grade>=0)//輸一次重機就進陣列一次
            {
                arr[i] = grade;
                i++;
            }
            if(i==20)//要求輸入20次
            {
                break;
            }

    }
    cout<<"The total number of grades is "<<i<<".";//顯示正確成績輸入幾次
    cout<<"\nThe maximum of grades is "<<GetMax( arr,i)<<".";//導入函式算出最大值
    cout<<"\nThe minimum of grades is "<<GetMin( arr,i)<<".";//導入函式算出最小值
    cout<<"\nThe average of grades is "<< GetAvg( arr, i)/i<<".";//導入函式算出平均值

}
int GetMax(int arr[ArraySize],int i )//回傳最大值 傳入陣列、成績總次數
{
    int a;
    for(int next=1; next<i; next++)
    {
        a= arr[next];
        int movement=next;

        while((movement>0) && (arr[movement-1])>a)
        {
            arr[movement] =arr[movement-1];
            movement--;
        }
        arr[movement] =a;
    }
    return arr[i-1];
}
int GetMin(int arr[ArraySize],int i)//回傳最小值 傳入陣列、成績總次數
{
    int a;
    for(int next=1; next<i; next++)
    {
        a= arr[next];
        int movement=next;

        while((movement>0) && (arr[movement-1])>a)
        {
            arr[movement] =arr[movement-1];
            movement--;
        }
        arr[movement] =a;
    }
    return arr[0];
}
double GetAvg(int arr[ArraySize],int i)//回傳平均值 傳入陣列、成績總次數
{
    int b=0;
    for(int j=0; j<i; j++)
    {
        b+=arr[j];
    }
    return b;
}
