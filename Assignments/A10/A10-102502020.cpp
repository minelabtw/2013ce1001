#include <iostream>
#define ArraySize 20            //指定陣列大小為20

using namespace std;

int GetMax(int arr[ArraySize],int _size);        //回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);        //回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);     //回傳平均值 傳入陣列、成績總次數

int main()
{
    int score=0;                                                           //宣告變數
    int _size=0;
    int grades[ArraySize]= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};      //宣告陣列

    cout << "Please enter students grades." << endl                        //輸入字串
         << "Maximum number of grades is 20." << endl
         << "You can also enter '-1' to finish enter grades." << endl;
    do
    {
        cout << "Enter grade?: ";                                          //輸入至多20個成績
        cin >> score;
        if(score==-1)                                                      //當輸入-1時 跳出迴圈
            break;
        else if(score<0 or score>100)
        {
            cout << "Out of range!" << endl;
        }
        else
        {
            grades[_size]=score;                                           //把成績存在陣列裡面
            _size++;
        }
    }
    while(_size<20);
    cout << "The total number of grades is " << _size << "." << endl
         << "The maximum of grades is " << GetMax(grades,_size) << "." << endl   //呼叫函式
         << "The minimum of grades is " << GetMin(grades,_size) << "." << endl
         << "The average of grades is " << GetAvg(grades,_size) << "." << endl;

    return 0;
}
int GetMax(int arr[],int _size)      //找尋最大值
{
    int maximum=arr[0];
    for(int i=0; i<_size; i++)
    {
        if(maximum<arr[i])
            maximum=arr[i];
    }
    return maximum;
}
int GetMin(int arr[],int _size)      //找尋最小值
{
    int minimum=arr[0];
    for(int j=0; j<_size; j++)
    {
        if(minimum>arr[j])
            minimum=arr[j];
    }
    return minimum;
}
double GetAvg(int arr[],int _size)   //計算算術平均數
{
    double average=0;
    double total=0;
    for(int k=0; k<_size; k++)
    {
        total=total+arr[k];
    }
    average=total/_size;
    return average;
}
