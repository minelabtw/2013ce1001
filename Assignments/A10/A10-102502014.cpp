#include<iostream>

using namespace std;
#define ArraySize 20
int GetMax(int grades[ArraySize],int counter)
{
  int max=grades[0];
      for(int i=1;i<counter;i++)
      {
          if(max<grades[i])
            max=grades[i];
      }
      return max;
}
int GetMin(int grades[ArraySize],int counter)
{
   int min=grades[0];

   for(int i=1;i<counter;i++)
   {
       if(min>grades[i])
        min=grades[i];
   }
   return min;
}
double GetAvg(int grades[ArraySize],int counter)
{
    double sum=0;
    double av=0;
    for(int i=0;i<counter;i++)
    {
        sum=sum+grades[i];
    }
    av=sum/counter;
    return av;
}
int main()
{
    int a;
    int grades[ArraySize];
    int counter=-1;
    int counter2;

    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;

    while(counter<20 && a!=-1)
    {
        cout<<"Enter grade?: ";
        cin>>a;
        while(a<-1 || a>100)
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter grade?: ";
            cin>>a;
        }
        counter++;
        if(a!=-1)
        grades[counter]=a;
        if(counter==19)
            counter++;
    }
    cout<<"The total number of grades is "<<counter<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,counter)<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,counter)<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,counter)<<endl;
    return 0;
}
