#include<iostream>
#include<math.h>
#define ArraySize 20
using namespace std;

int GetMax(int arr[ArraySize],int _size);   //宣告三個函式
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
int main()
{
    int _size = 0;      //宣告一個變數，初始值為零
    int grades[ArraySize];  //宣告一個陣列

    cout<<"Please enter students grades."<<endl<<"Maximum number of grades is 20."<<endl<<"You can also enter '-1' to finish enter grades."<<endl;

    for ( _size = 0; _size < 20; _size++ )      //條件當變數等於零，變數需小於20
    {
        cout<<"Enter grade?: ";
        cin>>grades[_size];
        while ( 100 < grades[_size] )          //條件陣列需大於100
        {
            cout<<"Out of range!"<<endl<<"Enter grade?: ";
            cin>>grades[_size];
        }
        if ( grades[_size] == -1 )          //當陣列等於-1時，跳出視窗
            break;

        while ( grades[_size] < 0 )        //當陣列小於零時
        {
            cout<<"Out of range!"<<endl<<"Enter grade?: ";
            cin>>grades[_size];
        }

    }
    cout<<"The total number of grades is "<< _size <<endl;
    cout<<"The maximum of grades is "<< GetMax( grades, _size) << "." << endl;
    cout<<"The minimum of grades is "<< GetMin( grades, _size) << "." <<endl;
    cout<<"The average of grades is "<< GetAvg( grades, _size) << "." <<endl;


    return 0 ;
}

int GetMax( int arr[ArraySize],int _size )      //宣告最大值
{
    int number;                                 //宣告一個變數
    for (int a = 0; a < _size; a++ )            //當a等於零，a小於最初設定的變數
    {
        if ( arr[a] > number )                 //當陣列a小於變數時
            number = arr[a];
    }
    return number;
}
int GetMin(int arr[ArraySize],int _size)       //宣告最小值
{
    int number = 100;
    for (int b = 0; b < _size; b++ )          //當b等於零，b大於最初設定的變數
    {
        if ( arr[b] < number )                //當陣列a小於變數時
            number = arr[b];
    }
    return number;
}
double GetAvg(int arr[ArraySize],int _size)    //宣告平均值
{
    double number;
    for ( int c = 0; c < _size; c++ )           //當c等於零，c小於最初設定的變數
        number += arr[c];                       //最初變數加陣列中第c個變數
    return number / _size;
}


