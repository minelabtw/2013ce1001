#include <iostream>
using namespace std;
#define ArraySize 20

int GetMax(int arr[ArraySize],int _size);
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);

int main()
{
    int grade=-2;                                    //宣告整數變數成績初始值為-2,才可進入迴圈
    int arr[ArraySize];                              //宣告陣列
    int _size=0;                                     //宣告陣列初始大小為0
    int sum=0;                                       //宣告總和初始值為0

    cout<<"Please enter students grades."<<endl;     //輸出字串
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;

    for(int i=0; i<ArraySize; i++)                  //重複執行20次
    {
        arr[i]=-1;                                  //arr初始值為-1

        while(grade!=-1)                            //判斷輸入值是否為-1
        {
            if(i==20)                               //若輸入為第21項即跳出迴圈
                break;
            do
            {
                cout<<"Enter grade?: ";
                cin>>grade;
                while(grade!=-1 and grade<0 or grade>100 ) //判斷是否超出範圍
                {
                    cout<<"Out of range!"<<endl;
                    cout<<"Enter grade?: ";
                    cin>>grade;
                }
                if(grade==-1)                       //若輸入值為-1即跳出迴圈
                    break;

                arr[i]=grade;                       //若成績為合理輸入值即存進陣列中
                i++;
                if(arr[i]!=-1)
                {
                    _size=i;                        //陣列大小為i項
                }
            }
            while(grade<0 or grade>100);
        }
    }

    cout<<"The total number of grades is "<<_size<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(arr,_size)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(arr,_size)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(arr,_size)<<"."<<endl;

    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int max=0;
    for(int i=0; i<_size; i++)
    {
        if(arr[i]>max)                          //若arr[i]大於max即覆蓋        {
        {
            max=arr[i];
        }
    }
    return max;
}

int GetMin(int arr[ArraySize],int _size)
{
    int min=100;
    for(int i=0; i<_size; i++)
    {
        if(arr[i]<=min)                         //若arr[i]小於min即覆蓋
        {
            min=arr[i];
        }
    }
    return min;
}

double GetAvg(int arr[ArraySize],int _size)
{
    double total=0;
    double avg=0;
    for(int i=0; i<_size; i++)
    {
        total=arr[i]+total;                    //總和為所有成績相加
        avg=(total/_size);                     //平均為總和除以項數
    }
    return avg;
}
