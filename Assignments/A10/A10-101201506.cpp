#include <iostream>
#define ArraySize 20
using namespace std;

int pickMin(int grades[], int _size)//回傳最大值 傳入陣列、成績總次數
{
    int Min=grades[0];
    for (int i=1; i<_size; i++)
    {
        if (grades[i]<Min)
            Min=grades[i];
    }
    return Min;
}

int pickMax(int grades[], int _size)//回傳最小值 傳入陣列、成績總次數
{
    int Max=grades[0];
    for(int i=1; i<_size; i++)
    {
        if(grades[i]>Max)
            Max=grades[i];
    }
    return Max;
}

double Avg(int grades[], int _size)//回傳平均值 傳入陣列、成績總次數
{
    double sum=0;
        for(int i=0; i<_size; i++)
        sum+=grades[i];

    return sum/_size;
}

int main()
{
    int grades[ArraySize] ; //陣列
    int n=0; //輸入次數
    int x=0;

    cout <<"Please enter students grades."<<endl
         <<"Maximum number of grades is 20."<<endl
         <<"You can also enter '-1' to finish enter grades."<<endl;

 for( int i=0; i<ArraySize; i++ )
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>x;

            if( x==-1 )
                break;

            if( x<0 || x>100)
                cout<<"Out of range!"<<endl;
        }
        while(x<0 || x>100);

        if(x==-1)
            break;

        grades[i]=x;
        n=i+1;
    }

    cout<<"The total number of grades is "<< n <<"."<<endl;
    cout<<"The maximum of grades is " <<pickMax(grades, n) <<"."<<endl;
    cout<<"The minimum of grades is " <<pickMin(grades, n) <<"."<<endl;
    cout<<"The average of grades is " <<Avg(grades, n)<<"."<<endl;

    return 0;
}





