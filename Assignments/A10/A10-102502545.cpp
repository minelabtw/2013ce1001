#include<iostream>
#include<stdlib.h>
#define ArraySize 20
using namespace std;

int GetMax(int arr[ArraySize],int Size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int Size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int Size);//回傳平均值 傳入陣列、成績總次數
int grades[ArraySize];//宣告成績陣列

int GetMax(int arr[ArraySize],int Size)//尋找最大值函式
{
    int Maximum=0;
    for(int i; i<Size; i++)
    {
        if(grades[i]>Maximum)
            Maximum=grades[i];
    }
    return Maximum;
}
int GetMin(int arr[ArraySize],int Size)//尋找最小值函式
{
    int Minimum=100;
    for(int i; i<Size; i++)
    {
        if(grades[i]<Minimum)
            Minimum=grades[i];
    }
    return Minimum;
}
double GetAvg(int arr[ArraySize],int Size)//尋找平均值函式
{
    double All=0,Avg;
    for(int i=0;i<Size;i++)
    {
        All +=grades[i];
    }
    Avg=All/Size;
    return Avg;
}


int main()
{
    int number,total;//宣告函數
    cout<<"Please enter students grades.\n";
    cout<<"Maximum number of grades is 20.\n";
    cout<<"You can also enter '-1' to finish enter grades.\n";

    for(int n=0; n<ArraySize; n++)//宣告迴圈令其記錄在陣列grades裡
    {
        do//宣告迴圈使得限制範圍
        {
            cout<<"Enter grade?: ";
            cin>>number;
            if(number==-1)
                break;
            if(number<0||number>100)
                cout<<"Out of range!!\n";

        }
        while(number<0||number>100);

        if(number==-1)
            break;

        grades[n]=number;
        total=n+1;

    }
    cout<<"The total number of grades is "<<total<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,total)<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,total)<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,total)<<endl;

    return 0;
}


