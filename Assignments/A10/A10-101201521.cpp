#include <iostream>
#define ArraySize 20
using namespace std;

int GetMax(int a[], int i)//找最大值
{
    int mymax=a[0];
    for(int j=1; j<i; j++)
        if(mymax<a[j])
            mymax=a[j];
    return mymax;
}
int GetMin(int a[], int i)//找最小值
{
    int mymin=a[0];
    for(int j=1; j<i; j++)
        if(mymin>a[j])
            mymin=a[j];
    return mymin;
}
double GetAvg(int a[], int i)//算平均值
{
    double myavg=0;
    for(int j=0; j<i; j++)
        myavg+=a[j];
    return myavg/i;
}
int main()
{
    int grade[ArraySize]={}, i=0;//grade儲存成績、i是成績個數
    cout << "Please enter students grades.\n"
            << "Maximum number of grades is 20.\n"
            << "You can also enter '-1' to finish enter grades.\n";
    for(i=0; i<20; i++)//預設20個成績
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> grade[i];
        }
        while((grade[i]>100 || grade[i]<0) && grade[i]!=-1 && (cout << "Out of range!\n"));
        if(grade[i]==-1)//輸入-1則挑出for
            break;
    }
    cout << "The total number of grades is " << i;
    cout << ".\nThe maximum of grades is " << GetMax(grade, i);
    cout << ".\nThe minimum of grades is " << GetMin(grade, i);
    cout << ".\nThe average of grades is " << GetAvg(grade, i);
    cout << ".\n";

    return 0;
}
