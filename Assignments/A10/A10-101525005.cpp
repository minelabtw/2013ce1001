#include <iostream>

using namespace std;

int GetMin(int gradeArray[],int gradeCount);
int GetMax(int gradeArray[],int gradeCount);
double GetAve(int gradeArray[],int gradeCount);

int main()
{
    int grade=0,gradeIndex=0,gradeSize=0;
    int gradeArray[20];

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
//判斷輸入值
    do
    {

        cout << "Enter grade?: " ;
        cin>> grade;
        if(grade<-1 || grade>100)
        {
            cout << "Out of range!" << endl;
        }
        else if(grade==-1 )
        {
            break;
        }
        else
        {
            gradeSize++;

            gradeArray[gradeIndex]=grade;
            gradeIndex++;

        }
    }
    while (100>=grade>=0 &&
            gradeSize<=19);
    //印出所有的最大最小
    cout << "The total number of grades is " << gradeSize << "." << endl;
    cout << "The maximum of grades is " << GetMax(gradeArray,gradeSize)<< "."  << endl;
    cout << "The minimum of grades is " << GetMin(gradeArray,gradeSize) << "." <<endl;
    cout << "The average of grades is " << GetAve(gradeArray,gradeSize)<< "."  <<endl;

    return 0;
}

int GetMax(int gradeArray[],int gradeCount)
{
    double max=0;
    max=gradeArray[0];
    for(int i=0; i<gradeCount; i++)
    {
        if(max<gradeArray[i])
        {
            max=gradeArray[i];
        }
    }

    return max;
}
int GetMin(int gradeArray[],int gradeCount)
{
    double min=0;
    min=gradeArray[0];
    for(int i=0; i<gradeCount; i++)
    {
        if(min>gradeArray[i])
        {
            min=gradeArray[i];
        }
    }

    return min;
}
double GetAve(int gradeArray[],int gradeCount)
{
    double ave=0,total=0;
    for(int i=0; i<gradeCount; i++)
    {
        total+= gradeArray[i];
    }
    ave=total/gradeCount;
    return ave;
}
