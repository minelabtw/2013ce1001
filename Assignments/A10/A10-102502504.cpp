#include <iostream>
#define ArraySize 20

using namespace std;

int GetMax(int arr[ArraySize],int _size);    //回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);    //回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size); //回傳平均值 傳入陣列、成績總次數

int main()
{
    int a;                 //輸入的分數
    int b=0;               //用來計算輸入的次數
    int grades[ArraySize]; //宣告成績陣列

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    for(int x=1; x<=20; x++) //用for迴圈去限制合格成績可輸入的次數為20次
    {
        cout << "Enter grade?: ";
        cin >> a;
        if(a==-1) //如果輸入-1，則跳出for迴圈→停止輸入
        {
            break;
        }
        while(a<-1||a>100) //若輸入錯誤範圍的成績，則顯示錯誤訊息，並要求重新輸入，直到合格範圍為止
        {
            cout << "Out of range!" << endl;
            cout << "Enter grade?: ";
            cin >> a;
        }
        grades[b]=a; //將合格範圍內的成績存入陣列
        b++; //輸入的總次數加一
    }

    b--; //將總次數減一，以減去上面for迴圈最下方之b++所多出之次數

    cout << "The total number of grades is "<<b+1<<"."<<endl;
    cout << "The maximum of grades is "<<GetMax(grades,b)<<"."<<endl;
    cout << "The minimum of grades is "<<GetMin(grades,b)<<"."<<endl;
    cout << "The average of grades is "<<GetAvg(grades,b)<<"."<<endl;

    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int max=arr[0];  //設最大值之初始值為第一個正確範圍內的成績
    for(int x=1; x<=_size; x++) //利用for迴圈，讓max去跟第一個以後的成績比較大小
    {
        if(max<arr[x]) //若max小於後者，則把後者數值取代原本max之值
            max=arr[x];
    }
    return max; //回傳最大值
}

int GetMin(int arr[ArraySize],int _size)
{
    int min=arr[0];  //設最小值之初始值為第一個正確範圍內的成績
    for(int x=1; x<=_size; x++) //利用for迴圈，讓min去跟第一個以後的成績比較大小
    {
        if(min>arr[x]) //若min大於後者，則把後者數值取代原本min之值
            min=arr[x];
    }
    return min; //回傳最小值
}

double GetAvg(int arr[ArraySize],int _size)
{
    double sum=0; //設總和之初始值為零
    for(int x=0; x<=_size; x++) //利用for迴圈加總所有範圍內之成績
    {
        sum=sum+arr[x];
    }
    return (sum/(_size+1)); //將總和平均並回傳平均值
}
