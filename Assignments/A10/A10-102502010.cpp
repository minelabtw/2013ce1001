#include <iostream>
using namespace std;
#define ArraySize 20

int GetMax(int arr[],int _size)  //回傳最大值 傳入陣列、成績總次數
{
    int i,maximum=0;
    for(i=0; i<_size; i++)
    {
        if(maximum<arr[i])
            maximum=arr[i];
    }
    return maximum;
}

int GetMin(int arr[],int _size)  //回傳最小值 傳入陣列、成績總次數
{
    int i,minimum=100;
    for(i=0; i<_size; i++)
    {
        if(minimum>arr[i])
            minimum=arr[i];
    }
    return minimum;
}

double GetAvg(int arr[],int _size)  //回傳平均值 傳入陣列、成績總次數
{
    int i;
    double sum=0;
    for(i=0; i<_size; i++)
    {
        sum+=arr[i];
    }
    return sum/_size;
}

int main()
{
    int i,grades[ArraySize]; //宣告成績陣列
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    for(i=0; i<20; i++)
    {
        cout<<"Enter grade?: ";
        cin>>grades[i];
        if(grades[i]==-1)break;  //輸入數值為-1時跳出
        while(grades[i]<0 || grades[i]>100)  //判斷數值是否在範圍內
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter grade?: ";
            cin>>grades[i];
            if(grades[i]==-1)break;  //輸入數值為-1時跳出
        }
        if(grades[i]==-1)break;  //輸入數值為-1時跳出
    }
    cout<<"The total number of grades is "<<i<<'.'<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,i)<<'.'<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,i)<<'.'<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,i)<<'.'<<endl;
    return 0;
}
