#include<iostream>
using namespace std;
#define ArraySize 20 //指定陣列大小為20
int grades[ArraySize];//宣告成績陣列
int GetMax(int arr[ArraySize],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int Max=arr[0];
    for(int j=0; j<_size; j++)
    {
        if(Max<arr[j])
            Max=arr[j];
    }
    return Max;
}
int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int Min=arr[0];
    for(int k=0; k<_size; k++)
    {
        if(Min>arr[k])
            Min=arr[k];
    }
    return Min;
}
double GetAvg(int arr[ArraySize],int _size)//回傳平均值 傳入陣列、成績總次數
{
    double Sum=0;
    for(int q=0; q<_size; q++)
    {
        Sum=Sum+arr[q];
    }
    return Sum/_size;
}
int main()
{
    int num=0;
    int i=0;
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    for(i=0; i<20; i++)
    {
        do
        {
            cout << "Enter grade?: ";
            cin >> num;

            if( num == -1 )
                break;
            if( num < 0 || num > 100 )
                cout << "Out of range!\n";
        }
        while( num < 0 || num > 100);

        if( num == -1 )break;

        grades[ i ] = num;
    }
    cout<<"The total number of grades is "<<i<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades, i)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades, i)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(grades, i)<<".";
    return 0;
}
