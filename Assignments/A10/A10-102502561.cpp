#include<iostream>
#define Arraynumber 20;
using namespace std;
int GetMax(int arr[],int);      //prototype
int GetMin(int arr[],int);      //prototype
double GetAvg(int arr[],int);   //prototype

int main()
{

    int grade[20]= {0}; //陣列
    int i=0;
    int sum=0;
    cout << "Please enter students grades.\n";
    cout << "Maximum number of grades is 20.\n";
    cout << "You can also enter '-1' to finish enter grades.\n";    //題目
    for(int j=0; j<20; j++)
    {
        cout << "Enter grade?: ";
        cin >> grade[i];
        if(grade[i]==-1)break;
        while(grade[i]<0||grade[i]>100) //符合條件
        {
            cout << "Out of range!\nEnter grade?: ";
            cin >> grade[i];
        }
        i++;
        GetMax(grade,i);    //執行prototype
        GetMin(grade,i);
        GetAvg(grade,i);
    }
    cout<< "The total number of grades is "<< i << "\n";
    int Max=GetMax(grade,i);
    cout << "The maximum of grades is " << Max << ".\n";
    int Min=GetMin(grade,i);
    cout << "The minimum of grades is " << Min << ".\n";
    double Avg=GetAvg(grade,i);
    cout << "The average of grades is " << Avg << ".\n"; //輸出解答


    return 0;
}

int GetMax(int arr[],int i)
{
    int Max=0;
    for(int x=0; x<i; x++)
    {
        if(Max<arr[x])
        {
            Max=arr[x];
        }
    }
    return Max;
}
int GetMin(int arr[],int i)
{
 int Min=0;
    for(int x=0; x<i; x++)
    {
        if(Min>arr[x])
        {
            Min=arr[x];
        }
    }
    return Min;
}
double GetAvg(int arr[],int i)
{
    int sum=0;             //宣告變數
    for(int x=0; x<i; x++)    //for迴圈
    {
        sum=sum+arr[x];
    }
    double average= (double)sum/i;  //平均
    return average;
}

