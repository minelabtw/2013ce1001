#include <iostream>
#define ArraySize 20
using namespace std;

void EnterGrade(int grade[], int _size, int &gradecounter);//傳入記錄成績的陣列，以及陣列大小，還有紀錄成績總次數的變數。
int GetMax(int grade[],int _size);//回傳最大值 傳入陣列、陣列大小。
int GetMin(int grade[],int _size);//回傳最小值 傳入陣列、陣列大小。
double GetAvg(int grade[],int _size);//回傳平均值 傳入陣列、陣列大小。

int main()
{
    int gradecounter=1;                                     //宣告gradecounter，記錄最後有幾個成績。
    int grade[ArraySize]= {};                               //宣告紀錄成績的陣列。

    cout << "Please enter students grades." << endl ;       //輸出字串。
    cout << "Maximum number of grades is 20." << endl ;
    cout << "You can also enter '-1' to finish enter grades." << endl ;

    EnterGrade(grade, ArraySize, gradecounter);             //呼叫EnterGrade。
    //for (int i=1; i<=gradecounter; i++)                   debug用的for迴圈，會顯示每次輸入的成績。
    //{
    //    cout << i<< " - " << grade[i] << endl;
    //}

    cout << "The total number of grades is " << gradecounter << "." << endl;        //輸出題目要求的答案。
    cout << "The maximum of grades is " << GetMax(grade, gradecounter) << "." << endl ;
    cout << "The minimum of grades is " << GetMin(grade, gradecounter) << "." << endl ;
    cout << "The average of grades is " << GetAvg(grade, gradecounter) << "." << endl ;
    return 0;
}

void EnterGrade(int grade[], int ArraySizeint ,int &gradecounter)
{
    int status = 0;                                         //利用status判斷是否要繼續輸入。
    while (status != -1 && gradecounter <= 20 )
    {
        cout << "Enter grade?: ";
        cin >> status;
        if (status != -1)
        {
            while(status < 0 || status > 100)
            {
                cout << "Out of range!" << endl;
                cout << "Enter grade?: ";
                cin >> status;
            }
            grade[gradecounter] = status;
            gradecounter = gradecounter + 1;
        }
    }
    gradecounter = gradecounter - 1;  //最後一次記得要把多加的gradecounter減回來。
}

int GetMax(int grade[] , int gradecounter)
{
    int Max = grade[1];                     //宣告一個Max變數來儲存最大值
    for (int i=1; i<=gradecounter; i++)
    {
        if(grade[i]>Max)
        {
            Max = grade[i];
        }
    }
    return Max;
}

int GetMin(int grade[] , int gradecounter)
{
    int Min = grade[1];                   //宣告一個Min變數來儲存最小值。
    for (int i=1; i<=gradecounter; i++)
    {
        if(grade[i] < Min)
        {
            Min = grade[i];
        }
    }
    return Min;
}

double GetAvg(int grade[] , int gradecounter)
{
    double Avg = 0;             //宣告一個Avg變數來儲存平均值。
    if(gradecounter == 0)       //假如沒有輸入成績的話，平均值應該是零。
    {
        Avg = 0;
    }
    else
    {
        for (int i=1; i<=gradecounter; i++)
        {
            Avg = Avg + grade[i];
        }
        Avg = Avg/gradecounter;
    }
    return Avg;
}
