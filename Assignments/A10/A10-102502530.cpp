#include<iostream>
#define ArraySize 20

int GetMax(int arr[ArraySize],int _size);    //prototype
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);

int main()
{
   int grades[ArraySize],i;   //declaration
   
   std::cout<<"Please enter students grades.\n"    //start
            <<"Maximum number of grades is 20.\n"
            <<"You can also enter '-1' to finish enter grades.\n";

   for(i=0;i!=ArraySize;i++)  //input
   {
      std::cout<<"Enter grade?: ";
      std::cin>>grades[i];
      if(grades[i]==-1)
         break;
      if(grades[i]<-1||grades[i]>100)
      {
         std::cout<<"Out of range!\n";
         i--;
      }
   }

   std::cout<<"The total number of grades is "<<i<<".\n"    //output
            <<"The maximum of grades is "<<GetMax(grades,i)<<".\n"
            <<"The minimum of grades is "<<GetMin(grades,i)<<".\n"
            <<"The average of grades is "<<GetAvg(grades,i)<<'.';

   return 0;
}

int GetMax(int arr[ArraySize],int _size)  //get maximum
{
   int max=0;
   for(int i=0;i!=_size;i++)
      max=max>arr[i]?max:arr[i];
   return max;
}

int GetMin(int arr[ArraySize],int _size)  //get minimum
{
   int min=100;
   for(int i=0;i!=_size;i++)
      min=min<arr[i]?min:arr[i];
   return min;
}
double GetAvg(int arr[ArraySize],int _size)  //get average
{
   double sum=0;
   for(int i=0;i!=_size;i++)
      sum+=arr[i];
   return sum/_size;
}
