#include<iostream>
#define ArraySize 20
using namespace std;
int gma(int [], int);                           //Line 4-6:declare the function
int gmi(int [], int);
double gav(int [], int);
int main()
{
    int cal=0;                                  //Line 9-11:declare the array and varibal
    const int arraysize=20;
    int grades[arraysize]= {};
    cout<<"Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades.\n";
    for(int i=0; i<20; i++)                 //Line 13-34:let user enter the grades
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grades[i];
            if((grades[i]<0&&grades[i]!=-1)||grades[i]>100)
                cout<<"Out of range!\n";
            else if(grades[i]==-1)
            {
                cout<<endl;
                break;
            }
            else
                cal++;
        }
        while(grades[i]<0||grades[i]>100);
        if(grades[cal]==-1)
        {
            grades[i]=0;
            break;
        }
    }
    cout<<"The total number of grades is "<<cal<<".\n";                 //Line 35-39:print the number of the grades and max, min, average value
    cout<<"The maximum of grades is "<<gma(grades,cal)<<".\n";
    cout<<"The minimum of grades is "<<gmi(grades,cal)<<".\n";
    cout<<"The average of grades is "<<gav(grades,cal)<<".\n";
    return 0;
}
int gma(int gr[], int n)                                                    //Line 41-74:define the function to get max, min, averge value
{
    int an;
    an=gr[0];
    for(int i=0; i<n; i++)
    {
        if(an<gr[i])
            an=gr[i];
    }
    return an;
}
int gmi(int gr[], int n)
{
    int an;
    an=gr[0];
    for(int i=0; i<n; i++)
    {
        if(an>gr[i])
            an=gr[i];
    }
    return an;
}
double gav(int gr[], int n)
{
    double av=0;
    for(int i=0; i<n; i++)
    {
        av+=gr[i];
    }
    av=av/n;
    return av;
}
