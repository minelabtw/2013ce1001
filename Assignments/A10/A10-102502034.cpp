#include<iostream>
#define ArraySize 20
using namespace std;
int GetMax(int arr[ArraySize],int asize);//宣告函式
int GetMin(int arr[ArraySize],int asize);
double GetAvg(int arr[ArraySize],int asize);
int main()
{
    int grades[ArraySize]= {};//宣告變數
    int i=0;
    cout <<"Please enter students grades."<<endl;//輸入輸出題目所需
    cout <<"Maximum number of grades is 20."<<endl;
    cout <<"You can also enter '-1' to finish enter grades."<<endl;

    while(i<=19)//當i<=19時
    {
        cout <<"Enter grade?: ";//輸出Enter grade?:
        cin  >>grades[i];//輸入grades
        if (grades[i]==-1)//當i!=-1時
        {
            grades[i]=0;
            break;
        }
        while(grades[i]>100 or grades[i]<-1)
        {
            cout <<"Out of range!"<<endl;//輸出Out of range
            cout <<"Enter grade?: ";//輸出Enter grade?:
            cin  >>grades[i];//輸入grades
        }
        i++;
    }
    cout <<"The total number of grades is "<<i<<"."<<endl;
    cout <<"The maximum of grades is "<< GetMax(grades,i)<<"."<<endl;//輸出max
    cout <<"The minimum of grades is "<< GetMin(grades,i)<<"."<<endl;//輸出min
    cout <<"The average of grades is "<< GetAvg(grades,i)<<"."<<endl;//輸出avg
    return 0;
}
int GetMax(int grades[] , int asize)//計算max
{
    int i=0 ,j=0;
    int amax=0;
    while (i!=asize&&j!=asize)//當全部試一遍
    {
        if(grades[j]>grades[i])//存大的
        {
            amax=j ;
            i++ ;
        }
        else
        {
            amax=i ;
            j++ ;
        }
    }
    return grades[amax];
}
int GetMin(int grades[] , int asize)//計算min
{
    int i=0 ,j=0;
    int amax=0;
    while (i!=asize&&j!=asize)//當全部試一遍
    {
        if(grades[j]<grades[i])//存小的
        {
            amax=j ;
            i++ ;
        }
        else
        {
            amax=i ;
            j++ ;
        }
    }
    return grades[amax];
}
double GetAvg(int grades[] , int asize)//計算avg
{
    double sum=0;
    for (int i=0; i<=19; i++)//當全部加一遍
    {
        sum=sum+grades[i];
    }
    return sum/asize;
}
