//============================================================================
// Name        : A10-102502038.cpp
// Author      : catLee
// Version     : 0.1
// Description : NCU CE1001 A10
//============================================================================

#include <iostream>
#define ArraySize 20

using namespace std;
int GetMax(int arr[ArraySize],int _size);
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
void gradePoss(int);
int total = 0,Max = 0,Min = 100,count = 0; //init var
int main(void){
  int grades[ArraySize];
  int temp;
  cout << "Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades.\n"; //remind user input
  for(int i = 0;i<ArraySize && temp != -1;i++){  //main loop
    cout << "Enter grade?: ";
    cin >> temp;
    if(temp < -1 || temp > 100){
      i--;
      cout << "Out of range!\n";
      continue;
    }
    grades[i] = temp;
  }
  for(int i = 0;i < ArraySize && grades[i] != -1;i++){
    gradePoss(grades[i]);  //calculate Max,Min and total
  }
  cout << "The total number of grades is " << count << ".\n";
  cout << "The maximum of grades is " << GetMax(grades,count) << ".\n";
  cout << "The minimum of grades is " << GetMin(grades,count) << ".\n";
  cout << "The average of grades is " << GetAvg(grades,count) << ".\n";
  return 0;
}
void gradePoss(int grade){  //calculate Max,Min and total
  count++;
  total += grade;
  Max = (grade>=Max?grade:Max);
  Min = (grade<=Min?grade:Min);
}
int GetMax(int grades[ArraySize],int _size){  //dummy function
  return Max;
}
int GetMin(int grades[ArraySize],int _size){  //dummy function
  return Min;
}
double GetAvg(int grades[ArraySize],int _size){  //dummy function
  return ((double)total/count);
}
