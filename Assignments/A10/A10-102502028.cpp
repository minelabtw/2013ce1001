#include <iostream>
#define ArraySize 20
using namespace std ;

int GetMax (int arr[ArraySize] , int _size) ;             //function prototype
int GetMin (int arr[ArraySize] , int _size );
double GetAvg (int arr[ArraySize] , int _size) ;

int main ()
{
    int counter = 0 ;            //宣告整數變數並初始值=0
    int grade = 0 ;
    int array [20] = {} ;        //宣告陣列

    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl ;
    cout << "You can also enter '-1' to finish enter grades." << endl ;
    while (counter <= ArraySize) //最多只能有20個成績
    {
        cout << "Enter grade?: " ;
        cin >> grade ;
        while (grade < -1 || grade > 100)        //不合格的成績必須輸出out of range
        {
            cout << "Out of range!" << endl ;
            cout << "Enter grade?: " ;
            cin >> grade ;
        }
        if (grade == -1)                         //輸入-1為停止鍵
        {
            break ;
        }

        array [counter] = grade ;                //儲存成績到陣列裡面
        counter ++ ;

    }
    cout << "The total number of grades is " << counter << "." << endl ;                //作一些運算並輸出
    cout << "The maximum of grades is " << GetMax(array , counter) << "." << endl ;
    cout << "The minimum of grades is " << GetMin(array , counter) << "." << endl ;
    cout << "The average of grades is " << GetAvg(array , counter)<< "." << endl ;

    return 0 ;
}

double GetAvg (int arr[ArraySize] , int _size)             //算平均
{
    double total = 0 ;
    for (int a = 0 ; a < _size ; a ++)
    {
        total = total + arr[a] ;
    }
    return total / _size ;
}

int GetMax (int arr[ArraySize] , int _size)                 //找最大值
{
    int Max = 0 ;
    for (int a = 0 ; a < _size ; a ++)
    {
        if (arr[a] > Max)
        {
            Max = arr[a] ;
        }
    }
    return Max ;
}

int GetMin (int arr[ArraySize] , int _size)                 //找最小值
{
    int Min = 100 ;
    for (int a = 0 ; a < _size ; a ++)
    {
        if (arr[a] < Min)
        {
            Min = arr[a] ;
        }
    }
    return Min ;
}
