#include <iostream>
// int cost ArraySize = 20 ;
#define ArraySize 20

using namespace std;

// 把分數由小至大排列 回傳最大值
int GetMax(int arr[] , int _size)
{
    if ( _size == 0 ) return 0 ;
    int tmp ;
    int k = _size -1  ;

    while ( k!= 0)
    {
        for ( int i = 0 ; i < k ; ++i )
        {
            if ( arr[i] > arr[i+1])
            {
                tmp = arr[i] ;
                arr[i] = arr [i+1] ;
                arr[i+1] = tmp ;
            }
        }
        -- k ;
    }
    return arr[_size - 1 ] ;
}

//回傳最小值
int GetMin(int arr[],int _size)
{
    if (_size == 0) return 0 ;
    int tmp ;
    int k = _size -1  ;

    while ( k!= 0)
    {
        for ( int i = 0 ; i < k ; ++i )
        {
            if ( arr[i] > arr[i+1])
            {
                tmp = arr[i] ;
                arr[i] = arr [i+1] ;
                arr[i+1] = tmp ;
            }
        }
        -- k ;
    }
    return arr[0] ;

}

// 回傳平均值
double GetAvg(int arr[],int _size)
{

    if (_size == 0) return 0 ;
    double sum = 0 ;

    for ( int i = 0 ; i < _size ; ++i )
    {
        sum += arr[i] ;
    }

    return  static_cast<double>(sum /_size) ;
}


int main()
{
    int g , M , m  , times = 0;
    int grades[ArraySize] ;

    cout << "Please enter students grades. " << endl ;
    cout << "Maximum number of grades is 20." << endl ;
    cout << "You can also enter '-1' to finish enter grades." << endl ;

    for ( int i = 0 ; i < 20 ; ++i )
    {
        cout << "Enter grade?: ";
        cin >> g ;

        if( g == -1 ) break ;

        while ( g < 0 || g >100 )
        {
            cout << "Out of range!" << endl ;
            cout << "Enter grade?: ";
            cin >> g ;
        }

        grades[i] = g  ;
        ++times ;
    }


    cout << "The total number of grades is " << times <<"." << endl;
    cout << "The maximum of grades is "<< GetMax(grades,times)<<"." << endl ;
    cout << "The minimum of grades is "<< GetMin(grades,times)<<"." << endl ;
    cout << "The average of grades is "<< GetAvg(grades,times)<<"." << endl ;

    return 0 ;
}
