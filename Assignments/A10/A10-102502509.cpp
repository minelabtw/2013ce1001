#include <iostream>
#include <iomanip>
using namespace std;

int GetMax(int [], int);
int GetMin(int [], int);
double GetAvg(int [], int);

int main()
{

    int grade;
    int total;
    const int n = 20; // 固定次數
    int ArraySize = 0; // 記得初始值
    int grades[n];


    cout << "Please enter students grades.\n"
         << "Maximum number of grades is 20.\n"
         << "You can also enter '-1' to finish enter grades.\n";

    for (int i = 0; i < n; i++) // 用count 限制次數
    {

        cout << "Enter grade?: ";
        cin >> grade;

        if (grade == -1)
            break;

        while ( grade < 0 || grade > 100)
        {
            cout << "Out of range!\n";
            cout << "Enter grade?: ";
            cin >> grade;
            if (grade == -1 )
                break; // 跳出while
        }

        if (grade == -1 )
            break; // 跳出for
        grades[ArraySize] = grade;
        ArraySize++; // 持續儲存
    }
    cout << "The total number of grade is " << ArraySize << ".\n";
    cout << "The maximum of grades is " << GetMax(grades,ArraySize) << ". \n";
    cout << "The minimum of grades is " << GetMin(grades,ArraySize) << ".\n";
    cout << "The average of grades is " << GetAvg(grades,ArraySize) << ".\n";

    return 0;
}

int GetMax(int arr[], int _size)
{
    int max = arr[0];
    for (int a = 0; a < _size; a++)
    {
        if (arr[a] > max)
            max = arr[a];
    }
    return max; // 回傳最大值
}
int GetMin(int arr[], int _size)
{
    int min = arr[0];

    for ( int b = 0; b < _size; b++)
    {

        if (arr[b] < min)
            min = arr[b];
    }
    return min; // 回傳最小值
}
double GetAvg(int arr[], int _size)
{
    double total = 0;
    double avg;
    for (int c = 0; c < _size; c++)
    {
        total += arr[c];
        avg = total / (_size);
    }
    return avg;
}
