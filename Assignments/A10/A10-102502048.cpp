#include<iostream>
#define ArraySize 20
using namespace std;
int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
void jug(int &j);//判斷是否在範圍內
void recursion(int &r,int &i);//執行輸入過程
double GetAvg(int arr[ArraySize],double _size);//回傳平均值 傳入陣列、成績總次數
int grades[ArraySize];//宣告成績陣列
int main()
{
    int i=0,grade=0;
    cout<<"Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades.\n";
    cout<<"Enter grade?: ";
    cin>>grade;
    recursion(grade,i);//進入函數
    cout<<"The total number of grades is "<<i<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,i)<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,i)<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,i)<<endl;
    return 0;
}
void recursion(int &r,int &i)
{
    do
    {
        jug(r);
        if(r==-1)
            break;
        grades[i]=r;
        i++;
    }
    while(i<20);
}
void jug(int &j)
{
    do
    {
        if(j==-1)
            break;
        if(j<0 || j>100)
            cout<<"Out of range!\n";
        cout<<"Enter grade?: ";
        cin>>j;
    }
    while(j<0 || j>100);
}
int GetMax(int arr[ArraySize],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int Max=0;
    for(int j=0; j<_size; j++)
        Max=arr[j]>Max?arr[j]:Max;
    return Max;
}
int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int Min=100;
    for(int j=0; j<_size; j++)
        Min=arr[j]<Min?arr[j]:Min;
    return Min;
}
double GetAvg(int arr[ArraySize],double _size)//回傳平均值 傳入陣列、成績總次數
{
    int sum=0;
    for(int j=0; j<_size; j++)
        sum+=arr[j];
    return sum/_size;;
}
