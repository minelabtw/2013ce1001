#include<iostream>
#define ArraySize 20

using namespace std;

int GetMax(int arr[ArraySize],int _size)  //回傳最大值 傳入陣列、成績總次數
{
    int Max = 0;
    for(int i=0; i<_size; i++) if(arr[i]>Max) Max=arr[i];
    return Max;
}

int GetMin(int arr[ArraySize],int _size) //回傳最小值 傳入陣列、成績總次數
{
    int Min = 100;
    for(int i=0; i<_size; i++) if(arr[i]<Min) Min=arr[i];

    return Min;
}

double GetAvg(int arr[ArraySize],int _size) //回傳平均值 傳入陣列、成績總次數
{
    double Avg =0;
    for(int i=0; i<_size; i++) Avg+=arr[i];
    return Avg/_size;
}

int main()
{
    int grades[ArraySize] = {};
    int i=0;
    int grade=0;

    cout << "Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades.\n";
    cout << "Enter grade?: ";
    while(cin >> grade && grade!=-1)
    {
        if(grade<0 || grade>100)
        {
            cout << "Out of range!\n";
            i--;
        }
        else
        {
            grades[i]=grade;
        }
        i++;
        if(i==ArraySize)break;
        cout << "Enter grade?: ";
    }

    cout << "The total number of grades is " << i << ".\n" << "The maximum of grades is " << GetMax(grades,i) << ".\n"
         << "The minimum of grades is " << GetMin(grades,i) << ".\n" << "The average of grades is " << GetAvg(grades,i)<< ".\n";

    return 0;
}
