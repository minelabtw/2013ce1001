#include<iostream>
#define ArraySize 20 //設定陣列大小為20項
using namespace std;

int GetMax(int arr[ArraySize],int _size) //宣告一計算最大直的涵式
{
    double MaxGrade=arr[0]; //宣告最大值起始為第一項
    for(int i=1;i<_size;i++) //輪流做比較若比最大值大則變為新的最大值否則不變
    {
        if(MaxGrade<arr[i])
            MaxGrade=arr[i];
    }
    return MaxGrade; //回傳最大值
}

int GetMin(int arr[ArraySize],int _size) //宣告一計算最小直的涵式
{
    double MinGrade=arr[0]; //宣告最大值起始為第一項
    for(int i=1;i<_size;i++) //輪流做比較若比最小值小則變為新的最小值否則不變
    {
        if(MinGrade>arr[i])
            MinGrade=arr[i];
    }
    return MinGrade; //回傳最小值
}

double GetAvg(int arr[ArraySize],int _size) //宣告一計算平均值的涵式
{
    double SumGrade=0; //宣告總分起始為0
    for(int i=0;i<_size;i++) //將輸入的每項成績加總
        SumGrade += arr[i];
    return SumGrade/_size; //回傳平均值
}

int main()
{
    double grade; //宣告變數grade代表輸入的成績
    int _size=0,arr[ArraySize]; //宣告_size代表數入成績的次數,arr[ArraySize]陣列紀錄各項成績

    cout << "Please enter students grades.\n" //輸出說明字串
         << "Maximum number of grades is 20.\n"
         << "You can also enter '-1' to finish enter grades.\n";

    while(_size<ArraySize) //當次數在20以內時
    {
        cout << "Enter grade?: "; //輸出字串要求輸入成績並傳給grade
        cin >> grade;
        if(grade != -1 && (grade>100 || grade<0)) //若不合條件則輸出Out of range!要求重新輸入
        {
            cout << "Out of range!\n";
            continue;
        }
        if(grade==-1) //若輸入-1則結束while
            break;
        arr[_size]=grade; //將有效的成績傳給該次的arr[]
        _size++; //將次數+1
    }
    cout << "The total number of grades is " << _size << ".\n"; //輸出成績的輸入次數
    cout << "The maximum of grades is " << GetMax(arr,_size) << ".\n"; //輸出最高成績
    cout << "The minimum of grades is " << GetMin(arr,_size) << ".\n"; //輸出最低成績
    cout << "The average of grades is " << GetAvg(arr,_size) << "."; //輸出平均成績

    return 0;
}
