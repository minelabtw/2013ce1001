#include<iostream>
#define ArraySize 20

using namespace std;
int GetMax(int arr[ArraySize],int _size);                               //prototype
int GetMin(int arr[ArraySize],int _size);
double GetAvg(int arr[ArraySize],int _size);
int main()
{
    cout<<"Please enter students grades.\n";
    cout<<"Maximum number of grades is 20.\n";
    cout<<"You can also enter '-1' to finish enter grades.\n";
    int counter=0;                                                   //計次用
    int a[ArraySize]= {};
    do
    {
        cout<<"Enter grade?: ";
        cin>>a[counter];
        if(a[counter]<-1||a[counter]>100)                           //判斷是否正確
        {
            cout<<"Out of range!\n";
        }
        else
        {
            if(a[counter]==-1)                                      //-1跳出
            {
                break;
            }
            counter++;
        }
    }
    while(counter<20);                                              //20次 跳出
    cout<<"The total number of grades is "<<counter<<endl;
    cout<<"The maximum of grades is "<<GetMax(a,counter)<<endl;     //run function
    cout<<"The minimum of grades is "<<GetMin(a,counter)<<endl;
    cout<<"The average of grades is "<<GetAvg(a,counter)<<endl;
    return 0;
}
int GetMax(int arr[ArraySize],int _size)
{
    int maximum=0;
    for(int i=0; i<_size; i++)                                      //max取最大值
    {
        if(arr[i]>maximum)
        {
            maximum=arr[i];
        }
    }
    return maximum;
}
int GetMin(int arr[ArraySize],int _size)
{
    int minimum=100;
    for(int i=0; i<_size; i++)                                      //min取最小
    {
        if(arr[i]<minimum)
        {
            minimum=arr[i];
        }
    }
    return minimum;

}
double GetAvg(int arr[ArraySize],int _size)
{
    double sum=0;
    for(int i=0; i<_size; i++)
    {
        sum+=arr[i];                                            //儲存總值
    }
    return sum/_size;                                           //回傳平均
}
