#include<iostream>
#define ArraySize 20
using namespace std;

int GetMax(int arr[ArraySize],int asize)
{
    int maxium = 0;
        for(int after=maxium+1; after< asize; after++)//將陣列中的數與後一項比較取最大值
        {
            if(arr[after]>arr[maxium])
                maxium = after;
        }
    return arr[maxium];
}
int GetMin(int arr[ArraySize],int asize)
{
    int minium = 0;
        for(int after=minium+1; after< asize; after++)//將陣列中的數與後一項比較取最小值
        {
            if(arr[after]<arr[minium])
                minium = after;
        }
    return arr[minium];
}
double GetAvg(int arr[ArraySize],int asize)//平均函式
{
    double total = 0;
    for(int k=0; k< asize; k++)
    {
        total+=arr[k];
    }
    return total/asize;
}
int main()
{
    int i=0;
    int number = 0;
    int grades[ArraySize] = {};
    int gradesize = 0;
    int total = 0;
    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl << "You can also enter '-1' to finish enter grades. " << endl;
    while(i<20)
    {
        cout << "Enter grade?: ";
        cin >> number;
        if(number == -1)
            i=20;
        else if(number<0 || number>100)
            cout << "Out of range!" << endl;
        else
        {
            grades[gradesize] = number;//儲存有效數到陣列
            gradesize++;
            i++;
        }
    }
    for(int k=0; k<gradesize; k++)//總和計算
    {
        total+=grades[k];
    }
    cout << "The total number of grades is " << total << "." << endl;
    cout << "The maximum of grades is " << GetMax(grades,gradesize) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,gradesize) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,gradesize) << "." ;
    return 0;
}
