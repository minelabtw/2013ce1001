#include <iostream>
#define ArraySize 20

using namespace std;

void input ( int (&arr)[ArraySize] , int &_size ); //function prototype
int GetMax ( int arr[ArraySize] , int _size );
int GetMin ( int arr[ArraySize] , int _size );
double GetAvg( int arr[ArraySize] , int _size );

int main ()
{
    int arr[ArraySize] = {}; //宣告型別為 int 的一維陣列，用來儲存所有成績的數值。
    int _size = 0; //宣告型別為 int 的變數(_size)，用來計算一共輸入了幾個成績。
    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
    input( arr , _size ); //輸入成績，並用call by reference的方法來改變main裡面所對應的變數的值。
    cout << "The total number of grades is " << _size << "." << endl; //輸出總共有幾個成績
    cout << "The maximum of grades is " << GetMax( arr , _size ) << "." << endl; //輸出所有分數中的最大值
    cout << "The minimum of grades is " << GetMin( arr , _size ) << "." << endl; //輸出所有分數中的最小值
    cout << "The average of grades is " << GetAvg( arr , _size ) << "." ; //輸出所有分數的平均值

    return 0;
}

void input ( int (&arr)[ArraySize] , int &_size ) //輸入成績
{
    int score = 0; //宣告型別為 int 的變數(score)，用來暫時儲存所輸入的成績。
    while ( _size < 20 ) //用while迴圈使其能重覆輸入，直到輸入-1或滿20個成績時結束。
    {
        cout << "Enter grade?: ";
        cin >> score;
        while ( score < -1 || score > 100 ) //用while迴圈檢驗使用者的輸入是否合乎標準，若不符，則要求其重新輸入。
        {
            cout << "Out of range!" << endl;
            cout << "Enter grade?: ";
            cin >> score;
        }
        if ( score == -1 ) //輸入-1時，跳出while迴圈。
        {
            break;
        }
        arr[_size] = score; //將所輸入的分數儲存在陣列之中。
        _size++;
    }
}

int GetMax ( int arr[ArraySize] , int _size ) //計算最大值
{
    int maximum = 0;
    for ( int i = 0 ; i < _size ; i++ )
    {
        if ( arr[i] > maximum )
        {
            maximum = arr[i];
        }
    }
    return maximum;
}

int GetMin ( int arr[ArraySize] , int _size ) //計算最小值
{
    int minimum = 100;
    for ( int i = 0 ; i < _size ; i++ )
    {
        if ( arr[i] < minimum )
        {
            minimum = arr[i];
        }
    }
    if ( _size == 0 )
    {
        return 0;
    }
    else
    {
        return minimum;
    }
}

double GetAvg ( int arr[ArraySize] , int _size ) //計算平均值
{
    double total = 0;
    double average = 0;
    for ( int i = 0 ; i < _size ; i++ )
    {
        total += arr[i];
    }
    average = total / _size;
    if ( _size == 0 )
    {
        return 0;
    }
    else
    {
        return average;
    }
}
