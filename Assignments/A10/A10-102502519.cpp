#include <iostream>
using namespace std;

int grades[20]= {};
int number=0;
int _size=0;

int GetMax(int ary[20],int _size);
int GetMin(int ary[20],int _size);
double GetAvg(int ary[20],int _size);

int main()
{

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    while(number!=-1)
    {
        for(int i=0; i<20; i++)
        {
            cout << "Enter grade?: ";
            cin >> number;
            if(number==-1)
                break;
            if(number>100||number<0)
                cout << "Out of range!";
            else
            {
                grades[i]=number;  //將符合範圍的值存入陣列grades裡
                _size++;
            }

        }
    }

    cout << "The total number of grades is " << _size << "." << endl;
    cout << "The maximum of grades is " << GetMax(grades,_size) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,_size) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,_size) << ".";

    return 0;
}

int GetMax(int ary[20],int _size)    //找出最大值並傳回
{
    int Max=0;
    for(int i=0; i<_size; i++)
    {
        if(ary[i]>Max)
            Max=ary[i];
    }
    return Max;
}

int GetMin(int ary[20],int _size)    //找出最小值並傳回
{
    int Min=100;
    for(int i=0; i<_size; i++)
    {
        if(ary[i]<Min)
            Min=ary[i];
    }
    return Min;
}

double GetAvg(int arr[20],int _size)    //找出平均值並傳回
{
    double total=0;
    for(int i=0; i<_size; i++)
    {
        total+=arr[i];
    }
    return total/_size;
}






