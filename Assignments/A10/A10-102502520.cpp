#include <iostream>
#include <iomanip>
#define ArraySize 20
using namespace std;
int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數
int num;//定義
int _size=0;
int arr[ArraySize] = {};
int main()
{
    int sum;//定義
    cout<<"Please enter students grades.\n";//輸出
    cout<<"Maximum number of grades is 20.\n";
    cout<<"You can also enter '-1' to finish enter grades.\n";
    GetMax(arr,_size);//呼叫函式
    GetMin(arr,_size);
    GetAvg(arr,_size);
    while (_size!=20)//_size不等於20，持續進行
    {
        do
        {
            cout<<"Enter grade?: ";//輸出
            cin>>num;//輸入
            arr[_size] = num;//儲存到陣列
            _size++;
            if (num==-1)//輸入-1跳出
            {
                arr[_size] = 0;
                _size--;
                break;
            }
            if (num<0||num>100)//檢驗
            {
                cout<<"Out of range!\n";
                arr[_size] = 0;
                _size--;
            }
        }
        while(num<0||num>100);
        if (num==-1)//輸入-1跳出
        {
            arr[_size] = 0;
            break;
        }
    }
    cout<<"The total number of grades is "<<_size<<endl;
    cout<<"The maximum of grades is "<<GetMax(arr,_size)<<endl;
    cout<<"The minimum of grades is "<<GetMin(arr,_size)<<endl;
    cout<<"The average of grades is "<<GetAvg(arr,_size)<<endl;

    return 0;
}

int GetMax(int arr[ArraySize],int _size)//取最大函式
{
    int Max;
    Max = arr[1];
    for(int t=0; t<_size; t++)
    {
        if (Max<arr[t])
        {
            Max = arr[t];
        }
    }
    return Max;
}
int GetMin(int arr[ArraySize],int _size)//取最小函式
{
    int Min;
    Min = arr[1];
    for(int y=0; y<_size; y++)
    {
        if (Min>arr[y])
        {
            Min = arr[y];
        }
    }
    return Min;
}
double GetAvg(int arr[ArraySize],int _size)//取平均函式
{
    double avg;
    double sum;
    for (int z=0; z<_size; z++)
    {
        sum += arr[z];
    }
    avg = sum/_size;
    return avg;
}
