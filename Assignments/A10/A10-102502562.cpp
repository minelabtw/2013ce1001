#include<iostream>
#define ArraySize 20
using namespace std;

int GetMax(int arr[],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int Max=arr[0];
    for(int i=0; i<_size; i++)
    {
        if(arr[i]>Max)
            Max=arr[i];
    }
    return Max;
}
int GetMin(int arr[],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int Min=arr[0];
    for(int i=0; i<_size; i++)
    {
        if(arr[i]<Min)
            Min=arr[i];
    }
    return Min;
}
double GetAvg(int arr[],int _size)//回傳平均值 傳入陣列、成績總次數
{
    int sum=0;
    for(int i=0; i<_size; i++)
    {
        sum+=arr[i];
    }
    return (double)sum / (double)_size;
}
int main()
{
    int grade[ArraySize]= {0}; //宣告成績陣列
    int number=0;
    int Size=0;
    cout << "Please enter students grades.\n";
    cout << "Maximum number of grades is 20.\n";
    cout << "You can also enter '-1' to finish enter grades.\n";
    while(1)
    {
        cout << "Enter grade?: ";
        cin >> number;
        while(number<-1 || number>100)
        {
            cout << "Out of range!\n";
            cout << "Enter grade?: ";
            cin >> number;
        }
        if(number==-1)
        {
            break;
        }
        grade[Size]=number;
        Size++;
        if(Size==20)
        {
            break;
        }
    }
    cout << "The total number of grades is " << Size << ".\n";
    cout << "The maximum of grades is " << GetMax(grade,Size) << ".\n";
    cout << "The minimum of grades is " << GetMin(grade,Size) << ".\n";
    cout << "The average of grades is " << GetAvg(grade,Size) << ".\n";

    return 0;
}
