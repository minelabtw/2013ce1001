#include <iostream>
#define ArraySize 20  //指定陣列大小為20。

using namespace std ;

int GetMax ( int arr [ArraySize] , int _size ) ;
int GetMin ( int arr [ArraySize] , int _size ) ;
double GetAvg ( int arr [ArraySize] , int _size ) ;  //宣告函式，

int main ()
{
    int grade = 0 ;
    int arr [ArraySize] ;
    int _size ;
    int total ;

    for( int i = 0 ; i < ArraySize ; i ++ )
        arr [i] = -1;  //初始化陣列，使每一項都為-1。

    cout << "Please enter students grades." << endl << "Maximum number of grades is 20." << endl ;
    cout << "You can also enter '-1' to finish enter grades." << endl ;

    for ( int i = 0 ; i < ArraySize ; i ++ )
    {
        cout << "Enter grades?: " ;
        cin >> grade ;  //使輸入成績。

        while ( ( grade < 0 || grade > 100 ) && grade != -1 )
        {
            cout << "Out of range!" << endl << "Enter grades?: " ;
            cin >> grade ;
        }  //當分數不合哩，就重新輸入。

        if ( grade == -1 )
            break ;  //若輸入-1就結束輸入成績的動作。

        arr [i] = grade ;  //依序放進陣列中。
    }

    for ( int i = 0 ; i < ArraySize ; i ++ )
    {
        if ( arr [i] != -1 )
            _size = i + 1 ;
    }  //計算項數。

    cout << "The total number of grades is " << _size << "." << endl ;
    cout << "The maximum of grades is " << GetMax ( arr , _size ) << "." << endl ;
    cout << "The minimum of grades is " << GetMin ( arr , _size ) << "." << endl ;
    cout << "The average of grades is " << GetAvg ( arr , _size ) << "." << endl ;  //輸出計算後之結果。

    return 0 ;
}

int GetMax ( int arr [ArraySize] , int _size )
{
    int max = 0 ;

    for(int i=0; i<_size; i++)
    {
        if ( arr [i] >= max )
            max = arr [i] ;
    }

    return max;
}  //定義函式：找出最大值。

int GetMin ( int arr [ArraySize] , int _size )
{
    int min = 100 ;

    for( int i = 0 ; i < _size ; i ++ )
    {
        if( arr [i] <= min )
            min = arr [i] ;
    }

    return min;
}  //定義函式：找出最小值。

double GetAvg ( int arr [ArraySize] , int _size )
{
    double sum = 0 ;

    for ( int i = 0 ; i < _size ; i ++ )
        sum += arr [i] ;

    return sum / _size ;
}  //定義函式：計算平均值。
