#include <iostream>

#define ArraySize 20//定義ArraySize=20

using namespace std ;

int GetMax(int arr[ArraySize],int i)//宣告函式
{
    int Max=0 ;

    for (int x=0; x<i; x++)
    {
        if (arr[x]>Max)
        {
            Max=arr[x] ;
        }
    }
    return Max ;

}

int GetMin(int arr[ArraySize],int i)
{
    int Min=100 ;

    for (int x=0; x<i; x++)
    {
        if (arr[x]<Min)
        {
            Min=arr[x] ;
        }
    }

    return Min ;
}

double GetAvg(int arr[ArraySize],int i)
{
    double sum=0 ;

    for (int x=0;x<i;x++)
    {
        sum=sum+arr[x] ;
    }

    sum=sum/i ;

    return sum ;
}

int main ()
{
    int sum=0, arr[ArraySize], g=0, i=0, a=0 ;

    cout << "Please enter students grades.\n" << "Maximum number of grades is 20.\n" ;

    cout << "You can also enter '-1' to finish enter grades.\n" ;

    while (i<20)//使用迴圈
    {
        cout << "Enter grade?: " ;

        cin >> g ;

        if (g<=100&&g>=0&&g!=-1)
        {
            arr[i]=g ;

            i++ ;
        }

        else if (g>100||g<0&&g!=-1)
            cout << "Out of range!\n" ;

        if (g==-1)
        {
            break ;
        }
    }

    cout << "The total number of grades is " << i << ".\n" ;

    cout << "The maximum of grades is " << GetMax(arr,i) << ".\n" ;//在螢幕上輸出，並呼叫函式，回傳值

    cout << "The minimum of grades is " << GetMin(arr,i) << ".\n" ;

    cout << "The average of grades is " << GetAvg(arr,i) << ".\n" ;
}
