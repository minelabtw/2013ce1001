#include<iostream>
#include<iomanip>

using namespace std;
#define arraySize 20                                                   //定義20為arraySize

int GetMax(int [],int );                                               //宣告方程，分別代入一個陣列和一個變數
int GetMin(int [],int );
double GetAvg(int [],int );

int GetMax(int Array[],int Size)
{
    int max = Array[0];                                                //宣告max為陣列第一個數

    for(int n=1 ; n<Size ; n++)                                        //依序比較max和陣列儲存的其他數大小
    {
        if(max<Array[n])                                               //假如max較小，用Array[n]重新定義max
        {
            max=Array[n];
        }
    }

    return max;                                                        //回傳最大值
}

int GetMin(int Array[],int Size)
{
    int min = Array[0];

    for(int n=1 ; n<Size ; n++)
    {
        if(min>Array[n])
        {
            min=Array[n];
        }
    }

    return min;
}

double GetAvg(int Array[],int Size)
{
    double total=0;

    for(int n=0 ; n<Size ; n++)
    {
        total=Array[n]+total;                                      //讓total為陣列裡所有數相加
    }

    return total/Size;                                             //回傳平均(total除以輸入成績次數)
}

int main()
{
    int grades[arraySize];
    int score=0;
    int i=0;

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    while(i<arraySize)
    {
        cout << "Enter grade?: ";
        cin >> score;
        while(score<-1 || score>100)
        {
            cout << "Out of range!" << endl;
            cout << "Enter grade?: ";
            cin >> score;
        }

        if(score==-1)                                        //假如輸入-1，終止輸入
        {
            break;
        }
        grades[i]=score;                                     //將每次輸入符合範圍的成績儲存進陣列
        i++;
    }

    cout << "The total number of grades is " << i << "." << endl;
    cout << "The maximum of grades is " << GetMax(grades,i) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,i) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,i) << ".";

    return 0;
}
