#include <iostream>
using namespace std;
#define ArraySize 20;

int GetMax(int [],int);
int GetMin(int [],int);
double GetAvg(int [],int);

int main()
{
	int arrsize=ArraySize;			//load constant into variable(?)
	int grades[arrsize];			//the array storing grades

	cout << "Please enter students grades.\n"
	<< "Maximum number of grades is "<< arrsize <<".\n"
	<< "You can also enter '-1' to finish enter grades.\n";

	int cnt=0;						//now grades counter
	while(cnt<20){
		int tmpgrade;
		cout << "Enter grade?: "; cin >> tmpgrade;
		if(tmpgrade==-1){break;}
		if(tmpgrade<0 || tmpgrade>100){cout << "Out of range!\n"; continue;}
		grades[cnt++]=tmpgrade;		//put this grade into grades array and index++
	}

	cout << "The total number of grades is " << cnt << ".\n";
	cout << "The maximum of grades is " << GetMax(grades,cnt) << ".\n";
	cout << "The minimum of grades is " << GetMin(grades,cnt) << ".\n";
	cout << "The average of grades is " << GetAvg(grades,cnt) << ".\n";

	return 0;
}

int GetMax(int arr[],int num)
{
	int themax=arr[0];		//hold first value
	for(int i=1;i<num;i++){if(arr[i]>themax){themax=arr[i];}}	//find the max
	return themax;
}

int GetMin(int arr[],int num)
{
	int themin=arr[0];		//hold first value
	for(int i=1;i<num;i++){if(arr[i]<themin){themin=arr[i];}}	//find the min
	return themin;
}

double GetAvg(int arr[],int num)
{
	double total=0;
	for(int i=0;i<num;i++){total+=arr[i];}						//add each values to total
	return total/num;		//return average
}
