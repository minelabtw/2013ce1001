#include<iostream>

#define ArraySize 20

using namespace std;

    //max
int GetMax(int *arr,int size)
{
    int max=0;
    for(int i=0;i<size;i++)
    {
        if(arr[i]>max)
            max=arr[i];
    }

    return max;
};
    //min
int GetMin(int *arr,int size)
{
    int min=100;
    for(int i=0;i<size;i++)
    {
        if(arr[i]<min)
            min=arr[i];
    }

    return min;
};
    //average
double GetAvg(int *arr,int size)
{
    int total=0;
    for(int i=0;i<size;i++)
        total+=arr[i];
    return total/size;
};

int main()
{
    int grades[ArraySize]={};
    int num=0;  //size
    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
    // input grades
    while(true)
    {
        cout << "Enter grade?: " ;
        cin >> grades[num];
        if(grades[num]>100 || grades[num]<-1)
            cout << "Out of range" << endl;
        else if(grades[num]==-1)
        {
            break;
        }
        else
            num++;

        if(num==20)
            break;
    }
    //number of greads
    cout << "The total number of grades is " << num << endl;
    //output ans
    cout << "The maximum of grades is " << GetMax(grades,num) << endl;
    cout << "The minimum of grades is " << GetMin(grades,num) << endl;
    cout << "The average of grades is " << GetAvg(grades,num) << endl;

    return 0;
}



