#include <iostream>

using namespace std;
#define ArraySize 20

int GetMax(int arr[],int _size)
{
    int minium = 0;
    int i = 1;
    int p = 0;

    p = _size - 1;//扣掉-1的部分

    for ( int i = 1 ; i <= p ; i++ )
    {
        if ( minium <= arr[i] )
        {
            minium = arr[i];//不斷做替換決定最大值
        }
    }

    return minium;
}

int GetMin(int arr[],int _size)
{
    int maximum = 100;
    int i = 1;
    int p = 0;

    p = _size - 1;

    for ( int i = 1 ; i <= p ; i++ )
    {
        if ( maximum >= arr[i] )
        {
            maximum = arr[i];
        }
    }

    return maximum;
}

double GetAvg(int arr[],int _size)
{
    int sum = 0;
    int number = 0;
    int p = 0;
    int i = 1;

    p = _size - 1;

    for ( int i = 1 ; i <= p ; i++ )
    {
        sum = sum + arr[i];//不斷作連加
    }

    number = sum / p;

    return number;
}


int main()
{
    int arr[ArraySize];
    int _size = 1;

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    cout << "Enter grade?: ";
    while ( cin >> arr[_size] )
    {
        if ( arr[_size] == -1 )
        {
            cout << "The total number of grades is " << _size - 1 << "." << endl;
            cout << "The maximum of grades is " << GetMax(arr,_size) << "." << endl;
            cout << "The minimum of grades is " << GetMin(arr,_size) << "." << endl;
            cout << "The average of grades is " << GetAvg(arr,_size) << "." << endl;

            break;
        }

        else if ( arr[_size] > 100 || arr[_size] < 0 )
        {
            cout << "Out of range!" << endl;
            cout << "Enter grade?: ";
        }

        else
        {
            cout << "Enter grade?: ";

            _size ++;
        }
    }

    return 0;
}
