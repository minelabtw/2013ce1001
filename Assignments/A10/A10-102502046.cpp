#include<iostream>
#define ArraySize 20
using namespace std;

int GetMax(int arr[ArraySize],int _size);       //回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);       //回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);    //回傳平均值 傳入陣列、成績總次數
int n(int arr[ArraySize]);

int main()
{
    int x=0;
    int grades[ArraySize]={};   //宣告成績陣列
    cout << "Please enter students grades." << endl
         << "Maximum number of grades is 20." << endl
         << "You can also enter '-1' to finish enter grades." << endl;
    x=n(grades);
    cout << "The total number of grades is " << x << "." << endl;               //輸出結果
    cout << "The maximum of grades is " << GetMax(grades,x) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades,x) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,x) << "." << endl;

    return 0;
}

int GetMax(int arr[ArraySize],int _size)        //找最大值
{
    int Max=arr[0];
    for (int i=1 ; i<=_size-1 ; i++)
    {
        if (arr[i]>Max)
            Max = arr[i];
    }
    return Max;
}
int GetMin(int arr[ArraySize],int _size)        //找最小值
{
    int Min=arr[0];
    for (int i=1 ; i<=_size-1 ; i++)
    {
        if (arr[i]<Min)
            Min = arr[i];
    }

    return Min;
}
double GetAvg(int arr[ArraySize],int _size)     //求平均
{
    int total=0;
    for (int i=0 ; i<_size ; i++)
        total += arr[i];
    return (double)total/_size;
}
int n( int arr[ArraySize] )
{
    int n=0;
    for (int i=0 ; i<20 ; i++)          //輸入成績
    {
        cout << "Enter grade?: ";
        cin >> arr[i];
        if (arr[i]==-1)              //如果輸入的值=-1，就不再輸入
            break;
        while(arr[i]<0||arr[i]>100)
        {
            cout << "Out of range!" << endl
                 << "Enter grade?: ";
            cin >> arr[i];
        }
        n=i+1;                      //紀錄輸入幾個
    }
    return n;
}
