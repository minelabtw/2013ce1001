#include <iostream>
#define ArraySize 20          //指定陣列大小為20
using namespace std;

int GetMax(int a[ArraySize],int _size);        //宣告函數
int GetMin(int a[ArraySize],int _size);
double GetAvg(int a[ArraySize],int _size);

int main()
{
    int grd=0;
    int a[ArraySize];
    int num=0;

    for(int i=0; i<ArraySize; i++)                //初始化Array每一項為-1
        a[i]=-1;

    cout << "Please enter students grades.\nMaximum number of grades is 20.\nYou can also enter '-1' to finish enter grades.\n";

    for(int i=0; i<ArraySize; i++)
    {
        cout << "Enter grade?: ";                   //輸入成績，判斷是否超出範圍
        cin >> grd;
        while ((grd!=-1) && (grd<0) || (grd>100))
        {
            cout << "Out of range!\n" << "Enter grade?: ";
            cin >> grd;
        }
        if(grd==-1)                                 //輸入-1則跳出
            break;
        a[i]=grd;                                   //將所有範圍內成績存入array
    }

    for(int i=0; i<ArraySize; i++)              //計算項數
    {
        if(a[i]!=-1)
            num=i+1;
    }

    cout << "The total number of grades is " << num << ".\n";
    cout << "The maximum of grades is " << GetMax(a,num) << ".\n";
    cout << "The minimum of grades is " << GetMin(a,num) << ".\n";
    cout << "The average of grades is " << GetAvg(a,num) << ".\n";

    return 0;
}

int GetMax(int a[ArraySize],int _size)   //回傳最大值 傳入陣列、成績總次數
{
    int max=0;
    for(int i=0; i<_size; i++)
    {
        if(a[i]>=max)
            max=a[i];
    }
    return max;
}

int GetMin(int a[ArraySize],int _size)   //回傳最小值 傳入陣列、成績總次數
{
    int min=100;
    for(int i=0; i<_size; i++)
    {
        if(a[i]<=min)
            min=a[i];
    }
    return min;
}

double GetAvg(int a[ArraySize],int _size)   //回傳平均值 傳入陣列、成績總次數
{
    double sum=0;
    for(int i=0; i<_size; i++)
        sum+=a[i];
    return sum/_size;
}
