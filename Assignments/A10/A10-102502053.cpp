#include <iostream>
using namespace std;

//function prototype
int GetMax(int[],int);
int GetMin(int[],int);
double GetAvg(int[],int);

int main()
{
    //call variables
    int grade;
    const int arraysize=20;//size of array
    int marks[arraysize]= {};//creat array
    int no=0;

    cout<<"Please enter students grades."<<endl; //display words
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish entering grades."<<endl;

    while(no!=20) //looping to input while total input numbers are not = to 20
    {
        do
        {
            cout<<"Enter grade?: ";
            cin>>grade;
            if(grade>100||grade<-1)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(grade>100||grade<-1);

        if(grade==-1)//exit while input -1
        {
            break;
        }
        marks[no]=grade; //save input into array
        no++; //counter
    }

    //display output
    cout<<"The total number of grades is "<<no<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(marks,no)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(marks,no)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(marks,no)<<"."<<endl;
    return 0;
}

int GetMax(int marks[],int no) //finding the maximum number
{
    int n=0;
    int a=marks[n];
    while(n<no-1)
    {
        int temp=marks[n+1];
        if(temp>a)
        {
            a=temp;
        }
        n++;
    }
    return a;
}

int GetMin(int marks[],int no) //finding the minimum number
{
    int n=0;
    int a=marks[n];
    while(n<no-1)
    {
        int temp=marks[n+1];
        if(temp<a)
        {
            a=temp;
        }
        n++;
    }
    return a;
}

double GetAvg(int marks[],int no) //calculate for the average
{
    double temp=0;
    for(int a=0; a<=no-1; a++)
    {
        temp=temp+marks[a];
    }
    temp=temp/no;
    return temp;
}
