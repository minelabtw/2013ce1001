#include<iostream>
using namespace std;
#define ArraySize 20 //指定陣列大小為20
int grades[ArraySize];//宣告成績陣列
int input(int grade, int time)
{
    do
    {
        cout<<"Enter grade?: ";
        cin>>grade;
        if(grade>=0&&grade<=100)grades[time]=grade; //用陣列紀錄分數
        else if(grade==-1)break; //中途結束
        else
        {
            cout<<"Out of range!"<<endl;
            continue; //回到迴圈頂端
        }
        time++; //計數器每次+1
    }
    while(time<20);
    return time; //回傳次數
}
int GetMax(int arr[ArraySize],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int Max=arr[0];
    for(int i=0; i<_size; i++)
    {
        if(Max<arr[i])Max=arr[i];
    }
    return Max;
}
int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int Min=arr[0];
    for(int i=0; i<_size; i++)
    {
        if(Min>arr[i])Min=arr[i];
    }
    return Min;
}
double GetAvg(int arr[ArraySize],int _size)//回傳平均值 傳入陣列、成績總次數
{
    double total=0;
    for(int i=0; i<_size; i++)
    {
        total+=arr[i];
    }
    return total/_size;
}
int main()
{
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    int Size=input(0,0);
    cout<<"The total number of grades is "<<Size<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,Size)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,Size)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,Size)<<".";
    return 0;
}
