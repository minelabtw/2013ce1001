#include <iostream>
using namespace std;

const int ArraySize=20;
int grades[ArraySize]= {};

int number=0;
int _size=0;

int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數

int main()
{
    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;
    for(int i=0; i<20; i++)
    {
        cout<<"Enter grade?: ";
        cin>>number;

        while(number<-1||number>100)//判斷number在0~100之間或等於-1
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter grade?: ";
            cin>>number;
        }

        if (number==-1)
            break;

        grades[i]=number;//將number的值傳入陣列
        _size++;
    }
    cout<<"The total number of grades is "<<_size<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,_size)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,_size)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,_size)<<".";

    return 0;
}

int GetMax(int arr[ArraySize],int _size)
{
    int Max=0;
    for(int i=0; i<_size; i++)//找出陣列中的最大值
    {
        if(arr[i]>Max)
            Max=arr[i];
    }
    return Max;
}

int GetMin(int arr[ArraySize],int _size)
{
    int Min=100;
    for(int i=0; i<_size; i++)//找出陣列中的最小值
    {
        if(arr[i]<Min)
            Min=arr[i];
    }
    return Min;
}

double GetAvg(int arr[ArraySize],int _size)
{
    double total=0;
    for(int i=0; i<_size; i++)//計算陣列中的平均值
    {
        total+=arr[i];
    }
    return total/_size;
}






