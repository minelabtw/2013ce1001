#include <iostream>

using namespace std;

int GetMax(int grades[20],int counter);
int GetMin(int grades[20],int counter);
double GetAvg(int agrades[20],int counter);

int main()
{
    float grade;
    int counter=0;//計數器
    int grades[20]= {};

    cout<<"Please enter students grades."<<endl;
    cout<<"Maximum number of grades is 20."<<endl;
    cout<<"You can also enter '-1' to finish enter grades."<<endl;

    do
    {
        cout<<"Enter grade?: ";
        cin>>grade;
        while(grade!=-1 && (grade <0 || grade >100))
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter grade?: ";
            cin>>grade;
        }
        if(grade!=-1)//grade=-1跳出且不算次數
        {
            grades[counter] = grade;
            counter = counter+1;
        }
    }
    while(grade!=-1 && counter<20);
    cout<<"The total number of grades is "<<counter<<"."<<endl;
    cout<<"The maximum of grades is "<<GetMax(grades,counter)<<"."<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,counter)<<"."<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,counter)<<".";

    return 0;
}

int GetMax(int grades[20],int counter)//找最大
{
    int max = grades[0];
    for (int i=1; i<counter; i++)
    {
        if (grades[i]>max)
            max = grades[i];
    }
    return max;
}
int GetMin(int grades[20],int counter)//找最小
{
    int min = grades[0];
    for (int i=1; i<counter; i++)
    {
        if (grades[i]<min)
            min = grades[i];
    }
    return min;
}
double GetAvg(int grades[20],int counter)//算平均
{
    double sum=0;
    if (counter==0)
        return 0;
    else
    {
        for (int i=0; i<counter; i++)
            sum = sum + grades[i];
        return sum/(counter);
    }
}
