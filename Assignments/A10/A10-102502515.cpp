#include <iostream>

using namespace std;

const int ArraySize=20;//固定ArraySize為20
int grades[ArraySize]= {};//宣告陣列
int GetMax(int arr[ ArraySize ],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數

int main()
{
    int i=0;//利用i限定執行次數
    int a;

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;


    while (i<20)//制定條件使在20次內
    {
        cout << "Enter grade?: " ;
        cin  >> a;

        if (a==-1)//如果輸入-1則跳出迴圈
        {
            break;
        }
        while (a<0 || a>100)//使輸入在範圍內
        {
            cout << "Out of range!" << endl;
            cout << "Enter grade?: ";
            cin  >> a;
        }
        grades[i]=a;//將輸入值存到陣列
        i++;
    }
    cout << "The total number of grades is " << i << endl;
    cout << "The maximum of grades is " << GetMax(grades,i) << endl;
    cout << "The minimum of grades is " << GetMin(grades,i) << endl;
    cout << "The average of grades is " << GetAvg(grades,i) << endl;

    return 0;
}

int GetMax(int arr[ ArraySize ],int _size)//回傳最大值 傳入陣列、成績總次數
{
    int maxNum=0;
    for (int j=0; j<_size; j++)//個別比較陣列內的所有數字，如果大於前一個，則取代前一個數字。
    {
        if (maxNum < arr[j])
        {
            maxNum = arr[j];
        }
    }
    return maxNum;//回傳最大值
}

int GetMin(int arr[ArraySize],int _size)//回傳最小值 傳入陣列、成績總次數
{
    int minNum=100;
    for (int k=0; k<_size; k++)//個別比較陣列內所有數字，如果小於前一個，則取代前一個數字。
    {
        if (minNum > arr[k])
        {
            minNum = arr[k];
        }
    }
    return minNum;//回傳最小值
}

double GetAvg(int arr[ArraySize],int _size)//回傳平均值 傳入陣列、成績總次數
{
    double sum=0;
    for (int l =0; l<=_size; l++)//用for迴圈加總和
    {
        sum = sum + arr[l];
    }
    return sum/_size;//回傳平均值
}
