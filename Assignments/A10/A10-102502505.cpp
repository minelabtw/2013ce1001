#include <iostream>
using namespace std;

int grades[20];//定義陣列，並使其大小為20
int i=1;//定義函數
int grade=0;//定義成績，並使其初始值為0
int GetMax(int[],int);//定義最大值函式，並在其中定義陣列
int GetMin(int[],int);//定義最小值函式，並在其中定義陣列
double GetAvg(int[],int);//定義平均數函式，並在其中定義陣列


int main()
{
    cout << "Please enter students grades." << endl;//輸出字串
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;

    while (grade!=-1)//設迴圈使輸入的成績不能為-1
    {
        cout << "Enter grade?: ";
        cin >> grade;

        if(grade==-1)//輸入-1時跳出迴圈
        {
            i = i-1;
            break;
        }

        while (grade>100 or grade<0)//限制成績範圍
        {
            cout << "Out of range!" << endl;//輸出此字串
            cout << "Enter grade?: ";
            cin >> grade;

            if(grade==-1)//假如重新輸入數字時為-1即會跳出迴圈
            {
                i=i-1;
                break;
            }
        }

        grades[i]=grade;//把成績輸入陣列中

        if (i==20)//當成績輸滿20時，即跳出大迴圈
        {
            break;
        }
        i++;//i值加1
    }

    cout << "The total number of grades is " << i << "." << endl;//輸出下列字串
    cout << "The maximum of grades is " << GetMax(grades,i) << "." << endl;//把成績陣列及i值輸入函式中
    cout << "The minimum of grades is " << GetMin(grades,i) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades,i) << ".";

    return 0;//返回初始值

}

int GetMax(int x[],int _size)//定義函式
{
    int maxnum=0;//先定義最大值為0
    for(int a=1; a<=_size; a++)//把陣列中的每個數跟最大值做比較
    {
        if (x[a]>maxnum)
            maxnum=x[a];
    }
    return maxnum;//最後輸出最大值
}

int GetMin(int y[],int _size)
{
    int minnum=y[1];
    for(int b=1; b<=_size; b++)
    {
        if (y[b]<minnum)
            minnum=y[b];
    }
    return minnum;
}


double GetAvg(int z[],int _size)//定義平均值函式
{
    double total=0;//定義總和為0
    for(int c=1; c<=_size; c++)//將陣列中的每個數加進總和變數中
    {
        total=total+z[c];
    }

    return total/_size;//最後輸出總和除與總數
}





















