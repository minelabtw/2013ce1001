#include<iostream>
#include<cstdlib>
#define ArraySize 20

using namespace std;

int grade[ArraySize];
int gradenenter();
int GetMax(int arr[ArraySize],int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數

int gradeenter()//將成績輸入陣列
{
    int a ,b, c;
    for(a = 0; a < 20; a++)
    {
        cout << "Enter grade?: ";
        cin >> c;
        while(c > 100 || c < 0)
        {
            if (c == -1)
            {
                break;
            }
            cout << "Out of range!\n" << "Enter grade?: ";
            cin >> c;
        }
        if (c == -1)
        {
            b = b - 1;
            break;
        }
        grade[a] = c;
        b++;

    }
    return b;

}
int GetMax(int arr[20],int gradesize)//取得成績中之最大值
{
    int a, b;
    b = arr[a];
    for(a = 1; a < gradesize; a++)
    {
        if (arr[a + 1] > b)
        {
            b = arr[a + 1];
        }
        else
        {
            b = b;
        }
    }
    return b;
}
int GetMin(int arr[20],int gradesize)//取得成績中之最小值
{
    int a, b;
    b = arr[a];
    for(a = 1; a < gradesize; a++)
    {
        if (arr[a + 1] < b)
        {
            b = arr[a + 1];
        }
        else
        {
            b = b;
        }
    }
    return b;
}
double GetAvg(int arr[20],int gradesize)//取得成績平均值
{
    int a, b = 0;
    for (a = 1; a <= gradesize; a++)
    {
        b = b + arr[a];
    }
    b = (b / gradesize);

        return b;

}

int main()//輸入成績,並得出最大值,最小值還有平均值
{
    int gradesize;
    int gmax;
    int gmin;
    int avg;
    cout << "Please enter students grades.\n" << "Maximum number of grades is 20.\n" << "You can also enter '-1' to finish enter grades.\n";
    gradesize = gradeenter();
    gmax = GetMax(grade, gradesize);
    gmin = GetMin(grade, gradesize);
    avg = GetAvg(grade, gradesize);


    cout << "The total number of grades is " << gradesize << ".\n" << "The maximum of grades is " << gmax << ".\n" << "The minimum of grades is " << gmin << ".\n"
            << "The average of grades is " << avg << ".";

    system("pause");

    return 0;




}
