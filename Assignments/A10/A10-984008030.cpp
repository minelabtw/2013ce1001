#include <iostream>
using namespace std;

#define ArraySize 20

void GetGrade(int arr[ArraySize], int& _size);//取得成績並存入arr
int GetMax(int arr[ArraySize], int _size);//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize], int _size);//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize], int _size);//回傳平均值 傳入陣列、成績總次數

int main(){
    int grades[ArraySize];//宣告型態為int的成績陣列
    int inputCount = 0;//宣告型態為int的輸入數量，並初始化為0

    cout << "Please enter students grades." << endl \
         << "Maximum number of grades is 20." << endl \
         << "You can also enter '-1' to finish enter grades." << endl;
    GetGrade(grades, inputCount);//取得成績
    cout << "The total number of grades is " << inputCount << "." << endl \
         << "The maximum of grades is " << GetMax(grades, inputCount) << "." << endl \
         << "The minimum of grades is " << GetMin(grades, inputCount) << "." << endl \
         << "The average of grades is " << GetAvg(grades, inputCount) << ".";
    return 0;
}

void GetGrade(int arr[ArraySize], int& _size){
    //取得成績並存入arr
    int grade = 0;//宣告型態為int的成績，並初始化為0
    while(grade != -1 && _size < ArraySize){
        cout << "Enter grade?: ";
        cin >> grade;
        if(grade < 0 || grade > 100){
            if(grade != -1){//等於-1時不用輸出，等迴圈自己判斷跳出
                cout << "Out of range!" << endl;
            }
        }
        else{
            arr[_size++] = grade;
        }
    }
}

int GetMax(int arr[ArraySize], int _size){
    //回傳最大值 傳入陣列、成績總次數
    int max = arr[0];//宣告型態為int的成績最大值，並初始化為arr[0]
    for(int i = 1; i < _size; i++){
        if(max < arr[i]){//若成績最大值小於目前讀到的值
            max = arr[i];
        }
    }
    return max;
}

int GetMin(int arr[ArraySize], int _size){
    //回傳最小值 傳入陣列、成績總次數
    int min = arr[0];//宣告型態為int的成績最小值，並初始化為arr[0]
    for(int i = 1; i < _size; i++){
        if(min > arr[i]){//若成績最小值大於目前讀到的值
            min = arr[i];
        }
    }
    return min;
}

double GetAvg(int arr[ArraySize], int _size){
    //回傳平均值 傳入陣列、成績總次數
    double avg = (double)arr[0];//宣告型態為int的成績平均值，並初始化為arr[0]
    for(int i = 1; i < _size; i++){
        avg += arr[i];
    }
    return avg / (double)_size;
}
