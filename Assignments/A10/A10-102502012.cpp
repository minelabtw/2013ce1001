#include<iostream>
using namespace std;
#define ArraySize 20
int GetMax(int arr[], int _size)
{
    int max = -2e9;
    for (int i = 0; i < _size; i++)
    {
        if (arr[i] > max)
            max = arr[i];
    }
    return max;
}
int GetMin(int arr[], int _size)
{
    int min = 2e9;
    for (int i = 0; i < _size; i++)
    {
        if (arr[i] < min)
            min = arr[i];
    }
    return min;
}
double GetAvg(int arr[], int _size)
{
    int sum = 0;
    for (int i = 0; i < _size; i++)
        sum += arr[i];
    return (double)sum / _size;
}
bool input(int arr[], int &ptr){ // the type is to match ? : operator, or I need to make another function call this line : (cout << "Out of range!" << endl && input(arr, ptr))
	cout << "Enter grade?: ";
	return ptr == 20 ? 0 : cin >> arr[ptr] && arr[ptr] == -1 ? 0 : arr[ptr] >= 0 && arr[ptr] <= 100 ? input(arr, ++ptr) : (cout << "Out of range!" << endl && input(arr, ptr));
}
int main()
{
    int grades[ArraySize];
    int ptr = 0;
    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl;
	(void)input(grades, ptr);
    cout << "The total number of grades is " << ptr << "." << endl;
    cout << "The maximum of grades is " << GetMax(grades, ptr) << "." << endl;
    cout << "The minimum of grades is " << GetMin(grades, ptr) << "." << endl;
    cout << "The average of grades is " << GetAvg(grades, ptr) << "." << endl;
    return 0;
}
