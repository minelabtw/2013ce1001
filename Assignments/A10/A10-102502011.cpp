#include<iostream>
#include <math.h>
using namespace std;
#define ArraySize 20

int GetMax(int arr[ArraySize],int _size) ;//回傳最大值 傳入陣列、成績總次數
int GetMin(int arr[ArraySize],int _size) ;//回傳最小值 傳入陣列、成績總次數
double GetAvg(int arr[ArraySize],int _size);//回傳平均值 傳入陣列、成績總次數

int main()
{
    int _size = 0 ;

    int grades[ArraySize] ;//宣告成績陣列

    cout << "Please enter students grades." << endl ;
    cout << "Maximum number of grades is 20." << endl ;
    cout << "You can also enter '-1' to finish enter grades." << endl ;

    for (_size=0; _size<ArraySize; _size++)
    {
        do
        {
            cout << "Enter grade?: " ;
            cin >> grades[_size] ;
            if (grades[_size] == -1 )
                break ;
            if (grades[_size] > 100 || grades[_size] < 0 )
                cout << "Out of range!" << endl ;
        }
        while(grades[_size] > 100 || grades[_size] < 0 ) ;

        if (grades[_size] == -1 ) //當輸入-1 跳出迴圈
            break ;
    }

    cout << "The total number of grades is " << _size << "." << endl ;
    cout << "The maximum of grades is " << GetMax( grades, _size) << "." << endl ;
    cout << "The minimum of grades is " << GetMin( grades, _size) << "." << endl ;
    cout << "The average of grades is " << GetAvg( grades, _size) << "." ;


    return 0;
}

int GetMax(int arr[ArraySize],int _size) //最大值算法
{
    int maximum = arr[0] ;
    for (int x=0; x<_size; x++)
    {
        if(maximum<arr[x])
            maximum=arr[x] ;
    }
    return maximum ;
}

int GetMin(int arr[ArraySize],int _size) //最小值算法
{
    int minimum = arr[0] ;
    for (int x=0; x<_size; x++)
    {
        if(minimum>arr[x])
            minimum=arr[x] ;
    }
    return minimum ;
}

double GetAvg(int arr[ArraySize],int _size) //平均值算法
{
    double total = 0 ;
    for (int x=0; x<_size; x++)
        total += arr[x] ;

    return total / _size ;
}

