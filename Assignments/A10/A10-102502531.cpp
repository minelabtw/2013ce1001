#include <iostream>
#define Arraysize 20

using namespace std;


int main()
{
    int grades[Arraysize];
    int GetMax(int [], int );
    int GetMin(int [], int );
    int Frequency=0;
    double GetAvg(int [],int ); //宣告

    cout << "Please enter students grades." << endl;
    cout << "Maximum number of grades is 20." << endl;
    cout << "You can also enter '-1' to finish enter grades." << endl; //說明

    for(int i=0; Frequency<Arraysize; i++) //輸入
    {
        cout<<"Enter grade?: ";
        cin>>grades[i];
        if(grades[i]<0&&grades[i]!=-1)
        {
            cout<<"Out of range!"<<endl;
            i--;
        }
        else if (grades[i]>100)
        {
            cout<<"Out of range!"<<endl;
            i--;
        }
        else if (grades[i]==-1)
            break;
        else
            Frequency=Frequency+1;
    }
    cout<<"The total number of grades is "<<Frequency<<endl;

    cout<<"The maximum of grades is "<<GetMax(grades,Frequency)<<endl;
    cout<<"The minimum of grades is "<<GetMin(grades,Frequency)<<endl;
    cout<<"The average of grades is "<<GetAvg(grades,Frequency)<<endl; //輸出
}
double GetAvg(int grades[] ,int _size) //平均
{
    double sum=0;
    for(int i=0; i<_size; i++)
    {
        sum=grades[i]+sum;
    }
    return sum/_size;
}
int GetMax(int grades[] ,int _size) //最大
{
    int maximum;
    for(int i=0; i<_size; i++)
    {
        int a;
        a=grades[i];
        int moveItem=i;

        while((moveItem>0)&&(grades[i-1]>a))
        {
            grades[moveItem]=grades[moveItem-1];
            moveItem--;
        }
        grades[moveItem]=a;
    }
    maximum=grades[_size];
    return maximum;
}
int GetMin(int grades[] ,int _size) //最小
{
    int minimum;
    for(int i=0; i<_size; i++)
    {
        int a;
        a=grades[i];
        int moveItem=i;

        while((moveItem>0)&&(grades[i-1]>a))
        {
            grades[moveItem]=grades[moveItem-1];
            moveItem--;
        }
        grades[moveItem]=a;
    }
    minimum=grades[0];
    return minimum;
}

