#include<iostream>
#include "A13-102502014.h"          //include class
using namespace std;
int main()
{
    GiftSystem a;                   //物件a
    int year;
    int m;

    cout<<"How old are you?: ";
    cin>>year;
    while(year>125 || year<0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
        cin>>year;
    }
    a.setYear(year);                    //設定年齡
    a.randColor();                      //顏色隨機
    a.printColor();                     //列印顏色

    do
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>m;
        switch(m)
        {
        case 1:
            a.exchangeGift();
            break;
        case 2:
            a.randColor();
            a.printColor();
            break;
        default:
            cout<<"Out of range!"<<endl;
            break;
        }
    }
    while(m!=1);               //沒換禮物就重新
    return 0;
}
