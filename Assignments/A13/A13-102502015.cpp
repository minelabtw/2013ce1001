#include <iostream>
#include "A13-102502015.h"                  //插入Class
using namespace std;
int main()
{
    GiftSystem a;       //物件a of GiftSystem
    int year;           //年齡
    int mode;           //存Mode
    do
    {                   //存年齡
        cout<<"How old are you?: ";
        cin>>year;
        if(year>125 ||year<0)               //超出範圍
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(year>125 ||year<0);               //超出範圍重新輸入
    a.setYear(year);                        //設定年齡
    a.randColor();                          //隨機顏色
    a.printColor();                         //印出顏色
    do
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>mode;
        switch(mode)
        {
        case 1:
            a.exchangeGift();               //換禮物
            break;
        case 2:
            a.randColor();                  //換顏色
            a.printColor();
            break;
        default:
            cout<<"Out of range!"<<endl;    //超出範圍
            break;
        }
    }
    while(mode!=1);                         //沒1就重新
    return 0;
}
