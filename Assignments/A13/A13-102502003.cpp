#include<iostream>
#include "A13-102502003.h"
using namespace std;

int main()
{
    GiftSystem choose;
    int year=0;
    int give=0;

    do
    {
        cout<<"How old are you?: ";
        cin>>year;
        if(year>125||year<0)
            cout<<"Out of range!"<<endl;
    }
    while(year>125||year<0);

    choose.setYear(year);

    choose.randColor();
    choose.printColor();

    do
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>give;

        switch(give)
        {
        case 1:
            choose.exchangeGift();
            break;
        case 2:
            choose.randColor();
            choose.printColor();
            break;
        default:
            cout<<"Out of range!"<<endl;
            break;
        }


    }
    while(give!=1);


    return 0;
}
