#include <iostream>
#include <cstdlib>
#include <ctime>
#ifndef GIFT_H
#define GIFT_H
using namespace std;

class GiftSystem
{
    public:
        void setYear(int);//設定年齡
        void randColor();//使用rand()隨機抽色卡
        void printColor();//將抽到的色卡cout出來
        void exchangeGift();//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
    private:
        int age,color;//設定年齡和顏色
};
GiftSystem::GiftSystem()
{
    age=color=0;//初始化
}

void GiftSystem::setYear(int year)
{
    while(year<0 || year>125)//判斷year的範圍
    {
        cout << "Out of range!" << endl;
        cout << "How old are you?: ";
        cin >> year;
    }
    age = year;
}

void GiftSystem::randColor()
{
    color = (rand()%3)+1;//隨機生成1~3
}

void GiftSystem::printColor()
{
    switch(color)//判斷顏色
    {
    case 1:
        cout << "Red" << endl;
        break;
    case 2:
        cout << "Green" << endl;
        break;
    case 3:
        cout << "White" << endl;
    }
}

void GiftSystem::exchangeGift()
{
    switch(color)//判斷哪一種顏色換哪一種禮物
    {
    case 1:
        if(age<=125 && age>=65)
            cout << "Denture";
        else if(age<65 && age>=18)
            cout << "Vitamin";
        else if(age<18 && age>=12)
            cout << "Movie ticket";
        else
            cout << "Lego";
        break;
    case 2:
        if(age<=125 && age>=65)
            cout << "Pipe";
        else if(age<65 && age>=18)
            cout << "Watch";
        else if(age<18 && age>=12)
            cout << "Baseball gloves";
        else
            cout << "Toy car";
        break;
    case 3:
        if(age<=125 && age>=65)
            cout << "Hair dye";
        else if(age<65 && age>=18)
            cout << "Wallet";
        else if(age<18 && age>=12)
            cout << "Ukulele";
        else
            cout << "Doraemon doll";
        break;
    }
}
#endif // GIFT_H
