#include<iostream>
#include"A13-102502504.h"

using namespace std;

int main()
{
    int num=2;

    GiftSystem a; //創造一個a(隸屬GiftSystem類別)
    a.setYear(1); //設定年齡

    do
    {
        if(num==2)
        {
            a.randColor(); //使用rand()隨機抽色卡
            a.printColor(); //將抽到的色卡cout出來。
        }

        cout << "1)Exchange gift 2)Change color?: ";
        cin >> num;
        if(num!=1&&num!=2)
        {
            cout << "Out of range!" << endl; //顯示錯誤訊息
        }

    }
    while(num!=1); //只要num不等於一就重複執行上述程式

    a.exchangeGift(); //依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。

    return 0;
}

