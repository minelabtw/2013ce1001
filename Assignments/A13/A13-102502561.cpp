#include "A13-102502561.h"
#include <iostream>
using namespace std;
int main()
{
    GiftSystem GS;  //call GiftSystem GS
    GS.setYear(0);  //call setyear from GS
    GS.randColor();  //call randcolor from GS
    GS.printColor();  //call printcolor from GS
    int choice=0;

    cout << "1)Exchange gift 2)Change color?: ";
    cin >> choice;
    while(choice!=1)    //choice=1 means that you choose the color at last
    {
        if(choice!=2)   //when choice isn't 1 nor 2 than it is out of range
        {
            cout << "Out of range!" << endl;
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> choice; //each move ends with a choice
        }
        else if(choice==2)  //choice=2 means you want to change a card
        {
            GS.randColor();
            GS.printColor();
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> choice;  //each move ends with a choice
        }
    }
    GS.exchangeGift();  //prints out your last choice
    return 0;
}
