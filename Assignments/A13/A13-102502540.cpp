#include <iostream>
#include "A13-102502540.h"
using namespace std;
int main()
{
    GiftSystem a; //物件a
    int year=0;
    do //迴圈
    {
        cout << "How old are you?: ";
        cin >> year;
        if (year<0 or year>125)
            cout << "Out of range!" << endl;
    }
    while(year<0 or year>125);
    a.setYear(year); //class函式
    year=2;
    do
    {
        if (year==2)
            a.printColor();
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> year;
        if (year!=1 and year!=2)
            cout << "Out of range!" << endl;
    }
    while (year!=1);
    a.exchangeGift();
    return 0;
}
