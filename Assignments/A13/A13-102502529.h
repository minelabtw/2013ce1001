#ifndef A13-102502529_H
#define A13-102502529_H

#include<ctime>
#include<cstdlib>
using namespace std;

class GiftSystem
{
public:

    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();

private:
    int new_setYear;
    int color;
};
void GiftSystem::setYear(int x)
{
    while(1)                        //判斷年齡,先確定年齡
    {
        cout<<"How old are you?: ";
        cin>>x;
        if(0<=x&&x<=125)
        {
            new_setYear=x;          //範圍正確就跳出
            break;
        }
        else
        {
            cout<<"Out of range!"<<endl;
        }
    }
    if(new_setYear<12)              //判斷年齡在哪個階段 並存值
        new_setYear=0;
    else if(new_setYear>=12&&new_setYear<18)
        new_setYear=1;
    else if(new_setYear>=18&&new_setYear<65)
        new_setYear=2;
    else
        new_setYear=3;
}
void GiftSystem::randColor()        //隨機顏色
{
    srand(time(NULL));
    color=rand()%3+0;
}
void GiftSystem::printColor()   //輸出卡片顏色
{
    if(color==0)
        cout<<"Red"<<endl;
    if(color==1)
        cout<<"Green"<<endl;
    if(color==2)
        cout<<"White"<<endl;
}
void GiftSystem::exchangeGift()             //兌換禮物
{
    if(color==0)
    {
        if(new_setYear==0)
        {
            cout<<"Lego"<<endl;
        }
        if(new_setYear==1)
        {
            cout<<"Movie ticket"<<endl;
        }
        if(new_setYear==2)
        {
            cout<<"Vitamin"<<endl;
        }
        if(new_setYear==3)
        {
            cout<<"Denture"<<endl;
        }
    }
    if(color==1)
    {
        if(new_setYear==0)
            cout<<"Toy car"<<endl;
        if(new_setYear==1)
            cout<<"Baseball gloves"<<endl;
        if(new_setYear==2)
            cout<<"Watch"<<endl;
        if(new_setYear==3)
            cout<<"Pipe"<<endl;
    }
    if(color==2)
    {
        if(new_setYear==0)
            cout<<"Doraemon doll"<<endl;
        if(new_setYear==1)
            cout<<"Ukulele"<<endl;
        if(new_setYear==2)
            cout<<"Wallet"<<endl;
        if(new_setYear==3)
            cout<<"Hair dye"<<endl;

    }
}


#endif // A13-102502529_H
