#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H

class GiftSystem
{
public:
    GiftSystem() ;
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
private:
    int age ;
    int color ;
};
#endif // GIFTSYSTEM_H
