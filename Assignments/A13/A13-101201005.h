#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class GiftSystem //命名
{
private:
    int age, color;
public:
    void setYear(int);  // 設定年齡
    void randColor();   // 使用rand()隨機抽色卡
    void printColor();  // 將抽到的色卡cout出來。
    void exchangeGift();// 依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
    GiftSystem();
};
