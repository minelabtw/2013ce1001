#include <iostream>
#include "A13-102502562.h"
using namespace std;

int main()
{
    GiftSystem GS;//產生一個叫GS的GiftSystem導像物件
    int year=-1;
    while(year<0 || year>125)//給使用者輸入年齡並判斷是否符合範圍
    {
        cout << "How old are you?: ";
        cin >> year;
        if(year<0 || year>125)
        {
            cout << "Out of range!" << endl;
        }
    }
    GS.setYear(year);//帶入year的值到setYear裡
    GS.randColor();
    GS.printColor();
    GS.exchangeGift();

    return 0;
}
