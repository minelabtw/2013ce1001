#include<iostream>
#include "A13-101602016.h"
using namespace std;
int main()
{
    int choice;//宣告選擇變數
    GiftSystem MyGift;//物件宣告
    MyGift.setYear();//輸入年紀
    MyGift.randColor();//隨機出現顏色
    do
    {
        MyGift.printColor();//輸出顏色
        do
        {
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>choice;//請使用者選擇並輸入choice變數
            if(choice<1||2<choice)
                cout<<"Out of range!"<<endl;//如超出範圍則重新輸入
        }
        while(choice<1||2<choice);
        if(choice==1)//如果選擇1，輸出禮物
            MyGift.exchangeGift();
        else if(choice==2)//如果選擇2，重新或得另一種隨機顏色
            MyGift.randColor();
    }
    while(choice!=1);

    return 0;
}
