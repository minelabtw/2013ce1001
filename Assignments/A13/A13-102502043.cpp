#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

class GiftSystem
{
    private:
        int color;
    public:
        void setYear(int age)
        {
            age;
        }
        void randColor()
        {
            srand(time(0));
            color=rand()%3;
        }
        void printColor()
        {
            if(color==0)
            {
                cout<<"Red"<<endl;
            }
            else if(color==1)
            {
                cout<<"Green"<<endl;
            }
            else if(color==2)
            {
                cout<<"White"<<endl;
            }
        }
        void exchangeGift(int a)
        {
            if(a<=125&&a>=65)
            {
                if(color==0)
                    cout<<"Denture";
                else if(color==1)
                    cout<<"Pipe";
                else if(color==2)
                    cout<<"Hair dye";
            }
            if(a<65&&a>=18)
            {
                if(color==0)
                    cout<<"Vitamin";
                else if(color==1)
                    cout<<"Watch";
                else if(color==2)
                    cout<<"Wallet";
            }
            if(a<18&&a>12)
            {
                if(color==0)
                    cout<<"Movie ticket";
                else if(color==1)
                    cout<<"Baseball gloves";
                else if(color==2)
                    cout<<"Ukulele";
            }
            if(a<12&&a>=0)
            {
                if(color==0)
                    cout<<"Lego";
                else if(color==1)
                    cout<<"Toy car";
                else if(color==2)
                    cout<<"Doraemon doll";
            }
        }
};

int main()
{
    GiftSystem g;
    int x;
    int change;
    cout<<"How old are you?: ";
    cin>>x;
    while(1)
    {
        if(x>125||x<0)
        {
            cout<<"Out of range!"<<endl;
            cout<<"How old are you?: ";
            cin>>x;
        }
        else
            break;
    }
    g.randColor();
    g.printColor();
    while(1)
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>change;
        if(change!=1&&change!=2)
        {
            cout<<"Out of range!"<<endl;
        }
         else if(change==2)
        {
            g.randColor();
            g.printColor();
        }
        else
            break;
    }
    g.exchangeGift(x);
    return 0;
}
