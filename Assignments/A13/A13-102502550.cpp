#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "A13-102502550.h"
using namespace std;

int main()
{
    int year,accept;
    GiftSystem aa;                                                                     //宣告物件實體

    srand(time(NULL));                                                                 //亂數初始化

    do                                                                                 //提示輸入
    {
        cout<<"How old are you?: ";
        cin>>year;
    }
    while( (year<0 || year>125) && cout<<"Out of range!"<<endl);

    aa.setYear(year);

    do
    {
        //印出顏色並詢問是否重新給顏色
        aa.randColor();
        aa.printColor();
        cout<<endl;

        do
        {
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>accept;
        }
        while( (accept!=1 && accept!=2) && cout<<"Out of range!"<<endl);

    }
    while(accept==2);

    aa.exchangeGift();                                             //送出禮物
    return 0;
}
