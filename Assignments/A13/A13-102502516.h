#ifndef A13-102502516_H_INCLUDED
#define A13-102502516_H_INCLUDED
#include <iostream>
#include <limits>
#include <cstdlib>
using namespace std;

class GiftSystem
{
public:
    GiftSystem();
    void input( int, int, bool, int& );  //用來讓使用者輸入
    void randColor();  //讓使用者抽顏色，不喜歡可以一直換到喜歡的
    void exchangeGift();  //拿禮物
    int age;  //main和class中都要用到的年齡變數
private:
    string color[3] = { "Red", "Green", "White" }; //把亂數代換成對應顏色
    string gift[3][4]= { {"Denture", "Vitamin", "Movie ticket", "Lego"} , { "Pipe", "Watch", "Baseball gloves", "Toy car" } , { "Hair dye", "Wallet", "Ukulele", "Doraemon doll" } };//年齡及顏色對應的禮物
    int choose, colorRnd;
};
GiftSystem::GiftSystem()
{
}
void GiftSystem::input( int Min, int Max, bool words, int &userInput ) //參數分別是最小值、最大值、決定問話內容的變數、輸入數值的儲存目標
{
    do
    {
        cout << (words ? "How old are you?: " : "1)Exchange gift 2)Change color?: " ); //true表問年齡、false表問下一步
        cin >> userInput;
        if ( !cin && (userInput = Max + 1 ) ) //順便把目前數值改成不合法的，以掉入迴圈
        {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }
    while ( ( userInput > Max || userInput < Min ) && cout << "Out of range!" << endl );
}
void GiftSystem::randColor()
{
    do
    {
        srand(rand());  //用亂數當種子：若用time(0)，發現抽太快時會重複抽到同樣的色卡，改成這樣即解決
        cout << color[ colorRnd = rand()%3 ] << endl; //抽顏色、存入變數，並印出來
        input( 1, 2, 0, choose);  //問話
    }
    while( choose == 2);  //重抽
}
void GiftSystem::exchangeGift()
{
    if ( age < 12)
        age = 3;
    else if ( age < 18)
        age = 2;
    else if ( age <65 )
        age = 1;
    else
        age = 0;  //依題目要求轉換年齡成代號
    cout << gift[colorRnd][age];  //印出禮物
}
#endif // A13-102502516_H_INCLUDED
