#include <iostream>
#include "A13-102502549.h"
using namespace std;

int main()
{
    int year;//年齡
    int num;//功能選擇
    GiftSystem gift;//建立GiftSystem物件

    do//輸入年齡並檢查
    {
        cout<<"How old are you?: ";
        cin>>year;

        if(year<0||year>125)
            cout<<"Out of range!"<<endl;
    }
    while(year<0||year>125);

    //以下淺顯易懂
    gift.setYear(year);
    gift.randColor();
    gift.printColor();

    //換顏色或兌換禮物
    while(true)
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>num;

        switch(num)
        {
        case 1:
            gift.exchangeGift();
            return 0;

        case 2:
            gift.randColor();
            gift.printColor();
            break;

        default:
            cout<<"Out of range!"<<endl;
            break;
        }
    }
}
