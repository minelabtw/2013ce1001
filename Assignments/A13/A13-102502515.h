#ifndef A13-102502515_H_INCLUDED
#define A13-102502515_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

class GiftSystem
{
public:
    GiftSystem();
    void setYear(int);//設定年齡
    void randColor();//使用rand()隨機抽色卡
    void printColor();//將抽到的色卡cout出來。
    void exchangeGift();//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
private:
    int Howold;
    int color;
};

GiftSystem::GiftSystem()//initialize elements of array sales to 0
{
    Howold=color=0;
}

void GiftSystem::setYear(int a)//get a
{
    Howold=a;
}

void GiftSystem::randColor()//get a color
{
    srand(time(0));
    color=rand()%3;
}

void GiftSystem::printColor()//print out the color
{
    if (color==0)
        cout << "Red" << endl;
    else if (color==1)
        cout << "Green" << endl;
    else if (color==2)
        cout << "White" << endl;
}

void GiftSystem::exchangeGift()//cout the gift
{

    if ( Howold >= 65 && color == 0 )
        cout << "Denture";
    else if ( Howold >= 65 && color == 1 )
        cout << "Pipe";
    else if ( Howold >= 65 && color == 2 )
        cout << "Hair dye";
    else if ( Howold >= 18 && color == 0 )
        cout << "Vitamin";
    else if ( Howold >= 18 && color == 1 )
        cout << "Watch";
    else if ( Howold >= 18 && color == 2 )
        cout << "Wallet";
    else if ( Howold >= 12 && color == 0 )
        cout << "Movie ticket";
    else if ( Howold >= 12 && color == 1 )
        cout << "Baseball gloves";
    else if ( Howold >= 12 && color == 2 )
        cout << "Ukulele";
    else if ( Howold >= 0 && color == 0 )
        cout << "Lego";
    else if ( Howold >= 0 && color == 1 )
        cout << "Toy car";
    else if ( Howold >= 0 && color == 2 )
        cout << "Doraemon doll";

}

#endif // A13-102502515_H_INCLUDED
