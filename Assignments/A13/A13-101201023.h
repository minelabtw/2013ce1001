class GiftSystem
{
public:
    void setYear()
    {
        cout << "How old are you?: ";
        cin >> year;
        while(year<0 || year>125)                  //判斷年齡是否在0~125以內
        {
            cout << "Out of range!" << endl;
            cout << "How old are you?: ";
            cin >> year;
        }
    }

    void randColor()
    {
        card=rand()%3;                             //隨機取0~2其中一數(隨機抽卡)
    }

    void printColor()
    {
        if(card==0)                                //讓0為抽到紅卡，並顯示出
        {
            cout << "Red" << endl;
        }

        else if(card==1)                           //讓1為綠卡
        {
            cout << "Green" << endl;
        }

        else                                       //讓2為白卡
        {
            cout << "White" << endl;
        }
    }

    void exchangeGift()
    {
        while(answer!=1)                                          //當answer為1時，中止
        {
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> answer;
            if(answer==1)                                         //判斷是否交換禮物，如果交換則輸出禮物
            {
                if(year>=65)
                {
                    if(card==0)
                    {
                        cout << "Denture";
                    }

                    else if(card==1)
                    {
                        cout << "Pipe";
                    }

                    else
                    {
                        cout << "Hair dye";
                    }
                }

                else if(year<65 && year>=18)
                {
                    if(card==0)
                    {
                        cout << "Vitamin";
                    }

                    else if(card==1)
                    {
                        cout << "Watch";
                    }

                    else
                    {
                        cout << "Wallet";
                    }
                }

                else if(year<18 && year>=12)
                {
                    if(card==0)
                    {
                        cout << "Movie ticket";
                    }

                    else if(card==1)
                    {
                        cout << "Baseball gloves";
                    }

                    else
                    {
                        cout << "Ukulele";
                    }
                }

                else
                {
                    if(card==0)
                    {
                        cout << "Lego";
                    }

                    else if(card==1)
                    {
                        cout << "Toy car";
                    }

                    else
                    {
                        cout << "Doraemon doll";
                    }
                }
            }

            else if(answer==2)                           //重新隨機抽卡
            {
                randColor();
                printColor();
            }

            else                                         //判斷answer是否符合範圍
            {
                cout << "Out of range!" << endl;
            }
        }
    }


private:
    int year;
    int card;
    int answer;
};
