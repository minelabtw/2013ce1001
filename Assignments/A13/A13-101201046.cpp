#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "A13-101201046.h"

using namespace std;

int main(void) {
    int y, opt;
    Giftsystem g;

    srand(time(NULL));

    do { //input the year
        cout << "How old are you?: ";
        cin >> y;
        } while((y < 0 || y > 125) && cout << "Out of range!\n");

    g.setgifts(y); //set gifts by year

    do { //exchange the gift or change the color
        g.randc(); //set color by random
        g.printc(); //print color
        do { //input option
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> opt;
            } while((opt < 1 || opt > 2) && cout << "Out of range!\n");
        } while(opt != 1);
    g.exchange(); //exchange the gift

    return 0;
    }
