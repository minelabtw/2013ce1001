#include <iostream>
#include <cstdlib>
#include <ctime>
#include "A13-102502020.h"                //呼叫檔案
using namespace std;

int main()
{
    int year=0;                           //宣告變數
    srand(time(0));
    GiftSystem x;                         //建立物件x
    do
    {
        cout << "How old are you?: ";
        cin >> year;
        if(year<0 or year>125)
        cout << "Out of range!" << endl;
        x.setYear(year);                  //呼叫函式
    }
    while(year<0 or year>125);
    int a=2;
    do
    {
        if(a==2)
        {
            x.randColor();
            x.printColor();
        }
        else
            cout << "Out of range!" << endl;
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> a;
    }
    while(a!=1);                          //當a=1時，開始交換禮物
    x.exchangeGift();

    return 0;
}
