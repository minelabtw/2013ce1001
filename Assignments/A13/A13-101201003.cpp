#include<iostream>
#include "A13-101201003.h"
using namespace::std;

int main()
{
    int choose=0;
    GiftSystem Mygift;//設定物件
    Mygift.setYear();//使用函式
    Mygift.randColor();//使用函式
    do
    {
        cout<<"1)Exchange gift 2)Change color?: ";//選擇要換顏色還是換禮物
        cin>>choose;
        if(choose!=1&&choose!=2)
        {
            cout<<"Out of range!"<<endl;
        }//輸入錯誤值

        if(choose==1)//選擇換禮物
        {
            Mygift.exchangeGift();
        }
        else if(choose==2)//選擇換顏色
        {
            Mygift.randColor();
        }
    }
    while(choose!=1);
    return 0;
}
