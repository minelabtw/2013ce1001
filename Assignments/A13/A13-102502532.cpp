#include "A13-102502532.h"          //加入檔

int main()
{
    srand(time(0));             //亂數 時間    srand()是建立一個亂數表  要放main裡

    int year =0;
    int select =0;

    GiftSystem g;                   //建立 object
    cout<<"How old are you?: ";
    cin>>year;
    while(year<0 or year>125)
    {
        cout<<"Out of range!"<<endl<<"How old are you?: ";
        cin>>year;
    }

    g.randColor();                   //呼叫  形成亂數
    g.printColor();

    cout<<endl<<"1)Exchange gift 2)Change color?: ";
    cin>>select;
    while(select!=1)                                             //重複輸入
    {
        while(select!=1 and select!=2)                                  //限制範圍
        {
            cout<<"Out of range!"<<endl<<"1)Exchange gift 2)Change color?: ";
            cin>>select;
        }
        while (select==2)
        {
            g.randColor();                      //再呼叫  形成亂數
            g.printColor();
            cout<<endl<<"1)Exchange gift 2)Change color?: ";
            cin>>select;
        }
    }

    while (select==1)
    {
        g.exchangeGift(year);
        break;
    }

    return 0;
}
