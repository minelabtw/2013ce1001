#include "A13-102502007.h"
int main()
{
    int age;//使用者年齡
    int mode;//輸入模式(1or2)
    int card;//抽到的卡片
    giftsystem gift;//建立gift物件
    gift.setyear(&age);//輸入年齡，並用call by pointer 紀錄
    gift.randcolor(&card);//隨機給卡片編號1,2,3，並用call by pointer紀錄
    gift.printcolor(&card);//1,2,3分別印出red,green,white
    do
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> mode;
        if(mode!=1 && mode!=2)
        {
            cout << "Out of range!" << endl;
            continue;
        }//loop until the correct mode is input
        switch(mode)
        {
        case 1:
        {
            gift.exchangegift(&card,&age);
            break;
            //利用call by pointer記錄的卡片編號和年齡
            //丟入換禮物的方法
            //給出相對應的結果
        }
        case 2:
        {
            gift.randcolor(&card);//再次隨機給卡片編號1,2,3，並用call by pointer紀錄
            gift.printcolor(&card);//1,2,3分別印出red,green,white
            mode=3;//觸發迴圈
        }
        default:
            break;
        }
    }
    while(mode!=1 && mode!=2);

    return 0;
}

