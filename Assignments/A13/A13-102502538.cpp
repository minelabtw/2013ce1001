#include<iostream>
#include"A13-102502538.h"

using namespace std;

int main()
{
    int age;
    int change=0;

    cout<<"How old are you?: ";
    cin>>age;
    while(age<0||age>125)//判定age的大小
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
        cin>>age;
    }
    setYear(age);//輸出抽取到的卡片
    randColor();
    printColor();

    while(change!=1)
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>change;
        if(change<1||change>2)//判定是否為1和2
        {
            cout<<"Out of range!"<<endl;
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>change;
        }
        else if(change==2)
        {
            randColor();//重抽一張卡片
            printColor();
        }
    }
    exchangeGift();//兌換禮物

    return 0;
}
