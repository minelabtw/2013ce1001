#ifndef A13-102502554_H
#define A13-102502554_H
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

class GiftSystem
{
public:
    GiftSystem();
    void setYear(int );
    void randColor();
    void printColor();
    void exchangeGift();//設定public
private:
    int year;
    int color;//設定private
};

GiftSystem::GiftSystem()
{
    year = 0;
    color = 0;
}//歸零

void GiftSystem::setYear( int x )
{
    year = x;//導入變數
}

void GiftSystem::randColor()
{
    srand( time( 0 ) );
    color = ( rand() % 3 ) + 1;
}//隨機變數代表抽出之顏色

void GiftSystem::printColor()
{
    if ( color == 1 )
        cout << "Red" << endl;
    if ( color == 2 )
        cout << "Green" << endl;
    if ( color == 3 )
        cout << "White" << endl;
}//判斷顏色並輸出

void GiftSystem::exchangeGift()
{
    if ( year < 12 && year >= 0 && color == 1 )
        cout << "Lego";
    if ( year < 12 && year >= 0 && color == 2 )
        cout << "Toy car";
    if ( year < 12 && year >= 0 && color == 3 )
        cout << "Doraemon doll";
    if ( year < 18 && year >= 12 && color == 1 )
        cout << "Movie ticket";
    if ( year < 18 && year >= 12 && color == 2 )
        cout << "Baseball gloves";
    if ( year < 18 && year >= 12 && color == 3 )
        cout << "Ukulele";
    if ( year < 65 && year >= 18 && color == 1 )
        cout << "Vitamin";
    if ( year < 65 && year >= 18 && color == 2 )
        cout << "Watch";
    if ( year < 65 && year >= 18 && color == 3 )
        cout << "Wallet";
    if ( year <= 125 && year >= 65 && color == 1 )
        cout << "Denture";
    if ( year <= 125 && year >= 65 && color == 2 )
        cout << "Pipe";
    if ( year <= 125 && year >= 65 && color == 3 )
        cout << "Hair dye";
}//判斷得到的禮物
#endif // A13-102502554_H
