#ifndef GiftSystem_H                  //沒定義的話 就定義
#define GiftSystem_H                  //定義class

#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;
class GiftSystem
{
private:                              //只在class裡用
    int year;
    int color;
public:
    void setYear(int a)
    {
        srand(time(0));               //初始化亂數
        year=a;
    }
    void randColor()
    {
        color=rand()%3+1;             //拿顏色
    }
    void printColor()
    {
        if (color==1)
        {
            cout<<"Red";
        }
        if (color==2)
        {
            cout<<"Green";
        }
        else if (color==3)
        {
            cout<<"White";
        }
        cout<<endl;
    }
    void exchangeGift()               //換禮物
    {
        if(125>=year && year>=65)
        {
            if(color==1)
            {
                cout<<"Denture";
            }
            if(color==2)
            {
                cout<<"Pipe";
            }
            else if(color==3)
            {
                cout<<"Hair dye";
            }
        }
        else if(65>year && year>=18)
        {
            if(color==1)
            {
                cout<<"Vitamin";
            }
            if(color==2)
            {
                cout<<"Watch";
            }
            else if(color==3)
            {
                cout<<"Wallet";
            }
        }
        else if(18>year && year>=12)
        {
            if(color==1)
            {
                cout<<"Movie ticket";
            }
            if(color==2)
            {
                cout<<"Baseball gloves";
            }
            else if(color==3)
            {
                cout<<"Ukulele";
            }
        }
        else if(12>year && year>=0)
        {
            if(color==1)
            {
                cout<<"Lego";
            }
            if(color==2)
            {
                cout<<"Toy car";
            }
            else if(color==3)
            {
                cout<<"Doraemon doll";
            }
        }
    }

};
#endif                            //結束if
