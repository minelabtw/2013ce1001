#ifndef A13-102502049_H_INCLUDED
#define A13-102502049_H_INCLUDED

using namespace std;

class GiftSystem //建立class
{
public:
    GiftSystem();
    void setYear(int);
    void yeartonum();
    void randColor();
    void printColor();
    void exchangeGift();
    void setstring();
private:
    int years;
    int yearsnum; //轉換區間為數值
    int randnum; //顏色數值
    string select[4]; //color對應物品
    string select1[4];
    string select2[4];
};

GiftSystem::GiftSystem()
{
    srand(time(0)); //打亂時間
}


void GiftSystem::setYear(int x)
{
    years=x; //輸入年齡
}

void GiftSystem::yeartonum()
{
    if(125>=years&&years>=65)
        yearsnum=1;
    else if(65 > years&&years>=18)
        yearsnum=2;
    else if(18 > years&&years>=12)
        yearsnum=3;
    else if(12 > years&&years>= 0)
        yearsnum=4;
}

void GiftSystem::randColor()
{
    randnum=(rand()%3)+1;
}

void GiftSystem::printColor()
{
    if(randnum==1)
        cout << "Red" << endl;
    else if(randnum==2)
        cout << "Green" << endl;
    else if(randnum==3)
        cout << "White" << endl;
}

void GiftSystem::setstring() //紀錄禮物
{
    select[0]="Denture";
    select[1]="Vitamin";
    select[2]="Movie ticket";
    select[3]="Lego";

    select1[0]="Pipe";
    select1[1]="Watch";
    select1[2]="Baseball gloves";
    select1[3]="Toy car";

    select2[0]="Hair dye";
    select2[1]="Wallet";
    select2[2]="Ukulele";
    select2[3]="Doraemon doll";
}


void GiftSystem::exchangeGift() //印出禮物
{
    switch(randnum)
    {
    case 1:
        if(yearsnum==1)
            cout << select[0] <<endl;
        else if(yearsnum==2)
            cout << select[1] << endl;
        else if(yearsnum==3)
            cout << select[2] <<endl;
        else if(yearsnum==4)
            cout << select[3] << endl;
        break;
    case 2:
        if(yearsnum==1)
            cout << select1[0] <<endl;
        else if(yearsnum==2)
            cout << select1[1] << endl;
        else if(yearsnum==3)
            cout << select1[2] <<endl;
        else if(yearsnum==4)
            cout << select1[3] << endl;
        break;
    case 3:
        if(yearsnum==1)
            cout << select2[0] <<endl;
        else if(yearsnum==2)
            cout << select2[1] << endl;
        else if(yearsnum==3)
            cout << select2[2] <<endl;
        else if(yearsnum==4)
            cout << select2[3] << endl;
        break;
    }
}


#endif // A13-102502049_H_INCLUDED
