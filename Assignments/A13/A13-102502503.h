#ifndef GIFT_H_INCLUDED
#define GIFT_H_INCLUDED
#include <iostream>
#include<ctime>
#include<cstdlib>
using namespace std;

class GiftSystem  //宣告class名稱
{
public:
    int gyear;
    int card;
    int choice;

    void setYear(int y)
    {
        gyear=y;  //將gyear值設定為傳入的y值
    }

    void randColor()
    {
        srand(time(0));
        card=rand()%3+1;  //隨機設定亂數1~3
    }

    void printColor()  //輸出卡片顏色
    {
        if (card==1)
            cout << "Red";
        else if (card==2)
            cout << "Green";
        else if (card==3)
            cout << "White";
    }

    void exchangeGift()
    {
        while (choice!=1)
        {
            do
            {
                cout << endl << "1)Exchange gift 2)Change color?: ";
                cin >> choice;
                if (choice!=1 && choice!=2)
                    cout << "Out of range!";
            }
            while (choice!=1 && choice!=2);

            if (choice==1)  //若輸入1則輸出得到的禮物
            {
                switch (card)
                {
                case 1:  //若卡片為紅色

                    if (125>=gyear && gyear>=65)
                        cout << "Denture";
                    else if (65 > gyear && gyear>=18)
                        cout << "Vitamin";
                    else if (18 > gyear && gyear>=12)
                        cout << "Movie ticket";
                    else if (12 > gyear && gyear>= 0)
                        cout << "Lego";

                    break;
                case 2:  //若卡片為綠色
                {
                    if (125>=gyear && gyear>=65)
                        cout << "Pipe";
                    else if (65 > gyear && gyear>=18)
                        cout << "Watch";
                    else if (18 > gyear && gyear>=12)
                        cout << "Baseball gloves";
                    else if (12 > gyear && gyear>= 0)
                        cout << "Toy car";
                }
                break;
                case 3:  //若卡片為白色
                {
                    if (125>=gyear && gyear>=65)
                        cout << "Hair dye";
                    else if (65 > gyear && gyear>=18)
                        cout << "Wallet";
                    else if (18 > gyear && gyear>=12)
                        cout << "Ukulele";
                    else if (12 > gyear && gyear>= 0)
                        cout << "Doraemon doll";
                }
                break;
                default:
                    break;
                }
            }
            else if (choice==2)  //若輸入2則重新抽卡片
            {
                randColor();  //呼叫函式隨機抽卡
                printColor();  //呼叫函式輸出卡片顏色
            }
        }
    }
};
#endif // GiftSystem
