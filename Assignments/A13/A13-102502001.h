#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

class GiftSystem
{
private:
    int age;
    int color;
public:
    GiftSystem () ;
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
};
GiftSystem::GiftSystem()
{
    age = 0 ;
    color = 0 ;
}
void GiftSystem::setYear(int _a)
{
    age=_a;
}
void GiftSystem::randColor()
{
    srand( time( 0 ) );
    color= rand() % 2;
}
void GiftSystem::printColor()
{
    if(color==0)
        cout<<"Red"<<endl;
    if(color==1)
        cout<<"Green"<<endl;
    if(color==2)
        cout<<"White"<<endl;
}
void GiftSystem::exchangeGift()
{
        if(age>=0 and age<12 and color==0)
            cout<<"Lego"<<endl;
        if(age>=0 and age<12 and color==1)
            cout<<"Toy car"<<endl;
        if(age>=0 and age<12 and color==2)
            cout<<"Doraemon doll"<<endl;
        if(age>=12 and age<18 and color==0)
            cout<<"Movie ticket"<<endl;
        if(age>=12 and age<18 and color==1)
            cout<<"Baseball gloves"<<endl;
        if(age>=12 and age<18 and color==2)
            cout<<"Ukulele"<<endl;
        if(age>=18 and age<65 and color==0)
            cout<<"Vitamin"<<endl;
        if(age>=18 and age<65 and color==1)
            cout<<"Watch"<<endl;
        if(age>=18 and age<65 and color==2)
            cout<<"Wallet"<<endl;
        if(age>=65 and age<=125 and color==0)
            cout<<"Denture"<<endl;
        if(age>=65 and age<=125 and color==1)
            cout<<"Pipe"<<endl;
        if(age>=65 and age<=125 and color==2)
            cout<<"Hair dye"<<endl;
}
#endif

