#include<iostream>
#include<iomanip>
#include<stdio.h>
#include <stdlib.h>
#include<ctime>
#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H
using namespace std ;
class GiftSystem
{
public:
    GiftSystem();
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
private:
    int color ;
    int year ;
};

GiftSystem::GiftSystem()
{
    year=color=0 ;

}
void GiftSystem::setYear(int a)    //設定年齡
{
    do
    {
        cout <<"How old are you?: " ;
        cin>>a ;
        if(a<0||a>125)
            cout <<"Out of range!"<<endl;
    }
    while(a<0||a>125);
    year=a ;
}
void GiftSystem::randColor()    //隨機顏色
{
    srand(time(NULL));
    color=(rand()%3)+1;
}
void GiftSystem::printColor()     //指定顏色
{
    if(color==1)
        cout <<"Red"<<endl ;
    if(color==2)
        cout <<"Green"<<endl ;
    if (color==3)
        cout<<"White"<<endl;
}
void GiftSystem::exchangeGift()     //交換禮物
{
    int number ;
    do
    {
        do
        {
            cout <<"1)Exchange gift 2)Change color?: ";
            cin >>number ;
            if(number<1 || number>2)
                cout <<"Out of range!"<<endl;
        }
        while(number<1 || number>2);
        if(number==2)
        {
            randColor() ;
            printColor() ;
        }
    }
    while(number==2);

    if(color==1)
    {
        if(125>=year && year>=65)
            cout <<"Denture";
        else if (65>year&&year>=18)
            cout<<"Vitamin";
        else if(18>year &&year>=12)
            cout<<"Movie ticket";
        else if(year<12)
            cout <<"Lego";
    }
    if(color==2)
    {
        if(125>=year&&year>=65)
            cout <<"Pipe";
        else if (65>year&&year>=18)
            cout<<"Watch";
        else if(18>year&&year>=12)
            cout<<"Baseball gloves";
        else if(year<12)
            cout <<"Toy car";
    }
    if(color==3)
    {
        if(125>=year&&year>=65)
            cout <<"Hair dye";
        else if (65>year&&year>=18)
            cout<<"Wallet";
        else if(18>year&&year>=12)
            cout<<"Ukulele";
        else if(year<12)
            cout <<"Doraemon doll";
    }

}
#endif
