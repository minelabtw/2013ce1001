#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "A13-102502520.h"
using namespace std;

int main()
{
    srand(time(0));//亂數變更
    int year;//定義
    int chose;
    GiftSystem gg;//物件實體
    do//檢驗
    {
        cout<<"How old are you? (0<=year<=125): ";
        cin>>year;
        if (year>125||year<0)
        {
            cout<<"Out of range! "<<endl;
        }
    }
    while (year>125||year<0);
    gg.setYear(year);
    do//重複
    {
        gg.randColor();//顯示顏色並詢問是否更換
        gg.printColor();
        cout<<endl;
        do
        {
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>chose;
            if (chose!=1 && chose!=2)
            {
                cout<<"Out of range! "<<endl;
            }
        }
        while(chose!=1 && chose!=2);

    }
    while(chose==2);
    gg.exchangeGift();
    return 0;
}
