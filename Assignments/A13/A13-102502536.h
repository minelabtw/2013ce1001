#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class GiftSystem  //class declaration
{
private:
    int year;
    int color;
public:
    GiftSystem();
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
};

GiftSystem::GiftSystem()
{
    year=0;
    color=0;
}

void GiftSystem::setYear(int a)
{
    year=a;
}

void GiftSystem::randColor()
{
    srand(time(0));
    color=rand()%3;
}

void GiftSystem::printColor()
{
    if(color==0)
        cout << "Red" << endl;
    if(color==1)
        cout << "Green" << endl;
    if(color==2)
        cout << "White" << endl;
}

void GiftSystem::exchangeGift()
{
    if(color==0)
    {
        if(year>=65 && year<=125)
            cout << "Denture";
        if(year>=18 && year<65)
            cout << "Vitamin";
        if(year>=12 && year<18)
            cout << "Movie ticket";
        if(year>=0 && year<12)
            cout << "Lego";
    }

    if(color==1)
    {
        if(year>=65 && year<=125)
            cout << "Pipe";
        if(year>=18 && year<65)
            cout << "Watch";
        if(year>=12 && year<18)
            cout << "Baseball gloves";
        if(year>=0 && year<12)
            cout << "Toy car";
    }

    if(color==2)
    {
        if(year>=65 && year<=125)
            cout << "Hair dye";
        if(year>=18 && year<65)
            cout << "Wallet";
        if(year>=12 && year<18)
            cout << "Ukulele";
        if(year>=0 && year<12)
            cout << "Doraemon doll";
    }
}

#endif
