#ifndef __A13_H__
#define __A13_H__

#include<iostream>
#include<string>
#include<cstdlib>
#include<ctime>

class GiftSystem  //define GiftSystem
{
   int year,color;   //members
public:
   GiftSystem();  //constructor
   void setYear(int);   //member functions
   void randColor();
   void printColor();
   void exchangeGift();
};
GiftSystem::GiftSystem()   //constructor
{
   srand(time(NULL));
}
void GiftSystem::setYear(int y)  //set year
{
   year=(y>=12)+(y>=18)+(y>=65);
}
void GiftSystem::randColor()  //rand color
{
   color=rand()%3;
}
void GiftSystem::printColor()    //print color
{
   std::string temp[3]={"Red","Green","White"};
   std::cout<<temp[color]<<std::endl;
}
void GiftSystem::exchangeGift()  //exchannge gift
{
   std::string temp[4][3]={{"Lego","Toy car","Doraemon doll"},{"Movie ticket","Baseball gloves","Ukulele"},{"Vitamin","Watch","Wallet"},{"Denture","Pipe","Hair dye"}};
   std::cout<<temp[year][color]<<std::endl;
}

#endif
