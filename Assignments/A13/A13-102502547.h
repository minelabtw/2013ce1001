#ifndef GiftSystem_H
#define GiftSystem_H

class GiftSystem
{
public:
    GiftSystem();
    void setYear(int); //設定年齡
    void randColor(); //使用rand()隨機抽色卡
    void printColor(); //將抽到的色卡cout出來。
    void exchangeGift(); //依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
private:
    int year;
    int color;
};
#endif // GiftSystem

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

GiftSystem::GiftSystem()
{
    year=0;
    color=0;
}

void GiftSystem::setYear(int y)
{
    if(y>=0 && y<12)
        year=1;
    if(y>=12 && y<18)
        year=2;
    if(y>=18 && y<65)
        year=3;
    if(y>=65 && y<=125)
        year=4;
}

void GiftSystem::randColor()
{
    srand(time(NULL));
    color=rand()%3;
}

void GiftSystem::printColor()
{
    if(color==0)
        cout << "Red" << endl;
    if(color==1)
        cout << "Green" << endl;
    if(color==2)
        cout << "White" << endl;
}
void GiftSystem::exchangeGift()
{
    if(year==1)
    {
        if(color==0)
            cout << "Lego";
        if(color==1)
            cout << "Toy car";
        if(color==2)
            cout << "Doraemon doll";
    }
    if(year==2)
    {
        if(color==0)
            cout << "Movie ticket";
        if(color==1)
            cout << "Baseball gloves";
        if(color==2)
            cout << "Ukulele";
    }
    if(year==3)
    {
        if(color==0)
            cout << "Vitamin";
        if(color==1)
            cout << "Watch";
        if(color==2)
            cout << "Wallet";
    }
    if(year==4)
    {
        if(color==0)
            cout << "Denture";
        if(color==1)
            cout << "Pipe";
        if(color==2)
            cout << "Hair dye";
    }
}
