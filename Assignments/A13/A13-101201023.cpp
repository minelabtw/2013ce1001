#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;
#include "A13-101201023.h"

int main()
{
    GiftSystem present;              //宣告一個物件導向名稱
    srand(time(NULL));               //讓rand隨機取樣

    present.setYear();               //呼叫class裡的方程
    present.randColor();
    present.printColor();
    present.exchangeGift();

    return 0;
}
