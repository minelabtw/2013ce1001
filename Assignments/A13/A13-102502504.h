#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

class GiftSystem
{
public:
    int year;
    int color;

    void setYear(int k)
    {
        do
        {
            cout << "How old are you? ";
            cin >> year;
            if(year<0||year>125) //顯示錯誤訊息
            {
                cout << "Out of range!" << endl;
            }
        }
        while(year<0||year>125);// 當年齡不符合範圍時，重複執行上述程式
    }

    void randColor()
    {
        srand(time(NULL)); //以time()函式為種子，改變一開始的亂數值
        color=rand()%(3)+1; //任意產生亂數1至3
    }

    void printColor() //將產生的color亂數，轉成對應之顏色，並顯示出來
    {
        if(color==1) //令color為1時，為紅色
        {
            cout << "Red" << endl;
        }
        if(color==2) //令color為2時，為綠色
        {
            cout << "Green" << endl;
        }
        if(color==3) //令color為3時，為白色
        {
            cout << "White" << endl;
        }
    }

    void exchangeGift()
    {
        switch (color) //依據表格分類之禮物，設定顏色及其年齡對應之禮物，並顯示出來
        {
        case 1:
            if(year>=0&&year<12)
                cout << "Lego";
            if(year>=12&&year<18)
                cout << "Movie ticket";
            if(year>=18&&year<65)
                cout << "Vitamin";
            if(year>=65&&year<=125)
                cout << "Denture";
            break;
        case 2:
            if(year>=0&&year<12)
                cout << "Toy card";
            if(year>=12&&year<18)
                cout << "Baseball gloves";
            if(year>=18&&year<65)
                cout << "Watch";
            if(year>=65&&year<=125)
                cout << "Pipe";
            break;
        case 3:
            if(year>=0&&year<12)
                cout << "Doraemon doll";
            if(year>=12&&year<18)
                cout << "Ukulele";
            if(year>=18&&year<65)
                cout << "Wallet";
            if(year>=65&&year<=125)
                cout << "Hair dye";
            break;
        }
    }
};
