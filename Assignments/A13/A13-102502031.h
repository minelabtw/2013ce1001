//A13-102502031.h is included only in A13-102502031.cpp
#ifndef A13-102502031_H
#define A13-102502031_H

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class GiftSystem
{
public:
    GiftSystem();
    void getYear();
    void getYearCategory();
    void randColor();
    void printColor();
    void changeColor();
    void exchangeGift();
private:
    int year;
    int yearCategory;
    int color;
    int changeButton;
};

GiftSystem::GiftSystem()
{
    srand(time(0));
}

void GiftSystem::getYear()
{
    do
    {
        cout << "How old are you?: ";
        cin >> year;
    }
    while ( (year>125 || year<0) && cout << "Out of range!" << endl);    //the range of year is [0, 125]
    getYearCategory();    //let year show as yearCategory
}

void GiftSystem::getYearCategory()
{
    if (year>=0 && year<12)
        yearCategory=1;
    else if (year>=12 && year<18)
        yearCategory=4;
    else if (year>=18 && year<65)
        yearCategory=7;
    else if (year>=65 && year<=125)
        yearCategory=10;
}

void GiftSystem::randColor()
{
    changeButton=2;
    do
    {
        if (changeButton==2)
        {
            color=rand()%3;    //color: 0=Red, 1=Green, 2=White
            printColor();
        }
        else
        {
            cout << "Out of range!" << endl;
        }
        changeColor();
    }
    while (changeButton!=1);
}

void GiftSystem::printColor()
{
    if (color==0)
        cout << "Red" << endl;
    else if (color==1)
        cout << "Green" << endl;
    else if (color==2)
        cout << "White" << endl;
}

void GiftSystem::changeColor()
{
    cout << "1)Exchange gift 2)Change color?: ";
    cin >> changeButton;
}

void GiftSystem::exchangeGift()
{
    switch ((yearCategory+color))
    {
        /**gift list (x is yearCategory value; y is color value; x+y is gift value)
        |---------------------------------------------------------------------|
        |  year(x)\color(y)  | Red (0)      | Green (1)       | White (2)     |
        |---------------------------------------------------------------------|
        | 125>=year>=65 (10) | Denture      | Pipe            | Hair dye      |
        | 65 > year>=18  (7) | Vitamin      | Watch           | Wallet        |
        | 18 > year>=12  (4) | Movie ticket | Baseball gloves | Ukulele       |
        | 12 > year>= 0  (1) | Lego         | Toy car         | Doraemon doll |
        |---------------------------------------------------------------------|
        */
    case 1:
        cout << "Lego" << endl;
        break;
    case 2:
        cout << "Toy car" << endl;
        break;
    case 3:
        cout << "Doraemon doll" << endl;
        break;
    case 4:
        cout << "Movie ticket" << endl;
        break;
    case 5:
        cout << "Baseball gloves" << endl;
        break;
    case 6:
        cout << "Ukulele" << endl;
        break;
    case 7:
        cout << "Vitamin" << endl;
        break;
    case 8:
        cout << "Watch" << endl;
        break;
    case 9:
        cout << "Wallet" << endl;
        break;
    case 10:
        cout << "Denture" << endl;
        break;
    case 11:
        cout << "Pipe" << endl;
        break;
    case 12:
        cout << "Hair dye" << endl;
        break;
    }
}

#endif
