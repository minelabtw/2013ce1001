#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
using namespace std;

#ifndef A101201521_H
#define A101201521_H
class GiftSystem
{
public:
    GiftSystem();//constructor
    void setYear(int);//set the age
    void randColor();//change color
    void printColor();//print color
    void exchangeGift();//exchange gift
private:
    int year;//the range of age
    int color;//the color of the
    string giftlist[4][3];//the list of the gifts
};
#endif //A101201521_H

GiftSystem::GiftSystem()
{
    year=0;
    color=0;
    //first row 65<= age <=125
    giftlist[0][0]="Denture";
    giftlist[0][1]="Pipe";
    giftlist[0][2]="Hair dye";
    //second row 18<= age <65
    giftlist[1][0]="Vitamin";
    giftlist[1][1]="Watch";
    giftlist[1][2]="Wallet";
    //third row 12<= age <18
    giftlist[2][0]="Movie ticket";
    giftlist[2][1]="Baseball gloves";
    giftlist[2][2]="Ukulele";
    //fourth row 0<= age <12
    giftlist[3][0]="Lego";
    giftlist[3][1]="Toy car";
    giftlist[3][2]="Doraemon doll";
}
void GiftSystem::setYear(int age)
{
    if(age<=125 && age>=65)//set year with different range of ages
        year=0;
    else if(age<65 && age>=18)
        year=1;
    else if(age<18 && age>=12)
        year=2;
    else if(age<12 && age>=0)
        year=3;
}
void GiftSystem::randColor()
{
    srand(time(0));
    color=rand()%3;//color is 0, 1 or 2
}
void GiftSystem::printColor()
{
    if(color==0)
        cout << "Red" << endl;//different number of color present different color
    else if(color==1)
        cout << "Green" << endl;
    else if(color==2)
        cout << "White" << endl;
}
void GiftSystem::exchangeGift()
{
    cout << giftlist[year][color] << endl;//print the gift
}
