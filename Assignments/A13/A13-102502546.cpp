#include <iostream>
#include "A13-102502546.h"//引入標頭檔

using namespace std ;

int main()
{
    int n,card ;
    GiftSystem gift ;

    cout << "How old are you?: " ;
    cin >> n ;//輸入年齡
    while(n>125||n<0)
    {
        cout << "Out of range!" << endl ;
        cout << "How old are you?: " ;
        cin >> n ;
    }
    gift.setYear(n) ;
    gift.randColor() ;
    gift.printColor() ;

    cout << "1)Exchange gift 2)Change color?: " ;
    cin >> card ;//輸入選項

    while(card!=1)
    {
        while(card<1 || card>2)
        {
            cout << "Out of range!" << endl ;
            cout << "1)Exchange gift 2)Change color?: " ;
            cin >> card ;
        }
        gift.randColor() ;
        gift.printColor() ;
        cout << "1)Exchange gift 2)Change color?: " ;
        cin >> card ;

        if(card==1)
        {
            break ;
        }

    }
    gift.exchangeGift() ;
    return 0 ;
}

