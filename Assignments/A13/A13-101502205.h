#ifndef A13_101502205_H
#define A13_101502205_H
#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

class GiftSystem{
public:
    GiftSystem();   //Constructor
    ~GiftSystem();  //Destructor
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
private:
    int year, card;
    static const int grade[4];
    static const char color[3][10];
    static const char gifts[12][20];
};
const int GiftSystem::grade[4] = {65, 18, 12, 0}; //Age (year) Condition
const char GiftSystem::color[3][10] = {"Red", "Green", "White"};    //string
const char GiftSystem::gifts[12][20] = {"Denture", "Pipe", "Hair dye", "Vitamin", "Watch", "Wallet", "Movie ticket", "Baseball gloves", "Ukulele", "Lego", "Toy car", "Doraemon doll"};

GiftSystem::GiftSystem(){
    //initialize
    year = 0;
    srand(time(NULL));
}
GiftSystem::~GiftSystem(){
}
void GiftSystem::setYear(int n){
    if(n<0)
        n=0;    //set 0 if n<0, if n<0 the following loop won't end
    for(year=0; n<grade[year]; year++);
    //test age, age>=65 y==0; age<65 y==1; age<18 y==2; age<12 y==3;
}
void GiftSystem::randColor(){
    card = rand()%3;    //rand a color0~2;
}
void GiftSystem::printColor(){
    cout << color[card] << endl;    //output the color (string)
}
void GiftSystem::exchangeGift(){
    cout << gifts[3*year+card] << endl;
    //Consider gifts as a two-dimensional array
}
#endif
