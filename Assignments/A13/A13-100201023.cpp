#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include "A13-100201023.h"

using namespace std;

int main()
{
	int year;

	srand(time(NULL));

	while(true) // input old year
	{
		cout << "How old are you?: ";
		cin >> year;

		if(0 <= year && year <= 125)
			break;

		cout << "Out of range!" << endl;
	}

	giftsystem present(year); // declare gift varience
	present.getcolor(); // output color

	while(true)
	{
		static int choice;
		cout << "1)Exchange gift 2)Change color?: ";
		cin >> choice;
		if(choice == 1)
		{
			present.changegift();
			present.getgift();
			break;
		}
		else if(choice == 2)
		{
			present.setcolor();
			present.getcolor();
		}
		else
			cout << "Out of range!" << endl;
	}

	system("pause");

	return 0;
}
