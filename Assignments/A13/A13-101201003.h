#include <stdlib.h>
#include <time.h>
using namespace::std;

class GiftSystem
{
public:
    int year,card;
    void setYear()
    {
        do
        {
            cout<<"How old are you?: ";
            cin>>year;
            if(year<0||year>125)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(year<0||year>125);
    }//設定年齡

    void randColor()
    {
        srand(time(NULL));
        card=(rand()%3)+1;
        if(card==1)
            cout<<"Red"<<endl;
        else if(card==2)
            cout<<"Green"<<endl;
        else if(card==3)
            cout<<"White"<<endl;

    }//使用rand()隨機抽色卡，並顯示出來

    void exchangeGift()
    {
        if(12>year&&year>=0&&card==1)
            cout<<"Lego";
        else if(12>year&&year>=0&&card==2)
            cout<<"Toy car";
        else if(12>year&&year>=0&&card==3)
            cout<<"Doraemon doll";
        else if(18>year&&year>=12&&card==1)
            cout<<"Movie ticket";
        else if(18>year&&year>=12&&card==2)
            cout<<"Baseball gloves";
        else if(18>year&&year>=12&&card==3)
            cout<<"Ukulele";
        else if(65>year&&year>=18&&card==1)
            cout<<"Vitamin";
        else if(65>year&&year>=18&&card==2)
            cout<<"Watch";
        else if(65>year&&year>=18&&card==3)
            cout<<"Wallet";
        else if(125>=year&&year>=65&&card==1)
            cout<<"Denture";
        else if(125>=year&&year>=65&&card==2)
            cout<<"Pipe";
        else if(125>=year&&year>=65&&card==3)
            cout<<"Hair dye";
    }//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
};
