#ifndef A13-102502053_H_INCLUDED
#define A13-102502053_H_INCLUDED
#include <iostream>// C++ classes
#include <cstdlib>
#include <ctime>
using namespace std;

class GiftSystem
{
public:
    //arange the input in appropriate class
    void setYear(int x)
    {
        if(x>=65&&x<=125)
        {
            y=1;
        }
        else if(x>=18&&x<65)
        {
            y=2;
        }
        else if(x>=12&&x<18)
        {
            y=3;
        }
        else if(x>=0&&x<12)
        {
            y=4;
        }
    }

    void randColor()//random and display a colour
    {
        srand(time(0));
        r=1+rand()%3; //random from 1-3
        switch(r)
        {
        case 1:
            cout<<"Red"<<endl;
            break;
        case 2:
            cout<<"Green"<<endl;
            break;
        case 3:
            cout<<"White"<<endl;
            break;
        default:
            break;
        }
    }

    void exchangeGift() //match the age class and colour with appropriate gift
    {
        switch(y)
        {
        case 1: //age class 1
            if(r==1)
            {
                cout<<"Denture";
            }
            else if(r==2)
            {
                cout<<"Pipe";
            }
            else if(r==3)
            {
                cout<<"Hair dye";
            }
            break;
        case 2://class 2
            if(r==1)
            {
                cout<<"Vitamin";
            }
            else if(r==2)
            {
                cout<<"Watch";
            }
            else if(r==3)
            {
                cout<<"Wallet";
            }
            break;
        case 3://class 3
            if(r==1)
            {
                cout<<"Movie ticket";
            }
            else if(r==2)
            {
                cout<<"Baseball gloves";
            }
            else if(r==3)
            {
                cout<<"Ukulele";
            }
            break;
        case 4: //class 4
            if(r==1)
            {
                cout<<"Lego";
            }
            else if(r==2)
            {
                cout<<"Toy car";
            }
            else if(r==3)
            {
                cout<<"Doraemon doll";
            }
            break;
        default:
            break;
        }
    }
private:
    //call variables
    int y;//class of year
    int r;//random of number
};


#endif // A13-102502053_H_INCLUDED
