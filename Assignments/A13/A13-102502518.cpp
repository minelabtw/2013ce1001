#include <iostream>
#include <cstdlib>
#include "A13-102502518.h"
using namespace std;
int main()
{
    GiftSystem g;
    int age=-1;
    do
    {
        cout<<"How old are you?: ";//讓使用者輸入年齡
        cin>>age;
        if (age<0||age>125)
            cout<<"Out of range!"<<endl;
    }
    while(age<0||age>125);
    g.setYear(age);
    g.randColor();
    g.printColor();
    int choice=0;
    do
    {
        cout<<"1)Exchange gift 2)Change color?: ";//使用者可以選擇 1)兌換禮物 2)更換色卡
        cin>>choice;
        switch (choice)
        {
        case 1:
            g.exchangeGift();//更換色卡
            break;
        case 2:
            g.randColor();//隨機抽取色卡
            g.printColor();//顯示出抽到的色卡
            break;
        default:
            cout<<"Out of range!"<<endl;
            break;
        }
    }
    while (choice!=1);
    return 0;
}
