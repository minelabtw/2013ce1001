#ifndef A13-102502035_H_INCLUDED
#define A13-102502035_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
class GiftSystem//宣告class
{
public:
    GiftSystem ();//宣告menber function
    void setYear (int);
    void randColor ();
    void printColor ();
    void exchangeGift ();
private:
    int year;//宣告變數
    int yearrange;
    int color;
    int choice;
};
GiftSystem::GiftSystem ()//建構子
{
    year =0;//=0
    yearrange =0;
    color =0;
    choice =0;
}
void GiftSystem::setYear (int year)
{
    while (year <0 || year >125)//將傳入的year做判斷
    {
        cout << "Out of range!" << endl << "How old are you?: ";//提示輸入
        cin >> year;//輸入
    }
    if (65 <=year && year <=125)//給yaer分段
        yearrange =0;
    else if (18 <=year && year <65)
        yearrange =1;
    else if (12 <=year && year <18)
        yearrange =2;
    else if (0 <=year && year <12)
        yearrange =3;
}
void GiftSystem::randColor ()//抽球函式
{
    srand (time (0));
    color =rand() %3;
}
void GiftSystem::printColor ()//輸出顏色函式
{
    randColor ();//呼叫抽球函式
    switch (color)//輸出顏色
    {
    case 0:
        cout << "Red";
        break;
    case 1:
        cout << "Green";
        break;
    case 2:
        cout << "White";
        break;
    default:
        break;
    }

    cout << endl;
}
void GiftSystem::exchangeGift ()
{
    printColor ();//呼叫輸出顏色函式
    while (choice !=1)//若要求換球則重複
    {
        cout << "1)Exchange gift 2)Change color?: ";//提示輸入
        cin >> choice;//輸入
        while (choice !=1 && choice !=2)
        {
            cout << "Out of range!" << endl << "1)Exchange gift 2)Change color?: ";//提示輸入
            cin >> choice;//輸入
        }
        if (choice ==2)
        {
            printColor ();//呼叫輸出顏色函式
        }
    }
    switch (yearrange)//輸出禮物
    {
    case 0:
        switch (color)
        {
        case 0:
            cout << "Denture";
            break;
        case 1:
            cout << "Pipe";
            break;
        case 2:
            cout << "Hair dye";
            break;
        default:
            break;
        }
        break;
    case 1:
        switch (color)
        {
        case 0:
            cout << "Vitamin";
            break;
        case 1:
            cout << "Watch";
            break;
        case 2:
            cout << "Wallet";
            break;
        default:
            break;
        }
        break;
    case 2:
        switch (color)
        {
        case 0:
            cout << "Movie ticket";
            break;
        case 1:
            cout << "Baseball gloves";
            break;
        case 2:
            cout << "Ukulele";
            break;
        default:
            break;
        }
        break;
    case 3:
        switch (color)
        {
        case 0:
            cout << "Lego";
            break;
        case 1:
            cout << "Toy car";
            break;
        case 2:
            cout << "Doraemon doll";
            break;
        default:
            break;
        }
        break;
    }
}
#endif // A13-102502035_H_INCLUDED
