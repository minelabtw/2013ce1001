#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;

class GiftSystem
{
public:
    int year,color;//宣告年紀以及顏色變數
    void setYear()//設定年紀的函數
    {
        do//請使用者輸入年紀，如果超出範圍則重新輸入
        {
            cout<<"How old are you?: ";
            cin>>year;
            if(year<0||125<year)
                cout<<"Out of range!"<<endl;
        }
        while(year<0||125<year);
    }

    void randColor()//隨機產生顏色的函數
    {
        srand(time(NULL));
        color=(rand()%3)+1;//使顏色變數為1~3的隨機數，1=Red,2=Green,3=White
    }

    void printColor()//輸出顏色
    {
        if(color==1)
            cout<<"Red";
        else if(color==2)
            cout<<"Green";
        else if(color==3)
            cout<<"White";
        cout<<endl;
    }

    void exchangeGift() //很冗的輸出禮物
    {
        if(12>year&&year>=0&&color==1)
            cout<<"Lego";
        else if(12>year&&year>=0&&color==2)
            cout<<"Toy car";
        else if(12>year&&year>=0&&color==3)
            cout<<"Doraemon doll";
        else if(18>year&&year>=12&&color==1)
            cout<<"Movie ticket";
        else if(18>year&&year>=12&&color==2)
            cout<<"Baseball gloves";
        else if(18>year&&year>=12&&color==3)
            cout<<"Ukulele";
        else if(65>year&&year>=18&&color==1)
            cout<<"Vitamin";
        else if(65>year&&year>=18&&color==2)
            cout<<"Watch";
        else if(65>year&&year>=18&&color==3)
            cout<<"Wallet";
        else if(125>=year&&year>=65&&color==1)
            cout<<"Denture";
        else if(125>=year&&year>=65&&color==2)
            cout<<"Pipe";
        else if(125>=year&&year>=65&&color==3)
            cout<<"Hair dye";
    }
} gift;
