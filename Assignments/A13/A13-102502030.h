#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

#ifndef A13-102502030_H_INCLUDED
#define A13-102502030_H_INCLUDED

class GiftSystem  //定義
{
public:
    GiftSystem();
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
private:
    string gift;
    int age;
    int card;
};
#endif // A13-102502030_H_INCLUDED

GiftSystem::GiftSystem()
{
    age=0;
    card=0;
}
void GiftSystem::setYear(int x)  //輸入年紀
{
    do
    {
        cout << "How old are you?: ";
        cin >> x;
    }
    while(x>125 && cout << "Out of range!" << endl || x<0 && cout << "Out of range!" << endl);
    if(x<=125 && x>=65)
        age=1;
    else if(x<65 && x>=18)
        age=2;
    else if(x<18 && x>=12)
        age=3;
    else if(x<12 && x>=0)
        age=4;
}
void GiftSystem::randColor()  //抽卡
{
    srand(time(NULL));
    card=(rand()%3)+1;
}
void GiftSystem::printColor()  //顯示卡片
{
    if(card==1)
        cout << "Red" << endl;
    else if(card==2)
        cout << "Green" << endl;
    else if (card==3)
        cout << "White" << endl;
}
void GiftSystem::exchangeGift()  //換禮物
{
    if(age==1 && card==1)
        cout << "Denture";
    if(age==1 && card==2)
        cout << "Pipe";
    if(age==1 && card==3)
        cout << "Hair dye";
    if(age==2 && card==1)
        cout << "Vitamin";
    if(age==2 && card==2)
        cout << "Watch";
    if(age==2 && card==3)
        cout << "Wallet";
    if(age==3 && card==1)
        cout << "Movie ticket";
    if(age==3 && card==2)
        cout << "Baseball gloves";
    if(age==3 && card==3)
        cout << "Ukulele";
    if(age==4 && card==1)
        cout << "Lego";
    if(age==4 && card==2)
        cout << "Toy car";
    if(age==4 && card==3)
        cout << "Doraemon doll";
}

