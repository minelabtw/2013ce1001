#include<iostream>
#include"A13-102502005.h"
using namespace std;

int main()
{
    int year,status;                //宣告即將要用到的兩個變數。
    GiftSystem GS;                  //產生GS這個物件。

    cout << "How old are you?: ";   //要求使用者輸入年齡，若年齡不符範圍則要求重新輸入。
    cin >> year;
    while(year<0 || year>125)
    {
        cout << "Out of range!" << endl << "How old are you?: ";
        cin >> year;
    }

    GS.setYear(year);               //將正確的年齡寫入GS的data裡，並計算年齡範圍。
    GS.randColor();                 //隨機產生色卡。
    GS.printColor();                //印出色卡顏色。
    cout << "1)Exchange gift 2)Change color?: ";//要求使用者更換顏色或印出禮物，若輸出不符範圍則要求重新輸入，若要求更換色卡則重新產生一次色卡。
    cin >> status;
    while(status!=1)
    {
        if(status!=2 && status!=1)
        {
            cout << "Out of range!" << endl << "1)Exchange gift 2)Change color?: ";
            cin >> status;
        }

        else if(status == 2)
        {
            GS.randColor();
            GS.printColor();
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> status;
        }
    }
    GS.exchangeGift();
    return 0;
}
