#ifndef A13-102502024_H_INCLUDED
#define A13-102502024_H_INCLUDED
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;
class GiftSystem  //宣告class
{
public:
    GiftSystem() {} //宣告四個函式
    void setYear();
    void randColor();
    void printColor();
    void exchangeGift();
private:
    int color=0;  //宣告顏色和年齡
    int YEAR=0;
};
void GiftSystem::setYear()  //設定年齡
{
    cout<<"How old are you?: ";
    cin>>YEAR;
    while(YEAR>125 || YEAR<0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
        cin>>YEAR;
    }
    if(YEAR<=125  && YEAR>=65)  //分成四個區域
    {
        YEAR=1;
    }
    else if(YEAR<65  && YEAR>=18)
    {
        YEAR=2;
    }
    else if(YEAR<18  && YEAR>=12)
    {
        YEAR=3;
    }
    else if(YEAR<12  && YEAR>=0)
    {
        YEAR=4;
    }
}
void GiftSystem::randColor()  //隨機產生顏色
{
    srand(time(0));
    color=(rand()%3) +1;
}
void GiftSystem::printColor()  //輸出顏色
{
    switch(color)
    {
    case 1 :
        cout<<"Red"<<endl;
        break;
    case 2 :
        cout<<"Green"<<endl;
        break;
    case 3 :
        cout<<"White"<<endl;
        break;
    }
}
void GiftSystem::exchangeGift()  //輸出相對應的禮物
{
    if(color==1 && YEAR==1)
    {
        cout<<"Denture";
    }
    if(color==1 && YEAR==2)
    {
        cout<<"Vitamin";
    }
    if(color==1 && YEAR==3)
    {
        cout<<"Movie ticket";
    }
    if(color==1 && YEAR==4)
    {
        cout<<"Lego";
    }
    if(color==2 && YEAR==1)
    {
        cout<<"Pipe";
    }
    if(color==2 && YEAR==2)
    {
        cout<<"Watch";
    }
    if(color==2 && YEAR==3)
    {
        cout<<"Baseball gloves";
    }
    if(color==2 && YEAR==4)
    {
        cout<<"Toy car";
    }
    if(color==3 && YEAR==1)
    {
        cout<<"Hair dye";
    }
    if(color==3 && YEAR==2)
    {
        cout<<"Wallet";
    }
    if(color==3 && YEAR==3)
    {
        cout<<"Ukulele";
    }
    if(color==3 && YEAR==4)
    {
        cout<<"Doraemon doll";
    }
}
#endif // A13-102502024_H_INCLUDED
