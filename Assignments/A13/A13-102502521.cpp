#include "A13-102502521.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{
    GiftSystem g;    //object g of GiftSystem
    int year=0;
    int choice=2;
    srand(time(0));

    do    //輸入年齡
    {
        cout<<"How old are you?: ";
        cin>>year;
        if(year>125||year<0)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(year>125||year<0);

    g.setYear(year);

    do    //換顏色或兌換禮物
    {
        g.randColor();
        if(choice==2)
        {
            g.printColor();
        }
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>choice;
        if(choice!=1&&choice!=2)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(choice!=1);

    g.exchangeGift();    //判斷+列印禮物

    return 0;
}
