#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
#ifndef GiftSystem_H
#define GiftSystem_H

class GiftSystem
{
private:
    int year;//定義
    int color;

public:
    GiftSystem();//實際內容
    void setYear(int);

    void randColor();

    void printColor();

    void exchangeGift();
};


GiftSystem::GiftSystem()
{
    year=color=0;
}
void GiftSystem::setYear(int year)
{
    this->year=year;
}

void GiftSystem::randColor()
{
    color=rand()%3;
}

void GiftSystem::printColor()//顯示顏色
{
    if (color==0)
    {
        cout<<"Red";
    }
    if (color==1)
    {
        cout<<"Green";
    }
    if (color==2)
    {
        cout<<"White";
    }
}

void GiftSystem::exchangeGift()//顯示兌換
{
    if (65<=year&&year<=125)
    {
        if(color==0)
        {
            cout<<"Denture";
        }
        else if(color==1)
        {
            cout<<"Pipe";
        }
        else if(color==2)
        {
            cout<<"Hair dye";
        }
    }
    if (18<=year&&year<=65)
    {
        if(color==0)
        {
            cout<<"Vitamin";
        }
        else if(color==1)
        {
            cout<<"Watch";
        }
        else if(color==2)
        {
            cout<<"Wallet";
        }
    }
    if (12<=year&&year<=18)
    {
        if(color==0)
        {
            cout<<"Movie ticket";
        }
        else if(color==1)
        {
            cout<<"Baseball gloves";
        }
        else if(color==2)
        {
            cout<<"Uklele";
        }
    }
    if (0<=year&&year<=12)
    {
        if(color==0)
        {
            cout<<"Lego";
        }
        else if(color==1)
        {
            cout<<"Toy car";
        }
        else if(color==2)
        {
            cout<<"Doraemon doll";
        }
    }
}

#endif // GiftSystem_h
