#include"A13-102502530.h"

int main()
{
   int year,mode=2;  //declare
   GiftSystem gift;

   do    //set year
   {
      std::cout<<"How old are you?: ";
      std::cin>>year;
      if(year<0||year>125)
         std::cout<<"Out of range!"<<std::endl;
   }while(year<0||year>125);
   gift.setYear(year);

   do    //change color
   {
      if(mode==2)
      {
         gift.randColor();
         gift.printColor();
      }
      else
         std::cout<<"Out of range!"<<std::endl;
      std::cout<<"1)Exchange gift 2)Change color?: ";
      std::cin>>mode;
   }while(mode!=1);

   gift.exchangeGift();    //exchange gift

   return 0;   //exit
}
