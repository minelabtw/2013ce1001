#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "A13-102502018.h"
using namespace std;
int main()
{
    int year,gift;
    srand(time(NULL));       //亂數
    GiftSystem t;            //宣告
    while(year<0||year>125)
    {
        cout<<"How old are you?: ";
        cin>>year;
        if(year<0||year>125)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    t.setYear(year);       //使用函式
    do
    {
        t.randColor();          //隨機選顏色
        t.printColor();         //輸出顏色
        cout<<endl;
        do
        {
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>gift;
            if(gift<1||gift>2)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(gift<1||gift>2);
        }
    while(gift==2);
        t.exchangeGift();    //判斷禮物是哪個

    return 0;
}
