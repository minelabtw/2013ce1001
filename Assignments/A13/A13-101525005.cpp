#include <iostream>
#include <cstdlib>
#include <ctime>
#include "A113.h"

using namespace std;
int main()
{
    int year=0,chooseNum=0;
    GiftSystem gs;
    //輸入年齡
    do
    {
        cout << "How old are you?: " ;
        cin>>year;
        if(year<1 || year>125)
        {
            cout << "Out of range!" << endl;
        }
        else
        {
            year=year;
            gs.setYear(year);
        }
    }
    while(year<1 || year>125);
    //發送顏色卡
    gs.randColor();
    gs.printColor();

    //詢問是否兌換顏色
    do
    {
        cout << "1)Exchange gift 2)Change color?: " ;
        cin>>chooseNum;
        if(chooseNum<1 || chooseNum>2)
        {
            cout << "Out of range!" << endl;
        }
        else if(chooseNum==1)
        {
            //兌換禮物

            gs.exchangeGift();
        }
        else if(chooseNum==2)
        {
            //交換顏色
            gs.randColor();
            gs.printColor();
        }
        else
        {
            cout << "!!!!!!!!!!!!!!!???????????" ;
        }
    }
    while(chooseNum!=1);


    return 0;
}
