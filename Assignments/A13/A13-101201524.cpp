#include<iostream>
#include"A13-101201524.h"//加入自定的類別
using namespace std;

int main()
{
    int year,choose;//宣告變數year代表年齡,choose代表選項
    GiftSystem GS;//宣告一類別執行 "A13-101201524.h" 內的指令

    do//詢問年齡若不合則要求重新輸入,符合則用 GS.setYear(year) 設定年齡區間
    {
        cout << "How old are you?: ";
        cin >> year;
        if(year<0 || year>125)
            cout << "Out of range!" << endl;
        else
            GS.setYear(year);
    }while(year<0 || year>125);

    do//輸出隨機選到的色卡,詢問是換禮物還是重選,若重選則重新執行,若換禮物則依年齡和卡片顏色輸出對應到的禮物
    {
        GS.randColor();
        GS.printColor();
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> choose;
        if(choose!=1 && choose!=2)
            cout << "Out of range!" << endl;
        if(choose==1)
            GS.exchangeGift();
    }while(choose!=1);

    return 0;
}
