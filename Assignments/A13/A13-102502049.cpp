#include <iostream>
#include <ctime>
#include <cstdlib>
#include "A13-102502049.h"
using namespace std;

int main()
{
    GiftSystem GS;
    int years; //年齡
    int mode; //換色或換禮物

    do //要求輸入年齡
    {
        cout << "How old are you?: ";
        cin >> years;
    }
    while((years<0||years>125) && cout << "Out of range!" << endl);

    GS.setYear(years); //設定年齡
    GS.yeartonum(); //轉換年齡成數值
    GS.setstring(); //設定禮物名


    do
    {
        GS.randColor(); //隨機亂數
        GS.printColor(); //印書對應顏色

        cout << "1)Exchange gift 2)Change color?: ";
        cin >> mode; //選擇模式

        if(mode!=1&&mode!=2)
            cout << "Out of range!" << endl;
    }
    while(mode==2||mode!=1);

    GS.exchangeGift(); //印出禮物名稱

    return 0;
}
