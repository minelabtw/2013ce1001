#include "A13-101201519.h"//include"A13-101201519.h"檔

int main()
{
    srand(time(NULL));//亂數種子

    GiftSystem gs;
    int year,select;

    cout << "How old are you?: ";
    cin >> year;
    gs.setYear(year);
    gs.randColor();
    gs.printColor();

    while(1)
    {
        select=0;
        while(select<1 || select>2)//輸入1或2
        {
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> select;
            if(select<1 || select>2)
                cout << "Out of range!" << endl;
        }
        if(select==1)//輸入1時輸出獲得的禮物
        {
            break;
        }
        else if(select==2)//輸入2時再隨機生成一種顏色
        {
            gs.randColor();
            gs.printColor();
        }

    }
    gs.exchangeGift();

    return 0;
}
