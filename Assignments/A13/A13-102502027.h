#ifndef GiftSystem_H
#define GiftSystem_H
#include <cstdlib>
#include <iostream>
using namespace std;

class GiftSystem
{
public:

	GiftSystem()
    {
        year = 0;
		color = rand()%3+1;
    }
    void setYear(int y)
    {
        year = y;
    }
    void randColor()
    {
        int v =rand()%3+1;
        color = v;
    }
    void printColor()
    {
		if(color==1)
            cout<<"White"<<endl;
        if(color==2)
            cout<<"Red"<<endl;
        if(color==3)
            cout<<"Green"<<endl;

    }
    void exchangeGift()
    {
        if(65<= year && year<=125)
        {
            if(color==1)
                cout<<"Hair dye"<<endl;
            if(color==2)
                cout<<"Denture"<<endl;
            if(color==3)
                cout<<"Pipe	"<<endl;
        }
        else if(18<=year && year<65)
        {
            if(color==1)
                cout<<"Wallet"<<endl;
            if(color==2)
                cout<<"Vitamin"<<endl;
            if(color==3)
                cout<<"Watch"<<endl;
        }

        else if(12<=year && year<18)
        {
            if(color==1)
                cout<<"Ukulele"<<endl;
            if(color==2)
                cout<<"Movie ticket"<<endl;
            if(color==3)
                cout<<"Baseball gloves"<<endl;
        }
        else if(0<=year && year<12)
        {
            if(color==1)
                cout<<"Doraemon doll"<<endl;
            if(color==2)
                cout<<"Lego"<<endl;
            if(color==3)
                cout<<"Toy car"<<endl;
        }
    }

private:
    int year;
	int color;
};
#endif

