#include <iostream>
#include <cstdlib>
using namespace std;

#ifndef A13-102502517_H_INCLUDED
#define A13-102502517_H_INCLUDED

class GiftSystem
{
public:
    GiftSystem();
    void setYear(); //輸入年紀
    void randColor(); //隨機產生顏色
    void printColor(); //輸出顏色
    void exchangeGift(); //選擇交換禮物或變更顏色
private:
    int age;
    int color;
    int choice;
};

GiftSystem::GiftSystem()
{
    age=color=choice=0; //初始化所有變數
}

void GiftSystem::setYear()
{
    do
    {
        cout << "How old are you?: ";
        cin >> age; //輸入年紀
        if (age<0 or age>125)
            cout << "Out of range!" << endl;
    }
    while (age<0 or age>125);
}

void GiftSystem::randColor()
{
    color = rand()%3; //隨機產生0到2的數字
}

void GiftSystem::printColor()
{
    randColor();
    if (color==0) //依照數字決定顏色
        cout << "Red" << endl;
    else if (color==1)
        cout << "Green" << endl;
    else if (color==2)
        cout << "White" << endl;
}

void GiftSystem::exchangeGift()
{
    do
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> choice;

        switch (choice)
        {
        case 1: //結束迴圈並輸出禮物
            break;
        case 2: //變更顏色
            printColor();
            break;
        default: //輸入不再範圍內
            cout << "Out of range!" << endl;
            break;
        }
    }
    while (choice!=1);

    if (age>=65 and color==0) //輸出禮物
        cout << "Denture";
    else if (age>=65 and color==1)
        cout << "Pipe";
    else if (age>=65 and color==2)
        cout << "Hair dye";
    else if (age>=18 and age<65 and color==0)
        cout << "Vitamin";
    else if (age>=18 and age<65 and color==1)
        cout << "Watch";
    else if (age>=18 and age<65 and color==2)
        cout << "Wallet";
    else if (age>=12 and age<18 and color==0)
        cout << "Movie ticket";
    else if (age>=12 and age<18 and color==1)
    cout << "Baseball gloves";
    else if (age>=12 and age<18 and color==2)
        cout << "Ukulele";
    else if (age>=0 and age<12 and color==0)
        cout << "Lego";
    else if (age>=0 and age<12 and color==1)
        cout << "Toy car";
    else if (age>=0 and age<12 and color==2)
        cout << "Doraemon doll";
}

#endif // A13-102502517_H_INCLUDED
