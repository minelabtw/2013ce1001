#ifndef A13_H_INCLUDED
#define A13_H_INCLUDED

#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

class GiftSystem
{
public:                                                 //宣告函式名稱
    GiftSystem();
    void setYear();
    void randColor();
    void printColor();
    void exchangeGift();
private:                                                //宣告所有變數
    int age;
    int color;
    int exchange;
};

GiftSystem::GiftSystem()                                //設定變數初始值為0
{
    age = 0;
    color = 0;
    exchange = 0;
}
void GiftSystem::setYear()                              //輸入年齡函式
{
    cout << "How old are you?: ";
    while ( cin >> age )                                     //當年齡在範圍外時輸出字彙並重新輸入
    {
        if ( age > 125 || age < 0 )
        {
            cout << "Out of range!" << endl;
            cout << "How old are you?: ";
        }
        else
            break;
    }
}
void GiftSystem::randColor()                            //隨機選取顏色函式
{
    srand(time(NULL));
    color = (rand()%3);                                       //以0.1.2代表三種顏色
}
void GiftSystem::printColor()                           //將數字轉換成顏色輸出函式
{
    if ( color == 0 )
        cout << "Red" << endl;
    else if ( color == 1 )
        cout << "Green" << endl;
    else if ( color == 2 )
        cout << "White" << endl;
}
void GiftSystem::exchangeGift()                         //交換禮物函式
{
    while( cout << "1)Exchange gift 2)Change color?: " )
    {
        cin >> exchange;
        if( exchange == 1 )                                        //輸入1的話做禮物的判斷
            break;
        else if( exchange == 2 )                                   //輸入2的話進行重新隨機顏色和輸出顏色的函式
        {
            randColor();
            printColor();
        }
        else
            cout << "Out of range!" << endl;            //輸入其他數字則輸出字彙並重新輸入
    }

    switch( color )                                           //選擇顏色之後再依年齡分配禮物
    {
    case 0:                                             //紅色禮物
        if( age <= 125 && age >= 65 )
            cout << "Denture" << endl;
        else if( age < 65 && age >= 18 )
            cout << "Vitamin" << endl;
        else if( age < 18 && age >= 12 )
            cout << "Movie ticket" << endl;
        else if( age < 12 && age >= 0 )
            cout << "Lego" << endl;
        break;
    case 1:                                             //綠色禮物
        if( age <= 125 && age >= 65 )
            cout << "Pipe" << endl;
        else if( age < 65 && age >= 18 )
            cout << "Watch" << endl;
        else if( age < 18 && age >= 12 )
            cout << "Baseball gloves" << endl;
        else if( age <12 && age >= 0 )
            cout << "Toy car" << endl;
        break;
    case 2:                                             //白色禮物
        if( age <= 125 && age >= 65 )
            cout << "Hair dye" << endl;
        else if( age < 65 && age >= 18 )
            cout << "Wallet" << endl;
        else if( age < 18 && age >= 12 )
            cout << "Ukulele" << endl;
        else if( age < 12 && age >= 0 )
            cout << "Doraemon doll" << endl;
        break;
    }
}

#endif
