// 102502023.h
#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

class GiftSystem
{
public:
    GiftSystem();
    void setYear(int );
    void randColor();
    void printColor();
    void exchangeGift();
private: // initialize integers year and color
    int year;
    int color;
};
GiftSystem::GiftSystem()
{
    year = 0; // sign 0 to year and color
    color = 0;
}
void GiftSystem::setYear( int y )
{
    year = y;
}
void GiftSystem::randColor()
{
    srand( time( 0 ) );
    color = rand() % 3;
}
void GiftSystem::printColor()
{
    if ( color == 0 )
        cout << "Red" << endl;
    if ( color == 1 )
        cout << "Green" << endl;
    if ( color == 2 )
        cout << "White" << endl;
}
void GiftSystem::exchangeGift()
{
    if ( year <= 125 && year >= 65 && color == 0 )
        cout << "Denture";
    if ( year <= 125 && year >= 65 && color == 1 )
        cout << "Pipe";
    if ( year <= 125 && year >= 65 && color == 2 )
        cout << "Hair dye";
    if ( year < 65 && year >= 18 && color == 0 )
        cout << "Vitamin";
    if ( year < 65 && year >= 18 && color == 1 )
        cout << "Watch";
    if ( year < 65 && year >= 18 && color == 2 )
        cout << "Wallet";
    if ( year < 18 && year >= 12 && color == 0 )
        cout << "Movie ticket";
    if ( year < 18 && year >= 12 && color == 1 )
        cout << "Baseball gloves";
    if ( year < 18 && year >= 12 && color == 2 )
        cout << "Ukulele";
    if ( year < 12 && year >= 0 && color == 0 )
        cout << "Lego";
    if ( year < 12 && year >= 0 && color == 1 )
        cout << "Toy car";
    if ( year < 12 && year >= 0 && color == 2 )
        cout << "Doraemon doll";
}
#endif // GIFTSYSTEM_H
