#include <iostream>
#include <cstdlib>
#include <ctime>
#include "A13-102502535.h"

using namespace std ;

int main()
{
    GiftSystem gift ;

    int age = -1 ;
    int change = 0 ;  //設兩變數。

    do
    {
        cout << "How old are you?: " ;
        cin >> age ;

        if ( age < 0 || age > 125 )
            cout << "Out of range!" << endl ;
    }
    while ( age < 0 || age > 125 ) ;  //使輸入年齡，並判斷是否合理。

    gift.setYear( age ) ;
    gift.randColor() ;
    gift.printColor() ;

    do
    {
        cout << "1)Exchange gift 2)Change color?: " ;
        cin >> change ;

        switch ( change )
        {
        case 1:
            gift.exchangeGift() ;
            break ;

        case 2:
            gift.randColor() ;
            gift.printColor() ;
            break ;

        default:
            cout << "Out of range!" << endl ;
            break ;
        }
    }
    while ( change != 1 ) ;  //使操作者選擇要領禮物還是換顏色。

    return 0;
}
