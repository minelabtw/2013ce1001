#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

#ifndef GIFT_H
#define GIFT_H

string colors[3]={"Red","Green","White"};

string gifts[4][3]={
	{"Lego","Toy car","Doraemon doll"},
	{"Movie ticket","Baseball gloves","Ukulele"},
	{"Vitamin","Watch","Wallet"},
	{"Denture","Pipe","Hair dye"}
};

class GiftSystem
{
public:
	GiftSystem();			//constructer
	void setAge(int);		//set age
	void randColor();		//set random color
	void printColor();		//print out the color
	void exchangeGift();	//exchange the gift
private:
	int age;
	int color;
};

GiftSystem::GiftSystem(){
	srand(time(NULL));
	randColor();
}

void GiftSystem::setAge(int year){
	age=year;
}

void GiftSystem::randColor(){
	color=rand()%3;
}

void GiftSystem::printColor(){
	cout << colors[color] << endl;
}

void GiftSystem::exchangeGift(){
	int agerange=
	(age<0) ? -1 :
	(age<12) ? 0 :
	(age<18) ? 1 :
	(age<65) ? 2 :
	(age<=125) ? 3 :
	-1;
	cout << gifts[agerange][color] << endl;
}
#endif
