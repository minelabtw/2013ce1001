#include <iostream>
#include "A13-102502558.h"
using namespace std;

// input function
void input(const char *prompt, const char *error, int &n)
{
    do
    {
        cout << prompt;
        cin >> n;
    } while ((n < 0 || n > 125) && cout << error << endl);
}

int main(int argc, char *argv[])
{
    srand(time(NULL)); // init random
    int year = 0, choose = 0; // variables
    GiftSystem GS; // declare a giftsystem
    input("How old are you?: ","Out of range!",year); // get year
    GS.setYear(year); // set year
    GS.printColor();  // print color

    while (choose != 1)
    {
        do
        {
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> choose;
        } while ((choose != 1 && choose != 2) && cout << "Out of range!" << endl);
        // get input - choose
        if (choose == 2)
        {
            GS.randColor(); // new color
            GS.printColor(); // show color
        }
        else
        {
            GS.exchangeGift();
        }
    }
    return 0;
}
