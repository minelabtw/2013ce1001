#ifndef A13-102502509_H_INCLUDED
#define A13-102502509_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

class GiftSystem // 物件宣告
{
public:
    GiftSystem();// 宣告物件函式像main一樣
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();

private:
    int old; //宣告年齡變數
    int color;// 宣告顏色變數

};

GiftSystem::GiftSystem() // construct
{ // 初始值
    old = 0;
    color = 0;
}

void GiftSystem::setYear(int age) // 改變數名稱以免編譯器搞混
{
    do
    {
        cout << "How old are you?: ";
        cin >> age;
        if (age < 0 || age > 125)
        {
            cout << "Out of range!" << endl;
        }
        old = age;
    }
    while (age < 0 || age > 125);
}

void GiftSystem::randColor()
{

    srand ( time(0) );
    color = rand() % 3;

}

void GiftSystem::printColor()
{
    if (color == 0)
        cout << "White";
    if (color == 1)
        cout << "Green";
    if (color == 2)
        cout << "Red";
}

void GiftSystem::exchangeGift()
{
    switch (color)
    {
    case 0:
        if (old >= 0 && old < 12)
            cout << "Doraemon doll" << endl;

        if (old >= 12 && old < 18)
            cout << "Ukulele" << endl;

        if (old >= 18 && old < 65)
            cout << "Wallet" << endl;

        if (old >= 65 && old <= 125 )
            cout << "Hair dye" << endl;

            break;

    case 1 :
        if (old >= 0 && old < 12 )
            cout << "Toy car" << endl;

        if (old >= 12 && old < 18)
            cout << "Baseball gloves" << endl;

        if (old >= 18 && old < 65)
            cout << "Watch" << endl;

        if (old >= 65 && old <= 125)
            cout << "Pipe" << endl;

            break;

    case 2:
        if (old >= 0 && old < 12)
            cout << "Lego" << endl;

        if (old >= 12 && old < 18)
            cout << "Movie ticket" << endl;

        if (old >= 18 && old < 65)
            cout << "Vitamin" << endl;

        if (old >= 65 && old < 125)
            cout << "Denture" << endl;

            break;
    }
}

#endif // A13-102502509_H_INCLUDED


