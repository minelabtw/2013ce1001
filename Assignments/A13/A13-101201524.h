#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

#ifndef GIFTSYSTEM_H//檢查 GIFTSYSTEM_H 是否已存在
#define GIFTSYSTEM_H//若不存在則定義 GIFTSYSTEM_H

class GiftSystem//定義一類別
{
public://設定公開的程式
    GiftSystem();
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
private://設定不公開的程式
    int card;//卡片顏色
    int age;//年齡區間
    string gifts[4][3];//依年齡和卡片顏色對應到的禮物
};

GiftSystem::GiftSystem()//宣告起始值
{
    card=0;
    age=0;
    gifts[3][0]="Denture",gifts[3][1]="Pipe",gifts[3][2]="Hair dye";
    gifts[2][0]="Vitamin",gifts[2][1]="Watch",gifts[2][2]="Wallet";
    gifts[1][0]="Movie ticket",gifts[1][1]="Baseball gloves",gifts[1][2]="Ukulele";
    gifts[0][0]="Lego",gifts[0][1]="Tor car",gifts[0][2]="Doraemon doll";
}

void GiftSystem::setYear(int year)//依年齡判斷所屬於的年齡區間
{
    if(year>=0 && year<12)
        age=0;
    else if(year>=12 && year<18)
        age=1;
    else if(year>=18 && year<65)
        age=2;
    else
        age=3;
}

void GiftSystem::randColor()//用時間產生0~2的隨意數代表卡片顏色
{
    srand(time(0));
    card=rand() %3;
}

void GiftSystem::printColor()//依變數輸出其所代表的顏色
{
    if(card==0)
        cout << "Red" << endl;
    else if(card==1)
        cout << "Green" << endl;
    else
        cout << "White" << endl;
}

void GiftSystem::exchangeGift()//依年齡和卡片顏色輸出對應到的禮物
{
    cout << gifts[age][card];
)

#endif//結束定義
