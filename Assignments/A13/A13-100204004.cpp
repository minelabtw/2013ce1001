#include"A13-100204004.h"
int main()
{
    GiftSystem g;
    int year;//年齡
    int Choice=0;//選擇
    do
    {
        cout<<"How old are you?: ";
        cin>>year;
        if(year<0||year>125)
            cout<<"Out of range!"<<endl;
    }
    while(year<0||year>125);
    g.setYear(year);//設定年齡
    g.randColor();//隨機抽色卡
    g.printColor();//將抽到的色卡cout出來
    do//無限次數更換色卡直到兌換禮物為止
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>Choice;
        switch(Choice)
        {
        case 1:
            g.exchangeGift();//兌換禮物
            break;
        case 2:
            g.randColor();//更換色卡
            g.printColor();//將抽到的色卡cout出來
            break;
        default:
            cout<<"Out of range!"<<endl;
            break;
        }
    }
    while(Choice!=1);
    return 0;
}
