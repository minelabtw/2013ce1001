#ifndef GIFTSY_H//增加執行效率<1>
#define GIFTSY_H//增加執行效率<2>
#include <iostream>
#include <cstdlib>//亂數所需<1>
#include <ctime>//亂數所需<2>
using namespace std;


class GiftSystem//定義一個類別
{
public://定義在公開區間中有三個函數
    GiftSystem();
    void setYear();//輸入年齡
    void randColor();//隨機跑出卡片顏色，並可隨機換卡片顏色
    void exchangeGift();//依照年齡和顏色來兌換禮物
private://定義不會在main中出現的數
    int age;//年齡
    int cardcolor;//卡片顏色
    int change;//變換函數
};

GiftSystem::GiftSystem()//把變數設初始值為零
{
    age=cardcolor=change=0;
}

void GiftSystem::setYear()//定義在class中的第一個函式
{
    cout << "How old are you?: ";//輸出字串
    cin >> age;//輸入年齡
    while ( age>125 or age<0 )//用迴圈使得年齡範圍有限制
    {
        cout << "Out of range!" << endl;
        cout << "How old are you?: ";
        cin >> age;
    }
}

void GiftSystem::randColor()//定義在class中的第二個函式
{
    srand(time(0));//以time(0)做種子
    cardcolor = rand()%3+1;//把任何數除以3取餘數再加1，就能控制變數在1到3之間

    if ( cardcolor==1 )//用if else來判斷顏色
        cout << "Red" << endl;
    else if ( cardcolor==2 )
        cout << "Green" << endl;
    else if ( cardcolor==3 )
        cout << "White" << endl;

    cout << "1)Exchange gift 2)Change color?: ";//輸出字串，來詢問是否要變顏色
    cin >> change;

    while ( change!=1 && change!=2 )//當輸出不是1或2時輸出下列
    {
        cout << "Out of range!" << endl;
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> change;
    }

    while ( change==2 )//當要重抽顏色時
    {
        cardcolor = rand()%3+1;//重跑亂數

        if ( cardcolor==1 )//判斷顏色並輸出
            cout << "Red" << endl;
        else if ( cardcolor==2 )
            cout << "Green" << endl;
        else if ( cardcolor==3 )
            cout << "White" << endl;

        cout << "1)Exchange gift 2)Change color?: ";//再輸出字串
        cin >> change;
        while ( change!=1 && change!=2 )
        {
            cout << "Out of range!" << endl;
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> change;
        }
    }
}

void GiftSystem::exchangeGift()//定義在class中的第三個函式
{
    if ( age<=125 && age>=65 )//利用if else來判斷得到禮物是甚麼
    {
        if ( cardcolor==1 )
            cout << "Denture";
        else if ( cardcolor==2 )
            cout << "Pipe";
        else if ( cardcolor==3 )
            cout << "Hair dye";
    }
    else if ( age<65 && age>=18 )
    {
        if ( cardcolor==1 )
            cout << "Vitamin";
        else if ( cardcolor==2 )
            cout << "Watch";
        else if ( cardcolor==3 )
            cout << "Wallet";
    }
    else if ( age<18 && age>=12 )
    {
        if ( cardcolor==1 )
            cout << "Movie ticket";
        else if ( cardcolor==2 )
            cout << "Baseball gloves";
        else if ( cardcolor==3 )
            cout << "Ukulele";
    }
    else if ( age<12 && age>=0 )
    {
        if ( cardcolor==1 )
            cout << "Lego";
        else if ( cardcolor==2 )
            cout << "Toy car";
        else if ( cardcolor==3 )
            cout << "Doraemon doll";
    }
}

#endif;//增加執行效率<3>
