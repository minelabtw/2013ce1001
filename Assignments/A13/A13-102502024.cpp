#include<ctime>
#include<iostream>
#include<cstdlib>
#include "A13-102502024.h"
using namespace std;
int main()
{
    GiftSystem t;
    int motion=0;  //宣告變數
    t.setYear();  //設定年齡
    t.randColor();  //隨機產生顏色
    t.printColor();  //輸出顏色
    while(motion!=1)
    {
        cout<<"1)Exchange gift 2)Change color?: ";  //輸出題目
        cin>>motion;
        while(motion!=1 && motion!=2)
        {
            cout<<"Out of range!"<<endl;
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>motion;
        }
        if(motion==1)
        {
            t.exchangeGift();  //兌換禮物
        }
        if(motion==2)
        {
            t.randColor();  //改變顏色
            t.printColor();
        }
    }
    return 0;
}
