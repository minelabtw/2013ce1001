#include "GiftSystem.h"
#include <cstdlib>
#include <iostream>

using namespace std;


int main()
{
    GiftSystem gs; //這樣寫會呼叫的的是GiftSystem()這個constructor
    //GiftSystem gs(10,2); //這樣是呼叫GiftSystem(int y, int c)這個constructor
    //gs.printColor();

    int year,number;
    do
    {
        cout<<"How old are you?: ";
        cin>>year;
    }
    while((year<0 || year>125 ) && cout<<"Out of range!"<<endl);
    gs.setYear(year);
    while(true)
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>number;
        if(number==1)
        {
            gs.exchangeGift();
            break;
        }
        else if(number==2)
        {
            gs.randColor();
            gs.printColor();
        }
        else
            cout<<"Out of range!"<<endl;
    }

    return 0;
}
