#ifndef GiftSystem_h
#define GiftSystem_h

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class GiftSystem //定義 GiftSystem 這個 class
{
public:
    GiftSystem();
    void setYear( int ); //設置年齡的函數
    void randColor(); //隨機卡片顏色的函數
    void printColor(); //印出卡片顏色的函數
    void exchangeGift(); //交換禮物的函數
private:
    int year; //宣告一個 int 的變數，用來儲存年齡的值。
    string color; //宣告一個 string 的變數，用來儲存卡片顏色。
};

GiftSystem::GiftSystem() //constructor
{
    year = 0;
}

void GiftSystem::setYear( int age )
{
    cin >> age;
    while ( age < 0 || age > 125 ) //檢驗輸入的 age 的值是否合乎標準，若不符，則要求其重新輸入。
    {
        cout << "Out of range!" << endl;
        cout << "How old are you?: ";
        cin >> age;
    }
    year = age;
}

void GiftSystem::randColor()
{
    int num = 0;
    srand(time(NULL));
    num = rand() % 3;
    if ( num == 0 )
    {
        color = "Red";
    }
    else if ( num == 1 )
    {
        color = "Green";
    }
    else
    {
        color = "White";
    }
}

void GiftSystem::printColor()
{
    cout << color << endl;
}

void GiftSystem::exchangeGift()
{
    int yearold = 0;
    int card = 0;
    string gift[4][3] = {"Lego","Toy car","Doraemon doll","Movie ticket","Baseball gloves","Ukulele","Vitamin","Watch","Wallet","Denture","Pipe","Hair dye"};
    if ( year >= 0 && year < 12 )
    {
        yearold = 0;
    }
    else if ( year >= 12 && year < 18 )
    {
        yearold = 1;
    }
    else if ( year >= 18 && year < 65 )
    {
        yearold = 2;
    }
    else
    {
        yearold = 3;
    }
    if ( color == "Red" )
    {
        card = 0;
    }
    else if ( color == "Green" )
    {
        card = 1;
    }
    else
    {
        card = 2;
    }
    cout << gift[yearold][card] << endl;
}

#endif
