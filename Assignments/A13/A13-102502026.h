//A13-102502026
#ifndef A13-102502026_H_INCLUDED
#define A13-102502026_H_INCLUDED
#include<iostream>
#include<ctime> //use for srand(time())
#include<stdlib.h>  //use for rand
using namespace std;
class GiftSystem
{
public:
    GiftSystem();
    void setYear(); //prototype
    void Color();   //prototype
    void exchangeGift();    //prototype
private:
    int year;
    int color;
    int birth;
    int random;
    int gift;
};

GiftSystem::GiftSystem()
{
    birth=-1;   //to enter the while
    color=0;
    year=0;
    random=0;
    gift=0;
}
void GiftSystem::setYear()  //to ask and print the age of the person
{
    while (birth<0 || birth>125)
    {
        cout<<"How old are you?: ";
        cin>>birth;
        if(birth<0 || birth>125)
            cout<<"Out of Range!"<<endl;
    }
    if (birth<=125 && birth>=65)
        year=1;         //for the gift 65~125
    else if (birth<65 && birth>=18)
        year=2;         //for the gift 18~64
    else if (birth<18 && birth>=12)
        year=3;         //for the gift 12~17
    else if (birth<12 && birth>=0)
        year=4;         //for the gift 0~11
}
void GiftSystem::Color()    //for random color
{
    srand(time(0));
    color= rand() %3;       //roll 0~2
    if(color ==0)
        cout<<"Red"<<endl;
    else if (color ==1)
        cout<<"Green"<<endl;
    else
        cout<<"White"<<endl;
}
void GiftSystem::exchangeGift() //for print gift
{
    if(year==1) //for 65~125
    {
        if(color==0)    //when is red
            cout<<"Denture";
        else if (color==1)     //when is green
            cout<<"Pipe";
        else          //when is white
            cout<<"Hair dye";
    }
    if(year==2) //for 18~64
    {
        if(color==0)    //when is red
            cout<<"Vitamin";
        else if (color==1)  //when is green
            cout<<"Watch";
        else          //when is white
            cout<<"Wallet";
    }
    if(year==3)     //for 12~17
    {
        if(color==0)    //when is red
            cout<<"Movie ticket";
        else if (color==1)      //when is green
            cout<<"Baseball gloves";
        else      //when is white
            cout<<"Ukulele";
    }
    if(year==4) //when is 0~11
    {
        if(color==0)    //when is red
            cout<<"Lego";
        else if(color==1)   //when is green
            cout<<"Toy car";
        else          //when is white
            cout<<"Doraemon doll";
    }
}
#endif // A13-102502026_H_INCLUDED
