#include <iostream>
#include "A13-102502517.h" //使用GiftSystem的class
using namespace std;

int main()
{
    GiftSystem g; //創造GiftSystem的物件g

    g.setYear(); //輸入年紀
    g.printColor(); //輸出顏色
    g.exchangeGift(); //輸出禮物

    return 0;
}
