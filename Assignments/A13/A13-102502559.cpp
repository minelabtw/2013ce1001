#include<iostream>
#include"A13-102502559.h"
using namespace std;

int main()
{
    Giftsystem JH;
    int year=0;
    cout<<"How old are you?: ";
    cin>>year;
    while(year<0 || year>125)
    {
        cout<<"Out of range !"<<endl<<"How old are you?: ";
        cin>>year;
    }
    JH.setYear(year);
    JH.randColor();
    JH.printColor();
    JH.exchangeGift();

    return 0;
}
