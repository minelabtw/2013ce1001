#include <iostream>
#include "A13-101502205.h"

using namespace std;

int main(){
    int n;
    GiftSystem gs;  //build a gift system
    while(1){
        cout << "How old are you?: ";
        cin >> n;
        if(n>=0 && n<=125)  //break when it's legal
            break;
        cout << "Out of range!" << endl;    //error
    }
    gs.setYear(n);  //set age to GiftSystem

    do{
        gs.randColor(); //randomize the color
        gs.printColor();    //print color
        while(1){
            cout << "1)Exchange gift 2)Change color?: ";    //ask
            cin >> n;
            if(n==1 || n==2)    //break when the input is legal
                break;
            cout << "Out of range!" << endl;
        }
    }while(n!=1);   //exchange the gift when n==1
    gs.exchangeGift();  //exchange

    return 0;
}
