#ifndef A13-102502531_H_INCLUDED
#define A13-102502531_H_INCLUDED

class GiftSystem
{
public:  //公共
    GiftSystem();
    void setYear(int );
    void randColor();
    void printColor();
    void exchangeGift();
private:  //私有
    int old_1 ;
    int color_1 ;
    int b;
};
#endif // A13-102502531_H_INCLUDED

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>
using namespace std;


GiftSystem::GiftSystem ()  //初始化
{
        srand(time(0));
    old_1=color_1=b=0;
}

void GiftSystem::setYear (int) //用來輸入年齡
{
    do
    {
        cout<<"How old are you?: ";
        cin >>old_1;
        if(old_1>125||old_1<0)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(old_1>125||old_1<0);
}

void GiftSystem::randColor() //用來隨機顏色
{

    color_1=rand()%3;
}

void GiftSystem::printColor() //用來印出顏色
{
    if (color_1==0)
    {
        cout<<"Red"<<endl;
    }
    if (color_1==1)
    {
        cout<<"Green"<<endl;
    }
    if (color_1==2)
    {
        cout<<"White"<<endl;
    }
}
void GiftSystem::exchangeGift() //用來換禮物
{
    int i=0;
    int j=0;
    int c=0;
    string a [4][3]={{"Denture","Pipe","Hair dye"},{"Vitamin","Watch","Wallet"},{"Movie ticket","Baseball gloves","Ukulele"},{"Lego","Toy car","Doraemon doll"}};

    do
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>b;
        if(b>2||b<1)
        {
            cout<<"Out of range!"<<endl;
        }
        if(b==2)
        {
            GiftSystem::randColor();
            GiftSystem::printColor();
        }
    }
    while(b==2);

    if(125>=old_1&&old_1>=65)
    {
        i=0;
    }
    if(65>old_1&&old_1>=18)
    {
        i=1;
    }
    if(18>old_1&&old_1>=12)
    {
        i=2;
    }
    if(12>old_1&&old_1>=0)
    {
        i=3;
    }
    if (color_1==0)
    {
        j=0;
    }
    if (color_1==1)
    {
        j=1;
    }
    if (color_1==2)
    {
        j=2;
    }
    cout <<a[i][j];
}
