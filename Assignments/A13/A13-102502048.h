#ifndef A13-102502048_H_INCLUDED
#define A13-102502048_H_INCLUDED
using namespace std;
class GiftSystem
{
public:
    void setYear();//設定年齡
    void randColor();//使用rand()隨機抽色卡
    void printColor();//將抽到的色卡cout出來。
    void exchangeGift();//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
private:
    int year=-1;
    int color=0;
};
void GiftSystem::setYear()
{
    while(year<0 || year>125)
    {
        cout<<"How old are you?: ";
        cin>>year;
        if(year<0 || year>125)
            cout<<"Out of range!\n";
    }
}
void GiftSystem::randColor()
{
    srand(time(0));//時間變數
    color=rand()%3;
}
void GiftSystem::printColor()
{
    switch (color)//顏色選擇
    {
    case 0 :
        {
            cout<<"Red\n";
            break;
        }
    case 1 :
        {
            cout<<"Green\n";
            break;
        }
    case 2 :
        {
            cout<<"White\n";
            break;
        }
    }
}
void GiftSystem::exchangeGift()
{
    switch (color)//禮物選擇
    {
    case 0 :
        {
            if(125>=year && year>=65)
                cout<<"Denture";
            else if(65 > year && year>=18)
                cout<<"Vitamin";
            else if(18 > year && year>=12)
                cout<<"Movie ticket";
            else
                cout<<"Lego";
            break;
        }
    case 1 :
        {
            if(125>=year && year>=65)
                cout<<"Pipe";
            else if(65 > year && year>=18)
                cout<<"Watch";
            else if(18 > year && year>=12)
                cout<<"Baseball gloves";
            else
                cout<<"Toy car";
            break;
        }
    case 2 :
        {
            if(125>=year && year>=65)
                cout<<"Hair dye";
            else if(65 > year && year>=18)
                cout<<"Wallet";
            else if(18 > year && year>=12)
                cout<<"Ukulele";
            else
                cout<<"Doraemon doll";
            break;
        }
    }
}
#endif // A13-102502048_H_INCLUDED
