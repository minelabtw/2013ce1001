#ifndef A13-102502511_H_INCLUDED
#define A13-102502511_H_INCLUDED

class GiftSystem
{
public: //輸入共同可運用的函式
    GiftSystem();
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
private: //共同的變數
    int year;
    int colornumber;
};


#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;


GiftSystem::GiftSystem()
{
    year = 0;
}

void GiftSystem::setYear(int) //給予函式定義
{
    cout << "How old are you?: ";
    cin >> year;
    while(year < 0 || year > 125)
    {
        cout << "Out of range!" << endl;
        cout << "How old are you?: ";
        cin >> year;
    }
}
void GiftSystem::randColor() //給予函式定義
{
    srand(time(0));
    colornumber = rand()%3+1;
}
void GiftSystem::printColor() //給予函式定義
{
    if(colornumber == 1)
        cout << "Red" << endl;
    else if(colornumber == 2)
        cout << "Green" << endl;
    else if(colornumber == 3)
        cout << "White" << endl;
}
void GiftSystem::exchangeGift()
{
    if(12 > year && year >= 0 )
    {
        if(colornumber == 1)
            cout << "Lego" << endl;
        else if(colornumber == 2)
            cout << "Toy car" << endl;
        else if(colornumber == 3)
            cout << "Doraemon doll" << endl;
    }
    if(18 > year && year >= 12)
    {
        if(colornumber == 1)
            cout << "Movie ticket" << endl;
        else if(colornumber == 2)
            cout << "Baseball gloves" << endl;
        else if(colornumber == 3)
            cout << "Ukulele" << endl;
    }
    if(65 > year && year >= 18)
    {
        if(colornumber == 1)
            cout << "Vitamin" << endl;
        else if(colornumber == 2)
            cout << "Watch" << endl;
        else if(colornumber == 3)
            cout << "Wallet" << endl;
    }
    if(125 >= year && year >= 65)
    {
        if(colornumber == 1)
            cout << "Denture" << endl;
        else if(colornumber == 2)
            cout << "Pipe" << endl;
        else if(colornumber == 3)
            cout << "Hair dye" << endl;
    }
}
#endif

