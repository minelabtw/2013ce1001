#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std ;

class GiftSystem
{
public :
    GiftSystem () ;
    void setYear ( int ) ;
    void randColor () ;
    void printColor () ;
    void exchangeGift () ;  //宣告四函數。

private :
    int age ;
    int color ;  //宣告兩變數。
} ;

GiftSystem::GiftSystem()
{
    age = 0 ;
    color = 0 ;
}  //訂定初使值。

void GiftSystem::setYear( int a )
{
    age = a ;
}  //使輸入之年齡丟入函式。

void GiftSystem::randColor()
{
    srand ( time( 0 ) ) ;
    color = rand () % 2 ;
}  //隨機挑顏色。

void GiftSystem::printColor()
{
    if ( color == 1 )
        cout << "Red" << endl ;
    if ( color == 2 )
        cout << "Green" << endl ;
    if ( color == 0 )
        cout << "White" << endl ;
}  //輸出隨機挑中的顏色。

void GiftSystem::exchangeGift()
{
    if ( age <= 125 && age >= 65 && color == 1 )
        cout << "Denture" ;
    if ( age <= 125 && age >= 65 && color == 2 )
        cout << "Pipe" ;
    if ( age <= 125 && age >= 65 && color == 0 )
        cout << "Hair dye" ;
    if ( age < 65 && age >= 18 && color == 1 )
        cout << "Vitamin" ;
    if ( age < 65 && age >= 18 && color == 2 )
        cout << "Watch" ;
    if ( age < 65 && age >= 18 && color == 0 )
        cout << "Wallet" ;
    if ( age < 18 && age >= 12 && color == 1 )
        cout << "Movie ticket" ;
    if ( age < 18 && age >= 12 && color == 2 )
        cout << "Baseball gloves" ;
    if ( age < 18 && age >= 12 && color == 0 )
        cout << "Ukulele" ;
    if ( age < 12 && age >= 0 && color == 1 )
        cout << "Lego" ;
    if ( age < 12 && age >= 0 && color == 2 )
        cout << "Toy car" ;
    if ( age < 12 && age >= 0 && color == 0 )
        cout << "Doraemon doll" ;
}  //依照顏色及年齡不同，輸出不同的禮物。
