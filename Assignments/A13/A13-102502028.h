#ifndef A13-102502028_H_INCLUDED
#define A13-102502028_H_INCLUDED
#include <iostream>

using namespace std ;

class GiftSystem
{
private :
    int year ;
    int color ;

public :
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
};

void GiftSystem::setYear(int a)  //設定年齡
{
    year = a ;
}

void GiftSystem::randColor()     //抽卡
{
    srand(time(0)) ;
    color = rand() % 3 ;
}

void GiftSystem::printColor()    //印出卡片顏色
{
    if (color == 0)
        cout << "Red" << endl ;
    else if (color == 1)
        cout << " Green" << endl ;
    else
        cout << "White" << endl ;
}

void GiftSystem::exchangeGift()  //換禮物
{
    if (year < 12)
    {
        if (color == 0)
            cout << "Lego" << endl ;
        else if (color == 1)
            cout << "Toy car" << endl ;
        else
            cout << "Doraemon doll" << endl ;
    }

    if (year >= 12 && year < 18)
    {
        if (color == 0)
            cout << "Movie ticket" << endl ;
        else if (color == 1)
            cout << "Baseball gloves" << endl ;
        else
            cout << "Ukulele" << endl ;
    }

    if (year >= 18 && year < 65)
    {
        if (color == 0)
            cout << "Vitamin" << endl ;
        else if (color == 1)
            cout << "Watch" << endl ;
        else
            cout << "Wallet" << endl ;
    }

    if (year >= 65)
    {
        if (color == 0)
            cout << "Denture" << endl ;
        else if (color == 1)
            cout << "Pipe" << endl ;
        else
            cout << "Hair dye" << endl ;
    }
}



#endif // A13-102502028_H_INCLUDED
