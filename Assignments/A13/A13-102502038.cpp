//============================================================================
// Name        : A13-102502038.cpp
// Author      : catLee
// Version     : 0.1
// Description : NCU CE1001 A13
//============================================================================

#include <iostream>
#include "A13-102502038.h"

using namespace std;

int main(void){  //main,BJ4
  GiftSystem * gift;
  int age,sen;
  while(1){
    cout << "How old are you?: ";
    cin >> age;
    if(age<0||age>125){
      cout << "Out of range!" << endl;
      continue;
    }
    break;
  }
  while(1){
    gift = new GiftSystem(age);
    while(1){
      cout << gift -> color << endl;//cin/cout control by main function when no special reason.
      cout << "1)Exchange gift 2)Change color?: ";
      cin >> sen;
      if(sen!=1&&sen!=2){
	cout << "Out of range!" << endl;
	continue;
      }
      break;
    }
    if(sen==2){
      delete gift;  //rebuild a gift
      continue;
    }
    break;
  }
  cout << gift -> contain << endl;
  return 0;  //end.
 }
