#include "A13-102502009.h"
int main()
{
    GiftSystem GS; //定義GS
    GS.setYear();
    GS.randColor();
    GS.printColor();
    int choose=0;
    do
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>choose;
        if(choose==2)
        {
            GS.randColor(); //重新抽顏色
            GS.printColor();
        }
        else if(choose!=1)
            cout<<"Out of range!" <<endl;
    }
    while(choose!=1);
    GS.exchangeGift(); //送出禮物
    return 0;
}
