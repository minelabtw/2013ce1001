#include <iostream>
#include "A13-102502527.h"            //加入A13.h的檔案

using namespace std;

int main()
{
    GiftSystem func;           //設定簡稱為func
    func.setYear();            //輸入年齡的函數
    func.randColor();          //隨機顏色的函數
    func.printColor();         //輸出顏色的函數
    func.exchangeGift();       //交換禮物或更改顏色的函數

    return 0;
}
