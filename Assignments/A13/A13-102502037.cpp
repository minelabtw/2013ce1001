#include <iostream>
#include "Ball.h"
using namespace std;

int main()
{
    int old,change,a=1;
    cout<<"How old are you?: ";
    while(cin>>old)//輸入年紀
    {
        if(old>125 || old<0)
        {
            cout<<"Out of range!"<<endl;
            cout<<"How old are you?: ";
        }
        else
            break;
    }
    GiftSystem gift;//call class將年紀丟過去並且執行
    gift.setYear(old);
    gift.randColor();
    gift.printColor();
    cout<<"1)Exchange gift 2)Change color?: ";//看是否換顏色
    while(cin>>change)
    {
        if(change!=1 && change!=2)
        {
            cout<<"Out of range!"<<endl;
            cout<<"1)Exchange gift 2)Change color?: ";
        }
        else if(change==2)
        {
            gift.randColor();
            gift.printColor();
            cout<<"1)Exchange gift 2)Change color?: ";
        }
        else if(change==1)
            break;
    }
    gift.exchangeGift();//呼叫輸出
    return 0;
}
