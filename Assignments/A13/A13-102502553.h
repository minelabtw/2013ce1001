#ifndef A13-102502553_H_INCLUDED
#define A13-102502553_H_INCLUDED
#include<iostream>
#include<cstdlib>
#include<string>
#include<ctime>
using namespace std;
class GiftSystem//建立class，命名為GiftSystem
{
public://公開的函式或變數
    GiftSystem();
    void setYear();
    void randColor();
    void printColor(class GiftSystem GS);
    void exchangeGift();
private://只為這個class用的函式或變數
    int year;
    int color;
};
GiftSystem::GiftSystem()//初始化為0
{
    year=color=0;
}
void GiftSystem::setYear()//設定年齡分級
{
    do
    {
        cout<<"How old are you?: ";
        cin>>year;
        if(year>125||year<0)
            cout<<"Out of range!"<<endl;
    }
    while(year>125||year<0);
    if(year<=125&&year>=65)
        year=0;
    else if(year<65&&year>=18)
        year=1;
    else if(year<18&&year>=12)
        year=2;
    else if(year<12&&year>=0)
        year=3;
}
void GiftSystem::randColor()//選顏色
{
    srand(time(0));
    int color=rand()%3;
    if(color==0)
        cout<<"Red"<<endl;
    else if(color==1)
        cout<<"Green"<<endl;
    else if(color==2)
        cout<<"White"<<endl;
}
void GiftSystem::printColor(class GiftSystem GS)//是否要換禮物還是換顏色，傳入class的變數進入函式
{
    int i;
    do
    {
        do
        {
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>i;
            if(i<1||i>2)
                cout<<"Out of range!"<<endl;
        }
        while(i<1||i>2);
        if(i==2)
        {
            GS.randColor();
        }
    }
    while(i!=1);
    GS.exchangeGift();//換禮物
}
void GiftSystem::exchangeGift()//有這些禮物
{
    string s[4][3];
    s[0][0]= {"Denture"};
    s[0][1]= {"Pipe"};
    s[0][2]= {"Hair dye"};
    s[1][0]= {"Vitamin"};
    s[1][1]= {"Watch"};
    s[1][2]= {"Wallet"};
    s[2][0]= {"Movie ticket"};
    s[2][1]= {"Baseball gloves"};
    s[2][2]= {"Ukulele"};
    s[3][0]= {"Lego"};
    s[3][1]= {"Toy car"};
    s[3][2]= {"Doraemon doll"};
    cout<<s[year][color];
}
#endif // A13-102502553_H_INCLUDED
