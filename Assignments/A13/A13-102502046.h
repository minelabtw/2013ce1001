#ifndef A13-102502046_H_INCLUDED
#define A13-102502046_H_INCLUDED
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<string>
using namespace std;
class GiftSystem
{
	public:
	GiftSystem(){}
	void setYear(int);
	void randColor();
	void printColor();
	void exchangeGift();

	private:
	int year;
	int color;
	char *gift[4][3]={{"Lego","Toy car","Doraemon doll"},{"Movie ticket","Baseball gloves","Ukulele"},{"Vitamin","Watch","Wallet"},{"Denture","Pipe","Hair dye"}};
};
void GiftSystem::setYear(int x) 		//判斷年齡層
{
	if(x<12)
		year=0;
	else if(x>=12&&x<18)
		year=1;
	else if(x>=18&&x<65)
		year=2;
	else
		year=3;
}
void GiftSystem::randColor()			//隨機顏色
{
	srand(time(NULL));
	color=rand()%3;
}
void GiftSystem::printColor()			//印出顏色
{
	if(color==0)
		cout << "Red" << endl;
	else if(color==1)
		cout << "Green" << endl;
	else
		cout << "White" << endl;
}
void GiftSystem::exchangeGift()			//印出禮物
{
	cout << gift[year][color] ;
}
#endif // A13-102502046_H_INCLUDED
