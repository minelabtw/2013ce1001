#include <iostream>
#include "A13-102502529.h"

int main()
{
    GiftSystem a;                           //宣告使用headerfile
    int forchange=0;
    a.setYear(0);
    a.randColor();
    a.printColor();
    while(1)                                //判斷要換禮物或換色
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>forchange;
        if(forchange==1||forchange==2)          //判斷要叫哪個function
        {
            if(forchange==1)
            {
                a.exchangeGift();
                break;                          //換完禮物跳出
            }

            if(forchange==2)                    //換色不跳出
            {
                a.randColor();
                a.printColor();
            }
        }
        else
            cout<<"Out of range!"<<endl;        //輸入錯誤
    }

    return 0;
}
