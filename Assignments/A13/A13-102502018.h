#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;
#ifndef GiftSystem_H
#define GiftSystem_H


class GiftSystem
{
public:
    GiftSystem();
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
private:
    int year,color0;
    char *color[3]= {"Red","Green","White"};
    char *gift[4][3]=
    {
        {"Denture","Pipe","Hair dye"},
        {"Vitamin","Watch","Wallet"},
        {"Movie ticket","Baseball gloves","Ukulele"},
        {"Lego","Toy car","Doraemon doll"}
    };    //禮物清單
};
GiftSystem::GiftSystem()
{
    year=color0=0;
}
void GiftSystem::setYear(int year)
{
    this->year=year;            //將cpp中的year值帶入h
}
void GiftSystem::randColor()
{
    color0=rand()%3;            //取亂數
}
void GiftSystem::printColor()
{
    cout<<color[color0];
}
void GiftSystem::exchangeGift()
{

    if(year>=65&&year<=125)         //判斷獲得的禮物
    {
        cout<<gift[0][color0];
    }
    else if(year>=18&&year<65)
    {
        cout<<gift[1][color0];
    }
    else if(year>=12&&year<18)
    {
        cout<<gift[2][color0];
    }
    else
    {
        cout<<gift[3][color0];
    }
}

#endif // 102502018_H_INCLUDED
