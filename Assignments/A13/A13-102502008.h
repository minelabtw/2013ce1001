#ifndef A13-102502008_H_INCLUDED
#define A13-102502008_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std ;
class GiftSystem
{
public :
    GiftSystem() ;
    void getyear(); //輸入年齡
    void randcolor(); //換顏色
    void printgift() ; //禮物
private :
    string gift[15]={"Red","Green","White","Denture","Pipe","Hair dye","Vitamin","Watch","Wallet","Movie ticket","Baseball gloves","Ukulele","Lego","Toy car","Doraemon doll"};
    int year ; //年齡
    int color ; //顏色
};
GiftSystem::GiftSystem()
{
    getyear();
    randcolor();
} // star
void GiftSystem::getyear()
{
    do
    {
        cout << "How old are you?: " ;
        cin >> year ;
        if(year<0 || year>125)
            cout << "Out of range!" <<endl ;
    }while(year<0 || year>125) ;
   if(year>=65)     //改成年齡層
        year = 1 ;
   else if(year>=18)
        year = 2 ;
   else if(year>=12)
        year = 3 ;
   else
        year =4 ;

}
void GiftSystem::randcolor()// 隨機取顏色並輸出顏色
{
    srand(time(NULL)) ;
    color= rand()%3 ;
    cout << gift[color] <<endl ;
}
void GiftSystem::printgift() // 輸出禮物
{
    cout << gift[3*year+color] <<endl;
}

#endif // A13-102502008_H_INCLUDED
