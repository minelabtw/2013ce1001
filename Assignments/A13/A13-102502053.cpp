#include <iostream>
#include "A13-102502053.h"
using namespace std;

int main()
{
    GiftSystem a; // create base class object
    //call variables`
    int age;
    int mode;
    char colour;

    do//input age and data validation
    {
        cout<<"How old are you?: ";
        cin>>age;
        if(age>125||age<0)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(age>125||age<0);

    a.setYear(age); //set age into different class
    a.randColor(); // random a colour
    do//exchange gift or change colour and data validation
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>mode;
        if(mode==1) //exchange gift
        {
            a.exchangeGift();
        }
        else if(mode==2) //change colour
        {
            a.randColor();
        }
        else if(mode!=1&&mode!=2) //invalid input
        {
            cout<<"Out of range!"<<endl;srand(time(0));
        }
    }
    while(mode!=1);

    return 0;
}
