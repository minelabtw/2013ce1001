#ifndef A13-102502534_H_INCLUDED
#define A13-102502534_H_INCLUDED
#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

class GiftSystem//定義一個類別
{
public:
    GiftSystem();
    void setYear(int);
    void randColor();
    void exchangeGift();
private:
    int color;
    int years;
};
GiftSystem::GiftSystem()
{
    color=0;
    years=0;
}
void GiftSystem::setYear(int y)
{
    years=y;
}
void GiftSystem::randColor()
{
    srand(time(0));
    color=rand()%3+1;//讓亂數介於1~3之間
    if(color==1)
    {
        cout<<"Red";
    }
    else if(color==2)
    {
        cout<<"Green";
    }
    else if(color==3)
    {
        cout<<"White";
    }
}
void GiftSystem::exchangeGift()
{
    if(years<= 125&&years>=65&&color==1)
    {
        cout<<"Denture";
    }
    if(years<=125&&years>=65&&color==2)
    {
        cout<<"Pipe";
    }
    if(years<=125&&years>=65&&color==3)
    {
        cout<<"Hair dye";
    }
    if(years<65&&years>=18&&color==1)
    {
        cout<<"Vitamin";
    }
    if(years<65&&years>=18&&color==2)
    {
        cout<<"Watch";
    }
    if( years<65&&years>=18&&color==3)
    {
        cout<<"Wallet";
    }
    if( years<18&&years>=12&&color==1)
    {
        cout<<"Movie ticket";
    }
    if(years<18&&years>=12&&color==2)
    {
        cout<<"Baseball gloves";
    }
    if(years<18&&years>=12&&color==3)
    {
        cout<<"Ukulele";
    }
    if(years<12&&years>=0&&color==1)
    {
        cout<<"Lego";
    }
    if(years<12&&years>=0&&color==2)
    {
        cout<<"Toy car";
    }
    if(years<12&&years>=0&&color==3)
    {
        cout<<"Doraemon doll";
    }
}
#endif // A13-102502534_H_INCLUDED
