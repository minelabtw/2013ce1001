#ifndef A13-102502032_H_INCLUDED
#define A13-102502032_H_INCLUDED

#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
using namespace std;

int judeAge(int);

class GiftSystem
{
public:
    GiftSystem();
    void setYear();      //ask for age
    void randColor();       //choose a card randomly
    void printColor();      //show the card color to user
    void exchangeGift();    //give gift to user
private:
    string giftList[3][4];
    int age;
    int color;
};

GiftSystem::GiftSystem()
{
    giftList[0][0] = "Denture";
    giftList[0][1] = "Vitamin";
    giftList[0][2] = "Movie ticket";
    giftList[0][3] = "Lego";
    giftList[1][0] = "Pipe";
    giftList[1][1] = "Watch";
    giftList[1][2] = "Baseball gloves";
    giftList[1][3] = "Toy car";
    giftList[2][0] = "Hair dye";
    giftList[2][1] = "Wallet";
    giftList[2][2] = "Ukulele";
    giftList[2][3] = "Doraemon doll";
    age = -1;
    color = -1;
}

void GiftSystem::setYear()
{
    do
    {
        cout << "How old are you?: ";
        cin >> age;
        if (age < 0 or age > 125)
            cout << "Out of range!" << endl;
    }
    while (age < 0 or age > 125);
}

void GiftSystem::randColor()
{
    srand(time(NULL));
    color = rand() % 3;
}

void GiftSystem::printColor()
{
    switch (color)
    {
    case 0:
        cout << "Red";
        break;
    case 1:
        cout << "Green";
        break;
    case 2:
        cout << "White";
        break;
    default:
        cerr << "Error when rand(), GiftSystem::printColor(), 102502032.h" << endl;
        exit(1);
        break;
    }
    cout << endl;
}

void GiftSystem::exchangeGift()
{
    int temp;
    do
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> temp;
        switch (temp)
        {
        case 1:
            cout << giftList[color][judeAge(age)];
            break;
        case 2:
            GiftSystem::randColor();
            GiftSystem::printColor();
            break;
        default:
            cout << "Out of range!" << endl;
            break;
        }

    }
    while (temp != 1);
}

int judeAge(int age)    //return which section the age is
{
    if (age >= 65)
        return 0;
    else if (age >= 18)
        return 1;
    else if (age >= 12)
        return 2;
    else
        return 3;
}

#endif // A13-102502032_H_INCLUDED
