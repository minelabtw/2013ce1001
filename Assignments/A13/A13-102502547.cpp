#include <iostream>
#include "A13-102502547.h"
using namespace std;

int main()
{
    GiftSystem x;

    int a;
    do
    {
        cout << "How old are you?: ";
        cin >> a;
        if(a<0 || a>125)
            cout << "Out of range!" << endl;
    }
    while(a<0 || a>125);

    x.setYear(a);
    x.randColor();
    x.printColor();

    int b;
    do
    {
        cout << "1)Exchange gift 2)Change color?: ";
             cin >> b;
        if(b<1 || b>2)
            cout << "Out of range!" << endl;
        if(b==2) //b是2時換顏色
        {
            x.randColor();
            x.printColor();
        }
    }
    while(b!=1); //b不是1時重複執行

    x.exchangeGift();

    return 0;
}
