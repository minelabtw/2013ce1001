#ifndef A13-102502009_H_INCLUDED
#define A13-102502009_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std ;
class GiftSystem
{
public:
    int year=0;
    int yeartype=0;
    int randcolor=0;
    void setYear()
{
    do
    {
        cout<<"How old are you?: ";
        cin>>year;
        if(year<0 || year>125)
            cout<<"Out of range!"<<endl;
    }
    while(year<0 || year>125);
    if(year>=65) //分成4種年齡層
        yeartype=1;
    else if(year>=18)
        yeartype=2;
    else if(year>=12)
        yeartype=3;
    else
        yeartype=4;
}
void randColor() //隨機抽顏色
{
    srand(time(0));
    randcolor=(rand()%3)+1;
}
void printColor() //印出顏色
{
    switch(randcolor)
    {
    case 1:
        cout<<"Red"<<endl;
        break;
    case 2:
        cout<<"Green"<<endl;
        break;
    case 3:
        cout<<"White"<<endl;
        break;
    }
}
void exchangeGift() //送出禮物
{
    if(randcolor==1 && yeartype==1)
        cout<<"Denture";
    if(randcolor==1 && yeartype==2)
        cout<<"Vitamin";
    if(randcolor==1 && yeartype==3)
        cout<<"Movie ticket";
    if(randcolor==1 && yeartype==4)
        cout<<"Lego";
    if(randcolor==2 && yeartype==1)
        cout<<"Pipe";
    if(randcolor==2 && yeartype==2)
        cout<<"Watch";
    if(randcolor==2 && yeartype==3)
        cout<<"Baseball gloves";
    if(randcolor==2 && yeartype==4)
        cout<<"Toy car";
    if(randcolor==3 && yeartype==1)
        cout<<"Hair dye";
    if(randcolor==3 && yeartype==2)
        cout<<"Wallet";
    if(randcolor==3 && yeartype==3)
        cout<<"Ukulele";
    if(randcolor==3 && yeartype==4)
        cout<<"Doraemon doll";
}
};
#endif // 102502009_H_INCLUDED
