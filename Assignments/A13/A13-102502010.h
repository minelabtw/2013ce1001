#ifndef A13-102502010_H_INCLUDED
#define A13-102502010_H_INCLUDED
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

class giftsystem
{
public:
    giftsystem(){};
    void setyear();
    void randcolor();
    void printcolor();
    void exchangegift();
private:
    int year;
    int color;
    int choose;
};


void giftsystem::setyear()
{
    cout<<"How old are you?: ";
    cin>>year;
    while(year<0 || year>125)
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
        cin>>year;
    }
}

void giftsystem::randcolor()
{
    srand(time(0));
    color=rand()%3+1;
}

void giftsystem::printcolor()
{
    if(color==1)
        cout<<"Red"<<endl;
    else if(color==2)
        cout<<"Green"<<endl;
    else
        cout<<"White"<<endl;
}

void giftsystem::exchangegift()
{
    cout<<"1)Exchange gift 2)Change color?: ";
    cin>>choose;
    while(choose<1 || choose>2)
    {
        cout<<"Out of range!"<<endl;
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>choose;
    }
    if(choose==1)
    {
        if(color==1)
        {
            if(year>=65)
                cout<<"Denture"<<endl;
            else if(year>=18)
                cout<<"Vitamin"<<endl;
            else if(year>=12)
                cout<<"Movie ticket"<<endl;
            else if(year>=0)
                cout<<"Lego"<<endl;
        }
        else if(color==2)
        {
            if(year>=65)
                cout<<"Pipe"<<endl;
            else if(year>=18)
                cout<<"Watch"<<endl;
            else if(year>=12)
                cout<<"Baseball gloves"<<endl;
            else if(year>=0)
                cout<<"Toy car"<<endl;
        }
        else
        {
            if(year>=65)
                cout<<"Hair dye"<<endl;
            else if(year>=18)
                cout<<"Wallet"<<endl;
            else if(year>=12)
                cout<<"Ukulele"<<endl;
            else if(year>=0)
                cout<<"Doraemon doll"<<endl;
        }
    }
    else
    {
        randcolor();
        printcolor();
        exchangegift();
    }
}
#endif // A13-102502010_H_INCLUDED
