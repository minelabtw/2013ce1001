#include <iostream>
#include "A13-102502514.h"

using namespace std;

int main()
{
    GiftSystem GS;  //建立 類別GiftSystem 的 物件GS
    GS.setYear(0);  //呼叫 物件GS 的 setYear函式
    GS.randColor();  //呼叫 物件GS 的 randColor函式
    GS.printColor();  //呼叫 物件GS 的 printColor函式
    int input=0;
    while (true)
    {
        do
        {
        cout <<"1)Exchange gift 2)Change color?: ";
        cin >>input;
        }
        while (input != 1 && input != 2 && cout <<"Out of range!\n");
        if (input==2)  //如果input==2,則變換卡牌顏色並印出
        {
            GS.randColor();
            GS.printColor();
        }
        else if (input==1)  //如果input==1,則兌換禮物,並跳出迴圈
        {
            GS.exchangeGift();
            break;
        }
    }
}
