#ifndef A13-102502539_H_INCLUDED
#define A13-102502539_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class GiftSystem
{
public:
    GiftSystem();
    void setYear ( int& );
    void randColor ( int& );
    void printColor ( int& );
    void exchangeGift ( int& , int& );
};

GiftSystem::GiftSystem() { } // 內容不知道要放什麼

void GiftSystem::setYear ( int& year )  // 設定年齡
{
    do
    {
        cout << "How old are you?: " ;
        cin >> year;
        if ( year > 125 || year < 0 )
            cout << "Out of range!" << endl;
    } while ( year > 125 || year < 0 );
}

void GiftSystem::randColor ( int& color )   // 隨機抽卡
{
    srand ( time ( NULL ) );
    color = rand () % 3 ;
}

void GiftSystem::printColor ( int& color )  // 卡片輸出 + 選擇兌換禮物或重新抽卡
{
    int i = 0 ;
    do
    {
        randColor ( color );
        if ( color == 0 )
            cout << "Red" ;
        else if ( color == 1 )
            cout << "Green" ;
        else
            cout << "White" ;
        do
        {
            cout << endl << "1)Exchange gift 2)Change color?: " ;
            cin >> i ;
            if ( i != 1 && i != 2 )
                cout << "Out of range!" ;
        } while ( i != 1 && i != 2 );
    } while ( i != 1 );
}

void GiftSystem::exchangeGift ( int& year , int& color )    // 禮物輸出
{
    if ( color == 0 )
    {
        if ( year < 13 )
            cout << "Lego" ;
        else if ( year < 19 && color == 0 )
            cout << "Movie ticket" ;
        else if ( year < 66 && color == 0 )
            cout << "Vitamin" ;
        else if ( year < 126 && color == 1 )
            cout << "Denture" ;
    }
    else if ( color == 1 )
    {
        if ( year < 13 )
            cout << "Toy car" ;
        else if ( year < 19 && color == 0 )
            cout << "Baseball gloves" ;
        else if ( year < 66 && color == 0 )
            cout << "Watch" ;
        else if ( year < 126 && color == 1 )
            cout << "Pipe" ;
    }
    else if ( color == 2 )
    {
        if ( year < 13 )
            cout << "Doraemon doll" ;
        else if ( year < 19 && color == 0 )
            cout << "Ukulele" ;
        else if ( year < 66 && color == 0 )
            cout << "Wallet" ;
        else if ( year < 126 && color == 1 )
            cout << "Hair dye" ;
    }
}

#endif // A13-102502539_H
