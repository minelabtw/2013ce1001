#ifndef A13_H_INCLUDED
#define A13_H_INCLUDED
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<string>

using namespace std;

class GiftSystem
{
public:
    GiftSystem()
    {
        age = 0;
        color = 0;
        colors[0] = "Red";
        colors[1] = "Green";
        colors[2] = "White";
        gifts[0] = "Denture";
        gifts[1] = "Pipe";
        gifts[2] = "Hair dye";
        gifts[3] = "Vitamin";
        gifts[4] = "Watch";
        gifts[5] = "Wallet";
        gifts[6] = "Movie ticket";
        gifts[7] = "Baseball gloves";
        gifts[8] = "Ukulele";
        gifts[9] = "Lego";
        gifts[10] = "Toy car";
        gifts[11] = "Doraemon doll";
    }
    void setYear(int year)//將傳入的值設為年齡
    {
        age = year;
    }
    void randColor()//隨機取一個顏色
    {
        srand(time(0));
        color = (rand() % 3);
    }
    void printColor()//將選到的顏色顯示出來
    {
        cout << colors[color];
    }
    void exchangeGift()//依照年齡跟選到的顏色兌換禮物
    {
        if (age <= 125 && age >= 65)
        {
            cout << gifts[color];
        }
        if (age < 65 && age >= 18)
        {
            cout << gifts[(color + 3)];
        }
        if (age < 18 && age >= 12)
        {
            cout << gifts[(color + 6)];
        }
        if(age < 12 && age >=0)
        {
            cout << gifts[(color + 9)];
        }
    }
private:
    string colors[3];
    string gifts[12];
    int age, color;
};

#endif // A13-102302053_H_INCLUDED
