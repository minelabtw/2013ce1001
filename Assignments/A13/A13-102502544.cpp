#include<iostream>
#include"A13-102502544.h"
using namespace std;
int main()
{
    GiftSystem a;//函式名稱
    int choice=0;//宣告變數
    do
    {
        cout<<"How old are you?: ";
        a.setYear();
        if(a.year<0 || a.year>125)
            cout<<"Out of range!"<<endl;
    }
    while(a.year<0 || a.year>125);//迴圈
    a.randColor();//帶出函式
    a.printColor();//帶出函式
    do
    {
        cout<<"1>Exchange gift 2>Change color?: ";
        cin>>choice;
        if(choice!=1 && choice!=2)
            cout<<"Out of range!"<<endl;
        else if(choice==2)
        {
            a.randColor();//帶出函式
            a.printColor();//帶出函式
        }
        else if(choice==1)
        {
            a.exchangeGift();//帶出函式
        }
    }
    while(choice==2 || (choice!=1 && choice!=2));//迴圈

    return 0;
}
