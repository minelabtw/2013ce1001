#include <iostream>
#include "A13-995002007.h"  //include definition of class Giftsytem
using namespace std;

int main()
{
    Giftsystem GS; //create Gidtsystem 物件
    int old=0;
    int x=0;

    GS.setYear(old); //呼叫物件的setYear函式
    GS.randColor();

    do
    {
        cout << "1)Exchange gift 2)Change color?: " ;  //選擇1 OR 2
        cin >> x;

        switch(x)
        {
        case 1:
            GS.exchangeGift(); //選擇1直接交換禮物
            break;
        case 2:
            GS.randColor();      //選擇2就換顏色, 然後就繼續選擇1or2
            break;
        default:
            cout << "Out of range!" << endl; //除了輸入1or2之外的都顯示"Out of rang"
            break;
        }
    }
    while(x!=1); //如果不是交換禮物就一直迴圈


    return 0;
}
