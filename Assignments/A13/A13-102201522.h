//A13-102201522
#ifndef A13-102201522_H_INCLUDED
#define A13-102201522_H_INCLUDED

#include<iostream>
#include<iomanip>
#include<cstdlib>
#include<ctime>
using namespace std;

//template<class T>
class GiftSystem
{
public:
    void setYear(int year) //定年齡,若超出範圍則輸出"Out of range!"
    {
        while (year<0||year>125)
        {
            cout << "Out of range! \n" << "How old are you?: ";
            cin >> year;
        }
    }


    void randColor(int &color) //使用rand()隨機抽色卡
    {
        color = rand()%3;
    }

    void printColor(int color) //將抽到的色卡cout出來。
    {
        switch (color)
        {
        case 0:
            cout << "Red\n";
            break;
        case 1:
            cout << "Green\n";
            break;
        case 2:
            cout << "White\n";
            break;
        }
    }

    void exchangeGift(int color,int year) //依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
    {
        string P[4][3] =
        {
            {"Denture","Pipe","Hair dye"},
            {"Vitamin","Watch","Wallet"},
            {"Movie ticket","Baseball gloves","Ukulele"},
            {"Lego","Toy car","Doraemon doll"}
        };

        int i,j;

        if (125>=year&&year>=65)  i = 0;
        else if (65 > year&&year>=18)  i = 1;
        else if (18 > year&&year>=12)  i = 2;
        else if (12 > year&&year>= 0)  i = 3;

        if (color == 0)  j = 0;
        else if (color == 1)  j = 1;
        else if (color == 2)  j = 2;
        cout << P[i][j];
    }
};



#endif // A13-102201522_H_INCLUDED
