//A13-102502026
#include "A13-102502026.h"
int main()
{
    GiftSystem P;   //define GiftSystem as P
    int number=0;
    P.setYear();        //do void GiftSystem::setYear()
    P.Color();          //do void GiftSystem::Color()
    while(number<1 || number>1)
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>number;
        if(number<1 || number>2)
            cout<<"Out of range!"<<endl;
        if(number==2)
            P.Color();  //do void GiftSystem::Color()
    }
    if (number==1)
    {
        P.exchangeGift();   //do void GiftSystem::exchangeGift()
    }
return 0;
}   //end program
