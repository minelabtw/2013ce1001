#include <iostream>
#include "A13-102502042.h"  //引入class
using namespace std;
int main()
{
    int n,t;
    GiftSystem gift;
    cout<<"How old are you?: ";
    while(cin>>n&&(n<0||n>125)) //輸入年齡
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
    }
    gift.setYear(n);
    while(1)
    {
        gift.randColor();
        gift.printColor();
        cout<<"1)Exchange gift 2)Change color?: ";
        while(cin>>t&&(t>2||t<1))   //輸入選項
        {
            cout<<"Out of range!"<<endl;
            cout<<"1)Exchange gift 2)Change color?: ";
        }
        if(t==1)
        {
            gift.exchangeGift();
            break;
        }
    }

    return 0;
}
