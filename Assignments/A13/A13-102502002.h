class GiftSystem
{
public:
    int year;
    int color;
    GiftSystem();
    ~GiftSystem();
    void setYear();
    void randColor();
    void printColor();
    void exchangeGift();
};
