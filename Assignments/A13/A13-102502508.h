#ifndef A13-102502508_H_INCLUDED
#define A13-102502508_H_INCLUDED
#include<cstdlib>
#include<ctime>
using namespace std;
class Giftsystem          //定義一個類別
{
public:
    Giftsystem();
    void setyear();      //用來設定年齡的成員函式
    void randColor();    //用來隨機挑選顏色並將其印出的函式
    void exchangeGift(); //利用前面瞭個成員函式所得到的資料來兌換禮物
private:
    int random;
    int year;
    int distribution;       //用來表示年齡層的分布

};
Giftsystem::Giftsystem()
{
    random=0;
    distribution=0;
    year=0;
}
void Giftsystem::setyear()
{
    do  //利用後測試迴圈及大量的判斷句來確認使用者的年齡分佈
    {
        cout<<"How old are you?: " ;
        cin>>year ;
        if(year<=125 && year>=65)
        {
            distribution=1 ;
            break ;
        }
        else if(year<65 && year>=18)
        {
            distribution=2 ;
            break ;
        }
        else if(year<18 && year>=12)
        {
            distribution=3 ;
            break ;
        }
        else if(year<12 && year>=0)
        {
            distribution=4 ;
            break ;
        }
        else
        {
            cout<<"Out of range!"<<endl ;
        }
    }
    while(2013);
}
void Giftsystem::randColor()
{   //利用隨機函式來獲取隨機變數並印出該數字所對映的顏色

    srand(time(0)) ;
    random=rand()%3+1 ;
    if(random==1)
    {
        cout<<"Red"<<endl ;
    }
    else if(random==2)
    {
        cout<<"Green"<<endl ;
    }
    else if(random==3)
    {
        cout<<"White"<<endl ;
    }


}
void Giftsystem::exchangeGift()
{   //利用前兩項成員函式所得到的資料來獲取所能兌換的禮物
    if(distribution==1)
    {
        if(random==1)
        {
            cout<<"Denture" ;

        }

        else if(random==2)
        {
            cout<<"Pipe"<<endl ;

        }
        else if(random==3)
        {
            cout<<"Hair dye"<<endl ;

        }
    }
    else if(distribution==2)
    {

        if(random==1)
        {
            cout<<"Vitamin"<<endl ;

        }

        else if(random==2)
        {
            cout<<"Watch"<<endl ;

        }
        else if(random==3)
        {
            cout<<"Wallet"<<endl ;

        }
    }
    else if(distribution==3)
    {
        if(random==1)
        {
            cout<<"Movie ticket"<<endl ;

        }

        else if(random==2)
        {
            cout<<"Baseball gloves"<<endl ;

        }
        else if(random==3)
        {
            cout<<"Ukulele"<<endl ;

        }
    }
    else if(distribution==4)
    {
        if(random==1)
        {
            cout<<"Lego"<<endl ;

        }

        else if(random==2)
        {
            cout<<"Toy car"<<endl ;

        }
        else if(random==3)
        {
            cout<<"Doraemon doll"<<endl ;

        }
    }



}




#endif // A13-102502508_H_INCLUDED
