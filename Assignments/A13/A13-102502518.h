#ifndef A13-102502518_H
#define A13-102502518_H
#include <iostream>
#include <cstdlib>
using namespace std;
class GiftSystem
{
public:
    GiftSystem();
    void setYear(int);//設定年齡
    void randColor();//隨機抽取色卡
    void printColor();//顯示出抽到的色卡
    void exchangeGift();//兌換禮物
private:
    int year;
    int color;
};
GiftSystem::GiftSystem()
{
    year=0;
    color=0;
}
void GiftSystem::setYear(int i)//設定年齡
{
    year=i;
}
void GiftSystem::randColor()//隨機抽取色卡
{
    srand(rand());
    color=rand()%3;
}
void GiftSystem::printColor()//顯示出抽到的色卡
{
    if (color==0)
        cout<<"Red"<<endl;
    if (color==1)
        cout<<"Green"<<endl;
    if (color==2)
        cout<<"White"<<endl;
}
void GiftSystem::exchangeGift()//兌換禮物
{
    if (year<=125&&year>=65&&color==0)
        cout<<"Denture";
    if (year<=125&&year>=65&&color==1)
        cout<<"Pipe";
    if (year<=125&&year>=65&&color==2)
        cout<<"Hair dye";
    if (year<65&&year>=18&&color==0)
        cout<<"Vitamin";
    if (year<65&&year>=18&&color==1)
        cout<<"Watch";
    if (year<65&&year>=18&&color==2)
        cout<<"Wallet";
    if (year<18&&year>=12&&color==0)
        cout<<"Movie ticket";
    if (year<18&&year>=12&&color==1)
        cout<<"Baseball gloves";
    if (year<18&&year>=12&&color==2)
        cout<<"Ukulele";
    if (year<12&&year>=0&&color==0)
        cout<<"Lego";
    if (year<12&&year>=0&&color==1)
        cout<<"Toy car";
    if (year<12&&year>=0&&color==2)
        cout<<"Doraemon doll";
}
#endif // A13-102502518_H
