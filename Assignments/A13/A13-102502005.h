#ifndef A13-102502005_H_INCLUDED
#define A13-102502005_H_INCLUDED
#include<cstdlib>
#include<ctime>
using namespace std;

class GiftSystem                    //宣告類別。
{
public:
    GiftSystem();                   //建構子。
    void setYear(int);              //設定Year及Range的函數。
    void randColor();               //隨機產生Color的函數。
    void printColor();              //印出Color的函數。
    void exchangeGift();            //確定交換禮物的函數。
private:                            //object的data。
    int Year;                       //年齡。
    int Color;                      //色卡顏色。
    int Range;                      //年齡在哪一個範圍。
};

GiftSystem::GiftSystem()            //建構子。
{
    Range = Year = Color = 0;       //將object的每一個data初始化為零。
}

void GiftSystem::setYear(int year)  //設定Year及Range的函數。
{
    Year = year;                    //將main中輸入的year存為年齡。

    if (Year>=65)                   //用if else判斷式決定年齡範圍。
    {
        Range = 1;
    }
    else if (Year<65 && Year>=18)
    {
        Range = 2;
    }
    else if (Year<18 && Year>=12)
    {
        Range = 3;
    }
    else if (Year<12 && Year>=0)
    {
        Range = 4;
    }
}

void GiftSystem::randColor()        //隨機產生Color的函數。
{
    srand(time(0));
    Color = rand()%3;               //利用rand來產生亂數，並由產生的亂數代表顏色。
}

void GiftSystem::printColor()       //印出Color的函數。
{
    switch (Color)                  //利用switch case印出Color代表的顏色。
    {
    case 0:
        cout << "Red" << endl;
        break;
    case 1:
        cout << "Green" << endl;
        break;
    case 2:
        cout << "White" << endl;
        break;
    }

}

void GiftSystem::exchangeGift()     //確定交換禮物的函數。
{
    int gift = Range*10+Color;      //利用右式計算出禮物代碼。

    switch (gift)                   //利用switch case印出代碼代表的禮物。
    {
    case 10:
        cout << "Denture";
        break;
    case 11:
        cout << "Pipe";
        break;
    case 12:
        cout << "Hair dye";
        break;
    case 20:
        cout << "Vitamin";
        break;
    case 21:
        cout << "Watch";
        break;
    case 22:
        cout << "Wallet";
        break;
    case 30:
        cout << "Movie Ticket";
        break;
    case 31:
        cout << "Baseball gloves";
        break;
    case 32:
        cout << "Ukulele";
        break;
    case 40:
        cout << "Lego";
        break;
    case 41:
        cout << "Toy car";
        break;
    case 42:
        cout << "Doraemon doll";
        break;
    default:
        break;
    }
}

#endif // A13-102502005_H_INCLUDED
