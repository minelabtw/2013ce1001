#include <iostream>
#include "A13-102502543.h"
using namespace std;
int main()
{
    GiftSystem g; //物件g
    int a=0;
    do
    {
        cout<<"How old are you?: "; //輸出
        cin>>a;
        if (a<0 or a>125)
            cout <<"Out of range!"<<endl;
    }
    while(a<0 or a>125);
    g.setYear(a); //class函式
    a=2;
    do //迴圈
    {
        if (a==2)
            g.printColor();
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>a;
        if (a!=1 and a!=2)
            cout <<"Out of range!"<<endl;
    }
    while (a!=1);
    g.exchangeGift();
    return 0;
}
