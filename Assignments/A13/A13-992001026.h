#ifndef GIFTGENERATOR
#define GIFTGENERATOR

#include <iostream>
#include <string>

#include <vector>
#include <map>

#include <ctime>


using namespace std;

//禮物生成器
class GiftGenerator
{
private:
    //以下是類別共用 static member
    //卡片陣列 Red, Green, White
    static vector<string> cards;
    //卡片年齡對應禮物的表格，呼叫方法: table[卡片][yearIndex]
    static  map<string, vector<string> > table;

    //年齡
    int year;
    //卡片
    string card;

    //將年紀轉成 yearIndex，注意是 private 所以外面呼叫不到
    int yearToIndex( int y )
    {
        int rtn;
        if( y>=0 && y<12 )
        {
            rtn = 0;
        }
        else if( y>=12 && y<18 )
        {
            rtn = 1;
        }
        else if( y>=18 && y<65 )
        {
            rtn = 2;
        }
        else if( y>=65 && y<125 )
        {
            rtn = 3;
        }
        else   //範圍外錯誤
        {
            rtn = -1;
        }

        return rtn;
    }
public:
    //設定年齡並設定 random seed
    GiftGenerator( int y ) : year(y)
    {
        srand( time(NULL) );
    }
    //生成一張卡片
    void genCard()
    {
        card = cards[rand()%cards.size()];
    }
    //印出目前卡片
    void printCard()
    {
        cout << card << endl;
    }
    //印出禮物
    void printGift()
    {
        cout << table[card][yearToIndex(year)] << endl;
    }
    static void init()
    {
        //設定 cards
        string cds[] = { "Red", "Green", "White" };
        cards = vector<string>( cds, cds+3 );

        //設定 table
        string red[] = { "Lego", "Movie ticket", "Vitamin", "Denture" };
        string green[] = { "Toy car", "Baseball gloves", "Watch", "Pipe"};
        string white[] = { "Doraemon doll", "Ukelele", "Wallet", "Hair dye" };
        table["Red"] = vector<string>(red, red+4);
        table["Green"] = vector<string>(green, green+4);
        table["White"] = vector<string>(white, white+4);

    }
};

//static member 必設初值
vector<string> GiftGenerator::cards = vector<string>();
map<string, vector<string> > GiftGenerator::table = map<string, vector<string> >();

#endif
