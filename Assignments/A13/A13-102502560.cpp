#include <iostream>
#include "A13-102502560.h"
using namespace std;

int main()
{
	GiftSystem gs;			//use class GiftSystem to create object gs
	
	int age;
	while(1){
		cout << "How old are you?: "; cin >> age;					//input age
		if(age<0 || age>125){cout << "Out of range!" << endl; continue;}
		gs.setAge(age);		//set age of gs
		break;
	}
	
	gs.printColor();		//print color of gs
	
	int mode;
	while(1){
		cout << "1)Exchange gift 2)Change color?: "; cin >> mode;	//input mode: 1 or 2
		switch(mode)
		{
		case 1:
			gs.exchangeGift();		//1: exchange gift and exit
			exit(0);
		case 2:
			gs.randColor();			//2: change the color
			gs.printColor();
			break;
		default:
			cout << "Out of range!" << endl;
			break;
		}
	}
	
	return 0;
}
