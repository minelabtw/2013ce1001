#include<iostream>
#include<stdlib.h>
#include<time.h>
#include "A13-102502034.h"


using namespace std;
GiftSystem::GiftSystem()//函式
{
    srand(time(NULL));
    //ctor
}

void GiftSystem::randColor(void)//隨機色彩
{
    int c;
    do
    {
        c=rand()%3 ;
    }
    while(c==color);
    color = c;
}

void GiftSystem::setYear(void)//設定年齡
{
    cout <<"How old are you?: ";
    cin >> age ;
    while (age<0 or age >125)
    {
        cout <<"Out of range!" <<endl;
        cout <<"How old are you?: ";
        cin >> age ;
    }


}

void GiftSystem::printColor(void)//亂數數字 將他 設定顏色
{
    if (color == 0)
        cout <<"Red";
    else if (color ==1)
        cout <<"Green";
    else if (color==2)
        cout <<"White";

    cout <<endl;

}
void GiftSystem::exchangeGift(void)//作出一個表格 然後看什麼禮物就給什麼禮物
{
    int ri ;
    char gift_table[3][4][20]=
    {
        "Lego","Movie ticket","Vitamin","Denture",
        "Toy car","Baseball gloves","Watch","Pipe",
        "Doraemon doll","Ukulele","Wallet","Hair dye"
    };

    if(age>=0 && age<12)
        ri = 0 ;

    else if (age>=12 && age <18)
        ri = 1;

    else if (age>=18 && age <65)
        ri = 2;
    else
        ri = 3;
        cout <<gift_table[color][ri];
        cout <<endl;
}


int main ()
{
    class GiftSystem obj_gift;
    int ans;
    obj_gift.setYear();//設定年齡
    obj_gift.randColor();//設定顏色
    obj_gift.printColor();//輸出顏色

    do//交換顏色 or 輸出禮物
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin >>ans;
        if (ans!=1 && ans!=2)
        {
            cout <<"Out of range!"<<endl;
            continue;
        }

        else if (ans==1)
        {
            obj_gift.exchangeGift();
        }
        else if (ans==2)
        {
            obj_gift.randColor();
            obj_gift.printColor();
        }

    }
    while(ans!=1);
}
