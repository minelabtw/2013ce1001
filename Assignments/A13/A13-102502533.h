#ifndef A13-102502533_H_INCLUDED
#define A13-102502533_H_INCLUDED
#include<iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

class changegifts
{
public:
    changegifts();
    void setYear(int);//設定年齡
    void randColor();//使用rand()隨機抽色卡
    void printColor();//將抽到的色卡cout出來。
    void exchangeGift();//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來
private:
    int color;
    int exchange;
    int age;
};

changegifts::changegifts()
{
    color=exchange=age=0;
}
void changegifts::setYear(int age)//設定年齡
{
    while(age>125 || age<0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
        cin>>age;
    }
}
void changegifts::randColor()//使用rand()隨機抽色卡
{
    color=rand()%3+1;
}
void changegifts::printColor()//將抽到的色卡cout出來。
{
    if(color==1)
        cout<<"Red";
    if(color==2)
        cout<<"Green";
    if(color==3)
        cout<<"White";

}
void changegifts::exchangeGift(int year)//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
{

    if(125>=year>=65 &&color==1)
        cout<<"Denture";
    if(125>=year>=65 &&color==2)
        cout<<"Pipe";
    if(125>=year>=65 &&color==3)
        cout<<"Hair dye";
    if(65 > year>=18 &&color==1)
        cout<<"Vitamin";
    if(65 > year>=18 &&color==2)
        cout<<"Watch";
    if(65 > year>=18 &&color==3)
        cout<<"Wallet";
    if( 18 > year>=12 &&color==1)
        cout<<"Movie ticket";
    if( 18 > year>=12 &&color==2)
        cout<<"Baseball gloves";
    if( 18 > year>=12 &&color==3)
        cout<<"Ukulele";
    if(12 > year>= 0 &&color==1)
        cout<<"Lego";
    if(12 > year>= 0 &&color==3 )
        cout<<"Doraemon doll";
    if(12 > year>= 0 &&color==2)
        cout<<"Toy car";
}


#endif // A13-102502533_H_INCLUDED
