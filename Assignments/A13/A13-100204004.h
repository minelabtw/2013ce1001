#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<string>
using namespace std;
class GiftSystem
{
public:
    GiftSystem();//constructor
    void setYear(int year);//設定年齡
    void randColor();//隨機抽色卡
    void printColor();//將抽到的色卡cout出來
    void exchangeGift();//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來
private:
    int year;
    int Color;
    int C;//存抽過的色卡
    int group;//年齡層
};
#endif// GIFTSYSTEM_H

GiftSystem::GiftSystem()
{
    year=Color=C=group=0;
}

void GiftSystem::setYear(int y)//設定年齡
{
    year=y;
}

void GiftSystem::randColor()//隨機抽色卡
{
    srand(time(0));
    if(Color==0)//第一次抽色卡
    {
        Color=(1+rand()%3);
        C=Color;
    }
    if(Color!=0)//換色卡
    {
        do
        {
            Color=(1+rand()%3);
        }
        while(C==Color);
        C=Color;
    }
}

void GiftSystem::printColor()//將抽到的色卡cout出來
{
    string pColor[4]= {"","Red","Green","White"};
    cout<<pColor[C]<<endl;
}

void GiftSystem::exchangeGift()//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來
{
        string gift[5][4]= {{},{"","Denture","Pipe","Hair dye"},{"","Vitamin","Watch","Wallet"},{"","Movie ticket","Baseball gloves","Ukulele"},{"","Lego","Toy car","Doraemon doll"}};
    if(year<=125&&year>=65)
        group=1;
    if(year<65&&year>=18)
        group=2;
    if(year<18&&year>=12)
        group=3;
    if(year<12&&year>=0)
        group=4;

    cout<<gift[group][C];
}
