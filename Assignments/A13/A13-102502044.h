/*************************************************************************
    > File Name: A13-102502044.h
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年12月27日 (週五) 03時05分55秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* inti gift table */
const char gift_table[4][3][20] =	
					{{"Denture",		"Pipe",				"Hair dye"},
					{"Vitamin",			"Watch",			"Wallet"},
					{"Movie ticket",	"Baseball gloves",	"Ukulele"},
					{"Lego",			"Toy car",			"Doraemon doll"}};

class GiftSystem
{
	public:
		/* GiftSystem is used to init */
		/* setYear is used to input years, and change to class format */
		/* randColor is used to rand color, and print it */
		/* printColor is used to print color by English format */
		/* exchangeGift is used to exchange or change color */
		GiftSystem();
		void setYear();
		void randColor();
		void printColor();
		void exchangeGift();

	private:
		/* year = 0 means 65~125 */
		/* year = 1 means 18~64 */
		/* year = 2 means 12~17 */
		/* year = 3 means 0~11 */
		/* color = 0 means Red */
		/* color = 1 means Green */
		/* color = 2 means White */
		/* tmp used to save something will not use long time */
		char year;
		char color;
		char tmp;
};	

/* init GiftSystem */
GiftSystem::GiftSystem()
{
	/* init seed of rand */
	srand(time(NULL));
	
	/* input year */
	setYear();

	/* rand one color */
	randColor();
	
	/* exchange gift */
	exchangeGift();
}

/* input year */
void GiftSystem::setYear()
{
	for(;;)
	{
		/* ui message */
		printf("How old are you?: ");
		
		/* input */
		scanf("%d", &year);

		/* if in range */
		if(0<=year && year<=125)
			/* then exit this loop */
			break;
		else
			/* else, print error message, and repeat input */
			puts("Out of range!");
	}

	/* change year into class format*/
	year = (year < 65) + (year < 18) + (year <12);
}

/* rand one color */
void GiftSystem::randColor()
{
	/* rand color {0, 1, 2}*/
	color = rand()%3;

	/*print this color */
	printColor();
}

/* print color int English format */
void GiftSystem::printColor()
{
	switch(color)
	{
		case 0:
			puts("Red");
			break;
		case 1:
			puts("Green");
			break;
		case 2:
			puts("White");
			break;
	}
}

/* change color or exchange gift */
void GiftSystem::exchangeGift()
{
	for(;;)
	{
		/* ui message */
		printf("1)Exchange gift 2)Change color?: ");

		/* input this change */
		scanf("%d", &tmp);

		switch(tmp)
		{
			case 1:
				/* print gift */
				puts(gift_table[year][color]);

				/* exit this program */
				exit(0);
				break;
			case 2:
				/* change color */
				randColor();
				break;
			default:
				/* print error message */
				puts("Out of range!");
				break;
		}
	}
}

