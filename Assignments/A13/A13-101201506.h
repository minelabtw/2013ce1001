#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
using namespace std;

class GIFTSYSTEM
{
public:
    GIFTSYSTEM();
    void setYear(int );
    void randColor();
    void printColor();
    void exchangeGift();
private:
    int x; //year old
    int y; //color
};
GIFTSYSTEM::GIFTSYSTEM() //initial value
{
    x=0;
    y=0;
}
void GIFTSYSTEM::setYear(int a)
{
    x=a;
}
void GIFTSYSTEM::randColor()
{
    srand(time(0));
    y=rand()%3;
}
void GIFTSYSTEM::printColor()
{
    if(y==0)
        cout<<"Red"<<endl;
    if(y==1)
        cout<<"Green"<<endl;
    if(y==2)
        cout<<"White"<<endl;
}
void GIFTSYSTEM::exchangeGift()
{
    if(x<=125 && x>=65 && y==0)
        cout<<"Denture";
    if(x<=125 && x>=65 && y==1)
        cout<<"Pipe";
    if(x<=125 && x>=65 && y==2)
        cout<<"Hair dye";
    if(x<65 && x>=18 && y==0)
        cout<<"Vitamin";
    if(x<65 && x>=18 && y==1)
        cout<<"Watch";
    if(x<65 && x>=18 && y==2)
        cout<<"Wallet";
    if(x<18 && x>=12 && y==0)
        cout<<"Movie ticket";
    if(x<18 && x>=12 && y==1)
        cout<<"Baseball gloves";
    if(x<18 && x>=12 && y==2)
        cout<<"Ukulele";
    if(x<12 && x>=0 && y==0)
        cout<<"Lego";
    if(x<12 && x>=0 && y==1)
        cout<<"Toy car";
    if(x<12 && x>=0 && y==2)
        cout<<"Doraemon doll";
}
#endif // GIFTSYSTEM_H
