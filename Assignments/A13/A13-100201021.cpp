#include<iostream>
#include<time.h>
#include<cstdlib>
#include "A13-100201021.h"

using namespace std;

int main()
{
    GiftSystem g1;  //object
    int y;
    while(true) //cin ages
    {
        cout << "How old are you?: ";
        cin >> y;
        if(y>125 || y<0)
            cout << "Out of range" << endl;
        else
            break;
    }
    g1.setYear(y);
    while(true)
    {
        int ch=0;
        g1.randColor(); //randcolor
        cout << "1)Exchange gift 2)Change color?: " ;
        cin >> ch;
        if(ch!=1&&ch!=2)
            cout << "Out of range!" << endl;
        else if(ch==1)
            break;  //exchange Gift
    }
    g1.exchangeGift();
    return 0;
}
