#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;
#ifndef A13-102502521_H_INCLUDED
#define A13-102502521_H_INCLUDED

class GiftSystem    //GiftSystem的class定義
{
public:
    GiftSystem();
    void setYear(int);    //將cpp檔裡的year傳入
    void randColor();    //隨機顏色
    void printColor();    //列印顏色
    void exchangeGift();    //兌換禮物
private:
    int year;
    int color;
};

GiftSystem::GiftSystem()
{
    year=0;
    color=0;
}

void GiftSystem::setYear(int year)
{
    this->year=year;
}

void GiftSystem::randColor()
{
    color=rand()%3+1;
}

void GiftSystem::printColor()
{
    if(color==1)
    {
        cout<<"Red"<<endl;
    }
    else if(color==2)
    {
        cout<<"Green"<<endl;
    }
    else
    {
        cout<<"White"<<endl;
    }
}

void GiftSystem::exchangeGift()
{
    switch(color)
    {
    case 1:
        if(year>=0&&year<12)
        {
            cout<<"Lego"<<endl;
        }
        else if(year>=12&&year<18)
        {
            cout<<"Movie ticket"<<endl;
        }
        else if(year>=18&&year<65)
        {
            cout<<"Vitamin"<<endl;
        }
        else
        {
            cout<<"Denture"<<endl;
        }
        break;

    case 2:
        if(year>=0&&year<12)
        {
            cout<<"Toy car"<<endl;
        }
        else if(year>=12&&year<18)
        {
            cout<<"Baseball gloves"<<endl;
        }
        else if(year>=18&&year<65)
        {
            cout<<"Watch"<<endl;
        }
        else
        {
            cout<<"Pipe"<<endl;
        }

    case 3:
        if(year>=0&&year<12)
        {
            cout<<"Doraemon doll"<<endl;
        }
        else if(year>=12&&year<18)
        {
            cout<<"Ukulele"<<endl;
        }
        else if(year>=18&&year<65)
        {
            cout<<"Wallet"<<endl;
        }
        else
        {
            cout<<"Hair dye"<<endl;
        }
    }
}
#endif // A13-102502521_H_INCLUDED
