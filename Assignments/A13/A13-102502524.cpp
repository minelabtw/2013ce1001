#include <iostream>
#include "A13.h"            //包入A13.h檔案
using namespace std;

int main()
{
    GiftSystem a;           //設定簡稱為a
    a.setYear();            //輸入年齡
    a.randColor();          //隨機顏色
    a.printColor();         //輸出顏色
    a.exchangeGift();       //選擇交換禮物或是更改顏色

    return 0;
}
