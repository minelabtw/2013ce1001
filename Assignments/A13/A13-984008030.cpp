#include "A13-984008030.h"
#ifndef IOSTREAM_DEF
#define IOSTREAM_DEF
#include <iostream>
using namespace std;
#endif

int main(){
    GiftSystem gift;//宣告型態為GiftSystem的gift抽禮物系統
    int year = 20;//宣告型態為int的歲數，並初始化為20(歲)
    int command = 1;//宣告型態為int的指令，並初始化為1(取得禮物指令)
    while(1){
        cout << "How old are you?: ";
        cin >> year;
        if(year > 125 || year < 0){//先判斷輸入的歲數是否超出範圍
            cout << "Out of range!" << endl;
        }
        else{//歲數分成四個區，再根據不同的歲數給予相對應的區段
            if(year >= 0 && year < 12){//區段3:0<=year<12
                gift.setYear(3);
            }
            else if(year >= 12 && year < 18){//區段2:12<=year<18
                gift.setYear(2);
            }
            else if(year >= 18 && year < 65){//區段1:18<=year<65
                gift.setYear(1);
            }
            else{//區段0:65<=year<=125
                gift.setYear(0);
            }
            break;
        }
    }
    gift.printColor();
    while(1){
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> command;
        if(command == 1){//抽禮物，並結束迴圈
            gift.exchangeGift();
            break;
        }
        else if(command == 2){//改變顏色
            gift.randColor();
            gift.printColor();
        }
        else{//非預設指令
            cout << "Out of range!" << endl;
        }
    }
    return 0;
}
