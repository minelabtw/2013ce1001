#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;

#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H

class GiftSystem{

public:
    GiftSystem();//constructor
    void setYear(int);//設定年齡 
    void randColor();//隨機抽色卡 
    void printColor();//輸出色卡顏色 
    void exchangeGift(); //兌換禮物 
private:     
    int year;//年齡(0~125) 
    int color;//色卡(0~2)紅綠白
};

GiftSystem::GiftSystem(){//初始化為0 
    year = 0;
    color = 0;
}

void GiftSystem::setYear(int age){
    year = age;
}

void GiftSystem::randColor(){
    srand(time(NULL));//以time(NULL)為種子打亂 
    color = rand()%3;//隨機產生0~2 
}

void GiftSystem::printColor(){
    if(color == 0)
        cout << "Red" << endl;
    else if(color == 1)
        cout << "Green" << endl;
    else
        cout << "White" << endl;
}

void GiftSystem::exchangeGift(){
    string gift[4][3]={{"Denture","Pipe","Hair dye"},{"Vitamin","Watch","Wallet"},{"Movie ticket","Baseball gloves","Ukulele"},{"Lego","Toy car","Doraemon doll"}};
    //兌換表 
    if(year<=125 && year>=65)
        cout << gift[0][color] << endl;
    else if(year<65 && year>=18)
        cout << gift[1][color] << endl;
    else if(year<18 && year>=12)
        cout << gift[2][color] << endl;
    else
        cout << gift[3][color] << endl;
}

#endif
