#include <iostream>
#include <cstdlib>
#include <ctime>
#include "A13-101201506.h"
using namespace std;

int main()
{
    GIFTSYSTEM gift;

    int x=0; // years old
    int n=0; // selection
    do // x interval
    {
        cout<<"How old are you?: ";
        cin>>x;
        if(x<0 || x>125)
            cout<<"Out of range!"<<endl;
    }
    while(x<0 || x>125);

    gift.setYear(x);
    gift.randColor();
    gift.printColor();

    while(1)
    {
        do // n interval
        {
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>n;
            if(n<1 || n>2)
                cout<<"Out of range!"<<endl;
        }
        while(n<1 || n>2);

        if(n==1)
        {
            gift.exchangeGift();
            break;
        }
        if(n==2)
        {
            gift.randColor();
            gift.printColor();
        }
    }
    return 0;
}
