#include<iostream>
#include "A13-102201522.h"
using namespace std;
int main(void)
{
    srand(int(time(0)));
    int year,color=0;
    GiftSystem A;
    cout << "How old are you?: "; //輸出提示字讓使用者輸入年齡(0<=year<=125)
    cin >> year;
    A.setYear(year); //若超出範圍則輸出"Out of range!"
    A.randColor(color); //隨機抽取色卡(Red,Green,White)
    A.printColor(color); //顯示出抽到的色卡

    int choice;
    cout << "1)Exchange gift 2)Change color?: "; //使用者可以選擇 1)兌換禮物 2)更換色卡。
    cin >> choice;
    while (choice<1||choice>=2)
    {
        if (choice==2) //可無限次數更換色卡直到兌換禮物為止
        {
            A.randColor(color);
            A.printColor(color);
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> choice;
        }
        else //若輸入的數字超出範圍同樣"Out of range!"
        {
            cout << "Out of range! \n" << "1)Exchange gift 2)Change color?: ";
            cin >> choice;
        }
    }
    A.exchangeGift(color,year); //兌換完禮物後顯示兌換到的禮物
    return 0;
}
