#include <string>
#include <time.h>
#include <cstdlib>
using namespace std;
#ifndef GIFTSYSTEM_H_INCLUDED
#define GIFTSYSTEM_H_INCLUDED

class GiftSystem
{
public:
    GiftSystem();
    void setYear();
    void randColor();
    void exchangeGift();
    void OutputGift();
private:
    int color;
    int age;
};
GiftSystem::GiftSystem()
{
    age = 0;             //歸零
    color = 0;
}
void GiftSystem::setYear()
{
    cout << "How old are you?: ";     //輸入age
    cin >> age;
    while(age<0||age>125)
    {
        cout << "Out of range!" << endl <<"How old are you?: ";
        cin >> age;
    }
}

void GiftSystem::randColor()
{
    srand(time(0));
    color = rand()%3;    //隨機出顏色
    switch(color)
    {
    case 0 :             //依數字輸出代表顏色
        cout << "Red";
        break;
    case 1 :
        cout << "Green";
        break;
    case 2 :
        cout << "White";
        break;
    }
    cout << endl;
}

void GiftSystem::exchangeGift()
{
    int ex;
    while(true)         //迴圈
    {
        do              //輸入模式
        {
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> ex;
            if(ex<1||ex>2)
                cout << "Out of Range!" << endl;
        }
        while(ex<1||ex>2);
        if(ex==2)             //輸入二時，再丟一次隨機顏色
            GiftSystem::randColor();
        else
            break;
    }
}

void GiftSystem::OutputGift()
{
    char gift[3][4][20]= {{"Lego","Movie ticket","Vitamin","Denture"},{"Toy car","Baseball gloves","Watch","Pipe"},{"Doraemon doll","Ukulele","Wallet","Hair dye"}};
    //以陣列儲存所有禮物
    if(age>=0 && age<12)          //依年齡分類輸出((應該有更好的辦法，不過時間有限，愚鈍如我只好如此了
        cout << gift[color][0] << endl;
    else if(age>=12 && age<18)
        cout << gift[color][1] << endl;
    else if(age>=18 && age<65)
        cout << gift[color][2] << endl;
    else
        cout << gift[color][3] << endl;
}

#endif
