#include <iostream>
#include <cstdlib>
#include <ctime>
#include "A13-102502023.h" // include header file A13-102502023
using namespace std;


int main()
{
    GiftSystem gs; // instantiate object gs of class GiftSystem
    int age = -1,mychoice = 0; // initialize integers age to -1 and mychoice to 0
    do
    {
        cout << "How old are you?: ";
        cin >> age;
        if ( age < 0 || age > 125 )
            cout << "Out of range!" << endl;
    }
    while ( age < 0 || age > 125 ); // do... while loop to check whether age is on demand

    gs.setYear(age); // assign age into function setYear
    gs.randColor(); // call function randColor
    gs.printColor(); // call function printColor

    do // do... while loop to change the color until user input 1
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> mychoice;

        switch ( mychoice ) // switch statemnet to execute the program according the number user input
        {
        case 1:
            gs.exchangeGift();
            break;
        case 2:
            gs.randColor();
            gs.printColor();
            break;
        default:
            cout << "Out of range!" << endl;
            break;
        }
    }
    while ( mychoice != 1 );

    return 0;
}
