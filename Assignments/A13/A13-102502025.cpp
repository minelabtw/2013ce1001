#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "A13-102502025.h"
using namespace std;

int main()
{
    int year=-1;
    int choose;
    GiftSystem Ryuk;                                                                     //宣告物件實體

    srand(time(NULL));                                                                 //亂數初始化

    while(year>125 || year<0)//限制年齡
    {
        cout << "How old are you?: ";
        cin >> year;
        if(year>125 || year<0)
        {
            cout << "Out of range!\n";
        }
    }

    Ryuk.setYear(year);

    do
    {
        //印出顏色並詢問是否重新換色
        Ryuk.randColor();
        Ryuk.printColor();
        cout<<endl;

        do
        {
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>choose;
        }
        while( (choose!=1 && choose!=2)&&cout<<"Out of range!"<<endl);

    }
    while(choose==2);

    Ryuk.exchangeGift();                                             //顯示對照禮物
    return 0;
}
