#ifndef A13-102502523_H_INCLUDED
#define A13-102502523_H_INCLUDED
#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std;
class giftsystem
{
private://宣告private的資料
    int color;
    int age;
    int year;
    string gift[4][3]= {{"Hair dye","Denture","Pipe	"},{"Wallet","Vitamin","Watch"},{"Ukulele","Movie ticket","Baseball gloves"},{"Doraemon doll","Lego","Toy car"}};
public://宣告poblic的函式
    giftsystem();
    void randcolor();
    void setyear();
    void exchangegift();
    void printcolor();
};
giftsystem::giftsystem(){
    color=0;
    age=0;
    year=0;
}
void giftsystem::randcolor() //隨機選出顏色
{
    srand(time(0));
    color=rand()%3;
}
void giftsystem::setyear() //輸入年齡
{
    cout<<"How old are you?: ";
    cin>>age;
    while(age<0||age>125) //判斷範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
        cin>>age;
    }
    if(age>64)
    {
        year=0;   //將年齡的代號存入year
    }
    else if(age>17)
    {
        year=1;
    }
    else if(age>11)
    {
        year=2;
    }
    else
    {
        year=3;
    }
}
void giftsystem::printcolor() //輸出顏色
{
    if(color==0)
    {
        cout<<"white"<<endl;
    }
    else if(color==1)
    {
        cout<<"red"<<endl;
    }
    else
    {
        cout<<"green"<<endl;
    }
}
void giftsystem::exchangegift() //要換顏色或換禮物
{
    int a;
    cout<<"1)Exchange gift 2)Change color?: ";
    cin>>a;
    while(a!=1&&a!=2)
    {
        cout<<"Out of range!"<<endl;
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>a;
    }
    if(a==2)
    {
        giftsystem::randcolor();
        giftsystem::printcolor();
        giftsystem::exchangegift();
    }
    else
    {
        cout<<gift[year][color];//輸出禮物結果
    }
}





#endif // A13-102502523_H_INCLUDED
