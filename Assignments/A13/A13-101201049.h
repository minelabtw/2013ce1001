#ifndef GiftSystem_H
#define GiftSystem_H
#include<iostream>
#include <cstdlib>
#include <ctime>
#include<cstring>

using namespace std;

class GiftSystem
{
    public:
        GiftSystem();
        void setYear(int);
        void randColor();
        void exchangeGift();
    private:
        int Y;
        string Color;
        string Gift;
};

GiftSystem::GiftSystem()
{
    Y=0;
    Color = "noncolor";
}
void GiftSystem::setYear(int a)
{
    Y=a;
}
void GiftSystem::randColor()
{
     srand(time(NULL));
     int R= rand()%3 +1;
     if(R==1)
     {
       Color = "Red";
       cout << Color << endl;
     }
     else if(R==2)
     {
       Color = "Green";
       cout << Color << endl;
     }
     else if(R==3)
     {
       Color = "White";
       cout << Color << endl;
     }
}
void GiftSystem::exchangeGift()
{

    if ( Y<= 125 && Y>= 65 && Color =="Red" )
    {
        Gift = "Denture";
        cout << Gift << endl;
    }
    else if ( Y<= 125 && Y>= 65 && Color =="Green" )
    {
        Gift = "Pipe";
        cout << Gift << endl;
    }
    else if ( Y<= 125 && Y>= 65 && Color =="White" )
    {
        Gift = "Hair dye";
        cout << Gift << endl;
    }
    else if ( Y< 65 && Y>= 18 && Color == "Red" )
    {
        Gift = "Vitamin";
        cout << Gift << endl;
    }
    else if ( Y< 65 && Y>= 18 && Color == "Green" )
    {
        Gift = "Watch";
        cout << Gift << endl;
    }
    else if ( Y< 65 && Y>= 18 && Color == "White" )
    {
        Gift = "Wallet";
        cout << Gift << endl;
    }
    else if ( Y< 18 && Y>= 12 && Color == "Red" )
    {
        Gift = "Movie ticket";
        cout << Gift << endl;
    }
    else if ( Y< 18 && Y>= 12 && Color == "Green" )
    {
        Gift = "Baseball gloves";
        cout << Gift << endl;
    }
    else if ( Y< 18 && Y>= 12 && Color == "White" )
    {
        Gift = "Ukulele";
        cout << Gift << endl;
    }
    else if ( Y< 12 && Y>= 0 && Color == "Red" )
    {
        Gift = "Lego";
        cout << Gift << endl;
    }
    else if ( Y< 12 && Y>= 0 && Color == "Green" )
    {
        Gift = "Toy car";
        cout << Gift << endl;
    }
    else if ( Y< 12 && Y>= 0 && Color == "White" )
    {
        Gift = "Doraemon doll";
        cout << Gift << endl;
    }


}

#endif // GiftSystem_H
