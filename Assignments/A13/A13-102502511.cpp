#include <iostream>
#include "A13-102502511.h"
using namespace std;
int main()
{
    int a;
    GiftSystem GS;

    GS.setYear(0);
    GS.randColor();
    GS.printColor();

    while(a!=1) //兌換顏色的條件
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> a;
        switch(a)
        {
        case 1: //當等於1時 即接兌換
        {
            GS.exchangeGift();
            break;
        }
        case 2: //等於2 隨機改變顏色
        {
            GS.randColor();
            GS.printColor();
            break;
        }
        default: //其他超出範圍 並跳出 重來
        {
            cout << "Out of range!" << endl;
            break;
        }
        }
    }
    return 0;
}
