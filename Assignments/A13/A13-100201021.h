#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H

#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;
class GiftSystem
{
public:
    GiftSystem();
    void setYear(int);
    void randColor();
    void exchangeGift();
private:
    int year;
    int color;
};
GiftSystem::GiftSystem()    //cons...
{
    year = 0;
    color = 0;
}
void GiftSystem::setYear( int yy )
{
    year = yy;
}
void GiftSystem::randColor()
{
    srand(time( 0 ));
    color = rand() % 3; //0, 1, 2
    if (color == 0)
        cout << "Red" << endl;
    else if (color == 1)
        cout << "Green" << endl;
    else            //color=2
        cout << "White" << endl;
}
void GiftSystem::exchangeGift()
{
//~65
    if (year >= 65 && color == 0 )
        cout << "Denture";
    else if (year >= 65 && color == 1 )
        cout << "Pipe";
    else if  (year >= 65 && color == 2 )
        cout << "Hair dye";
//64~18:
    else if  (year >= 18 && color == 0 )
        cout << "Vitamin";
    else if  (year >= 18 && color == 1 )
        cout << "Watch";
    else if  (year >= 18 && color == 2 )
        cout << "Wallet";
//17~12
    else if  (year >= 12 && color == 0 )
        cout << "Movie ticket";
    else if  (year >= 12 && color == 1 )
        cout << "Baseball gloves";
    else if  (year >= 12 && color == 2 )
        cout << "Ukulele";
//11~
    else if  (year >= 0 && color == 0 )
        cout << "Lego";
    else if  (year >= 0 && color == 1 )
        cout << "Toy car";
    else if  (year >= 0 && color == 2 )
        cout << "Doraemon doll";
}


#endif
