#include <iostream>
#include "A13-102502012.h"
using namespace std;
int main(){
	int year, command;
	do{
		cout << "How old are you?: ";
		cin >> year;
	} while ((year<0 || year>125) && cout << "Out of range!" << endl);
	GiftSystem gift = GiftSystem(year); // declare an object
	while (true){
		do{
			cout << "1)Exchange gift 2)Change color ? : ";
			cin >> command;
		} while ((command<1 || command>2) && cout << "Out of range!" << endl);
		if (command == 1){
			gift.exchange();
			break;
		}
		else
			gift.changeColor();
	}
	return 0;
}