#ifndef A13-102502544_H_INCLUDED
#define A13-102502544_H_INCLUDED
#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std;
class GiftSystem
{
public:
    GiftSystem();//constructor
    int year=0;//宣告變數
    void setYear();//函式
    void randColor();//函式
    void printColor();//函式
    void exchangeGift();//函式
private:
    int color=0;//宣告變數
};
GiftSystem::GiftSystem()
{
    srand(time(0));
}
void GiftSystem::setYear()
{
    cin>>year;
}
void GiftSystem::randColor()
{
    color=rand()%3;
}
void GiftSystem::printColor()
{
    switch(color)
    {
    case 0:
    {
        cout<<"red"<<endl;
        break;
    }
    case 1:
    {
        cout<<"green"<<endl;
        break;
    }
    case 2:
    {
        cout<<"white"<<endl;
        break;
    }
    }
}
void GiftSystem::exchangeGift()
{
    if(year>=0 && year<12)
    {
        if(color==0)
            cout<<"Lego"<<endl;
        else if(color==1)
            cout<<"Toy car"<<endl;
        else if(color==2)
            cout<<"Doraemon doll"<<endl;
    }
    if(year>=12 && year<18)
    {
        if(color==0)
            cout<<"Movie ticket"<<endl;
        else if(color==1)
            cout<<"Baseball golves"<<endl;
        else if(color==2)
            cout<<"Ukulele"<<endl;
    }
    if(year>=18 && year<65)
    {
        if(color==0)
            cout<<"Vitamin"<<endl;
        else if(color==1)
            cout<<"Watch"<<endl;
        else if(color==2)
            cout<<"Wallet"<<endl;
    }
    if(year>=65 && year<=125)
    {
        if(color==0)
            cout<<"Denture"<<endl;
        else if(color==1)
            cout<<"Pipe"<<endl;
        else if(color==2)
            cout<<"Hair dye"<<endl;
    }
}
#endif // A13-102502544_H_INCLUDED
