#include<iostream>
#include<cstdlib>
#include<ctime>

#ifndef A13-102502017
#define A13-102502017
using namespace std;


class GiftSystem
{
public:
    GiftSystem()            //創造亂數表
    {
        srand(time(NULL));
    }
    void setYear(){         //輸入年齡
        cout << "How old are you?: ";
        while(cin >> year){
            if(year<0 || year>125)cout << "Out of range!" << endl << "How old are you?: ";
            else break;
        }
    }
    void randColor(){       //隨機顏色
        color = rand()%3;
    }
    void printColor(){      //判斷得到(?)哪個顏色
        cout << ((color==0) ? "Red" : ((color==1)? "Green" : "White")) << endl;
    }
    void exchangeGift(){    //判斷得到哪個禮物
        switch(color){
        case 0:
        //Red
            if(year<12)cout << "Lego";
            else if(year<18)cout << "Movie ticket";
            else if(year<65)cout << "Vitamin";
            else cout << "Denture";
            break;
        case 1:
        //Green
            if(year<12)cout << "Toy car";
            else if(year<18)cout << "Baseball gloves";
            else if(year<65)cout << "Watch";
            else cout << "Pipe";
            break;
        case 2:
        //White
            if(year<12)cout << "Doraemon doll";
            else if(year<18)cout << "Ukulele";
            else if(year<65)cout << "Wallet";
            else cout << "Hair dye";
            break;
        }
    }

private:
    int year=0;
    int color=0;
};

#endif // A13
