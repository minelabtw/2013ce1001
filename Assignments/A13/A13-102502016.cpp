#include <iostream>
#include "A13-102502016.h"

int main()
{
    int age;
    int choice;
    GiftSystem g;
    cout<<"How old are you?: ";
    cin>>age;
    while (age<0 || age>125)
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
        cin>>age;
    }
    g.setYear(age);
    g.randColor();
    g.printColor();
    do
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>choice;
        if (choice==2)
        {
            g.randColor();
            g.printColor();
        }
        if (choice!=2 && choice!=1)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while (choice!=1);
    g.exchangeGift();
    return 0;
}
