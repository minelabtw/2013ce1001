#include "A13-102502506.h"
int main()
{
    GiftSystem GS; //定義GS
    GS.setYear();
    GS.rpColor();
    int choose = 0;
    do
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> choose;
        if( choose == 2 )
        {
            GS.rpColor(); //重抽顏色跟輸出
        }
        else if( choose != 1 )
            cout << "Out of range!" << endl;
    }
    while( choose != 1 );
    GS.exchangeGift(); //送出禮物
    return 0;
}
