#ifndef GiftSystem_H
#define GiftSystem_H
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;
class GiftSystem
{
public:
    void setYear(int a)
    {
        srand(time(0));
        age = a;
    }
    void randColor()
    {
        color= rand()%3+1;
    }
    void printColor()
    {
        if (color==1)
            cout<<"Red"<<endl;
        if(color==2)
            cout<<"Green"<<endl;
        if(color==3)
            cout<<"White"<<endl;
    }
    void exchangeGift()
    {
        if (age<=125 && age>=65)
        {
            switch(color)
            {
            case 1:
                cout<<"Denture";
                break;
            case 2:
                cout<<"Pipe";

                break;
            case 3:
                cout<<"Hair dye";
                break;
            }
        }
        if (age>=18 && age<65)
        {
            switch(color)
            {
            case 1:
                cout<<"Vitamin";
                break;
            case 2:
                cout<<" Watch";
                break;
            case 3:
                cout<<"Wallet";
                break;
            }
        }
        if (age>=12 && age<18)
        {
            switch(color)
            {
            case 1:
                cout<<"Movie ticket";
                break;
            case 2:
                cout<<" Baseball gloves";
                break;
            case 3:
                cout<<"Ukulele";
                break;
            }
        }
        if (age>=0 && age<12)
        {
            switch(color)
            {
            case 1:
                cout<<"Lego";
                break;
            case 2:
                cout<<" Toy car";
                break;
            case 3:
                cout<<"Doraemon doll";
                break;
            }
        }
    }
private:
    int color;
    int age;

};
#endif
