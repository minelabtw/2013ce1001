#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
#ifndef GiftSystem_H
#define GiftSystem_H

class GiftSystem
{
private:
    int year, menu;
    char *str[4][3]={{"Denture","Pipe","Hair dye"},                      //兌換對照表
                     {"Vitamin","Watch","Wallet"},
                     {"Movie ticket","Baseball gloves","Ukulele"},
                     {"Lego","Toy car","Doraemon doll"}};
    char *color[3]={"Red","Green","White"};
public:
    GiftSystem();
    void setYear(int);                                               //實作內容

    void randColor();

    void printColor();

    void exchangeGift();
};


    GiftSystem::GiftSystem()
    {
         year=menu=0;
    }
    void GiftSystem::setYear(int year)                                               //實作內容
    {
        this->year=year;//設定年齡
    }

    void GiftSystem::randColor()
    {

        this->menu=rand()%3;//使用rand()隨機抽色卡
    }

    void GiftSystem::printColor()
    {
        cout<<color[menu];//將抽到的色卡cout出來。
    }

    void GiftSystem::exchangeGift()
    {
        if( 125>=year && year>=65 )                                            //依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
        {
            cout<<str[0][menu];
        }
        else if(65 > year && year>=18)
        {
            cout<<str[1][menu];
        }
        else if(18 > year && year>=12)
        {
            cout<<str[2][menu];
        }
        else
        {
            cout<<str[3][menu];
        }

    }

#endif // GiftSystem_h
