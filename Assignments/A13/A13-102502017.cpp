#include<iostream>
#include "A13-102502017.h"

using namespace std;

int main(void)
{
    int input=0;
    GiftSystem user1;

    user1.setYear();
    while(input!=1)     //在輸入1時給予禮物
    {
        user1.randColor();
        user1.printColor();
        cout << "1)Exchange gift 2)Change color?: ";
        while(cin >> input)
        {
            if(input!=1 && input!=2)cout << "Out of range!" << endl << "1)Exchange gift 2)Change color?: ";
            else break; //輸入符合，跳出迴圈
        }
    }
    user1.exchangeGift();
    return 0;
}
