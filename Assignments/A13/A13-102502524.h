#ifndef A13_H_INCLUDED
#define A13_H_INCLUDED

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

class GiftSystem
{
public:                                                 //宣告函式名稱
    GiftSystem();
    void setYear();
    void randColor();
    void printColor();
    void exchangeGift();
private:                                                //宣告所需變數
    int y;
    int c;
    int e;
};

GiftSystem::GiftSystem()                                //設定變數初始值
{
    y = 0;
    c = 0;
    e = 0;
}
void GiftSystem::setYear()                              //輸入年齡函式
{
    cout << "How old are you?: ";
    while(cin >> y)                                     //檢查輸入的年齡有沒有超出範圍
    {
        if (y>125 || y<0)
        {
            cout << "Out of range!" << endl;
            cout << "How old are you?: ";
        }
        else
            break;
    }
}
void GiftSystem::randColor()                            //隨機選取顏色函式
{
    srand(time(NULL));
    c=(rand()%3);                                       //以0.1.2代表三種顏色
}
void GiftSystem::printColor()                           //將數字轉換成顏色輸出函式
{
    if ( c == 0 )
        cout << "Red" << endl;
    else if ( c == 1 )
        cout << "Green" << endl;
    else if ( c == 2 )
        cout << "White" << endl;
}
void GiftSystem::exchangeGift()                         //交換禮物函式
{
    while(cout << "1)Exchange gift 2)Change color?: ")
    {
        cin >> e;
        if(e==1)                                        //若輸入1，則進行禮物的判斷
            break;
        else if(e==2)                                   //若輸入2，則進行重新隨機顏色和輸出顏色的函式
        {
            randColor();
            printColor();
        }
        else
            cout << "Out of range!" << endl;            //輸入其他數字則輸出超出範圍並重新輸入
    }

    switch(c)                                           //以顏色作為分類依據，之後再依年齡輸出所得禮物
    {
    case 0:                                             //紅色的禮物部分
        if(y<=125 && y>=65)
            cout << "Denture" << endl;
        else if(y<65 && y>=18)
            cout << "Vitamin" << endl;
        else if(y<18 && y>=12)
            cout << "Movie ticket" << endl;
        else if(y<12 && y>=0)
            cout << "Lego" << endl;
        break;
    case 1:                                             //綠色的禮物部分
        if(y<=125 && y>=65)
            cout << "Pipe" << endl;
        else if(y<65 && y>=18)
            cout << "Watch" << endl;
        else if(y<18 && y>=12)
            cout << "Baseball gloves" << endl;
        else if(y<12 && y>=0)
            cout << "Toy car" << endl;
        break;
    case 2:                                             //白色的禮物部分
        if(y<=125 && y>=65)
            cout << "Hair dye" << endl;
        else if(y<65 && y>=18)
            cout << "Wallet" << endl;
        else if(y<18 && y>=12)
            cout << "Ukulele" << endl;
        else if(y<12 && y>=0)
            cout << "Doraemon doll" << endl;
        break;
    }
}

#endif // A13_H_INCLUDED
