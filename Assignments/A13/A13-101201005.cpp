#include "A13-101201005.h"//引用A13-101201005.h檔

void GiftSystem::setYear(int year) //實作在 GiftSystem 的這個命名空間底下的 setYear 函數
{
    age = year;
}
void GiftSystem::randColor()
{
    color = (rand()%3) +1;
}
void GiftSystem::printColor()
{
    if (color==1)
        cout << "Red\n";
    else if (color==2)
        cout << "Green\n";
    else if (color==3)
        cout << "White\n";
}
void GiftSystem::exchangeGift()
{
    int year=age;
    if (color==1)
    {
        if(year<=125 && year>=65)
            cout << "Denture";
        else if(year<65 && year>=18)
            cout << "Vitamin";
        else if(year<18 && year>=12)
            cout << "Movie ticket";
        else
            cout << "Lego";
    }
    else if (color==2)
    {
        if(year<=125 && year>=65)
            cout << "Pipe";
        else if(year<65 && year>=18)
            cout << "Watch";
        else if(year<18 && year>=12)
            cout << "Baseball gloves";
        else
            cout << "Toy car";
    }
    else if (color==3)
    {
        if(year<=125 && year>=65)
            cout << "Hair dye";
        else if(year<65 && year>=18)
            cout << "Wallet";
        else if(year<18 && year>=12)
            cout << "Ukulele";
        else
            cout << "Doraemon doll";
    }
}
GiftSystem::GiftSystem()
{
    age=color=0;//初始化
}
int main()
{
    srand(time(NULL)); //亂數種子

    GiftSystem gs; //宣告 gs 這個變數
    int year, select;

    cout << "How old are you?: ";
    cin >> year;
    while( year<0 || year>125 )
    {
        cout << "Out of range!\n";
        cout << "How old are you?: ";
        cin >> year;
    }

    gs.setYear(year); //call mrthod
    gs.randColor();
    gs.printColor();

    while( true )
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> select;
        while( select<0 || select>2 )
        {
            cout << "Out of range!\n";
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> select;
        }

        if(select==1)
        {
            break;
        }
        else if(select==2)
        {
            gs.randColor();
            gs.printColor();
        }
    }
    gs.exchangeGift();

    return 0;
}
