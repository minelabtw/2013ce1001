#include<iostream>
#include<ctime>
#include<cstdlib>
#include"A13-102502022.h"
using namespace std;

int main()
{
    int n=0;
    GiftSystem gs;             //class的名稱以gs來取代
    cout<<"How old are you?: ";
    gs.setYear();             //呼叫class中的函式
    while(gs.year<0 || gs.year>125)  //迴圈
    {
        cout<<"Out of range!"<<endl;
        cout<<"How old are you?: ";
        gs.setYear();
    }
    gs.randColor();       //呼叫class中的函式
    gs.printColor();
    while(n<10000000)     //迴圈
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>n;
        while(n<1 || n>2)       //迴圈
        {
            cout<<"Out of range!"<<endl;
            cout<<"1)Exchange gift 2)Change color?: ";
            cin>>n;
        }

        if(n==2)      //假設
        {
            gs.randColor();         //呼叫class中的函式
            gs.printColor();
        }
        else if(n==1)             //假設
        {
            gs.exchangeGift();       //呼叫函式
            break;              //跳出迴圈
        }
    }
    return 0;
}

