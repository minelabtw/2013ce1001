#ifndef A13-102502514_H_INCLUDED
#define A13-102502514_H_INCLUDED

#include<cstdlib>
#include<ctime>

using namespace std;

class GiftSystem
{
public:
    GiftSystem();  //建構子
    void setYear(int);  //設定年齡
    void randColor();  //使用rand()隨機抽色卡
    void printColor();  //將抽到的色卡cout出來
    void exchangeGift();  //依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來
private:
    int year;
    int color;
};

GiftSystem::GiftSystem()
{
    year=0;
}

void GiftSystem::setYear(int)
{
    cout <<"How old are you?: ";
    cin >>year;
    while (year>125||year<=0)
    {
        cout <<"Out of range!"<<endl;
        cout <<"How old are you?: ";
        cin >>year;
    }
}

void GiftSystem::randColor()
{
    srand(time(0));
    color=rand()%3;  //設定一個0~2的亂數
}

void GiftSystem::printColor()
{
    if (color==0)
        cout <<"Red"<<endl;  //0代表紅卡
    else if (color==1)
        cout <<"Green"<<endl;  //1代表綠卡
    else if (color==2)
        cout <<"White"<<endl;  //2代表白卡
}

void GiftSystem::exchangeGift()
{
    if (year<12)
    {
        if (color==0)
            cout <<"Lego";
        else if (color==1)
            cout <<"Toy car";
        else if (color==2)
            cout <<"Doraemon doll";
    }
    else if (year<18)
    {
        if (color==0)
            cout <<"Movie ticket";
        else if (color==1)
            cout <<"Baseball gloves";
        else if (color==2)
            cout <<"Ukulele";
    }
    else if (year<65)
    {
        if (color==0)
            cout <<"Vitamin";
        else if (color==1)
            cout <<"Watch";
        else if (color==2)
            cout <<"Wallet";
    }
    else if (year<=125)
    {
        if (color==0)
            cout <<"Denture";
        else if (color==1)
            cout <<"Pipe";
        else if (color==2)
            cout <<"Hair dye";
    }
}

#endif // A13-102502514_H_INCLUDED
