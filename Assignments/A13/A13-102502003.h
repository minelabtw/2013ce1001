#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

class GiftSystem
{
public:
    GiftSystem();
    void setYear(int);  //設定年齡
    void randColor();  //使用rand()隨機抽色卡
    void printColor();  //將抽到的色卡顯示出來
    void exchangeGift();  //依據抽到的色卡及年齡來兌換禮物

private:
    int age;  //年齡 0-125
    int color;  //顏色 1-3
    int gift;  //禮物
};

GiftSystem::GiftSystem()
{
    age=0;
    color=0;
    gift=0;
}

void GiftSystem::setYear(int year)
{
    age=( year>=0 && year<=125 )? year:0;
}

void GiftSystem::randColor()
{
    srand(time(0));
    color=rand()%4;
}

void GiftSystem::printColor()
{
    if(color==1)
        cout<<"Red"<<endl;
    else if(color==2)
        cout<<"Green"<<endl;
    else
        cout<<"White"<<endl;
}

void GiftSystem::exchangeGift()
{
    switch(color)
    {
    case 1:
        if(age>=0&&age<12)
            cout<<"Lego"<<endl;
        else if(age>=12&&age<18)
            cout<<"Movie ticket"<<endl;
        else if(age>=18&&age<65)
            cout<<"Vitamin"<<endl;
        else
            cout<<"Denture"<<endl;
        break;

    case 2:
        if(age>=0&&age<12)
            cout<<"Toy car"<<endl;
        else if(age>=12&&age<18)
            cout<<"Baseball gloves"<<endl;
        else if(age>=18&&age<65)
            cout<<"Watch"<<endl;
        else
            cout<<"Pipe"<<endl;
        break;

    case 3:
        if(age>=0&&age<12)
            cout<<"Doraemon doll"<<endl;
        else if(age>=12&&age<18)
            cout<<"Ukulele"<<endl;
        else if(age>=18&&age<65)
            cout<<"Wallet"<<endl;
        else
            cout<<"Hair dye"<<endl;
        break;

    default:
        break;
    }

}

