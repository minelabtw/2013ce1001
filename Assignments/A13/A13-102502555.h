#ifndef A13-102502555_H_INCLUDED
#define A13-102502555_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std;

class GiftSystem{  //宣告禮物系統類別
public:
    GiftSystem();  //初始化
    void setyear();  //設定年齡
    void randColor();  //隨機換顏色
    void printColor();  //印出顏色
    void exchangeGift();  //換禮物
    void change(class GiftSystem gs);  //換禮物或顏色
private:
    int year;  //年齡
    int card;  //顏色卡
};

GiftSystem::GiftSystem(){  //初始化隨機卡片顏色
    srand(time(0));
    card = rand() % 3;
}

void GiftSystem::setyear(){  //設定年齡
    do{  //檢查年齡有無超出範圍
        cout << "How old are you?: ";
        cin >> year;
        if(year < 0 || year > 125){
            cout << "Out of range!" << endl;
        }
    }while(year < 0 || year > 125);
}

void GiftSystem::randColor(){  //隨機換卡片顏色
    srand(time(0));
    card = rand() % 3;
}

void GiftSystem::printColor(){  //印出顏色
    switch(card){
        case 0:
            cout << "Red" << endl;
            break;

        case 1:
            cout << "Green" << endl;
            break;

        case 2:
            cout << "White" << endl;
            break;

        default:
            break;
    }
}

void GiftSystem::exchangeGift(){  //按照年齡與顏色換禮物
    int old;
    string gifts[4][3] = {"Denture" , "Pipe" , "Hair dye" , "Vitamin" , "Watch" , "Wallet" ,"Movie ticket" , "Baseball gloves" , "Ukulele" ,"Lego" , "Toy car" , "Doraemon doll"};

    if(year >= 65 && year <= 125){  //區間0
        old = 0;
    }else if(year >= 18 && year < 65){  //區間1
        old = 1;
    }else if(year >= 12 && year < 18){  //區間2
        old = 2;
    }else if(year >= 0 && year < 12){  //區間3
        old = 3;
    }

    cout << gifts[old][card] << endl;
}

void GiftSystem::change(class GiftSystem gs){  //換禮物或換色卡
    int choise;  //選擇
    do{
        do{  //檢查選擇有無超出範圍
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> choise;
            if(choise != 1 && choise != 2){
                cout << "Out of range!" << endl;
            }
        }while(choise != 1 && choise != 2);

        if(choise == 2){  //換顏色並印出
            gs.randColor();
            gs.printColor();
        }

    }while(choise != 1);
    if(choise == 1){
        gs.exchangeGift();  //換禮物囉
    }
}
#endif // A13-102502555_H_INCLUDED
