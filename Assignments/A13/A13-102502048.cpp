#include<iostream>
#include<ctime>
#include<cstdlib>
#include"A13-102502048.h"
using namespace std;
int main()
{
    int ch=0;
    GiftSystem g;
    g.setYear();//設定年齡
    g.randColor();//使用rand()隨機抽色卡
    g.printColor();//將抽到的色卡cout出來。
    while(ch!=1 && ch!=2)
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>ch;
        if(ch==1)
            g.exchangeGift();//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
        else if(ch==2)
        {
            g.randColor();
            g.printColor();
            ch=3;
        }
        else
            cout<<"Out of range!\n";
    }
    return 0;
}
