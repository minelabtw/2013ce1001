#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;
class GiftSystem{
	enum Color{ GREEN = 0, RED = 1, WHITE = 2 };
public:
	GiftSystem(int);
	void changeColor();
	void exchange();
private:
	int year;
	Color color;
};
GiftSystem::GiftSystem(int year){ // constructor, set year
	this->year = year;
	srand((unsigned)time(NULL));
	changeColor();
}
void GiftSystem::changeColor(){
	color = (Color)(rand() % 3);
	if (color == GREEN)	cout << "Green" << endl;
	if (color == RED)	cout << "Red" << endl;
	if (color == WHITE)	cout << "White" << endl;
}
void GiftSystem::exchange(){
	if (year < 12){
		if (color == RED)	cout << "Lego" << endl;
		if (color == GREEN)	cout << "Toy car" << endl;
		if (color == WHITE)	cout << "Doraemon doll" << endl;
	}
	else if (year < 18){
		if (color == RED)	cout << "Movie ticket" << endl;
		if (color == GREEN)	cout << "Baseball gloves" << endl;
		if (color == WHITE)	cout << "Ukulele" << endl;
	}
	else if (year < 65){
		if (color == RED)	cout << "Vitamin" << endl;
		if (color == GREEN)	cout << "Watch" << endl;
		if (color == WHITE)	cout << "Wallet" << endl;
	}
	else{
		if (color == RED)	cout << "Denture" << endl;
		if (color == GREEN)	cout << "Pipe" << endl;
		if (color == WHITE)	cout << "Hair dye" << endl;
	}
	/*
	| ---------------------------------------------------------------- - |
	|      year\color   | Red          | Green           | White         |
	| ---------------------------------------------------------------- - |
	| 125 >= year >= 65 | Denture      | Pipe            | Hair dye      |
	| 65 > year >= 18   | Vitamin      | Watch           | Wallet        |
	| 18 > year >= 12   | Movie ticket | Baseball gloves | Ukulele       |
	| 12 > year >= 0    | Lego         | Toy car         | Doraemon doll |
	| ---------------------------------------------------------------- - |
	*/
}