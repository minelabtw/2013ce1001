#include <iostream>
#include <cstdlib>
#ifndef A13-102502020_H
#define A13-102502020_H     //定義GiftSystem class
using namespace std;

class GiftSystem            //GiftSystem class裡的內容
{
public:
    GiftSystem();           //constructor
    void setYear(int);      //宣告函式
    void randColor();
    void printColor();
    void exchangeGift();
private:
    char *Gift1[3]= {"Denture","Pipe","Hair dye"};                  //宣告字元陣列
    char *Gift2[3]= {"Vitamin","Watch","Wallet"};
    char *Gift3[3]= {"Movie ticket","Baseball gloves","Ukulele"};
    char *Gift4[3]= {"Lego","Toy car","Doraemon doll"};
    int year;                                                       //宣告變數
    int color;
};
GiftSystem::GiftSystem()             //使所有GiftSystem的物件都有初始值
{
    year=color=0;
}
void GiftSystem::setYear(int year)   //設定年齡
{
    this->year=year;                 //將輸入的年齡存在year中
}
void GiftSystem::randColor()         //隨機抽色卡
{
    color=rand()%3+1;
}
void GiftSystem::printColor()        //輸出色卡的顏色
{
    switch(color)
    {
    case 1:
    {
        cout << "Red" << endl;
        break;
    }
    case 2:
    {
        cout << "Green" << endl;
        break;
    }
    case 3:
    {
        cout << "White" << endl;
        break;
    }
    }
}
void GiftSystem::exchangeGift()     //輸出交換到的禮物
{
    if(125>=year && year>=65)
        cout << Gift1[color-1];
    else if(65>year && year>=18)
        cout << Gift2[color-1];
    else if(18>year && year>=12)
        cout << Gift3[color-1];
    else
        cout << Gift4[color-1];
}
#endif // GiftSystem_H



