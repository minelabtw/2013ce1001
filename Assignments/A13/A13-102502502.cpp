#include <iostream>
#include "A13-102502502.h"
using namespace std;

int main()
{
    int b;
    GiftSystem a;
    a.setYear();
    a.randColor();   //使用rand()隨機抽色卡
    a.printColor();  //將抽到的色卡cout出來。
    cout << "1) Exchange gift 2) Change color?: ";
    while (cin >> b)
    {
        if (b == 1)
        {
            a.exchangeGift();
            break;
        }
        else if (b == 2)
        {
            a.randColor();   //使用rand()隨機抽色卡
            a.printColor();  //將抽到的色卡cout出來。
        }
        else
            cout << "Out of range!" << endl << "1) Exchange gift 2) Change color?: ";
    }
    return 0;
}
