#ifndef GIFTSYSTEM_H
#define GIFTSYSTEM_H


class GiftSystem
{
private:
    int age ;

public :
    int color;

    GiftSystem();
    void randColor(void);
    void setYear(void);
    void printColor(void);
    void exchangeGift(void);
};

#endif // GIFTSYSTEM_H
