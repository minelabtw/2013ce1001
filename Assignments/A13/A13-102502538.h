#ifndef A13-102502538_H_INCLUDED
#define A13-102502538_H_INCLUDED

#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

class GiftSystem
{
public :
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();

private :
};
    int old;
    int color;
void setYear(int year)//設定年齡
{
    old=year;
}
void randColor()//使用rand()隨機抽色卡
{
    srand(time(NULL));
    color=rand()%3;
}
void printColor()//將抽到的色卡cout出來。
{
    if(color==0)
        cout<<"Red"<<endl;
    else if(color==1)
        cout<<"Green"<<endl;
    else if(color==2)
        cout<<"White"<<endl;
}
void exchangeGift()//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
{
    if ( old <= 125 && old >= 65 && color == 0 )
        cout << "Denture";
    if ( old <= 125 && old >= 65 && color == 1 )
        cout << "Pipe";
    if ( old <= 125 && old >= 65 && color == 2 )
        cout << "Hair dye";
    if ( old < 65 && old >= 18 && color == 0 )
        cout << "Vitamin";
    if ( old < 65 && old >= 18 && color == 1 )
        cout << "Watch";
    if ( old < 65 && old >= 18 && color == 2 )
        cout << "Wallet";
    if ( old < 18 && old >= 12 && color == 0 )
        cout << "Movie ticket";
    if ( old < 18 && old >= 12 && color == 1 )
        cout << "Baseball gloves";
    if ( old < 18 && old >= 12 && color == 2 )
        cout << "Ukulele";
    if ( old < 12 && old >= 0 && color == 0 )
        cout << "Lego";
    if ( old < 12 && old >= 0 && color == 1 )
        cout << "Toy car";
    if ( old < 12 && old >= 0 && color == 2 )
        cout << "Doraemon doll";
}

#endif // A13-102502538_H_INCLUDED
