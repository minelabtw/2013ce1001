#ifndef A13-102502007_H_INCLUDED
#define A13-102502007_H_INCLUDED
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;
class giftsystem
{
public:
    giftsystem();//constructor
    void setyear(int*);//prototype
    void randcolor(int*);
    void printcolor(int*);
    void exchangegift(int*,int*);
};
giftsystem::giftsystem()
{
}
void giftsystem::setyear(int *age)
{
    do
    {
        cout << "How old are you?: ";
        cin >> *age;
        if(*age<0 or *age>125)
            cout << "Out of range!" << endl;
    }
    while(*age<0 or *age>125);//loop until correct age is input
}//輸入年齡的方法，並用call by pointer 紀錄
void giftsystem::randcolor(int *card)
{
    srand(time(NULL));
    *card=rand()%3+1;
    //卡片1,2,3 代表三個不同顏色
}//亂數函式，隨機給出1,2,3三種結果
void giftsystem::printcolor(int *card)
{
    switch(*card)
    {
    case 1:
    {
        cout << "Red" << endl;
        break;
    }
    case 2:
    {
        cout << "Green" << endl;
        break;
    }
    case 3:
    {
        cout << "White" << endl;
        break;
    }
    default:
        break;
    }
}//由1,2,3三種結果給出相對應的輸出
void giftsystem::exchangegift(int *card , int *age)
{
    if(*card==1 && 0<=*age && *age<12)
        cout << "Lego" << endl;
    else if(*card==1 && 12<=*age && *age<18)
        cout << "Movie ticket" << endl;
    else if(*card==1 && 18<=*age && *age<65)
        cout << "Vitamin" << endl;
    else if(*card==1 && 65<=*age && *age<=125)
        cout << "Denture" << endl;
    else if(*card==2 && 0<=*age && *age<12)
        cout << "Toy car" << endl;
    else if(*card==2 && 12<=*age && *age<18)
        cout << "Baseball gloves" << endl;
    else if(*card==2 && 18<=*age && *age<65)
        cout << "Watch" << endl;
    else if(*card==2 && 65<=*age && *age<=125)
        cout << "Pipe" << endl;
    else if(*card==3 && 0<=*age && *age<12)
        cout << "Doraemon doll" << endl;
    else if(*card==3 && 12<=*age && *age<18)
        cout << "Ukulele" << endl;
    else if(*card==3 && 18<=*age && *age<65)
        cout << "Wallet" << endl;
    else
        cout << "Hair dye" << endl;
}//兌換禮物方法
//3種卡片4個年齡層共12個輸出


#endif // A13-102502007_H_INCLUDED
