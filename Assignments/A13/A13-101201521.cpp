#include <iostream>
#include "A13-101201521.h"//include the class GiftSystem
using namespace std;

int main()
{
    GiftSystem user;
    int age=0, choice=0;

    do//ask age
    {
        cout << "How old are you?: ";
        cin >> age;
    }
    while((age>125 || age<0) && cout << "Out of range!" << endl);
    user.setYear(age);//set the age
    user.randColor();//change color
    user.printColor();//print color
    do//ask to change color or exchange gift
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> choice;
        if(choice==1)
        {
            user.exchangeGift();
            break;
        }
        else if(choice==2)
        {
            user.randColor();
            user.printColor();
        }
    }
    while((choice!=1 && choice!=2) && cout << "Out of range!" << endl
            || choice!=1);

    return 0;
}
