#include <iostream>
#include "A13-102502515.h"//include A13-102502515 class definition
using namespace std;

int main()
{
    GiftSystem gift;
    int choice;
    int years;

    do//make sure that the cin is in the range
    {
        cout << "How old are you?: " ;
        cin  >> years;
        if (years>125||years<0)
        {
            cout << "Out of range!" << endl;
        }
    }
    while (years>125||years<0);

    gift.setYear(years);//use class gift setYear
    gift.randColor();
    gift.printColor();



    while (true)//perform
    {
        do//make sure that cin is in the range
        {
            cout << "1)Exchange gift 2)Change color?: " ;
            cin  >> choice;
            if (choice!=1&&choice!=2)
            {
                cout << "Out of range!" << endl;
            }
        }
        while (choice!=1&&choice!=2);

        if (choice==2)//if choose 2, regain a new color
        {
            gift.randColor();
            gift.printColor();
        }
        else if (choice==1)
        {
            gift.exchangeGift();
            break;
        }
    }
    return 0;
}
