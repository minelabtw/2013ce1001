#include<iostream>
#include<cstdlib>
#include "A13-102302053.h"


using namespace std;

int main()
{
    GiftSystem giftsystem;

    int y ,choice = 2;

    do//請使用者輸入年齡,年齡介於0歲到125歲之間
    {
        cout << "How old are you?: ";
        cin >> y;
    }
    while(((y < 0) || (y > 125)) && cout << "Out of range!\n");

    giftsystem.setYear(y);

    while(choice != 1)//使用者選擇是否要換顏色,換的話重新給一個顏色,不換的話顯示禮物
    {
        giftsystem.randColor();
        giftsystem.printColor();
        cout << "\n";
        do
        {
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> choice;
        }
        while(((choice < 1) || (choice > 2)) && cout << "Out of range!\n");
    }
    giftsystem.exchangeGift();

    cout << endl;
    system("pause");
    return 0;
}


