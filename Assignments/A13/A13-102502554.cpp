#include <iostream>
#include <cstdlib>
#include <ctime>
#include "A13-102502554.h"
using namespace std;


int main()
{
    GiftSystem GS;
    int age = 0;//年齡
    int change = 0;//表示是否要換色的變數

    cout << "How old are you?: ";
    cin >> age;
    while ( age < 0 || age > 125 )
    {
        cout << "Out of range!" << endl;
        cout << "How old are you?: ";
        cin >> age;
    }//輸入年齡

    GS.setYear(age);//判斷年齡範圍
    GS.randColor();//隨機抽顏色
    GS.printColor();//輸出顏色

    do
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> change;//輸入要不要換色

        if ( change == 1 )
        {
            GS.exchangeGift();//抽禮物並輸出
        }
        else if ( change == 2 )
        {
            GS.randColor();
            GS.printColor();//換色後輸出
        }
        else if ( change <= 1 || change >= 2 )
            cout << "Out of range!" << endl;//不合的範圍
    }
    while ( change != 1 );//重複輸入

    return 0;
}
