#include "A13-102502031.h"

int main()
{
    GiftSystem user;

    user.getYear();    //prmppt user for data year, and get yearCategory value defined in gift list in A13-102502031.h
    user.randColor();    //make a randon value of color, and make sure user get the color he or she wants
    user.exchangeGift();    //give the gift corrrespond to the user's age and the color he choose

    return 0;
}
