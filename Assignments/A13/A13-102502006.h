#ifndef A13-102502006_H
#define A13-102502006_H
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class GiftSystem // 建立class
{

public:
    GiftSystem(); // 歸零
    void setYear();//設定年齡
    void randColor();//使用rand()隨機抽色卡
    void choosemode(); // 選擇模式
    void exchangeGift(); //依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。

private:
    int color; // 字卡顏色
    int mode; // 模式
    int a; // 年齡模式
    int age; // 年齡
};

GiftSystem::GiftSystem() // 歸零
{
    age = -1;
    color = mode = a =0;
}

void GiftSystem::setYear() // 年齡輸入
{
    while(age<0 || age >125)
    {
        cout << "How old are you?: ";
        cin >> age;
        if(age<0 || age>125)cout << "Out of range!"<<endl;
    }

    if(age<12)a=0;
    else if(age<18)a=1;
    else if(age<65)a=2;
    else a=3;
}

void GiftSystem::randColor() // 亂數顏色
{
    color = rand()%3+1;
    switch(color)
    {
    case 1:
        cout << "Red" << endl;
        break;
    case 2:
        cout << "Green" << endl;
        break;
    case 3:
        cout << "White" << endl;
        break;
    default:
        break;
    }

    GiftSystem::choosemode();
}

void GiftSystem::choosemode() // 選擇模式
{
    mode = 0;
    while(mode!= 1 && mode!=2)
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> mode;
        if(mode!=1 && mode !=2)cout << "Out of range!" << endl;
    }
    mode==1 ? GiftSystem::exchangeGift() : GiftSystem::randColor();
}

void GiftSystem::exchangeGift()  // 很冗的輸出
{
    switch(color)
    {
    case 1:
        if(a==0)cout << "Lego" << endl;
        else if(a==1)cout << "Movie ticket" << endl;
        else if(a==2)cout << "Vitamin" << endl;
        else cout << "Denture" << endl;
        break;
    case 2:
        if(a==0)cout << "Toy car" << endl;
        else if(a==1)cout << "Baseball gloves" << endl;
        else if(a==2)cout << "Watch" << endl;
        else cout << "Pipe" << endl;
        break;
    case 3:
        if(a==0)cout << "Doraemon doll" << endl;
        else if(a==1)cout << "Ukulele" << endl;
        else if(a==2)cout << "Wallet" << endl;
        else cout << "Hair dye" << endl;
        break;
    default:
        break;
    }
}
#endif // A13-102502006_H_INCLUDED
