#include<iostream>
#include<cstdlib>
#include<string>
#include<ctime>
using namespace std;

class Giftsystem  // 宣告 Giftsystem class
{
public:

    void setYear(int old)  //setYear function
    {
        year =old;
        do              //做掛號裡面的動作
        {
            cout << "How old are you?: " ;
            cin >> year;
            if(year<0 || year>=125)     //如果(0>year>=125)會顯示"Out of rang"
                cout << "Out of range!" << endl;
        }
        while(year<0 || year>=125); //超過條件就迴圈
    }

    void randColor()
    {
        srand(time(0));     //隨機選擇顏色
        a=1+rand()%3;       //只有3種顏色
        printColor(a);          //呼叫function printColor
    }

    void printColor(int a)
    {
        if(a==1)        //如果電腦由1~3選出1的話就紅色, 2=綠色, 3=白色
            cout << "Red" << endl;
        else if(a==2)
            cout << "Green" << endl;
        else
            cout << "white" << endl;
    }

    void exchangeGift()     //對換禮物的function
    {
        int c=year; //以年齡和顏色對換禮物
        int d=a;

        if(d==1)            //如果是紅色
        {
            if(c>=65 && c<=125)  //年齡在65~125之間就換"Denture"
                cout << "Denture" << endl;
            else if(c>=18 && c<65)  //年齡在18~65之間就換"Vitamin"
                cout << "Vitamin" << endl;
            else if(c>=12 && c<18)  //年齡在12~18之間就換"Movie ticket"
                cout << "Movie ticket" << endl;
            else                    //年齡在0~12之間就換"Lego"
                cout << "Lego" << endl;
        }
        if(d==2)
        {
            if(c>=65 && c<=125)
                cout << "Pipe" << endl;
            else if(c>=18 && c<65)
                cout << "watch" << endl;
            else if(c>=12 && c<18)
                cout << "Baseball gloves" << endl;
            else
                cout << "Toy car" << endl;
        }
        if(d==3)
        {
            if(c>=65 && c<=125)
                cout << "Hair dye" << endl;
            else if(c>=18 && c<65)
                cout << "Wallet" << endl;
            else if(c>=12 && c<18)
                cout << "Ukalele" << endl;
            else
                cout << "Doraemon doll" << endl;
        }
    }
private:
    int a=0,year=0;
};
