#ifndef A13_101201046_H
#define A13_101201046_H

#include <iostream>
#include <stdlib.h>

class Giftsystem {
    private:
        static const char *table[4][3]; //gift table
        static const char *color[3]; //color names
        int opt;
        int c_type;
    public:
        void setgifts(int y) { opt = (y > 11) + (y > 17) + (y > 64); } //set gifts by year

        void randc(void) { c_type = (rand() / (RAND_MAX / 3 + 1)); } //random color type

        void printc(void) { std::cout << color[c_type] << "\n"; } //print color

        void exchange(void) { std::cout << table[opt][c_type] << "\n"; } //exchange the gift
    };

const char *Giftsystem::table[4][3] = {{"Lego", "Toy car", "Doraemon doll"},
                                       {"Movie ticket", "Baseball gloves", "Ukulele"},
                                       {"Vitamin", "Watch", "Wallet"},
                                       {"Denture", "Pipe", "Hair dye"}};
const char *Giftsystem::color[3] = {"Red", "Green", "White"};
#endif // A13_101201046_H

