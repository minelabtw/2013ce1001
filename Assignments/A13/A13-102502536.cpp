#include<iostream>
#include <cstdlib>
#include <ctime>

#include"A13-102502536.h"

using namespace std;

int main()
{
    GiftSystem gift;

    int a=0,b=0;

    cout << "How old are you?: ";
    cin  >> a;

    while(a>125 || a<0)
    {
        cout << "Out of range!" << endl;
        cout << "How old are you?: ";
        cin  >> a;
    }

    gift.setYear(a);
    gift.randColor();
    gift.printColor();

    do
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin  >> b;

        if(b!=1 && b!=2)
        {
            cout << "Out of range!" << endl;
            cout << "1)Exchange gift 2)Change color?: ";
            cin  >> b;
        }

        if(b==1)
        {
            gift.exchangeGift();
        }

        if(b==2)
        {
            gift.randColor();
            gift.printColor();
            continue;
        }
    }
    while(b!=1);

    return 0;
}
