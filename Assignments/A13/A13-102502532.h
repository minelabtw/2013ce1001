#ifndef A13-102502532_H
#define A13-102502532_H

#include<iostream>
#include<cstdlib>                 //contain srand()  rand()
#include<ctime>                  //contain time()
using namespace std;

class GiftSystem                //類別 +名字
{
public:
    GiftSystem();                          //constructor
    void randColor();
    void printColor();
    void exchangeGift(int);

private:
    int rcolor;
    string gift[4][3]= { {"Denture","Pipe","Hair dye"},{"Vitamin","Watch","Wallet"},{"Movie ticket","Baseball gloves","Ukulele"},{"Lego","Toy car","Doraemon doll"} };
    //二維陣列 [row][column] p291
};

GiftSystem::GiftSystem()
{
    rcolor =0;
}

void GiftSystem::randColor()
{
    rcolor = rand()%3;
}

void GiftSystem::printColor()
{
    switch(rcolor)
    {
    case 0:
        cout<<"Red";                 //可直接cout
        break;
    case 1:
        cout<<"Green";
        break;
    case 2:
        cout<<"White";
        break;
    }
}

void GiftSystem::exchangeGift(int year)      //換禮物
{
    if(year>=0 and year<12)
    {
        cout<<gift[3][rcolor];
    }
    else if(year>=12 and year<18)
    {
        cout<<gift[2][rcolor];
    }
    else if(year>=18 and year<65)
    {
        cout<<gift[1][rcolor];
    }
    else if(year>=65 and year<=125)
    {
        cout<<gift[0][rcolor];
    }
}

#endif // A13-102502532_H_INCLUDED

