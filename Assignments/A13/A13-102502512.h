#ifndef A13-102502512_H_INCLUDED
#define A13-102502512_H_INCLUDED

#include<cstdlib>
#include<ctime>
using namespace std;
class giftsystem                    //Line 7: declare a class named giftsystem
{
public:                             //Line 9-16: declare class's members
    giftsystem();
    void randcolor();
    void printcolor();
    void exchangegift(int);
private:
    int year;
    int colorr;
};
giftsystem::giftsystem()            //Line 18-21: define the varibles in class to be zero
{
    colorr=year=0;
}
void giftsystem::randcolor()        //Line 22-35: choose the color randomly
{
    srand(time(0));
    colorr=1+rand()%3;
}
void giftsystem::printcolor()
{
    if(colorr==1)
        cout<<"Red"<<endl;
    else if(colorr==2)
        cout<<"Green"<<endl;
    else if(colorr==3)
        cout<<"White"<<endl;
}
void giftsystem::exchangegift(int y)        //Line 36-81: tell the user the gift they get
{
    switch (y)
    {

    case 1:
    {
        if(colorr==1)
            cout<<"Denture"<<endl;
        else if(colorr==2)
            cout<<"Pipe"<<endl;
        else if(colorr==3)
            cout<<"Hair dye"<<endl;
        break;
    }

    case 2:
    {
        if(colorr==1)
            cout<<"Vitamin"<<endl;
        else if(colorr==2)
            cout<<"Watch"<<endl;
        else if(colorr==3)
            cout<<"Wallet"<<endl;
        break;
    }
    case 3:
    {
        if(colorr==1)
            cout<<"Movie ticket"<<endl;
        else if(colorr==2)
            cout<<"Baseball gloves"<<endl;
        else if(colorr==3)
            cout<<"Ukulele"<<endl;
        break;
    }
    case 4:
    {

        if(colorr==1)
            cout<<"Lego "<<endl;
        else if(colorr==2)
            cout<<"Toy car "<<endl;
        else if(colorr==3)
            cout<<"Doraemon doll "<<endl;
        break;
    }

    }

}

#endif // 102502512_H_INCLUDED
