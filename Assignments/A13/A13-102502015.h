#ifndef GiftSystem_H                //沒定義 就定義
#define GiftSystem_H                //定義Class
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;
class GiftSystem                    //宣告class
{
public:
    void setYear(int a)             //設定年齡
    {
        srand(time(0));             //初始化亂數
        year=a;
    }
    void randColor()
    {
        color=rand()%3+1;           //拿新顏色
    }
    void printColor()               //輸出顏色
    {
        if (color==1)
        {
            cout<<"Red";
        }
        if (color==2)
        {
            cout<<"Green";
        }
        if (color==3)
        {
            cout<<"White";
        }
        cout<<endl;
    }
    void exchangeGift()             //領禮物
    {
        if(125>=year && year>=65)
        {
            if(color==1)
            {
                cout<<"Denture";
            }
            if(color==2)
            {
                cout<<"Pipe";
            }
            if(color==3)
            {
                cout<<"Hair dye";
            }
        }
        else if(65>year && year>=18)
        {
            if(color==1)
            {
                cout<<"Vitamin";
            }
            if(color==2)
            {
                cout<<"Watch";
            }
            if(color==3)
            {
                cout<<"Wallet";
            }
        }
        else if(18>year && year>=12)
        {
            if(color==1)
            {
                cout<<"Movie ticket";
            }
            if(color==2)
            {
                cout<<"Baseball gloves";
            }
            if(color==3)
            {
                cout<<"Ukulele";
            }
        }
        else if(12>year && year>=0)
        {
            if(color==1)
            {
                cout<<"Lego";
            }
            if(color==2)
            {
                cout<<"Toy car";
            }
            if(color==3)
            {
                cout<<"Doraemon doll";
            }
        }
    }
private:
    int year;           //年齡
    int color;          //顏色
};
#endif                  //結束IF
