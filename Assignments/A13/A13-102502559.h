#ifndef A13-102502559_H_INCLUDED
#define A13-102502559_H_INCLUDED
#include<cstdlib>
#include<iostream>
#include<ctime>
#include<cstring>
using namespace std;
class Giftsystem
{
    public:
    Giftsystem()
    {

    }
    void setYear(int);
    void randColor();
    void printColor();
    void exchangeGift();
    private:
        int y ;
        int color;
        static string colortype[3];
        static string gift[4][3];
};
string Giftsystem::colortype[3]={"Red","Green","White"};
string Giftsystem::gift[4][3]={{"Denture","Pipe","Hair Dye"},{"Vitamin","Watch","Wallet"},{"Movie ticket","Baseball gloves","Ukulele"},{"Lego","Toy car","Doraemon doll"}};
void Giftsystem::setYear(int year)
{
    if(year<=125 && year>=65)
        y=0;
    if(year<65 && year>=18)
        y=1;
    if(year<18 && year>=12)
        y=2;
    if(year<12 && year>=0)
        y=3;
}
void Giftsystem::randColor()
{
    srand(time(NULL));
    color=rand()%3;
}
void Giftsystem::printColor()
{
    cout<<colortype[color]<<endl;
}
void Giftsystem::exchangeGift()
{
    int change;
    while(1)
    {
         cout << "1)Exchange gift 2)Change color?: ";
        cin >> change;
        if(change<1 || change>2)
        {
            cout << "Out of range!" << endl;
        }
        else if(change==1)//��§��
        {
            cout << gift[y][color];
            break;
        }
        else if(change==2)//���C��
        {
            Giftsystem::randColor();
            Giftsystem::printColor();
        }
    }
}
#endif // A13-102502559_H_INCLUDED
