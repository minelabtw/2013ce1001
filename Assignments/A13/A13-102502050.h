using namespace std;
class GiftSystem
{
public:
    GiftSystem(int,string [],string [],string [],string []);//prototype: 年齡/顏色/禮物1/禮物2/禮物3
    void randColor();//隨機選顏色
    void printColor();//輸出現在的顏色
    void exchangeGift();//兌換禮物

private:
    int year;
    string color[3];
    string gift[3][4];
    int colorindex;
    int giftindex;
};
GiftSystem::GiftSystem(int setyear,string colorlist[],string giftlist1[],string giftlist2[],string giftlist3[])
{
    year=setyear;
    giftindex=(year>11)+(year>17)+(year>64);
    for(int i=0;i<3;i++)
        GiftSystem::color[i]=colorlist[i];

    for(int i=0;i<4;i++)
        GiftSystem::gift[0][i]=giftlist1[i];
    for(int i=0;i<4;i++)
        GiftSystem::gift[1][i]=giftlist2[i];
    for(int i=0;i<4;i++)
        GiftSystem::gift[2][i]=giftlist3[i];
    srand(time(0));
}
void GiftSystem::randColor()
{
    colorindex=rand()%3;
}
void GiftSystem::printColor()
{
    cout << color[colorindex] << endl;
}
void GiftSystem::exchangeGift()
{
    cout << gift[colorindex][giftindex] << endl;
}
