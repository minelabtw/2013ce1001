#include <iostream>

#include <cstdlib>

#include <time.h>

#ifndef  Giftsystem_h//確認標頭檔沒有定義過

#define  Giftsystem_h

using namespace std ;

class GiftSystem//宣告類別叫做GiftSystem
{
public:
    GiftSystem() ;

    void setYear() ;//宣告函式

    void randColor() ;

    void printColor() ;

    void exchangeGift();

private:
    int y=0, number=0 ;
};

#endif

GiftSystem::GiftSystem()
{
    y=number=0 ;
}

void GiftSystem::setYear()
{

    while (true)
    {
        cout << "How old are you?: " ;

        cin >> y ;

        if (y>125||y<0)
        {
            cout << "Out of range!\n" ;
        }

        else
            break ;
    }
}

void GiftSystem::randColor()
{

    srand(time(NULL)) ;//隨機選取變數

    number=(rand()%3) ;

    if (number==0)
    {
        cout << "White\n" ;
    }

    else if (number==1)
    {
        cout << "Red\n" ;
    }

    else
        cout << "Green\n" ;
}

void GiftSystem::printColor()
{
    int a=0, x=0 ;

    while (x==0)
     {
        cout << "1)Exchange gift 2)Change color?: " ;

        cin >> a ;

        x=0 ;

        switch (a)
        {
        case 1:
            exchangeGift() ;//呼叫函式exchangeGift

            x=1 ;

            break ;

        case 2:
            randColor() ;

            break ;

        default :
            cout << "Out of range!\n" ;

            printColor() ;//呼叫自己

            x=1 ;

            break ;
        }

    }
}

void GiftSystem::exchangeGift()
{
    if (y<=125&&y>=65)
    {
        switch (number)//使用switch來當作條件
        {


        case 0:
            cout << "Hair dye" ;

            break ;

        case 1:
            cout << "Denture" ;

            break ;

        case 2:
            cout << "Pipe" ;

            break ;
        }
    }

    if (y<65&&y>=18)
    {
        switch (number)
        {
        case 0:
            cout << "Wallet" ;

            break ;

        case 1:
            cout << "Vitamin" ;

            break ;

        case 2:
            cout << "Watch" ;

            break ;
        }
    }

    if (y<18&&y>=12)
    {
        switch (number)
        {
        case 0:
            cout << "Ukulele" ;

            break ;

        case 1:
            cout << "Movie ticket" ;

            break ;

        case 2:
            cout << "Baseball gloves" ;

            break ;
        }
    }

    if (y<12&&y>=0)
    {
        switch (number)
        {
        case 0:
            cout << "Doraemon doll" ;

            break ;

        case 1:
            cout << "Lego" ;

            break ;

        case 2:
            cout << "Toy car" ;
        }
    }
}
