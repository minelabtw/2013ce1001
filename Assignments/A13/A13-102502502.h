#ifndef A13-102502502_H_INCLUDED
#define A13-102502502_H_INCLUDED
#include <iostream>
#include <string>
#include <cstdlib>      //contain srand() and rand()
#include <ctime>        //contain time()
using namespace std;

class GiftSystem
{
public:

    GiftSystem();        //自己呼叫自己
    void setYear();       //設定年齡

    void randColor();     //使用rand()隨機抽色卡

    void printColor();    //將抽到的色卡cout出來。

    void exchangeGift();  //依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。

private:
    int _age;             // 年齡
    int _color;           // 色卡
};

GiftSystem::GiftSystem()        //自己呼叫自己
{
    _age = 0;
    _color = 0;
}

void GiftSystem::setYear() //設定年齡
{
    cout << "How old are you?: ";
    while(cin >> _age)
    {
        if (_age < 0 || _age > 125)
            cout << "Out of range!" << endl << "How old are you?: ";
        else
            break;
    }
}

void GiftSystem::randColor()  //使用rand()隨機抽色卡
{
    srand(time(0));
    _color = rand()%3;   //(0<=randNum<=2)
}

void GiftSystem::printColor()  //將抽到的色卡cout出來。
{
    if (_color == 0)
        cout << "Red" << endl;
    else if (_color == 1)
        cout << "Green" << endl;
    else if (_color == 2)
        cout << "White" << endl;
}

void GiftSystem::exchangeGift() //依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
{
    if (-1 < _age && _age < 12)
    {
        if (_color == 0 )
            cout << "Lego";
        else  if (_color == 1)
            cout << "Toy car";
        else if (_color == 2)
            cout << "Doraemon doll";
    }
    else if   (11 < _age && _age < 18)
    {
        if (_color == 0 )
            cout << "Movie ticket";
        else  if (_color == 1)
            cout << "Baseball gloves";
        else if (_color == 2)
            cout << "Ukulele";
    }
    else if   (17 < _age && _age < 65)
    {
        if (_color == 0 )
            cout << "Vitamin";
        else  if (_color == 1)
            cout << "Watch";
        else if (_color == 2)
            cout << "Wallet";
    }
    else if   (64 < _age && _age < 126)
    {
        if (_color == 0 )
            cout << "Denture";
        else  if (_color == 1)
            cout << "Pipe";
        else if (_color == 2)
            cout << "Hair dye";
    }
}
#endif // A13-102502502_H_INCLUDED
