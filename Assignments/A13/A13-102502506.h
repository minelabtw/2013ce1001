#ifndef A13-102502506_H_INCLUDED
#define A13-102502506_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std ;
class GiftSystem
{
public:
    int year;
    int newyear=0;
    int randcolor=0;
    void setYear()
{
    do
    {
        cout << "How old are you?: " ;
        cin >> year ;
        if(year < 0 || year > 125)
            cout << "Out of range!" << endl ;
    }while(year < 0 || year > 125) ;
    if( year >= 65 )  //分4種年齡層
        newyear = 1;
    else if( year >= 18 )
        newyear = 2;
    else if( year >= 12 )
        newyear = 3;
    else
        newyear = 4;
}
void rpColor()  //隨機抽顏色跟輸出
{
    srand( time(0));
    randcolor = ( rand () % 3 ) + 1;
    switch(randcolor)
    {
    case 1:
        cout << "Red" << endl;
        break;
    case 2:
        cout << "Green" << endl;
        break;
    case 3:
        cout << "White" << endl;
        break;
    }
}
void exchangeGift()  //送出禮物
{
    switch(newyear)
    {
    case 1:
        switch(randcolor)
        {
        case 1:
            cout << "Denture";
            break;
        case 2:
            cout << "Pipe";
            break;
        case 3:
            cout << "Hair dye";
            break;
        }
    case 2:
        switch(randcolor)
        {
        case 1:
            cout << "Vitamin";
            break;
        case 2:
            cout << "Watch";
            break;
        case 3:
            cout << "Wallet";
            break;
        }
    case 3:
        switch(randcolor)
        {
        case 1:
            cout << "Movie ticket";
            break;
        case 2:
            cout<< "Baseball gloves";
            break;
        case 3:
             cout<< "Ukulele";
             break;
        }
    case 4:
        switch(randcolor)
        {
        case 1:
            cout<< "Lego";
            break;
        case 2:
            cout<< "Toy car";
            break;
        case 3:
            cout<< "Doraemon doll";
            break;
        }
    }
}
};
#endif // A13-102502506_H_INCLUDED
