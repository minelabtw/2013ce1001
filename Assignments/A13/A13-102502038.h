#ifndef A13_102502038_H
#define A13_102502038_H
#include <stdlib.h>
#include <time.h>
#include <string>

using namespace std;

string colorArr[3] = {"Red","Green","White"};  //define color name array
string giftArr[3][4] = {
  {"Lego","Movie ticket","Vitamin","Denture"/*Seriously,WHAT?*/},
  {"Toy car","Baseball gloves","Watch","Pipe"},
  {"Doraemon doll","Ukulele","Wallet","Hair dye"}
};  //define gift name array

class GiftSystem{
 public:
  string color,contain;
  GiftSystem(int _age){
    this -> age = _age;
    this -> randColor();
  }
 private:
  unsigned int age,colorCode;
  void randColor(void){
    srand(time(NULL));
    this -> colorCode = rand()%3;
    this -> color = colorArr[this -> colorCode];
    this -> contain = giftArr[this -> colorCode][(age<12?0:(age<18?1:(age<65?2:3)))];
  }
  /* dummy function */
  void setYear(int){};
  void printColor(){};
  void exchangeGift(){};
};
#endif /* A13_102502038_H */
