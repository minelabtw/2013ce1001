#include <iostream>
#include <cstdlib>
#include <ctime>
#include "A13-102502001.h"
using namespace std;

int main()
{
    GiftSystem gift;               //引入GiftSystem
    int age=0;
    int mode=0;
    int color=0;
    cout<<"How old are you?: ";   //顯示字串
    cin>>age;
    while(age<0 or age>125)       //判斷範圍
    {
        cout<<"Outof range!"<<endl;
        cout<<"How old are you?: ";
        cin>>age;
    }
    gift.setYear(age);
    gift.randColor();
    gift.printColor();
    do                            //選擇範圍
    {
        cout<<"1)Exchange gift 2)Change color?: ";
        cin>>mode;
        switch(mode)
        {
        case 1:                  //若為1,顯示禮物
            gift.exchangeGift();
            break;

        case 2:                  //若為2,重新顯示顏色
        {
            gift.randColor();
            gift.printColor();
        }
        break;
        default:                //若為其他數為不合法輸入
        {
            cout<<"Out of range!"<<endl;
            break;
        }
        }
    }
    while(mode!=1);            //若不為1,則重複執行

    return 0;
}
