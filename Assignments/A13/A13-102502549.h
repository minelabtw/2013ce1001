#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

#ifndef A13-102502549_H_INCLUDED
#define A13-102502549_H_INCLUDED

class GiftSystem
{
public:

    void setYear(int x)//印年齡
    {
        year=x;
    }

    void randColor()//隨機顏色
    {
        srand(time(0));
        int temp=rand()%3+1;

        if(temp==1)
            color="red";
        if(temp==2)
            color="Green";
        if(temp==3)
            color="White";
    }

    void printColor()//印顏色
    {
        cout<<color<<endl;
    }

    void exchangeGift()//兌換禮物
    {
        if(year>=0&&year<12)
        {
            if(color=="red")
                cout<<"Lego";
            if(color=="Green")
                cout<<"Toy car";
            if(color=="White")
                cout<<"Doraemon doll";
        }

        if(year>=12&&year<18)
        {
            if(color=="red")
                cout<<"Movie ticket";
            if(color=="Green")
                cout<<"Baseball gloves";
            if(color=="White")
                cout<<"Ukulele";
        }

        if(year>=18&&year<65)
        {
            if(color=="red")
                cout<<"Vitamin";
            if(color=="Green")
                cout<<"Watch";
            if(color=="White")
                cout<<"Wallet";
        }

        if(year>=65&&year<=125)
        {
            if(color=="red")
                cout<<"Denture";
            if(color=="Green")
                cout<<"Pipe";
            if(color=="White")
                cout<<"Hair dye";
        }
    }

private:

    string color;//顏色
    int year;//年齡
};

#endif // A13-102502549_H_INCLUDED
