#ifndef A13-102502561_H
#define A13-102502561_H

#include<cstdlib>
#include<ctime>
#include<iostream>
using namespace std;


class GiftSystem
{
public:
    GiftSystem();  //object name
    void setYear(int);  //used to set year
    void randColor();  //randomly draw a color card
    void printColor();  //cout the color drawn
    void exchangeGift();  //exchange the present the the correspond color & age
private:
    int year; //two varibles
    int color;
};

GiftSystem::GiftSystem()
{
    year=0;
}

void GiftSystem::setYear(int)
{
    cout << "How old are you?: ";
    cin >> year;
    while(year>125||year<=0)
    {
        cout << "Out of range!" << endl;
        cout << "How old are you?: ";
        cin >> year;
    }
}

void GiftSystem::randColor()
{
    srand(time(0));
    color=rand()%3;  //a random number from 0~2
}

void GiftSystem::printColor()
{
    if(color==0)
        cout << "Red" << endl;  //0 for red
    else if(color==1)
        cout << "Green" << endl;  //1 for green
    else if(color==2)
        cout << "White" << endl;  //2 for white
}

void GiftSystem::exchangeGift()
{
    if (year<12)
    {
        if(color==0)
            cout << "Lego";
        else if(color==1)
            cout << "Toy car";
        else if(color==2)
            cout << "Doraemon doll";
    }
    else if (year<18)
    {
        if(color==0)
            cout << "Movie ticket";
        else if(color==1)
            cout << "Baseball gloves";
        else if(color==2)
            cout << "Ukulele";
    }
    else if(year<65)
    {
        if(color==0)
            cout << "Vitamin";
        else if(color==1)
            cout << "Watch";
        else if(color==2)
            cout << "Wallet";
    }
    else if(year<=125)
    {
        if(color==0)
            cout << "Denture";
        else if(color==1)
            cout << "Pipe";
        else if(color==2)
            cout << "Hair dye";
    }
}

#endif // A13-102502561_H
