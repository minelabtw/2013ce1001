using namespace std;

class giftsystem
{
private:
	int year;
	int color;
	string gift;

public:
	giftsystem(int year) : year(year)
	{
		setcolor();
		changegift();
	}

	void setcolor()
	{
		color = rand() % 3 + 1;
	}

	void changegift()
	{
		if(0 <= year && year < 12)
		{
			if(color == 1)
				gift = "Lego";
			else if(color == 2)
				gift = "Toy car";
			else
				gift = "Doraemon doll";
		}
		else if(12 <= year && year < 18)
		{
			if(color == 1)
				gift = "Movie ticket";
			else if(color == 2)
				gift = "Baseball gloves";
			else
				gift = "Ukulele";
		}
		else if(18 <= year && year < 65)
		{
			if(color == 1)
				gift = "Vitamin";
			else if(color == 2)
				gift = "Watch";
			else
				gift = "Wallet";
		}
		else
		{
			if(color == 1)
				gift = "Denture";
			else if(color == 2)
				gift = "Pipe";
			else
				gift = "Hair dye";
		}
	}

	void getcolor()
	{
		if(color == 1)
			cout << "Red" << endl;
		else if(color == 2)
			cout << "Green" << endl;
		else
			cout << "White" << endl;
	}

	void getgift()
	{
		cout << gift << endl;
	}

};
