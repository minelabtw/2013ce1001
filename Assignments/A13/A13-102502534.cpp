#include<iostream>
#include"A13-102502534.h"//宣告.h利用在main中
using namespace std;

int main()
{
    GiftSystem GS;

    int ages=0;//宣告變數
    int color=0;
    int choice=0;

    cout<<"How old are you?: ";
    cin>>ages;
    while(ages<0 or ages>125)//輸入範圍需在0~125之間
    {
        cout<<"Out of range!"<<endl<<"How old are you?: ";
        cin>>ages;
    }
    GS.setYear(ages);
    GS.randColor();
    do
    {
        cout<<endl<<"1)Exchange gift 2)Change color?: ";
        cin>>choice;
        switch (choice)
        {
        case 1:
            GS.exchangeGift();
            break;
        case 2:
            GS.randColor();
            break;
        default:
            cout<<"Out of range!";
            break;
        }
    }
    while(choice!=1);
    return 0;
}

