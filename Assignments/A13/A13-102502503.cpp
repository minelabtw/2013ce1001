#include <iostream>
#include<ctime>
#include<cstdlib>
#include "A13-102502503.h"  //載入A13-102502503.h
using namespace std;
int main()
{
    GiftSystem g;  //將class GiftSystem宣告為g物件

    int year;
    do  //設定年紀
    {
        cout << "How old are you?: ";
        cin >> year;
        if (year>125 or year<0)
            cout << "Out of range!" << endl;
    }
    while (year>125 or year<0);

    g.setYear(year);  //將年紀傳入setYear函式
    g.randColor();
    g.printColor();
    g.exchangeGift();

return 0;
}
