#include <iostream>
#include "A13-102502556.h"

using namespace std;

int main ()
{
    int control = 0; //宣告一個 int 變數(control)，用來決定要隨機卡片顏色或交換禮物。
    GiftSystem gs; //宣告一個 GiftSystem 為 gs。
    cout << "How old are you?: ";
    gs.setYear( 0 ); //呼叫設置年齡的函數。
    gs.randColor(); //呼叫隨機卡片顏色的函數。
    gs.printColor(); //呼叫印出卡片顏色的函數。
    while ( control != 1 ) //當 control 不為 1 時，可以繼續進行隨機卡片顏色或交換禮物的動作。
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> control;
        while ( control != 1 && control != 2 ) //檢驗輸入的 control 的值是否合乎標準，若不符，則要求其重新輸入。
        {
            cout << "Out of range!" << endl;
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> control;
        }
        if ( control == 1 ) //control 的值為 1 時，
        {
            gs.exchangeGift(); //呼叫交換禮物的函數。
        }
        else //否則，
        {
            gs.randColor(); //呼叫隨機卡片顏色的函數。
            gs.printColor(); //呼叫印出卡片顏色的函數。
        }
    }
    return 0;
}
