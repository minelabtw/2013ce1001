#include <iostream>
#include "A13-101201522.h"//include GiftSystem 
using namespace std;

int main(){
    GiftSystem gift;//宣告一個GiftSystem的object 
    int age,choice;//age為年齡,choice為選項 
    do{//詢問年齡,範圍為(0~125) 
        cout << "How old are you?: ";
        cin >> age;
    }while((age<0 || age>125) && cout << "Out of range!" << endl);
    gift.setYear(age);//設定年齡 
    while(1){
        gift.randColor();//抽色卡 
        gift.printColor();//輸出色卡顏色 
        do{//詢問選擇的選項,範圍(1~2) 
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> choice;
        }while(choice!=1 && choice!=2 && cout << "Out of range!" << endl);
        if(choice == 1){//兌換禮物 
            gift.exchangeGift();
            break;
        }
    }
    return 0;
}
