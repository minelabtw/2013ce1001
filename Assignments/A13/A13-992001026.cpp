#include <iostream>
#include <string>

#include <vector>
#include <algorithm>
#include "A13-992001026.h"

using namespace std;

//第一次呼叫的話不印出，第二次才印出， out of range 好用
string withoutOnce( string str )
{
    static vector<string> strs;
    if( find(strs.begin(), strs.end(), str) != strs.end() )
    {
        return str;
    }
    else
    {
        strs.push_back( str );
        return "";
    }
}

int main()
{
    //設定好所有 static member
    GiftGenerator::init();

    //年齡
    int n;
    //檢查年齡
    do
    {
        cout << withoutOnce( "Out of range!\n" );
        cout << "How old are you?: ";
        cin >> n;
    }
    while( n>125 || n<0 );

    //用年齡生成物件
    GiftGenerator gg(n);
    //換卡與否的選擇
    int choice = 2;
    do
    {
        //判斷 out of range
        if( choice>2 || choice<1 )cout << "Out of range! \n";
        else if( choice == 2 )
        {
            gg.genCard();
            gg.printCard();
        }

        cout << "1)Exchange gift 2)Change color?: ";
        cin >> choice;
    }
    while( choice != 1 );

    //印出禮物
    gg.printGift();
    return 0;
}

