#ifndef IOSTREAM_DEF
#define IOSTREAM_DEF
#include <iostream>
using namespace std;
#endif

#ifndef CSTDLIB_DEF
#define CSTDLIB_DEF
#include <cstdlib>
#endif

#ifndef CTIME_DEF
#define CTIME_DEF
#include <ctime>
#endif

#ifndef GIFTSYSTEM_DEF
#define GIFTSYSTEM_DEF
const string _giftArray[4][3] = {{"Denture", "Pipe", "Hair dye"},
                                     {"Vitamin", "Watch", "Wallet"},
                                     {"Movie ticket", "Baseball gloves", "Ukulele"},
                                     {"Lego", "Toy car", "Doraemon doll"}};
const string _colorArray[3] = {"Red", "Green", "White"};
class GiftSystem{
public:
    GiftSystem(){
        srand((unsigned int)time(0));
        _year = 0;
        this->randColor();
    }
    void setYear(int year){//設定年齡
        this->_year = year;
    }
    void randColor(){//使用rand()隨機抽色卡
        this->_color = rand() % 3;
    }
    void printColor(){//將抽到的色卡cout出來。
        cout << _colorArray[_color] << endl;
    }
    void exchangeGift(){//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來。
        cout << _giftArray[_year][_color] << endl;
    }
private:
    int _year;
    int _color;
    };

#endif
