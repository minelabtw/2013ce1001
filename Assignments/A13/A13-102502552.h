#ifndef A13-102502552_H_INCLUDED
#define A13-102502552_H_INCLUDED
#include<iostream>
#include<cstdlib>
#include<string>
#include<ctime>
using namespace std;

class GiftSystem
{
public:
    GiftSystem();
    void setyear(int);
    void randcolor(int);
    void printcolor(int);
    void exchangegift(int,int);
    int ageclass;
    int colorcode;
private:
};//建一個class

GiftSystem::GiftSystem()
{
    ageclass = colorcode = 0;
}//初始化
void GiftSystem::setyear(int ageclass)
{
    int age;
    do
    {
        cout << "How old are you?:";
        cin >> age;

        if(age >= 65 && age <=125)
        {
            this->ageclass = 0;
        }
        else if(age < 65 && age >= 18)
        {
            this->ageclass = 1;
        }
        else if(age < 18 && age >= 12)
        {
            this->ageclass = 2;
        }
        else if(age < 12 && age >= 0)
        {
            this->ageclass = 3;
        }
        else
        {
            cout << "Out of range!" << endl;
        }
    }
    while(age < 0 || age > 125);
}//詢問年齡，并分層

void GiftSystem::randcolor(int colorcode)
{
    srand(time(0));
    this->colorcode = rand() % 3;
}//隨機抽色卡

void GiftSystem::printcolor(int colorcode)
{
    switch(this->colorcode)
    {
    case 0:
        cout << "Red" << endl;
        break;
    case 1:
        cout << "Green" << endl;
        break;
    case 2:
        cout << "White" << endl;
        break;
    }
}//顯示色卡

void GiftSystem::exchangegift(int ageclass,int colorcode)
{
    int choose = 0;
    string gifts[4][3] = {{"Denture","Pipe","Hair dye"} ,{"Vitamin","Watch","Wallet"},{"Movie ticket","Baseball gloves","Ukulele"},{"Lego","Toy car","Doraemon doll"}};;
    //禮物的陣列
    do
    {
        cout << "1)Exchange gift 2)Change color?:";
        cin >> choose;

        switch(choose)
        {
        case 1:
            cout << gifts[ageclass][colorcode];//顯示禮物
            break;
        case 2:
            randcolor(this->colorcode);
            printcolor(this->colorcode);
            break;//重抽色卡
        default:
            cout << "Out of range!" << endl;
        }
    }while(choose != 1);
}


#endif // A13-102502552_H_INCLUDED
