#ifndef A113_H_INCLUDED
#define A113_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class GiftSystem
{
    public:
        GiftSystem();

        void setYear(int);
        void randColor();
        void printColor();
        void exchangeGift();

    protected:
    private:

        int _year;
        int _color;
};

GiftSystem::GiftSystem()
{
    _year,_color;

    //ctor
}
void GiftSystem :: setYear(int year)
{
    _year=year;
}
void GiftSystem :: randColor()
{
    srand(time(0));
    _color=rand()%3;
}

void GiftSystem :: printColor()
{
    if(_color==0)
    {
        cout << "Red" << endl;
    }
    else if(_color==1)
    {
        cout << "Green" << endl;
    }
    else if(_color==2)
    {
        cout << "White" << endl;
    }
    else
    {
        cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    }
}
void GiftSystem :: exchangeGift()
{
    if(-1<_year && _year <12 && _color==0)
    {
        cout << "Lego" << endl;
    }
    else if(-1<_year && _year <12 && _color==1)
    {
        cout << "Toy car" << endl;
    }
    else if(-1<_year && _year <12 && _color==2)
    {
        cout << "Doraemon doll" << endl;
    }
    else if(11<_year && _year <18 && _color==0)
    {
        cout << "Movie ticket" << endl;
    }
    else if(11<_year && _year <18 && _color==1)
    {
        cout << "Baseball gloves" << endl;
    }
    else if(11<_year && _year <18 && _color==2)
    {
        cout << "Ukulele" << endl;
    }
    else if(17<_year && _year <65 && _color==0)
    {
        cout << "Vitamin" << endl;
    }
    else if(17<_year && _year <65 && _color==1)
    {
        cout << "Watch" << endl;
    }
    else if(17<_year && _year <65 && _color==2)
    {
        cout << "Wallet" << endl;
    }
    else if(64<_year && _year <126 && _color==0)
    {
        cout << "Denture" << endl;
    }
    else if(64<_year && _year <126 && _color==1)
    {
        cout << "Pipe" << endl;
    }
    else if(64<_year && _year <126 && _color==2)
    {
        cout << "Hair dye" << endl;
    }
    else
    {
        cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    }
}

#endif // A113_H_INCLUDED
