#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

class GiftSystem
{
public:
    int colorn,color,_old;
    void setYear(int old)//存入年齡
    {
        _old=old;
    }
    void randColor()//取變數
    {
        srand(time(0));
        colorn=rand()%100;
    }
    void printColor()//輸出顏色
    {
        color=colorn%3;
        if(color==0)
            cout<<"Red"<<endl;
        else if(color==1)
            cout<<"Green"<<endl;
        else
            cout<<"White"<<endl;
    }
    void exchangeGift()//輸出結果
    {
        if(_old<=125 && _old>=65 && color==0)
            cout<<"Denture";
        else if(_old<=125 && _old>=65 && color==1)
            cout<<"Pipe";
        else if(_old<=125 && _old>=65 && color==2)
            cout<<"Hair dye";
        else if(_old<=65 && _old>=18 && color==0)
            cout<<"Vitamin";
        else if(_old<=65 && _old>=18 && color==1)
            cout<<"Watch";
        else if(_old<=65 && _old>=18 && color==2)
            cout<<"Wallet";
        else if(_old<=18 && _old>=12 && color==0)
            cout<<"Movie ticket";
        else if(_old<=18 && _old>=12 && color==1)
            cout<<"Baseball gloves";
        else if(_old<=18 && _old>=12 && color==2)
            cout<<"Ukulele";
        else if(_old<=12 && _old>=0 && color==0)
            cout<<"Lego";
        else if(_old<=12 && _old>=0 && color==1)
            cout<<"Toy car";
        else if(_old<=12 && _old>=0 && color==2)
            cout<<"Doraemon doll";
    }

//private:
//    double _radius; // 半徑
//    string _name; // 名稱
};
