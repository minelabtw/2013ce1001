using namespace std;
class GiftSystem//類別宣告
{
private:
    int age;
    int color;

public:
    void setYear(int);//宣告函式
    void randColor();
    void printColor();
    void exchangeGift();
};
void GiftSystem::setYear(int year)//函式運算
{
    age=year;
}
void GiftSystem::randColor()
{
    srand(time(0));
    color=rand()%3;
}
void GiftSystem::printColor()
{
    GiftSystem::randColor();
    switch(color)
    {
    case 0:
        cout<<"Red"<<endl;
        break;
    case 1:
        cout<<"Green"<<endl;
        break;
    case 2:
        cout<<"White"<<endl;
    }
}
void GiftSystem::exchangeGift()
{
    if(age<=125 && age>=65 && color==0)
    {
        cout<<"Denture";
    }
    if(age<=125 && age>=65 && color==1)
    {
        cout<<"Pipe";
    }
    if(age<=125 && age>=65 && color==2)
    {
        cout<<"Hair dye";
    }
    if(age<65 && age>=18 && color==0)
    {
        cout<<"Vitamin";
    }
    if(age<65 && age>=18 && color==1)
    {
        cout<<"Watch";
    }
    if(age<65 && age>=18 && color==2)
    {
        cout<<"Wallet";
    }
    if(age<18 && age>=12 && color==0)
    {
        cout<<"Movie ticket";
    }
    if(age<18 && age>=12 && color==1)
    {
        cout<<"Baseball gloves";
    }
    if(age<18 && age>=12 && color==2)
    {
        cout<<"Ukulele";
    }
    if(age<12 && age>=0 && color==0)
    {
        cout<<"Lego";
    }
    if(age<12 && age>=0 && color==1)
    {
        cout<<"Toy car";
    }
    if(age<12 && age>=0 && color==2)
    {
        cout<<"Doraemon doll";
    }
}


