#include<iostream>
#include "A13-102502519.h"
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int num1=0,num2=0;
    GiftSystem x;
    srand(time(0));

    do    //年齡
    {
        cout << "How old are you?: ";
        cin >> num1;
        if(num1>125 || num1<0)
            cout << "Out of range!" << endl;
    }
    while(num1>125 || num1<0);

    x.setYear(num1);

    do    //選卡、選擇換卡或拿禮物
    {
        x.randColor();
        x.printColor();
        cout << endl;
        while(num2<1 || num2>2)
        {
            cout << "1)Exchange gift 2)Change color?: ";
            cin >> num2;
            if(num2<1 || num2>2)
                cout << "Out of range!" << endl;
        }
    }
    while(num2==2);
    x.exchangeGift();    //拿禮物
    return 0;
}
