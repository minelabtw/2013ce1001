#ifndef _A13-102502558_H_
#define _A13-102502558_H_
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;
class GiftSystem
{
private:
    // const strings
    static const string colors[3];
    static const string table[3][4];
    int level; // calc from year
    int gift;  // color
public:
    GiftSystem()
    {
        // new color
        randColor();
    }
    void setYear(int y)
    {
        level = (y >= 65) + (y >= 18) + (y >= 12); // calc year
    }
    void randColor()
    {
        gift = rand() % 3; // new color
    }
    void printColor()
    {
        cout << colors[gift] << endl; // print color
    }
    void exchangeGift()
    {
        cout << table[gift][level] << endl; // print gift
    }
};
// const list
const string GiftSystem::colors[3] = {"Red", "Green", "White"};
const string GiftSystem::table[3][4] =
{
    {"Lego","Movie ticket","Vitamin","Denture"},
    {"Toy car","Baseball gloves","Watch","Pipe"},
    {"Doraemon doll","Ukulele","Wallet","Hair dye"}
};


#endif // _A13-102502558_H_
