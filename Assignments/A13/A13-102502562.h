#ifndef A13-102502562_H_INCLUDED
#define A13-102502562_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

class GiftSystem
{
public:
    GiftSystem();//建構子
    void setYear(int);//設定年齡
    void randColor();//使用rand()隨機抽色卡
    void printColor();//將抽到的色卡cout出來
    void exchangeGift();//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物cout出來或是換顏色
private:
    int color;//儲存顏色的號碼
    int y;//儲存年齡
    static string colors[3];//儲存顏色
    static string gift[4][3];//儲存禮物
};
string GiftSystem::colors[3]={"Red","Green","White"};
string GiftSystem::gift[4][3]={{"Lego","Toy car","Doraemon doll"},{"Movie ticket","Baseball gloves","Ukulele"},{"Vitamin","Watch","Wallet"},{"Denture","Pipe","Hair dye"}};
GiftSystem::GiftSystem()
{

}
void GiftSystem::setYear(int year)//把年齡換成代號
{
    if(year>=0 && year<12)
        y=0;
    else if(year>=12 && year<18)
        y=1;
    else if(year>=18 && year<65)
        y=2;
    else if(year>=65 && year<=125)
        y=3;
}
void GiftSystem::randColor()//隨機抽顏色
{
    srand(time(NULL));
    color=rand()%3;
}
void GiftSystem::printColor()//輸出顏色
{
    cout << colors[color] << endl;
}
void GiftSystem::exchangeGift()//換禮物或是換顏色
{
    int change;
    while(1)
    {
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> change;
        if(change<1 || change>2)
        {
            cout << "Out of range!" << endl;
        }
        else if(change==1)//換禮物
        {
            cout << gift[y][color];
            break;
        }
        else if(change==2)//換顏色
        {
            GiftSystem::randColor();
            GiftSystem::printColor();
        }
    }
}
#endif // A13-102502562_H_INCLUDED
