#include "A13-102502002.h" //包含所建的.h檔
#include <iostream>
#include <cstdlib> //class
#include <ctime> //time
using namespace std;

GiftSystem::GiftSystem()
{

}
GiftSystem::~GiftSystem()
{

}
void GiftSystem::setYear()//設定年齡
{
    int yr;
    cout << "How old are you?: ";
    cin >> yr;
    while( yr<0||yr>125 )
    {
        cout << "Out of range!" << endl
             << "How old are you?: ";
        cin >> yr;
    }
    this->year = yr;
}

void GiftSystem::randColor()//使用rand()隨機抽色卡
{
    srand(time(0));
    this->color=(rand()%3);
}

void GiftSystem::printColor()//將抽到的色卡輸出
{
    if(this->color==0)
        cout << "Red" << endl;
    else if(this->color==1)
        cout << "Green" << endl;
    else
        cout << "White" << endl;
}

void GiftSystem::exchangeGift()//依據抽到的色卡及年齡來兌換禮物，並將兌換到的禮物輸出
{
    if(this->year<12)
    {
        if(this->color==0)
            cout << "Lego";
        else if(this->color==1)
            cout << "Toy car";
        else
            cout << "Doraemon doll";
    }
    else if(this->year<18)
    {
        if(this->color==0)
            cout << "Movie ticket";
        else if(this->color==1)
            cout << "Baseball gloves";
        else
            cout << "Ukulele";
    }
    else if(this->year<65)
    {
        if(this->color==0)
            cout << "Vitamin";
        else if(this->color==1)
            cout << "Watch";
        else
            cout << "Wallet";
    }
    else if(this->year>=65)
    {
        if(this->color==0)
            cout << "Denture";
        else if(this->color==1)
            cout << "Pipe";
        else
            cout << "Hair dye";
    }
}

int main()
{
    int choice=0;
    GiftSystem gs;     //創建gs物件
    gs.setYear();
    while(gs.year!=-1)
    {
        gs.randColor();
        gs.printColor();
        cout << "1)Exchange gift 2)Change color?: ";
        cin >> choice;
        while(choice<1||choice>2)
        {
            cout << "Out of range!" << endl
                 << "1)Exchange gift 2)Change color?: ";
            cin >> choice;
        }
        if(choice==1)//若選擇1，兌換禮物後跳出迴圈
        {
            gs.exchangeGift();
            break;
        }
    }
    return 0;
}
