#include <iostream>
#include <ctime>
#include <cstdlib>
#include "A13-102502028.h"
using namespace std ;

int main ()
{
    int old = 0 ;              //宣告
    int change = 0 ;
    GiftSystem a ;

    do                         //輸入年齡
    {
        cout << "How old are you?(0<=year<=125): " ;
        cin >> old ;
    }
    while ((old < 0 || old > 125) && cout << "Out of range!" << endl) ;

    a.setYear(old) ;             //設定年齡
    a.randColor() ;              //抽卡
    a.printColor() ;             //印出卡片顏色

    while (change == 2 || change == 0)        //可以一直換卡
    {
        do                     //換禮物還是換卡
    {
        cout << "1)Exchange gift 2)Change color?: " ;
        cin >> change ;
    }
    while ((change <= 0 || change >= 3) && cout << "Out of range!" << endl) ;

    if (change == 1)           //換禮物
        a.exchangeGift() ;
    else if (change == 2)      //換卡並印出來
    {
        //srand(time(0)) ;
        a.randColor() ;
        a.printColor() ;
    }
    }

    return 0 ;
}
