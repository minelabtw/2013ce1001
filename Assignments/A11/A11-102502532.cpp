#include<iostream>
using namespace std;

int arrayA[5] = {10,8,6,4,2};          //陣列 有初始值
int arrayB[5] = {9,7,5,3,1};
int arrayC[10] = {};

int Size =5;
int bubblesort(int *arr , int Size);
void Merge(int *arr1,int *arr2,int *arr3,int Size);       //不用&

int main()
{
    int *arrA;
    int *arrB;
    int *arrC;
    arrA =arrayA;             //指標 存 陣列的值
    arrB =arrayB;                  //指標 *arr 陣列 array[]
    arrC =arrayC;

    cout<<"Before sort"<<endl<<"array1 elements: ";
    for (int i =0; i <5 ; i++)                         //從 0 開始
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl<<"array2 elements: ";
    for (int i =0; i <5 ; i++)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl<<"After sort";
    cout<<endl<<"array1 elements: ";
    bubblesort(arrayA , Size);            //先表示 函式
    for (int i =0; i <5 ; i++)
    {
        cout<<*(arrA+i)<<" ";               //指標
    }
    cout<<endl<<"array2 elements: ";
    bubblesort(arrayB , Size);
    for (int i =0; i <5 ; i++)
    {
        cout<<*(arrB+i)<<" ";
    }
    cout<<endl<<"After merging two arrays"<<endl<<"array3 elements: ";
    Merge(arrayA ,arrayB ,arrayC ,Size);
    for (int i =0; i <10 ; i++)
    {
        cout<<*(arrC+i)<<" ";                //指標
    }

    return 0;
}

int bubblesort(int *arr , int Size)
{
    for(int round =0; round<Size-1; round++)               //round可少一次
    {
        for (int n =0; n<Size-1; n++)
        {
            if (arr[n] > arr[n+1])
                swap (arr[n],arr[n+1]);          //交換()裡兩數的位置
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int Size)
{
    Size =10;
    int n2 =0;                     //有 n n1 n2
    int n1 =0;

    for (int n =0; n < Size; n++)
    {
        if (n2==5)
            arr3[n] =arr1[n1];
        else if (arr1[n1] < arr2[n2])
        {
            arr3[n] =arr1[n1];
            n1++;
        }
        else if (arr1[n1] > arr2[n2])
        {
            arr3[n] =arr2[n2];
            n2++;
        }
    }
}




