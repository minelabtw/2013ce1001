#include<iostream>
using namespace std;

void bubblesort(int *arr, int _size);                   //泡沫排序函數prototype。
void showarray(int *arr, int _size);                    //印出陣列的函數prtotype。
void Merge(int *arrA, int *arrB, int *arrC,int _size);  //Merge的函數prototype。

int main()
{
    int *arrA,*arrB,*arrC;                              //宣告指標。
    int arrayA[5]= {10,8,6,4,2};                        //宣告陣列。
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10]= {};

    arrA = arrayA;                                      //讓各指標記下各陣列的位置。
    arrB = arrayB;
    arrC = arrayC;

    cout << "Before sort" << endl;                      //印出字串。
    cout << "array1 elements: " ;
    showarray(arrA,5);                                  //呼叫函數印出陣列。
    cout << endl << "array2 elements: ";                //印出字串。
    showarray(arrB,5);                                  //呼叫函數印出陣列。
    bubblesort(arrA,5);                                 //呼叫函數排序陣列元素。
    bubblesort(arrB,5);
    cout << endl << "After sort" << endl;               //印出字串。
    cout << "array1 elements: " ;
    showarray(arrA,5);                                  //呼叫函數印出陣列。
    cout << endl << "array2 elements: ";                //印出字串。
    showarray(arrB,5);                                  //呼叫函數印出陣列。
    Merge(arrA,arrB,arrC,10);                           //呼叫函數融合陣列。
    cout << endl << "After merging two arrays" << endl; //印出字串。
    cout << "array3 elements: ";
    showarray(arrC,10);                                 //呼叫函數印出陣列。

    return 0;
}

void bubblesort(int *arr, int _size)                    //泡沫排序的函數。
{
    int _swap;
    for(int i=0; i<_size; i++)                          //利用兩個for迴圈一一進行排序。
    {
        for(int j=0; j<_size; j++)
        {
            if(arr[i]<arr[j])
            {
                _swap = arr[i];
                arr[i] = arr[j];
                arr[j] = _swap;
            }
        }
    }
}

void showarray(int *arr, int _size)                     //印出陣列的函數。
{
    for(int i=0; i<_size; i++)                          //利用for迴圈依序印出陣列元素。
    {
        cout << arr[i] << " ";
    }
}

void Merge(int *arrA, int *arrB, int *arrC, int _size)  //將兩個陣列融合的函數。
{
    int _swap,arrAelm=0,arrBelm=0;                      //先把兩個陣列的第零個元素相互比較，下次再比較時較小的陣列元素會拿出下一個元素比較。

    for (int i=0; i<_size; i++)
    {
        if(arrA[arrAelm]<arrB[arrBelm] && arrAelm<5)
        {
            arrC[i] = arrA[arrAelm];
            arrAelm++;
        }
        else if (arrA[arrAelm]>arrB[arrBelm] && arrBelm<5)
        {
            arrC[i] = arrB[arrBelm];
            arrBelm++;
        }
        else if (arrAelm>=5)                            //若是元素用完了會拿到宣告範圍以外的陣列元素，所以要讓程式把另一個比較陣列剩下的元素全部印到合併的陣列去。
        {
            arrC[i] = arrB[arrBelm];
            arrBelm++;
        }
        else if (arrBelm>=5)
        {
            arrC[i] = arrA[arrAelm];
            arrAelm++;
        }
    }
}
