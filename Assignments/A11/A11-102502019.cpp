#include<iostream>

using namespace std;

void bubblesort(int *,int );
void Merge(int *,int *,int *,int );
int main()
{
    int arrayA[]={10,8,6,4,2},arrayB[]={9,7,5,3,1},arrayC[10]={};
    cout <<"Before sort"<<endl<<"array1 elements: ";
    for (int k=0; k<5; k++)
    {
        cout <<arrayA[k]<<" ";
    }
    cout <<endl<<"array2 elements: ";
    for (int j=0; j<5; j++)
    {
        cout <<arrayB[j]<<" ";
    }
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);
    cout <<endl<<"After sort"<<endl<<"array1 elements: ";
    for (int k=0; k<5; k++)
    {
        cout <<*(arrayA+k)<<" ";
    }
    cout <<endl<<"array2 elements: ";
    for (int j=0; j<5; j++)
    {
        cout <<*(arrayB+j)<<" ";
    }
    cout <<endl<<"After merging two arrays"<<endl<<"array3 elements: ";
    Merge(arrayA,arrayB,arrayC,10);
    for (int n=0; n<10; n++)
    {
        cout <<*(arrayC+n)<<" ";
    }

}
void bubblesort(int *arr, int size1)
{
    for (int p=1; p<size1; p++)
    {
        int store=*(arr+p);
        while ((p>0)&&(*(arr+p-1)>store))
        {
            *(arr+p)=*(arr+p-1);
            p--;
        }
        *(arr+p)=store;
    }
}
void Merge(int *arr1,int *arr2,int *arr3, int size2)
{
    int counter1=0,counter2=0,counter3=0;
    for (int y=0; y<size2; y++)
    {
        while ((arr1[counter1]>arr2[counter2])&&(counter2<5))
        {
            arr3[counter3]=arr2[counter2];
            counter2++;
            counter3++;
        }
        while ((arr1[counter1]<arr2[counter2])&&(counter1<5))
        {
            arr3[counter3]=arr1[counter1];
            counter1++;
            counter3++;
        }
        if (counter1==5)
        {
            arr3[size2-1]=arr2[4];
        }
        else if (counter2==5)
        {
            arr3[size2-1]=arr1[4];
        }
    }
}
