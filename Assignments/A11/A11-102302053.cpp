#include<iostream>
#include<cstdlib>

using namespace std;

int arrayA[5] = {10,8,6,4,2};
int arrayB[5] = {9,7,5,3,1};
int arrayC[10];

int bubblesort(int* array , int size)//將傳入的陣列依值的大小進行排列
{
    int i, s, tmp;
    for (s = (size - 1); s > 0; s-- )
    {
        for (i = 0; i < (s); i++)
        {
            if(*(array+i) > *(array+1+i))//兩元素比較,小的排後面
            {
                tmp = *(array+1+i);
                *(array+(i+1)) = *(array+i);
                *(array+i) = tmp;
            }
            else continue;
        }
    }

}
int Merge(int *arr1,int *arr2,int *arr3,int size)//將傳入的兩個陣列依次進行元素的大小比較,再擺入第三個陣列
{
    int i, a = 0, b = 0;
    for (i = 0; i < size ; i++)
    {
        if(i == (size - 1))//最後一個元素的比較
        {
            if(a > b)
            {
                *(arr3+i) = *(arr2+b);
            }
            else
            {
                *(arr3+i) = *(arr1+a);
            }
            break;
        }

        if(*(arr1+a) > *(arr2+b))
        {
            *(arr3+i) = *(arr2+b);
            b++;
        }

        else
        {
            *(arr3+i) = *(arr1+a);
            a++;
        }
    }
}

int main ()
{
    int a, b, c;

    //以下顯示尚未處理過的1.2陣列
    cout << "Before sort\n";
    cout << "array1 elements:";
    for (a = 0; a < 5; a++)
    {
        cout << " " << arrayA[a];
    }
    cout << endl;
    cout << "array2 elements:";
    for (b = 0; b < 5; b++)
    {
        cout << " " << arrayB[b];
    }
    cout << endl;

    //以下顯示處理過後的1.2.3陣列
    cout << "After sort\n";
    bubblesort(arrayA , 5);//呼叫函式將陣列1.2進行排列
    bubblesort(arrayB , 5);
    cout << "array1 elements:";
    for (a = 0; a < 5; a++)
    {
        cout << " " << arrayA[a];
    }
    cout << endl;
    cout << "array2 elements:";
    for (b = 0; b < 5; b++)
    {
        cout << " " << arrayB[b];
    }
    cout << endl;
    cout << "After merging two arrays\n";
    cout << "array3 elements:";
    Merge(arrayA,arrayB,arrayC,10);//呼叫函式混和1.2陣列進行排列
    for (c = 0; c < 10; c++)
    {
        cout << " " << arrayC[c];
    }
    cout << endl;

    system("pause");
    return 0;
}


