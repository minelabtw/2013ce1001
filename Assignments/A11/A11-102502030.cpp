#include <iostream>
using namespace std;
void bubblesort( int*, int );
void Merge( int*, int*, int*, int );
main()
{
    int Size=5;  //設陣列大小
    int Size2=10;
    int arrayA[ 5 ]= { 10, 8, 6, 4, 2 };  //設兩陣列
    int arrayB[ 5 ]= { 9, 7, 5, 3, 1 };
    int arrayC[ 10 ]= {};
    int *arrA;  //使用指標
    int *arrB;
    int *arrC;
    arrA=arrayA;
    arrB=arrayB;
    arrC=arrayC;

    cout << "Before sort" << endl;  //排序前
    cout << "array1 elements:";
    for( int i=0; i<5; i++ )
    {
        cout << " " << *( arrA+i );
    }
    cout << endl;
    cout << "array2 elements:";
    for( int j=0; j<5; j++ )
    {
        cout << " " << *( arrB+j );
    }
    cout << endl;

    bubblesort( arrA, Size );
    bubblesort( arrB, Size );
    cout << "After sort" << endl;  //分別排序後
    cout << "array1 elements:";
    for( int i=0; i<5; i++ )
    {
        cout << " " << *( arrA+i );
    }
    cout << endl;
    cout << "array2 elements:";
    for( int j=0; j<5; j++ )
    {
        cout << " " << *( arrB+j );
    }
    cout << endl;

    Merge( arrA, arrB, arrC, Size2 );
    cout << "After merging two arrays" << endl;  //整合
    cout << "array3 elements:";
    for( int k=0; k<10; k++ )
    {
        cout << " " << *( arrC+k );
    }

    return 0;
}
void bubblesort( int* Array, int number )
{
    for( int i=0; i<number; i++ )  //多次執行
    {
        for( int j=1; j<number; j++ )  //比較大小
        {
            if( *(Array+j)<*(Array+(j-1)) )  //交換
            {
                *(Array+j)+=*(Array+(j-1));
                *(Array+(j-1))=*(Array+j)-*(Array+(j-1));
                *(Array+j)-=Array[ j-1 ];
            }
        }
    }
}
void Merge( int* arr1, int* arr2, int* arr3, int Size )
{
    for( int i=0; i<Size-1; i++ )  //存入除了最後一項
    {
        if( *arr1<=*arr2 )
        {
            *(arr3+i)=*arr1;  //存入小的
            for( int j=0; j<4; j++ )
            {
                *(arr1+j)=*(arr1+(j+1));  //用過的陣列左移
            }
        }
        else if( *arr1>*arr2 )
        {
            *(arr3+i)=*arr2;
            for( int k=0; k<4; k++ )
            {
                *(arr2+k)=*(arr2+(k+1));
            }
        }
    }
    if( *(arr1+(Size-1))<*(arr2+(Size-1)) )  //存入最後一項
        *(arr3+(Size-1))=*(arr2+(Size-1));
    else if( *(arr1+(Size-1))>*(arr2+(Size-1)) )
        *(arr3+(Size-1))=*(arr1+(Size-1));
}
