#include <iostream>
using namespace std;

void Merge(int *arr1,int *arr2,int *arr3,int size);
void bubblesort(int* array , int size);

int main()
{
    int arrayA[5]= {10,8,6,4,2};             //宣告陣列，並初始化
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];

    cout << "Before sort" << endl << "array1 elements: ";      //輸出排序前元素
    for(int i=0; i<5; i++)
        cout << arrayA[i] << " ";
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++)
        cout << arrayB[i] << " ";

    cout << endl << "After sort" << endl;                      //輸出排序後元素
    bubblesort(arrayA,5);                                      //呼叫排序函式
    cout << "array1 elements: ";
    for(int i=0; i<5; i++)
        cout << *(arrayA+i) << " ";
    bubblesort(arrayB,5);
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++)
        cout << *(arrayB+i) << " ";

    cout << endl << "After merging two arrays" << endl;
    Merge(arrayA,arrayB,arrayC,10);                              //呼叫合成函式
    cout << "array3 elements: ";
    for(int i=0; i<10; i++)
        cout << *(arrayC+i) << " ";

    return 0;
}

void bubblesort(int*arr , int size) //排序函式，傳入指標、大小，回傳陣列
{
    int a=0;                    //宣告一變數存放元素的值
    int *aptr=arr;              //宣告一指標，指向array

    for(int i=0; i<size; i++)
    {
        for(int i=0; i<size-1; i++)
        {
            if(*aptr<=*(aptr+1));    //若指向的值小於等於下一項，不做動作
            else                     //若指向的值大於下一項，兩項交換
            {
                a=*(aptr+1);
                *(aptr+1)=*aptr;
                *aptr=a;
            }
            aptr++;                  //aptr指向下一項
        }
        aptr=arr;                    //aptr重新指向第一項
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    for(int i=0; i<size-1; i++)
    {
        if(*(arr1+i)<=*(arr2+i))          //陣列1和陣列2依序分項比較大小
        {           //若陣列1小於等於陣列2，將陣列1傳入陣列3的第一項，陣列2傳入第二項
            *(arr3+i)=*(arr1+i);
            *(arr3+i+1)=*(arr2+i);
        }
        else
        {           //若陣列1大於陣列2，將陣列2傳入陣列3的第一項，陣列1傳入第二項
            *(arr3+i)=*(arr2+i);
            *(arr3+i+1)=*(arr1+i);
        }
        arr3++;     //arr3指向下一項
    }
}
