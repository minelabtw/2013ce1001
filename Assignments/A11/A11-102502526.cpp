#include <iostream>
#define arraysize 20
using namespace std;
void bubblesort(int* arr , int Size)  //泡泡排序法(用指標取值)
{
    int temp;   //暫存的arr[i4]的變數
    for(int hahaha=1; hahaha<=Size; hahaha++)
    {
        for(int i4=0; i4<Size-1; i4++)
        {
            if(*(arr+i4)>*(arr+(i4+1))) //arr[k]=> *(arr+k)
            {
                temp=*(arr+i4);
                *(arr+i4)=*(arr+(i4+1));
                *(arr+(i4+1))=temp;
            }
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int Size) //合併排序法(用陣列取值)
{
    int a=0;  //arr1[a]
    int b=0;  //arr2[b]
    for (int k=0; k<Size; k++)
    {
        if (a==5)    //a值等於5的時候就超過arr1的Size了，所以輸出arr2[b]
            arr3[k]=arr2[b];
        else if (b==5)  //同上
            arr3[k]=arr1[a];
        else if (arr1[a]>arr2[b]) //當arr1[a]的值大於arr2[b]，輸出arr2[b]的值
                                  //因為在我們主函式宣告的時候，arr1[a]跟arr2[b]已經是由小到大排序好了
        {
            arr3[k]=arr2[b];
            b++;
        }
        else if (arr1[a]<arr2[b])  //同上
        {
            arr3[k]=arr1[a];
            a++;
        }
    }
}

int main()
{
    int arr1[5]= {10,8,6,4,2};
    int arr2[5]= {9,7,5,3,1};
    int arr3[10];

    cout<<"Before sort"<<endl<<"Array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<arr1[i]<<" ";
    }
    cout<<endl;
    cout<<"Array2 elements: ";
    for(int i2=0; i2<5; i2++)
    {
        cout<<arr2[i2]<<" ";
    }
    cout<<endl;
  bubblesort(arr1,5);   //霹靂卡霹靂拉拉波波莉娜貝貝魯多，召喚bubblesort function
  bubblesort(arr2,5);

    cout<<"After sort"<<endl<<"Array1 elements: ";
    for(int i5=0; i5<5; i5++)
    {
        cout<<arr1[i5]<<" ";
    }
    cout<<endl;
    cout<<"Array2 elements: ";
    for(int i6=0; i6<5; i6++)
    {
        cout<<arr2[i6]<<" ";
    }
    cout<<endl;
    cout<<"After merging two arrays"<<endl<<"array3 elements: ";
    Merge(arr1,arr2,arr3,10);   //召喚Merge function
    for(int i9=0; i9<10;i9++)
    {
    cout<<arr3[i9]<<" ";
    }

    return 0;
}
