#include<iostream>

using namespace std;
void merge(int *arrA,int *arrB,int *arrC,int size)    //合併arrayA跟arrayB的函式
{
    int i=0;                            //計算arrA增加次數
    int j=0;                            //計算arrB增加次數
    for(int v=0; v<size-1; v++)         //v為arrC之counter
    {
        if(*(arrA+i)>*(arrB+j))         //較小的放入arrC
        {
            *(arrC+v)=*(arrB+j);
            j++;                        //比完的後移
            if(j==5)                    //比完則把剩下的給arrC5之最後一個
            {
                *(arrC+v+1)=*(arrA+i);
            }
        }
        else                            //較小的放入arrC
        {
            *(arrC+v)=*(arrA+i);
            i++;                        //比完的後移
            if(i==5)                    //比完則把剩下的給arrC之最後一個
            {
                *(arrC+v+1)=*(arrB+j);
            }
        }
    }
}

int bubblesort(int *array,int ArraySize)
{
    int temp;
    for(int h=ArraySize; h>1; h--)      //總共跑h-1次可把陣列依大小排列
    {
        for(int i=0; i<h-1; i++)        //找每一次的最大數
        {
            if(*(array+i)>*(array+i+1))    //若比下一個大則替換
            {
                temp=*(array+i);
                *(array+i)=*(array+i+1);
                *(array+i+1)=temp;
            }
        }
    }
}

int main()
{
    int arrayA[5]= {10,8,6,4,2};       //宣告3個陣列
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    int *arr;                          //宣告一個指標

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    arr=arrayA;                        //指向arrayA的位置
    for(int i=0; i<5; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl;
    cout<<"array2 elements: ";
    arr=arrayB;                        //指向arrayB的位置
    for(int i=0; i<5; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl;

    cout<<"After sort"<<endl;
    bubblesort(arrayA,5);              //兩陣列依大小排列
    arr=arrayA;
    cout<<"arrayA elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl;
    bubblesort(arrayB,5);
    arr=arrayB;
    cout<<"arrayB elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl;
    cout<<"After merging two arrays"<<endl;
    merge(arrayA, arrayB, arrayC, 10);        //合併兩陣列AB到陣列C
    arr=arrayC;                               //指向arrayC位置
    cout<<"arrayC elements: ";
    for(int j=0; j<10; j++)
    {
        cout<<*(arr+j)<<" ";
    }
    return 0;
}
