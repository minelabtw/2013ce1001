/*************************************************************************
    > File Name: A11-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年12月12日 (週四) 17時29分42秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>

/* function to sort */
void bubblesort(int * array, int size);

/* function to merge */
void Merge(int *arr1, int *arr2, int *arr3, int size);

/* function to print all number in arr[] */
void output(int *arr, int size, int no);

int main()
{
	/* array will be sorted */
	int arrayA[5] = {10, 8, 6, 4 ,2};
	int arrayB[5] = {9, 7, 5, 3, 1};
	int arrayC[10];

	/* ui message */
	puts("Before sort");

	/* print all number in array{A, B} */
	output(arrayA, 5, 1);
	output(arrayB, 5, 2);

	/* sort */
	bubblesort(arrayA, 5);
	bubblesort(arrayB, 5);

	/* ui message */
	puts("After sort");

	/* print all number in array{A, B} */
	output(arrayA, 5, 1);
	output(arrayB, 5, 2);

	/* merge array{A, B} into arrayC */
	Merge(arrayA, arrayB, arrayC, 5);

	/* uimessage */
	puts("After merging two arrays");

	/* print all number in arrayC */
	output(arrayC, 10, 3);

	return 0;
}

/* use call by point to swap *a *b */
void swap(int *a, int *b)
{
	/* three virable swap */
	int t=*a;
	*a=*b;
	*b=t;
}

/* bubble sort */
void bubblesort(int * array, int size)
{
	/* use troll index to trace */
	#define mod(x) ((x)%size)

	/* compare size*size times */
	for(int i=0 ; i<size*size ; i++)
		/* if array[i] > array[i+1] */
		if(mod(i+1) != 0 && array[mod(i)] > array[mod(i+1)])
			/* ,then swap them */
			swap(array+mod(i), array+mod(i+1));
	#undef mod
}

/* merge arr1, arr2 into arr3 */
void Merge(int *arr1, int *arr2, int *arr3, int size)
{
	/* put number into arr3[k] */
	for(int i=0, j=0, k=0 ; k<size*2 ; k++)
		/* put the smaller number between arr1[i] and arr2[j] */
		/* , then the index of small number += 1*/
		if(i!=size && (arr1[i] < arr2[j] || j==size))
			arr3[k] = arr1[i++];
		else
			arr3[k] = arr2[j++];
}

/* print all number in arr */
void output(int *arr, int size, int no)
{
	/* ui message */
	printf("array%d elements:", no);

	/* print all number in arr */
	for(int i=0 ; i<size ; i++)
		printf(" %d", arr[i]);

	/* endl (?) */
	puts("");
}

