#include <iostream>
using namespace std;
void bubblesort(int *array , int size)
{
    for(int i=0 ; i<size-1 ; i++)
        for(int j=i+1 ; j<size ; j++)
        {
            //把每個元素都跟第一個元素比，比較小的往前放，比完之後最小的數就在第一格，再讓剩下的數和第二個元素比，依此類推。
            if(array[i]>array[j])
                swap(array[i],array[j]);
        }
}//set up a function to sort the elements from small to huge
void printarray(int *array , int size , int n)
{
    //陣列從0開始,size為陣列長度，也就是元素個數，陣列從[0]到[size-1]，共size個
    cout << "array" << n << " elements: ";
    for(int i=0 ; i<size ; i++)//n代表陣列的編號
        cout << array[i] << " ";
    cout << endl;
}//set up a function to print the value of the array
void Merge(int *arr1 , int *arr2 , int *arr3 , int size)
{
    for( int i=0 ; i<size ; i++)
        arr3[i]=arr1[i];//先存array1的值
    for( int i=0 ; i<size ; i++)
        arr3[i+size]=arr2[i];//往後推size格，開始存放
    bubblesort(arr3,10);//排序
    cout << endl;
}//merge array1 and array2
int main()
{
    int arrayA[]= {10,8,6,4,2};
    int arrayB[]= {9,7,5,3,1};
    int arrayC[10]= {};
    int nA,nB;
    nA=sizeof(arrayA)/sizeof(arrayA[0]);//length of the array1
    nB=sizeof(arrayB)/sizeof(arrayB[0]);//length of the array2
    cout << "Before sort" << endl;
    printarray(arrayA,nA,1);
    printarray(arrayB,nB,2);
    cout << "After sort" << endl;
    bubblesort(arrayA,nA);
    bubblesort(arrayB,nB);
    printarray(arrayA,nA,1);
    printarray(arrayB,nB,2);
    cout << "After merging two arrays";
    Merge(arrayA,arrayB,arrayC,nA);
    printarray(arrayC,10,3);
    return 0;
}
