#include<iostream>
using namespace std;
void outputarray(int *array,int size);
void bubblesort(int* array,int size);
void Merge(int *arrA,int *arrB,int *arrC,int size);

int main()
{
    int arrayA[5]= {10,8,6,4,2};//宣告陣列
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10]= {};

    cout<<"Befort sort"<<endl;
    cout<<"arrayA elements: ";
    outputarray(arrayA,5);
    cout<<"arrayB elements: ";
    outputarray(arrayB,5);

    bubblesort(arrayA,5);
    bubblesort(arrayB,5);

    cout<<"After sort"<<endl;
    cout<<"arrayA elements: ";
    outputarray(arrayA,5);
    cout<<"arrayB elements: ";
    outputarray(arrayB,5);

    cout<<"After merging two arrays"<<endl;
    cout<<"arrayC elements: ";
    Merge(arrayA,arrayB,arrayC,5);
    outputarray(arrayC,10);

    return 0;
}
void outputarray(int *array,int size)//輸出陣列
{
    for(int i=0; i<size; i++)
        cout<<*(array+i)<<" ";
    cout<<endl;
}
void bubblesort(int* array,int size)//排列大小
{
    for(int b=1; b<size; b++)
    {
        int memory=*(array+b);//存入陣列的值
        while((b>0)&&(*(array+b-1)>memory))//判斷是否需要排列
        {
            *(array+b)=*(array+b-1);
            b--;
        }
        *(array+b)=memory;
    }
}
void Merge(int *arrA,int *arrB,int *arrC,int size)//兩個陣列合併並排序
{
    for(int i=0; i<size; i++)
    {
        if(arrA[i]>arrB[i])
        {
            *(arrC+2*i)=*(arrB+i);//用指標存入arrC
            *(arrC+2*i+1)=*(arrA+i);
        }
        if(arrA[i]<arrB[i])
        {
            *(arrC+2*i)=*(arrA+i);
            *(arrC+2*i+1)=*(arrB+i);
        }
    }
}
