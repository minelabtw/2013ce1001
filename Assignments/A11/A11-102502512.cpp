#include<iostream>
using namespace std;
void bubblesort(int*, int);
void Merge(int* ,int* ,int* ,int);
int main()
{
    int arrayA[5]= {10,8,6,4,2};                    //Line 7-9: define three arrays
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10]= {};
    for(int j=0; j<2; j++)                          //Line 10-34: print arrays before sort and after sort
    {
        if(j==1)
        {
            bubblesort(arrayA, 5);
            bubblesort(arrayB, 5);
            cout<<"After sort"<<endl;
        }
        else
        {
            cout<<"Before sort"<<endl;
        }
        cout<<"array1 elements: ";
        for(int i=0; i<5; i++)
        {
            cout<<arrayA[i]<<" ";
        }
        cout<<endl;
        cout<<"array2 elements: ";
        for(int i=0; i<5; i++)
        {
            cout<<arrayB[i]<<" ";
        }
        cout<<endl;
    }
    Merge(arrayA, arrayB, arrayC,5);                    //Line 35: merge first two arrays and sort the merged arrays.
    cout<<"After merging two arrays"<<endl;             //Line 36-40: print the merged array.
    for(int i=0; i<10; i++)
    {
        cout<<arrayC[i]<<" ";
    }
    return 0;
}
void bubblesort(int* arra,int siz)                  //Line 43-58: define the function bubblesort.
{
    int a;
    for(int i=0; i<siz; i++)
    {
        for(int j=0; j<siz; j++)
        {
            a=arra[i];
            if(arra[i]<arra[j])
            {
                arra[i]=arra[j];
                arra[j]=a;
            }
        }
    }
}
void Merge(int* arra1,int* arra2,int* arra3, int n)   //Line 59-73: define the function Merge.
{
    for(int i=0; i<2*n; i++)
    {
        if(i<n)
        {
            arra3[i]=arra1[i];
        }
        else if(i>=n)
        {
            arra3[i]=arra2[i-n];
        }
    }
    bubblesort(arra3, 2*n);
}
