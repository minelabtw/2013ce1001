#include <iostream>
using namespace std;
int main()
{
    int arrayA[5]= {10,8,6,4,2};  //宣告陣列
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10]= {};
    int bubblesort(int* array , int _size);  //宣告函式
    int Merge(int *arr1,int *arr2,int *arr3,int _size);
    int _size=5,*arr1,*arr2,*arr3;  //宣告變數
    cout<<"Before sort\n";
    cout<<"array1 elements: ";  //輸出變化之前的陣列
    for(int i=0; i<5; i++)
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl;
    cout<<"After sort\n";  //輸出變化之後的陣列
    arr1=arrayA;
    bubblesort(arr1,_size);  //泡沫排序
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<*(arr1+i)<<" ";
    }
    cout<<endl;
    arr2=arrayB;
    bubblesort(arr2,_size);  //泡沫排序
    cout<<"array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<*(arr2+i)<<" ";
    }
    cout<<endl;
    cout<<"After merging two arrays\n";
    arr3=arrayC;
    Merge(arr1,arr2,arr3,_size);  //兩個陣列排成一個陣列
    cout<<"array3 elements: ";
    for(int i=0; i<10; i++)
    {
        cout<<*(arr3+i)<<" ";
    }
    return 0;
}
int bubblesort(int* array , int _size)  //泡沫排序法
{
    int *arr=array;
    for(int v=0; v<5; v++)
    {
        for(int i=0; i<5; i++)
        {
            if(arr[i]>arr[v])
            {
                swap(arr[i],arr[v]);
            }
        }
    }
}
int Merge(int *arr1,int *arr2,int *arr3,int _size)  //兩陣列比大小
{
    int x=0,y=0,i=0;
    while(x<5 && y<5)
    {
        if(x==4 && y==4)
        {
            if(arr1[x]>arr2[y])
            {
                arr3[i]=arr2[y];
                i++;
                arr3[i]=arr1[x];
                i++;
                break;
            }
        }
        else if(arr1[x]>arr2[y])
        {
            arr3[i]=arr2[y];
            y++;
            i++;
        }
        else if(arr1[x]<arr2[y])
        {
            arr3[i]=arr1[x];
            x++;
            i++;
        }
    }
}
