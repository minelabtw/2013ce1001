#include <iostream>

using namespace std;

void exchange(int &a, int &b);
int BubbleSort(int* _array, int ArraySize);
int MergeSort(int* ArrayInput1, int* ArrayInput2, int* ArrayOutput, int ArrayInput1Size, int ArrayInput2Size, int ArrayOutputSize);
int GetArraySize(int* _array);
void output(string _string, int* _array, int ArraySize);

int main(void)
{
    int arrayA[]= {10,8,6,4,2};
    int arrayB[]= {9,7,5,3,1};
    int ArraySizeA=sizeof(arrayA)/sizeof(arrayA[0]);
    int ArraySizeB=sizeof(arrayB)/sizeof(arrayB[0]);
    int arrayC[ArraySizeA+ArraySizeB];
    arrayC[ArraySizeA+ArraySizeB]={};

    cout << "Before sort" << endl;
    output("array1 elements: ", arrayA, ArraySizeA);
    output("array2 elements: ", arrayB, ArraySizeB);

    BubbleSort(arrayA, ArraySizeA);
    BubbleSort(arrayB, ArraySizeB);
    cout << "After sort" << endl;
    output("array1 elements: ", arrayA, ArraySizeA);
    output("array2 elements: ", arrayB, ArraySizeB);

    MergeSort(arrayA, arrayB, arrayC, ArraySizeA, ArraySizeB, ArraySizeA+ArraySizeB);
    cout << "After merging two arrays" << endl;
    output("array3 elements: ", arrayC, ArraySizeA+ArraySizeB);

    return 0;
}

void exchange(int &a, int &b)
{
    a=a+b;
    b=a-b;
    a=a-b;
}

int BubbleSort(int* _array, int ArraySize)
{
    for (int i=1; i<ArraySize; i=i+1)
        for (int j=0; i+j<ArraySize; j=j+1)
            if (_array[j]>_array[j+1])
                exchange(_array[j],_array[j+1]);
}

int MergeSort(int* ArrayInput1, int* ArrayInput2, int* ArrayOutput, int ArrayInput1Size, int ArrayInput2Size, int ArrayOutputSize)
{
    for (int i=0; i<ArrayOutputSize; i=i+1)
    {
        static int j=0;
        static int k=0;
        if ((j<ArrayInput1Size && k<ArrayInput2Size && ArrayInput1[j]>=ArrayInput2[k]) || j==ArrayInput1Size)
        {
            ArrayOutput[i]=ArrayInput2[k];
            k=k+1;
        }
        else if ((j<ArrayInput1Size && k<ArrayInput2Size && ArrayInput1[j]<ArrayInput2[k]) || k==ArrayInput2Size)
        {
            ArrayOutput[i]=ArrayInput1[j];
            j=j+1;
        }
    }
}

int GetArraySize(int* _array)
{
    return sizeof(_array);
}

void output(string _string, int* _array, int ArraySize)
{
    cout << _string;
    for (int i=0; i<ArraySize; i=i+1)
        cout << _array[i] << " ";
    cout << endl;
}
