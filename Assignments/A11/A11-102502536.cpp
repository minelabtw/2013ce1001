#include<iostream>

using namespace std;

int bubblesort(int* arr , int s);  //宣告函式
int Merge(int *arr1,int *arr2,int *arr3,int s);

int main()
{
    int *arr1;  //宣告指標
    int *arr2;
    int *arr3;

    int arrayA[5]={10,8,6,4,2};  //宣告陣列
    int arrayB[5]={9,7,5,3,1};
    int arrayC[10]={0};

    arr1=arrayA;  //用指標存陣列的值
    arr2=arrayB;
    arr3=arrayC;

    cout << "Before sort\narray1 elements: 10 8 6 4 2\narray2 elements: 9 7 5 3 1\n";  //輸出元素

    bubblesort(arrayA , 5);  //呼叫函式 重新排列陣列值
    bubblesort(arrayB , 5);

    cout << "After sort\narray1 elements: ";  //輸出重新排列後的值
    for(int i=0;i<5;i++)
    {
        cout << arrayA[i] << " ";
    }
    cout << endl << "array2 elements: ";
    for(int i=0;i<5;i++)
    {
        cout << arrayB[i] << " ";
    }

    Merge(arr1 , arr2 , arr3 , 10);  //呼叫函數 融合並重新排列兩陣列元素 傳回arr3=arrayC

    cout << endl << "After merging two arrays\narray3 elements: ";  //輸出arrayC元素
    for(int i=0;i<10;i++)
    {
        cout << arrayC[i] << " ";
    }

    return 0;
}

int bubblesort(int* arr , int s)  //重排元素
{
    int a=0;

    for(int i=s-1;i>0;i--)
    {
        for(int j=0;j<i;j++)
        {
            if(arr[j]>arr[j+1])
            {
                a=arr[j+1];
                arr[j+1]=arr[j];
                arr[j]=a;
            }

        }
    }

    return arr[s];
}

int Merge(int *arr1,int *arr2,int *arr3,int s)  //融合並重新排列兩陣列元素
{
    int i=0;
    int j=0;
    int f=0;

    for(i<5;j<5;f<s)
    {
        while(*(arr1+i)<*(arr2+j))
        {
            *(arr3+f)=*(arr1+i);
            i++;
            f++;
        }
        while(*(arr1+i)>*(arr2+j))
        {
            *(arr3+f)=*(arr2+j);
            j++;
            f++;
        }
    }

    if(*(arr1+4)<*(arr2+4))
    {
        *(arr3+9)=*(arr2+4);
    }
    if(*(arr2+4)<*(arr1+4))
    {
        *(arr3+9)=*(arr1+4);
    }

    return *arr3;

}
