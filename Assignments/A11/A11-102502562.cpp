#include<iostream>
using namespace std;

void bubblesort(int *arr ,int Size)//氣泡排序法排列輸入的數字
{
    int change=Size-1;
    while(change)
    {
        int a=change;
        change=0;
        for(int i=0;i<a;i++)
        {
            if(*(arr+i)>*(arr+(i+1)))
            {
                int b=*(arr+i);
                *(arr+i)=*(arr+(i+1));
                *(arr+(i+1))=b;
                change=i;
            }
        }
    }

}
void Merge(int *arr1,int *arr2,int *arr3,int Size)//把兩個陣列組合在一起並由小到大排好
{
    for(int i=0,j=0,k=0;k<Size;k++)
    {
        if(*(arr1+i)<*(arr2+j))
        {
            *(arr3+k)=*(arr1+i);
            i++;
            if(i==5)
            {
                *(arr3+(k+1))=*(arr2+j);
                break;
            }
        }
        else if(*(arr2+j)<*(arr1+i))
        {
            *(arr3+k)=*(arr2+j);
            j++;
            if(j==5)
            {
                *(arr3+(k+1))=*(arr1+i);
                break;
            }
        }
    }
}
int main()
{
    int *arrA,*arrB,*arrC;//宣告指標*arrA,*arrB,*arrC
    int arrayA[5]={10,8,6,4,2};//宣告陣列arrayA[5]並初始化其值為{10,8,6,4,2}
    int arrayB[5]={9,7,5,3,1};//宣告陣列arrayB[5]並初始化其值為{9,7,5,3,1}
    int arrayC[10]={0};//宣告陣列arrayC[10]並初始化其值為{0}

    arrA=arrayA;
    arrB=arrayB;
    arrC=arrayC;
    cout << "Before sort" << endl;
    cout << "array1 elements:";
    for(int i=0;i<5;i++)//輸出arrA陣列的數字
    {
        cout << " " << *(arrA+i);
    }
    cout << endl;
    cout << "array2 elements:";
    for(int i=0; i<5; i++)//輸出arrB陣列的數字
    {
        cout << " " << *(arrB+i);
    }
    cout << endl;
    bubblesort(arrA,5);//用bubblesort函式排列arrA
    bubblesort(arrB,5);//用bubblesort函式排列arrB
    cout << "After sort" << endl;
    cout << "array1 elements:";
    for(int i=0;i<5;i++)//輸出排列後arrA陣列的數字
    {
        cout << " " << *(arrA+i);
    }
    cout << endl;
    cout << "array2 elements:";
    for(int i=0;i<5;i++)//輸出排列後arrB陣列的數字
    {
        cout << " " << *(arrB+i);
    }
    cout << endl;
    cout << "After merging two arrays" << endl;
    Merge(arrA,arrB,arrC,10);//利用Merge函式排列arrA和arrB並傳入arrC
    cout << "array3 elements:";
    for(int i=0;i<10;i++)//輸出排列後arrC陣列的數字
    {
        cout << " " << *(arrC+i);
    }

    return 0;
}
