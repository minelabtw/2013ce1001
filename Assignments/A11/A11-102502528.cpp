#include<iostream>
using namespace std;

void bubblesort(int* arr , int _size)  //泡沫排序
{
    int storearr[_size];
    for(int i=_size-1; i>0; i--)
    {
        for(int j=0; j<i; j++)
        {
            if(arr[j]>arr[j+1])
            {
                storearr[j] = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = storearr[j];
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int _size)   //把兩個陣列中較小的值傳入陣列C
{
    int x=0,y=0;
    for(int i=0; i<_size; i++)
    {
        if(arr1[x]<arr2[y] || y>4)
            arr3[i]=arr1[x++];
        else
            arr3[i]=arr2[y++];
    }
}
void showarr(int *arr1,int *arr2,int _size)   //輸出陣列(單純懶)
{
    cout << "array1 elements:";
    for(int i=0; i<_size; i++)
        cout <<" "<<arr1[i];
    cout <<endl<< "array2 elements:";
    for(int j=0; j<_size; j++)
        cout <<" "<<arr2[j];
    cout << endl;
}

int main(void)
{
    int *arr;
    int arrayA[5]= {10,8,6,4,2},arrayB[5]= {9,7,5,3,1},arrayC[10];
    cout << "Before sort" << endl;
    showarr(arrayA,arrayB,5);
    cout << "After sort" << endl;
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);
    showarr(arrayA,arrayB,5);
    cout << "After merging two arrays"<<endl;
    Merge(arrayA,arrayB,arrayC,10);
    arr=arrayC;                       //題目要求用指標，說實話我真的不知道該用這東西指誰www
    cout << "array3 elements:";
    for(int i=0; i<10; i++)
        cout << " " << arr[i];
}
