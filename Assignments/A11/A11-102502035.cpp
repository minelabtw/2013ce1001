#include <iostream>
using namespace std;
void bubblesort(int *arrayd,int sized);
void Merge(int *arr1,int *arr2,int *arr3,int sizee);
int main ()
{
    const int sizea =5;//宣告變數等於陣列長度
    const int sizec =10;
    int arraya[sizea] = {10,8,6,4,2};//宣告變數依題目要求
    int arrayb[sizea] = {9,7,5,3,1};
    int arrayc[sizec] = {};//宣告變數並=0
    int *arr1 =0;
    int *arr2 =0;
    int *arr3 =0;
    arr1 =arraya;//指定指標
    arr2 =arrayb;
    arr3 =arrayc;
    cout << "Before sort" << endl;//輸出題目要求
    cout << "array1 elements:";
    for (int i=0; i<sizea; i++)
    {
        cout << " " << *(arr1+i);//輸出原始陣列
    }
    cout << endl << "array2 elements:";//輸出題目要求
    for (int i=0; i<sizea; i++)
    {
        cout << " " << *(arr2+i);//輸出原始陣列
    }
    bubblesort (arr1,sizea);//轉換排列
    bubblesort (arr2,sizea);
    cout << endl << "After sort" << endl;//輸出題目要求
    cout << "array1 elements:";
    for (int i=0; i<sizea; i++)
    {
        cout << " " << *(arr1+i);//輸出轉換後
    }
    cout << endl << "array2 elements:";//輸出題目要求
    for (int i=0; i<sizea; i++)
    {
        cout << " " << *(arr2+i);//輸出轉換後
    }
    cout << endl << "After merging two arrays" << endl;//輸出題目要求
    Merge (arr1,arr2,arr3,sizec);
    cout << "array3 elements:";//輸出題目要求
    for (int i=0; i<sizec; i++)
    {
        cout << " " << *(arr3+i);//輸出結合後
    }
    return 0;
}
void bubblesort(int *arrayd , int sized)
{
    int temp =0;//宣告變數並=0
    for (int i =sized; i >0; i--)
    {
        for (int j =0; j <i-1; j++)
        {
            if (arrayd[j] >arrayd[j+1])
            {
                temp =arrayd[j];//先轉存
                arrayd[j] =arrayd[j+1];//換過去
                arrayd[j+1] =temp;//存過來
                temp =0;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int sizee)
{
    int k=0;//宣告變數並=0
    int i=0;
    int j=0;
    for (k =0; i <(sizee /2) &&j <(sizee /2); k++)//存小的之後換下一個比，直到有一陣列比完
    {
        if (arr1[i] >arr2[j])
        {
            arr3[k] =arr2[j];
            j++;
        }
        else
        {
            arr3[k] =arr1[i];
            i++;
        }
    }
    if (k<sizee)//有一陣列沒用完，全部存剩餘的陣列
    {
        for (; i <(sizee /2) &&j >=(sizee /2); i++)
        {
            arr3[k] =arr1[i];
        }
        for (; i >=(sizee /2) &&j <(sizee /2); j++)
        {
            arr3[k] =arr2[j];
        }
    }
}
