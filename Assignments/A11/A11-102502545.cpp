#include<iostream>
#include<stdlib.h>
using namespace std;

int bubblesort(int *Array , int Size);//宣告bubblesort函式
int Merge(int *arr1,int *arr2,int *arr3,int Size);//宣告Merge函式
int main()
{
    int *apple,*tomato,*potato,Size=5;//宣告變數
    int  bananaA[5]= {10,8,6,4,2},bananaB[5]= {9,7,5,3,1},watermelon[10];//宣告三陣列
    apple=bananaA;//指標函數
    tomato=bananaB;
    potato=watermelon;
    cout<<"Before sort "<<endl;
    cout<<"array1 elements: ";

    for(int i=0; i<5; i++)//設置迴圈使得陣列可輸出
    {
        cout<<*(apple+i)<<" ";
    }

    cout<<endl<<"array2 elements: ";

    for(int i=0; i<5; i++)//設置迴圈使得陣列可輸出
    {
        cout<<*(tomato+i)<<" ";
    }

    cout<<endl<<"After sort"<<endl;
    bubblesort(apple,Size);
    cout<<"array1 elements: ";

    for(int i=0; i<5; i++)//設置迴圈使得陣列可輸出
    {
        cout<<*(apple+i)<<" ";
    }
    bubblesort(tomato,Size);
    cout<<endl<<"array2 elements: ";

    for(int i=0; i<5; i++)//設置迴圈使得陣列可輸出
    {
        cout<<*(tomato+i)<<" ";
    }
    cout<<endl<<"After merging two arrays"<<endl<<"array3 elements: ";
    Merge(apple,tomato,potato,10);
    for(int i=0; i<10; i++)
    {
        cout<<*(potato+i)<<" ";
    }

    return 0;
}
int bubblesort(int *Array , int Size)//宣告bubblesort函式
{
    for(int i=0; i<Size; i++)
    {
        for(int j=0; j<Size-1; j++)
        {
            if(*(Array+j)>*(Array+j+1))
            {
                int x=*(Array+j);
                *(Array+j)=*(Array+j+1);
                *(Array+j+1)=x;
            }
        }
    }
    return *Array;
}
int Merge(int *arr1,int *arr2,int *arr3,int Size)//宣告Merge函式
{
    int x=0,y=0;
    for(int i=0; i<Size; i++)
    {

        if(i!=Size-1)
        {
            if(*(arr1+x)>*(arr2+y))
            {
                *(arr3+i)=*(arr2+y);
                if(y<4)
                    y++;
            }
            else if(*(arr1+x)<*(arr2+y))
            {
                *(arr3+i)=*(arr1+x);
                if(x<4)
                    x++;
            }
        }
        else
        {
            if(*(arr1+x)>*(arr2+y))
                *(arr3+i)=*(arr1+x);
            else if(*(arr1+x)<*(arr2+y))
                *(arr3+i)=*(arr2+y);



        }
    }
}







