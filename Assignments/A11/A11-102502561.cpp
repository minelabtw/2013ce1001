#include<iostream>
using namespace std;
void bubblesort(int *_array, int _size);
void Merge(int *arrayA,int *arrayB,int *arrayC,int _size);

int main()
{

    int arrayA[5] = {10,8,6,4,2};   //3 arrays
    int arrayB[5] = {9,7,5,3,1};
    int arrayC[10]= {0};

    cout << "Before sort" ;         //before sort arrayA and arrayB
    cout << endl << "array1 elements: ";
    for(int i=0; i<5; i++) //output array after sorting
    {
        cout << arrayA[i] << " ";
    }
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++) //output array after sorting
    {
        cout << arrayB[i] << " ";
    }
    cout << endl;
    //sort-ing
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);
    cout << "After sort";           //after sort arrayA and arrayB
    cout << endl << "array1 elements: ";
    for(int i=0; i<5; i++) //output array after sorting
    {
        cout << arrayA[i] << " ";
    }
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++) //output array after sorting
    {
        cout << arrayB[i] << " ";
    }
    cout << endl;

    Merge(arrayA,arrayB,arrayC,10); //now sort both arrays together
    cout << "After merging two arrays" << endl << "array3 elements: ";
    for(int i=0; i<10; i++)
    {
        cout << arrayC[i] << " ";
    }
    return 0 ;  //end
}

void bubblesort(int *_array , int _size)
{
    for(int j =0; j<_size; j++)
    {
        for(int i =0; i<_size-1; i++)
        {
            if(_array[i]>_array[i+1])   //exchange if [i] is bigger than [i+1]
            {
                _array[i]^=_array[i+1];
                _array[i+1]^=_array[i];
                _array[i]^=_array[i+1];
                i = 0;
            }
        }
    }
}

void Merge(int *arrayA,int *arrayB,int *arrayC,int _size)
{
    int j=0;
    int i=0;
    int k=0;

    if(arrayA[k] > arrayB[i])       //find the smallest one
    {
        arrayC[j] = arrayB[i];
    }
    for(int j=1; j<10; j++)     //arrayC[0] is already used
    {

        if(arrayA[k] > arrayB[i])
        {
            arrayC[j] = arrayA[k];
            i++;
        }
        else
        {
            arrayC[j] = arrayB[i];
            k++;
        }
    }
}

