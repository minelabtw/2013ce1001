#include <iostream>
using namespace std;

void bubblesort(int* _array , int _size ) ;
void Merge(int *arr1,int *arr2,int *arr3,int _size) ;

int main()
{
    const int  _size = 5 ;
    int arrayA[ _size] = {10,8,6,4,2} ; //宣告陣列
    int arrayB[ _size] = {9,7,5,3,1} ;
    int arrayC[10] = {} ;

    cout << "Before sort" << endl ;
    cout << "array1 elements: " ;

    for(int x = 0 ; x < _size ; x++ )
        cout << arrayA[x] << " " ;

    cout << endl << "array2 elements: " ;

    for(int x = 0 ; x < _size ; x++ )
        cout << arrayB[x] << " " ;

    cout << endl << "After sort" << endl ; //輸出結果
    cout << "array1 elements: " ;
    bubblesort( arrayA , _size ) ;
    cout << endl << "array2 elements: " ;
    bubblesort( arrayB , _size ) ;
    cout << endl << "After merging two arrays" << endl ;
    cout << "array3 elements: " ;
    Merge(arrayA , arrayB , arrayC , _size) ;

    return 0 ;
}

void bubblesort(int* _array , int _size ) //由小到大的排列函式
{
    int hello = 0 , x = 0 ;

    for(int y = 0 ; y < _size ; y++ )
    {
        for(x = 0 ; x < _size-1 ; x++ )
        {
            if( _array[x] > _array[x+1] )
            {
                hello = _array[x+1] ;
                _array[x+1] = _array[x] ;
                _array[x] = hello ;
            }
        }
        x=0 ;
    }

    for(int x = 0 ; x < _size ; x++ )
        cout << *(_array+x) << " " ;
}

void Merge(int *arr1,int *arr2,int *arr3,int _size) //將兩陣列排進新陣列的函式
{
    int x = 0 , y = 0 ;
    for (int z = 0 ; z < 10 ; z++ )
    {
        if ( x == 4 && y == 5 ) //讓10能夠存進陣列
        {
            arr3[z] = arr1[x] ;
            break ;
        }
        else if (arr1[x] > arr2[y] )
        {
            arr3[z] = arr2[y] ;
            y++ ;
        }
        else if (arr1[x] < arr2[y] )
        {
            arr3[z] = arr1[x] ;
            x++ ;
        }
    }
    for (int z = 0 ; z < 10 ; z++ )
        cout << *(arr3+z) << " " ;
}
