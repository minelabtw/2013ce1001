#include<iostream>
#define swap(a,b){int t=a;a=b;b=t;}//ab交換
using namespace std;
void bubblesort(int* array , int size);//排序
void Merge(int *arr1,int *arr2,int *arr3,int size);//合併
int main()
{
    int arrayA[5]= {10,8,6,4,2},arrayB[5]= {9,7,5,3,1},arrayC[10];
    cout<<"Before sort\narray1 elements: ";
    for(int i=0; i<5; i++)
        cout<<*(arrayA+i)<<" ";
    cout<<"\narray2 elements: ";
    for(int i=0; i<5; i++)
        cout<<*(arrayB+i)<<" ";
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);
    cout<<"\nAfter sort\narray1 elements: ";
    for(int i=0; i<5; i++)//顯示陣列
        cout<<*(arrayA+i)<<" ";
    cout<<"\narray2 elements: ";
    for(int i=0; i<5; i++)//顯示陣列
        cout<<*(arrayB+i)<<" ";
    Merge(arrayA,arrayB,arrayC,10);
    cout<<"\nAfter merging two arrays\narray3 elements: ";
    for(int i=0; i<10; i++)//顯示陣列
        cout<<*(arrayC+i)<<" ";
    return 0;
}
void bubblesort(int* a , int _size)//作排列
{
    /*for(int j=0; j<_size; j++) ***選擇排序***
    {
        int min=a[j],mink=j;
        for(int k=j; k<_size; k++)
        {
            if(a[k]<min)
            {
                min=a[k];
                mink=k;
            }
        }
        swap(a[j],a[mink]);
    }*/
    for(; _size>0; _size--)
        for(int k=0; k<_size-1; k++)
            if(*(a+k)>*(a+k+1))
                swap(*(a+k),*(a+k+1));
}
void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int i=0,j=0,k=0;
    while(k<size)
    {
        if(i<5&&(j==5||*(arr1+i)<*(arr2+j)))
        {
            *(arr3+k)=*(arr1+i);
            i++;
        }
        else
        {
            *(arr3+k)=*(arr2+j);
            j++;
        }
        k++;
    }
}
