#include<iostream>
using namespace std;
int arrayA[5]= {10,8,6,4,2};//宣告陣列
int arrayB[5]= {9,7,5,3,1};
int arrayC[10]= {};
int bubblesort(int*arr,int Size);//function prototype
int Merge(int *arr1,int *arr2,int *arr3,int Size);
int main()
{
    cout<<"Before sort"<<endl<<"array1 elements: ";
    for(int i=0; i<=4; i++)
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl<<"array2 elements: ";
    for(int i=0; i<=4; i++)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl<<"After sort"<<endl<<"array1 elements: ";
    bubblesort(arrayA,5);//回傳排好的陣列
    for(int i=0; i<=4; i++)
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl<<"array2 elements: ";
    bubblesort(arrayB,5);//回傳排好的陣列
    for(int i=0; i<=4; i++)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl<<"After merging two arrays"<<endl<<"array3 elements: ";
    Merge(arrayA,arrayB,arrayC,10);//回傳排好的陣列
    for(int i=0; i<=9; i++)
    {
        cout<<arrayC[i]<<" ";
    }
    cout<<endl;
    return 0;
}
int bubblesort(int*arr,int Size)//function definition
{
    for(int i=0; i<Size-1; i++)
    {
        for(int j=0; j<Size-1; j++)
        {
            if(arr[j]>arr[j+1])
            {
                swap(arr[j],arr[j+1]);
            }
        }
    }
}
int Merge(int *arr1,int *arr2,int *arr3,int Size)//function definition
{
    int n1=0;
    int n2=0;
    for(int i=0; i<=Size-1; i++)
    {
        if(arr1[n1]>arr2[n2])
        {
            arr3[i]=arr2[n2];
            n2++;
        }
        else if(arr1[n1]<arr2[n2])
        {
            arr3[i]=arr1[n1];
            n1++;
        }
        if(i==9)
        {
            arr3[9]=arr1[4];
        }
    }
}
