#include <iostream>
using namespace std;

void bubblesort(int *, int); //bubble sort increasing
void exchange(int *, int *); //exchange two numbers' values
void Merge(int *, int *, int *, int);   //sort and merge 2 arries into 1 array

int main()
{
    //decalaration and initialization
    int arrayA[5] = {10,8,6,4,2};
    int arrayB[5] = {9,7,5,3,1};
    int arrayC[10] = {};
    int *arrayAPtr = arrayA;
    int *arrayBPtr = arrayB;
    int *arrayCPtr = arrayC;

    //show the values before sorting
    cout << "Before sort" << endl
         << "array1 elements:";
    for ( int i = 0; i < 5; i ++)
        cout << ' ' << *(arrayAPtr + i);
    cout << endl
         << "array2 elements:";
    for ( int i = 0; i < 5; i ++)
        cout << ' ' << *(arrayBPtr + i);
    cout << endl;

    //sort arrayAPtr; sort arrayBPtr;
    bubblesort(arrayAPtr, 5);
    bubblesort(arrayBPtr, 5);

    //show the values after sorting
    cout << "After sort" << endl
         << "array1 elements:";
    for ( int i = 0; i < 5; i ++)
        cout << ' ' << *(arrayAPtr + i);
    cout << endl
         << "array2 elements:";
    for ( int i = 0; i < 5; i ++)
        cout << ' ' << *(arrayBPtr + i);
    cout << endl;

    Merge(arrayAPtr, arrayBPtr, arrayCPtr, 10);

    //show the values of arrayC
    cout << "After merging two arrays" << endl
         << "array3 elements:";
    for ( int i = 0; i < 10; i ++)
        cout << ' ' << *(arrayCPtr + i);

    return 0;
}

void bubblesort(int *arr, int arr_size) //bubble sort
{
    //sortint
    for (int i = 0; i < arr_size - 1; i ++)
        for (int j = 0; j < arr_size - 1; j ++)
            if (*(arr + j) > *(arr + j + 1))
                exchange((arr + j), (arr + j + 1));

    //check
    for (int i = 0; i < arr_size - 1; i ++)
        if (*(arr + i) > *(arr + i + 1))
            cerr << "error while bubble sorting!" << endl;
}

void exchange(int *no_1, int *no_2) //exchange two numbers' values
{
    int temp = 0; //temporary
    temp = *no_1;
    *no_1 = *no_2;
    *no_2 = temp;
}

void Merge(int *arr1, int *arr2, int *arr3, int arr3size)   //sort and merge 2 arries into 1 array
{
    int ctr3 = -1;      //counter of array3

    //merge arr1 and arr2
    for (int ctr1 = 0; ctr1 < 5; ctr1 ++)
        for (int ctr2 = ctr1; ctr2 < 5; ctr2 ++)
        {
            ctr3 ++;
            if (*(arr1 + ctr1) > *(arr2 + ctr2))
            {
                *(arr3 + ctr3) = *(arr2 + ctr2);
                if (ctr3 == arr3size -2)
                    *(arr3 + ctr3 + 1) = *(arr1 + ctr1);
            }
            else
            {
                *(arr3 + ctr3) = *(arr1 + ctr1);
                if (ctr3 == arr3size -2)
                    *(arr3 + ctr3 + 1) = *(arr2 + ctr2);
                break;
            }
        }

    //check
    for (int i = 0; i < arr3size - 1; i ++)
        if (*(arr3 + i) > *(arr3 + i + 1))
            cerr << "error while merging!" << endl;

}
