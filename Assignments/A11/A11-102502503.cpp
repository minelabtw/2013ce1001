#include <iostream>
using namespace std;
void bubblesort(int* array , int size);  //宣告bubblesort函式
void Merge(int *arr1,int *arr2,int *arr3,int size);  //宣告Merge函式

int main()
{
    int *arrA;  //宣告指標A
    int arrayA[5]= {10,8,6,4,2};
    arrA=arrayA;  //將指標A指向陣列A
    int *arrB;
    int arrayB[5]= {9,7,5,3,1};
    arrB=arrayB;
    int *arrC;
    int arrayC[10];
    arrC=arrayC;

    cout << "Before sort" << endl;
    cout << "array1 elements:";
    for (int i=0; i<5; i++)  //輸出陣列A內容
    {
        cout << " " << *(arrA+i);
    }
    cout << endl << "array2 elements:";  //輸出陣列B內容
    for (int i=0; i<5; i++)
    {
        cout << " " << *(arrB+i);
    }

    cout << endl << "After sort" << endl;
    bubblesort(arrB,5);
    bubblesort(arrA,5);  //呼叫函式
    cout << "array1 elements:";  //重新輸出陣列A內容
    for (int i=0; i<5; i++)
    {
        cout << " " << *(arrA+i);
    }

    cout << endl << "array2 elements:";  //重新輸出陣列B內容
    for (int i=0; i<5; i++)
    {
        cout << " " << *(arrB+i);
    }

    Merge(arrA,arrB,arrC,10);  //呼叫函式


    return 0;
}

void bubblesort(int* array , int size)
{
    int a,b;
    for (int i=0; i<size-1; i++)  //將陣列裡的值排列大小
    {
        for (int i=0; i<size; i++)
        {
            if (*(array+i)>*(array+i+1))
            {
                a=*(array+i);
                b=*(array+i+1);
                *(array+i)=b;
                *(array+i+1)=a;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int c=0;
    for (int i=0; i<5; i++)  //將兩陣列的值比較大小並存入陣列C
    {
        if (*(arr1+i)>*(arr2+i))
        {
            *(arr3+2*c)=*(arr2+i);
            *(arr3+2*c+1)=*(arr1+i);
            c++;
        }
        else
        {
            *(arr3+2*c)=*(arr1+i);
            *(arr3+2*c+1)=*(arr2+i);
            c++;
        }
    }
    cout <<  endl << "After merging two arrays" << endl;
    cout << "array3 elements:";  //輸出陣列C
    for (int c=0; c<size; c++)
    {
        cout << " " << *(arr3+c);
    }
}
