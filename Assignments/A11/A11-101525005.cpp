#include <iostream>

using namespace std;

void bubblesort(int* array , int size);

int main()
{
    int arrayA[5]= {10,8,6,4,2},arrayB[5]= {9,7,5,3,1},arrayC[10];
    int* arr;
    cout << "Before sort" << endl;


    cout<< "array1 elements: "<<arrayA[0]<<" "<<arrayA[1]<<" "<<arrayA[2]<<" "<<arrayA[3]<<" "<<arrayA[4]<<" " <<endl;
    cout<< "array2 elements: "<<arrayB[0]<<" "<<arrayB[1]<<" "<<arrayB[2]<<" "<<arrayB[3]<<" "<<arrayB[4]<<" " <<endl;

    // cout<<"point "<<i<<":"<<*(arr+i)<<"  array "<<i<<":"<<array[i]<<endl;

//排序A
    cout << "After sort" << endl;
    arr=arrayA;
    bubblesort(arr,5);
    cout<< "array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<< arr[i]<<" ";
    }
    cout<<endl;
    //排序B
    arr=arrayB;
    bubblesort(arr,5);
    cout<< "array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<< arr[i]<<" ";
    }
    cout<<endl;

    //Merge
    cout<<"After merging two arrays"<<endl;
    cout<<"array3 elements: ";
    int j=0,k=0;
    for(int i=0; i<10; i++)
    {
        if(j>4 )
        {
            arrayC[i]=arrayB[k];
            k++;
        }
        else if(k>4)
        {
            arrayC[i]=arrayA[j];
            j++;
        }
        else if(arrayA[j]<arrayB[k])
        {
            arrayC[i]=arrayA[j];
            j++;
        }
        else
        {
            arrayC[i]=arrayB[k];
            k++;
        }


    }


for(int i=0; i<10; i++ )
{
    cout<< arrayC[i]<<" ";
}

return 0;
}
void bubblesort(int* array , int size)
{
    int temp,change;
    //取第一個到最後一個數
    //取出來的數依序與下一個做比較，若較大則互換，一值做到此陣列結束為止
    //3 2 1  2 3 1  2 1 3   2 4 1 3 = 2 1 4 3= 2 1 3 4
    //2 1 3  1 2 3                    1 2 3 4
    //執行次數共n次   1 3 2   1 3 2  1 2 3
    for(int i=0; i< size-1; i++)
    {
        for(int j=0; j<size-1; j++)
        {
            if(array[j]>array[j+1])
            {
                temp=array[j+1];
                array[j+1]=array[j];
                array[j]=temp;
            }
        }
    }
}
