#include <iostream>
using namespace std;

void bubblesort(int* array , int size);                 //宣告函式
void Merge(int *arr1,int *arr2,int *arr3,int size);     //宣告函式

int main()
{
    int size=5;                                        //宣告陣列大小為5
    int arrayA[5]= {10,8,6,4,2};                       //宣告arrayA之值
    int arrayB[5]= {9,7,5,3,1};                        //宣告arrayB之值
    int arrayC[10];                                    //宣告arrayC
    int *arr;
    int *arr3;

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)                             //顯示排列前的arrayA
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl;

    cout<<"array2 elements: ";
    for(int i=0; i<5; i++)                             //顯示排列前的arrayB
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl;

    arr=arrayA;

    bubblesort(arr,size);
    cout<<"After sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)                             //顯示排列後的arrayA
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl;

    arr=arrayB;

    bubblesort(arr,size);
    cout<<"array2 elements: ";
    for(int i=0; i<5; i++)                             //顯示排列後的arrayB
    {
        cout<<*(arr+i)<<" ";
    }

    size=10;
    arr3=arrayC;
    Merge(arrayA,arrayB,arrayC,size);

    cout<<endl;
    cout<<"After merging two arrays"<<endl;          //顯示arr3
    cout<<"array3 elements: ";
    for(int k=0; k<10; k++)
    {
        cout<<*(arr3+k)<<" ";
    }
    return 0;
}

void bubblesort(int *arr , int size)
{
    int k=0;
    for(int i=0; i<4; i++)
    {
        for(int i=0; i<4; i++)                      //排列arr之值,若arr[i+1]小於arr[i]即覆蓋,若無則維持原狀
        {
            k=arr[i];
            if(arr[i]<=arr[i+1]);
            else if(arr[i]>arr[i+1])
            {
                arr[i]=arr[i+1];
                arr[i+1]=k;
            }
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size) //arr1和arr2兩兩比較
{
    int i=0;
    int j=0;
    int k=0;

    for(i<5; j<5;)
    {
        while(arr1[i]<arr2[j])                     //將較小的陣列值存取,較大的和被存取陣列的下一項比較
        {
            arr3[k]=arr1[i];
            i=i+1;
            k=k+1;
        }

        while(arr1[i]>arr2[j])
        {
            arr3[k]=arr2[j];
            j=j+1;
            k=k+1;
        }

    }
    if(arr1[4]<arr2[4])                          //因迴圈無法存取arr3[9],所以另外存
    {
        arr3[9]=arr2[4];
    }
    if(arr2[4]<arr1[4])
    {
        arr3[9]=arr1[4];
    }
}
