#include <iostream>
using namespace std;
void bubblesort(int *x,int y=5); //傳入陣列與陣列大小
void Merge(int *arr1,int *arr2,int *arr3,int x=10);  //傳入三個陣列與陣列3大小
int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];

    cout << "Before sort" << endl << "array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout << arrayA[i] << " "; //輸出
    }
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout << arrayB[i] << " "; //輸出
    }

    bubblesort(arrayA);
    bubblesort(arrayB);

    cout << endl << "After sort" << endl << "array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout << arrayA[i] << " "; //輸出
    }
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout << arrayB[i] << " "; //輸出
    }

    Merge(arrayA,arrayB,arrayC);
    cout << endl << "After merging two arrays" << endl << "array3 elements: ";
    for(int i=0; i<10; i++)
    {
        cout << arrayC[i] << " "; //輸出
    }
    return 0;
}

void bubblesort(int *x , int y) //由小排到大
{
    for (int i=y-1; i>0; i--)
    {
        for (int j=0; j<i; j++)
        {
            if (x[j]>x[j+1])
                swap(x[j],x[j+1]);
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int x) //合併
{
    int a=0;
    int b=0;
    for(int i=0; i<x; i++)
    {
        if(a==5)
        {
            *(arr3+i)=*(arr2+b);
            b++;
        }
        else if(b==5)
        {
            *(arr3+i)=*(arr1+a);
            a++;
        }
        else if(*(arr1+a)>=*(arr2+b))
        {
            *(arr3+i)=*(arr2+b);
            b++;
        }
        else if(*(arr1+a)<*(arr2+b))
        {
            *(arr3+i)=*(arr1+a);
            a++;
        }
    }
}
