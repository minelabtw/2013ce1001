#include <iostream>
using namespace std;
void bubblesort(int *array,int _size);//宣告函式
void Merge(int *arr1,int *arr2,int *arr3,int _size);
void printarray(int *array,int _size);
int main()
{   int _size=5;//宣告變數
    int *arr1;
    int *arr2;
    int *arr3;
    int array1[5]={10,8,6,4,2};
    int array2[5]={9,7,5,3,1};
    int array3[10];
    arr1=array1;
    arr2=array2;
    arr3=array3;
    cout<<"Before sort"<<endl;
    cout<<"array1 elements:";
    printarray(arr1,_size);//執行函式
    cout<<endl<<"array2 elements:";
    printarray(arr2,_size);
    bubblesort(arr1,_size);
    bubblesort(arr2,_size);
    Merge(arr1,arr2,arr3,_size);
    cout<<endl<<"After sort"<<endl;
    cout<<"array1 elements:";
    printarray(arr1,_size);
    cout<<endl<<"array2 elements:";
    printarray(arr2,_size);
    cout<<endl<<"After merging two arrays"<<endl;
    cout<<"array3 elements:";
    printarray(arr3,_size+5);


}
void bubblesort(int *array,int _size){//用函式排列陣列
    for(int a=0;a<_size;a++){
        for(int a=0;a<_size-1;a++){
            if(*(array+a)>*(array+a+1)){
               int x=*(array+a);
               int y=*(array+a+1);
               *(array+a+1)=x;
               *(array+a)=y;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int _size){//用函式合併陣列
    for(int a=0;a<_size;a++){
        if(arr1[a]>arr2[a]){
            arr3[a*2]=arr2[a];
            arr3[a*2+1]=arr1[a];
        }
        else if(arr1[a]<arr2[a]){
            arr3[a*2]=arr2[a];
            arr3[a*2+1]=arr1[a];
        }

    }
}
void printarray(int *array,int _size){//用函式輸出陣列
    for(int a=0;a<_size;a++){
        cout<<" "<<*(array+a);
    }

}
