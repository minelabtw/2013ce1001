#include <iostream>

using namespace std;

int bubblesort(int*,int);
int Merge(int*,int*,int*,int);
void output(int*,int);

int main()
{
    const int A = 5; //宣告陣列大小
    int arrayA[A] = {10,8,6,4,2}; // 宣告陣列
    const int B = 5; //宣告陣列大小
    int arrayB[B] = {9,7,5,3,1}; // 宣告陣列
    const int C = 10; //宣告陣列大小
    int arrayC[C] = {}; // 宣告陣列

    cout << "Before sort" << endl;
    cout << "arrayA elements: ";
    output(arrayA,A); //輸出排序前數字
    cout << "arrayB elements: ";
    output(arrayB,B); //輸出排序前數字

    bubblesort(arrayA,A);
    bubblesort(arrayB,B);

    cout << "After sort" << endl;
    cout << "arrayA elements: ";
    output(arrayA,A); //輸出排序後數字
    cout << "arrayB elements: ";
    output(arrayB,B); //輸出排序後數字

    Merge(arrayA,arrayB,arrayC,C);
    bubblesort(arrayC,C);
    cout << "After merging two arrays" << endl;
    cout << "arrayC elements: ";
    output(arrayC,C); //輸出排序後數字

    return 0;
}

int bubblesort(int* array,int size) //由小到大排序
{
    int n = 0;
    int i = 1;

    for (int m=1; m<size; m++)
    {
        n = i;
        do
        {
            if(array[n]<array[n-1])
            {
                array[n] += array[n-1];
                array[n-1] = array[n] - array[n-1];
                array[n] -= array[n-1];
            }
            n++;
        }
        while (n<size);
    }

    return array[size];
}

int Merge(int* arr1,int* arr2,int* arr3,int size) //合併兩陣列數字
{
    for (int i=0; i<5; i++)
        arr3[i] = arr1[i];
    for (int j=5 ;j<size; j++)
        arr3[j] = arr2[j-5];

    return arr3[size];
}

void output(int* array,int size) //輸出陣列內數字
{
    for (int p=0; p<size; p++)
    {
        cout << array[p] << " ";
    }
    cout << endl;
}
