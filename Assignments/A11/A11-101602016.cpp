#include <iostream>
using namespace std;

void bubblesort(int* array,int size);//宣告冒泡排序的函數
void Merge(int *arr1,int *arr2,int *arr3,int size);//宣告將A,B陣列排完大小傳入C陣列的函數

int main()
{
    int *arrA,*arrB,*arrC;//宣告指向A,B,C陣列的指針
    int arrayA[5]= {10,8,6,4,2},arrayB[5]= {9,7,5,3,1},arrayC[10];//宣告A,B,C陣列

    arrA=arrayA;//令三個指針指向A,B,C陣列
    arrB=arrayB;
    arrC=arrayC;

    cout<<"Before sort"<<endl<<"array1 elements:";
    for(int i=0; i<5; i++)//顯示經由冒泡排序前的A,B陣列值
        cout<<" "<<*(arrA+i);
    cout<<endl<<"array2 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<*(arrB+i);
    cout<<endl;

    bubblesort(arrA,5);//以冒泡排序,排序A陣列

    bubblesort(arrB,5);//以冒泡排序,排序B陣列

    cout<<"After sort"<<endl<<"array1 elements:";
    for(int i=0; i<5; i++)//顯示經由冒泡排序後的A,B陣列值
        cout<<" "<<*(arrA+i);
    cout<<endl<<"array2 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<*(arrB+i);
    cout<<endl;

    Merge(arrA,arrB,arrC,10);//將A,B陣列的值,以小排到大後存到C陣列內

    cout<<"After merging two arrays"<<endl<<"array3 elements:";
    for(int i=0; i<10; i++)//顯示最終C陣列的值
        cout<<" "<<*(arrC+i);

    return 0;
}

void bubblesort(int* arr,int size)//冒泡排序的函數
{
    int temp=0,i=0,j=0;

    for(i=0; i<size; i++)//冒泡排序
    {
        for(j=0; j<size-1; j++)
        {
            if(*(arr+j)>*(arr+j+1))//比較兩兩相鄰的數，若前項大於後項，則兩數交換位置
            {
                temp=*(arr+j);
                *(arr+j)=*(arr+j+1);
                *(arr+j+1)=temp;
            }
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)//將A,B陣列的值，以小排到大後存到C陣列內的函數
{
    /*
    用這種方式將A,B的值依小到大排序後存入C的前提是
    我們需要先將A,B陣列內的值由小到大排好
    因為在前面我們以經使用冒泡排序將A,B陣列的值排列過
    所以我們才可以使用這種方式將A,B的值存入C
    */
    int i=0,j=0;
    for(int k=0; k<size; k++)
    {//為了使A[i]以及B[j]不超出陣列大小，必須設定i,j的最大值
        if(i!=5&&j!=5)//當i,j皆不等於5時(因為A,B陣列的大小皆為5)
        {
            if(*(arr1+i)<*(arr2+j))//若A陣列的值小於B陣列的值
            {
                *(arr3+k)=*(arr1+i);//將A陣列的值存入C
                i++;//考慮A陣列的下一項
            }
            else if(*(arr1+i)>*(arr2+j))//若A陣列的值大於B陣列的值
            {
                *(arr3+k)=*(arr2+j);//將B陣列的值存入C
                j++;//考慮B陣列的下一項
            }
        }
        else if(i==5)//當i=5，也就是說A陣列的數全都比較完了
        {
            *(arr3+k)=*(arr2+j);//將剩於的B陣列的數，全部存到C陣列內
            j++;
        }
        else if(j==5)//當j=5，也就是說B陣列的數全都比較完了
        {
            *(arr3+k)=*(arr1+i);//將剩於的A陣列的數，全部存到C陣列內
            i++;
        }
    }
}



































