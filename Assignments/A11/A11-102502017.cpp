#include<iostream>
using namespace std;

void bubblesort(int* array , int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

int main(void)
{
    int arrayA[]= {10,8,6,4,2},arrayB[]= {9,7,5,3,1};
    int arrayC[10]= {};
    cout << "Before sort" << endl;
    for(int i=0; i<5; i++)cout << ((i==0) ? "array1 elements: ":" ") << arrayA[i];
    cout << endl;
    for(int i=0; i<5; i++)cout << ((i==0) ? "array2 elements: " : " ") << arrayB[i];
    cout << endl;
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);
    cout << "After sort" << endl;
    for(int i=0; i<5; i++)cout << ((i==0) ? "array1 elements: ":" ") << arrayA[i];
    cout << endl;
    for(int i=0; i<5; i++)cout << ((i==0) ? "array2 elements: " : " ") << arrayB[i];
    cout << endl;
    Merge(arrayA,arrayB,arrayC,10);
    cout << "After merging two arrays" << endl;
    for(int i=0; i<10; i++)cout << ((i==0) ?  "array3 elements: " : " ") <<*(arrayC+i);

    return 0;
}

void bubblesort(int* array,int size)
{
    for(int j=0,k=size; j<size; j++)
    {
        for(int i=0; i<k-1; i++)
        {
            if(array[i]>array[i+1])
            {
                array[i]+=array[i+1];
                array[i+1] = array[i]-array[i+1];
                array[i]-=array[i+1];
            }
        }
        k--;
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    for(int i=0,x=0,y=0; i<size; i++)
    {
        if(x!=5 && y!=5)                //如果不是陣列的最後一項
        {
            if(*(arr1+x)<*(arr2+y))     //將其中較小的項讀入array3中
            {
                *(arr3+i)=*(arr1+x);
                x++;
            }
            else
            {
                *(arr3+i)=*(arr2+y);
                y++;
            }
        }
        else if(x==5)*(arr3+i)=*(arr2+y);   //若比較的最後一項是x 則 x=5
        else *(arr3+i)=*(arr1+x);           //反之
    }
}
