#include<iostream>
using namespace::std;

void bubblesort(int *arr,int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    int *arrA,*arrB,*arrC;
    arrA=arrayA;//讓指針指向陣列
    arrB=arrayB;
    arrC=arrayC;
    cout<<"Before sort"<<endl<<"array1 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<arrayA[i];
    cout<<endl<<"array2 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<arrayB[i];//輸出陣列
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);  //用泡沫排序法排序
    cout<<endl<<"After sort"<<endl<<"array1 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<arrayA[i];
    cout<<endl<<"array2 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<arrayB[i];  //輸出排序後的陣列
    cout<<endl<<"After merging two arrays"<<endl<<"array3 elements:";
    Merge(arrayA,arrayB,arrayC,10);//到函式裡將兩個陣列比大小
    for(int i=0; i<10; i++)
        cout<<" "<<arrayC[i];//輸出陣列
    return 0;
}

void bubblesort(int *arr,int size)
{
    for(int i=0; i<size-1; i++)
    {
        for(int j=i+1; j<size; j++)
        {
            if(*(arr+i)>*(arr+j))
            {
                int x=*(arr+i);
                int y=*(arr+j);
                *(arr+i)=y;
                *(arr+j)=x;
            }
        }
    }//泡沫排列
}

void Merge(int *arr1,int *arr2,int *arr3,int size)//將兩個已經由小排到大的陣列再次比大小
{
    int a=0,b=0,c=0;

    for(int c=0; c<size; c++)
    {
        if(a!=5&&b!=5)//當兩個陣列都還沒比完的時候就可以比大小
        {
            if(*(arr1+a)<*(arr2+b))
            {
                *(arr3+c)=*(arr1+a);
                a++;
            }
            else
            {
                *(arr3+c)=*(arr2+b);
                b++;
            }
        }
        else if(a==5)//當第一個陣列的值都放入第三個陣列之後，將第二個陣列的值依序放入第三個
        {
            *(arr3+c)=*(arr2+b);
            b++;
        }
        else if(b==5)//當第二個陣列的值都放入第三個陣列之後，將第一個陣列的值依序放入第三個
        {
            *(arr3+c)=*(arr1+a);
            a++;

        }
    }









}
