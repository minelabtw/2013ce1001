#include<iostream>

#define swap(x,y) x^=y^=x^=y  //swap

#define le(x) (x*2+1)   //left child
#define ri(x) (x*2+2)   //right child

void heapify(int* array,int size,int i)   //heapify
{
   int max=i;
   if(le(i)<size&&*(array+le(i))>*(array+i))
      max=le(i);
   if(ri(i)<size&&*(array+ri(i))>*(array+max))
      max=ri(i);
   if(max!=i)
   {
      swap(*(array+i),*(array+max));
      heapify(array,size,max);
   }
}

void buildHeap(int* array,int size)    //build heap
{
   for(int i=size/2;i>0;i--)
      heapify(array,size,i);
}

void bubblesort(int* array,int size)   //sort
{
   buildHeap(array,size); 
   for(int i=size-1;i!=0;i--)
   {
      heapify(array,i+1,0);
      swap(*(array),*(array+i));
   }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)    //merge
{
   int i=0,j=0,k;
   for(k=0;i!=size&&j!=size;k++)
      *(arr3+k)=*(arr1+i)<*(arr2+j)?*(arr1+(i++)):*(arr2+(j++));
   for(;i!=size;i++,k++)
      *(arr3+k)=*(arr1+i);
   for(;j!=size;j++,k++)
      *(arr3+k)=*(arr2+j);
}

int main()
{
   int arrayA[]={10,8,6,4,2};    //declare
   int arrayB[]={9,7,5,3,1};
   int arrayC[10];
   int* arr[3]={arrayA,arrayB,arrayC};

   std::cout<<"Before sort"<<std::endl;   //before
   for(int i=0;i!=2;i++)
   {
      std::cout<<"array"<<i+1<<" elements:";
      for(int j=0;j!=5;j++)
         std::cout<<' '<<*(arr[i]+j);
      std::cout<<std::endl;
   }

   bubblesort(arr[0],5);   //sort
   bubblesort(arr[1],5);

   std::cout<<"After sort"<<std::endl;    //after
   for(int i=0;i!=2;i++)
   {
      std::cout<<"array"<<i+1<<" elements:";
      for(int j=0;j!=5;j++)
         std::cout<<' '<<*(arr[i]+j);
      std::cout<<std::endl;
   }

   Merge(arr[0],arr[1],arr[2],5);   //merge

   std::cout<<"After merging two arrays"<<std::endl;  //after merge
   std::cout<<"array3 elements:";
   for(int i=0;i!=10;i++)
      std::cout<<' '<<*(arr[2]+i);
   std::cout<<std::endl;

   return 0;   //exit
}
