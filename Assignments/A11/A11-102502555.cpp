#include <iostream>
using namespace std;

void Bubblesort(int* array , int size);  //泡泡排序法
void Merge(int *arr1 , int *arr2 , int *arr3 , int size);  //把兩個陣列比大小並把結果存到另一個陣列
void Show(int arr[] , int size , int name);  //輸出陣列

int main(){
    int arrayA[] = {10 , 8 , 6 , 4 , 2};  //陣列A
    int arrayB[] = {9 , 7 , 5 , 3 , 1};  //陣列B
    int arrayC[10] = {};  //陣列C
    int *arr1 , *arr2 , *arr3;  //用來指陣列的指標
    arr1 = arrayA;  //指標1指到陣列A
    arr2 = arrayB;  //指標2指到陣列B
    arr3 = arrayC;  //指標3指到陣列C

    cout << "Before sort" << endl;  //  排序前

    Show(arrayA , 5 , 1);  //展示陣列內容
    Show(arrayB , 5 , 2);

    cout << "After sort" << endl;  //排序後

    Bubblesort(arr1 , 5);  //用泡泡排序法排序
    Bubblesort(arr2 , 5);

    Show(arrayA , 5 , 1);  //展示排序後陣列的內容
    Show(arrayB , 5 , 2);

    cout << "After merging two arrays" << endl;  //混合兩個陣列

    Merge(arr1 , arr2 , arr3 , 10);  //混合

    Show(arrayC , 10 , 3);  //展示混合後的陣列

    return 0;

}

void Bubblesort(int* array , int size){  //泡泡排序法
    for(int i = size - 1 ; i > 0 ; i--){
        for(int j = 0 ; j < i ; j++){
            if(array[i] < array[j]){
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
    }
}
void Merge(int *arr1 , int *arr2 , int *arr3 , int size ){  //混合陣列並由小到大排列
    int count1 = 0;  //紀錄陣列一的索引
    int count2 = 0;  //紀錄陣列二的索引

    for(int i = 0 ; i < size ; i++){  //比兩個陣列的大小並存入另一個陣列
        if(*(arr1 + count1) > *(arr2 + count2)){
            if(i != size - 1){
                *(arr3 + i) = *(arr2 + count2);
            }else{
                *(arr3 + i) = *(arr1 + count1);
            }
            if(count2 < 4){
                count2++;
            }
        }else if(*(arr2 + count2) > *(arr1 + count1)){
            if(i != size - 1){
                *(arr3 + i) = *(arr1 + count1);
            }else{
                *(arr3 + i) = *(arr2 + count2);
            }
            if(count1 < 4){
                count1++;
            }
        }
    }
}
void Show(int arr[] , int size , int name){  //展示陣列
    cout << "array" << name << " elements: ";
    for(int i = 0 ; i < size ; i++){  //從第零個到最後一個
        cout << arr[i] << " ";
        if(i == size - 1){
            cout << endl;
        }
    }
}
