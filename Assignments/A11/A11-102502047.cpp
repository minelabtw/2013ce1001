#include <iostream>

using namespace std;
void B(int* a , int b);//宣告函數
void M(int *a,int *b,int *c,int d);
int main()
{
    int a[5]= {10,8,6,4,2};//宣告陣列
    int b[5]= {9,7,5,3,1};
    int c[10]= {};
    int *arr;//宣告指標
    arr=c;//指向C的位址
    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)
        cout<<a[i]<<" ";
    cout<<endl<<"array2 elements: ";
    for(int i=0; i<5; i++)
        cout<<b[i]<<" ";
    cout<<endl<<"After sort"<<endl;
    cout<<"array1 elements: ";
    B(a,5);
    for(int i=0; i<5; i++)
        cout<<a[i]<<" ";
    cout<<endl<<"array2 elements: ";
    B(b,5);
    for(int i=0; i<5; i++)
        cout<<b[i]<<" ";
    cout<<endl<<"After merging two arrays"<<endl;
    cout<<"array3 elements: ";
    M(a,b,c,10);
    for(int i=0; i<10; i++)
        cout<<*(arr+i)<<" ";
    return 0;
}
void B(int* a , int b)//由小到大重新排列
{
    for(int i=0; i<b-1; i++)
        for(int j=0; j<b-1; j++)
        {
            if(a[j]>a[j+1])
            {
                int k=a[j];
                a[j]=a[j+1];
                a[j+1]=k;
            }
        }
}
void M(int *a,int *b,int *c,int d)//兩個陣列內的元素比較大小並由小到大存入
{
    for(int i=0; i<5; i++)
        c[i]=a[i];
    for(int i=0; i<5; i++)
        c[i+5]=b[i];
    B(c,10);
}

