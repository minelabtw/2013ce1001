#include <iostream>

using namespace std ;

int bubblesort(int* array , int size) ;                       //function prototype
int Merge(int *arr1,int *arr2,int *arr3,int size);
int main ()
{
    int arrayA [5] = {10,8,6,4,2} ;                           //宣告3個陣列
    int arrayB [5] = {9,7,5,3,1} ;
    int arrayC [10] = {} ;

    cout << "Before sort" << endl << "array1 elements: " ;    //輸出2個未排序的陣列
    for (int a = 0 ; a < 5 ; a ++)
    {
        cout << arrayA [a] << " " ;
    }
    cout << endl << "array2 elements: " ;
    for (int a = 0 ; a < 5 ; a ++)
    {
        cout << arrayB [a] << " " ;
    }
    cout << endl << "After sort" << endl << "array1 elements: " ;      //輸出2個排序過後的陣列
    bubblesort (arrayA , 5) ;
    for (int a = 0 ; a < 5 ; a ++)
    {
        cout << arrayA [a] << " " ;
    }
    cout << endl << "array2 elements: " ;
    bubblesort (arrayB , 5) ;
    for (int a = 0 ; a < 5 ; a ++)
    {
        cout << arrayB [a] << " " ;
    }

    cout << endl << "After merging two arrays" << endl << "array3 elements: " ;     //輸出結合2個陣列的新陣列
    Merge (arrayA , arrayB , arrayC , 10) ;
    for (int a = 0 ; a < 10 ; a ++)
    {
        cout << arrayC [a] << " " ;
    }

    return 0 ;
}

int bubblesort(int* array , int size)
{
    for (int b = 0 ; b < size - 1 ; b ++)                         //次數
    {
        for (int a = 0 ; a < size - 1 ; a ++)                     //排序
        {
            if (*(array+a) > *(array+(a+1)))
                swap (*(array+a) ,  *(array+(a+1))) ;
        }
    }
}

int Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int a = 0 ;
    int b = 0 ;
    int c = 0 ;

        while (a < 5 && b < 5 && c < size)
        {
            if (*(arr1+a) > *(arr2+b))                             //小的優先放入arr3
            {
                *(arr3+c) = *(arr2+b) ;
                c ++ ;
                b ++ ;
                if (b == 5)                                        //特殊情況:尾巴是大的才放
                {
                    b = 4 ;
                    if (*(arr1+a) > *(arr2+b))
                        *(arr3+(size - 1)) = *(arr1+a) ;
                }
                if (c == size)
                    break ;
            }

            if (*(arr1+a) < *(arr2+b))
            {
                *(arr3+c) = *(arr1+a) ;
                a ++ ;
                c ++ ;
                if (c == size )
                    break ;
                    if (a == 5)
                    {
                        a = 4 ;
                        if (*(arr1+a) < *(arr2+b))
                            *(arr3+(size - 1)) = *(arr2+b) ;
                    }
            }
        }
}


