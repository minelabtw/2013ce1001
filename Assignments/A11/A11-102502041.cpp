#include<iostream>
using namespace std;
void Bubblesort(int *arr, int _size)
{       //每次至少會排出一個最小的
    for(int i=1,sp; i<_size; i++)
    {
        sp=-1;
        for(int j=_size-1,temp; j>=i; j--)
            if(arr[j]<arr[j-1])
            {
                temp=arr[j];
                arr[j]=arr[j-1];
                arr[j-1]=temp;
                sp=0;
            }
        if(sp==-1)break;
    }
}
void printarray(int *arr, int _size)
{
    for(int i=0; i<_size; i++)cout<<arr[i]<<" ";
    cout<<endl;
}
void Merge(int *arr1,int *arr2, int *arrResult, int _size)
{
    int i=0,j=0,k=0;
    while(k<_size)
    {
        if(arr1[i]>arr2[j]&&j!=5)
        {   //若j=5,則超出array's boundary,造成結果錯誤
            arrResult[k]=arr2[j];
            k++,j++;
        }
        else
        {
            arrResult[k]=arr1[i];
            k++,i++;
        }
    }
}
int main()
{
    int arrayA[]= {10,8,6,4,2},arrayB[]= {9,7,5,3,1},arrayC[10];
    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    printarray(arrayA,5);
    cout<<"array2 elements: ";
    printarray(arrayB,5);
    cout<<"After sort"<<endl;
    Bubblesort(arrayA,5);
    Bubblesort(arrayB,5);
    cout<<"array1 elements: ";
    printarray(arrayA,5);
    cout<<"array2 elements: ";
    printarray(arrayB,5);
    cout<<"After merging two arrays"<<endl;
    Merge(arrayA,arrayB,arrayC,10);
    cout<<"array3 elements: ";
    printarray(arrayC,10);
    return 0;
}
