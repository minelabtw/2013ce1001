#include <iostream>
using namespace std ;

void Out(int arr[],int size,int name) ;//自己命名
void Bubblesort(int* array,int size) ;
void Merge(int *arr1,int *arr2,int *arr3,int size) ;//宣告函式
int main()
{
    int arrayA[]= {10,8,6,4,2} ;
    int arrayB[]= {9,7,5,3,1} ;
    int arrayC[10] ;
    int *arr1 , *arr2 , *arr3 ;//宣告指標

    arr1=arrayA ;
    arr2=arrayB ;
    arr3=arrayC ;

    cout << "Before sort" << endl ;
    Out(arr1,5,1) ;
    Out(arr2,5,2) ;

    Bubblesort(arr1,5) ;
    Bubblesort(arr2,5) ;

    cout << "After sort" << endl ;
    Out(arr1,5,1) ;
    Out(arr2,5,2) ;

    cout << "After merging two arrays" << endl ;
    Merge(arr1,arr2,arr3,10) ;
    Out(arr3,10,3) ;

    return 0 ;
}

void Out(int arr[],int size,int name)//輸出 需要的ELEMENT
{
    cout << "array" << name << " elements: " ;
    for(int i=0 ; i<size ; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl ;
}

void Bubblesort(int* array,int size)//排列
{
    for(int i=0 ; i< size ; i++)
    {
        for(int i=0 ; i<size-1 ; i++)
        {
            if(array[i]>array[i+1])
            {
                int temp=array[i] ;
                array[i]=array[i+1] ;
                array[i+1]=temp ;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int size)//兩兩位置排列 後放入
{
    for(int i=0 ; i<size ; i++)
    {
        if(arr1<arr2)
        {
            arr3[2*i]=arr1[i] ;
            arr3[2*i+1]=arr2[i] ;
        }
        if(arr2<arr1)
        {
            arr3[2*i]=arr2[i] ;
            arr3[2*i+1]=arr1[i] ;
        }
    }
}
