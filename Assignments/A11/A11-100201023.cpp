#include <iostream>
#include <algorithm>

using namespace std;

void bubblesort(int* array , int size);
void Merge(int* arr1 , int* arr2 , int* arr3 , int size);

int main()
{
	int arrayA[] = {10 , 8 , 6 , 4 , 2};
	int arrayB[] = {9 , 7 , 5 , 3 , 1};
	int arrayC[10];
	int* arr;

	cout << "Before sort" << endl; // output before sort
	arr = arrayA; // array1
	cout << "array1 elements: ";
	for(int i = 0 ; i < 5 ; ++i)
		cout << *(arr + i) << " ";
	cout << endl;
	arr = arrayB; // array2
	cout << "array2 elements: ";
	for(int i = 0 ; i < 5 ; ++i)
		cout << *(arr + i) << " ";
	cout << endl;

	bubblesort(arrayA , 5); // bubble sort array
	bubblesort(arrayB , 5);

	cout << "After sort" << endl; // output after sort
	arr = arrayA; // array1
	cout << "array1 elements: ";
	for(int i = 0 ; i < 5 ; ++i)
		cout << *(arr + i) << " ";
	cout << endl;
	arr = arrayB; // array2
	cout << "array2 elements: ";
	for(int i = 0 ; i < 5 ; ++i)
		cout << *(arr + i) << " ";
	cout << endl;

	Merge(arrayA , arrayB , arrayC , 10);

	cout << "After merging two arrays" << endl; // output after merging
	arr = arrayC; // array3
	cout << "array3 elements: ";
	for(int i = 0 ; i < 10 ; ++i)
		cout << *(arr + i) << " ";
	cout << endl;

	return 0;
}

void bubblesort(int* array , int size) // implement bubblesort
{
	int* arr = array;
	for(int i = size - 1 ; i > 0 ; --i)
		for(int j = 0 ; j < i ; ++j)
			if(*(arr + j) > *(array + j + 1))
				swap(*(arr + j) , *(arr + j + 1));
}

void Merge(int* arr1 , int* arr2 , int* arr3 , int size) // implement merge
{
	int *p1 , *p2 , *p3;
	int count1 = 0 , count2 = 0 , count3 = 0;
	p1 = arr1;
	p2 = arr2;
	p3 = arr3;

	while(count1 < size / 2 && count2 < size / 2)
	{
		if(*(p1 + count1) <= *(p2 + count2))
		{
			*(p3 + count3) = *(p1 + count1);
			++count1;
			++count3;
		}
		else
		{
			*(p3 + count3) = *(p2 + count2);
			++count2;
			++count3;
		}
	}

	if(count1 == size / 2)
	{
		for(count2 ; count2 < size / 2 ; ++count2)
		{
			*(p3 + count3) = *(p2 + count2);
			++count2;
			++count3;
		}
	}
	else if(count2 == size / 2)
	{
		for(count1 ; count1 < size / 2 ; ++count1)
		{
			*(p3 + count3) = *(p1 + count1);
			++count1;
			++count3;
		}
	}
}
