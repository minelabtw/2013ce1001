#include <iostream>
using namespace std;

void bubblesort(int*, int);
void Merge(int*, int*, int*, int, int);
void printarr(int*, int);


int main()
{
	int arrayA[]={10,8,6,4,2};
	int arrayB[]={9,7,5,3,1};
	int sizeA=sizeof(arrayA)/sizeof(int);
	int sizeB=sizeof(arrayB)/sizeof(int);
	int sizeC=sizeA+sizeB;
	int arrayC[sizeC];
	int * pA=arrayA,* pB=arrayB,* pC=arrayC;

	cout << "Before sort" << endl;
	cout << "array1 elements:";	printarr(pA,sizeA);
	cout << "array2 elements:";	printarr(pB,sizeB);

	bubblesort(pA,sizeA);
	bubblesort(pB,sizeB);

	cout << "After sort" << endl;
	cout << "array1 elements:";	printarr(pA,sizeA);
	cout << "array2 elements:";	printarr(pB,sizeB);

	Merge(pA,pB,pC,sizeA,sizeB);

	cout << "After merging two arrays" << endl;
	cout << "array3 elements:";	printarr(pC,sizeC);

	return 0;
}

void Merge(int* arr1,int* arr2,int* arrmerge, int size1, int size2)
{
	int idx1=0,idx2=0,idx3=0;						//set starting index
	while(idx1<size1&&idx2<size2){					//if no array is at the end
		if(*(arr1+idx1)<*(arr2+idx2)){				//if the next element in arr1 is smaller then in arr2
			*(arrmerge+idx3++)=*(arr1+idx1++);		//add the next element of arr1 to arrmerge
		}else{										//else
			*(arrmerge+idx3++)=*(arr2+idx2++);		//add the next element of arr2 to arrmerge
		}
	}

	if(idx1==size1){											//if arr1 is at the end
		while(idx2<size2){*(arrmerge+idx3++)=*(arr2+idx2++);}	//add all remain elements in arr2 to arrmerge
	}else{														//else (if arr2 is at the end)
		while(idx1<size1){*(arrmerge+idx3++)=*(arr1+idx1++);}	//add all remain elements in arr1 to arrmerge
	}
}

void bubblesort(int* arr, int siz)		//bubblesort, from small to big
{
	for(int endidx=siz-2;endidx>=0;endidx--){
		for(int i=0;i<=endidx;i++){
			if(*(arr+i)>*(arr+i+1)){
				int tmp=*(arr+i+1);
				*(arr+i+1)=*(arr+i);
				*(arr+i)=tmp;
			}
		}
	}
}

void printarr(int* arr, int siz)			//print out array
{
	for(int i=0;i<siz;i++){cout << " " << *(arr+i);}
	cout << endl;
}
