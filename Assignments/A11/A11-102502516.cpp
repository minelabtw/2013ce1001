#include <iostream>
using namespace std;

int main()
{
    int arrayA[ 5 ] = { 10, 8, 6, 4, 2 }, arrayB[ 5 ] = { 9, 7, 5, 3, 1 }, arrayC[ 10 ];  //宣告三個陣列
    void bubblesort( int *arr , int Length );
    void arrOutput ( int arrA[], int arrB[], string str);
    void Merge( int *arr1, int *arr2, int *arr3, int size);
    arrOutput( arrayA, arrayB, "Before");  //輸出排序前的陣列A、B
    bubblesort( arrayA, 5);  //排序A
    bubblesort( arrayB, 5);  //排序B
    arrOutput( arrayA, arrayB, "After");  //輸出排序後的陣列A、B
    Merge( arrayA, arrayB, arrayC, 5);  //合併
    cout << "After merging two arrays" << endl << "array3 elements: ";
    for( int counter=0; counter<10; counter++)
        cout << arrayC[ counter ] << " ";  //輸出合併後的陣列
    return 0;
}

void arrOutput ( int arrA[], int arrB[], string str)  //輸出陣列內容的函式
{
    cout << str << " sort" << endl << "array1 elements: ";
    for( int counter=0; counter<5; counter++)
        cout << arrA[ counter ] << " ";
    cout << endl << "array2 elements: ";
    for( int counter=0; counter<5; counter++)
        cout << arrB[ counter ] << " ";
    cout << endl;
}
void bubblesort( int *arr ,int Length)  //氣泡排序
{
    for ( int i=Length; i>1; i-- )
        for ( int j=0; j<i-1; j++ )
            if ( arr[ j ]>arr[ j+1 ] )  //兩兩比較，內層迴圈執行完最大的會浮到最後面
            {
                arr[ j ] = arr[ j ]-arr[ j+1 ];
                arr[ j+1 ] = arr[ j+1 ]+arr[ j ];
                arr[ j ] = arr[ j+1 ]-arr[ j ];  //兩兩交換
            }
}
void Merge( int *arr1, int *arr2, int *arr3, int size)
{
    for( int i=0; i<size; i++)
    {
        if( *(arr1+i)<*(arr2+i) ) //從頭把arr1和arr2比較，小的放前面
        {
            *( arr3+i*2 )=*(arr1+i);
            *( arr3+i*2+1 )=*(arr2+i);
        }
        else
        {
            *( arr3+i*2 )=*(arr2+i);
            *( arr3+i*2+1 )=*(arr1+i);
        }
    }
}
