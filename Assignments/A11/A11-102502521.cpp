#include <iostream>
using namespace std;

int bubblesort(int* _array , int _size);    //prototype
int Merge(int *arr1,int *arr2,int *arr3,int _size);

int main()
{
    const int _size=5;    //設定變數與常數
    int arrayA[_size]={10,8,6,4,2};
    int arrayB[_size]={9,7,5,3,1};
    int arrayC[10];

    cout<<"Before sort"<<endl;    //顯示數據
    cout<<"array1 elements: ";
    for(int elet=0;elet<_size;elet++)
    {
        cout<<arrayA[elet]<<" ";
    }
    cout<<endl;

    cout<<"array2 elements: ";
    for(int elet=0;elet<_size;elet++)
    {
        cout<<arrayB[elet]<<" ";
    }
    cout<<endl;

    bubblesort(arrayA,_size);    //呼叫函數
    bubblesort(arrayB,_size);

    cout<<"After sort"<<endl;
    cout<<"array1 elements: ";
    for(int elet=0;elet<_size;elet++)
    {
        cout<<arrayA[elet]<<" ";
    }
    cout<<endl;
    cout<<"array2 elements: ";
    for(int elet=0;elet<_size;elet++)
    {
        cout<<arrayB[elet]<<" ";
    }
    cout<<endl;

    Merge(arrayA,arrayB,arrayC,_size);    //呼叫函數

    cout<<"After merging two arrays"<<endl;
    cout<<"array3 elements: ";
    for(int elet=0;elet<_size*2;elet++)
    {
        cout<<arrayC[elet]<<" ";
    }

    return 0;
}

int bubblesort(int* _array , int _size)
{
    for(int t=0;t<_size-1;t++)    //排序
    {
        for(int num=0;num<_size-1;num++)
        {
            int change;
            if(_array[num]>=_array[num+1])
            {
                change=_array[num];
                _array[num]=_array[num+1];
                _array[num+1]=change;
            }
        }
    }
}

int Merge(int *arr1,int *arr2,int *arr3,int _size)
{
    for(int elet=0;elet<_size*2;elet=elet+2)    //排序後輸進array3
    {
        if(arr1[elet/2]>=arr2[elet/2])
        {
            arr3[elet]=arr2[elet/2];
            arr3[elet+1]=arr1[elet/2];
        }
        else
        {
            arr3[elet]=arr1[elet/2];
            arr3[elet+1]=arr2[elet/2];
        }
    }
}
