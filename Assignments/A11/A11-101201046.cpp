#include <iostream>
#include <algorithm>

//output array
#define OUT_ARR(p, arr) for (p = arr, cout << #arr << " elements:"; \
                             p != arr + (sizeof(arr) / sizeof(int));\
                             cout << " " << *p++);cout << endl

using namespace std;

//bubble sort
void bubblesort(int *array, int size) {
    for (int *p = array + (size - 1); p != array; p--)
        for (int *q = array; q != p; )
            if (*q > *(++q))
                swap(*q, *(q - 1));
    }

//merge arr1 and arr2 into arr3
//and I think the better merge function is 
//Merge(int *arr1, int size1, int *arr2, int *arr3, int size3)
void Merge(int *arr1, int *arr2, int *arr3, int size) {
    int *e1 = arr1 + size, *e2 = arr2 + size;

    while (arr1 != e1 || arr2 != e2)
        *arr3++ = (arr2 == e2 || arr1 != e1 && *arr1 < *arr2) ? *arr1++: *arr2++;
    }


int main(void) {
    int array1[] = {10,8,6,4,2};
    int array2[] = {9,7,5,3,1};
    int array3[10];
    int *p;
	
	//before sort
    cout << "Before sort\n";
    OUT_ARR(p, array1);
    OUT_ARR(p, array2);

	//sort array1 and array2
    bubblesort(array1, 5);
    bubblesort(array2, 5);

	//after sort
    cout << "After sort\n";
    OUT_ARR(p, array1);
    OUT_ARR(p, array2);

	//merge array1 and array2 into array3
    Merge(array1, array2, array3, 5);

    cout << "After merging two arrays\n";
    OUT_ARR(p, array3);

    return 0;
    }
