#include<iostream>

using namespace std;

void bubblesort(int *_array, int _size) //排序
{
    int tmp =0;
    for(int i=_size; i>0; i--)for(int j=0; j<i-1; j++)
        {
            if(*(_array+j)>*(_array+j+1))
            {
                swap(*(_array+j),*(_array+j+1));
            }
        }
}

void Merge(int *arr1, int *arr2, int *arr3, int _size) // 合併
{
    int j =0;
    for(int i=0; i<_size; i++)
    {
        if(*(arr1+i)>*(arr2+i))
        {
            *(arr3+j)= *(arr2+i);
            j++;
            *(arr3+j)= *(arr1+i);
            j++;
        }
        else
        {
            *(arr3+j)= *(arr1+i);
            j++;
            *(arr3+j)= *(arr2+i);
            j++;
        }
    }
}

int main()
{

    int arrayA[5] = {10,8,6,4,2};
    int arrayB[5] = {9,7,5,3,1};
    int arrayC[10] = {};

    int *arr1;
    int *arr2;
    int *arr3;

    arr1=arrayA; // 指標 arr1
    arr2=arrayB; // 指標 arr2
    arr3=arrayC; // 指標 arr3

    cout << "Before sort" << endl << "array1 elements: ";
    for(int i=0; i<5; i++)cout << *(arr1+i) << " ";
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++)cout << *(arr2+i) << " ";
    cout << endl;

    bubblesort(arr1,5);
    bubblesort(arr2,5);


    cout << "After sort" << endl << "array1 elements: ";
    for(int i=0; i<5; i++)cout << *(arr1+i) << " ";
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++)cout << *(arr2+i) << " ";
    cout << endl;

    Merge(arr1,arr2,arr3,5);

    cout << "After merging two arrays" << endl << "array3 elements: ";
    for(int i=0; i<10; i++)cout << *(arr3+i) << " ";
    cout << endl;

    return 0;
}
