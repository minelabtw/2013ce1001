//A11-102502026

#include<iostream>
using namespace std;
void bubblesort(int* ,int );    //prototype to sort
void Merge(int*,int*,int*,int );    //prototype to merge A and B
int main()
{
    int A[5]= {10,8,6,4,2}; //array 1
    int B[5]= {9,7,5,3,1};  //array 2
    int C[10];  //array for merge
    int S=5;    //size 5
    int *arr1;
    int *arr2;
    int *arr3;
    arr1=A;
    arr2=B;
    arr3=C;
    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for (int z=0; z<5; z++)     //print the numbers before sort of array 1
        cout<<*(arr1+z)<<" ";
    cout<<endl<<"array2 elements: ";
    for (int z=0; z<5; z++)     //print the numbers before sort of array 2
        cout<<*(arr2+z)<<" ";
    cout<<endl<<"After sort"<<endl;
    bubblesort(A, S);           //sort for array 1
    cout<<"array1 elements: ";
    for (int z=0; z<5; z++)     //print the numbers after sort of array 1
        cout<<*(arr1+z)<<" ";
    bubblesort(B, S);           //sort for array 2
    cout<<endl<<"array2 elements: ";
    for (int z=0; z<5; z++)     //print the numbers after sort of array 2
        cout<<*(arr2+z)<<" ";
    Merge(arr1,arr2,arr3,10);   //merge numbers and sort
    cout<<endl<<"After merging two arrays"<<endl;
    cout<<"array3 elements: ";
    for (int z=0; z<10; z++)    //print all number sort
        cout<<*(arr3+z)<<" ";
    cout<<endl;
    return 0;
}
void bubblesort(int* Z,int z)   //function for sorting A and B
{
    int h=0;
    for(int x=z-1; x>0; x--)
    {
        for(int y=0; y<z-1; y++)
        {
            if(*(Z+y)>*(Z+y+1))
            {
                h=*(Z+y);
                *(Z+y)=*(Z+y+1);
               *(Z+y+1)=h; //swap the numbers
            }
        }
    }
}
void Merge(int *a,int *b,int *c,int z)  //function for merging and sorting A and B
{
    int m=0;
    int n=0;
    for(int i=0; i<z; i++)
    if(i==9)
    *(c+i)=*(a+m);
    else if(*(a+m)<*(b+n))
    {
        *(c+i)=*(a+m);
        m++;
    }
    else if (*(a+m)>*(b+n))
    {
        *(c+i)=*(b+n);
        n++;
    }
}
