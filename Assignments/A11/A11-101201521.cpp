#include <iostream>
#define arraysize 5
using namespace std;
void coutarray(int* array, int size)//輸出陣列
{
    for(int i=0; i<size; i++)
        cout << *(array+i) << " ";
    cout << endl;
}

void bubblesort(int* array, int size)//bubble sort
{
    int tmp=0;
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size-1; j++)
        {
            if(*(array+j)>*(array+j+1))
            {
                tmp= *(array+j);
                *(array+j)=*(array+j+1);
                *(array+j+1)=tmp;
            }
        }
    }
}

void Merge(int *arr1, int *arr2, int *arr3, int size)//合併
{
    for(int i=0, i1=0, i2=0; i<2*size; i++)//i, i1, i2分別為arr3, arr1, arr2的項數
    {
        if(i2>=size || (i1<size && *(arr1+i1)<*(arr2+i2)))//arr2跑完，arr1沒跑完，arr1的值比arr2小
            *(arr3+i)=*(arr1+(i1++));
        else
            *(arr3+i)=*(arr2+(i2++));
    }
}
int main()
{
    int arrayA[arraysize]={10, 8, 6, 4, 2}, arrayB[arraysize]={9, 7, 5, 3, 1}, arrayC[10]={};
    cout << "Before sort" << endl;
    cout << "array1 elements: ";
    coutarray(arrayA, arraysize);
    cout << "array2 elements: ";
    coutarray(arrayB, arraysize);

    bubblesort(arrayA, arraysize);//排A
    bubblesort(arrayB, arraysize);//排B

    cout << "After sort" << endl;
    cout << "array1 elements: ";
    coutarray(arrayA, arraysize);
    cout << "array2 elements: ";
    coutarray(arrayB, arraysize);

    cout << "After merging two arrays" << endl;
    Merge(arrayA, arrayB, arrayC, arraysize);//A，B排進C
    cout << "array3 elements: ";
    coutarray(arrayC, 2*arraysize);

    return 0;
}
