#include<iostream>
using namespace std;
void change(int *x, int *y)         //自訂交換函數
{
    int s=*x;
    *x=*y;
    *y=s;
}
void bubblesort(int* arr,int _size)	//泡沫排序法
{
	for (int i=0 ; i<_size ; i++)
		for (int j=0 ; j<_size-1 ; j++)
			if (arr[j]>arr[j+1])
				change(arr+j,arr+j+1);
}
void Merge(int *arr1,int *arr2,int *arr3,int _size)	//Merge排序
{
	for (int i=0,j=0,k=0 ;k<10;)
	{
		if (arr1[i]<arr2[j])
		{
			arr3[k++]=arr1[i++];
			if (i==5)
			{
				for (; k<_size ;)
				{
					arr3[k++]=arr2[j++];
				}
				break;
			}
		}

		else
		{
			arr3[k++]=arr2[j++];
			if (j==5)
			{
				for (; k<_size ;)
				{
					arr3[k++]=arr1[i++];
				}
				break;
			}
		}
	}
}
int main()
{
	int arrayA[5]={10,8,6,4,2};		//宣告整數陣列
	int arrayB[5]={9,7,5,3,1};
	int arrayC[10]={};
	cout << "Before sort" << endl << "array1 elements: ";	//印出一開始的
	for (int i=0 ; i<5 ; i++)
		cout << arrayA[i] << " ";

	cout << endl <<"array2 elements: ";
	for (int i=0 ; i<5 ; i++)
		cout << arrayB[i] << " ";
	cout << endl << "After sort" << endl << "array1 elements: ";

	bubblesort(arrayA , 5);		//排序
	bubblesort(arrayB , 5);

	for (int i=0 ; i<5 ; i++)	//印出排序後的
		cout << arrayA[i] << " ";

	cout << endl <<"array2 elements: ";

	for (int i=0 ; i<5 ; i++)
		cout << arrayB[i] << " ";

	Merge(arrayA ,arrayB ,arrayC , 10);		//將第一個陣列及第二個陣列融合排序後印出

	cout << endl << "After merging two arrays" << endl << "array3 elements: ";
	for (int i=0 ; i<10 ; i++)
		cout << arrayC[i] << " ";

	return 0;
}

