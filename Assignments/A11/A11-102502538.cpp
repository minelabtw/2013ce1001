#include<iostream>

using namespace std;

void bubblesort(int* array , int size)//排序輸入的數
{
    for(int i=0; i<size-1; i++)
    {
        for(int j=0; j<size-1; j++)
        {
            int x;
            if(array[j]>array[j+1])//若後面較大就交換
            {

                x=array[j];
                array[j]=array[j+1];
                array[j+1]=x;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int size)//合併arr1和arr2
{
    for(int i=0; i<size*2; i++)
    {
        if(i<size)
        arr3[i]=arr2[i];
        else
        arr3[i]=arr1[i-size];
    }
}

int main()
{
    int L=5;
    int array1[]= {10,8,6,4,2};
    int array2[]= {9,7,5,3,1};
    int array3[]= {};

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<L; i++)//輸出array1內的值
    {
        cout<<array1[i]<<" ";
    }
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<L; i++)//輸出array2內的值
    {
        cout<<array2[i]<<" ";
    }
    cout<<endl;

    cout<<"After sort"<<endl;
    bubblesort(array1,L);//呼叫函數
    cout<<"array1 elements: ";
    for(int i=0; i<L; i++)//輸出排列後的陣列
    {
        cout<<array1[i]<<" ";
    }
    cout<<endl;
    bubblesort(array2,L);//呼叫函數
    cout<<"array2 elements: ";
    for(int i=0; i<L; i++)//輸出排列後的陣列
    {
        cout<<array2[i]<<" ";
    }
    cout<<endl;

    cout<<"After merging two arrays"<<endl;
    Merge(array1,array2,array3,L);//呼叫函數
    bubblesort(array3,L*2);
    cout<<"array3 elements: ";
    for(int i=0; i<L*2; i++)//輸出排列後的陣列
    {
        cout<<array3[i]<<" ";
    }

    return 0;
}
