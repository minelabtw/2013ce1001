#include <iostream>
using namespace std;

void bubblesort(int*, int );
void suffer(int *, int *, int *, int );

int main()
{
    int arrayA[5] = {10,8,6,4,2};
    int arrayB[5] = {9,7,5,3,1};
    int arrayC[10]; // 提供10格預設值為0的空位

    cout << "Before sort" << endl << "array1 elements:";
    for (int i = 0; i < 5; i++ )
        cout << " " << arrayA[i];

    cout << endl << "array2 elements:";
    for (int i = 0; i < 5; i++)
        cout << " " << arrayB[i];

    cout << endl << "After sort" << endl << "array1 elements:";
    bubblesort(arrayA, 5); // 一階比大小
    for (int i = 0; i < 5; i++)
        cout << " " << arrayA[i];

    bubblesort(arrayB, 5);
    cout << endl << "array2 elements:";
    for (int j = 0; j < 5; j++)
        cout << " " << arrayB[j];

    cout << endl << "After merging two arrays" << endl << "array3 elements:";
    suffer (arrayA, arrayB, arrayC, 10); // A, B 放入C，進行排序

    for (int i = 0; i < 10; i++)
        cout << " " << arrayC[i];

        cout << endl;

    return 0;
}

void bubblesort(int *arrt, int number)
{
    for (int s = 0; s < number - 1; s++)
    {
        for (int x = 0; x < number-1; x++)
        {
            int hold;
            hold = arrt[x+1];
            if (arrt[x] > arrt[x+1])
            {
                arrt[x+1] = arrt[x];
                arrt[x] = hold;
            }
        }
    }
}

void suffer (int *acc, int *bcc, int *ccc, int qq)
{
    int hand = 0;// 位置代號
    int hand1 = 0;

    for (int i = 0; i < qq; i++)
    {
        if (hand == 5) // 最後一項的結果，第五為空位，所以要將另一陣列的最後一項放入C陣列最後一位
        {
            ccc[i] = bcc[hand1];
        }
        else if (hand1 == 5)
        {
            ccc[i] = acc[hand];
        }
        else if (acc[hand] > bcc[hand1])
        {
            ccc[i] = bcc[hand1];// 小則放入，位置加一，大則保留。
            hand1++;
        }
        else if (acc[hand] < bcc[hand1])
        {
            ccc[i] = acc[hand];
            hand++;
        }

    }
}
