#include <iostream>

using namespace std;

void bubblesort(int* arrayx , int sizeofx)//將陣列的值由小到大排序
{
    for (int i=1; i<sizeofx; i++)
        for (int j=1; j<sizeofx; j++)
        {
            if (*(arrayx+j-1)>*(arrayx+j))
                swap(*(arrayx+j-1),*(arrayx+j));
        }


}
void Merge(int *arr1,int *arr2,int *arr3,int sizeofc)
{
    int a= 0, b= 0;//a為arr1陣列的組碼，b為arr2陣列的組碼
    int i;
    for (i=0; i<sizeofc&&a<5&&b<5; i++)
    {
        if (*(arr1+a)<*(arr2+b))
        {
            *(arr3+i) = *(arr1+a);
            a++;//arr1陣列的組碼+1
        }
        else
        {
            *(arr3+i) = *(arr2+b);
            b++;//arr2陣列的組碼+1
        }

    }
    while (a<5)//a陣列未比較完成
    {
        *(arr3+i) = *(arr1+a);
        i++;//arr3陣列的組碼+1
        a++;//arr1陣列的組碼+1
    }
}
int main()
{
    int arrayA[5]= {10,8,6,4,2,}, arrayB[5]= {9,7,5,3,1,}, arrayC[10]= {};
    int *arra, *arrb, *arrc;
    cout << "Before sort" << endl;
    cout << "array1 elements: ";
    for (int i=0; i<5; i++)
        cout << arrayA[i] << " ";
    cout << endl;
    cout << "array2 elements: ";
    for (int i=0; i<5; i++)
        cout << arrayB[i] << " ";
    cout << endl;
    cout << "After sort" << endl;
    bubblesort(arrayA,5);//將陣列A的值排列
    bubblesort(arrayB,5);//將陣列B的值排列
    arra=arrayA;
    arrb=arrayB;
    arrc=arrayC;
    cout << "array1 elements: ";
    for (int i=0; i<5; i++)
        cout << *(arra+i) << " ";
    cout << endl;
    cout << "array2 elements: ";
    for (int i=0; i<5; i++)
        cout << *(arrb+i) << " ";
    cout << endl;
    cout << "After merging two arrays" << endl;
    cout << "array3 elements: ";
    Merge(arrayA,arrayB,arrayC,10);//將A陣列和B陣列比較後排列儲存於C陣列
    for (int i=0; i<10; i++)
        cout << *(arrc+i) << " ";
    cout << endl;
    return 0;
}
