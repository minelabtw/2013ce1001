#include <iostream>
using namespace std;
void merge(int *arr1,int *arr2,int *arr3,int size)
{
    int i=0;                            //arr1 counter
    int j=0;                            //arr2 counter
    for(int k=0; k<size-1; k++)         //arr3 counter=k
    {
        if(*(arr1+i)>*(arr2+j))         //比較 較小的 則放入arr3
        {
            *(arr3+k)=*(arr2+j);
            j++;                        //比較完的 後移
            if(j==5)                    //當陣列 比完則把剩下的給arr3最後一個
            {
                *(arr3+k+1)=*(arr1+i);
            }
        }
        else                            //比較 較小的 則放入arr3
        {
            *(arr3+k)=*(arr1+i);
            i++;                        //比較完的 後移
            if(i==5)                    //當陣列 比完則把剩下的給arr3最後一個
            {
                *(arr3+k+1)=*(arr2+j);
            }
        }
    }
}
void bubblesort(int* array , int size)
{
    int temp;
    for(int h=size; h>1; h--)                   //迴圈 size-1次  (找最大size-1次)
    {
        for(int j=0; j<h-1; j++)                //找最大 從arr[0] 找到 arr[h] 比較h-1次  每次 比上一次比較少1次
        {
            if(*(array+j)>*(array+j+1))             //若比下一個大 則 替換
            {
                temp=*(array+j);
                *(array+j)=*(array+j+1);
                *(array+j+1)=temp;
            }
        }
    }
}
int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    cout<<"Before sort"<<endl;
    int *arr;                           //宣告指標
    arr=arrayA;                         //指向arrayA初始位置
    cout<<"array1 elements: ";
    for(int j=0; j<5; j++)              //印出陣列
    {
        cout<<*(arr+j)<<" ";
    }
    cout<<endl;
    arr=arrayB;                         //指向arrayB初始位置
    cout<<"array2 elements: ";
    for(int j=0; j<5; j++)              //印出陣列
    {
        cout<<*(arr+j)<<" ";
    }
    cout<<endl;
    cout<<"After sort"<<endl;
    bubblesort(arrayA,5);                        //排列
    bubblesort(arrayB,5);
    arr=arrayA;
    cout<<"array1 elements: ";
    for(int j=0; j<5; j++)
    {
        cout<<*(arr+j)<<" ";
    }
    cout<<endl;
    arr=arrayB;
    cout<<"array2 elements: ";
    for(int j=0; j<5; j++)
    {
        cout<<*(arr+j)<<" ";
    }
    cout<<endl;
    cout<<"After merging two arrays"<<endl;
    merge(arrayA,arrayB,arrayC,10);             //比較兩陣列
    arr=arrayC;                                 //指向arrayC初始位置
    cout<<"array3 elements: ";
    for(int j=0; j<10; j++)                     //印出陣列
    {
        cout<<*(arr+j)<<" ";
    }
    return 0;
}
