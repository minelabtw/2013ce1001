#include <iostream>
using namespace std;

void bubblesort(int *arr,int size){//bubblesort
    int i,j;
    for(i=0;i<size;i++){
        for(j=1;j<size-i;j++){
            if(*(arr+j) < *(arr+j-1))
                swap(*(arr+j),*(arr+j-1));//將小的交換到前面 
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size1,int size2){//Merge
    int i,j,k;//i為第三陣列目前位置,j,k分別為兩陣列目前指到的位置 
    for(i=0,j=0,k=0;j<size1 && k<size2;i++){//將兩陣列位置0開始,將指到的位置兩兩比較放入第三個陣列,直到其中一陣列全放入為止 
        if(*(arr1+j) < *(arr2+k))
            *(arr3+i) = *(arr1+(j++));
        else{
            *(arr3+i) = *(arr2+(k++));
        }
    }
    while(j<size1)//將剩下沒放入的值照順序放入
        *(arr3+(i++)) = *(arr1+(j++));
    while(k<size2)
        *(arr3+(i++)) = *(arr2+(k++));
}

int main(){
    const int size1=5,size2=5;//size1,size2分別為第一個和第二個陣列的大小,
    int *arr,array1[size1]={10,8,6,4,2},array2[size2]={9,7,5,3,1},array3[size1+size2],i;
    cout << "Before sort\n";//輸出排序前的兩陣列 
    arr = array1;
    for(i=0,cout << "array1 elements:";i<size1;i++){
        cout << " " << *(arr+i);
    }
    arr = array2;
    for(i=0,cout << "\narray1 elements:";i<size2;i++){
        cout << " " << *(arr+i);
    }
    bubblesort(array1,size1);//排序陣列1 
    bubblesort(array2,size2);//排序陣列2
    cout << "\nAfter sort\n";//輸出排序後的兩陣列 
    arr = array1;
    for(i=0,cout << "array1 elements:";i<size1;i++){
        cout << " " << *(arr+i);
    }
    arr = array2;
    for(i=0,cout << "\narray1 elements:";i<size2;i++){
        cout << " " << *(arr+i);
    }
    Merge(array1,array2,array3,size1,size2);//Merge兩陣列至第三個陣列 
    cout << "\nAfter merging two arrays\n";//輸出排序後第三個陣列 
    arr = array3;
    for(i=0,cout << "array3 elements:";i<size1+size2;i++){
        cout << " " << *(arr+i);
    }
    cout << endl;
    return 0;
}
