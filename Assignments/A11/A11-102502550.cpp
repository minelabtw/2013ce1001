#include <iostream>
using namespace std;
void bubblesort(int* , int );
void Merge(int* ,int* ,int* ,int);

int main()
{
    int arrayA[5]= {10,8,6,4,2},arrayB[5]= {9,7,5,3,1},arrayC[10];                             //陣列宣告

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";                                                                 //排序之前
    for(int i=0; i<5; i++) cout<<*(arrayA+i)<<" ";
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<5; i++) cout<<*(arrayB+i)<<" ";
    cout<<endl;

    bubblesort(arrayA,5);                                                                      //呼叫泡沫排序
    bubblesort(arrayB,5);

    cout<<"After sort"<<endl;

    cout<<"array1 elements: ";                                                                  //排序之後
    for(int i=0; i<5; i++) cout<<*(arrayA+i)<<" ";
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<5; i++) cout<<*(arrayB+i)<<" ";
    cout<<endl;

    Merge(arrayA,arrayB,arrayC,5);                                                              //呼叫合併函數

    cout<<"After merging two arrays\n";

    cout<<"array3 elements: ";                                                                  //輸出最後結果
    for(int i=0; i<10; i++) cout<<*(arrayC+i)<<" ";


    return 0;
}
void bubblesort(int* array , int size)                                                           //泡沫排序法
{
    int tmp,count=0;
    for(int i=size-1; i>=1; i--)
    {
        for(int j=0; j<i; j++)
        {
            if(*(array+j) > *(array+j+1))
            {
                tmp=*(array+j);
                *(array+j)=*(array+j+1);
                *(array+j+1)=tmp;
            }
            count++;
        }
        if(!count) break;

    }

}
void Merge(int *arr1,int *arr2,int *arr3,int size)                                                   //合併陣列
{
    int c=0;
    for(int i=0; i<size; i++)
    {
        if(*(arr1+i)<*(arr2+i))
        {
            *(arr3+c++)=*(arr1+i);
            *(arr3+c++)=*(arr2+i);
        }
        else
        {
            *(arr3+c++)=*(arr2+i);
            *(arr3+c++)=*(arr1+i);
        }
    }
}
