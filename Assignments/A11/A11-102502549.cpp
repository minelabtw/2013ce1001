#include <iostream>
using namespace std;

void bubblesort(int* array , int size);//宣告bubblesort原型
void Merge(int *arr1,int *arr2,int *arr3);//宣告Merge原型

int main()
{
    //以下宣告一目瞭然
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    int *arr1=arrayA;
    int *arr2=arrayB;
    int *arr3=arrayC;

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";

    for(int i=0; i<5; i++)//印出排序前結果
    {
        if(i==4)
            cout<<*(arr1+i)<<endl;
        else
            cout<<*(arr1+i)<<" ";
    }

    cout<<"array2 elements: ";

    for(int i=0; i<5; i++)//印出排序前結果
    {
        if(i==4)
            cout<<*(arr2+i)<<endl;
        else
            cout<<*(arr2+i)<<" ";
    }

    bubblesort(arrayA,5);//將arrayA bubblesort
    bubblesort(arrayB,5);//將arrayB bubblesort

    cout<<"After sort"<<endl;
    cout<<"array1 elements: ";

    for(int i=0; i<5; i++)//印arrayA bubblesort後結果
    {
        if(i==4)
            cout<<*(arr1+i)<<endl;
        else
            cout<<*(arr1+i)<<" ";
    }

    cout<<"array2 elements: ";

    for(int i=0; i<5; i++)//印出arrayB bubblesort結果
    {
        if(i==4)
            cout<<*(arr2+i)<<endl;
        else
            cout<<*(arr2+i)<<" ";
    }

    Merge(arrayA,arrayB,arrayC);//將array AarrayB合併排序

    cout<<"After merging two arrays"<<endl;
    cout<<"array3 elements: ";

    for(int i=0; i<10; i++)//印合併排序後結果
    {
        if(i==9)
            cout<<*(arr3+i);
        else
            cout<<*(arr3+i)<<" ";
    }

    return 0;//結束
}

//實作bubblesort
void bubblesort(int* array , int size)
{
    int temp;//拿來存較小值

    for(int i=0; i<size-1; i++)
    {
        for(int j=0; j<size-1-i; j++)
        {
            if(array[j]>array[j+1])
            {
                temp=array[j+1];
                array[j+1]=array[j];
                array[j]=temp;
            }
        }
    }
}

//實作Merge
void Merge(int *arr1,int *arr2,int *arr3)//陣列大小已經給定所以不用size參數
{
    int i=0;
    int j=0;
    int k=0;

    while(i!=5&&j!=5)//將兩陣列從第一個元素比大小，小的放到arr3直到其中一陣列比完
    {
        if(*(arr1+i)<*(arr2+j))
        {
            *(arr3+k)=*(arr1+i);
            i++;
        }
        else
        {
            *(arr3+k)=*(arr2+j);
            j++;
        }

        k++;
    }

    for(int a=k; a<10; a++)//將剩下的值直接丟給arr3
    {
        if(i!=5)
        {
            *(arr3+a)=*(arr1+i);
            i++;
        }
        else
        {
            *(arr3+a)=*(arr2+j);
            j++;
        }
    }
}

