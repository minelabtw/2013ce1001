#include <iostream>
using namespace std;

void bubblesort(int* array , int size) //把arry由小到大重新排列的函式
{
    for(int i=1; i<size; i++)
    {
        for(int j=0; i+j<size; j++)
        {
            if(array[j]>array[1+j])
            {
                int t=array[j];
                array[j]=array[1+j];
                array[1+j]=t;
            }
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size) //把arr1和arr2兩個陣列內的元素比較大小並由小到大存入arr3中
{
    int i=0,j=0;
    while(i+j<10)
    {
        if(arr1[i]>arr2[j]&&j<5)
        {
            arr3[i+j]=arr2[j];
            j++;
        }
        else
        {
            arr3[i+j]=arr1[i];
            i++;
        }
    }
}

int main()
{
    int arrayA[]= {10,8,6,4,2}; //首先宣告三個陣列其中兩個陣列一開始就分別初始化
    int arrayB[]= {9,7,5,3,1};
    int arrayC[10];
    cout<<"Before sort\narray1 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<arrayA[i];
    cout<<"\narray2 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<arrayB[i];
    bubblesort(arrayA,5); //利用bubblesort，把array1和array2由小到大重新排列並輸出
    cout<<"\nAfter sort\narray1 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<arrayA[i];
    bubblesort(arrayB,5);
    cout<<"\narray2 elements:";
    for(int i=0; i<5; i++)
        cout<<" "<<arrayB[i];
    Merge(arrayA,arrayB,arrayC,10); //把array1和array2兩個陣列內的元素比較大小並由小到大存入arrayC中
    cout<<"\nAfter merging two arrays\narray3 elements:";
    for(int i=0; i<10; i++)
        cout<<" "<<arrayC[i]; //最後輸出array3的結果

    return 0;
}
