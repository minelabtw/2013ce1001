#include<iostream>

using namespace std;

void bubblesort(int* array , int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

void printArray(int *array, const char *arrName, int size)
{
    //printing all elements on screen
    cout << arrName << " elements:"; //arrName is the name printed on screen such as "array1"
    for(int i=0; i<size ; i++)
        cout << " " << array[i];
    cout << endl;
}

int main()
{
    int arrayA[5] = {10,8,6,4,2};
    int arrayB[5] = {9,7,5,3,1};
    int arrayC[10],i;
    //before sort
    cout << "Before sort" << endl;
    printArray(arrayA, "array1", 5); //output
    printArray(arrayB, "array2", 5);
    //After sort
    cout << "After sort" << endl;
    bubblesort(arrayA, 5); //sorted
    printArray(arrayA, "array1", 5); //output
    bubblesort(arrayB, 5);
    printArray(arrayB, "array2", 5);
    //Let's Mergeeeeeeeeeeeeeeee
    cout << "After merging two arrays" << endl;
    Merge(arrayA, arrayB, arrayC, 10); //merged
    printArray(arrayC, "array3", 10); //output

    return 0;
}

void bubblesort(int* array , int size)
{
    int i, tmp;
    bool flag; //a flag checking if swap has been done
    do
    {
        flag = false; //initialize flag
        size--;
        //Initially, we just need to check 0 to size-1
        //In the next loop, the last element was sorted, so we reduce size by 1
        for(i=0; i<size; i++)
        {
            if(*(array+i) > *(array+i+1))
            {
                //swap if array[i] is larger then the following
                tmp = *(array+i);
                *(array+i) = *(array+i+1);
                *(array+i+1) = tmp;
                flag = true; //swap has been done
            }
        }
    }
    while(flag || size==1);
    //if nothing was swaped in last loop
    //or if the last two element was sorted
    //then all data was sorted
}

void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int i=0, j=0, k=0; //index for three array
    //it's merge sort, you know it
    while(i!=size/2 && j!=size/2)
    {
        //choose the smaller
        if(*(arr1+i) < *(arr2+j))
        {
            *(arr3+k) = *(arr1+i);
            i++;
        }
        else
        {
            *(arr3+k) = *(arr2+j);
            j++;
        }
        k++;
    }
    //an array is clear
    //so only a while loop will be executed in the following
    //remaining terms in array1
    while(i<size/2)
    {
        *(arr3+k) = *(arr1+i);
        i++;
        k++;
    }
    //remaining terms in array2
    while(j<size/2)
    {
        *(arr3+k) = *(arr2+j);
        j++;
        k++;
    }
}
