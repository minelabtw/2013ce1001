#include <iostream>

using namespace std;

void sort(int* , int );
void Merge(int* ,int* ,int* ,int);

int main()
{
    int arrayA[5]= {10,8,6,4,2},arrayB[5]= {9,7,5,3,1},arrayC[10];

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";

    for(int i=0;i<5;i++)                                                                //輸出
    {
        cout<<*(arrayA+i)<<" ";
    }

    cout<<endl<<"array2 elements: ";

    for(int i=0;i<5;i++)
    {
        cout<<*(arrayB+i)<<" ";
    }

    cout<<endl<<"After sort"<<endl;

    sort(arrayA,5);                                                                         //排列
    sort(arrayB,5);

    cout<<"array1 elements: ";

    for(int i=0;i<5;i++)
    {
        cout<<*(arrayA+i)<<" ";
    }

    cout<<endl<<"array2 elements: ";

    for(int i=0;i<5;i++)
    {
        cout<<*(arrayB+i)<<" ";
    }

    cout<<endl<<"After merging two arrays"<<endl;

    Merge(arrayA,arrayB,arrayC,5);                                                           //全排列

    cout<<"array3 elements: ";

    for(int i=0; i<10;i++)
    {
        cout<<*(arrayC+i)<<" ";
    }
    return 0;
}
void sort(int* array,int size)                                                              //泡沫排列
{
    int tmp,count=0;
    for(int i=size-1;i>=1;i--)
    {
        for(int j=0;j<i;j++)
        {
            if(*(array+j)>*(array+j+1))
            {
                tmp=*(array+j);
                *(array+j)=*(array+j+1);
                *(array+j+1)=tmp;
            }
            count++;
        }
        if(!count)
        {
            break;
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)                                          //比對
{
    int k=0,i=0,j=0;

    while(i<size && j<size)
    {
        if(*(arr1+i)<*(arr2+j))
        {
            *(arr3+k++)=*(arr1+i++);
        }
        else
        {
            *(arr3+k++)=*(arr2+j++);
        }
    }
    if (i>=size)
        for(int m=j,c=0;m<size;m++,c++)
        {
            arr3[k+c]=arr2[m];
        }
    else
        for(int m=i,c=0; m<size;m++,c++)
        {
            arr3[k+c]=arr1[m];
        }
}
