#include <iostream>

using namespace std;

void bubblesort( int* , int );
void Merge( int* , int* , int* , int );

int main()
{
    int arrayA[5] = { 10 , 8 , 6 , 4 , 2 };
    int arrayB[5] = { 9 , 7 , 5 , 3 , 1 };
    int arrayC[10];


    cout << "Before sort" << endl << "array1 elements: " ;  //原式輸出
    for ( int h = 0 ; h < 5 ; h++ )
        cout << arrayA[h] << " ";
    cout << endl << "array2 elements: " ;
    for ( int h = 0 ; h < 5 ; h++ )
        cout << arrayB[h] << " ";

    bubblesort ( arrayA , 5 );
    bubblesort ( arrayB , 5 );

    cout << endl << "After sort" << endl << "array1 elements: " ;
    for ( int k = 0 ; k < 5 ; k++ )
        cout << arrayA[k] << " " ;
    cout << endl << "array2 elements: " ;
    for ( int k = 0 ; k < 5 ; k++ )
        cout << arrayB[k] << " ";

    Merge( arrayA , arrayB , arrayC , 10 );
    cout << endl << "After merging two arrays" << endl << "array3 elements: " ;
    for ( int i = 0 ; i < 10 ; i++ )
        cout << arrayC[i] << " " ;

    return 0;
}

void bubblesort( int* arr , int arraysize ) //比大小 & 重新排列
{
    for ( int x = 0 ; x < arraysize ; x++ )
    {
        for ( int y = 0 ; y < arraysize ; y++ )
        {
            if ( *(arr+y) > *(arr+x) )
            {
                int hold = *(arr+x) ;
                *(arr+x) = *(arr+y) ;
                *(arr+y) = hold ;
            }
        }
    }
}

void Merge( int* arrA , int* arrB , int* arrC , int arraysize ) //組合
{
    int x = 0 ;
    int y = 0 ;

    for ( int i = 0 ; i < arraysize ; i++ )
    {
        if ( *(arrA+x) < *(arrB+y) || y == 5 )  //arrayB 在 y = 5 中無值
        {
            *(arrC+i) = *(arrA+x) ;
            x++;
        }
        else
        {
            *(arrC+i) = *(arrB+y) ;
            y++;
        }
    }
}
