#include<iostream>

using namespace std;


void bubblesort(int *arrA ,int size);
void Merge(int *arrA,int *arrB,int *arrC,int size);




int main()
{
    int A[5]={10,8,6,4,2};
    int B[5]={9,7,5,3,1};
    int C[10]={0};
    int *arrA=A;
    int *arrB=B;
    int *arrC=C;
    int q,r;

    cout << "Before sort" << endl << "array1 elements: ";
    for(q=0;q<5;q++)
    {
        cout << *(arrA+q) << " ";
    }
    cout << endl << "array2 elements: ";
    for(r=0;r<5;r++)
    {
        cout << *(arrB+r) << " ";
    }
    cout << endl << "After sort" << endl;

    bubblesort(arrA,5);
    bubblesort(arrB,5);
    Merge(arrA,arrB,arrC,5);

    cout << "array1 elements: ";
     for(q=0;q<5;q++)
    {
        cout << *(arrA+q) << " ";
    }
    cout << endl << "array2 elements: ";
    for(r=0;r<5;r++)
    {
        cout << *(arrB+r) << " ";
    }
    cout << endl << "After merging two arrays" << endl;
    cout << "array3 elements: ";
     for(r=0;r<10;r++)
    {
        cout << *(arrC+r) << " ";
    }

}

void bubblesort(int *arrA , int size)
{
   int c,i,j;
   for(i=size - 1;i >0;i--)
   {
       for(j=0;j < i;j++)
       if(*(arrA+j)> *(arrA+j+1))
       {
           c = *(arrA+j);
           *(arrA+j)= *(arrA+j+1);
           *(arrA+j+1) =  c ;
       }
   }
}

void Merge(int *arrA,int *arrB,int *arrC,int size)
{
    int countA, countB , countC;
    int *arr1 = arrA , *arr2 = arrB , *arr3 = arrC;
    countA = 0;
    countB = 0;
    countC = 0;
    while(countA < 5 && countB < 5)
    {
        if(*(arr1+countA) > *(arr2+countB))
        {
            *(arr3+countC) = *(arr2+countB);
            countB++;
            countC++;
        }
        else
        {
            *(arr3+countC) = *(arr1+countA);
            countA++;
            countC++;
        }
    }

        if(countA == 5)
        {
            while(countB < 5)
            {
                *(arr3+countC) = *(arr2+countB);
                countC++;
                countB++;
            }
        }
        else
        {
            while(countA < 5)
            {
                *(arr3+countC) = *(arr1+countA);
                countC++;
                countA++;
            }
        }
}
