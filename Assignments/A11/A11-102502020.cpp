#include <iostream>

using namespace std;

void bubblesort(int *array,int size);                  //宣告bubblesort函式
void Merge(int *arr1,int *arr2,int *arr3,int size);    //宣告Merge函式
void output(int number,int *array,int size);           //宣告output函式

int main()
{
    int i=0;                        //宣告變數
    int arrayA[5]= {10,8,6,4,2};    //宣告陣列
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    int *arr1=arrayA;               //用指標存取陣列的值
    int *arr2=arrayB;
    int *arr3=arrayC;
    do
    {
        if(i==0)
            cout << "Before sort" << endl;
        else
            cout << "After sort" << endl;
        output(1,arr1,5);                        //呼叫output函式
        output(2,arr2,5);
        bubblesort(arrayA,5);                    //呼叫bubblesort函式
        bubblesort(arrayB,5);
        i++;
    }
    while(i<2);
    cout << "After merging two arrays" << endl;
    Merge(arrayA,arrayB,arrayC,10);              //呼叫Merge函式
    output(3,arr3,10);

    return 0;
}
void bubblesort(int *arr,int _size)              //bubblesort排序法
{
    int number=0;
    for(int i=1; i<_size; i++)                   //由小到大排列
    {
        for(int j=0; j<_size-i; j++)             //把比較小的排在後面
        {
            if(*(arr+j)>*(arr+j+1))
            {
                number=*(arr+j+1);
                *(arr+j+1)=*(arr+j);
                *(arr+j)=number;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int _size)   //Merge排序法
{
    int i=0,j=0;
    while(i<5 && j<5)                                 //把比較小的排前面
    {
        if(*(arr1+i)<*(arr2+j))
        {
            *(arr3+i+j)=*(arr1+i);
            i++;
        }
        else
        {
            *(arr3+i+j)=*(arr2+j);
            j++;
        }
    }
    for(int k=i+j; k<=_size; k++)                     //把剩下的排在後面
    {
        if(i>4)
        {
            *(arr3+k)=*(arr2+j);
            j++;
        }
        else
        {
            *(arr3+k)=*(arr1+i);
            i++;
        }
    }
}
void output(int number,int *arr,int _size)            //輸出陣列
{
    cout << "array" << number << " elements:";
    for(int i=0; i<_size; i++)
    {
        cout << " " << *(arr+i);
    }
    cout << endl;
}
