#include <iostream>

using namespace std ;

int bubblesort ( int* array , int size ) ;  //宣告bubblesort函式。
int Merge ( int* arr1 , int* arr2 , int* arr3 , int size ) ;  //宣告Merge函式。

int main ()
{
    int arrayA [ 5 ] = { 10 , 8 , 6 , 4 , 2 } ;  //陣列一。
    int arrayB [ 5 ] = { 9 , 7 , 5 , 3 , 1 } ;  //陣列二。
    int arrayC [ 10 ] ;  //陣列三，存放之後排列好的值。

    cout << "Before sort" << endl << "array1 elements: " ;
    for ( int i = 0 ; i <= 4 ; i ++ )
    {
        cout << arrayA [ i ] << " " ;
    }  //輸出陣列一的各項值。
    cout << endl << "array2 elements: " ;
    for ( int i = 0 ; i <= 4 ; i ++ )
    {
        cout << arrayB [ i ] << " " ;
    }  //輸出陣列二的各項值。

    cout << endl << "After sort" << endl ;

    bubblesort ( arrayA , 5 ) ;
    bubblesort ( arrayB , 5 ) ;  //將陣列一和二做氣泡排序。

    cout << "array1 elements: " ;
    for ( int i = 0 ; i <= 4 ; i ++ )
    {
        cout << arrayA [ i ] << " " ;
    }  //將排序後的陣列一輸出。
    cout << endl << "array2 elements: " ;
    for ( int i = 0 ; i <= 4 ; i ++ )
    {
        cout << arrayB [ i ] << " " ;
    }  //將排序後的陣列二輸出。

    cout << endl << "After merging two arrays" << endl << "array3 elements: " ;

    Merge ( arrayA , arrayB , arrayC , 10 ) ;  //將兩陣列放進陣列三中，並做由小到大的排序。

    for ( int i = 0 ; i < 10 ; i ++ )
    {
        cout << arrayC [ i ] << " " ;
    }  //輸出陣列三各項之值。

    return 0 ;
}

int bubblesort ( int* array , int size )
{
    for ( int i = size - 1 ; i > 0 ; i -- )
    {
        for ( int j = 0 ; j < i ; j ++ )
        {
            if ( array [ j ] > array [ j + 1 ] )
                swap ( array [ j ] , array [ j + 1 ] ) ;
        }
    }
    return array [ size ] ;
}  //定義bubblesort函式：由小到大排列。

int Merge ( int* arr1 , int* arr2 , int* arr3 , int size )
{
    int x = 0 ;
    int y = 0 ;

    for ( int i = 0 ; i < size ; i ++ )
    {
        if ( x != 5 && (arr1 [ x ] < arr2 [ y ]) )
        {
            arr3 [ i ] = arr1 [ x ] ;
            x ++ ;
        }
        else if ( y != 5 && (arr1 [ x ] > arr2 [ y ]) )
        {
            arr3 [ i ] = arr2 [ y ] ;
            y ++ ;
        }
        else if ( i == size - 1 )
            {
            arr3 [ i ] = arr1 [ x ] ;
            }
    }
    return arr3 [ size ] ;
}  //定義Merge函式：將兩陣列融合，由小到大排列，並傳回陣列三。
