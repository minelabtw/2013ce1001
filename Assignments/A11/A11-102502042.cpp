#include <iostream>
using namespace std;
void bubblesort(int*,int );
void Merge(int*,int*,int*,int);
void output(int*,int,int);
int main()
{
    ios::sync_with_stdio(0);
    const int siz=5;    //siz is arrayA and arrayB's size
    int arrayA[]= {10,8,6,4,2};
    int arrayB[]= {9,7,5,3,1};
    int arrayC[10];
    cout<<"Before sort"<<endl;
    output(arrayA,siz,1);
    output(arrayB,siz,2);
    bubblesort(arrayA,5);   //sort arrayA
    bubblesort(arrayB,5);   //sort arrayB
    cout<<"After sort"<<endl;
    output(arrayA,siz,1);
    output(arrayB,siz,2);
    Merge(arrayA,arrayB,arrayC,siz);    //merge arrayA and arrayB
    cout<<"After merging two arrays"<<endl;
    output(arrayC,2*siz,3);
    return 0;
}
void bubblesort(int *arr,int siz)   //bubble sort!!
{
    for(int i=0; i<siz; ++i)
        for(int j=i+1; j<siz; ++j)
            if(*(arr+i)>*(arr+j))
            {
                int tmp=*(arr+i);
                *(arr+i)=*(arr+j);
                *(arr+j)=tmp;
            }
}
void Merge(int *arrayA,int *arrayB,int *arrayC,int siz) //merge function
{
    int id1=0,id2=0,id3=0;  //the index of array
    while(id1<siz||id2<siz)
    {
        if(id1==siz)
        {
            while(id2<siz)
            {
                *(arrayC+id3)=*(arrayB+id2);
                id2++;
                id3++;
            }
        }
        else if(id2==siz)
        {
            while(id1<siz)
            {
                *(arrayC+id3)=*(arrayA+id1);
                id1++;
                id3++;
            }
        }
        else
        {
            if(*(arrayA+id1)>*(arrayB+id2))
                *(arrayC+id3)=*(arrayB+id2),id2++;
            else
                *(arrayC+id3)=*(arrayA+id1),id1++;
            id3++;
        }
    }
}
void output(int *arr,int siz,int type)  //type is to express one of arrayA~C
{
    if(type==1)cout<<"array1 ";
    else if(type==2)cout<<"array2 ";
    else cout<<"array3 ";
    cout<<"elements:";
    for(int i=0; i<siz; ++i)
        cout<<" "<<*(arr+i);
    cout<<endl;
}
