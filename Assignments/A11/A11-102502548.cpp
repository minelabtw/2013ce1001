#include <iostream>

using namespace std ;

void bubblesort(int* ptr )//宣告函式
{
    int storenumber=0 ;

    for(int x=4; x>0; x--)//使用迴圈
    {
        for (int i=0; i<x; i++)
        {
            if (ptr[i]>ptr[i+1])
            {
                storenumber=ptr[i] ;

                ptr[i]=ptr[i+1] ;

                ptr[i+1]=storenumber ;
            }
        }
    }

    return ;
}

void Merge(int* ptr, int* ptr2, int* ptr3)//宣告函式，用指標宣告陣列
{
    int n=0 ;

    for (int x=0; x<9; x++, n=n+2 )
    {
        if (ptr[x]>ptr2[x])
        {
            ptr3[n]=ptr2[x] ;

            ptr3[n+1]=ptr[x] ;
        }

        else if (ptr[x]<ptr2[x])
        {
            ptr3[n]=ptr[x] ;

            ptr[n+1]=ptr2[x] ;
        }
    }

    return ;
}

int main ()
{
    int arrayA[5]= {10,8,6,4,2}, arrayB[5]= {9,7,5,3,1}, arrayC[10];

    int size=0 ;

    cout << "Before sort\n" << "array1 elements: " ;

    for (int size=0; size<5; size++)
    {
        cout << arrayA[size] << " " ;
    }

    cout << endl ;

    cout << "array2 elements: " ;

    for (int size=0; size<5; size++)
    {
        cout << arrayB[size] << " " ;
    }

    cout << endl ;

    cout << "After sort\n" << "array1 elements: " ;


    bubblesort(arrayA);//呼叫函式
    bubblesort(arrayB);


    for (int size=0; size<5; size++)
    {
        cout << arrayA[size] << " " ;
    }

    cout << endl ;

    cout << "array2 elements: " ;

    for (int size=0; size<5; size++)
    {
        cout << arrayB[size] << " " ;
    }

    cout << endl ;

    cout << "After merging two arrays\n" ;

    cout << "array3 elements: " ;

    Merge(arrayA, arrayB, arrayC) ;

    for (int size=0;size<10;size++)
    {
        cout << arrayC[size] << " " ;
    }

    return 0 ;
}
