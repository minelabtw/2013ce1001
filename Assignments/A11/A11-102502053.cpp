#include <iostream>
using namespace std;

//function prototype
void bubblesort(int*,int);
void Merge(int*,int*,int*,int);

//start of main
int main()
{
    //call variables
    int *arra;
    int *arrb;
    int *arrc;
    const int arraysize=5;
    const int arraysizec=10;
    int a[arraysize]={10,8,6,4,2};//initialise array
    int b[arraysize]={9,7,5,3,1};//initialise array
    int c[arraysizec]={};//initialise array

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    arra=a;
    for(int n=0;n<arraysize;n++)//display of elements inside array
    {
        cout<<*(arra+n)<<" ";
    }
    cout<<endl;

    cout<<"array2 elements: ";
    arrb=b;
    for(int n=0;n<arraysize;n++)//display of elements inside array
    {
        cout<<*(arrb+n)<<" ";
    }
    cout<<endl;

    bubblesort(arra,arraysize); //call out function to sort
    bubblesort(arrb,arraysize); //call out function to sort

    cout<<"After sort"<<endl;
    cout<<"array1 elements: ";
    for(int n=0;n<arraysize;n++)//display of elements inside array after sort
    {
        cout<<*(arra+n)<<" ";
    }
    cout<<endl;

    cout<<"array2 elements: ";
    for(int n=0;n<arraysize;n++)//display of elements inside array after sort
    {
        cout<<*(arrb+n)<<" ";
    }
    cout<<endl;

    //marge arrays
    arrc=c;
    Merge(arra,arrb,arrc,arraysize);
    cout<<"After merging two arrays"<<endl;
    cout<<"array3 elements: ";
    for(int n=0;n<10;n++)
    {
        cout<<*(arrc+n)<<" ";
    }

    return 0;
}

void bubblesort(int*sortarray,int arraysize) //function for sorting
{
    int hold=0;
    while(arraysize>1)
    {

        for(int n=0;n<arraysize-1;n++)
        {
            hold=*(sortarray+n);
            if(hold>*(sortarray+n+1))
            {
                *(sortarray+n)=*(sortarray+n+1);
                *(sortarray+n+1)=hold;
            }
        }
        arraysize--;
    }
}

void Merge(int*arr1,int*arr2,int*arr3,int arraysize) //function for merging 2 arrays
{
    int m=0;
    int n=0;
    for(int i=0; i<10; i++)
    {
        if(*(arr1+n)<=*(arr2+m))
        {
            *(arr3+i)=*(arr1+n);
            if(n<4)
            {
                n++;
            }
        }
        else if(*(arr1+n)>*(arr2+m))
        {
            *(arr3+i)=*(arr2+m);
            if(m<4)
            {
                m++;
            }
        }
    }
    if(n==arraysize-1&&m==arraysize-1)//save the largest in the last place of arr3
    {
        if(*(arr1+n)>=*(arr2+m))
        {
            *(arr3+9)=*(arr1+n);
        }
        else
            *(arr3+9)=*(arr2+m);
    }
}
