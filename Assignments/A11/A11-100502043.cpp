#include <iostream>
using namespace std;

int bubblesort(int *array,int size);  //prototype
int swap(int *ele1ptr,int *ele2ptr);  //prototype
int Merge(int *arr1,int *arr2,int *arr3,int size);   //prototype

int main()
{
    int arrayA[5]={10,8,6,4,2};
    int arrayB[5]={9,7,5,3,1};
    int arrayC[10];

    cout << "Before sort" << "\narray1 elements: ";  //印出原始A、B陣列
    for (int i=0;i<5;i++)
        cout << arrayA[i] << " ";
    cout << "\narray2 elements: ";
    for (int i=0;i<5;i++)
        cout << arrayB[i] << " ";

    bubblesort(arrayA,5);  //呼叫轉換函式排列A陣列大小
    bubblesort(arrayB,5);  //呼叫轉換函式排列B陣列大小

    cout << "\nAfter sort" << "\narray1 elements: ";  //印出排序後的A、B陣列
    for (int i=0;i<5;i++)
        cout << arrayA[i] << " ";
    cout << "\narray2 elements: ";
    for (int i=0;i<5;i++)
        cout << arrayB[i] << " ";

    Merge(arrayA,arrayB,arrayC,5);  //呼叫合併函式，將A、B陣列存入C陣列，並排序大小

    cout << "\nAfter merging two arrays" << "\narray3 elements: ";  //印出C陣列
    for (int i=0;i<10;i++)
        cout << arrayC[i] << " ";

    return 0;
}

int bubblesort(int *array,int size)
{
    int Min;
    for (int i=0;i<size-1;i++)
    {
        Min=i;
        for (int index=i+1;index<size;index++)
            if(array[index]<array[Min])
                Min=index;
        swap(&array[i],&array[Min]);
    }
}
int swap(int *ele1ptr,int *ele2ptr)
{
    int hold=*ele1ptr;
    *ele1ptr=*ele2ptr;
    *ele2ptr=hold;
}
int Merge(int *arr1,int *arr2,int *arr3,int size)  //合併函式
{
    int i=0,j=0,k=0;

    while (i<size&&j<size)  //while重複比較
    {
        if (arr1[i]<arr2[j])
            arr3[k++]=arr1[i++];  //若陣列1第i項較大，則傳入陣列3第k項，並使i和k加1繼續比較
        else if (arr1[i]>arr2[j])
            arr3[k++]=arr2[j++];  //若陣列2第j項較大，則傳入陣列3第k項，並使j和k加1繼續比較
    }
    while (i<size)
        arr3[k++]=arr1[i++];  //將陣列1剩餘的第i項傳入陣列3
    while (j<size)
        arr3[k++]=arr2[j++];  //將陣列2剩餘的第j項傳入陣列3
}


