#include <iostream>
using namespace std;

int bubblesort(int* _array , int _size);
int Merge(int *arr1,int *arr2,int *arr3,int Mergesize);

int main()
{
    int arrayA[5]= {10,8,6,4,2};//初始化其值
    int arrayB[5]= {9,7,5,3,1};//初始化其值
    int arrayC[10];//宣告一陣列
    int *arr;

    cout << "Before sort" << endl;
    cout << "array1 elements: " ;
    for (int i=0; i<5; i++)//output陣列數字
    {
        cout << arrayA[i] << " ";
    }
    cout << endl;
    cout << "array2 elements: ";
    for (int i=0; i<5; i++)//output陣列數字
    {
        cout << arrayB[i] << " ";
    }
    cout <<endl;
    cout << "After sort" << endl;

    bubblesort(arrayA,5);//call function
    bubblesort(arrayB,5);

    cout << "array1 elements: " ;
    for (int i=0; i<5; i++)//output陣列數字
    {
        cout << arrayA[i] << " ";
    }
    cout << endl;
    cout << "array2 elements: ";
    for (int i=0; i<5; i++)//output陣列數字
    {
        cout << arrayB[i] << " ";
    }
    cout <<endl;

    Merge(arrayA,arrayB,arrayC,10);//call function
    cout << "After merging two arrays" << endl;


    arr=arrayC;//指標存取陣列的值
    cout << "array3 elements: " ;
    for (int l=0; l<10; l++)
    {
        cout << *(arr+l) << " ";
    }

    return 0;
}

int bubblesort(int* _array , int _size)//用來由小到大排序
{
    int hold;
    for (int l=0; l<_size-1; l++)//perform size-1 time (at most)
    {
        for (int k=0; k<_size-1; k++)//第一個數字和後面的數字相比
        {
            if (_array[k]>_array[k+1])//如果前面數字大於後面，則對調
            {
                hold=_array[k+1];
                _array[k+1]=_array[k];
                _array[k]=hold;
            }
        }
    }
}

int Merge(int *arr1,int *arr2,int *arr3,int Mergesize)//將兩個陣列中的數字兩兩比較
{
    int j = 0;
    int k = 0;

    for (int i=0; i<Mergesize; i++)//迴圈，共存到Array3十個數字
    {
        if (j==5)//arr1中的陣列都比arr2小，要將arr2剩下的值輸入到arr3
        {
            arr3[i]=arr2[k];
            k++;
        }
        else if (k==5)//同理
        {
            arr3[i]=arr1[j];
            j++;
        }
        else if (arr1[j]>arr2[k])//兩個陣列同時從0開始比，arr1[0]>arr2[0]時，arr1[0]在跟arr2[1]比
        {
            arr3[i]=arr2[k];
            k++;
        }
        else if (arr1[j]<arr2[k])//同理
        {
            arr3[i]=arr1[j];
            j++;
        }
    }
}
