#include<iostream>
using namespace std;

void bubblesort(int *arr,int size)//冒泡排序
{
    int temp=0;
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size-1; j++)
        {
            if(arr[j]>arr[j+1])
            {
                temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int i=0,j=0;
    for(int k=0; k<size; k++)
    {
        if(i!=5&&j!=5)//i,j的變化讓AB兩兩做比較,當i or j=5時另一個已經比較完，接著執行下面的else if把C陣列填滿
        {
            if(arr1[i]<arr2[j])
            {
                arr3[k]=arr1[i];
                i++;//判斷下一個i
            }
            else if(arr1[i]>arr2[j])
            {
                arr3[k]=arr2[j];
                j++;//判斷下一個j
            }
        }
        else if(i==5)//A陣列已全部比較完，C剩餘的空間由B陣列填滿
        {
            arr3[k]=arr2[j];
            j++;
        }
        else if(j==5)//B陣列已全部比較完，C剩餘的空間由A陣列填滿
        {
            arr3[k]=arr1[i];
            i++;
        }
    }
}
int main()
{
    int arrayA[5]={10,8,6,4,2};
    int arrayB[5]={9,7,5,3,1};
    int arrayC[10]={0};

    cout << "Before sort" << endl << "array1 elements:";
    for(int i=0; i<5; i++)//輸出原始陣列
        cout << " " << arrayA[i];
    cout<<endl<<"array2 elements:";
    for(int i=0; i<5; i++)
        cout << " " << arrayB[i];
    cout << endl;

    bubblesort(arrayA,5),bubblesort(arrayB,5);

    cout << "After sort" << endl << "array1 elements:";
    for(int i=0; i<5; i++)//輸出經過冒泡排序後的陣列
        cout << " " << arrayA[i];
    cout<<endl<<"array2 elements:";
    for(int i=0; i<5; i++)
        cout << " " << arrayB[i];
    cout << endl;

    Merge(arrayA,arrayB,arrayC,10);//將A,B陣列以小排到大存到C陣列

    cout << "After merging two arrays" << endl << "array3 elements:";
    for(int i=0; i<10; i++)
        cout << " " << arrayC[i];

    return 0;
}

