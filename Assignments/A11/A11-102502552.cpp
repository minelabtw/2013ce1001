#include<iostream>
using namespace std;

void bubblesort(int []);//呼叫排序函式
void Merge(int *,int *,int *);//呼叫合體排序函式

int main ()
{
    int arrayA[5] = {10,8,6,4,2};//宣告三個陣列
    int arrayB[5] = {9,7,5,3,1};
    int arrayC[10] = {};

    cout << "Before sort" << endl << "Array1 elements:";

    for(int c1 = 0;c1 < 5;c1++)
    {
        cout << arrayA[c1] << " ";
    }//輸出第一個陣列

    cout << endl << "Array2 elements:";

    for(int c2 = 0;c2 < 5;c2++)
    {
        cout << arrayB[c2] << " ";
    }//輸出第一個陣列

    cout << endl << "After sort" << endl << "Array1 elements:";

    bubblesort(arrayA);//對第一陣列排序

    for(int c3 = 0;c3 < 5;c3++)
    {
        cout << arrayA[c3] << " ";
    }//輸出排序後的第一陣列

    cout << endl << "Array2 elements:";

    bubblesort(arrayB);//對第二陣列排序

    for(int c4 = 0;c4 < 5;c4++)
    {
        cout << arrayB[c4] << " ";
    }//輸出排序後的第二陣列

    cout << endl << "After merging two arrays" << endl << "array3 elements:";

    Merge(arrayA,arrayB,arrayC);//合體排序

    for(int c6 = 0;c6 < 10;c6++)
    {
        cout << arrayC[c6] << " ";
    }//輸出合體後並排序的陣列

return 0;//回傳
}

void bubblesort(int Array[])
{
    int change = 0;
    for(int i = 0;i < 4;i++)
    {
        for(int j = 0;j < 4 - i;j++)
        {
           if(Array[j] > Array[j + 1])
           {
               change = Array[j];
               Array[j] = Array[j + 1];
               Array[j + 1] = change;
           }
        }
    }
}//定義排序陣列

void Merge(int *arrayA,int *arrayB,int *arrayC)
{
    int a = 0;
    int b = 0;

    for(int c5 = 0;c5 < 10;c5++)
    {
        if(arrayA[a] > arrayB[b])
        {
            arrayC[c5] = arrayB[b];
            b++;
        }
        else
        {
            arrayC[c5] = arrayA[a];
            a++;
        }//比較兩陣列，將小的依序填入
        if( c5 == 9 )
        {
            if(arrayA[a] < arrayB[b])
            {
            arrayC[c5] = arrayB[b];
            }
            else
            {
            arrayC[c5] = arrayA[a];
            }
        }//最後剩下最大值，不能比小，於是比大
    }
}//定義合體排序陣列
