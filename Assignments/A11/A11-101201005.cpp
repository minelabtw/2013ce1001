//Merge
#include<iostream>
using namespace std;

void bubblesort(int* array , int size)
{
    for (int i=0; i<size; i++) //由小排列到大
    {
        for (int j=size-1; j >i; j--)
        {
            if(array[j] < array[j-1])
            {
                int tmp = array[j-1];
                array[j-1] = array[j];
                array[j] = tmp;
            }
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size) //把array1和array2兩個陣列內的元素比較大小並由小到大存入arrayC中
{
    for (int i=0, j=0, k=0; i<size; i++)
    {
        if (j<5 && k<5)
        {
            if( arr1[j]<arr2[k] )
            {
                arr3[i] = arr1[j];
                j++;
            }
            else
            {
                arr3[i] = arr2[k];
                k++;
            }
        }
        else if (j>=5)
        {
            arr3[i] = arr2[k];
        }
        else if (k>=5)
        {
            arr3[i] = arr1[j];
        }
    }
}

int main()
{
    int *arr1;
    int *arr2;
    int arrayA[5]= {10,8,6,4,2}; //陣列
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    arr1=arrayA;
    arr2=arrayB;

    cout << "Before sort\n";
    cout << "array1 elements: " ;
    for (int i=0; i<5; i++)
        cout << arr1[i] << " ";
    cout << "\narray2 elements: ";
    for (int i=0; i<5; i++)
        cout << arr2[i] << " ";
    bubblesort(arr1,5); //bubblesort function
    bubblesort(arr2,5);
    cout << "\nAfter sort\n";
    cout << "array1 elements: ";
    for (int i=0; i<5; i++)
        cout << arr1[i] << " ";
    cout << "\narray2 elements: ";
    for (int i=0; i<5; i++)
        cout << arr2[i] << " ";
    Merge(arrayA,arrayB,arrayC,10); //引入merge
    cout << "\nAfter merging two arrays\n";
    cout << "array3 elements: ";
    for (int i=0; i<10; i++)
        cout << arrayC[i] << " ";

    return 0;
}
