#include <iostream>
using namespace std;

void Bubblesort(int* array , int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

int main(){

    int arrayA[5] = {10,8,6,4,2};
    int arrayB[5] = {9,7,5,3,1};
    int arrayC[10];

    cout << "Before sort" << endl;
    cout << "array1 elements:";
    for(int i=0; i<5; i++)               //�L�X�}�C
        cout << " " << arrayA[i];
    cout << endl << "array2 elements:";
    for(int i=0; i<5; i++)
        cout << " " << arrayB[i];

    Bubblesort(arrayA, 5);
    Bubblesort(arrayB, 5);

    cout << endl << "After sort" << endl;
    cout << "array1 elements:";
    for(int i=0; i<5; i++)
        cout << " " << arrayA[i];
    cout << endl << "array2 elements:";
    for(int i=0; i<5; i++)
        cout << " " << arrayB[i];

    Merge(arrayA, arrayB, arrayC, 10);
    cout << endl << "After merging two arrays" << endl << "array3 elements:";
    for(int i=0; i<10; i++)
        cout << " " << arrayC[i];

    return 0;
}

void Bubblesort(int* array , int size){
    int temp;

    for(int i=size-1; i>=1; i--){
        for(int j=0; j<i; j++){
            if(*(array+j) > *(array+j+1)){
                temp = *(array+j);
                *(array+j) = *(array+j+1);
                *(array+j+1) = temp;
            }
        }
    }
}

void Merge(int* arr1, int* arr2, int* arr3, int size){
    int* temp1 = arr1;
    int* temp2 = arr2;

    for(int i=0; i<size-1; i++){
        if(*temp1 < *temp2){
            *(arr3+i) = *temp1;
            temp1++;
        }
        else{
            *(arr3+i) = *temp2;
            temp2++;
        }
    }

    if(*(arr1+4) > *(arr2+4))
        *(arr3+9) = *(arr1+4);
    else
        *(arr3+9) = *(arr2+4);
}
