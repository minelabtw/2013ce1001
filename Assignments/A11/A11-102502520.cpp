#include <iostream>
#include <iomanip>

using namespace std;
int arrA[5]={10,8,6,4,2};//定義陣列
int arrB[5]={9,7,5,3,1};
int arrC[10]={};
int arr1[5]={};
int arr2[5]={};
void bubblesort(int *arr1, int *arr2);//定義函式
void Merge (int *arr1, int *arr2, int *arrC);
int main()
{
    cout<<"Before sort\n";//輸出
    cout<<"array1 elements: ";
    for (int x=0;x<5;x++)//顯示陣列
    {
        cout<<setw(3)<<arrA[x];
    }
    cout<<endl<<"array2 elements: ";//輸出
    for (int x=0;x<5;x++)//顯示陣列
    {
        cout<<setw(3)<<arrB[x];
    }

    bubblesort (arr1, arr2);//呼叫函式
    cout<<endl<<"After sort\n";//輸出
    cout<<"array1 elements: ";
    for (int x=0;x<5;x++)//顯示陣列
    {
        cout<<setw(3)<<arrA[x];
    }
    cout<<endl<<"array2 elements: ";//輸出
    for (int x=0;x<5;x++)//顯示陣列
    {
        cout<<setw(3)<<arrB[x];
    }
    Merge(arr1,arr2,arrC);//呼叫函式
    cout<<endl<<"After merging two arrays \n";//輸出
    cout<<"array3 elements: ";
    for (int z=0; z<10;z++)//顯示陣列
    {
        cout<<setw(3)<<arrC[z];
    }
    return 0;
}

void bubblesort (int*arr1, int*arr2)
{
    arr1=arrA;//陣列A的大小排列
    for (int i=4;i>0;i--)
        for (int j=0;j<i;j++)
        if (arr1[j]>arr1[j+1])
            swap(arr1[j],arr1[j+1]);
    arr2=arrB;//陣列B的大小排列
    for (int i=4;i>0;i--)
        for (int j=0;j<i;j++)
        if (arr2[j]>arr2[j+1])
            swap(arr2[j],arr2[j+1]);
}

void Merge(int *arr1, int *arr2, int *arr3)
{
    arr1=arrA;
    arr2=arrB;
    arr3=arrC;
    for(int a=0; a<5; a++)//將陣列A之值傳入陣列C
    {
        arr3[a]=arr1[a];
    }
    for(int b=5; b<10; b++)//將陣列B之值傳入陣列C
    {
        arr3[b]=arr1[b];
    }
    for (int i=9;i>0;i--)//陣列C的大小排列
        for (int j=0;j<i;j++)
        if (arr3[j]>arr3[j+1])
            swap(arr3[j],arr3[j+1]);
}
