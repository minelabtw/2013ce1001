#include<iostream>
using namespace std;

void change(int *a,int *b)//交換函數
{
    int s=*a;//先把a的值另外存在一個變數中
    *a=*b;//然後把b的值存入給a
    *b=s;//再把先前儲存的a值給b
}
void bubble_sort(int *x,int size)
{
    for(int i=1; i<size; i++)//限定總共有幾個數要比較,即限定要比較幾次
    {
        for(int j=0; j<size-1; j++)
            if(x[j]>x[j+1])//弱勢比前一項的數值大,就用change函數交換位置
                change(x+j,x+j+1);
    }
}
int size=5;
void merge(int *arr1,int *arr2,int *arr3,int size)
{
    for (int i=0,j=0,k=0 ; k<10;)
    {
        if (arr1[i]<arr2[j])
        {
            arr3[k]=arr1[i];//比較後將較小的優先給arr3陣列
            i++;
            k++;
            if (i==5)
            {
                for (; k<size;)
                    arr3[k++]=arr2[j++];
                break;
            }
        }

        else
        {
            arr3[k++]=arr2[j++];
            if (j==5)
            {
                for (; k<size ;)
                    arr3[k++]=arr1[i++];
                break;
            }
        }
    }
}

int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10]= {};
    int *arr1,*arr2,*arr3;
    arr1=arrayA;
    arr2=arrayB;
    arr3=arrayC;
    cout<<"Before sort"<<endl;
    cout<<"array1 elements: "<<*(arr1)<<" "<<*(arr1+1)<<" "<<*(arr1+2)<<" "<<*(arr1+3)<<" "<<*(arr1+4)<<endl;
    cout<<"array2 elements: "<<*(arr2)<<" "<<*(arr2+1)<<" "<<*(arr2+2)<<" "<<*(arr2+3)<<" "<<*(arr2+4)<<endl;
    cout<<"After sort"<<endl;
    bubble_sort(arr1,size);
    cout<<"array1 elements: "<<*(arr1)<<" "<<*(arr1+1)<<" "<<*(arr1+2)<<" "<<*(arr1+3)<<" "<<*(arr1+4)<<endl;
    bubble_sort(arr2,size);
    cout<<"array2 elements: "<<*(arr2)<<" "<<*(arr2+1)<<" "<<*(arr2+2)<<" "<<*(arr2+3)<<" "<<*(arr2+4)<<endl;
    size=10;
    merge(arr1,arr2,arr3,size);
    cout<<"After merging two arrays"<<endl;
    cout<<"array3 elements: "<<*(arr3)<<" "<<*(arr3+1)<<" "<<*(arr3+2)<<" "<<*(arr3+3)<<" "<<*(arr3+4)<<" "<<*(arr3+5)<<" "<<*(arr3+6)<<" "<<*(arr3+7)<<" "<<*(arr3+8)<<" "<<*(arr3+9);
}
