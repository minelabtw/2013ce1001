#include<iostream>
using namespace std ;
void bubblesort(int*,int) ;//宣告函式
void Merge(int*,int*,int*,int);
int main()
{
    int *arrA ;//宣告變數
    int *arrB ;
    int *arrC ;
    int n=5 ;
    int arrayA[5]= {10,8,6,4,2} ;//宣告陣列
    int arrayB[5]= {9,7,5,3,1} ;
    int arrayC[10]= {} ;
    cout << "Before sort" << endl ;
    arrA=arrayA ;//使用指標存取陣列的值
    arrB=arrayB ;
    arrC=arrayC ;
    cout << "array1 elements: " ;
    for (int i=0; i<5; i++)
    {
        cout << *(arrA+i) << " " ; //輸出陣列arrA的值
    }
    cout << endl << "array2 elements: " ;
    for (int i=0; i<5; i++)
    {
        cout << *(arrB+i) << " " ;//輸出陣列arrB的值
    }
    cout << endl << "After sort" << endl ;
    bubblesort(arrA,n);//呼叫函式
    cout << "array1 elements: " ;
    for (int i=0; i<5; i++)
    {
        cout << *(arrA+i) << " " ;
    }
    bubblesort(arrB,n);//呼叫含式
    cout << endl << "array2 elements: " ;
    for (int i=0; i<5; i++)
    {
        cout << *(arrB+i) << " " ;
    }
    cout << endl << "After merging two arrays" << endl << "array3 elements: " ;
    Merge(arrA,arrB,arrC,n);//呼叫函式
    for (int i=0; i<10; i++)
    {
        cout << *(arrC+i) << " " ;
    }
    return 0 ;
}
void bubblesort(int *arrA1,int n1)//函式將陣列值由小到大排列在回傳
{
    int x=0 ;
    for (int j=0; j<n1-1; j++)
    {
        for (int i=0; i<n1-1; i++)
        {
            if (*(arrA1+i)>*(arrA1+i+1))
            {
                x=*(arrA1+i);
                *(arrA1+i)=*(arrA1+i+1);
                *(arrA1+i+1)=x ;
            }
        }
    }
}
void Merge(int *arrA1,int *arrB1,int *arrC1,int n1)//函式將arrA arrB 的值傳入arrC 再由小到大排列然後回傳
{
    for (int i=0; i<n1; i++)
    {
        *(arrC1+2*i)=*(arrA1+i);
        *(arrC1+2*i+1)=*(arrB1+i);
    }
    int x=0 ;
    for (int j=0; j<9; j++)
    {
        for (int i=0; i<9; i++)
        {
            if (*(arrC1+i)>*(arrC1+i+1))
            {
                x=*(arrC1+i);
                *(arrC1+i)=*(arrC1+i+1);
                *(arrC1+i+1)=x ;
            }
        }
    }
}

