#include <iostream>
using namespace std;

int bubblesort(int* array , int size); // function prototype
int Merge(int *arr1,int *arr2,int *arr3,int size); // function prototype

int main()
{
    int arrayA[] = { 10, 8, 6, 4, 2, }; // initialize arrayA
    int arrayB[] = { 9, 7, 5, 3, 1 }; // initialize arrayB
    int arrayC[ 10 ] = {}; // initialize arrayC

    cout << "Before sort" << endl;

    cout << "array1 elements: ";

    for ( int i = 0; i < 5; i++ ) // for loop to output arrayA
    {
        cout << arrayA[ i ] << " ";
    }

    cout << endl;

    cout << "array2 elements: ";

    for ( int j = 0; j < 5; j++ ) // for loop to output arrayB
    {
        cout << arrayB[ j ] << " ";
    }

    bubblesort( arrayA , 5 );
    bubblesort( arrayB , 5 );

    cout << endl << "After sort" << endl;

    cout << "array1 elements: ";

    for ( int i = 0; i < 5; i++ ) // for loop to output arrayA
    {
        cout << arrayA[ i ] << " ";
    }

    cout << endl;

    cout << "array2 elements: ";

    for ( int j = 0; j < 5; j++ ) // for loop to output arrayB
    {
        cout << arrayB[ j ] << " ";
    }

    Merge( arrayA, arrayB, arrayC, 10 );

    cout << endl << "After merging two arrays" << endl << "array3 elements: ";

    for ( int m = 0; m < 10; m++ ) // for loop to output arrayC
    {
        cout << arrayC[ m ] << " ";
    }

    return 0;
}

int bubblesort(int* array , int size)
{
    for ( int k = size - 1; k > 0; k-- )
    {
        for ( int k1 = 0; k1 < size - 1; k1++ )
        {
            if ( array[ k1 ] > array[ k1 + 1] )
            {
                int t = array[ k1 ];
                array[ k1 ] = array[ k1 + 1 ];
                array[ k1 + 1 ] = t;
            }
        }
    }
}

int Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int i = 0,j = 0,k = 0;
    while ( i < 5 && j < 5 )
    {
        if ( arr1[ i ] < arr2[ j ] )
        {
            arr3[ k ] = arr1[ i ];
            i++;
        }
        else
        {
            arr3[ k ] = arr2[ j ];
            j++;
        }
        k++;
    }

        if ( i >= 5 )
        {
            for ( int m = j, c = 0; m < 5; m++, c++ )
            {
                arr3[ k + c ] = arr2[ m ];
            }

        }
        else
        {
            for ( int m = i, c = 0; m < 5; m++, c++ )
                arr3[ k + c ] = arr1[ m ];
        }

}
