#include <iostream>
#include <cstdlib>
using namespace std ;
void bubble_sort(int * , int ) ; //氣泡排序
void print(int * , int ) ;       //輸出
void Merge(int *,int *,int *,int );//合併
int main()
{
    int arrayA[5]= {10,8,6,4,2} ;
    int arrayB[5]= {9,7,5,3,1} ;
    int arrayC[10]= {} ;
    cout << "Before sort" << endl ;
    cout << "array1 elements: " ;
    print(arrayA,5) ;
    cout << "array2 elements: " ;
    print(arrayB,5) ;
    //排序前
    cout << "After sort" << endl ;
    bubble_sort(arrayA,5) ;
    bubble_sort(arrayB,5) ;
    cout << "array1 elements: " ;
    print(arrayA,5) ;
    cout << "array2 elements: " ;
    print(arrayB,5) ;
    //排序後
    cout << "After merging two arrays" << endl ;
    Merge(arrayA,arrayB,arrayC,10) ; //合併
    cout << "array3 elements: " ;
    print(arrayC,10) ;
    //合併後
    return 0 ;
}
void bubble_sort(int *arr , int Size)
{
    for (int i=Size ; i>0 ; i--)
        for(int j=0 ; j<i-1 ; j++)
            if(*(arr+j)>*(arr+j+1))
                swap(*(arr+j),*(arr+j+1)) ;
}
void print(int *arr , int Size)
{
    for(int i=0 ; i< Size ; i++)
        cout << *(arr+i) << " " ;
    cout << endl ;
}
void Merge(int *arr1,int *arr2,int *arr3,int Size)
{
    for(int i=0 ; i<(Size/2) ; i++) //較小的放前面 大的放後面
        if(*(arr1+i)<*(arr2+i))
        {
            *(arr3+2*i)=*(arr1+i);
            *(arr3+1+2*i)=*(arr2+i) ;
        }
        else
        {
            *(arr3+2*i)=*(arr2+i) ;
            *(arr3+1+2*i)=*(arr1+i) ;
        }


}
