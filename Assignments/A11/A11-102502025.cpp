#include <iostream>
using namespace std;
void bubblesort(int*,int);      //泡沫排序法function
void Merge(int*,int*,int*,int);     //c集合=b集合+a集合，並泡沫排序function
int main()
{
    int a[5]={10,8,6,4,2};      //a集合
    int b[5]={9,7,5,3,1};       //b集合
    int c[10];                  //c集合
    int Size=5;                 //範圍大小

    cout << "Before sort" << endl;
    cout << "array1 elements: ";
    for(int k=0;k<Size;k++)     //顯示a集合
    {
        cout << a[k] << " ";
    }
    cout << endl;
    cout << "array2 elements: ";
    for(int l=0;l<Size;l++)     //顯示b集合
    {
        cout << b[l] << " ";
    }
    cout << endl;

    bubblesort(a,Size);     //進行bubblesort function
    bubblesort(b,Size);

    cout << "After sort" << endl;
    cout << "array1 elements: ";
    for(int k=0;k<Size;k++)     //顯示bubblesort function後集合
    {
        cout << a[k] << " ";
    }
    cout << endl;
    cout << "array2 elements: ";
    for(int l=0;l<Size;l++)
    {
        cout << b[l] << " ";
    }
    cout << endl;

    Merge(a,b,c,Size);      //進行c集合=b集合+a集合，並泡沫排序function

    cout << "After merging two arrays" << endl;
    cout << "array3 elements: ";
    for(int i=0;i<Size*2;i++)       //顯示出c集合
    {
        cout << c[i] << " ";
    }
}

void bubblesort (int* a,int Size)       //泡沫排序法function運算
{
    for(int m=Size;m>0;m--)
    {
        for(int n=0;n<Size-1;n++)
        {
            if(a[n]>a[n+1])
            {
                swap(a[n],a[n+1]);
            }
        }
    }
}

void Merge(int* a,int* b,int* c,int Size)       //c集合=b集合+a集合，並泡沫排序function運算
{
    for(int j=0;j<Size*2;j++)
    {
        if(j<Size)
        {
            for(int h=0;h<Size;h++)
            {
                c[h]=a[h];
            }
        }
        else if(j>=Size && j<Size*2)
        {
            int f=0;
            for(int z=5;z<Size*2;z++)
            {
                c[z]=b[f];
                f++;
            }
        }
    }
    for(int m=Size*2;m>0;m--)
    {
        for(int n=0;n<(Size*2)-1;n++)
        {
            if(c[n]>c[n+1])
            {
                swap(c[n],c[n+1]);
            }
        }
    }
}
