#include <iostream>
using namespace std;
void bubblesort(int* array, int size){
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size - i - 1; j++){
			if (*(array + j) >*(array + j + 1))
				swap(*(array + j), *(array + j + 1));
		}
	}
}
void print(int *S, int size){
	for (int i = 0; i < size; i++)
		cout << " " << *(S + i);
	cout << endl;
}
void Merge(int *A, int *B, int *C, int size){
	int ptrA = 0, ptrB = 0, ptrC = 0;
	while (ptrA < size&&ptrB < size){
		if (*(A + ptrA)>*(B + ptrB))
			*(C + ptrC) = *(B + ptrB), ptrB++, ptrC++;
		else
			*(C + ptrC) = *(A + ptrA), ptrA++, ptrC++;
	}
	while (ptrA<size)
		*(C + ptrC) = *(A + ptrA), ptrA++, ptrC++;
	while (ptrB<size)
		*(C + ptrC) = *(B + ptrB), ptrB++, ptrC++;
}
int main(){
	int A[5] = { 10, 8, 6, 4, 2 }; // size = 5
	int B[5] = { 9, 7, 5, 3, 1 }; // size = 5
	int C[10]; // size = 5 + 5
	cout << "Before sort" << endl;
	cout << "array1 elements :";
	print(A, 5);
	cout << "array2 elements :";
	print(B, 5);
	cout << "After sort" << endl;
	bubblesort(A, 5);
	bubblesort(B, 5);
	cout << "array1 elements :";
	print(A, 5);
	cout << "array2 elements :";
	print(B, 5);
	Merge(A, B, C, 5);
	cout << "After merging two arrays" << endl;
	cout << "array3 elements :";
	print(C, 5 + 5);
	return 0;
}