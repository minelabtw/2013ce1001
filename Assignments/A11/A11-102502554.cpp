#include <iostream>
using namespace std;

void bubblesort (int* , int);
void Merge (int* , int* , int* , int);

int main ()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];//宣告3陣列，並於陣列A、B中輸入初值
    int *array1=arrayA;
    int *array2=arrayB;//pointer

    cout << "Before sort" << endl;
    cout << "array1 elements: ";
    for (int i = 0 ; i < 5 ; i ++)
    {
        cout << arrayA[i] << " ";
    }
    cout << endl;
    cout << "array2 elements: ";
    for (int i = 0 ; i < 5 ; i ++)
    {
        cout << arrayB[i] << " ";
    }
    cout << endl;//輸出未經排序之兩陣列

    cout << "After sort" << endl;
    bubblesort (array1 , 5);//排序A
    cout << "array1 elements: ";
    for (int i = 0 ; i < 5 ; i ++)
    {
        cout << arrayA[i] << " ";
    }
    cout << endl;
    bubblesort (array2 , 5);//排序B
    cout << "array2 elements: ";
    for (int i = 0 ; i < 5 ; i ++)
    {
        cout << arrayB[i] << " ";
    }
    cout << endl;//輸出經排序後的兩陣列

    cout << "After merging two arrays" << endl;
    Merge (arrayA , arrayB , arrayC , 5);
    for (int i = 0 ; i < 10 ; i ++)
    {
        cout << arrayC[i] << " ";
    }
    cout << endl;//將兩陣列合併為一陣列並排序大小

    return 0;
}

void bubblesort (int* array , int _size)
{
    int temp;
    for(int i = _size ; i > 0 ; i--)
    {
        for(int j = 0 ; j < _size-1 ; j++)
        {
            if (array[j] > array[j+1]) //若前一個數字比後面數字大則交換
            {
                swap(array[j],array[j+1]);//交換
            }
        }
    }
}//由小到大排序

void Merge (int* arr1 , int* arr2 , int* arr3 , int _size)
{
    for(int i = 0 ; i < _size ; i++)
    {
        if (arr1[i] > arr2 [i])
        {
            arr3[2*i] = arr2[i];
            arr3[2*i+1] = arr1[i];
        }
        else if (arr1[i] < arr2 [i])
        {
            arr3[2*i] = arr1[i];
            arr3[2*i+1] = arr2[i];
        }
    }
    bubblesort (arr3 , 10);
}//由兩個陣列的開始位置0兩兩比較，比較小的放入arrayC中並將之排序
