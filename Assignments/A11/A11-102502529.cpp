#include <iostream>
using namespace std;

void bubble(int *,int );
void store (int *,int*);
void Merge(int *arr1,int *arr2,int *arr3,int arraysize);                //function prototype

int main()
{
    int a1[5]= {10,8,6,4,2};
    int a2[5]= {9,7,5,3,1};
    int a3[10]= {};
    int *arr1,*arr2,*arr3 ;                                            //宣告pointer
    arr1=a1 ;                                                          //儲存array address
    arr2=a2;
    cout<<"Before sort"<<endl;
    store(arr1,arr2);                                                   //用來輸出 array1 array2 的輸出
    cout<<endl;
    cout<<"After sort"<<endl;
    bubble(arr1,5);                                                     //進行 排序
    bubble(arr2,5);
    store(arr1,arr2);                                                   //輸出
    cout<<endl;
    cout<<"After merging two arrays"<<endl;
    arr3=a3;                                                            //儲存a3 address
    Merge(arr1,arr2,arr3,10);                                           //將 a1 a2 合併 按照大小排序
    cout<<"array3 elements: ";
    for(int i=0; i<10; i++)                                             //輸出排列後的a3
    {
        cout<<*(arr3+i)<<" ";
    }

}

void bubble(int *a,int arrsize)
{
    int counter=1;                                          //控制迴圈
    while(counter)
    {
        counter=0;                                          //先歸零
        for(int i=0; i<arrsize-1; i++)
        {
            if(a[i]>a[i+1])                                //若排列完畢 counter 值不變 為0
            {
                int t=a[i];
                a[i]=a[i+1];
                a[i+1]=t;
                counter=1;
            }
        }
    }
}
void store (int *arr1,int *arr2)
{
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)                                              //輸出 arr1 address 的值
    {
        cout<<*(arr1+i)<<" ";
    }
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<5; i++)                                              //輸出 arr2 address的值
    {
        cout<<*(arr2+i)<<" ";
    }

}
void Merge(int *arr1,int *arr2,int *arr3,int arraysize)
{
    int m=0;
    int n=0;
    int k=0;
    while(m<5 && n<5)                                           //還沒完成儲存 就跑回圈
    {
        if(*(arr1+m) < *(arr2+n))                               //看誰較小

        {
            *(arr3+k)=*(arr1+m);                                //儲存後 某陣列下一次就會比下一個
            m++;
        }
        else
        {
            *(arr3+k)=*(arr2+n);                                //同上
            n++;
        }

        k++;                                                    //存完後 存下一個
    }
    while(m<5&& k<arraysize)                                    //要控制 arr3 在arraysize內 會友其中一個arr 先被排完
    {
        *(arr3+k)=*(arr1+m);
        k++;                                                    //存下一個
        m++;
    }

    while(n<5&& k<arraysize)                                    //要控制 arr3 在arraysize內  會友其中一個arr 先被排完
    {
        *(arr3+k)=*(arr2+n);
        k++;                                                    //存下一個
        n++;
    }

}
