#include<iostream>
using namespace std;
void bubblesort(int* xarray , int xsize)//比較一個陣列大小
{
    int save = 0;
    for(int i=0; i<xsize; i++)
    {
        for(int j=0; j<xsize-1; j++)
        {
            if(*(xarray+j)>*(xarray+j+1))
            {
                save=*(xarray+j);
                *(xarray+j)=*(xarray+j+1);
                *(xarray+j+1)= save;
            }
        }
    }
}
void print(int* xarray , int xsize)//印出陣列
{
    for(int k=0; k<xsize; k++)
    {
        cout << *(xarray+k) << " ";
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int xsize)//比較兩個陣列大小
{
    int order = 0;
    for(int i=0; i<5; i++)
    {
        for(int k=0; k<5; k++)
        {
            if(*(arr1+k)>*(arr2+i))
            {
                *(arr3+order)=*(arr2+i);
                order++;
                k=5;
            }
        }
        for(int j=0; j<5; j++)
        {
            if(*(arr1+i)<*(arr2+j))
            {
                *(arr3+order)=*(arr1+i);
                order++;
                j=5;
            }
            else
                *(arr3+order)=*(arr1+i);//儲存最大值
        }

    }
}
int main()
{
    int arrayA[5] = {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10] = {};
    int *arr;
    arr=arrayA;
    cout << "Before sort" << endl << "array1 elements: ";
    print(arrayA,5);
    cout << endl << "array2 elements: " ;
    print(arrayB,5);
    cout << endl << "After sort" << endl << "array1 elements: ";
    bubblesort(arrayA,5);
    print(arrayA,5);
    cout << endl << "array2 elements: " ;
    bubblesort(arrayB,5);
    print(arrayB,5);
    cout << endl << "After merging two arrays" << endl << "array3 elements: ";
    Merge(arrayA, arrayB, arrayC,10);
    print(arrayC,10);
    return 0;

}
