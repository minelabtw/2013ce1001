#include <iostream>
using namespace std;

void bubblesort(int* a,int l);                                  //宣告函式
void Merge(int *a,int *b,int *c,int m);

int main()
{
    int A[5] = {10,8,6,4,2};
    int B[5] = {9,7,5,3,1};
    int C[10];
    int *a;                                                     //各陣列的的指標皆以小寫表示
    int *b;
    int *c;
    int l = 5;
    int m = 10;

    cout << "Before sort" << endl;
    a=A;
    cout << "array1 elements:";                                 //輸出整理前的陣列1
    for(int i=0; i<l; i++)
    {
        cout << " " << *(a+i);
    }
    cout << endl;
    a=B;
    cout << "array2 elements:";                                 //輸出整理前的陣列2
    for(int i=0; i<l; i++)
    {
        cout << " " << *(a+i);
    }
    cout << endl;

    cout << "After sort" << endl;
    a=A;
    bubblesort(a,l);
    cout << "array1 elements:";                                 //輸出整理後的陣列1
    for(int i=0; i<l; i++)
    {
        cout << " " << *(a+i);
    }
    cout << endl;
    a=B;
    bubblesort(a,l);
    cout << "array2 elements:";                                 //輸出整理後的陣列2
    for(int i=0; i<l; i++)
    {
        cout << " " << *(a+i);
    }
    cout << endl;

    cout << "After merging two arrays" << endl;
    a=A;
    b=B;
    c=C;
    Merge(a,b,c,m);
    cout << "array3 elements:";                                 //輸出統合之後的陣列3
    for(int i=0; i<m; i++)
    {
        cout << " " << *(c+i);
    }
    cout << endl;

    return 0;
}

void bubblesort(int* a,int l)                                   //函式－泡泡排序
{
    for (int j=1; j<=l; j++)
    {
        for (int i=0; i<l-1; i++)
        {
            int temp = 0;
            if (*(a+i)>*(a+i+1))
            {
                temp = *(a+i);
                *(a+i) = *(a+i+1);
                *(a+i+1) = temp;
            }

        }
    }
}

void Merge(int *a,int *b,int *c,int m)                          //兩函式比較，較小的放入新函式
{
    int x = 0;
    int y = 0;
    for(int i=0; i<m; i++)
    {
        if (y>4)                                                //因為b陣列的數會先輸入完，所以判斷b如果輸入完了，直接把a的最後一個數放入
            *(c+i)=*(a+x);
        else if(*(a+x)>*(b+y))
        {
            *(c+i)=*(b+y);
            y++;
        }
        else if(*(a+x)<*(b+y))
        {
            *(c+i)=*(a+x);
            x++;
        }
    }
}
