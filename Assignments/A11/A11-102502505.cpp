#include <iostream>
using namespace std;
int exchange1;//定義交換所需的兩個數
int exchange2;
int bubblesort(int [],int);//定義排列大小的函數
int Merge(int *,int *,int *,int);//定義混和兩陣列的函式

int main()
{
    int arrayA[5]= {10,8,6,4,2};//定義陣列A
    int arrayB[5]= {9,7,5,3,1};//定義陣列B
    int arrayC[10];//定義陣列C

    cout << "Before sort" << endl;//輸出下列字串
    cout << "array1 elements: ";
    for(int i=0; i<5; i++)//用迴圈使得陣列A輸出
    {
        cout << *(arrayA+i) << " ";//以指標方式表示
    }
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout << *(arrayB+i) << " ";
    }
    cout << endl;

    bubblesort(arrayA,5);//呼叫函式
    bubblesort(arrayB,5);

    cout << "After sort" << endl;//輸出字串
    cout << "array1 elements: ";
    for(int i=0; i<5; i++)//再輸出一次陣列
    {
        cout << *(arrayA+i) << " ";//以指標方式表示
    }
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout << *(arrayB+i) << " ";
    }

    Merge(arrayA,arrayB,arrayC,10);//呼叫函式，使兩陣列元素輸入陣列C
    cout << endl << "After merging two arrays" << endl;//輸出字串
    cout << "array3 elements: ";
    for(int i=0; i<10; i++)//輸出陣列C
    {
        cout << *(arrayC+i) << " ";//以指標方式表示輸出
    }

    return 0;//返回初始值

}

int bubblesort(int x[],int _size)//定義排列大小函示
{
    for (int l=_size; l>0; l--)//定義第一個迴圈
    {
        for (int m=0; m<=l-2; m++)//定義第二個迴圈
        {
            if (x[m]>x[m+1])//當前一個數大於後一個數時，互換
            {
                exchange1=x[m];
                exchange2=x[m+1];
                x[m]=exchange2;
                x[m+1]=exchange1;
            }
        }
    }
}

int Merge(int *arr1,int *arr2,int *arr3,int _size)//定義三個指標
{
    int i=0;//定義兩函數初始值為零
    int j=0;

    while (i<10 && j<5)//符合條件時在此迴圈中
    {
        if (*(arr1+j)>*(arr2+j))//假如符合此條件
        {
            *(arr3+i)=*(arr2+j);//先將小的值(陣列2值)傳入陣列C
            i++;
            *(arr3+i)=*(arr1+j);//再傳另一個值
            i++;
            j++;
        }
        else//假如不符合上述條件，則跑下列指令
        {
            *(arr3+i)=*(arr1+j);//先將小的值(陣列1值)傳入陣列C
            i++;
            *(arr3+i)=*(arr2+j);//再傳另一個值
            i++;
            j++;
        }
    }
}






