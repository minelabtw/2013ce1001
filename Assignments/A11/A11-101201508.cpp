#include<iostream>

using std::cout;
using std::cin;
using std::endl;
void Print(int arr[],int _size)           //列印出來
{
    for (int i=0; i<_size; ++i)
    {
        cout << arr[i] << " " ;
    }
    cout << endl ;
}
void bubblesort(int *a_ptr , int _size)   //氣泡排序
{
    int temp ;
    for (int i=_size-1; i>=0; --i)
    {
        for (int j=0; j<i; j++)
        {
            if ((*a_ptr+j)>*(a_ptr+j+1))
            {
                temp=*(a_ptr+j) ;
                *(a_ptr+j)=*(a_ptr+j+1) ;
                *(a_ptr+j+1)=temp ;
            }
        }
    }
}
void Merge(int *ptr1,int *ptr2,int *ptr3,int _size)//小的先放
{
    int *fin =ptr3+_size ;
    int *fin1=ptr1+5 ;
    int *fin2=ptr2+5 ;
    do
    {
        if (*ptr1>*ptr2 && ptr2!=fin2)
        {
            *ptr3=*ptr2 ;
            ptr2++ ;
        }
        else if (ptr1!=fin1)
        {
            *ptr3=*ptr1 ;
            ptr1++ ;
        }
        ptr3++ ;
    }
    while (ptr3!=fin);
}
int main()
{
    int arr1[5]= {1,2,3,4,5} ;
    int arr2[6]= {6,7,8,9,10,11} ;
    int arr3[10]= {0} ;

    cout << "Before sort" << endl ;
    cout << "array1 elements: " ;
    Print(arr1,5) ;
    cout << "array2 elements: " ;
    Print(arr2,6) ;
    bubblesort(arr1,5) ;
    bubblesort(arr2,6) ;
    cout << "After sort" << endl ;
    cout << "array1 elements: " ;
    Print(arr1,5) ;
    cout << "array2 elements: " ;
    Print(arr2,6) ;
    Merge(arr1,arr2,arr3,11) ;
    cout << "After merging two arrays" << endl ;
    cout << "array3 elements: " ;
    Print(arr3,11) ;
    return 0;
}


