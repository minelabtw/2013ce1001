#include <iostream>
using namespace std;

int arrayA[]= {10,8,6,4,2};
int arrayB[]= {9,7,5,3,1};
int arrayC[10];

void Merge(int *arr1,int *arr2,int *arr3,int size)
{

    int arr1Count = 0, arr2Count = 0, arr3Count = 0;

    do
    {
        if ( *(arr1+arr1Count) < *(arr2+arr2Count) )
        {
            *(arr3+arr3Count) = *(arr1+arr1Count);
            arr1Count++;
            arr3Count++;
        }
        else
        {
            *(arr3+arr3Count) = *(arr2+arr2Count);
            arr2Count++;
            arr3Count++;
        }

        if ( arr1Count == size )
        {
            for ( ; arr2Count < size; arr2Count++ )
            {
                *(arr3+arr3Count) = *(arr2+arr2Count);
                arr3Count++;
            }
        }
        else if ( arr2Count == size )
        {
            for ( ; arr1Count < size; arr1Count++ )
            {
                *(arr3+arr3Count) = *(arr1+arr1Count);
                arr3Count++;
            }
        }

    }
    while( arr3Count < 2*size );
}

void bubblesort(int *array , int size)
{
    for (int i=0 ; i<size ; i++ )
    {
        for(int j=0 ; j<size-1 ; j++ )
            while ( *(array+j) > *(array +j+1) )
            {
                int x;
                x= * (array+j+1);
                *(array+j+1)=*(array+j);
                *(array+j)=x;
            }
    }
}

void out (int arr[],int size)
{
    for(int j=0 ; j<size ; j++ )
    {
        cout << *(arr+j)<<" " ;
        if (j==size-1)
            cout<<endl;
    }
}

int main()
{
    cout <<"Before sort"<<endl ;
    cout <<"arrayA elements: ";
    out (arrayA,5);
    cout <<"arrayB elements: ";
    out (arrayB,5);

    cout <<"After sort"<<endl;
    bubblesort(arrayA , 5);
    bubblesort(arrayB , 5);
    cout <<"arrayA elements: ";
    out (arrayA,5);
    cout <<"arrayB elements: ";
    out (arrayB,5);

    cout <<"After merging two arrays" <<endl;
    Merge(arrayA ,arrayB ,arrayC , 5);
    cout <<"array3 elements: ";
    out (arrayC,10);

    return 0 ;
}
