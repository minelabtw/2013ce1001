#include <iostream>

using namespace std;
void bubblesort(int* array , int size)
{
    int change;
    for (int h=0; h<size-1; h++)
    {
        for (int j=0; j<size-1; j++)
        {
            if (*(array+j)>*(array+j+1))
            {
                change=*(array+j);
                *(array+j)=*(array+j+1);
                *(array+j+1)=change;
            }
        }
    }
}

void merge(int *arr1,int *arr2,int *arr3,int size)
{
    int a=0;
    int b=0;
    for(int l=0; l<size-1; l++)//共10個數比9次
    {
        if(*(arr1+a)<*(arr2+b))
        {
            *(arr3+l)=*(arr1+a);
            a++;
            if(a==5)//因為會超出陣列,故加上這個條件
                *(arr3+l+1)=*(arr2+b);//把剩餘的用上去
        }
        else
        {
            *(arr3+l)=*(arr2+b);
            b++;
            if(b==5)
                *(arr3+l+1)=*(arr1+a);
        }
    }
}
int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10]= {};
    int *arr;
    arr=arrayA;
    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl<<"array2 elements: ";
    arr=arrayB;
    for(int i=0; i<5; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl<<"After sort"<<endl;
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);
    cout<<"array1 elements: ";
    arr=arrayA;
    for(int i=0; i<5; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl<<"array2 elements: ";
    arr=arrayB;
    for(int i=0; i<5; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    merge(arrayA,arrayB,arrayC,10);
    cout<<endl<<"After merging two arrays"<<endl;
    cout<<"array3 elements: ";
    arr=arrayC;
    for(int i=0; i<10; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    return 0;
}
