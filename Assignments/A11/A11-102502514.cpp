#include <iostream>
using namespace std;

void bubblesort(int* _array , int _size);  //宣告bubblesort函式
void Merge(int *arr1,int *arr2,int *arr3,int _size);  //宣告Merge函式

int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];  //宣告一個陣列提供merge函式存取元素

    cout <<"Before sort"<<endl;
    cout <<"array1 elements: ";
    for (int i=0; i<5; i++)
        cout <<arrayA[i]<<" ";
    cout <<endl;
    cout <<"array2 elements: ";
    for (int j=0; j<5; j++)
        cout <<arrayB[j]<<" ";
    cout <<endl;

    bubblesort(arrayA,5);  //呼叫函式

    cout <<"After sort"<<endl;
    cout <<"array1 elements: ";
    for (int k=0; k<5; k++)
        cout <<arrayA[k]<<" ";
    cout <<endl;

    bubblesort(arrayB,5);  //呼叫函式

    cout <<"array2 elements: ";
    for (int l=0; l<5; l++)
        cout <<arrayB[l]<<" ";
    cout <<endl;

    Merge(arrayA,arrayB,arrayC,10);  //呼叫函式

    cout <<"After merging two arrays"<<endl;
    cout <<"array3 elements: ";
    for (int m=0; m<10; m++)
        cout <<arrayC[m]<<" ";

    return 0;
}

void bubblesort(int* _array , int _size)
{
    int hold;  //宣告一個變數hold暫存陣列
    for (int i=0; i<_size-1; i++)
    {
        for (int j=0; j<_size-1; j++)
        {
            if (_array[j]>_array[j+1])
            {
                hold=_array[j+1];
                _array[j+1]=_array[j];
                _array[j]=hold;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int _size)
{
    int i=0;
    int j=0;
    for (int k=0; k<_size; k++)
    {
        if (i==5)  //當i==5時迴圈不能繼續執行下去,因為i最大也只能4,所以執行到i==5時陣列arr3的最大元素就確定是arr2[4]
            arr3[k]=arr2[j];
        else if (j==5)  //反過來,當j==5時迴圈不能繼續執行下去,因為j最大也只能4,所以執行到j==5時陣列arr3的最大元素就確定是arr1[4]
            arr3[k]=arr1[i];
        else if (arr1[i]>arr2[j])
        {
            arr3[k]=arr2[j];
            j++;
        }
        else if (arr1[i]<arr2[j])
        {
            arr3[k]=arr1[i];
            i++;
        }
    }
}
