#include <iostream>
using namespace std;

void bubblesort(int* arr , int _size);
void Merge(int *arr1,int *arr2,int *arr3,int _size);

int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10]= {};

    cout << "Before sort" << endl;
    cout << "array1 elements: ";
    for(int i=0; i<5; ++i)
        cout << arrayA[i] << " ";
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; ++i)
        cout << arrayB[i] << " ";

    bubblesort(arrayA,5);    //呼叫函式
    bubblesort(arrayB,5);    //呼叫函式

    cout << endl;
    cout << "After sort" << endl;
    cout << "array1 elements: ";
    for(int i=0; i<5; ++i)
        cout << arrayA[i] << " ";
    cout << endl << "array2 elements: ";
    for(int i=0; i<5; ++i)
        cout << arrayB[i] << " ";

    Merge(arrayA,arrayB,arrayC,10);

    cout << endl;
    cout << "After merging two arrays" << endl;
    cout << "array3 elements: ";
    for(int i=0; i<10; ++i)
        cout << arrayC[i] << " ";

    return 0;
}

void bubblesort(int* arr , int _size)    //排序
{
    for(int i=0; i<_size; ++i)
    {
        for(int j=0; j<_size-1; ++j)
        {
            if(arr[j]>arr[j+1])
            {
                int x=arr[j+1];
                arr[j+1]=arr[j];
                arr[j]=x;
            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int _size)    //將2個陣列的參數重新排入一陣列
{
    int x=0;
    for(int i=0; i<_size; ++i)
    {
        if(arr1[i]<arr2[i])
        {
            arr3[x]=arr1[i];
            arr3[x+1]=arr2[i];
        }
        else if(arr1[i]>arr2[i])
        {
            arr3[x]=arr2[i];
            arr3[x+1]=arr1[i];
        }
        x+=2;
    }
}
