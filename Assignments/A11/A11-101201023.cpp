#include<iostream>
#include<iomanip>

using namespace std;

void bubblesort(int* array , int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

void bubblesort(int *array , int size)
{
    int *arr=array;

    for(int j=0 ; j<size-1 ;j++)                             //將陣列從第一個比較，比較"size-1"次
    {
        int n=0;

        while(n<size-1)
        {
            if(*(arr+n)>*(arr+n+1))
            {
                swap(*(arr+n),*(arr+n+1));                   //交換兩個值
            }
            n++;
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int n=0;
    int m=0;
    int *a1=arr1;
    int *a2=arr2;
    int *a3=arr3;

    for(int i=0 ; i<size ; i++)
    {
        while(n==5)                                 //當arr1的值排完，將arr2的值按順序排完
        {
            for(int j=i ; j<size ; j++)
            {
                *(a3+j)=*(a2+m);
                m++;
            }
            i=size;
            break;
        }


        while(m==5)                                //當arr2的值排完，將arr1的值按順序排完
        {
            for(int j=i ; j<size ; j++)
            {
                *(a3+j)=*(a1+n);
                n++;
            }
            i=size;
            break;
        }
        if(i == size)                              //脫離for迴圈
            break;

        if(*(a1+n)<*(a2+m))                        //將較小的值放進第三個陣列裡
        {
            *(a3+i)=*(a1+n);
            n++;
        }

        else
        {
            *(a3+i)=*(a2+m);
            m++;
        }
    }
}

int main()
{
    int *arr;
    int arrayA[5]={10,8,6,4,2};
    int arrayB[5]={9,7,5,3,1};
    int arrayC[10];

    cout << "Before sort" << endl;
    cout << "array1 elements:";
    arr=arrayA;
    for(int i=0 ; i<5 ; i++)                         //將arrayA內的值依序輸出
    {
        cout << " " << *(arr+i);
    }
    cout << endl;
    cout << "array2 elements:";
    arr=arrayB;
    for(int i=0 ; i<5 ; i++)                         //將array0B內的值依序輸出
    {
        cout << " " << *(arr+i);
    }
    cout << endl;

    bubblesort(arrayA,5);                            //將arrayA裡的值由小到大重新排列                            //
    bubblesort(arrayB,5);                            //將arrayB裡的值由小到大重新排列

    cout << "After sort" << endl;
    cout << "array1 elements:";
    arr=arrayA;
    for(int i=0 ; i<5 ; i++)
    {
        cout << " " << *(arr+i);
    }
    cout << endl;
    cout << "array2 elements:";
    arr=arrayB;
    for(int i=0 ; i<5 ; i++)
    {
        cout << " " << *(arr+i);
    }
    cout << endl;

    Merge(arrayA,arrayB,arrayC,10);                              //將兩陣列的值由小到大存放進第三個陣列

    cout << "After merging two arrays" << endl;
    cout << "array3 elements:";
    arr=arrayC;
    for(int i=0 ; i<10 ; i++)
    {
        cout << " " << *(arr+i);
    }

    return 0;
}
