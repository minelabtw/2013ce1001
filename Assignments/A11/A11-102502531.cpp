#include <iostream>

using namespace std;

void bubblesort_1(int* array , int size);
void bubblesort_2(int* array , int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

int main()
{
    int array_1[5]= {10,8,6,4,2};
    int array_2[5]= {9,7,5,3,1};
    int array_3[10];
    int _size1=5;
    int _size2=10;
    int* arr1;
    int* arr2;
    int* arr3;

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for (int i=0; i<5; i++)
    {
        cout<<array_1[i]<<" ";
    }
    cout<<endl;

    cout<<"array2 elements: ";
    for (int i=0; i<5; i++)
    {
        cout<<array_2[i]<<" ";
    }
    cout<<endl;

    cout << "After sort"<<endl;
    bubblesort_1(array_1 , _size1);
    bubblesort_2(array_2 , _size1);

    arr1=array_1;
    arr2=array_2;
    arr3=array_3;

    cout << "After merging two arrays"<<endl;
    Merge(arr1,arr2,arr3,_size2);


}

void bubblesort_1(int* array , int size)
{
    int i;
    int j;
    int temp;

    for(i=size-1 ; i>0; i--)
    {
        for(j=0; j<size-1; j++)
        {
            if (array[j]>array[j+1])
            {
                temp = array[j];
                array[j]=array[j+1];
                array[j+1]=temp;
            }
        }
    }
    cout<<"array1 elements: ";
    for (int i=0; i<5; i++)
    {
        cout<<array[i]<<" ";
    }
    cout<<endl;
}

void bubblesort_2(int* array , int size)
{
    int i;
    int j;
    int temp;

    for(i=size-1 ; i>0; i--)
    {
        for(j=0; j<size-1; j++)
        {
            if (array[j]>array[j+1])
            {
                temp = array[j];
                array[j]=array[j+1];
                array[j+1]=temp;
            }
        }
    }
    cout<<"array2 elements: ";
    for (int i=0; i<5; i++)
    {
        cout<<array[i]<<" ";
    }
    cout<<endl;
}
void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int i;
    int j=0;

    for(i=0 ; i<5; i++)
    {
        if( arr1[i] > arr2[i] )
        {
            arr3[j]=arr2[i];
            arr3[j+1]=arr1[i];
            j=j+2;
        }
        if( arr1[i] < arr2[i] )
        {
            arr3[j]=arr1[i];
            arr3[j+1]=arr2[i];
            j=j+2;
        }
    }
    cout<<"array3 elements: ";
    for (int i=0; i<size; i++)
    {
        cout<<arr3[i]<<" ";
    }
    cout<<endl;

}
