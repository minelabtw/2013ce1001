#include<iostream>
using namespace std;

void bubblesort(int* Array , int Size);//排序
void Merge(int *arr1,int *arr2,int *arr3,int Size);//整合排序

int main()
{
    int Size=5;//arrayA、arrayB的size
    int SizeC=10;//arrayC的size
    int *arrA;
    int *arrB;
    int *arrC;
    int ArrayA[5]= {10,8,6,4,2};
    int ArrayB[5]= {9,7,5,3,1};
    int ArrayC[10]= {};

    arrA=ArrayA;
    arrB=ArrayB;
    arrC=ArrayC;

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<Size; i++)//輸出array1未排列前結果
    {
        cout<<*(arrA+i)<<" ";
    }
    cout<<endl<<"array2 elements: ";
    for(int i=0; i<Size; i++)//輸出array2未排列前結果
    {
        cout<<*(arrB+i)<<" ";
    }
    cout<<endl<<"After sort"<<endl;
    bubblesort(arrA , Size);//排列array1
    cout<<"array1 elements: " ;
    for(int i=0; i<Size; i++)//輸出array1排列結果
    {
        cout<<*(arrA+i)<<" ";
    }
    cout<<endl<<"array2 elements: ";
    bubblesort(arrB , Size);//排列array2
    for(int i=0; i<Size; i++)//輸出array2排列結果
    {
        cout<<*(arrB+i)<<" ";
    }
    cout<<endl<<"After merging two arrays"<<endl;
    Merge(arrA,arrB,arrC,SizeC);//整合array1、array2，排序
    cout<<"array3 elements: ";
    for(int i=0; i<SizeC; i++)//輸出整合排序後結果
    {
        cout<<*(arrC+i)<<" ";
    }
    return 0;
}
void bubblesort(int* Array , int Size)//排序用
{
    int Insert;
    for(int i=1; i<Size; i++)
    {
        Insert=*(Array+i);
        int j=i;
        while(j>0&&*(Array+(j-1))>Insert)
        {
            *(Array+j)=*(Array+(j-1));
            j--;
        }
        *(Array+j)=Insert;
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int Size)//整合排序用
{
    int siteA=0;//存到arr1的位置
    int siteB=0;//存到arr2的位置
    int Count=0;//存到arr3的位置

    while(Count<Size)
    {
        if(*(arr2+siteB)>*(arr1+siteA)&&siteA<4)//比較前幾項，小的存到arr3
        {
            *(arr3+Count)=*(arr1+siteA);
            siteA++;
        }
        else if(*(arr2+siteB)<*(arr1+siteA)&&siteB<4)
        {
            *(arr3+Count)=*(arr2+siteB);
            siteB++;
        }
        else if(siteA==4&&siteB==4)//比較最後兩項
        {
            if(*(arr2+siteB)<*(arr1+siteA))
            {
                *(arr3+Count)=*(arr2+siteB);
                *(arr3+Count+1)=*(arr1+siteA);
            }

            else if(*(arr2+siteB)>*(arr1+siteA))
            {
                *(arr3+Count)=*(arr1+siteA);
                *(arr3+Count+1)=*(arr2+siteB);
            }
            Count++;
        }
        Count++;
    }
}
