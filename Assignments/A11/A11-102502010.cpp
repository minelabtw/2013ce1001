#include <iostream>
using namespace std;

void bubblesort(int* arr , int _size)  //泡沫排序法
{
    int i,j,*arrbubble;
    arrbubble=arr;
    for(i=0; i<_size; i++)
        for(j=i+1; j<_size; j++)
            if(*(arrbubble+i)>*(arrbubble+j))
                swap(*(arrbubble+i),*(arrbubble+j));
}

void Merge(int *arr1,int *arr2,int *arr3,int _size)  //合併
{
    int x=0,y=0,i=0,*arrA,*arrB,*arrC;
    arrA=arr1;
    arrB=arr2;
    arrC=arr3;
    while(x<_size && y<_size)  //將排序的數字放入陣列C
    {
        if(*(arrA+x)<*(arrB+y))
        {
            *(arrC+i)=*(arrA+x);
            i++;
            x++;
        }
        else
        {
            *(arrC+i)=*(arrB+y);
            i++;
            y++;
        }
    }
    while(x<_size)  //將剩餘的數字放入
    {
        *(arrC+i)=*(arrA+x);
        i++;
        x++;
    }
    while(y<_size)  //將剩餘的數字放入
    {
        *(arrC+i)=*(arrB+y);
        i++;
        y++;
    }
}

int main()
{
    int i,*arr;  //指標
    int arrayA[5]= {10,8,6,4,2},arrayB[5]= {9,7,5,3,1},arrayC[10]; //陣列ABC
    cout<<"Before sort"<<endl;
    cout<<"array1 elements:";
    arr=arrayA;
    for(i=0; i<5; i++)
        cout<<' '<<*(arr+i);
    cout<<endl;
    cout<<"array2 elements:";
    arr=arrayB;
    for(i=0; i<5; i++)
        cout<<' '<<*(arr+i);
    cout<<endl;
    bubblesort(arrayA,5);  //氣泡排序
    bubblesort(arrayB,5);  //氣泡排序
    cout<<"After sort"<<endl;
    cout<<"array1 elements:";
    arr=arrayA;
    for(i=0; i<5; i++)
        cout<<' '<<*(arr+i);
    cout<<endl;
    cout<<"array2 elements:";
    arr=arrayB;
    for(i=0; i<5; i++)
        cout<<' '<<*(arr+i);
    cout<<endl;
    Merge(arrayA,arrayB,arrayC,5);  //倆倆合併
    cout<<"After merging two arrays"<<endl;
    cout<<"array3 elements:";
    arr=arrayC;
    for(i=0; i<10; i++)
        cout<<' '<<*(arr+i);
    cout<<endl;
    return 0;
}
