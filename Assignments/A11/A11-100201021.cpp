#include <iostream>
using namespace std;

void bubblesort(int* array , int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

int main()
{
    int arrayA[5]={10,8,6,4,2};
    int arrayB[5]={9,7,5,3,1};
    int arrayC[10];

    /* //example//
    arr=array;
    for(int i=0;i<5;i++)
        cout<<"point "<<i<<":"<<*(arr+i)<<"  array "<<i<<":"<<array[i]<<endl;
    */
    cout << "Before sort" << endl;
    //A before
    cout << "array1 elements: ";
    for(int i=0;i<5;i++)
        cout << " " <<arrayA[i];
    cout << endl;
    //B before
    cout << "array2 elements: ";
    for(int i=0;i<5;i++)
        cout << " " <<arrayB[i];
    cout << endl;
    //
    cout << "After sort" << endl;
    bubblesort(arrayA ,5);
    bubblesort(arrayB ,5);
    //A after
    cout << "array1 elements: ";
    for(int i=0;i<5;i++)
        cout << " " <<arrayA[i];
    cout << endl;
    //B after
    cout << "array2 elements: ";
    for(int i=0;i<5;i++)
        cout << " " <<arrayB[i];
    cout << endl;
    //set C
    cout << "After merging two arrays" << endl;
    Merge(arrayA,arrayB,arrayC,10);
    //output C
    cout << "array3 elements: ";
    for(int i=0;i<10;i++)
        cout << " " <<arrayC[i];
    cout << endl;

    return 0;
}

void bubblesort(int* array , int size)
{
    int t=1099;    //for change

    for(int j=0;j<size;j++)
        for(int i=0;i<size-1;i++)
        {
            if(*(array+i) > *(array+i+1))   //if exist x<y   arr[x]> arr[y];
            {
                //change
                t= (*(array+i));
               *(array+i)= (*(array+i+1));
               *(array+i+1)=t;
            }

        }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int a1=0,a2=0,ss=0; //for step of arr1,arr2,arr3
    while(ss!=size)     //arr3 complete;
    {
        if(a1==5)
        {
            *(arr3+ss)=*(arr2+a2);
            a2++;
        }
        else if(a2==5)
        {
            *(arr3+ss)=*(arr1+a1);
            a1++;
        }
        else if(*(arr1+a1)>*(arr2+a2))
        {
            *(arr3+ss)=*(arr2+a2);
            a2++;
        }
        else
        {
            *(arr3+ss)=*(arr1+a1);
            a1++;
        }
        ss++;
    }
}
