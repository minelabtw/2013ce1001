#include<iostream>
using namespace std ;
void bubblesort(int* _array , int _size) ;
void  Merge(int *arr1,int *arr2,int *arr3,int _size);
int main ()
{

    int arrayA[5]= {10,8,6,4,2} ;
    int arrayB[5]= {9,7,5,3,1} ;
    int arrayC[10] ;
    cout <<"Before sort"<<endl;
    int _size ;
    cout<<"array1 elements: ";
    for(int i=0; i<5; ++i)
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl ;
    cout<<"array2 elements: ";
    for(int i=0; i<5; ++i)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl ;
    cout<<"After sort"<<endl;
    cout<<"array1 elements: ";
    bubblesort( arrayA ,  5);
    for(int i=0; i<5; ++i)
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl ;
    cout<<"array2 elements: ";
    bubblesort( arrayB , 5) ;
    for(int i=0; i<5; ++i )
    {
        cout<<arrayB[i]<<" " ;
    }
    cout<<endl ;
    cout<<"After merging two arrays "<<endl;
    cout<<"array3 elements: ";
    Merge(arrayA, arrayB, arrayC,10);
    for(int i=0; i<10; ++i )
    {
        cout<<arrayC[i]<<" " ;
    }
    return 0 ;
}
void bubblesort(int* _array , int _size)     //把數列由小排到大
{
    int tmp ;
    for(int i =0; i<_size; ++i)
    {
        for(int j=0; j<_size-1; ++j)
        {
            if(_array[j]>_array[j+1])
            {
                tmp=_array[j] ;
                _array[j]=_array[j+1] ;
                _array[j+1]=tmp ;
            }
        }
    }

}
void  Merge(int *arr1,int *arr2,int *arr3,int _size)
{

    for(int i =0 ; i<_size; i=i+2)    //交替放進來
    {
        arr3[i]=arr2[i/2];
        arr3[i+1]=arr1[i/2];
    }

}

