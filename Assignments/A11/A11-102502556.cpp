#include <iostream>

using namespace std;

void Bubblesort ( int * , int ); //function prototype
void Merge ( int * , int * ,int * ,int );

int main ()
{
    int arrayA[5] = {10,8,6,4,2}; //宣告型別為 int 的一維陣列(arrayA)，並初始化其內部元素的值。
    int arrayB[5] = {9,7,5,3,1}; //宣告型別為 int 的一維陣列(arrayB)，並初始化其內部元素的值。
    int arrayC[10] = {}; //宣告型別為 int 的一維陣列(arrayC)，並初始化其內部元素的值為0。
    int _size = 5; // 宣告型別 int 的變數(_size)，用來儲存陣列的大小。
    int *arrA = arrayA; //宣告型別為 int 的指標(arrA)，其值為 arrayA 的記憶體位址。
    int *arrB = arrayB; //宣告型別為 int 的指標(arrB)，其值為 arrayB 的記憶體位址。
    int *arrC = arrayC; //宣告型別為 int 的指標(arrC)，其值為 arrayC 的記憶體位址。
    cout << "Before sort" << endl;
    cout << "array1 elements: ";
    for ( int i = 0 ; i < _size ; i++ ) //輸出還未排序的第一個陣列。
    {
        cout << arrayA[i] << " ";
    }
    cout << endl;
    cout << "array2 elements: ";
    for ( int i = 0 ; i < _size ; i++ ) //輸出還未排序的第二個陣列。
    {
        cout << arrayB[i] << " ";
    }
    Bubblesort( arrA , _size ); //將 arrayA 用泡泡排序法做重新排序。
    Bubblesort( arrB , _size ); //將 arrayB 用泡泡排序法做重新排序。
    cout << endl;
    cout << "After sort" << endl;
    cout << "array1 elements: ";
    for ( int i = 0 ; i < _size ; i++ ) //輸出已排序的第一個陣列。
    {
        cout << arrayA[i] << " ";
    }
    cout << endl;
    cout << "array2 elements: ";
    for ( int i = 0 ; i < _size ; i++ ) //輸出已排序的第二個陣列。
    {
        cout << arrayB[i] << " ";
    }
    cout << endl;
    _size = 0;
    Merge( arrA , arrB , arrC , _size ); //將 arrayA 和 arrayB 用合併排序法重新排序，並儲存於 arrayC 之中。
    _size = 10;
    cout << "After merging two arrays" << endl;
    cout << "array3 elements: ";
    for ( int i = 0 ; i < _size ; i++ ) //輸出用來儲存已排序的結果的第三個陣列(arrayC)。
    {
        cout << arrayC[i] << " ";
    }

    return 0;
}

void Bubblesort ( int *arr , int _size ) //泡泡排序法
{
    while ( _size > 1 )
    {
        int mid = 0;
        for ( int i = 0 ; i < _size - 1 ; i++ )
        {
            if ( *(arr+i) > *(arr+i+1) ) //相鄰的元素互相比大小，若前者大於後者，則將兩者做交換。
            {
                mid = *(arr+i);
                *(arr+i) = *(arr+i+1);
                *(arr+i+1) = mid;
            }
        }
        _size--;
    }
}

void Merge ( int *arrA , int *arrB , int *arrC , int _size ) //合併排序法
{
    int i = 0;
    int j = 0;
    while ( i != 5 || j != 5 )
    {
        if ( j == 5 || i == 5 )
        {
            if ( j == 5 )
            {
                *(arrC+ _size) = *(arrA+i);
                i++;
                _size++;
            }
            else
            {
                *(arrC+ _size) = *(arrB+j);
                j++;
                _size++;
            }
        }
        else if ( *(arrA+i) < *(arrB+j) )
        {
            if ( i < 5 )
            {
                *(arrC+ _size) = *(arrA+i);
                i++;
                _size++;
            }
        }
        else
        {
            if ( j < 5 )
            {
                *(arrC+ _size) = *(arrB+j);
                j++;
                _size++;
            }
        }
    }
}
