#include <iostream>
using namespace std;
void bubblesort(int* array , int size)//由小到大排列
{
    for(int i=0 ; i<size ; i++)
    {
        for(int j=0 ; j<size-1 ; j++)
        {
            if(*(array+j)>*(array+j+1))
            {
                int temp;
                temp=*(array+j);
                *(array+j)=*(array+j+1);
                *(array+j+1)=temp;
            }

        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int size)//混合排列
{
    for(int i=0 ; i<(size/2) ; i++)
        if(*(arr1+i)<*(arr2+i))
        {
            *(arr3+2*i)=*(arr1+i);
            *(arr3+1+2*i)=*(arr2+i) ;
        }
        else
        {
            *(arr3+2*i)=*(arr2+i) ;
            *(arr3+1+2*i)=*(arr1+i) ;
        }
}
int main()//輸出題目所需
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl<<"array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl<<"After sort"<<endl;
    bubblesort(arrayA,5);
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<arrayA[i]<<" ";
    }
    bubblesort(arrayB,5);
    cout<<endl<<"array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl<<"After merging two arrays"<<endl;
    Merge(arrayA,arrayB,arrayC,10);
    cout<<"array3 elements: ";
    for(int i=0; i<10; i++)
    {
        cout<<arrayC[i]<<" ";
    }
    return 0;
}
