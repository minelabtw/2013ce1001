#include<iostream>

using namespace std;

void bubblesort(int* array,int Size);

void Merge(int *arr1,int *arr2,int *arr3,int Size);

int main()
{
    int arrayA[5]={10,8,6,4,2};
    int arrayB[5]={9,7,5,3,1};
    int arrayC[10];
    int i,j,k,l,m;
    int *arr1,*arr2,*arr3;
    int Size;

    cout << "Before sort" << endl;
    cout << "array1 elements: ";
    for ( i = 0 ; i < 5 ; i++ )
    {
        cout << arrayA[i] << " ";
    }
    cout << endl;

    cout << "array2 elements: ";
    for ( j = 0 ; j < 5 ; j++ )
    {
        cout << arrayB[j] << " ";
    }
    cout << endl;

    bubblesort(arrayA,Size);

    cout << "After sort" << endl;

    cout << "array1 elements: ";
    for ( k = 0 ; k < 5 ; k++ )
    {
        cout << arrayA[k] << " ";
    }
    cout << endl;

    bubblesort(arrayB,Size);

    cout << "array2 elements: ";
    for ( l = 0 ; l < 5 ; l++ )
    {
        cout << arrayB[l] << " ";
    }
    cout << endl;

    arr1 = arrayA;
    arr2 = arrayB;
    arr3 = arrayC;

    Merge(arr1,arr2,arr3,Size);

    cout << "After merging two arrays" << endl;
    cout << "array3 elements: ";
    for ( m = 0 ; m < 10 ; m++ )
    {
        cout << arrayC[m] << " ";
    }

    return 0;
}


void bubblesort(int* array,int Size)
{
    Size = 5;

    for (int a = 0 ; a < Size ; a++ )
    {
        for (int b = 0 ; b < Size - 1; b++ )
        {
            int com = 0;
            if ( array[b] > array[b+1] )
            {
                com = array[b];
                array[b] = array[b+1];
                array[b+1] = com;
            }

        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int Size)
{
    Size = 5;
    for ( int c = 0 ; c < Size ; c++ )
    {
        if ( *(arr1+c) > *(arr2+c) )
        {
            int d = 2 * c;
            *(arr3+d+1) = *(arr1+c);
            *(arr3+d) = *(arr2+c);
        }
    }
}
