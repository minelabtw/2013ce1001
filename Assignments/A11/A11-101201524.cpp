#include<iostream>
using namespace std;

void bubblesort(int* arr , int size)//宣告一涵式將陣列中的數字從第一項到倒數第j項跟自己的下一項比大小，小的排到左邊
{
    for(int j=size-1;j>0;j--)
    {
        for(int i=0;i<j;i++)
        {
            if(*(arr+i)>*(arr+i+1))
                swap(*(arr+i),*(arr+i+1));
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)////宣告一涵式將兩由小排到大陣列中的數字兩兩相比更小的排進另一陣列中
{
    for(int i=0,j=0,k=0;k<2*size;k++)
    {
        if(j>=size || (i<size && *(arr1+i)<*(arr2+j)))
            *(arr3+k) = *(arr1+(i++));
        else
            *(arr3+k) = *(arr2+(j++));
    }
}

int main()
{
    int A[5]={10,8,6,4,2},B[5]={9,7,5,3,1},C[10]={},i;//宣告兩個給訂的陣列和一大小為兩陣列相加的陣列及一變數i

    cout << "Before sort" << endl;//輸出排列前兩陣列的樣子
    cout << "array1 elements:";
    for(i=0;i<5;i++)
        cout << " " << *(A+i);
    cout << endl << "array2 elements:";
    for(i=0;i<5;i++)
        cout << " " << *(B+i);

    bubblesort(A,5);//將A的數字經涵式由小到大排好
    bubblesort(B,5);//將B的數字經涵式由小到大排好

    cout << endl << "After sort" << endl;//輸出排列後兩陣列的樣子
    cout << "array1 elements:";
    for(i=0;i<5;i++)
        cout << " " << *(A+i);
    cout << endl << "array2 elements:";
    for(i=0;i<5;i++)
        cout << " " << *(B+i);

    Merge(A,B,C,5);//將A,B經Merge照順序排進C陣列中

    cout << endl << "After merging two arrays" << endl;//輸出陣列C
    cout << "array1 elements:";
    for(i=0;i<10;i++)
        cout << " " << *(C+i);

    return 0;
}
