#include <iostream>

using namespace std;
void bubblesort(int *array, int size)
{
    for(int i=size; i>0; --i)
    {
        for(int j=0; j<i-1; j++)
        {
            if(*(array+j)>*(array+j+1))
            {
                swap(*(array+j),*(array+j+1));
            }
        }
    }
}
void Merge(int *arr1, int *arr2, int *arr3,int size)
{
    for(int i=0,j=0; i<5; ++i,j+=2)
    {
        if(*arr1+i<*arr2+i)
        {
            *(arr3+j)=*(arr1+i);
            *(arr3+j+1)=*(arr2+i);
        }
        else
        {
            *(arr3+j)=*(arr2+i);
            *(arr3+j+1)=*(arr1+i);
        }
    }
}
int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    cout << "Before sort\n";
    cout << "array1 elements:";
    for(int i=0; i<5; ++i)//ouput
    {
        cout << " "<< arrayA[i];
    }
    cout << endl;
    cout << "array2 elements:";
    for(int i=0; i<5; ++i)
    {
        cout << " "<< arrayB[i];//ouput
    }
    cout << endl;
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);
    cout << "After sort\n";
    cout << "array1 elements:";
    for(int i=0; i<5; ++i)
    {
        cout << " "<< arrayA[i];//ouput
    }
    cout << endl;
    cout << "array2 elements:";
    for(int i=0; i<5; ++i)
    {
        cout << " "<< arrayB[i];//ouput
    }
    cout << endl;
    Merge(arrayA,arrayB,arrayC,10);
    cout << "After merging two arrays\n";
    cout << "array3 elements:";
    for(int i=0;i<10;++i)
    {
        cout << " " << arrayC[i];//ouput
    }
    cout << endl;
    return 0;
}
