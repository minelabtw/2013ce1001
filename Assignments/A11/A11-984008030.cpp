#include <iostream>
using namespace std;

void bubblesort(int* array , int size);//將array[0]~array[size-1]進行排序
void printArray(int* array , int size);//印出array[0]~array[size-1]
void Merge(int *arr1,int *arr2,int *arr3,int size);//將arr1[]和arr2[]合併成arr3

int main(){
    int arrayA[5] = {10, 8, 6, 4, 2};//宣告整數元素數量5個的陣列A，並初始化為10, 8, 6, 4, 2
    int arrayB[5] = {9, 7, 5, 3, 1};//宣告整數元素數量5個的陣列B，並初始化為9, 7, 5, 3, 1
    int arrayC[10] = {};//宣告整數元素數量10個的陣列C，並將所有元素初始化為0

    cout << "Before sort" << endl;
    cout << "array1 elements: ";
    printArray(arrayA, 5);
    cout << "array2 elements: ";
    printArray(arrayB, 5);

    bubblesort(arrayA, 5);//排序arrayA
    bubblesort(arrayB, 5);//排序arrayB
    Merge(arrayA, arrayB, arrayC, 10);//將arrayA和arrayB合併到arrayC

    cout << "After sort" << endl;
    cout << "array1 elements: ";
    printArray(arrayA, 5);
    cout << "array2 elements: ";
    printArray(arrayB, 5);
    cout << "After merging two arrays" << endl;
    cout << "array3 elements: ";
    printArray(arrayC, 10);

    return 0;
}

void bubblesort(int* array , int size){//將array[0]~array[size-1]進行排序
    for(int i = size; i > 0; i--){
        //每一次最大的放到array[i-1]後i就減一
        for(int j = 1; j < i; j++){
            int temp = 0;
            if(array[j - 1] > array[j]){//上一個和目前的元素比較大小，若上一個比較大就交換
                temp = array[j - 1];
                array[j - 1] = array[j];
                array[j] = temp;
            }
        }
    }
}

void printArray(int* array , int size){//印出array[0]~array[size-1]
    for(int i = 0; i < size; i++){
        cout << array[i] << " ";
    }
    cout << endl;
}

void Merge(int *arr1,int *arr2,int *arr3,int size){//將arr1[]和arr2[]合併成arr3
    int arrOneIndex = 0;
    int arrTwoIndex = 0;
    int arrThrIndex = 0;
    while(arrOneIndex < 5 && arrTwoIndex < 5 && arrThrIndex < size){
        //若三個條件其中一個成立就跳出，為避免超出原本設定的陣列長度而溢出
        if(arr1[arrOneIndex] <= arr2[arrTwoIndex]){
            arr3[arrThrIndex++] = arr1[arrOneIndex++];
        }
        else{
            arr3[arrThrIndex++] = arr2[arrTwoIndex++];
        }
    }
    //假使arr3夠大，在此處只會有兩種情況發生:
    //1. arr2完全放入arr3，此時arr1還有一些元素還沒放入
    while(arrOneIndex < 5 && arrThrIndex < size){
        arr3[arrThrIndex++] = arr1[arrOneIndex++];
    }
    //2. arr1完全放入arr3，此時arr2還有一些元素還沒放入
    while(arrTwoIndex < 5 && arrThrIndex < size){
        arr3[arrThrIndex++] = arr2[arrTwoIndex++];
    }
    //但為了避免arr3的size不夠大，所以在兩個迴圈加入arrThrIndex < size的限制條件!
}
