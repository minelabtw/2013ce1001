#include <iostream>
using namespace std;

void bubblesort(int* array , int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

int main()
{
    int *arrA;
    int *arrB;
    int *arrC;
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];

    arrA=arrayA;
    arrB=arrayB;
    arrC=arrayC;

    cout << "Before sort" << endl;

    cout << "array1 elements: ";
    for(int j = 0 ; j < 5 ; j++)
    {
        cout << arrA[j] << " ";
    }
    cout << endl;

    cout << "array2 elements: ";
    for(int k = 0 ; k < 5 ; k++)
    {
        cout << arrB[k] << " ";
    }
    cout << endl;

    cout << "After sort" << endl;

    bubblesort(arrA,5);
    cout << "array1 elements: ";
    for(int j = 0 ; j < 5 ; j++)
    {
        cout << arrA[j] << " ";
    }
    cout << endl;

    bubblesort(arrB,5);
    cout << "array2 elements: ";
    for(int k = 0 ; k < 5 ; k++)
    {
        cout << arrB[k] << " ";
    }
    cout << endl;

    cout << "After merging two arrays" << endl;

    Merge(arrA,arrB,arrC,10);
    cout << "array3 elements: ";

    for(int h = 0 ; h < 10 ; h++)
    {
        cout << arrC[h] << " ";
    }
    cout << endl;


    return 0;
}

void bubblesort(int* array , int size)
{
    for(int  a = 1 ; a <= size ; a++) //每次步驟重複的次數為1~size及該次要輸入的數的數量
    {
        int b = 0;

        while(b < size-1) //當該數比後一個數大時，該數往後拉，繼續和後一個比，但該數大小不變，故用上面儲存的h代表該數
            //@size-1的目的在於陣列中若有5個數，該陣列最高只到第4個數,故size需先減1不然會跟陣列中第五個數比較到大小，該數為0，因此會影響到輸出結果，因為運作到6個數
        {
            int c = array[b]; //先儲存第b個數，用來與前一個數互換
            if(c > array[b+1])
            {
                array[b] = array[b+1];
                array[b+1] = c;
            }
            b++; //b++的目的在於持續地跟後一個數比較大小，直到不符合while迴圈
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    int d = 0;
    int b = 0;
    int c = 0;

    while(d < size)
    {
        if(b == 5) //當1或2陣列到達第5個數時，因為已經超過其陣列，故直接跳出
        {
            arr3[d] = arr2[c];
            break;
        }
        else if(c == 5)
        {
            arr3[d] = arr1[b];
            break;
        }

        if(arr1[b] > arr2[c])
        {
            arr3[d] = arr2[c];
            c++;
        }
        else if(arr2[c] > arr1[b])
        {
            arr3[d] = arr1[b];
            b++;
        }
        d++;
    }
}
