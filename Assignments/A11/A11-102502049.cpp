#include <iostream>
using namespace std;

void bubblesort(int* array , int size) //sort two array A&B
{
    for (int i=0; i<size-1; i++)
    {
        int _size=size;
        for (int j=0,k=j+1; j<_size-1; j++,k++)
        {
            if (array[j]>array[k])
            {
                int hold;
                hold=array[j];
                array[j]=array[k];
                array[k]=hold;
            }
        }
        _size--;
    }
}



void Merge(int *arr1,int *arr2,int *arr3,int size) //Merge two array A&B to C
{
    int i=0;
    int j=0;
    int k=0;

    do
    {
        if(i<size && j<size)
        {
            if (arr1[i]<arr2[j])
            {
                arr3[k]=arr1[i];
                i++;
                k++;
            }
            else
            {
                arr3[k]=arr2[j];
                j++;
                k++;
            }
        }
        else
        {
            if(i<size)
            {
                for(; i<size; i++,k++)
                {
                    arr3[k]=arr1[i];
                }
            }
            else
            {
                for(; j<size; j++,k++)
                {
                    arr3[k]=arr2[j];
                }
            }

        }
    }
    while (i!=5 || j!=5);

}

void showout (int* array, int size, int mode) //print the consequence
{
    cout << "array" << mode << " elements: ";
    for (int i=0; i<size; i++)
    {
        cout << array[i] << " ";
    }
    cout << endl;
}

int main()
{
    const int size=5;
    int A[size]= {10,8,6,4,2};
    int B[size]= {9,7,5,3,1};
    int C[10];  //store the answer after merging

    cout << "Before sort" << endl;
    showout(A,size,1);
    showout(B,size,2);

    bubblesort(A,size);
    bubblesort(B,size);
    Merge(A,B,C,5);

    cout << "After sort" << endl;
    showout(A,size,1);
    showout(B,size,2);

    cout << "After merging two arrays" << endl;
    cout << "array3 elements: ";
    for (int j=0; j<10; j++)
        cout << C[j] << " ";

    return 0;
}
