#include <iostream>

using namespace std;
void bubblesort(int* array , int size); //原型
void Merge(int *arr1,int *arr2,int *arr3,int size);//原型

int main()
{
    int size=5; //宣告
    int arrayA[5]= {10,8,6,4,2}; //宣告陣列
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    cout << "Before sort"<<endl<<"array1 elements: "; //輸出
    for(int k=0; k<size; k++) //迴圈
        cout<<arrayA[k]<<" ";
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<size; i++)
        cout<<arrayB[i]<<" ";
    cout<<endl;
    bubblesort(arrayA ,size); //呼叫
    cout<<"After sort"<<endl<<"array1 elements: ";
    for(int k=0; k<size; k++)
        cout<<arrayA[k]<<" ";
    cout<<endl;
    bubblesort(arrayB ,size);
    cout<<"array2 elements: ";
    for(int i=0; i<size; i++)
        cout<<arrayB[i]<<" ";
    cout<<endl;
    cout<<"After merging two arrays"<<endl;
    cout<<"array3 elements: ";
    Merge(arrayA, arrayB, arrayC, size);
    for(int j=0; j<size*2; j++)
        cout<<arrayC[j]<<" ";

    return 0;
}
void bubblesort(int* array , int size) //大小排列
{
    int next;
    for(int j=0; j<size-1; j++)
        for(int k=0; k<size-1-j; k++)
        {
            if(*(array+k)>*(array+k+1))
            {
                next=*(array+k);
                *(array+k)=*(array+k+1);
                *(array+k+1)=next;
            }
        }
}
void Merge(int *arr1,int *arr2,int *arr3,int size) //把array1和array2兩個陣列內的元素比較大小並由小到大存入arrayC中
{
    for(int i=0,j=0,k=0; i<size*2;i++)
    {
        if(j>=size)
        {
            *(arr3+i)=*(arr1+k);
            k++;
        }
        else if(k>=size)
        {
            *(arr3+i)=*(arr2+j);
            j++;
        }
        else if(*(arr1+k)>*(arr2+j))
        {
            *(arr3+i)=*(arr2+j);
            j++;
        }
        else if(*(arr1+k)<=*(arr2+j))
        {
            *(arr3+i)=*(arr1+k);
            k++;
        }
    }

}
