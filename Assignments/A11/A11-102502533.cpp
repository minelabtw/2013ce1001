#include<iostream>
using namespace std;

void bubblesort(int* array , int _size);//排列大小的函式
void Merge(int *arr1,int *arr2,int *arr3,int _size);//兩陣列比較大小的函式

int main()
{
    int arrayA[5]= {10,8,6,4,2};//第一個陣列
    int arrayB[5]= {9,7,5,3,1};//第二個陣列
    int arrayC[10];//給一個空陣列放比較後的數

    cout<<"Before sort"<<endl<<"array1 elements: ";//從第13-19行先顯示預設陣列
    for(int a=0; a<5; a++)
        cout<<arrayA[a]<<" ";
    cout << endl << "array2 elements: ";
    for(int b=0; b<5; b++)
        cout<<arrayB[b]<<" ";
    cout<<endl;

    bubblesort(arrayA ,5);//排大小
    bubblesort(arrayB ,5);//排大小

    cout<<"After sort"<<endl<<"array1 elements: ";//從第24-29行顯示排完後的陣列
    for(int a=0; a<5; a++)
        cout<<arrayA[a]<<" ";
    cout << endl << "array2 elements: ";
    for(int b=0; b<5; b++)
        cout<<arrayB[b]<<" ";

    Merge(arrayA,arrayB,arrayC,5);//兩陣列比較大小

    cout<<endl<<"After merging two arrays"<<endl<<"array3 elements: ";//顯示比較後的新陣列
    for(int a=0; a<10; a++)
        cout<<arrayC[a]<<" ";


}
void bubblesort(int* array , int _size)//比大小的函式
{
    for(int j=0; j<_size; j++)
    {
        for(int i=0; i<_size-1; i++)
        {
            int first=*(array+i);
            int second=*(array+i+1);
            if(first>second)
            {
                *(array+i+1)=first;
                *(array+i)=second;

            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int _size)//讓兩陣列比較大小
{

    for(int i=0; i<_size; i++)
    {
        if(*(arr1+i)>*(arr2+i))
        {
            *(arr3+2*i+1)=*(arr1+i);
            *(arr3+2*i)=*(arr2+i);
        }
        else
        {
            *(arr3+2*i+1)=*(arr2+i);
            *(arr3+2*i)=*(arr1+i);
        }
    }
}

