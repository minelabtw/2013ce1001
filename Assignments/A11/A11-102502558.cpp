#include <iostream>
#define _swap(x,y) (x) ^= (y) ^= (x) ^= (y)
using namespace std;
// bubblesort
void bubblesort(int *arr, int _size)
{
    for (int i=0;i<_size;i++)
        for (int j=1;j<_size-i;j++)
            if (*(arr+j-1) > *(arr+j))
                _swap(*(arr+j-1), *(arr+j));

}

void show_array(int *arrA,int *arrB, int _size)
{
    // show two array
    cout << "array1 elements:";
    for (int i=0;i<_size;i++)
        cout << " " << *(arrA+i);
    cout << endl;
    cout << "array2 elements:";
    for (int i=0;i<_size;i++)
        cout << " " << *(arrB+i);
    cout << endl;
}

void Merge(int *arr1,int *arr2,int *arr3,int _size)
{
    // merge
    int x = 0,y = 0, i;
    for (i=0;i<_size && x<_size/2 && y <_size/2;i++)
        *(arr3+i) = (*(arr1+x) < *(arr2+y)) ? *(arr1+(x++)) : *(arr2+(y++));
    // add rest
    while (x < _size/2)
        *(arr3+i) = *(arr1+(x++));
    while (y < _size/2)
        *(arr3+i) = *(arr2+(y++));
}

int main(int argc, char *argv[])
{
    // declaration
    int arrayA[5]={10,8,6,4,2};
    int arrayB[5]={9,7,5,3,1};
    int arrayC[10];

    cout << "Before sort" << endl;
    show_array(arrayA, arrayB, 5);
    // sort
    bubblesort(arrayA, 5);
    bubblesort(arrayB, 5);
    Merge(arrayA, arrayB, arrayC, 10); //merge

    cout << "After sort" << endl;
    show_array(arrayA, arrayB, 5);

    // print result
    cout << "After merging two arrays" << endl;
    cout << "array3 elements:";
    for (int i=0;i<10;i++)
        cout << " " << *(arrayC+i);
    cout << endl;
    return 0;
}
