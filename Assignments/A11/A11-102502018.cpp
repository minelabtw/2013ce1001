#include <iostream>
using namespace std;

int bubblesort(int* ,int );
int Merge(int *,int *,int *,int );

int main()
{
    const int size0=5;
    const int size1=10;
    int arrayA[size0]= {10,8,6,4,2};
    int arrayB[size0]= {9,7,5,3,1};
    int arrayC[size1];
    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<size0; i++)
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<size0; i++)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl;
    cout<<"After sort"<<endl;
    bubblesort(arrayA,size0);
    cout<<"array1 elements: ";
    for(int i=0; i<size0; i++)
    {
        cout<<arrayA[i]<<" ";
    }
    cout<<endl;
    bubblesort(arrayB,size0);
    cout<<"array2 elements: ";
    for(int i=0; i<size0; i++)
    {
        cout<<arrayB[i]<<" ";
    }
    cout<<endl;
    cout<<"After merging two arrays"<<endl;
    Merge(arrayA,arrayB,arrayC,size1);
    cout<<"array3 elements: ";
    for(int i=0; i<size1; i++)
    {
        cout<<arrayC[i]<<" ";
    }
    return 0;
}
int bubblesort(int* arr,int size0)
{
    int p;
    for(int k=0; k<size0; k++)
    {
        for(int h=1; h<size0; h++)
        {
            p=arr[h-1];             //讓每一項帶入p
            if(p>arr[h])            //如果前一項大於後一項，前後交換
            {
                arr[h-1]=arr[h];
                arr[h]=p;
            }
            else;
        }
    }
}
int Merge(int *arr1,int *arr2,int *arr3,int size1)
{
    int a=0,b=0;
    for(int k=0; k<size1; k++)
    {
        if(arr1[a]>arr2[b])
        {
            arr3[k]=arr2[b];
            b++;
            if(b==5)                         //當b等於5時，讓arr1的剩下的項全不輸出
            {
                for(;a<5;a++)
                {
                    arr3[k+1]=arr1[a];
                    k++;
                }
                break;
            }
        }
        else if(arr1[a]<arr2[b])
        {
            arr3[k]=arr1[a];
            a++;
            if(a==5)                         //當a等於5時，讓arr1的剩下的項全不輸出
            {
                for(;b<5;b++)
                {
                    arr3[k+1]=arr1[a];
                    k++;
                }
                break;
            }
        }
    }
}
