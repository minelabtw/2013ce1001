#include <iostream>

using namespace std;
void bubblesort(int*  , int );
void Merge(int *,int *,int *,int );
int main()
{
    int *arra;										//宣告指標 陣列
    int *arrb;
    int *arrc;
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    arra=arrayA;
    arrb=arrayB;
    arrc=arrayC;

    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)							//顯示原來陣列
    {
        cout<<*(arra+i)<<" ";						//進入迴圈用陣列表示
    }
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<*(arrb+i)<<" ";
    }
    cout<<endl;
    cout<<"After sort"<<endl;						//顯示排序後的陣列
	bubblesort(arra,5);
    bubblesort(arrb,5);
    cout<<"array1 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<*(arra+i)<<" ";
    }
    cout<<endl;
    cout<<"array2 elements: ";
    for(int i=0; i<5; i++)
    {
        cout<<*(arrb+i)<<" ";
    }
    cout<<endl;
    cout<<"After merging two arrays"<<endl;
    Merge(arra,arrb,arrc,10);
    cout<<"array3 elements: ";						//顯示合併陣列
	for(int i=0;i<10;i++)
	{
		cout<<*(arrc+i)<<" ";
	}

    return 0;
}
void bubblesort(int *arr , int _size)
{
    int a;
    int b;
    for(a=0; a<_size; a++)							//將最小數排到左邊
    {
        for(b=_size-2; b>=0; b--)
        {
            if(arr[b]>arr[b+1])						//將直排序大小
            {
                int x;
                x=arr[b+1];
                arr[b+1]=arr[b];
                arr[b]=x;

            }
        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int _size)
{
	for(int j=0,i=0,k=0;j<10;j++)					//合併陣列
	{
		if(*(arr1+i)>*(arr2+k)&&(k!=5))				//比較大小將較小的值傳入 並將較小的數下一個位置的值與較大的位置值比較
		{
			*(arr3+j)=*(arr2+k);
			k++;
		}
		else if(k==5)								//值超過陣列位數
		{
			*(arr3+j)=*(arr1+i);
			i++;
		}
		else if(i==5)
		{
			*(arr3+j)=*(arr2+k);
			k++;
		}
		else
		{
			*(arr3+j)=*(arr1+i);
			i++;
		}
	}
}
