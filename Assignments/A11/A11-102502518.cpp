#include <iostream>
using namespace std;

void bubblesort(int* Array , int Size);//排序陣列內數字的函式
void Merge(int *arr1,int *arr2,int *arr3,int size);//將另外兩函式合併並排序的函式

int main()
{
    const int sizeA=5;
    const int sizeB=5;
    const int sizeC=10;
    int arrayA[sizeA]= {10,8,6,4,2};
    int arrayB[sizeB]= {9,7,5,3,1};
    int arrayC[sizeC]= {};

    cout<<"Before sort";

    cout<<endl<<"array1 elements: ";
    for(int i=0; i<sizeA; i++)//輸出排列前的陣列A內容
        cout<<arrayA[i]<<" ";

    cout<<endl<<"array2 elements: ";
    for(int i=0; i<sizeB; i++)//輸出排列前的陣列B內容
        cout<<arrayB[i]<<" ";

    cout<<endl<<"After sort";

    cout<<endl<<"array1 elements: ";
    for(int a=0; a<sizeA; a++)//輸出排列後的陣列A內容
    {
        bubblesort(arrayA,sizeA);
        cout<<arrayA[a]<<" ";
    }

    cout<<endl<<"array2 elements: ";
    for(int a=0; a<sizeB; a++)//輸出排列後的陣列B內容
    {
        bubblesort(arrayB,sizeB);
        cout<<arrayB[a]<<" ";
    }

    cout<<endl<<"After merging two arrays";

    cout<<endl<<"array3 elements: ";
    Merge(arrayA,arrayB,arrayC,sizeC);
    for(int a=0; a<sizeC; a++)//輸出排列後的陣列C內容
        cout<<arrayC[a]<<" ";

    return 0;
}

void bubblesort(int* Array , int Size)
{
    for(int i=Size-1; i>=0; i--) //使用迴圈排列大小
    {
        if (Array[i]<Array[i-1])
        {
            int t=Array[i];
            Array[i]=Array[i-1];
            Array[i-1]=t;
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int Size)
{
    for(int i=0; i<Size; i++)//判斷兩陣列之數字大小並重新排列且合併陣列
    {
        if(arr2[i]<arr1[i])
        {
            arr3[2*i]=arr2[i];
            arr3[2*i+1]=arr1[i];
        }
        else
        {
            arr3[2*i]=arr1[i];
            arr3[2*i+1]=arr2[i];
        }
    }
}
