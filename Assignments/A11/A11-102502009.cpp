#include <iostream>
using namespace std;
void bubblesort(int* _array , int _size)
{
    for(int i=0 ; i<_size ; i++)
    {
        for(int j=0 ; j<_size-1 ; j++)
        {
            if(*(_array+j)>*(_array+j+1)) //前大於後就交換
            {
                int temp;
                temp=*(_array+j);
                *(_array+j)=*(_array+j+1);
                *(_array+j+1)=temp;
            }

        }
    }
}
void Merge(int *arr1,int *arr2,int *arr3,int _size)
{
    for(int i=0 ; i<(_size/2) ; i++)
        if(*(arr1+i)<*(arr2+i)) //小的放前面大的放後面
        {
            *(arr3+2*i)=*(arr1+i);
            *(arr3+1+2*i)=*(arr2+i);
        }
        else
        {
            *(arr3+2*i)=*(arr2+i);
            *(arr3+1+2*i)=*(arr1+i);
        }
}
void output(int *arr , int _size) //太多output了,用成涵式
{
    for(int i=0 ; i< _size ; i++)
    {
        cout<<*(arr+i)<<" ";
    }
    cout<<endl;
}
int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    cout<<"Before sort"<<endl;
    cout<<"array1 elements: ";
    output(arrayA,5);
    cout<<"array2 elements: ";
    output(arrayB,5);
    cout<<"After sort"<<endl;
    bubblesort(arrayA,5);
    bubblesort(arrayB,5);
    cout<<"array1 elements: ";
    output(arrayA,5);
    cout<<"array2 elements: ";
    output(arrayB,5);
    cout<<"After merging two arrays"<<endl;
    Merge(arrayA,arrayB,arrayC,10);
    cout<<"array3 elements: ";
    output(arrayC,10);
    return 0;
}
