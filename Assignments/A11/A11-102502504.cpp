#include <iostream>
using namespace std;
void bubblesort(int* array , int size);
void Merge(int *arr1,int *arr2,int *arr3,int size);

int main()
{
    int arrayA[5]= {10,8,6,4,2};
    int arrayB[5]= {9,7,5,3,1};
    int arrayC[10];
    int *arr1,*arr2,*arr3;

    arr1=arrayA; //將arrayA第一個數值的地址丟給arr1
    arr2=arrayB; //將arrayB第一個數值的地址丟給arr2
    arr3=arrayC; //將arrayC第一個數值的地址丟給arr3

    cout << "Before sort" << endl;
    cout << "array1 elements:";
    for(int i=0; i<=4; i++) //輸出arrayA的內容
    {
        cout << " " << *(arr1+i) ;
    }
    cout << endl;
    cout << "array2 elements:";
    for(int i=0; i<=4; i++) //輸出arrayB的內容
    {
        cout << " " << *(arr2+i);
    }
    cout << endl;

    bubblesort(arr1,5); //將arr1及5丟入bubblesort副涵式中，使arrayA由小到大排序
    bubblesort(arr2,5); //將arr2及5丟入bubblesort副涵式中，使arrayB由小到大排序
    cout << "After sort" << endl;
    cout << "array1 elements:";
    for(int i=0; i<=4; i++) //輸出arrayA排列後的內容
    {
        cout << " " << *(arr1+i) ;
    }
    cout << endl;

    cout << "array2 elements:";
    for(int i=0; i<=4; i++) //輸出arrayB排列後的內容
    {
        cout << " " << *(arr2+i);
    }
    cout << endl;

    Merge(arr1,arr2,arr3,5); //使arrayA及arrayB合併，並由小到大排列
    cout << "After merging two arrays" << endl;
    cout << "array3 elements:";
    for(int i=0; i<=9; i++) //輸出arrayC的內容
    {
        cout << " " << *(arr3+i);
    }

    return 0;
}

void bubblesort(int* array , int size)
{
    int a; //用來暫存
    for(int i=1; i<=size-1; i++) //第i次排列
    {
        for(int i=0; i<=size-2; i++) //相鄰位置兩兩比較
        {
            if(*(array+i)>*(array+i+1)) //若左向大於右項，則互換位置
            {
                a=*(array+i);
                *(array+i)=*(array+i+1);
                *(array+i+1)=a;
            }
        }
    }
}

void Merge(int *arr1,int *arr2,int *arr3,int size)
{
    for(int i=0; i<=size-1; i++) //比較arrayA及arrayB第i個位置
    {
        if(*(arr1+i)<*(arr2+i)) //若arrayA第i位的值小於arrayB第i位的值，則A之值置於B之值的前面
        {
            *(arr3+2*i)=*(arr1+i);
            *(arr3+2*i+1)=*(arr2+i);
        }
        if(*(arr1+i)>*(arr2+i)) //若arrayA第i位的值大於arrayB第i位的值，則B之值置於A之值的前面
        {
            *(arr3+2*i)=*(arr2+i);
            *(arr3+2*i+1)=*(arr1+i);
        }
    }
}
