#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main()
{
    int i,n=-1,j=1;  //宣告變數J表示輸入的次數
    int Max=100,Min=0;
    int c[j];  //宣告陣列C

    srand(time(0));
    i = rand()%101;

    while (n!=i)
    {
        while (n>Max||n<Min)
        {

            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
            cin >> n;
            c[ j ] = n;  //將輸入值傳給陣列
            j++;  //每輸入一次就增加陣列元素個數
            if (n>Max||n<Min)
                cout << "Out of range!" << endl;
        }

        if (n==i)
            cout << "You win!" << endl;
        else if (n>i)
            Max = n;
        else
            Min = n;

        if (n==Max)
            Max--;
        if (n==Min)
            Min++;

        if (Max==Min)
        {
            cout << "You lose!Answer is " << i << endl;
            break;
        }
    }

    for (int frequencyofinput=1;frequencyofinput<j;frequencyofinput++)  //用for loop印出陣列所有元素
    {
        cout << frequencyofinput << " - " << c[frequencyofinput] << endl;
    }

    return 0;
}
