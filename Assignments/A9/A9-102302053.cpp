#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main ()
{
    int upperb = 100;//uuperbound for the guessing.
    int lowerb = 0;//lowerbound for the guessing.
    int guess;
    int a[100];//use to store the process of guessing.
    int n = 1, i;

    srand(time(0));//seed for the answer.
    int answer = rand()%101;//get a random number from 0 to 100 to be the answer.

    do//prompt user to give a number until user wins or loses.
    {
        cout << "Enter number(" << lowerb << "<=number<=" << upperb << ")?: ";
        cin >> guess;//prompt user to enter a number for guessing.
        a[n] = guess;//store the input
        n++;

        if (guess == answer)//if user enter the same number as the answer, user wins.
        {
            cout << "You win!";
            cout << endl;
            break;
        }
        else if (guess > upperb || guess < lowerb)//check whether the answer is legal.
        {
            cout << "Out of range!" << endl;
            n--;
        }

        else if (guess < answer && guess >= lowerb)//change the hint for guessing(lowerbound).
        {
            lowerb = (guess + 1);
        }
        else if (guess > answer && guess <= upperb)//change the hint for guessing(upperbound).
        {
            upperb = (guess - 1);
        }


        if (upperb == lowerb)//if there is no number for user to guess, user loses.
        {
            cout << "You lose! Answer is " << answer << endl;
            break;
        }

    }
    while(guess != answer);

    for(i = 1; i < n; i++)//show the guessing process to the user.
    {
        cout << i <<" - " << a[i] << "\n";
    }



    system("pause");
    return 0;
}
