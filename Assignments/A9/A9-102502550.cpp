#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int num=0,a[110]= {},guess=0,right=100,left=0,i=0;                                        //變數宣告
    bool flag=true;

    srand(time(NULL));                                                          //亂數設定
    num=rand()%101;

    while(1)
    {
        cout<<"Enter number("<<left<<"<=number<="<<right<<")?: ";
        cin>>guess;

        if(guess>right || guess<left)
        {
            cout<<"Out of range!"<<endl;
        }
        else
        {

            a[i]=guess;
            i++;
            if(guess==num)                                                         //成功條件
            {
                cout<<"You win!"<<endl;
                for(int j=0; j<i; j++)                                             //印出過程
                {
                    cout<<j+1<<" - "<<a[j]<<endl;
                }
                break;
            }
            else
            {
                if(num>guess)                                                     //設定左右界
                {
                    left=guess+1;
                }
                else
                {
                    right=guess-1;
                }
                if(right==left)                                                 //失敗條件
                {
                    cout<<"You lose! Answer is "<<num<<endl;
                    for(int j=0; j<i; j++)                                      //印出過程
                    {
                        cout<<j+1<<" - "<<a[j]<<endl;
                    }
                    break;
                }

            }
        }
    }

    return 0;
}
