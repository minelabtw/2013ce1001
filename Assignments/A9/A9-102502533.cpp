#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));//亂數以時間打亂
    int number=rand()%101;//使數字介於1-100間
    int guess;//猜測值
    int Max=100;//最大值100
    int Min=0;//最小值0
    int a[100]= {};//使猜測次數為100次
    int i=1;//宣告I

    while(guess!=number&&i<100)//當沒猜中進入迴圈，限制在100次內
    {

        cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";//顯示最大值跟最小值
        cin>>guess;                                           //輸入猜測值
        a[i] =guess;                                          //使猜測值取代陣列內的植
        if(guess > Max || guess < Min)                        //超出猜測值
        {
            cout<<"Out of range!"<<endl;

        }
        else if(guess < number)                               //猜測值小於預設值
        {
            Min=guess+1;
            i++;
        }
        else if(guess > number)                               //猜測值大於預設值
        {
            Max=guess-1;
            i++;
        }

        if(guess==number)                                     //猜測值等於預設值，則贏
        {
            cout<<"You win!"<<endl;
            for( int j=1; j<i+1 ; j++)
                cout<<j<<" - "<<a[j]<<endl;
            break;
        }
        else if(Max==Min)                                     //最大值等於最小值，則輸
        {
            cout<<"You lose! Answer is "<<number<<endl;
            for( int j=1; j<i; j++)
                cout<<j<<" - "<<a[j]<<endl;
            break;
        }
    }
    return 0;
}
