#include<iostream>
#include<cstdlib>
#include<ctime>
#include<iomanip>
using namespace std;

int main()
{
    srand(time(0));                                                         //指定rand的seed。
    int randnum = rand () % 101, upbound = 100, lowbound = 0, ans = -1, anstime = 0 ;    // 宣告變數。
    const int arraysize = 100 ;                                                          //宣告一個常數，使其成為陣列的大小。
    int log[arraysize] = {};                                                             //宣告一個陣列來儲存過程中輸入的值。

    do
    {
        cout << "Enter number(" << lowbound << "<=number<=" << upbound <<")?: ";//要求使用者輸入答案。
        cin >> ans;

        if (ans > upbound || ans < lowbound)                                //若輸入的答案不符範圍，輸出字串。
        {
            cout << "Out of range!" << endl;
        }

        if (ans < randnum && ans >= lowbound)                               //若輸入的答案小於真正的答案，修改下限。
        {
            lowbound = ans + 1;
            log[anstime] = ans;                                             //將過程中輸入正確的答案儲存起來。
            anstime = anstime + 1;                                          //將回答的計數器加一。
        }
        else if(ans > randnum && ans <= upbound)                            //若輸入的答案大於真正的答案，修改上限。
        {
            upbound = ans - 1;
            log[anstime] = ans ;                                            //將過程中輸入正確的答案儲存起來。
            anstime = anstime + 1;                                          //將回答的計數器加一。
        }
        if(upbound == lowbound)                                             //若最後沒有猜出答案，輸出字串。
        {
            cout << "You lose! Answer is " << randnum << endl;

            for (int i=0; i<=anstime-1; i++)                                //輸出輸入過程
            {
                cout << i+1 << " - " << log[i] << endl;
            }
            break;
        }
    }
    while(ans != randnum);

    if (ans == randnum)                                                     //若使用者猜出正確的答案，輸出字串。
    {
        cout << "You win!" << endl;

        log[anstime] = ans;
        anstime = anstime + 1;
        for (int i=0; i<=anstime-1; i++)                                    //輸出輸入過程。
        {
            cout << i+1 << " - " << log[i] << endl;
        }
    }
    return 0;
}
