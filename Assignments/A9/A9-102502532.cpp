#include<iostream>
#include<cstdlib>                 //contain srand()  rand()
#include<ctime>             //contain time()

using namespace std;

int main()
{
    srand(time(0));                 //亂數 時間

    int rnumber = rand()%101;
    int input =0;
    int maxnum =100;
    int minnum =0;
    int countn =1;                                  //初始  要=1
    int show[100] = {};                         //陣列100個   初始值都0

    cout<<"Enter number(0<=number<=100)?: ";
    while(input != rnumber)                             //while 可以一直跑
    {
        cin>>input;                          //一直輸入
        show[countn] =input;                          //儲存為資料

        if (input >maxnum or input <minnum)
            cout<<"Out of range!\n";

        else if (input < rnumber)                            //改變最小值
        {
            minnum = input+1;
            countn++;                                     //+1
        }
        else if (input > rnumber)                          //改變最大值
        {
            maxnum = input-1;
            countn++;                                    //+1
        }
        if (maxnum == minnum)                        //判斷輸贏
        {
            cout<<"You lose! Answer is "<< rnumber<<endl;
            for ( int n =1; n<countn ; n++)                    //顯示
                cout<<n<<" - "<<show[n]<<endl;                //是 show[n]   不是show[countn]
            break;                                             //跳到while{}外     結束
        }
        else if(input == rnumber )
        {
            cout<<"You win!\n";
            for ( int n =1; n<=countn ; n++)                   //顯示
                cout<<n<<" - "<<show[n]<<endl;
            break;
        }
        cout<<"Enter number("<<minnum<<"<=number<="<<maxnum<<")?: ";             //放最後就好
    }

    return 0;
}

