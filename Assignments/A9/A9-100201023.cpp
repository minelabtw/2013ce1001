#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

int main()
{
	int answer , input , min , max;
	vector <int> history;

	srand(time(NULL));
	answer = rand() % 101; // set rnadom number

	min = 0; // set default boundary
	max = 100;

	while(true)
	{
		cout << "Enter number(" << min << "<=number<=" << max << ")?: "; // input the value
		cin >> input;
		
		if(input >= min && input <= max) // determine if input between the boundary
		{
			if(input == answer) // the correct answer
			{
				cout << "You win !" << endl; // output win message
				break;
			}

			if(input > answer) // set boundary
				max = input - 1;
			else
				min = input + 1;

			history.push_back(input); // store the number

			if(min == max)
			{
				cout << "You lose! Answer is " << answer << endl; // output answer
				break;
			}
		}
		else
			cout << "Out of range!" << endl;
	}

	for(int i = 0 ; i < history.size() ; ++i)
		cout << i << " - " << history[i] << endl; // output history number

	return 0;
}
