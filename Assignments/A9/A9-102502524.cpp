#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int main()
{
    int q = 0;                                                              //設定變數
    int a = 0;
    int m = 100;
    int n = 0;
    int i = 0;
    int c[100];                                                             //設定儲存輸入數字的陣列，若符合輸入範圍，則存入此數列

    srand(time(NULL));                                                      //取得亂數值
    q=(rand()%101);                                                         //範圍定於0~100之間，存於q

    cout << "Enter number(" << n << "<=number<=" << m << ")?: ";            //開始終極密碼迴圈
    while(cin >> a)
    {
        if (a<n || a>m)                                                     //檢查有無超出範圍
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << n << "<=number<=" << m << ")?: ";
        }
        else if (a==q)                                                      //答對則宣告勝利並跳出迴圈
        {
            i++;
            c[i]=a;
            cout << "You win!" <<endl;
            break;
        }
        else if (a>q)                                                       //大於答案則縮小範圍
        {
            i++;
            c[i]=a;
            m=a-1;
            if (m==n)                                                       //若範圍內只剩答案，則宣告失敗並輸出答案
            {
                cout << "You lose! Answer is " << q << endl;
                break;
            }
            cout << "Enter number(" << n << "<=number<=" << m << ")?: ";
        }
        else if (a<q)                                                       //小於答案則縮小範圍
        {
            i++;
            c[i]=a;
            n=a+1;
            if (m==n)                                                       //若範圍內只剩答案，則宣告失敗並輸出答案
            {
                cout << "You lose! Answer is " << q << endl;
                break;
            }
            cout << "Enter number(" << n << "<=number<=" << m << ")?: ";
        }
    }

    for (int h=1; h<=i; h++)                                                //輸出所有照順序猜測的數字
    {
        cout << h << " - " << c[h] << endl;
    }

    return 0;
}
