#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
    int number;
    int a = -1;
    srand (time (0)); // 隨著時間改變的亂數
    number = rand()%101;// 亂數除以101的餘數為0~100，為亂數範圍
    int Size = 0; // 預設變數為0
    int array[Size]; // 指定陣列

    cout << "Enter number(0<=number<=100)?: ";
    cin >> a;
    array[Size] = a; //將猜想儲存於陣列中
    Size++; // 有下一個時儲存到第二個

    while (a < 0||a >100) // 限制輸入範圍
    {
        cout << "Out of Range!\n";
        cout << "Enter number(0<=number<=100)?: ";
        cin >> a; // 未計次因為輸入錯誤
    }

    int low = 0; // 設置下限為零(起始值)
    int up =100; // 設置上限為零(起始值)

    while (a != number) // 猜想不等於答案時跳進來
    {

        if (a < number)
        {
            low = a + 1; // 猜想小於答案，上限不變。
            if(low == up) // 上下線相等，輸了
            {
                cout << "You lose! Answer is " << number <<endl;
                array[Size] = a;
                Size++;
                break;                     //離開if(low == up)
            }
            array[Size] = a;
            Size++;
        }
        else if (a > number )
        {
            up = a - 1; // 猜想大於答案，下限不變。
            if(low == up)
            {
                cout << "You lose! Answer is " << number << endl;
                array[Size] = a;
                Size++;
                break; //離開if(low == up)
            }
            array[Size] = a;
            Size++;
        }
        // 繼續猜想，提出公因式
        cout << "Enter number(" << low << "<=number<=" << up << ")?: ";
        cin >> a; // 未計次，因為會重複顯示

        while (a < low || a > up )
        {
            cout << "Out of range!\n";
            cout << "Enter number(" << low << "<=number<=" << up << ")?: ";
            cin >> a;
        }

    }

    if (a == number)
    {
        cout << "You win!\n";
        array[Size] = a;
        Size++;
    }

    for ( int counter = 1; counter < Size; counter++) // 輸出陣列
    {
        cout << counter << " - " << array [counter] << endl; // 叫出陣列儲存數值
    }

    return 0;
}
