#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main()
{
    int a=0,b=100,number=101,randnum=0,i=0;  //宣告變數
    srand(time(0));  //產生亂數
    randnum=(rand()%100)+1;
    int x[100];
    int v=1;
    while(number!=randnum)  //開始遊戲
    {
        cout<<"Enter number("<<a<<"<=number<="<<b<<")?: ";  //輸出題目
        cin>>number;
        while(number>b || number<a)  //判斷是否符合條件
        {
            cout<<"Out of range!\n";
            cout<<"Enter number("<<a<<"<=number<="<<b<<")?: ";
            cin>>number;
        }
        if(number==randnum)
        {
            cout<<"You win!\n";
        }
        if(number<randnum)
        {
            a=number+1;
        }
        if(number>randnum)
        {
            b=number-1;
        }
        if(a==b)
        {
            cout<<"You lose! Answer is "<<randnum<<endl;
            break;
        }
        i++;
        x[v]=number;
        v++;
    }
    for(int y=1; y<=i; y++)  //輸出過程
    {
        cout<<y<<" - "<<x[y]<<endl;
    }
    return 0;
}

