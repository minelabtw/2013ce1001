#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));//隨機選取數字1~100
    int ans=rand()%101;
    int a=0;
    int Max=100; //最大值,最小值
    int Min=0;
    int counter[100]={};
    int n=0;

    for(n=0;;n++)
    {
        cout << "Enter number(" << Min << "<=number<=" << Max << ")?: " ;//輸出字元
        cin >> a;
        while(a<Min || a>Max)
        {
            cout << "Out of range!" << endl ;
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: " ;
            cin >> a;
        }
        counter[n]=a;
        if (a>ans)//當輸入大於亂數，輸入最大值等於輸入-1
        {
            Max=a-1;
        }
        else if (a<ans)//當輸入大於亂數，輸入最小值等於輸入+1
        {
            Min=a+1;
        }
        if (Min == Max)//若是最大值與最小值相同，則程式結束
        {
            cout << "You lose! Answer is " << ans << endl;
            break;
        }
        else if(a == ans)//若是輸入等於亂數，則顯示字元，程式結束
        {
            cout << "You win!" << endl;
            break;
        }
    }
    for(int m=0;m<=n;m++)
        cout << m+1 << " - " << counter[m] << endl;
    return 0;
}
