#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));//讓亂數由當時的時間來產出
    int ans=rand() %101,ubd=100,lbd=0,g=101;//宣告ans=0~100的亂數當作答案,ubd,lbd當作選項的上下界(upper/lower bound)起始值為100,0而g則用來代表猜的數字
    int guess[101] = {},times = 0;//宣告一陣列guess來儲存每次猜的數,及一個變數times來記錄輸入有效數的次數

    while(1)
    {
        cout << "Enter number(" << lbd << "<=number<=" << ubd << ")?: ";//輸出字串要求猜一個在上下界內的整數
        cin >> g;//將猜的數宣告給g
        if(g<lbd || g>ubd)//當輸入的數字不再範圍內時，輸出字串Out of range!重新要求猜數字
        {
            cout << "Out of range!\n";
            continue;
        }
        if(g<ans)//若g比ans小則宣告新的下界為g+1
            lbd=g+1;
        else if(g>ans)//若g比ans大則宣告新的上界為g-1
            ubd=g-1;
        else//若g=ans則紀錄新的g到guess後輸出字串You win!結束程式
        {
            cout << "You win!";
            guess[times] = g;
            break;
        }
        if(lbd==ubd)//若上下界相等時則紀錄新的g到guess後輸出字串"You lose! Answer is "公布答案然後結束程式
        {
            cout << "You lose! Answer is " << ans;
            guess[times] = g;
            break;
        }
        guess[times] = g;//將猜的數傳給該次的guess[times]
        times += 1;//將次數+1
    }

    for(int j=0;j<=times;j++)//依照順序輸出每次猜測的數字
        cout << "\n" << j+1 << " - " << guess[j];

    return 0;
}
