#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
using namespace std;

int main()
{
    //decalaration and initialization
    int num = 0;
    int bound[2] = { 0, 100 };
    int log_size = 100;
    vector< int > log( log_size, -1 );      //record
    int temp = -1;                          //temporary

    //set answer
    srand( time(NULL) );
    num = rand() % 101;

    for ( int i = 0; i <= log_size; i ++ )
    {
        //ask for a number
        do
        {
            cout << "Enter number(" << bound[0] << "<=number<=" << bound[1] << ")?: ";
            cin >> temp;
        }
        while ( ( temp < bound[0] or temp > bound[1] ) and cout << "Out of range!" << endl );

        log[i] = temp;

        //argue whether the number inputed is answer
        //if not, change the boundary
        if ( temp == num )
        {
            cout << "You win!" << endl;
            break;
        }
        else if ( temp > num )
            bound[1] = temp - 1;
        else        //if temp < num
            bound[0] = temp + 1;

        //if there's only one possible number on the interval (bound[0],bound[1])
        if ( bound[0] == bound[1] )
        {
            cout << "You lose! Answer is " << num  << endl;
            break;
        }
    }

    for ( int i = 0; i <= log_size; i ++ )
        if ( log[i] > -1 )
            cout << i + 1 << " - " << log[i] << endl;
        else
            break;

    return 0;
}
