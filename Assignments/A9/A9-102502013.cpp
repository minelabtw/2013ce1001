#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{
    int answer = 0, number = 0, l = 0, h = 100, I=0;;
    int f[100] = {};
    srand(time(0));
    answer = (rand()%101);
    l = 0;//最小值
    h = 100;//最大值
    cout << "Enter number(0<=number<=100)?: ";
    cin >> number;
    while (true)
    {

        if (number<l||number>h)
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << l << "<=number<=" << h << " )?: ";
            cin >> number;
        }
        else if (answer<number&&number<=h)//輸入值介於答案和最大值之間
        {
            f[I] = number;//存入合法輸入的值
            h = number - 1;
            if (l==h)
            {
                cout << "You lose! Answer is " << answer << endl;
                for (int i=0; i<=I; i++)
                {
                    cout << i + 1 << " - " << f[i] << endl;//依序輸出答案
                }
                break;
            }
            cout << "Enter number(" << l << "<=number<=" << h << ")?: ";
            cin >> number;
            I++;
        }
        else if (l<=number&&number<answer)//輸入值介於答案和最小值之間
        {
            f[I] = number;//存入合法輸入的值
            l = number + 1;
            if (l==h)
            {
                cout << "You lose! Answer is " << answer << endl;
                for (int i=0; i<=I; i++)
                {
                    cout << i + 1 << " - " << f[i] << endl;//依序輸出答案
                }
                break;
            }
            cout << "Enter number(" << l << "<=number<=" << h << ")?: ";
            cin >> number;
            I++;
        }
        else if (number==answer)
        {
            f[I] = number;//存入合法輸入的值
            cout << "You win!" << endl;
            for (int i=0; i<=I; i++)
            {
                cout << i + 1 << " - " << f[i] << endl;//依序輸出答案
            }
            I++;
            break;
        }
    }
    return 0;
}
