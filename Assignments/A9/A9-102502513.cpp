//終極密碼遊戲
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));  //建立亂數表
    int randNum=rand()%101;

    int Max=100, Min=0;
    int choose=-1;

    int integer[99]= {}; //建立陣列
    int i=0;

    while(randNum!=choose)
    {
        do
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
            cin >> choose;

            if(choose>Max||choose<Min)
            {
                cout << "Out of range!\n";
            }
            else
            {
                integer[i]=choose;
            }
            i++;
        }
        while(choose>Max||choose<Min);

        if(choose==randNum)
        {
            cout << "You win!\n";
            i--;  //將i值正確還原
        }
        else if(choose>randNum)
        {
            Max = choose;
        }
        else
        {
            Min = choose;
        }

        if(choose==Max)
        {
            Max--;
        }
        else if(choose==Min)
        {
            Min++;
        }

        if(Max==Min)  //只剩一個數字，玩家lose
        {
            cout << "You lose! Answer is " << Max << endl;
            i--;  //將i值正確還原
            break;
        }
    }

    for(int j=0; j<=i; j++)  //運用陣列依序將所輸入之值印出
    {
        cout << j+1 << " - " << integer[j] << endl;
    }

    return 0;
}
