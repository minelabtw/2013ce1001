#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;



int main()
{
    int a,number,counter = 0,i; // initialize integer a and number and counter to 0 and i
    int Min = 0; // initialize integer Min and set it to 0
    int Max = 100; // initialize integer Max and set it to 0
    int answer[ 101 ] = {}; // initialize array answer
    srand( time ( 0 ) ); // use time as seed to set up random number
    number = rand() % 101; // sign a value to number between 0 to 100


    while ( 1 ) // while loop to let user guess the number
    {
        do
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
            cin >> a;
            if( a < Min || a > Max)
                cout << "Out of range!" << endl;
        }
        while( a < Min || a > Max);
        answer[ counter ] = a;
        counter++;


        if( a == number)       //猜中數字
        {
            cout << "You win!\n";
            for( int i = 0; i < counter; i++ )
                cout << i << " - " << answer[ i ] << endl;
            break;
        }
        else if( a > number)   //輸入的數字大於要猜的數字
        {
            Max = a;           //改變提示字範圍的最大值

        }
        else                        //輸入的數字小於要猜的數字
        {
            Min = a;           //改變提示字範圍的最小值

        }

        if( a == Max)           //輸入的數字等於最大值
            Max--;                  //將最大值範圍往內縮，最大值-1
        if( a == Min)           //輸入的數字等於最小值
            Min++;                  //將最小值範圍往內縮，最小值+1

        if( Max == Min)              //如果最大值等於最小值，代表只剩一個數字，所以玩家lose
        {
            cout << "You lose! Answer is " << Max << endl;;
            for( int i = 0; i < counter; i++ )
                cout << i << " - " << answer[ i ] << endl;
            break;
        }

    }


    return 0;

}



