#include <iostream>

#include <stdlib.h>//using the function  bank

#include <time.h>

using namespace std ;

int main ()
{
    int number=0, a=0, b=100, c=0, x=0, s[102], i=1, A=1 ;

    srand(time(NULL)) ;//using the function srand

    number=rand ()%101 ;

    while (1)//using the loop
    {
        cout << "Enter number(0<=number<=100)?: " ;

        cin >> a ;

        if (a<0||a>100)
        {
            cout << "Out of range!\n" ;
        }

        else if (a>=0&&a<=100)
        {
            s[i]=a ;

            break ;
        }
    }

    while (true)
    {
        while (a>number)
        {
            b=a ;

            b=b-1 ;

             if (c==b)
            {
                cout << "You lose! Answer is " << c ;

                x=1 ;

                break ;
            }

            cout << "Enter number(" << c << "<=number<="<< b << ")?: " ;

            cin >> a ;

            if (a>=c&&a<=b&&b!=c)
            {
                s[i+1]=a ;//storing a into s[i]

                i++ ;
            }

            else if (a<c||a>b)
            {
                cout << "Out of range!\n" ;

                a=b+1 ;
            }
        }

        while (a<number)
        {
            c=a ;

            c=c+1 ;

            if (c==b)
            {
                cout << "You lose! Answer is " << c << endl ;

                x=1 ;

                break ;
            }

            cout << "Enter number(" << c << "<=number<="<< b << ")?: " ;

            cin >> a ;

             if (a>=c&&a<=b&&b!=c)
            {
                s[i+1]=a ;

                i++ ;
            }

            else if (a<c||a>b)
            {
                cout << "Out of range!\n" ;

                a=c-1 ;
            }

        }

        if (a==number)
        {
            cout << "You win!\n" ;

            break ;
        }

        if (x==1)//escaping from the loop
        {
            break ;
        }

    }

    for (int y=1;y<=i;y++)
    {
        cout << y << " - " << s[A] << endl ;

        A++ ;
    }

    return 0 ;
}
