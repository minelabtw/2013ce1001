//============================================================================
// Name        : E9-102502038-KAI.cpp
// Author      : catLee
// Version     : 0.1 KAI
// Description : NCU CE1001 E9 (KAI)
//============================================================================                                                                         
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
using namespace std;

int main(void){
  int ram,num,header = 100,footer = 0; 
  srand(time(NULL));  //rand init
  ram = (rand() % 101);  //call rand
  /*
   linked array list
   int num
   int *ptrNext
   int *Array[2] = {*ptr_num,*ptr_ptrNext}
   int **ptr_PresentListItem  //ram ptr,moving place to place while working
   
   every time add a new item,malloc a new space for int *Array,save into *ptrNext
   then,chroot ptr_PresentListItem to the new Item
   malloc a num for it,assign NULL ptr for *ptrNext
   once sure the item is used,do above again
   */
  //===linked array list init===
  int *listHead[2];
  int **ptr_PresentListItem = listHead;
  *ptr_PresentListItem = (int *)malloc(sizeof(int));
  ptr_PresentListItem++;
  *ptr_PresentListItem = NULL;
  ptr_PresentListItem--;
  //============================
  while(1){  //main loop,receive number
    if(header == footer){
      cout << "You lose! Answer is " << ram << "\n";  //end game
      break;
    }
    cout << "Enter number(" << footer << "<=number<=" << header << ")?: ";
    cin >> num;
    if(!((num <= header)&&(num >= footer))){
      cout << "Out of range!\n";
      continue;
    }
    //===linked array list logging,don't mind===
    **ptr_PresentListItem = num;
    ptr_PresentListItem++;
    *ptr_PresentListItem = (int *)malloc(2*sizeof(int*));
    ptr_PresentListItem = (int **)*ptr_PresentListItem;
    *ptr_PresentListItem = (int *)malloc(sizeof(int));
    ptr_PresentListItem++;
    *ptr_PresentListItem = NULL;
    ptr_PresentListItem--;
    //==========================================
    ((ram > num) ? footer : header) = ((ram > num) ? (num + 1) : (num - 1)); //adjust header or footer
    if(num == ram){
      cout << "You win!\n";  //end game
      break;
    }
  }
  ptr_PresentListItem = listHead;
  for(int i = 1;true;i++){  //sub loop,echo log
    *ptr_PresentListItem++;
    if(*ptr_PresentListItem == NULL){
      break;
    }
    *ptr_PresentListItem--;
    cout << i << " - " << **ptr_PresentListItem <<"\n";
    ptr_PresentListItem++;
    ptr_PresentListItem = (int **)*ptr_PresentListItem;
  }
  return 0;
}
