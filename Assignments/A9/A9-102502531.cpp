#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    int a=0; //設變數時間
    int b=0;
    int c=100;
    int d=0;
    int array[101]={};
    int i=1;
    srand(time(0));
    a=rand()%101+1;

    do //先做一次
    {
        if (b!=c) //判斷哪邊該換哪邊需要結束
        {
            cout<<"Enter number("<<b<<"<=number<="<<c<<")?: ";
            cin>>d;
            if(d<=c&&d>=b)
            {
            array[i]=d;
            i++;
            }
        }
        else
        {
            cout<<"You lose! Answer is "<<b<<endl;
            break;
        }
        if (d>c||d<b)
        {
            cout<<"Out of range!"<<endl;
        }
        else if (d>a)
        {
            c=d-1;
        }
        else if (d<a)
        {
            b=d+1;
        }
        else if (d=a)
        {
            cout<<"You win!"<<endl;
            break;
        }
    }
    while (d!=a||b!=c);

    for (int i=1,b=1;i<=array[i];i++,b++) //印出來
    {
        cout<<b<<" - "<<array[i]<<endl;
    }

    return 0;
}
