#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int ans,i,x;  //解答 變數 你所猜的數字
    int step[101];  //儲存輸入的數字
    int l=0,h=100,p=1;  //最小值  最大值  計錄陣列有多長
    srand(time(0));
    ans=rand()%101;
    cout<<"Enter number("<<l<<"<=number<="<<h<<")?: ";
    while(cin>>x)
    {
        while(x<l || x>h)  //計算是否超出範圍
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number("<<l<<"<=number<="<<h<<")?: ";
            cin>>x;
        }
        if(x<ans)
        {
            l=x+1;
            step[p]=x;  //儲存變數
            p++;
            if(l==h)
            {
                cout<<"You lose! Answer is "<<ans<<endl;
                break;
            }
        }
        else if(x>ans)
        {
            h=x-1;
            step[p]=x;  //儲存變數
            p++;
            if(l==h)
            {
                cout<<"You lose! Answer is "<<ans<<endl;
                break;
            }
        }
        else
        {
            step[p]=x;  //儲存變數
            p++;
            cout<<"You win!"<<endl;
            break;
        }
        cout<<"Enter number("<<l<<"<=number<="<<h<<")?: ";
    }
    for(i=1; i<p; i++)  //輸出你所輸入的數字
        cout<<i<<" - "<<step[i]<<endl;
    return 0;
}
