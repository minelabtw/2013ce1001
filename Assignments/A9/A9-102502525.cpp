#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    int number = -1;    //預設number為-1
    int Max = 100;      //宣告最大值
    int Min = 0;        //宣告最小值
    int re[100]= {};
    int coun=0;
    srand(time(0));             //以time(0)做seed
    int randNum = rand()%101;   //設定數字，範圍為0-100
    while(randNum != number)
    {
        do
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
            cin >> number;
            if(number < Min || number > Max)
            cout << "Out of range!" << endl;
        }
        while(number < Min || number > Max);
        re[coun] = number;
        coun++;
        if(number == randNum)       //猜中數字
            cout << "You win!" <<endl;
        else if(number > randNum)
            Max = number;
        else
            Min = number;
        if(number == Max)
            Max--;
        if(number == Min)
            Min++;
        if(Max == Min)
        {
            cout << "You lose! Answer is " << Max <<endl;
            break;
        }
    }

    for(int i=0; i<coun; i++) //coun設為浮動的上限，值為我們輸入幾次number

    {
        cout<<i+1<<" - "<<re[i]<<endl;//i+1為第幾次輸入
    }
    return 0;
}


