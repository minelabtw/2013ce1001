#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int number;//宣告型別為整數的nmber儲存隨機設定的數
    int Max=100,Min=0;//宣告型別為整數的Max=100,Min=0儲存最大最小值
    int a;//宣告型別為整數的a儲存使用者輸入的數
    int arr[101]= {0}; //宣告一個陣列記錄使用者輸入的數字
    int j=1;
    srand(time(0));//讓number等於0~100間隨機設定的數
    number=rand()%101;

    while(1)//讓使用者猜數字
    {
        if(Max==Min)//判斷最大值是否等於最小值
        {
            cout << "You lose! Answer is " << number << endl;
            for(int i=1; i<j; i++)//輸出使用者輸入的數字
            {
                cout << i << " - " << arr[i] << endl;
            }
            break;
        }
        cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
        cin >> a;
        if(a<Min || a>Max)//判斷a是否<最小值或>最大值
        {
            cout << "Out of range!\n";
        }
        else if(a<number)//判斷a是否<隨機設定的數字
        {
            arr[j]=a;//用陣列記錄輸入的數字
            j++;
            Min = a+1;
        }
        else if(a>number)//判斷a是否>隨機設定的數字
        {
            arr[j]=a;
            j++;
            Max = a-1;
        }
        else if(a==number)//判斷a是否=隨機設定的數字
        {
            cout << "You win!\n";
            arr[j]=a;
            j++;
            for(int i=1; i<j; i++)//輸出使用者輸入的數字
            {
                arr[j]=a;
                j++;
                cout << i << " - " << arr[i] << endl;
            }
            break;
        }

    }

    return 0;
}
