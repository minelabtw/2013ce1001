#include <iostream>
#include <stdlib.h>
using namespace std;
void Count( int, int &, int &, int );
main()
{
    int number=rand()%101;  //設答案
    int test=0;
    int least=0;  //最小限制
    int most=100;  //最大限制
    int data[ 100 ]={};  //輸入紀錄
    int i=0;

    do  //重複至成功或失敗
    {
        do  //檢測合理性
        {
            cout << "Enter number(" << least << "<=number<=" << most << ")?: ";
            cin >> test;
        }
        while( test>most && cout << "Out of range!\n" || test<least && cout << "Out of range!\n");
        data[ i ]=test;  //紀錄輸入
        i++;
        Count( test, least, most, number );
    }
    while( test!=number && least!=most );

    if( test==number )  //答對
        cout << "You win!\n";
    if( least==most )  //輸
        cout << "You lose! Answer is " << number << endl;

    for( int j=0; j<i; j++)  //輸出紀錄
    {
        cout << j+1 << " - " << data[ j ] << endl;
    }

    return 0;
}
void Count( int w, int &x, int &y, int z )  //調整上下限
{
    if( w > z )
        y=w-1;
    if( w < z )
        x=w+1;
}
