#include<iostream>
#include<cstdlib>
#include<ctime>
#include<iomanip>
using namespace std;

int main()
{
    int a=1;
    const int boxsize=100;  //宣告陣列
    int box[boxsize]= {}; //初始化陣列
    int number , guess , upper=101, lower=-1;  //宣告變數
    srand(time(NULL));
    number = rand() % 101;  //決定變數
    cout << "Enter number(0<=number<=100)?: ";  //顯示
    cin >> guess;
    while(guess>100 or guess<0)  //當guess>100或guess<0進入迴圈
    {
        cout << "Out of range!" <<endl;
        cout << "Enter number(0<=number<=100)?: ";
        cin >> guess;
    }
    box[0]=guess;  //將第一次輸入的guess存到陣列的0號位
    while(guess!=number)
    {
        if (guess>number)  //若guess>number
        {
            upper=guess;  //upper會等於guess
        }
        else if(guess<number)
        {
            lower=guess;
        }
        if (upper-1==lower+1 )  //猜輸的狀況
        {
            cout << "You lose! Answer is " << number << endl;
            break;  //跳脫迴圈
        }
        cout << "Enter number(" << lower+1 << "<=number<=" << upper-1 << ")?: ";  //輸出提示
        cin >> guess;

        while(guess>=upper or guess<=lower)
        {
            cout << "Out of range!" <<endl;
            cout << "Enter number(" << lower+1 << "<=number<=" << upper-1 << ")?: ";
            cin >> guess;
        }
        box[a]=guess;  //將接下來的guess接續放入陣列
        a++;
    }
    if(guess==number) //猜贏的狀況
    {
        cout << "You Win!" << endl;
    }
    for(int b=0 ; b<=a-1 ; b++ )//輸出整個過程
        cout << b+1 << setw(3) << "-" << setw(3) << box[b] << endl;
    }
    return 0;
}
