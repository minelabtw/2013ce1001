#include<iostream>
#include<time.h>
#include<stdlib.h>
#include<stdio.h>
#include<vector>
using namespace std;
int main()
{

    int i = 0;
    int j = 0;
    int a[101]= {0};
    int number;
    int guess=101;
    int x1 = 0;
    int x2 = 100;
    number = (rand()%100);                             //random number
    while(guess!=number)
    {
        cout << "Enter number(" << x1 << "<=number<=" << x2 << ")?:";
        cin >> guess;
        while(guess<x1||guess>x2)                      //guess is between 0~100
        {
            cout << "Out of range!\n";
            cout << "Enter number(" << x1 <<"<=number<=" << x2 << ")?:";
            cin >> guess;                              //guess is between 0~100
        }
        a[i] = {guess};
        if(guess>number)x2 = guess-1;
        if(guess<number)x1 = guess+1;
        i = i+1;
        if(x1==x2)                                     //Lose
        {
            cout << "You lose! Answer is " << number << "\n";
            break;
        }
        if(number==guess)                              //Win
        {
            cout << "You win!\n";
            break;
        }
    }
    j=i;
    for(i = 0; i<=j-1; i++)
        cout << i+1 << " - " << a[i] << "\n";
    return 0;
}
