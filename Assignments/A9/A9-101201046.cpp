/*
Method 1 better than Method 2.
And Method 1 is safety.
*/
#define Method 2

#if Method == 1 //Mthod 1

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sstream>

using namespace std;

#define N 101

int main(void) {
    int ans, lb = 0, ub = N - 1, num;
    int cnt = 1;
    stringstream log;

    srand(time(NULL)); //initial random seed
    ans = rand() / (RAND_MAX / N + 1); //since rand()%N is poor on
                                       //generating 0-N

    do {
        cout << "Enter number(" << lb << "<=number<=" << ub <<")?: ";
        cin >> num; //input number

        /*check out of range*/
        if (num > ub || num < lb) {
            cout << "Out of range!\n";
            continue;
            }

        log << cnt++ << " - " << num << endl; //log  a log for num

        lb = (num < ans) ? (num + 1):lb; //set lower bound
        ub = (num > ans) ? (num - 1):ub; //set upper bound

        ((lb == ub) && // lose condition
            (cout << "You lose! Answer is " << ans << endl)) ||
        ((num == ans) && //win condition
            (cout << "You win!\n"));

        } while(num != ans && lb != ub);

    cout << log.str();

    return 0;
    }

#else //Method 2

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

#define N 101

int main(void) {
    int ans, lb = 0, ub = N - 1, num;
    int cnt = 1;
    int log[N + 10]; //+ 10 is for giving it a space

    srand(time(NULL)); //initial random seed
    ans = rand() / (RAND_MAX / N + 1); //since rand()%N is poor on
                                       //generating 0-N
    ans = 75;

    do {
        cout << "Enter number(" << lb << "<=number<=" << ub <<")?: ";
        cin >> num; //input number

        /*check out of range*/
        if (num > ub || num < lb) {
            cout << "Out of range!\n";
            continue;
            }

        log[cnt++] = num; //log num  into log

        lb = (num < ans) ? (num + 1):lb; //set lower bound
        ub = (num > ans) ? (num - 1):ub; //set upper bound

        ((lb == ub) && // lose condition
            (cout << "You lose! Answer is " << ans << endl)) ||
        ((num == ans) && //win condition
            (cout << "You win!\n"));

        } while(num != ans && lb != ub);

    for (int i = 1; i < cnt; i++)
        cout << i << " - " << log[i] << endl;

    return 0;
    }


#endif
