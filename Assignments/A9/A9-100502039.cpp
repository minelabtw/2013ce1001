#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

int main(){

    int index = 0;
    int array[100];
    srand(time(0));
    int target = rand()%101;
    int input, status = 0;
    int upperbound = 100;
    int lowerbound = 0;
    cout << target;

    do{
        cout << "Enter number(" << lowerbound << "<=number<=" << upperbound << "): ";
        cin >> input;
        if(input<lowerbound || input>upperbound)
            cout << "Out of range!" << endl;
        else if(input == target){
            cout << "You win!";
            status = 1;
            array[index] = input;
             index++;
        }
        else if(input < target){
             if(target==upperbound && input==(target-1)){
                 cout << "You lose! The answer is : " << target;
                 status = 1;
             }
             lowerbound = input+1;
             array[index] = input;
             index++;
        }
        else if(input > target){
             if(target==lowerbound && input==(target+1)){
                 cout << "You lose! The answer is : " << target;
                 status = 1;
             }
             upperbound = input-1;
             array[index] = input;
             index++;
        }
    }while(status == 0);

    for(int i=0; i<index; i++)
        cout << endl << i+1 << " : " << array[i];

    return 0;
}
