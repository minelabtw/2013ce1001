#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main ()
{
    srand ( time( 0 ) );
    int randNum = rand()%101;  //設亂數0-100
    int number = 0,time = 0,MAX = 100,MIN = 0;  //設玩家猜猜的變數,猜的次數,最大值,最小值
    int record[100] = {0};  //設一個陣列可以存100個數拿來存每次輸入的數

    cout << "Enter number(0<=number<=100)?: ";
    cin >> number;
    if ( MIN <= number && number <= MAX)  //如果猜的數沒有超出範圍就將她依次記錄在record陣列中
    {
        record[time] = number;
        time++;
    }
    while ( number != randNum )
    {
        if ( number > MAX || number < MIN )  //猜的數超出範圍輸出Out of range!然後讓玩家重新輸入
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?: ";
            cin >> number;
            if ( MIN <= number && number <= MAX )  //如果猜的數沒有超出範圍就將她依次記錄在record陣列中
            {
                record[time] = number;
                time++;
            }
        }
        else if ( number > randNum )  //猜的數大於答案,最大值變成所猜的數減一
        {
            MAX = number - 1;
            if ( MAX == MIN )  //若最大值等於最小值(只剩一個數能猜)就輸出You lose!及答案
            {
                cout << "You lose! Answer is " << randNum << endl;
                break;
            }
            cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?: ";
            cin >> number;
            if ( MIN <= number && number <= MAX)  //如果猜的數沒有超出範圍就將她依次記錄在record陣列中
            {
                record[time] = number;
                time++;
            }
        }
        else if ( number < randNum )  //若猜的數小於答案,最小值變成猜的數加一
        {
            MIN = number + 1;
            if ( MAX == MIN )  //若最大值等於最小值(只剩一個數能猜)就輸出You lose!及答案
            {
                cout << "You lose! Answer is " << randNum << endl;
                break;
            }
            cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?:";
            cin >> number;
            if ( MIN <= number && number <= MAX )  //如果猜的數沒有超出範圍就將她依次記錄在record陣列中
            {
                record[time] = number;
                time++;
            }
        }
    }
    if ( number == randNum )  //若猜的數跟答案一樣就輸出You win!
    {
        cout << "You win!" << endl;
    }
    for(int i=0; i<time; i++)  //輸出玩家猜了幾次及每次的答案,從鎮咧裡找從第一次到最後一次所猜的數
    {
        cout << i+1 << " - " << record[i] << endl;
    }

    return 0;
}
