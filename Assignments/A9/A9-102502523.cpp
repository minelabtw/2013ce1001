#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    srand(time(0));
    int number;//宣告變數
    int randNum=rand()%101;
    int maxx=100;
    int minn=0;
    const int lengh=99;
    int iarr[lengh]= {};//宣告陣列
    int time=0;//宣告變數
    cout<<"Enter number(0<=number<=100)?: ";
    cin>>number;//輸入數字
    while(number<0||number>100) //若超入範圍則進入迴圈
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>number;
    }
    iarr[time]=number;//把數字帶入陣列
    time++;
    while(1) //進入迴圈
    {
        if (number==randNum)//判斷number
        {
            int time2=time;//宣告第二個次數變數
            time=0;//重置變數
            cout<<"You win!"<<endl;

            for(int a=1; time2>0; time2--,a++)
            {

                cout<<a<<" - "<<iarr[time]<<endl;
                time++;
            }
            break;
        }
        else if (number>randNum)
        {
            maxx=number-1;
            if (minn==maxx)
            {
                int time2=time;//宣告第二個次數變數
                time=0;//重置變數
                cout<<"You lose! Answer is "<<randNum<<endl;
                for(int a=1; time2>0; time2--,a++)//印出每次的結果
                {

                    cout<<a<<" - "<<iarr[time]<<endl;
                    time++;
                }
                break;
            }
            else
            {
                cout<<"Enter number("<<minn<<"<=number<="<<maxx<<")?: ";

                cin>>number;//再次猜測number的值
                while(number<minn||number>maxx)
                {
                    cout<<"Out of range!"<<endl;
                    cout<<"Enter number("<<minn<<"<=number<="<<maxx<<")?: ";
                    cin>>number;//超出範圍重新輸入number
                }
            }
        }
        else if (number<randNum)
        {
            minn=number+1;
            if (minn==maxx)//判斷number
            {
                int time2=time;
                time=0;
                cout<<"You lose! Answer is "<<randNum<<endl;
                for(int a=1; time2>0; time2--,a++)
                {

                    cout<<a<<" - "<<iarr[time]<<endl;
                    time++;
                }
                break;
            }
            else
            {
                cout<<"Enter number("<<minn<<"<=number<="<<maxx<<")?: ";

                cin>>number;//再次猜測number的值
                while(number<minn||number>maxx)
                {
                    cout<<"Out of range!"<<endl;
                    cout<<"Enter number("<<minn<<"<=number<="<<maxx<<")?: ";
                    cin>>number;//超出範圍重新輸入number
                }
            }
        }
        iarr[time]=number;//將數字代入陣列
        time++;
    }
    return 0;
}
