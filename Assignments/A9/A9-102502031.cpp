#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void inverse(int objection[], int key)
{
    for (int i=0; i<=100; i=i+1)
        if (objection[i]==key)
            cout << i;
}

int main()
{
    srand(time(0));
    int number=rand()%101;  //randon number in the range [0,100]
    int up_limit=100;
    int down_limit=0;
    int x=0;                //input from user
    int record[100]= {};    //place to store oridinal
    int ordinal=0;          //mark the ordinal of the input in record[]

    do
    {
        {
            cout << "Enter number(" << down_limit << "<=number<=" << up_limit << ")?: ";
            cin >> x;
            if (x>up_limit || x<down_limit)
                cout << "Out of range!" << endl;   //reprompt x while input is out of range [down_limit,up_limit]
            else
            {
                ordinal=ordinal+1;
                record[x]=ordinal;   //record the ordinal in record[]
                if (x==number)
                    cout << "You win!" << endl;                        //user is win while input is number
                else if ((up_limit==x+1 && x<number) || (down_limit==x-1 && x>number))
                {
                    cout << "You lose! Answer is " << number << endl;  //user is lose while the range have only a number
                    x=number;                                          //end while
                }
                else if (x<number)
                    down_limit=x+1;   //change the down limit while user guess wrong number and the range have still two or more number
                else if (x>number)
                    up_limit=x-1;     //change the up limit while user guess wrong number and the range have still two number or above
            }
        }
    }
    while (number!=x);   //end while after user get number value not matter he is win or lose

    for (int i=1; i<=ordinal; i=i+1)
    {
        cout << i << " - ";
        inverse(record, i);
        cout << endl;
    }
    return 0;
}
