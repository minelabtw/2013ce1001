#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    srand(time(0));//時間亂數
    int guess=102,l=0,h=100,temp=rand()%101,num[100]={},i=0;//宣告
    while((guess<l || guess>h)&&(guess!=temp && h!=l))//判斷
    {
        cout<<"Enter number("<<l<<"<=number<="<<h<<")?: ";
        cin >> guess;
        num[i]=guess;
        if(guess<l || guess>h)//判斷有無超出範圍
            cout<<"Out of range!\n";
        h=(temp<=guess&&guess<=h)?guess-1:h;//改變頂值
        l=(temp>=guess&&guess>=l)?guess+1:l;//改變底值
        i++;
    }
    (guess==temp)?cout<<"You win!":cout<<"You lose! Answer is "<<temp<<endl;//判斷顯示
    for(int j=0;j<i;j++)//顯示結果
        cout<<j+1<<" - "<<num[j]<<endl;
    return 0;
}
