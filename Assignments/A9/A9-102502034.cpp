#include<iostream>
#include<cstdlib>
#include<ctime>
#include<iomanip>
using namespace std;
int main()
{
    int i=1;
    const int boxsize=100 ;//宣告陣列
    int box[boxsize]={} ;//初始化陣列
    int number , guess, upper=101, lower=-1;//宣告變數
     srand(time(NULL));
    number = rand() % 101 ;//決定變數
    cout << "Enter number(0<=number<=100)?: " ;//輸出輸入題目所需
    cin >> guess ;
    while(guess>100 or guess<0)
    {
        cout <<"Out of range!" <<endl;
        cout <<"Enter number(0<=number<=100)?: ";
        cin >>guess ;
    }
    box[0]=guess; //將第一次輸入的GUESS存到陣列的0號位
    while(guess != number)//當guess不等於number
    {


        if (guess>number)//若guess > number
        {
            upper=guess;//upper會等於guess
        }
        else if(guess <number)//若guess < number
        {
            lower=guess ;//lower 會等於 guess
        }
        if (upper-1 == lower+1 )//若upper-1等於lower+1
        {
            cout <<"You lose! Answer is "<<number<<endl;//輸出你輸了
            break;//跳脫迴圈
        }
       cout << "Enter number(" << lower+1 << "<=number<=" << upper-1 << ")?: ";//輸出提示
       cin >> guess ;//輸入guess

       while(guess>=upper or guess<=lower)//輸出out of range
       {
           cout <<"Out of range!" <<endl;
           cout <<"Enter number(" << lower+1 << "<=number<=" << upper-1 << ")?: ";
           cin >> guess;
       }
        box[i]=guess;//將接下來的GUESS接續放入陣列
           i++;
    }
    if(guess==number)//猜贏的狀況
    {
        cout <<"You Win!"<<endl;
    }
    for(int j=0 ; j<=i-1 ; j++ )//輸出1 - guess ......
    {
    cout << j+1 <<setw(3)<<"-"<<setw(3)<<box[j]<<endl;
    }
    return 0;
}
