#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    srand(time(NULL));//以時間作亂數種子

    int num;//密碼
    int ans;//輸入的數字
    int min=0;//最小值0
    int max=100;//最大值100
    int temp[100]= {};//宣告記錄用陣列並初始為0
    int i=0;//用來操作陣列索引

    num=rand()%101;//取亂數(0~100)

//不斷縮小範圍and檢查是否超出範圍
    while(min!=max)
    {
        cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
        cin>>ans;

        if(ans>=min&&ans<=max)
        {
            temp[i]=ans;//將每次結果存入temp
            i++;//存完將索引+1
        }

        if(ans<min||ans>max)
        {
            cout<<"Out of range!"<<endl;
        }
        else if(ans<num)
        {
            min=ans+1;
        }
        else if(ans>num)
        {
            max=ans-1;
        }
        else
        {
            cout<<"You win!"<<endl;

            for(int a=0; a<i; a++)//印出紀錄
            {
                if(a==i-1)
                    cout<<a+1<<" - "<<temp[a];
                else
                    cout<<a+1<<" - "<<temp[a]<<endl;
            }

            return 0;
        }
    }

    cout<<"You lose! Answer is "<<num<<endl;

    for(int a=0; a<i; a++)//印出紀錄
    {
        if(a==i-1)
            cout<<a+1<<" - "<<temp[a];
        else
            cout<<a+1<<" - "<<temp[a]<<endl;
    }

    return 0;
}
