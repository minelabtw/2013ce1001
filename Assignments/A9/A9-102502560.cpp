#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
	srand(time(NULL));				//use now time as seed
	int rmin=0,rmax=100;			//set the initial range
	int number=75;					//get the random number 0~100
	int record[100]={}, datanum=0;	//build record list and position index to record
	while(1){
		int mynum;
		cout << "Enter number(" << rmin << "<=number<=" << rmax << ")?: ";
		cin >> mynum;

		if(mynum<rmin || mynum>rmax){
			cout << "Out of range!\n";
		}else if(mynum<number){
			rmin=mynum+1;						//modifiy the range
			record[datanum++]=mynum;			//write record
		}else if(mynum>number){
			rmax=mynum-1;						//modifiy the range
			record[datanum++]=mynum;			//write record
		}else{
			record[datanum++]=mynum;			//write record
			cout << "You win!\n"; break;		//win
		}

		if(rmin==rmax){cout << "You lose! Answer is " << number << "\n"; break;}		//lose
	}

	for(int i=0;i<datanum;i++){cout << i+1 << " - " << record[i] << "\n";}				//print records

	return 0;
}
