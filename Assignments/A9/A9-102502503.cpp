#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0));
    rand();
    int number;
    int record[100]= {}; //宣告record陣列
    int answer=rand()%101;  //用rand設定答案值為0~100
    int high=101;  //設定max值
    int low=-1;  //設定min值
    int i=0;
    int counter=0;  //計算輸入多少次紀錄

    cout << "Enter number(0<=number<=100)?: ";
    cin >> number;

    while(number>100 or number<0)  //用while迴圈判斷輸入的值是否在範圍內
    {
        cout << "Out of range!" << endl << "Enter number(0<=number<=100)?: ";
        cin >> number;

    }
    if (0<=number<=100)
    {
        record[counter]=number;  //將第一次輸入的值放在record陣列0的位置
        counter++;  //紀錄+1
    }

    if (number==answer)  //若輸入的值等於答案的值
    {
        cout << "You win!";
    }
    while (number!=answer && low+1!=high-1)  //當輸入的值不等於答案或是Max值不等於min值則進行下列程式,否則停止
    {


        if (number>answer)  //若輸入的值大於答案
        {
            high=number;  //將最大值改為輸入的值
            if (low+1==high-1)
            {
                cout << "You lose! Answer is " << answer;
            }
            else  //輸出新範圍
            {
                cout << "Enter number(" << low+1 << "<=number<=" << high-1 << ")?: ";
                cin >> number;
                while (number>high-1 or number<low+1)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter number(" << low+1 << "<=number<=" << high-1 << ")?: ";
                    cin >> number;
                }
            }
        }


        else if (number<answer)  //若輸入的值小於答案
        {
            low=number;  //將最小值改為輸入的值
            if (low+1==high-1)
            {
                cout << "You lose! Answer is " << answer;
            }
            else  //輸出新範圍
            {
                cout << "Enter number(" << low+1 << "<=number<=" << high-1 << ")?: ";
                cin >> number;
                while (number>high-1 or number<low+1)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter number(" << low+1 << "<=number<=" << high-1 << ")?: ";
                    cin >> number;
                }
            }
        }
        else  //若輸入的值等於答案的值
        {
            cout << "You win!";
        }
        if (low+1<=number && number<=high-1 && low+1!=high-1)  //若輸入的值在正確範圍
        {
            record[counter]=number;  //將輸入的值一一存放在record陣列
            counter++;  //紀錄遞增
        }
    }
    for (i=0; i<counter; i++)
    {
        cout << endl << i+1 << " - " << record[i];  //輸出紀錄
    }
    return 0;
}
