#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int number = -1;                   //設為-1可先進入迴圈
    int Max = 100;
    int Min = 0;

    srand(time(0));
    int randNum = rand()%101;
    int times=1;
    int n[100]= {};                    //設一個可儲存100個數的陣列

    while(randNum != number)
    {

        do                             //判定輸入的數介在max與min之間
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
            cin >> number;
            if(number < Min || number > Max)
                cout << "Out of range!" << endl;
        }
        while(number < Min || number > Max);

        n[times] = number;             //將輸入的數傳入陣列

        if(number == randNum)          //贏的狀況
        {
            cout << "You win!"<<endl;
            times++;
        }
        else if(number > randNum)      //輸入的數大於亂數
        {
            Max = number-1;
            times++;
        }
        else                           //輸入的數小於亂數
        {
            Min = number+1;
            times++;
        }

        if(Max == Min)                 //輸的狀況
        {
            cout << "You lose! Answer is " << Max<<endl;
            break;
        }
    }

    for (int i=1; i<times; i++)        //顯示先前輸入過且有效的數
        cout<<i<<" - "<<n[i]<<endl;

    return 0;
}


