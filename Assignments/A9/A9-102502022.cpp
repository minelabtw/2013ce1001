#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{

    srand(time(0));
    int randx= (rand() % 101);        //宣告名稱為randx並且介於0到100的亂數作為變數
    int x=200;                     //宣告變數並設初始值符合範圍以便一開始便能進入迴圈
    int Min=0;                     //宣告變數
    int Max=100;
    int temp=0;
    int a[100];                  //宣告陣列  大小為100
    int variable=1;

    while(x!=randx)                //迴圈
    {
        cout<< "Enter number("<< Min<< "<=number<="<< Max<< ")?: ";
        cin>> x;

        while(x<Min || x>Max)      //迴圈
        {
            cout<< "Out of range!\n"<<"Enter number("<< Min<< "<=number<="<< Max<< ")?: ";
            cin>> x;
        }
        if(x<randx)               //假設
        {
            Min=x+1;
            temp=temp+1;         //次數加一
        }
        if(x>randx)
        {
            Max=x-1;
            temp=temp+1;
        }
        if(Min==Max)
        {
            cout<< "You lose! Answer is "<< randx<< endl;        //傳入變數的值
            temp=temp+1;
            break;                //跳出迴圈
        }
        if(x==randx)
        {
            cout<< "You win!"<<endl;
            temp=temp+1;
        }
        a[variable]=x;               //宣告陣列存入輸入的值
        variable++;
    }

    for(int i=1; i<=temp; i++)             //迴圈
    {
        cout<< i<< " - "<< a[i]<< endl;     //輸出陣列
    }

    return 0;
}
