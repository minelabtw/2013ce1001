#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int answer = 0;
    int number[100];
    int maximum = 100;
    int minium = 0;
    int p = 1;
    int i;

    srand(time(NULL));//設定亂數
    answer = (rand()%101);

    cout << "Enter number(" << minium << "<=number<=" << maximum << ")?: ";
    while ( cin >> number[p] )//當輸入時做下列動作
    {
        if ( number[p] > maximum || number[p] < minium )
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << minium << "<=number<=" << maximum << ")?: ";
        }

        else if ( number[p] == answer )//當數字與答案一樣時叫出字彙
        {
            cout << "You win!" <<endl;
            break;
        }

        else if ( number[p] > answer )//當數字比答案大時更改範圍
        {
            maximum = number[p] - 1;
            if ( maximum == minium )
            {
                cout << "You lose! Answer is " << answer << endl;
                break;
            }
            cout << "Enter number(" << minium << "<=number<=" << maximum << ")?: ";
            p++;//將號碼加一
        }

        else if ( number[p] < answer )//當數字比答案小時更改範圍
        {
            minium = number[p] + 1;
            if ( maximum == minium )
            {
                cout << "You lose! Answer is " << answer << endl;
                break;
            }
            cout << "Enter number(" << minium << "<=number<=" << maximum << ")?: ";
            p++;//將號碼加一
        }
    }

    for( i = 1 ; i < p + 1 ; i++ )
    {
        cout << i << " - " << number[i] << endl;
    }

    return 0;
}
