#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int i = 0 ;
    int number [i];
    int answer;
    int celling = 100;
    int bottom = 0;

    srand( time(NULL) );    //設定變數種子
    answer = rand() % 101;

    do
    {
        if ( celling == answer && bottom == answer )    //輸
        {
            cout << "You lose! Answer is " << answer << endl;
            for ( int x = 1 ; x <= i ; x++ )
                cout << x << " - " << number[x] <<  endl ;
            return 0;
        }
        i++;
        cout << "Enter number(" << bottom << "<=number<=" << celling << ")?: " ;
        cin >> number[i];
        if ( number[i] < bottom || number[i] > celling )
        {
            cout << "Out of range!" << endl;
            i--;
        }
        else if ( number[i] < answer )
            bottom = number[i] + 1 ;
        else if ( number[i] > answer )
            celling = number[i] - 1 ;
    } while ( number[i] != answer ) ;   //猜數字迴圈

    cout << "You win!" << endl;     //贏
    for ( int x = 1 ; x <= i ; x++ )
        cout << x << " - " << number[x] << endl ;

    return 0;
}
