#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main(){
    int answer;  //宣告變數
    int input;
    int up = 100;
    int down = 0;
    int counter = 0;
    int record[101] = {};

    srand(time(0));  //設定亂數種子
    answer = rand() % 101;  //設定亂數

    do{
        do{  //輸入數字並判斷有無超出範圍
            cout << "Enter number(" << down << "<=number<=" << up << ")?: ";
            cin >> input;
            if(input > up || input < down){
                cout << "Out of range!" << endl;
            }
        }while(input > up || input < down);

        record[counter] = input;  //存入陣列
        counter++;  //計數器加一

        if(input != answer){  //調整上下範圍
            if(input > answer){
                up = input - 1;
            }else if(input < answer){
                down = input + 1;
            }
        }
        if(up == down){  //判斷輸贏
            cout << "You lose! Answer is " << answer << endl;
            input = answer;
        }else if(input == answer){
            cout << "You win!" << endl;
        }
    }while(input != answer);

    for(int i = 0 ; i < counter ; i++){  //輸出輸入紀錄
        cout << (i + 1) << " - " << record[i] << endl;
    }

    return 0;
}
