#include<iostream>
#include<cstdlib> //rand
#include<ctime> //time

using namespace std;

int main()
{
    srand(time(NULL)); //seed
    int number = rand()%101; //rand a num 0 to 100
    int guess[100], counter=0; //history
    int top=100, bottom=0, input;
    while(1)
    {
        cout << "Enter number(" << bottom << "<=number<=" << top <<")?: ";
        cin >> input;
        if(input<bottom || input>top)
        {
            //input illegal
            cout << "Out of range!" << endl;
            continue;
        }
        guess[counter] = input; //store the input
        counter++;
        if(input==number)
        {
            //win
            cout << "You win!" << endl;
            break;
        }
        else if(input>number)
        {
            top = input-1;  //top fixed
        }
        else
        {
            bottom = input+1; //bottom fixed
        }
        if(top==bottom)
        {
            //lose
            cout << "You lose! Answer is " << number << endl;
            break;
        }
    }
    //output the history
    for(int i=0; i<counter; i++)
    {
        cout << i+1 << " - " << guess[i] << endl;
    }
    return 0;
}
