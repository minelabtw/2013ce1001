#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    int record[50]= {};
    int cal=0;
    int qus, ans;                                       //Line 7~9 define the varibles
    int control;
    int boundt=100, boundb=0;
    int rec[50]= {};
    srand(time(0));                                     //Line 10~11 make a random number
    qus=rand()%101;
    do                                                  //line 12~19 let user enter number for first time
    {
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>ans;
        if(ans<0||ans>100)
            cout<<"Out of range!\n";
        else
            record[cal]=ans;
    }
    while(ans<0||ans>100);
    do                                                   //Line 20~72 let user guess the number
    {
        cal++;
        if(ans==qus)
        {
            record[cal]=ans;
            cout<<"You win!\n";
        }
        else if(ans-boundb==1||ans-boundt==-1)             //Line 26 to 30 when the number that user guessed has only one unit distance form answer
        {                                                  //print that user lose
            cout<<"You lose! Answer is "<<qus<<endl;
            break;
        }
        else if(ans>qus)                  //Line 31 to 70 give the user some hint when he or she doesn't guess the right number
        {
            do
            {
                cout<<"Enter number("<<boundb<<"<=number<="<<ans-1<<")?: ";
                control=ans-1;
                cin>>ans;
                if(ans<boundb||ans>control)
                {
                    cout<<"Out of range!\n";
                    ans=control+1;
                }
                else if(ans==qus)
                {
                    cout<<"You win!\n";
                }
                else
                {
                    record[cal]=ans;
                }
            }
            while(ans<boundb||ans>control);
            boundt=control;
        }
        else if(ans<qus)
        {
            do
            {
                cout<<"Enter number("<<ans+1<<"<=number<="<<boundt<<")?: ";
                control=ans+1;
                cin>>ans;
                if(ans<control||ans>boundt)
                {
                    cout<<"Out of range!\n";
                    ans=control-1;
                }
                else if(ans==qus)
                {
                    cout<<"You win!\n";
                }
                else
                {
                    record[cal]=ans;
                }

            }
            while(ans<control||ans>boundt);
            boundb=control;
        }
    }
    while(qus!=ans);                                                //Line 89 to end: porint out the record user made
    cout<<1<<" - "<<record[0]<<endl;
    if(cal>1)
    {
        for(int i=0; i<cal; i++)
        {
            if(record[i+1]!=0)
                cout<<i+2<<" - "<<record[i+1]<<endl;
        }
        if(qus==ans)
        {
            cout<<cal+1<<" - "<<qus;
        }
    }

    return 0;
}
