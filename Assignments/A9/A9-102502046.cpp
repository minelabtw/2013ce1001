#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;

int main()
{
    srand(time(NULL));          //隨機變數
    int a=0;
    int MIN=0;
    int MAX=100;
    int ans=rand()%101;
    int x[100]={};
    int i=0;

    for(i=0;;i++)                    //進入迴圈
    {
        cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?: " ;
        cin >> a;               //猜數字

        while (a>MAX||a<MIN)
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?: " ;
            cin >> a;
        }
        x[i]=a;         //紀錄輸入了什麼
        if (a<ans)              //變更提示
            MIN = a+1;

        else if (a>ans)
            MAX = a-1;

        if (MIN==MAX)           //判斷輸贏
        {
            cout << "You lose! Answer is " << ans << endl ;
            break;
        }
        if (a==ans)
        {
            cout << "You win!" << endl;
            break;
        }
    }
    for (int j=0 ; j<=i ; j++)          //輸出猜了什麼
        cout << j+1 << " - " << x[j] << endl;
    return 0;

}
