#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int number[99];         //宣告array
    int n1=0,n2=100;        //宣告變數
    int i=0;
    srand(time(0));         //以time(0)做seed
    int answer=rand()%101;  //設亂數

    for(i=0; i<99; i++) //為防止number[i]和answer值相同而無法進入while，所以預設number[i]為-1
    {
        number[i]=-1;
    }

    for(i=0; i<99; i++)
    {
        while(number[i]!=answer)
        {
            do
            {
                cout << "Enter number(" << n1 << "<=number<=" << n2 << ")?: ";  //輸出number範圍的提示字請玩家輸入
                cin  >> number[i];                                              //玩家輸入數字

                if(number[i]<n1 || number[i]>n2)
                    cout << "Out of range!" << endl;
            }
            while(number[i]<n1 || number[i]>n2);

            if(number[i]==answer)      //猜中answer 已贏
            {
                cout << "You win!" << endl;
                break;                 //跳出while迴圈
            }

            else if(number[i]>answer)  //輸入的數字大於要猜的數字
                n2=number[i];          //改變提示字範圍的最大值
            else                       //輸入的數字小於要猜的數字
                n1=number[i];          //改變提示字範圍的最小值

            if(number[i]==n2)          //輸入的數字等於最大值
            {
                n2--;                  //將最大值範圍往內縮，最大值-1
                if(n2==n1)             //如果最大值等於最小值，代表只剩一個數字，所以玩家lose
                    cout << "You lose! Answer is " << n1 << endl;
                break;                 //跳出while迴圈
            }
            if(number[i]==n1)          //輸入的數字等於最小值
            {
                n1++;                  //將最小值範圍往內縮，最小值+1
                if(n2==n1)             //如果最大值等於最小值，代表只剩一個數字，所以玩家lose
                    cout << "You lose! Answer is " << n1 << endl;
                break;                 //跳出while迴圈
            }
        }
        if(number[i]==answer)  //已贏 跳出for迴圈
            break;
        if(n2==n1)             //已輸 跳出for迴圈
            break;
        continue;              //繼續進行for迴圈
    }

    for(i=0; i<99; i++) //輸出過程所猜數字
    {
        if(number[i]!=-1)
            cout << i+1 << " - " << number[i] << endl;
    }

    return 0;
}
