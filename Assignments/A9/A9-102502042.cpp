#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0));
    int ans=rand()%101; //get random num
    int up=100;
    int low=0;
    int num;
    int v[100];
    int id=0;
    cout<<"Enter number(0<=number<=100)?: ";
    while(cin>>num) //input
    {
        while(num<low||num>up)
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number("<<low<<"<=number<="<<up<<")?: ";
            cin>>num;
        }
        v[id++]=num;
        if(num>ans)
            up=num-1;
        else if(num<ans)
            low=num+1;
        if(num==ans)    //win
        {
            cout<<"You win!"<<endl;
            break;
        }
        else if(up==low)    //lose
        {
            cout<<"You lose! Answer is "<<ans<<endl;
            break;
        }
        else
        {
            cout<<"Enter number("<<low<<"<=number<="<<up<<")?: ";
        }
    }
    for(int i=0;i<id;++i)
        cout<<i+1<<" - "<<v[i]<<endl;
    return 0;
}
