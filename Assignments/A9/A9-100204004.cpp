#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()

{
    int counter=0;
    int input=-1;//避免input跟亂數answer相同
    int Min=0;//預設最小值
    int Max=100;//預設最大值
    int number[100]= {};//可以存100數的array，起始值為0

    srand(time(0));//use time(0) as seed
    int answer=rand()%101;//得0-100的亂數


    while(input!=answer)//猜數字loop
    {
        while(1)//確認使用者輸入的值在最大和最小值間
        {
            cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>input;
            if(input<Min||input>Max)
                cout<<"Out of range!"<<endl;
            else
                break;
        }

        if(input<answer)//輸入比答案小，改最小值
        {
            Min=input+1;
            number[counter]=input;//存輸入值到array
        }
        else if(input>answer)//輸入比答案大，改最大值
        {
            Max=input-1;
            number[counter]=input;
        }
        else if(input==answer)//答對
        {
            number[counter]=input;
            cout<<"You win!"<<endl;
            for(int i=0; i<=counter; i++)
                cout<<i+1<<" - "<<number[i]<<endl;//輸出猜的次數，和輸入數字
            break;
        }
        if(Max==Min)//只剩一個數可以猜
        {
            number[counter]=input;
            cout<<"You lose! Answer is "<<Max<<endl;
            for(int i=0; i<=counter; i++)
                cout<<i+1<<" - "<<number[i]<<endl;
            break;
        }
        counter++;
    }
    return 0;
}
