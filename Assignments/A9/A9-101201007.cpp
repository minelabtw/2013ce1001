#include<iostream>
#include<ctime>
#include<cstdlib>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int answer ;
    int do_try ;
    int Min,Max ;
    int i ;
    int data[100] ;                                                   //宣告陣列 有100個空間
    srand(time(0)) ;                                                  //產生亂數

    i=1 ;
    answer =rand()%101;                                                 //選出介於0~100的一個整數亂數
    Min=0;                                                              //設定初始最小範圍
    Max=100;                                                            //設定初始最大範圍
    cout <<"Enter number(0<=number<=100)?: " ;
    cin >> do_try ;
    data[0]=do_try ;
    while (true)
    {
        if (answer==do_try)                                               //輸入值的和亂數跑的相等>答對了
        {
            cout<<"You win!" ;
            break;
        }
        else if (do_try<Min || do_try > Max)                          //不再目前的範圍>out of rangle
        {
            cout << "Out of range!" <<endl ;
            i-- ;
        }
        else if (do_try>answer)                                           //縮小範圍
        {
            Max=do_try-1;
        }
        else if (do_try<answer)                                           //縮小範圍
        {
            Min=do_try+1;
        }
        if (Min!=Max)                                                     //沒有輸掉就繼續
        {
            cout <<"Enter number("<<Min<<"<=number<="<<Max<<")?: " ;
            cin >>do_try;
            data[i]=do_try ;
            i++ ;
        }
        else
        {
            cout <<"You lose! Answer is "<< answer ;
            break;                                                        //輸了結束
        }
    }
    for (int j=0;j<i;++j)
    {
        cout <<endl<<j+1<<" - "<<data[j] ;                                //輸出輸入過的嘗試值
    }
    return 0;
}
