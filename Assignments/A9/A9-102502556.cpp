#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main ()
{
    int ans = 0; //宣告型別為 整數(int) 的第一個變數(ans)，並初始化其數值為0，用來儲存隨機所得到的答案值。
    int input = 0; //宣告型別為 整數(int) 的第二個變數(input)，並初始化其數值為0，用來儲存使用者所輸入的數字。
    int left = 0; //宣告型別為 整數(int) 的第三個變數(left)，並初始化其數值為0，用來儲存左邊的邊界範圍。
    int right = 100; //宣告型別為 整數(int) 的第四個變數(right)，並初始化其數值為100，用來儲存右邊的邊界範圍。
    int data[ 100 ] = {}; //宣告型別為 整數(int) 的一維陣列(data)，並初始化其所有元素為0，用來儲存所有輸入過的數字。
    int counter = 0; //宣告型別為 整數(int) 的第五個變數(counter)，並初始化其數值為0，用來計算總共輸入了幾次。
    srand( time ( NULL ) ); //用srand使之能產生真正的隨機。
    ans = rand() % 101; //用 rand() 產生一個隨機的答案0。
    while ( input != ans ) //當使用者尚未猜到答案時，用while迴圈使其能重複猜謎。
    {
        cout << "Enter number(" << left << "<=number<=" << right << ")?: ";
        cin >> input;
        while( input < left || input > right ) //檢驗使用者所輸入的數字是否合乎標準，若不符，則要求其重新輸入。
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << left << "<=number<=" << right << ")?: ";
            cin >> input;
        }
        data[ counter ] = input; //把輸入成功的結果儲存到 data 這個陣列裡
        counter++;
        if ( input < ans ) //調整左邊界
        {
            left = input + 1;
        }
        else if ( input > ans ) //調整右邊界
        {
            right = input - 1;
        }
        else
        {
            cout << "You win!" << endl;
        }
        if ( left == right )
        {
            cout << "You lose! Answer is " << ans << endl;
            break;
        }
    }
    for ( int i = 0 ; i < counter ; i++ ) //用for迴圈輸出整個猜數字的過程
    {
        cout << i + 1 << " - " << data[ i ] << endl;
    }
    return 0;
}
