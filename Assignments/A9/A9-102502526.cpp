#include <iostream>
#include <cstdlib>      //contain srand() and rand()
#include <ctime>        //contain time()
using namespace std;

int main()
{
    int number = -1;    //為防止number和randNum值相同而無法進入while，所以預設number為-1
    int Max = 100;      //預設最大值
    int Min = 0;        //預設最小值
    int re[100]={};
    int counter=0;

    srand(time(0));             //以time(0)做seed
    int randNum = rand()%101;   //隨機設定要猜的數字(0<=randNum<=100)

    while(randNum != number)
    {
        do
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";    //輸出number範圍的提示字請玩家輸入
            cin >> number;  //玩家輸入數字
            if(number < Min || number > Max)    //若輸入的數值不在number提示字的範圍則Out of range!
                cout << "Out of range!" << endl;
        }
        while(number < Min || number > Max);


    re[counter] = number;
    counter++;

        if(number == randNum)       //猜中數字
            cout << "You win!" <<endl;
        else if(number > randNum)   //輸入的數字大於要猜的數字
            Max = number;           //改變提示字範圍的最大值
        else                        //輸入的數字小於要猜的數字
            Min = number;           //改變提示字範圍的最小值

        if(number == Max)           //輸入的數字等於最大值
            Max--;                  //將最大值範圍往內縮，最大值-1
        if(number == Min)           //輸入的數字等於最小值
            Min++;                  //將最小值範圍往內縮，最小值+1

        if(Max == Min)              //如果最大值等於最小值，代表只剩一個數字，所以玩家lose
        {
            cout << "You lose! Answer is " << Max <<endl;
            break;
        }
    }
        /////////////////////////////////////////////////////////////////////////////////◆E9秆氮

for(int i=0;i<counter;i++)   //counter我將其理解成一個浮動的上限，其值取決於我們輸入幾次number，i就是作為我們運算並令其重複輸出結果的運算子
    //這邊i<counter而非i<=counter，是因為我們所宣告的counter在while迴圈裡面，所以counter跑到最後一個我們想要的數後，從while輸出出來還會再+1
{
        cout<<i+1<<" - "<<re[i]<<endl;
}

    return 0;
}
