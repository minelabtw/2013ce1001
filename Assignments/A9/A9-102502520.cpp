#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include <math.h>
using namespace std;
void function0(int &num, int &Max ,int &Min, int &randNum);//函式
int main()
{
    srand(time(0));//設定變數
    int randNum = (rand() % 101);
    int num;
    int Max=0;
    int Min=100;
    int counter=1;
    const int arrsize = 100;//定義陣列大小
    int arr[arrsize] = {};//陣列

    do
    {
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>num;
        if (num<0||num>100)//檢驗輸入之數
        {
            cout<<"Out of range!\n";
        }
        arr[counter] = num;//儲存num
        counter++;
    }
    while (num<0||num>100);

    while(1)//沒猜對進入while
    {
        function0(num, Max, Min,randNum);//呼叫函式
        if (Max==Min)//輸了，結束wile
        {
            cout<<"You lose! Answer is "<<randNum<<endl;
            break;
        }
        if (num==randNum)//猜對，獲勝
        {
            cout<<"You win!"<<endl;
            break;
        }
        if (num!=randNum)//更改範圍
        {
            cout<<"Enter number("<<Max<<"<=number<="<<Min<<")?: ";
            cin>>num;
            arr[counter] = num;//儲存num
            counter++;
        }
        if (num<Max||num>Min)//輸出超出範圍
        {
            cout<<"Out of range!\n";
            arr[counter] = 0;//超出範圍時，陣列值為零
            counter--;//記步減少
        }
    }

    for (int i=1; i<counter; i++)//輸出陣列
    {
        cout<<setw(3)<<i<<" -"<<setw(3)<<arr[i]<<endl;
    }
    return 0;
}

void function0(int &num, int &Max,int &Min,int &randNum)//函式
{
    if (num>Min)//比較大，不做任何事
    {

    }
    else if (num>randNum)//輸入在範圍內，更改Min
    {
        Min = num - 1;
    }
    if (num<Max)//比較小，不做任何事
    {

    }
    else if (num<randNum)//輸入在範圍內，更改Max
    {
        Max = num + 1;
    }
}
