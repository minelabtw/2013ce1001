#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int number,random;
    int MAX=100,MIN=0;//設定極限值
    int times=1;//設定計數器
    int answer[100]= {}; //定義陣列

    srand(time(NULL));//設定亂數
    random = rand()%101;

    cout <<"Enter number(0<=number<=100)?: ";
    cin >>number;
    while(number<0||number>100)//檢驗是否合於範圍
    {
        cout <<"Out of range!"<<endl;
        cout <<"Enter number(0<=number<=100)?: ";
        cin >>number;
    }
    answer[ times ]=number;
    times++;//每運行一次計數器+1
    while(number)
    {
        if(number>random)//極限值替換
            MAX = number-1;
        else
            MIN = number+1;

        if(MAX == MIN)//設定輸贏條件
        {
            cout <<"You lose! Answer is "<<random<< endl ;
            for( int x = 1; x <= times-1; x++ )//輸出陣列
                cout << x << " - " << answer[ x ] << endl;
            break;
        }
        else if(number==random)
        {
            cout <<"You win!"<<endl;
            for( int x = 1; x <= times-1; x++ )//輸出陣列
                cout << x << " - " << answer[ x ] << endl;
            break;
        }

        cout <<"Enter number("<<MIN<<"<=number<="<<MAX<<")?: ";
        cin >>number;
        while(number<MIN||number>MAX)//檢驗是否合於範圍
        {
            cout <<"Out of range!"<<endl;
            cout <<"Enter number(0<=number<=100)?: ";
            cin >>number;
        }
        answer[ times ]=number;
        times++;//每運行一次計數器+1
    }
    return 0;
}

