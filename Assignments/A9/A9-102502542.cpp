#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std ;

int main()
{
    int number=0 ; //宣告變數
    int rain=0 ;
    int a=0 ;
    int b=100 ;
    srand(time(0)); //做亂數
    rain=rand()%101 ; // 隨機挑取亂數
    const int arraysize = 101 ; //宣告陣列大小
    int n[arraysize] = {} ;
    cout << "Enter number(0<=number<=100)?: " ; //輸出
    cin >> number ; //輸入
    while (number<0 or number>100) // 當number<0 or number>100 進入迴圈
    {
        cout << "Out of range!" << endl << "Enter number(0<=number<=100)?: " ;
        cin >> number ;
    }
    int c=0 ; //宣告變數
    while (number!=rain) //當number!=rain進入迴圈
    {
        if (number>rain) //判斷式
        {
            b=number-1 ;
            c++ ;
            n[c] = number ; //將number丟給陣列儲存作為第C項
        }
        else if (number<rain) //同上
        {
            a=number+1 ;
            c++ ;
            n[c] = number ; //同上
        }
        if (a==b) //同上
        {
            cout << "You lose! Answer is " << rain << endl ;
            break ; //離開
        }
        cout << "Enter number(" << a << "<=number<=" << b << ")?: " ; //輸出
        cin >> number ; //輸入
        while (number<a or number>b) //如果number<a or number>b 進入以下迴圈
        {
            cout << "Out of range!" << endl << "Enter number(" << a << "<=number<=" << b << ")?: " ;
            cin >> number ;
        }
    }
    if (number==rain) //判斷式.
    {
        cout << "You win!" << endl ;
        c++ ;
        n[c] = number ; //同上
    }
    for (int i=1 ; i<=c ; i++) //宣告項數
        cout << i << " - " << n[i] << endl ; //輸出已儲存陣列值
    return 0 ;
}
