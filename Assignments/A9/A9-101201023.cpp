#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int number=0;
    int integer=0;
    int origin1=0;
    int origin2=100;
    int n[101];
    int i=0;
    srand(time(NULL));                                 //初始化隨機數字

    number = rand()%101;                               //讓number為0到100的隨機一個數字

    cout << "Enter number(" << origin1 << "<=number" << "<=" << origin2 << ")?: ";
    cin >> integer;
    n[i]=integer;
    while(integer<origin1 || integer>origin2)                    //判斷integer是否符合範圍
    {
        cout << "Out of range!" << endl;
        cout << "Enter number(" << origin1 << "<=number" << "<=" << origin2 << ")?: ";
        cin >> integer;
    }

    while(integer!=number)                             //當integer符合範圍，但不等於number時
    {
        if(number<integer)                             //判斷number與integer的大小關係
        {
            origin2=integer-1;                         //讓原來範圍的上界重新定為輸入的數減1
            if(origin1!=origin2)                       //判斷新的範圍可猜的數字是否大於一個
            {
                cout << "Enter number(" << origin1 << "<=number" << "<=" << origin2 << ")?: ";
                cin >> integer;
                i++;
                while(integer<origin1 || integer>origin2)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter number(" << origin1 << "<=number" << "<=" << origin2 << ")?: ";
                    cin >> integer;
                }
            }

            else                                       //可猜範圍只有一個數字，就輸出You lose! Answer is "number"並結束遊戲
            {
                cout << "You lose! Answer is " << number << endl;
                integer=number;
            }
        }

        else if(number>integer)
        {
            origin1=integer+1;                     //讓原來範圍的下界重新定為輸入的數加1
            if(origin1!=origin2)
            {
                cout << "Enter number(" << origin1 << "<=number" << "<=" << origin2 << ")?: ";
                cin >> integer;
                i++;
                while(integer<origin1 || integer>origin2)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter number(" << origin1 << "<=number" << "<=" << origin2 << ")?: ";
                    cin >> integer;
                }
            }

            else
            {
                cout << "You lose! Answer is " << number << endl;
                integer=number;
            }
        }
        n[i]=integer;
    }

    if(integer==number && origin1!=origin2)              //當猜中時，輸出You win!
    {
       cout <<  "You win!" << endl;
    }

    for(int x=0 ; x<=i ; x++)
    {
        cout << x+1 << " - " << n[x] << endl;
    }

    return 0;
}
