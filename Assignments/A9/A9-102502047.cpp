#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int a=0,b[100]= {},c=0,d=100,i=0;
    srand(time(0));
    a=rand()%101;
    cout<<"Enter number(0<=number<=100)?: ";
    cin>>b[i];
    for(; b[i]<0||b[i]>100;)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>b[i];
    }
    for(; a!=b[i]&&c!=d;)
    {
        if(b[i]>a)
            d=b[i]-1;
        if(b[i]<a)
            c=b[i]+1;
        if(c==d)
            cout<<"You lose! Answer is "<<c;
        if(c!=d)
        {
            cout<<"Enter number("<<c<<"<=number<="<<d<<")?: ";
            i=i+1;
            cin>>b[i];
            for(; b[i]>d||b[i]<c;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter number("<<c<<"<=number<="<<d<<")?: ";
                cin>>b[i];
            }
        }
    }
    if(a==b[i])
        cout<<"You win!";
    cout<<endl;
    for(int j=1; j<=i+1; j++)
        cout<<j<<" - "<<b[j-1]<<endl;
    return 0;
}
