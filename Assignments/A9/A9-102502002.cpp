#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));                      //choose a random number (changes through time).
    int randnum=rand()%100+1;            //the range of randnum is from 1 to 100.
    int number=0;                        //declare a variable called "number".
    int n1=0;                            //the upper limit
    int n2=100;                          //the lower limit
    int a[100];
    int cIndex=0;

    for (int i=0; i<100; i++)
        a[i]=-1;

    cout << "Enter number(0<=number<=100)?: ";            //enter a number between 0 and 100.
    cin >> number;
    while(number)
    {
        if(((number-randnum == 1) && (randnum==n1)) ||
                ((n2==randnum) && (randnum-number ==1)))
            //if you don't guess the right number by the end of the game, output the answer.
        {
            a[cIndex]=number;
            cout << "You lose! Answer is " << randnum;
            break;
        }

        if((number<n1)||(number>n2))                          //if number is bigger than the upper limit or smaller than the lower limit output out of range.
        {
            cout << "Out of range!\n" << "Enter number(" << n1 << "<=number<=" << n2 << ")?: ";
            cin >> number;
        }
        else
        {
            a[cIndex]=number;                 //give the value of each guess to array
            cIndex++;
            if(number==randnum)               //if you guess the correct number output you win.
            {
                cout << "You win!";
                break;
            }
            else if(number<randnum)           //if number is smaller than randnum, change the upper limit to number.
            {
                n1=number+1;
                cout << "Enter number(" << n1 << "<=number<=" << n2 << ")?: ";
                cin >> number;
            }
            else if(number>randnum)           //if number is bigger than randnum, change the lower limit to number.
            {
                n2=number-1;
                cout << "Enter number(" << n1 << "<=number<=" << n2 << ")?: ";
                cin >> number;
            }
        }
    }

    cout << endl;

    for(int i=0; i<100; i++)                //output every step of your guess
    {
        if(a[i]!=-1)
        {
            cout << i+1 << " - " << a[i] << endl;
        }
    }
    return 0;
}
