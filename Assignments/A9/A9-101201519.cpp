#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));//讓種子隨著時間變化
    int ans = rand()%101;
    int guess=0,min=0,max=100;
    int A[100],i=0;
    do
    {
        cout << "Enter number("<< min <<"<=number<=" << max <<")?: ";
        cin >> guess;
        if (guess<min || guess>max)//判斷猜的範圍是否正確
        {
            cout << "Out of range!\n";
            continue;//停止下面的程式,返回執行上面的程式(不能讓錯誤的數字也存進陣列中)
        }

        else if (guess<ans)//若沒猜到正確數字則改變猜測的範圍
            min=guess+1;
        else if (guess>ans)
            max=guess-1;
        else if (guess==ans)
        {
            cout << "You win!";
            A[i]=guess;
            i++;
            for (int j=0; j<i; ++j)
            {
                cout  << endl << j+1 << " - " << A[j];
            }
            return 0;
        }
            A[i]=guess;//將輸入的數字存入陣列中
            i++;

    }
    while(max!=min);
    cout << "You lose! Answer is " << ans ;
    for (int j=0; j<i; ++j)
    {
        cout  << endl << j+1 << " - " << A[j];//輸出剛剛有輸入的數字
    }
    return 0;

}
