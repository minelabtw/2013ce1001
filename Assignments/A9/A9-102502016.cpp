#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    int number;//number輸入,low,high最小最大
    int array[101]= {};//101個(0~100使用1~100)
    int counter=0;
    int low=0;
    int high=100;
    int answer;
    answer = (rand()%100)+1;//亂數
    do
    {
        cout<<"Enter number("<<low<<"<=number<="<<high<<")?: ";
        cin>>number;
        if (number<low || number>high)
            cout<<"Out of range!"<<endl;
        else
        {
            counter++;//每成功輸入一次就+1
            array[counter] = number;
            if (number<answer)
            {
                low = number+1;
            }
            if (number>answer)
            {
                high = number-1;
            }
        }
    }
    while(low!=high && answer!=number);

    if(low==high)
        cout<<"You lose! Answer is "<<answer;
    else
        cout<<"You win!";
    cout<<endl;
    for (int i=1; i<=counter; i++)
    {
        cout<<i<<" - "<<array[i];
        if (i!=counter)
            cout<<endl;
    }
    return 0;
}

