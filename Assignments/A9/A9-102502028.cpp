#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std ;

int main ()
{
    srand (time(0)) ;
    int number =  rand() % 101  ;      //宣告整數變數,陣列
    int x = 0 ;
    int Max = 100 ;
    int Min = 0 ;
    int count = 0 ;
    int array [100] = {} ;

    while (x != number)         //不等於答案時繼續迴圈
    {
        do                      //要求輸入迴圈
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: " ;
            cin >> x ;
            if(x < Min || x > Max)
            {
                cout << "Out of range!" << endl;
            }
        }
        while (x < Min || x > Max) ;

        if (x == number)                                             //判斷密碼是否正確及其範圍
        {
            cout << "You win!" << endl ;
            count ++ ;                                               //處存輸入到陣列裡面
            array [count] = number ;
            for (int c = 1; c <= count ; c ++)                       //輸出陣列結果
            {
                cout << c << " - " << array [c] << endl ;
            }
            break ;
        }
        else if (Min <= x && x <= number)
        {
            Min = x + 1 ;
            count ++ ;
            array [count] = x ;

        }
        else if (number <= x && x <= Max)
        {
            Max = x - 1 ;
            count ++ ;
            array [count ] = x ;

        }
        if (Max - Min == 0)
        {
            cout << "You lose! Answer is " << number << endl ;
            for (int d = 1 ; d <= count ; d ++)                           //輸出陣列結果
            {
                cout << d << " - " << array [d] << endl ;
            }
            break ;
        }
    }
    return 0 ;
}
