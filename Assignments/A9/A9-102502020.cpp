#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
    int a=0;
    int b=100;
    int number=0;                //你猜的數字
    int answer=0;                //正確答案
    int Frequency=1;             //猜的次數，不包括超出範圍的數字
    int num[100]= {};            //你最多只能猜100次

    srand(time(0));              //以time(0)的隨機變數當作seed
    answer=rand()%100+1;         //隨機變數的範圍在0到100之間
    //cout << answer << endl;
    do
    {
        if(a!=b)                                                             //當a不等於b
            cout << "Enter number(" << a << "<=number<=" << b <<" )?: ";     //輸出正確答案在哪個範圍裡
        else                                                                 //當二選一你猜錯時
            break;                                                           //跳出迴圈
        cin >> number;                                                       //請猜個數字
        if(number<a or number>b)                                             //當猜的數字超出範圍時
            cout << "Out of range!" << endl;                                 //超出範圍囉~~
        else if(number<answer)                                               //當猜的數字比正確答案小時
        {
            a=number+1;                                                      //調整最小值範圍
            num[Frequency]=number;                                           //你第幾次猜的數字
            Frequency=Frequency+1;
        }
        else                                                                 //當猜的數字比正確答案大時
        {
            b=number-1;                                                      //調整最大值範圍
            num[Frequency]=number;
            Frequency=Frequency+1;
        }
    }
    while(number!=answer);                                                   //你沒猜中正確答案，請重猜一次
    if(number!=answer)
        cout << "You lose! Answer is " << answer << endl;                    //真可惜~你猜錯啦!!
    else
        cout << "You win!" << endl;                                          //恭喜~你猜對啦!!
    for(int j=1; j<Frequency; j++)
    {
        cout << j << " - " << setw(2) << num[j] << endl;                     //按照順序輸出你所猜的答案
    }

    return 0;
}
