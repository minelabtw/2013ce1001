#include <iostream>
#include <cstdlib>      //contain srand() and rand()
#include <ctime>        //contain time()
using namespace std;

int main()
{
    int number = -1;    //為防止number和randNum值相同而無法進入while，所以預設number為-1
    int Max = 100;      //預設最大值
    int Min = 0;        //預設最小值
    int n[100];         //設定陣列大小100個
    int i=0;            //次數
    srand(time(0));             //以time(0)做seed
    int randNum = rand()%101;   //隨機設定要猜的數字(0<=randNum<=100)

    while(randNum != number)
    {
        do
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";    //輸出number範圍的提示字請玩家輸入
            cin >> number;  //玩家輸入數字
            n[i]=number;
            i++;                                //次數輸入值就+1
            if(number < Min || number > Max)    //若輸入的數值不在number提示字的範圍則Out of range!
            {
                cout << "Out of range!" << endl;
                i--;
            }//輸入無效就把次數扣回

        }
        while(number < Min || number > Max);

        if(number > randNum)   //輸入的數字大於要猜的數字
            Max = number;           //改變提示字範圍的最大值
        else                        //輸入的數字小於要猜的數字
            Min = number;           //改變提示字範圍的最小值

        if(number == Max)           //輸入的數字等於最大值
            Max--;                  //將最大值範圍往內縮，最大值-1
        if(number == Min)           //輸入的數字等於最小值
            Min++;                  //將最小值範圍往內縮，最小值+1

        if(Max == Min&&Max==randNum)              //如果最大值等於最小值，代表只剩一個數字，所以玩家lose
        {
            cout << "You lose! Answer is " << randNum;
            break;
        }
        else if(Max == Min&&Max!=randNum)
            cout << "You win!";
        else if(number == randNum)       //猜中數字
            cout << "You win!";
    }

    for(int k=0;k<i;k++)        //輸出每次有效的值
        cout<<endl<<k+1<<" - "<<n[k];
    return 0;
}
