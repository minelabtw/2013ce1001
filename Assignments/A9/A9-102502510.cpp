#include <iostream>
#include <cstdlib>// rand ,srand
#include <ctime>// time
#include <vector>// vector
using namespace std;

int main()
{
    srand(time(0));
    int ans=rand()%101;
    int left=0;
    int right=100;
    int number;
    vector<int> process;
    while(true)
    {
        cout << "Enter number("<< left <<"<=number<="<< right <<")?: ";
        cin >> number;
        if(left>number||right<number)
        {
            cout << "Out of range!" << endl;
            continue;
        }
        if(number<ans)
        {
            left=number+1;
        }
        else if(number>ans)
        {
            right=number-1;
        }
        process.push_back(number);
        if(left==right)
        {
            cout << "You lose! Answer is " << ans << endl;
            break;
        }
        else if(number==ans)
        {
            cout << "You win!" << endl;
            break;
        }
    }
    for(size_t i=0;i<process.size();++i)//ouput process
    {
        cout << i+1 << " - " << process[i] << endl;
    }
    return 0;
}
