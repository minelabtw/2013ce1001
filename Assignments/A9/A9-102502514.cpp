#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int number;
    int top=100;  //設定最大值初始值為100
    int bottom=0;  //設定最小值初始值為0
    srand(time(0));
    int randNum=rand()%101;  //宣告亂數為0~100的整數
    int n[100]= {};  //宣告一個100element的陣列,供使用者輸入的數字傳入
    int i=0;  //i代表傳入陣列的次數,初始值設為0
    cout <<"Enter number(0<=number<=100)?: ";
    cin >>number;
    if (number>=0&&number<=100)
    {
        n[i]=number;  //這是第一個輸入的數字,如果此猜測符合範圍限制,則傳入陣列儲存,並讓i=i+1
        i++;
    }

    while (number!=randNum)
    {
        while (number<bottom||number>top)  //當輸入的number在任一時刻不符合最大最小值所在的區間,則印出Out of range!
        {
            cout <<"Out of range!"<<endl;
            cout <<"Enter number("<<bottom<<"<=number<="<<top<<")?: ";
            cin >>number;
            if (number>=bottom&&number<=top)  //如果此猜測符合範圍限制,則傳入陣列儲存,並讓i=i+1
            {
                n[i]=number;
                i++;
            }
        }
        if (randNum<number&&number<=top)
        {
            top=number-1;  //如果randNum<number,將number設定為最大值,繼續猜
            if (bottom==top)
            {
                cout <<"You lose! Answer is "<<randNum<<endl;//當最大值等於最小值,印出遊戲失敗,並跳出遊戲,否則繼續猜測
                break;
            }
            else
            {
                cout <<"Enter number("<<bottom<<"<=number<="<<top<<")?: ";
                cin >>number;
                if (number>=bottom&&number<=top)  //如果此猜測符合範圍限制,則傳入陣列儲存,並讓i=i+1
                {
                    n[i]=number;
                    i++;
                }
            }
        }
        if (bottom<=number&&number<randNum)
        {
            bottom=number+1;  //如果randNum>number,將number設定為最小值,繼續猜
            if (top==bottom)
            {
                cout <<"You lose! Answer is "<<randNum<<endl;  //當最大值等於最小值,印出遊戲失敗,並跳出遊戲,否則繼續猜測
                break;
            }
            else
            {
                cout <<"Enter number("<<bottom<<"<=number<="<<top<<")?: ";
                cin >>number;
                if (number>=bottom&&number<=top)  //如果此猜測符合範圍限制,則傳入陣列儲存,並讓i=i+1
                {
                    n[i]=number;
                    i++;
                }
            }
        }
    }
    if (number==randNum)  //當number==randNum則跳出while迴圈,並印出遊戲勝利
        cout <<"You win!"<<endl;
    for (int j=0; j<i; j++)
        cout <<j+1<<" - "<<n[j]<<endl;  //最後印出每次有效猜測的紀錄

    return 0;
}
