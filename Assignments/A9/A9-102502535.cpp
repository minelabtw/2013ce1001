#include <iostream>
#include <stdlib.h>
#include <ctime>

using namespace std ;

int main ()
{
    srand(time(0)) ;

    int number ;
    int n1 = 0 ;
    int n2 = 100 ;  //設變數。
    int answer = rand()%101;  //隨機找變數。
    int counter = 0 ;  //計算次數。
    int show [99] ;  //設陣列。
    int i = 1 ;

    for ( int i=0 ; i<100 ; i++ )
        show [i] = 0 ;  //定義陣列。

    cout << "Enter number(0<=number<=100)?: " ;
    cin >> number ;

    while ( number < 0 || number > 100 )
    {
        cout << "Out of range!" << endl << "Enter number(0<=number<=100)?: " ;
        cin >> number ;
    }  //判斷合理與否。

    do
    {
        if ( number < answer )
        {
            n1 = number + 1 ;
            counter ++ ;
            show [i] = number ;
            i ++ ;  //使儲存輸入數值。
        }

        else if ( number > answer )
        {
            n2 = number - 1 ;
            counter ++ ;
            show [i] = number ;
            i ++ ;
        }

        if ( n1 == n2 )
        {
            cout << "You lose! Answer is " << answer << endl ;

            for ( int i = 1 ; i < counter + 1 ; i ++ )
            {
                cout << i << " - " << show [i] << endl ;
            }  //使輸出猜的歷史紀錄。
            return 0 ;
        }

        cout << "Enter number(" << n1 << "<=number<=" << n2 << ")?: " ;
        cin >> number ;

        while ( number < n1 || number > n2 )
        {
            cout << "Out of range!" << endl << "Enter number(" << n1 << "<=number<=" << n2 << ")?: " ;
            cin >> number ;
        }  //判斷合理範圍。
    }
    while ( number != answer ) ;  //猜數字。

    if ( number = answer )
    {
        cout << "You win!" << endl ;

        for ( int i = 1 ; i <= counter + 1 ; i ++ )
        {
            cout << i << " - " << show [i] << endl ;
        }  //使輸出猜的歷史紀錄。
    }

    return 0 ;
}
