#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int a=0;
    int b=100;
    srand(time(NULL));
    int c=(rand()%101); //c值為亂數
    int x;
    int y[100]={0}; //宣告陣列y，項數為100，初始值為0
    int i=0;

    do
    {
        cout << "Enter number(" << a << "<=number<=" << b << ")?: ";
        cin >> x;
        if(x<a || x>b)
            cout << "Out of range!\n";
        else
        {
            y[i]=x;
            i++;

            if(x<c)
                a=x+1;
            if(x>c)
                b=x-1; //猜錯時縮小範圍
            if(a==b)
                cout << "You lose! Answer is " << c << endl; //如果a=b，失敗，並輸出c值
            if(x==c)
            {
                cout << "You win!\n"; //如果x=c時，獲勝
                a=c;
                b=c;
            }
        }
    }
    while(a!=b); //a不等於b時執行迴圈

    for(int j=0;j<i;j++)
    {
        cout << j+1 << " - " << y[j] << endl; //輸出陣列內的數字
    }

    return 0;
}
