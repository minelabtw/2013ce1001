#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
void Outputgp(int i, int a[])//輸出猜測過程
{
    for(int j=1; j<=i; j++)
        cout << j << " - " << a[j] << endl;
}
int main()
{
    srand(time(0));//讓種子隨時間改變
    int ans=rand()%101, lbd=0, ubd=100, guess=0;//宣告變數為整數，ans為答案、lbd為下界、ubd為上界、guess為猜測答案
    int gp[101]={}, i=0;//gp為儲存猜測過程、
    ans=75;
    do
    {
        cout << "Enter number(" << lbd << "<=number<=" << ubd << ")?: ";
        cin >> guess;
        if(guess<lbd || guess>ubd)//檢查是否在範圍內
        {
            cout << "Out of range!\n";
            continue;
        }
        i++;
        gp[i]=guess;
        if(guess==ans)//贏
        {
            cout << "You win!\n";
            Outputgp(i, gp);
            return 0;
        }
        else if(guess<ans)//改變範圍
            lbd=guess+1;
        else if(guess>ans)
            ubd=guess-1;
    }
    while(lbd!=ubd);
    //輸
    cout << "You lose! Answer is " << ans << endl;
    Outputgp(i, gp);
    return 0;

}
