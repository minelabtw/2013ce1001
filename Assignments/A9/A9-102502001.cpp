#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0));              //隨機取變數
    int randnum=rand()%101;      //將任意數除以101,餘數會介於0~100之間
    int num1;                    //宣告一整數變數
    int max=100;                 //宣告一初始最大值=100
    int min=0;                   //宣告一初始最小值=0
    int history[101];
    int index=0;

    cout<<"Enter number(0<=number<=100)?: ";
    cin>>num1;                   //輸入變數
    history[index]=num1;         //將此存為history[1]
    index++;

    while(num1)
    {
        if(num1<min or num1>max)        //若不再範圍內則顯示"Out of range!"並重新輸入
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
            cin>>num1;
        }
        else if(num1==randnum)         //猜中數字則贏
        {
            cout<<"You win!"<<endl;

            for(int i = 0; i < index; i++)
            {
                cout<<i+1<<" - "<< history[i] <<endl;
            }
            break;
        }
        if(num1<randnum)               //若變數小於隨機變數則使min=num1並重新顯示範圍
        {
            min=num1+1;
            if(max==min+1 or max==min)
            {
                cout<<"You lose! Answer is "<<randnum<<endl;
                for(int i = 0; i < index; i++)
                {
                    cout<<i+1<<" - "<<history[i]<<endl;   //顯示第i+1次猜的數字
                }
                break;

            }
            else
            {
                cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
                cin>>num1;
                history[index]=num1;               //將所猜的數字記錄到history中
                index++;
            }
        }

        if(num1>randnum)              //若變數大於隨機變數則使max=num1並重新顯示範圍
        {
            max=num1-1;
            if(max==min+1 or max==min)
            {
                cout<<"You lose! Answer is "<<randnum<<endl;
                for(int i = 0; i <index; i++)
                {
                    cout <<i+1<<" - "<< history[i] <<endl; //顯示i+1次猜的數字
                }
                break;
            }
            else
            {
                cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
                cin>>num1;
                history[index] = num1;           //將所猜的數字記錄到history中
                index++;
            }
        }
    }
    return 0;
}
