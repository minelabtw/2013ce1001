#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int r[100]; //紀錄輸入值
    int i=-1;
    srand(time(0)); //打亂變數
    int toget=rand()%101; //產生0~100亂數
    int a=0; //下限
    int b=100; //上限
    int x; //輸入值

    do
    {
        do
        {
            cout << "Enter number(" << a << "<=number<=" << b << ")?: ";
            cin >> x;
        }
        while ( (x<a || x>b) && cout << "Out of range!" << endl ); //輸出題目

        i++;
        r[i]= x; //存入陣列

        if ( x==toget )
        {
            cout << "You win!" << endl;
            break;
        } //答對
        else if ( x<toget )
        {
            a=(x+1);
        }
        else if ( x>toget )
        {
            b=(x-1);
        } //修正上下限

        if ( a==b )
        {
            cout << "You lose! Answer is " << toget <<endl;
            break;
        } //答錯

    }
    while ( a!=b ); //反覆輸入

    for ( int j=0 ; j<=i ; j++ )
        cout << j+1 << " - " << r[j] << endl; //輸出所有值

    return 0;
}
