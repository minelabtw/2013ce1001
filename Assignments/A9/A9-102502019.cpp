#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;


int main()                                                                     //宣告變數
{
    srand(time(0));                                                            //亂數
    int a=(rand()%101),x=0, minnumber=0, maxnumber=100,p=1;
    int process[100]= {};
    cout <<"Enter number(0<=number<=100)?: ";
    cin >>x;
    process[1]=x;                                                              //用來記錄輸入的過程

    while (x!=a )                                                              //當沒猜中時繼續猜
    {
        while(x<minnumber || x>maxnumber)                                      //不能超出範圍
        {
            cout <<"Out of range!\n"<<"Enter number("<<minnumber<<"<=number<="<<maxnumber<<")?: ";
            cin >>x;
        }
        process[p]=x;                                                          ////用來記錄輸入的過程
        p=p+1;
        if(x>a)
        {
            maxnumber=x-1;
            if(maxnumber==a && minnumber==a)                                    //若最後都沒猜中就輸了
            {
                cout <<"\nYou lose! Answer is "<<a;
                break;
            }
            else
            {
                cout <<"Enter number("<<minnumber<<"<=number<="<<x-1<<")?: ";
                cin >>x;
            }
        }
        else if(x<a)
        {
            minnumber=x+1;
            if(maxnumber==a && minnumber==a)
            {
                cout <<"\nYou lose! Answer is "<<a;
                break;
            }
            else
            {
                cout <<"Enter number("<<x+1<<"<=number<="<<maxnumber<<")?: ";
                cin >>x;
            }

        }
    }
    if(x==a)
    {
        cout <<"You win!";
        process[p]=x;
        for(int k=1; k<p+1; k++)
        {
            cout <<endl<<k<<" - "<<process[k]<<endl;
        }
    }
    else
    {
        for(int k=1; k<p; k++)
        {
            cout <<endl<<k<<" - "<<process[k]<<endl;
        }

    }
    return 0;
}
