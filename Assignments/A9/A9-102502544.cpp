#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int x=0;    //宣告變數
    int xMax=100;
    int xMin=0;
    int counter=0;
    int Out[100]= {};
    srand(time(0));
    int code=rand()%101;

    while(x!=code)   //設定迴圈
    {
        cout<<"Enter number("<<xMin<<"<=number<="<<xMax<<")?: ";
        cin>>x;
        while(x<xMin || x>xMax)
        {
            cout<<"Out of range!\nEnter number("<<xMin<<"<=number<="<<xMax<<")?: ";
            cin>>x;
        }
        Out[counter]=x;
        counter++;
        if(x>code)
        {
            xMax=x-1;
        }
        else if(x<code)
        {
            xMin=x+1;
        }
        else if(x==code)
        {
            cout<<"You win!"<<endl;
        }
        if(xMax==code && xMin==code)
        {
            cout<<"You lose! Answer is "<<code<<endl;
            break;
        }
    }

    for(int i=0; i<counter; i++)
        cout<<i+1<<" - "<<Out[i]<<endl;

    return 0;
}
