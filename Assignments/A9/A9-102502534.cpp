#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    int number=0;//宣告變數
    int randNum=0;
    int MIN=0;
    int MAX=100;
    int a[100]= {};
    int i=1;

    srand(time(0));
    randNum=rand()%101;//讓亂數介於1~100間

    while(number!=randNum)
    {
        cout<<"Enter number("<<MIN<<"<=number<="<<MAX<<")?: ";//顯示要猜的數的範圍
        cin>>number;

        if(number<MIN or number>MAX)//判斷輸入範圍是否正確
        {
            cout<<"Out of range!"<<endl;
        }
        else if(number<randNum)
        {
            MIN=number+1;
            a[i]=number;//將輸入的數存入陣列
            i++;
        }
        else if(number>randNum)
        {
            MAX=number-1;
            a[i]=number;//將輸入的數存入陣列
            i++;
        }

        if(number==randNum)
        {
            cout<<"You win!"<<endl;
            a[i]=number;//將輸入的數存入陣列
            i++;
            for(int i=1; i<=100&&a[i]!=0; i++)//將存在陣列的數輸出
            {
                cout<<i<<" - "<<a[i]<<endl;
            }
            break;
        }
        else if(MAX==MIN)
        {
            cout<<"You lose! Answer is "<<randNum<<endl;
            for(int i=1; i<=100&&a[i]!=0; i++)//將存在陣列的數輸出
            {
                cout<<i<<" - "<<a[i]<<endl;
            }
            break;
        }
    }
    return 0;
}
