#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int main(){
    srand(time(0));//以time(0)作種子打亂rand
    int number=rand()%101,sup,inf,input,t=0,save[101];//宣告number為答案,sup為上界,inf為下界,input為玩家猜的數字,t為猜的次數,save[]為儲存的數字 
    for(inf=0,sup=100;inf<sup;inf=(number>input ? input+1 : inf),sup=(number>input ? sup : input-1)){//預設上界為100,下界為0,如果上下界相等即跳出,每次輸入更新上下界 
        do{
            cout << "Enter number(" << inf << "<=number<=" << sup << ")?: ";
            cin >> input;
        }while((input<inf || input>sup) && cout << "Out of range!\n");//判斷輸入是否符合範圍,否則要求重新輸入
        if((save[t++]=input) && input==number)//猜對即跳出,並且記錄猜的過程 
            break;
    }
    inf==sup ? cout << "You lose! Answer is " << number << endl
             : cout << "You win!\n";//輸出勝或敗
    for(int i=0;i<t;i++){
        cout << i+1 << " - " << save[i] << endl;//輸出過程 
    }
    return 0;
}
