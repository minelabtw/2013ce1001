#include<iostream>
#include<ctime>            //srand需輸入一數值為參數,通常以時間產生該數值
#include<cstdlib>          //執行rand函式產生介於0到RAND_MAX的數字,而RAND_MAX定義在cstdlib
using namespace std ;
int main()
{
    int guess=1 ;
    int target=0 ;
    int Emax=100 ;
    int Emin=0 ;
    int j=0 ;
    int EP[100]= {};
    srand(time(NULL));
    target=rand()%101+0 ;   //rand()%(上限值-下限值+1)+下限值 ,該餘數介於0到(上限值-下限值)範圍

    do          //利用後測試迴圈可以讓迴圈至少執行一次
    {



        cout<<"Enter number("<<Emin<<"<<=number<<="<<Emax<<")?: " ;
        cin>>guess  ;



        if(guess<Emin || guess>Emax)
        {
            cout<<"Out of range!"<<endl ;


        }

        if(guess>=target && guess<=Emax)
        {
            Emax=guess-1  ;
            EP[j]=guess ;
            j=j+1 ;


        }
                //從33行到48皆是在利用迴圈不斷改變最大值和最小值的範圍,同時將使用者所猜測的值輸入到陣列中

        else if(guess<=target && guess>=Emin)
        {
            Emin=guess+1  ;
            EP[j]=guess ;
            j=j+1 ;
        }


        if(guess==target)
        {
            cout<<"You win!"<<endl ;
            EP[j]=guess ;
            break ;

        }
        else if(Emax==Emin)
        {
            cout<<"You lose! Answer is "<<target<<endl ;
            break ;
        }



    }
    while(guess!=target);

    for(int i=0 ; i<j ; i=i+1)
    {
        cout<<i+1<<" - "<<EP[i]<<endl ;
                                       //利用陣列的迴圈來顯示出遊戲從開始到結束時使用者在合理範圍內所輸入的數值依序為何
    }



    return 0 ;

}
