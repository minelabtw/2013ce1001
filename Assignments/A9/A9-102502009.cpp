#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    srand(time(0));
    int randnum=rand()%101; //亂數0-100
    int number=0,randmax=100,randmin=0,time=0; //猜的數字,上下限,次數
    int record[100]= {0}; //紀錄每次猜的數字
    do
    {
        cout<<"Enter number("<<randmin<<"<=number<="<<randmax<<")?: ";
        cin>>number; //猜數字
        if(number<randmin || number>randmax) //範圍不符
        {
            cout<<"Out of range!"<<endl;
            continue; //回到迴圈開頭
        }
        record[time]=number; //紀錄數字
        if(number>randnum) //改變範圍
            randmax=number-1;
        else if(number<randnum)
            randmin=number+1;
        if(number==randnum) //猜對了
            cout<<"You win!"<<endl;
        else if(randmax==randmin) //被夾殺
            cout<<"You lose! Answer is "<<randnum<<endl;
        time++; //每次記錄在陣列的不同位置
    }
    while(number!=randnum && randmax!=randmin); //還沒猜對而且沒被夾殺
    for(int i=0; i<time-1; i++)
    {
        cout<<i+1<<" - "<<record[i]<<endl; //印出次數和猜的數字
    }
    cout<<time<<" - "<<record[time-1]; //最後一次不用換行
    return 0;
}
