#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    //call variables
    int first=0;
    int last=100;
    int number;
    int arrayplace=0; //call variable for array size
    int n[101]= {}; //array n has 101 elements
    srand(time(0));//make it to get different random number according to time change
    int terminator=rand()%101;


    do
    {
        do// data validation
        {
            cout<<"Enter number("<<first<<"<=number<="<<last<<")?: ";
            cin>>number;
            if(number<first || number>last)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(number<first || number>last);

        //calculate for the range after input
        if(number<terminator)
        {
            first=number+1;
        }
        else if(number>terminator)
        {
            last=number-1;
        }

        //to check whether you win or not
        if(number==terminator)
        {
            cout<<"You win!"<<endl;
        }
        else if(first==last)
        {
            cout<<"You lose! Answer is "<<terminator<<endl;
        }

        arrayplace++; // increase the array place
        n[arrayplace]=number; //save number into array memory
    }
    while (number!=terminator && first!=last); //Loop to continue the game  until lose or win

    //display the numbers saved in memory
    for(int x=1; x<=arrayplace; x++)
    {
        cout<<x<<" - "<<n[x]<<endl;
    }
    return 0;
}
