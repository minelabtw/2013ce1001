#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
    int random;    //設定變數
    int number;
    int m=0;
    int Ma=100;
    int t=1;
    int i=1;
    int p[100];    //設定陣列

    srand(time (0));    //亂數函數

    random=rand()%101;

    do    //判斷輸入的值是否合理
    {
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>number;
        if(number>100||number<0)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(number>100||number<0);

    p[1]=number;    //先存入第一個數字

    while(m!=Ma)    //迴圈
    {
        if(random==number)    //判斷贏
        {
            cout<<"You win!"<<endl;
            m=Ma;    //跳出迴圈
        }

        else if(random>number)
        {
            m=number+1;

            if(m==Ma)    //判斷最大跟最小值是否一樣，若一樣就輸了
            {
                cout<<"You lose! Answer is "<<random<<endl;
            }

            else
            {
                do    //判斷值是否在最大跟最小之間
                {
                    cout<<"Enter number("<<m<<"<=number<="<<Ma<<")?: ";
                    cin>>number;
                    if(number>Ma||number<m)
                    {
                        cout<<"Out of range!"<<endl;
                    }
                }
                while(number>Ma||number<m);
            }
        }

        else if(random<number)
        {
            Ma=number-1;

            if(m==Ma)
            {
                cout<<"You lose! Answer is "<<random<<endl;
            }

            else
            {
                do
                {
                    cout<<"Enter number("<<m<<"<=number<="<<Ma<<")?: ";
                    cin>>number;
                    if(number>Ma||number<m)
                    {
                        cout<<"Out of range!"<<endl;
                    }
                }
                while(number>Ma||number<m);
            }
        }
        t++;    //遞增

        p[t]=number;    //使用陣列
    }
    while(i<t)    //印出陣列
    {
        cout<<i<<" - "<<p[i]<<endl;
        i++;
    }

    return 0;
}

