#include <iostream>
#include <cstdlib>
#include <ctime>
#include <limits>

using namespace std;

int main()
{
    srand (time (0));   //用時間當亂數種子
    int userGuess[100]= {} , Min=0, Max=100 , number=rand()%101 , guessCount=0;    //userGuess是使用者每次猜的數字，因為不會動態陣列所以設99+1個，第0個不要用。Min是最小值，Max是最大值，number是這次遊戲的密碼
    do
    {
        guessCount++;   //每次把使用者猜的數字次數+1，因為預設零所以先+1再輸入
        do      //這個回圈用來取得正確的使用者猜測數字
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: " ;
            cin >>  userGuess[guessCount];
            if ( userGuess[guessCount]>Max ||  userGuess[guessCount]<Min || !(cin))   //若比Min小、比Max大、或是非整數時
            {
                cin.clear();        //先清除cin的錯誤狀態
                cin.ignore(numeric_limits<streamsize>::max(), '\n');        //刪除緩衝區所有的資料
                cout << "Out of range!\n";      //提示錯誤
            }
        }
        while ( userGuess[guessCount]>Max ||  userGuess[guessCount]<Min || !(cin));

        if ( userGuess[guessCount]<number)
            Min =  userGuess[guessCount]+1;      //猜的數字比密碼小時，Min更改
        if ( userGuess[guessCount]>number)
            Max =  userGuess[guessCount]-1;      //反之更改Max
        if ( userGuess[guessCount]==number)
            cout << "You win!";     //猜中了~
        if (Max==Min)
            cout << "You lose! Answer is " << number;   //最大等於最小就沒啥好猜了
    }
    while ( userGuess[guessCount]!=number && Max!=Min);
    for ( int counter=1 ; counter<=guessCount ; counter++)
    {
        cout << endl << counter << " - " << userGuess[counter] ;        //印出猜數字的錄
    }
    return 0;
}
