#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{

    int uplimit = 100; // 上限
    int downlimit = 0; // 下限
    int num = 0; // 答案
    int guess = -1; // 猜測
    int g[101] = {}; // 猜測的歷史

    srand(time(0)); // 建立亂數
    num = rand() % 101; // 選取亂數

    int s = 0; // 計次數
    while(guess != num)
    {
        if(uplimit==downlimit)
        {
            cout << "You lose! Answer is " << num << endl; // 輸
            break;
        }

        cout << "Enter number(" << downlimit << "<=number<=" << uplimit << ")?: ";
        cin >> guess;

        if(guess < downlimit || guess > uplimit)cout << "Out of range!\n";
        else
        {
            g[s] = guess; // 存入合法輸入
            s++;
            if (guess==num)cout << "You win!\n"; // 贏
            else
            {
                guess > num ? uplimit = guess-1 : downlimit = guess+1;
            }
        }
    }

    for(int i=0; i<s; i++) // 輸出歷史
    {
        cout << i+1 << " - " << g[i] << endl;
    }

    return 0;

}
