#include <iostream>
#include <cstdlib>
#include <ctime>
using  namespace std;
int main ()
{
    int ans=0;
    int input=0;
    int min=0;
    int max=100;
    srand(time(NULL));//用srand使其真正隨機產生
    ans=rand()%101;
    int counter=0;
    int guess[100];
    while (input!=ans)
     {
        cout<<"Enter number("<<min <<"<=number<=" <<max <<")?: ";
        cin>>input;
        while (input<min||input>max)
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number("<<min <<"<=number<=" <<max <<")?: ";
            cin>>input;
        }
        
        guess[counter] = input;//把第一次猜的存進guess陣列的第0個位置，依次類推
        counter++;//而且先把counter+1後才結束回圈，所第n次輸入數字後，counter會是n，但是n裡面並沒有存數字
        
        if (input<ans)
        {
            min=input+1;
        }
        else if (input>ans)
        {
            max=input-1;
        }
        else
            cout<<"You Win!"<<endl;//不用break 因為跳出while 回圈後就不會再進來了
        if (min==max)
        {
            cout<<"You Lose! Answer is "<<ans<<endl;
            break;
        }
     }

    for (int temp=0; temp<counter ; temp++)
    {
        cout<<temp+1<<" - ";//印出第幾次猜 因為temp初始直視0，所以要加一
        cout<<guess[temp]<<endl;//印出那一個你所猜的是多少
    }
    return 0;
    
}