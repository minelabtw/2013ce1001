#include<iostream>
#include<stdlib.h>
#include<time.h>

using namespace std;

void change(int &a,int &b,int ans,int input);                             //prototype
int judge (int &input,int &up,int &down,int ans);
int main()
{
    int ans=0;
    srand(time(0));                                                      //seed
    ans=rand()%100+1;
    int input=0;
    int up=100,down=0 ;
    int fail=0,counter=0;
    int a[counter];                                                         //紀錄輸出值用
    a[counter]= {};                                                         //歸零
    do
    {
        cout<<"Enter number("<<down<<"<=number<="<<up<<")?: ";
        cin>>input ;
        fail=judge(input,up,down,ans);                                           //先進入判斷的function
        if(up-down==0)                                                          //輸了才會執行
        {
            cout<<"You lose! Answer is "<<ans<<endl;
            counter++;
            a[counter]=input;                                                   //紀錄最後一次輸入的值
            break;
        }
        counter++;
        a[counter]=input;                                                       //記錄每一次正確的值
    }
    while(fail!=0);
    for(int i =1 ; i<=counter; i++)
    {
        cout<<i<<" - "<<a[i]<<endl;                                             //輸出輸入紀錄
    }

}

void change(int &a,int &b,int ans,int input)                                    //改來改變上下值
{
    if(input>ans)                                                               //大於答案改上值
        b=input-1;
    if(input<ans)                                                               //反之
        a=input+1;
}
int judge (int &input,int &up,int &down,int ans)                                //判斷正確與否
{
    while(input>up||input<down)                                                 //錯誤判斷
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter number("<<down<<"<=number<="<<up<<")?: ";
        cin>>input;
    }
    if(input==ans)                                                              //正確 則跳出 並回傳值0
    {
        cout<<"You win!"<<endl ;
        return 0;
    }
    else                                                                        //範圍正確但答案不正確 進入修改上下值的function
    {
        change(down,up,ans,input);
        return 1;                                                               //回傳值1
    }

}

