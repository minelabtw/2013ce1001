//A9-102502026

#include<iostream>
#include<iomanip> //use for setw
#include<ctime> //use for srand(time())
#include<stdlib.h>  //use for rand
using namespace std;
int main()  //start program
{   //defining all the variables
    int random=0;
    int number=-1;  //to prevent no entering while
    int high=100;   //max number
    int low=0;      //min number
    srand(time(0));
    random = rand() %101;   //0~100
    int N=1;
    int s[50];   //using array

    while(number!=random)   //when number != random
    {
        cout<<"Enter number("<<low<<"=number<="<<high<<")?: ";  //ask number
        cin>>number;
        if (number<=high and number>=low)   //print only number in the range
        {
            s[N]=number;
            N++;
        }
        if (number>high || number<low)  //wrong in the range
            cout<<"Out of range!\n";
        if (number>random && number<=high)  //max number change
            high= number-1;
        if (number<random && number>=low)   //min number change
            low = number+1;
        if (high == low)    //max and low equal = lose
        {
            cout<<"You lose! Answer is "<<random<<endl;
            break;
        }
    }
    if (number == random)   //win
        cout<<"You win!"<<endl;
    for(int i=1; i<N; i++)  //print the numbers typed
        cout<<setw(2)<<i<<" - "<<setw(2)<<s[i]<<endl;
    return 0;
}   //end program
