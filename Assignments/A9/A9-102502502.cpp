#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main ()
{
    int isecret,inumber,counter = 1,i;                                                       //初始化密碼和猜的數字
    int a=0,b=100;                                                                           //設a的初始值為零，b的初始值為一百
    int guessnumber[100] = {};
    srand (time(NULL));                                                                      //初始亂數種子
    isecret = rand ()%101;                                                                   //產生1~100的亂數
    cout << isecret;
    cout << "Enter number(0<=number<=100)?: ";
    cin >> inumber;
    while (inumber!=isecret)                                                                 //當輸入的值不等於密碼時
    {//當輸入的值不在範圍內時
        {
        while (inumber < a || inumber > b)
            cout << "Out of range!\n" << "Enter number("<< a << "<=number<=" << b << ")?: "; //顯示超出範圍並要求再次輸入
            cin >> inumber;
        }
        guessnumber[ counter ] = inumber;
        counter++;
        if ( inumber < isecret && a <= inumber && inumber+1!=b)                              //如果輸入的值小於密碼且大於等於下限且+1不等於上限時
        {
            a=inumber+1;                                                                     //給的範圍提示中範圍中下限要加1
            cout << "Enter number("<< a << "<=number<=" << b << ")?: ";
        }
        else if (inumber > isecret && inumber <= b && inumber-1!=a)                          //如果輸入的值大於密碼且小於等於上限且-1不等於下陷時
        {
            b=inumber-1;                                                                     //給的範圍提示中上限要減1
            cout << "Enter number("<< a << "<=number<=" << b << ")?: ";
        }
        else if (a == isecret && inumber-1==a)                                               //如果下限等於密碼且輸入的值-1等於密碼時
        {
            cout << "You lose! Answer is " << isecret << endl;                               //顯示輸和正解
            for (i=1; i<counter; i++)
            {
                cout << i << " - " << guessnumber[ i ] << endl;
            }
            break;
        }
        if (b == isecret && inumber+1==b)                                                    //如果上限等於密碼且輸入的值+1等於密碼時
        {
            cout << "You lose! Answer is " << isecret << endl;                               //顯示輸和正解
            for (i=1; i<counter; i++)
            {
                cout << i << " - " << guessnumber[ i ] << endl;
            }
            break;
        }
        cin >> inumber;
    }
    guessnumber[ counter ] = inumber;
    counter++;
    if (inumber == isecret)                                                                    //當輸入的值等於密碼時
    {
        cout << "You win!\n";                                                                  //顯示贏
        for (i=1; i<counter; i++)
        {
            cout << i << " - " << guessnumber[ i ] << endl;
        }
    }
    return 0;
}
