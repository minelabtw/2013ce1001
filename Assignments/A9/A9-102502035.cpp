#include <iostream>//載入函示庫
#include <cstdlib>
#include <ctime>
using namespace std;
void overall (int alltime,int array [])//函式輸出最終結果
{
    for (int outputtime =1; outputtime <=alltime; outputtime++)//迴圈正確輸入次
    {
        cout << outputtime << " - " << array [outputtime] <<endl;
    }
}
int main ()
{
    int random =0;//宣告變數
    int up =101;
    int down =-1;
    int input =0;
    int times =0;
    int array [100] = {};
    srand(time(NULL));//隨機種子
    random =rand() %101;//隨機變數
    while (input != random)//當沒猜中繼續
    {
        cout << "Enter number(" << down +1 << "<=number<=" << up -1 << ")?: ";//提示輸入
        cin >> input;//輸入
        while (input >=up ||input <=down)//當超出範圍
        {
            cout << "Out of range!" <<endl;//提示重新輸入
            cout << "Enter number(" << down +1 << "<=number<=" << up -1 << ")?: ";//提示輸入
            cin >> input;//輸入
        }
        array [++times] = {input};
        if (input >random)//若比答案大
            up =input;//修正上限
        else if (input <random)//若比答案小
            down =input;//修正下限
        if (up -1 ==down +1)//當沒猜中
        {
            cout << "You lose! Answer is " << up -1 <<endl;//輸了
            overall(times,array );//最後結果
            return 0;//結束程式
        }
    }
    cout << "You win!" <<endl;//贏了
    overall(times,array );//最後結果
    return 0;//結束程式
}
