#include<iostream>
#include<iomanip>
#include<stdlib.h>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));//隨機變數為當前時間
    int answer = rand() % 101;//宣告答案為一1-100的隨機變數
    int input;//宣告輸入數
    int inputcounter = 0;//宣告輸入計數器的變數
    int small = 0;//宣告較小數
    int big = 100;//宣告較大數
    int record[100]={};//宣告一個有100個元素來存放輸入值的array

    do{
        cout << "Enter number(" << small << "<=number<=" << big << ")?:";//提示輸入範圍
        cin >> input;

       if (input < small || input > big)
            cout << "Out of range!" << endl;//提示超圍
       else if(input == answer)
           {
            cout << "You win!" << endl;
            inputcounter++;//輸入值在提示範圍內，輸入計數器加1
            record[inputcounter]=input;//根據輸入次數存入相應輸入值
            break;//若答對即結束
           }
       else if (input < answer && input > small)
           {
            small = input + 1;//若比答案小且最為靠近答案,複製比輸入數大1的數至較小數
            inputcounter++;//輸入值在提示範圍內，輸入計數器加1
            record[inputcounter]=input;//根據輸入次數存入相應輸入值
           }
       else if (input > answer && input < big)
           {
            big = input - 1;//若比答案大且最為靠近答案,複製比輸入數小1的數至較大數
            inputcounter++;//輸入值在提示範圍內，輸入計數器加1
            record[inputcounter]=input;//根據輸入次數存入相應輸入值
           }
       if (big == small)
           {
            cout << "You lose! Answer is " << answer << endl;//若較大數已等於較小數,提示已經輸掉了
            inputcounter++;//輸入值在提示範圍內，輸入計數器加1
            record[inputcounter]=input;//根據輸入次數存入相應輸入值
           }
       }while(big != small);//較大數等於較小數跳出迴圈

    for(int i=1;i <= inputcounter;i++)//建立一個陣列的迴圈
            {
                cout << setw(2)<< i << " - " << setw(2) << record[i] << endl;//顯示輸入的次數以及對應輸入值
            }

return 0;//回傳
}
