#include<iostream>
#include<ctime>
#include<cstdlib>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int answer ;
    int do_try ;
    int a_min ;
    int a_max ;
    int i ;
    int A[100] ;
    srand(time(0)) ;

    i=1 ;
    answer =rand()%101;                                                   //電腦亂跑出一個0~100的數字
    a_min=0;                                                              //一開始的最小範圍
    a_max=100;                                                            //一開始的最大範圍
    cout <<"Enter number(0<=number<=100)?: " ;
    cin >> do_try ;
    A[0]=do_try ;
    while (true)
    {
        if (answer==do_try)                                               //嘗試的和亂數跑的相等，所以答對了
        {
            cout<<"You win!" ;
            break;
        }
        else if (do_try<a_min || do_try > a_max)                          //不再目前的範圍，所以out of rangle
        {
            cout << "Out of range!" <<endl ;
            i-- ;
        }
        else if (do_try>answer)                                           //縮小範圍
        {
            a_max=do_try-1;
        }
        else if (do_try<answer)                                           //縮小範圍
        {
            a_min=do_try+1;
        }
        if (a_min!=a_max)                                                     //沒有輸掉就繼續
        {
            cout <<"Enter number("<<a_min<<"<=number<="<<a_max<<")?: " ;
            cin >>do_try;
            A[i]=do_try ;
            i++ ;
        }
        else
        {
            cout <<"You lose! Answer is "<< answer ;
            break;                                                        //輸了就結束
        }
    }
    for (int j=0;j<i;++j)
    {
        cout <<endl<<j+1<<" - "<<A[j] ;
    }
    return 0;
}
