#include<iostream>
#include<cstdlib>//使用亂數
#include<ctime>
using namespace std;
int main()
{
    int number=rand()%101;
    int guess=-1;//讓他能進第一個迴圈
    int min=0;
    int max=100;
    int n=0;//記他猜第幾個
    int process[100]={};//給他100格猜，用來儲存的陣列

    while(guess!=number)
    {
        cout << "Enter number(" << min << "<=number<=" << max << ")?: ";
        cin  >> guess;
        while(guess>max || guess<min)//猜測的數字若是超過範圍重新輸入
        {
            cout << "Out of range!\n";
            cout << "Enter number(" << min << "<=number<=" << max << ")?: ";
            cin  >> guess;
        }

        if(guess==number)
        {
            cout << "You win!";
        }

        else if(guess<number)
        {
            min=guess+1;
        }
        else
        {
            max=guess-1;
        }

        if(min==max)
        {
            cout << "You lose! Answer is " << number;
            guess=number;
        }
        n++;//猜完一次就加1
        process[n]=guess;//儲存每一次的猜測


    }

    for(int i=1;i<=n;i++)
        cout << "\n1 - " << process[i] << "\n";


    return 0;
}

