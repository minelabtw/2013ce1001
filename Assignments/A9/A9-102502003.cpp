#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));
    int number=rand()%101;
    int guess=-1;  //避免隨數字為0
    int Max=100;
    int Min=0;
    int Count=0;  //輸入次數
    int Guess[]= {};  //輸出陣列

    do
    {

        do  //規範輸入值
        {
            cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>guess;  //玩家輸入數字
            if(guess<Min || guess>Max)
                cout << "Out of range!" << endl;
        }
        while(guess<Min || guess>Max);

        if(guess==number)
            cout<<"You win!"<<endl;
        else if(guess<number)  //改變下限
            Min=guess;
        else  //改變上限
            Max=guess;

        if(guess==Max)
            Max--;
        if(guess==Min)
            Min++;

        if(Max==Min)
        {
            cout<<"You lose! Answer is "<<number<<endl;
            break;
        }

    }
    while(guess!=number&&Count<100);


    for(int i=0; i<Count; i++)  //輸出陣列
    {
        if(i==Count)
            cout<<i+1<<" - "<<Guess[i]<<endl;
    }

    return 0;
}
