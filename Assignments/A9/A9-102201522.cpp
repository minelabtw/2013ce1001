#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0)); //隨機設定一個整數
    int mini=0, maxi=100, guess, times=0;
    int number = rand()%101; //隨機設定number，範圍為0<=number<=100。
    int P[100]; //宣告一維陣列P
    do
    {
        times++;
        cout << "Enter number(" << mini << "<=number<=" << maxi << ")?: "; //玩家輸入整數來猜數字
        cin >> guess;
        while (guess<mini||guess>maxi) //輸入超出提示字的範圍則顯示”Out of range!”。
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << mini << "<=number<=" << maxi << ")?: ";
            cin >> guess;
        }
        if (guess>number) //若沒猜中就會更改提示字範圍並繼續遊戲
            maxi = guess-1;
        else
            mini = guess+1;
            P[times-1]=guess; //使用陣列來儲存玩家猜數字的過程
    }
    while (guess!=number&&maxi!=mini);
    if (guess==number) //若猜中就顯示”You win!”
        cout << "You win!";
    else //若剩一個數字可猜，則直接顯示”You lose! Answer is ”和正確答案
        cout << "You lose! Answer is " << number;
    for (int i=0;i<times;i++) //將整個結果顯示出來
        cout << endl << i+1 << " - " << P[i];
             return 0;
}
