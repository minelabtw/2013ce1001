#include <iostream>
#include<stdlib.h>
#include<time.h>
#include <list>
#include<numeric>
using namespace std;
typedef list<int> LISTINT;

int main()
{
    //原本要練習用list來儲存 但是因為試不出來先暫時使用陣列來處理

    int correctNum=0,guessNum=0,upBound=100,lowBound=0,isGameOver=0;
    srand((unsigned)time(0));
    LISTINT listGuess;
    correctNum=rand() % 101;

    int guessArray[10];
    int guessCount=0;

    while(isGameOver==0)
    {
    //  判斷書入的值
        do
        {
            cout << "Enter number("<<lowBound<<"<=number<="<<upBound<<")?: " ;

            cin>>guessNum;
            if(guessNum<0 || guessNum>100)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(guessNum<0 || guessNum>100);


        if(guessNum<correctNum)
        {
            if(guessNum<lowBound)
            {
                guessNum=lowBound;
                cout<<"Out of range!"<<endl;
                lowBound=guessNum;
            }
            else
            {
                lowBound=guessNum+1;
                //listGuess.push_back(guessNum) ;
                guessArray[guessCount]=guessNum;
                //k++;
                guessCount++;
            }

            if(upBound==lowBound)
            {
                cout<<"You lose! Answer is "<<correctNum;
                isGameOver=1;
            }
        }
        else if(guessNum>correctNum)
        {
            if(guessNum>upBound)
            {
                guessNum=upBound;
                cout<<"Out of range!"<<endl;
                upBound=guessNum;
            }
            else
            {
                upBound=guessNum-1;
                //listGuess.push_back(guessNum) ;
                guessArray[guessCount]=guessNum;
                //k++;
                guessCount++;
            }

            if(upBound==lowBound)
            {

                cout<<"You lose! Answer is "<<correctNum<<endl;
                isGameOver=1;
            }
        }
        else if(guessNum=correctNum)
        {
            //listGuess.push_back(guessNum) ;
            guessArray[guessCount]=guessNum;
                //k++;
                guessCount++;
            cout<<"You win!"<<endl;
            isGameOver=1;
        }
    }
/*
    int j=1;
    for(int i=listGuess.begin(); i !=listGuess.end(); ++i)
    {
        cout<< j<< " - "<<i;
        j++;
    }
*/
//印出陣列內的植
    int j=1;
    for(int i=0;i<guessCount;i++)
    {
        cout<<j<< " - "<<guessArray[i]<<endl;
        j++;
    }



        return 0;
}
