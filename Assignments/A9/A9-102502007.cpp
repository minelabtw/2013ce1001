#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    int lowerbound=0,upperbound=100,input=0,frequency=0;//初始化上下限為0和100,frequency為計數器，存放陣列時會用到
    srand(time(NULL));//打亂時間種子
    int randnumber=rand()%101;//設定亂數
    int record[100]= {}; //要100個陣列儲存每次輸出的值，因為最多輸出100次
    do
    {
        cout<<"Enter number("<<lowerbound<<"<=number<="<<upperbound<<")?: ";
        cin>>input;
        if(input<lowerbound or input>upperbound)
        {
            cout<<"Out of range!"<<endl;
            continue;
        }//這邊用了continue可以過濾超出範圍的輸入值，不會被儲存
        record[frequency]=input;//陣列第1個數為(record[0])
        frequency += 1;//利用計數器將每次的輸出儲存至陣列
        if(input<randnumber)
            lowerbound=input+1;//如果輸入值小於該數字，則將下限改為輸入值+1
        else if(input>randnumber)
            upperbound=input-1;//如果輸入值大於該數字，則將上限改為輸入值-1
        if(upperbound==lowerbound)
        {
            cout<<"You lose! Answer is "<<randnumber<<endl;
            break;//如果上下限相等，則玩家輸了，結束遊戲並跳出迴圈
        }
        else if(input==randnumber)
        {
            //如果輸入值等於數字，則玩家贏了，結束遊戲並跳出迴圈
            cout<<"You win!"<<endl;//這邊不用break,因為此時if的條件已經滿足脫離迴圈的條件
        }
        else;
    }
    while(input<lowerbound or input>upperbound);
    for(int i=1; i<=frequency; i++)//印出每次輸出的值
        cout<<i<<" - "<<record[i-1]<<endl;//陣列是[i-1]因為i為1且frequency設為0
    return 0;
}
