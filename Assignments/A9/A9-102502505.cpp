#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int x;//設定輸入值
    int maxnum = 100;//先設定上限為100
    int minnum = 0;//下限為0
    int a[100];//陣列大小最大為100
    int i=0;//陣列變數
    int l;//陣列變數的最大數

    srand(time(0));//以time(0)做種子
    int randnum = rand()%101;//把任何數除以101取餘數，就可以把隨機數控制在100以內

    while(randnum != x)//設迴圈，使得輸入數不為答案是皆在此迴圈內
    {
        do//設do while來重複下列動作
        {
            cout << "Enter number(" << minnum << "<=number<=" << maxnum << ")?: ";//輸出字串
            cin >> x;
            while( x < minnum or x > maxnum )
            {
                cout << "Out of range!" << endl;
                cout << "Enter number(" << minnum << "<=number<=" << maxnum << ")?: ";
                cin >> x;
            }

        }while( x < minnum && x > maxnum );

            if( x == randnum )//假設輸入數為亂數，則符合此條件，跑出下列字串，跳出上述迴圈
                cout << "You win!" << endl;
            else if(x > randnum)//假設輸入數大於亂數
                maxnum = x-1;//則改變上限值
            else
                minnum = x+1;//其餘則改變下限值

            if( x == maxnum)//假設輸入數為上限
                maxnum--;//則上限-1
            if(x == minnum)//假設輸入數為下限時
                minnum++;//則下限+1

            if( x+1 == maxnum or x-1 ==minnum )//假設輸入的數使得"可猜的數"只剩答案時，則輸了
            {
                cout << "You lose! Answer is " << maxnum << endl;
                break;//並跳出迴圈
            }

        a[i]=x;//每當跑過一次迴圈，則記錄輸入的數
        i++;//讓輸入數可以繼續記錄下去
    }

    l=i;//把l變為要輸出陣列多少個的標準

    for (i=0; i<l; i++)//設for迴圈，輸出陣列所擁有的值，但不包括最後一個
    {
        cout << i+1 << " - " << a[i] << endl;
    }

    cout << i+1 << " - " << x;//並輸出最後輸入的值

    return 0;//返回初始值
}
