#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

// input function
void input(int &n, const int &range_min, const int &range_max)
{
	// show ui
	do
	{
		cout << "Enter number(" << range_min << "<=number<=" << range_max << ")?: ";
		cin >> n;
	} while ((n < range_min || n > range_max) && cout << "Out of range!" << endl);
}

int main()
{
	// init random seed
	srand(time(NULL));
	
	int ans = rand() % 101; // random number
	int guess = -1;
	int _min = 0;
	int _max = 100;
	bool win = true; // flag
	int arr[101] = {-1}; // save record
	int k = 0; // count
	while (guess != ans)
	{
		input(guess, _min, _max);
		arr[k] = guess;
		k++;
		if (guess > ans)
			_max = guess - 1;
		else if (guess < ans)
			_min = guess + 1;
		if (_min == _max)
		{
			cout << "You lose! Answer is " << _min << endl;
			win = false;
			break;
		}
	}
	// check win
	if (win)
		cout << "You win!" << endl;
	// print record
	for (int i=0;i<k;i++)
		cout << i+1 << " - " << arr[i] << endl;
	return 0;
}