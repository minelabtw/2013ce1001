#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <ctime>
using namespace std;

int main()
{
    //srand((time(0)));
    int answer = rand()%101; //跑出隨機數字
    int number;
    int count =0; //宣告
    int a=100;
    int b=0;
    int apply[100]= {}; //array apply有100個數

    do //執行
    {
        cout<<"Enter number("<<b<<"<=number<="<<a<<")?: "; //輸出
        cin>>number; //輸出
        if(number<b || number>a) //條件
            cout<<"Out of range!\n";
        if(number>answer && number<a)
        {
            a=number-1;
            count++; //count+1
            apply[count]=number;

        }
        if(number<answer && number>b)
        {
            b=number+1;
            count++;
            apply[count]=number;

        }
        if(number==answer)
        {
            cout<<"You win!"<<endl;
            for(int i=1; i<=count; i++) //迴圈
                cout<<i<<" - "<<apply[i]<<endl;
            break; //跳出迴圈
        }
        if(a==b)
        {
            cout<<"You lose! Answer is "<<answer<<endl;
            for(int i=1; i<=count; i++)
                cout<<i<<" - "<<apply[i]<<endl;
        }
    }while(a!=b);
    return 0;
}
