#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int number = 0 , MaxNum = 100 , MinNum = 0 , p = 1 ; //宣告最大值=100,最小值=0

    srand( time(0) ) ;
    int randNum = rand() % 101 ; //隨機亂數,讓亂數出現在0~100之間

    int play[ 100 ] = {} ; //設定陣列

    do
    {
        do
        {
            do
            {
                cout << "Enter number(" << MinNum << "<=number<=" << MaxNum << ")?: " ;
                cin >> number ;
                if ( number < MinNum || number > MaxNum )
                    cout << "Out of range!" << endl ;
            }
            while ( number < MinNum || number > MaxNum ) ;

            play [ p ] = number ; // 當輸入數字有效時,p+1變繼續猜密碼
             p++ ;

            if ( number < randNum )//當輸入小於答案的情況
                MinNum = number +1 ;

            else if ( number > randNum ) //當輸入大於答案的情況
                MaxNum = number -1 ;

            if ( MinNum == MaxNum )
            {
                cout << "You lose! Answer is " << randNum << endl ; //輸的結果
                break ;
            }

            else if ( number == randNum ) //贏的結果
                cout << "You win!" << endl ;
        }
        while ( number != randNum ) ;

        if ( MinNum == MaxNum )
            break ;

        if ( number == randNum )
            break ;

    }
    while( number > 0 && number < 100 ) ;

    for (int i = 1 ; i < p ; i++ )        //輸出猜密碼的過程
        cout << i << " – " << play[i] << endl ;

    return 0;
}
