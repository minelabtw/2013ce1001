#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0)); //隨機變數
    int a=(0+rand()%101); //宣告變數a的範圍為0到100
    int b; //宣告變數
    int c=0;
    int d=100;
    int e=0;
    int number[101]= {}; //宣告陣列大小
    do
    {
        cout <<"Enter number("<<c<<"<=number<="<<d<<")?: "; //輸出
        cin >>b; //輸入
        if (b>=c && b<=d)
        {
            e++;
            number[e]=b; //b值儲存到陣列
        }
        if (b<c || b>d) //判斷變數
            cout <<"Out of range!"<<endl;
        else if (b<a && b!=d-1)
            c=b+1;
        else if (b>a && b!=c+1)
            d=b-1;
    }
    while (b!=d-1 && b!=c+1 && b!=a); //迴圈
    if (b==a) //判斷變數
        cout <<"You win!";
    else if (b==d-1 || b==c+1)
        cout <<"You lose! Answer is "<<a;
    for (int f=1; f<=e; f++) //輸出陣列
        cout <<endl<<f<<" - "<<number[f];
    return 0;
}
