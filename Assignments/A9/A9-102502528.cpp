#include<iostream>
#include<cstdlib>
#include <time.h>
using namespace std;

int main()
{
    srand(time(0));                         //依時間出亂數
    int ans =(rand()%101),input;           //指定一個介於1-100的亂數
    int Min = 0,Max = 100,counter = 1;
    int answer[100] = {};
    while(input != ans)
    {
        cout << "Enter number("<< Min<< "<=number<=" << Max << ")?: ";
        cin >> input;
        answer[counter] = input;        //用陣列紀錄輸入情形
        counter ++;                     //跳下一個數字
        while(input < Min || input > Max)         //判定輸入範圍
        {
            cout << "Out of range!" <<endl;
            cout << "Enter number("<< Min<< "<=number<=" << Max << ")?: ";
            cin >> input;
        }
        if(input == ans)         //正解，跳出循環
            cout << "You win!" << endl;
        else if(input > ans)
            Max = input - 1;
        else
            Min = input + 1;
        if(Max == Min)           //失敗，顯示答案並跳出循環
        {
            cout << "You lose! Answer is " << ans << endl;
            break;
        }
    }
    for(int i=1; i<counter; i++)                 //輸出陣列內容
        cout << i << " - " << answer[i] << endl;

    return 0;
}

