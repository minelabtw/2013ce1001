#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
    unsigned seed;
    int number = 0;
    int num1 = 0;
    int num2 = 100;
    const int arraysize = 100;
    int save[arraysize] = {};
    int i = 0;
    cout << "Please enter seed: ";
    cin >> seed;
    srand(seed);//打散亂數
    int bingo = rand()%101;
    do
    {
        cout << "Enter number(" << num1 << "<=number<=" << num2 << ")?: ";
        cin >> number;
        if(number<0 || number>100 || number<num1 || number>num2)//超出範圍輸出Out of range!
            cout << "Out of range!" << endl;
        else
        {
            if(number<bingo)
                num1 = number+1;
            else if((number>bingo))
                num2 = number-1;
            else//猜到數字輸出You win!
                cout << "You win!" << endl;
            if(num1==num2)//無範圍可猜輸出You lose! Answer is
            {
                cout << "You lose! Answer is " << bingo << endl;
            }
        save[i]=number;//用陣列儲存number
        i++;//i加一
        }
    }
    while(number!=bingo && num1!=num2);//未猜到數字和有範圍可猜時迴圈
    for(int j=0; j<i; j++)
    {
     cout << j+1 << " - " << save[j] << endl;
    }
    return 0;
}
