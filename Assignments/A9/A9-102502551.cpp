#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int a,min=0,max=100,b,c=0,i,j;
    float num[100];

    srand(time(NULL));                                                                              //亂數
    a=(rand()%100)+1;


    for(i=0;i<100;i++)                                                                                   //讓他持續做
    {
        cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
        cin>>num[i];
        while(num[i]>max || num[i]<min)                                                                       //判斷是否在範圍內
        {
            cout<<"Out of range!"<<endl<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
            cin>>num[i];
        }                                                                                           //判斷各情況
        if(num[i]>a)
        {
            max=num[i]-1;
        }
        if(num[i]<a)
        {
            min=num[i]+1;
        }
        if(num[i]==a)
        {
            cout<<"You win!"<<endl;
            for(j=0;j<i+1;j++)
            {
                cout<<j+1<<" - "<<num[j]<<endl;
            }
            break;
        }
        if(min==a && max==a)
        {
            cout<<"You lose! Answer is "<<a<<endl;
            for(j=0;j<i+1;j++)
            {
                cout<<j+1<<" - "<<num[j]<<endl;
            }
            break;
        }
    }
    return 0;
}
