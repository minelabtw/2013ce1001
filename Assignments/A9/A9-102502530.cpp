#include<iostream>
#include<ctime>
#include<cstdlib>

int main()
{
   int num,input;    //declaration
   int left=0,right=100;
   int log[100],now=0;

   srand(time(NULL));   //start random
   num=rand()%101;

   do
   {
      std::cout<<"Enter number("<<left<<"<=number<="<<right<<")?: ";    //input
      std::cin>>input;

      if(input<left||input>right)   //out of range
         std::cout<<"Out of range!\n";
      
      else if(num!=input)  //wrong
      {
         left=num>input?input+1:left;
         right=num<input?input-1:right;
         log[now++]=input;
         if(left==right)   //lose
         {
            std::cout<<"You lose! Answer is "<<num<<'\n';
            break;
         }
      }

      else  //win
      {
         log[now++]=input;
         std::cout<<"You win!\n";
         break;
      }

   }while(1);

   for(int i=0;i!=now;i++)   //log
      std::cout<<i+1<<" - "<<log[i]<<'\n';

   return 0;
}
