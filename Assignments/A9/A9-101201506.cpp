#include<iostream>
#include<cstdlib>  //contain srand() and rand()
#include<ctime>    //contain times()

using namespace std;

int main()
{
    int number=-1; //輸入的數字
    int Max=100; //上限
    int Min=0; //下限
    int s[101]; //正列
    int i=0,n=0; //i 給for 用 n是算輸入幾次
    srand(time(0)); //創造隨機的數字
    int randNum= rand()%101; //指派randNum為隨機的數字

    while(number!=randNum)
    {
        do //確認number範圍
        {
            cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>number;
            if(number<Min || number>Max)
                cout<<"Out of range!"<<endl;
        }
        while(number<Min || number>Max);

        n++; //看輸入幾次 然後存在正列裡面
        s[n-1]=number;

        if(number==randNum) //猜中時
            cout<<"You win!"<<endl;
        else if(number>randNum) //輸入數大於答案時
            Max=number-1;
        else //輸入數小於答案時
            Min=number+1;

        if(Max==Min) //猜到沒東西可以猜了 顯示輸了
        {
            cout<<"You lose! Answer is "<<Max<<endl;
            break;
        }

    }

    for(i=1;i<=n;i++) //最後顯示出來 之前打的數字
        cout<<i<<" - "<<s[i-1]<<endl;

    return 0;
}
