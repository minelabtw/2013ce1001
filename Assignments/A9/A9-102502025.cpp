#include <iostream>
#include <iomanip>//賦予setw指令
#include <stdlib.h>//賦予srand指令
#include <ctime>//賦予time指令
using namespace std;

int main()
{
    srand(time(0));//讓設一變數的初始值為零
    int number=rand()%101;//設一變數不超過101
    int input=-1;
    int MAX=100;
    int MIN=0;
    int s[100];//令陣列
    int k=1;

    while(input!=number)//當input不等於number時進行迴圈
    {
        while(input<MIN || input>MAX)//進行不適當輸入值迴圈
        {
            cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?: ";
            cin >> input;
            if(input<MIN || input>MAX)
            {
                cout << "Out of range!\n";
            }
        }
        if(input>=MIN && input<=MAX)//假如輸入正確，把值加入陣列
        {
            s[k]=input;//當k=某數字時，等於什麼輸入
            k++;
        }
        if(input==number)//正確的話輸出勝利
        {
            cout << "You win!\n";
        }
        if(input>number && input<=MAX)//壓縮最高值
        {
            MAX=input-1;

        }
        else if(input<number && input>=MIN)//壓縮最低值
        {
            MIN=input+1;

        }
        if(MIN==MAX)//當最高值=最低值時輸出失敗
        {
            cout << "You lose! Answer is " << number << endl;
            break;
        }
    }

    for(int l=1; l<k; l++)//輸出陣列值
    {
        cout << setw(3) << l << " - " << setw(3) << s[l] << endl;
    }
    return 0;
}
