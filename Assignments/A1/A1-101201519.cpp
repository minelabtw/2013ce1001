#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main(){

    int length,width;
    cout << "Please input the length of a rectangle:";
    cin >> length;
    cout << "Please input the width of a rectangle:";
    cin >> width;

    cout <<"The perimeter of the rectangle:" << (length+width)*2 << endl;
    cout <<"The area of the rectangle:" << length*width << endl;

    return 0;


}
