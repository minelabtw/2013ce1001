#include<iostream>

int main()
{
    int length,width;		//宣告長與寬

    std::cout<<"請輸入長度：";	//輸出"請輸入長度："
    std::cin>>length;		//輸入長

    std::cout<<"請輸入寬度：";	//輸出"請輸入寬度："
    std::cin>>width;		//輸入寬

    std::cout<<"周長="<<2*length+2*width<<'\n'<<"面積="<<length*width<<'\n';	//輸出結果
}
