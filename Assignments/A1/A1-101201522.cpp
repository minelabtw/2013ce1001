#include <iostream>
using namespace std;

int main(){

    int length = 0; //宣告一整數為長度，並初始化為0
    int width = 0; //宣告一整數為寬度，並初始化為0

    cout << "請輸入長度:";
    cin >> length; //輸入長度
    cout << "請輸入寬度:";
    cin >> width; //輸入寬度

    cout << "周長=" << (length+width)*2 << endl; //計算周長並輸出
    cout << "面積=" << length*width << endl; //計算面積並輸出

    return 0;
}
