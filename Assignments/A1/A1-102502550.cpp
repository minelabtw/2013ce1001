#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int L,W;     //宣告兩個整數變數。
    int perimeter= 0;                        //宣告名稱為perimeter的整數變數，並初始化其數值為0。
    int area=0;                 //宣告名稱為area的整數變數

    cout << "請輸入長度：";        //將字串"請輸入長度："輸出到螢幕上。
    cin >> L;                     //從鍵盤輸入，並將輸入的值給L

    cout << "請輸入寬度：";
    cin >> W;



    perimeter = (L + W)*2 ;   //將兩個整數相加再乘2，並將值丟給perimeter
    cout << "周長=" << perimeter<< endl;

    area = L * W;   //將兩個整數相乘，並將值丟給area
    cout << "面積=" << area ;

    return 0;
}
