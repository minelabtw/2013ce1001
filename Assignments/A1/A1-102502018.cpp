#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1;
    int integer2;                      //宣告二個整數變數。
    int sum1 = 0;                      //宣告名稱為sum1的整數變數，並初始化其數值為0。
    int sum2 = 0;                      //宣告名稱為sum2的整數變數，並初始化其數值為0。

    cout << "請輸入長度：";            //將字串"請輸入長度："輸出到螢幕上。
    cin >> integer1;                   //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";            //將字串"請輸入寬度："輸出到螢幕上。
    cin >> integer2;                   //從鍵盤輸入，並將輸入的值給integer2。


    sum1 = (integer1 + integer2)*2;    //將二個整數相加後乘以2，並將相加後的值丟給sum1。
    cout << "周長為：" << sum1 << endl;
    sum2 = integer1 * integer2;        //將二個整數相乘，並將相乘後的值丟給sum2。
    cout << "面積為：" << sum2 ;

    return 0;
}
