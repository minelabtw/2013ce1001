#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length = 0,width = 0;     //宣告型別為int的數
    int perimeter = 0;            //宣告型別為int"周長"，初始化值為0
    int area = 0;                 //宣告型別為int"的面積"，初始化值為0


    cout << "請輸入長度：";
    cin >> length;

    cout << "請輸入寬度：";
    cin >> width;

    perimeter = (length + width)* 2;     //周長
    cout << "perimeter=" << perimeter << endl;

    area = length * width;                 //面積
    cout << "area=" << area;

    return 0;
}
