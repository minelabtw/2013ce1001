#include<iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int length=0;   //宣告型別為 整數 的 長度
    int width=0;    //宣告型別為 整數 的 寬度

    cout << "請輸入長度：";
    cin >> length ;

    cout << "請輸入寬度：";
    cin >> width ;

    cout << "周長=" << (length+width)*2 << endl;  //計算周長並列印出結果
    cout << "面積=" << length*width ;             //計算面積並列印出結果

    return 0 ;
}
