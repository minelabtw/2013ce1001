#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int l=0,w=0;         //宣告兩變數為整數 分別為長度與寬度 並定義其初始值為0
    int perimeter=0;     //宣告變數perimeter周長為整數 並定義其初始值為0
    int area=0;          //宣告變數area面積為整數 並定義其初始值為0


    cout << "請輸入長度：";
    cin >> l;               //給變數長度l一值

    cout << "請輸入寬度：";
    cin >> w;               //給變數寬度w一值

    perimeter=(l+w)* 2;     //計算周長
    cout <<"周長="<<perimeter<<endl;

    area=l*w;               //計算面積
    cout <<"面積="<<area;

    return 0;
}

