#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int i1 = 0;                             //定義兩個變數 分別為長寬 兩個變數為 周長 面積
    int i2 = 0;
    int perimeter = 0;
    int area = 0;


    cout << "請輸入長度：";                 //給變數i1 長度的值
    cin >> i1;

    cout << "請輸入寬度：";                 //給變數i2 寬度的值
    cin >> i2;

    perimeter = (i1+i2)*2;                  //周長
    cout << "周長=" << perimeter << endl;

    area =  i1*i2;                          //面積
    cout << "面積=" << area;

    return 0;
}
