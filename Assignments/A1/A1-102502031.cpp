#include <iostream>

int main()
{
    // variable declarations
    int length; // the length of the rectangle
    int width; // the width of the rectangle
    int perimeter; // the perimeter of the rectangle
    int area; // the area of the rectangle

    std::cout << "請輸入長度： "; // prompt user for data
    std::cin >> length; // read first integer from user into length

    std::cout << "請輸入寬度： "; // prompt user for data
    std::cin >> width; // read second integer from user into width

    perimeter = 2 * length + 2 * width; // calculate the perimeter of the rectangle; store result in perimeter
    area = length * width; // calculate the area of the rectangle; store result in area

    std::cout << "周長 = " << perimeter << "\n"; // display perimeter; new line
    std::cout << "面積 = " << area << std::endl; // display area; end line

    return 0;
}
