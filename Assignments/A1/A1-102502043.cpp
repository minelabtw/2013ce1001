#include <iostream>

using namespace std;

int main()
{
    int length;                                  //宣告長度
    int width;                                   //宣告寬度
    cout << "請輸入長度: " << endl;              //長度
    cin  >> length;                              //輸入長度
    cout << "請輸入寬度" << endl;                //寬度
    cin  >> width;                               //輸入寬度
    cout << "面積:" << length*width << endl;     //計算面積
    cout << "周長:" << 2*(length+width) << endl; //計算周長
    return 0;
}
