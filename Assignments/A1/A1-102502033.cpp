#include <iostream>
int main()
{
    int length;//宣告整數變數
    int wide;//宣告整數變數
    int area;//宣告整數變數，用來存取長乘寬的值
    int perimeter;//宣告整數變數，用來存取周長的值

    std::cout << "請輸入長度:";//輸出到螢幕上
    std::cin  >> length;//輸入長度
    std::cout << "請輸入寬度:";
    std::cin  >> wide;//輸入寬度

    perimeter = (length+wide)*2;//將算出的周長存取到perimeter
    std::cout << "周長=" << perimeter <<"\n";
    area = length*wide;//將算出的面積存取到area
    std::cout << "面積=" << area;

    return 0;

}
