#include <iostream>

using namespace std;

int main()
{
    int a;                              //設定長方形長的變數為a
    int b;                              //設定長方形寬的變數為b

    cout << "請輸入長度：";             //將字串"請輸入長度："輸出到螢幕上
    cin >> a;                           //從鍵盤輸入，並將輸入的值給a

    cout << "請輸入寬度：";             //將字串"請輸入寬度："輸出到螢幕上
    cin >> b;                           //從鍵盤輸入，並將輸入的值給b

    cout << "周長=" << 2*(a+b) << endl; //將兩變數相加後乘二得到周長並輸出結果到螢幕上
    cout << "面積=" << a*b << endl;     //將兩變數相乘後得到面積並輸出結果到螢幕上

    return 0;
}
