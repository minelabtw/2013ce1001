#include <iostream>

using namespace std;

int main()
{
    int length = 0; //宣告長度並初始化其數值為 0
    int width = 0;  //宣告寬度並初始化其數值為 0

    cout << "請輸入長度：" ;
    cin >> length;

    cout << "請輸入寬度：" ;
    cin >> width;

    cout << "周長：" << 2 * ( length + width ) << endl;
    cout << "面積：" << length * width << endl;

    return 0;
}
