#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;     //宣告二個整數變數。
    int perimeter = 0;                        //宣告名稱為perimeter的整數變數。用來存放(integer1 + integer2)*2後的值
    int area;                 //宣告名稱為area的整數變數。用來存放integer1 * integer2後的值

    cout << "請輸入長度：";        //將字串"請輸入第一個整數："輸出到螢幕上
    cin >> integer1;                     //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";
    cin >> integer2;

    perimeter = (integer1 + integer2)*2;   //將四個整數相加，並將相加後的值丟給perimeter。
    cout << "周長為：" << perimeter << endl;

    area = integer1 * integer2;   //將integer1 * integer2，並將乘後的值丟給area。
    cout << "面積為：" << area ;

    return 0;
}
