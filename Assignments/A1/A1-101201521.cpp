#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1, number2;
    //Enter inputs
    cout << "請輸入長度:";
    cin >> number1;
    cout << "請輸入寬度:";
    cin >> number2;
    //Providing answers
    cout << "周長=" << 2 * ( number1 + number2 ) << endl;
    cout << "面積=" << number1 * number2 << endl;
    return 0;
}
