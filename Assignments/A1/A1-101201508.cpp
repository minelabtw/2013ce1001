#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1;                       //宣告一個整數變數，儲存長度
    int integer2;                       //宣告一個整數變數，儲存寬度
    int perimeter = 0;                  //宣告名稱為sum的整數變數為周長，並初始化數值為0。
    int area=0;                         //宣告名稱為area的整數變數，並初始化其數值為0。

    cout << "請輸入長度：";             //將字串"請輸入長度："輸出到螢幕上。
    cin >> integer1;                    //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";
    cin >> integer2;

    perimeter = 2*(integer1 + integer2);//將兩個整數相加之後再乘2，並將之後的值丟給perimeter。
    cout << "周長=" << perimeter << endl;

    area = integer1 * integer2;         //將長度乘以寬度，並將之後的值丟給area。
    cout << "面積=" << area ;

    return 0;
}
