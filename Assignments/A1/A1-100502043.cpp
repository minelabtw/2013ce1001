#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1,number2;  //宣告2整數變數
    int sum = 0; //宣告變數sum，並使其初始化為０
    int circumference; //宣告周長
    int area;  //宣告面積

    cout << "請輸入長度:";  //輸入字串至螢幕上
    cin >> number1;  //將鍵盤輸入的值給number1

    cout << "請輸入寬度:";  //輸入字串至螢幕上
    cin >> number2;  //將鍵盤輸入的值給number2

    circumference = 2*( number1 + number2 );  //周長=兩倍長+寬
    cout << "周長=" << circumference << endl;  //輸出結果至螢幕並用endl換行

    area = number1 * number2;  //面積=長*寬
    cout << "面積=" << area;


    return 0;
}

