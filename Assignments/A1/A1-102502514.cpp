#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
     int interger1,interger2;  //宣告兩個整數變數
     int circumference = 0;  //宣告周長為整數變數,並初始化其數值為零
     int area = 0;  //宣告面積為整數變數,並初始化其值為零

     cout << "請輸入長度:";  //將字串"請輸入長度:"輸出至螢幕上
     cin >> interger1;  //從鍵盤輸入,並將其值給interger1

     cout << "請輸入寬度:";  //將字串"請輸入長度:"輸出至螢幕上
     cin >> interger2;  //從鍵盤輸入,並將其值給interger2

     circumference = interger1*2 + interger2*2;  //將二個整數乘以二後相加，並將其值丟給circumference
     cout << "周長:" << circumference << endl;

     area = interger1*interger2; //將兩個變數相乘，並將其值丟給area
     cout << "面積:" << area;

     return 0;
}
