#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;     //宣告兩個整數變數。
    int circumference = 0;                        //宣告名稱為circumference的整數變數，並初始化其數值為0。
    int measure = 0;                 //宣告名稱為measure的整數變數，用來存放兩個變數的相乘結果。

    cout << "請輸入長度：";        //將字串"請輸入長度："輸出到螢幕上。
    cin >> integer1;                     //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";
    cin >> integer2;

    circumference = (integer1 + integer2)*2;   //將周長算出，並將相加後的值丟給circumference。
    cout << "周長=" << circumference << endl;

    measure = integer1 * integer2;   //將兩個整數相乘，並將值丟給measure。
    cout << "面積=" << measure ;

    return 0;
}
