#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int Length,Width;   //宣告長度和寬度的變數.
    int Perimeter;      //宣告名稱為Perimeter的變數,用來存放(Length+Width)*2
    int Area;           //宣告名稱為Area的變數,用來存放Length*Width

    cout << "請輸入長度:";   //將字串"請輸入長度:"輸出到螢幕上
    cin >> Length;           //從鍵盤輸入一個值,並將輸入的值給Length

    cout << "請輸入寬度:";   //將字串"請輸入寬度:"輸出到螢幕上
    cin >> Width;            //從鍵盤輸入一個值,並將輸入的值給Width

    Perimeter = (Length + Width) * 2;       //將(Length+Width)*2,並將值給Perimeter
    cout << "周長=" << Perimeter << endl;

    Area = Length * Width;                  //將Length*Width,並將值給Area
    cout << "面積=" << Area ;

    return 0;
}
