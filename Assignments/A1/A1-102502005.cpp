#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length;                          //宣告名稱為length的整數變數。
    int width;                           //宣告名稱為width的整數變數。
    int perimeter;                       //宣告名稱為perimeter的整數變數。
    int aera;                            //宣告名稱為aera的整數變數。

    cout << "請輸入長度：";              //將字串"請輸入長度"輸出到螢幕上。
    cin >> length;                       //從鍵盤輸入，並將輸入的值給length。

    cout << "請輸入寬度：";              //將字串"請輸入寬度"輸出到螢幕上。
    cin >> width;                        //從鍵盤輸入，並將輸入的值給width。

    perimeter = length*2 + width*2 ;     //將length和width的值分別乘二後相加，相加後的值丟給perimeter。
    cout << "周長=" << perimeter << endl;//將字串"周長="以及perimeter的值輸出到螢幕上後換行。

    aera = length*width;                 //將length和width相乘，並將相乘後的值丟給aera。
    cout << "面積=" << aera << endl ;            //將字串"面積="以及aera的值出到螢幕上後換行。

    system ("pause");
    return 0;
}
