#include <iostream>
using namespace std ;

int main()
{
    int length=0 ; // 宣告長度
    int width=0 ; // 宣告寬度
    int perimeter=0 ; // 宣告周長
    int area=0 ; // 宣告面積

    cout << "請輸入長度:" ; //將字串"請輸入長度："輸出到螢幕上。
    cin >> length ; //從鍵盤輸入，並將輸入的值給length。

    cout << "請輸入寬度:"; //將字串"請輸入寬度："輸出到螢幕上。
    cin >> width ; //從鍵盤輸入，並將輸入的值給width。

    perimeter = ( length + width ) * 2 ; // 計算周長
    cout << "周長:" << perimeter << endl ; // 輸出周長

    area = length * width  ; //計算面積
    cout << "面積:" << area ; // 輸出面積

    return 0 ;


}
