#include <iostream>
using namespace std;

int main()
{
    int length,width;    //宣告length和width兩個整數變數
    int perimeter=0 ;    //宣告名字為perimeter的整數變數，並初始畫數值為0
    int area        ;     //宣告名字為area的整數變數,即面積
    cout <<"請輸入第一個整數:";//將字串"請輸入第一個整數："輸出到螢幕上
    cin  >>length;             //從鍵盤輸入，並將輸入的值給length
    cout <<"請輸入第二個整數:";//將字串"請輸入第二個整數："輸出到螢幕上
    cin  >>width;              //從鍵盤輸入，並將輸入的值給width
    perimeter=(length+width)*2;
    area=length*width;
    cout <<"周長為"<<perimeter<<"\n";
    cout <<"面積為"<<area<<"\n";

    return 0;
}
