#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;     //宣告兩個整數變數
    int perimeter = 0;                         //宣告名稱為perimeter的整數變數，並初始化其數值為0
    int area;                 //宣告名稱為area的整數變數。用來存放integer1 * integer2

    cout << "請輸入長度:";        //將字串"請輸入長度:"輸入到螢幕上。
    cin  >> integer1;                   //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度:";           //將字串"請輸入寬度:"輸入到螢幕上。
    cin  >> integer2;               //從鍵盤輸入，並將輸入的值給integer2。



    perimeter = (integer1 + integer2) * 2 ;    //將長和寬相加乘二，並將值丟給perimeter。
    cout << "周長:" << perimeter << endl;               //將字串"周長:"輸入到螢幕上。

    area = integer1 * integer2;   //將長與寬相乘後的值丟給ssquareInteger1。
    cout << "面積:" << area ;      //將字串"面積"輸入到螢幕上。

    return 0;
}
