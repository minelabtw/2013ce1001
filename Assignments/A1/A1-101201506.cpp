#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;     //宣告三個整數變數。
    int sum = 0;                        //宣告名稱為sum的整數變數，並初始化其數值為0。
    int squareInteger1;                 //宣告名稱為squareInteger1的整數變數。用來存放integer1平方後的值。

    cout << "請輸入長度：";        //將字串"請輸入第一個整數："輸出到螢幕上。
    cin >> integer1;                     //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";
    cin >> integer2;

    sum = integer1 + integer2 + integer1 + integer2  ;   //將三個整數相加，並將相加後的值丟給sum。
    cout << "周長：" << sum << endl;

    squareInteger1 = integer1 * integer2;   //將第一個整數平方，並將平方後的值丟給squareInteger1。
    cout << "面積：" << squareInteger1 ;

    return 0;
}
