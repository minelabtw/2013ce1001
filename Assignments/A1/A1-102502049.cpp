#include <iostream>
using namespace std;

int main()
{
    int length = 0;  //宣告型別為 整數(int) 的長度，並初始化其數值為0。
    int width = 0;   //宣告型別為 整數(int) 的寬度，並初始化其數值為0。
    int grith = 0;   //宣告型別為 整數(int) 的周長，並初始化其數值為0。
    int product = 0; //宣告型別為 整數(int) 的面積，並初始化其數值為0。

    cout << "請輸入長度:"; //顯示 請輸入長度
    cin >> length; //輸入長度

    cout << "請輸入寬度:"; //顯示 請輸入寬度
    cin >> width; //輸入寬度

    grith = 2*(length + width); //將長度與寬度相加乘2其值給grith
    cout << "周長=" << grith << endl; //顯示出周長

    product = length * width;   //將長度與寬度相乘其值給product
    cout << "面積=" << product; //顯示出面積

    return 0;
}
