#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;     //宣告兩個整數變數。
    int perimeter= 0;          //宣告名稱為perimeter的整數變數，並初始化其數值為0。
    int area;                 //宣告名稱為area的整數變數。用來存放integer1與integer2乘積的值。

    cout << "請輸入長度：";        //將字串"請輸入長度："輸出到螢幕上。
    cin >> integer1;               //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";        //將字串"請輸入寬度:"輸出到螢幕上。
    cin >> integer2;              //從鍵盤輸入,並將輸入的值給integaer2。


    perimeter = (integer1+integer2)*2;   //將兩個整數相加後再乘上2，並將得到的結果丟給周長。
    cout << "周長：" << perimeter<< endl;

    area = integer1 * integer2;   //將第一個整數乘上第二個整數，並將乘積的值丟給面積。
    cout << "面積：" << area ;

    return 0;
}
