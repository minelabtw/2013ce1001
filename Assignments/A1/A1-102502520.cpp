#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{

    int integer1;                   //宣告一個變數integer1作為長方形的一邊長
    int integer2;                   //宣告另一個常數integer2作為長方形另一個邊常

    int sum=0;                      //宣告sum的整數變數，並初始化其值為0
    int area;                       //宣告area的整數變數，用來存取面積


    cout<<"輸入一整數長方形邊長!\n";//將字串"輸入一整數長方形邊長!"輸出到螢幕上
    cin>>integer1;                  //從鍵盤輸入，並將輸入的值給integer1

    cout<<"輸入長方形另一個邊長!\n";
    cin>>integer2;

    sum=integer1*2+integer2*2;      //將兩整數乘2後相加，計算此長方形周長，並將值丟給sum
    cout<<"此長方形周長為:"<<sum<<endl;

    area=integer1*integer2;         //將兩整數相乘，計算此長方形面積，並將值丟給area
    cout<<"此長方形面積為:"<<area<<endl;

    return 0;
}
