#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1;  //宣告一個整數number1表示長度。
    int number2;  //宣告一個整數number2表示寬度。
    int sum1 = 0; //宣告一個整數變數sum1表示周長，其初始值為0。
    int sum2 = 0; //宣告一個整數變數sum2表示面積，其初始值為0。

    cout << "請輸入長度:";
    cin >> number1;

    cout << "請輸入寬度:";
    cin >> number2;

    sum1 = ( number1 + number2 ) * 2; //計算周長:長加寬乘以二
    cout << "周長=" << sum1 << endl;

    sum2 = number1 * number2;  //計算面積:長乘以寬
    cout << "面積=" << sum2 << endl;

    return 0;

}
