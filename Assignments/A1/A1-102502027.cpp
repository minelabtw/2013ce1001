#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;        //宣告兩個整數為變數
    int sum = 0;                  //宣告名稱為sum的整數變數，並初始化其數值為0
    int square;                   //宣告名稱為square的整數變數。用來存放integer1和integer2相乘


    cout<<"請輸入長度:";          //將字串"請輸入長度："輸出到螢幕上
    cin>> integer1;               // 從鍵盤輸入，並將輸入的值給integer1


    cout<<"請輸入寬度:";          //將字串"請輸入寬度："輸出到螢幕上
    cin>> integer2;               // 從鍵盤輸入，並將輸入的值給integer2


    sum = (integer1+integer2)*2;   //將兩個整數相加再乘二，並將值丟給sum
    cout<<"周長="<<sum<<endl;


    square = integer1*integer2;    //將長度和寬度相乘，並將值丟給square
    cout<<"面積="<<square;


    return 0;
}
