#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{


    int length=0,width=0;     //宣告型別為整數(int)的長度及寬度，並初始化其值為零。
    int perimeter = 0;        //宣告型別為整數(int)的周長，並初始化其值為零。
    int area=0;                //宣告型別為整數(int)的面積，並初始化其值為零。

    cout << "請輸入長度：";       //將字串"請輸入長度："輸出到螢幕上。
    cin >> length;               //從鍵盤輸入，並將輸入的值給length。

    cout << "請輸入寬度：";       //將字串"請輸入寬度："輸出到螢幕上
    cin >> width;                //從鍵盤輸入，並將輸入的值給width。



    perimeter = (length + width)*2 ;   //計算周長。
    cout << "周長為：" << perimeter << endl;

   area= length * width;   //計算面積。
    cout << "面積為：" << area ;

    return 0;
}
