#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main ()
{
int length,width; //宣告長跟寬
int sum=0; //宣告周長並將它初始化其值為0
int area=0; //宣告面積並將它初始化其值為0

cout<<"請輸入長度:"; //將字串"請輸入長度："輸出到螢幕上
cin>>length; //將鍵盤輸入的值給length

cout<<"請輸入寬度:"; //將字串"請輸入長度："輸出到螢幕上
cin>>width ; //將鍵盤輸入的值給width

sum=2*(length+width) ;//將右值計算完後給左值
area=length*width ; //將右值計算完後給左值

cout<<"周長="<<sum<<endl; //將sum值輸出在螢幕上

cout<<"面積="<<area<<endl;//將area值輸出在螢幕上

return 0 ;

}
