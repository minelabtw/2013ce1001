#include <iostream>

using namespace std;

int main()
{
    int integer1,integer2;     //宣告兩個整數變數。
    int area = 0;                        //宣告名稱為area的整數變數，並初始化其數值為0。
    int perimeter=0;         //宣告名稱perimeter的整數變數 並初始化其數值為0
    cout << "請輸入長：";        //將字串"請輸入長："輸出到螢幕上。
    cin >> integer1;                     //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬：";        //將字串"請輸入寬:"輸出在螢幕上
    cin >> integer2;        //從鍵盤輸入並將輸入的值给integer2


    area = integer1 * integer2 ;   //將長與寬相乘，並將相乘後的值丟給area。
    perimeter = 2*integer1 + 2*integer2 ;   //算出長方形周長 並將值丟給perimeter
    cout << "長方形面積為：" << area << "\n" ;  //將字串"長方形面積為:"輸出到螢幕上 並輸出面積的值
    cout << "長方形周長為:" << perimeter << endl ;//將字串""長方形周長為:"輸出到螢幕上 並輸出周長的值

    return 0;
}
