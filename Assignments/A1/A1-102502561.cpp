#include <iostream>
using std::cout;
using std::cin;


int main()
{
    int length; //宣告長度。

    int width;  //宣告寬度。

    int girth;  //宣告周長。

    int area;   //宣告面積。

    cout << "請輸入長度："; //將字串"請輸入長度："輸出到螢幕上。
    cin >> length;         //從鍵盤輸入，並將輸入的值給length。

    cout << "請輸入寬度："; //將字串"請輸入寬度："輸出到螢幕上。
    cin >> width;          //從鍵盤輸入，並將輸入的值給width。

    girth = (length + width)*2 ; //將兩個數相加後乘以二，並將計算後的值丟給girth。
    cout << "周長=" << girth << "\n";  //將"周長 = (計算結果)"輸出到螢幕上。

    area = length * width; //將兩個數相乘，並將計算後的值丟給area。
    cout << "面積=" << area;   //將"面積 = (計算結果)"輸出到螢幕上。

    return 0;
}
