// Assessment 1 - 9/26
// Basic geometrical test
#include <iostream>

// function main begins execution
int main()
{
    // Declarations
    int integer1;    // a length to add
    int integer2;   // a width to add
    int perimeter;  // display the value of the count
    int area;  // display the value of the count

    std::cout << "請輸入長度:"; // prompt user for data
    std::cin >> integer1; // read first integer from user into integer1

    std::cout <<"請輸入寬度:"; // prompt user for data
    std::cin >> integer2; // read second integer grom user into integer2

    perimeter = ( integer1 + integer2 ) * 2; // calculate the perimeter; send result in perimeter
    std::cout << "周長=" << perimeter << std::endl; // displays perimeter; end line

    area = integer1 * integer2; // calculate the area of the numbers; store result in area
    std::cout << "面積=" << area << std::endl; // displays area; end line

    return 0; // indicate that program end successfully

} // the end
