#include <iostream>

using  std::cout ;
using  std::cin ;
using  std::endl ;

int main()
{
   int length , width ; //宣告此長方形的長與寬
   int perimeter = 0 ; //宣告此長方形的周長,並初始化其數值為0
   int area ; //宣告此長方形之面積

   cout << "請輸入長度:" ; //將長方形的長度輸出在螢幕上
   cin >> length ;  //從鍵盤輸入,並將輸入的值給length

   cout << "請輸入寬度:" ;    //將長方形的寬度輸出在螢幕上
   cin >> width ;

   perimeter = length*2 + width*2 ; //為長方形的周長: (長 + 寬)*2,並將其值交給perimeter
   cout << "周長=" << perimeter << endl ;

   area = length * width ; //為長方形的面積: 長*寬,並將其值交給area
   cout << "面積=" << area ;

    return 0;
}
