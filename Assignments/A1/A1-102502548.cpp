#include <iostream>

using namespace std;

int main()
{
    int  length = 0;//宣告一個變數叫做"length"，並使其初始化值為0
    int  width  = 0;//宣告一個變數叫做"width"，並使其初始化值為0

    cout << "請輸入長度\n";//將字串"請輸入長度"輸出到螢幕上，並用"\n"換行

    cin  >>  length;//從鍵盤輸入，並將輸入的值給length

    cout << "請輸入寬度\n";//將字串"請輸入寬度"輸出到螢幕上，並用"\n"換行

    cin  >>  width;//從鍵盤輸入，並將輸入的值給width

    cout << "周長=" << (length+width)*2 << "\n";//將字串"周長="輸出到螢幕上，並計算(length+width)*2的值，並用"\n"換行
    cout << "面積=" << length*width;//length*width;將字串"面積="輸出到螢幕上，並計算length*width的值

    return 0;
}
