#include <iostream>
using std::cout;
using std::cin;
using std::endl ;

int main()
{
   int length;                 //declare variable of length
   int width;                  //declare variable of width
   int perimeter;              //declare variable of perimeter , and store the value of (length+width)*2 in perimeter
   int area;                   //declare variable of area , and store the value of length*width in area

   cout << "請輸入長度:"       //let the character string,請輸入長度:,output on the screen
   cin >> length;              //input the value by the keyboard , and store the value in length

   cout << "請輸入寬度:";
   cin >> width;

   perimeter = (length+width)*2;         //let the value of (length+width)*2 store in perimeter
   cout << "周長="<< perimeter << endl;

   area = length*width;                  //let the value of length*width store in area
   cout << "面積=" << area << endl;

   return 0;
}
