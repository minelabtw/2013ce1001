#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1,number2;       //宣告二個變數。
    int perimeter = 0;         //宣告名稱為perimeter的變數，並初始化其數值為0。
    int area = 0;                 //宣告名稱為area的變數。

    cout << "請輸入矩形的長度：";        //將字串"請輸入矩形的長度："輸出到螢幕上。
    cin >> number1;                     //從鍵盤輸入，並將輸入的值給number1。

    cout << "請輸入矩形的寬度：";
    cin >> number2;



    perimeter = number1*2 + number2*2;   //將number1乘以2再與number2乘以2相加的值丟給perimeter。
    cout << "矩形的周長為：" << perimeter << endl;

    area = number1 * number2;   //將number1乘以number2的值丟給area。
    cout << "矩形的面積為：" << area ;

    return 0;
}

