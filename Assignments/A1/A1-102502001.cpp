#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1; //宣告一個整數變數
    int integer2; //宣告第二個整數變數
    int sum ; //宣告名稱為sum的整數變數,以存放(integer1+integer2)*2之值
    int area1 ; //宣告名稱為area的整數變數,以存放integer1*integer2之值

    cout << "請輸入長度" ;  //將字串"請輸入長度"輸出到螢幕上
    cin >> integer1 ; //從鍵盤輸入,並將輸入的值給integer1

    cout <<"請輸入寬度" ;
    cin >>integer2 ;

    sum = (integer1 +integer2) *2 ; //將兩數相加乘以二
    cout << "周長=" << sum << endl ;

    area1 = integer1 *integer2 ; //將兩數相乘,將其值放入area1
    cout << "面積=" << area1 ;

    return 0 ;
    }
