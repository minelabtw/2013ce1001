#include <iostream>
using namespace std;
int main()
{
    int length;                                              //宣告一個整數型別變數當長度
    int width;                                               //宣告一個整數型別變數當寬度
    cout << "請輸入長度 : ";
    cin >> length;                                           //從cin輸入一個整數存到length裡面
    cout << "請輸入寬度 : ";
    cin >> width;                                            //從cin輸入一個整數存到width裡面
    cout << "周長=" << 2 * (length + width) << endl;         //輸出周長運算結果
    cout << "面積=" << length * width <<endl;                //輸出面積運算結果
    return 0;
}
