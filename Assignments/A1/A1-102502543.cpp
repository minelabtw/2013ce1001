#include<iostream>

using namespace std;


int main()
{
    int length = 0; //宣告長度
    int width = 0; //宣告寬度
    int perimeter = 0; //宣告周長
    int area = 0; //宣告面積


    cout << "請輸入長度："; //顯示"請輸入長度："
    cin >> length; //輸入長度

    cout << "請輸入寬度："; //顯示"請輸入寬度："
    cin >> width; //輸入寬度

    perimeter = ( length + width ) * 2; //計算周長
    cout << "周長=" << perimeter << endl; //輸出周長 換行

    area =  length * width; //計算面積
    cout << "面積=" << area; //輸出面積

    return 0;
}
