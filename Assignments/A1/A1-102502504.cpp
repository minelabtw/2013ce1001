#include <iostream>
using std::cout;
using std::cin;
using std::endl;
 
int main()
{
    int int1,int2;     //宣告兩個整數變數。
    int sum=0;         //宣告名稱為sum的整數變數，並初始化其數值為0。
    int area;          //宣告名稱為area的整數變數。用來存放int1乘以int2後的值。
 
    cout << "請輸入長度：";        //將字串"請輸入長度："輸出到螢幕上。
    cin >> int1;                   //從鍵盤輸入，並將輸入的值給int1。
 
    cout << "請輸入寬度：";        //將字串"請輸入寬度："輸出到螢幕上。
    cin >> int2;                   //從鍵盤輸入，並將輸入的值給int2。
 
    sum = int1*2 + int2*2 ;        //將兩個整數各乘以兩倍後相加，並將相加後的值丟給sum。
    cout << "周長為：" << sum << endl;
 
    area = int1 * int2;   //將第一個整數乘以第二個整數，並將值丟給area。
    cout << "面積為：" << area ;
 
    return 0;
}
