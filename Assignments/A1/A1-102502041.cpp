#include <iostream>                             //將iostream這個函式庫含括進來

using namespace std;                            //指名使用std這個名稱空間

int main()
{
    int length;                                 //宣告整數變數    長度
    int width;                                  //宣告整數變數    寬度
    int circumference;                          //宣告整數變數    周長
    int area;                                   //宣告整數變數    面積

    cout << "請輸入長度:";
    cin >> length;

    cout << "請輸入寬度:";
    cin >> width;

    circumference = length*2 + width*2;         //將長度和寬度分別乘以二再相加之後的值丟回給周長
    area = length * width;                      //將長度乘以寬度的值丟回給面積

    cout << "周長=" << circumference << endl;   //輸出字串"周長="和整數變數circumference
    cout << "面積=" << area;                    //輸出字串"面積="和整數變數area
    return 0;
}
