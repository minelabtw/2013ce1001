#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1; //宣告變數1
    int integer2; //宣告變數2
    int sum = 0; //宣告名稱為sum的整數變數，並初始化其數值為0
    int square; //宣告名稱為square的整數變數

    cout << "請輸入長度："; //將字串"請輸入長度："輸出到螢幕上
    cin >> integer1; //從鍵盤輸入，並將輸入的值給integer1

    cout << "請輸入寬度：";
    cin >> integer2;

    sum = ( integer1 + integer2 ) *2; //將兩個整數相加乘以二，並將值給sum
    cout << "周長=" << sum << endl;

    square = integer1 * integer2; //將兩個整數平方，並將平方後的值給square
    cout << "面積=" << square;

    return 0;
}
