#include<iostream>

using std::cout ;
using std::cin ;
using std::endl ;
int main()          //star
{
    int length ;    // define variable length;
    int width ;     // define variable width;
    int perimeter ; // define variable perimeter;
    int area ;      // define variable area;

    cout << "請輸入長度：" ;    //print instruction to scream
    cin >> length ;             //scan integer to length
    cout << "請輸入寬度：" ;    //print instruction to scream
    cin >> width ;              //scan integer to width
    cout << "周長=" << ( length + width ) * 2 << endl ; //print perimeter
    cout << "面積=" <<  length*width << endl ;                 //print area

    return 0;                                           // over
}
