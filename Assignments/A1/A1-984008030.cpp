#include<iostream>
using namespace std;
int main(){
    int length = 0;     //宣告型別為 整數(int) 的長，並初始化其值為0
    int width = 0;      //宣告型別為 整數(int) 的寬，並初始化其值為0
    int circum = 0;     //宣告型別為 整數(int) 的周長，並初始化其值為0
    int area = 0;       //宣告型別為 整數(int) 的面積，並初始化其值為0

    cout << "請輸入長度: ";
    cin >> length;
    cout << "請輸入寬度: ";
    cin >> width;

    circum = 2 * (length + width);  //計算周長
    area = length * width;          //計算面積

    cout << "周長=" << circum << endl;
    cout << "面積=" << area << endl;

    return 0;
}

