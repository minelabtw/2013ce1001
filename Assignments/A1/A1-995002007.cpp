// Assessment 1
// 由鍵盤輸入長方形之長與寬(皆為整數)，並輸出此長方形周長與面積

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int lengt;                      //宣告整數 lengt
    int wide;                       //宣告整數 wide
    int perimeter;                  //宣告整數 perimeter
    int area;                       //宣告整數 area

    cout << "請輸入長方形的長度: ";       //將字串"請輸入長方形的長度:"輸出螢幕上
    cin >> lengt;                        //由鍵盤輸入,並將輸入的直給lengt

    cout << "請輸入長方形的寬度: ";       //將字串"請輸入長方形的寬度:"輸出螢幕上
    cin >> wide;                         //由鍵盤輸入,並將輸入的直給wide

    perimeter=(lengt+wide)*2;               //把兩個整數相加然後乘2, 並將結果給perimeter
    cout << "周長:" << perimeter << endl;

    area = lengt*wide;                      //把兩個整數相乘, 並將結果給area
    cout << "面積:" << area << endl;

    return 0;
}
