#include<iostream>
using namespace std;

int main()
{
    int length = 0; //輸入一個變數length，並初始化為0
    int width = 0; //輸入一個變數width，並初始化為0

    cout << "請輸入長度和寬度" <<endl; //輸入字串，提示使用者輸入長度和寬度，並換行
    cin >> length; //使用者輸入長度
    cin >> width; //使用者輸入寬度

    cout << "周長" << ( length + width ) * 2 <<endl; //計算出周長並換行
    cout << "面積" << length * width; //計算出面積

    return 0;
}
