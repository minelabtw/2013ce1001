#include <iostream>
using namespace std;

/*
請寫出一個程式，能由鍵盤輸入一長方形之長與寬(皆為整數)，並輸出此長方形周長與面積。輸出結果如下所示:
請輸入長度：4
請輸入寬度：3
周長=14
面積=12
*/

int main()
{
    int rectangle_length, rectangle_width; //宣告整數變數。

    cout << "請輸入長度："; //輸出提示訊息到螢幕上。
    cin >> rectangle_length; //從鍵盤輸入值到變數rectangle_length。

    cout << "請輸入寬度："; //輸出提示訊息到螢幕上。
    cin >> rectangle_width; //從鍵盤輸入值到變數rectangle_width。

    cout << "周長=" << 2*(rectangle_length+rectangle_width) << endl; //計算並輸出周長。

    cout << "面積=" << rectangle_length*rectangle_width; //計算並輸出面積。

    return 0;
}
