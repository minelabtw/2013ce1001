#include <iostream>
using std::cout;//宣告使用sandard裡的參數"cout"。
using std::cin;//宣告使用sandard裡的參數"cin"。
using std::endl;//宣告使用sandard裡的參數"endl"。

int main ()
{
    int integer1 ;//宣告第一個整數
    int integer2 ;//宣告第二個整數
    int sum2=0 ;//宣告名稱為sum2的整數變數，並初始化其數值為0。
    int area=0 ;//宣告名稱為area的整數變數，並初始化其數值為0。

    cout << "請輸入長度：" ;//將字串"請輸入長度："輸出到螢幕上。
    cin >> integer1;//從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";//將字串"請輸入寬度："輸出到螢幕上。
    cin >> integer2;//從鍵盤輸入，並將輸入的值給integer2。

    sum2 = 2*(integer1 + integer2);   //將長度寬度相加，並將相加後的值丟給sum2。
    area = integer1*integer2;  //將長度寬度相乘，並將相乘後的值丟給area。

    cout << "周長=" << sum2 << endl; //將周長輸出到螢幕上，並換行。
    cout << "面積=" << area; //將面積輸出到螢幕上。

    return 0;
}
