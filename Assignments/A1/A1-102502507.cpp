#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2,integer3;
    int sum = 0;
    int squareInteger1;

    cout << "請輸入長度：";
    cin >> integer1;

    cout << "請輸入寬度：";
    cin >> integer2;

    sum = (integer1 + integer2)*2;
    cout << "周長=：" << sum << endl;

    squareInteger1 = integer1 * integer2;
    cout << "面積=：" << squareInteger1 ;

    return 0;
}
