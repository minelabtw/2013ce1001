#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length,width;             // 宣告兩個整數變數.
    int girth = 0;                // 宣告名稱為girth的整數變數,並初始化其數值為0.
    int area;                       // 宣告名稱為area的整數變數.用來存放
    cout<<"請輸入長度:";           //將字串"請輸入長度"輸出到螢幕上.
    cin >>length;                  //從鍵盤輸入,並將輸入的值給length.

    cout << "請輸入寬度:";
    cin >>width;

    girth=( length+width)*2 ;            //將兩個整數相加後並乘以二,並將加後的值丟給girth
    cout <<  "周長=" << girth << endl;

    area= length * width;                 //將長跟寬互乘,並將得到的數值丟給area
    cout << "面積=" << area ;

    return 0;

}
