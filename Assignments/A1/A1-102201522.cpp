#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length,wide;     //宣告三個整數變數。

    cout << "請輸入長度：";        //將字串"請輸入長度："輸出到螢幕上。
    cin >> length;                     //從鍵盤輸入，並將輸入的值給length。

    cout << "請輸入寬度：";        //將字串"請輸入寬度："輸出到螢幕上。
    cin >> wide;                       //從鍵盤輸入，並將輸入的值給width。

    cout << "周長=" << (length + wide)*2 << endl;
                  //將字串"周長="和((length + wide)*2)的值輸出到螢幕上，並以endl換行。
    cout << "面積=" << length * wide ;
                  //將字串"面積="和(length * wide)的值輸出到螢幕上，並以endl換行。
    return 0;
}
