#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length;//first integer to add
    int width;//second integer to add
    int perimeter;//sum of length and width then multiply 2
    int area;//multiply length and width

    cout << "請輸入長度:";//prompt user for data
    cin >> length;//read first integer from user into length

    cout << "請輸入寬度:";//prompt user for data
    cin >> width;//read second integer from user into width

    perimeter =  (length + width) * 2 ;//add numbers then multiply 2; store result in perimeter
    cout << "周長= " << perimeter << endl;//display perimeter; end line

    area = length * width;//multply numbers; store result in area
    cout << "面積= " << area;//display area

    return 0;
}
