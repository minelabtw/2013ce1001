#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()

    {
        int length;                                       //宣告長度個整數變數。
        int width;                                        //宣告寬度個整數變數。
        int perimeter = 0;                                //宣告名稱為perimeter的整數變數，並初始化其數值為0,用來存放兩個長度與兩個寬度相加的周長。
        int area =0;                                      //宣告名稱為area的整數變數，並初始化其數值為0,用來存放長度與寬度相乘的周長面積。

        cout << "請輸入長度：";                           //將字串"請輸入長度："輸出到螢幕上。
        cin >> length;                                    //從鍵盤輸入，並將輸入的值給length。

        cout << "請輸入寬度：";
        cin >> width;

        perimeter = length *2+ width * 2;                //將兩個長度與兩個寬度相加，並將相加後的值丟給perimeter。
        cout << "周長為：" <<perimeter <<endl;

        area = length * width;
        cout << "面積為：" << area ;                    //將長度與寬度相乘，並將平方後的值丟給area。

        return 0;
    }
