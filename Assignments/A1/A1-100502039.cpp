#include <iostream>
using namespace std;

int main()
{
    int length = 0;  //宣告一個變數length初值為0
    int width = 0;   //宣告一個變數width初值為0

    cout << "Please input length and width :" << endl; //輸出字串, 換行
    cin >> length;  //輸入length
    cin >> width;   //輸入width

    cout << "周長 : " << (length+width)* 2 << endl <<"面積 : " << length*width;  //輸出字串, 周長和面積的值, 換行

    return 0;
}
