#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main ()
{
    int integer1,integer2;  //宣告兩個整數變數。
    int girth = 0;          //宣告名稱為girth的整數變數，並初始化其數值為0。
    int area;               //宣告名稱為area的整數變數。用來存放integer1*integer2。

    cout << "請輸入長度:";  //將字串"請輸入長度:"輸出到螢幕上。
    cin >> integer1;        //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度:";
    cin >> integer2;

    girth = (integer1 + integer2) * 2;  //將兩個整數相加後乘2，並將得到的值丟給girth。
    cout << "周長=" << girth <<endl;

    area = integer1 * integer2;  //將兩個整數相乘，並將相乘後的值丟給area。
    cout << "面積=" << area;

    return 0;
}
