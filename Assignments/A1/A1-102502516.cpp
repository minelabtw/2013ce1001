#include <iostream>

using std::cout;
using std::cin;
using std::endl;


int main()
{
    int length;     //宣告長度的變數
    int width;      //宣告寬度的變數
    int aera =0;        //宣告面積變數並賦值為0
    int perimeter =0;   //宣告週長變數並賦值為0

    cout << "請輸入長度：" ;      //請使用者輸入長度
    cin >> length;                //把使用者輸入的數值給length變數

    cout << "請輸入寬度：";       //再請使用者輸入寬度
    cin >> width;                 //把使用者輸入的數值給width變數

    perimeter = ( length + width )*2;   //計算該矩形的周長，並把數值給perimeter
    aera = length * width;              //計算面積，再把數值給aera

    cout << "周長=" << perimeter << endl;     //在螢幕上印出運算結果和"周長＝"的字串
    cout << "面積=" << aera << endl << endl;  //以及面積的運算結果

    return 0 ;



}
