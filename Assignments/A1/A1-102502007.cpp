#include<iostream>
using namespace std;
int main()
{
    int length; //length to add
    int width; //width to add
    int perimeter; // perimeter of rectangle
    int area; //area of rectangle

    cout<<"請輸入長度:";
    cin>>length; //read length from user into length
    cout<<"請輸入寬度:";
    cin>>width; //read width from user into width

    perimeter=(length+width)*2; //compute the perimeter
    area=length*width; // compute the area

    cout<<"周長="<<perimeter<<endl;
    cout<<"面積="<<area;

    return 0;
}
