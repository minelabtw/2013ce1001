#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;               //宣告三個整數變數。
    int S = 0;                           //宣告名稱為S的整數變數，並初始化其數值為0。
    int T = 0;                           //宣告名稱為T的整數變數，並初始化其數值為0。

    cout << "請輸入長度：";              //將字串"請輸入長度："輸出到螢幕上。
    cin >> integer1;                     //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";              //將字串"請輸入寬度："輸出到螢幕上。
    cin >> integer2;                     //從鍵盤輸入，並將輸入的值給integer2。

    S = 2*integer1 + 2*integer2;         //將長與寬的兩倍相加得出周長，並將值丟給S
    cout << "周長 = " << S << endl;      //將字串"周長="和S的值輸出到螢幕上。

    T = integer1 * integer2;             //將長與寬相乘得出面積，並將值丟給T
    cout << "面積 = " << T << endl;      //將字串"面積="和T的值輸出到螢幕上。

    return 0;
}
