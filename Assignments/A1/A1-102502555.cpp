#include<iostream>

using namespace std;

int main()
{
    int length = 0;//長方形的長
    int width = 0;//長方形的寬

    cout << "請輸入長度：";//提示使用者輸入長度
    cin >> length;//輸入長度
    cout << "請輸入寬度：";//提示使用者輸入長度
    cin >> width;//輸入寬度

    cout << "周長=" << (length + width)*2 << endl;//輸出周長
    cout << "面積=" << length*width << endl;//輸出面積

    return 0;

}
