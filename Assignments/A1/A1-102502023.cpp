#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2; //宣告二個整數變數。
    int perimeter = 0; //宣告名稱為perimeter的整數變數，並初始化其數值為0。
    int area; //宣告名稱為area的整數變數。用來存放integer1*integer2。

    cout << "請輸入長度:"; //將字串"請輸入長度:"輸出到螢幕上。
    cin >> integer1; //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度:"; //將字串"請輸入寬度:"輸出到螢幕上。
    cin >> integer2; //從鍵盤輸入，並將輸入的值給integer2。

    perimeter = (integer1 + integer2)*2; //將二個數相加，並將相加後的值丟給perimeter。
    cout << "周長=" << perimeter << endl;

    area = integer1 * integer2; //將integer1 * integer2相乘，並將計算後的值丟給area。
    cout << "面積=" << area << endl;

    return 0;
}
