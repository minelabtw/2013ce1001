#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length;                         //宣告長與寬。
    int width;
    int sum1 = 0;                       //宣告名稱為sum1的整數變數，並初始化其數值為0。
    int sum2;                           //宣告名稱為sum2的整數變數。用來存放長與寬相乘的值。

    cout << "請輸入長度：";             //將字串"請輸入長度："輸出到螢幕上。
    cin >> length;                      //從鍵盤輸入，並將輸入的值給length。

    cout << "請輸入寬度：";             //將字串"請輸入寬度："輸出到螢幕上。
    cin >> width;                       //從鍵盤輸入，並將輸入的值給width。

    sum1 = 2*(length+width);             //將長與寬相加後乘二倍，並將結果丟給sum1。
    cout << "周長= " << sum1 << endl;

    sum2 = length*width;                 //將長與寬相乘後的值丟給sum2。
    cout << "面積= " << sum2;

    return 0;
}
