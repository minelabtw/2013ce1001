#include <iostream>
using std::cout;    //宣告使用standard裡的參數"cout"。
using std::cin;     //宣告使用standard裡的參數"cin"。


int main ()
{
    int integer1 ;  //宣告整數一
    int integer2 ;  //宣告整數二
    int length=0 ;  //宣告名稱為length的整數變數，並初始化其數值為0。
    int area=0 ;    //宣告名稱為area的整數變數，並初始化其數值為0。

    cout << "請輸入長度：";   //螢幕顯示"請輸入長度:"
    cin >> integer1;          //從鍵盤輸入數字，並將輸入的值給integer1。

    cout << "請輸入寬度：";  //螢幕顯示"請輸入寬度："
    cin >> integer2;         //從鍵盤輸入數字，並將輸入的值給integer2。

    length = 2*(integer1 + integer2); //將長度寬度相加，並將相加後乘以2的值丟給length。
    area = integer1*integer2;         //將長度寬度相乘，並將相乘後的值丟給area。

    cout << "周長=" << length << "\n";  //將"周長=length的值"輸出到螢幕上，並換行。
    cout << "面積=" << area;            //將"面積=area的值"輸出到螢幕上。

    return 0;
}
