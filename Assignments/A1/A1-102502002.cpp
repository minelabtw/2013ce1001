#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1;   //宣告第一個整數變數。
    int integer2;   //宣告第二個整數變數。
    int perimeter;            //宣告名稱為perimeter的整數變數。用以存放integer1,integer2的和。
    int area;                 //宣告名稱為area的整數變數。用以存放integer1,integer2的積。

    cout << "請輸入長度：";            //將字串"請輸入長度："輸出至螢幕。
    cin >> integer1;                 //由鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";            //將字串"請輸入寬度："輸出至螢幕。
    cin >> integer2;                 //由鍵盤輸入，並將輸入的值給integer2。

    perimeter = (integer1 + integer2)*2;   //兩數相加後成二，並將相計算後的值給perimeter。
    cout << "周長：" << perimeter << endl;

    area = integer1 * integer2;   //將兩數相乘，並將運算後的值丟給area。
    cout << "面積：" << area ;

    return 0;
}
