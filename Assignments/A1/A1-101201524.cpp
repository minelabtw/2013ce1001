#include <iostream>
 using namespace std;

 int main()
 {
     int length;                                             //宣告一個變數代表長度
     int width;                                              //宣告一個變數代表寬度

     cout << "請輸入長度:";                                  //將字串"請輸入長度："輸出到螢幕上。
     cin >> length;                                          //從鍵盤輸入，並將輸入的值給length。
     cout << "請輸入寬度:";                                  //將字串"請輸入寬度："輸出到螢幕上。
     cin >> width;                                           //從鍵盤輸入，並將輸入的值給width。

     cout << "周長=" << (length + width) * 2 << "\n";        //將字串"周長="輸出到螢幕上，將兩個數length,width相加再乘以2後輸出到螢幕上，並空行。
     cout << "面積=" << length * width << "\n";              //將字串"面積="輸出到螢幕上，將兩個數length,width相乘後輸出到螢幕上。

     return 0;

 }
