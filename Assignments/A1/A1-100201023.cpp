#include <iostream>

using namespace std;

int main()
{
	int length , width , perimeter , area;

	cout << "請輸入長度：";
	cin >> length;
	cout << "請輸入寬度：";
	cin >> width;

	perimeter = 2*(length + width);
	area = length * width;

	cout << "周長=" << perimeter << endl << "面積=" << area << endl;

	return 0;
}
