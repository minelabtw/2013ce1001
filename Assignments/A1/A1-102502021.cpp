#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2,integer3;   //宣告三個整數變數。
    int sum = 0;                      //宣告名稱為sum的整數變數，並初始化其數值為0。
    int squareInteger1;                //宣告名稱為squareInter1的變數整數。用來存放integer1相乘後的值。

    cout << "請輸入第一個整數:";      //將字串"請輸入第一個整數:"輸出到螢幕上。
    cin >> integer1;                  //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入第二個整數:";
    cin >> integer2;

    sum = integer1 *2 + integer2 *2;     //將兩個整數乘以2後相加，並將相加後的值丟給sum。
    cout << "周長:" << sum << endl;

    squareInteger1 = integer1 * integer2;  //將第一個整屬成以第二個整數，並將相乘後的值丟給squareInterger1。
    cout << "面積:" << squareInteger1 ;

    return 0;

}
