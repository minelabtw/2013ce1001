#include <iostream>
using namespace std;
int main()//開始主程式
{
	int length = 0;// 先設置一個長的變數
	int width = 0; //再設置一個寬的變數

	cout << "請輸入長度";//輸出字串提示使用者輸入長
	cin >> length;//使用者將輸入長

	cout << "請輸入寬度";//輸出字串提示使用者輸入寬
	cin >> width;//使用者將輸入寬

	cout << "算出的周長是" << (length + width) * 2 <<endl;//計算出周長並顯示,然後換行以保持美觀

	cout << "算出的面積是" << (length * width);//計算出面積並顯示

	return 0;//回歸0
}//結束主程式
