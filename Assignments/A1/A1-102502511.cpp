#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1; //宣告第一個整數變數
    int integer2; //宣告第二個整數變數
    int perimeter = 0; //宣告名稱為perimeter的整數變數，用來存放周長，並初始化其數值為0
    int area; //宣告名稱為area的整數變數，用來存放面積

    cout << "請輸入長度:"; //將字串"請輸入長度："輸出到螢幕上
    cin >> integer1; //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度:"; //將字串"請輸入寬度："輸出到螢幕上
    cin >> integer2; //從鍵盤輸入，並將輸入的值給integer1。

    perimeter = 2 * integer1 + 2 * integer2; //將兩個數相加後相乘，並將結果的值丟給perimeter。
    cout << "周長:" << perimeter << endl;

    area = integer1 * integer2; //將兩個數相乘，並將結果的直都給area
    cout << "面積:" << area;

    return 0;
}
