#include <iostream>

using namespace std;

int main(){
    int h,w; //declare two integers, hight and width

    cout << "請輸入長度："; //print
    cin >> h; //input value from keyboard

    cout << "請輸入寬度：";
    cin >> w;

    cout << "周長=" << 2*(h+w) << endl; //estimate and output the value
    cout << "面積=" << h*w << endl;

    return 0;
}
