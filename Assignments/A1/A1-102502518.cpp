#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;                    //宣告兩個整數變數。
    int circumference;                        //宣告名稱為circumference的整數變數。用來存放兩個整數相加後乘以二的值。
    int area;                                 //宣告名稱為area的整數變數。用來存放兩個整數相乘後的值。

    cout << "請輸入長度：";                   //將字串"請輸入長度："輸出到螢幕上。
    cin >> integer1;                          //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入寬度：";                   //將字串"請輸入寬度："輸出到螢幕上。
    cin >> integer2;                          //從鍵盤輸入，並將輸入的值給integer2。

    circumference = (integer1 + integer2)*2;  //將兩個整數相加並乘以二後的值丟給circumference。
    cout << "周長=" << circumference << endl;

    area = integer1 * integer2;               //將兩個整數相乘後的值丟給area。
    cout << "面積=" << area ;

    return 0;
}
