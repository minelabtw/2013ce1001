#include <iostream>
using std::cin;
using std::cout;


int main()
{
    int lenght;//宣告長度
    int width;//宣告寬度
    int perimeter;//宣告周長
    int dimension;//宣告面積
    cout<<"請輸入長度\n";//將字串"請輸入長度"輸入到螢幕上
    cin>>lenght;//從鍵盤輸入並將輸入的值給lenght
    cout<<"請輸入寬度\n";//將字串"請輸入寬度"輸出到螢幕上
    cin>>width;//從鍵盤輸入並將輸入的值給width
    perimeter=lenght*2+width*2;//將輸入的lenght以及width乘以2並把相加後的值丟給perimeter
    dimension=lenght*width;//將輸入的lenght及width相乘並丟給dimension
    cout<<"周長="<<perimeter<<"\n";//輸出周長=perimeter的值
    cout<<"面積="<<dimension<<"\n";//輸出面積=dimension的值
    return 0;
}
