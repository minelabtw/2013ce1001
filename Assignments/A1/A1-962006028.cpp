#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int x;    //設定4個變數
    int y;
    int p=0;
    int a=0;

    cout << "請輸入長度：";  //顯示所需資訊
    cin >> x;          //抓取變數

    cout << "請輸入寬度：";
    cin >> y;

    p=2*(x+y); //後面運算的丟給前面
      cout << "周長=" << p << endl;

    a=x*y ; //後面運算的丟給前面
      cout << "面積=" << a << endl;
    return 0;
}
