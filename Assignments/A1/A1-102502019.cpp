#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1,number2;    //宣告二個整數變數。
    int sum = 0;            //宣告名稱為sum的整數變數，並初始化其數值為0。
    int product;            //宣告名稱為product的整數變數。用來存放number1和number2相乘後的值。

    cout << "請輸入長度:";  //將字串"請輸入長度："輸出到螢幕上。
    cin  >> number1;        //從鍵盤輸入，並將輸入的值給number1。

    cout << "請輸入寬度:";
    cin  >> number2;

    sum = number1*2+number2*2; //將兩個整數相加並乘2，並將相加後的值丟給sum。

    cout << "周長 ="  <<  sum <<  endl;

    product = number1*number2; //將第一個整數與第二個整數相乘，並將相乘後的值丟給product。
    cout << "面積 ="  << product;

    return 0;


}
