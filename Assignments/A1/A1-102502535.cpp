#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length ;  //宣告第一個整數變數。
    int width ;  //宣告第二個整數變數。
    int sum ;  //  宣告名稱為sum的整數變數。
    int rectangle ;  //宣告一個名為rectangle的變數。

    cout << "請輸入長度：" ;  //將字串輸出至螢幕上。
    cin >> length ;  //將操作者輸入的值給length。

    cout << "請輸入寬度：" ;  //將字串輸出至螢幕上。
    cin >> width ;  //將操作者輸入的值給width。

    sum = length*2 + width*2 ;  //將長寬之值相加乘二，並丟給sum（周長）。
    rectangle = length * width ;  //將長寬相乘，並將得到的值給rectangle（面積）。

    cout << "周長=" << sum << endl ;
    cout << "面積=" << rectangle << endl ;

    return 0 ;
}
