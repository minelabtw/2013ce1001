/*****************************************
* Assignment 1
* Name: 賴建勳
* School Number: 102502558
* E-mail: 102502558@cc.ncu.edu.tw
* Course: 2013-CE1001
* Submitted: 2013/09/28
*
***********************************************/
#include <iostream>

using namespace std;

int main()
{
    int a,b;
    cout << "請輸入長度："; cin >> a;
    cout << "請輸入寬度："; cin >> b;
    cout << "周長=" << 2 * (a+b) << endl;
    cout << "面積=" << a*b <<endl;
    return 0;
}
