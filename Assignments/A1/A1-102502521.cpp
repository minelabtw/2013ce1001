#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length;                             //宣告名稱為length的整數變數
    int width;                              //宣告名稱為width的整數變數
    int perimeter;                          //宣告名稱為sum的整數變數，用來存放長方形的周長
    int area;                               //宣告名稱為area的整數變數。用來存放長方形的面積。

    cout << "請輸入長度：";                 //將字串"請輸入長度："輸出到螢幕上
    cin >> length;                          //從鍵盤輸入，並將輸入的值給length

    cout << "請輸入寬度：";                 //將字串"請輸入寬度："輸出到螢幕上
    cin >> width;                           //從鍵盤輸入，並將輸入的值給width

    perimeter = (length + width)*2 ;                           //將長度和寬度相加後乘以二，並將相加後的值丟給perimeter
    cout << "周長：" << perimeter << endl;                     //將perimeter數值印在「周長:」後方

    area = length * width;                                     //將長度與寬度相乘，將其值丟給area
    cout << "面積：" << area;                                  //將area數值印在「周長:」後方

    return 0;
}
