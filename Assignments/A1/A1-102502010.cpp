#include <iostream>
using namespace std;

int main()
{
    int length; //長度
    int wide; //寬度
    cout << "請輸入長度：";
    cin >> length; //輸入長度
    cout << "請輸入寬度：";
    cin >> wide;  //輸入寬度
    cout << "周長=" << 2 * ( length + wide ) << endl; //輸出周長
    cout << "面積=" << length * wide << endl;  //輸出面積
    return 0;
}
