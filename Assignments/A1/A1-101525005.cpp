#include <iostream>

using namespace std;

int main()
{
     cout << "提示:長寬可以不用為'整數'"<< endl;
    //宣告四個整數變數:長、寬、周長、面積
    double length,width,circumference,area;

    //讀取長
    cout << "請輸入長方形的長："<< endl;
    cin >> length;
    //讀取寬
    cout << "請輸入長方形的寬："<< endl;
    cin >> width;

    //計算出周長並印出
    circumference = 2*(length + width);
    cout << "長方形的周長=" << circumference << endl;
    //計算出面積並印出
    area = length * width;
    cout << "長方形的面積=" << area ;

    return 0;
}
