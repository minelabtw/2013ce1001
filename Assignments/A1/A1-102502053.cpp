#include <iostream>

int main()
{
    int integer1; //Variable for the length
    int integer2; //Variable for the width
    int sum = 0; //Perimeter, reset to 0
    int area = 0; //Area, reset to 0

    std::cout << "請輸入長度： "; //words display
    std::cin >> integer1; //input length

    std::cout << "請輸入寬度： "; //words display
    std::cin >> integer2; //input width

    sum = (integer1+integer2)*2; //calculate for the perimeter
    std::cout << "周長=" << sum << std::endl;

    area = integer1*integer2; //calculate for the are
    std::cout << "面積=" << area;

    return 0;

}
