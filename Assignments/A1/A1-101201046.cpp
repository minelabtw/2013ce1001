#include <iostream>

using namespace std;

int main(void) {
    int length = 0,width = 0; //length: length of rectangle ,and initial value is 0
                              //width: width of rectangle ,and initial value is 0
    int area;                 // area: area of rectangle
    int perimeter;            //perimeter: perimeter of rectangle
    cout << "請輸入長度 :" ;

    cin >> length; //input length of rectangle

    cout << "請輸入寬度 :" ;

    cin >> width; //input width of rectangle
    perimeter = (length + width)*2; //compute perimeter of rectangle

    cout << "周長=" << perimeter << endl;

    area = length * width; //compute area of rectangle

    cout << "面積=" << area;

    return 0;
    }
