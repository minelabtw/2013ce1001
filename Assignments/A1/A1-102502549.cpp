#include <iostream>

using namespace std;

int main()
{
    int length;       //宣告長方形長度
    int width;        //宣告長方形寬度
    int perimeter;    //宣告長方形周長
    int area;         //宣告長方形面積

    cout<<"請輸入長度：";   //輸出字串:"請輸入長度："
    cin>>length;            //輸入長方形長度

    cout<<"請輸入寬度：";   //輸出字串:"請輸入寬度："
    cin>>width;             //輸入長方形寬度

    perimeter=2*(length+width);  //周長=2(長+寬)
    area=length*width;           //面積=長x寬

    cout<<"周長="<<perimeter<<"\n";    //輸出周長並換行
    cout<<"面積="<<area<<"\n";         //輸出面積(題目並無要求換行，但還是換一下好，呵呵)


    return 0;
}
