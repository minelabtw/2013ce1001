#include <iostream> // allow program to perform output and input.
#include <cstdlib> // let the program pause after computing the answer.

using std::cout; // allow us to use cout, cin, endl, in the program.
using std::cin;
using std::endl;


int main ()
{
    double number1; //length
    double number2; //width
    double number3; //perimeter
    double number4; //area

    cout << "請輸入長度:"; //prompt user to give a value for length
    cin >> number1 ;

    cout <<"請輸入寬度:"; //prompt user to give a value for width
    cin >> number2 ;

    number3 = 2*(number1 + number2); // perimeter = 2*(length + width)
    number4 = number1 * number2; // area = length * width

    cout << "周長=" << number3 << "\n";//display the answer to user.
    cout << "面積=" << number4 << endl;

    system("pause");

    return 0;



}
