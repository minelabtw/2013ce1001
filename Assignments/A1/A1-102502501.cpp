#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1;//定義一個整數變數
    int number2;//定義第二個整數變數
    int lg;//宣告lg成周長
    int area;//宣告area成面積

    cout<< "請輸入長度" ; //把"請輸入長度"至螢幕上
    cin>> number1;//把輸入的值丟至number1

    cout<<"請輸入寬度" ;
    cin>> number2;

    lg = (number1+ number2 ) *2 ;//兩數相加乘以二
    cout << "周長=" << lg << endl ;

    area = number1*number2 ; // 把兩數相成，丟至area
    cout << "面積=" << area ;

    return 0 ;
    }
