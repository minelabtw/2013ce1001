#include <iostream>
using namespace std;

int main()
{
    //varible decalaration start
    int length = 0;     //decalarate an integer varible named length and initialize to 0
    int width = 0;      //decalarate an integer varible named width and initialize to 0
    int perimeter = 0;  //decalarate an integer varible named perimeter and initialize to 0
    int area = 0;       //decalarate an integer varible named area and initialize to 0
    //varible dacalaration end

    cout << "length=";        //ask for the length
    cin >> length;            //memorize the length inputed

    cout << "width=";         //ask for the width
    cin >> width;             //memorize the width inputed

    perimeter = 2 * ( length + width ); //compute the perimeter
    area = length * width;               //compute the area

    //output results
    cout << "�P��=" << perimeter << endl;   //output the perimeter
    cout << "���n=" << area << endl;   //output the area
    return 0;
}
