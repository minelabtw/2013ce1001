#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main(){
    int integer1;   //宣告第一個變數
    int integer2;   //宣告第二個變數
    int perimeter;  //宣告perimeter為周長
    int area;       //宣告area為面積

    cout << "請輸入長度:" ;
    cin  >> integer1 ;

    cout <<"請輸入寬度:" ;
    cin  >>integer2 ;

    perimeter = 2*(integer1+integer2) ;   //變數相加再乘以二
    cout <<"周長" << perimeter << endl ;

    area = integer1*integer2 ; //兩變數相乘
    cout <<"面積" <<area ;
    return 0 ;
}
