#include<iostream> //include iostream的東西


int main() //主函數開始執行程序
{
    int number_1; //宣告整數的變數number_1
    int number_2; //宣告整數的變數number_2
    int sum=0;  //宣告名稱為sum的整數變數，並初始化其數值為0
    int squareInteger_1; //宣告名稱為squareInteger_1的整數變數,用來存放number_1*number_2的值。

    std::cout<<"請輸入長度:"; //將字串"請輸入長度"輸出到螢幕上。
    std::cin >> number_1; //從鍵盤輸入，並將輸入的值給integer_1。

    std::cout<<"請輸入寬度:"; //將字串"請輸入寬度"輸出到螢幕上。
    std::cin >> number_2; //從鍵盤輸入，並將輸入的值給number_2。

    sum=2*number_1 +2*number_2; //將兩個整數各乘以二後相加，並將之後的值儲存sum。
    std::cout<<"周長="<<sum<<std::endl; //將sum的值傳給cout,使其顯示於"周長="之後。

    squareInteger_1= number_1*number_2;//將兩個整數相乘後，並將之後的值儲存squareInteger_1。
    std::cout<<"面積="<<squareInteger_1;//將squareInteger_1的值傳給cout。,使其顯示於"面積="之後。

    return 0;

} //結束function main
