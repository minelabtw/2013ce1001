#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length;                                  //宣告一個長度整數變數
    int breadth;                                 //宣告一個寬度整數變數
    int circumference = 0;                       //宣告一個周長整數變數，並初始化其數值為0
    int area = 0;                                //宣告一個面積整數變數，並初始化其數值為0

    cout << "請輸入長度:";                       //將字串"請輸入長度:"輸出到螢幕上
    cin >> length;                               //從鍵盤輸入，並將輸入的值給length

    cout << "請輸入寬度:";                       //將字串"請輸入寬度:"輸出到螢幕上
    cin >> breadth;                              //從鍵盤輸入，並將輸入的值給breadth

    circumference = ( length + breadth ) * 2;    //將長度與寬度相加後乘以二，並將計算出的值給circumference

    cout << "周長=" << circumference << endl;    //將字串"周長="與circumference的值輸出到螢幕上

    area = length * breadth;                     //將長度與寬度相乘，並將計算出的值給area

    cout << "面積=" << area << endl;             //將字串"面積="與area的值輸出到螢幕上

    return 0;

}
