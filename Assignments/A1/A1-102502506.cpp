#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2,integer3;
    int sum = 0;
    int squareinteger1;

    cout << "請輸入第一個整數:";
    cin >> integer1;

    cout << "請輸入第二個整數:";
    cin >> integer2;

    cout << "請輸入第三個整數:";
    cin >> integer3;

    sum = integer1 + integer2 + integer3;
    cout << "三個整數相加後的結果:" << sum << endl;

    squareinteger1 = integer1 * integer1;
    cout << "第一個整數平方後結果為:" << squareinteger1;

    return 0;
}
