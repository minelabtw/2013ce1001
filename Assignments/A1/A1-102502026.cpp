//A1-10205026
// 長方形周長與面積

#include <iostream>
using std::cout;
using std::cin;

int main()//Start program
 {

int base; // definning integer base of the rectangle
int high; // defining integer high of the rectangule
int sum = 0;
int area = 0;


cout << "請輸入長方形寬度:\a"; // The program says
cin >> base; // Put a integer for base

cout << "請輸入長方形的長度\a:"; //The program says
cin >> high; //Put a integer for high

sum= (2*base)+(2*high); //the sum for the perimeter of rectangle
cout <<"長方形的周長為" <<sum<<"\n"<<"\a"; //The result of the perimeter

area= base*high; // Solving the area of the rectangle
cout <<"長方形的面積為:" <<area<<"\n\a"; // The result of the area

return 0;

}//end of the program


