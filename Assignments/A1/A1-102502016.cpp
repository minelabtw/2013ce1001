#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length = 0,width = 0;//宣告長度,寬度,周長,面積
    int perimeter = 0;
    int area = 0;


    cout << "請輸入長度：";//輸入長度並列出結果
    cin >> length;

    cout << "請輸入寬度：";//輸入寬度並列出結果
    cin >> width;

    perimeter = ( length + width ) * 2;//輸入周長並列出結果
    cout << "周長=" << perimeter << endl;

    area = length * width;//輸入面積並列出結果
    cout << "面積=" << area;

    return 0;
    }
