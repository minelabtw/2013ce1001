/*************************************************************************
    > File Name: A1-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年10月03日 (週四) 00時14分02秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

int main()
{
	/*********************/
	/* int l means lenth */
	/* int w means width */
	/*********************/
	int l, w;

	printf("請輸入長度："); // input lenth
	scanf("%d", &l);

	printf("請輸入寬度："); // input width
	scanf("%d", &w);

	printf("周長=%d\n", l+l+w+w); // output perimeter 
	printf("面積=%d\n", l*w);     // output area
	
	return 0;
}

