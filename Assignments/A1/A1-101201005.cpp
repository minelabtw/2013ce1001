#include <iostream>
using std::cout;
using std::cin;


int main()
{
    int integer1,integer2;
    int sum = 0;
    int area;

    cout << "請輸入長度：";
    cin >> integer1;

    cout << "請輸入寬度：";
    cin >> integer2;

    sum = integer1 * 2 + integer2 * 2;
    cout << "周長：" << sum <<"\n";

    area = integer1 * integer2;

    cout << "面積：" << area <<"\n";

    return 0;


}
