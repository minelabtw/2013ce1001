#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length = 0,width = 0;   //宣告整數(int)的長度及寬度，並使其原值為0。
    int perimeter = 0;          //宣告整數(int)的周長，並使其原值為0。
    int area = 0;               //宣告整數(int)的面積，並使其原值為0。


    cout << "請輸入長度：";
    cin >> length;

    cout << "請輸入寬度：";
    cin >> width;

    perimeter = ( length + width ) * 2;//計算周長
    cout << "周長=" << perimeter << endl;

    area =  length * width;//計算面積
    cout << "面積=" << area;

    return 0;
}
