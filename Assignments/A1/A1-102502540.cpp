#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int length;    //宣告長度
    int width;     //宣告寬度
    int perimeter;            //宣告周長
    int area;                 //宣告面積

    cout << "請輸入長度：";        //將字串"請輸入長度："輸出到螢幕上
    cin >> length;                 //從鍵盤輸入，並將輸入的值給length

    cout << "請輸入寬度：";        //將字串"請輸入寬度："輸出到螢幕上
    cin >> width;                  //從鍵盤輸入，並將輸入的值給width

    perimeter = (length + width) *2;   //將長度與寬度相加後乘以二，並將相加後的值丟給perimeter
    cout << "周長=：" << perimeter << endl;


    area = length * width;   //將長度與寬度相乘，並將相乘後的值丟給area
    cout << "面積=：" << area ;

    return 0;
}
