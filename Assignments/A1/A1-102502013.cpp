#include <iostream>
using std::cout;
using std::cin;
using std::endl;


int main()
{
    int length; //代表長度的變數
    int wide; //代表寬度的變數
    int sum = 0; //表示sum是一個整數變數，並初始其數值為0
    int multiply; //表示multiply是一個整數變數，用來存放lenrth*wide

    cout << "請輸入長度："; //將字串"請輸入長度："輸出到螢幕上。
    cin >> length; //從鍵盤輸入，並將值放至length
    cout << "請輸入寬度："; //將字串"請輸入寬度："輸出到螢幕上。
    cin >> wide; //從鍵盤輸入，並將值放至wide

    sum = 2*(length + wide); //將length和wide相加並乘2，並將完成後的值丟給sum
    cout << "周長=" << sum << endl; //將字串"周長="輸出到螢幕上
    multiply = length * wide; //將length和wide相乘，並將完成後的值丟給multiply
    cout << "面積=" << multiply ; //將字串"面積="輸出到螢幕上

    return 0;
}






