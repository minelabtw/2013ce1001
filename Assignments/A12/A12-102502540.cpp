#include<iostream>
using namespace std;

int c(char[],int); //宣告函式

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    for (int a=0; str[a]!='\0'; a++) //進入迴圈
    {
        cout<<str[a]; //輸出單字
        if(str[a+1]==' ' or str[a+1]=='\0')
            cout<<": "<<c(str,a)<<endl; //單字後顯示冒號及次數
        while(str[a+1]==' ') //跳過空白字元
            a++;
    }
    return 0;
}

int c(char s[],int e) //函式
{
    int b=e;
    int t=0; //字元數
    while(b>=0 and s[b]!=' ') //運算單字的字元數
    {
        t++;
        b--;
    }
    int d=1;
    int f=0; //相同字元數
    int g=1; //次數
    for (int a=e; s[a+1]!='\0'; a++) //進入迴圈
    {
        while(s[a+1]==s[e-t+d] and d<=t+1 ) //依序判斷字元是否和單字相同
        {
            f++;
            d++;
            a++;
        }
        d=1; //初始d值
        if(f==t+1) //判斷是否為重複的單字
        {
            g++; //次數加一
            for (int q=1; q<=t; q++) //將重複的單字從陣列中清除
                s[a-q]=' ';
        }
        f=0; //初始f值
    }
    return g; //回傳次數
}
