#include<iostream>
#include<cstring>
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *temp[81]= {};
    char *word;
    string record[42];    //宣告字串陣列用來記錄出現過的單字
    int counter[42]= {0};   //宣告陣列用來計算出現次數
    int n=0;    //宣告變數用來記錄陣列大小

    word = strtok(str," ");  //分割字串
    while(word!=NULL)
    {
        for(int i=0; i<81; i++)
        {
            temp[i] = word;  //將分割好的字串存入陣列
            word = strtok(NULL," ");
        }
    }

    for(int i=0; i<81; i++)
    {
        int check=0;    //宣告變數來判斷是否為出現過的單字 (0為未出現,1為出現過)

        for(int j=0; j<=n; j++)
        {
            if(temp[i]==record[j])  //若為出現過的單字
            {
                counter[j]++;  //紀錄+1
                check=1;
            }
        }
        if(check==0)
        {
            record[n]=temp[i];  //若為未出現過的單字則存入陣列
            counter[n]++;  //紀錄+1
            n++;
        }
    }

    for(int j=0; j<n-1; j++)  //輸出紀錄
        cout << record[j] << ": " << counter[j] << endl;

    cout << record[n-1] << ": " << counter[n-1];
    return 0;
}
