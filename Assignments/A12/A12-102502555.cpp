#include <iostream>
#include <cstring>
using namespace std;

int main(){
	char str[] = "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
	string str1[100];  //裝切割好的單字
	string output[100];  //整理過的單字
	int outputcount[100] = {};  //單字計數
	int outputsize = 0;  //不重複的單字有幾個
	int str1size = 0;  //切割好的單字有幾個
    char *ptr;  //暫存用指標

    ptr = strtok(str , " ");  //分割字串
	while(ptr != NULL){  //當指標不等於結束符號存到陣列裡
        str1[str1size] = ptr;
        str1size++;
        ptr = strtok(NULL , " ");
	}

	output[0] = str1[0];  //整理單字並檢查單字出現幾次
	for(int i = 1 ; i < str1size ; i++){  //檢查到最後一個單字
        int same = 0;  //一開始設單字都不一樣
        for(int j = outputsize ; j >= 0 ; j--){  //和要輸出的陣列比較有沒有一樣的
            if((output[j].compare(str1[i])) == 0){  //如果一樣就把該單字計數器加一
                outputcount[j]++;
                same = 1;
            }
        }
        if(same == 0){  //不一樣就存到要輸出的陣列裡
            outputsize++;
            output[outputsize] = str1[i];
        }
	}

    for(int i = 0 ; i <= outputsize ; i++){  //輸出整理好的陣列與次數
        cout << output[i] << ": " << outputcount[i] + 1 << endl;
    }

	return 0;
}
