#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
	// declare variable
	char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
	istringstream input(str);
	string temp;
	vector <string> output;
	vector <int> count;

	while(input >> temp) // read one word from str to temp
	{
		static vector <string> :: iterator it; // declare variable in while

		it = find(output.begin() , output.end() , temp); // check repeat word
		if(it == output.end()) // handle not repeat
		{
			output.push_back(temp);
			count.push_back(1);
		}
		else // handle repeat
			++count[distance(output.begin() , it)];
	}

	for(int i = 0 ; i < output.size() ; ++i)
		cout << output[i] << ": " << count[i] << endl;

	return 0;
}
