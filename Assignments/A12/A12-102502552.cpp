#include<iostream>
#include<string>
#include<vector>
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight ";
    vector<string>rstr(0);//建一個分割容器
    vector<string>singlestr(1);//建一個單個詞語容器
    vector<int>counter(1);//建一個記錄詞語次數的容器
    string cutter;//宣告分割的輔助變數

    for(int alpha = 0; alpha < sizeof(str) - 1; alpha++)//分割的迴圈
    {
        if(str[alpha] == ' ')//讀到空格進行分割動作
        {
            rstr.push_back(cutter);//將助手的值輸入分割容器
            cutter = "";//助手歸零
        }
        else
        {
            cutter += str[alpha];//每讀一個字節就會添加到分割助手
        }
    }

    for(int a = 0; a < rstr.size(); a++) //記錄詞語與次數的迴圈
    {
        for(int b = 0; b < singlestr.size() ; b++) //用分割容器來比單字容器
        {
            if(b < singlestr.size() - 1)//還沒比到最後一個
            {
                if(singlestr[b] == rstr[a])//出現相同詞
                {
                    counter[b] += 1;//計數容器會加一
                    break;
                }
            }
            else
            {
                if(singlestr[b] == rstr[a])//已經比到最後一個
                {
                    counter[b] += 1;
                }
                else//仍未有相同詞，證明是新詞，記錄到單字容器並未計數器添加一項
                {
                    singlestr.push_back(rstr[a]);
                    counter.push_back(0);
                }
            }
        }
    }

    for(int c = 1; c < singlestr.size(); c++)//輸出的迴圈
    {
        cout << singlestr[c] << ":" << counter[c] << endl;
    }

    return 0;//回傳0
}
