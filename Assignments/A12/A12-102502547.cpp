#include <iostream>
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    int a=0; //變數a計算單字長度

    for(int i=0; str[i]!='\0'; i++)
    {
        if(str[i]!=' ')
        {
            cout << str[i]; //輸出單字
            a++; //str[i]不是空格時 輸出str[i]並計算單字長度
        }

        if((str[i+1]==' ' || str[i+1]=='\0') && str[i]!=' ') //符合條件時時 開始判斷後面有沒有重複單字出現
        {
            int t=1; //變數t計算單字出現次數

            for(int j=i+1; str[j]!='\0'; j++)
            {
                if(str[j]==str[i-a+1] && str[j-1]==' ')
                {
                    int x=1; //變數x判斷是否重複 重複為1 不重複為0

                    for(int u=1; u<a; u++)
                    {
                        if(str[j+u]!=str[i-a+1+u]) //如果其中1字元不同 x為0
                            x=0;
                    }

                    if(str[j+a] != ' ' && str[j+a]!='\0') //如果單字後並非空格或字串結束字元 x為0
                        x=0;

                    if(x==1) //如果重複 次數+1 重複部分變為空格
                    {
                        for(int u=0; u<=a; u++)
                        {
                            str[j+u]=' ';
                        }
                        t++;
                    }
                }
            }

            cout << ": " << t << endl; //輸出出現次數
            a=0; //歸零
        }
    }
    return 0;
}
