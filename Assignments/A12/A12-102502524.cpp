#include <iostream>
#include <string.h>
using namespace std;

int main()
{
    char *b = " ";                              //設定用strtok分割字串str的指標b、w
    char *w;
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight ";
    char *a[81];                                //設定陣列a，用來儲存分割完成後的str
    int s = 0;                                  //設定變數s，計算切割出的字串數量
    int t = 0;                                  //設定變數t，計算字串種類的數量
    int c[81];                                  //設定陣列c，用來儲存同種類字串的數目
    string d[81];                               //設定陣列d，用來儲存字串種類的(單字庫)

    w = strtok(str,b);                          //16~22行，將str以空白為分界線分割儲存至陣列a
    while(w!=NULL)
    {
        a[s]=w;
        s++;
        w=strtok(NULL,b);
    }

    for(int j=0; j<s; j++)                      //24~42行，利用for迴圈比對待計算字串及單字庫
    {
        int k = 0;                              //利用變數k判斷是否在單字庫內找尋到單字，有則回傳1
        for(int i=0; i<t; i++)
        {
            if (a[j]==d[i])                     //把單字庫的字串從第一個比對到最後一個，若和待分類字串相符，則該類計數加一
            {
                c[i]++;
                k=1;
                break;
            }
        }
        if (k==0)                               //若單字庫內無符合(接收到k回傳0)，則將此字串新增至單字庫內並計次數一
        {
            c[t]=1;
            d[t]=a[j];
            t++;
        }
    }

    for(int l=0; l<t; l++)                      //44~47行，輸出統計資料
    {
        cout << d[l] << ": " << c[l] << endl;
    }

    return 0;
}
