#include <iostream>

using namespace std;

int main()
{
    int num[42]= {};                                                 //宣告計算次數的陣列
    char *vocabulary[42]= {};                                        //宣告有42個單字的陣列
    char *str[81]=                                                   //宣告有81個歌詞的陣列
    {
        "jingle", "bells",
        "jingle", "bells",
        "jingle", "all", "the", "way",
        "Oh", "what", "fun","it", "is", "to", "ride",
        "In", "a", "one", "horse", "open", "sleigh",
        "jingle", "bells",
        "jingle", "bells",
        "jingle", "all", "the", "way",
        "Oh", "what", "fun", "it", "is", "to", "ride",
        "In", "a", "one", "horse", "open", "sleigh",
        "Dashing", "through", "the", "snow",
        "In", "a", "one", "horse", "open", "sleigh",
        "Oer", "the", "fields", "we", "go",
        "Laughing", "all", "the", "way",
        "Bells", "on", "bob", "tails", "ring",
        "Making", "spirits", "bright",
        "What", "fun", "it", "is", "to", "laugh", "and", "sing",
        "A", "sleighing", "song", "tonight"
    };
    for(int i=0,j=0; i<81; i++)                                       //把歌詞依序拿出來比較
    {
        for(int k=0; k<=j; k++)
        {
            if(vocabulary[k]==str[i])                                 //計算歌詞出現的次數
            {
                num[k]++;
                break;
            }
            else if(k==j)                                             //把第一次出現的歌詞存在單字裡
            {
                vocabulary[k]=str[i];
                num[k]++;
                j++;
                break;
            }
        }
    }
    for(int i=0; i<42;i++)                                            //輸出每個單字出現的次數
    {
        cout << vocabulary[i] << ": " << num[i] << endl;
    }
    return 0;
}
