#include <iostream>
#include <fstream>
using namespace std;

template <typename T> int isin(T str, T strarr[],int endidx);

int main()
{
	char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
	int strsize=sizeof(str)/sizeof(char)-1;

	ofstream OUT("output.txt");					//read from file
	for(int i=0;i<strsize;i++){OUT << str[i];}	//output to file
	OUT.close();								//close file

	string word;				//temp

	ifstream IN("output.txt");	//load from file
	int wcnt=0;					//words counter
	while(IN >> word){wcnt++;}	//count
	IN.clear();					//clear EOF state

	string strarr[wcnt];		//make array for words
	int strcnt[wcnt];			//make array for words' numbers
	int lastidx=-1;				//now last index

	IN.seekg(0);				//re-read from beginning
	while(IN >> word){
		int idx=-1;
		if((idx=isin(word,strarr,lastidx))!=-1){strcnt[idx]++;}		//if in table: add 1 to that counter
		else{strarr[++lastidx]=word;strcnt[lastidx]=1;}				//if not: add new element and set that counter to 1
	}

	IN.close();					//close file

	for(int i=0;i<=lastidx;i++){
		cout << strarr[i] << ": " << strcnt[i] << endl;				//print the result
	}

	return 0;
}

template <typename T>
int isin(T str, T strarr[],int endidx)	// function to check whether an element is in a assigned array and return the position
{
	for(int i=0;i<=endidx;i++){
		if(str==strarr[i]){return i;}
	}
	return -1;
}
