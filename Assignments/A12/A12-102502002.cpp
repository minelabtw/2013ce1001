#include <iostream>
#include <string.h>
#include<stdio.h>
using namespace std;

string ans[42];    //存放最後所輸出字串的陣列
string a[81];      //存放個別的字
int check(string cWord);

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *sin;     //指向個別單字的指標
    int count[42]; //計算次數的陣列

    for(int i=0; i<42; i++) //初始化
        count[i]=0;

    int k=0;
    sin = strtok(str," ");     //將歌詞依字切割成個別的字串，遇空格即切割
    while(sin!=NULL)
    {
        a[k]=sin;
        k++;
        sin = strtok(NULL," ");
    }

    int n=0;
    for(int i=0; i<81; i++)
    {
        if(check(a[i])==0)  //判別為重複出現的字
        {
            ans[n]=a[i];    //若為新的數字，將它存進ans[]
            n++;
        }
    }

    for(int i=0; i<42; i++)
    {
        for(int j=0; j<81; j++)
        {
            if(ans[i]==a[j])  //比較a[]和ans[]，若出現相同的字將計數器加一
                count[i]++;
        }
    }

    for(int i=0; i<42; i++)
        cout << ans[i] << ": " << count[i] << endl;  //輸出結果

    return 0;
}

int check(string cWord)
{
    string tWord;
    for(int i=0; i<42; i++)  //比較ans[]和a[]
    {
        tWord=ans[i];
        if(cWord==tWord)  //若出現相同字，回傳1
            return 1;
    }
    return 0;             //若未出現相同字，回傳0
}
