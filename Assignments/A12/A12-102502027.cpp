#include <iostream>
#include<cstring>
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    int count=0;
    size_t destination_size = sizeof (str); //讀取整個str字串的大小
    char *s = new char[destination_size]; //宣告
    strncpy(s, str, destination_size); //複製
    char *delim = " ";
    char  *pch;
    pch = strtok(str,delim); //用空白切割
    while (pch != NULL){
        pch = strtok (NULL, delim); //切割
        count++;
    }
    string *token = new string[count]; //動態宣告
    int *number = new int[count];//動態宣告
    for(int k=0; k<count; k++)number[k]=1; //設初始值為1
    pch = strtok(s,delim); //用空白切割
    int a=0;
    for(int i=0; i<count; i++){
        int thesame=1;
        for(int j=0; j<a; j++) //和前面的字元比較是否一樣
        {
            if(token[j]==pch)
            {
                number[j]++;
                thesame=0; //一樣時
                break;
            }
        }
        if(thesame==1) //不一樣時
        {
            token[a]=pch;
            a++;
        }
        pch = strtok (NULL, delim); //切割
    }
   for(int i=0;i<a;i++) //輸出全部
       cout<<token[i]<<": "<<number[i]<<endl;
    delete [] token; //釋放記憶體空間
    delete [] number;
    return 0;
}
