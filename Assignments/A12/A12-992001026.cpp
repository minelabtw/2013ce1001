#include <iostream>
#include <sstream>

#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";

    // 轉成 sting
    string article = str;
    // 轉成 istringstream istr
    istringstream istr(str);
    // 轉成 istream_iterator
    istream_iterator<string> istri( istr );
    //原始字串用空白分割而成的 vecotr
    vector<string> arr;
    //央 istingstream 輸入到 arr
    copy( istri, istream_iterator<string>(), back_insert_iterator< vector<string> >(arr) );

    //所有字串，但只出現一次
    vector<string> strs;
    //所有字串出現次數，下標對應到 strs 的下標
    vector<int> counters;
    //巡迴 arr 所有元素
    for( int i=0 ; i<arr.size() ; ++i )
    {
        //找位置
        vector<string>::iterator position = find( strs.begin(), strs.end(), arr[i] );

        if( position != strs.end() ) //如果找到了
        {
            //把同位置的 counter 加一
            ++counters[ distance(strs.begin(), position) ];
        }
        else
        {
            //新增這個詞
            strs.push_back( arr[i] );
            //出現次數為 1
            counters.push_back( 1 );
        }
    }

    // 印出來
    for( int i=0 ; i<strs.size() ; ++i )
    {
        // BJ4
        cout << strs[i] << ": " << counters[i] << endl;
    }

    return 0;
}
