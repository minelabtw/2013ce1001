#include<iostream>
#include<cstdlib>
using namespace std;
typedef struct word
{
    char name;
    bool wordend;//判斷這一個字元是否為一個單字的最後一個字
    int times;//計算單字出現次數
    struct word *next[52];//空間暴力 0~25 : a~z   26~51 : A~Z   連結到下一個字元
    struct word *previous;//連結到前一個字元
};
typedef struct wordpointer
{
    struct word *endofword;//儲存單字最後一個字元的位置
    struct wordpointer *next;//連結到下一個單字
};
void newlink(struct word *);//初始化 struct word
void output(struct word*,int );//輸出單字
int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    int index;
    struct wordpointer *pstar,*pcurrent;
    struct word *star,*current;

    pstar=(struct wordpointer *)malloc(sizeof(struct wordpointer));
    pcurrent=pstar;
    star=(struct word *)malloc(sizeof(struct word));
    newlink(star);
    current=star;
    pcurrent->next=NULL;
    //初始化

    for(int i=0; str[i]; i++)
        if((str[i]>='a'&&str[i]<='z') || (str[i]>='A'&&str[i]<='Z'))
        {
            index=(str[i]>='a' && str[i]<='z')?str[i]-'a':str[i]-'A'+26;  // 將 a~z 轉為 0~25   A~Z 轉為 26~51
            if(current->next[index]==NULL)
            {
                current->next[index]=(struct word *)malloc(sizeof(struct word));
                newlink(current->next[index]);
                current->next[index]->previous=current;
                current->next[index]->name=str[i];

            }
            current=current->next[index];
            if( str[i+1]==' ' || str[i+1]=='\0')//判斷單字結尾
            {
                if(current->wordend==false)//如果這個單字沒出現過
                {
                    current->wordend=true;
                    pcurrent->endofword=current;
                    pcurrent->next=(struct wordpointer *)malloc(sizeof(struct wordpointer));
                    pcurrent=pcurrent->next;
                    pcurrent->next=NULL;
                }
                current->times++;//出現次數++
                current=star;//返回起點
            }
        }
    for(pcurrent=pstar ;pcurrent->next!=NULL;pcurrent=pcurrent->next)
        output(pcurrent->endofword,0);

    return 0;
}
void newlink(struct word *current)
{
    current->wordend=false;
    current->times=0;
    current->previous=NULL;
    for(int i=0; i<52; i++)
        current->next[i]=NULL;
}
void output(struct word *current,int times)
{
    if(current->previous->previous!=NULL)//返回單字的第一個字元
        output(current->previous,times+1);
    cout << current->name;
    if(times==0)
    {
        cout << ": "<<current->times << endl;
    }
}

