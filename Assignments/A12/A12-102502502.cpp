#include<iostream>
#include<string.h>

using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *delim = " ";                          //用strtok分割字串str的指標delim、pch
    char *pch;
    char *a[81];                                //儲存分割後的str
    int n = 0;                                  //切出的字串數量
    int i = 0;                                  //字串多少種類
    int c[81];                                  //儲存同類字串數目
    string v[81];                               //儲存字串種類

    pch = strtok(str,delim);
    while (pch != NULL)
    {
        a[n]=pch;
        n++;
        pch = strtok(NULL,delim);
    }
    for(int x=0; x<n; x++)                      //待算字串及單字庫
    {
        int k = 0;
        for(int y=0; y<i; y++)
        {
            if (a[x]==v[y])                     //把v內的字串從頭比對到尾，若和待分類字串一樣，則該類計數加一
            {
                c[y]++;
                k=1;
                break;
            }
        }
        if (k==0)                               //若v內無相同者，則將此字串新增至v內且計一次數
        {
            c[i]=1;
            v[i]=a[x];
            i++;
        }
    }
    for(int h=0; h<i; h++)                      //輸出結果
    {
        cout << v[h] << ": " << c[h] << endl;
    }
    return 0;
}
