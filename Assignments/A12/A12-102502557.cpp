#include <iostream>
#include <vector>
#include <cstring>
using namespace std;
int main()
{
   char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    
    const char *sp=" ";
    char *w;//存分割下來的每一個字串
    char *data[100];//每一個位置存分割下來的每一個字串
    int counter=0;//當作計數器 後面會放進data[counter]
    w=strtok(str,sp);
    while (w!= NULL)//沒辦法再切的時候 會顯示NULL
    {
        data[counter]=w;
        counter++;
        w=strtok(NULL,sp);//切完每一個字串的時候 strtok會把切完的變成NULL 再繼續找到下一個sp(即空白），這中間的字元會再被切出來
    }
    
    vector <string> output(1);//設一個動態陣列output儲存
    vector <int> count(1);//設一個動態陣列儲存出現次數
    
    for (int l=0; l<counter; l++)//L如果小於counter(即data裡面的counter)，讓他一個一個往後推進
    {
        for (int k=0; k<output.size() ; k++)//k如果小於output的size(這個size是總共有幾種不同的字)
        {
            if (k!=output.size()-1 )//output.size()-1 是最後一個位址的意思                     //所以data[2]那邊的jingles會走到這兒
            {
                if (data[l]==output[k])//如果找到一樣的(ex. data[0]和output[2]內容均為jingle)
                {
                    count[k]+= 1;//出現次數就會加一
                    break;//要break讓這裡結束
                }
            }
            else//else就是output最後一個的情形(所以不會是相同的字串)
            {
                 if (data[l]==output[k])
                 {
                     count[k]+=1;
                 }
                 else
                 {
                    output.push_back(data[l]);//debug一整個下午的點＝＝(原本是k)//k的話會發生印出jingles:0的慘況
                    count.push_back(0);//第一次走的時候 會把output從0變1（零的轉移）
                 }
            }
           
       }
    }
    for (int x=1; x<output.size(); x++)
    {
        cout<<output[x]<<": "<<count[x]<<endl;
    }
}
/*
vector <string> name(1);//vector：向量  <變數形態> 名稱（幾個位置）
cout<<name.size();
name.push_back("abc");//name陣列有n個位置（0~n-1），就會在n-1的下一個（即第n個位置）
cout<<name[0];
*/
