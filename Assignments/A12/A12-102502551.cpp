#include <iostream>

using namespace std;

double newFibonacci(double number);

int main()
{
    for(int i=1;i<=9;i++)                                                               //輸出
    {
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl;
    }

    for(int j=10;j<=40;j=j+10)
    {
        cout<<"newfibonacci("<<j<<") = "<<newFibonacci(j)<<endl;
    }

    return 0;
}

double newFibonacci(double number)
{
    if(number==1)                                                                      //函式
    {
        return 1;
    }
    if(number==2)
    {
        return 2;
    }
    if(number==3)
    {
        return 3;
    }
    else
    {
       return  newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
    }
}
