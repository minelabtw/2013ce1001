#include<iostream>
#include<cstring>
using namespace std;

void passstr(char str1[],char str2[],int &x,int &i) //紀錄原文的內容及讀入點(?)位置、將單字抓入temp中
{
    int j=0;
    while(str1[i]!=' ' && str1[i]!='\0')
    {
        str2[j]=str1[i];
        i++;
        j++;
    }
    if(str1[i]=='\0')x=1;
    i++;
}

void pass(char str1[],char str2[])      //將str1的內容讀入str2中
{
    for(int i=0; (str1[i]!='\0'); i++)str2[i]=str1[i];
}

int main(void)
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char output[100][10]= {};           //最多的單字字母數為9 -> 9+1=10；使用單字數上限為100
    int record[100]= {};
    int i=0;
    int where=0,finsh=0;

    while(finsh!=1)
    {
        char temp[10]= {};              //暫存
        if(str[where]==' ')where++;     //陰險的「he way  Bell」
        else
        {
            passstr(str,temp,finsh,where);
            for(int j=0,k=i; j<=k; j++)
            {
                if(!strcmp(temp,output[j])) //若temp與儲存的字串(output[j]相等，record[j]+1
                {
                    record[j]++;
                    break;
                }
                else if(j==i)               //for迴圈跑到最後一項 => temp為新的單字
                {
                    pass(temp,output[i]);
                    record[i]++;
                    i++;                    //單字數+1
                }
            }
        }
    }
    for(int j=0; j<i; j++)cout <<output[j] << ": " << record[j] << endl;
    return 0;
}
