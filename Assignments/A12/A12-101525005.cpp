#include <iostream>
#include <string.h>
using namespace std;

int main()
{
    int i=0,data_height=2,data_width=30;
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
//創造兩個一維陣列  一個存字 一個存次數
    char* words[41];
    int counters[41];
    for(int i=0; i<41; i++)
    {
        words[i]=NULL;
        counters[i]=0;
    }

    int b;
    char* word;

    //切割字串
    word=strtok(str," ");

    while(word)
    {
        //cout << word << endl;

        for(int i=0; i<41; i++)
        {
            //依照判斷放入字串並增加次數
            if(words[i]==NULL)
            {
                words[i]=word;
                counters[i]++;
                break;
            }

            else if(strcmp(words[i], word)==0)
            {
                counters[i]++;
                break;
            }
        }

        word=strtok(NULL," ");
    }
//印出
    for(int i=0; i<41; i++)
    {
        cout << words[i] <<": "<< counters[i]<< endl;
    }
    return 0;
}


