#include <iostream>
#include <string.h>
#include <vector>
using namespace std;
int main()
{
    vector <string> c (100); // 暫存切割字串的動態陣列
    vector <string> d (1);
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *a = " "; //切割終點
    char *b; //切割出來的字串
    int e = 0;
    int f[100]= {}; //此陣列紀錄出現的次數

    b = strtok(str,a); //strtok切割str 從一開始到a代表的位置
    while(b != NULL)
    {
        c[e] = b; //用c動態陣列紀錄字串
        b = strtok(NULL,a); //下一個字串
        e++; //這個步驟 1將陣列往下移一個 讓下一個陣列 繼續儲存 2紀錄字串的個數
    }

    for(int g = 0 ; g < e ; g++)
    {
        if(c[g] != d[0]) //當c動態陣列不等於未知動態陣列時
        {
            for(int h = g ; h < e ; h++) //讓c檢查它和它以後的動態陣列
            {
                if(c[g] == c[h]) //當陣列相同時,運作次數 使次數上升 ,用來記錄出現次數
                {
                    f[g]++;
                    if(h != g) //當它不是第一次出現的字串時 , 將出現的字串變更為空白
                    {
                        c[h] = d[0];
                    }
                }
            }
        }
    }


    for(int i = 0 ; i < e ; i++)
    {
        if(c[i] != d[0]) //當動態陣列不是空白的動態陣列時
        {
            cout << c[i] << ": "  << f[i] << endl; //叫出動態陣列中的內容 , 並且叫出次數
        }
    }

    return 0;
}
//次數陣列的次數記錄在開字串地一次出現的位置 , 而非第0 第1 這樣等差增長的位置中

