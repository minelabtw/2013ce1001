#include<iostream>
#include<string>

using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    string word[100];
    string kind[100];
    int j=0,n=0,count[100] = {0};

    for(int i=0 ; i<sizeof(str) ; i++)           //將最初陣列裡的元素以一個單字為單位存到一個新陣列
    {
        if(str[i]!=' ')
        {
            word[j] += str[i];
        }

        else
        {
            j++;
        }
    }
    ++j;

    for(int i=0 ; i<j ; i++)
    {
        bool check = 0;

        for(int m=0 ; m < n ; m++)
        {
            if(word[i] == kind[m])                          //如果word[i]跟kind[m]相同，則此單字次數加一
            {
                check = 1;
                count[m]++;
                break;
            }
        }

        if(!check)                                          //如果為第一次出現，存到新陣列，並計算次數加一
        {
            kind[n] = word[i];
            count[n]++;
            n++;
        }
    }

    for(int i=0; i<n ; i++)                                        //輸出各種單字以及它的出現次數
    {
        cout << kind[i] << ": " << count[i] << endl;
    }

    return 0;
}
