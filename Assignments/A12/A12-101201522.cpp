#include <iostream>
#include <map>
#include <string>
#include <sstream>
using namespace std;

int main(){
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    //預設歌詞 
    int i,num=0;//i為陣列用變數,num為有幾種不同的單字 
    string word[10001],s;//word[]按照輸入順序存不同的單字,s存擷取到的字串 
    istringstream input;//只允許輸入的字串串流 
    map<string,int> song;//用map存字串與其對應的出現次數,song內存的是pair<string,int> 
    input.str(str);//將預設歌詞放入input串流內,而後當作輸入 
    while(input >> s){//從串流內輸入擷取的第一個字串,以空格或換行分割同cin 
        if(!song[s])//該字串沒出現過便存入word[]內 
            word[num++] = s;
        song[s]++;//該字串對應的次數+1 
    }
    for(i=0;i<num;i++){
        cout << word[i] << ": " << song[word[i]] << endl;//按照輸入順序輸出 
    }
    return 0;
}
