#include <iostream>
#include <string.h>
#include <vector>

using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    vector <string> vector1(100);  //設100是預知歌詞字串不超過100個,如果超過會出錯
    vector <string> vector2(1);  //初始值設為1,讓第23行迴圈可以發動
    vector <int> number(1);  //初始值設為1,讓第42行不會出錯

    char *p;
    p=strtok(str," ");  //跑str這個字串,遇到空格就切割字串
    int i=0;
    while(p)
    {
        vector1[i]=p;  //切割字串後存入動態陣列vector1
        p=strtok(NULL," ");  //讓指標指向字串開頭(NULL),繼續跑str這個字串,遇到空格就切割字串
        i++;
    }

    for(int x=0; x<vector1.size(); x++)
    {
        for (int y=0; y<vector2.size(); y++)  //vector1的各元素放進vector2做檢查,如果重複計次就加一,如果沒重複就存入vector2然後計次開始
        {
            if (y<vector2.size()-1)
            {
                if (vector1[x]==vector2[y])
                {
                    number[y]=number[y]+1;
                    break;  //命令跳出內層for迴圈
                }
            }
            else
            {
                if (vector1[x]==vector2[y])
                    number[y]=number[y]+1;
                else
                {
                    vector2.push_back(vector1[x]);  //將vector1的值存入一個新增加在vector2後面的element
                    number.push_back(0);  //number陣列的初始值原先就設為1了(第12行),所以這裡增加的element為0
                }
            }
        }
    }

    for (int j=1; j<vector2.size(); j++)  //vector2初始值原先設為1,因此這邊的變數j也跟著宣告初始值為1
        cout <<vector2[j]<<": "<<number[j]<<endl;

    return 0;
}

