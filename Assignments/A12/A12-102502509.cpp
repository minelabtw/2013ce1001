#include <iostream>
#include <string.h>
#include <vector>
using namespace std;

int main()
{
    int c = 0;
    int s = 0;
    int frequency[100]= {};// 計次陣列

    vector < string > other(1); // 暫存比較的動態陣列
    vector < string > arr(100); // 暫存切割字串的動態陣列

    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to\
                ride In a one horse open sleigh jingle bells jingle bells jingle all\
                the way Oh what fun it is to ride In a one horse open sleigh Dashing\
                through the snow In a one horse open sleigh Oer the fields we go\
                Laughing all the way  Bells on bob tails ring Making spirits bright\
                What fun it is to laugh and sing A sleighing song tonight";
    const char *a = " "; // 切割終點
    char *b; // 切割字串

    b = strtok(str, a); // strtok == 進行切割動作
    while (b != NULL) // 當切割陣列存在時
    {
        arr[c] = b; // 字串轉動態陣列
        c++;
        b = strtok(NULL, a); // 下一組字串
    }

    for ( int x = 0; x < 81; x++ ) // 抓第一項來比，有81組
    {
        for ( int y = 0; y < other.size(); y++ ) // 抓比較陣列中的字串來比
        {
            if ( y != other.size() - 1 ) // (-1) == 字串位置
            {
                if ( arr[x] == other[y] ) // 若和比較陣列相同者
                {
                    frequency[y]++; // 計次陣列+1
                    break;
                }

            }
            else
            {
                if ( arr[x] == other[y] ) // 可能為比較陣列中最後一項
                {
                    frequency[y]++;
                }
                else // 載入比較陣列
                {
                    other.push_back( arr[x] ); // 補入最後一項
                }

            }
        }
    }
for ( int k = 1; k < other.size(); k++ )
    cout << other[k] << ": " << frequency[k] << endl;


    return 0;
}
