#include <iostream>

using namespace std;

void CountWord (int &WordNumber, char* str);    //count the number of words, continual spaces can be checked in function
void StoreWord (char* str, char* getword, string* word);    //turn char into string one by one
void WordClassify(string* WordCategory, int* WordFrequency, int &WordOrdinal, int WordNumber, string* word);    //count the frequency of word, and clean those repeat

int main(void)
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";

    int WordNumber=0;
    CountWord (WordNumber, str);

    char getword[sizeof(str)];
    string word[WordNumber];    //array (type string) used as a place to store words one by one
    StoreWord (str, getword, word);

    string WordCategory[WordNumber];    //array (type string) used as a place to store words without repeat
    int WordFrequency[WordNumber];    //array (type int) used as a place to store the frequency of words
    int WordOrdinal=0;
    WordClassify(WordCategory, WordFrequency, WordOrdinal, WordNumber, word);

    for (int i=0; i<WordOrdinal; i++)
        cout << WordCategory[i] << ": " << WordFrequency[i] << endl;

    return 0;
}

void CountWord (int &WordNumber, char* str)
{
    for (int i=1; str[i]!='\0'; i++)
        if (str[i]==' ' && str[i-1]!=' ')    //a word must end at a alphbet with ' ' behind, and there is no word between two continual spaces
            WordNumber++;
    if (str[sizeof(str)-1]!=' ')    //if the last character is not ' ', the last word will be lost-count
        WordNumber++;
}

void StoreWord (char* str, char* getword, string* word)
{
    int x[2]= {0,1};    //x[0] use as a point which point to the first alphbet of each word, x[1] use as a point which point to the ' ' behind the word, x[0] and x[1] are always a pair
    int y=0;    //count the oridinal of the word have been written in
    while (str[x[0]]!='\0')
    {
        if (x[0]==0 && str[x[0]]!=' ')
            while (str[x[1]]!=' ' && str[x[1]]!='\0')
                x[1]++;    //str[x[1]] is ' ' or '\0' before str[x[0]] which is not ' '
        else
        {
            while (str[x[0]]==' ')
                x[0]++;    //str[x[0]] is the char at the next of ' ' which is not ' '
            x[1]=x[0]+1;
            while (str[x[1]]!=' ' && str[x[1]]!='\0')
                x[1]++;    //str[x[1]] is ' ' or '\0' before str[x[0]] which is not ' '
        }
        for (int i=x[0]; i<x[1]; i++)
            getword[i-x[0]]=str[i];
        getword[x[1]-x[0]]='\0';
        word[y]=getword;
        y++;
        x[0]=x[1];
    }
}

void WordClassify(string* WordCategory, int* WordFrequency, int &WordOrdinal, int WordNumber, string* word)
{
    for (int i=0; i<WordNumber; i++)    //clean the garbage
    {
        WordCategory[i]="";
        WordFrequency[i]=0;
    }
    for (int i=0; i<WordNumber; i++)
    {
        int test=0;    //variable used as checking whether the string has been written in
        for (int j=0; j<WordOrdinal; j++)
            if (word[i]==WordCategory[j])
            {
                WordFrequency[j]++;    //the string has been written in, count
                test=1;    //the string has been written in, tell at if that don't write in again
                j=WordOrdinal;    //end for as known that the string has been written in
            }
        if (test==0)    //the string has not been written in yet
        {
            WordCategory[WordOrdinal]=word[i];    //write in
            WordFrequency[WordOrdinal]++;    //start counting
            WordOrdinal++;
        }
    }
}
