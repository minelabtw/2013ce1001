#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main ()
{
    char str[] = "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    string s; //宣告一個 stirng 變數，用來儲存暫時的資料。
    vector<string> data( 0 ); //宣告三個動態陣列，data 儲存 str 分割好之後的各個字串。
    vector<string> word( 1 ); // word 儲存全部的字串。
    vector<int> counter( 1 ); // counter 儲存全部的字串所出現的次數。
    for ( int i = 0 ; i < sizeof(str) ; i++ ) //把 str 分割好之後丟到 data 裡做儲存
    {
        if ( str[i] == ' ' || i == sizeof(str) - 1 )
        {
            data.push_back( s ); //把資料丟到 data 的最後面
            s = ""; //把 s 所儲存的資料消除
        }
        else
        {
            s += str[i];
        }
    }

    for ( int x = 0 ; x < data.size() ; x++ ) //把 data 內部的資料一個一個拿出來作比對
    {
        for ( int y = 0 ; y < word.size() ; y++ ) //把 data 的資料和每一個 word 內部的資料作比對
        {
            if ( y != word.size() - 1 ) //如果還沒比到最後一個
            {
                if ( data[x] == word[y] ) //有找到一樣的，就把對應的次數 + 1，然後離開for迴圈。
                {
                    counter[y] += 1;
                    break;
                }
            }
            else //如果比到了最後一個
            {
                if ( data[x] == word[y] ) //有可能最後一個剛好是一樣的
                {
                    counter[y] += 1;
                }
                else //如果沒有，就把這東西儲存到 word 陣列裡面，對應的 counter 也要一起變大。
                {
                    word.push_back ( data[x] );
                    counter.push_back ( 0 );
                }
            }
        }
    }
    for ( int j = 1 ; j < word.size() ; j++ ) //輸出結果
    {
        cout << word[j] << ": " << counter[j] << endl;
    }

    return 0;
}
