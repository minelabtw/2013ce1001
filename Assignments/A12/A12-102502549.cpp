#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
using namespace std;

//宣告並實作wordcount函式，此函式會依序檢查每個單字並統計出現次數
void wordcount(string warray[],int carray[],string word[],int size)
{
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<42; j++)
        {
            if(warray[j]==word[i])
            {
                carray[j]++;
                break;
            }
        }
    }
}

int main()
{
    //我們的題目....
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";

    //wordbank和count分別存出現的單字和出現次數
    string wordbank[42]= {{"jingle"},{"bells"},{"all"},{"the"},{"way"},{"Oh"},{"what"},{"fun"},{"it"},{"is"},{"to"},{"ride"},{"In"},{"a"},{"one"},{"horse"},{"open"},{"sleigh"},{"Dashing"},{"through"},{"snow"},{"Oer"},{"fields"},{"we"},{"go"},{"Laughing"},{"Bells"},{"on"},{"bob"},{"tails"},{"ring"},{"Making"},{"spirits"},{"bright"},{"What"},{"laugh"},{"and"},{"sing"},{"A"},{"sleighing"},{"song"},{"tonight"}};
    int count[42]= {0};

    //temp用來存讀到的字，wordnum存總共讀到幾個單字
    string word[100];
    int wordnum=0;

    ofstream out("temp.txt",ios::out);//建立ofstream物件
    ifstream in("temp.txt",ios::in);//建立ifstream物件

    //檢查檔案是否正常開啟，若否，則結束程式
    if((!out)||(!in))
    {
        cerr<<"File could not be opened!";
        return 0;
    }

    //以下實作將字元陣列的單字存到另一字串陣列
    //首先將字元一個一個輸出到檔案中
    for(int i=0; i<strlen(str); i++)
    {
        out<<str[i];
    }

    //用完要關掉
    out.close();

    //然後從檔案一次讀入一個單字存入陣列並得出單字數量
    while(!in.eof())
    {
        in>>word[wordnum];
        wordnum++;
    }

    //用完記得關掉
    in.close();

    wordcount(wordbank,count,word,wordnum);//呼叫這神奇的函式

    //印出結果
    for(int i=0; i<42; i++)
    {
        if(i==41)
            cout<<wordbank[i]<<": "<<count[i];
        else
            cout<<wordbank[i]<<": "<<count[i]<<endl;
    }

    remove("temp.txt");//刪掉temp.txt

    //結束
    return 0;
}
