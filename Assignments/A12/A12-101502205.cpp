#include<iostream>
#define word_length 20

using namespace std;

/*
---------------
I do some practice though the STD is useful and this assignment doesn't require that much.
---------------
*/

//the structure used to store a word including it's key and frequency
//key: order of appearance, cause this assignment require it
struct Words{
    int key, frequency;
    char data[word_length]; //word_length: maximum length of a word
};

//heapsort time complexity: O(n*lg(n))
class Heapsort{
public:
    //Constructor
    Heapsort(int s = 100){
        arr = new Words*[size];
        length = 0;
    }
    //Destructor
    ~Heapsort(){
        for(int i=1; i<=length; i++){
            delete arr[i];
        }
        delete[] arr;
    }
    //add an element(word), but we will build it later
    void add(int k, const char *c, int f){
        length++;
        arr[length] = new struct Words;
        arr[length]->key = k;
        //Copy the string
        int i=-1;
        do{
            i++;
            arr[length]->data[i] = c[i];
        }while(c[i]!=0);
        arr[length]->frequency = f;
    }
    //Let's sort it!
    void sortHeap(){
        buildHeap(); //buildHeap (bottom-up)
        struct Words *tmp;
        for(int i=length; i>1; i--){
            swap(1, i); //swap the largest element and ith element
            adjust(1, i-1); //Heapify O(lg n)
            //ith element is sorted
        }
    }
    //print all elements
    void printAll(){
        for(int i=1; i<=length; i++){ //arr[0] is empty
            cout << arr[i]->data << ": " << arr[i]->frequency << endl;
        }
    }
private:
    //swaping arr[i] and arr[j]
    void swap(int i, int j){
        struct Words *tmp;
        tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
    //adjust a subtree so that the subtree is still a heap
    void adjust(int parent, int last){
        struct Words *hold = arr[parent]; //hold the root of subtree cause I didn't use swap;
        int child = parent*2;
        while(child<=last){
            if(child+1 <= last){ //if right child is exist
                if(arr[child]->key < arr[child+1]->key) //test which child is larger
                    child++;
            }
            if(hold->key < arr[child]->key){
                arr[parent] = arr[child];
            }else{
                break;
            }
            parent = child;
            child *= 2;
        }
        arr[parent] = hold; //put it in correct place
    }
    //Heapify
    void buildHeap(){
        //bottom-up for building it in linear time
        for(int i=length/2; i>0; i--){
            adjust(i, length);
        }
    }
    int size, length;
    struct Words **arr;
};


/*
---------------
Dictionary (Binary Tree) MuQ~
---------------
To look up a word:
    As we using a dictionary, we start from the first letter.
    1. go to the root
    2. if the root isn't the letter we want, than go through the right subtree and repeat step 1
    3. if the root is the letter we want, than go through the left subtree and look for the next letter
    4. repeat 1~3 until we find the last letter
Time Complexity:
    O(l)    l: string lenth of whole song    Linear Time :)
Unfortunately, this method can only output in the order of ASCII code,
*/
class Patchouli{    //Patchouli: a kind of plant, trust me  MuQ~
public:
    Patchouli(char c = 0, Patchouli *r = 0){
        //Constructor   MuQ~
        //if there is no input then this is the root
        data = c;
        right = r;
        left = 0;
        counter = 0;
        key = 0;
    }
    ~Patchouli(){
        //Destructor    MuQ~
        delete left;
        delete right;
    }
    //Took up a word and add the counter    MuQ~
    void addWord(char *letter){
        addWord(letter, key);
        //(root).key: the (largest key)+1 in this tree
        //(node or leaf).key: order of appearance {key==0: this word never apear}
    }
    //Final ouput   MuQ~
    void printByKeys(){
        char c[word_length];
        Heapsort h;
        copyToHeap(h, c);
        h.sortHeap();   //sort
        h.printAll();
    }
    //useless in this in this assignment    MuQ~
    void printByASCII(){
        char c[word_length];
        Heapsort h;
        copyToHeap(h, c);
        h.printAll();
    }
private:
    //recurrence    MuQ~
    void addWord(char *letter, int &k){
        //if the letter is NULL
        //do nothing
        if(!*letter)
            return;
        if(*letter==data){
            //find the letter
            letter++;
            if(!*letter){
                //if next letter is NULL, than we find the word
                if(!counter)
                    key = ++k;
                //give the key value if the counter is zero
                counter++;
            }else{
                //find the letter but havn't found the word => go left subtree
                if(left){
                    if(*letter < left->data){  //we hope this dictionary is still in order (ASCII)
                        left = new Patchouli(*letter, left);    //add node if it doesn't exist
                    }
                }else{
                    //add node if it doesn't exist
                    left = new Patchouli(*letter);
                }
                left->addWord(letter, k); //continue recurrence
            }
        }else{
            //havn't found the letter => go right subtree
            if(right){
                if(*letter < right->data){  //we hope this dictionary is still in order (ASCII)
                    right = new Patchouli(*letter, right);  //add node if it doesn't exist
                }
            }else{
                //add node if it doesn't exist
                right = new Patchouli(*letter);
            }
            right->addWord(letter, k); //continue recurrence
        }
    }
    //copy data MuQ~
    void copyToHeap(Heapsort &h, char *c, int i=0){
        c[i]=data;
        c[i+1]=0;
        if(counter){
            h.add(key, c, counter);
        }
        if(left)
            left->copyToHeap(h, c, i+1);
        if(right)
            right->copyToHeap(h, c, i);
    }
    char data;
    int counter, key;
    Patchouli *left, *right;
};

int main(){
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    Patchouli tree_root; //MuQ~
    char *ptr = str, word[word_length];
    int i;
    while(*ptr){
        //identify a word
        i=0;
        while(*ptr && *ptr!=' '){
            word[i] = *ptr;
            ptr++;
            i++;
        }
        word[i] = 0; //add NULL at the end
        if(i)
            tree_root.addWord(word);    //count the word
        if(!*ptr)
            break;  //end of the song
        ptr++;
    }
    tree_root.printByKeys();
    return 0;
}
