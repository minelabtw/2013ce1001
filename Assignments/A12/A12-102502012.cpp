#include<iostream>
#include<cstring>
#include<vector>
using namespace std;
struct word{
	int count;
	string voc;
	word(int _count, string _voc){
		count = _count;
		voc = _voc;
	}
};

int main(){
	vector<struct word> Q;
	char str[] = "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
	char S[100][100];
	char *ptr;
	char *delma = " ";
	ptr = strtok(str, delma);
	while (ptr != NULL){ // split str by ' '
		bool found = false;
		for (int i = 0; i < Q.size() && !found; i++){
			if (ptr == Q[i].voc){
				found = true;
				Q[i].count++;
			}
		}
		if (!found){
			struct word S(1, ptr);
			Q.push_back(S);
		}
		ptr = strtok(NULL, delma);
	}
	for (int i = 0; i < Q.size(); i++)
		cout << Q[i].voc << ": " << Q[i].count << endl;
	return 0;
}
