#include <iostream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
struct data
{
    string s;
    int num;    //the number of string
};
int main()
{
    ios::sync_with_stdio(0);
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    stringstream ss(str);
    string tmp;
    string revstr;
    vector<data>v;
    while(ss>>tmp)  //revese the string
        revstr+=(" "+tmp);
    stringstream ss2(revstr);
    while(ss2>>tmp)
    {
        bool flag=true; //check whether data had pushed
        for(size_t i=0; i<v.size(); ++i)
            if(v[i].s==tmp)
            {
                v[i].num++;
                flag=false;
                break;
            }
        if(flag)v.push_back((data){tmp,1}); //push data into vector
    }
    for(size_t i=0; i<v.size(); ++i)
        cout<<v[i].s<<": "<<v[i].num<<endl;
    return 0;
}
