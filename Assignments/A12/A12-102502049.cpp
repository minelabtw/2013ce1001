#include <iostream>
#include <cstring>
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    string word[100]= {}; //儲存單字.次數
    int space=-1;
    int nextspace; //前後空格
    int k=0; //第k+1個單字


    for (int i=0; i<=sizeof(str); i++)
    {

        if (str[i]==' ') //抓取前後空格
        {
            nextspace=i;
            space++;
            for (int space1=space; space1<nextspace; space1++) //儲存單字
            {
                word[k]+=str[space1];
            }
            k++;
            space=nextspace; //移動前空格位置
        }
    }

    word[k]="tonight"; //補上最後一個單字
    k++;

    int times=0;
    int p=0;

    for (int i=0; i<k; i++)
    {
        int same=0;
        for (int j=0; j<i; j++) //判斷一樣
        {
            if (word[i]==word[j])
                same=1;
        }

        if(same) //skip
            continue;
        for (int j=0; j<k; j++) //times
        {
            if (word[i]==word[j]) //次數
            {
                times++;
            }
        }
        cout << word[i] << ": " << times << endl; //印出單字與次數
        times=0;
    }

    return 0;
}
