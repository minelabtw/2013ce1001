#include <iostream>
using namespace std;

int main(){
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    string strFoundArray[100] = {};//宣告string型別的strFoundArray陣列，元素數量為100個，皆初始化為空字串，用來存到目前為止發現的單字
    int strCountArray[100] = {};//宣告int型別的strCountArray，元素數量為100個，皆初始化為0，用來存各個單字出現次數
    //ex: strFoundArray[0] = "jingle", strCountArray[0] = 6，代表jingle出現六次
    string word = "";//宣告string型別的word，並初始化為空字串，用來存目前正讀到的單字

    for(char* strPtr = str;;){//判斷式和遞增式放在迴圈結尾判斷
        //每次迴圈先讀取一個字元 判斷該字元是空白或結尾，還是其他字
        if(*strPtr == ' ' || *strPtr == '\0'){
            //word已經讀完一個完整單字
            int j = 0;//因為j在迴圈外需要用到 所以放在迴圈外宣告
            for(; strFoundArray[j].length() != 0; j++){
            //若strFoundArray[j]為空字串，其長度必為0，這樣可以避免跑到沒有放字串的地方
                if(strFoundArray[j] == word){//找到就跳出迴圈
                    break;
                }
            }
            if(strFoundArray[j].length() == 0){//如果找不到，需要先新增單字到strFoundArray
                strFoundArray[j] = word;
            }
            strCountArray[j]++;
            word = "";//清掉單字以供下個迴圈使用
        }
        else{//word尚未讀完，把目前獨到的字元接到word後面
            word += *strPtr;
        }
        if( *strPtr != '\0'){//若目前讀到字元不是結尾則讀取下個字元
            strPtr++;
        }
        else{//否則就結束迴圈!
            break;
        }
    }
    for(int i = 0; strFoundArray[i].length() != 0; i++){
        cout << strFoundArray[i] << ": " << strCountArray[i] << endl;
    }
    return 0;
}
