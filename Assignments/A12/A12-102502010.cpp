#include<iostream>
#include<string>
#include<cstring>
#include<map>
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *cut=" ",*point; //用空白當分割,指字串
    int n=0,i;  //紀錄字串陣列長度
    string character[100];  //紀錄出現過的單字
    map<string,int>time;  //計算單字出現次數
    point=strtok(str,cut);  //切割啦
    while(point!=NULL)
    {
        if(time[point]==0)  //如果沒出現過
            character[n++]=point;  //紀錄在字串陣列中
        time[point]++;  //單字次數加一
        point=strtok(NULL,cut);
    }
    for(i=0; i<n; i++)
        cout<<character[i]<<": "<<time[character[i]]<<endl;
    return 0;
}
