#include <iostream>
#include <string.h>
#include <vector>       //賦予vector有著指令
using namespace std;
int main()
{
    int k=0;
    int f[100]={};      //設一個函數
    vector < string > other(1);     //讓當指令指向它時，可以從頭開始跑起，other是一個容器
    vector < string > g(100);       ////讓當指令指向它時，可以從頭開始跑起，g是另一個容器
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    const char *a=" ";      //char是讓位元可以輸入字
    char *b;
    b=strtok(str,a);        //strtok是切割動作
    while(b!=NULL)      //把str轉進集合
    {
        g[k]=b;
        k++;
        b=strtok(NULL,a);
    }
    for(int m=0; m<k ;m++)      //做原集合的抽取
    {
        for(int n=0 ; n<other.size() ; n++)     //做other集合的比較
        {
            if (n!=other.size()-1)
            {
                if (g[m]==other[n])     //既然路數量
                {
                    f[n]++;
                    break;
                }
            }
            else
            {
                if (g[m]==other[n])     //既然路數量
                {
                    f[n]++;
                }
                else
                {
                    other.push_back( g[m] );        //在尾部加一位置
                }
            }
        }
    }

    int z=1;

    while(z<other.size())       //輸出答案
    {
        cout << other[z] << ": " << f[z] << endl;
        z++;
    }
    return 0;
}
