#include<iostream>
#include<string>
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    int wordcounter=0;                  //用來計算單字數目的變數。
    for(int i=0; str[i]!='\0' ; i++)    //每遇到一個空格就代表空格前一定有一個單字。
    {
        if(str[i] == ' ')
        {
            wordcounter++;
        }
    }
    wordcounter++;                      //最後一個單字的後面沒有空格，所以要把單字數再加一。


    string word[wordcounter];           //用來儲存所有單字的string陣列。(單字重複也要寫進去)
    string data[wordcounter];           //用來儲存整理過後的單字表的string陣列。(裡面不會有重複的單字)
    int coun[wordcounter];              //用來儲存各個單字出現次數的int陣列。(編號和data陣列同步)

    for(int q=0; q<wordcounter; q++)    //初始化個陣列。
    {
        data[q] = "";
        coun[q] = 0 ;
    }

    int k=0;
    for(int j=0; str[j]!='\0'; j++)     //逐一檢查str陣列中的元素。
    {
        if(str[j]!=' ')                 //若字元不為空白，則將字元加到目前所在的word陣列的最後一欄。
        {
            word[k].push_back(str[j]);
        }
        else
        {
            k=k+1;                      //若字元為空白，則代表需要跳到下一個word陣列(也就是要換字了)填入字元。
        }
    }

    int m,n,o=0,status=0;
    for(m=0; m<=wordcounter-1; m++)     //將word陣列中的每一個元素一一和data陣列比對。
    {
        status=0;
        for(n=0; n<=wordcounter-1; n++)
        {
            if(word[m].compare(data[n]) == 0)   //若在data中找到和word陣列中相同的元素，就把相對應的coun陣列值+1。
            {
                coun[n]++;
                status=1;
            }
        }

        if(status==0)                   //word陣列中一個元素和data所有元素對比完後，假如沒有找到相同的元素，status的值就會保持0，並進入if迴圈。
        {
            data[o].assign(word[m]);    //將這次對比的word陣列中的元素複製到data陣列中。
            coun[o]++;                  //同時把計數加上去
            o++;
        }
    }

    for(int r=0; r<=o; r++)             //利用for迴圈印出結果。
    {
        cout << data[r] << ": " << coun[r] << endl;
    }

    return 0;
}
