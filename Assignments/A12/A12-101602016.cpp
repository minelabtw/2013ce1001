#include<iostream>
#include<string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace::std;

int main()
{
    int i=0,j=0,k=0,number=0,number2=1;//i,j,k給迴圈使用;number是單字數;number2是個別單字出現次數
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle  bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    for(i=0; i<sizeof(str); i++){
        if(str[i]==' '){
            str[i]='\0';//將空格變成\0
            number++;//同時計算空格數
        }
    }
    number++;//單字數為空格數加1
    char *save[number];
    save[0]=&str[0];//將文章第一個單字存進save[0]
    for(i=0; i<sizeof(str); i++){
        if(str[i]=='\0'){
            j++;
            save[j]=&str[i+1];//將文章的所有單字個別存入save陣列裡
        }
    }
    for(i=0; i<number; i++){//由save裡的第一個單字開始比較
        for(j=i+1; j<number; j++){//比較該單字後面有沒有與他相同的單字
            if(strcmp(save[i],save[j])==0){//如果後面有相同的單字
                number2++;//該單字出現次數加一
                for(k=j; k<number; k++)
                    save[k]=save[k+1];//並將該單字移除(也就是將之後全部的單字往前推一位)
                number--;//單字總數減一
            }
        }
        if(strcmp(save[i],"")!=0)//為了排除文章可能同時出現兩個空格的BUG
            cout<<save[i]<<": "<<number2<<endl;//輸出該單字的出現次數
        number2=1;//將單字出現數歸1
    }
    return 0;
}
