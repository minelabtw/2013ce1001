#include <iostream>
#include <string.h>  //給strock
#include <map>  //給map
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *token, *slpit[81];  //token暫存用途、slpit為分割後的單字
    int n=0;

    token = strtok( str, " " );
    while ( token != NULL)
    {
        slpit[ n++ ] = token;
        token = strtok( NULL, " " );
    }  //分割成單字存到slpit陣列

    map<string, int> myMap;  //使用map讓此題更方便、快速
    for( int n=0; n<81; n++)
        myMap[slpit[n]]++;

    map<string, int>::iterator ite;  //用iterator
    for (ite = myMap.begin(); ite != myMap.end(); ite++)  //從map的第一個元素到最後一個元素
        cout << ite->first << ": " << ite->second << endl;  //輸出

    return 0;
}
