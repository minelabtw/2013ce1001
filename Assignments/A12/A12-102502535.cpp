#include <iostream>
#include <string.h>
#include <stdio.h>

using namespace std ;

string answer[ 42 ] ;  //最後輸出字串的陣列。
string single[ 81 ] ;  //放個別的字。
int check ( string cWord ) ;

int main()
{
    char str[] = "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *sin ;
    int count[ 42 ] ;  //計算次數。

    for ( int i = 0 ; i < 42 ; i ++ )
        count[ i ] = 0 ;

    int a = 0 ;
    sin = strtok ( str , " " ) ;  //將歌詞切割成數個字，以空格來分辨。
    while ( sin != NULL )
    {
        single[ a ] = sin ;
        a ++ ;
        sin = strtok ( NULL , " " ) ;
    }

    int n = 0 ;
    for ( int i = 0 ; i < 81 ; i ++ )
    {
        if ( check ( single[ i ] ) == 0 )
        {
            answer[ n ] = single[ i ] ;
            n ++ ;
        }
    }  //判別是否重複；如果是新的，將它存進ans中。

    for ( int i = 0 ; i < 42 ; i ++ )
    {
        for ( int j = 0 ; j < 81 ; j ++ )
        {
            if ( answer[ i ] == single[ j ] )
                count[ i ] ++ ;
        }
    }  //比較兩陣列，若出現相同的字即把計數器加一。

    for ( int i = 0 ; i < 42 ; i ++ )
        cout << answer[ i ] << ": " << count[ i ] << endl ;

    return 0 ;
}

int check ( string cWord )
{
    string tWord ;

    for ( int i = 0 ; i < 42 ; i ++ )  //比較兩陣列。
    {
        tWord = answer[ i ] ;
        if ( cWord == tWord )
            return 1 ;  //若出現相同字，回傳1。
    }
    return 0 ;  //如沒有出現相同，回傳0。
}
