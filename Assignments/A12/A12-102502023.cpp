#include <iostream>
#include <string.h>
using namespace std;

int main()
{
    // initialize integer from counter to counter41
    int counter=0,counter1=0,counter2=0,counter3=0,counter4=0,counter5=0,counter6=0,counter7=0,counter8=0,counter9=0,counter10=0,counter11=0,counter12=0,counter13=0,counter14=0,counter15=0,counter16=0,counter17=0,counter18=0,counter19=0,counter20=0,counter21=0,counter22=0,counter23=0,counter24=0,counter25=0,counter26=0,counter27=0,counter28=0,counter29=0,counter30=0,counter31=0,counter32=0,counter33=0,counter34=0,counter35=0,counter36=0,counter37=0,counter38=0,counter39=0,counter40=0,counter41=0;
    // initialize string from s to s41
    string s("jingle"),s1("bells"),s2("all"),s3("the"),s4("way"),s5("Oh"),s6("what"),s7("fun"),s8("it"),s9("is"),s10("to"),s11("ride"),s12("In"),s13("a")
    ,s14("one"),s15("horse"),s16("open"),s17("sleigh"),s18("Dashing"),s19("through"),s20("snow"),s21("Oer"),s22("fields"),s23("we"),s24("go"),s25("Laughing")
    ,s26("Bells"),s27("on"),s28("bob"),s29("tails"),s30("ring"),s31("Making"),s32("spirits"),s33("bright"),s34("What"),s35("laugh"),s36("and"),s37("sing")
    ,s38("A"),s39("sleighing"),s40("song"),s41("tonight");
    // initialize character array str
    char str[]= "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *array[ 81 ]; // initialize character pointer array
    char *test = strtok( str, " " ); // initialize character pointer test to dump the strings
    int i = 0; // initialize integer i and set its value to 0

    while ( test != NULL ) // while loop to dump the string into array
    {
        array[ i ] = test;

        test = strtok ( NULL, " " );
        i++;
    }

    for ( int j = 0; j < 81; j++ ) // for loop to count the appearance of strings
    {

        if ( array[ j ] == s )
            counter++;
        if ( array[ j ] == s1 )
            counter1++;
        if ( array[j] == s2 )
            counter2++;
        if ( array[j]== s3 )
            counter3++;
        if ( array[j] == s4 )
            counter4++;
        if ( array[j] == s5 )
            counter5++;
        if ( array[j] == s6 )
            counter6++;
        if ( array[j] == s7 )
            counter7++;
        if ( array[j] == s8 )
            counter8++;
        if ( array[j] == s9 )
            counter9++;
        if ( array[j] == s10 )
            counter10++;
        if ( array[j] == s11 )
            counter11++;
        if ( array[j] == s12 )
            counter12++;
        if ( array[j] == s13 )
            counter13++;
        if ( array[j] == s14 )
            counter14++;
        if ( array[j] == s15 )
            counter15++;
        if ( array[j] == s16 )
            counter16++;
        if ( array[j] == s17 )
            counter17++;
        if ( array[j] == s18 )
            counter18++;
        if ( array[j] == s19 )
            counter19++;
        if ( array[j] == s20 )
            counter20++;
        if ( array[j] == s21 )
            counter21++;
        if ( array[j] == s22 )
            counter22++;
        if ( array[j] == s23 )
            counter23++;
        if ( array[j] == s24 )
            counter24++;
        if ( array[j] == s25 )
            counter25++;
        if ( array[j] == s26 )
            counter26++;
        if ( array[j] == s27 )
            counter27++;
        if ( array[j] == s28 )
            counter28++;
        if ( array[j] == s29 )
            counter29++;
        if ( array[j] == s30 )
            counter30++;
        if ( array[j] == s31 )
            counter31++;
        if ( array[j] == s32 )
            counter32++;
        if ( array[j] == s33 )
            counter33++;
        if ( array[j] == s34 )
            counter34++;
        if ( array[j] == s35 )
            counter35++;
        if ( array[j] == s36 )
            counter36++;
        if ( array[j] == s37 )
            counter37++;
        if ( array[j] == s38 )
            counter38++;
        if ( array[j] == s39 )
            counter39++;
        if ( array[j] == s40 )
            counter40++;
        if ( array[j] == s41 )
            counter41++;
    }

    cout << "jingle: " << counter << endl
         << "bells: " << counter1 << endl
         << "all: " << counter2 << endl
         << "the: " << counter3 << endl
         << "way: " << counter4 << endl
         << "Oh: " << counter5 << endl
         << "what: " << counter6 << endl
         << "fun: " << counter7 << endl
         << "it: " << counter8 << endl
         << "is: " << counter9 << endl
         << "to: " << counter10 << endl
         << "ride: " << counter11 << endl
         << "In: " << counter12 << endl
         << "a: " << counter13 << endl
         << "one: " << counter14 << endl
         << "horse: " << counter15 << endl
         << "open: " << counter16 << endl
         << "sleigh: " << counter17 << endl
         << "Dashing: " << counter18 << endl
         << "through: " << counter19 << endl
         << "snow: " << counter20 << endl
         << "Oer: " << counter21 << endl
         << "fields: " << counter22 << endl
         << "we: " << counter23 << endl
         << "go: " << counter24 << endl
         << "Laughing: " << counter25 << endl
         << "Bells: " << counter26 << endl
         << "on: " << counter27 << endl
         << "bob: " << counter28 << endl
         << "tails: " << counter29 << endl
         << "ring: " << counter30 << endl
         << "Making: " << counter31 << endl
         << "spirits: " << counter32 << endl
         << "bright: " << counter33 << endl
         << "What: " << counter34 << endl
         << "laugh: " << counter35 << endl
         << "and: " << counter36 << endl
         << "sing: " << counter37 << endl
         << "A: " << counter38 << endl
         << "sleighing: " << counter39 << endl
         << "song: " << counter40 << endl
         << "tonight: " << counter41;

    return 0;
}



