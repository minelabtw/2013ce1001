#include <iostream>
#include <cstring>
#include <string>
#include <map>
#include <vector>
using namespace std;


int main()
{
	char str[ ]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    map<string,int> words; //使用 map 紀錄單字出現的次數
	map<string,int>::iterator it;
	vector<string> wordsOrder; // 使用 vector 紀錄單字出現的順序
	char *tokenPtr;

	tokenPtr = strtok(str, " "); // 使用 strtok 斷詞

	while (tokenPtr != NULL) {
		string strword =  tokenPtr;

		it =  words.find(strword);

		if ( it == words.end() ) {
			words[strword] = 1;
			wordsOrder.push_back (strword);
		} else {
			words[strword]++;
		}
		tokenPtr = strtok (NULL, " ");
	}

	for ( vector<string>::iterator it = wordsOrder.begin(); it != wordsOrder.end(); ++it) {
        cout<< *it << ": " << words[*it] << endl;
    }
	return 0;
}
