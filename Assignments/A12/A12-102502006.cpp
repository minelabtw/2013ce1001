#include <iostream>

using namespace std;

void judge(char str[],int i,int space[],int &t,string word[], int &t2,int god[])
{
    string tmp=""; // 暫存字
    space[t]=i; // 存入空白位子
    for(int k=space[t-1]; k<space[t]-1; k++)tmp=tmp+str[k]; // 儲存成字串

    for(int j=0; j<t2; j++)
    {
        if(tmp==word[j]) //等於之前出現過的字串
        {
            tmp="";
            god[j]++;
            break;
        }
    }

    if(tmp != "")
    {
        word[t2-1]=tmp; // 存入成新字
        t2++;
    }

    t++;
}


int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";

    int space[400]= {}; // 儲存空白位子
    space[0]=0; // 第一個字母
    int t=1; // 空白位子的儲存位址
    string word[400]= {""}; // 出現的字
    int t2=1; // 重複字的位址
    int god[400]= {}; // 重複字的次數
    // 最後一個英文 位址 393
    // sizeof(str) = 395

    for(int i=0; i<sizeof(str)-1; i++)if(str[i]==' ' || i==sizeof(str)-2)judge(str,i+1,space,t,word,t2,god);

    for(int i=0; i<t2-1; i++)cout << word[i] << ": " << god[i]+1 << endl;

    return 0;
}
