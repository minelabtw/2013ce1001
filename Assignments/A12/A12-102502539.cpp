#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    int x = 0 ;
    int times[42] = {};
    string word;
    string vab[42] = { "jingle" , "bells" , "all" , "the" , "way", "Oh" , "what" , "fun" ,
                     "it" , "is" , "to" , "ride" , "In" , "a" , "one" , "horse" , "open" ,
                     "sleigh" , "Dashing" , "through" , "snow" , "Oer" , "fields" , "we" ,
                     "go" , "Laughing" , "Bells" , "on" , "bob" , "tails" , "ring" , "Making" ,
                     "spirits" , "bright" , "What" , "laugh" , "and" , "sing" , "A" , "sleighing" ,
                     "song" , "tonight" };  //單字個別存入字串陣列
    char str[] = "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight" ;

    for ( int i = 0 ; i <= strlen(str) ; i++ )
    {
        if ( str[i] != ' ' && str[i] != '\0' )
            word += str[i]; //從字元陣列中取出單字
        else
        {
            for ( int j = 0 ; j < 42 ; j++ )
            {
                if ( word == vab[j] )   //計算單字出現次數
                    times[j] ++;
            }
            x++;
            word = "";
        }
    }

    for ( int i = 0 ; i < 42 ; i++ )
    {
        cout << vab[i] << ": " << times[i] << endl;
    }

    return 0;
}
