#include <iostream>
#include <map>
#include <string>

int main()
{
    const char *s[]= //題目的一堆英文單字的矩陣
    {
        "jingle",
        "bells",
        "jingle",
        "bells",
        "jingle",
        "all",
        "the",
        "way",
        "Oh",
        "what",
        "fun",
        "it",
        "is",
        "to",
        "ride",
        "In",
        "a",
        "one",
        "horse",
        "open",
        "sleigh",
        "jingle",
        "bells",
        "jingle",
        "bells",
        "jingle",
        "all",
        "the",
        "way",
        "Oh",
        "what",
        "fun",
        "it",
        "is",
        "to",
        "ride",
        "In",
        "a",
        "one",
        "horse",
        "open",
        "sleigh",
        "Dashing",
        "through",
        "the",
        "snow",
        "In",
        "a",
        "one",
        "horse",
        "open",
        "sleigh",
        "Oer",
        "the",
        "fields",
        "we",
        "go",
        "Laughing",
        "all",
        "the",
        "way",
        "Bells",
        "on",
        "bob",
        "tails",
        "ring",
        "Making",
        "spirits",
        "bright",
        "What",
        "fun",
        "it",
        "is",
        "to",
        "laugh",
        "and",
        "sing",
        "A",
        "sleighing",
        "song",
        "tonight",
    };

    std::map<std::string, int> myMap; //儲存讀過的字 和記錄這個單字的次數
    for (size_t n = 0; n < sizeof s / sizeof s[0]; n++)
    {
        ++myMap[s[n]];
    }

    std::map<std::string, int>::const_iterator cpos;
    for (cpos=myMap.begin() ; cpos!=myMap.end() ; cpos++) //顯示出來
    {
        std::cout<<cpos->first<<": "<<cpos->second<<std::endl;
    }
    return 0;
}
