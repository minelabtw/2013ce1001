#include <iostream>
#include <string> //使用string

using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    const int arraySize = sizeof(str); //宣告陣列大小為字串大小
    string word[arraySize] = {}; //宣告陣列紀錄單字
    int w_count[arraySize] = {}; //宣告陣列紀錄字串出現次數
    int i = 0;
    int check = 0;

    for (int a=0; a<=sizeof(str)-1; a++) //宣告針對所有字串內字元檢查的迴圈
    {
        if (str[a]!=' ' and a!=sizeof(str)-1) //當字元不是空格時將字元加入陣列內
            word[i] += str[a];
        else if ( (str[a]==' ' and a!=sizeof(str)-1) or a==sizeof(str)-1 ) //當字元是空格時檢查當前單字是否與以記錄的單字重複
        {
            check = 0;
            while (check<=i)
            {
                if (word[i]!=word[check] and check!=i) //當單字不重複時繼續檢查
                    check++;
                else if (word[i]==word[check] and check!=i) //當單字重複時將當前陣列清空並計數及結束迴圈
                {
                    word[i].clear();
                    w_count[check]++;
                    break;
                }
                else if (check==i) //檢查不出重複時計數並結束迴圈
                {
                    if (a!=sizeof(str)-1) //當字串尚未結束時繼續
                    {
                        w_count[i]++;
                        i++;
                        break;
                    }
                    else //字串結束
                    {
                        w_count[i]++;
                        break;
                    }
                }
            }
        }
    }

    if(word[i]=="") //若最後一個字串為空字串，使i減一讓其不輸出
        i--;

    for (int j=0; j<=i; j++) //輸出結果
        cout << word[j] << ": " << w_count[j] << endl;

    return 0;
}
