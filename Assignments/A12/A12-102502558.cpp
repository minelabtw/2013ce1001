#include <iostream>
#include <vector>
#include <cstring>
using namespace std;
class word // custom class word
{
public:
    string name; // name
    int c; // count
    word(int _c, string _name) : c(_c), name(_name) {} // init with value
};

int main(int argc, char *argv[])
{
    // song
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char *sp = " ", *w; // split and tmp word
    bool found;
    vector<word> _list; // vector to check if the word has been added
    for (w = strtok(str, sp); w != NULL; w = strtok(NULL, sp))
    {
        for (int j=0, found = false;j<_list.size();j++)
            if (_list[j].name == w)
            {
                found = true;
                _list[j].c++; // count++
                break;
            }
        if (found == false) // if not found
        {
            word new_word(1,w);
            _list.push_back(new_word); // push into vector
        }
    }
    for (int i=0;i<_list.size();i++) // print result
        cout << _list[i].name << ": " << _list[i].c << endl;
    return 0;
}
