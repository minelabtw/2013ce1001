#include<iostream>
#include<string.h>
using namespace std;

int main()
{
	//原字串
	char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
	char *delim = " ";	//分隔符號
	char *pch;			//存分割的東西
	pch = strtok(str, delim);	//分割字串
	int b[50]={};		//用來記數的陣列
	string a[50]={"jingle","bells","all","the","way","Oh","what","fun","it","is","to","ride","In","a","one","horse","open","sleigh","Dashing","through","snow","Oer","fields","we","go","Laughing","Bells","on","bob","tails","ring","Making","spirits","bright","What","laugh","and","sing","A","sleighing","song"};
	while(pch!=NULL)	//當分割出來的
	{
		string c(pch);	//轉成string型態
		for(int j=0 ; j<41 ; j++)	//判斷有沒有一樣
		{
			if(c==a[j])			//如果一樣，計次加1
				b[j]++;
		}
		pch = strtok(NULL, delim);	//下一串字串
	}
	for (int i=0 ; i<41 ; i++)		//輸出結果
		cout << a[i] << ": " << b[i] << endl;
}
