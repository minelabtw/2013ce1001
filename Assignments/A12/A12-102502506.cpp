#include <iostream>
#include <cstring>
using namespace std;
int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    string a[100];  //存全部單字
    string same[100];  //存不重複的單字
    char *z;
    int counter=0;  //總單字數目減一
    int number=0;  //不重複單字數目減一
    int times[100]= {0};  //每個單字出現次數
    z = strtok(str," ");
    while(z)  //把分割單字存入字串陣列
    {
        a[counter]=z;
        counter++;  //計算總次數
        z =  strtok(NULL," ");
    }
    for( int j = 0; j <= counter; j++ )  //從第一個開始數 數到最後一個
    {
        int tmp = 0;  //判斷
        for( int k = 0; k < number; k++ )  //如果沒重複則tmp=0
        {
            if( same[k] == a[j] )
            {
                tmp++;
            }
        }
        if( tmp == 0 )  //沒有重複就進行計算
        {
            for( int i = j; i <= counter; i++ )  //從第j個開始數到最後一個
            {
                if( a[j] == a[i] )  //每有一個一樣的
                {
                    same[number]=a[j];  //把未重複的字串存入same字串
                    times[number]++;  //就加一次
                }
            }
            number++;  //移到下一個
        }
    }
    for ( int j = 0; j < number - 1; j++ )  //印出
    {
        cout << same[j] << ": " << times[j] << endl;
    }
    return 0;
}
