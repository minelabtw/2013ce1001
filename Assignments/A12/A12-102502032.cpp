#include <iostream>
#include <string>
using namespace std;

int main()
{
    //set data
    char str[] = "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";

    //caculate number of vocabularies
    //in this case, it doesn't take special case like double spaces ('  ') into account
    int ctr_1 = 0;  //counter
    if (str[0] != ' ')
        ctr_1 ++;
    for (int i = 0; i < sizeof(str); i ++)
        if (str[i] == ' ' and str[i + 1] != ' ')
            ctr_1 ++;

    //decalaration and initialization
    int location[ctr_1 + 1];    //the first word location of each vocabulary and the end location
    location[ctr_1 + 1] = {};
    location[ctr_1 + 1] = sizeof(str);  //end location
    string index[ctr_1];    //list of vocularies
    int index_count[ctr_1]; //times of each voculary shows
    for (int i = 0; i < ctr_1; i ++)
        index_count[i] = 0;
    int ctr_2 = 0;

    //find the locations
    if (str[0] != ' ')
    {
        location[0] = 0;
        ctr_2 ++;
    }
    for (int i = 0; i < sizeof(str); i ++)  //locate vocabularies
        if (str[i] == ' ')
        {
            location[ctr_2] = i + 1;
            ctr_2 ++;
        }

    //make a list and count how many times it shows
    for (int i = 0; i < ctr_1; i ++)
    {
        string temp; //temporary
        temp.assign(str, location[i], location[i + 1] - location[i] - 1);   //get a vocabulary
        for (int j = 0; j < ctr_1; j ++)
            if (index[j].compare(temp) == 0 )
            {
                index_count[j] ++;
                break;
            }
            else if (index[j].size() == 0)
            {
                index[j] = temp;    //add into list
                index_count[j] ++;
                break;
            }
    }

    //output the result
    for (int i = 0; i < ctr_1; i ++)
        if (index[i].size() > 0)
            cout << index[i] << ": " << index_count[i] << endl;
        else
            break;

    return 0;
}
