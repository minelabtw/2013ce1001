#include <iostream>
#include <vector>
#include <string>
using namespace std;
main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    vector<string> data(0);  //分割用
    string word;  //暫存
    vector<int> num(0);  //計數用
    int number;  //暫存
    vector<string> name(0);  //記名用

    for(int i=0; i<sizeof(str); i++)  //存進向量
    {
        if(str[i]!=' ' && str[i]!='\0')
            word+=str[i];
        else if( str[i]==' ' || str[i]=='\0')
        {
            data.push_back(word);
            word="";
        }
    }
    for(int j=0; j<data.size(); j++)  //刪去重複的並計數
    {
        if(data[j]!="0")
        {
            word=data[j];
            number=0;
            for(int k=0; k<data.size(); k++)
            {
                if(word==data[k])
                {
                    data[k]="0";
                    number++;
                }
            }
            name.push_back(word);
            num.push_back(number);
        }
    }

    for(int l=0; l<name.size(); l++)  //輸出結果
    {
        cout << name[l] << ": " << num[l];
        if(l!=name.size()-1)
            cout << endl;
    }

    return 0;
}
