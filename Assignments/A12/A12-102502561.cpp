#include<iostream>
#include<fstream>
#include<cstring>
using namespace std;

int main()
{
    int k[100]= {0};    //check if repeat
    int t=0;  //t for turtle counting (runns only once)
    int a=0;  //for rabbit circling(runns lot more times)
    string word1[100];  //to make sure words only appear once
    string stringx[100]; //use with rabbit circling (this is the array with all words in the song) concluding repeated words
    int counter[100] = {0}; //keep track of times the words appeared already

    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    ofstream fout ("Xmas song.txt"); //creates a file with such name
    fout << "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    // inputs this into that file
    fout.close();   //closes the file
    ifstream fin ("Xmas song.txt"); //calls this file
    for(int i=0; i<100; i++)
    {
        fin >> stringx[i];  //copys every word from file to stringx[]
    }

    //this is where real business starts

    for(t=0; t<100; t++)     //inner circle :runns slower
    {
        for(a=0; a<100; a++)    //circle the array
        {
            if(stringx[a]==stringx[t])  //if the words showed of before do the following
            {
                word1[t]=stringx[t];    //copy the word in the rabbit circle to the turtle circle
                counter[t]++;           //counter +1 :stands for the times the word shows up
                if(counter[t]>1)        //if the word appeared over twice(2) then mark it with k[a]=1
                {
                    k[a]=1;
                }
            }
        }
    }
    //output

    for(a=0; a<100; a++)
    {
        if(k[a]==1)
        {
            cout << ""; //output nothing when mark appears
        }
        else
        {
            if(word1[a]=="")
            {
                break;      //this is the rest of the space in the array filled by ""
            }
            else
            cout << word1[a] << ": " << counter[a] << endl;
        }
    }

    return 0;
}
