//============================================================================
// Name        : A12-102502038.cpp
// Author      : catLee
// Version     : 0.1
// Description : NCU CE1001 A12
//============================================================================

#include <iostream>

using namespace std;

class lyric{
  private:
  string _str;
  string analy_word[100];
  unsigned int analy_wordCount[100];
  unsigned int wordsCount;
  unsigned int analyCount; 
  void analyStr(void){
    unsigned int length = _str.length();
    wordsCount = 0;
    analyCount = 0;
    string temp = "";  //temp,for present word
    for(int i = 0;i<length;i++){  //loop!
      if(_str[i] != '\0' && _str[i] != ' '){  //end scan word mode when ' ' or \0
	temp += _str[i];
      }else if(temp != ""){
	int wrote = 0;
	for(int j = 0;j<analyCount;j++){  //loop!
	  if(analy_word[j] == temp){
	    wrote = 1;
	    analy_wordCount[j] += 1;
	    break;
	  }
	}
	if(!wrote){
	  analy_word[analyCount] = temp;
	  analy_wordCount[analyCount++] = 1;
	}
	temp = "";  //clean temp
	wordsCount++;
      }else{
	temp = "";
      }
    }
  }
public:
  lyric(string text){
    _str = text;
    analyStr();
  }
  void showCount(void){
    for(int i = 0;i<analyCount;i++){
      cout << analy_word[i] << ": " << analy_wordCount[i] << endl;
    }
  }
};

int main(void){  //main,BJ4
  string str="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
  lyric lrc(str);
  lrc.showCount();
}

//Commenting your code is like cleaning your bathroom — you never want to do it, but it really does create a more pleasant experience for you and your guests.
