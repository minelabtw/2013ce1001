#include <iostream>
using namespace std;
int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    //str[0]~str[392]共393個字，加上\0共394
    int displacement=0;//字元陣列從0開始，由左向右移動
    int length=0;//字串長度
    int time[50]= {};//計數器
    int startpoint=0;//每次讀取字元的起始位置
    int wordindex=0;//單字的位置
    string word[50]="";//儲存陣列的單字
    string temporary="";//暫存的字串，用來比對
    int k=0;
    do
    {
        if(str[displacement]==' ')
        {
            temporary="";//empty
            for(int i=0; i<length; i++)
                temporary = temporary +str[i+startpoint];//逐一儲存字元，形成字串
            length=0;//長度歸零
            displacement++;//位移
            startpoint=displacement;//新的起始點
            for(k=0 ; k<=wordindex ; k++)
            {
                //與前面放過的字串逐一比對
                if(temporary==word[k])
                {
                    time[k]++;//如果有重複，則該字串的計數器+1
                    break;
                }
            }
            if(temporary!=word[k])
            {
                word[wordindex]=temporary;//若無重複，則放入字串
                wordindex++;
            }
        }
        else
        {
            length++;
            displacement++;
            if(str[displacement]=='\0')//處理末端字元的問題，同上
            {
                temporary="";
                for(int i=0; i<length; i++)
                    temporary = temporary +str[i+startpoint];
                length=0;
                displacement++;
                startpoint=displacement;
                for(k=0 ; k<=wordindex ; k++)
                {
                    if(temporary==word[k])
                    {
                        time[k]++;
                        break;
                    }
                }
                if(temporary!=word[k])
                {
                    word[wordindex]=temporary;
                    wordindex++;
                }
            }
        }
    }
    while(str[displacement]!='\0');//位移至末端(結束字元)時結束迴圈
    for(int i=0 ; i<wordindex-1 ; i++)//印出單字
        cout << word[i] << ": " << time[i]+1 << endl;//放進去的第一次是0，故+1
    return 0;
}
