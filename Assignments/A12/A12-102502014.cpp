#include <iostream>
#include <string>
using namespace std;

const int WORD_SIZE = 50;
int main()
{
    string word[WORD_SIZE];      //存讀出的詞
    int frequencies[WORD_SIZE]= {};  //存詞的出現次數
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    char charNow;   //每次讀到的字元
    string wordNow=""; //每次讀出的詞
    bool foundWord=false;
    int s0=-1;          //前面的空白
    int s1;             //後面的空白
    int readIndex=0;   //取詞的時候讀到哪一個字元的位置
    int wordIndex=0;  //目前取出的詞數量
    int strlength = sizeof(str)/sizeof(char);  //題目的長度

    for(readIndex=0; readIndex<strlength; readIndex++)  //將歌詞中的每個字元讀入
    {
        charNow=str[readIndex];                         //讀出的字元存入charNow
        if(charNow==' ' || charNow=='\0')               //讀到空白或走到最後
        {
            foundWord=false;
            s1=readIndex;
            wordNow="";                //清理wordNow
            for(int i=s0+1; i<s1; i++)
            {
                charNow=str[i];
                wordNow += charNow;                   //將兩空白間的字元依次存入wordNow
            }
            for(int j=0; j<wordIndex; j++)            //目前word array中有幾個字 就找幾次(檢查目前的wordNow與word array中有無一樣的詞)
            {
                if(word[j]==wordNow)                  //word array中已有目前讀出的wordNow
                {
                    frequencies[j]++;
                    foundWord=true;
                }
            }
            if(foundWord==false)                      //再word array中沒找到目前讀出的wordNow
            {
                word[wordIndex]=wordNow;
                frequencies[wordIndex]++;
                wordIndex++;
            }
            s0=s1;
        }
    }
        for(int i=0;i<wordIndex;i++)                //列印結果
       {
           cout<<word[i]<<": "<<frequencies[i]<<endl;
       }
    return 0;
}
