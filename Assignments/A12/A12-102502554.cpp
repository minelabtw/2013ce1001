#include <iostream>
#include <cstring>
#include <malloc.h>

using namespace std;

int main()
{
    char buffer[]= {"jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight"};
    char *p; //用來截取 不含空白字元的字串
    char *str[100]= {};
    int  count[100]= {};
    int  i = 0, j, flag = 0;

    p = strtok(buffer," ");//將buffer切割,切割條件為空格

    while(p != 0)
    {
        for(j = 0 ; j < i; j++ )//尋找已存的字串迴圈
        {
            if(strcmp(str[j], p) == 0 )//判斷字串是否重覆
            {
                count[j]++;//計數加1
                flag = 1;//通知此字串已存
                break;
            }
        }
        if(flag == 0 )//p 尚未存入str陣列
        {
            str[i] = (char*)malloc(sizeof(p));//取得字串長度並取得記憶空間
            strcpy(str[i], p);//將p存入str陣列
            count[i] = 1;//設定count值等於
            i++;//增加str陣列index
        }
        p = strtok(0," ");//從buffer中讀取下一個字串
        flag = 0;
    }
    for( j = 0; j < i; j++)
    {
        cout << str[j] << ": " <<count[j] << endl;
    }//輸出

    return 0;
}
