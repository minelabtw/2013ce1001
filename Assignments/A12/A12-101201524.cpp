#include<iostream>
using namespace std;

int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    //題目給訂字串
    string word[100],token;//宣告word陣列儲存不同的單字,token用來佔存str提出來的字元
    int times[100]={},i,j=0,k=0,size=sizeof(str)/sizeof(str[0]);//宣告times陣列紀錄對應的word陣列單字的出現次數,i,j,k用在for迴圈,size代表歌曲長度

    for(i=0;i<size;i++)//讓str從第一個字判斷到最後一個字
    {
        if(str[i] != ' ')//若不是空格則加到暫存的token
            token=token+str[i];
        else//若遇到空格(token成為一單字)
        {
            for(j=0;j<=k;j++)//讓token跟已出現過的單字做比較
            {
                if(j==k)//若都不相同則將token傳到新一格的word裡並將該單字的出現次數加一
                {
                    word[j]=token;
                    times[j]++;
                    k++;
                    break;
                }
                else if(word[j]==token)//若有相同的則將該單字的出現次數加一
                {
                    times[j]++;
                    break;
                }
            }
            token="";//清空token讓他記錄下一個單字
        }
    }
    if(sizeof(token)!=0)//若判斷完str後token內還有單字(只會有一個)
    {
        for(j=0;j<=k;j++)//用相同方法去找到相同的單字或自加一新單字
            {
                if(j==k)
                {
                    word[j]=token;
                    times[j]++;
                    k++;
                    break;
                }
                else if(word[j]==token)
                    times[j]++;
            }
    }
    for(i=0;i<k;i++)//按照出現順序輸出各單字和其出現次數
        cout << word[i] << ": " << times[i] << endl;

    return 0;
}
