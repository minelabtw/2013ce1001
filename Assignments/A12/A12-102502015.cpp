#include <sstream>
#include <iostream>
using namespace std;
int main()
{
    char str[]="jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way  Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";
    string c[100];              //存全部
    string same[100];           //存不重複
    stringstream str2(str);     //分割字串
    int counter=0;              //總共單字數目-1
    int number=0;               //不重複單字數目-1
    int times[100]= {0};        //各單字出現次數
    while(str2>>c[counter])     //把分割單字存入字串陣列
    {
        counter++;              //計算總次數
    }
    counter--;                  //最後一個++減掉
    for(int j=0; j<=counter; j++)//從第一個開始數 數到最後一個
    {
        int tmp=0;                      //判斷
        for(int k=0; k<number; k++)     //如果沒有重複則tmp=0
        {
            if(same[k]==c[j])
            {
                tmp++;
            }
        }
        if(tmp==0)                      //沒有重複就進行計算
        {
            for(int i=j; i<=counter; i++)//從第j個開始數到最後一個(因為前面算過的不會出現 所以不用從第一個開始數)
            {
                if(c[j]==c[i])           //每有一個一樣的
                {
                    same[number]=c[j];   //把未重複的字串存入same字串
                    times[number]++;     //就+1次
                }
            }
            number++;                    //移到下一個
        }
    }
    for (int j=0; j<number; j++)       //印出
    {
        cout<<same[j]<<": "<<times[j]<<endl;
    }
    return 0;
}
