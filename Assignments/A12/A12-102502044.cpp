/*************************************************************************
    > File Name: A12-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年12月20日 (週五) 13時23分02秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>

int main()
{
	/* init the song */
	char song[] = "jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh jingle bells jingle bells jingle all the way Oh what fun it is to ride In a one horse open sleigh Dashing through the snow In a one horse open sleigh Oer the fields we go Laughing all the way Bells on bob tails ring Making spirits bright What fun it is to laugh and sing A sleighing song tonight";

	/* str[][] used to save all string */
	/* times[] used to save times of string */
	/* top used to save how many string */
	char str[50][15];
	char times[50];
	int top = 0;

	/* init the times */
	memset(times, 0, sizeof(times));

	for(int i=0, j=0 ; ;)
	{
		/* if now is a end of vocabulary */
		if(song[i] == ' ' || song[i] == '\0')
		{
			/* add end of string for str[top] */
			str[top][j] = '\0';

			/* flag means if there is the same string in str[][] */
			int flag = 1;
			for(j = 0 ; j<top ; j++)
				/* if str[top] == str[j] */
				if(!strcmp(str[top], str[j]))
				{
					/* flag down, and times +1 */
					flag = 0;
					times[j]++;
					break;
				}

			/* if there is no same string */
			if(flag)
				/* new times +1 */
				times[top++]++;

			/* if there is end of string of song[] */
			if(song[i] == '\0')
				break;
			/* else, init i, j */
			else
			{
				j = 0;
				i++;
			}
		}
		/* if now is not a end of vocabulary */
		else
			/* copy it to the end of str[top] */
			str[top][j++] = song[i++];
	}

	/* output */
	for(int i=0 ; i<top ; i++)
		printf("%s: %d\n", str[i], times[i]);

	return 0;
}

