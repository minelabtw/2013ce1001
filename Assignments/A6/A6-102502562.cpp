#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

int main()
{
    int mode=0;                                     //宣告型別為整數的mode 並初始化其值為0
    double pi=3.14159,r=0,w=0,h=0;                  //宣告型別為浮點數的pi,r,w,h 並初始化其值為3.14159和0

    cout << "Calculate the area.\n";                //輸出"Calculate the area.\n"到螢幕上
    cout << "Choose the mode.\n";                   //輸出"Choose the mode.\n"到螢幕上
    cout << "Area of circle=1.\n";                  //輸出"Area of circle=1.\n"到螢幕上
    cout << "Area of triangle=2.\n";                //輸出"Area of triangle=2.\n"到螢幕上
    cout << "Area of rectangle=3.\n";               //輸出"Area of rectangle=3.\n"到螢幕上
    cout << "Exit=-1.\n";                           //輸出"Exit=-1.\n"到螢幕上

    while(mode!=-1)                                 //如果mode不等於-1就進入迴圈
    {
        cout << "Enter mode: ";                     //輸出"Enter mode: "到螢幕上
        cin >> mode;                                //輸入一個值給mode
        switch(mode)                                //判斷mode的值
        {
        case 1:                                     //判斷mode的值是否為1
            while(1)                                //詢問r的迴圈
            {
                cout << "Enter radius: ";           //輸出"Enter radius: "到螢幕上
                cin >> r;                           //輸入一個值給r
                if(r<=0)                            //判斷r的值是否<=0
                {
                    cout << "Out of range!\n";      //輸出"Out of range!\n"到螢幕上
                }
                else
                {
                    break;                          //跳出迴圈
                }
            }
            cout << "Area of circle: " << pi * pow(r,2) << endl;            //輸出"Area of circle: "和圓面積到螢幕上並換行
            break;                                  //跳出switch到最下面

        case 2:                                     //判斷mode的值是否為2
            while(1)                                //詢問w的迴圈
            {
                cout << "Enter width: ";            //輸出"Enter width: "到螢幕上
                cin >> w;                           //輸入一個值給w
                if(w<=0)                            //判斷w的值是否<=0
                {
                    cout << "Out of range!\n";      //輸出"Out of range!\n"到螢幕上
                }
                else
                {
                    break;                          //跳出迴圈
                }
            }
            while(1)                                //詢問h的迴圈
            {
                cout << "Enter height: ";           //輸出"Enter height: "到螢幕上
                cin >> h;                           //輸入一個值給h
                if(h<=0)                            //判斷h的值是否<=0
                {
                    cout << "Out of range!\n";      //輸出"Out of range!\n"到螢幕上
                }
                else
                {
                    break;                          //跳出迴圈
                }
            }
            cout << "Area of triangle: " << w * h / 2 << endl;               //輸出"Area of triangle: "和三角形面積到螢幕上並換行
            break;                                  //跳出switch到最下面

        case 3:                                     //判斷mode的值是否為3
            while(1)                                //詢問w的迴圈
            {
                cout << "Enter width: ";            //輸出"Enter width: "到螢幕上
                cin >> w;                           //輸入一個值給w
                if(w<=0)                            //判斷w的值是否<=0
                {
                    cout << "Out of range!\n";      //輸出"Out of range!\n"到螢幕上
                }
                else
                {
                    break;                          //跳出迴圈
                }
            }
            while(1)                                //詢問h的迴圈
            {
                cout << "Enter height: ";           //輸出"Enter height: "到螢幕上
                cin >> h;                           //輸入一個值給h
                if(h<=0)                            //判斷h的值是否<=0
                {
                    cout << "Out of range!\n";      //輸出"Out of range!\n"到螢幕上
                }
                else
                {
                    break;                          //跳出迴圈
                }
            }
            cout << "Area of rectangle: " << w * h << endl;                  //輸出"Area of rectangle: "和矩形面積到螢幕上並換行
            break;                                  //跳出switch到最下面

        case -1:                                    //判斷mode的值是否為-1
            break;                                  //跳出switch到最下面

        default:                                    //判斷mode是否為非上面的其他情形
            cout << "Illegal input!\n";             //輸出"Illegal input!\n"到螢幕上
            break;                                  //跳出switch到最下面
        }
    }

    return 0;
}
