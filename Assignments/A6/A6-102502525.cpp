#include<iostream>
using namespace std;
int main ()
{
    double p=3.14159;
    double r,width,height;
    double area;
    int mode;
    cout <<"Calculate the area."<<endl;
    cout <<"Choose the mode."<<endl;
    cout <<"Area of circle=1."<<endl;
    cout <<"Area of triangle=2."<<endl;
    cout <<"Area of rectangle=3."<<endl;
    cout <<"Exit=-1."<<endl;
    do
    {
        cout <<"Enter mode: ";
        cin >>mode;
        if (mode<-1 || mode==0 || mode>3)
        cout <<"Illegal input!"<<endl;
    }
    while (mode<-1 || mode==0 || mode>3);

    while (mode!=-1)
    {
        switch (mode)
        {
        case 1:
        do
        {
            cout <<"Enter radius: ";
            cin >>r;
            if (r<=0)
            cout <<"Out of range!"<<endl;
        }
        while (r<=0);
        area=p*r*r;
        cout <<"Area of circle: "<<area;
        break;
        case 2:
        do
        {
            cout <<"Enter width: ";
            cin >>width;
            if (width<=0)
            cout <<"Out of range!"<<endl;
        }
        while (width<=0);
        do
        {
            cout <<"Enter height: ";
            cin >>height;
            if (height<=0)
            cout <<"Out of range!"<<endl;
        }
        while (height<=0);
        area=(width*height)/2;
        cout <<"Area of triangle: "<<area;
        break;
        case 3:
        do
        {
            cout <<"Enter width: ";
            cin >>width;
            if (width<=0)
            cout <<"Out of range!"<<endl;
        }
        while (width<=0);
        do
        {
            cout <<"Enter height: ";
            cin >>height;
            if (height<=0)
            cout <<"Out of range!"<<endl;
        }
        while (height<=0);
        area=(width*height);
        cout <<"Area of rectangle: "<<area;
        break;
    }
    do
    {
        cout <<"\nEnter mode: ";
        cin >>mode;
        if (mode<-1 || mode==0 || mode>3)
        cout <<"Illegal input!"<<endl;
    }
    while (mode<-1 || mode==0 || mode>3);
}
return 0;
}
