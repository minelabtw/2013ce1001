#include <iostream>
using namespace std;
void checkw (double &nub1)//檢驗寬之函式(不主動回傳)
{
    while (nub1<=0)//當不符合條件
    {
        cout << "Out of range!" << endl << "Enter width: ";//提示不符合條件
        cin >> nub1;//重新輸入
    }
}
void checkh (double &nub2)//檢驗長之函式(不主動回傳)
{
    while (nub2<=0)//當不符合條件
    {
        cout << "Out of range!" << endl << "Enter height: ";//提示不符合條件
        cin >> nub2;//重新輸入
    }
}
int main ()//主函式
{
    int mode =0;//宣告變數並=0
    double radius =0;
    double width =0;
    double height =0;
    cout << "Calculate the area." << endl << "Choose the mode." << endl;//輸出題目要求
    cout << "Area of circle=1." << endl << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl << "Exit=-1." << endl;
    while (mode !=-1)//當符合繼續條件
    {
        cout << "Enter mode: ";//提示輸入
        cin >> mode;//輸入
        switch (mode)//選擇模式
        {
        case 1://模式1
            cout << "Enter radius: ";//提式輸入半徑
            cin >> radius;//輸入半徑
            while (radius<=0)//當不符合條件
            {
                cout << "Out of range!" << endl << "Enter radius: ";//提示不符合條件
                cin >> radius;//重新輸入
            }
            cout << "Area of circle: " << radius*radius*3.14159 << endl;//輸出結果
            break;//跳離
        case 2://模式2
            cout << "Enter width: ";//提式輸入寬
            cin >> width;//輸入寬
            checkw (width);//執行檢驗寬函式
            cout << "Enter height: ";//提式輸入長
            cin >> height;//輸入長
            checkh (height);//執行檢驗長函式
            cout << "Area of triangle: " << width*height/2 << endl;//輸出結果
            break;//跳離
        case 3://模式3
            cout << "Enter width: ";//提式輸入寬
            cin >> width;//輸入寬
            checkw (width);//執行檢驗寬函式
            cout << "Enter height: ";//提式輸入長
            cin >> height;//輸入長
            checkh (height);//執行檢驗長函式
            cout << "Area of rectangle: " << width*height << endl;//輸出結果
            break;//跳離
        default ://其他
            if (mode !=-1)//不符合條件時
                cout << "Illegal input!" << endl;//提示不符合條件
            break;//跳離
        }
    }
    return 0;
}
