#include <iostream>
using namespace std;

int main()
{
    int mode;                                                       //定義一些要用之數字
    double radius;
    double width;
    double height;
    double cirArea;
    double triArea;
    double recArea;
    cout<<"Calculate the area.\n";                                  //輸出
    cout<<"Choose the mode.\n";
    cout<<"Area of circle=1.\n";
    cout<<"Area of triangle=2.\n";
    cout<<"Area of rectangle=3.\n";
    cout<<"Exit=-1.\n";

    while (mode!=-1)                                                //若mode!=-1,持續使用switch
    {
        cout<<"Enter mode: ";
        cin>>mode;
        switch (mode)
        {
        case 1:                                                     //case1 算圓面積
            cout<<"Enter radius: ";                                 //輸出
            cin>>radius;                                            //輸入
            while (radius <= 0)                                     //檢驗
            {
                cout<<"Out of range!\n";
                cout<<"Enter radius: ";
                cin>>radius;
            }
            cirArea = radius * radius * 3.14159;                    //圓面積
            cout<<"The area of circle is "<<cirArea<<endl;          //輸出
            break;                                                  //結束switch
        case 2:                                                     //case2 算三角形面積
            cout<<"Enter width: ";                                  //輸出
            cin>>width;                                             //輸入
            while (width <= 0)                                      //檢驗
            {
                cout<<"Out of range!\n";
                cout<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";                                 //輸出
            cin>>height;                                            //輸入
            while (height <= 0)                                     //檢驗
            {
                cout<<"Out of range!\n";
                cout<<"Enter height: ";
                cin>>height;
            }
            triArea = width * height * 0.5;                         //三角形面積
            cout<<"The area of triangle is "<<triArea<<endl;        //輸出
            break;                                                  //結束switch
        case 3:                                                     //case3 算長方形面積
            cout<<"Enter width: ";                                  //輸出
            cin>>width;                                             //輸入
            while (width <= 0)                                      //檢驗
            {
                cout<<"Out of range!\n";
                cout<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";                                 //輸出
            cin>>height;                                            //輸入
            while (height <= 0)                                     //檢驗
            {
                cout<<"Out of range!\n";
                cout<<"Enter height: ";
                cin>>height;
            }
            recArea = width * height;                               //長方形面積
            cout<<"The area of rectangle is "<<recArea<<endl;       //輸出
            break;                                                  //結束switch

        case -1:                                                    //case4 結束程式
            break;                                                  //結束switch
        default:
            cout<<"Illegal input!\n";                               //輸出
            break;                                                  //結束switch
        }
    }
    return 0;
}
