#include <iostream>
using namespace std;

int main()
{
    int number,i; // initialize integer number and i
    cout << "Calculate the area.\n"
         << "Choose the mode.\n"
         << "Area of circle=1.\n"
         << "Area of triangle=2.\n"
         << "Area of rectangle=3.\n"
         << "Exit=-1.\n"
         << "Enter mode: ";
    cin >> number;

    while (i) // while loop to execute switch statement
    {

        switch (number) // switch statement to execute the corresponding case of number
        {
        case 1: // if number is 1
            double radius,area; // initialize double radius and area
            cout << "radius: ";
            cin >> radius;

            while (radius<=0) // while loop to check whether radius is on demand
            {
                cout << "Out of range!\n"
                     << "Enter radius: ";
                cin >> radius;
            }

            area = radius*radius*3.14159; // circle area function
            cout << "Area of circle: " << area << endl;
            cout << "Enter mode: ";
            cin >> number;
            break;

        case 2: // if number is 2
            double width,height,area1; // initialize double width, height, and area1
            cout << "Enter width: ";
            cin >> width;

            while (width<=0) // while loop to check whether width is on demand
            {
                cout << "Out of range!\n"
                     << "Enter width: ";
                cin >> width;
            }

            cout << "Enter height: ";
            cin >> height;

            while (height<=0) // while loop to check whether height is in demand
            {
                cout << "Out of range!\n"
                     << "Enter height: ";
                cin >> height;
            }

            area1 = width*height*0.5; // the function of triangle area
            cout << "Area of triangle: " << area1 << endl;
            cout << "Enter mode: ";
            cin >> number;
            break;

        case 3: // if number is 3
            double width1,height1,area2; // initialize double width1, height1, and area2
            cout << "Enter width: ";
            cin >> width1;

            while (width1<=0) // while loop to check whether width1 is on demand
            {
                cout << "Out of range!\n"
                     << "Enter width: ";
                cin >> width1;
            }

            cout << "Enter height: ";
            cin >> height1;

            while (height1<=0) // while loop to check whether height1 is on demand
            {
                cout << "Out of range!\n"
                     << "Enter height: ";
                cin >> height1;
            }

            area2 = width1 * height1; // function of rectangle area
            cout << "area of rectangle: " << area2 << endl;
            cout << "Enter mode: ";
            cin >> number;
            break;

        case -1: // if number is -1
            break;

        default: // if number is the others
            cout << "Illegal input!\n";
            cout << "Enter mode: ";
            cin >> number;
            break;
        }
    }

    cout << "Enter mode: ";
    cin >> number;
    return 0;
}
