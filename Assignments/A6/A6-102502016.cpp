#include <iostream>

using namespace std;

int main()
{
    int a=0;//a模式,b寬度,c高度,r半徑
    float b;
    float c;
    float r;

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

    while(a != -1)
    {
        cout<<"Enter mode: ";
        cin >>a;
        switch (a)
        {
        case 2://三角形面積
            cout<<"Enter width: ";
            cin >>b;
            while (b<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>b;
            }
            cout<<"Enter height: ";
            cin>>c;
            while (c<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>c;
            }
            cout<<"Area of triangle: "<<(b*c)/2<<endl;

            break;
        case 1://圓形面積
            cout<<"Enter radius: ";
            cin>>r;
            while (r<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>r;
            }
            {
                double Circle = r*r*(3.14159);
                cout<<"Area of circle: "<<Circle<<endl;
            }
            break;
        case 3://矩形面積
            cout<<"Enter width: ";
            cin >>b;
            while (b<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>b;
            }
            cout<<"Enter height: ";
            cin>>c;
            while (c<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>c;
            }
            cout<<"Area of rectangle:"<<b*c<<endl;
            break;
        case -1://退出
            break;

        default:
            cout<<"Illegal input!"<<endl;
            break;
        }
    }
    return 0;
}
