#include <iostream>
using namespace std;
int main()
{
    int mode = 0; //宣告變數其初始值為0
    double radius = 0;
    double width = 0;
    double height = 0;

    cout << "Calculate the area." << endl << "Choose the mode." << endl << "Area of circle=1." << endl << "Area of triangle=2." << endl << "Area of rectangle=3." << endl << "Exit=-1." << endl; //顯示

    cout << "Enter mode: "; //顯示Enter mode:
    cin >> mode; //輸入值給mode

    while ( mode != -1 )  //當mode不等於-1時進入以下迴圈
    {
        switch (mode)
        {
        case 1 : //計算圓面積
            cout << "Enter radius: ";
            cin >> radius;
            while ( radius <= 0 )
            {
                cout << "Out of range!" << endl << "Enter radius: ";
                cin >> radius;
            }
            cout << "Area of circle: " << radius*radius*3.14159 << endl << "Enter mode: ";
            cin >> mode;
            break; //跳出
        case 2 : //計算三角形面積
            cout << "Enter width: ";
            cin >> width;
            while ( width <= 0 )
            {
                cout << "Out of range!" << endl << "Enter width: ";
                cin >> width;
            }

            cout << "Enter height: ";
            cin >> height;
            while ( height <= 0 )
            {
                cout << "Out of range!" << endl << "Enter height: ";
                cin >> height;
            }

            cout << "Area of triangle: " << (width*height)*1/2;
            cout << endl << "Enter mode: ";
            cin >> mode;
            break; //跳出
        case 3 : //計算矩形面積
            cout << "Enter width: ";
            cin >> width;
            while ( width <= 0 )
            {
                cout << "Out of range!" << endl << "Enter width: ";
                cin >> width;
            }

            cout << "Enter height: ";
            cin >> height;
            while ( height <= 0 )
            {
                cout << "Out of range!" << endl << "Enter height: ";
                cin >> height;
            }
            cout << "Area of rectangle: " << width*height;
            cout << endl << "Enter mode: ";
            cin >> mode;
            break; //跳出
        case -1 :
            return 0; //結束

        default : //輸入其他
            cout << "Illegal input!" << endl << "Enter mode: "; //顯示
            cin >> mode;
        }
    }
    return 0;
}
