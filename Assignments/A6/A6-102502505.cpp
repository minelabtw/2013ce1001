#include <iostream>
#include <iomanip>

using namespace std;

double r;//半徑，並使之可為小數
double w;//寬度
double h;//高度
int mode;//模式，1,2,3,-1

int main()
{
    cout << "Calculate the area." << endl;//輸出字串
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;

    cout << "Enter mode: ";//輸出字串
    cin >> mode;//輸入模式變數

    while ( mode !=-1 )//設迴圈，使模式在等於-1以外皆不結束
    {
        while ( mode!=1 && mode!=2 && mode!=3 && mode!=-1 )//設迴圈，使模式條件符合
        {
            cout << "Illegal input!" << endl;//輸出字串
            cout << "Enter mode: ";
            cin >> mode;//重新輸入模式
        }

        switch (mode)//設定多重選擇
        {
        case 1://當模式=1時，輸出下列字串
            cout << "Enter radius: ";
            cin >> r;
            while ( r <= 0 )//設迴圈使辯輸符合條件
            {
                cout << "Out of range!" << endl;
                cout << "Enter radius: ";
                cin >> r;
            }
            cout << "Area of circle: " << r*r*3.14159;//輸出字串，並計算圓面積
            break;//跳出多重選擇
        case 2://當模式=2時，輸出下列字串
            cout << "Enter width: ";
            cin >> w;//輸入寬度
            while ( w <= 0 )
            {
                cout << "Out of range!" <<endl;
                cout << "Enter width: ";
                cin >> w;
            }

            cout << "Enter height: ";
            cin >> h;//輸入高度
            while ( h <= 0)
            {
                cout << "Out of range!" <<endl;
                cout << "Enter height: ";
                cin >> h;
            }
            cout << "Area of triangle: " << w*h*0.5;//輸入三角形面積
            break;
        case 3://模式為3時輸出下列字串
            cout << "Enter width: ";
            cin >> w;
            while ( w <= 0)
            {
                cout << "Out of range!" <<endl;
                cout << "Enter width: ";
                cin >> w;
            }
            cout << "Enter height: ";
            cin >> h;
            while ( h <= 0)
            {
                cout << "Out of range!" <<endl;
                cout << "Enter height: ";
                cin >> h;
            }
            cout << "Area of rectangle: " << w*h;//輸出長方形面積
            break;
        case -1://當模式為-1時，跳出switch
            break;
        }
        cout << endl << "Enter mode: ";//結束一次計算後，再次跑出此字串，使程式可重複運算，直至模式=-1
        cin >> mode;
    }

    return 0;//返回初始值

}
