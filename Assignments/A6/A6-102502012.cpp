#include<iostream>
#include<math.h>
#define PI 3.14159 // 2*acos(0.0)
using namespace std;
int main()
{
    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

    int mode;
    double R,L,W;
    while( true )
    {
        do
        {
            cout<<"Enter mode: ";
            cin>>mode;
        }
        while( !(mode==1||mode==2||mode==3||mode==-1)&&cout<<"Illegal input!"<<endl );
        switch( mode )
        {
        case 1:
            do
            {
                cout<<"Enter radius: ";
                cin>>R;
            }
            while( R<=0&&cout<<"Out of range!"<<endl );
            cout<<"Area of circle: "<<R*R*PI<<endl; // circle area=r*r*pi
            break;
        case 2:
            do
            {
                cout<<"Enter width: ";
                cin>>W;
            }
            while( W<=0&&cout<<"Out of range!"<<endl );
            do
            {
                cout<<"Enter height: ";
                cin>>L;
            }
            while( L<=0&&cout<<"Out of range!"<<endl );
            cout<<"Area of triangle: "<<W*L/2.0<<endl; // triangle area=W*L/2
            break;
        case 3:
            do
            {
                cout<<"Enter width: ";
                cin>>W;
            }
            while( W<=0&&cout<<"Out of range!"<<endl );
            do
            {
                cout<<"Enter height: ";
                cin>>L;
            }
            while( L<=0&&cout<<"Out of range!"<<endl );
            cout<<"Area of rectangle: "<<W*L<<endl; // rectangle area=W*L
        }
        if( mode==-1 )
            break;
    }
    return 0;
}
