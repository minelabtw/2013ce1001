#include<iostream>
using namespace std;
double Pi=3.14159;
double radius=0;
double width=0;
double height=0;

void radius0() // intput radius
{
    cout<<"Enter radius: ";
    cin>>radius;
    while(radius<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter radius: ";
        cin>>radius;
    }
}

void width0() // input width
{
    cout<<"Enter width: ";
    cin>>width;
    while(width<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter width: ";
        cin>>width;
    }
}

void height0() // input height
{
    cout<<"Enter height: ";
    cin>>height;
    while(height<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter height: ";
        cin>>height;
    }
}

int main()
{
    int mode=0;

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

    cout<<"Enter mode: "; // intput mode
    cin>>mode;
    while(mode!=-1 && mode!=1 && mode!=2 && mode!=3)
    {
        cout<<"Illegal input!"<<endl;
        cout<<"Enter mode: ";
        cin>>mode;
    }

    while(mode!=-1)
    {
        switch(mode)
        {
        case 1:
            radius0(); // call out the function radius0
            cout<<"Area of circle: "<<radius*radius*Pi<<endl;
            break;
        case 2:
            width0(); // call out the function
            height0();
            cout<<"Area of triangle: "<<width*height/2<<endl;
            break;
        case 3:
            width0(); // call out the function
            height0();
            cout<<"Area of rectangle: "<<width*height<<endl;
            break;
        default:
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>mode;
        }

        cout<<"Enter mode: "; // intput mode until mode=-1
        cin>>mode;
        while(mode!=-1 and mode!=1 and mode!=2 and mode!=3)
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>mode;
        }
    }
    return 0;
}
