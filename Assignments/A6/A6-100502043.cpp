#include<iostream>

using namespace std;

int main()
{
    int mode=0;

    cout << "Calculate the area." << "\nChoose the mode.";
    cout << "\nArea of circle=1." << "\nArea of triangle=2." << "\nArea of rectangle=3." << "\nExit=-1." << endl;

    while (mode!=-1)  //模式為-1時結束迴圈
    {
        cout << "Enter mode: ";
        cin >> mode;

        switch (mode)  //選擇模式
        {
        case 1:
            {
                double radius=0;

                while (radius<=0)
                {
                    cout << "Enter radius: ";
                    cin >> radius;
                    if (radius<=0)
                        cout << "Out of range!";
                }
                cout << "Area of circle: " << 3.14159*radius*radius << endl;  //輸出圓面積
            }
            break;

        case 2:
            {
                double width=0,height=0;

                while (width<=0)
                {
                    cout << "Enter width: ";
                    cin >> width;
                    if (width<=0)
                        cout << "Out of range!";
                }
                while (height<=0)
                {
                    cout << "Enter height: ";
                    cin >> height;
                    if (height<=0)
                        cout << "Out of range!";
                }
                cout << "Area of triangle: " << width*height/2 << endl;  //輸出三角形面積
            }
            break;

        case 3:
            {
                double width=0,height=0;

                while (width<=0)
                {
                    cout << "Enter width: ";
                    cin >> width;
                    if (width<=0)
                        cout << "Out of range!";
                }
                while (height<=0)
                {
                    cout << "Enter height: ";
                    cin >> height;
                    if (height<=0)
                        cout << "Out of range!";
                }
                cout << "Area of rectangle: " << height*width << endl;  //輸出矩形面積
            }
            break;

        case -1:
            break;

        default:
            cout << "Illegal input!" << endl;
            break;
        }
    }

    return 0;
}
