#include<iostream>

using namespace std ;

int main ()
{
    int number=0 ;

    double r=0, w=0, h=0 ;//宣告變數

    cout << "Calculate the area.\n" << "Choose  the mode.\n" << "Area of circle=1.\n" ;

    cout << "Area of triangle=2.\n" << "Area of rectangle=3.\n" << "Exit=-1.\n" ;

    while (true)//迴圈
    {
        cout << "Enter mode: " ;

        cin >> number ;

        switch (number)//使用switch，判斷路徑
        {
        case 1 :


            while (true)
            {
                cout << "Enter radius: " ;

                cin >> r ;

                if (r<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

           cout << "Area of circle: " << 3.14159*r*r << endl ;

            break ;

        case 2 :

            while (true)
            {
                cout << "Enter width: " ;

                cin >> w ;

                if (w<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            while (true)
            {
                cout << "Enter height: " ;

                cin >> h ;

                if (h<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            cout << "Area of triangle: " << w*h/2 << endl ;

            break ;

        case 3 :


            while (true)
            {
                cout << "Enter width: " ;

                cin >> w ;

                if (w<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            while (true)
            {
                cout << "Enter height: " ;

                cin >> h ;

                if (h<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            cout << "Area of rectangle: " << w*h << endl ;

            break ;

        case -1 :
            break ;

        default :
            cout << "Illegal input!\n" ;
        }

        if (number==-1)
        {
            break ;//跳出迴圈
        }
    }
    return 0 ;
}
