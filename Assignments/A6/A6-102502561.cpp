#include<iostream>
using namespace std;

int main()
{
    int mode;
    double r; //radius
    double w; //width
    double h; //height

    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\nEnter mode: ";        //question:
    cin >> mode;
    while(mode!=-1)                         //while mode is not -1 the following repeats
    {
        if(mode<=0||mode>3)
        {
            if(mode==-1)                    //mode-1:exit
            {
                break;
            }
            cout << "Illegal input!\nEnter mode: ";
            cin >> mode;
        }

        if(mode==1)                         //mode1:area of circle
        {
            cout << "Enter radius: ";
            cin >>  r;
            while(r<=0)
            {
                cout << "Out of range!\nEnter radius: \n";
                cin >> r;
            }
            cout << "Area of circle: " << r*r*3.14159 << "\nEnter mode: ";
            cin >> mode;
        }

        if(mode==2)                         //mode2:area of triangle
        {
            cout << "Enter width: ";
            cin >> w;
            while(w<=0)
            {
                cout << "Out of range!\nEnter width: ";
                cin >> w;
            }
            cout << "Enter height: ";
            cin >> h;
            while(h<=0)
            {
                cout << "Out of range!\nEnter height: ";
                cin >> h;
            }
            cout << "Area of triangle: " << (w * h)/2 << "\nEnter mode: ";
            cin >> mode;
        }

        if(mode==3)                         //mode3:area of rectangle
        {
            cout << "Enter width: ";
            cin >> w;
            while(w<=0)
            {
                cout << "Out of range!\nEnter width: ";
                cin >> w;
            }
            cout << "Enter height: ";
            cin >> h;
            while(h<=0)
            {
                cout << "Out of range!\nEnter height: ";
                cin >> h;
            }
            cout << "Area of rectangle: " << w * h << "\nEnter mode: ";
            cin >> mode;
        }
    }
    return 0;
}
