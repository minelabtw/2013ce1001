#include<iostream>
#include <math.h> //可使用指令pow
using namespace std;
int main()
{
    int a;
    float r,x,y;
    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";
    cout << "Enter mode:\ ";
    cin >> a;
    while(a!=-1) //a=-1時跳出while迴圈
    {
        switch(a)
        {
        case 1: //a=1時執行
            cout << "Enter radius:\ ";
            cin >> r;
            while(r<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter radius:\ ";
                cin >> r;
            }
            cout << "Area of circle:\ " << pow(r,2)*3.14159 << "\n"; //pow(r,2)為r平方
            break; //跳出switch
        case 2: //a=2時執行
            cout << "Enter width:\ ";
            cin >> x;
            while (x<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter width:\ ";
                cin >> x;
            }
            cout << "Enter height:\ ";
            cin >> y;
            while (y<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter height:\ ";
                cin >> y;
            }
            cout << "Area of triangle:\ " << x*y/2 << "\n";
            break; //跳出switch
        case 3: //a=3時執行
            cout << "Enter width:\ ";
            cin >> x;
            while (x<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter width:\ ";
                cin >> x;
            }
            cout << "Enter height:\ ";
            cin >> y;
            while (y<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter height:\ ";
                cin >> y;
            }
            cout << "Area of rectangle:\ " << x*y << "\n" ;
            break; //跳出switch
        default: //其他情況
            cout << "Illegal input!\n";
            break; //跳出switch
        }
        cout << "Enter mode:\ ";
        cin >> a ;
    }
    return 0;
}
