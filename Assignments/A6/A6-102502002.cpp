#include <iostream>
using namespace std;

int main()
{
    int mode=0;                  //mode
    double rad=1;                //radius
    double twid=1;               //width of triangle
    double thit=1;               //height of triangle
    double rwid=1;               //width of rectangle
    double rhit=1;               //height of rectangle


    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\nEnter mode: ";          //output a string
    cin >> mode;                 //inout mode

    while(mode!=-1)               //run while loop when mode doesn't equal to -1, that is, the input -1 will end the program
    {
        switch(mode)
        {
        case 1:                                       //find the area of a circle
            cout << "Enter radius: ";                 //enter the radius>0
            cin >> rad;
            for(double R=rad; R<=0;)
            {
                cout <<  "Out of range!\n" << "Enter radius: ";
                cin >> rad;
            }
            cout << "Area of circle: " << 3.14159*rad*rad << endl;      //calculate the area = 3.14159*radius*radius
            break;                                    //end switch

        case 2:                                       //find the area of a triangle
            cout << "Enter width: ";                  //enter the width>0
            cin >> twid;
            for(double W=twid; W<=0;)
            {
                cout <<  "Out of range!\n" << "Enter width: ";
                cin >> twid;
            }
            cout << "Enter height: ";                 //enter the height>0
            cin >> thit;
            for(double H=thit; H<=0;)
            {
                cout <<  "Out of range!\n" << "Enter height: ";
                cin >> thit;
            }
            cout << "Area of triangle: " << thit*twid*0.5 << endl;       //calculate the area = (height*width)/2
            break;                                    //end switch

        case 3:                                       //find the area of a rectangle
            cout << "Enter width: ";                  //enter the width>0
            cin >> rwid;
            for(double W=rwid; W<=0;)
            {
                cout <<  "Out of range!\n" << "Enter width: ";
                cin >> rwid;
            }
            cout << "Enter height: ";                 //enter the height>0
            cin >> rhit;
            for(double H=rhit; H<=0;)
            {
                cout <<  "Out of range!\n" << "Enter height: ";
                cin >> rhit;
            }
            cout << "Area of rectangle: " << rwid*rhit << endl;            //calculate the area = height*width
            break;                                    //end switch

        default:                                      //other inputs rather than 1,2,3,-1 ,output "Out of range"
            cout << "Out of range!\n";
            break;                                    //end switch
        }
        cout << "Enter mode: ";                       //enter mode again after ending switch
        cin >> mode;
    }
    return 0;
}
