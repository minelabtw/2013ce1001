#include <iostream>
using namespace std;
double circlearea (double) ;//宣告函式
double trianglearea (double, double ) ;//宣告函式
double rectanglearea (double , double );//宣告函式
int main ()

{
    int mode  ; //宣告變數
    double radius,x,width , height ;
    cout << "Calculate the area." << endl ;//輸出題目所需
    cout <<"Choose the mode." <<endl ;
    cout <<"Area of circle=1." << endl ;
    cout << "Area of triangle=2." << endl ;
    cout << "Area of rectangle=3." <<endl ;
    cout << "Exit=-1." << endl;
    while ( mode != -1 )//當mode !=1時執行以下
    {
    cout << "Enter mode: " ;//輸出enter mode
    cin >> mode ;//輸入mode
    switch ( mode )//選擇模式
    {
    case 1 ://第一種模式
    {
        cout <<"Enter radius: ";//輸出Enter mode
        cin >> radius ;//輸入半徑
        while ( radius <=0)//當半徑小於等於0時
        {
            cout << "Illegal input!" << endl ;//輸出Illeagal input !
            cout << "Enter radius: " ;//輸出Enter radius
            cin >> radius ;//輸入radius
        }
        cout << "Area of circle: " <<circlearea( radius )<<endl  ;//輸出圓面積 (函式)
        break;//離開case
    }
    case 2 ://第二種模式
    {
        cout << "Enter width: " ;//輸出輸入寬
        cin >> width  ;//輸入寬
        while (width <=0)//當寬<=0時 執行以下
        {
            cout << "Illegal input!" <<endl ;//輸出Illegal input
            cout << "Enter width: " ;//輸出Enter width
            cin >> width ;//輸入寬

        }
        cout << "Enter height: ";//輸出enter height
        cin >> height ;//輸入高
        while (height<=0 )//當高<=0時 執行以下
        {
            cout << "Illegal input!" <<endl ;//輸出Illeagal input
            cout << "Enter height: " ;//輸出Enter height
            cin >> height ;//輸入height
        }
        cout << "Area of triangle: " <<trianglearea(height , width) << endl;//輸出三角形面積 (函式)
        break;//離開第二種模式
    }
    case 3 ://第三種模式
    {
        cout <<"Enter width: " ;//輸出Enter width
        cin >> width ; //輸入寬
        while ( width <=0 )//當width <=0時 執行以下
        {
            cout << "Illeagal input!" << endl ;//輸出Illeagal input!
            cout << "Enter width: " ; //輸出Enter width
            cin >> width ;//輸入width
        }
        cout << "Enter height: ";//輸出Enter height
        cin >> height ;//輸入height
        while (height<=0 )//當height小於等於0時 執行以下
        {
            cout << "Illegal input!" <<endl ;//輸出Illegal input!
            cout << "Enter height: " ;//輸出Enter height
            cin >> height ;//輸入height
        }
        cout << "Area of rectangle: " << rectanglearea(height,width)<<endl;//輸出矩形面積
        break;//離開第三種模式
    }
    case -1 ://輸入-1時
        break;//離開
    default ://其他
        {
        cout << "Illegal input!" <<endl ;//輸出Illeagal input!
        break;
        }
    }
    }
}
double circlearea(double x)//圓面積函式
{
    return x*x*3.14159 ;//回傳值等於半徑乘以半徑乘上3.14159
}
double trianglearea (double y , double z)//三角形面積函式
{
    return (z*y)/2 ;//回傳值等於底乘高除以二
}
double rectanglearea (double a , double b)//矩形面積函式
{
    return a*b ;//回傳值等於底乘高
}
