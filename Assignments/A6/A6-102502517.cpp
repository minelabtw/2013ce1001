#include <iostream>

using namespace std;

int main()
{
    int mode = 0; //宣告變數
    double radius = 0; //宣告變數
    double width = 0; //宣告變數
    double height = 0; //宣告變數

    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;

    while (mode==0)
    {
        cout << "Enter mode: ";
        cin >> mode;  //輸入模式

        switch (mode)  //使用switch迴圈
        {
        case 1:
            while (radius<=0)
            {
                cout << "Enter radius: ";
                cin >> radius;
                if (radius<=0)
                    cout << "Out of range!" << endl;
            }

            cout << "Area of circle: " << radius*radius*3.14159 << endl;
            mode = 0, radius = 0, width = 0, height = 0; //初始化變數
            break;

        case 2:
            while (width<=0)
            {
                cout << "Enter width: ";
                cin >> width;
                if (width<=0)
                    cout << "Out of range!" << endl;
            }

            while (height<=0)
            {
                cout << "Enter height: ";
                cin >> height;
                if (height<=0)
                    cout << "Out of range!" << endl;
            }

            cout << "Area of triangle: " << width*height/2 << endl;
            mode = 0, radius = 0, width = 0, height = 0; //初始化變數
            break;

        case 3:
            while (width<=0)
            {
                cout << "Enter width: ";
                cin >> width;
                if (width<=0)
                    cout << "Out of range!" << endl;
            }

            while (height<=0)
            {
                cout << "Enter height: ";
                cin >> height;
                if (height<=0)
                    cout << "Out of range!" << endl;
            }

            cout << "Area of rectangle: " << width*height << endl;
            mode = 0, radius = 0, width = 0, height = 0; //初始化變數
            break;

        default:  //輸入其他值時跳回迴圈
            cout << "Illegal input!" << endl;
            mode = 0;
            break;

        case -1:  //結束迴圈
            break;
        }
    }

    return 0;
}
