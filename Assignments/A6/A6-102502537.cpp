#include<iostream>
using namespace std;
double pi=3.14159;
double radius=0;
double width=0;
double height=0;

void radius1() // 輸入半徑
{
    cout<<"Enter radius: ";
    cin>>radius;
    while(radius<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter radius: ";
        cin>>radius;
    }
}

void width1() // 輸入寬度
{
    cout<<"Enter width: ";
    cin>>width;
    while(width<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter width: ";
        cin>>width;
    }
}

void height1() // 輸入高度
{
    cout<<"Enter height: ";
    cin>>height;
    while(height<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter height: ";
        cin>>height;
    }
}

int main()
{
    int mode=0;

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

    cout<<"Enter mode: "; // 輸入 mode
    cin>>mode;
    while(mode!=-1 && mode!=1 && mode!=2 && mode!=3)
    {
        cout<<"Illegal input!"<<endl;
        cout<<"Enter mode: ";
        cin>>mode;
    }

    while(mode!=-1)
    {
        switch(mode)
        {
        case 1:
            radius1(); // 叫出函式radius1
            cout<<"Area of circle: "<<radius*radius*pi<<endl;
            break;
        case 2:
            width1(); // 叫出函式width1
            height1();
            cout<<"Area of triangle: "<<width*height/2<<endl;
            break;
        case 3:
            width1(); // 叫出函式width1
            height1();
            cout<<"Area of rectangle: "<<width*height<<endl;
            break;
        default:
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>mode;
        }

        cout<<"Enter mode: "; // 輸入 mode 直到 mode=-1
        cin>>mode;
        while(mode!=-1 and mode!=1 and mode!=2 and mode!=3)
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>mode;
        }
    }
    return 0;
}
