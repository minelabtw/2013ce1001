#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    cout << "Calculate the area." << endl;  //輸出輸入規則
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;

    int mode;  //設定變數
    double r;
    double pi = 3.14159;
    double width;
    double height;
    double aoc;
    double aot;
    double aor;

    cout << "Enter mode: ";
    cin >> mode;
    while ( mode != -1 && mode != 1 && mode != 2 && mode != 3 )  //若輸入mode不等於-1,1,2,3執行下列迴圈
    {
        cout << "Illegal input!" << endl;
        cout << "Enter mode: ";
        cin >> mode;
    }

    while ( mode != -1 )
    {
        switch ( mode )  //判斷mode數值然後對照執行下列case
        {
        case 1:  //輸入一個半徑數值(大於0)算出園面積
            cout << "Enter radius: ";
            cin >> r;
            while ( r <= 0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter radius: ";
                cin >> r;
            }
            aoc = r * r * pi;
            cout << "Area of circle:" << aoc << endl;

            break;
        case 2:  //輸入一個底一個高算出三角形面積
            cout << "Enter width: ";
            cin >> width;
            while ( width <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> width;
            }
            cout << "Enter height: ";
            cin >> height;
            while ( height <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> height;
            }
            aot = (width * height) / 2;
            cout << "Area of triangle: " << aot << endl;

            break;
        case 3:  //給一個長跟寬算出長方形面積
            cout << "Enter width: ";
            cin >> width;
            while ( width <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> width;
            }
            cout << "Enter height: ";
            cin >> height;
            while ( height <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> height;
            }
            aor = width * height;
            cout << "Area of rectangle: " << aor << endl;

            break;
        default:  //若輸入其他數值執行下列
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> mode;

            break;
        }
        cout << "Enter mode: ";
        cin >> mode;
        while ( mode != -1 && mode != 1 && mode != 2 && mode != 3 )
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> mode;
        }
    }
    return 0;
}
