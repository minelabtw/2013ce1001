#include <iostream>
#include <math.h>
using namespace std;
int main()
{
    int mode=0;
    double radius=0;  //宣告double型態的半徑
    double areac=0;  //圓面積
    double width1=0;  //宣告double型態的寬
    double height1=0;  //宣告double型態的長
    double areat=0;  //三角形面積
    double width2=0;
    double height2=0;
    double arear=0;  //矩形面積

    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;

    while (mode!=-1)  //若mode不為1則進行下列程式,否則停止
    {
        cout << "Enter mode: ";
        cin >> mode;

        switch (mode)
        {
        case 1:
            cout << "Enter radius: ";
            cin >> radius;  //輸入半徑
            while (radius<=0)
            {
                cout << "Out of range!" << endl << "Enter radius: ";
                cin >> radius;
            }
            areac=pow(radius,2)*3.14159;  //計算圓面積
            cout << "Area of circle: " << areac << endl;
            break;

        case 2:
            cout << "Enter width: ";
            cin >> width1;  //輸入寬
            while (width1<=0)
            {
                cout << "Out of range!" << endl << "Enter width: ";
                cin >> width1;
            }
            cout << "Enter height: ";
            cin >> height1;  //輸入長
            while (height1<=0)
            {
                cout << "Out of range!" << endl << "Enter height: ";
                cin >> height1;
            }
            areat=width1*height1*0.5;  //計算三角形面積
            cout << "Area of triangle: " << areat << endl;
            break;

        case 3:
            cout << "Enter width: ";
            cin >> width2;  //輸入寬
            while (width2<=0)
            {
                cout << "Out of range!" << endl << "Enter width: ";
                cin >> width2;
            }
            cout << "Enter height: ";
            cin >> height2;  //輸入長
            while (height2<=0)
            {
                cout << "Out of range!" << endl << "Enter height: ";
                cin >> height2;
            }
            arear=width2*height2;  //計算矩形面積
            cout << "Area of rectangle: " << arear << endl;
            break;

        case -1:
            break;

        default:  //若mode不為1,2,3,-1則顯示輸入錯誤字串
            cout << "Illegal input!" << endl;
            break;
        }
            }
            return 0;
}
