#include <iostream>
using namespace std;

main()
{
    int a=0;
    double x=0;
    double y=0;
    double pi=3.14159;

    cout << "Calculate the area.\nChoose the mode.\n";
    cout << "Area of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";
    do
    {
        cout << "Enter mode: ";
        cin >> a;
        if(a!=-1) //如果a不等於-1時，執行
        {
            if(a==1) //如果a等於1，執行
            {
                do
                {
                    cout << "Enter radius: ";
                    cin >> x;
                    if(x<=0)
                        cout << "Out of range!\n";
                }
                while(x<=0); //x非正數時重新輸入

                cout << "Area of circle: " << x*x*pi << endl;
            }

            else if(a==2) //如果a等於2時，執行
            {
                do
                {
                    cout << "Enter width: ";
                    cin >> x;
                    if(x<=0)
                        cout << "Out of range!\n";
                }
                while(x<=0); //x非正數時重新輸入

                do
                {
                    cout << "Enter height: ";
                    cin >> y;
                    if(y<=0)
                        cout << "Out of range!\n";
                }
                while(y<=0); //y非正數時重新輸入

                cout << "Area of triangle: " << x*y/2 <<endl;
            }

            else if(a==3) //a等於3時，執行
            {
                do
                {
                    cout << "Enter width: ";
                    cin >> x;
                    if(x<=0)
                        cout << "Out of range!\n";
                }
                while(x<=0); //x非正數時重新輸入

                do
                {
                    cout << "Enter height: ";
                    cin >> y;
                    if(y<=0)
                        cout << "Out of range!\n";
                }
                while(y<=0); //y非正數時重新輸入

                cout << "Area of rectangle: " << x*y <<endl;
            }
            else //其他情況
                cout << "Illegal input!\n";
        }
    }
    while (a!=-1); //a不等於-1時執行迴圈

    return 0;
}
