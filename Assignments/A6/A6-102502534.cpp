#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int mode=0;//宣告變數
    double radius=0;//宣告變數
    double width=0;//宣告變數
    double height=0;//宣告變數
    cout<<"Calculate the area."<<endl<<"Choose the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl<<"Area of rectangle=3."<<endl<<"Exit=-1."<<endl;
    while(mode!=-1)
    {
        cout<<"Enter mode: ";
        cin>>mode;
        switch(mode)//選擇模式
        {
        case 1://輸入的模式為1時
            cout<<"Enter radius: ";
            cin>>radius;
            while(radius<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter radius: ";
                cin>>radius;
            }
            cout<<"Area of circle: "<<3.14159*pow(radius,2)<<endl;//計算圓面積
            break;
        case 2://輸入的模式為2時
            cout<<"Enter width: ";
            cin>>width;
            while(width<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";
            cin>>height;
            while(height<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>height;
            }
            cout<<"Area of triangle: "<<0.5*width*height<<endl;//計算三角形面積
            break;
        case 3://輸入的模式為3時
            cout<<"Enter width: ";
            cin>>width;
            while(width<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";
            cin>>height;
            while(height<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>height;
            }
            cout<<"Area of rectangle: "<<width*height<<endl;//計算矩形面積
            break;
        case -1://輸入的變數為-1時
            break;//結束計算
        default://輸入的變數不為1&2&3&-1時
            cout<<"Illegal input!"<<endl;
            break;
        }
    }
    return 0;
}
