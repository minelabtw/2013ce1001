#include <iostream>
#define pi 3.14159

double circle(double);//算圓面積函數
double triangle(double,double);//算三角形面積函數
double rectangle(double,double);//算長方形面積函數

using namespace std;

int main()
{
    int m;//模式
    double w;//寬
    double h;//高
    double r;//半徑

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

//開始
    while(true)
    {
        cout<<"Enter mode: ";
        cin>>m;

//選擇模式
        switch(m)
        {
        case 1:
            while(true)
            {
                cout<<"Enter radius: ";
                cin>>r;

                if(r<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            cout<<"Area of circle: "<<circle(r)<<endl;
            break;

        case 2:
            while(true)
            {
                cout<<"Enter width: ";
                cin>>w;

                if(w<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            while(true)
            {
                cout<<"Enter height: ";
                cin>>h;

                if(h<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            cout<<"Area of triangle: "<<triangle(w,h)<<endl;
            break;


        case 3:
            while(true)
            {
                cout<<"Enter width: ";
                cin>>w;

                if(w<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            while(true)
            {
                cout<<"Enter height: ";
                cin>>h;

                if(h<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            cout<<"Area of rectangle: "<<rectangle(w,h)<<endl;
            break;

        case -1:
            return 0;

        default:
            cout<<"Illegal input!"<<endl;
            break;
        }
    }
}

//實作circle
double circle(double x)
{
    double area;
    area=pi*x*x;
    return area;
}

//實作triangle
double triangle(double x,double y)
{
    double area;
    area=0.5*x*y;
    return area;
}

//實作rectangle
double rectangle(double x,double y)
{
    double area;
    area=x*y;
    return area;
}


