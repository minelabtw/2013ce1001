#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int x;
    double h=0,w=0,r=0;
    double k=3.14159;


    cout<<"Calculate the area.\n";
    cout<<"Choose the mode.\n";
    cout<<"Area of circle=1.\n";
    cout<<"Area of triangle=2.\n";
    cout<<"Area of rectangle=3.\n";
    cout<<"Exit=-1.\n";
    cout<<"Enter mode: ";
    cin>>x;

    while(x!=-1)                                 //X等於-1跳出迴圈
    {
        switch(x)
        {
        case 1:
            while(r<=0)
            {
                cout<<"Enter radius: ";
                cin>>r;
                if(r<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            cout<<"Area of circle: "<<r*r*k;           //計算圓面積
            break;
        case 2:
            while(w<=0)
            {
                cout<<"Enter width: ";
                cin>>w;
                if(w<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            while(h<=0)
            {
                cout<<"Enter height: ";
                cin>>h;
                if(h<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            cout<<"Area of triangle: "<<w*h/2;          //計算三角形面積
            break;
        case 3:
            while(w<=0)
            {
                cout<<"Enter width: ";
                cin>>w;
                if(w<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            while(h<=0)
            {
                cout<<"Enter height: ";
                cin>>h;
                if(h<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            cout<<"Area of rectangle: "<<w*h;            //計算矩形面積
            break;
        default:
            cout<<"Illegal input!";
            break;
        }
        cout<<endl;
        cout<<"Enter mode: ";
        cin>>x;
        h=0,w=0,r=0;           //使變數變回初始值
    }
    return 0;
}
