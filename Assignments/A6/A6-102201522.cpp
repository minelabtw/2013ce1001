#include<iostream>
#include <math.h>
using namespace std;

int main()
{
    int mode;
    double a1,a2;
    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;
    cout << "Enter mode: ";  //要求輸入模式代號mode(1,2,3,-1)
    cin >> mode;
    while (mode!=-1)  //只要不輸入-1就能重複計算，-1:結束計算
    {
        while (mode!=1 && mode!=2 && mode!=3)  //輸入非(1,2,3,-1)要輸出"Illegal input!"
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";  //並重複輸入
            cin >> mode;
        }
        switch (mode)
        {
        case 1: //模式1:計算圓面積
            cout << "Enter radius: ";
            cin >> a1;
            while (a1<=0) //半徑要非零正整數
            {
                cout << "Out of range!" << endl; //否則要輸出"Out of range!"
                cout << "Enter radius: "; //從輸入錯誤的該項重新輸入，提示也要再顯示一遍
                cin >> a1;
            }
            cout << "Area of circle: " << pow(a1,2)*3.14159;
            break;
        case 2: //模式2:計算三角形面積
            cout << "Enter width: ";
            cin >> a1;
            while (a1<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> a1;
            }
            cout << "Enter height: ";
            cin >> a2;
            while (a2<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> a2;
            }
            cout << "Area of triangle: " << a1*a2/2;
            break;
        case 3: //模式3:計算矩形面積
            cout << "Enter width: ";
            cin >> a1;
            while (a1<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> a1;
            }
            cout << "Enter height: ";
            cin >> a2;
            while (a2<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> a2;
            }
            cout << "Area of rectangle: " << a1*a2;
            break;
        }
        cout << endl;
        cout << "Enter mode: ";
        cin >> mode;
    }
    return 0;
}
