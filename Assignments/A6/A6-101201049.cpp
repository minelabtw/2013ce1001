#include<iostream>
#include <math.h>                              //可使用指令pow

using namespace std;

int main()
{
    int interger1;
    float r,x,y;
    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";
    cout << "Enter mode:\ ";
    cin >> interger1;
    while(interger1!=-1)                       //當變數等於-1時跳出while迴圈
    {
        switch(interger1)
        {
           case 1:                            //輸入1時執行
              cout << "Enter radius:\ ";
              cin >> r;                       //輸入半徑
              while(r<=0)                     //當半徑小於等於0進入迴圈
              {
                cout << "Out of range!\n";
                cout << "Enter radius:\ ";
                cin >> r;
              }
              cout << "Area of circle:\ " << pow(r,2)*3.14159 << "\n";      //pow(r,2)為r平方
              break;                          //跳出switch
           case 2:                            //輸入2時執行
              cout << "Enter width:\ ";
              cin >> x;                       //輸入寬度
              while (x<=0)                    //當寬度小於等於0進入迴圈
              {
                cout << "Out of range!\n";
                cout << "Enter width:\ ";
                cin >> x;
              }
              cout << "Enter height:\ ";      //輸入長度
              cin >> y;                       //當長度小於等於0進入迴圈
              while (y<=0)
              {
                cout << "Out of range!\n";
                cout << "Enter height:\ ";
                cin >> y;
              }
              cout << "Area of triangle:\ " << x*y/2 << "\n";
              break;                           //跳出switch
           case 3:                             //輸入3時執行
              cout << "Enter width:\ ";
              cin >> x;                        //輸入寬度
              while (x<=0)                     //當寬度小於等於0時進入迴圈
              {
                cout << "Out of range!\n";
                cout << "Enter width:\ ";
                cin >> x;
              }
              cout << "Enter height:\ ";
              cin >> y;                        //輸入長度
              while (y<=0)                     //當長度小於等於0時進入迴圈
              {
                cout << "Out of range!\n";
                cout << "Enter height:\ ";
                cin >> y;
              }
              cout << "Area of rectangle:\ " << x*y << "\n" ;
              break;                      //跳出switch
        default:
            cout << "Illegal input!\n";
            break;                        //跳出switch
        }
        cout << "Enter mode:\ ";
        cin >> interger1 ;
    }
    return 0;
}
