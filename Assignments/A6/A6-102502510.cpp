#include <iostream>

using namespace std;

int main()
{
    int mode;
    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;
    while(true)
    {
        double pi=3.14159;
        bool endwhile=false;
        cout << "Enter mode: ";
        cin >> mode;
        double height=0;
        double width=0;
        double r=0;
        switch(mode)
        {
        case 1://circle
            while(r<=0)
            {
                cout << "Enter radius: ";
                cin >> r;
                if(r<=0)
                    cout << "Out of range!" << endl;
            }
            cout << "Area of circle: " << r*r*pi << endl;
            break;
        case 2://triangle
            while(width<=0)
            {
                cout << "Enter width: ";
                cin >> width;
                if(width<=0)
                    cout << "Out of range!" << endl;

            }
            while(height<=0)
            {
                cout << "Enter height: ";
                cin >> height;
                if(height<=0)
                    cout << "Out of range!" << endl;
            }
            cout << "Area of triangle: " << height*width/2 << endl;
            break;
        case 3://rectangle
            while(width<=0)
            {
                cout << "Enter width: ";
                cin >> width;
                if(width<=0)
                    cout << "Out of range!" << endl;

            }
            while(height<=0)
            {
                cout << "Enter height: ";
                cin >> height;
                if(height<=0)
                    cout << "Out of range!" << endl;
            }
            cout << "Area of rectangle: " << height*width << endl;
            break;
        case -1:
            endwhile=true;
            break;
        default:
            cout << "Illegal input!" << endl;
            break;
        }
        if(endwhile==true)
            break;
    }
    return 0;
}
