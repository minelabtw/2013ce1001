#include <iostream>
#include <cmath>

using namespace std;

double radius=0; // 半徑
double width=0; // 寬度
double height=0; // 高度
double pi=3.14159; // pi = 3.14159

// 計算並輸出
int calculate(int mod,double rad,double wid,double hei)
{
    switch(mod)
    {
    case 1: // 圓
        cout << "Area of circle: " << pow(rad,2)*pi << endl;
        radius = 0;
        break;

    case 2: // 三角形
        cout << "Area of triangle: " << wid*hei*0.5 << endl;
        width = 0;
        height = 0;
        break;

    case 3: // 矩形
        cout << "Area of rectangle: " << wid*hei << endl;
        width = 0;
        height = 0;
        break;
    default:
        break;
    }
}

// 按照模式輸入
void inputmode(int mode)
{
    if(mode==1) // 圓形的輸入
    {
        while(radius<1)
        {
            cout << "Enter radius: ";
            cin >> radius;
            if(radius<1)cout << "Out of range!\n";
        }
    }
    else // 三角形 和 矩形 的輸入
    {
        while(width<1)
        {
            cout << "Enter width: ";
            cin >> width;
            if(width<1)cout << "Out of range!\n";
        }
        while(height<1)
        {
            cout << "Enter height: ";
            cin >> height;
            if(height<1)cout << "Out of range!\n";
        }
    }
    calculate(mode,radius,width,height); // 丟出去計算
}

int main()
{
    int mode=0; // 模式

    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\nEnter mode: ";

    // 判斷輸入的模式
    while(cin >> mode && mode != -1)
    {
        if(mode==1 || mode==2 || mode==3)inputmode(mode);
        else cout << "Illegal input!\n";
        cout << "Enter mode: ";
    }
    return 0;
}





