//E6-102502026
//Area of circle, triangle, rectangle
#include<iostream>
#include<cmath>
using namespace std;

int main()                   //start the program
{
    double pi=3.14159;       //define pi
    double radius=0;         //deifne radius
    int mode=0;              //define mode
    double Ac=0;             //define Area of circle
    double Twidth=0;         //define width of triangle
    double Theight=0;        //define height of triangle
    double At=0;             //define Area of triangle
    double Rwidth=0;         //define width of rectangle
    double Rheight=0;        //define height of rectangle
    double Ar=0;             //define Area of rectangle

    cout<<"Calculate the area.\n";
    cout<<"Choose the mode.\n";
    cout<<"Area of circle=1.\n";
    cout<<"Area of triangle=2.\n";
    cout<<"Area of rectangle=3.\n";
    cout<<"Exit=-1.\n";
    cout<<"Enter mode: ";
    cin>>mode;                                //ask the mode

    while(mode!=-1)                           //do until mode = -1
    {
        switch(mode)
        {
        case 1:                               //if mode = 1
            cout<<"Enter radius: ";
            cin>>radius;                      //ask radius
            while(radius<=0)                  //do until radius>0
            {
                cout<<"Out of range!\n";      //wrong
                cout<<"Enter radius: ";
                cin>>radius;                  //ask again radius
            }
            Ac= (pow(radius,2)) * pi;         //formula for Area of circle
            cout<<"Area of circle: "<<Ac;     //Answer
            cout<<"\n";
            cout<<"Enter mode: ";
            break;

        case 2:                                 //if mode =2
            cout<<"Enter width: ";
            cin>>Twidth;                        //ask width
            while(Twidth<=0)                  //do until width>0
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"Enter width: ";
                cin>>Twidth;                    //ask again width
            }
            cout<<"Enter height: ";
            cin>>Theight;                       //ask height
            while(Theight<=0)                   //do until height>0
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"Enter height: ";
                cin>>Theight;                   //ask again height
            }
            At= (Twidth * Theight)/2;            //formula for Area of triangle
            cout<<"Area of triangle: "<<At;      //Answer
            cout<<"\n";
            cout<<"Enter mode: ";
            break;

        case 3:                                 //if mode=3
            cout<<"Enter width: ";
            cin>>Rwidth;                        //ask width
            while(Rwidth<=0)                  //do until width>0
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"Enter width: ";
                cin>>Rwidth;                    //ask again width
            }
            cout<<"Enter height: ";
            cin>>Rheight;                       //ask height
            while(Rheight<=0)                   //do until height>0
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"Enter height: ";
                cin>>Rheight;                   //ask again height
            }
            Ar= Rwidth * Rheight;                //formula for Area of rectangle
            cout<<"Area of rectangle: "<<Ar;     //Answer
            cout<<"\n";
            cout<<"Enter mode: ";
            break;

        case '\n':                              //if n1=\n
        case '\t':                              //if n1=\t
        case ' ':                               //if n1=" "
            break;

        default:                                //if is another number or letter
            cout<<"Illegal input!\n";           //print error
            cout<<"Enter mode: ";
            break;
        }
        cin>>mode;                                //all finish return to ask again n1
    }
    return 0;
}                                               //end program
