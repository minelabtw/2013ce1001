#include<iostream>
#define PI 3.14159
using namespace std;
double radius=0,height=0,width=0;//set global variables and initial them as 0
void radius1()
{
    radius=0;//reset the number to start the while loop
    while(radius<=0)
    {
        cout<<"Enter radius: ";
        cin>>radius;
        if(radius<=0)
            cout<<"Out of range!"<<endl;
    }//loop until the correct number is input
}//set up a function to replace all the same input
void height1()
{
    height=0;//reset the number to start the while loop
    while(height<=0)
    {
        cout<<"Enter height: ";
        cin>>height;
        if(height<=0)
            cout<<"Out of range!"<<endl;
    }//loop until the correct number is input
}//set up a function to replace all the same input
void width1()
{
    width=0;//reset the number to start the while loop
    while(width<=0)
    {
        cout<<"Enter width: ";
        cin>>width;
        if(width<=0)
            cout<<"Out of range!"<<endl;
    }//loop until the correct number is input
}//set up a function to replace all the same input
int main()
{
    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;
    int mode=0;
    while(1)
    {
        cout<<"Enter mode: ";
        cin>>mode;
        if(mode==-1)
            break;//keep looping until the user input -1
        switch(mode)
        {
        case 1:
        {
            radius1();
            cout<<"Area of circle: "<<radius*radius*PI<<endl;
        }//if the user choose mode 1, then call out the function and compute the area
        break;
        case 2:
        {
            width1();
            height1();
            cout<<"Area of triangle: "<<width*height/2<<endl;
        }//if the user choose mode 2 , then call out the function and compute the area
        break;
        case 3:
        {
            width1();
            height1();
            cout<<"Area of rectangle: "<<width*height<<endl;
        }//if the user choose mode 3, then call out the function and compute the area
        break;
        default :
            cout<<"Illegal input!"<<endl;
            break;//just jump out the switch case and keep looping
        }
    }
    return 0;
}

