#include <iostream>

using namespace std;

int main()
{
    int x;
    cout<<"Calculate the area."<<endl<<"Choose the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl<<"Area of rectangle=3."<<endl<<"Exit=-1."<<endl;
    while(1)
    {
        cout<<"Enter mode: ";
        cin>>x;
        while(x!=-1&&x!=1&&x!=2&&x!=3)
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>x;
        }
           if(x==1)
        {
            double y=3.14159;
            double r;
            cout<<"Enter radius: ";
            cin>>r;
            while(r<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>r;
            }
            cout<<"Area of circle: "<<(r*r*y)<<endl;
        }
        if(x==2)
		{
			double w;
			double l;
			cout<<"Enter width: ";
			cin>>w;
			while(w<=0)
			{
				cout<<"Out of range!"<<endl;
				cout<<"Enter width: ";
				cin>>w;
			}
			cout<<"Enter height: ";
			cin>>l;
			while(l<=0)
			{
				cout<<"Out of range!"<<endl;
				cout<<"Enter height: ";
				cin>>l;
			}
			cout<<"Area of triangle: "<<(l*w/2)<<endl;
		}
		if(x==3)
		{
			double w;
			double l;
			cout<<"Enter width: ";
			cin>>w;
			while(w<=0)
			{
				cout<<"Out of range!"<<endl;
				cout<<"Enter width: ";
				cin>>w;
			}
			cout<<"Enter height: ";
			cin>>l;
			while(l<=0)
			{
				cout<<"Out of range!"<<endl;
				cout<<"Enter height: ";
				cin>>l;
			}
			cout<<"Area of rectangle: "<<(l*w)<<endl;
		}
		if(x==-1)
		{
			break;
		}

    }
    return 0;
}
