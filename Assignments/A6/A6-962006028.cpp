#include<iostream>
using namespace std;
int main()
{
    int x;  //設定變數
    double r , h , l , a ;
    double pi=3.14159 ;
    cout << "Calculate the area." << endl << "Choose the mode." << endl ;   //提示使用者
    cout << "Area of circle=1." <<endl <<"Area of triangle=2." <<endl ;
    cout << "Area of rectangle=3." << endl << "Exit=-1." <<endl ;
    do  //迴圈開始
    {
        cout << "Enter mode: " ;
        cin >> x ;
        while (x<-1 || x > 3 || x==0)    //如果不符合範圍就重新輸入
        {
            cout << "Illegal input!" <<endl ;
            cout << "Enter mode: " ;
            cin >> x ;
        }

        switch ( x )    //判斷要進行哪種計算
        {
        case 1 :
            cout << "Enter radius:"  ;
            cin >> r ;
            while (r<0)  //如果不符合範圍就重新輸入
            {
                cout << "Out of range!" <<endl ;
                cout << "Enter radius: " ;
                cin >>r ;
            }
            cout << "Area of circle: " ;
            cout << pi*r*r ;
            cout << endl ;
            break;

        case 2 :
            cout << "Enter width: " ;
            cin >> l;
            while (l<0)  //如果不符合範圍就重新輸入
            {
                cout << "Out of range!" <<endl ;
                cout << "Enter width: " ;
                cin >>l ;
            }
            cout << "Enter height: " ;
            cin >>h ;
            while (h<=0)  //如果不符合範圍就重新輸入
            {
                cout << "Out of range!" <<endl ;
                cout << "Enter height: " ;
                cin >>h ;
            }
            a=l*h/2 ;
            cout << "Area of triangle: " << a << endl ;

            break;

        case 3 :
            cout << "Enter width: " ;
            cin >> l;
            while (l<0)  //如果不符合範圍就重新輸入
            {
                cout << "Out of range!" <<endl ;
                cout << "Enter width: " ;
                cin >>l ;
            }
            cout << "Enter height: " ;
            cin >>h ;
            while (h<=0)  //如果不符合範圍就重新輸入
            {
                cout << "Out of range!" <<endl ;
                cout << "Enter height: " ;
                cin >>h ;
            }
            a=h*l ;
            cout << "Area of rectangle: " << a  ;
            cout << endl ;
            break;

        }
    }
    while (x!=-1) ;

    return 0 ;
}
