#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int a = 0;                                                      //設定所需變數
    double r1 = 0;
    double pi = 3.14159;
    double x2 = 0;
    double y2 = 0;
    double x3 = 0;
    double y3 = 0;

    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;
    cout << "Enter mode: ";

    while (cin >> a)
    {
        if (a==1 || a==2 || a==3 || a==-1)                          //挑出符合範圍的模式
        {
            switch(a)
            {
            case 1:                                                 //計算圓面積
                cout << "Enter radius: ";
                cin >> r1;
                while (r1<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter radius: ";
                    cin >> r1;
                }
                cout << "Area of circle: " << pow(r1,2)*pi << endl;
                cout << "Enter mode: ";
                break;

            case 2:                                                 //計算三角形面積
                cout << "Enter width: ";
                cin >> x2;
                while (x2<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter width: ";
                    cin >> x2;
                }
                cout << "Enter height: ";
                cin >> y2;
                while (y2<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter height: ";
                    cin >> y2;
                }
                cout << "Area of triangle: " << x2*y2*0.5 << endl;
                cout << "Enter mode: ";
                break;

            case 3:                                                 //計算矩形面積
                cout << "Enter width: ";
                cin >> x3;
                while (x3<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter width: ";
                    cin >> x3;
                }
                cout << "Enter height: ";
                cin >> y3;
                while (y3<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Enter height: ";
                    cin >> y3;
                }
                cout << "Area of rectangle: " << x3*y3 << endl;
                cout << "Enter mode: ";
                break;

            case -1:                                                //離開switch
                break;
            }
            if (a==-1)                                              //離開程式
                break;
        }
        else                                                        //若非模式所需之數字，則重新輸入
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
        }
    }
    return 0;
}
