#include<iostream>
using namespace std;

int main()
{
    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1." << endl;

    int number1=0;
    double radius=0;
    double width1=0,height1=0;
    double width2=0,height2=0;

    cout << "Enter mode: ";
    cin >> number1;

    while(number1 != 1 && number1 != 2 && number1 != 3 && number1 != -1)    //如果number1不等於那四個數字，輸出Illegal input!，並再次輸入
    {
        cout << "Illegal input!\n";
        cout << "Enter mode: ";
        cin >> number1;
    }

    while(number1 != -1)    //如果number1不等於一，執行迴圈
    {
        switch(number1)     //判斷number1
        {
        case 1 :
            while(radius <= 0)
            {
                cout << "Enter radius: ";
                cin >> radius;
                if(radius <= 0)
                    cout << "Out of range!" << endl;
            }
            cout << "Area of circle: " << 3.14159 * radius * radius;    //運算圓面積
            cout << "\nEnter mode: ";
            cin >> number1;
            break;
        case 2 :
            while(width1 <= 0)
            {
                cout << "Enter width: ";
                cin >> width1;
                if(width1 <= 0)
                    cout << "Out of range!" << endl;
            }

            while(height1 <= 0)
            {
                cout << "Enter height: ";
                cin >> height1;
                if(height1 <= 0)
                    cout << "Out of range!" << endl;
            }
            cout << "Area of triangle: " << width1 * height1 / 2;    //運算三角形面積
            cout << "\nEnter mode: ";
            cin >> number1;
            break;
        case 3 :
            while(width2 <= 0)
            {
                cout << "Enter width: ";
                cin >> width2;
                if(width2 <= 0)
                    cout << "Out of range!" << endl;
            }
            while(height2 <= 0)
            {
                cout << "Enter height: ";
                cin >> height2;
                if(height2 <= 0)
                    cout << "Out of range!" << endl;
            }
            cout << "Area of rectangle: " << width2 * height2;    //運算長方形面積
            cout << "\nEnter mode: ";
            cin >> number1;
            break;
        }
    }
    return 0;
}
