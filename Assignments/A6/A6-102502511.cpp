#include <iostream>
using namespace std;
int main()
{
    cout << "Calculate the area."<<endl;
    cout << "Choose the mode."<<endl;
    cout << "Area of circle=1."<<endl;
    cout << "Area of triangle=2."<<endl;
    cout << "Area of rectangle=3."<<endl;
    cout << "Exit=-1."<<endl;

    int entermode=1; //設entermode初始值為1 使他符合while條件

    while(entermode!=-1)//當entermode不等於-1時 不停重複
    {
        cout << "Enter mode: ";
        cin>>entermode;

        switch(entermode)//裡面條件皆判斷entermode
        {
        case 1: //當entermode=1 運用下列動作
        {
            double r;
            double x = 3.14159;

            cout <<"Enter radius: ";
            cin >> r;
            while(r<1) //當r小於1 執行下列程式
            {
                cout << "Out of range!"<<endl;
                cout <<"Enter radius: ";
                cin >> r;
            }

            cout << "Area of circle: " << r*r*x;

            cout << endl;
            break;
        }

        case 2: //當entermode=2 行駛下列動作
        {
            double w1;
            double h1;

            cout <<"Enter width: ";
            cin >> w1;
            while(w1<1) //當w1小於1 執行下列程式
            {
                cout << "Out of range!"<<endl;
                cout <<"Enter width: ";
                cin >> w1;
            }

            cout << "Enter height: ";
            cin >> h1;
            while(h1<1) //當h1小於1 執行下列程式
            {
                cout << "Out of range!"<<endl;
                cout <<"Enter height: ";
                cin >> h1;
            }

            cout << "Area of triangle: " << w1*h1/2;

            cout <<endl;
            break;
        }

        case 3: //當entermode等於3 行駛下列動作
        {
            double w2;
            double h2;

            cout << "Enter width: ";
            cin >> w2;
            while(w2<1) //當w2小於1 執行下列程式
            {
                cout << "Out of range!"<<endl;
                cout <<"Enter width: ";
                cin >> w2;
            }

            cout << "Enter height: ";
            cin >> h2;
            while(h2<1) //當h2小於1 執行下列程式
            {
                cout << "Out of range!"<<endl;
                cout <<"Enter height: ";
                cin >> h2;
            }

            cout << "Area of rectangle: " << w2*h2;

            cout <<endl;
            break;
        }

        case -1: //當entermode=-1時 直接結束
        {
            break;
        }

        default: //當entermode不等於1 2 3 -1 執行下列動作
        {
            cout << "Illegal input!"<<endl;
            break;
        }
        }
    }
    return 0;
}

