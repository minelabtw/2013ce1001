/*可以重複計算面積
有三種模式
模式1:計算圓面積
模式2:計算三角形面積
模式3:計算矩形面積
-1:結束計算
在"Enter mode: “後輸入模式代號(1,2,3,-1)
只要不輸入-1就能重複計算
輸入非模式代號(1,2,3,-1)要輸出"Illegal input!"*/
#include <iostream>
using namespace std;

int main()
{
    int mode;
    double radius,width,height,area;
    cout << "Calculate the area.\n"//輸出說明
         << "Choose the mode.\n"
         << "Area of circle=1.\n"
         << "Area of triangle=2.\n"
         << "Area of rectangle=3.\n"
         << "Exit=-1.\n";
    while(1)
    {
        cout << "Enter mode: ";
        cin >> mode;
        if (mode==-1)//判斷mode,輸入-1則跳出迴圈
            break;
        else if (mode==1)//mode1的圓面積
        {
            while(1)//判斷半徑是否符合>0
            {
                cout << "Enter radius: ";
                cin >> radius;
                if(radius>0)
                    break;
                else
                    cout << "Out of range!\n";
            }
            cout << "Area of circle: " << radius*radius*3.14159 << endl;
        }
        else if (mode==2 || mode==3)//處理mode2&mode3
        {
             while(1)//判斷寬是否>0
            {
                cout << "Enter width: ";
                cin >> width;
                if(width>0)
                    break;
                else
                    cout << "Out of range!\n";
            }
            while(1)//判斷高是否>0
            {
                cout << "Enter height: ";
                cin >> height;
                if(height>0)
                    break;
                else
                    cout << "Out of range!\n";
            }
            area=(mode==2 ? (height*width)/2 : height*width);//兩種mode的情況下不同的面積
            cout << (mode==2 ? "Area of triangle: " : "Area of rectangle: ") << area << endl;//輸出面積
        }
        else//mode不是1,2,3,-1的話則要求再輸入一次
            cout << "Illegal input!\n";
    }
    return 0;
}
