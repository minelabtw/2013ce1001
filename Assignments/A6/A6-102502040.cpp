#include<iostream>
#include<iomanip>
using namespace std;

int main(void)
{
    cout << "Calculate the area." << endl;//顯示字元
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;
    double a=0,l=0;//半徑或是長寬
    double p=0;//面積
    double n=3.14159;//圓周率
    int i=0;

    do//重複進行程式,直到輸入指令-1
    {
        cout << "Enter mode: " ;//輸入模式
        cin >> i;
        switch(i)
        {

        case 1://輸入半徑輸出圓面積
            cout << "Enter radius: " ;
            cin >> a;
            while(a<0)
            {
                cout << "Illegal input!" << endl;
                cout << "Enter radius: ";
                cin >> a;
            }
            p=a*a*n;
            cout << "Area of circle: " << p <<endl;
            break;

        case 2://輸入長寬輸出三角形面積
            cout << "Enter width: " ;
            cin >> a;
            while(a<0)
            {
                cout << "Illegal input!" << endl;
                cout << "Enter width: " ;
                cin >> a;
            }
            cout << "Enter height: " ;
            cin >> l;
            while(l<0)
            {
                cout << "Illegal input!" << endl;
                cout << "Enter height: " ;
                cin >> l;
            }
            p=a*l/2;
            cout << "Area of triangle: " << p << endl;
            break;

        case 3://輸入長寬輸出矩形面積
            cout << "Enter width: " ;
            cin >> a;
            while(a<0)
            {
                cout << "Illegal input!" << endl;
                cout << "Enter width: " ;
                cin >> a;
            }
            cout << "Enter height: " ;
            cin >> l;
            while(l<0)
            {
                cout << "Illegal input!" << endl;
                cout << "Enter height: ";
                cin >> l;
            }
            p=a*l;
            cout << "Area of rectangle: " << p << endl;
            break;

        case -1://輸入-1程式結束
            break;
        default://指令輸入不在範圍內則顯示字元
            cout << "Illegal input!" << endl;
            break;

        }
    }
    while(i!=-1);//結束

    return 0;
}
