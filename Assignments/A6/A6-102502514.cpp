#include <iostream>
using namespace std;

int main()
{
    cout <<"Calculate the area.\n";
    cout <<"Choose the mode.\n";
    cout <<"Area of circle=1.\n";
    cout <<"Area of triangle=2.\n";
    cout <<"Area of rectangle=3.\n";
    cout <<"Exit=-1.\n";
    int mode=0;

    while (mode!=-1) //當輸入-1時結束程式,否則重複迴圈指令
    {
        cout <<"Enter mode: ";
        cin >>mode;
        while (mode!=1&&mode!=2&&mode!=3&&mode!=-1) //mode數值需介於-1~3,否則請求重新輸入
        {
            cout <<"Illegal input!\nEnter mode: ";
            cin >>mode;
        }
        switch (mode)
        {
        case 1: //請求輸入圓半徑,並算出圓面積
        {
            double radius=0;
            double pi=3.14159;
            cout <<"Enter radius: ";
            cin >>radius;
            while (radius<=0)
            {
                cout <<"Out of range!\nEnter radius: ";
                cin >>radius;
            }
            cout <<endl;
            cout <<"Area of circle: "<<radius*radius*pi<<endl;
            break;
        }
        case 2: //請求輸入長與寬,並算出三角形的面積
        {
            double width=0;
            double height=0;
            cout <<"Enter width: ";
            cin >>width;
            while (width<=0)
            {
                cout <<"Out of range!\nEnter width: ";
                cin >>width;
            }
            cout <<"Enter height: ";
            cin >>height;
            while (height<=0)
            {
                cout <<"Out of range!\nEnter height: ";
                cin >>height;
            }
            cout <<endl;
            cout <<"Area of triangle: "<<width*height/2<<endl;
            break;
        }
        case 3: //請求輸入長與寬,並算出長方形面積
        {
            double width=0;
            double height=0;
            cout <<"Enter width: ";
            cin >>width;
            while (width<=0)
            {
                cout <<"Out of range!\nEnter width: ";
                cin >>width;
            }
            cout <<"Enter height: ";
            cin >>height;
            while (height<=0)
            {
                cout <<"Out of range!\nEnter height: ";
                cin >>height;
            }
            cout <<endl;
            cout <<"Area of rectangle: "<<width*height<<endl;
            break;
        }
        case -1: //跳出程式
            break;
        }
    }
}
