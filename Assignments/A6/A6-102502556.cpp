#include <iostream>
#include <cmath>
using namespace std;

void AOC (); //function prototype
void AOT ();
void AOR ();

int main ()
{
    cout << "Calculate the area." << endl; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;

    int mode = 0; //宣告型別為 整數(int) 的判斷值(mode)，並初始化其數值為0。
    while (1) //用while迴圈使其能重複做運算，若輸入 -1 則程式結束。
    {
        cout << "Enter mode: ";
        cin >> mode;
        while ( mode != 1 && mode != 2 && mode != 3 && mode != -1 )
            //用while迴圈檢驗使用者的輸入是否合乎標準，若不符，則要求其重新輸入。
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> mode;
        }
        if ( mode == -1 ) //若 mode = -1 ，則程式結束。
        {
            break;
        }
        else
        {
            switch ( mode ) //使用switch迴圈，使程式能針對不同的判斷值做不同的運算。
            {
            case 1:
                AOC();
                break;
            case 2:
                AOT();
                break;
            case 3:
                AOR();
                break;
            }
        }
        cout << endl;
    }
    return 0;
}

void AOC () //計算圓形面積。
{
    double radius = 0;
    cout << "Enter radius: ";
    cin >> radius;
    while ( radius <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Enter radius: ";
        cin >> radius;
    }
    cout << "Area of circle: " << 3.14159 * pow ( radius , 2 );
}

void AOT () //計算三角形面積。
{
    double width = 0;
    double height = 0;
    cout << "Enter width: ";
    cin >> width;
    while ( width <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Enter width: ";
        cin >> width;
    }
    cout << "Enter height: ";
    cin >> height;
    while ( height <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Enter height: ";
        cin >> height;
    }
    cout << "Area of triangle: " << 0.5 * width * height;
}

void AOR () //計算矩形面積。
{
    double width = 0;
    double height = 0;
    cout << "Enter width: ";
    cin >> width;
    while ( width <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Enter width: ";
        cin >> width;
    }
    cout << "Enter height: ";
    cin >> height;
    while ( height <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Enter height: ";
        cin >> height;
    }
    cout << "Area of rectangle: " << width * height;
}
