#include <iostream>
#include <string>
using namespace std;
#define PI 3.14159
void input(string s,double &n)  //��J�禡
{
    cout<<"Enter "<<s<<": ";
    cin>>n;
    while(n<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter "<<s<<": ";
        cin>>n;
    }
}
double mode1()  //MODE1
{
    double radius;
    input("radius",radius);
    return radius*radius*PI;
}
double mode2()  //MODE2
{
    double width;
    double length;
    input("width",width);
    input("length",length);
    return width*length/2;
}
double mode3()  //MODE3
{
    double width;
    double length;
    input("width",width);
    input("length",length);
    return width*length;
}
int main()
{
    cout<<"Calculate the area.\n";
    cout<<"Choose the mode.\n";
    cout<<"Area of circle=1.\n";
    cout<<"Area of triangle=2.\n";
    cout<<"Area of rectangle=3.\n";
    cout<<"Exit=-1.\nEnter mode: ";
    int mode;
    while(cin>>mode&&mode!=-1)  //��J�Ҧ�
    {
        if(mode==1)
            cout<<"Area of circle: "<<mode1()<<endl;
        else if(mode==2)
            cout<<"Area of triangle: "<<mode2()<<endl;
        else if(mode==3)
            cout<<"Area of rectangle: "<<mode3()<<endl;
        else
            cout<<"Illegal input!\n";
        cout<<"Enter mode: ";
    }
    return 0;
}
