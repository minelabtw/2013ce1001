#include <iostream> //to input and output the data

using std::cin; //standard the input
using std::cout; //standard the output

int main () //main function
{
    int a = 0; //enter mode
    double pi = 3.14159; //constant of circle
    double r = 0; //radius
    double w = 0; //width
    double h = 0; //height
    double i = 0; //area of circle
    double j = 0; //area of triangle
    double k = 0; //area of rectangle

    cout <<"Calculate the area.\n"; //print the words
    cout <<"Choose the mode.\n"; //print the words
    cout <<"Area of circle=1.\n"; //print the words
    cout <<"Area of triangle=2.\n"; //print the words
    cout <<"Area of rectangle=3.\n"; //print the words
    cout <<"Exit=-1.\n"; //print the words

    while (a!=-1) //determine the input
    {
        cout <<"Enter mode:"; //print the words
        cin >>a; //input the data
        switch (a) //for each input
        {
        case 1: //to circle calculate
        {
            cout <<"Enter radius:"; //print the words
            cin >>r; //enter the radius
            while (r<=0) //determine the r
            {
                cout <<"Out of range!\n"; //print the words
                cout <<"Enter radius:"; //repeat
                cin >>r; //repeat enter
            }
            i = r * r * pi; //calculate area
            cout <<"Area of circle:"<<i<<"\n"; //print data
            break; //end case
        }
        case 2: //to triangle calculate
        {
            cout <<"Enter width:"; //print the words
            cin >>w; //enter the width
            while (w<=0) //determine the w
            {
                cout <<"Out of range!\n"; //print the words
                cout <<"Enter width:"; //repeat
                cin >>w; //repeat enter
            }
            cout <<"Enter height:"; //print the words
            cin >>h; //enter
            while (h<=0) //determine
            {
                cout <<"Out of range!\n"; //print the words
                cout <<"Enter height:"; //repeat
                cin >>h; //repeat enter
            }
            j = w * h / 2; //calculte area of truangle
            cout <<"Area of triangle:"<<j<<"\n"; //print data
            break; //end case
        }
        case 3: //to rectangle calculate
        {
            cout <<"Enter width:"; //print the words
            cin >>w; //enter the w
            while (w<=0) //determine
            {
                cout <<"Out of range!\n"; //print the words
                cout <<"Enter width:"; //repeat
                cin >>w; //repeat enter
            }
            cout <<"Enter height:"; //print the words
            cin >>h; //enter the h
            while (h<=0) //determine
            {
                cout <<"Out of range!\n"; //print the words
                cout <<"Enter height:"; //repeat
                cin >>h; //repeat enter
            }
            k = w * h; //calculate area of rectangle
            cout <<"Area of rectangle:"<<k<<"\n"; //print data
            break; //end case
        }
        case -1: //if enter is -1
            break; //end case
        default: //others
        {
            cout <<"Illegal input!\n"; //print illegal
            break; //end default
        }
        }
    }
    return 0; //indicate that program ended successfully
} //end function

