#include <iostream>
using namespace std;
#define pi 3.14159//定義pi圓周率為3.14159 

int main(){
    double mode,r,width,height,i; 
    cout << "Calculate the area.\n"//輸出說明 
         << "Choose the mode.\n"
         << "Area of circle=1.\n"
         << "Area of triangle=2.\n"
         << "Area of rectangle=3.\n"
         << "Exit=-1.\n";
    while(1){
        cout << "Enter mode: ";
        cin >> mode;
        if(mode == -1)//如果輸入-1即跳出程式 
            break;
        else if(mode == 1){//處理mode1的數列 
            do{
                cout << "Enter radius: ";
                cin >> r;
            }while(r<=0 && cout << "Out of range!\n");//輸入半徑,錯誤則要求重新輸入 
            cout << "Area of circle: " << r*r*pi <<endl;//輸出圓形面積 
        }
        else if(mode == 2 || mode == 3){//處理mode2,3的數列
            do{
                cout << "Enter width: ";
                cin >> width;
            }while(width<=0 && cout << "Out of range!\n");//輸入寬,錯誤則要求重新輸入
            do{
                cout << "Enter height: ";
                cin >> height;
            }while(height<=0 && cout << "Out of range!\n");//輸入高,錯誤則要求重新輸入
            mode == 2 ? cout << "Area of triangle: " << width*height/2 <<endl
                      : cout << "Area of rectangle: " << width*height <<endl;//輸出三角形或長方形面積 
        }
        else//輸入錯誤,要求重新輸入模式 
            cout << "Illegal input!\n";
    }
    return 0;    
}
