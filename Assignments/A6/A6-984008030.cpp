#include <iostream>
using namespace std;
bool isNotPositive(double number);//檢查number是否為正

int main(){
    int mode = 1;//宣告型態為int的模式變數，並初始化為1
    /*圓形專用*/
    double radius = 1.0;//宣告型態為double的半徑，並初始化為1.0
    const double pi = 3.14159;//宣告型態為const double的園周率，並初始化為3.14159
    /*三角形 長方型專用*/
    double width = 1.0;//宣告型態為double的長，並初始化為1.0
    double height = 1.0;//宣告型態為double的寬，並初始化為1.0

    cout << "Calculate the area." << endl \
         << "Choose the mode." << endl \
         << "Area of circle=1." << endl \
         << "Area of triangle=2." << endl \
         << "Area of rectangle=3." << endl \
         << "Exit=-1." << endl;
    while(mode != -1){//當mode為-1表結束，跳出
        cout << "Enter mode: ";
        cin >> mode;
        switch(mode){
            case 1://計算圓面積
                while(1){//輸入半徑長
                    cout << "Enter radius: ";
                    cin >> radius;
                    if(!isNotPositive(radius)){
                        break;
                    }
                }
                cout << "Area of circle: " << radius * radius * pi << endl;
                break;
            case 2://計算三角形面積
                while(1){//輸入長
                    cout << "Enter width: ";
                    cin >> width;
                    if(!isNotPositive(width)){
                        break;
                    }
                }
                while(1){//輸入寬
                    cout << "Enter height: ";
                    cin >> height;
                    if(!isNotPositive(height)){
                        break;
                    }
                }
                cout << "Area of triangle: " << width * height / 2.0 << endl;
                break;
            case 3://計算長方形面積
                while(1){//輸入長
                    cout << "Enter width: ";
                    cin >> width;
                    if(!isNotPositive(width)){
                        break;
                    }
                }
                while(1){//輸入寬
                    cout << "Enter height: ";
                    cin >> height;
                    if(!isNotPositive(height)){
                        break;
                    }
                }
                cout << "Area of rectangle: " << width * height << endl;
                break;
            case -1://結束
                //不做任何事情，且在迴圈開始時mode=-1，跳出。
                break;
            default:
                //不合法mode
                cout << "Illegal input!" << endl;
                break;
        }
    }
    return 0;
}

bool isNotPositive(double number){//檢查number是否為正
    if(number <= 0){
        cout << "Out of range!" << endl;
        return true;
    }
    else{
        return false;
    }
}

