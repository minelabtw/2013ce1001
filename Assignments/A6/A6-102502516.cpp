#include<iostream>
#include <limits>
using namespace std;

int mode;
double width, height;

void inputLegal (double &input , string type )  //自訂一個判斷輸入值是否正確的函式
{                                               //input變數表示將cin儲存到那個變數，type則可以改變顯示的文字
    do
    {
        cout << "Enter " << type <<": ";        //提示使用者輸入何種長度
        cin >> input;
        if ( input <= 0 || !(cin)  )            //若cin進非零負數或甚至讓cin錯誤時
        {
            cin.clear();                        //先清除cin的錯誤狀態
            cin.ignore(numeric_limits<streamsize>::max(), '\n');    //刪除緩衝區所有的資料
            cout << "Out of range!\n";          //提示錯誤
        }
    }
    while ( input <= 0 || !(cin));
}

int main()
{
    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.";
    while (true)
    {
        cout << "\nEnter mode: ";
        cin >> mode;
        if ( !(cin) )           //排除使用者輸入非數字的情形
        {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            mode = 4;           //讓mode=4，以跳到default中
        }

        switch ( mode )
        {
        case 1:
            inputLegal(width , "radius");                           //呼叫inputLegal函式
            cout << "Area of circle: " << width * width * 3.14159;  //計算圓面積
            break;

        case 2:
            inputLegal(width , "width");
            inputLegal(height , "height");                          //呼叫inputLegal函式
            cout << "Area of triangle: " << width * height /2;      //計算三角形面積
            break;

        case 3:
            inputLegal(width , "width");
            inputLegal(height , "height");                          //呼叫inputLegal函式
            cout << "Area of rectangle: " << width * height;        //計算矩形面積
            break;

        case -1:
            return 0;                   //-1時退出程式

        default:
            cout << "Illegal input!";   //輸入錯誤時的狀況
            break;
        }
    }
}
