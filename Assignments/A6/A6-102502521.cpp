#include <iostream>

using namespace std;

int main()
{
    int mode;    //宣告變數

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

    while(mode!=-1)    //利用while來達到重複選擇模式的效果，在mode=-1時跳出迴圈
    {
        double radius=0;    //宣告變數
        double width=0;
        double height=0;

        cout<<"Enter mode: ";
        cin>>mode;

        switch(mode)    //使用switch判斷使用模式
        {
        case 1:
            while(radius<=0)    //使用while判斷數值是否合規定
            {
                cout<<"Enter radius: ";
                cin>>radius;
                if(radius<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            cout<<"Area of circle: "<<radius*radius*3.14159;    //計算
            cout<<endl;

            break;

        case 2:
            while(width<=0)
            {
                cout<<"Enter width: ";
                cin>>width;
                if(width<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(height<=0)
            {
                cout<<"Enter height: ";
                cin>>height;
                if(height<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            cout<<"Area of triangle: "<<width*height/2;
            cout<<endl;

            break;

        case 3:
            while(width<=0)
            {
                cout<<"Enter width: ";
                cin>>width;
                if(width<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(height<=0)
            {
                cout<<"Enter height: ";
                cin>>height;
                if(height<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            cout<<"Area of rectangle: "<<width*height;
            cout<<endl;

            break;

        default:
            if(mode!=-1)    //如果mode為-1就不會顯示
            {
                cout<<"Illegal input!"<<endl;
            }

            break;
        }
    }
    return 0;
}
