#include <iostream>
using namespace std;
int main()
{
    int a=0;      //宣告變數a
    double b=0;   //宣告變數b
    double c=0;   //宣告變數c
    cout <<"Calculate the area."<<endl<<"Choose the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl<<"Area of rectangle=3."<<endl<<"Exit=-1."<<endl<<"Enter mode: "; //輸出
    cin >> a; //輸入a
    while (a!=-1) //進入迴圈
    {
        switch (a) //進入以下case
        {
        case 1: //圓形
            cout <<"Enter radius: ";
            cin >> b; //輸入半徑
            while (b<1)
            {
                cout <<"Out of range!"<<endl<<"Enter radius: ";
                cin >>b;
            }
            cout <<"Area of circle: "<<b*b*3.14159<<endl<<"Enter mode: "; //輸出圓形面積
            cin >>a;
            break; //跳出switch
        case 2: //三角形
            cout <<"Enter width: ";
            cin >> b; //輸入寬
            while (b<1)
            {
                cout <<"Out of range!"<<endl<<"Enter width: ";
                cin >>b;
            }
            cout <<"Enter height: ";
            cin >> c; //輸入高
            while (c<1)
            {
                cout <<"Out of range!"<<endl<<"Enter height: ";
                cin >>c;
            }
            cout <<"Area of triangle: "<<b*c/2<<endl<<"Enter mode: "; //輸出三角形面積
            cin >>a;
            break;
        case 3: //長方形
            cout <<"Enter width: ";
            cin >> b; //輸入寬
            while (b<1)
            {
                cout <<"Out of range!"<<endl<<"Enter width: ";
                cin >>b;
            }
            cout <<"Enter height: ";
            cin >> c; //輸入高
            while (c<1)
            {
                cout <<"Out of range!"<<endl<<"Enter height: ";
                cin >>c;
            }
            cout <<"Area of rectangle: "<<b*c<<endl<<"Enter mode: "; //輸出長方形面積
            cin >>a;
            break;
        case -1:
            return 0; //結束
        default:
            cout <<"Illegal input!"<<endl<<"Enter mode: ";
            cin >>a;
        }
    }
    return 0; //結束
}
