#include<iostream>
using namespace std;
int main()
{
    int area = 0; //令一個整數變數初始化=0
    double radius = 0; //令半徑為實數初始值=0
    double width = 0;
    double height = 0;
    int x = 1;

    cout << "Calculate the area." << "\nChoose the mode." << "\nArea of circle=1." << "\nArea of triangle=2."
         << "\nArea of rectangle=3." << "\nExit=-1."; //顯示字串
    do
    {
        cout << "\nEnter mode: ";
        cin >> area; //輸入字串

        switch ( area ) //選擇算哪種面積
        {
        case 1: //計算圓形面積
            do
            {
                cout << "Enter radius: ";
                cin >> radius;
                if ( radius <= 0 ) //
                    cout << "Out of range!" << endl;
            }
            while ( radius <= 0 );
            cout << "Area of circle: " << radius * radius * 3.14159;
            break; //跳出選擇

        case 2: //計算三角形面積
            do
            {
                cout << "Enter width: ";
                cin >> width;
                if ( width <= 0 )
                    cout << "Out of range!" << endl;
            }
            while ( width <= 0 );
            do
            {
                cout << "Enter height: ";
                cin >> height;
                if ( height <= 0 )
                    cout << "Out of range!" << endl;
            }
            while ( height <= 0 );
            cout << "Area of triangle: " << width * height * 0.5;
            break;

        case 3: //計算矩形面積
            do
            {
                cout << "Enter width: ";
                cin >> width;
                if ( width <= 0 )
                    cout << "Out of range!" << endl;
            }
            while ( width <= 0 );
            do
            {
                cout << "Enter height: ";
                cin >> height;
                if ( height <= 0 )
                    cout << "Out of range!" << endl;
            }
            while ( height <= 0 );
            cout << "Area of rectangle: " << width * height;
            break;

        case -1: //結束選擇
            x = -1;
            break;

        default: //輸入mode錯誤
            cout << "Illegal input!";
            break;
        }
    }
    while ( x != -1 );

    return 0; //結束
}
