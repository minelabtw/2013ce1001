#include<iostream>
#include <iomanip>
#include<cmath>
using namespace std;

int main()
{
    float r=0,height=0,width=0,height1=0,width1=0;
    double circle=0, triangle=0, rectangle=0;
    int mode=0;

    cout << "Calculate the area." << endl << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;



    do        //當mode不等於-1時會一直迴圈
    {
        cout << "Enter mode: " ;
        cin >> mode;

        switch(mode)
        {
        case 1:
            do//輸入首項
            {
                cout << "Enter radius: " ;
                cin >> r;
                if(r<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(r<=0);
            circle=3.14159*r*r;     //圓面積
            cout << "Area of circle: " << circle << endl;

            break;

        case 2:
            do//輸入首項
            {
                cout << "Enter height: " ;
                cin >> height;
                if(height<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(height<=0);
            do//輸入首項
            {
                cout << "Enter width: " ;
                cin >> width;
                if(width<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(width<=0);

            triangle=height*width/2;        //三角形面積
            cout << "Area of triangle: " << triangle << endl;

            break;

        case 3:
            do//輸入首項
            {
                cout << "Enter height: " ;
                cin >> height1;
                if(height1<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(height1<=0);
            do//輸入首項
            {
                cout << "Enter width: " ;
                cin >> width1;
                if(width1<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(width1<=0);

            rectangle=height1*width1;       //矩形面積
            cout << "Area of rectangle: " << rectangle << endl;

            break;

        case -1:
            break;

        default:    //不是1,2,3,-1就印出Illegal input
            cout << "Illegal input!" <<endl;
            break;
        }
    }
    while(mode!=-1);


    return 0;
}
