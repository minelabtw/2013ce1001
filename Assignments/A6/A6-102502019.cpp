#include<iostream>

using namespace std;

int main()
{
    int x=0;
    double circle=3.14159,r=0,height1=0,height2=0,width1=0,width2=0;

    cout <<"Calculate the area.\n";
    cout <<"Choose the mode.\n";
    cout <<"Area of circle=1.\n";
    cout <<"Area of triangle=2.\n";
    cout <<"Area of rectangle=3.\n";
    cout <<"Exit=-1.\n";
    cout <<"Enter mode: ";
    cin >>x;
    while (x!=-1 && x!=1 && x!=2 && x!=3)
    {
        cout <<"Illegal input!\n"<< "Enter mode: ";
        cin >>x;
    }
    while (x!=-1)
    {

        switch (x)
        {
        case 1:
        {
            cout <<"Enter radius: ";
            cin >>r;
            while (r<0)
            {
                cout <<"Out of range!\n"<<"Enter radius: ";
                cin >>r;
            }
            cout <<"Area of circle: "<< r*r*circle;
            x=0;
            cout <<"\nEnter mode: ";
            cin >>x;
            while (x!=-1 && x!=1 && x!=2 && x!=3)
            {
                cout <<"Illegal input!\n"<< "Enter mode: ";
                cin >>x;
            }
        }
        break;
        case 2:
        {
            cout <<"Enter width: ";
            cin >>width1;
            while (width1<0)
            {
                cout <<"Out of range!\n"<<"Enter width: ";
                cin >>width1;
            }
            cout <<"Enter height: ";
            cin >>height1;
            while (height1<0)
            {
                cout <<"Out of range!\n"<<"Enter height: ";
                cin >>height1;
            }
            cout <<"Area of triangle: "<<width1*height1*0.5;
            x=0;
            cout <<"\nEnter mode: ";
            cin >>x;
            while (x!=-1 && x!=1 && x!=2 && x!=3)
            {
                cout <<"Illegal input!\n"<< "Enter mode: ";
                cin >>x;
            }
        }
        break;
        case 3:
        {
            cout <<"Enter width: ";
            cin >>width2;
            while (width2<0)
            {
                cout <<"Out of range!\n"<<"Enter width: ";
                cin >>width2;
            }
            cout <<"Enter height: ";
            cin >>height2;
            while (height2<0)
            {
                cout <<"Out of range!\n"<<"Enter height: ";
                cin >>height2;
            }
            cout <<"Area of rectangle: "<< width2*height2;
            x=0;
            cout <<"\nEnter mode: ";
            cin >>x;
            while (x!=-1 && x!=1 && x!=2 && x!=3)
            {
                cout <<"Illegal input!\n"<< "Enter mode: ";
                cin >>x;
            }
        }
        break;
        }
    }
}


