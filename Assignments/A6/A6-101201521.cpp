#include <iostream>
using namespace std;

int main()
{
    double pi=3.14159, wr=0, h=0;   //宣告變數為實數型態，pi 為圓周率、wr 為 width 或 radius、h為height

    cout << "Calculate the area.\n"     //功能說明
         << "Choose the mode.\n"
         << "Area of circle=1.\n"
         << "Area of triangle=2.\n"
         << "Area of rectangle=3.\n"
         << "Exit=-1.\n";
    while(1)
    {
        int flag=0, mode=0;
        double input=0;
        cout << "Enter mode: ";     //模式輸入
        cin >> mode;
        if(mode==-1)    //模式輸入-1則結束程式
            break;
        else if(mode==1 || mode==2 || mode==3)    //各模式內容
        {
            while(flag<2)
            {
                if(flag==0)
                {
                    cout << (mode==1 ? "Enter radius: " : "Enter width: ");     //按照模式輸出要求
                    cin >> input, wr=input;
                }
                else
                {
                    cout << "Enter height: ";
                    cin >> input, h=input;
                }
                if(input<=0)
                    cout << "Out of range!\n";
                else if(mode==1)    //mode為1時只要求輸入一個值
                    break;
                else
                    flag++;
            }
            if(mode==1)     //各模式輸出
                cout << "Area of circle: " << wr*wr*pi << endl;
            else if(mode==2)
                cout << "Area of triangle: " << wr*h/2 << endl;
            else if(mode==3)
                cout << "Area of rectangle: " << wr*h << endl;
        }
        else
            cout << "Illegal input!\n";
    }
    return 0;
}
