#include<iostream>
using namespace std;

int main ()
{
    int mode =0;                      //不能用double
    double radius =0;                  //非零正數(double形態)
    double width =0;
    double height =0;

    cout <<"Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";

    while(mode != -1)                        //重複計算
    {
        cout <<"Enter mode: ";
        cin >> mode;
        switch (mode)                       //用switch判斷 模式
        {
        case 1:
            cout <<"Enter radius: ";
            cin >> radius;
            while (radius <= 0)                   //非零正數
            {
                cout <<"Out of range!\nEnter radius: ";
                cin >> radius;
            }
            cout <<"Area of circle: "<<radius*radius*3.14159<<endl;          //圓周率r平方
            break;

        case 2:
            cout <<"Enter width: ";
            cin >> width;
            while (width <= 0)                     //非零正數
            {
                cout <<"Out of range!\nEnter width: ";
                cin >> width;
            }
            cout <<"Enter height: ";
            cin >> height;
            while (height <= 0)                     //非零正數
            {
                cout <<"Out of range!\nEnter height: ";
                cin >> height;
            }
            cout <<"Area of triangle: "<<(width*height)/2<<endl;
            break;

        case 3:
            cout <<"Enter width: ";
            cin >> width;
            while (width <= 0)
            {
                cout <<"Out of range!\nEnter width: ";
                cin >> width;
            }
            cout <<"Enter height: ";
            cin >> height;
            while (height <= 0)
            {
                cout <<"Out of range!\nEnter height: ";
                cin >> height;
            }
            cout <<"Area of rectangle: "<<width*height<<endl;
            break;

        case -1:                                   //-1結束
            break;

        default:                                     //非 1,2,3,-1
            cout <<"Illegal input!\n";                    //除了輸入
            break;
        }
    }
    return 0;
}
