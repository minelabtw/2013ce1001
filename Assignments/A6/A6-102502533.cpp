#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int Enter_mode=0;//宣告整數
    double Enter_radius=0,Enter_width=0,Enter_height=0,Area_of_circle=0,Enter_radiu=0,Area_of_triangle=0,Area_of_rectangle=0;
    //宣告小數
    cout<<"Calculate the area."<<"\nChoose the mode."<<"\nArea of circle=1."<<"\nArea of triangle=2."
        <<"\nArea of rectangle=3."<<"\nExit=-1."<<endl;
    cout<<"Enter mode: ";//輸入模式
    cin>>Enter_mode;

    while( Enter_mode !=-1)
    {
        switch(Enter_mode)
        {
        case 1://輸入1跑出圓面積
            cout<<"Enter radius: ";
            cin>>Enter_radius;
            while(Enter_radius<=0)
            {
                cout<<"Out of range!";
                cout<<"\nEnter radius: ";
                cin>>Enter_radius;
            }
            Area_of_circle = Enter_radius*Enter_radius*3.14159;
            cout<<"Area of circle: "<<Area_of_circle<<endl;
            cout<<"Enter mode: ";
            cin>>Enter_mode;
            break;

        case 2://輸入2跑出三角形面積
            cout<<"Enter width: ";
            cin>>Enter_width;
            while(Enter_width<=0)
            {
                cout<<"Out of range!";
                cout<<"\nEnter width: ";
                cin>>Enter_width;
            }
            cout<<"Enter height: ";
            cin>>Enter_height;
            while(Enter_height<=0)
            {
                cout<<"Out of range!";
                cout<<"\nEnter height: ";
                cin>>Enter_height;
            }
            Area_of_triangle= Enter_height*Enter_width/2;
            cout<<"Area of triangle: "<<Area_of_triangle<<endl;
            cout<<"Enter mode: ";
            cin>>Enter_mode;
            break;

        case 3://輸入3跑出矩形面積
            cout<<"Enter width: ";
            cin>>Enter_width;
            while(Enter_width<=0)
            {
                cout<<"Out of range!";
                cout<<"\nEnter width: ";
                cin>>Enter_width;
            }
            cout<<"Enter height: ";
            cin>>Enter_height;
            while(Enter_height<=0)
            {
                cout<<"Out of range!";
                cout<<"\nEnter height: ";
                cin>>Enter_height;
            }
            Area_of_rectangle =Enter_height*Enter_width;
            cout<<"Area of rectangle: "<<Area_of_rectangle<<endl;
            cout<<"Enter mode: ";
            cin>>Enter_mode;
            break;

        case -1://輸入-1結束程式
            break;

        default://除1.2.3.-1之外,其他要求重新輸入
            cout<<"Illegal input!";
            cout<<"\nEnter mode: ";
            cin>>Enter_mode;
        }
    }
    return 0;

}
