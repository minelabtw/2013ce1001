#include<iostream>
using namespace std;
void inputandcheck(int number,int mode,double &input)
{
    do
    {
        if(mode==1)
            cout << "Enter radius: ";
        else if(number==0)
            cout <<"Enter width: ";
        else
            cout <<"Enter height: ";
        cin >> input;
    }
    while(input <=0 && cout <<"Out of range!\n");
}
int main()
{
    double pi=3.14159;
    double input1,input2;                    // input1 = (r or width )   input2 = height
    int mode;
    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";
    do
    {
        cout << "Enter mode: ";
        cin >> mode;
        switch(mode)
        {
        case 1:
            inputandcheck(0,1,input1);                                        //input radius
            cout << "Area of circle: "  << (input1*input1)*pi << endl;        // r*r*(pi)
            break;
        case 2:
        case 3:
            inputandcheck(0,2,input1);                                       //input height
            inputandcheck(1,2,input2);                                       //input width
            if(mode==2)
                cout << "Area of triangle: " << (input1*input2)/2 << endl;     //width*height/2
            else
                cout << "Area of rectangle: " << input1*input2 << endl;        //width*hight
            break;
        default:
            cout << "Illegal input!\n";
        }
    }
    while(mode!=-1);
    return 0;
}
