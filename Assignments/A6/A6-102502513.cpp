#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    cout << "Calculate the area.\n";
    cout << "Choose the mode.\n";
    cout << "Area of circle=1.\n";
    cout << "Area of triangle=2.\n";
    cout << "Area of rectangle=3.\n";
    cout << "Exit=-1.\n";

    int mode=0;

    do
    {
        cout << "Enter mode: ";
        cin >> mode;

        switch(mode)
        {
        case 1:
        {
            double Pi=3.14159 , radius=0, area1=0;

            while(radius<=0)
            {
                cout << "Enter radius: ";
                cin >> radius;

                if(radius<=0)
                {
                    cout << "Out of range!\n";
                }
            }

            area1 = radius*radius*Pi;  //求圓形面積.
            cout << "Area of circle: " << area1 << endl;
            break;
        }

        case 2:
        {
            double height=0 , width=0 , area2=0;

            while(width<=0)
            {
                cout << "Enter width: ";
                cin >> width;

                if(width<=0)
                {
                    cout << "Out of range!\n";
                }
            }

            while(height<=0)
            {
                cout << "Enter height: ";
                cin >> height;

                if(height<=0)
                {
                    cout << "Out of range!";
                }
            }

            area2 = width*height*0.5; //求三角形面積.
            cout << "Area of triangle: " << area2 << endl;
            break;
        }

        case 3:
        {
            double width=0 , height=0 , area3=0;

            while(width<=0)
            {
                cout << "Enter width: ";
                cin >> width;

                if(width<=0)
                {
                    cout << "Out of range!\n";
                }
            }

            while(height<=0)
            {
                cout << "Enter height: ";
                cin >> height;

                if(height<=0)
                {
                    cout << "Out of range!";
                }
            }

            area3 = width*height;  //求矩形面積.
            cout << "Area of rectangle: " << area3 << endl;
            break;
        }

        case -1:
        {
            break;
        }

        default :
        {
            cout << "Illegal input!\n";
            break;
        }
        }
    }
    while(mode!=-1);

    return 0;
}
