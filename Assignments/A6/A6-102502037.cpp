#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

int main()
{
    int w;  //設定所需變數與pi
    double a,b,c,v;
    double pi = 3.14159;
    cout<<"Calculate the area."<<endl<<"Choose  the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl<<"Area of rectangle=3."<<endl<<"Exit=-1.";
    //輸出說明
    for(v=0 ; v<=1 ; v++) //建立迴圈以能重複輸入
    {   cout<<endl<<"Enter mode: ";
        cin>>w; //輸入w
        switch(w) //判定w是否合格並且該進入何種運算
        {

        case 1: //算圓的面積
            cout<<"Enter radius: ";
            cin>>a;
            while(a<=0) //判斷數字是否超出範圍
            {
                cout<<"Out of range!"<<endl<<"Enter radius: ";
                cin>>a;
            }
                b=pow(a,2)*pi;
                cout<<"Area of circle: "<<b;
                v=0;
            break;

        case 2: //算三角形面積
            cout<<"Enter width: ";
            cin>>a;
            while(a<=0) //判斷數字是否合格
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>a;
            }
            cout<<"Enter height: ";
            cin>>b;
            while(b<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>b;
            }
                c=a*b/2;
                cout<<"Area of triangle: "<<c;
                v=0;
            break;

        case 3: //算三角形面積
            cout<<"Enter width: ";
            cin>>c;
            while(c<=0) //檢查數字是否合格
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>c;
            }
           cout<<"Enter height: ";
            cin>>a;
            while(a<=0) //檢查數字是否合格
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>a;
            }
            b=a*c;
            cout<<"Area of rectangle: "<<b;
            v=0;
            break;

        case -1: //若輸入-1則結束運算
        v=1;
        break;

        default: //不合格輸出"Illegal input!"
        {
            cout<<"Illegal input!";
            v=0;
        }
        }
    }
    return 0;
}
