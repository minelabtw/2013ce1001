#include <iostream>

using namespace std;

int main()
{
    double i=0;
    double n=0;
    double r=0;
    double Pi=3.14159;
    double w=0;
    double h=0;
    double l=0;


    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;


    while(i<1)
    {
        cout << "Enter mode: ";
        cin >> n;
        if(n==1)
        {
            cout << "Enter radius: ";
            cin >> r;
            while(r<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter radius: ";
                cin >> r;
            }
            cout << "Area of circle: " << r*r*Pi << endl;
        }


        else if(n==2)
        {
            cout << "Enter width: ";
            cin >> w;
            while(w<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> w;
            }


            cout << "Enter height: ";
            cin >> h;
            while(h<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> h;
            }
            cout << "Area of triangle: " << w*h*0.5 << endl;
        }


        else if(n==3)
        {
            cout << "Enter width: ";
            cin >> w;
            while(w<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> w;
            }


            cout << "Enter height: ";
            cin >> l;
            while(l<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> l;
            }
            cout << "Area of rectangle: " << w*l << endl;
        }


        else if(n==-1)
        {
            i++;
        }


        else
        {
            cout << "Illegal input!" << endl;
        }
    }


    return 0;
}
