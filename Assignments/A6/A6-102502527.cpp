#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

int main()
{
    int mode = 0;
    double r = 0;
    double pi = 3.14159;
    double l = 0;
    double h = 0;
    double a = 0;
    double b = 0;
    double area = 0;

    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;

    cout << "Enter mode: ";//輸出字彙並將輸入的值給MODE
    cin >> mode;

    while ( mode != -1 )//做出迴圈
    {
        switch ( mode )
        {
        case 1:
            cout << "Enter radius: ";
            cin >> r;
            while ( r <= 0 )//當R不是正數時做以下動作
            {
                cout << "Out of range!" << endl;
                cout << "Enter radius: ";
                cin >> r;
            }

            area = pi * pow(r,2);//設定方程式

            cout << "Area of circle: " << area << endl;
            cout << "Enter mode: ";
            cin >> mode;
            break;//結束此方案

        case 2:
            cout << "Enter width: ";
            cin >> l;
            while ( l <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> l;
            }

            cout << "Enter height: ";
            cin >> h;
            while ( h <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> h;
            }

            area = 0.5 * l * h;

            cout << "Area of triangle: " << area << endl;
            cout << "Enter mode: ";
            cin >> mode;
            break;

        case 3:
            cout << "Enter width: ";
            cin >> a;
            while ( a <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> a;
            }

            cout << "Enter height: ";
            cin >> b;
            while ( b <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> b;
            }

            area = a * b;

            cout << "Area of rectangle: " << area << endl;
            cout << "Enter mode: ";
            cin >> mode;
            break;

        case -1:
            break;//為-1時直接結束

        default://mode為其餘數值時重新輸入
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> mode;
        }
    }

    return 0;
}

