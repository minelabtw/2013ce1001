#include <iostream>
#include <stdio.h>//為了使用pow函式
#include <math.h>//為了使用pow函式
using namespace std;

int main()
{
    int mode;
    double x=0,y=0,pi=3.14159,A=0;
    cout<<"Calculate the area."<<endl<<"Choose the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl<<"Area of rectangle=3."<<endl<<"Exit=-1."<<endl;//解說此程式
    cout<<"Enter mode: ";//輸入模式
    cin>>mode;
    //當模式不是-1時，進入程式
    while(mode!=-1)
    {
        switch(mode)
        {
        case 1://模式1
            cout<<"Enter radius: ";
            cin>>x;//輸入半徑並檢查其值是否在範圍內
            while(x<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>x;
            }
            A=pi*pow(x,2);//計算面積
            cout<<"Area of circle: "<<A<<endl;//輸出面積
            break;

        case 2://模式2
            cout<<"Enter width: ";
            cin>>x;//輸入寬並檢查其值是否在範圍內
            while(x<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>x;
            }

            cout<<"Enter height: ";
            cin>>y;//輸入高並檢查其值是否在範圍內
            while(y<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>y;
            }

            A=x*y*0.5;//計算面積
            cout<<"Area of triangle: "<<A<<endl;//輸出面積
            break;

        case 3:
            cout<<"Enter width: ";
            cin>>x;//輸入寬並檢查其值是否在範圍內
            while(x<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>x;
            }

            cout<<"Enter height: ";
            cin>>y;//輸入高並檢查其值是否在範圍內
            while(y<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>y;
            }

            A=x*y;//計算面積
            cout<<"Area of circle: "<<A<<endl;//輸出面積
            break;

        default:
            cout<<"Illegal input!"<<endl;//當mode在範圍外時，顯示錯誤
            break;

        }
        cout<<"Enter mode: ";
        cin>>mode;//使程式可重複計算面積
    }
    return 0;
}
