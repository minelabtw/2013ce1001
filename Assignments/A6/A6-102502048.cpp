#include<iostream>
using namespace std;
int main()
{
    int a=0;//宣告變數
    double r=0,w=0,h=0;//宣告變數
    const double pi=3.14159F;//宣告變數
    cout<<"Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";//顯示字串
    do//先執行後判斷迴圈
    {
        cout<<"Enter mode: ";//顯示字串
        cin>>a;//輸入
        while(a!=1 && a!=2 && a!=3 && a!=-1)
        {
            cout<<"Illegal input!";//顯示字串
            cin>>a;//輸入
        }
        switch(a)//判斷a值
        {
        case 1://等於1
        {
            do//先執行後判斷迴圈
            {
                cout<<"Enter radius: ";//顯示字串
                cin>>r;//輸入
                cout<<"Out of range! \n";//顯示字串
            }
            while(r<=0);//不讓值小於0
            cout<<"Area of circle: "<<r*r*pi;
            break;
        }
        case 2://等於2
        {
            do//先執行後判斷迴圈
            {
                cout<<"Enter width: ";//顯示字串
                cin>>w;//輸入
                cout<<"Out of range! \n";//顯示字串
            }
            while(w<=0);//不讓值小於0
            do//先執行後判斷迴圈
            {
                cout<<"Enter height: ";//顯示字串
                cin>>h;//輸入
                cout<<"Out of range! \n";//顯示字串
            }
            while(h<=0);//不讓值小於0
            cout<<"Area of triangle: "<<w*h/2;//顯示字串
            break;
        }
        case 3://等於3
        {
            do//先執行後判斷迴圈
            {
                cout<<"Enter width: ";//顯示字串
                cin>>w;//輸入
                cout<<"Out of range! \n";//顯示字串
            }
            while(w<=0);//不讓值小於0
            do//先執行後判斷迴圈
            {
                cout<<"Enter height: ";//顯示字串
                cin>>h;//輸入
                cout<<"Out of range! \n";//顯示字串
            }
            while(h<=0);//不讓值小於0
            cout<<"Area of rectangle: "<<w*h;//顯示字串
            break;
        }
        default://結束 等於-1
            break;
        }
        cout<<endl;//換行
    }
    while(a==1 || a==2 || a==3);//迴圈
    return 0;
}
