#include <iostream>
using namespace std;

int main()
{
	//display infomation of mode
	cout << "Calculate the area.\n"
	<< "Choose the mode.\n"
	<< "Area of circle=1.\n"
	<< "Area of triangle=2.\n"
	<< "Area of rectangle=3.\n"
	<< "Exit=-1.\n";

	while(1){
		int mode;
		cout << "Enter mode: "; cin >> mode;
		if(mode==-1){break;}				//mode -1: exit program
		while(mode!=1 && mode!=2 && mode!=3){
			//warn the user and re-input if mode is illegal
			cout << "Illegal input!\n";
			cout << "Enter mode: "; cin >> mode;
		}

		double r, w, h ,pi=3.14159;

		switch(mode)
		{
			case 1:			//mode 1: Area of circle

			while(1){
				cout << "Enter radius: "; cin >> r;
				if(r<=0){cout << "Out of range!\n"; continue;}
				break;
			}

			cout << "Area of circle: " << r*r*pi << "\n";

			break;

			case 2:			//mode 2: Area of triangle

			while(1){
				cout << "Enter width: "; cin >> w;
				if(w<=0){cout << "Out of range!\n"; continue;}
				break;
			}
			while(1){
				cout << "Enter height: "; cin >> h;
				if(h<=0){cout << "Out of range!\n"; continue;}
				break;
			}

			cout << "Area of triangle: " << w*h/2 << "\n";

			break;

			case 3:			//mode 3: Area of rectangle

			while(1){
				cout << "Enter width: "; cin >> w;
				if(w<=0){cout << "Out of range!\n"; continue;}
				break;
			}
			while(1){
				cout << "Enter height: "; cin >> h;
				if(h<=0){cout << "Out of range!\n"; continue;}
				break;
			}

			cout << "Area of rectangle: " << w*h <<"\n";

			break;

			default:

			;//break;
		}
	}

	return 0;
}
