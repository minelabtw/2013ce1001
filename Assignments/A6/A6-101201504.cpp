#include<iostream>
using namespace std ;

int a ;
int main()
{
    cout <<"Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n" ;
    while(a!=-1)
    {

        float radius , width , height ;
        cout <<"Enter mode: " ;
        cin >>a ;
        if(a==1)         //算圓面積
        {
            double pi =3.14159 ;
            cout <<"Enter radius: " ;
            cin >>radius ;
            while(radius <=0)
            {
                cout <<"Out of range!\n" ;
                cout <<"Enter radius: " ;
                cin >>radius ;
            }

            cout<<"Area of circle: "<< radius*radius*pi<<endl ;

        }
        else if(a==2)     //算三角形面積
        {
            cout<<"Enter width: ";
            cin >>width ;
            while(width <=0)
            {
                cout <<"Out of range!\n" ;
                cout <<"Enter width: " ;
                cin >>width ;
            }
            cout <<"Enter height: ";
            cin >>height ;
            while(height <=0)
            {
                cout <<"Out of range!\n" ;
                cout <<"Enter height: " ;
                cin >>height ;
            }
            cout<< "Area of triangle: "<< width*height/2<<endl  ;
        }
        else if(a==3)    //算矩行面積
        {
            cout<<"Enter width: ";
            cin >>width ;
            while(width <=0)
            {
                cout <<"Out of range!\n" ;
                cout <<"Enter width: " ;
                cin >>width ;
            }
            cout <<"Enter height: ";
            cin >>height ;
            while(height <=0)
            {
                cout <<"Out of range!\n" ;
                cout <<"Enter height: " ;
                cin >>height ;
            }
            cout <<"Area of rectangle: "<<width*height<<endl ;
        }
        else if (a==-1)     //結束
            break ;
        else           //其餘狀況
        {
            cout <<"Illegal input!\n" ;
        }
    }
    return 0 ;
}

