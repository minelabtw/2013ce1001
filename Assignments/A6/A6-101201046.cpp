#include <iostream>

using namespace std;

/*INPUT*/
#define INPUT(x) \
            do {\
                cout << "Enter " << #x << ": ";\
                cin >> x;\
                }while(x <= 0.0 && cout << "Out of range!\n")
/*OUTPUT*/
#define OUTPUT(x) cout << "Area of " << #x  << ": "<< x << endl



int main(void) {
    const double pi = 3.14159;
    int mode;
    double width, height, radius;
    double rectangle, triangle, circle;

    cout << "Calculate the area.\n"
            "Choose the mode.\n"
            "Area of circle=1.\n"
            "Area of triangle=2.\n"
            "Area of rectangle=3.\n"
            "Exit=-1.\n" ;

    while(1) {
        cout << "Enter mode: "; //enter mode
        cin >> mode;

        if (mode == -1)
            break;
        else if (mode == 2) { //caculate area of triangle
            INPUT(width);
            INPUT(height);

            triangle = width * height / 2.0;

            OUTPUT(triangle);
            }
        else if (mode == 3){ //caculate area of rectangle
            INPUT(width);
            INPUT(height);

            rectangle = width * height;

            OUTPUT(rectangle);
            }
        else if (mode == 1) { //caculate area of circle
            INPUT(radius);

            circle = radius * radius * pi;

            OUTPUT(circle);
            }
        else //illegal input mode
            cout << "Illegal input!\n";
        }

    return 0;
    }
