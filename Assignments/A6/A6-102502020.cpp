#include <iostream>

using namespace std;

int main()
{
    double area=0;                                          //宣告變數
    double radius=0;
    double width=0;
    double height=0;
    double pi=3.14159;
    int i=0;
    int j=0;


    cout << "Calculate the area." << endl                   //輸出字串
         << "Choose the mode." << endl
         << "Area of circle=1." << endl
         << "Area of triangle=2." << endl
         << "Area of rectangle=3." << endl
         << "Exit=-1." << endl;

    while(i==0)                                             //用i判斷是否進行迴圈
    {
        cout << "Enter mode: ";
        cin >> j;
        switch(j)                                           //用j的值判斷進行哪個動作
        {
        case 1:                                             //當j=1時
        {
            while(i==0)
            {
                cout << "Enter radius: ";
                cin >> radius;
                if(radius<=0)                               //如果radius<=0，則執行以下動作。
                    cout << "Out of range!" << endl;
                else
                {
                    area=radius*radius*pi;                  //計算圓面積
                    cout << "Area of circle: " << area;
                    i++;                                    //i=1
                }
            }
            cout << endl;
            i=0;
            break;                                          //跳出switch
        }
        case 2:                                             //當i=2時
        {
            while(i==0)
            {
                cout << "Enter width: ";
                cin >> width;
                if(width<=0)
                    cout << "Out of range!" << endl;
                else i++;                                   //i=1
            }
            while(i==1)
            {
                cout << "Enter height: ";
                cin >> height;
                if(height<=0)
                    cout << "Out of range!" << endl;
                else
                {
                    area=width*height*0.5;                  //計算三角形面積
                    cout << "Area of triangle: " << area;
                    i++;                                    //i=2
                }
            }
            cout << endl;
            i=0;
            break;
        }
        case 3:                                             //當i=3時
        {
            while(i==0)
            {
                cout << "Enter width: ";
                cin >> width;
                if(width<=0)
                    cout << "Out of range!" << endl;
                else i++;                                   //i=1
            }
            while(i==1)
            {
                cout << "Enter height: ";
                cin >> height;
                if(height<=0)
                    cout << "Out of range!" << endl;
                else
                {
                    area=width*height;                      //計算矩形面積
                    cout << "Area of rectangle: " << area;
                    i++;                                    //i=2
                }
            }
            cout << endl;
            i=0;
            break;
        }
        case -1:                                            //當i=-1時
        {
            i=1;                                            //不用再執行while
            break;
        }
        default:                                            //當j等於其他值時
        {
            cout << "Illegal input!" << endl;
            i=0;
            break;
        }
        }
    }
    return 0;
}
