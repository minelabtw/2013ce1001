#include<iostream>
using namespace std;
int main()
{
    int mode=0;
    float r=0,circlearea=0,width=0,height=0,trianglearea=0,rectanglearea=0;  //宣告型別可為小數的半徑 長 寬 面積
    cout<<"Calculate the area.\n"<<"Choose the mode.\n"<<"Area of circle=1.\n"<<"Area of triangle=2.\n"<<"Area of rectangle=3.\n"<<"Exit=-1.\n";  //輸出題目
    while(mode!=-1)
    {
        cout<<"Enter mode: ";
        cin>>mode;
        switch(mode)  //選擇題目
        {
        case 1 :
            cout<<"Enter radius: ";
            cin>>r;
            while(r<=0)  //判斷是否符合條件
            {
                cout<<"Out of range!\n"<<"Enter radius: ";
                cin>>r;
            }
            circlearea=r*r*3.14159;  //計算面積
            cout<<"Area of circle: "<<circlearea;  //輸出面積
            cout<<endl;
            break;
        case 2 :
            cout<<"Enter width: ";
            cin>>width;
            while(width<=0)  //判斷是否符合條件
            {
                cout<<"Out of range!\n"<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";
            cin>>height;
            while(height<=0)  //判斷是否符合條件
            {
                cout<<"Out of range!\n"<<"Enter height: ";
                cin>>height;
            }
            trianglearea=0.5*height*width;  //計算面積
            cout<<"Area of triangle: "<<trianglearea;  //輸出面積
            cout<<endl;
            break;
        case 3 :
            cout<<"Enter width: ";
            cin>>width;
            while(width<=0)  //判斷是否符合條件
            {
                cout<<"Out of range!\n"<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";
            cin>>height;
            while(height<=0)  //判斷是否符合條件
            {
                cout<<"Out of range!\n"<<"Enter  height: ";
                cin>>height;
            }
            rectanglearea=width*height;  //計算面積
            cout<<"Area of rectangle: "<<rectanglearea;  //輸出面積
            cout<<endl;
            break;
        case -1 :
            break;  //結束計算
        default:
            cout<<"Illegal input!";  //錯誤輸入
            break;
        }
    }
    return 0;
}
