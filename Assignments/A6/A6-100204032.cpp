#include<iostream>

using namespace std;

int main()
{
    int mode;
    double area=0;
    double a=0,b=0,c=0,d=0,e=0;

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;


    do
    {
        cout<<"Enter mode: ";
        cin>>mode;

        switch(mode)
        {
        case 1:

            do
            {
                cout<<"Enter radius: ";
                cin>>a;
                if(a<=0)
                    cout<<"Illegal input! "<<endl;
            }
            while(a<=0);

            area=a*a*3.14159;
            cout<<"Area of circle: ";
            cout<<area<<endl;

            break;

        case 2:



            do
            {
                cout<<"Enter width: ";
                cin>>b;
                if(b<=0)
                    cout<<"Illegal input! "<<endl;
            }
            while(b<=0);

            do
            {
                cout<<"Enter height: ";
                cin>>c;
                if(c<=0)
                    cout<<"Illegal input! "<<endl;
            }
            while(c<=0);

            area=b*c/2;
            cout<<"Area of triangle=: ";
            cout<<area<<endl;

            break;

        case 3:

            do
            {
                cout<<"Enter width: ";
                cin>>d;
                if(d<=0)
                    cout<<"Illegal input! "<<endl;
            }
            while(d<=0);

            do
            {
                cout<<"Enter height: ";
                cin>>e;
                if(e<=0)
                    cout<<"Illegal input! "<<endl;
            }
            while(e<=0);

            area=d*e;
            cout<<"Area of rectangle: ";
            cout<<area<<endl;

            break;

        case-1:
            break;

        default:
            cout<<"Illegal input! "<<endl;
            break;
        }

    }
    while(mode!=-1);

    return 0;
}

