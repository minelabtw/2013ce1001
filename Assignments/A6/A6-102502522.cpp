#include <iostream>
#include<iomanip>
using namespace std;

int main()
{
    double radius;//宣告非0正數變數半徑
    double area;//宣告非0正數變數 面積
    double width;//宣告非0正數變數 寬
    double height;//宣告非0正數變數 高
    double triangle;//宣告非0正數變數 三角形
    double rectangle;//宣告非0正數變數 矩形
    int Entermode;//宣告正整數變數 模式

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

    while(Entermode!=-1)//輸入-1時退出迴圈
    {

     cout<<"Enter mode: ";
     cin>>Entermode;

     switch(Entermode)
     {
        case 1://模式一
            cout<<"Enter radius: ";
            cin>>radius;
            while(radius<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>radius;
            }
            area=radius*radius*3.14159;
            cout<<"Area of circle: "<<area<<endl;
            break;

        case 2://模式二
            cout<<"Enter width: ";
            cin>>width;
            while(width<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";
            cin>>height;
            while(height<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>height;
            }
            area=(height*width)*0.5;
            cout<<"Area of triangle: "<<area<<endl;
            break;

        case 3://模式三
            cout<<"Enter width: ";
            cin>>width;
            while(width<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";
            cin>>height;
            while(height<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>height;
            }
            area=(height*width);
            cout<<"Area of rectangle: "<<area<<endl;
            break;

        case -1://退出模式
            cout<<" ";
            break;

        default://輸入值不符合時
            cout<<"Illegal input!"<<endl;
            break;

       }
    }

    return 0;
}
