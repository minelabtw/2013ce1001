#include <iostream>
using namespace std;

int main()
{
    int mode = 1;
    double rad = 0;
    double wid = 0;
    double hei = 0;
    double cir;
    double tri;
    double rec;


    cout << "Calculate the area.\n"
         << "Choose the mode.\n"
         << "Area of circle=1.\n"
         << "Area of triangle=2.\n"
         << "Area of rectangle=3.\n"
         << "Exit=-1.\n";

    while ( mode != -1)
    {
        cout << "Enter mode: ";
        cin >> mode;

        switch (mode)
        {
        case 1:
            {
                while ( rad <= 0 )
                {
                    cout << "Enter radius: ";
                    cin >> rad;
                     if ( rad <= 0)
                    cout << "\aOut of range!\n";
                }
                cir = (3.14159)* (rad) * (rad); // calculate the area.
                cout << "Area of circle: " << cir << endl;
                rad = 0; // initialize the number.
                break;
            }
        case 2:
            {
                while ( wid <= 0 )
                {
                    cout << "Enter width: ";
                    cin >> wid;
                     if ( wid <= 0)
                    cout << "\aOut of range!\n";
                }
                while ( hei <= 0 )
                {
                    cout << "Enter height: ";
                    cin >> hei;
                     if ( hei <= 0)
                    cout << "\aOut of range!\n";
                }
                tri = ( wid * hei ) / 2; // calculate the area.
                cout << "Area of triangle: " << tri << endl;
                wid = 0; // initialize the number.
                hei = 0; // initialize the number.
                break;
            }
            case 3:
            {
             while ( wid <= 0 )
                {
                    cout << "Enter width: ";
                    cin >> wid;
                     if ( wid <= 0)
                    cout << "\aOut of range!\n";
                }
                while ( hei <= 0 )
                {
                    cout << "Enter height: ";
                    cin >> hei;
                     if ( hei <= 0)
                    cout << "\aOut of range!\n";
                }
                rec = wid * hei; // calculate the area.
                cout << "Area of rectangle: " << rec << endl;
                wid = 0; // initialize the number.
                hei = 0; // initialize the number.
                break;
        }
        case -1:
        {
            cout << endl;
            break; // EOF
        }


        default:
        {
            cout << "\aIlligal input!\n";
            break;
        }
        }
    }
     return 0;

}
