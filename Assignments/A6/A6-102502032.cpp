#include <iostream>
#include <string>
using namespace std;

//ask for radius, width or height; ref: main.data[]
void ask_data( string str, double &input )
{
    do
    {
        cout << "Enter " << str << ": ";
        cin >> input;
    }
    while( input <= 0 and cout << "Out of range!" << endl );
}

int main()
{
    //decalaration and initialization
    int mode = 0;
    float mode_t = 0;       //temp
    //double pi = 3.14159;
    double data[3];         //[0] radius [1] width [2] height
    for (int i = 0; i <= 2; i ++ )
        data[i] = 0;

    //ask for mode
    do
    {
        cout << "Enter mode: ";
        cin >> mode_t;
        mode = mode_t;

        //if input is not integer, set mode as illegal input
        if ( static_cast <float> (mode) != mode_t )
            mode = 0;

        //ask for data and output
        switch ( mode )
        {
        case -1:
            break;

        case 1:
            ask_data( "radius", data[0] );
            cout << "Area of circle: " << data[0] * data[0] *3.14159 << endl;
            break;

        case 2:
            ask_data( "width", data[1] );
            ask_data( "height", data[2] );
            cout << "Area of triangle: " << 0.5 * data[1] * data[2] << endl;
            break;

        case 3:
            ask_data( "width", data[1] );
            ask_data( "height", data[2] );
            cout << "Area of rectangle: " << data[1] * data[2] << endl;
            break;

        default:
            cout << "Illegal input!" <<endl;
            break;
        }
    }
    while( mode != -1 );

    return 0;
}
