#include<iostream>
using namespace std;
int main()
{
    int a;//宣告變數
    cout<<"Calculate the area.\n";
    cout<<"Choose the mode.\n";
    cout<<"Area of circle=1.\n";
    cout<<"Area of triangle=2.\n";
    cout<<"Area of rectangle=3.\n";
    cout<<"Exit=-1.\n";
    cout<<"Enter mode: ";
    cin>>a;//輸入a的值
    while(1)
    {
        if(a!=-1)//若a非-1則執行以下程式
        {
            while(a!=1&&a!=2&&a!=3)//若a亦非1或2或3則進入迴圈
            {
                cout<<"Illegal input!\n";
                cout<<"Enter mode: ";
                cin>>a;//重新給定a的值
            }
            if(a==1)//若a等於1則執行下列程式
            {
                float radius;//宣告變數
                float area;
                cout<<"Enter radius: ";
                cin>>radius;//輸入radius
                while(radius<=0)//radius小於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"Enter radius: ";
                    cin>>radius;//重新給定radius
                }
                area=(float)(radius*radius*3.14159);
                cout<<"Area of circle: "<<area;

            }
            else if(a==2)//若a=2則執行下列程式
            {
                float width;//宣告變數
                float height;
                float area;
                cout<<"Enter width: ";
                cin>>width;//給定width
                while(width<=0)//若width小於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"Enter width: ";
                    cin>>width;//重新給定width
                }
                cout<<"Enter height: ";
                cin>>height;//給定height
                while(height<=0)//若height小於等於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"Enter height: ";
                    cin>>height;//重新給定height值
                }
                area=(float)(height*width*0.5);
                cout<<"Area of triangle: "<<area;

            }
            else
            {
                float width;//宣告變數
                float height;
                float area;
                cout<<"Enter width: ";
                cin>>width;//給定width
                while(width<=0)//若width小於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"Enter width: ";
                    cin>>width;//重新給定width
                }
                cout<<"Enter height: ";
                cin>>height;//給定height
                while(height<=0)//若height小於等於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"Enter height: ";
                    cin>>height;//重新給定height值
                }
                area=(float)(height*width);
                cout<<"Area of rectangle: "<<area;

            }

            cout<<"\nEnter mode: ";
            cin>>a;//重新給定a值判斷是否再次進入迴圈
        }
        else break;
    }
    return 0;
}
