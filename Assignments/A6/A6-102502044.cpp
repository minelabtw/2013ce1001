/*************************************************************************
  > File Name: A6-102502044.cpp
  > Author: rockwyc992
  > Mail: rockwyc992@gmail.com 
  > Created Time: 西元2013年10月31日 (週四) 17時40分12秒
 ************************************************************************/

#include <stdio.h>

/* define pi = 3.14159 */
#define PI 3.14159

/* function to check input */
inline int check_mode(int x);
inline int check_num(double x);

/* function to output area */
inline void circle(double radius);
inline void triangle(double width,double height);
inline void rectangle(double width,double height);

/* function to input number */
inline void input_int(const char *str, const char *error, int *tmp,int (*check)(int));
inline void input_double(const char *str, const char *error, double *tmp,int (*check)(double));

int main()
{
	/* print ui */
	puts("Calculate the area.");
	puts("Choose the mode.");
	puts("Area of circle=1.");
	puts("Area of triangle=2.");
	puts("Area of rectangle=3.");
	puts("Exit=-1.");

	/* declare all variables */
	int mode;
	double radius, width, height;
	
	for(;;)
	{
		/* input the mode */
		input_int("Enter mode: ", "Illegal input!", &mode, check_mode);

		/* check which mode */
		switch(mode)
		{
			case 1:
				/* input all variable */
				input_double("Enter radius: ", "Out of range!", &radius, check_num);
				
				/* output the area of circle */
				circle(radius);
				
				break;
			
			case 2:
				/* input all variable */
				input_double("Enter width: " , "Out of range!", &width  , check_num);
				input_double("Enter height: ", "Out of range!", &height , check_num);
				
				/* output the area of triangle */
				triangle(width, height);

				break;
			
			case 3:
				/* input all variable */
				input_double("Enter width: " , "Out of range!", &width  , check_num);
				input_double("Enter height: ", "Out of range!", &height , check_num);

				/* output the area of rectangle */
				rectangle(width, height);

				break;

			case -1:
				/* exit the program */
				return 0;
		}
	}
	return 0;
}

/* x means the number will be check */
inline int check_mode(int x)
{
	/* check x is in set{1, 2, 3, -1} */
	switch(x)
	{
		case 1:
		case 2:
		case 3:
		case -1:
			return 1;

		default:
			return 0;
	}
}

/* x means the number will be check */
inline int check_num(double x)
{
	/* check x is in range (0, unlimit) */
	return x > 0.0;
}

/*******************************************/
/* this function is used to input int      */
/*	                                       */
/*   str means the ui message			   */
/* error means the error message		   */
/*  *tmp means the point of input		   */
/* check means the function to check input */
/*******************************************/
inline void input_int(const char *str, const char *error, int *tmp, int (*check)(int))
{
	for(;;)
	{
		printf("%s", str);	// print ui

		scanf("%d", tmp);	// input number

		if(check(*tmp))		// check the input is in range
			return;
		else
			puts(error);
	}
}

/*******************************************/
/* this function is used to input double   */
/*	                                       */
/*   str means the ui message			   */
/* error means the error message		   */
/*  *tmp means the point of input		   */
/* check means the function to check input */
/*******************************************/
inline void input_double(const char *str, const char *error, double *tmp, int (*check)(double))
{
	for(;;)
	{
		printf("%s", str);	// print ui

		scanf("%lf", tmp);	// input number

		if(check(*tmp))		// check the input is in range
			return;
		else
			puts(error);
	}
}

inline void circle(double radius)
{
	/* output area */
	printf("Area of circle: %.4lf\n", radius * radius * PI);
}

inline void triangle(double width,double height)
{
	/* output area */
	printf("Area of triangle: %.4lf\n", width * height * 0.5);
}

inline void rectangle(double width,double height)
{
	/* output area */
	printf("Area of rectangle: %.4lf\n", width * height);
}
