#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int mode; //宣告mode為整數
    double radius,wide,length; //宣告 radius,wide,length為小數
    cout<<"Calculate the area."<<endl<<"Choose the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl<<"Exit=-1."<<endl<<"Enter mode: ";
    //輸出Calculate the area......等

    while(cin>>mode && mode!=-1) //輸入mode且當mode不等於-1時
    {
        switch(mode) //抓進mode
        {
        case 1: //當mode=1
            cout<<"Enter radius: ";
            cin>>radius;
            while(radius<=0) //當radius<=0時
            {
                cout<<"Out of range!"<<endl<<"Enter radius: ";
                cin>>radius;
            }
            cout<<"Area of circle: "<<pow(radius,2)*3.14159<<endl;
            break; //跳出迴圈
        case 2:
            cout<<"Enter width: ";
            cin>>wide;
            while(wide<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>wide;
            }
            cout<<"Enter height: ";
            cin>>length;
            while(length<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>length;
            }
            cout<<"Area of triangle: "<<length*wide/2<<endl;
            break;
        case 3:
            cout<<"Enter width: ";
            cin>>wide;
            while(wide<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>wide;
            }
            cout<<"Enter height: ";
            cin>>length;
            while(length<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>length;
            }
            cout<<"Area of rectangle: "<<length*wide<<endl;
            break;
        default: //抓入其他字元
            cout<<"Illegal input!"<<endl;
            break;
        }
        cout<<"Enter mode: ";
    }
    return 0;
}
