#include <iostream>
using namespace std;
int main()
{
    int mode;  //模式
    double r;  //圓半徑
    double pi=3.14159;  //圓周率
    double w;  //三角形底
    double h;  //三角形高
    double w2;  //矩形長
    double h2;  //矩形寬

    cout << "Calculate the area.\n";  //提示
    cout << "Choose the mode.\n";
    cout << "Area of circle=1.\n";
    cout << "Area of triangle=2.\n";
    cout << "Area of rectangle=3.\n";
    cout << "Exit=-1.\n";

    do
    {
        cout << "Enter mode: ";  //選擇模式
        cin >> mode;
        switch ( mode )
        {
        case 1:
            do{  //輸入半徑並判斷合理性
                cout << "Enter radius: ";
                cin >> r;
            }while( r<=0 && cout << "Out of range!\n");
            cout << "Area of circle: " << r*r*pi << endl;  //計算圓面積
            break;
        case 2:
            do{  //輸入底和高並判斷合理性
                cout << "Enter width: ";
                cin >> w;
            }while( w<=0 && cout << "Out of range!\n");
            do{
                cout << "Enter height: ";
                cin >> h;
            }while( h<=0 && cout << "Out of range!\n");
            cout << "Area of triangle: " << w*h/2 << endl;  //計算三角形面積
            break;
        case 3:
            do{  //輸入常和高並判斷合理性
                cout << "Enter width: ";
                cin >> w2;
            }while( w2<=0 && cout << "Out of range!\n");
            do{
                cout << "Enter height: ";
                cin >> h2;
            }while( h2<=0 && cout << "Out of range!\n");
            cout << "Area of triangle: " << w2*h2 << endl;  //計算矩形面積
            break;
        case -1:  //跳出
            break;
        default:
            cout << "Illegal input!\n";  //錯誤輸入
            break;
        }
    }while( mode!=-1 );

    return 0;
}
