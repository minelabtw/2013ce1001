#include <iostream>
#include <math.h>
using namespace std;

int main(){
    cout << "Calculate the area." << endl << "Choose the mode." << endl << "1 for Area of circle." << endl << "2 for Area of triangle." << endl << "3 for Area of rectangle." << endl << "-1 for Exit.";
    int mode = 0;

    do{
        cout << "Enter mode: " ;
        cin >> mode;

        switch(mode){
            case 1 :
                double radius = 0;

                while(radius <= 0){
                    cout << "Enter radius: " << endl;
                    cin >> radius;
                    if(radius <= 0)
                        cout << "Out of range!" << endl;
                }
                cout << "Area of circle: " << 3.14159*radius^2 << endl;

                break;

            case 2 :
                double length, width = 0;

                while(length <= 0){
                    cout << "Enter length: ";
                    cin >> length;
                    if(length <= 0)
                        cout << "Out of range!" << endl;
                }
                while(width <= 0){
                    cout << "Enter width: ";
                    cin >> width;
                    if(width <= 0)
                        cout << "Out of range!" << endl;
                }
                cout << "Area of triangle: " << length*width/2 << endl;

                break;

            case 3 :
                double length, width = 0;

                while(length <= 0){
                    cout << "Enter length: ";
                    cin >> length;
                    if(length <= 0)
                        cout << "Out of range!" << endl;
                }
                while(width <= 0){
                    cout << "Enter width: ";
                    cin >> width;
                    if(width <= 0)
                        cout << "Out of range!" << endl;
                }
                cout << "Area of rectangle: " << length*width << endl;

                break;

            case -1 :
                break;

            default :
                cout << "Illegal input!" << endl;
                break;
        }
    }while(mode != -1);

    return 0;
}
