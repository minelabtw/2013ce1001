#include <iostream>

using namespace std;

const double PI=3.14159; // 題目規定的PI
double w,h,r; // 下列函數會用到的變數

// 因為輸入很麻煩
// 所以做了輸入用的函數
void input(const char *prompt,const char *error, double &n)
{
    do {
        cout << prompt;
        cin >> n;
    } while(n <= 0 && cout << error << endl);
    return;
}
//計算圓形面積
void circle()
{
    input("Enter radius: ","Out of range!",r);
    cout << "Area of circle: " << PI * r * r << endl;
    return;
}
//計算三角形面積
void triangle()
{
    input("Enter width: ","Out of range!",w);
    input("Enter height: ","Out of range!",h);
    cout << "Area of triangle: " << w * h / 2 << endl;
    return;
}
//計算長方形面積
void rectangle()
{
    input("Enter width: ","Out of range!",w);
    input("Enter height: ","Out of range!",h);
    cout << "Area of rectangle: " << w * h << endl;
    return;
}
int main()
{
    // 使用者輸入的模式
    int mode = 0;
    // 提示使用者
    cout << "Calculate the area." << endl
         << "Choose the mode." << endl
         << "Area of circle=1." << endl
         << "Area of triangle=2." << endl
         << "Area of rectangle=3." << endl
         << "Exit=-1." << endl
         << "Enter mode: ";
    while (cin >> mode)
    {
        if (mode == -1) // 負一跳出
            break;
        switch(mode)
        {
        case 1:
            circle();
            break;
        case 2:
            triangle();
            break;
        case 3:
            rectangle();
            break;
        default: // 其餘都是Illegal
            cout << "Illegal input!" << endl;
            break;
        }
        cout << "Enter mode: ";
    }
    return 0;
}
