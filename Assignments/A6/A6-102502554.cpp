#include <iostream>

using namespace std;

int main ()
{
    int enter;//宣告整數變數enter
    double radiuz , width , height;//宣告小數變數這3者代表半徑、寬、長

    cout << "Calculate the area." << endl << "Choose the mode." << endl;//輸出Calculate the area.和Choose the mode.
    cout << "Area of circle=1." << endl << "Area of triangle=2." << endl << "Area of rectangle=3." << endl << "Exit=-1." << endl;//輸出模式的代號

    do//用do...while迴圈來進行重複輸入
    {
        cout << "Enter mode: ";
        cin >> enter;//輸入代號

        switch ( enter )
        {
        case 1://模式一
            cout << "Enter radius: ";
            cin >> radiuz;//輸入半徑
            while ( radiuz <= 0 )
            {
                cout << "Out of range!" << endl << "Enter radius: ";
                cin >> radiuz;
            }//不符條件時輸出Out of range!並重新輸入
            cout << "Area of circle: " << 3.14159 * radiuz *radiuz << endl;//計算圓面積並輸出
            break;//結束後跳脫switch但因外面有do...while迴圈故可重新輸入

        case 2://模式二
            cout << "Enter width: ";
            cin >> width;//輸入底邊長
            while ( width <= 0 )
            {
                cout << "Out of range!" << endl << "Enter width: ";
                cin >> width;
            }//不符條件時輸出Out of range!並重新輸入
            cout << "Enter height: ";
            cin >> height;//輸入高
            while ( height <= 0 )
            {
                cout << "Out of range!" << endl << "Enter height: ";
                cin >> height;
            }//不符條件時輸出Out of range!並重新輸入
            cout << "Area of triangle: " << width * height / 2 << endl;//計算三角形面積並輸出
            break;//結束後跳脫switch但因外面有do...while迴圈故可重新輸入

        case 3://模式三
            cout << "Enter width: ";
            cin >> width;//輸入寬
            while ( width <= 0 )
            {
                cout << "Out of range!" << endl << "Enter width: ";
                cin >> width;
            }//不符條件時輸出Out of range!並重新輸入
            cout << "Enter height: ";
            cin >> height;//輸入長
            while ( height <= 0 )
            {
                cout << "Out of range!" << endl << "Enter height: ";
                cin >> height;
            }//不符條件時輸出Out of range!並重新輸入
            cout << "Area of rectangle: " << width * height << endl;//計算長方形面積並輸出
            break;//結束後跳脫switch但因外面有do...while迴圈故可重新輸入

        case -1:
            break;//因符合do...while條件結束迴圈

        default :
            cout << "Illegal input!" << endl;
            break;//結束後跳脫switch但因外面有do...while迴圈故可重新輸入
        }
    }
    while ( enter != -1 );//do...while迴圈之條件

    return 0;
}
