#include<iostream>
#include<cmath>
using namespace std;

int main ()
{
    int status=1,mode=0,i;                                //宣告變數。
    double m1r,m2w,m2h,m3w,m3h;

    cout << "Calculate the area." <<endl;                 //輸出字串。
    cout << "Choose the mode." <<endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl ;
    cout << "Exit=-1." << endl;

    while (status==1)                                     //當status為1時執行以下程式來判斷要輸出哪種面積，或是要改變status結束程式，預設status值為1讓第一次執行時可以進入。
    {
        cout << "Enter mode: ";                           //要求使用者輸入mode值來決定要進入哪個case。
        cin >> mode;

        while (mode!=-1 && mode!=1 && mode!=2 && mode!=3) //當mode值不符條件時要求使用者重新輸入。
        {
            cout << "Illegal input!" << endl ;
            cout << "Enter mode: " ;
            cin >> mode;

        }

        switch (mode)                                    //利用switch case來判斷要輸出哪種面積，或是直接更改status值結束程式。
        {

        case 1 :                                         //case1為圓形面積。
            cout << "Enter radius: " ;                   //要求使用者輸入相關參數，若輸入錯誤則要求重新輸入。
            cin >> m1r ;
            while (m1r<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter radius: " ;
                cin >> m1r;
            }
            cout << "Area of circle: " << pow(m1r,2)*3.14159 << endl ;//利用pow函數直接輸出結果。
            break;

        case 2 :
            cout << "Enter width: " ;                    //要求使用者輸入相關參數，若輸入錯誤則要求重新輸入。
            cin >> m2w;
            while (m2w<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> m2w;
            }

            cout << "Enter height: " ;
            cin >> m2h ;
            while (m2h<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> m2h;
            }

            cout << "Area of triangle: " << m2w*m2h/2 << endl;   //直接輸出結果。
            break;

        case 3 :
            cout << "Enter width: " ;                     //要求使用者輸入相關參數，若輸入錯誤則要求重新輸入。
            cin >> m3w;
            while (m3w<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: ";
                cin >> m3w;
            }

            cout << "Enter height: " ;
            cin >> m3h;
            while (m3h<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: ";
                cin >> m3h;
            }

            cout << "Area of rectangle: " << m3w*m3h << endl;    //直接輸出結果。
            break;

        case -1 :                                        //若使用者要求退出程式，則修改status值跳出while迴圈並結束程式。
            status=-1;
            break;

        default:
            break;
        }
    }
    return 0;
}
