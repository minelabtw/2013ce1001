#include <iostream>
#include <math.h>
using namespace std;
int main()
{
    int way = 0;//宣告方式的變數
    double pie = 3.14159;//宣告圓周率
    double r = 0;//宣告半徑的變數
    double width = 0;//宣告寬（底）變數
    double height = 0;//宣告長（高）變數

    cout << "Calculate the area." << endl << "Choose the mode." << endl;//提示相應按鍵的內容
    cout << "Area of circle=1." << endl << "Area of triangle=2." << endl << "Area of rectangle=3." << endl << "Exit=-1." << endl;//同上

    do
    {cout << "Enter mode:";
     cin >> way;//輸入方式變數
        switch( way )//將方式的變數帶入switch
        {
        case 1://輸入1進入圓面積的計算
            do{
               cout << "Enter radius:";
               cin >> r;
               if( r <= 0 )
               cout << "Out of range!" << endl;
            }while( r <= 0);//輸入半徑並檢查
            cout << "Area of circle:" << pow(r,2) * pie << endl;//印出圓的面積
        break;//直接結束switch

        case 2://輸入2進入三角面積計算
            do{
               cout << "Enter width:";
               cin >> width;
               if( width <= 0 )
               cout << "Out of range!" << endl;
            }while( width <= 0);//輸入底並檢查
            do{
               cout << "Enter height:";
               cin >> height;
               if( height <= 0 )
               cout << "Out of range!" << endl;
            }while( height <= 0);//輸入高並檢查
            cout << "Area of triangle:" << ( width * height ) / 2 << endl;//印出三角面積
        break;//直接結束switch

        case 3://輸入3進入矩形面積計算
           do{
               cout << "Enter width:";
               cin >> width;
               if( width <= 0 )
               cout << "Out of range!" << endl;
            }while( width <= 0);//輸入寬並檢查
            do{
               cout << "Enter height:";
               cin >> height;
               if( height <= 0 )
               cout << "Out of range!" << endl;
            }while( height <= 0);//輸入長並檢查
            cout << "Area of rectangle:" << width * height <<endl;//印出矩形面積
        break;//直接結束switch

        case -1://-1為離開不顯示任何值
        break;//直接結束switch

        default:
            cout << "Illegal input!" << endl;//輸入其他值,提示錯誤
            break;//直接結束switch
        }
    }while( way != -1 );//檢查方式變數,若為-1則跳出大迴圈
return 0;//回傳到0
}
