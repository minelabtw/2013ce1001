#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    int mode;        //宣告一整數變數mode
    double rad;      //宣告變數rad
    double wid;      //宣告變數wid
    double hei;      //宣告變數hei

    cout<<"Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";   //輸出字串
    cout<<"Enter mode: ";
    cin>>mode;       //選擇mode

    do               //若mode不等於-1時執行switch迴圈
    {
        switch(mode)
        {
        case 1:     //輸入1,計算圓面積
        {
            cout<<"Enter radius: ";
            cin>>rad;
            while(rad<=0)  //rad不可小於等於0
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>rad;
            }
            cout<<"Area of circle: "<<3.14159*pow(rad,2);   //圓面積=3.14159*rad*rad
            break;
        }
        case 2:    //輸入2,計算三角形面積
        {
            cout<<"Enter width: ";
            cin>>wid;
            while(wid<=0)  //wid不可小於等於0
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>wid;
            }
            cout<<"Enter height: ";
            cin>>hei;
            while(hei<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>hei;
            }
            cout<<"Area of triangle: "<<wid*hei*0.5;    //三角形面積=wid*hei*0.5
            break;
        }
        case 3:   //輸入3,計算矩形面積
        {
            cout<<"Enter width: ";
            cin>>wid;
            while(wid<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>wid;
            }
            cout<<"Enter height: ";
            cin>>hei;
            while(hei<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>hei;
            }
            cout<<"Area of triangle: "<<wid*hei;    //矩形面積=wid*hei
            break;
        }
        default:    //其餘輸出"Illegal input!"
            cout<<"Illegal input!";
            break;
        }
    cout<<endl<<"Enter mode: ";    //使mode可重複執行
    cin>>mode;
    }
    while(mode!=-1);

    return 0;
}
