#include<iostream>
using namespace std;

int main()
{
    double pi=3.14159; //宣告一個double型態的PI值
    int mode=0; //宣告一個正數的變數作為模式選擇
    double input ( int ); //宣告一個函式
    double width;
    double height;
    double radius; //宣告型態為double的長寬高

    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl; //顯示題目需求

    do
    {
        do
        {
            cout << "Enter mode: "; //顯示題目需求
            cin >> mode; //輸入模式
        }
        while ( mode!=1 && mode!=2 && mode!=3 && mode!=-1 && cout << "Illegal input!\n" ); //不符合時顯示錯誤

        switch (mode) //選擇模式
        {
        case 1:
            radius = input ( 0 );
            cout << "Area of circle: " <<(radius*radius*pi) << endl; //顯示圓面積
            break;
        case 2:
            width = input ( 1 );
            height = input ( 2 );
            cout << "Area of triangle: " <<(width*height/2) << endl; //顯示三角形面積
            break;
        case 3:
            width = input ( 1 );
            height = input ( 2 );
            cout << "Area of rectangle: " <<(width*height) << endl; //顯示長方形面積
        case -1:
            break; //跳出迴圈結束
        }

    }
    while ( mode != -1 );//只有輸入-1才能終止程式

    return 0;
}
double input ( int x ) //依題目顯示並要求輸入
{
    double y;
    do
    {
        if ( x == 0 )
        {
            cout << "Enter radius: ";
        }
        else if ( x== 1 )
        {
            cout << "Enter width: ";
        }
        else
        {
            cout << "Enter height: ";
        }
        cin >> y;
    }
    while ( y<=0 && cout << "Out of range!" <<endl); //要求大於0的正數

    return y;
}
