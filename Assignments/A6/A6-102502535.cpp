#include <iostream>

using namespace std ;

int main()
{
    float r ;  //設變數為半徑r。
    float width , height ;  //設變數為三角形的高及底。
    float x , y ;  //設變數為矩形的長及寬。
    float circle , triangle , rectangle ;  //設三變數，分別為圓形、三角形及矩形的面積。
    int mode = 0 ;  //設一變數為mode，用於後續之判斷。

    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1." << endl ;

    do
    {
        cout << "Enter mode: " ;
        cin >> mode ;  //輸入mode。

        switch ( mode )  //判斷模式。
        {
        case 1 :  //若為一，進行以下指令。
            do
            {
                cout << "Enter radius: " ;
                cin >> r ;
                if ( r <= 0 )
                    cout << "Out of range!" << endl ;
            }
            while ( r <= 0 ) ;

            circle = r * r * 3.14159 ;
            cout << "Area of circle: " << circle << endl ;
            break ;


        case -1 :  //若為一，跳出判斷。
            break ;


        case 2 :  //若為二，進行以下指令。
            do
            {
                cout << "Enter width: " ;
                cin >> width ;
                if ( width <= 0 )
                    cout << "Out of range!" << endl ;
            }
            while ( width <= 0 ) ;

            do
            {
                cout << "Enter height: " ;
                cin >> height ;
                if ( height <= 0 )
                    cout << "Out of range!" << endl ;
            }
            while ( height <= 0 ) ;

            triangle = width * height / 2 ;
            cout << "Area of triangle: " << triangle << endl ;
            break ;


        case 3 :  //若為三，進行以下指令。
            do
            {
                cout << "Enter width: " ;
                cin >> x ;
                if ( x <= 0 )
                    cout << "Out of range!" << endl ;
            }
            while ( x <= 0 ) ;

            do
            {
                cout << "Enter height: " ;
                cin >> y ;
                if ( y <= 0 )
                    cout << "Out of range!" << endl ;
            }
            while ( y <= 0 ) ;

            rectangle = x * y ;
            cout << "Area of rectangle: " << rectangle << endl ;
            break ;


        default :  //若都不是，進行以下指令。
            cout << "Illegal input!" << endl ;
            break ;
        }
    }
    while ( mode != -1 ) ;

    return 0 ;
}
