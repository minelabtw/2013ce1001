#include <iostream>
#include <iomanip>
#include <math.h>//用於pow（平方）。

using namespace std;

int main()
{
    int input=0;//令Enter mode的變數整數

    cout << "Calculate the area. \n";
    cout << "Choose the mode. \n";
    cout << "Area of circle = 1. \n";
    cout << "Area of triangle = 2. \n";
    cout << "Area of rectangle = 3. \n";
    cout << "Enter mode: ";
    cin >> input;

    while (input!=-1)//設：當不是-1時進行迴圈。
    {
        double r=0;//令半徑的變數整數。
        double pia=3.14159;//為pia設一變數整數。
        double w=0;//為寬設變數整數。
        double h=0;//為高設變數整數。
        double output=0;//為答案設一變數整數。

        switch (input)//進行選擇
        {
        case 1:
            while (r<=0)
            {
                cout << "Enter radius: ";
                cin >> r;
                if (r<=0)
                {
                    cout << "Out of range!\n";
                }
            }
            output = pia * pow (r,2);//pow為次方，2為2次方 此列是圓形面積的公式
            cout << "Area of circle: " << output << endl;//輸出圓形答案。

            break;//跳出選擇。

        case 2:
            while (w<=0)
            {
                cout << "Enter width: ";
                cin >> w;
                if (w<=0)
                {
                    cout << "Out of range!\n";
                }
            }
            while (h<=0)
            {
                cout << "Enter height: ";
                cin >> h;
                if (h<=0)
                {
                    cout << "Out of range!\n";
                }
            }
            output = 0.5 * w * h;//三角形面積公式。
            cout << "Area of triangle: " << output << endl;//輸出答案。

            break;

        case 3:
            while (w<=0)
            {
                cout << "Enter width: ";
                cin >> w;
                if (w<=0)
                {
                    cout << "Out of range!\n";
                }
            }
            while (h<=0)
            {
                cout << "Enter height: ";
                cin >> h;
                if (h<=0)
                {
                    cout << "Out of range!\n";
                }
            }
            output = w * h;//四角形公式。
            cout << "Area of rectangle: " << output << endl;//輸出答案。

            break;

        default:
            cout << "illegal input!\n";//非選項裡或-1的數值顯示錯誤。
        }

        cout << "Enter mode: ";
        cin >> input;
    }
    return 0;
}
