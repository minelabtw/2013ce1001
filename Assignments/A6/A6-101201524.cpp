#include<iostream>
using namespace std;

int main()
{
    double wr,h,mode,pi=3.14159;//宣告變數wr(寬或半徑),h(高),mode(模式選項),pi(圓周率)=3.14159

    cout << "Calculate the area.\n";//輸出說明程式使用方法的字串
    cout << "Choose the mode.\n";
    cout << "Area of circle=1.\n";
    cout << "Area of triangle=2\n.";
    cout << "Area of rectangle=3.\n";
    cout << "Exit=-1.";

    while(1)
    {
        wr=0,h=0;//將wr,h先宣告為初始值0
        cout << "\nEnter mode: ";//輸出字串"Enter mode: "
        cin >> mode;//將選項宣告給mode
        if(mode==-1)//若mode=-1則結束程式
            break;
        while(mode==1 || mode==2 || mode==3)//若mode=1,2,3則要求輸入半徑(1時)或寬(2,3時)，將直宣告給wr，若wr<=0則輸出字串"Out of range!"要求重新輸入
        {
            cout << (mode==1 ? "Enter radius: " : "Enter width: ");
            cin >> wr;
            if(wr<=0)
                cout << "Out of range!\n";
            else
                break;
        }
        while(mode==2 || mode==3))//若mode=2,3則要求輸入高，將直宣告給h，若h<=0則輸出字串"Out of range!"要求重新輸入
        {
            cout << "Enter height: ";
            cin >> h;
            if(h<=0)
                cout << "Out of range!\n";
            else
                break;
        }
        if(mode==1 || mode==2 || mode==3//依據mode輸出不同的解答
        {
            cout << "Area of " << (mode==1 ? "circle: " : mode==2 ? "triangle: " : "rectangle: ");
            cout << (mode==1 ? wr*wr*pi : mode==2 ? wr*h/2 : wr*h);
            continue;//結束這次程式重新執行一個新的mode
        }
        cout << "Illegal input!";//若輸入的mode不為選項則輸出字串"Illegal input!"
        continue;//結束這次程式重新執行一個新的mode
    }
    return 0;
}
