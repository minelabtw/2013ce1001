#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    cout << "Calculate the area." << endl ;
    cout << "Choose the mode." << endl ;
    cout << "Area of circle=1." << endl ;
    cout << "Area of triangle=2." << endl ;
    cout << "Area of rectangle=3." << endl ;
    cout << "Exit=-1." ;

    int mode = 0 ;
    double a = 0 , b = 0 , r = 0 , circle = 0 , triangle = 0 , rectangle = 0 , pi = 3.14159 ; //pi為圓周率

    while( mode != -1 ) //如果mode為-1,便可結束迴圈,但一開始宣告其為0,便進入迴圈
    {
        cout << endl << "Enter mode: " ;
        cin >> mode ;

        switch (mode)
        {
        case 1 :  //計算園面積
            cout << "Enter radius: " ;
            cin >> r ;

            while ( r <= 0 )
            {
                cout << "Out of range!" << endl ;
                cout << "Enter radius: " ;
                cin >> r ;
            }

            circle = r*r*pi ; // 公式
            cout << "Area of circle: " << circle ;
            break ;
        case 2 : // 計算三角形面積
            cout << "Enter width: " ;
            cin >> a ;

            while ( a <= 0 )
            {
                cout << "Out of range!" << endl ;
                cout << "Enter width: " ;
                cin >> a ;
            }

            cout << "Enter height: " ;
            cin >> b ;

            while ( b <= 0 )
            {
                cout << "Out of range!" << endl ;
                cout << "Enter height: " ;
                cin >> b ;
            }

            triangle = a * b * 0.5 ; // 公式
            cout << "Area of triangle: " << triangle ;
            break ;
        case 3 : //計算四邊形面積
            cout << "Enter width: " ;
            cin >> a ;

            while ( a <= 0 )
            {
                cout << "Out of range!" << endl ;
                cout << "Enter width: " ;
                cin >> a ;
            }

            cout << "Enter height: " ;
            cin >> b ;

            while ( b <= 0 )
            {
                cout << "Out of range!" << endl ;
                cout << "Enter height: " ;
                cin >> b ;
            }

            rectangle = a * b ; //公式
            cout << "Area of rectangle: " << rectangle ;
            break ;
        case -1 :
            break ; //當mode為-1,結束迴圈

        default : //其他的情況
            cout << "Illegal input!" ;
            break ;
        }

    }

    return 0 ;
}
