#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    float radius=0;
    float width=0;
    float height=0;
    int mode=0;

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode: ";

    while (mode!=-1)                            //因為尚未cin>>mode值，故直接進入迴圈
    {
        cin>>mode;

        if (mode==-1)                           //如果mode為-1，跳出while迴圈
            break;

        switch (mode)                           //開始判斷mode值
        {

        case 1:                                 //如果mode為1，計算圓面積

            cout<<"Enter radius: ";
            cin>>radius;

            while (radius<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>radius;
            }

            cout<<"Area of circle: "<<pow(radius,2)*3.14159<<endl;
            cout<<"Enter mode: ";
            break;

        case 2:                                 //如果mode為2，計算三角形面積

            cout<<"Enter width: ";
            cin>>width;

            while (width<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>width;
            }

            cout<<"Enter height: ";
            cin>>height;

            while (height<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>height;
            }

            cout<<"Area of triangle: "<<(width*height)/2<<endl;
            cout<<"Enter mode: ";
            break;

        case 3:                                 //如果mode為3，計算矩形面積

            cout<<"Enter width: ";
            cin>>width;

            while (width<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>width;
            }

            cout<<"Enter height: ";
            cin>>height;

            while (height<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>height;
            }

            cout<<"Area of rectangle: "<<width*height<<endl;
            cout<<"Enter mode: ";
            break;

        default:                                //如果mode為其他數字，輸出Illegal input!

            cout<< "Illegal input!"<<endl;
            cout<<"Enter mode: ";
            break;
        }
    }
    return 0;
}
