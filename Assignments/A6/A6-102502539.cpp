#include <iostream>

using namespace std;

int main()
{
    int input = 0;  //宣告變數且 input 設定為 0 以執行 while
    double width;
    double height;

    cout << "Calculate the area.\n"  << "Choose the mode.\n"
         << "Area of circle=1.\n"    << "Area of triangle=2.\n"
         << "Area of rectangle=3.\n" << "Exit=-1." ;

    while ( input != -1)
    {
        cout << endl << "Enter mode: " ;
        cin >> input;

        switch ( input )
        {
        case 1:     //圓面積
            cout << "Enter radius: " ;
            cin >> width;
            while ( width < 0 )
            {
                cout << "Out of range!\n" << "Enter radius: " ;
                cin >> width;
            }
            cout << "Area of circle: " << width * width * 3.14159 ;
            break;
        case 2:     //三角形面積
            cout << "Enter width: " ;
            cin >> width;
            while ( width < 0 )
            {
                cout << "Out of range!\n" << "Enter width: " ;
                cin >> width;
            }
            cout << "Enter height: " ;
            cin >> height;
            while ( height < 0 )
            {
                cout << "Out of range!\n" << "Enter height: " ;
                cin >> height;
            }
            cout << "Area of triangle: " << ( width * height ) / 2 ;
            break;

        case 3:     //矩形面積
            cout << "Enter width: " ;
            cin >> width;
            while ( width < 0 )
            {
                cout << "Out of range!\n" << "Enter width: " ;
                cin >> width;
            }
            cout << "Enter height: " ;
            cin >> height;
            while ( height < 0 )
            {
                cout << "Out of range!\n" << "Enter height: " ;
                cin >> height;
            }
            cout << "Area of rectangle: " << width * height ;
            break;

        case -1:
            break;

        default :
            cout << "Illegal input!" ;
        }
    }
    return 0;
}
