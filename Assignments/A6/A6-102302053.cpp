#include <iostream>
#include <stdio.h>

using namespace std;

int main()
{
    char a = 0;
    double b;
    double c;
    double d;
    double pi = 3.14159;

    cout << "Calculate the area.\n" << "Choose the mode.\n" << "Area of circle=1.\n" << "Area of triangle=2.\n" << "Area of rectangle=3.\n" << "Exit=-1.\n";
    cout << "Enter mode: ";
    while(a != -1)
    {
        cin >> a;

        switch(a)
        {
        case '1':
            cout << "Enter radius: ";
            cin >> b;
            while (b <= 0 )
            {
                cout << "Out of range!\n";
                cout << "Enter radius: ";
                cin >> b;
            }
            d = b * b * pi ;
            cout << "Area of circle: " << d;
            cout << endl;
            cout << "Enter mode: ";
            break;

        case '2':
            cout << "Enter width: ";
            cin >> b;
            while (b <= 0 )
            {
                cout << "Out of range!\n";
                cout << "Enter width: ";
                cin >> b;
            }
            cout << "Enter height: ";
            cin >> c;
            while (c <= 0)
            {
                cout << "Out of range!\n";
                cout << "Enter height: ";
                cin >> c;
            }
            d = (b * c) / 2;
            cout << "Area of triangle: " << d;
            cout << endl;
            cout << "Enter mode: ";
            break;

        case '3':
            cout << "Enter width: ";
            cin >> b;
            while (b <= 0 )
            {
                cout << "Out of range!\n";
                cout << "Enter width: ";
                cin >> b;
            }
            cout << "Enter height: ";
            cin >> c;
            while (c <= 0)
            {
                cout << "Out of range!\n";
                cout << "Enter height: ";
                cin >> c;
            }
            d = (b * c);
            cout << "Area of rectangle: " << d;
            cout << endl;
            cout << "Enter mode: ";
            break;

        case '4':
            break;

        default:
            cout << "Illegal input!\n";
            cout << "Enter mode: ";
            break;


        }

    }
    return 0;
}
