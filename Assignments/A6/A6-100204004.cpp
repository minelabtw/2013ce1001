#include<iostream>
using namespace std;
int main()
{
    double width,height;//triangle 的 width,height
    double radius;
    double width1,height1;//rectangle 的 width,height
    int mode;
    double circle,triangle,rectangle;

    cout<<"Calculate the area.\nChoose the mode."<<endl;
    cout<<"Area of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1."<<endl;

    while(1)
    {
        cout<<"Enter mode: ";
        cin>>mode;
        if(mode==1 || mode==2 || mode==3)//若是mode值為1,2,3，進入switch
        {
            switch(mode)
            {
            case 1://Area of circle
                while(1)//限制radius>=0
                {
                    cout<<"Enter radius: ";
                    cin>>radius;
                    if(radius<=0)
                        cout<<"Out of range!"<<endl;
                    else
                        break;
                }
                circle=3.14159*radius*radius;//Area of circle
                cout<<"Area of circle: "<<circle<<endl;
                break;

            case 2://Area of triangle
                while(1)//限制width>=0
                {
                    cout<<"Enter width: ";
                    cin>>width;
                    if(width<=0)
                        cout<<"Out of range!"<<endl;
                    else
                        break;
                }
                while(1)//限制height>=0
                {
                    cout<<"Enter height: ";
                    cin>>height;
                    if(height<=0)
                        cout<<"Out of range!"<<endl;
                    else
                        break;
                }
                triangle=0.5*height*width;//Area of triangle
                cout<<"Area of triangle: "<<triangle<<endl;
                break;

            case 3://Area of rectangle
                while(1)//限制width>=0
                {
                    cout<<"Enter width: ";
                    cin>>width1;
                    if(width1<=0)
                        cout<<"Out of range!"<<endl;
                    else
                        break;
                }
                while(1)//限制height>=0
                {
                    cout<<"Enter height: ";
                    cin>>height1;
                    if(height1<=0)
                        cout<<"Out of range!"<<endl;
                    else
                        break;
                }
                rectangle=height1*width1;//Area of rectangle
                cout<<"Area of rectangle: "<<rectangle<<endl;
                break;

            default:
                break;
            }
        }
        else if(mode==-1)//Exit
            break;
        else
            cout<<"Illegal input!"<<endl;
    }
    return 0;
}
