#include <iostream>

using namespace std ;

int main ()

{
    cout << "Calculate the area." << endl << "Choose the mode." << endl ;       //輸出題目
    cout << "Area of circle=1." << endl << "Area of triangle=2." << endl ;
    cout << "Area of rectangle=3." << endl << "Exit=-1." << endl ;

    int mode = 1 ;      //宣告一系列的變數
    double r ;
    double height ;
    double width ;

    while (mode != -1)  // 當mode不等於-1時 迴圈繼續
    {
        cout << "Enter mode: " ;    //輸出題目
        cin >> mode ;

        switch (mode)   //轉換mode
        {
        case 1 : //第一種情況

            do   //求原面積
            {
                cout << "Enter radius: " ;
                cin >> r ;
            }
            while (r < 0 && cout << "Out of range!" << endl ) ;

            cout << "Area of circle: " << r*r*(3.141592653) << endl ;
            break ;

        case 2 :  //第二種情況
            do    //求三角形面積
            {
                cout << "Enter width: " ;
                cin >> width ;
            }
            while (width < 0 && cout << "Out of range!" << endl ) ;

            do
            {
                cout << "Enter height: " ;
                cin >> height ;
            }
            while (height < 0 && cout << "Out of range!" << endl ) ;

            cout << "Area of triangle: " << width*height/2 << endl ;
            break ;

        case 3 :  //第三種情況
            do    //求矩形面積
            {
                cout << "Enter width: " ;
                cin >> width ;
            }
            while (width < 0 && cout << "Out of range!" << endl ) ;

            do
            {
                cout << "Enter height: " ;
                cin >> height ;
            }
            while (height < 0 && cout << "Out of range!" << endl ) ;

            cout << "Area of rectangle: " << width*height << endl ;
            break ;

        case -1 :      //等於-1時的情況結束程式
            break ;

        default :      //其他
            cout << "Illegal input!" << endl ;
            break ;
        }
    }
    return 0 ;
}
