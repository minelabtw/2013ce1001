#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    double r;//宣告型別為小數的變數
    double circlearea;
    double triangleheight;
    double trianglewidth;
    double trianglearea;
    double rectangleheight;
    double rectanglewidth;
    double rectanglearea;
    int mode=0;

    cout<<"Calculate the area."<<endl<<"Choose the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl<<"Area of rectangle=3."<<endl<<"Exit=-1.";//輸出說明

    while ( mode!=-1 )//進行判斷與計算
    {
        cout<<endl<<"Enter mode: ";
        cin>>mode;
        switch ( mode )
        {
        case 1:
            cout<<"Enter radius: ";
            cin>>r;
            while ( r<=0 )
            {
                cout<<"Out of range!"<<endl<<"Enter radius: ";
                cin>>r;
            }
            circlearea=3.14159*pow(r,2);
            cout<<"Area of circle: "<<circlearea;
            break;
        case 2:
            cout<<"Enter width: ";
            cin>>trianglewidth;
            while ( trianglewidth<=0 )
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>trianglewidth;
            }
            cout<<"Enter height: ";
            cin>>triangleheight;
            while ( triangleheight<=0 )
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>triangleheight;
            }
            trianglearea=trianglewidth*triangleheight/2;
            cout<<"Area of triangle: "<<trianglearea;
            break;
        case 3:
            cout<<"Enter width: ";
            cin>>rectanglewidth;
            while ( rectanglewidth<=0 ){
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>rectanglewidth;
            }
            cout<<"Enter height: ";
            cin>>rectangleheight;
            while ( rectangleheight<=0 ){
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>rectangleheight;
            }
            rectanglearea=rectanglewidth*rectangleheight;
            cout<<"Area of rectangle: "<<rectanglearea;
            break;
        case -1:
            break;
        default:
            cout<<"Illegal input!";
            break;
        }
    }
    return 0;
}
