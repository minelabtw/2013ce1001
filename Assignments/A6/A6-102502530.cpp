#include<iostream>

double Circle()  //圓
{
   double r;   //宣告半徑

   do    //輸入
   {
      std::cout<<"Enter radius: ";
      std::cin>>r;
      if(r<=0)
         std::cout<<"Out of range!\n";
   }while(r<=0);

   return 3.14159*r*r;  //回傳結果
}

double angle()    //三角形或矩形
{
   double w,h;    //宣告底(長)與高(寬)
   
   do    //輸入
   {
      std::cout<<"Enter width: ";
      std::cin>>w;
      if(w<=0)
         std::cout<<"Out of range!\n";
   }while(w<=0);
   do
   {
      std::cout<<"Enter height: ";
      std::cin>>h;
      if(h<=0)
         std::cout<<"Out of range!\n";
   }while(h<=0);

   return w*h;    //回傳結果
}

int main()
{
   int mode;   //宣告模式

   std::cout<<"Calculate the area.\n"<<   //輸出提示
              "Choose the mode.\n"<<
              "Area of circle=1.\n"<<
              "Area of triangle=2.\n"<<
              "Area of rectangle=3.\n"<<
              "Exit=-1.";

   do
   {
      std::cout<<"\nEnter mode: ";  //輸入
      std::cin>>mode;

      switch(mode)   //輸出結果
      {
      case 1:
         std::cout<<Circle();
         break;
      case 2:
         std::cout<<angle()/2;
         break;
      case 3:
         std::cout<<angle();
         break;
      case -1:
         return 0;
      default:
         std::cout<<"Illegal input!";
      }
   }while(1);
}
