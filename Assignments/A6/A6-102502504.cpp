#include<iostream>
using namespace std;

int main()
{
    int mode=0;
    double pi=3.14159;
    int r;
    int w;
    int h;

    cout<<"Calculate the area."<< endl;
    cout<<"Choose the mode."<< endl;
    cout<<"Area of circle=1."<< endl;
    cout<<"Area of triangle=2."<< endl;
    cout<<"Area of rectangle=3."<< endl;
    cout<<"Exit=-1."<< endl;


    while(mode==0)//利用while迴圈，使程式能夠不斷重新輸入
    {
        cout<<"Enter mode: ";
        cin>> mode;
        while(mode!=1&&mode!=2&&mode!=3&&mode!=-1)  //當mode不為-1、1、2、3時，顯示錯誤訊息，並要求重新輸入
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>> mode;
        }

        switch(mode) //依據變數mode的值來判斷進入哪一個case
        {
        case 1:
            cout<<"Enter radius: ";
            cin>>r;
            while(r<=0) //利用while迴圈，判斷r是否大於0，若無則顯示錯誤訊息
            {
                cout<<"Illegal input!"<<endl;
                cout<< "Enter radius: ";
                cin>>r;
            }
            cout<<"Area of circle: "<<pi*r*r<<endl;
            break;

        case 2:
            cout<<"Enter width: ";
            cin>>w;
            while(w<=0) //利用while迴圈，判斷width是否大於0，若無則顯示錯誤訊息
            {
                cout<<"Illegal input!"<<endl;
                cout<<"Enter width: ";
                cin>>w;
            }

            cout<<"Enter height: ";
            cin>>h;
            while(h<=0) //利用while迴圈，判斷height是否大於0，若無則顯示錯誤訊息
            {
                cout<<"Illegal input!"<<endl;
                cout<< "Enter height: ";
                cin>>h;
            }

            cout<<"Area of triangle: "<<w*h/2<<endl;
            break;

        case 3:
            cout<<"Enter width: ";
            cin>>w;
            while(w<=0) //利用while迴圈，判斷width是否大於0，若無則顯示錯誤訊息
            {
                cout<<"Illegal input!"<<endl;
                cout<< "Enter width: ";
                cin>>w;
            }

            cout<<"Enter height: ";
            cin>>h;
            while(h<=0) //利用while迴圈，判斷height是否大於0，若無則顯示錯誤訊息
            {
                cout<<"Illegal input!"<<endl;
                cout<<"Enter height: ";
                cin>>h;
            }
            cout<<"Area of rectangle: "<<w*h<<endl;
            break;

        }
        if(mode==-1) //若mode=-1則結束程式
            break;
        mode=0;
    }

    return 0;

}
