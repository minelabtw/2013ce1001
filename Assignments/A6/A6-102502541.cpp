#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    cout << " Calculate the area." << endl << "Choose the mode." << endl << "Area of circle=1." << endl << "Area of triangle=2." << endl << "Area of rectangle=3." << endl << "Exit=-1." << endl;
    int m = 0;//宣告變數m等於0
    while(m!=-1)//當m=-1結束
    {
        double r = 0;//宣告變數r等於0
        double w = 0;//宣告變數w等於0
        double h = 0;//宣告變數h等於0
        double o = 3.14159;//宣告變數o等於3.14159
        cout << "Enter mode: ";//輸出Enter mode:
        cin >> m;//輸入m
        if(m!=1 && m!=2 && m!=3 && m!=-1)//當m不等於1 2 3 -1
            cout << "Illegal input!" << endl;//輸出Illegal input! 換行
        switch(m)//依m判斷
        {
        case 1://m=1時
            while(r<=0)//當r小於等於0迴圈
            {
                cout << "Enter radius: ";//輸出Enter radius:
                cin >> r;//輸入r
                if(r<=0)//當r小於等於0時
                    cout << "Out of range!" << endl;//輸出Out of range! 換行
            }
            cout << "Area of circle: " << pow(r,2)*o << endl;//輸出Area of circle: r^2*o 換行
            break;//結束
        case 2://m=2 3時
        case 3:
            while(w<=0)//當w小於等於0迴圈
            {
                cout << "Enter width: ";//輸出Enter width:
                cin >> w;//輸入w
                if(w<=0)//當w小於等於0時
                    cout << "Out of range!" << endl;//輸出Out of range! 換行
            }
            while(h<=0)//當h小於等於0迴圈
            {
                cout << "Enter height: ";//輸出Enter height:
                cin >> h;//輸入h
                if(h<=0)//當h小於等於0時
                    cout << "Out of range!" << endl;//輸出Out of range! 換行
            }
            if(m==2)//當m等於2時
                cout << "Area of triangle: " << w*h*0.5 << endl;//輸出Area of triangle:  w*h*0.5 換行
            else//其餘
                cout << "Area of rectangle: " << w*h << endl;//輸出Area of triangle:  w*h 換行
        break;//結束
        }
    }
    return 0;
}

