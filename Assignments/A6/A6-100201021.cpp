#include<iostream>

using namespace std;
main()
{
    int mode=0;
    float v1=0,v2=0;
    const double pi=3.14159;
    //output title
    cout << "Calculate the area\n" << "Choose the mode.\n" << "Area of circle=1.\n" << "Area of triangle=2.\n" << "Area of rectangle=3.\n" << "Exit=-1." << endl;

    //choose mode and input something
    while(true)
    {
        cout << "Enter mode: ";
        cin >> mode;
        switch(mode)
        {
            //exit
            case -1:
                cout << "" << endl;
                break;
            //circle
            case 1:
                while(v1<=0)
                {
                    cout << "Enter radius: " ;
                    cin >> v1;
                    if(v1<=0)
                        cout << "Out of range!" << endl;
                }
                cout << "Area of circle: " << v1*v1*pi << endl;
                break;
            //triangle
            case 2:
                while(v1<=0)
                {
                    cout << "Enter width: " ;
                    cin >> v1;
                    if(v1<=0)
                        cout << "Out of range!" << endl;
                }
                while(v2<=0)
                {
                    cout << "Enter height: " ;
                    cin >> v2;
                    if(v2<=0)
                        cout << "Out of range!" << endl;
                }
                cout << "Area of triangle: " << v1*v2/2 << endl;
                break;
            //rectangle
            case 3:
                while(v1<=0)
                {
                    cout << "Enter width: " ;
                    cin >> v1;
                    if(v1<=0)
                        cout << "Out of range!" << endl;
                }
                while(v2<=0)
                {
                    cout << "Enter height: " ;
                    cin >> v2;
                    if(v2<=0)
                        cout << "Out of range!" << endl;
                }
                cout << "Area of rectangle:" << v1*v2 << endl;
                break;
            //mode != -1,1,2,3.
            default :
                cout << "Illegal input!" << endl;
                break;
            break;
        }
        //let v1,v2 to 0;
        v1=0;
        v2=0;
        //if choose exit,do it.
        if(mode==-1)
            break;
    }
    return 0;
}
