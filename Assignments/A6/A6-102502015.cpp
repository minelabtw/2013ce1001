#include <iostream>
using namespace std;
int main()
{
    int mode=0;                                 //模式 初始0
    float a;                                      //b 圓周率
    float b=3.14159;
    float c;
    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;
    while (mode!=-1)
    {
        cout<<"Enter mode:";
        cin>>mode;
        switch(mode)                                    //判斷模式
        {
        case 1:                                         //當mode=1的時候
            cout<<"Enter radius: ";                     //a=半徑
            cin>>a;
            while(a<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"Enter radius: ";
                cin>>a;
            }
            cout<<"Area of circle: "<<a*a*b;            //算結果
            cout<<endl;
            break;
        case 2:                                         //當mode=2的時候
            cout<<"Enter width: ";                      //a=寬
            cin>>a;
            while(a<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"Enter width: ";
                cin>>a;
            }
            cout<<"Enter height: ";                     //c=高
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"Enter height: ";
                cin>>c;
            }
            cout<<"Area of triangle: "<<a*c*0.5;        //計算
            cout<<endl;
            break;
        case 3:                                         //當mode=3時
            cout<<"Enter width: ";                      //a=寬
            cin>>a;
            while(a<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"Enter width: ";
                cin>>a;
            }
            cout<<"Enter height: ";                     //c=高
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"Enter height: ";
                cin>>c;
            }
            cout<<"Area of rectangle: "<<a*c;           //計算
            cout<<endl;
            break;
        case -1:
            break;
        default:                                        //1 2 3 -1 以外 顯示錯誤
            cout<<"Illegal input!"<<endl;
            break;
        }
    }
    return 0;
}
