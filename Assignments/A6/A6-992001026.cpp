#include <iostream>

using namespace std;

int main()
{
    const double PI = 3.14159 ;
    double h , w ,r ;
    int mode ;
    cout << "Calculate the area."<<endl ;
    cout << "Choose the mode."<<endl ;
    cout << "Area of circle=1."<<endl ;
    cout << "Area of triangle=2."<<endl ;
    cout << "Area of rectangle=3."<<endl ;
    cout << "Exit=-1."<<endl ;



    while (1)
    {
        cout << "Enter mode: ";
        cin >> mode ;

        if (mode == -1) break ;

        switch (mode)
        {

        case 1 :

            cout << "Enter radius: " ;
            cin >>  r ;

            while ( r  <= 0 )
            {
                cout <<"Out of range!" << endl;
                cout << "Enter radius: " ;
                cin >> r ;
            }

            cout << "Area of circle: " <<r*r*PI << endl;

            break ;
        case 2 :


            cout << "Enter width: "  ;
            cin >> w ;

            while ( w  <= 0 )
            {
                cout <<"Out of range!" << endl;
                cout << "Enter width: " ;
                cin >> w ;
            }

            cout << "Enter height: ";
            cin >> h ;

            while ( h  <= 0 )
            {
                cout <<"Out of range!" << endl;
                cout << "Enter height: " ;
                cin >> h ;
            }


            cout << "Area of triangle: " << h*w/2  << endl;



            break ;
        case 3 :


            cout << "Enter width: "  ;
            cin >> w ;

            while ( w  <= 0 )
            {
                cout <<"Out of range!" << endl;
                cout << "Enter width: " ;
                cin >> w ;
            }
            cout << "Enter height: ";
            cin >> h ;

            while ( h  <= 0 )
            {
                cout <<"Out of range!" << endl;
                cout << "Enter height: " ;
                cin >> h ;
            }


            cout <<"Area of rectangle: " << h*w <<endl;
            break ;
        default :
            cout << "Illegal input!" << endl;

        }

    }



    return 0;
}
