#include<iostream>
using namespace std ;
int main()
{   //本次練習目的在於運用程式來提供使用者選擇系統並進行其面積的運算
    double a=0    ;
    double b=0     ;
    double c=3.14159  ;        //用double的目的在於能使數字的精準度到達小數點以下
    double r=0 ;
    double d=0 ;
    int    e=0 ;

    cout<<"Calculate the area."<<endl ;
    cout<<"Choose the mode."<<endl ;
    cout<<"Area of circle=1."<<endl ;
    cout<<"Area of triangle=2." <<endl;
    cout<<"Area of rectangle=3."<<endl ;
    cout<<"Exit=-1." <<endl;
    cout<<"Enter mode: ";

    while(cin>>e and e!=-1)
    {
        switch(e)          //轉換系統的關鍵
        {
        case 1:
            cout<<"Enter radius: " ;
            cin >>r ;
            if (r<=0)
            {
                cout<<"Out of range!" <<endl ;
                cout<<"Enter radius: ";
                cin >>r ;
            }
            d=r*r*c ;
            cout<<"Area of circle: "<<d ;
            break ;

        case 2:
            cout<<"Enter width: " ;
            cin >>a ;
            if(a<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"Enter width: " ;
                cin >>a ;
            }
            cout<<"Enter height: " ;
            cin >>b ;
            if(b<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"Enter height: " ;
                cin >>b ;
            }
            d=(a*b)/2 ;
            cout<<"Area of triangle: "<<d ;
            break ;
        case 3:
            cout<<"Enter width: " ;
            cin >>a ;
            if(a<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"Enter width: " ;
                cin >>a ;
            }
            cout<<"Enter height: " ;
            cin >>b ;
            if(b<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"Enter height: " ;
                cin >>b ;
            }
            d=a*b ;
            cout<<"Area of rectangle: "<<d ;
            break ;

        default:
            cout<<"Illegal input!" ;
            break ;
        }

        cout<<endl ;
        cout<<"Enter mode: " ;     //使系統能重複被使用
        if(e==-1)
        {
            cout<<endl ;
        }
    }
    return 0 ;
}
