#include<iostream>
#include<cmath>
using namespace std;

int main()
{
    double r=0;  //半徑
    double width=0;  //寬
    double height=0;  //長
    double pi=3.14159;  //圓周率
    int mode=0;
    double circlearea=0;
    double trianglearea=0;
    double rectanglearea=0;

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

    do  //使重複執行
    {
        cout<<"Enter mode: ";
        cin>>mode;


        switch(mode)  //選擇模式
        {
        case 1:
            do
            {
                cout<<"Enter radius: ";
                cin>>r;
                if(r<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(r<=0);

            circlearea=r*r*pi;  //計算圓面積
            cout<<"Area of circle: "<<circlearea<<endl;
            break;

        case 2:
            do
            {
                cout<<"Enter width: ";
                cin>>width;
                if(width<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(width<=0);

            do
            {
                cout<<"Enter height: ";
                cin>>height;
                if(height<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(height<=0);

            trianglearea=(width*height)/2;  //計算三角形面積
            cout<<"Area of triangle: "<<trianglearea<<endl;
            break;

        case 3:
            do
            {
                cout<<"Enter width: ";
                cin>>width;
                if(width<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(width<=0);

            do
            {
                cout<<"Enter height: ";
                cin>>height;
                if(height<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(width<=0);

            rectanglearea=width*height;  //計算矩形面積
            cout<<"Area of rectangle: "<<rectanglearea<<endl;
            break;

        case -1:
            break;

        default:
            cout<<"Illegal input!"<<endl;
            break;

        }



    }
    while(mode!=-1);

    return 0;

}
