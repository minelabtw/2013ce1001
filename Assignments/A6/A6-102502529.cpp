#include<iostream>

using namespace std ;


int main()
{
    cout<<"Calculate the area.\n" ;
    cout<<"Choose the mode.\n";
    cout<<"Area of circle=1.\n" ;
    cout<<"Area of triangle=2.\n" ;
    cout<<"Area of rectangle=3.\n" ;
    cout<<"Exit=-1.\n" ;
    cout<<"Enter mode: " ;

    int input=0 ;
    double r=0,h=0,w=0 ;

    while(cin>>input)                           //輸入input值 進入迴圈 並用以判斷mode
    {
        if(input==-1)                           //-1 跳出while迴圈
        {
            break;
        }
        switch(input)                           //判斷input的值
        {
        case 1:
            cout<<"Enter radius: " ;
            cin>>r ;
            while(r==0||r<0)                                //當r值不符才進入迴圈 並重新輸入其值
            {
                cout<<"Out of range!\n";
                cout<<"Enter radius: ";
                cin>>r ;
            }
            cout<<"Area of circle: "<<r*r*3.14159<<endl ;   //輸出計算值
            cout<<"Enter mode: " ;
            break;                                          //跳出switch 迴圈
        case 2:
            cout<<"Enter width: " ;
            cin>>w;
            while(w==0||w<0)                                //當w值不符才進入while迴圈 並重新輸入其值
            {
                cout<<"Out of range!\n";
                cout<<"Enter width: ";
                cin>>w ;
            }
            cout<<"Enter height: " ;
            cin>>h ;
            while(h==0||h<0)                                //h值亦同
            {
                cout<<"Out of range!\n";
                cout<<"Enter height: ";
                cin>>h ;
            }
            cout<<"Area of triangle: "<<w*h/2<<endl ;       //輸出面積
            cout<<"Enter mode: ";
            break ;                                         //跳出switch
        case 3:
            cout<<"Enter width: " ;
            cin>>w;
            while(w==0||w<0)                                //w值不符進入迴圈並重新輸入
            {
                cout<<"Out of range!\n";
                cout<<"Enter width: ";
                cin>>w ;
            }
            cout<<"Enter height: " ;
            cin>>h ;
            while(h==0||h<0)                               //h亦同
            {
                cout<<"Out of range!\n";
                cout<<"Enter height: ";
                cin>>h ;
            }
            cout<<"Area of rectangle: "<<w*h<<endl ;        //輸出面積
            cout<<"Enter mode: ";
            break;                                          //跳出switch
        default :                                           //當選擇的mode未在定義的
            cout<<"Illegal input!"<<endl;                   //輸出錯誤訊息
            cout<<"Enter mode: " ;
            break ;                                         //跳出switch 由while迴圈再輸入一次
        }
    }
    return 0 ;
}
