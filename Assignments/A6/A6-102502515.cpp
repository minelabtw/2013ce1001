#include <iostream>

using namespace std;

int main()
{

    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;
    int mode ;

    do                                                                  //利用do先執行第一次，再開始進入while條件
    {
        double r ;                                                      //設定實數變數r
        double width ;                                                  //設定實數變數寬
        double height ;                                                 //設定實數變數高

        cout << "Enter mode: " ;
        cin  >> mode ;

        while (mode!=1 && mode!=2 && mode!=3 && mode!=-1)               //設定條件，讓輸入值符合題目要求，否則不斷重新輸入
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: " ;
            cin  >> mode ;
        }

        switch (mode)                                                   //選擇該執行的case
        {
        case 1 :                                                        //case1 計算圓面積
            cout << "Enter radius: " ;
            cin  >> r ;
            while (r<=0)                                                //設定條件使半徑r大於0
            {
                cout << "Out of range!" << endl;
                cout << "Enter radius: " ;
                cin  >> r ;
            }
            cout << "Area of circle: " << r*r*3.14159 << endl;          //設定面積公式
            break;                                                      //跳出switch

        case 2 :                                                        //case2，三角形面積
            cout << "Enter width: " ;
            cin  >> width ;
            while (width<=0)                                            //利用while使寬、高大於0
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: " ;
                cin  >> width ;
            }
            cout << "Enter height: " ;
            cin  >> height ;
            while (height<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: " ;
                cin  >> height ;
            }
            cout << "Area of triangle: " << width*height*0.5 << endl;   //放三角形面積公式
            break;

        case 3 :                                                        //case3，長方形面積
            cout << "Enter width: " ;
            cin  >> width ;
            while (width<=0)                                            //利用while使長、寬大於0
            {
                cout << "Out of range!" << endl;
                cout << "Enter width: " ;
                cin  >> width ;
            }
            cout << "Enter height: " ;
            cin  >> height ;
            while (height<=0)
            {
                cout << "Out of range!" << endl;
                cout << "Enter height: " ;
                cin  >> height ;
            }
            cout << "Area of rectangle: " << width*height << endl;      //放入長方形面積公式
            break;                                                      //跳出switch
        }

    }
    while (mode != -1);                                                 //在輸入不等於-1時，不斷執行上面動作
    return 0;
}
