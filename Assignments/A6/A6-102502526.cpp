#include <iostream>

using namespace std;

int main()
{
    int mode=0;
    double r=0;
    double w=0;
    double h=0;

    cout<<"Calculate the area."<<endl<<"Choose the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl<<"Enter mode: ";

    while(cin>>mode)               //將「選擇mode」這個動作進行迴圈
    {
        while(mode<-1||mode==0||mode>3)  //除了-1、1、2、3，輸入其他字元都會輸出"Illegal input!"
        {
            cout<<"Illegal input!"<<endl<<"Enter mode: ";
            cin>>mode;
        }
        switch(mode)                     //選擇不同mode進行不同的處理
        case 1:                          //mode 1,circle
    {
        cout<<"Enter radius: ";
        cin>>r;
        while(r<=0)
        {
            cout<<"Out of range!";
            cin>>r;
        }
        cout<<"Area of circle: "<<r*r*3.14159<<endl<<"Enter mode: ";
        break;

        case 2:                         //mode 2,triangle
            cout<<"Enter width: ";
            cin>>w;
            while(w<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>w;
            }
            cout<<"Enter height: ";
            cin>>h;
            while(h<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>h;
            }
            cout<<"Area of triangle: "<<h*w/2<<endl<<"Enter mode: ";
            break;

        case 3:                         //mode 3,rectangle
            cout<<"Enter width: ";
            cin>>w;
            while(w<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>w;
            }
            cout<<"Enter height: ";
            cin>>h;
            while(h<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>h;
            }
            cout<<"Area of rectangle: "<<w*h<<endl<<"Enter mode: ";
            break;

        case -1:                       //mode -1,end all
            return 0;
        }
    }
}
