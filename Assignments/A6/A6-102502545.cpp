#include <iostream>
using namespace std;

int main()
{
    int mode=0;
    double p=3.14159,r,Area=0,a=0,b=0;
    cout<<"Calculate the area.\nChoose the mode.\n";
    cout<<"Area of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";

    do
    {
        cout<<"Enter mode: ";
        cin>>mode;
        while(mode!=1&&mode!=2&&mode!=3&&mode!=-1)
        {
            cout<<"Illegal input!\n";
            cout<<"Enter mode: ";
            cin>>mode;
        }

        switch(mode)
        {
        case 1:
            do//設定r的範圍
            {
                cout<<"Enter radius: ";
                cin>>r;
                if(r<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(r<=0);

            Area=r*r*p;//圓形面積
            cout<<"Area of circle: "<<Area<<endl;
            break;

        case 2:
            do//設定邊長的範圍
            {
                cout<<"Enter width: ";
                cin>>a;
                if(a<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(a<=0);

            do//設定邊長的範圍
            {
                cout<<"Enter height: ";
                cin>>b;
                if(b<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(b<=0);

            Area=a*b/2;//計算三角形的面積
            cout<<"Area of triangle: "<<Area<<endl;
            break;

        case 3:
            do//設定邊長的範圍
            {
                cout<<"Enter width: ";
                cin>>a;
                if(a<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(a<=0);

            do//設定邊長的範圍
            {
                cout<<"Enter height: ";
                cin>>b;
                if(b<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(b<=0);

            Area=a*b;//計算矩形的面積
            cout<<"Area of rectangle: "<<Area<<endl;
            break;
        case -1:
            break;
        default:
            break;
        }
    }
    while(mode!=-1);

    return 0;
}
