#include<iostream>
#include<cmath>
using namespace std;
int pow();
int main()
{
    int mode,a=0 ;
    double area,height,width,r;
    cout<<"Calculate the area."<<endl<<"Choose the mode."<<endl<<"Area of circle=1."<<endl<<"Area of triangle=2."<<endl<<"Area of rectangle=3."<<endl<<"Exit=-1."<<endl<<"Enter mode: ";
    while(a==0)
    {
        cin>>mode;
        switch(mode)
        {
        case 1:
            cout<<"Enter radius: ";
            cin>>r;
            while(r<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter radius: ";
                cin>>r;
            }
            cout<<"Area of circle: "<<3.14159*pow(r,2)<<endl;
            break;
        case 2:
            cout<<"Enter width: ";
            cin>>width;
            while(width<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";
            cin>>height;
            while(height<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>height;
            }
            cout<<"Area of triangle: "<<height*width*0.5<<endl;
            break;
        case 3:
            cout<<"Enter width: ";
            cin>>width;
            while(width<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>width;
            }
            cout<<"Enter height: ";
            cin>>height;
            while(height<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>height;
            }
            cout<<"Area of rectangle: "<<height*width<<endl;
            break;
        case -1:
            return 0;
        default:
            cout<<"Illegal input!"<<endl;
        }
        cout<<"Enter mode: ";
    }
}
