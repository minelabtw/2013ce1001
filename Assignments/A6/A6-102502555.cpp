#include<iostream>

using namespace std;

int main(){
    double height,width,r;
    int mode;
    double pi = 3.14159;

    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;

    do{
       cout << "Enter mode: ";
       cin >> mode;

        switch(mode){
            case 1:
                do{
                    cout << "Enter radius: ";
                    cin >> r;
                    if(r <= 0){
                        cout << "Out of range!" << endl;
                    }
                }while(r <= 0);

                cout << "Area of circle : " << r * r * pi << endl;

                break;

            case 2:
                do{
                    cout << "Enter width: ";
                    cin >> width;
                    if(width <= 0){
                        cout << "Out of range!" << endl;
                    }
                }while(width <= 0);
                do{
                    cout << "Enter height: ";
                    cin >> height;
                    if(height <= 0){
                        cout << "Out of range!" << endl;
                    }
                }while(height <= 0);

                cout << "Area of triangle: " << (width * height) / 2 << endl;

                break;

            case 3:
                do{
                    cout << "Enter width: ";
                    cin >> width;
                    if(width <= 0){
                        cout << "Out of range!" << endl;
                    }
                }while(width <= 0);
                do{
                    cout << "Enter height: ";
                    cin >> height;
                    if(height <= 0){
                        cout << "Out of range!" << endl;
                    }
                }while(height <= 0);

                cout << "Area of rectangle: " << width * height << endl;

                break;

            case -1:
                break;

            default:
                cout << "Illegal input!" << endl;
                break;

        }
    }while(mode != -1);
    return 0;
}
