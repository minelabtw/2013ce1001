#include<iostream>
using namespace std;

int main()
{
    int mode=0;//宣告變數並設定初始值
    double r=0,w=0,h=0,n=0;
    double x=3.14159;

    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1." << endl;//輸出字串
    cout << "Enter mode: ";
    cin  >> mode;//輸入並將值給變數

    while(mode!=1 and mode!=2 and mode!=3 and mode!=-1)//當mode不等於1,2,3,-1時進入迴圈
    {
        cout << "Illegal input!" << endl << "Enter mode: ";
        cin  >> mode;
    }

    while(mode!=-1)//mode不等於-1時進入迴圈
    {
        switch(mode)//指定mode的方案
        {
        case 1://mode=1為圓形面積
            cout << "Enter radius: ";
            cin  >> r;

            while(r<=0)//r大於0
            {
                cout << "Out of range!" << endl << "Enter radius: ";
                cin  >> r;
            }

            cout << "Area of circle: " << r*r*x;
            break;

        case 2://mode=2為三角形面積
            cout << "Enter width: ";
            cin  >> w;

            while(w<=0)//w大於0
            {
                cout << "Out of range!" << endl << "Enter width: ";
                cin  >> w;
            }

            cout << "Enter height: ";
            cin  >> h;

            while(h<=0)//h大於0
            {
                cout << "Out of range!" << endl << "Enter height: ";
                cin  >> h;
            }

            cout << "Area of triangle: " << w*h*0.5;
            break;

        case 3://mode=3為長方形面積
            cout << "Enter width: ";
            cin  >> w;

            while(w<=0)//w大於0
            {
                cout << "Out of range!" << endl << "Enter width: ";
                cin  >> w;
            }

            cout << "Enter height: ";
            cin  >> h;

            while(h<=0)//h大於0
            {
                cout << "Out of range!" << endl << "Enter height: ";
                cin  >> h;
            }

            cout << "Area of rectangle: " << w*h;
            break;

        default://不符合的方案
            cout << "Illegal input!";
            break;
        }

        cout << endl;
        cout << "Enter mode: ";//再次進行迴圈
        cin  >> mode;

        while(mode!=1 and mode!=2 and mode!=3 and mode!=-1)
        {
            cout << "Illegal input!" << endl << "Enter mode: ";
            cin  >> mode;
        }

    }

    return 0;
}
