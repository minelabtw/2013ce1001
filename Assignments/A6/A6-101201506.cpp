#include<iostream>
using namespace std;

int main()
{

    double Pi=3.14159; //圓周率
    double r=0; //半徑
    double W=0; //寬
    double H=0; //高
    double Ac=0; //圓面積
    double At=0; //三角形面積
    double Ar=0; //長方形面積
    int mode=0; //要運作哪種模式

    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode: ";
    cin>>mode;

    do
    {
        switch(mode)
        {
        case 1: //計算圓面積
            do
            {
                cout<<"Enter radius: ";
                cin>>r;
                if(r<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(r<=0);

            Ac=r*r*Pi;
            cout<<"Area of circle: "<<Ac<<endl;
            break;

        case 2: //計算三角形面積
            do
            {
                cout<<"Enter width: ";
                cin>>W;
                if(W<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(W<=0);

            do
            {
                cout<<"Enter height: ";
                cin>>H;
                if(H<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(H<=0);

            Ar=W*H*0.5;
            cout<<"Area of triangle: "<<Ar<<endl;
            break;

        case 3: //計算長方形面積
            do
            {
                cout<<"Enter width: ";
                cin>>W;
                if(W<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(W<=0);

            do
            {
                cout<<"Enter height: ";
                cin>>H;
                if(H<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(H<=0);

            Ar=W*H;
            cout<<"Area of rectangle: "<<Ar<<endl;
            break;

        case -1:
            break;

        default:
            cout<<"Illegal input!"<<endl;
            break;
        }
    cout<<"Enter mode: ";
    cin>>mode;
    }
    while(mode!=-1);

    return 0;
}
