#include<iostream>

using namespace std;

int main()

{
    cout<< "Calculate the area.\n";
    cout<< "Choose the mode.\n";
    cout<< "Area of circle=1.\n";
    cout<< "Area of triangle=2.\n";
    cout<< "Area of rectangle=3.\n";
    cout<< "Exit=-1.\n";

    int x=0;    //宣告名稱為x的變數

    cout<< "Enter mode: ";
    cin>> x;
    switch (x)    //讀取x
    {
    case 1:       //將1的值丟給x
        break;

    case 2:       //將2的值丟給x
        break;

    case 3:       //將3的值丟給x
        break;

    case -1:       //將-1的值丟給x
        break;

    default:
        while(x!=1 || x!=2 || x!=3 || x!=-1)    //迴圈   當x不等於1或2或3或-1時
        {
            cout<< "Illegal input!\n""Enter mode:  ";
            cin>> x;
        }
    }

    while(x==1)          //迴圈  條件為x等於1
    {

        double r=0;                //宣告名稱為r的變數
        double area=0;             //宣告名稱為area的變數
        double rate=3.14159;       //宣告名稱為rate的變數,值為3.14159

        cout<< "Enter radius: ";
        cin>> r;
        while(r<=0)           //迴圈  條件為r小於等於0
        {
            cout<< "Out of range!\n""Enter radius: ";
            cin>> r;
        }

        area = rate*r*r;      //area的計算式
        cout<< "Area of circle: "<< area;
        break;
    }

    while(x==2)          //迴圈  條件為x等於2
    {
        double w=0;      //宣告名稱為w的變數
        double h=0;      //宣告名稱為h的變數
        double area=0;    //宣告名稱為area的變數

        cout<< "Enter width: ";
        cin>> w;
        while(w<=0)        //迴圈  條件為w小於等於0
        {
            cout<< "Out of range!\n""Enter width: ";
            cin>> w;
        }
        cout<< "Enter height: ";
        cin>> h;
        while(h<=0)        //迴圈  條件為h小於等於0
        {
            cout<< "Out of range!\n""Enter height: ";
            cin>> h;
        }

        area = w*h*0.5;       //arae的計算式
        cout<< "Area of triangle: "<< area;
        break;
    }

    while(x==3)             //迴圈  條件為x等於3
    {
        double w=0;          //宣告名稱為w的變數
        double h=0;          //宣告名稱為h的變數
        double area=0;       //宣告名稱為area的變數

        cout<< "Enter width: ";
        cin>> w;
        while(w<=0)         //迴圈  條件為w小於等於0
        {
            cout<< "Out of range!\n""Enter width: ";
            cin>> w;
        }
        cout<< "Enter height: ";
        cin>> h;
        while(h<=0)        //迴圈  條件為h小於等於0
        {
            cout<< "Out of range!\n""Enter height: ";
            cin>> h;
        }

        area = w*h;           //area的計算式
        cout<< "Area of rectangle: "<< area;
        break;
    }
    return 0;

}
