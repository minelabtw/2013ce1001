#include<iostream>

using namespace std;

int main()
{
    int m=0;
    double r=0;//可以宣告小數的的double
    double b=0;//r:半徑 b:三角形的底 h:三角形的高 l:矩形的長 w:矩形的寬
    double h=0;
    double l=0;
    double w=0;


    cout << "Calculate the area.\n";
    cout << "Choose the mode.\n";
    cout << "Area of circle=1.\n";
    cout << "Area of triangle=2.\n";
    cout << "Area of rectangle=3.\n";
    cout << "Exit=-1.\n";
    cout << "Enter mode: ";
    cin  >> m;
    while(m!=-1)//數入-1跳出迴圈
    {
        switch(m)
        {
        case 1:
            cout << "Enter radius: ";
            cin  >> r;
            while(r<=0)//小於等於零重輸入的迴圈
            {
                cout << "Out of range!\n";
                cout << "Enter radius: ";
                cin  >> r;
            }
            cout << "Area of circle: ";
            cout << r*r*3.14159;//半徑乘半徑乘圓周率
            break;

        case 2:
            cout << "Enter width: ";
            cin  >> b;
            while(b<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter width: ";
                cin  >> b;
            }
            cout << "Enter height: ";
            cin  >> h;
            while(h<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter height: ";
                cin  >> h;
            }

            cout << "Area of triangle: " << b*h/2;//底乘高除2
            break;

        case 3:
            cout << "Enter width: ";
            cin  >> l;
            while(l<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter width: ";
                cin  >> l;
            }
            cout << "Enter height: ";
            cin  >> w;
            while(w<=0)
            {
                cout << "Out of range!\n";
                cout << "Enter height: ";
                cin  >> w;
            }

            cout << "Area of rectangle: " << l*w;//長乘寬
            break;

        default:
            cout << "Illegal input!";
            break;
        }

        cout << "\n";//重新輸入mode
        cout << "Enter mode: ";
        cin  >> m;

    }
    return 0;

}
