#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int mode; //call variable for mode
    double radius=0; // call varable for radius
    double width=0; // call variable for width
    double height=0;//call varable for height
    double pi=3.14159; // call variable for pi and set the value to 3.14159

    // display words
    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;

    do
    {
        //enter mode
        mode=0;
        cout<<"Enter mode: ";
        cin>>mode;

        switch(mode)
        {
        case 1: //for choose mode 1
            radius=0;
            while(radius<=0)
            {
                cout<<"Enter radius: "; //enter value
                cin>>radius;
                if(radius<=0) //data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            cout<<"Area of circle: "<<pi*pow(radius,2)<<endl; //calculatio for area of circle
            break;

        case 2: //for choose mode 2
            width=0;
            height=0;
            //enter value for width
            while(width<=0)
            {
                cout<<"Enter width: ";
                cin>>width;
                if(width<=0) //data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            //enter value for height
            while(height<=0)
            {
                cout<<"Enter height: ";
                cin>>height;
                if(height<=0)//data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            cout<<"Area of triangle: "<<(width*height)/2<<endl;// calculation for area
            break;

        case 3: //for choose mode3
            width=0;
            height=0;
            //enter value for width
            while(width<=0)
            {
                cout<<"Enter width: ";
                cin>>width;
                if(width<=0)//data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            //enter value for height
            while(height<=0)
            {
                cout<<"Enter height: ";
                cin>>height;
                if(height<=0)// data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            cout<<"Area of rectangle: "<<width*height<<endl; //calculation for area
            break;

        case -1: //for choose mode -1
            break; //leave the program

        default: //with improper input in mode
            cout<<"Illegal input!"<<endl;
            break;
        }
    }
    while(mode!=-1);//continue the program while mode is not -1

    return 0;
}
