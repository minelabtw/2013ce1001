#include <iostream>

using namespace std;

int main()
{
    int a=0;
    double b=0;
    double c=0;
    double d=0;
    double e=0;
    double f=0; //宣告變數

    cout << "Calculate the area."<<endl;
    cout << "Choose the mode."<<endl;
    cout << "Area of circle=1."<<endl;
    cout << "Area of triangle=2."<<endl;
    cout << "Area of rectangle=3."<<endl;
    cout << "Exit=-1."<<endl; //說明文字

    do  //選模式

    {
        cout << "Enter mode: " ;
        cin >> a;
        if(a<-1||a>=4||a==0)
            cout << "Illegal input!"<<endl;

        switch (a) //模式選擇
        {
        case 1 : //圓的面積
            do
            {
                cout<<"Enter radius: ";
                cin>>b;
                if(b<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(b<=0);
            cout<<"Area of circle: "<<b*b*3.14159<<endl;
            a=4;
            break;

        case 2 : //三角形面積
            do
            {
                cout<<"Enter width: ";
                cin>>c;
                if(c<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(c<=0);
            do
            {
                cout<<"Enter height: ";
                cin>>d;
                if(d<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(d<=0);
            cout<<"Area of triangle: "<<c*d*0.5<<endl;
            a=4;
            break;

        case 3 : //矩形面積
            do
            {
                cout<<"Enter width: ";
                cin>>e;
                if(e<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(e<=0);
            do
            {
                cout<<"Enter height: ";
                cin>>f;
                if(f<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(f<=0);
            cout<<"Area of rectangle: "<<f*e<<endl;
            a=4;
            break;

        default: //其他打破
            break;
        }
    }
    while(a<-1||a>=4||a==0); //迴圈繼續
    return 0;
}
