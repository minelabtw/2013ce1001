#include<iostream>
#include<cmath>
using namespace std ;
void load(int); //define function to load in radius, width, height
void compute_mode(int); // compute area when the mode == 1, 2, 3
double radius ;
double width ;
double height ;
double pi=3.14159;
int main()
{
    int mode ; //mode is a int
    cout << "Calculate the area.\n"
         << "Choose the mode.\n"
         << "Area of circle=1.\n"
         << "Area of triangle=2.\n"
         << "Area of rectangle=3.\n"
         << "Exit=-1.\n"
         << "Enter mode: " ;
    while(cin >> mode) //cin mode if mode!=-1
    {
        switch (mode)
        {
        case 1:
            compute_mode(1); //compute Area of circle
            break ;
        case 2:
            compute_mode(2); //compute Area of triangle
            break ;
        case 3:
            compute_mode(3); //compute Area of rectangle
            break ;
        case -1:
            return 0 ;
            break ;
        default :
            cout << "Illegal input!\n" ;
            break ;
        }
        cout << "Enter mode: " ;
    }
    return 0;
}
// compute area
void compute_mode(int mode)
{
    load(mode) ; //load in radius, width, height
    switch(mode)
    {
    case 1:
        cout << "Area of circle: " << pow(radius,2)*pi << endl ;
        break ;
    case 2:
        cout << "Area of triangle: " << 0.5*width*height << endl ;
        break ;
    case 3:
        cout << "Area of rectangle: " << width*height << endl ;
        break ;
    }
}
void load(int mode)
{
    radius=0;
    width=0 ;
    height =0 ;
    do //third to load height when mode!=1
    {
        do //secend to load width when mode!=1
        {
            do //first to load radius when mode = 1
            {
                if(mode!=1)
                    break ;
                cout << "Enter radius: " ;
                cin >> radius ;
                if(radius<=0)
                    cout << "Out of range!\n" ;
            }
            while(radius<=0) ;

            if(mode==1||width>0)
                break ;
            cout << "Enter width: " ;
            cin >> width ;
            if(width<=0)
                cout << "Out of range!\n" ;
        }
        while(width<=0) ;
        if(mode==1)
            break ;
        cout << "Enter height: " ;
        cin >> height ;
        if(height<=0)
            cout << "Out of range!\n" ;
    }
    while(height<=0) ;
}
