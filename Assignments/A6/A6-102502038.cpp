//============================================================================
// Name        : CE1001-A6改.cpp
// Author      : catLee/102502038 in NCU-CSIE
// Version     : 0.1
//============================================================================

#include <iostream>
using namespace std;

/*
    cir_sizeCalc,rec_sizeCalc,tri_sizeCalc are basically the same.

    void getUserInput(void) //active input action.
    double calc(void) //calculate area size,can call it outside.
    void displayCalc(void) //call calc,and cout.
*/
//circle size calculate.
class cir_sizeCalc{
public:
    void getUserInput(void){
        while(true){
            cout << "Enter radius: ";
            cin >> m_radius;
            if(m_radius>0)break;
            error();
        };
    };
    double calc(void){
        return m_radius*m_radius*m_pi;
    };
    //I really don't know why I did this.Maybe I was going to call it outside.
    void error(void){
        cout << "Illegal input!\n";
    };
    void displayCalc(void){
        cout << "Area of circle: " << calc() << "\n";
    };
private:
    double m_radius;
    double m_pi=3.14159;
};
//rectangle size calculate.
class rec_sizeCalc{
public:
    void getUserInput(void){
        while(true){
            cout << "Enter width: ";
            cin >> m_width;
            if(m_width>0)break;
            error();
        };
        while(true){
            cout << "Enter height: ";
            cin >> m_height;
            if(m_height>0)break;
            error();
        };
    };
    double calc(void){
        return m_width*m_height;
    };
    void error(void){
        cout << "Illegal input!\n";
    };
    double m_width,m_height;
    void displayCalc(void){
        cout << "Area of rectangle: " << calc() << "\n";
    };
};
//triangle size calculate.
class tri_sizeCalc:public rec_sizeCalc{
public:
    double calc(void){
        return m_width*m_height/2.0;
    };
    void displayCalc(void){
        cout << "Area of triangle: " << calc() << "\n";
    };
};
int main(void){
    string mode;
    cout << "Calculate the area.\nChoose the mode.\nArea of circle=1.\nArea of triangle=2.\nArea of rectangle=3.\nExit=-1.\n";  //Tell user to input.
    while(true){
        cout << "Enter mode: ";
        cin >> mode;
        if(mode!="1"&&mode!="2"&&mode!="3"&&mode!="-1"){    //like switch,but able to use string.
            cout << "Illegal input!\n";
        }else if(mode == "1"){
            cir_sizeCalc c1;
            c1.getUserInput();
            c1.displayCalc();
        }else if(mode == "3"){
            rec_sizeCalc c1;
            c1.getUserInput();
            c1.displayCalc();
        }else if(mode == "2"){
            tri_sizeCalc c1;
            c1.getUserInput();
            c1.displayCalc();
        }else if(mode == "-1"){
            break;
        };
    }
}
