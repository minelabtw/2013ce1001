#include <iostream>
using namespace std;

int main()
{
    double mode;  //模式
    double r;  //半徑
    double h;  //高
    double w;  //寬
    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode: ";
    while(cin>>mode,mode!=-1)  //輸入模式
    {
        if(mode==1)
        {
            cout<<"Enter radius: ";
            cin>>r;   //輸入半徑
            while(r<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>r;
            }
            cout<<"Area of circle: "<<r*r*3.14159<<endl;  //輸出圓面積
        }
        else if(mode==2)
        {
            cout<<"Enter width: ";
            cin>>w;  //輸入寬
            while(w<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>w;
            }
            cout<<"Enter height: ";
            cin>>h;  //輸入高
            while(h<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>h;
            }
            cout<<"Area of triangle: "<<w*h/2<<endl;   //輸出三角形面積
        }
        else if(mode==3)
        {
            cout<<"Enter width: ";
            cin>>w;  //輸入寬
            while(w<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>w;
            }
            cout<<"Enter height: ";
            cin>>h;  //輸入高
            while(h<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>h;
            }
            cout<<"Area of rectangle: "<<w*h<<endl;  //輸出矩形面積
        }
        else
            cout<<"Illegal input!"<<endl;
        cout<<"Enter mode: ";
    }
    return 0;
}
