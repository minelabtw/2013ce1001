#include <iostream>
using namespace std;

int main()
{
    int m;           //mode
    float r;         //半徑
    float d;         //三角形的底
    float h;         //三角形的高
    float l;         //矩形的長
    float w;         //矩形的寬
    float sum1;      //圓面積
    float sum2;      //三角形面積
    float sum3;      //矩形面積
    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;
    while (m!=-1)       //當mode不輸入1時 進入此迴圈
    {
        cout<<"Enter mode: ";
        cin>>m;
        switch (m)
        {
        case 1:
            cout<<"Enter radius: ";
            cin>>r;
            while(r<=0)
            {
                cout<<"out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>r;
            }
            sum1=3.14159*r*r;
            cout<<"Area of circle: "<<sum1;
            cout<<endl;
            break;
        case 2:
            cout<<"Enter width:";
            cin>>d;
            while(d<=0)
            {
                cout<<"out of range!"<<endl;
                cout<<"Enter width:";
                cin>>d;
            }
            cout<<"Enter height: ";
            cin>>h;
            while(h<=0)
            {
                cout<<"out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>h;
            }
            sum2=0.5*d*h;
            cout<<"Area of triangle: "<<sum2;
            cout<<endl;
            break;
        case 3:
            cout<<"Enter width: ";
            cin>>l;
            while(l<=0)
            {
                cout<<"out of range!"<<endl;
                cout<<"Enter width: ";
                cin>>l;
            }
            cout<<"Enter height: ";
            cin>>w;
            while(w<=0)
            {
                cout<<"out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>w;
            }
            sum3=l*w;
            cout<<"Area of rectangle: "<<sum3;
            cout<<endl;
            break;
        case -1:
            break;

        default:                      //當mode不輸(1,2,3,-1)時 列印"Illegal input!"
            cout<<"Illegal input!";
            cout<<endl;
            break;
        }
    }
    return 0;
}
