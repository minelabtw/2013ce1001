#include <iostream>
#include <math.h>
using namespace std ;

int main()
{
    double radius=0 ; //宣告變數
    double width=0 ;
    double height=0 ;
    int a=0 ;
    cout << "Calculate the area." << endl ;//輸出題目所需
    cout << "Choose the mode." << endl ;
    cout << "Area of circle=1." << endl ;
    cout << "Area of triangle=2." << endl ;
    cout << "Area of rectangle=3." << endl ;
    cout << "Exit=-1." << endl ;
    while (a!=-1)//當a不等於-1時進入以下迴圈
    {
        cout << "Enter mode: " ;//輸出Enter mode:
        cin >> a ;//輸入a
        switch (a)//判斷模式
        {
        case 1://方案1
            cout << "Enter radius: " ;//輸出Enter radius:
            cin >> radius ;//輸入radius
            while (radius<=0)//當radius小於等於0進入以下迴圈
            {
                cout << "Out of range!" << endl << "Enter radius: " ;
                cin >> radius ;
            }
            cout << "Area of circle: " << (3.14159)*pow(radius,2) ; //輸出圓面積
            break ;//離開switch
        case 2://方案2
            cout << "Enter width: " ;//輸出Enter width:
            cin >> width ;//輸入width
            while (width<=0)//當width小於等於0進入以下迴圈
            {
                cout << "Out of range!" << endl << "Enter width: " ;
                cin >> width ;
            }
            cout << "Enter height: " ;//輸出Enter height:
            cin >> height ;//輸入height
            while (height<=0)//當height小於等於0進入以下迴圈
            {
                cout << "Out of range!" << endl << "Enter height: " ;
                cin >> height ;
            }
            cout << "Area of triangle: " << (0.5)*width*height ;//輸出三角形面積
            break ;//離開switch
        case 3://方案3
            cout << "Enter width: " ;//輸出Enter width:
            cin >> width ;//輸入width
            while (width<=0)//當width小於等於0時進入以下迴圈
            {
                cout << "Out of range!" << endl << "Enter width: " ;
                cin >> width ;
            }
            cout << "Enter height: " ;//輸出Enter height:
            cin >> height ;//輸入height
            while (height<=0)//當height小於等於0時進入以下迴圈
            {
                cout << "Out of range!" << endl << "Enter height: " ;
                cin >> height ;
            }
            cout << "Area of rectangle: " << width*height ;//輸出矩形面積
        case -1://方案 -1
            break ; //離開switch
        default ://其他狀況
            cout << "Illegal input!" ;//輸出Illegal input!
            break ;
        }
        cout << endl ;
    }
    return 0 ;
}
