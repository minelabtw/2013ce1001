#include <iostream>

using namespace std;

int main()
{
    int a=0;//宣告整數a,初始值為0
    double r=0,w=0,h=0,x=0,y=0;//宣告r,w,h,x,y為浮點數,初始值為0
    cout<<"Calculate the area."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode: ";
    cin>>a;
    for(; a!=-1;)//只要不輸入-1就能重複計算
    {
        for(; a!=1&&a!=2&&a!=3;)//輸入非模式代號(1,2,3,-1)要輸出"Illegal input!" 並重複輸入
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>a;
        }
        switch(a)//有三種模式
        {
        case 1://模式1:計算圓面積
            cout<<"Enter radius: ";
            cin>>r;
            for(;r<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>r;
            }
            cout<<"Area of circle: "<<3.14159*r*r<<endl;
            break;
        case 2://模式2:計算三角形面積
            cout<<"Enter width: ";
            cin>>w;
            for(;w<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>w;
            }
            cout<<"Enter height: ";
            cin>>h;
            for(;h<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>h;
            }
            cout<<"Area of triangle: "<<w*h/2<<endl;
            break;
        case 3://模式3:計算矩形面積
            cout<<"Enter width: ";
            cin>>x;
            for(;x<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>x;
            }
            cout<<"Enter height: ";
            cin>>y;
            for(;y<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter radius: ";
                cin>>y;
            }
            cout<<"Area of rectangle: "<<x*y<<endl;
            break;
        default:
            break;
        }
        cout<<"Enter mode: ";
        cin>>a;
    }
    return 0;
}
