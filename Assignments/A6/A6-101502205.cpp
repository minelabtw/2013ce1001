#include<iostream>

using namespace std;

int main()
{
    int mode;
    const double pi = 3.14159;
    double r, w, h;
    //output instruction
    cout << "Calculate the area." << endl;
    cout << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl;
    cout << "Area of triangle=2." << endl;
    cout << "Area of rectangle=3." << endl;
    cout << "Exit=-1." << endl;
    //nonstop loop
    do{
        cout << "Enter mode: "; //enter mode
        cin >> mode;

        switch(mode)
        {
        case 1: //mode 1
            while(1)
            {
                cout << "Enter radius: "; //input radius
                cin >> r;
                if(r>0) //stop while if input is legal
                    break;
                cout << "Out of range!" << endl; //error
            }
            cout << "Area of circle: " << r*r*pi << endl; //calculate area & output
            break;
        case 2: //mode 2
            while(1)
            {
                cout << "Enter width: "; //input width
                cin >> w;
                if(w>0) //stop while if input is legal
                    break;
                cout << "Out of range!" << endl; //error
            }
            while(1)
            {
                cout << "Enter height: ";//input height
                cin >> h;
                if(h>0) //stop while if input is legal
                    break;
                cout << "Out of range!" << endl; //error
            }
            cout << "Area of triangle: " << 0.5*w*h << endl;  //calculate area & output
            break;
        case 3: //mode 3
            while(1)
            {
                cout << "Enter width: "; //input width
                cin >> w;
                if(w>0) //stop while if input is legal
                    break;
                cout << "Out of range!" << endl; //error
            }
            while(1)
            {
                cout << "Enter height: ";//input height
                cin >> h;
                if(h>0) //stop while if input is legal
                    break;
                cout << "Out of range!" << endl; //error
            }
            cout << "Area of rectangle: " << w*h << endl;  //calculate area & output
            break;
        case -1: //exit
            break;
        default: //not (1,2,3,-1)
            cout << "Illegal input!" << endl;
            break;
        }
    }
    while(mode!=-1); //exit if mode == -1
    return 0;
}
