#include<iostream>
using namespace std;

int i=0;
double input[2]= {};                    //宣告輸入用的兩個變數 型別為Double
void o_of_r(double x)                   //測試是否Out of Range 的Function o_of_r
{
    if(x<=0)
        cout << "Out of range!" << endl;
    else
        i=-1;
}
void output(int x)                      //輸出計算結果的Function output
{
    switch(x)
    {
    case 1:
        cout << "Area of circle: " << (input[0]*input[0])*3.14159 << endl;
        break;
    case 2:
        cout << "Area of triangle: " << input[0]*input[1]/2 << endl;
        break;
    case 3:
        cout << "Area of rectangle: " << input[0]*input[1] << endl;
        break;
    }
}
/*-------------------------------main_start----------------------------*/
int main(void)
{
    cout << "Calculate the area." << endl << "Choose the mode." << endl << "Area of circle=1." << endl << "Area of triangle=2." << endl << "Area of rectangle=3." << endl << "Exit=-1." << endl;

    int mode=0;
    while(mode!=-1)
    {
        cout << "Enter mode: ";
        cin >> mode;
        switch(mode)                    //判斷輸入模式
        {
        case 1:
            i=0;
            while(i!=-1)
            {
                cout << "Enter radius: ";
                cin >> input[0];
                o_of_r(input[0]);
            }
            break;
        case 2:
        case 3:
            i=0;
            while(i!=-1)
            {
                cout << "Enter width: ";
                cin >> input[0];
                o_of_r(input[0]);
            }
            i=0;
            while(i!=-1)
            {
                cout << "Enter height: ";
                cin >> input[1];
                o_of_r(input[1]);
            }
            break;
        case -1:
            break;
        default:
            cout << "Illegal input!" << endl;
            break;
        }
        output(mode);
    }
    return 0;
}
