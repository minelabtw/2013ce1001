#include<iostream>
using namespace std;
int main()
{
    int x;//宣告變數
    double t;
    double y=3.14159;
    double z;
    double w;
    cout<<"Calculate the area."<<endl;//一開始跑出以下文字敘述
    cout<<"Choose the mode."<<endl;
    cout<<"Area of circle=1."<<endl;
    cout<<"Area of triangle=2."<<endl;
    cout<<"Area of rectangle=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode: ";
    while(cin>>x and x!=-1)//當輸入x且x不等於-1時往下執行
    {
        switch(x)
        {
        case 1://輸入1
            cout<<"Enter radious: ";//要求輸入半徑
            cin>>t;
            while(t<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter radious: ";
                cin>>t;
            }
            cout<<"Area of circle: "<<t*t*y<<endl<<"Enter mode: ";
            break;
        case 2://輸入2
            cout<<"Enter width: ";//要求輸入寬
            cin>>z;
            while(z<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>z;
            }
            cout<<"Enter height: ";//要求輸入長
            cin>>w;
            while(w<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter height: ";
                cin>>w;
            }
            cout<<"Area of triangle: "<<z*w/2<<endl<<"Enter mode: ";
            break;
        case 3://輸入3
            cout<<"Enter width: ";//要求輸入寬
            cin>>z;
            while(z<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter width: ";
                cin>>z;
            }
            cout<<"Enter height: ";//要求輸入長
            cin>>w;
            while(w<=0)
            {
                cout<<"Out of range!"<<endl<<"Enter height: ";
                cin>>w;
            }
            cout<<"Area of rectangle: "<<z*w<<endl<<"Enter mode: ";
            break;
        default://其他跑出錯誤警告,要求再輸入一次
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            break;
        }
    }
    return 0;
}
