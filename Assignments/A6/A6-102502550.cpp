#include <iostream>

using namespace std;

int main()
{
    double w=0,r=0,h=0;                                        //變數宣告
    int m=0,flag=0;

    cout<<"Calculate the area.\n";                             //使用者提式
    cout<<"Choose the mode.\n";
    cout<<"Area of circle=1.\n";
    cout<<"Area of triangle=2.\n";
    cout<<"Area of rectangle=3.\n";
    cout<<"Exit=-1.\n";

    do
    {
       cout<<"Enter mode: ";                                   //輸入模式
       cin>>m;
       switch(m)                                               //判斷模式
       {
          case 1:                                              //每個模式將要求輸入直到在正確範圍
               do
               {
                   cout<<"Enter radiu s: ";
                   cin>>r;
                   if(r<=0)
                      cout<<"Out of range!\n";
               }while(r<=0);

               cout<<"Area of circle: "<<(double)3.14159*r*r;             //輸出結果
               cout<<endl;
               break;

          case 2:
                do
               {
                   cout<<"Enter width: ";
                   cin>>w;
                   if(w<=0)
                      cout<<"Out of range!\n";
               }while(w<=0);
               do
               {
                   cout<<"Enter height: ";
                   cin>>h;
                   if(h<=0)
                      cout<<"Out of range!\n";
               }while(h<=0);

               cout<<"Area of triangle: "<<(double)w*h/2;                           //輸出結果
               cout<<endl;
               break;
          case 3:
               do
               {
                   cout<<"Enter width: ";
                   cin>>w;
                   if(w<=0)
                      cout<<"Out of range!\n";
               }while(w<=0);
               do
               {
                   cout<<"Enter height: ";
                   cin>>h;
                   if(h<=0)
                      cout<<"Out of range!\n";
               }while(h<=0);

               cout<<"Area of rectangle: "<<w*h;                              //輸出結果
               cout<<endl;
               break;
          case -1:                                                            //-1時結束程式
             flag=1;
             break;
          default:
             cout<<"Illegal input!\n";                                        //1,2,3,-1以外時近入下一次選模式
             break;
       }

    }while(!flag);
    return 0;
}


