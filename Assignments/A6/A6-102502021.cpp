#include<iostream>
using namespace std;

int main()
{
    int mode=0;
    double a=0, b=0 , c=3.14159, sum=0;
    cout << "Calculate the area." << endl << "Choose the mode." << endl ;
    cout << "Area of circle=1." << endl << "Area of triangle=2." << endl << "Area of rectangle=3." << endl << "Exit=-1." << endl;

    do
    {
        cout << endl << "Enter mode:" ;
        cin >> mode;
        switch (mode)
        {
        case 1 :
            do
            {
                 cout << "Enter radius:";
                 cin >> a;
                 if ( a<=0 )
                    {
                        cout << "Out of range!";
                    }
            }
            while ( a <=0);
            sum = a*a*c;
            cout << "Area of circle:" << sum ;
            break;
        case 2 :
            do
            {
                cout << endl << "Enter width:";
                cin >> a;
                if ( a<=0 )
                {
                    cout << "Out of range!";
                }
            }
            while ( a<=0 );
            do
            {
                cout << endl << "Enter height:";
                cin >> b;
                if ( b<=0 )
                {
                    cout << "Out of range!";
                }
            }
            while ( b<=0 );
            sum = 0.5*a*b;
            cout << "Area of triangle:" << sum ;
            break;
        case 3 :
                do
                {
                    cout << endl << "Enter width:";
                    cin >> a;
                    if( a<=0 )
                    {
                        cout << "Out of range!";
                    }
                }
                while ( a<=0 );
                do
            {
                cout << endl << "Enter height:";
                cin >> b;
                if ( b<=0 )
                {
                    cout << "Out of range!";
                }
            }
            while ( b<=0 );
                sum = a*b ;
                cout << "Area of rectangle:" << sum ;
                break;
        case -1 :
            break;
        default :
            cout << "Illegal input!" << endl;
            break;
           }
        }
        while ( mode != -1);
        return 0;
}

