#include<iostream>
using namespace std;

int main()
{
    int mode;
    double pi = 3.14159;
    double r,width,height,ans;

    cout << "Calculate the area." << endl << "Choose the mode." << endl;
    cout << "Area of circle=1." << endl << "Area of triangle=2." << endl << "Area of rectangle=3." << endl << "Exit=-1." << endl;
    do                     //執行迴圈
    {
        cout << "Enter mode: ";
        cin >> mode;
        switch(mode)
        {
        case 1:           //計算圓面積
        {
            cout << "Enter radius: ";
            cin >> r;
            while(r<=0)
            {
                cout << "Illegal input!"<<endl <<"Enter radius: ";
                cin >> r;
            }
            cout << "Area of circle: " << r * r * pi<<endl;
        }
        break;
        case 2:            //計算三角形面積
        {
            cout << "Enter width: ";
            cin >> width;
            while(width<=0)
            {
                cout << "Illegal input!"<<endl <<"Enter width: ";
                cin >> width;
            }
            cout << "Enter height: ";
            cin >> height;
            while(height<=0)
            {
                cout << "Illegal input!"<<endl <<"Enter height: ";
                cin >> height;
            }
            cout <<"Area of triangle: " << width * height *.5 << endl;
        }
        break;
        case 3:              //計算矩形面積
        {
            cout << "Enter width: ";
            cin >> width;
            while(width<=0)
            {
                cout << "Illegal input!"<<endl <<"Enter width: ";
                cin >> width;
            }
            cout << "Enter height: ";
            cin >> height;
            while(height<=0)
            {
                cout << "Illegal input!"<<endl <<"Enter height: ";
                cin >> height;
            }
            cout <<"Area of rectangle: " << width * height << endl;
        }
        break;
        case -1:       //-1時,脫出迴圈
            break;
        default:     //輸入其他數時,顯示Illegal input!
        {
            cout << "Illegal input!"<<endl;
        }
        break;
        }

    }
    while(mode!=-1);          //-1時,脫出迴圈

    return 0;
}
