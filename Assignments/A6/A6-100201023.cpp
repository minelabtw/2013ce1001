#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int main()
{
	int mode; // declare input variable
	const double pi = 3.14159; // declare pi
	string trash;
	
	cout << "Calculate the area." << endl;
	cout << "Choose the mode." << endl;
	cout << "Area of circle=1." << endl;
	cout << "Area of triangle=2." << endl;
	cout << "Area of rectangle=3." << endl;
	cout << "Exit=-1." << endl;

	while(true)
	{
		cout << "Enter mode: ";
		if(!(cin >> mode)) // prevent not integer input
		{
			cin.clear();
			getline(cin , trash);
			cout << "Illegal input!" << endl;
			continue;
		}
		
		if(mode == 1) //Area of circle
		{
			double radius;

			while(true) // input ����
			{
				cout << "Enter radius: ";
				if(!(cin >> radius))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(radius > 0)
					break;
				cout << "Out of range!" << endl;
			}
			cout << "Area of circle: " << pi * radius * radius << endl;
		}
		else if(mode == 2) // Area of triangle
		{
			double width , height;

			while(true) // input width
			{
				cout << "Enter width: ";
				if(!(cin >> width))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(width > 0)
					break;
				cout << "Out of range!" << endl;
			}

			while(true) // input height
			{
				cout << "Enter height: ";
				if(!(cin >> height))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(height > 0)
					break;
				cout << "Out of range!" << endl;
			}

			cout << "Area of triangle: " << width * height / 2 << endl;
		}
		else if(mode == 3) // Area of rectangle
		{
			double width , height;

			while(true) // input width
			{
				cout << "Enter width: ";
				if(!(cin >> width))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(width > 0)
					break;
				cout << "Out of range!" << endl;
			}

			while(true) // input height
			{
				cout << "Enter height: ";
				if(!(cin >> height))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(height > 0)
					break;
				cout << "Out of range!" << endl;
			}

			cout << "Area of rectangle: " << width * height << endl;
		}
		else if(mode == -1)
			break;
		else
		{
			cout << "Illegal input!" << endl;
			continue;
		}
	}

	return 0;
}
