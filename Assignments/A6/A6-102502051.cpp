#include <iostream>
#include <math.h>
using namespace std;

int main()
{
double r=0,w=0,h=0,z=3.14159 ;//半徑,長度,寬度以及圓周率
int mode=0;

cout<<"Calculate the area."<<endl;
cout<<"Choose the mode."<<endl;
cout<<"Area of circle=1."<<endl;
cout<<"Area of triangle=2."<<endl;
cout<<"Area of rectangle=3."<<endl;
cout<<"Exit=-1."<<endl;

do
{
cout<<"Enter mode: "; //進行判定
cin>>mode;

switch(mode)
{
case 1:

cout<<"Enter radius:";//輸入半徑
cin>>r;
while (r<=0)
{
    cout<<"Out of range!"<<endl;
    cout<<"Enter radius:";
    cin>>r;
}

cout<<"Area of circle: "<<r*r*z<<endl ; //計算圓面積

break;

case 2:

cout<<"Enter width: ";//輸入寬度
cin>>w;
while (w<=0)
{   cout<<"Out of range!"<<endl;
    cout<<"Enter width: ";
    cin>>w;
}
cout<<"Enter height: ";//輸入長度
cin>>h;
while (h<=0)
{
cout<<"Out of range!"<<endl;
cout<<"Enter height: ";
cin>>h ;
}
cout<<"Area of triangle: "<<(w*h)/2<<endl; //計算三角形面積

break;
case 3:

cout<<"Enter width: ";//輸入寬度
cin>>w;
while (w<=0)
{cout<<"Out of range!"<<endl;
 cout<<"Enter width: ";
cin>>w;
}
cout<<"Enter height: ";//輸入長度
cin>>h ;
while (h<=0)
{
cout<<"Out of range!"<<endl;
cout<<"Enter height: ";
cin>>h;
}
cout<<"Area of rectangle: "<<w*h<<endl; //計算矩形面積
break;
case -1:
break;
default:
cout<<"Illegal input!"<<endl;
break;
}
}
while(mode!=-1);

return 0;
}
