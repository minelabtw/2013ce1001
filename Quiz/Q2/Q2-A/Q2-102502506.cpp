#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main ()
{
    int Throw = 0,times = 0;  //設一個變數Throw來放玩家選的拳,一個變數times來當計次器
    int record[5] = {0};  //設一個陣列可以存5個數,拿來存每次遊戲雙方的拳  沒用到XD
    cout << "Rock-paper-scissors" << endl;  //輸出遊戲名稱
    cout << "AI:Let's battle!" << endl;
    while ( times != 5 )  //若完的次數沒到5次就一直讓他玩
    {
        srand ( time ( 0 ) );  //每次進入迴圈就讓電腦取一個1-3的亂數
        int randnum = 1 + rand()%3;
        cout << "Throw1)Rock,2)Paper,3)Scissors?: ";
        cin >> Throw;
        while ( Throw < 1 || Throw > 3 )  //判斷玩家輸入的數字是否等於1-3其中之一
        {
            cout << "Out of range!" << endl;
            cout << "Throw1)Rock,2)Paper,3)Scissors?: ";
            cin >>Throw;
        }
        if ( Throw == 1 )  //若玩家輸入1"執行"下列有可能三種結果
        {
            if ( randnum ==  1)
            {
                cout << "You: Rock! VS.AI: Rock!" << endl << "Draw!" << endl;
                times ++;  //每次執行完任一種結果計次器times就加1
            }
            else if ( randnum == 2)
            {
                cout << "You: Rock! VS.AI: Paper!" << endl << "You lose!" << endl;
                times ++;
            }
            else if ( randnum == 3 )
            {
                cout << "You: Rock! VS.AI: Scissors!" << endl << "You win!" << endl;
                times ++;
            }
        }
        else if ( Throw == 2 )  //若玩家輸入2"布"執行下列有可能三種結果
        {
            if ( randnum == 1 )
            {
                cout << "You: Paper! VS.AI: Rock!" << endl << "You win!" << endl;
                times ++;
            }
            else if ( randnum == 2 )
            {
                cout << "You: Paper! VS.AI: Paper!" << endl << "Draw!" << endl;
                times ++;
            }
            else if ( randnum == 3 )
            {
                cout << "You: Paper! VS.AI: Scissors!" << endl << "You lose!" << endl;
                times ++;
            }
        }
        else if ( Throw == 3 )  //若玩家輸入3"剪刀"執行下列有可能三種結果
        {
            if ( randnum == 1 )
            {
                cout << "You: Scissors! VS.AI: Rock!" << endl << "You lose!" << endl;
                times ++;
            }
            else if ( randnum == 2 )
            {
                cout << "You: Scissors! VS.Ai: Paper!" << endl << "You win!" << endl;
                times ++;
            }
            else if ( randnum == 3 )
            {
                cout << "You: Scissors! VS.Ai: Scissors!" << endl << "Draw!" << endl;
                times ++;
            }
        }
    }
    return 0;
}
