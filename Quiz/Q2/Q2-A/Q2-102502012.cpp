#include<iostream>
#include<stdlib.h>
#include<time.h>
#define DRAW 0
#define WIN 1
#define LOSE 2
using namespace std;
void judge(int S[])  // judge whether I wins or not
{
    switch (S[0])
    {
    case 1:
        switch (S[1])
        {
        case 1:
            S[2] = DRAW;
            break;
        case 2:
            S[2] = LOSE;
            break;
        case 3:
            S[2] = WIN;
            break;
        }
        break;
    case 2:
        switch (S[1])
        {
        case 1:
            S[2] = WIN;
            break;
        case 2:
            S[2] = DRAW;
            break;
        case 3:
            S[2] = LOSE;
            break;
        }
        break;
    case 3:
        switch (S[1])
        {
        case 1:
            S[2] = LOSE;
            break;
        case 2:
            S[2] = WIN;
            break;
        case 3:
            S[2] = DRAW;
            break;
        }
        break;
    }
}
char* convert(int i)  // make integer convert into string
{
    char *str = "";
    switch (i)
    {
    case 1:
        str = "Rock!";
        break;
    case 2:
        str = "Paper!";
        break;
    case 3:
        str = "Scissors!";
        break;
    }
    return str;
}
int main()
{
    int result[5][3]; // result[i][0]->player result[i][1]->computer result[i][2]->outcome
    srand((unsigned)time(NULL));
    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle" << endl;
    for (int i = 0; i < 5; i++)
    {
        do
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> result[i][0];
        }
        while ((result[i][0]>3 || result[i][0] < 1) && cout << "Out of range!" << endl);
        result[i][1] = rand() % 3 + 1; // rand generate gesture
        judge(result[i]);
        cout << "Rock! Paper! Scissors!" << endl;
        cout << "You: " << convert(result[i][0]) << " VS. AI: " << convert(result[i][1]) << endl;
        switch (result[i][2])
        {
        case WIN:
            cout << "You win!" << endl;
            break;
        case LOSE:
            cout << "You lose!" << endl;
            break;
        case DRAW:
            cout << "Draw!" << endl;
            break;
        }
        cout << endl;
    }
    for (int i = 0; i < 5; i++)
    {
        cout << i + 1 << " - You: " << convert(result[i][0]) << " VS. AI: " << convert(result[i][1]) << endl;
    }
    return 0;
}
