#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;


    for(int i=1; i<=5; ++i)
    {
        srand(time(0));    //亂數
        int num1=rand()%4,num2=0;    //取不大於4的值存入num1

        if(i>1)
            cout << endl << endl;

        while(num2>3 || num2<1)
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> num2;
            if(num2>3 || num2<1)    //取1~3的值存入num2，否則輸出Out of range!
                cout << "Out of range!" << endl;
        }

        cout << "Rock! Paper! Scissors!" << endl;

        switch(num2)  //剪刀石頭布遊戲開始
        {
        case 1:
            if(num1==1)
            {
                cout << "You:Rock! VS. AI:Rock!" << endl;
                cout << "Draw!";
            }

            else if(num1==2)
            {
                cout << "You:Rock! VS. AI:Paper!" << endl;
                cout << "You lose!";
            }

            else
            {
                cout << "You:Rock! VS. AI:Scissors!" << endl;
                cout << "You win!";
            }
            break;
        case 2:
            if(num1==1)
            {
                cout << "You:Paper! VS. AI:Rock!" << endl;
                cout << "You win!";
            }

            else if(num1==2)
            {
                cout << "You:Paper! VS. AI:Paper!" << endl;
                cout << "Draw!";
            }

            else
            {
                cout << "You:Paper! VS. AI:Scissors!" << endl;
                cout << "You lose!";
            }
            break;
        case 3:
            if(num1==1)
            {
                cout << "You:Scissors! VS. AI:Rock!" << endl;
                cout << "You lose!";
            }

            else if(num1==2)
            {
                cout << "You:Scissors! VS. AI:Paper!" << endl;
                cout << "You win!";
            }

            else
            {
                cout << "You:Scissors! VS. AI:Scissors!" << endl;
                cout << "Draw!";
            }
            break;
        default:
            break;
        }
    }

    return 0;
}
