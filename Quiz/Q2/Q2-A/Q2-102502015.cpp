#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;
void judge(int a)                   //判斷出的拳
{
    if(a==1)
    {
        cout<<"Rock";
    }
    if(a==2)
    {
        cout<<"Paper";
    }
    if(a==3)
    {
        cout<<"Scissors";
    }
}
void get (int &player)              //取得要出的拳
{
    do
    {
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>player;
        if(player>3 || player<1)    //超出範圍則重輸入
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(player>3 || player<1);
}
int main()
{
    int ai;                 //電腦出的拳
    int player;             //玩家出的拳
    int count=0;            //記錄出拳的次數
    int saveai[6];          //儲存電腦出拳
    int saveplayer[6];      //儲存玩家出拳
    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;
    srand(time(0));         //初始化亂數
    for (int j=1; j<=5; j++)//迴圈5次
    {
        get(player);        //取得出拳
        count++;            //紀錄出拳數
        saveplayer[count]=player;       //記錄到陣列
        ai=rand()%3+1;                  //電腦出拳
        saveai[count]=ai;               //記錄到陣列
        cout<<"Rock! Paper! Scissors!"<<endl;
        cout<<"You: ";
        judge(player);                  //判斷出的拳
        cout<<"! VS. AI: ";
        judge(ai);                      //判斷出的拳
        cout<<"!"<<endl;
        if (player==ai)                 //一樣則平手
        {
            cout<<"Draw!";
        }
        else if ((player==1 && ai==3) || (player==2 && ai==1) || (player==3 && ai==2))  //三種情況贏
        {
            cout<<"You win!";
        }
        else                            //剩餘輸
        {
            cout<<"You lose!";
        }
        cout<<endl<<endl;
    }
    for(int i=1; i<=count; i++)         //輸出紀錄
    {
        cout<<i<<" - You: ";
        judge(saveplayer[i]);           //判斷紀錄拳
        cout<<"! VS. AI: ";
        judge(saveai[i]);               //判斷紀錄拳
        cout<<"!";
        if(i!=count)
        {
            cout<<endl;
        }
    }
    return 0;
}
