#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
void change(int);
int main()
{
    int qus,ans;
    int record[6]= {};
    int recordc[6]= {};
    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;
    for(int i=0; i<5; i++)
    {
        srand(time(0));                                                 //Line15 and 16:let computer choose number
        qus=1+rand()%3;
        do                                                              //Line 18-26:let user choose what they want to throw
        {
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>ans;
            if(ans>3||ans<1)
                cout<<"Out of range!\n";
            else
                record[i]=ans;
            recordc[i]=qus;
        }
        while(ans>3||ans<1);
        cout<<"Rock! Paper! Scissors!\n";                               //Line 27-39:print the result
        cout<<"You: ";
        change(ans);
        cout<<"! VS. AI: ";
        change(qus);
        cout<<"!"<<endl;
        if(ans==qus)
            cout<<"Draw!\n";
        else if((ans==1&&qus==3)||(ans==2&&qus==1)||(ans==3&&qus==2))
            cout<<"You win!\n";
        else if((ans==1&&qus==2)||(ans==2&&qus==3)||(ans==3&&qus==1))
            cout<<"You lose!\n";
    }
    for(int i=0; i<5; i++)                                              //Line 40-48:print the record of the game
    {
        cout<<i+1<<" - ";
        cout<<"You: ";
        change(record[i]);
        cout<<"! VS. AI: ";
        change(recordc[i]);
        cout<<"!"<<endl;
    }
    return 0;
}
void change(int x)                                                      //Line 51-59:make a function to change the number to rock paper and scessors
{
    if(x==1)
        cout<<"Rock";
    else if(x==2)
        cout<<"Paper";
    else if(x==3)
        cout<<"Scissors";
}
