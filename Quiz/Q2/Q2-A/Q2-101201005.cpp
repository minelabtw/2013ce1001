#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0)); //亂數種子
    int b; //輸入數字
    int A[5];
    int j;
    cout << "Rock-paper-scissors\nAI: Let's battle!\n";

    for (int i=0; ++i; i<5)
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin >> b;

        while( b>3 || b<0 )
        {
            cout << "Out of range!\n";
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> b;
        }

        int a=(rand()%3)+1; //亂數1~3
        cout << "Rock! Paper! Scissors!\n";

        if (a==1)
        {
            if(b==1) //平手
            {
                cout << "You: Rock! VS. AI: Rock!";
                cout << "\nDraw!\n\n";
            }
            else if(b==2) //贏
            {
                cout << "You: Paper! VS. AI: Rock!";
                cout << "\nYou win!\n\n";
            }
            else if(b==3) //輸
            {
                cout << "You: Scissors! VS. AI: Rock!";
                cout << "\nYou lose!\n\n";
            }
        }
        else if (a==2)
        {
            if(b==2) //平手
            {
                cout << "You: Paper! VS. AI: Paper!";
                cout << "\nDraw!\n\n";
            }
            else if(b==3) //贏
            {
                cout << "You: Scissors! VS. AI: Paper!";
                cout << "\nYou win!\n\n";
            }
            else if(b==1) //輸
            {
                cout << "You: Rock! VS. AI: Paper!";
                cout << "\nYou lose!\n\n";
            }
        }
        else if (a==3)
        {
            if(b==3) //平手
            {
                cout << "You: Scissors! VS. AI: Scissors!";
                cout << "\nDraw!\n\n";
            }
            else if(b==1) //贏
            {
                cout << "You: Rock! VS. AI: Scissors!";
                cout << "\nYou win!\n\n";
            }
            else if(b==2) //輸
            {
                cout << "You: Paper! VS. AI: Scissors!";
                cout << "\nYou lose!\n\n";
            }
        }
        if (i==5)
            break;
    }

    return 0;
}
