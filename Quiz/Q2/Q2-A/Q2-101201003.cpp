#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{

    int a=0;
    int i=0;
    string n[5];             //玩家結果的陣列
    string c[5];             //電腦結果的陣列
    string result(int a);    //函數



    cout<<"Rock-paper-scissors"<<endl<<"AI: Let's battle!"<<endl;//開始

    for(int i=0; i<5;) //做五次猜拳
    {
        srand(time(0));          //亂數隨時間改變
        int randnum=(rand()%3)+1;//隨機選號1~3

        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>a;//輸入選擇
        n[i]=result(a);
        c[i]=result(randnum);//儲存到陣列
        while(a<1||a>3)
        {
            cout<<"Out of range!"<<endl<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>a;
        }//輸入值不符合

        cout<<"Rock! Paper! Scissors!"<<endl
            <<"You: "<<result(a)<< "VS. AI: "<<result(randnum);
        cout<<endl;
        if(randnum==1&&a==3)
            cout<<"You lose!"<<endl;
        else if(randnum==2&&a==1)
            cout<<"You lose!"<<endl;
        else if(randnum==3&&a==2)
            cout<<"You lose!"<<endl;
        else if(randnum==3&&a==1)
            cout<<"You win!"<<endl;
        else if(randnum==1&&a==2)
            cout<<"You win!"<<endl;
        else if(randnum==2&&a==3)
            cout<<"You win!"<<endl;
        else if(randnum==a)
            cout<<"Draw!"<<endl;    //比賽判斷
        i++;
    }
    for(int i=0; i<5; i++)
        cout<<i+1<<" - You: "<<n[i]<<" VS. AI: "<<c[i]<<endl;//輸出結果
    return 0;
}
string result(int a)                //函數回傳值
{
    if(a==1)
        return "Rock! ";
    else if(a==2)
        return "Paper! ";
    else if(a==3)
        return "Scissors! ";

}

