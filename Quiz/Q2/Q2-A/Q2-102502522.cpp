#include<iostream>
#include <stdlib.h>
#include<ctime>
#include<iomanip>
using namespace std;

main()
{
    int yourthrow;//你出拳
    int throwtime;//玩的次數
    int AIsthrow;//電腦出拳
    int throwtimes[5];//各次出拳的組合
    srand(time(0));

    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;

    for(throwtime=1;throwtime<=5;throwtime++)//共玩五次
    {
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>yourthrow;
        AIsthrow=rand()%3+1;//隨意1到3


        if(yourthrow==1)
        {
            if(AIsthrow==1)
            {
                cout<<"You: Rock! VS. AI: Rock!"<<endl;
                cout<<"Draw!"<<endl;
                throwtimes[throwtime]=1;//把此類組合標註為1
            }
            else if(AIsthrow==2)
            {
                cout<<"You: Rock! VS. AI: Paper!"<<endl;
                cout<<"You Lose!"<<endl;
                throwtimes[throwtime]=2;//依序
            }
            else
            {
                cout<<"You: Rock! VS. AI: Scissors!"<<endl;
                cout<<"You Win!"<<endl;
                throwtimes[throwtime]=3;
            }
        }
        else if(yourthrow==2)
        {
             if(AIsthrow==1)
            {
                cout<<"You: Paper! VS. AI: Rock!"<<endl;
                cout<<"You Win!"<<endl;
                throwtimes[throwtime]=4;
            }
            else if(AIsthrow==2)
            {
                cout<<"You: Paper! VS. AI: Paper!"<<endl;
                cout<<"Draw!"<<endl;
                throwtimes[throwtime]=5;
            }
            else
            {
                cout<<"You: Paper! VS. AI: Scissors!"<<endl;
                cout<<"You Lose!"<<endl;
                throwtimes[throwtime]=6;
            }
        }
        else if(yourthrow==3)
            {
               if(AIsthrow==1)
            {
                cout<<"You: Scissors! VS. AI: Rock!"<<endl;
                cout<<"You Lose!"<<endl;
                throwtimes[throwtime]=7;
            }
            else if(AIsthrow==2)
            {
                cout<<"You: Scissors! VS. AI: Paper!"<<endl;
                cout<<"You Win!!"<<endl;
                throwtimes[throwtime]=8;
            }
            else
            {
                cout<<"You: Scissors! VS. AI: Scissors!"<<endl;
                cout<<"Draw!"<<endl;
                throwtimes[throwtime]=9;
            }
            }
        else
            {
                cout<<"Out of range!"<<endl;
                throwtime--;

            }

        }
        for(int times=1;times<=5;times++)//最後的輸出
        {
            if(throwtimes[times]==1)//1到9分別代表9種不同的組合
            {
                cout<<times<<" "<<"-"<<" "<<"You: Rock! VS. AI: Rock!"<<endl;
            }
            if(throwtimes[times]==2)
            {
                cout<<times<<" "<<"-"<<" "<<"You: Rock! VS. AI: Paper!"<<endl;
            }
            if(throwtimes[times]==3)
            {
                cout<<times<<" "<<"-"<<" "<<"You: Rock! VS. AI: Scissors!"<<endl;
            }
            if(throwtimes[times]==4)
            {
                cout<<times<<" "<<"-"<<" "<<"You: Paper! VS. AI: Rock!"<<endl;
            }
            if(throwtimes[times]==5)
            {
                cout<<times<<" "<<"-"<<" "<<"You: Paper! VS. AI: Paper!"<<endl;
            }
            if(throwtimes[times]==6)
            {
                cout<<times<<" "<<"-"<<" "<<"You: Paper! VS. AI: Scissors!"<<endl;
            }
            if(throwtimes[times]==7)
            {
                cout<<times<<" "<<"-"<<" "<<"You: Scissors! VS. AI: Rock!"<<endl;
            }
            if(throwtimes[times]==8)
            {
                cout<<times<<" "<<"-"<<" "<<"You: Scissors! VS. AI: Paper!"<<endl;
            }
            if(throwtimes[times]==9)
            {
                cout<<times<<" "<<"-"<<" "<<"You: Scissors! VS. AI: Scissors!"<<endl;
            }

        }
        return 0;
    }

