#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int userHand, aiHand, record[6][3] = {};  //userHand 與 aiHand 分別表示玩家和電腦出拳的代碼，record 陣列則記錄每次的猜拳結果，拋棄第0個
    string Hand[4] = { "", "Rock!", "Paper!", "Scissors!" };  //代碼對應到拳的種類，1石頭、2布、3剪刀，拋棄Hand[0]

    srand( time(0) );  //用當前時間當亂數種子
    cout << "Rock-paper-scissors" << endl << "AI: Let's battle!" << endl;  //Yoooo杯偷！

    for ( int counter = 1; counter <= 5; counter++ )  //可以玩五次
    {
        do
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: "; //剪.刀.石.頭 ~~
            cin >> userHand; //布！！
        }
        while( !( userHand == 1 || userHand == 2 || userHand == 3) && (cout << "Out of range!" << endl) ); //如果你亂出的話會被電腦抗議，然後重來

        aiHand = rand()%3 + 1; //電腦利用亂數出拳
        cout << "Rock! Paper! Scissors!" << endl << "You: " << Hand[userHand] << " Vs. AI: " << Hand[aiHand] << endl; //雙方出的拳

        if ( userHand == aiHand )
            cout << "Draw!"; //兩個都一樣就是平手
        else if ( (userHand == 1 && aiHand == 3) || (userHand == 2 && aiHand == 1) || (userHand == 3 && aiHand == 2) )
            cout << "You win!"; //當你的剪刀剪破電腦的布、你的石頭撞壞電腦的剪刀，或是你的布包住電腦的石頭時，恭喜你贏了！
        else
            cout << "You lose!"; //喔喔~輸了 :(

        cout << endl << endl;   //換行換行
        record[counter][1] = userHand;
        record[counter][2] = aiHand;  //把雙方出拳記錄下來
    }

    for ( int counter = 1; counter <= 5; counter++ )
        cout << counter << " - You: " << Hand[ record[counter][1] ] << " VS. AI: " << Hand[ record[counter][2] ] << endl;  //五次的戰果檢視
    return 0; //結束！
}
