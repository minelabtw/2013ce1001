#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>
using namespace std;
void Result(int x, int y)//判斷及輸出輸、贏、或平手
{
    if(x==y)
        cout << "Draw!\n";
    else if((x==y+1) || (x==1 && y==3))
        cout << "You win!\n";
    else if((x==y-1) || (x==3 && y==1))
        cout << "You lose!\n";

}
void Rps(int x)//判斷及輸出拳型
{
    if(x==1)
        cout << "Rock!";
    else if(x==2)
        cout << "Paper!";
    else if(x==3)
        cout << "Scissors!";
}
int main()
{
    srand(time(0));
    int input=0, ai=0, save[5][2]={};//宣告變數為整數型態，input為玩家出拳、ai為電腦出拳、save儲存過程

    cout << "Rock-paper-scissors\n" << "AI: Let's battle!\n";
    for(int i=0; i<5; i++)//共玩5次
    {
        ai=1+rand()%3;
        do//判斷輸入是否在範圍內，否則發出警告並從新輸入
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> input;
        }
        while(input!=1 && input!=2 && input!=3 &&(cout << "Out of range!\n"));
        save[i][0]=input;
        save[i][1]=ai;
        cout << "Rock! Paper! Scissors!\n";
        cout << "You: ";
        Rps(input);
        cout << " VS. AI: ";
        Rps(ai);
        cout << endl;
        Result(input, ai);
        cout << endl;
    }
    for(int j=0; j<5; j++)//輸出過程
    {
        cout << j+1 << " - You: ";
        Rps(save[j][0]);
        cout << " VS. AI: ";
        Rps(save[j][1]);
        cout << endl;
    }
    return 0;
}
