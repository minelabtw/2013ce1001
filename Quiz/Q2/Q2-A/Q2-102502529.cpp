#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main()
{
    int ai=0,player=0;
    int counter=0;
    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;
    do
    {
        srand(time(0));                                                 //seed
        ai=rand()%3+1;                                                  //隨機1~3
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>player;
        if(player==1||player==2||player==3)                             //符合範圍的
        {
            cout<<"Rock! Paper! Scissors!"<<endl;
            if(player==ai)                                              //以下全為判斷輸贏 或平手
            {
                if(player==1)
                {
                    cout<<"You: Rock! VS. AI: Rock!"<<endl;
                    cout<<"Draw!"<<endl;
                    cout<<endl;
                }
                if(player==2)
                {
                    cout<<"You: Paper! VS. AI: Paper!"<<endl;
                    cout<<"Draw!"<<endl;
                    cout<<endl;
                }
                if(player==3)
                {
                    cout<<"You: Scissors! VS. AI: Scissors!"<<endl;
                    cout<<"Draw!"<<endl;
                    cout<<endl;
                }
            }

            if(player==1)
            {
                if(ai==2)
                {

                    cout<<"You: Rock! VS. AI: Paper!"<<endl;
                    cout<<"You lose!"<<endl;
                    cout<<endl;
                }
                if(ai==3)
                {

                    cout<<"You: Rock! VS. AI: Scissors!"<<endl;
                    cout<<"You win!"<<endl;
                    cout<<endl;
                }
            }
            if(player==2)
            {
                if(ai==1)
                {
                    cout<<"You: Paper! VS. AI: Rock!"<<endl;
                    cout<<"You win!"<<endl;
                    cout<<endl;
                }
                if(ai==3)
                {
                    cout<<"You: Paper! VS. AI: Scissors!"<<endl;
                    cout<<"You lose!"<<endl;
                    cout<<endl;
                }
            }
            if(player==3)
            {
                if(ai==1)
                {
                    cout<<"You: Scissors! VS. AI: Rock!"<<endl;
                    cout<<"You lose!"<<endl;
                    cout<<endl;
                }
                if(ai==2)
                {
                    cout<<"You: Scissors! VS. AI: Paper!"<<endl;
                    cout<<"You win!"<<endl;
                    cout<<endl;
                }
            }


            counter++;

        }
        else                                                        //不合的
        {
            cout<<"Out of range!"<<endl;
        }


    }
    while(counter<5);
}

