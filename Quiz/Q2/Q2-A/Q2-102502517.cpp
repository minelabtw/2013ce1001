#include <iostream>
#include <cstdlib> //使用srand和rand
#include <ctime> //使用time

using namespace std;

int main()
{
    int player_throw = 0;
    srand(time(0)); //以時間為seed的隨機變數
    const int player_arraySize = 4; //設定arraysize
    int p[player_arraySize] = {};
    const int AI_arraySize = 4; //設定arraysize
    int a[AI_arraySize] = {};
    int played_time = 0;

    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;

    do
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin >> player_throw;

        if (player_throw!=1 and player_throw!=2 and player_throw!=3)
            cout << "Out of range!" << endl;
        else if (player_throw==1 and rand()%3+1==1)
        {
            cout << "You: Rock! VS. AI: Rock!" << endl;
            cout << "Draw!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 1;
            played_time++;
        }
        else if (player_throw==2 and rand()%3+1==2)
        {
            cout << "You: Paper! VS. AI: Paper!" << endl;
            cout << "ScissorsDraw!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 2;
            played_time++;
        }
        else if (player_throw==3 and rand()%3+1==3)
        {
            cout << "You: Scissors! VS. AI: Scissors!" << endl;
            cout << "Draw!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 3;
            played_time++;
        }
        else if (player_throw==1 and rand()%3+1==2)
        {
            cout << "You: Rock! VS. AI: Paper!" << endl;
            cout << "You lose!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 2;
            played_time++;
        }
        else if (player_throw==1 and rand()%3+1==3)
        {
            cout << "You: Rock! VS. AI: Sissors!" << endl;
            cout << "You win!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 3;
            played_time++;
        }
        else if (player_throw==2 and rand()%3+1==1)
        {
            cout << "You: Paper! VS. AI: Rock!" << endl;
            cout << "You win!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 1;
            played_time++;
        }
        else if (player_throw==2 and rand()%3+1==3)
        {
            cout << "You: Paper! VS. AI: Scissors!" << endl;
            cout << "You lose!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 3;
            played_time++;
        }
        else if (player_throw==3 and rand()%3+1==1)
        {
            cout << "You: Scissors! VS. AI: Rock!" << endl;
            cout << "You lose!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 1;
            played_time++;
        }
        else if (player_throw==3 and rand()%3+1==2)
        {
            cout << "You: Scissors! VS. AI: Paper!" << endl;
            cout << "You win!" <<endl;
            p[played_time] = player_throw;
            a[played_time] = 2;
            played_time++;
        }
    }
    while (played_time!=5);

    for(int i = 0; i<played_time; i++) //輸出結果
    {
        cout << i+1 << " - You: ";
        {
            if (p[i]==1)
                cout << "Rock";
            else if (p[i]==2)
                cout << "Paper";
            else if (p[i]==3)
                cout << "Scissors";
        }
        cout << "! VS. AI: ";
        {
            if (a[i]==1)
                cout << "Rock";
            else if (a[i]==2)
                cout << "Paper";
            else if (a[i]==3)
                cout << "Scissors";
        }
        cout << "!" << endl;
    }

    return 0;
}
