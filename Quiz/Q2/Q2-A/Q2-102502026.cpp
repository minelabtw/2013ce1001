//Quiz 2 - 102502026

#include<iostream>
#include<iomanip>
#include<ctime>
#include<stdlib.h>
using namespace std;
int main()  //start program
{
    int num=0;  //define the number typed
    int random=0;   //define the Ai answer
    int N=1;    //couter
    srand(time(0));

    cout<<"Rock-paper-scissors\n";
    cout<<"AI: Let's battle!\n";

    while(N<=5) //when the counter is <=5 do...
    {
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>num;
        random=rand()%3+1;  //roll each game
        if (num<4 &&num >0) //if num is 1~3
            N++;    //the counter+1
        if(num<4 && num>0)  //if num is 1~3
            cout<<"Rock! Paper! Scissors!\n";   //print
        switch(num) //cases for 1,2,3
        {
        case 1: //when player type 1
            if(random==1)
            {
                cout<<"You: Rock! VS. AI: Rock!\n";
                cout<<"Draw!\n\n";
            }
            if(random==2)
            {
                cout<<"You: Rock! VS. AI: Paper!\n";
                cout<<"You lose!\n\n";
            }
            if(random==3)
            {
                cout<<"You: Rock! VS. AI: Scissors!\n";
                cout<<"You win!\n\n";
            }

            break;

        case 2: //when player type 2
            if(random==1)
            {
                cout<<"You: Paper! VS. AI: Rock!\n";
                cout<<"You win!\n\n";
            }
            if(random==2)
            {
                cout<<"You: Paper! VS. AI: Paper!\n";
                cout<<"Draw!\n\n";
            }
            if(random==3)
            {
                cout<<"You: Paper! VS. AI: Scissors!\n";
                cout<<"You lose!\n\n";
            }
            break;

        case 3: //when player type 3
            if(random==1)
            {
                cout<<"You: Scissors! VS. AI: Rock!\n";
                cout<<"You lose!\n\n";
            }
            if(random==2)
            {
                cout<<"You: Scissors! VS. AI: Paper!\n";
                cout<<"You win!\n\n";
            }
            if(random==3)
            {
                cout<<"You: Scissors! VS. AI: Scissors!\n";
                cout<<"Draw!\n\n";
            }
            break;

        default:                                //if is another number
            cout<<"Out of range!\n";           //print error
            break;
        }   //end switch
    }   //end while
    return 0;
}   //end program
