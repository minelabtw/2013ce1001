#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0));  //以time(0)當種子
    rand();
    int ai=rand()%4+1;  //宣告電腦的出拳結果為1~3的任意亂數整數
    int player;  //宣告玩家出拳結果

    cout << "Rock-paper-scissors" << endl << "AI: Let's battle!" << endl;
    for (int i=1; i<=5; i++)  //玩五次猜拳
    {
        do  //可重複輸入直到玩家輸入值在範圍內
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> player;
            if (player!=1 && player!=2 && player!=3)
                cout << "Out of range!" << endl;
        }
        while (player!=1 && player!=2 && player!=3);

        cout << "Rock! Paper! Scissors!" << endl;
        if (player==1)  //若玩家出石頭
        {
            if (ai==1)
                cout << "You: Rock! VS. AI: Rock!" << endl << "Draw!" << endl;
            else if (ai==2)
                cout << "You: Rock! VS. AI: Paper!" << endl << "You lose!" << endl;
            else
                cout << "You: Rock! VS. AI: Scissors!" << endl << "You win!" << endl;
        }
        if (player==2)  //若玩家出布
        {
            if (ai==1)
                cout << "You: Paper! VS. AI: Rock!" << endl << "You win!" << endl;
            else if (ai==2)
                cout << "You: Paper! VS. AI: Paper!" << endl << "Draw!" << endl;
            else
                cout << "You: Paper! VS. AI: Scissors!" << endl << "You lose!" << endl;
        }
        if (player==3)  //若玩家出剪刀
        {
            if (ai==1)
                cout << "You: Scissors! VS. AI: Rock!" << endl << "You lose!" << endl;
            else if (ai==2)
                cout << "You: Scissors! VS. AI: Paper!" << endl << "You win!" << endl;
            else
                cout << "You: Scissors! VS. AI: Scissors!" << endl << "Draw!" << endl;
        }
        ai=rand()%4+1;  //重新設定電腦出拳結果
    }

    return 0;
}
