#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

int main()
{
    int s[5];       //設玩家猜拳的集合，而集合內不會超越5所以設5
    int r[5];       //設AI猜拳的集合，而集合內不會超越5所以設5
    int k=1;      //設玩家集合內的變數
    int l=1;      //設玩家集合內的變數


    cout << "Rock-paper-scissors\n";        //輸出標題
    cout << "AI: Let's battle!\n";

    for(int a=0; a<5; a++)      //設置5次的猜拳
    {
        srand(time(0));         //設一隨機變數
        int AI=rand()%4;        //設變數不會超越4

        int input=0;        //輸入值

        while(input<=0 || input>=4)     //假如超越3或小於1進行迴圈
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> input;
            if(input<=0 || input>=4)        //假如輸入值小於1或大於3將
            {
                cout << "Out of range!\n";
            }
        }

        cout << "Rock! Paper! Scissors!\n";
        cout << "You: ";
        if(input==1)        //假設輸入值=1
        {
            cout << "Rock!";
        }
        else if(input==2)       //假設輸入值=2
        {
            cout << "Paper!";
        }
        else if(input==3)       //假設輸入值=3
        {
            cout << "Scissors!";
        }
        cout << " VS. AI: ";
        if(AI==1)       //假設AI=1
        {
            cout << "Rock!";
        }
        else if(AI==2)      ////假設AI=2
        {
            cout << "Paper!";
        }
        else if(AI==3)      ////假設AI=3
        {
            cout << "Scissors!";
        }
        cout << endl;
        if(input<4 && input>0)      //假設在範圍內，將判斷輸贏和平手
        {
            if(AI==input)
            {
                cout << "Draw!\n";
            }
            else if(input==1 && AI==3)
            {
                cout << "You win!\n";
            }
            else if(input==3 && AI==2)
            {
                cout << "You win!\n";
            }
            else if(input==2 && AI==1)
            {
                cout << "You win!\n";
            }
            else if(input==1 && AI==2)
            {
                cout << "You lose!\n";
            }
            else if(input==2 && AI==3)
            {
                cout << "You lose!\n";
            }
            else if(input==3 && AI==1)
            {
                cout << "You lose!\n";
            }
        }
        cout << endl;

        s[k]=input;     //記錄輸入值
        r[l]=AI;        //記錄AI值
        k++;
        l++;
    }

    for(int m=1; m<6; m++)      //輸出記錄
    {
        cout << m << " - You: ";
        if(s[m]==1)
        {
            cout << "Rock!";
        }
        else if(s[m]==2)
        {
            cout << "Paper!";
        }
        else if(s[m]==3)
        {
            cout << "Scissors!";
        }
        cout << " VS. AI: ";
        if(r[m]==1)
        {
            cout << "Rock!";
        }
        else if(r[m]==2)
        {
            cout << "Paper!";
        }
        else if(r[m]==3)
        {
            cout << "Scissors!";
        }
        cout << endl;
    }

    return 0;
}
