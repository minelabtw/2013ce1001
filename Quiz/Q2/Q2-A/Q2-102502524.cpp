#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

void win(int,int,int &);                                        //宣告判斷出拳函式
int main()
{
    int c1[5];                                                  //設定變數
    int c2[5];
    int a = 0;
    int r = 0;
    int q = 0;
    int i = 0;

    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;
    cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    while(cin >> a)
    {
        srand(time(NULL));                                      //AI出拳
        q=(rand()%3+1);

        if (a<1 || a>3)                                         //判斷有無超出範圍
        {
            cout << "Out of range!" << endl;
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        }
        else
        {
            win(a,q,r);                                         //計算勝負
            i++;
            c1[i]=a;                                            //儲存紀錄
            c2[i]=q;

            if (r==0)                                           //平手
            {
                cout << "Rock! Paper! Scissors!" << endl;
                cout << "You: ";
                if(a==1)
                    cout << "Rock!";
                else if (a==2)
                    cout << "Paper!";
                else if (a==3)
                    cout << "Scissors!";
                cout << " VS. AI: ";
                if(q==1)
                    cout << "Rock!";
                else if (q==2)
                    cout << "Paper!";
                else if (q==3)
                    cout << "Scissors!";
                cout << endl;
                cout << "Draw!" << endl;
            }
            else if (r==1)                                      //勝利
            {
                cout << "Rock! Paper! Scissors!" << endl;
                cout << "You: ";
                if(a==1)
                    cout << "Rock!";
                else if (a==2)
                    cout << "Paper!";
                else if (a==3)
                    cout << "Scissors!";
                cout << " VS. AI: ";
                if(q==1)
                    cout << "Rock!";
                else if (q==2)
                    cout << "Paper!";
                else if (q==3)
                    cout << "Scissors!";
                cout << endl;
                cout << "You win!" << endl;
            }
            else if (r==2)                                      //輸了
            {
                cout << "Rock! Paper! Scissors!" << endl;
                cout << "You: ";
                if(a==1)
                    cout << "Rock!";
                else if (a==2)
                    cout << "Paper!";
                else if (a==3)
                    cout << "Scissors!";
                cout << " VS. AI: ";
                if(q==1)
                    cout << "Rock!";
                else if (q==2)
                    cout << "Paper!";
                else if (q==3)
                    cout << "Scissors!";
                cout << endl;
                cout << "You lose!" << endl;
            }
        }
        if (i==5)                                               //進行五次後結束猜拳並輸出紀錄
        {
            cout << endl;
            for(int h=1; h<=i; h++)
            {
                cout << h << " - " << "You: ";
                if(c1[h]==1)
                    cout << "Rock!";
                else if (c1[h]==2)
                    cout << "Paper!";
                else if (c1[h]==3)
                    cout << "Scissors!";
                cout << " VS. AI: ";
                if(c2[h]==1)
                    cout << "Rock!";
                else if (c2[h]==2)
                    cout << "Paper!";
                else if (c2[h]==3)
                    cout << "Scissors!";
                cout << endl;
            }
            break;
        }
         cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    }

    return 0;
}

void win(int a,int q,int &r)                                    //判斷勝利與否函式
{
    if (a==q)
        r=0;
    else if (a==1 && q==3 || a==2 && q==1 || a==3 && q==2)
        r=1;
    else if (a==1 && q==2 || a==2 && q==3 || a==3 && q==1)
        r=2;
}

