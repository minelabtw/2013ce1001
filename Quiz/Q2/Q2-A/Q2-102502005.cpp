#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int playerlog[5]= {};                 //宣告需要的變數。
    int AIlog[5]= {};
    int player,AI,counter=1,result;

    cout << "Rock-paper-scissors" << endl << "AI: Let's battle!" << endl;

    while(counter <=5)
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";  //要求輸入並檢查。
        cin >> player;
        while (player<1 || player>3)
        {
            cout << "Out of range!" << endl;
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> player;
        }

        srand(time(0));
        AI = rand() % 2 +1;
        playerlog[counter] = player;
        AIlog[counter] = AI ;
        player = player * 10;
        result = player + AI;

        switch (result)                 //利用switch case一一判斷結果。
        {
        case 11:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Rock! VS. AI: Rock!" << endl;
            cout << "Draw!" << endl;
            break;

        case 12:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Rock! VS. AI: Paper!" << endl;
            cout << "You lose!" << endl;
            break;

        case 13:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Rock! VS. AI: Scissors!" << endl;
            cout << "You win!" << endl;
            break;

        case 21:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Paper! VS. AI: Rock!" << endl;
            cout << "You win!" << endl;
            break;

        case 22:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Paper! VS. AI: Paper!" << endl;
            cout << "Draw!" << endl;
            break;

        case 23:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Paper! VS. AI: Scissora!" << endl;
            cout << "You lose!" << endl;
            break;

        case 31:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Scissors! VS. AI: Rock!" << endl;
            cout << "You lose!" << endl;
            break;

        case 32:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Scissors! VS. AI: Paper!" << endl;
            cout << "You win!" << endl;
            break;

        case 33:
            cout << "Rock! Paper! Scissors!" << endl;
            cout << "You: Scissors! VS. AI: Scissors!" << endl;
            cout << "Draw!" << endl;
            break;

        default:
            break;
        }

        cout << endl;
        counter = counter +1;

    }


    for (int i=1; i<=5; i++)       //將結果逐一印出。
    {

        string As;
        string Ps;
        switch (playerlog[i])
        {
        case 1:
            Ps = "Rock!";
            break;

        case 2:
            Ps = "Paper!";
            break;

        case 3:
            Ps = "Scissors!";
            break;

        default:
            break;
        }

        switch (AIlog[i])
        {
        case 1:
            As = "Rock!";
            break;

        case 2:
            As = "Paper!";
            break;

        case 3:
            As = "Scissors!";
            break;

        default:
            break;
        }
        cout << i << " - You: " << Ps << " VS. AI: " << As << endl ;
    }


    return 0;
}
