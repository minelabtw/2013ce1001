#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    int number;
    int computer;


    srand(time(0));//設定亂數
    computer = (rand()%4);

    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!'" << endl;
    cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
    cin >> number;

    while ( number > 3 || number < 1 )//在範圍外時執行動作
    {
        cout << "Out of range!" << endl;
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
        cin >> number;
    }

    while ( number <= 3 && number >= 1 )
    {
        switch ( number )//選擇方案
        {
        case 1:
            cout << "Rock! Paper! Scissors!" << endl;//當電腦與你的出拳各種狀況時的動作
            if ( computer == 1 )
            {
                cout << "You: Rock! VS. AI: Rock!" << endl;
                cout << "Draw!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }

            if ( computer == 2 )
            {
                cout << "You: Rock! VS. AI: Paper!" << endl;
                cout << "You lose!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }
            if ( computer == 3 )
            {
                cout << "You: Rock! VS. AI: Scissors!" << endl;
                cout << "You win!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }

        case 2:
            cout << "Rock! Paper! Scissors!" << endl;
            if ( computer == 1 )
            {
                cout << "You: Paper! VS. AI: Rock!" << endl;
                cout << "You win!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }

            if ( computer == 2 )
            {
                cout << "You: Paper! VS. AI: Paper!" << endl;
                cout << "Draw!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }
            if ( computer == 3 )
            {
                cout << "You: Paper! VS. AI: Scissors!" << endl;
                cout << "You lose!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }

        case 3:
            cout << "Rock! Paper! Scissors!" << endl;
            if ( computer == 1 )
            {
                cout << "You: Scissors! VS. AI: Rock!" << endl;
                cout << "You lose!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }

            if ( computer == 2 )
            {
                cout << "You: Scissors! VS. AI: Paper!" << endl;
                cout << "You win!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }
            if ( computer == 3 )
            {
                cout << "You: Scissors! VS. AI: Scissors!" << endl;
                cout << "Draw!" << endl << endl;
                cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
                cin >> number;
            }

        default:
            cout << "Out of range!" << endl;
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?:";
            cin >> number;
        }
    }

    return 0;

}
