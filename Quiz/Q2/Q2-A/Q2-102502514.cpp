#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()

{
    void Game(int &number,int &randNum);  //宣告函式

    int number1,number2,number3,number4,number5;  //宣告五次輸入的變數
    srand(time(0));
    int randNum1=rand()%3+1;  //宣告五次亂數
    int randNum2=rand()%3+1;
    int randNum3=rand()%3+1;
    int randNum4=rand()%3+1;
    int randNum5=rand()%3+1;

    cout <<"Rock-paper-scissors\nAI: Let's battle!"<<endl;
    cout <<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    cin >>number1;
    Game(number1,randNum1);  //呼叫函式
    cout <<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    cin >>number2;
    Game(number2,randNum2);  //呼叫函式
    cout <<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    cin >>number3;
    Game(number3,randNum3);  //呼叫函式
    cout <<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    cin >>number4;
    Game(number4,randNum4);  //呼叫函式
    cout <<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    cin >>number5;
    Game(number5,randNum5);  //呼叫函式

    return 0;
}
void Game(int &number,int &randNum)
{
    while (number!=1&&number!=2&&number!=3)
    {
        cout <<"Out of range!\nThrow 1)Rock, 2)Paper, 3)Scissors?: ";
        cin >>number;
    }
    cout <<"Rock! Paper! Scissors!\n";
    if (number==1&&randNum==2)
        cout <<"You: Rock! VS. AI: Paper!\nYou lose!\n\n";
    if (number==2&&randNum==3)
        cout <<"You: Paper! VS. AI: Scissors!\nYou lose!\n\n";
    if (number==3&&randNum==1)
        cout <<"You: Scissors! VS. AI: Rock!\nYou lose!\n\n";
    if (randNum==1&&number==2)
        cout <<"You: Paper! VS. AI: Rock!\nYou win!\n\n";
    if (randNum==2&&number==3)
        cout <<"You: Scissors! VS. AI: Paper!\nYou win!\n\n";
    if (randNum==3&&number==1)
        cout <<"You: Rock! VS. AI: Scissors!\nYou win!\n\n";
    if (number==randNum)
    {
        if (number==1&&randNum==1)
            cout <<"You: Rock! VS. AI: Rock!\nDraw!\n\n";
        if (number==2&&randNum==2)
            cout <<"You: Paper! VS. AI: Paper!\nDraw!\n\n";
        if (number==3&&randNum==3)
            cout <<"You: Scissors! VS. AI: Scissors!\nDraw!\n\n";
    }
}
