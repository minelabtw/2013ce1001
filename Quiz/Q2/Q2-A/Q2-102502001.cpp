#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    int num1;                //宣告一整數變數
    srand(time(0));
    int randnum=rand()%4;  //隨機變數除以4,則只可能會有1,2,3
    int arr[5];            //宣告arr儲存玩家輸入之數
    int arr2[5];           //宣告arr2儲存隨機變數
    int index=0;

    cout<<"Rock-paper-scissors"<<endl; //顯示字串
    cout<<"AI: Let's battle!"<<endl;   //顯示字串

    for(int i=1; i<=5; i++)    //執行五次
    {
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>num1;             //輸入num1

        while(num1<1 or num1>3)     //若num1不在範圍內則重新輸入
        {
            cout<<"Out of range!"<<endl;
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors? ";
            cin>>num1;
        }

        switch(num1)
        {
        case 1:                     //若玩家輸入1(石頭)
        {
            if(randnum==1)          //隨機變數為1
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Rock! VS. AI: Rock!"<<endl;
                cout<<"Draw!"<<endl<<endl; //顯示平手
            }
            else if(randnum==2)    //隨機變數為2
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Rock! VS. AI: Paper!"<<endl;
                cout<<"You lose!"<<endl<<endl;  //顯示輸了
            }
            else if(randnum==3)   //隨機變數為3
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Rock! VS. AI: Scissors!"<<endl;
                cout<<"You win!"<<endl<<endl;  //顯示贏了
            }
        }
        break;

        case 2:                           //隨機變數為2(布)
        {
            if(randnum==1)
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Paper! VS. AI: Rock!"<<endl;
                cout<<"You win!"<<endl<<endl;
            }
            else if(randnum==2)
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Paper! VS. AI: Paper!"<<endl;
                cout<<"Draw!"<<endl<<endl;
            }
            else if(randnum==3)
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Paper! VS. AI: Scissors!"<<endl;
                cout<<"You lose!"<<endl<<endl;
            }
        }
        break;

        case 3:               //隨機變數為3(剪刀)
        {
            if(randnum==1)
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Scissors! VS. AI: Rock!"<<endl;
                cout<<"You lose!"<<endl<<endl;
            }
            else if(randnum==2)
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Scissors! VS. AI: Paper!"<<endl;
                cout<<"You win!"<<endl<<endl;
            }
            else if(randnum==3)
            {
                cout<<"Rock! Paper! Scissors!"<<endl;
                cout<<"You: Scissors! VS. AI: Scissors!"<<endl;
                cout<<"Draw!"<<endl<<endl;
            }
        }
        break;
        }
        arr[index]=num1;
        arr2[index]=randnum;
        index++;
    }

    for(int i=0; i<5; i++)    //執行五次顯示,依照所存陣列值顯示字串
    {
        if(arr[i]==1 and arr2[i]==1)
            cout<<i+1<<" - You: Rock! VS. AI: Rock!"<<endl;
        if(arr[i]==1 and arr2[i]==2)
            cout<<i+1<<" - You: Rock! VS. AI: Paper!"<<endl;
        if(arr[i]==1 and arr2[i]==3)
            cout<<i+1<<" - You: Rock! VS. AI: Scissors!"<<endl;
        if(arr[i]==2 and arr2[i]==1)
            cout<<i+1<<" - You: Paper! VS. AI: Rock!"<<endl;
        if(arr[i]==2 and arr2[i]==2)
            cout<<i+1<<" - You: Paper! VS. AI: Paper!"<<endl;
        if(arr[i]==2 and arr2[i]==3)
            cout<<i+1<<" - You: Paper! VS. AI: Scissors!"<<endl;
        if(arr[i]==3 and arr2[i]==1)
            cout<<i+1<<" - You: Scissors! VS. AI: Rock!"<<endl;
        if(arr[i]==3 and arr2[i]==2)
            cout<<i+1<<" - You: Scissors! VS. AI: Paper!"<<endl;
        if(arr[i]==3 and arr2[i]==3)
            cout<<i+1<<" - You: Scissors! VS. AI: Scissors!"<<endl;
    }

    return 0;
}
