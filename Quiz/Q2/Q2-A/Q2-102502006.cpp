#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

void kind(int kind) // ���P�_
{
    switch(kind)
    {
    case 1:
        cout << "Rock!";
        break;
    case 2:
        cout << "Paper!";
        break;
    case 3:
        cout << "Scissors!";
        break;
    default:
        break;
    }
}

void winlose(int player, int AI) // ��Ĺ�P�_
{
    if(player==AI)cout << "Draw!\n";
    else
    {
        if(player+AI==4)
        {
            if(player==1)cout << "You win!\n";
            else cout << "You loose!\n";
        }
        else
        {
            player>AI ? cout << "You win!\n" : cout << "You loose!\n";
        }
    }
}

int main()
{
    int AI; // �q������J
    int history[5][2] = {}; //�������v

    srand(time(0)); // �إ߶üƪ�

    cout << "Rock-Paper-scissors\nAI: Let's battle!\n";

    for(int i=0; i<5; i++)
    {
        int player = -1;
        while(player<0 || player>3) // ���a��J
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> player;
            if(player<0 || player>3)cout << "Out of range!\n";
        }

        AI = rand()%3+1; // AI��J

        history[i][0]=player; // ���a���v
        history[i][1]=AI; // AI���v

        cout << "Rock! Paper! Scissors!\nYou: ";
        kind(player);
        cout << " VS. AI: ";
        kind(AI);
        cout << endl;
        winlose(player, AI);
        cout << endl;
    }

    for(int i=0; i<5; i++) // ��X���v
    {
        cout << i+1 << " - You: ";
        kind(history[i][0]);
        cout << " VS. AI: ";
        kind(history[i][1]);
        cout << endl;
    }

    return 0;
}
