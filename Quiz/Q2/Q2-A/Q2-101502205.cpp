#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main(){
    int history[5][2], input, ai;
    char s[6][10]={"Rock!", "Paper!", "Scissors!", "Draw!", "You win!", "You lose!"}; //output strings
    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;
    //start game
    for(int round=0; round<5; round++){
        //The throwwwwwwwwws!!!!!!!
        while(1){
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> input;
            if(input>=1 && input<=3){ //break if legal
                input--; //input 0:Rock 1:Paper 2:Scissors
                break;
            }
            cout << "Out of range!" << endl; //error
        }
        cout << "Rock! Paper! Scissors!" << endl;
        srand(time(NULL)); //seed
        ai = rand()%3; //rand an num 0~2 0:Rock 1:Paper 2:Scissors
        history[round][0] = input; //save the game!!!!
        history[round][1] = ai; //save the world!!!!
        cout << "You: " << s[input] << " VS. AI: " << s[ai] << endl; //output
        cout << s[(input-ai+3)%3+3] << endl << endl;
        /*
        (input-ai) has five resault -2, -1, 0, 1, 2

        table as follow:
        AI/YOU!!!  Rock    Paper   Scissors
        Rock        0 D     1 W     2 L
        Paper       -1 L    0 D     1 W
        Scissors    -2 W    -1 L    0 D

        By doing this (input-ai+3)%3 , then table will be
        AI/YOU!!!  Rock    Paper   Scissors
        Rock        0 D     1 W     2 L
        Paper       2 L     0 D     1 W
        Scissors    1 W     2 L     0 D

        0:Draw  1:Win   2:Lose
        */
    }
    for(int i=0; i<5; i++){
        //output history
        cout << i+1 << " - You: " << s[history[i][0]] << " VS. ";
        cout << "AI: " << s[history[i][1]] << endl;
    }
    return 0;
}
