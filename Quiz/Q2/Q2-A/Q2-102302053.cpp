#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main()
{
    int a;//記錄使用者出拳.
    int b;//紀錄電腦出拳.
    int c = 0, d = 0;
    srand(time(NULL));//random seed.



    cout << "Rock-paper-scissors\n" << "AI: Let's battle!\n";

    for ( c = 0; c < 5; c++)//一共要做五次猜拳.
    {
        cout << "Throw 1)Rock, 2)Paper, 3)scissors?: ";
        cin >> a;
        while (a < 1 || a > 3)//檢查輸入是否符合範圍.
        {
            cout << "Out of range!\n";
            cout << "Throw 1)Rock, 2)Paper, 3)scissors?: ";
            cin >> a;
        }

        b = (rand()%3);//隨機產生一個亂數.
        //以下比對使用者和電腦的出拳,給予相應的顯示和結果.
        if (a == 1 && b == 0)//石 , 石
        {
            cout << "You: Rock! VS. AI: Rock!\n" << "Draw!\n";
        }
        if (a == 1 && b == 1)// 石 . 布
        {
            cout << "You: Rock! VS. AI: Paper!\n" << "You lose!\n";
        }
        if (a == 1 && b == 2)//石 . 剪
        {
            cout << "You: Rock! VS. AI: Scissors!\n" << "You win!\n";
        }
        if (a == 2 && b == 0)//布 . 石
        {
            cout << "You: Paper! VS. AI: Rock!\n" << "You win!\n";
        }
        if (a == 2 && b == 1)//布 . 布
        {
            cout << "You: Paper! VS. AI: Paper!\n" << "Draw!\n";
        }
        if (a == 2 && b == 2)//布 . 剪
        {
            cout << "You: Paper! VS. AI: Scissors!\n" << "You lose!\n";
        }
        if (a == 3 && b == 0)//剪 . 石
        {
            cout << "You: Scissors! VS. AI: Rock!\n" << "You lose!\n";
        }
        if (a == 3 && b == 1)//剪 . 布
        {
            cout << "You: Scissors! VS. AI: Paper!\n" << "You win!\n";
        }
        if (a == 3 && b == 2)//剪 .剪
        {
            cout << "You: Scissors! VS. AI: Scissors!\n" << "Draw!\n";
        }

    }

    system("pause");

    return 0;

}
