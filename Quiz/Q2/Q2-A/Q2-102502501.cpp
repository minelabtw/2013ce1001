#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int number = 0;     //宣告一個變數，初始值為零

    cout<<"Rock-paper-scissors"<<endl<<"AI: Let's battle!"<<endl<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";   //輸出Rock-paper-scissors換行，顯示AI: Let's battle!換行顯示Throw 1)Rock, 2)Paper, 3)Scissors?:
    cin>>number;    //輸入變數
    while (number<0) //當變數小於零時
    {
        cout<<"Out of range!"<<endl<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";  //螢幕上顯示Out of range!換行顯示Throw 1)Rock, 2)Paper, 3)Scissors?:
        cin>>number;   //輸入變數
    }
    while (number>3)    //當變數大於3時
    {
        cout<<"Out of range!"<<endl<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";  //螢幕上顯示Out of range!換行顯示Throw 1)Rock, 2)Paper, 3)Scissors?:
        cin>>number;    //輸入變數
    }
    while (number<=3)    //當變數大於3時
    {
        cout<<"Rock-paper-scissors"<<endl<<"You: Rock! VS. AI: Paper!"<<endl<<"You lose!";  //螢幕上顯示Rock-paper-scissors換行顯示You: Rock! VS. AI: Paper!
        cin>>number;    //輸入變數
    }

}
