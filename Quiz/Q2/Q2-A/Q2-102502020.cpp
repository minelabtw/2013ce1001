#include <iostream>
#include <cmath>

using namespace std;

void game(int a,int b)                             //宣告函式
{
    if(a==1&&b==1)
    {
        cout << "You: Rock! VS.AI: Rock!" << endl
             << "Draw!";
    }
    else if(a==1&&b==2)
    {
        cout << "You: Rock! VS.AI: Paper!" << endl
             << "You lose!";
    }
    else if(a==1&&b==3)
    {
        cout << "You: Rock! VS.AI: Scissors!" << endl
             << "You win!";
    }
    else if(a==2&&b==1)
    {
        cout << "You: Paper! VS.AI: Rock!" << endl
             << "You win!";
    }
    else if(a==2&&b==2)
    {
        cout << "You: Paper! VS.AI: Paper!" << endl
             << "Draw!";
    }
    else if(a==2&&b==3)
    {
        cout << "You: Paper! VS.AI: Scissors!" << endl
             << "You lose!";
    }
    else if(a==3&&b==1)
    {
        cout << "You: Scissors! VS.AI: Rock!" << endl
             << "You lose!";
    }
    else if(a==3&&b==2)
    {
        cout << "You: Scissors! VS.AI: Paper!" << endl
             << "You win!";
    }
    else if(a==3&&b==3)
    {
        cout << "You: Scissors! VS.AI: Scissors!" << endl
             << "Draw!";
    }
    cout << endl << endl;
}
int main()
{
    int number=0;
    int computer=1;

    cout << "Rock-paper-scissors" << endl                   //輸出字串
         << "AI: Let's battle!" << endl;
    for(int i=0; i<5; i++)                                  //猜五次拳
    {
        do
        {
            cout << "Throw 1)Rock,2)Paper,3)Scissors?: ";
            cin >> number;
            if(number<1 or number>3)
                cout << "Out or range!" << endl;
        }
        while(number<1 or number>3);
        cout << "Rock! Paper! Scissors!" << endl;
        game(number,computer);                              //呼叫函式
    }
    for(int j=1;j<=5;j++)                                   //列出猜拳結果
    {
        cout << j <<" - "<< "You: Rock! VS.AI: Rock!" << endl;
    }

    return 0;
}
