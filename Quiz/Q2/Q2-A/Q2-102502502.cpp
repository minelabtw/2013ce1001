#include <iostream>
#include <cstdlib>      //contain srand() and rand()
#include <ctime>        //contain time()

using namespace std;

void gn(int &a)
{
    if (a==1)
    {
        cout << "Rock!";
    }
    else if (a==2)
    {
        cout << "Paper!";
    }
    else if (a==3)
    {
        cout << "Scissors!";
    }
}
void ai(int &b)
{
    if (b==1)
    {
        cout << "Rock!";
    }
    else if (b==2)
    {
        cout << "Paper!";
    }
    else if (b==3)
    {
        cout << "Scissors!";
    }
}

int main()
{
    int Numg,Numai;
    int srand ((0));
    Numai = rand ()%4;
    cout << "Rock-paper-scissors\n" << "AI: Let's battle!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    while(cin >> Numg)
    {
        while (0<=Numg)
        {
            if(Numg==1 || Numg==2 || Numg==3)
            {
                cout << "Rock! Paper! Scissors!\n";
                break;
            }
            else
            {
                cout << "Out of range!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            }
            cin >> Numg;
        }
        while (Numg==1 || Numg==2 || Numg==3)
        {
            cout << "You: ";
            gn(Numg);
            cout << Numg << " VS. " << "AI: ";
            ai(Numai);
            cout << Numai << endl;
            if (Numg==1 && Numai!=1)
            {
                if (Numai==2)
                {
                    cout << "You lose!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
                }
                else if(Numai==3)
                {
                    cout << "You win!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
                }
            }
            if (Numg==2 && Numai!=2)
            {
                if (Numai==3)
                {
                    cout << "You lose!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
                }
                else if(Numai==1)
                {
                    cout << "You win!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
                }
            }
            if (Numg==3 && Numai!=3)
            {
                if (Numai==1)
                {
                    cout << "You lose!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
                }
                else if(Numai==2)
                {
                    cout << "You win!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
                }
            }
            if (Numg==Numai)
            {
                cout << "Draw!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            }
            cin >> Numg;
        }
    }
    return 0;
}

