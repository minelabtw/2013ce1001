#include<iostream>
#include<ctime>
#include<cstdlib>

using namespace std;

int main()
{
    int x=0;     //我選的數字是名稱為X的變數
    srand(time(0));
    int y=(rand()%3)+1;//電腦選的數字是名稱為Y的亂數

    cout<< "Rock-paper-scissors\n";
    cout<< "AI:Let's battle!\n";
    for(int t=1; t<=5; t++)            //for 迴圈重複執行五次
    {
        cout<< "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>x;
        while(x<1 || x>3)      //迴圈   超出設定範圍的狀況
        {
            cout<< "Out of range!\n";
            cout<< "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>x;
        }
        cout<< "Rock! Paper! Scissors!\n";

        if(x==1)       //假設
        {
            cout<< "You: Rock! VS. ";
            if(y==1)              //假設
            {
                cout<< "AI: Rock!\n";
                cout<< "Draw!";
            }
            if(y==2)
            {
                cout<< "AI: Paper!\n";
                cout<< "You lose!";
            }
            if(y==3)
            {
                cout<< "AI: Scissors!\n";
                cout<< "You win!";
            }
        }
        if(x==2)           //假設
        {
            cout<< "You: Paper! VS. ";
            if(y==1)
            {
                cout<< "AI: Rock!\n";
                cout<< "You win!";
            }
            if(y==2)
            {
                cout<< "AI: Paper!\n";
                cout<< "Draw!";
            }
            if(y==3)
            {
                cout<< "AI: Scissors!\n";
                cout<< "You lose!";
            }
        }
        if(x==3)               //假設
        {
            cout<< "You: Scissors! VS. ";
            if(y==1)
            {
                cout<< "AI: Rock!\n";
                cout<< "You lose!";
            }
            if(y==2)
            {
                cout<< "AI: Paper!\n";
                cout<< "You win!";
            }
            if(y==3)
            {
                cout<< "AI: Scissors!\n";
                cout<< "Draw!";
            }
        }
        cout<< endl;                //空行



    }
    return 0;


}
