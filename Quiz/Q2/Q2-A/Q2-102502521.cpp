#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
    int AI;    //設定變數
    int You;
    int t=1;
    int result;
    int show[t];

    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;

    while(t<=5)    //做5次自動彈出
    {
        srand(time(0));    //使AI之值為1~3亂數
        AI=rand()%3+1;

        do
        {
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>You;
            if(You!=1&&You!=2&&You!=3)    //判斷出拳是否合理
            {
                cout<<"Out of range!"<<endl;
            }
            else    //若合理則進入比賽
            {
                cout<<"Rock! Paper! Scissors!"<<endl;

                if(You==AI)    //平手
                {
                    if(You==1)    //平手 石頭
                    {
                        cout<<"You: Rock! VS. AI: Rock!"<<endl;
                        result=1;
                    }

                    else if(You==2)    //平手 布
                    {
                        cout<<"You: Paper! VS. AI: Paper!"<<endl;
                        result=2;
                    }

                    else    //平手 剪刀
                    {
                        cout<<"You: Scissors! VS. AI: Scissors!"<<endl;
                        result=3;
                    }

                    cout<<"Draw!"<<endl<<endl;
                }

                else if(You==1)    //沒平手出 石頭
                {
                    if(AI==2)    //AI出布 輸
                    {
                        cout<<"You: Rock! VS. AI: Paper!"<<endl;
                        cout<<"You lose!"<<endl<<endl;
                        result=4;
                    }

                    else    //AI出剪刀 贏
                    {
                        cout<<"You: Rock! VS. AI: Scissors!"<<endl;
                        cout<<"You win!"<<endl<<endl;
                        result=5;
                    }
                }

                else if(You==2)    //沒平手出 布
                {
                    if(AI==3)    //AI出剪刀 輸
                    {
                        cout<<"You: Paper! VS. AI: Scissors!"<<endl;
                        cout<<"You lose!"<<endl<<endl;
                        result=6;
                    }

                    else    //AI出石頭 贏
                    {
                        cout<<"You: Paper! VS. AI: Rock!"<<endl;
                        cout<<"You win!"<<endl<<endl;
                        result=7;
                    }
                }

                else    //沒平手出 剪刀
                {
                    if(AI==1)    //AI出石頭 輸
                    {
                        cout<<"You: Scissors! VS. AI: Rock!"<<endl;
                        cout<<"You lose!"<<endl<<endl;
                        result=8;
                    }

                    else    //AI布 贏
                    {
                        cout<<"You: Scissors! VS. AI: Paper!"<<endl;
                        cout<<"You win!"<<endl<<endl;
                        result=9;
                    }
                }
            }
        }
        while(You!=1&&You!=2&&You!=3);

        show[t]=result;    //將每次結果儲存

        t++;    //t遞增
    }

    t=1;    //重新設定t為1

    while(t<=5)    //列印出五場結果
    {
        switch(show[t])
        {
        case 1:
            cout<<t<<" - You: Rock! VS. AI: Rock!"<<endl;
            break;

        case 2:
            cout<<t<<" - You: Paper! VS. AI: Paper!"<<endl;
            break;

        case 3:
            cout<<t<<" - You: Scissors! VS. AI: Scissors!"<<endl;
            break;

        case 4:
            cout<<t<<" - You: Rock! VS. AI: Paper!"<<endl;
            break;

        case 5:
            cout<<t<<" - You: Rock! VS. AI: Scissors!"<<endl;
            break;

        case 6:
            cout<<t<<" - You: Paper! VS. AI: Scissors!"<<endl;
            break;

        case 7:
            cout<<t<<" - You: Paper! VS. AI: Rock!"<<endl;
            break;

        case 8:
            cout<<t<<" - You: Scissors! VS. AI: Rock!"<<endl;
            break;

        case 9:
            cout<<t<<" - You: Scissors! VS. AI: Paper!"<<endl;
            break;

        default:
            break;
        }
        t++;    //t遞增
    }

    return 0;
}
