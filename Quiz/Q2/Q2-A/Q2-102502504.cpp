#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
using namespace std;

void com(int &computer);

int main()
{
    srand(time(0));
    int computer=(rand()%3)+1; //電腦選的亂數(1~3)
    int num; //玩家輸入的數

    cout << "Rock-paper=scissors" << endl;
    cout << "AI: Let's battle!" << endl;
    cout << "Throw 1)Rock, 2)Paper 3)Scissors: ";
    cin >> num; //輸入值

    while(num<1||num>3) //排出超出範圍的
    {
        cout << "Out of range!" << endl;
        cout << "Throw 1)Rock, 2)Paper 3)Scissors: ";
        cin >> num;
    }

    switch(num) //利用switch重復輸入
    {
    case 1:
        cout << "Rock! Paper! Scissors! " << endl;
        cout << "You: Rock! VS. AI: " <<computer<<" !";
        break;
    case 2:
        cout << "Rock! Paper! Scissors! " << endl;
        cout << "You: Paper! VS. AI: " <<computer<< " !";
        break;
    case 3:
        cout << "Rock! Paper! Scissors! " << endl;
        cout << "You: Paper! VS. AI: " <<computer<<" !";
        break;


    }

    return 0;
}
void com(int &computer) //當computer=1或2或3時分別顯示設定的拳
{
    if(computer==1)
    {
        cout << "Rock! ";
    }
    if(computer==2)
    {
        cout << "Paper! ";
    }
    if(computer==3)
    {
        cout << "Scossors! ";
    }
}
