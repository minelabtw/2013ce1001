#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

int main()
{
    int rock=1,paper=2,scissors=3;
    int a=0,t=5,i=0;
    cout<<"Rock-paper-scissors\nAI: Let's battel!\n";
    do
    {
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>a;
        srand(time(0));
        int hand=rand()%3+1;
        if(a<=0||a>3)                             //判斷輸入數字是否符合範圍
        {
            cout<<"Out of range!\n";
        }
        if(a>0&&a<4)

        {
            cout<<"Rock! Paper! Scissors!\n";
            switch(a)
            {
            case 1:
                if(hand==1)
                {
                    cout<<"You: Rock! VS. AI: Rock!\n";
                    cout<<"Draw!\n";
                    cout<<endl;
                }
                else if (hand==2)
                {
                    cout<<"You: Rock! VS. AI: Paper!\n";
                    cout<<"You lose!\n";
                    cout<<endl;
                }
                else
                {
                    cout<<"You: Rock! VS. AI: Scissors!\n";
                    cout<<"You win!\n";
                    cout<<endl;
                }
                break;
            case 2:
                if(hand==1)
                {
                    cout<<"You: Paper! VS. AI: Rock!\n";
                    cout<<"You win!\n";
                    cout<<endl;
                }
                else if (hand==2)
                {
                    cout<<"You: Paper! VS. AI: Paper!\n";
                    cout<<"Draw!\n";
                    cout<<endl;
                }
                else
                {
                    cout<<"You: Paper! VS. AI: Scissors!\n";
                    cout<<"You lose!\n";
                    cout<<endl;
                }
                break;
            case 3:
                if(hand==1)
                {
                    cout<<"You: Scissors! VS. AI: Rock!\n";
                    cout<<"You lose!\n";
                    cout<<endl;
                }
                else if (hand==2)
                {
                    cout<<"You: Scissors! VS. AI: Paper!\n";
                    cout<<"You win!\n";
                    cout<<endl;
                }
                else
                {
                    cout<<"You: Scissors! VS. AI: Scissors!\n";
                    cout<<"Draw!\n";
                    cout<<endl;
                }
                break;
            }
            if(a>0&&a<4)          //若數字符合範圍，則算一次
            {
                i++;
            }
        }
    }
    while(i<t);                   //當正確輸入5次後跳出

    return 0;
}
