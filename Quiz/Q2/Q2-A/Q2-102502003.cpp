#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std;

int main()
{
    int battle=0;  //玩家出拳
    int Count=0;  //猜拳次數

    cout<<"Rock-paper-scissors"<<endl<<"AI: Let's battle!"<<endl;


    do
    {
        srand(time(0));  //作為隨機數的seed
        int AI=rand()%4;  //AI出拳

        do  //規定範圍
        {
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>battle;
            if(battle<0||battle>3)
                cout<<"Out of range!"<<endl;
        }
        while(battle<0||battle>3);

        cout<<"Rock!Paper!Scissors!"<<endl;

        switch(battle)  //根據玩家出拳顯示不同結果
        {
        case 1:
            if(battle==AI)  //雙方相同
                cout<<"You: Rock! VS. AI: Rock!"<<endl<<"Draw!"<<endl;
            if(AI==2)
                cout<<"You: Rock! VS. AI: Paper!"<<endl<<"You lose!"<<endl;
            if(AI==3)
            {
                cout<<"You: Rock! VS. AI: Scissors!"<<endl<<"You win!"<<endl;
            }
            cout<<endl;
            break;

        case 2:
            if(battle==AI)
                cout<<"You: Paper! VS. AI: Paper!"<<endl<<"Draw!"<<endl;
            if(AI==1)
                cout<<"You: Paper! VS. AI: Rock!"<<endl<<"You win!"<<endl;
            if(AI==3)
            {
                cout<<"You: Paper! VS. AI: Scissors!"<<endl<<"You lose!"<<endl;
            }
            cout<<endl;
            break;

        default:
            if(battle==AI)
                cout<<"You: Scissors! VS. AI: Scissors!"<<endl<<"Draw!"<<endl;
            if(AI==1)
                cout<<"You: Scissors! VS. AI: Rock!"<<endl<<"You lose!"<<endl;
            if(AI==2)
            {
                cout<<"You: Scissors! VS. AI: Paper!"<<endl<<"You win!"<<endl;
            }
            cout<<endl;
            break;
        }

        Count++;  //次數累加

    }
    while(Count!=5);  //共猜五次


    return 0;

}
