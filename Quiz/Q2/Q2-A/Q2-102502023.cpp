#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

string MyChoice(int); // function prototype
void Statue(int, int); // function prototype
void Play(int); // function prototype

int rnumber; // initialize integer rnumber

int main()
{
    int choice; // initialize integer choice

    cout<<"Rock-paper-scissors"<<endl<<"AI: Let's battle!\n";

    for(int i=1;i<=5;i++) // for loop to execute inner thing five times
    {

    srand(time(0)); // rnumber seed
    do
    {
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>choice;
        if(choice!=1 && choice !=2 && choice!=3)
            cout<<"Out of range!\n";
    }
    while(choice!=1 && choice !=2 && choice!=3); // do...while loop to check whether choice is on demand

    Play(choice);
    cout<<endl;
    }




    return 0;
}

string MyChoice( int choice )
{
    string s;
    if(choice==1)
        s ="Rock";
    if(choice==2)
        s ="Paper";
    if(choice==3)
        s = "Scissors";

    return s;
}

void Statue(int choice, int rnumber)
{
    if(choice==rnumber)
        cout<<"Draw!";
    if(choice==1&&rnumber==2)
        cout<<"You lose!";
    if(choice==1&&rnumber==3)
        cout<<"You win!";
    if(choice==2&&rnumber==3)
        cout<<"You lose!";
    if(choice==2&&rnumber==1)
        cout<<"You win!";
    if(choice==3&&rnumber==1)
        cout<<"You lose!";
    if(choice==3&&rnumber==2)
        cout<<"You win!";
}

void Play( int choice)
{

    rnumber= (1+ rand() % 3); // to make a number between 1 to 3

    cout<<"Rock! Paper! Scissors!\n";
    cout << "You: " << MyChoice(choice) <<  "! VS. AI: " << MyChoice(rnumber) << "!" << endl;
    Statue(choice,rnumber);
}
