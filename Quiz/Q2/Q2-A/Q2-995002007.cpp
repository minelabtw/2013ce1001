#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int input=0;
    srand(time(0));



    cout << "Rock-papper-scissors" << endl;         //輸入字串
    cout << "AI: Let's battle!" << endl;

    for(int i=1; i<=5; i++)
    {

        int randnum=1+rand()%3;   // 電腦的AI


        do
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: "; //如果不是輸入1,2,3就迴圈
            cin >> input;
            if(input!=1 && input!=2 && input!=3)
            {
                cout << "Out of range!" << endl;
            }



        }
        while (input!=1 && input!=2 && input!=3);

        switch(input)
        {
        case 1:    //you are Rock

            cout << "Rock! Paper! Scissor!" << endl;
            if(randnum==1)
            {
                cout << "You: Rock! VS. Rock!" << endl;
                cout << "Draw!" << endl;
            }

            if(randnum==2)
            {
                cout << "You: Rock! VS. Paper!" << endl;
                cout << "You lose!" << endl;
            }

            if(randnum==3)
            {
                cout << "You: Rock! VS. Scissors!" << endl;
                cout << "You win!" << endl;
            }
            break;

        case 2:  // you are Paper

            cout << "Rock! Paper! Scissor!" << endl;
            if(randnum==1)
            {
                cout << "You: Paper! VS. Rock!" << endl;
                cout << "You win!" << endl;
            }

            if(randnum==2)
            {
                cout << "You: Paper! VS. Paper!" << endl;
                cout << "Draw!" << endl;
            }

            if(randnum==3)
            {
                cout << "You: Paper! VS. Scissors!" << endl;
                cout << "You lose!" << endl;
            }
            break;

        case 3: // you are Scissors

            cout << "Rock! Paper! Scissor!" << endl;
            if(randnum==1)
            {
                cout << "You: Scissors! VS. Rock!" << endl;
                cout << "You lose!" << endl;
            }

            if(randnum==2)
            {
                cout << "You: Scissors! VS. Paper!" << endl;
                cout << "You win!" << endl;
            }

            if(randnum==3)
            {
                cout << "You: Scissors! VS. Scissors!" << endl;
                cout << "Draw!" << endl;
            }
            break;

        }
        cout << endl;
    }
    cout << endl;
    return 0;
}
