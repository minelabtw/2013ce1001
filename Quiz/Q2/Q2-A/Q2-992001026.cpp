#include <iostream>
#include <ctime>
#include <string>
#include <cstdlib>
using namespace std ;


int main ()
{
    // A 陣列 和 Y 陣列 儲存每次電腦與玩家的猜拳
    int A[5] ,Y[5] ;
    // 字串陣列 儲存三個拳法
    string G[3] = {"Rock!", "Paper!", "Scissors!"} ;
    int a  , b;
    //輸出
    cout <<"Rock-paper-scissors" << endl ;
    cout <<"AI: Let's battle!" << endl ;

    // 開始五次的猜拳
    for ( int i = 0 ; i < 5 ; ++i )
    {
        srand (static_cast <unsigned>(time(NULL)));
        //變數設定
        b = rand()%3 +1;

        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: " ;
        cin >> a ;
        while ( a != 1 && a!= 2 && a!= 3 )
        {

            cout << "Out of range!" <<endl ;
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: " ;
            cin >> a ;

        }
        cout << "Rock! Paper! Scissors!" << endl ;
        A[i] = b-1 ;
        Y[i] = a-1 ;
        cout << "You: " << G[Y[i]] << " VS. AI: " << G[A[i]] << endl ;

        //判斷輸贏
        if ( a ==  b ) cout <<"Draw!" << endl;
        else if( a == b - 1) cout << "You lose!" << endl;
        else if (b == a - 1 )cout << "You win!"<< endl ;
        else if ( a > b ) cout << "You win!" << endl;
        else if ( b > a ) cout << "You lose!" << endl;

        cout << endl ;
    }

    cout << endl ;

    // 顯示過程
    for (int i = 0 ; i< 5 ; ++i )
    {
        cout << i+1 << " - " << "You: " << G[Y[i]] << " VS. AI: " << G[A[i] ]<< endl ;

    }

    return 0 ;
}
