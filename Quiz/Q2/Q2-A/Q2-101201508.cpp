#include<iostream>
#include<ctime>
#include<cstdlib>
using std::cout;
using std::cin;
using std::endl;
void P(int thing)
{
    if (thing==1)
        cout << "Rock!" ;
    else if (thing==2)
        cout << "Paper!" ;
    else if (thing==3)
        cout << "Scissors!" ;
}
int main()
{
    int user[5] ;
    int computer[5] ;
    srand(time(0)) ;
    cout << "Rock-paper-scissors" << endl ;
    cout << "AI: Let's battle!" << endl ;
    for (int i=0;i<5;++i)
    {
        do
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: " ;
            cin >> user[i] ;
            if (user[i]!=1 && user[i]!=2 && user[i]!=3)
            {
                cout << "Out of range!" << endl ;
            }
        }
        while(user[i]!=1 && user[i]!=2 && user[i]!=3);
        cout << "Rock! Paper! Scissors!" <<endl ;
        computer[i]=rand()%3+1 ;
        cout << "You: ";
        P(user[i]) ;
        cout << " VS. AI: ";
        P(computer[i]) ;
        cout << endl ;
        if (user[i]==computer[i]-1 || (user[i]==3 && computer[i]==1))
            cout << "You lose!" ;
        else if (computer[i]==user[i])
            cout << "Draw!" ;
        else if (user[i]==computer[i]+1 || (user[i]==1 && computer[i]==3))
            cout << "You win!" ;
        cout << endl << endl ;
    }
    for (int i=0;i<5;++i)
    {
        cout << i+1 << " - You: " ;
        P(user[i]);
        cout << " VS. AI: " ;
        P(computer[i]) ;
        cout << endl ;
    }
}
