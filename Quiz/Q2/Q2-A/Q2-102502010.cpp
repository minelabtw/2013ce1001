#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

string hand(int a) //出的拳
{
    if(a==1)
        return "Rock!";
    else if(a==2)
        return "Paper!";
    else
        return "Scissors!";
}

int main()
{
    int you[5],AI[5],i; //我出的拳,AI出的拳,計算次數
    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;
    for(i=0; i<5; i++)  //迴圈跑五次
    {
        srand(time(0));
        AI[i]=(rand()%3+1);  //亂數取電腦出拳
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>you[i];
        while(you[i]<1 || you[i]>3)  //判斷輸入是否正確
        {
            cout<<"Out of range!"<<endl;
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>you[i];
        }
        cout<<"Rock! Paper! Scissors!"<<endl;
        cout<<"You: "<<hand(you[i])<<" VS. AI: "<<hand(AI[i])<<endl;
        if(you[i]==1 && AI[i]==2 || you[i]==2 && AI[i]==3 || you[i]==3 && AI[i]==1)  //判斷誰輸誰贏
            cout<<"You lose!"<<endl;
        else if(you[i]==2 && AI[i]==1 || you[i]==3 && AI[i]==2 || you[i]==1 && AI[i]==3)
            cout<<"You win!"<<endl;
        else
            cout<<"Draw!"<<endl;
        cout<<endl;
    }
    for(i=0; i<5; i++) //輸出猜拳紀錄
    {
        cout<<i+1<<" - You: "<<hand(you[i])<<" VS. AI: "<<hand(AI[i])<<endl;
    }
    return 0;
}
