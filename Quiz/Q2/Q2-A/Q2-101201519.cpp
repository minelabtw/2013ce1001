#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    const int ArraySize=5;//陣列大小
    int randAI[ArraySize],PlayerInput[ArraySize];
    srand(time(NULL));//使種子隨著時間變化
    cout << "Rock-paper-scissors\n";
    cout << "AI: Let's battle!\n";
    for(int i=1; i<=5; i++)//猜拳猜五次
    {
        int AI=rand()%3+1;
        int player=0;
        randAI[i-1]=AI;//將AI值存至陣列
        while(player<1 || player>3)//判斷輸入的值的範圍
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> player;
            PlayerInput[i-1]=player;//將player值存至陣列
            if (player<1 || player>3)
                cout << "Out of range!\n";
        }
        cout << "Rock! Paper! Scissors!\n";
        switch(player)//判斷與電腦的輸贏
        {
        case 1:
        {
            if(AI==1)
                cout << "You: Rock! VS. AI: Rock!\n" << "Draw!\n";
            else if (AI==2)
                cout << "You: Rock! VS. AI: Paper!\n" << "You lose!\n";
            else
                cout << "You: Rock! VS. AI: Scissors!\n" << "You win!\n";
            cout << "\n";
            break;
        }
        case 2:
        {
            if(AI==1)
                cout << "You: Paper! VS. AI: Rock!\n" << "You win!\n";
            else if (AI==2)
                cout << "You: Paper! VS. AI: Paper!\n" << "Draw!\n";
            else
                cout << "You: Paper! VS. AI: Scissors!\n" << "You lose!\n";
            cout << "\n";
            break;
        }
        case 3:
        {
            if(AI==1)
                cout << "You: Scissors! VS. AI: Rock!\n" << "You lose!\n";
            else if (AI==2)
                cout << "You: Scissors! VS. AI: Paper!\n" << "You win!\n";
            else
                cout << "You: Scissors! VS. AI: Scissors!\n" << "Draw!\n";
            cout << "\n";
            break;
        }
        default :
            break;
        }
    }
    for(int j=1; j<=ArraySize; j++) //紀錄與電腦的互動
    {
        cout << j << " - " << "You: ";
        if(PlayerInput[j-1]==1)
            cout << "Rock!";
        else if(PlayerInput[j-1]==2)
            cout << "Paper!";
        else if(PlayerInput[j-1]==3)
            cout << "Scissors!";
        cout << " VS. AI: ";
        if(randAI[j-1]==1)
            cout << "Rock!";
        else if(randAI[j-1]==2)
            cout << "Paper!";
        else if(randAI[j-1]==3)
            cout << "Scissors!";
        cout << "\n";
    }
    return 0;
}
