#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    const int Size=5;//可以猜五次
    int a[Size]= {};//存放輸入
    int choice=-1;//出拳選擇
    int Count=0;//猜的次數
    int AI[Size]= {};//AI存放

    int AIchoice;//電腦出拳

    cout<<"Rock-paper-scissors\nAI: Let's battle!"<<endl;
    do
    {
        srand(time(0));
        AIchoice=(1+rand()%3);//AI隨機取1-3的變數
        AI[Count]=AIchoice;//將AI變數存入array
        while(1)//choice loop
        {
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>choice;
            if(choice!=1&&choice!=2&&choice!=3)
                cout<<"Out of range!"<<endl;
            else
                break;
        }
        a[Count]=choice;//將玩家choice 存入array
        Count++;//次數加
        cout<<"Rock! Paper! Scissors!"<<endl;

        if(choice==1&&AIchoice==1)//玩家出石頭
            cout<<"You: Rock! VS. AI: Rock! "<<endl;
        else if(choice==1&&AIchoice==2)
            cout<<"You: Rock! VS. AI: Paper! "<<endl;
        else if(choice==1&&AIchoice==3)
            cout<<"You: Rock! VS. AI: Scissors! "<<endl;

        if(choice==2&&AIchoice==1)//玩家出布
            cout<<"You: Paper! VS. AI: Rock! "<<endl;
        else if(choice==2&&AIchoice==2)
            cout<<"You: Paper! VS. AI: Paper! "<<endl;
        else if(choice==2&&AIchoice==3)
            cout<<"You: Paper! VS. AI: Scissors! "<<endl;

        if(choice==3&&AIchoice==1)//玩家出剪刀
            cout<<"You: Scissors! VS. AI: Rock! "<<endl;
        else if(choice==3&&AIchoice==2)
            cout<<"You: Scissors! VS. AI: Paper! "<<endl;
        else if(choice==3&&AIchoice==3)
            cout<<"You: Scissors! VS. AI: Scissors! "<<endl;

        if(choice==AIchoice)//判斷輸贏
            cout<<"Draw!"<<endl;
        else if(choice==1&&AIchoice==2||choice==2&&AIchoice==3||choice==3&&AIchoice==1)
            cout<<"You lost!"<<endl;
        else if(choice==2&&AIchoice==1||choice==3&&AIchoice==2||choice==1&&AIchoice==3)
            cout<<"You win!"<<endl;

        cout<<endl;//跳行
    }
    while(Count<5);//玩完五次
    for(int i=0; i<Count; i++)//輸出玩家猜拳史
    {
        cout<<i+1<<" - ";
        if(a[i]==1&&AI[i]==1)//玩家出石頭
            cout<<"You: Rock! VS. AI: Rock! "<<endl;
        else if(a[i]==1&&AI[i]==2)
            cout<<"You: Rock! VS. AI: Paper! "<<endl;
        else if(a[i]==1&&AI[i]==3)
            cout<<"You: Rock! VS. AI: Scissors! "<<endl;

        if(a[i]==2&&AI[i]==1)//玩家出布
            cout<<"You: Paper! VS. AI: Rock! "<<endl;
        else if(a[i]==2&&AI[i]==2)
            cout<<"You: Paper! VS. AI: Paper! "<<endl;
        else if(a[i]==2&&AI[i]==3)
            cout<<"You: Paper! VS. AI: Scissors! "<<endl;

        if(a[i]==3&&AI[i]==1)//玩家出剪刀
            cout<<"You: Scissors! VS. AI: Rock! "<<endl;
        else if(a[i]==3&&AI[i]==2)
            cout<<"You: Scissors! VS. AI: Paper! "<<endl;
        else if(a[i]==3&&AI[i]==3)
            cout<<"You: Scissors! VS. AI: Scissors! "<<endl;
    }
    return 0;
}
