#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
using namespace std;
int main()
{
    srand(time(0));//調整randNum之值
    int randNum = (rand() % 3)+1;//randNum是1~3中之一數
    int num;//定義會用到的數
    int show;
    int counter=0;
    cout<<"Rock-paper-scissors\n";//輸出
    cout<<"AI: Let's battle!\n";

    while (counter!=5)//未滿五次，持續進行
    {
        cout<<"Throw 1)Rock 2)Paper 3)scissors?: ";//輸出
        cin>>num;//輸入
        while (num<1||num>3)//超出範圍，重新輸入
        {
            cout<<"Out of range!\n";
            cout<<"Throw 1)Rock 2)Paper 3)scissors?: ";
            cin>>num;
        }
        counter++;//進一次while，counter+1
        switch (num)//三種case
        {
        case 1://輸入1
            cout<<"You Rock! VS. AI: ";//輸出
            if (randNum==1)
            {
                cout<<"Rock!"<<endl;
            }
            if (randNum==2)
            {
                cout<<"paper!"<<endl;
            }
            if (randNum==3)
            {
                cout<<"scissors!"<<endl;
            }
            if (num==1&&randNum==3)//石頭贏剪刀
            {
                cout<<"You Win!\n";
            }
            else if (num>randNum)//其餘
            {
                cout<<"You Win!\n";
            }
            else if (num<randNum)
            {
                cout<<"You Lose!\n";
            }
            if (num==randNum)//相同時，平手
            {
                cout<<"Draw!\n";
            }
            break;
        case 2://輸入2
            cout<<"You Paper! VS. AI: ";
            if (randNum==1)
            {
                cout<<"Rock!"<<endl;
            }
            if (randNum==2)
            {
                cout<<"paper!"<<endl;
            }
            if (randNum==3)
            {
                cout<<"scissors!"<<endl;
            }
            if (num>randNum)//其餘
            {
                cout<<"You Win!\n";
            }
            else if (num<randNum)
            {
                cout<<"You Lose!\n";
            }
            if (num==randNum)//相同時，平手
            {
                cout<<"Draw!\n";
            }
            break;
        case 3://輸入3
            cout<<"You Scissors! VS. AI: ";
            if (randNum==1)
            {
                cout<<"Rock!"<<endl;
            }
            if (randNum==2)
            {
                cout<<"paper!"<<endl;
            }
            if (randNum==3)
            {
                cout<<"scissors!"<<endl;
            }
            if (num==1&&randNum==3)//石頭贏剪刀
            {
                cout<<"You Win!\n";
            }
            if (num==3&&randNum==1)//石頭贏剪刀
            {
                cout<<"You Lose!\n";
            }
            else if (num>randNum)//其餘
            {
                cout<<"You Win!\n";
            }
            else if (num<randNum)
            {
                cout<<"You Lose!\n";
            }
            if (num==randNum)//相同時，平手
            {
                cout<<"Draw!\n";
            }
            break;
        }
        if (counter==5)
            break;
    }
    return 0;
}

