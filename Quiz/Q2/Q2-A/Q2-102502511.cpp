#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

void w(int [],int ); //設一個函數 用來啟動最下面的部分

int main()
{
    int a;
    int srand(time(0));
    int b = rand()%3+1;
    int j = 0;
    int d[j]; //用陣列來記錄結果

    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;

    for (int c = 0 ; c < 5 ; c++) //c從0到4 史內容執行4次
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin >> a;
        while(a!=1 && a!=2 && a!=3)
        {
            cout << "Out of range!" << endl;
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> a;
        }

        cout << "Rock! Paper! Scissors!" << endl;

        if(a == 1)
            cout << "You: Rock! VS. ";
        else if (a == 2)
            cout << "You: Paper! VS. ";
        else if (a == 3)
            cout << "You: Scissors! VS. ";

        if(b == 1)
            cout << "AI: Rock!";
        else if (b == 2)
            cout << "AI: Paper!";
        else if (b == 3)
            cout << "AI: Scissors!";

        if(a == b)
            cout << endl << "Draw!";
        else if((a==1 && b==2) || (a==2 && b==3)||(a==3 && b==1))
            cout << endl << "You lose!";
        else
            cout << endl << "You win!";

        if(a==1 && b==1) //用條件來記錄下陣列 使後來可以用出不同的結果
            d[j] = 0;
        else if (a==1 && b==2)
            d[j] = 1;
        else if(a==1 && b==3)
            d[j] = 2;
        else if (a==2 && b==2)
            d[j] = 3;
        else if(a==2 && b==3)
            d[j] = 4;
        else if (a==3 && b==3)
            d[j] = 5;

        j++;

        cout << endl;
    }

    w(d,j); //叫出函式的內容

}

void w(int x[],int y)
{
    for(int s = 0 ; s < 5 ; s++)
    {
        if(x[s]==0)
            cout << s+1 << " - You: Rock! VS. AI: Rock!" << endl;
        else if(x[s]==1)
            cout << s+1 << " - You: Rock! VS. AI: Paper!" << endl;
        else  if(x[s]==2)
            cout << s+1 << " - You: Rock! VS. AI: Scissors!" << endl;
        else if(x[s]==3)
            cout << s+1 << " - You: Paper! VS. AI: Paper!" << endl;
        else if(x[s]==4)
            cout << s+1 << " - You: Paper! VS. AI: Scissors!" << endl;
        else if(x[s]==5)
            cout << s+1 << " - You: Scissors! VS. AI: Scissors!" << endl;
    }
}



