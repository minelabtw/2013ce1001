#include<iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main()
{
    int AI=0;  //宣告變數
    int i=1,my=0;
    int x[9];
    cout<<"Rock-paper-scissors\n";  //輸出遊戲名稱
    cout<<"AI:Let's battle!\n";
    while(i<=5)  //開始遊戲
    {
        srand(time(0));
        AI=(rand()%3)+1;
        cout<<"Throw 1)Rock,2)Paper,3)Scissors?: ";  //輸出可出甚麼
        cin>>my;  //輸入要出甚麼
        while(my!=1 && my!=2 && my!=3)
        {
            cout<<"Out of range!\n";
            cout<<"Throw 1)Rock,2)Paper,3)Scissors?: ";
            cin>>my;
        }
        cout<<"Rock! Paper! Scissors!\n";
        if(my==1 && AI==2)  //開始猜拳
        {
            cout<<"You: Rock! VS. AI: Paper!\n";
            cout<<"You lose!\n";
            x[i]=1;
        }
        if(my==1 && AI==3)
        {
            cout<<"You: Rock! VS. AI: Scissors!\n";
            cout<<"You win!\n";
            x[i]=2;
        }
        if(my==2 && AI==3)
        {
            cout<<"You: Paper! VS. AI: Scissors!\n";
            cout<<"You lose!\n";
            x[i]=3;
        }
        if(my==2 && AI==1)
        {
            cout<<"You: Paper! VS. AI: Rock!\n";
            cout<<"You win!\n";
            x[i]=4;
        }
        if(my==3 && AI==1)
        {
            cout<<"You: Scissors! VS. AI: Rock!\n";
            cout<<"You lose!\n";
            x[i]=5;
        }
        if(my==3 && AI==2)
        {
            cout<<"You:  Scissors! VS. AI: Paper!\n";
            cout<<"You win!\n";
            x[i]=6;
        }
        if(my==AI)
        {
            if(my==1)
            {
                cout<<"You: Rock! VS. AI: Rock!\n";
                cout<<"Draw!\n";
                x[i]=7;
            }
            if(my==2)
            {
                cout<<"You: Paper! VS. AI: Paper!\n";
                cout<<"Draw!\n";
                x[i]=8;
            }
            if(my==3)
            {
                cout<<"You: Scissors! VS. AI: Scissors!\n";
                cout<<"Draw!\n";
                x[i]=9;
            }
        }
        cout<<endl;
        i++;
    }
    for(int v=1; v<=5; v++)  //輸出過程
    {
        switch(x[v])
        {
        case 1 :
            cout<<v<<" - You: Rock! VS. AI: Paper!\n";
            break;
        case 2 :
            cout<<v<<" - You: Rock! VS. AI: Scissors!\n";
            break;
        case 3 :
            cout<<v<<" - You: Paper! VS. AI: Scissors!\n";
            break;
        case 4 :
            cout<<v<<" - You: Paper! VS. AI: Rock!\n";
            break;
        case 5 :
            cout<<v<<" - You: Scissors! VS. AI: Rock!\n";
            break;
        case 6 :
            cout<<v<<" - You:  Scissors! VS. AI: Paper!\n";
            break;
        case 7 :
            cout<<v<<" - You: Rock! VS. AI: Rock!\n";
            break;
        case 8 :
            cout<<v<<" - You: Paper! VS. AI: Paper!\n";
            break;
        case 9 :
            cout<<v<<" - You: Scissors! VS. AI: Scissors!\n";
            break;
        }
    }
    return 0;
}
