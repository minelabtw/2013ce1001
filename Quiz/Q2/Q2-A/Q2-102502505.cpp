#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int x;//定義輸入數
int a[5];//定義輸入數的陣列
int b[5];//定義亂數陣列
int randnum;//亂數
int i=1;//陣列1函數
int j=1;//陣列2函數

int main()
{
    cout << "Rock-paper-scissors" << endl;//輸出字串
    cout << "AI: Let's battle!" << endl;
    cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    cin >> x;//輸入數字

    while ( x!=1 && x!=2 && x!=3 )//設定迴圈使得輸入數要等於1或2或3
    {
        cout << "Out of range!" << endl;
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin >> x;
    }

    cout << "Rock! Paper! Scissors!" << endl;//輸出字串

    while (i<=5)//設迴圈使得猜拳猜五次
    {
        srand(time(0));//以time(0)做種子
        int randnum = rand()%3+1;//設亂數使亂數在1到3之間

        if ( x==1&&randnum==1 )//設if else，在下列條件下，會出現不同結果
        {
            cout << "You: Rock! VS. AI: Rock!" << endl;
            cout << "Draw!" << endl;
        }
        else if( x==1&&randnum==2 )
        {
            cout << "You: Rock! VS. AI: Paper!" << endl;
            cout << "You lose!" << endl;
        }
        else if (x==1&&randnum==3)
        {
            cout << "You: Rock! VS. AI: Scissors!" << endl;
            cout << "You win!" << endl;
        }
        else if (x==2&&randnum==1)
        {
            cout << "You: Paper! VS. AI: Rock!" << endl;
            cout << "You win!" << endl;
        }
        else if (x==2&&randnum==2)
        {
            cout << "You: Paper! VS. AI: Paper!" << endl;
            cout << "Draw!" << endl;
        }
        else if (x==2&&randnum==3)
        {
            cout << "You: Paper! VS. AI: Scissors!" << endl;
            cout << "You lose!" << endl;
        }
        else if (x==3&&randnum==1)
        {
            cout << "You: Scissors! VS. AI: Rock!" << endl;
            cout << "You lose!" << endl;
        }
        else if (x==3&&randnum==2)
        {
            cout << "You: Scissors! VS. AI: Paper!" << endl;
            cout << "You win!" << endl;
        }
        else if (x==3&&randnum==3)
        {
            cout << "You: Scissors! VS. AI: Scissors!" << endl;
            cout << "Draw!" << endl;
        }
        cout << endl;//空行

        a[i]=x;//紀錄陣列1
        b[j]=randnum;//紀錄陣列2
        i++;//i+1
        j++;//j+1

        if(i==6)
            break;

        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";//輸出字串
        cin >> x;

        while ( x!=1 && x!=2 && x!=3 )
        {
            cout << "Out of range!" << endl;
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> x;
        }

        cout << "Rock! paper! scissors!" << endl;
    }

    i=1;//將i和j變為初始值
    j=1;

    while (i<=5)//設條件使得這迴圈跑5次
    {
        cout << i << " - ";

        if ( a[i]==1&&b[j]==1 )//用if else，來輸出符合的字串
        {
            cout << "You: Rock! VS. AI: Rock!" << endl;
        }
        else if( a[i]==1&&b[j]==2 )
        {
            cout << "You: Rock! VS. AI: Paper!" << endl;
        }
        else if (a[i]==1&&b[j]==3)
        {
            cout << "You: Rock! VS. AI: Scissors!" << endl;
        }
        else if (a[i]==2&&b[j]==1)
        {
            cout << "You: Paper! VS. AI: Rock!" << endl;
        }
        else if (a[i]==2&&b[j]==2)
        {
            cout << "You: Paper! VS. AI: Paper!" << endl;
        }
        else if (a[i]==2&&b[j]==3)
        {
            cout << "You: Paper! VS. AI: Scissors!" << endl;
        }
        else if (a[i]==3&&b[j]==1)
        {
            cout << "You: Scissors! VS. AI: Rock!" << endl;
        }
        else if (a[i]==3&&b[j]==2)
        {
            cout << "You: Scissors! VS. AI: Paper!" << endl;
        }
        else if (a[i]==3&&b[j]==3)
        {
            cout << "You: Scissors! VS. AI: Scissors!" << endl;
        }

        i++;//i+1
        j++;//j+1
    }

    return 0;//返回初始值

}




