//猜拳遊戲
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));  //建立亂數表
    int randNum=rand()%3+1;  //1~3

    int choose=0;

    cout << "Rock-paper-scissors\n";
    cout << "AI: Let's battle!\n";

    for(int x=1; x<=5; x++)  //玩五次
    {
        do
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> choose;

            if(choose>3)
                cout << "Out of range!\n";
            else if(choose<1)
                cout << "Out of range!\n";
        }
        while(choose>3||choose<1);

        if(choose==1) //當出石頭時
        {
            cout << "Rock! Paper! Scissors!\n";
            cout << "You: Rock! VS. AI: ";
            if(randNum==1)
                cout << "Rock";
            else if(randNum==2)
                cout << "Paper";
            else if(randNum==3)
                cout << "Scissors";
            cout << "!\n";

            if(randNum==1)
            {
                cout << "Draw!\n\n";
            }
            else if(randNum==2)
            {
                cout << "You lose!\n\n";
            }
            else if(randNum==3)
            {
                cout << "You win\n\n";
            }
        }

        else if(choose==2)  //當出布時
        {
            cout << "Rock! Paper! Scissors!\n";
            cout << "You: Paper! VS. AI: ";
            if(randNum==1)
                cout << "Rock";
            else if(randNum==2)
                cout << "Paper";
            else if(randNum==3)
                cout << "Scissors";
            cout << "!\n";

            if(randNum==1)
            {
                cout << "You win!\n\n";
            }
            else if(randNum==2)
            {
                cout << "Draw!\n\n";
            }
            else if(randNum==3)
            {
                cout << "You lose!\n\n";
            }
        }

        else if(choose==3)  //當出剪刀時
        {
            cout << "Rock! Paper! Scissors!\n";
            cout << "You: Scissors! VS. AI: ";
            if(randNum==1)
                cout << "Rock";
            else if(randNum==2)
                cout << "Paper";
            else if(randNum==3)
                cout << "Scissors";
            cout << "!\n";

            if(randNum==1)
            {
                cout << "You lose!\n\n";
            }
            else if(randNum==2)
            {
                cout << "You win!\n\n";
            }
            else if(randNum==3)
            {
                cout << "Draw\n\n";
            }
        }
    }

    return 0;
}
