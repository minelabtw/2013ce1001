#include<iostream>
#include<cstdlib>
#include<ctime>
int main()
{
   int user,ai,log[5][2];  //declaration
   char w[3][9]={"Rock","Paper","Scissors"};

   std::cout<<"Rock-paper-scissors\nAI: Let's battle!";  //start
   srand(time(NULL));

   for(int i=0;i!=5;i++)
   {
      std::cout<<"\nThrow 1)Rock, 2)Paper 3)Scissors?: ";   //input
      std::cin>>user;
      if(user<1||user>3)
      {
         std::cout<<"Out of Range!\n";
         i--;
         continue;
      }

      user--;
      ai=rand()%3;   //ai

      log[i][0]=user;   //save log
      log[i][1]=ai;

      std::cout<<"Rock! Paper! Scissors!\n";    //output
      std::cout<<"You: "<<w[user]<<"! VS. AI: "<<w[ai]<<"!\n";
      if(user==(ai+1)%3)
         std::cout<<"You win!\n";
      else if(user==(ai+2)%3)
         std::cout<<"You lose!\n";
      else
         std::cout<<"Draw!\n";
   }

   for(int i=0;i!=5;i++)   //show log
      std::cout<<'\n'<<i+1<<" - You: "<<w[log[i][0]]<<"! VS. AI: "<<w[log[i][1]]<<'!';

   return 0;   //exit
}
