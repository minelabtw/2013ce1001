#include <iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;

int main()
{
    int myGuess=0,totalGuess=5,randomNum=0,guessTime=0;
    string guessContain[5];
    cout << "Rocl-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;

//總共要猜五次
    for(int i=0; i<totalGuess; i++)
    {
        //每次出拳前詢問
        do
        {

            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: " ;
            cin>>myGuess;
            if(myGuess>=4 || myGuess<=0)
            {
                cout << "Out of range!" << endl;
            }
        }
        while(myGuess>=4 || myGuess<=0);
        cout << "Rock! Paper! Scissors! "<<endl ;
        srand(time(0));
        randomNum=(rand() % 3 )+1;

        //cout << randomNum << endl;
        //判斷輸贏 九種判斷
        if((myGuess==1 && randomNum==1))
        {
            cout << "You: Rock! VS. AI: Rock!" << endl;
            cout << "Draw!" << endl;
            guessContain[guessTime]="You: Rock! VS. AI: Rock!";
            guessTime++;
        }
        else if((myGuess==2 && randomNum==2))
        {
            cout << "You: Paper! VS. AI: Paper!" << endl;
            cout << "Draw!" << endl;
            guessContain[guessTime]="You: Paper! VS. AI: Paper!";
            guessTime++;
        }
        else if((myGuess==3 && randomNum==3))
        {
            cout << "You: Scissors! VS. AI: Scissors!" << endl;
            cout << "Draw!" << endl;
            guessContain[guessTime]="You: Scissors! VS. AI: Scissors!";
            guessTime++;
        }
        else if((myGuess==1 && randomNum==2))
        {
            cout << "You: Rock! VS. AI: Paper!" << endl;
            cout << "You lose!" << endl;
            guessContain[guessTime]="You: Rock! VS. AI: Paper!";
            guessTime++;
        }
        else if((myGuess==1 && randomNum==3))
        {
            cout << "You: Rock! VS. AI: Scissors!" << endl;
            cout << "You win!" << endl;
            guessContain[guessTime]="You: Rock! VS. AI: Scissors!";
            guessTime++;
        }
        else if((myGuess==2 && randomNum==1))
        {
            cout << "You: Paper! VS. AI: Rock!" << endl;
            cout << "You win!" << endl;
            guessContain[guessTime]="You: Paper! VS. AI: Rock!";
            guessTime++;
        }
        else if((myGuess==2 && randomNum==3))
        {
            cout << "You: Paper! VS. AI: Scissors!" << endl;
            cout << "You lose!" << endl;
            guessContain[guessTime]="You: Paper! VS. AI: Scissors!";
            guessTime++;
        }
        else if((myGuess==3 && randomNum==1))
        {
            cout << "You: Scissors! VS. AI: Rock!" << endl;
            cout << "You lose!" << endl;
            guessContain[guessTime]="You: Scissors! VS. AI: Rock!";
            guessTime++;
        }
        else if((myGuess==3 && randomNum==2))
        {
            cout << "You: Scissors! VS. AI: Paper!" << endl;
            cout << "You win!" << endl;
            guessContain[guessTime]="You: Scissors! VS. AI: Paper!";
            guessTime++;
        }

    }
//印出所有過程
    for(int j=1; j<=totalGuess; j++)
    {
        cout << j<<" - "<< guessContain[j-1]<< endl;
    }


    return 0;
}
