#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{   srand(time(0));
    int choose=0;
    int AIrecord[5]= {0}; //用陣列紀錄變數
    int record[5]= {0};
    int time=0;

    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;
    do
    {
        int AI=(rand()%3)+1; //宣告隨機變數
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors: ";
        cin>>choose;
        switch(choose)
        {
        case 1:
            cout<<"Rock! Paper! Scissors!"<<endl;
            record[time]=1; //紀錄
            AIrecord[time]=AI;
            time++; //計數器每次+1
            if(AI==1)
                cout<<"You: Rock! VS. AI: Rock!"<<endl<<"Draw!"<<endl;
            else if(AI==2)
                cout<<"You: Rock! VS. AI: Paper!"<<endl<<"You lose!"<<endl;
            else if(AI==3)
                cout<<"You: Rock! VS. AI: Scissors!"<<endl<<"You win!"<<endl;
            break;
        case 2:
            cout<<"Rock! Paper! Scissors!"<<endl;
            record[time]=2;
            AIrecord[time]=AI;
            time++;
            if(AI==1)
                cout<<"You: Paper! VS. AI: Rock!"<<endl<<"You win!"<<endl;
            else if(AI==2)
                cout<<"You: Paper! VS. AI: Paper!"<<endl<<"Draw!"<<endl;
            else if(AI==3)
                cout<<"You: Paper! VS. AI: Scissors!"<<endl<<"You lose!"<<endl;
            break;
        case 3:
            cout<<"Rock! Paper! Scissors!"<<endl;
            record[time]=3;
            AIrecord[time]=AI;
            time++;
            if(AI==1)
                cout<<"You: Scissors! VS. AI: Rock!"<<endl<<"You lose!"<<endl;
            else if(AI==2)
                cout<<"You: Scissors! VS. AI: Paper!"<<endl<<"You win!"<<endl;
            else if(AI==3)
                cout<<"You: Scissors! VS. AI: Scissors!"<<endl<<"Draw!"<<endl;
            break;
        default:
            cout<<"Out of range!"<<endl;
            continue; //回到迴圈頂端
        }
        cout<<endl;
    }
    while(time<5); //共猜5次
    for(int i=0; i<5; i++) //印出紀錄
    {
        if(record[i]==1 && AIrecord[i]==1)
            cout<<i+1<<" - "<<"You: Rock! VS. AI: Rock!"<<endl;
        else if(record[i]==1 && AIrecord[i]==2)
            cout<<i+1<<" - "<<"You: Rock! VS. AI: Paper!!"<<endl;
        else if(record[i]==1 && AIrecord[i]==3)
            cout<<i+1<<" - "<<"You: Rock! VS. AI: Scissors!"<<endl;
        else if(record[i]==2 && AIrecord[i]==1)
            cout<<i+1<<" - "<<"You: Paper! VS. AI: Rock!"<<endl;
        else if(record[i]==2 && AIrecord[i]==2)
            cout<<i+1<<" - "<<"You: Paper! VS. AI: Paper!"<<endl;
        else if(record[i]==2 && AIrecord[i]==3)
            cout<<i+1<<" - "<<"You: Paper! VS. AI: Scissors!"<<endl;
        else if(record[i]==3 && AIrecord[i]==1)
            cout<<i+1<<" - "<<"You: Scissors! VS. AI: Rock!"<<endl;
        else if(record[i]==3 && AIrecord[i]==2)
            cout<<i+1<<" - "<<"You: Scissors! VS. AI: Paper!"<<endl;
        else if(record[i]==3 && AIrecord[i]==3)
            cout<<i+1<<" - "<<"You: Scissors! VS. AI: Scissors!"<<endl;
    }
    return 0;
}
