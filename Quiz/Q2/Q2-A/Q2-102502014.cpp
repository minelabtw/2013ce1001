#include<iostream>
#include<cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int a;                          //玩家輸入的數 代表出何種拳
    srand(time(0));                 //用時間作rand的種子
    int b=rand()%3+1;               //b是電腦選的數字 代表它出何種拳 它出拳的範圍在1~3之間
    int c=1;                        //代表玩遊戲的次數
    int array[5]= {};               //宣告一個可以存5個數的陣列

    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;

    while(c<=5)                     //總共玩5次
    {
        c++;
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>a;
        while (a!=1 && a!=2 && a!=3)         //當a不是1或2或3 輸出"Out of range!"
        {
            cout<<"Out of range!"<<endl;
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>a;
        }
        if (a=1)                             //當你出石頭時的情況
        {
            if(b=1)
            {
                cout<<"You: Rock! VS. AI: Rock!"<<endl<<"Draw!"<<endl;
            }
            else if(b=2)
            {
                cout<<"You: Rock! VS. AI: Paper!"<<endl<<"You lose!"<<endl;
            }
            else if(b=3)
            {
                cout<<"You: Rock! VS. AI: Scissors!"<<endl<<"You win!"<<endl;
            }
        }
        else if (a=2)                         //當你出布的情況
        {
             if(b=1)
            {
                cout<<"You: Paper! VS. AI: Rock!"<<endl<<"You win!"<<endl;
            }
            else if(b=2)
            {
                cout<<"You: Paper! VS. AI: Paper!"<<endl<<"Draw!"<<endl;
            }
            else if(b=3)
            {
                cout<<"You: Paper! VS. AI: Scissors!"<<endl<<"You lose!"<<endl;
            }
        }
        else if (a=3)                         //當你出剪刀的情況
        {
            if(b=1)
            {
                cout<<"You: Scissors! VS. AI: Rock!"<<endl<<"You lose!"<<endl;
            }
            else if(b=2)
            {
                cout<<"You: Scissors! VS. AI: Paper!"<<endl<<"You win!"<<endl;
            }
            else if(b=3)
            {
                cout<<"You: Scissors! VS. AI: Scissors!"<<endl<<"Draw!"<<endl;
            }
        }
    }

    return 0;
}
