#include <iostream>

using namespace std;

int main()
{
    int ai=2;
    //int ai=srand()%3+1;
    int thw=0;
    int i=0;
    //int a[i];

    cout << "Rock-paper-scissors\n" << "AI: Let's battle!\n";          //輸出字串

    for(int i=1; i<=5; i++)                  //五個輪迴
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";               //輸出字串，輸入所猜的拳
        cin >> thw;
        while(thw<1 || thw>3)
        {
            cout << "Out of range!\n" << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";        //當輸入不符合範圍，輸出Out of range
            cin >> thw;                                                 //重新輸入
        }
        cout << "Rock! Paper! Scissors!\n";                       //輸出字串

        if(thw==1)                                            //玩家出石頭的三種狀況
        {
            cout << "You: Rock! VS. AI: ";
            if(ai=1)
                cout << "Rock!\n" << "Draw!\n" << endl;         //AI出石頭，平手
            else if(ai=2)
                cout << "Paper!\n" << "You lose!\n" << endl;     //AI出布，玩家輸
            else if(ai=3)
                cout << "Scissors!\n" << "You win!\n" << endl;    //AI出剪刀，玩家贏
        }

        else if(thw==2)                                         //玩家出布
        {
            cout << "You: Paper! VS. AI: ";
            if(ai=1)
                cout << "Rock!\n" << "You win!\n" << endl;      //贏
            else if(ai=2)
                cout << "Paper!\n" << "Draw!\n" << endl;        //平手
            else if(ai=3)
                cout << "Scissors!\n" << "You lose!\n" << endl;  //輸
        }

        else if(thw==3)                                          //玩家出剪刀
        {
            cout << "You: Scissors! VS. AI: ";
            if(ai=1)
                cout << "Rock!\n" << "You lose!\n" << endl;
            else if(ai=2)
                cout << "Paper!\n" << "You win!\n" << endl;
            else if(ai=3)
                cout << "Scissors!\n" << "Draw!\n" << endl;
        }
    }
    //for(int i=1;i<=5;i++)
    //    cout << i << " - " << a[i] << endl;

    return 0;
}
