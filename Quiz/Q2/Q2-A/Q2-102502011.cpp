#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main()
{
    int pk = 0 , x = 0 ;
    int play[5] = {} ;

    cout << "Rock-paper-scissors" << endl ;
    cout << "AI: Let's battle!" << endl ;

    for (int x=1; x<=5; x++) //玩五次猜拳的情況
    {
        srand( time(0) ) ;
        int compk = rand() % 3 + 1 ; // 設定電腦出拳的情況,放入迴圈讓它能重新出拳

        do
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissor?: " ;
            cin >> pk ;
            if(pk != 1 && pk != 2 && pk != 3 ) // 不符合出拳的情況
                cout << "Out of range!" << endl ;
        }
        while (pk != 1 && pk != 2 && pk != 3 ) ;

        cout << "Rock! Paper! Scissor!" << endl ;

        if (pk == 1 && compk == 1 ) //出拳的結果,共九種,因為我弱,只能這樣寫九遍
        {
            cout << "You: Rock! VS. AI: Rock! " << endl ;
            cout << "Draw!" << endl << endl ;
        }


        if (pk == 1 && compk == 2 )
        {
            cout << "You: Rock! VS. AI: Paper! " << endl ;
            cout << "You lose!" << endl << endl ;
        }

        if (pk == 1 && compk == 3 )
        {
            cout << "You: Rock! VS. AI: Scissor! " << endl ;
            cout << "You win!" << endl << endl ;
        }

        if (pk == 2 && compk == 1 )
        {
            cout << "You: Paper! VS. AI: Rock! " << endl ;
            cout << "You win!" << endl << endl ;
        }

        if (pk == 2 && compk == 2 )
        {
            cout << "You: Paper! VS. AI: Paper! " << endl ;
            cout << "Draw!" << endl << endl ;
        }

        if (pk == 2 && compk == 3 )
        {
            cout << "You: Paper! VS. AI: Scissor! " << endl ;
            cout << "You lose!" << endl << endl ;
        }


        if (pk == 3 && compk == 1 )
        {
            cout << "You: Scissor! VS. AI: Rock! " << endl ;
            cout << "You lose!" << endl << endl ;
        }

        if (pk == 3 && compk == 2 )
        {
            cout << "You: Scissor! VS. AI: Paper! " << endl ;
            cout << "You win!" << endl << endl ;
        }

        if (pk == 3 && compk == 3 )
        {
            cout << "You: Scissor! VS. AI: Scissor! " << endl ;
            cout << "Draw!" << endl << endl ;
        }
    }

    for(x=1; x<=5; x++) // 加分題的部分,不會
    {
        cout << x << " - " << endl ;

    }

    return 0;
}
