#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    int f=5, a=0, p=0, I=0;//次數、電腦出拳、玩家出拳
    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;
    while (f--)
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        while (cin >> p)//輸入玩家出拳
        {
            a=(rand()%3)+1;
            if (p<1||p>3)//判斷出拳是否在範圍內
            {
                cout << "Out of range!" << endl;
                f++;
                break;
            }
            else if (a==p)//出拳相同
            {
                cout << "Draw!" << endl;
                break;
            }
            else if (a==1&&p==2||a==2&&p==3||a==3&&p==1)//贏的情況
            {
                cout << "You win!" << endl;
                break;
            }
            else if (a==1&&p==3||a==2&&p==1||a==3&&p==2)//輸的情況
            {
                cout << "You lose!" << endl;
                break;
            }
        }
        I++;
    }
    return 0;
}
