#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
void RPS(int x)             //輸入數字 -> 剪刀石頭布
{
    switch(x)
    {
    case 1:
        cout << "Rock";
        break;
    case 2:
        cout << "Paper";
        break;
    case 3:
        cout << "Scissors";
        break;
    }
}
int main()
{
    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;

    srand(time(NULL));
    int counter=0;
    int record[2][5];                   //紀錄輸入
    while(counter<5)
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin >> record[0][counter];
        switch(record[0][counter])      //判斷值是否為1、2、3
        {
        default:
            cout << "Out of range!\n";
            break;
        case 1:                         //輸的情況：1,2 2,3 3,1 -->輸入的數-rand的數 = -1 or 2
        case 2:                         //贏的情況：2,1 3,2 1,3 -->輸入的數-rand的數 = 1 or -2
        case 3:
            record[1][counter] = rand()%3+1;
            cout << "Rock! Paper! Scissors!\n";
            cout << "You: ";
            RPS(record[0][counter]);
            cout << "! VS. AI: ";
            RPS(record[1][counter]);
            cout << "!\n";
            if(record[0][counter]==record[1][counter])
                cout << "Draw!\n\n";
            else if(record[0][counter]-record[1][counter]==-1 || record[0][counter]-record[1][counter]==2)
                cout << "You lose!\n\n";
            else if(record[0][counter]-record[1][counter]==1 || record[0][counter]-record[1][counter]==-2)
                cout << "You win!\n\n";
            counter++;
            break;
        }
    }
    for(int i=0; i<5; i++)
    {
        cout << i+1 << " - You: ";
        RPS(record[0][i]);
        cout << "! VS. AI: ";
        RPS(record[1][i]);
        cout << "!" ;
        if(i!=4)cout << endl;       //最後一行不換行
    }
    return 0;
}
