#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));                                       //產生亂數
    int number=0;
    int n[5]= {};
    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;

    for ( int i = 0; i < 5; i++ )                         //進行五次猜拳
    {
    cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
    cin>>number;

    while ( number <= 0 or number >3)                     //判斷number為1,2,3
    {
        cout<<"Out of range!"<<endl;
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>number;
    }

    cout<<"Rock! Paper! Scissors!"<<endl;

    int ai=(rand()%3+1);                                  //產生1~3的亂數

    switch (number)
    {

    case 1:                                               //出石頭的情況

        if (ai==1)
        {
            cout<<"You: Rock! VS. AI: Rock!"<<endl;
            cout<<"Draw!"<<endl;
            cout<<endl;
        }
        else if (ai==2)
        {
            cout<<"You: Rock! VS. AI: Paper!"<<endl;
            cout<<"You lose!"<<endl;
            cout<<endl;
        }
        else if (ai==3)
        {
            cout<<"You: Rock! VS. AI: Scissors!"<<endl;
            cout<<"You win!"<<endl;
            cout<<endl;
        }
        break;

    case 2:                                                  //出布的情況

        if (ai==2)
        {
            cout<<"You: Paper! VS. AI: Paper!"<<endl;
            cout<<"Draw!"<<endl;
            cout<<endl;
        }
        else if (ai==3)
        {
            cout<<"You: Paper! VS. AI: Scissors!"<<endl;
            cout<<"You lose!"<<endl;
            cout<<endl;
        }
        else if (ai==1)
        {
            cout<<"You: Paper! VS. AI: Rock!"<<endl;
            cout<<"You win!"<<endl;
            cout<<endl;
        }
        break;

    case 3:                                                   //出剪刀的情況

        if (ai==3)
        {
            cout<<"You: Scissors! VS. AI: Scissors!"<<endl;
            cout<<"Draw!"<<endl;
            cout<<endl;
        }
        else if (ai==1)
        {
            cout<<"You: Scissors! VS. AI: Rock!"<<endl;
            cout<<"You lose!"<<endl;
            cout<<endl;
        }
        else if (ai==2)
        {
            cout<<"You: Scissors! VS. AI: Paper!"<<endl;
            cout<<"You win!"<<endl;
            cout<<endl;
        }
        break;

    }
    }

    for (int i=0; i<5; i++)
        cout<<i+1<<" - "<<n[i]<<endl;

    return 0;
}
