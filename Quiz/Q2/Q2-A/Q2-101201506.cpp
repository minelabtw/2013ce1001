#include<iostream>
#include<cstdlib>  //contain srand() and rand()
#include<ctime>    //contain times()
using namespace std;

int main()
{

    int x=0; //玩家出的
    int S[5]; //陣列

    srand(time(0)); //電腦隨機出的拳
    int randNum=rand()%3+1;

    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;
    do //檢查是否輸入正確
    {
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>x;

        if(x<1 || x>3)
            cout<<"Out of range!"<<endl;
    }
    while(x<1 || x>3);

    while(randNum==x) //平手時
    {
        if(x==1)
        {
            cout<<"You: Rock! VS. AI: Rock!"<<endl<<"Draw!"<<endl;
            break;
        }
        if(x==2)
        {
            cout<<"You: Paper! VS. AI: Paper!"<<endl<<"Draw!"<<endl;
            break;
        }

        if(x==3)
        {
            cout<<"You: Scissors! VS. AI: Scissors!"<<endl<<"Draw!"<<endl;
            break;
        }

    }

    while(randNum!=x) //沒有平手
    {
        if(1==randNum)//電腦出石頭時
        {
            if(x==2)
            {
                cout<<"You: Paper! VS. AI: Rock!"<<endl<<"You win!"<<endl;
                break;
            }
            if(x==3)
            {
                cout<<"You: Scissors! VS. AI: Rock!"<<endl<<"You lose!"<<endl;
                break;
            }
        }

        if(2==randNum)//電腦出布時
        {
            if(x==1)
            {
                cout<<"You: Rock! VS. AI: Paper!"<<endl<<"You lose!"<<endl;
                break;
            }
            if(x==3)
            {
                cout<<"You: Scissors! VS. AI: Paper!"<<endl<<"You win!"<<endl;
                break;
            }
        }

        if(3==randNum)//電腦出剪刀時
        {
            if(x==1)
            {
                cout<<"You: Rock! VS. AI: Scissors!"<<endl<<"You lose!"<<endl;
                break;
            }
            if(x==2)
            {
                cout<<"You: Paper! VS. AI: Scissors!"<<endl<<"You win!"<<endl;
                break;
            }
        }
    }





    return 0;
}
