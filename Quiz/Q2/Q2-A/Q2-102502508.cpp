#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std ;
int main()//本次考試目的在於運用無限迴圈和讓電腦產生亂數來進行猜拳的遊戲
{
    int c=0 ;
    int p=0 ;
    srand(time(0)) ;
    c=rand()%3+1 ;
    int i=0 ;

    cout<<"Rock-paper-scissors"<<endl ;
    cout<<"AI: Let's battle!"<<endl ;

    do   //利用後測試迴圈使迴圈至少能跑一次
    {
        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: " ;
        cin>>p ;

        if((p<=0 || p>3))
        {
            cout<<"Out of range!"<<endl ;

        }


        if(p==1 && c==3 )
        {
            cout<<"You: Rock! VS. AI: Scissors!"<<endl ;    //因為技術不夠成熟,觀念不夠紮實故運用多組判斷來顯示使用者和電腦的輸贏
            cout<<"You win!"<<endl ;
            i=i+1 ;

        }
        else if(p==1 && c==2)
        {
            cout<<"You: Rock! VS. AI: Paper!"<<endl ;
            cout<<"You lose!"<<endl ;
            i=i+1 ;


        }
        else if(p==2 && c==3)
        {
            cout<<"You: Paper! VS. AI: Scissors!"<<endl ;
            cout<<"You lose!"<<endl ;
            i=i+1 ;


        }
        else if(p==2 && c==1)
        {
            cout<<"You: Paper! VS. AI: Rock"<<endl ;
            cout<<"You win!"<<endl ;
            i=i+1 ;


        }
        else if(p==3 && c==2)
        {
            cout<<"You: Scissors! VS. AI: Paper"<<endl ;
            cout<<"You win!"<<endl ;
            i=i+1 ;


        }
        else if(p==3 && c==1)
        {
            cout<<"You: Scissors! VS. AI: Rock"<<endl ;
            cout<<"You lose!"<<endl ;
            i=i+1 ;

        }
        else if(p==c)
        {

            if(p==1 && c==1 )
            {
                cout<<"You: Rock! VS. AI: Rock!"<<endl ;
                i=i+1 ;

            }
            else if(p==2 && c==2 )
            {
                cout<<"You: Paper! VS. AI: Paper!"<<endl ;
                i=i+1 ;

            }
            else if(p==3 && c==3 )
            {
                cout<<"You: Scissors! VS. AI: Scissors!"<<endl ;
                i=i+1 ;

            }
            cout<<"Draw!"<<endl ;
        }


    }
    while(i<5) ; //進行共5次的猜拳遊戲
    return 0 ;
}
