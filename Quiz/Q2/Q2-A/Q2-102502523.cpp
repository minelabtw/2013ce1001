#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    const int lengh=10;
    int arr[lengh]= {}; //宣告陣列
    int thr;
    int a=0;
    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;
    for(int c=5; c>0; c--) //進入迴圈
    {

        cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin>>thr;//輸入要出的拳
        while(thr<1||thr>3)
        {
            cout<<"Out of range!"<<endl;
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>thr;//判斷範圍
        }
        arr[a]=thr;
        a=a+1;
        srand(time(0));
        int ai=rand()%3+1;//電腦出拳
        arr[a]=ai;
        a=a+1;
        cout<<"Rock! Paper! Scissors!"<<endl;
        cout<<"You: ";
        if(thr==1)
        {
            cout<<"Rock! ";   //判斷出拳
        }
        else if(thr==2)
        {
            cout<<"Paper! ";
        }
        else
        {
            cout<<"Scissors! ";
        }
        cout<<"VS. AI: ";
        if(ai==1)
        {
            cout<<"Rock! "<<endl;
        }
        else if(ai==2)
        {
            cout<<"Paper! "<<endl;
        }
        else
        {
            cout<<"Scissors! "<<endl;
        }
        if(ai==thr)
        {
            cout<<"Draw!"<<endl<<endl;   //判斷勝負
        }
        else if(thr==3&&ai==1)
        {
            cout<<"You lose!"<<endl<<endl;
        }
        else if(thr>ai)
        {
            cout<<"You win!"<<endl<<endl;
        }
        else if(thr==1&&ai==3)
        {
            cout<<"You win!"<<endl<<endl;
        }
        else
        {
            cout<<"You lose!"<<endl<<endl;
        }
    }
    a=0;
    for(int b=1; b<6; b++) //輸出過程
    {

        cout<<b<<" - You: ";
        if(arr[a]==1)
        {
            cout<<"Rock! ";
        }
        else if(arr[a]==2)
        {
            cout<<"Paper! ";
        }
        else if(arr[a]==3)
        {
            cout<<"Scissors! ";
        }
        a=a+1;
        cout<<"VS. AI: ";
        if(arr[a]==1)
        {
            cout<<"Rock! "<<endl;
        }
        else if(arr[a]==2)
        {
            cout<<"Paper! "<<endl;
        }
        else if(arr[a]==3)
        {
            cout<<"Scissors! "<<endl;
        }
        a=a+1;

    }

}
