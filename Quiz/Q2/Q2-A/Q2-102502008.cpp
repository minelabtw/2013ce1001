#include <iostream>
#include <cstdlib>
#include <ctime> ;
using namespace std ;
void rps(int a) //名稱
{
    switch (a)
    {
    case 1:
        cout << "Rock!";
        break ;
    case 2:
        cout << "Paper!";
        break ;
    case 3:
        cout << "Scissors!" ;
        break ;
    default :
        break ;
    }
}
int main()
{
    srand(time(NULL)); //亂數
    cout << "Rock-paper-scissors\nAI: Let's battle!\n" ;
    int time=0 ; // 次數
    int win[10]= {} ; //勝負
    int choose=0 ; //輸入
    while(time<5)
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin >> choose ;
        win[time]=choose;
        win[5+time]=rand()%3+1 ;
        if(choose==1||choose==2||choose==3)
        {
            cout << "Rock! Paper! Scissors!\n" ;
            cout << "You: " ;
            rps(win[time]) ; //名稱
            cout << " VS. AI: " ;
            rps(win[5+time]) ; // 名稱
            cout << endl;
            if(win[time]==win[5+time]) //平手
                cout << "Draw!\n\n" ;
            else if(win[time]==3 && win[5+time]==1) //剪刀輸石頭
                cout << "You lose!\n\n" ;
            else if(win[time]==1 && win[5+time]==3) //石頭贏剪刀
                cout << "You win!\n\n" ;
            else if(win[time]>win[5+time])  //數字大的贏
                cout << "You win!\n\n" ;
            else
                cout << "You lose!\n\n" ;
            time++ ;
        }
        else
            cout << "Out of range!\n" ;
    }
    for(int i=0; i<5; i++) //輸出全部的名稱
    {
        cout << i+1 <<" - You: " ;
        rps(win[i]) ;
        cout << " VS. AI: " ;
        rps(win[5+i]) ;
        cout <<endl ;
    }
    return 0 ;
}
