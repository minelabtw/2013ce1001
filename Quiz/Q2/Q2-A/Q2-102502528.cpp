#include<iostream>
#include<cstdlib>
#include <ctime>
using namespace std;
int judge(int&ai,int&ply);

int main()
{
    srand(time(0));
    int thr;
    int ai[5];

    for(int j = 0; j<5; j++)           //產生5個不同亂數並儲存
    {
        ai[j] = (rand()%3)+1;
    }
    cout << "Rock-paper-scissors"<<endl<<"AI: Let's battle!"<<endl;
    for(int times = 0; times<5; times++)         //重複執行五次
    {
        cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
        cin >> thr;
        while(thr<1||thr>3)             //判定輸入
        {
            cout << "Out of range!"<<endl<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin >> thr;
        }
        cout << "Rock! Paper! Scissors!"<<endl;
        judge(ai[times],thr);          //判定輸贏
    }

    return 0;
}
int judge(int&ai,int&ply)
{
    if(ai==ply)          //平手
    {
        if(ai == 1)
            cout << "You: Rock! VS. AI: Rock!";
        else if(ai == 2)
            cout << "You: Paper! VS. AI: Paper!";
        else
            cout << "You: Scissors! VS. AI: Scissors!";
        cout <<endl<< "Draw!"<<endl<<endl;
    }
    else if(ai-ply==1||ai-ply==-2)           //輸
    {
        if(ai == 1)
            cout << "You: Scissors! VS. AI: Rock!";
        else if(ai == 2)
            cout << "You: Rock! VS. AI: Paper!";
        else
            cout << "You: Paper! VS. AI: Scissors!";
        cout <<endl<< "You lose!"<<endl<<endl;
    }
    else               //贏
    {
        if(ai == 1)
            cout << "You: Paper! VS. AI: Rock!";
        else if(ai == 2)
            cout << "You: Scissors! VS. AI: Paper!";
        else
            cout << "You: Rock! VS. AI: Scissors!";
        cout <<endl<< "You Win!"<<endl<<endl;
    }
}

