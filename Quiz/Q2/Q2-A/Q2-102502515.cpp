#include <iostream>
#include <cstdlib>//srand/rand使用
#include <ctime>//srand使用到

using namespace std;

void name(int n)//函式用來cout數字相對應的拳法
{
    switch (n)
    {
    case 1:
        cout << "Rock! " ;
        break;//跳出switch
    case 2:
        cout << "Paper! ";
        break;
    case 3:
        cout << "Scissors! ";
        break;
    }
}

void compare(int a, int AI)//函式用來決定玩家和電腦誰輸誰贏
{
    if (a==AI)//兩者相同
    {
        cout << "Draw!" << endl;
    }
    else if (AI==3 && a==1)
    {
        cout << "You win!" << endl;
    }
    else if (AI==1 && a==3)
    {
        cout << "You lose!" << endl;
    }
    else if (AI<a)
    {
        cout << "You win!" << endl;
    }
    else if (AI>a)
    {
        cout << "You lose!" << endl;
    }
}

int main()
{
    int a;
    int player[4];//儲存玩家輸入值的陣列
    int ai[4];//儲存電腦值的陣列

    srand(time(0));//產生亂數

    cout << "Rock-paper-scissors" << endl;
    cout << "AI: Let's battle!" << endl;

    for (int i=0; i<5; i++)//製造五回合的迴圈
    {
        int AI=rand()%3 + 1;//將產生的亂數給AI
        ai[i]=AI;//將AI存到ai陣列

        do
        {
            cout << "Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin  >> a;

            if (a!=1 && a!=2 && a!=3)
            {
                cout << "Out of range!" << endl;
            }
        }
        while (a!=1 && a!=2 && a!=3);//令輸入值在範圍內的迴圈

        player[i]=a;//將玩家輸入值存到player陣列

        cout << "Rock! Paper! Scissors!" << endl;
        cout << "You: ";
        name(a);//call name函式
        cout << "VS. AI: ";
        name(AI);
        cout << endl;

        compare(a,AI);//call compare函式
        cout << endl;
    }

    for (int k=0; k<5; k++)//cout五回合出的拳
    {
        cout << player[k] << endl;
        cout << k+1 << " - " << "You: ";
        name(player[k]);
        cout << "VS. AI: ";
        name(ai[k]);
        cout << endl;
    }

    return 0;
}
