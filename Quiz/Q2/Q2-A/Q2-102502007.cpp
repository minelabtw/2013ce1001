#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(NULL));//打亂時間種子
    int randnumber,input;//input是玩家每次輸入的值
    int frequency1=0,frequency2=0;//次數1用來記錄AI,次數2用來記錄玩家
    int record1[5]= {},record2[5]= {};
    cout<<"Rock-paper-scissors"<<endl;
    cout<<"AI: Let's battle!"<<endl;
    for(int i=1; i<=5; i++)
    {
        randnumber=rand()%3+1;//randnumber是每次遊戲時程式重新給的亂數
        do
        {
            cout<<"Throw 1)Rock, 2)Paper, 3)Scissors?: ";
            cin>>input;//1=石頭,2=布,3=剪刀 ,3贏2 ,2贏1 ,1贏3
            if(input!=1 and input!=2 and input!=3)
                cout<<"Out of range!"<<endl;
        }//先控制輸入值
        while(input!=1 and input!=2 and input!=3);
        record1[frequency1]=randnumber;//儲存每次電腦的亂數值
        record2[frequency2]=input;//儲存每次玩家的輸入值
        frequency1 +=1;//次數每次+1以便下一次紀錄
        frequency2 +=1;//次數每次+1以便下一次紀錄
        cout<<"Rock! Paper! Scissors!"<<endl;
        switch(input)//依據1,2,3給出不同的結局，1,2,3分別代表石頭,布和剪刀
        {
        case 1:
        {
            cout<<"You: Rock! VS. AI: ";
            if(randnumber==1)
                cout<<"Rock!"<<endl;
            else if(randnumber==2)
                cout<<"Paper!"<<endl;
            else
                cout<<"Scissors"<<endl;
            if(randnumber==1)
                cout<<"Draw!"<<endl;
            else if(randnumber==2)
                cout<<"You lose!"<<endl;
            else
                cout<<"You win!"<<endl;
            cout<<endl;
            break;
        }//玩家出石頭的結果
        case 2:
        {
            cout<<"You: Paper! VS. AI: ";
            if(randnumber==1)
                cout<<"Rock!"<<endl;
            else if(randnumber==2)
                cout<<"Paper!"<<endl;
            else
                cout<<"Scissors"<<endl;
            if(randnumber==1)
                cout<<"You win!"<<endl;
            else if(randnumber==2)
                cout<<"Draw!"<<endl;
            else
                cout<<"You lose!"<<endl;
            cout<<endl;
            break;
        }//玩家出布的結果
        case 3:
        {
            cout<<"You: Scissors! VS. AI: ";
            if(randnumber==1)
                cout<<"Rock!"<<endl;
            else if(randnumber==2)
                cout<<"Paper!"<<endl;
            else
                cout<<"Scissors"<<endl;
            if(randnumber==1)
                cout<<"You lose!"<<endl;
            else if(randnumber==2)
                cout<<"You win!"<<endl;
            else
                cout<<"Draw!"<<endl;
            cout<<endl;
            break;
            default :
                break;
            }//玩家出剪刀的結果
        }
    }//迴圈5次
    for(int i=1; i<=frequency2; i++)
    {
        cout<<i<<" - You: ";
        if(record2[i-1]==1)
        {
            cout<<"Rock! ";
            if(record1[i-1]==1)
                cout<<"VS. AI: Rock!";
            else if(record1[i-1]==2)
                cout<<"VS. AI: Paper!";
            else
                cout<<"VS. AI: Scissors!";
            cout<<endl;
        }
        else if(record2[i-1]==2)
        {
            cout<<"Paper! ";
            if(record1[i-1]==1)
                cout<<"VS. AI: Rock!";
            else if(record1[i-1]==2)
                cout<<"VS. AI: Paper!";
            else
                cout<<"VS. AI: Scissors!";
            cout<<endl;
        }
        else
        {
            cout<<"Scissors! ";
            if(record1[i-1]==1)
                cout<<"VS. AI: Rock!";
            else if(record1[i-1]==2)
                cout<<"VS. AI: Paper!";
            else
                cout<<"VS. AI: Scissors!";
            cout<<endl;
        }
    }//這個迴圈印出每次玩家與電腦的對戰紀錄
    return 0;
}
