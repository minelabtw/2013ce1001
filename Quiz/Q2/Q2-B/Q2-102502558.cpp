#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;

int main()
{
    srand(time(NULL));
    // 事先存好會用到的字串 方便使用
    string prompt[] = {"First","Second","Third"};
    string prompt2[] = {"Math","Chinese","English"};
    double student[3][3] = {}; // 存成績的陣列
    // student[學生][成績]
    for (int i=0;i<3;i++)
        for (int j=0;j<3;j++)
            student[i][j] = rand() % 101; // 0 - 100
    for (int i=0;i<3;i++)
    {
        // 印出成績
        cout << prompt[i] << " student Math score: " << student[i][0] << endl;
        cout << prompt[i] << " student Chinese score: " << student[i][1] << endl;
        cout << prompt[i] << " student English score: " << student[i][2] << endl;
        cout << prompt[i] << " student total score: " << student[i][0]+student[i][1]+student[i][2] << endl;
    }
    for (int i=0;i<3;i++)
    {
        // 印出平均
        cout << prompt2[i] << " average score: " << (student[0][i]+student[1][i]+student[2][i])/3.0 << endl;
    }
    return 0;
}
