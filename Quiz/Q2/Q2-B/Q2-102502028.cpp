#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std ;

int total (int,int,int) ;                    //function prototype
double average (int,int,int) ;

int main()
{
    int student1[3] = {} ;                   //宣告陣列並設初始值=0
    int student2[3] = {} ;
    int student3[3] = {} ;
    srand (time(0)) ;

    for (int a = 0 ; a <= 2 ; a++)           //陣列裡的數值使用亂數
    {
        student1[a] = rand() % 101 ;
    }

    for (int b = 0 ; b <= 2 ; b++)
    {
        student2[b] = rand() % 101 ;
    }

    for (int c = 0 ; c <= 2 ; c++)
    {
        student3[c] = rand() % 101 ;
    }

    cout << "First student Math score: " << student1[0] << endl ;                                               //第一個學生的數學英文國文成績還有加總
    cout << "First student Chinese score: " << student1[1] << endl ;
    cout << "First student English score: " << student1[2] << endl ;
    cout << "First student total score: " << total (student1[0],student1[1],student1[2]) << endl ;

    cout << "Second student Math score: " << student2[0] << endl ;                                              //第二個學生的數學英文國文成績還有加總
    cout << "Second student Chinese score: " << student2[1] << endl ;
    cout << "Second student English score: " << student2[2] << endl ;
    cout << "Second student total score: " << total (student2[0],student2[1],student2[2]) << endl ;

    cout << "Third student Math score: " << student3[0] << endl ;                                                //第三個學生的數學英文國文成績還有加總
    cout << "Third student Chinese score: " << student3[1] << endl ;
    cout << "Third student English score: " << student3[2] << endl ;
    cout << "Third student total score: " << total (student3[0],student3[1],student3[2]) << endl ;

    cout << "Math average score: " << average (student1[0],student2[0],student3[0]) << endl ;                    //三個學生的數學成績平均
    cout << "Chinese average score: " << average (student1[1],student2[1],student3[1]) << endl ;                 //三個學生的數學成績平均
    cout << "English average score: " << average (student1[2],student2[2],student3[2]) << endl ;                 //三個學生的數學成績平均

    return 0 ;
}

int total (int a,int b,int c)                 //function裡面的內容
{
    int t = a + b + c ;
    return t ;
}

double average (int a,int b,int c)
{
    double average = ((double)a + (double)b + (double)c) / 3 ;
    return average ;
}


