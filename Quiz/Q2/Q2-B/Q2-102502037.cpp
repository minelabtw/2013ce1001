#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    int x=0,y=0;
    srand(time(0)); //取變數
    int score[2][2]; //設定陣列並且將變數值存入
    while(x<=2)
    {
        while (y<=2)
        {
          score[x][y]=rand()%100+1;
          y++;
        }
        x++;
        y=0;
    }
    cout<<"First student Math score: "<<score[0][0]<<endl; //輸出所需的答案
    cout<<"First student Chinese score: "<<score[0][1]<<endl;
    cout<<"First student English score: "<<score[0][2]<<endl;
    cout<<"First student total score: "<<score[0][0]+score[0][1]+score[0][2]<<endl;
    cout<<"Second student Math score: "<<score[1][0]<<endl;
    cout<<"Second student Chinese score: "<<score[1][1]<<endl;
    cout<<"Second student English score: "<<score[1][2]<<endl;
    cout<<"Second student total score: "<<score[1][0]+score[1][1]+score[1][2]<<endl;
    cout<<"Third student Math score: "<<score[2][0]<<endl;
    cout<<"Third student Chinese score: "<<score[2][1]<<endl;
    cout<<"Third student English score: "<<score[2][2]<<endl;
    cout<<"Third student total score: "<<score[2][0]+score[2][1]+score[2][2]<<endl;
    double a=score[0][0]+score[1][0]+score[2][0];
    double b=score[0][1]+score[1][1]+score[2][1];
    double c=score[0][2]+score[1][2]+score[2][2];
    double d=a/3;
    double e=b/3;
    double f=c/3;
    cout<<"Math average score: "<<d<<endl;
    cout<<"Chinese average score: "<<e<<endl;
    cout<<"English average score: "<<f;
    return 0;
}
