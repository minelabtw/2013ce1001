#include<iostream>
#include<ctime>
#include<cstdlib>

using namespace std;
int main()
{
    int acc[3][3];  //student and score
    float ma,ca,ea; //average of math, chinese and english
    srand(time(NULL));  //random
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            acc[i][j]=rand()%101;   //set score
        }
    }
    // output first
    cout << "First student Math score: " << acc[0][0] << endl;
    cout << "First student Chinese score: " << acc[1][0] << endl;
    cout << "First student English score: " << acc[2][0] << endl;
    cout << "First student total score: " << acc[0][0]+acc[1][0]+acc[2][0] << endl;
    //output second
    cout << "Second student Math score: " << acc[0][1] << endl;
    cout << "Second student Chinese score: " << acc[1][1] << endl;
    cout << "Second student English score: " << acc[2][1] << endl;
    cout << "Second student total score: " << acc[0][1]+acc[1][1]+acc[2][1] << endl;
    //output third
    cout << "Third student Math score: " << acc[0][2] << endl;
    cout << "Third student Chinese score: " << acc[1][2] << endl;
    cout << "Third student English score: " << acc[2][2] << endl;
    cout << "Third student total score: " << acc[0][2]+acc[1][2]+acc[2][2] << endl;
    //for average
    ma=acc[0][0]+acc[0][1]+acc[0][2];
    ma/=3;
    ca=acc[1][0]+acc[1][1]+acc[1][2];
    ca/=3;
    ea=acc[2][0]+acc[2][1]+acc[2][2];
    ea/=3;
    //output average
    cout << "Math average score: " << ma << endl;
    cout << "Chinese average score: " << ca << endl;
    cout << "English average score: " << ea << endl;

    return 0;
}
