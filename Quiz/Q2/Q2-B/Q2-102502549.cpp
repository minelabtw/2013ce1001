#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{

    double score[3][3];//儲存成績用二維陣列
    double amath;//數學平均
    double achi;//國文平均
    double aeng;//英文平均

    srand(time(0));//設定以時間作亂數種子

//把二維陣列填入亂數
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            score[i][j]=rand()%101;//範圍0~100
        }
    }

//計算平均
    amath=(score[0][0]+score[1][0]+score[2][0])/3;
    achi=(score[0][1]+score[1][1]+score[2][1])/3;
    aeng=(score[0][2]+score[1][2]+score[2][2])/3;

//漫長的輸出
    cout<<"First student Math score: "<<score[0][0]<<endl;
    cout<<"First student Chinese score: "<<score[0][1]<<endl;
    cout<<"First student English score: "<<score[0][2]<<endl;
    cout<<"First student total score: "<<score[0][0]+score[0][1]+score[0][2]<<endl;

    cout<<"Second student Math score: "<<score[1][0]<<endl;
    cout<<"Second student Chinese score: "<<score[1][1]<<endl;
    cout<<"Second student English score: "<<score[1][2]<<endl;
    cout<<"Second student total score: "<<score[1][0]+score[1][1]+score[1][2]<<endl;

    cout<<"Third student Math score: "<<score[2][0]<<endl;
    cout<<"Third student Chinese score: "<<score[2][1]<<endl;
    cout<<"Third student English score: "<<score[2][2]<<endl;
    cout<<"Third student total score: "<<score[2][0]+score[2][1]+score[2][2]<<endl;

    cout<<"Math average score: "<<amath<<endl;
    cout<<"Chinese average score: "<<achi<<endl;
    cout<<"English average score: "<<aeng;

    return 0;//結束

}
