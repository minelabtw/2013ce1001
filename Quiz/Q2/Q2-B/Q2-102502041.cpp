#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    srand(time(0));                       /*讓random的seed隨著時間去變化*/
    float data[3][3];
    for(int i=0; i<=2; i++)               /*使用兩層for迴圈將分數存進data陣列*/
    {
        for(int j=0; j<=2; j++)
        {
            data[i][j]=rand()%101;
        }
    }
    cout<<"First student Math score: "<<data[0][0]<<endl;
    cout<<"First student Chinese score: "<<data[0][1]<<endl;
    cout<<"First student English score: "<<data[0][2]<<endl;
    cout<<"First student total score: "<<data[0][0]+data[0][1]+data[0][2]<<endl;
    cout<<"Second student Math score: "<<data[1][0]<<endl;
    cout<<"Second student Chinese score: "<<data[1][1]<<endl;
    cout<<"Second student English score: "<<data[1][2]<<endl;
    cout<<"Second student total score: "<<data[1][0]+data[1][1]+data[1][2]<<endl;
    cout<<"Third student Math score: "<<data[2][0]<<endl;
    cout<<"Third student Chinese score: "<<data[2][1]<<endl;
    cout<<"Third student English score: "<<data[2][2]<<endl;
    cout<<"Third student total score: "<<data[2][0]+data[2][1]+data[2][2]<<endl;
    cout<<"Math average score: "<<float((data[0][0]+data[1][0]+data[2][0])/3)<<endl;
    cout<<"Chinese average score: "<<float((data[0][1]+data[1][1]+data[2][1])/3)<<endl;
    cout<<"English average score: "<<float((data[0][2]+data[1][2]+data[2][2])/3)<<endl;
    return 0;
}
