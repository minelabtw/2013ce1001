#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

int main(){
    string studentArray[3] = {"First", "Secend", "Third"};
    //宣告型態為stirng的一圍學生名稱陣列並初始化為"First", "Secend", "Third"
    string courseArray[4] = {"Math", "Chinese", "English", "total"};
    //宣告型態為stirng的一圍科目名稱陣列並初始化為"Math", "Chinese", "English", "total"
    float reacordArray[3][4];//宣告型態為float的二圍成績陣列，陣列大小為3*4，未初始化
    //reacordArray[0][0-2]為數學成績, reacordArray[0][3]為數學平均
    //reacordArray[1][0-2]為國文成績, reacordArray[1][3]為國文平均
    //reacordArray[2][0-2]為英文成績, reacordArray[2][3]為英文平均

    srand(time(NULL));//初始化random generator
    for(int i = 0; i < 3; i++){
        //初始化reacordArray的學生各科成績和其加總
        for(int j = 0; j < 3; j++){
            reacordArray[j][i] = rand()%101;
        }
    }
    for(int i = 0; i < 3; i++){
        //初始化reacordArray的各科平均成績
        float score = 0.0;//宣告型態為float的成績加總
        for(int j = 0; j < 3; j++){
            score += reacordArray[i][j];
        }
        reacordArray[i][3] = score / 3.0;
    }

    for(int i = 0; i < 3; i++){//輸出學生各科成績和其總成績
        float score = 0.0;//宣告型態為float的成績加總
        for(int j = 0; j < 4; j++){
            cout << studentArray[i] << " student " << courseArray[j] << " score: ";
            if(j < 3){//j在0-2輸出學生各科成績
                cout <<  reacordArray[j][i] << endl;
                score += reacordArray[j][i];
            }
            else{//在3輸出學生總成績
                cout <<  score << endl;
            }
        }
    }
    for(int i = 0; i < 3; i++){//輸出各科平均
        cout << courseArray[i] << " average score: " << reacordArray[i][3] << endl;
    }

    return 0;
}
