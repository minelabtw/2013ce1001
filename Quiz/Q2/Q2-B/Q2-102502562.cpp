#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));//亂數用時間當種子
    int grade1[3]={0};//宣告型別為整數的各科成績
    int grade2[3]={0};
    int grade3[3]={0};
    double math_average,chinese_average,english_average;//宣告型別為浮點數的各科平均

    grade1[1]=rand()%101;//用亂數決定成績
    grade1[2]=rand()%101;
    grade1[3]=rand()%101;
    grade2[1]=rand()%101;
    grade2[2]=rand()%101;
    grade2[3]=rand()%101;
    grade3[1]=rand()%101;
    grade3[2]=rand()%101;
    grade3[3]=rand()%101;
    math_average = (grade1[1]+grade2[1]+grade3[1]) /3;//計算平均
    chinese_average = (grade1[2]+grade2[2]+grade3[2]) / 3;
    english_average = (grade1[3]+grade2[3]+grade3[3]) / 3;
    cout << "First student Math score: " << grade1[1] << endl;//輸出各科成績和總和及平均
    cout << "First student Chinese score: " << grade1[2] << endl;
    cout << "First student English score: " << grade1[3] << endl;
    cout << "First student total score: " << grade1[1]+grade1[2]+grade1[3] << endl;
    cout << "Second student Math score: " << grade2[1] << endl;
    cout << "Second student Chinese score: " << grade2[2] << endl;
    cout << "Second student English score: " << grade2[3] << endl;
    cout << "Second student total score: " << grade2[1]+grade2[2]+grade2[3] << endl;
    cout << "Third student Math score: " << grade3[1] << endl;
    cout << "Third student Chinese score: " << grade3[2] << endl;
    cout << "Third student English score: " << grade3[3] << endl;
    cout << "Third student total score: " << grade3[1]+grade3[2]+grade3[3] << endl;
    cout << "Math average score: " << math_average << endl;
    cout << "Chinese average score: " << chinese_average << endl;
    cout << "English average score: " << english_average << endl;

    return 0;
}
