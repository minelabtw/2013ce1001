/*************************************************************************
    > File Name: Q2-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年12月04日 (週三) 16時04分25秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	/* init the max number of subject and student */
	#define MAX_sub 3
	#define MAX_stu 3

	/* init the subject name and student name */
	char subject[MAX_sub][10] = {"Math", "Chinese", "English"};
	char name[MAX_stu][10] = {"First", "Second", "Third"};
	
	/* score[i][j] , i is the id of sutdent, j is the id of subject */
	/* tot[i] is the total score of student_i */
	/* avg[i] is the total score of subject_i */
	char score[MAX_stu][MAX_sub];
	int tot[MAX_stu];
	int avg[MAX_sub];

	/* init the tot and avg array */
	for(int i=0 ; i<MAX_sub ; i++)
		avg[i] = 0;
	for(int i=0 ; i<MAX_stu ; i++)
		tot[i] = 0;

	/* init the seed of rand() */
	srand(time(NULL));


	for(int i=0 ; i<MAX_stu ; i++)
	{
		for(int j=0 ; j<MAX_sub ; j++)
		{
			/* rand number in range [0, 100] */
			score[i][j] = rand() % 101;
			
			/* calc the total score of subject and student */
			avg[j] += score[i][j]; 
			tot[i] += score[i][j]; 

			/* output the score */
			printf("%s student %s score: %d\n", name[i], subject[j], score[i][j]);
		}
		/* output the total score of student */
		printf("%s student total score: %d\n", name[i], tot[i]);
	}

	for(int i=0 ; i<MAX_sub ; i++)
		/* output the average score of subject */
		printf("%s average score: %lf\n", subject[i], (double)avg[i] / 3.0);


	#undef MAX_stu
	#undef MAX_sub
	
	return 0;
}

