#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace::std;

int main()
{
    double s[3][3];//宣告二維陣列
    srand(time(NULL));//改變系統起始值
    int i=0,j=0;//迴圈變數

    for(i=0;i<3;i++)//隨機產生亂數儲存到二維陣列裡
    {
        for(j=0;j<3;j++)
        {
            s[i][j]=rand()%101;
        }
    }
    //顯示第一位學生的成績
    cout<<"First student Math score: "<<s[0][0]<<endl;
    cout<<"Fitst student Chinese score: "<<s[0][1]<<endl;
    cout<<"First student English score: "<<s[0][2]<<endl;
    cout<<"First student total score: "<<s[0][0]+s[0][1]+s[0][2]<<endl;
    //顯示第二位學生的成績
    cout<<"Second student Math score: "<<s[1][0]<<endl;
    cout<<"Second student Chinese score: "<<s[1][1]<<endl;
    cout<<"Second student English score: "<<s[1][2]<<endl;
    cout<<"Second student total score: "<<s[1][0]+s[1][1]+s[1][2]<<endl;
    //顯示第三位學生的成績
    cout<<"Third student Math score: "<<s[2][0]<<endl;
    cout<<"Third student Chinese score: "<<s[2][1]<<endl;
    cout<<"Third student English score: "<<s[2][2]<<endl;
    cout<<"Third student total score: "<<s[2][0]+s[2][1]+s[2][2]<<endl;
    //顯示三種科目的成績平均
    cout<<"Math average score: "<<(s[0][0]+s[1][0]+s[2][0])/3<<endl;
    cout<<"Chinese average score: "<<(s[0][1]+s[1][1]+s[2][1])/3<<endl;
    cout<<"English average score: "<<(s[0][2]+s[1][2]+s[2][2])/3<<endl;

    return 0;
}
