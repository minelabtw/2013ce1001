#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

int main()
{
    //decalaration and initialization
    double grade[4][4] = {};
    string grade_name;

    //set grades
    srand( time( NULL ) );
    for ( int i = 0; i <= 2; i ++ )
        for ( int j = 0; j <= 2; j ++ )
        {
            grade[i][j] = rand() % 101;
            grade[i][3] += grade[i][j];
        }

    //caculate each subject's total score
    for ( int i = 0; i <= 2; i ++ )
        for ( int j = 0; j <= 2; j ++ )
            grade[3][i] +=  grade[j][i];

    //output each student's grades (include personal totle)
    for ( int i = 0; i <= 2; i ++ )
    {
        for ( int j = 0; j <= 3; j ++ )
        {
            switch ( j )
            {
            case 0:
                grade_name = "Math";
                break;
            case 1:
                grade_name = "Chinese";
                break;
            case 2:
                grade_name = "English";
                break;
            default:
                grade_name = "total";
                break;
            }

            switch ( i )
            {
            case 0:
                cout << "First";
                break;
            case 1:
                cout << "Second";
                break;
            default:
                cout << "Third";
                break;
            }

            cout << " student " << grade_name << " socore: " << grade[i][j] << endl;
        }
    }

    //output avg of each subject
    for ( int i = 0; i <= 2; i ++)
    {
        switch ( i )
        {
        case 0:
            grade_name = "Math";
            break;
        case 1:
            grade_name = "Chinese";
            break;
        case 2:
            grade_name = "English";
            break;
        default:
            grade_name = "total";
            break;
        }
        cout << grade_name << " average socore: " << grade[3][i] / 3 << endl;
    }

    return 0;
}
