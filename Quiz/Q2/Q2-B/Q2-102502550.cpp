#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

int main()
{
    int score[4][4],j;                                                                 //score[人][科目]
    char *a[4]= {"","First","Second","Third"};                                         //字串陣列
    float d;

    srand(time(NULL));                                                                //亂數初始


    for(int i=1; i<=3; i++)
    {
        for(int j=1; j<=3; j++)
        {
            score[i][j]=rand()%101;                                                   //填入亂數
        }
    }

    for(int i=1; i<=3; i++)                                                              //印出陣列
    {
        j=1;
        cout<<a[i]<<" student Math score: "<<score[i][j]<<endl;
        j++;
        cout<<a[i]<<" student Chinese score: "<<score[i][j]<<endl;
        j++;
        cout<<a[i]<<" student English score: "<<score[i][j]<<endl;
        j++;
        cout<<a[i]<<" student total score: "<<(score[i][1]+score[i][2]+score[i][3])<<endl;
        j++;
    }

    cout<<"Math average score: "<<(float)(score[1][1]+score[2][1]+score[3][1])/3<<endl;         //印出平均值
    cout<<"Chinese average score: "<<(float)(score[1][2]+score[2][2]+score[3][2])/3<<endl;
    cout<<"English average score: "<<(float)(score[1][3]+score[2][3]+score[3][3])/3<<endl;

    return 0;
}
