#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;

int main()
{
    srand(time(0));//以time(0)為種子打亂
    double average,score[3][3];//宣告double,average為平均分數,score[][]為三人三科的成績分別為數學國文英文
    int i,j;//迴圈用變數
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            score[i][j] = rand()%101;//以rand()產生分數
        }
    }
    cout << "First student Math score: " << score[0][0] << endl;
    cout << "First student Chinese score: " << score[0][1] << endl;
    cout << "First student English score: " << score[0][2] << endl;
    cout << "First student total score: " << score[0][0]+score[0][1]+score[0][2] << endl;
    cout << "Second student Math score: " << score[1][0] << endl;
    cout << "Second student Chinese score: " << score[1][1] << endl;
    cout << "Second student English score: " << score[1][2] << endl;
    cout << "Second student total score: " << score[1][0]+score[1][1]+score[1][2] << endl;
    cout << "Third student Math score: " << score[2][0] << endl;
    cout << "Third student Chinese score: " << score[2][1] << endl;
    cout << "Third student English score: " << score[2][2] << endl;
    cout << "Third student total score: " << score[2][0]+score[2][1]+score[2][2] << endl;
    average = (score[0][0]+score[1][0]+score[2][0])/3;
    cout << "Math average score: " << average << endl;
    average = (score[0][1]+score[1][1]+score[2][1])/3;
    cout << "Chinese average score: " << average << endl;
    average = (score[0][2]+score[1][2]+score[2][2])/3;
    cout << "English average score: " << average << endl;//輸出成績
    return 0;
}
