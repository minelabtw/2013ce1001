#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main ()
{
    double s[3][3]= {};//3學生之成績陣列
    int ta,tb,tc;//成績之總和

    srand (time(0));
    for (int i=0; i<=2; i++)
    {
        for (int j=0; j<=2; j++)
        {
        s[i][j]=(rand()%100)+1;
        }
    }//隨機取出9數字放入陣列表示成績

    ta=s[0][0]+s[0][1]+s[0][2];
    tb=s[1][0]+s[1][1]+s[1][2];
    tc=s[2][0]+s[2][1]+s[2][2];//計算總合


    cout << "First student Math score: " << s[0][0] << endl;
    cout << "First student Chinese score: " << s[0][1] << endl;
    cout << "First student English score: " << s[0][2] << endl;
    cout << "First student total score: " << ta << endl;

    cout << "Second student Math score: " << s[1][0] << endl;
    cout << "Second student Chinese score: " << s[1][1] << endl;
    cout << "Second student English score: " << s[1][2] << endl;
    cout << "Second student total score: " << tb << endl;

    cout << "Third student Math score: " << s[2][0] << endl;
    cout << "Third student Chinese score: " << s[2][1] << endl;
    cout << "Third student English score: " << s[2][2] << endl;
    cout << "Third student total score: " << tc << endl;//輸出結果

    double math=(s[0][0]+s[1][0]+s[2][0])/4;
    double chinese=(s[0][1]+s[1][1]+s[2][1])/4;
    double english=(s[0][2]+s[1][2]+s[2][2])/4;//計算各科平均

    cout << "Math average score: " << math << endl;
    cout << "Chinese average score: " << chinese << endl;
    cout << "English average score: " << english << endl;//輸出平均

    return 0;
}
