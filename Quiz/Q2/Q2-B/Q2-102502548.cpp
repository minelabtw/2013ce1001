#include <iostream>

#include <stdlib.h>

#include <time.h>

using namespace std;

int main()
{
    int sa[4] ;//宣告陣列

    int sb[4] ;

    int sc[4] ;

    int i=1 ;

    int number=0 ;

    double totala=0, totalb=0, totalc=0 ;

    double averageM=0, averageC=0, averageE=0 ;// 宣告變數


    while (i<=3)//使用迴圈
    {
        srand(time(NULL)) ;//隨機選數

        number=rand()%101 ;

        sa[i]=number ;

        i++ ;
    }

    totala=sa[1]+sa[2]+sa[3] ;


    for(int x=1; x<=3; x++)
    {
        srand(time(0)) ;

        number=rand()%101 ;

        sb[x]=number ;
    }

    totalb=sb[1]+sb[2]+sb[3] ;

    for(int y=1; y<=3; y++)
    {
        srand(time(NULL)) ;

        number=rand()%101 ;

        sc[y]=number ;
    }

    totalc=sc[1]+sc[2]+sc[3] ;

    averageM=(sa[1]+sb[1]+sc[1])/3 ;

    averageC=(sa[2]+sb[2]+sc[2])/3 ;

    averageE=(sa[3]+sb[3]+sc[3])/3 ;

    cout << "First student Math score: " << sa[1] << endl ;//在螢幕上輸出

    cout << "First student Chinese score: " << sa[2] << endl ;

    cout << "First student English score: " << sa[3] << endl ;

    cout << "First student total score: " << totala << endl ;

    cout << "Second student Math score: " << sb[1] << endl ;

    cout << "Second student Chinese score: " << sb[2] << endl ;

    cout << "Second student English score: " << sb[3] << endl ;

    cout << "Second student total score: " << totalb << endl ;

    cout << "Third student Math score: " << sc[1] << endl ;

    cout << "Third student Chinese score: " << sc[2] << endl ;

    cout << "Third student English score: " << sc[3] << endl ;

    cout << "Third student total score: " << totalc << endl ;

    cout << "Math average score: " << averageM << endl ;

    cout << "Chinese average score: " << averageC << endl ;

    cout << "English average score: " << averageE ;

    return 0 ;
}
