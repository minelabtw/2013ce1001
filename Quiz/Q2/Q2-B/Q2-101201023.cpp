#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main()
{
    double FG[3];
    double SG[3];
    double TG[3];
    double EA=0;
    srand(time(NULL));

    for(int i=1 ; i<=3 ; i++)
    {
        if(i==1)
        {
            FG[i]=rand()%101;                                                   //將數學成績存進陣列，且範圍在零到一百
            cout << "First student Math score: " << FG[i] << endl;
        }

        if(i==2)
        {
            FG[i]=rand()%101;                                                   //將國文成績存進陣列，且範圍在零到一百
            cout << "First student Chinese score: " << FG[i] << endl;
        }

        if(i==3)
        {
            FG[i]=rand()%101;                                                   //將英文成績存進陣列，且範圍在零到一百
            cout << "First student English score: " << FG[i] << endl;
            cout << "First student total score: " << FG[1]+FG[2]+FG[3] << endl;
        }
    }

    for(int i=1 ; i<=3 ; i++)
    {
        if(i==1)
        {
            SG[i]=rand()%101;
            cout << "Second student Math score: " << SG[i] << endl;
        }

        if(i==2)
        {
            SG[i]=rand()%101;
            cout << "Second student Chinese score: " << SG[i] << endl;
        }

        if(i==3)
        {
            SG[i]=rand()%101;
            cout << "Second student English score: " << SG[i] << endl;
            cout << "Second student total score: " << SG[1]+SG[2]+SG[3] << endl;
        }
    }

    for(int i=1 ; i<=3 ; i++)
    {
        if(i==1)
        {
            TG[i]=rand()%101;
            cout << "Third student Math score: " << TG[i] << endl;
        }

        if(i==2)
        {
            TG[i]=rand()%101;
            cout << "Third student Chinese score: " << TG[i] << endl;
        }

        if(i==3)
        {
            TG[i]=rand()%101;
            cout << "Third student English score: " << TG[i] << endl;
            cout << "Third student total score: " << TG[1]+TG[2]+TG[3] << endl;
        }
    }

    cout << "Math average score: " << (FG[1]+SG[1]+TG[1])/3 << endl;                                 //輸出各科平均
    cout << "Chinese average score: " << (FG[2]+SG[2]+TG[2])/3 << endl;
    cout << "English average score: " << (FG[3]+SG[3]+TG[3])/3;

    return 0;
}
