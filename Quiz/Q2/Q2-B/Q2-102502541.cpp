#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int sum(int x,int y,int z)//總和函式
{
    return x+y+z;
}
double average(double x,double y,double z)//平均函式
{
    return (x+y+z)/3;
}
int main()
{
    const int scoresize=4;
    int score[scoresize][scoresize]= {};
    srand(time(0));
    for(int i=1; i<scoresize; i++)//儲存亂數到陣列
    {
        for(int j=1; j<scoresize; j++)
        {
            score[i][j]=rand()%101;
        }
    }
    cout << "First student Math score: " << score[1][1] << endl;
    cout << "First student Chinese score: " << score[1][2] << endl;
    cout << "First student English score: " << score[1][3] << endl;
    cout << "First student total score: " <<sum(score[1][1],score[1][2],score[1][3])<< endl;
    cout << "Second student Math score: " << score[2][1] << endl;
    cout << "Second student Chinese score: " << score[2][2] << endl;
    cout << "Second student English score: " << score[2][3] << endl;
    cout << "Second student total score: " << sum(score[2][1],score[2][2],score[2][3])<< endl;
    cout << "Third student Math score: " << score[3][1] << endl;
    cout << "Third student Chinese score: " << score[3][2] << endl;
    cout << "Third student English score: " << score[3][3] << endl;
    cout << "Third student total score: " << sum(score[3][1],score[3][2],score[3][3])<< endl;
    cout << "Math average score: " << average(score[1][1],score[2][1],score[3][1]) << endl;
    cout << "Chinese average score: " <<average(score[1][2],score[2][2],score[3][2]) << endl;
    cout << "English average score: "  << average(score[1][3],score[2][3],score[3][3]) ;
    return 0;

}
