#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    int a[3]= {};//宣告陣列存放各科成績
    int b[3]= {};
    int c[3]= {};
    //宣告變數
    float Math_average_score=0;
    float Chinese_average_score=0;
    float English_average_score=0;
    int g1=0;
    int g2=0;
    int g3=0;
    int g4=0;
    int g5=0;
    int g6=0;
    int g7=0;
    int g8=0;
    int g9=0;

    srand(time(0));

    //輸出第一位學生各科成績和總成績
    g1=rand()%101;
    cout<<"First student Math score: "<<g1<<endl;
    a[1]=g1;//將成績存入陣列中

    g2=rand()%101;
    cout<<"First student Chinese score: "<<g2<<endl;
    a[2]=g2;

    g3=rand()%101;
    cout<<"First student English score: "<<g3<<endl;
    a[3]=g3;
    cout<<"First student total score: "<<a[1]+a[2]+a[3]<<endl;

    //輸出第二位學生各科成績和總成績
    g4=rand()%101;
    cout<<"Second student Math score: "<<g4<<endl;
    b[1]=g4;//將成績存入陣列中

    g5=rand()%101;
    cout<<"Second student Chinese score: "<<g5<<endl;
    b[2]=g5;

    g6=rand()%101;
    cout<<"Second student English score: "<<g6<<endl;
    b[3]=g6;
    cout<<"Second student total score: "<<b[1]+b[2]+b[3]<<endl;

    //輸出第三位學生各科成績和總成績
    g7=rand()%101;
    cout<<"Third student Math score: "<<g7<<endl;
    c[1]=g7;//將成績存入陣列中

    g8=rand()%101;
    cout<<"Third student Chinese score: "<<g8<<endl;
    c[2]=g8;

    g9=rand()%101;
    cout<<"Third student English score: "<<g9<<endl;
    c[3]=g9;
    cout<<"Third student total score: "<<c[1]+c[2]+c[3]<<endl;

    Math_average_score=(a[1]+a[2]+a[3])/3;//計算數學平均分數
    cout<<"Math average score: "<<Math_average_score<<endl;

    Chinese_average_score=(b[1]+b[2]+b[3])/3;//計算國文平均分數
    cout<<"Chinese average score: "<<Chinese_average_score<<endl;

    English_average_score=(c[1]+c[2]+c[3])/3;//計算英文平均分數
    cout<<"English average score: "<<English_average_score<<endl;

    return 0;

}
