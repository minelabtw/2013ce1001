#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    srand(time(0));//時間變數
    double s[3][3],M=0,C=0,E=0;
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
            s[i][j]=rand()%101;//將值寫入二維陣列
    }
    //****以下顯示各學生的各科成績****
    cout<<"First student Math score: "<<s[0][0]<<endl;
    cout<<"First student Chinese score: "<<s[0][1]<<endl;
    cout<<"First student English score: "<<s[0][2]<<endl;
    cout<<"First student total score: "<<s[0][0]+s[0][1]+s[0][2]<<endl;

    cout<<"Second student Math score: "<<s[1][0]<<endl;
    cout<<"Second student Chinese score: "<<s[1][1]<<endl;
    cout<<"Second student English score: "<<s[1][2]<<endl;
    cout<<"Second student total score: "<<s[1][0]+s[1][1]+s[1][2]<<endl;

    cout<<"Third student Math score: "<<s[2][0]<<endl;
    cout<<"Third student Chinese score: "<<s[2][1]<<endl;
    cout<<"Third student English score: "<<s[2][2]<<endl;
    cout<<"Third student total score: "<<s[2][0]+s[2][1]+s[2][2]<<endl;

    M=(s[0][0]+s[1][0]+s[2][0])/3;//做平均
    C=(s[0][1]+s[1][1]+s[2][1])/3;//做平均
    E=(s[0][2]+s[1][2]+s[2][2])/3;//做平均

    cout<<"Math average score: "<<M<<endl;
    cout<<"Chinese average score: "<<C<<endl;
    cout<<"English average score: "<<E<<endl;

    return 0;
}
