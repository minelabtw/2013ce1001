#include<iostream>
#include<ctime>
#include<algorithm>
using namespace std;

int main()
{
    srand(time(0)); //讓變數隨時間取亂數
    float Grade[3][3]= {}; //宣告Grade二階矩陣
    string Student[3]= {"First","Second","Third"}; //宣告Student矩陣
    string Subject[3]= {"Math","Chinese","English"}; //宣告Subject矩陣
    for (int i=0; i<=2; i++)
    {
        for (int j=0; j<=2; j++)
        {
            Grade[i][j]=rand()%100; //存到陣列中的變數範圍在0到100之間
            cout << Student[i] << " student " << Subject[j]<<" score: " <<Grade[i][j]<<endl; //輸出三位同學的各科成績
        }
        cout << Student[i] << " student total score: " <<Grade[i][0]+Grade[i][1]+Grade[i][2]<<endl; //輸出三位同學的個人總成績
    }
    for (int i=0; i<=2; i++)
        cout << Subject[i] << " average score: " <<(Grade[i][0]+Grade[i][0]+Grade[i][0])/3<<endl; //輸出數學、國文和英文平均成績
    return 0;
}
