#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    srand(time(0));
    int score[3][3] = {};  //前面的[3]為學生序號(0=First, 1=Second, 2=Third)，後面的[3]為科目序號(0=Math, 1=Chinese ,2=English)
    double sum=0;          //科目總和
    string number;
    string subject;

    for (int j=0; j<=2; j=j+1)
        for (int i=0; i<=2; i=i+1)
        {
            score[i][j] = rand()%101;  //產生隨機分數
        }

    for (int i=0; i<=2; i=i+1)
    {
        if (i==0)
            number="First";
        else if (i==1)
            number="Second";
        else if (i==2)
            number="Third";
        for (int j=0; j<=2; j=j+1)
        {
            if (j==0)
                subject="Math";
            else if (j==1)
                subject="Chinese";
            else if (j==2)
                subject="English";
            cout << number << " student " << subject << " score: " << score[i][0] << endl;
        }
        cout << number << " student total score: " << score[i][0] + score[i][1] + score[i][2] << endl;
    }

    sum = score[0][0] + score[1][0] + score[2][0];
    cout << "Math average score: " << sum/3 << endl;
    sum = score[0][1] + score[1][1] + score[2][1];
    cout << "Chinese average score: " << sum/3 << endl;
    sum = score[0][2] + score[1][2] + score[2][2];
    cout << "English average score: " << sum/3 << endl;

    return 0;
}
