//============================================================================
// Name        : Q2-102502038.cpp
// Author      : catLee
// Version     : 0.1 - ver.Naka
// Description : NCU CE1001 Q2 ver.Naka
//============================================================================
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

//define class student
class student{
public:
  double score[3];  //score[] = {math,chinese,english}
  student(int _math,int _zh,int _en){
    score[0] = _math;
    score[1] = _zh;
    score[2] = _en;
    serialNum = 48;
    name = "Naka";
  }
  int total(void){
    return (score[0] + score[1] + score[2]);
  }
  int serialNum;
  string name;
  void get(void){
    cout << "Kandai no idol,Naka-chan dayo~\n";
  }
  void supply(void){
    cout << "Naka-chan,power up!";
  }
  /*
  dissamble function,but do not uncomment it wwww
  ~student(void){
    return "2-4-11";  //sure,why not
  }
  */
};

int main(void){
  srand(time(NULL));
  //create first student
  student Naka_1(rand()%101,rand()%101,rand()%101);
  //echo data of first student
  cout << "First student Math score: " << Naka_1.score[0] << "\n";
  cout << "First student Chinese score: " << Naka_1.score[1] << "\n";
  cout << "First student English score: " << Naka_1.score[2] << "\n";
  cout << "First student total score: " << Naka_1.total() << "\n";
  //create second student
  student Naka_2(rand()%101,rand()%101,rand()%101);
  //echo data of second student
  cout << "Second student Math score: " << Naka_2.score[0] << "\n";
  cout << "Second student Chinese score: " << Naka_2.score[1] << "\n";
  cout << "Second student English score: " << Naka_2.score[2] << "\n";
  cout << "Second student total score: " << Naka_2.total() << "\n";
  //create third student
  student Naka_3(rand()%101,rand()%101,rand()%101);
  //echo data of third student
  cout << "Third student Math score: " << Naka_3.score[0] << "\n";
  cout << "Third student Chinese score: " << Naka_3.score[1] << "\n";
  cout << "Third student English score: " << Naka_3.score[2] << "\n";
  cout << "Third student total score: " << Naka_3.total() << "\n";
  //calculate average of each subject,and echo
  cout << "Math average score: " << (Naka_1.score[0] + Naka_2.score[0] + Naka_3.score[0]) / 3 << "\n";
  cout << "Chinese average score: " << (Naka_1.score[1] + Naka_2.score[1] + Naka_3.score[1]) / 3 << "\n";
  cout << "English average score: " << (Naka_1.score[2] + Naka_2.score[2] + Naka_3.score[2]) / 3 << "\n";
  return 0;
}
