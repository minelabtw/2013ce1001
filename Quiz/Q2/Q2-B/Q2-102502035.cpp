#include <iostream>//載入函式庫
#include <cstdlib>
#include <ctime>
using namespace std;
int main ()//主程式
{
    srand (time (0));//將種子隨機改變
    double arraya [3][3] = {{rand () %101,rand () %101,rand () %101},{rand () %101,rand () %101,rand () %101},{rand () %101,rand () %101,rand () %101}}; //宣告變數並存入亂數
    cout << "First student Math score: " << arraya [0][0] <<endl;//依題目要求輸出個別成績
    cout << "First student Chinese score: " << arraya [0][1] <<endl;//依題目要求輸出個別成績
    cout << "First student English score: " << arraya [0][2] <<endl;//依題目要求輸出個別成績
    cout << "First student total score: " << arraya [0][0] +arraya [0][1] +arraya [0][2] << endl;//依題目要求輸出總分
    cout << "Second student Math score: " << arraya [1][0] <<endl;//依題目要求輸出個別成績
    cout << "Second student Chinese score: " << arraya [1][1] <<endl;//依題目要求輸出個別成績
    cout << "Second student English score: " << arraya [1][2] <<endl;//依題目要求輸出個別成績
    cout << "Second student total score: " << arraya [1][0] +arraya [1][1] +arraya [1][2] << endl;//依題目要求輸出總分
    cout << "Third student Math score: " << arraya [2][0] <<endl;//依題目要求輸出個別成績
    cout << "Third student Chinese score: " << arraya [2][1] <<endl;//依題目要求輸出個別成績
    cout << "Third student English score: " << arraya [2][2] <<endl;//依題目要求輸出個別成績
    cout << "Third student total score: " << arraya [2][0] +arraya [2][1] +arraya [2][2] << endl;//依題目要求輸出總分
    cout << "Math average score: " << (arraya [0][0] +arraya [1][0] +arraya [2][0])/3 <<endl;//依題目要求輸出平均
    cout << "Chinese average score: " << (arraya [0][1] +arraya [1][1] +arraya [2][1])/3 <<endl;//依題目要求輸出平均
    cout << "English average score: " << (arraya [0][2] +arraya [1][2] +arraya [2][2])/3 <<endl;//依題目要求輸出平均
    return 0;
}
