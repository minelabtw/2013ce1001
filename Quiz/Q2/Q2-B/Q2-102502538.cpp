#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    float math[ 3 ]= {};
    float chinese[ 3 ]= {};
    float english[ 3 ]= {};
    float averagex,averagey,averagez;
    srand(time(NULL));

    for(int x=1; x<=3;)//設定math的亂數
    {
        math[ x ]= rand()%101;
        x++;
    }

    for(int x=1; x<=3;)//設定chinese的亂數
    {
        chinese[ x ]= rand()%101;
        x++;
    }

    for(int x=1; x<=3;)//設定english的亂數
    {
        english[ x ]= rand()%101;
        x++;
    }

    cout<<"First student Math score: "<<math[ 1 ]<<endl;//輸出第一人的成績
    cout<<"First student Chinese score: "<<chinese[ 1 ]<<endl;
    cout<<"First student English score: "<<english[ 1 ]<<endl;
    cout<<"First student Total score: "<<(math[ 1 ]+chinese[ 1 ]+english[ 1 ])<<endl;

    cout<<"Second student Math score: "<<math[ 2 ]<<endl;//輸出第二人的成績
    cout<<"Second student Chinese score: "<<chinese[ 2 ]<<endl;
    cout<<"Second student English score: "<<english[ 2 ]<<endl;
    cout<<"Second student Total score: "<<(math[ 2 ]+chinese[ 2 ]+english[ 2 ])<<endl;

    cout<<"Third student Math score: "<<math[ 3 ]<<endl;//輸出第三人的成績
    cout<<"Third student Chinese score: "<<chinese[ 3 ]<<endl;
    cout<<"Third student English score: "<<english[ 3 ]<<endl;
    cout<<"Third student Total score: "<<(math[ 3 ]+chinese[ 3 ]+english[ 3 ])<<endl;

    averagex = (math[ 1 ]/3+math[ 2 ]/3+math[ 3 ]/3);//設定average的算法
    averagey = (chinese[ 1 ]/3+chinese[ 2 ]/3+chinese[ 3 ]/3);
    averagez = (english[ 1 ]/3+english[ 2 ]/3+english[ 3 ]/3);

    cout<<"Math average score: "<<averagex<<endl;//輸出各科目的average
    cout<<"Chinese average score: "<<averagey<<endl;
    cout<<"English average score: "<<averagez<<endl;

    return 0;
}
