#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int a[3][3]= {0}; //宣告3*3陣列a，初始值為0
    srand(time(NULL)); //依時間決定隨機產生的數字

    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            a[i][j]=(rand()%101); //隨機產生數字，範圍為0~100，填入陣列a
        }
    }

    for(int i=0; i<3; i++)
    {
        int x=0; //宣告整數x，初始值為0
        for(int j=0; j<3; j++)
        {
            switch(i) //判斷是哪個學生
            {
            case 0:
                cout << "First student ";
                break;
            case 1:
                cout << "Second student ";
                break;
            case 2:
                cout << "Third student ";
                break;
            default:
                break;
            }

            switch(j) //判斷是哪個科目
            {
            case 0:
                cout << "Math score: ";
                break;
            case 1:
                cout << "Chinese score: ";
                break;
            case 2:
                cout << "English score: ";
                break;
            default:
                break;
            }

            cout << a[i][j] << endl; //輸出成績
            x=x+a[i][j]; //x增加值為成績
        }

        switch(i) //判斷是哪個學生
        {
        case 0:
            cout << "First student total score: ";
            break;
        case 1:
            cout << "Second student total score: ";
            break;
        case 2:
            cout << "Third student total score: ";
            break;
        default:
            break;
        }
        cout << x << endl; //輸出總成績
    }

    for(int j=0;j<3;j++)
    {
        float y=0; //宣告浮點數y，初始值為0
        for(int i=0;i<3;i++)
    {
        y=y+a[i][j]; //y增加值為成績
    }
        switch(j) //判斷是哪個科目
            {
            case 0:
                cout << "Math average score: ";
                break;
            case 1:
                cout << "Chinese average score: ";
                break;
            case 2:
                cout << "English average score: ";
                break;
            default:
                break;
            }
        cout << y/3 << endl; //輸出平均
    }

    return 0;
}
