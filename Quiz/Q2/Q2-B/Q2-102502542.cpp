#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std ;
int main()
{
    srand(time(0)); //做亂數表
    int rain=0 ; //宣告變數
    const int arraysize=9 ; //宣告陣列大小
    int n[arraysize]= {};

    for(int i=1; i<=9; i++) //i=1進入迴圈
    {
        rain = rand()%101 ; //隨機挑選亂數
        n[i]=rain ; //將得到的亂數值依序儲存
    }
    cout << "First student Math score: " << n[1] << endl ; //輸出數學成績
    cout << "First student Chinese score: " <<  n[2] << endl ; //輸出中文成績
    cout << "First student English score: " << n[3] << endl ; //輸出英文成績
    int a=0 ;
    a=(n[1]+n[2]+n[3]) ; //數學 中文 英文 的平均值
    cout << "First student total score: " << a << endl ; //同上
    cout << "Second student Math score: " << n[4] << endl ;
    cout << "Second student Chinese score: " << n[5] << endl ;
    cout << "Second student English score: " << n[6] << endl ;
    int b=0 ;
    b=(n[4]+n[5]+n[6]) ;
    cout << "Second student total score: " << b << endl ;//同上
    cout << "Third student Math score: " << n[7] << endl ;
    cout << "Third student Chinese score: " << n[8] << endl ;
    cout << "Third student English score: " << n[9] << endl ;
    int c=0 ;
    c=(n[7]+n[8]+n[9]) ;
    cout << "Third student total score: " << c << endl ;
    double d=0 ;
    d= n[1]/3+n[4]/3+n[7]/3 ; //數學平均值
    cout << "Math average score: " << d << endl ;
    double e=0 ;
    e=(n[2]+n[5]+n[8])/3 ; //中文平均值
    cout << "Chinese average score: " << e << endl ;
    double f=0 ;
    f=(n[3]+n[6]+n[9])/3 ; //英文平均值
    cout << "English average score: " << f ;

    return 0 ;

}
