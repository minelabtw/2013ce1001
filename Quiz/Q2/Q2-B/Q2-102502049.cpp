#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    double a[3]= {};
    double b[3]= {};
    double c[3]= {};
    double q; //數學平均
    double w; //國文平均
    double e; //英文平均
    int seed;


    seed=time(0);
    srand(seed); //打亂rand

    for(int i=0; i<3; i++)
    {
        a[i]=rand()%101; //儲存0~100數
    }  //a

    seed=time(0)-40; //-40再次打亂
    srand(seed);
    for(int i=0; i<3; i++)
    {
        b[i]=rand()%101;
    }  //b

    seed=time(0)+30;
    srand(seed);

    for(int i=0; i<3; i++)
    {
        c[i]=rand()%101;
    }  //c


    cout << "First student Math score: " << a[0] << endl;
    cout << "First student Chinese score: " << a[1] << endl;
    cout << "First student English score: " << a[2] << endl;
    cout << "First student total score: " << (a[0]+a[1]+a[2]) <<endl;
    cout << "Second student Math score: " << b[0] << endl;
    cout << "Second student Chinese score: " << b[1] << endl;
    cout << "Second student English score: " << b[2] << endl;
    cout << "Second student total score: " << (b[0]+b[1]+b[2]) <<endl;
    cout << "Third student Math score: " << c[0] << endl;
    cout << "Third student Chinese score: " << c[1] << endl;
    cout << "Third student English score: " << c[2] << endl;
    cout << "Third student total score: " << (c[0]+c[1]+c[2]) <<endl; //輸出成績

    q=(a[0]+b[0]+c[0])/3; //計算平均
    cout << "Math average score: " << q << endl;
    w=(a[1]+b[1]+c[1])/3;
    cout << "Chinese average score: " << w << endl;
    e=(a[2]+b[2]+c[2])/3;
    cout << "English average score: " << e << endl; //輸出平均


    return 0;
}
