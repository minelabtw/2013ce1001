#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstring>

using namespace std;

int main(void) {
    int stu[3][4]; //the scores of students
    int avg[3] = {0, 0, 0}; //the average of subjects
    char *th[3] = {"Fisrt", "Second", "Third"};
    char *sub[4] = {"Math", "Chinese", "English", "total"}; //subjects

    srand(time(NULL)); //initialize random seed

    memset(stu, 0, sizeof(stu));
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 4; j++) {
            if (j < 3) {
                stu[i][j] = rand() / (RAND_MAX / 101 + 1); //random score
                stu[i][3] += stu[i][j];
                avg[j] += stu[i][j];
                }
            /*output student score*/
            cout << th[i] << " student " << sub[j] << " score: " << stu[i][j] << endl;
            }

    for (int i = 0; i < 3; i++) //output average
        cout << sub[i] << " average score: " << (double)avg[i] / 3 << endl;
    return 0;
    }
