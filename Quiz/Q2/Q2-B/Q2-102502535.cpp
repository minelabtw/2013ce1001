#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std ;

int main()
{
    srand ( time(0) ) ;

    int firm = rand () % 101 ;
    int firc = rand () % 101 ;
    int fire = rand () % 101 ;
    int secm = rand () % 101 ;
    int secc = rand () % 101 ;
    int sece = rand () % 101 ;
    int thirm = rand () % 101 ;
    int thirc = rand () % 101 ;
    int thire = rand () % 101 ;  //隨機找九個變數。

    int arr1 [3] = { firm , firc , fire } ;
    int arr2 [3] = { secm , secc , sece } ;
    int arr3 [3] = { thirm , thirc , thire } ;  //將九個隨機變數分別存進陣列中。

    float mathaverage = ( arr1 [0] + arr2 [0] + arr3 [0] ) / 3 ;
    float chiaverage = ( arr1 [1] + arr2 [1] + arr3 [1] ) / 3 ;
    float engaverage = ( arr1 [2] + arr2 [2] + arr3 [2] ) / 3 ;  //設三個浮點數存放三個科目之平均。

    int firtotal = arr1 [0] + arr1 [1] + arr1 [2] ;
    int sectotal = arr2 [0] + arr2 [1] + arr2 [2] ;
    int thirtotal = arr3 [0] + arr3 [1] + arr3 [2] ;  //設三個變數存放三位學生之總成績。

    cout << "First student Math score: " << arr1 [0] << endl ;
    cout << "First student Chinese score: " << arr1 [1] << endl ;
    cout << "First student English score: " << arr1 [2] << endl ;
    cout << "First student total score: " << firtotal << endl ;
    cout << "Second student Math score: " << arr2 [0] << endl ;
    cout << "Second student Chinese score: " << arr2 [1] << endl ;
    cout << "Second student English score: " << arr2 [2] << endl ;
    cout << "Second student total score: " << sectotal << endl ;
    cout << "Third student Math score: " << arr3 [0] << endl ;
    cout << "Third student Chinese score: " << arr3 [1] << endl ;
    cout << "Third student English score: " << arr3 [2] << endl ;
    cout << "Third student total score: " << thirtotal << endl ;
    cout << "Math average score: " << mathaverage << endl ;
    cout << "Chinese average score: " << chiaverage << endl ;
    cout << "English average score: " << engaverage << endl ;  //輸出計算過後之列表。

    return 0 ;
}
