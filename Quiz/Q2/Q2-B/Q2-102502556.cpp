#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main ()
{
    int score[3][3] = {}; //宣告型別為 整數(int) 的二維陣列(score)，用來存放三個學生的三種成績。
    float average1 = 0; //宣告型別為 float 的三個變數(average1,average2,average3)，用來儲存三種成績的平均值。
    float average2 = 0;
    float average3 = 0;
    srand(time(NULL)); //用srand打亂隨機
    for ( int i = 0 ; i < 3 ; i++ ) //用for迴圈使之能產生共 3 * 3 = 9 次隨機(使用rand)，並將其儲存於陣列之中。
    {
        for ( int j = 0 ; j < 3 ; j++ )
        {
            score[i][j] = rand() % 101;
        }
    }
    for ( int k = 0 ; k < 3 ; k++ ) //用for迴圈輸出三個學生的各科成績和總成績。
    {
        if ( k == 0 )
        {
            cout << "First student Math score: " << score[k][0] << endl;
            cout << "First student Chinese score: " << score[k][1] << endl;
            cout << "First student English score: " << score[k][2] << endl;
            cout << "First student total score: " << score[k][0] + score[k][1] + score[k][2] << endl;
        }
        else if ( k == 1 )
        {
            cout << "Second student Math score: " << score[k][0] << endl;
            cout << "Second student Chinese score: " << score[k][1] << endl;
            cout << "Second student English score: " << score[k][2] << endl;
            cout << "Second student total score: " << score[k][0] + score[k][1] + score[k][2] << endl;
        }
        else
        {
            cout << "Third student Math score: " << score[k][0] << endl;
            cout << "Third student Chinese score: " << score[k][1] << endl;
            cout << "Third student English score: " << score[k][2] << endl;
            cout << "Third student total score: " << score[k][0] + score[k][1] + score[k][2] << endl;
        }
    }
    average1 = ( (float)score[0][0] + (float)score[1][0] + (float)score[2][0] ) / 3; //先將各科成績轉換成float型別，再計算各科的平均值，並分別儲存於不同的變數之中。
    average2 = ( (float)score[0][1] + (float)score[1][1] + (float)score[2][1] ) / 3;
    average3 = ( (float)score[0][2] + (float)score[1][2] + (float)score[2][2] ) / 3;
    cout << "Math average score: " << average1 << endl;
    cout << "Chinese average score: " << average2 << endl;
    cout << "English average score: " << average3;
    return 0;
}
