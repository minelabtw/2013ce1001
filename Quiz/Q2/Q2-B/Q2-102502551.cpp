#include <iostream>
#include <stdlib.h>
#include <time.h>


using namespace std;

int main()
{
    int a,i,j,k,time;
    float stu1[3],stu2[3],stu3[3];
    float x,y,z;

    for(i=0; i<4; i++)                                                               //放不同陣列
    {
        if(i==0)
        {
            a=rand();                                                                //隨機
            while(a<0 || a>100)                                                      //判斷是否在範圍內
            {
                a=rand();
            }
            stu1[0]=a;                                                               //放入陣列
            cout<<"First student Math score: "<<stu1[0]<<endl;
        }
        if(i==1)
        {
            a=rand();
            while(a<0 || a>100)
            {
                a=rand();
            }
            stu1[1]=a;
            cout<<"First student Chinese score: "<<stu1[1]<<endl;
        }
        if(i==2)
        {
            a=rand();
            while(a<0 || a>100)
            {
                a=rand();
            }
            stu1[2]=a;
            cout<<"First student English score: "<<stu1[2]<<endl;
        }
        if(i==3)
        {
            cout<<"First student total score: "<<stu1[0]+stu1[1]+stu1[2]<<endl;
        }
    }
    for(i=0; i<4; i++)
    {
        if(i==0)
        {
            a=rand();
            while(a<0 || a>100)
            {
                a=rand();
            }
            stu2[0]=a;
            cout<<"Second student Math score: "<<stu2[0]<<endl;
        }
        if(i==1)
        {
            a=rand();
            while(a<0 || a>100)
            {
                a=rand();
            }
            stu2[1]=a;
            cout<<"Second student Chinese score: "<<stu2[1]<<endl;
        }
        if(i==2)
        {
            a=rand();
            while(a<0 || a>100)
            {
                a=rand();
            }
            stu2[2]=a;
            cout<<"Second student English score: "<<stu2[2]<<endl;
        }
        if(i==3)
        {
            cout<<"Second student total score: "<<stu2[0]+stu2[1]+stu2[2]<<endl;
        }
    }
    for(i=0; i<4; i++)
    {
        if(i==0)
        {
            a=rand();
            while(a<0 || a>100)
            {
                a=rand();
            }
            stu3[0]=a;
            cout<<"Third student Math score: "<<stu3[0]<<endl;
        }
        if(i==1)
        {
            a=rand();
            while(a<0 || a>100)
            {
                a=rand();
            }
            stu3[1]=a;
            cout<<"Third student Chinese score: "<<stu3[1]<<endl;
        }
        if(i==2)
        {
            a=rand();
            while(a<0 || a>100)
            {
                a=rand();
            }
            stu3[2]=a;
            cout<<"Third student English score: "<<stu3[2]<<endl;
        }
        if(i==3)
        {
            cout<<"Third student total score: "<<stu3[0]+stu3[1]+stu3[2]<<endl;
        }
    }
    x=(stu1[0]+stu2[0]+stu3[0])/3;                                                                  //算平均
    y=(stu1[1]+stu2[1]+stu3[1])/3;
    z=(stu1[2]+stu2[2]+stu3[2])/3;
    cout<<"Math average score: "<<x<<endl<<"Chinese average score: "<<y<<endl<<"English average score: "<<z;
    return 0;
}
