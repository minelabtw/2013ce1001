#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;
main()
{
    double data1[ 3 ]= {};  //紀錄同學一
    double data2[ 3 ]= {};  //紀錄同學二
    double data3[ 3 ]= {};  //紀錄同學三

    srand(time(NULL));  //設亂數種子
    for( int i=0; i<=2; i++ )  //存入成績
    {
        data1[ i ]=rand()%101;
    }
    for( int j=0; j<=2; j++ )  //存入成績
    {
        data2[ j ]=rand()%101;
    }
    for( int k=0; k<=2; k++ )  //存入成績
    {
        data3[ k ]=rand()%101;
    }

    double Maverage=(data1[ 0 ]+data2[ 0 ]+data3[ 0 ])/3;  //計算平均
    double Caverage=(data1[ 1 ]+data2[ 1 ]+data3[ 1 ])/3;
    double Eaverage=(data1[ 2 ]+data2[ 2 ]+data3[ 2 ])/3;

    cout << "First student Math score: " << data1[ 0 ] << endl;  //輸出各科成績
    cout << "First student Chinese score: " << data1[ 1 ] << endl;
    cout << "First student English score: " << data1[ 2 ] << endl;
    cout << "First student total score: " << data1[ 0 ]+data1[ 1 ]+data1[ 2 ] << endl;  //輸出總成績
    cout << "Second student Math score: " << data2[ 0 ] << endl;  //輸出各科成績
    cout << "Second student Chinese score: " << data2[ 1 ] << endl;
    cout << "Second student English score: " << data2[ 2 ] << endl;
    cout << "Second student total score: " << data2[ 0 ]+data2[ 1 ]+data2[ 2 ] << endl;  //輸出總成績
    cout << "Third student Math score: " << data3[ 0 ] << endl;  //輸出各科成績
    cout << "Third student Chinese score: " << data3[ 1 ] << endl;
    cout << "Third student English score: " << data3[ 2 ] << endl;
    cout << "Third student total score: " << data3[ 0 ]+data3[ 1 ]+data3[ 2 ] << endl;  //輸出總成績
    cout << "Math average score: " << Maverage << endl;  //輸出平均
    cout << "Chinese average score: " << Caverage << endl;
    cout << "English average score: " << Eaverage;

    return 0;
}
