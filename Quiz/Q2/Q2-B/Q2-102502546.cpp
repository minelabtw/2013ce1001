#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std ;

int main()
{
    double score[9]= {} ; //宣告陣列
    double a ;
    double b ;
    double c ;//宣告平均值

    srand(time(0)) ;//令亂數隨機

    for(int i=1 ; i<=9 ; i++)
    {
        score[i]=rand()%101 ;//亂數值
    }

    cout << "First student Math score: " << score[1] << endl ;
    cout << "First student Chinese score: " << score[2] << endl ;
    cout << "First student English score: " << score[3] << endl ;
    cout << "First student total score: " << score[1]+score[2]+score[3] << endl ;

    cout << "Second student Math score: " << score[4] << endl ;
    cout << "Second student Chinese score: " << score[5] << endl ;
    cout << "Second student English score: " << score[6] << endl ;
    cout << "Second student total score: " << score[4]+score[5]+score[6] << endl ;

    cout << "Third student Math score: " << score[7] << endl ;
    cout << "Third student Chinese score: " << score[8] << endl ;
    cout << "Third student English score: " << score[9] << endl ;
    cout << "Third student total score: " << score[7]+score[8]+score[9] << endl ;//輸出所需要的文字

    a=(score[1]+score[4]+score[7])/3 ;
    b=(score[2]+score[5]+score[8])/3 ;
    c=(score[3]+score[6]+score[9])/3 ;//計算三種平均
    cout << "Math average score: " << a << endl ;
    cout << "Chinese average score: " << b << endl ;
    cout << "English average score: " << c << endl ;//輸出平均值

    return 0 ;

}
