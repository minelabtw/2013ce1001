#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;

int main()
{
    string a[3]={"First","Second","Third"};     //以string存3個字串
    string b[3]={"Math","Chinese","English"};   //以string存3個字串
    srand(time(NULL));                          //使每次數字都是隨機
    int total[3]={};    //宣告int陣列，用來存加總
    int score[3][3]={}; //宣告陣列來存成績
    for(int i=0 ; i<3 ; i++)            //存入成績
        for(int j=0 ; j<3 ; j++)
            score[i][j]=rand()%101;
    for (int i=0 ; i<3 ; i++)           //印出成績，並加總印出
    {
        for (int j=0 ; j<3 ; j++)
        {
            cout << a[i] << " student " << b[i] << "score: " << score[i][j] << endl;
            total[i] += score[i][j];
        }

        cout << a[i] <<" student total score: " << total[i] << endl;
    }
    for(int i=0 ; i<3 ; i++)            //計算平均並印出
        cout << b[i] << " average score: " << (double)total[i]/3 << endl;

    return 0;
}
