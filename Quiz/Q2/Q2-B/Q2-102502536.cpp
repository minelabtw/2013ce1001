#include<iostream>
#include<stdlib.h>
#include<ctime>

using namespace std;

int main()
{
    srand(time(0));  //使輸出不同亂數

    int score[9]= {};      //宣告陣列
    for(int i=0; i<9; i++) //把隨機變數存到陣列中
    {
        score[i]=rand()%101;
    }

    cout << "First student Math score: " << score[0] << endl;  //輸出字串
    cout << "First student Chinese score: " << score[1] << endl;
    cout << "First student English score: " << score[2] << endl;
    cout << "First student toatal score: " << score[0]+score[1]+score[2] << endl;  //輸出個人總成績
    cout << "Second student Math score: " << score[3] << endl;  //輸出字串
    cout << "Second student Chinese score: " << score[4] << endl;
    cout << "Second student English score: " << score[5] << endl;
    cout << "Second student toatal score: " << score[3]+score[4]+score[5] << endl;  //輸出個人總成績
    cout << "Third student Math score: " << score[6] << endl;  //輸出字串
    cout << "Third student Chinese score: " << score[7] << endl;
    cout << "Third student English score: " << score[8] << endl;
    cout << "Third student toatal score: " << score[6]+score[7]+score[8] << endl;  //輸出個人總成績

    double M=score[0]+score[3]+score[6]; //宣告變數並設定初始值
    double C=score[1]+score[4]+score[7];
    double E=score[2]+score[5]+score[8];

    cout << "Math average score: " << M/3 << endl;  //輸出字串
    cout << "Chinese average score: " << C/3 << endl;
    cout << "English average score: " << E/3 << endl;

    return 0;
}
