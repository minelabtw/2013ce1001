#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    double total = 0;   //總成績

    srand( time( NULL ) );  //設定變數種子

    /* 個別宣告變數 & 運算 */

    int score1 = rand() % 101 ;
    cout << "First student Math score: " << score1 << endl;

    int score2 = rand() % 101 ;
    cout << "First student Chinese score: " << score2 << endl;

    int score3 = rand() % 101 ;
    cout << "First student English score: " << score3 << endl;

    cout << "First student total score: " << score1 + score2 + score3 << endl;

    int score4 = rand() % 101 ;
    cout << "Second student Math score: " << score4 << endl;

    int score5 = rand() % 101 ;
    cout << "Second student Chinese score: " << score5 << endl;

    int score6 = rand() % 101 ;
    cout << "Second student English score: " << score6 << endl;

    cout << "Second student total score: " << score4 + score5 + score6 << endl;

    int score7 = rand() % 101 ;
    cout << "Third student Math score: " << score7 << endl;

    int score8 = rand() % 101 ;
    cout << "Third student Chinese score: " << score8 << endl;

    int score9 = rand() % 101 ;
    cout << "Third student English score: " << score9 << endl;

    cout << "Third student total score: " << score7 + score8 + score9 << endl;

    total = score1 + score4 + score7 ;
    cout << "Math average score: " << total / 3 << endl;
    total = score2 + score5 + score8 ;
    cout << "Chinese average score: " << total / 3 << endl;
    total = score3 + score6 + score9 ;
    cout << "English average score: " << total / 3 << endl;



    return 0;
}
