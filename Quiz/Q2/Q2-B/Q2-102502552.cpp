#include<iostream>
#include<stdlib.h>
#include<ctime>
using namespace std;

int main()
{
    double sc[9] = {};//建一個九個元素的array存放成績
    srand(time(0));//設置一個不斷改變的亂數

    for(int element=1;element<=9;element++)
    {
        sc[element] = rand() % 101;
    }//將亂數輸出的成績存入

    cout << "First student Math score:" <<  sc[1] << endl;//第一個學生的數學成績
    cout << "First student Chinese score:" << sc[2]  << endl;//第一個學生的國文成績
    cout << "First student English score:" << sc[3]  << endl;//第一個學生的英文成績
    cout << "First student total score:" << sc[1] + sc[2] + sc[3] << endl;//第一個學生的總成績
    cout << "Second student Math score:" <<  sc[4] << endl;//第二個學生的數學成績
    cout << "Second student Chinese score:" << sc[5]  << endl;//第二個學生的國文成績
    cout << "Second student English score:" << sc[6]  << endl;//第二個學生的英文成績
    cout << "Second student total score:" << sc[4] + sc[5] + sc[6] << endl;//第二個學生的總成績
    cout << "Third student Math score:" <<  sc[7] << endl;//第三個學生的數學成績
    cout << "Third student Chinese score:" << sc[8]  << endl;//第三個學生的國文成績
    cout << "Third student English score:" << sc[9]  << endl;//第三個學生的英文成績
    cout << "Third student total score:" << sc[7] + sc[8] + sc[9] << endl;//第三個學生的總成績
    cout << "Math Average score:" << (sc[1] + sc[4] + sc[7]) / 3 << endl;//數學平均
    cout << "Chinese average score:" << (sc[2] + sc[5] + sc[8]) /3 << endl;//國文平均
    cout << "English average score:" << (sc[3] + sc[6] + sc[9]) / 3;//英文平均

return 0;//回傳
}
