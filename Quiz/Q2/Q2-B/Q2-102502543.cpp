#include <iostream>
#include <ctime>
#include <stdlib.h>
using namespace std;
int main()
{
    srand(time(0)); //隨機變數
    int a; //宣告變數
    float b[3][3]= {}; //宣告陣列大小
    float c=0;
    float d=0;
    float e=0;
    int f=1;
    for (int g=1; g<=3; g++) //將隨機數值儲存到陣列
    {
        while(f<=3)
        {
            a=rand()%101;
            b[g][f]=a;
            f++;
        }
        f=1;
    }
    cout <<"First student Math score: "<<b[1][1]<<endl; //輸出成績
    cout <<"First student Chinese score: "<<b[1][2]<<endl;
    cout <<"First student English score: "<<b[1][3]<<endl;
    cout <<"First student total score: "<<b[1][1]+b[1][2]+b[1][3]<<endl;
    cout <<"Second student Math score: "<<b[2][1]<<endl;
    cout <<"Second student Chinese score: "<<b[2][2]<<endl;
    cout <<"Second student English score: "<<b[2][3]<<endl;
    cout <<"Second student total score: "<<b[2][1]+b[2][2]+b[2][3]<<endl;
    cout <<"Third student Math score: "<<b[3][1]<<endl;
    cout <<"Third student Chinese score: "<<b[3][2]<<endl;
    cout <<"Third student English score: "<<b[3][3]<<endl;
    cout <<"Third student total score: "<<b[3][1]+b[3][2]+b[3][3]<<endl;
    c=(b[1][1]+b[2][1]+b[3][1])/3; //運算平均成績
    cout <<"Math average score: "<<c<<endl;
    d=(b[1][2]+b[2][2]+b[3][2])/3;
    cout <<"Chinese average score: "<<d<<endl;
    e=(b[1][3]+b[2][3]+b[3][3])/3;
    cout <<"English average score: "<<e<<endl;
    return 0;
}
