#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;

int main()
{
    int i=0;
    int j=0;
    int score[3][3];        //score
    float mathavg=0;        //math average score
    float chineseavg=0;     //chinses average score
    float englishavg=0;     //english average score
    srand(time(NULL));      //random number
    for(i=0; i<3; i++)      //gives each array a random score
    {
        for(j=0; j<3; j++)
        {
            score[i][j] = (rand()%100);
        }
    }

    cout << "First sutdent Math score: " << score[0][0] << "\n";
    cout << "First student Chinese score: " << score[0][1] << "\n";
    cout << "First student English score: " << score[0][2] << "\n";
    cout << "First student total score: " << score[0][0]+score[0][1]+score[0][2] << "\n"; //first student

    cout << "Second sutdent Math score: " << score[1][0] << "\n";
    cout << "Second student Chinese score: " << score[1][1] << "\n";
    cout << "Second student English score: " << score[1][2] << "\n";
    cout << "Second student total score: " << score[1][0]+score[1][1]+score[1][2] << "\n";//second student

    cout << "Third sutdent Math score: " << score[2][0] << "\n";
    cout << "Third student Chinese score: " << score[2][1] << "\n";
    cout << "Third student English score: " << score[2][2] << "\n";
    cout << "Third student total score: " << score[2][0]+score[2][1]+score[2][2] << "\n"; //third student

    //average scores math - chinese - english
    mathavg = score[0][0]+score[1][0]+score[2][0];
    mathavg = mathavg/3;
    cout << "Math average score: " << mathavg << "\n";
    chineseavg = score[0][1]+score[1][1]+score[2][1];
    chineseavg = chineseavg/3;
    cout << "Chinese average score: " << chineseavg << "\n";

    englishavg = score[0][2]+score[1][2]+score[2][2];
    englishavg = englishavg/3;
    cout << "English average score: " << englishavg << "\n";

    return 0;
}

