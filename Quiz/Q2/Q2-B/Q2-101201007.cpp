#include<iostream>
#include<ctime>
#include<cstdlib>

using namespace std;


main()
{
    int srand( time(0) );            //以TIME(0)做為亂數種子

    int Mathdata[3];
    int Chinesedata[3];              //宣告三個陣列 以及儲存空間
    int Englishdata[3];





    int Firstmath=rand()%101;       //產生成績，介於0~100的隨機數
    int Firstch=rand()%101;
    int Firsteng=rand()%101;
    int Secmath=rand()%101;
    int Secch=rand()%101;
    int Seceng=rand()%101;
    int Thirdmath=rand()%101;
    int Thirdch=rand()%101;
    int Thirdeng=rand()%101;

    Mathdata[0]=Firstmath;                                          //把隨機產生的變數儲存到宣告的陣列中
    Chinesedata[0]=Firstch;
    Englishdata[0]=Firsteng;
    Mathdata[1]=Secmath;
    Chinesedata[1]=Secch;
    Englishdata[1]=Seceng;
    Mathdata[2]=Thirdmath;
    Chinesedata[2]=Thirdch;
    Englishdata[2]=Thirdeng;

    cout <<"First student Math score: "<<Firstmath<<endl;                     //輸出第一位同學的各科成績
    cout <<"First student Chinese score: "<<Firstch<<endl;
    cout <<"First student English score: "<<Firsteng<<endl;
    cout <<"First student total score: "<<Firstmath+Firstch+Firsteng<<endl;   //輸出第一位同學的個人總成績

    cout <<"Second student Math score: "<<Secmath<<endl;                      //輸出第二位同學的各科成績
    cout <<"Second student Chinese score: "<<Secch<<endl;
    cout <<"Second student English score: "<<Seceng<<endl;
    cout <<"Second student total score: "<<Secmath+Secch+Seceng<<endl;        //輸出第二位同學的個人總成績

    cout <<"Third student Math score: "<<Thirdmath<<endl;                     //輸出第三位同學的各科成績
    cout <<"Third student Chinese score: "<<Thirdch<<endl;
    cout <<"Third student English score: "<<Thirdeng<<endl;
    cout <<"Third student total score: "<<Thirdmath+Thirdch+Thirdeng<<endl;   //輸出第三位同學的個人總成績




    cout <<"Math average score: "<<(Firstmath+Secmath+Thirdmath)/3<<endl;    //計算並輸出三位同學的數學成績平均
    cout <<"Chinese average score: "<<(Firstch+Secch+Thirdch)/3<<endl;       //計算並輸出三位同學的國文成績平均
    cout <<"English average score: "<<(Firsteng+Seceng+Thirdeng)/3<<endl;    //計算並輸出三位同學的英文成績平均




    return 0;
}
