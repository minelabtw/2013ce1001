#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std ;

double a[3] ,b[3],c[3] ;  //設定陣列

int main ()
{
    srand(time(0)) ;    //亂數
    a[0]= rand() %101 ;   //放入陣列
    a[1]= rand() %101 ;
    a[2]= rand() %101 ;
    b[0]= rand() %101 ;
    b[1]= rand() %101 ;
    b[2]= rand() %101 ;
    c[0]= rand() %101 ;
    c[1]= rand() %101 ;
    c[2]= rand() %101 ;

    cout<<"First student Math score: " << a[0] <<endl;     //輸出分數
    cout<<"First student Chinese score: "<< a[1]<<endl;
    cout<<"First student English score: "<< a[2]<<endl;
    cout<<"First student total score: "<< ( a[0]+a[1]+a[2] ) <<endl;

    cout<<"Second student Math score: "<<b[0]<<endl;
    cout<<"Second student Chinese score: "<<b[1]<<endl;
    cout<<"Second student English score: "<<b[2]<<endl;
    cout<<"Second student total score: "<<( b[0]+b[1]+b[2] ) <<endl;

    cout<<"Third student Math score: "<<c[0]<<endl;
    cout<<"Third student Chinese score: "<<c[1]<<endl;
    cout<<"Third student English score: "<<c[2]<<endl;
    cout<<"Third student total score: "<<( c[0]+c[1]+c[2] ) <<endl;

    cout<<"Math average score: "<< ( a[0]+b[0]+c[0] )/3  <<endl;   //平均分數
    cout<<"Chinese average score: "<<( a[1]+b[1]+c[1] )/3  <<endl;
    cout<<"English average score: "<<( a[2]+b[2]+c[2] )/3  <<endl;

    return 0 ;
}
