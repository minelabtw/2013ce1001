#include<iostream>
#include<cstdlib>                          //包含rand srand
#include<ctime>                              //包含 time

using namespace std;

int main()
{
    srand(time(0));

    float showscore[9] = {};                          //陣列 9個          int不行

    for(int n =1; n<=9; n++)
    {
        showscore[n] = rand()%101;                         //儲存資料
    }

    cout<<"First student Math score: "<<showscore[1]<<"\nFirst student Chinese score: "<<showscore[2]<<"\nFirst student English score: "<<showscore[3];        //顯示
    cout<<"\nFirst student total score: "<<showscore[1]+showscore[2]+showscore[3];               //顯示總和

    cout<<"\nSecond student Math score: "<<showscore[4]<<"\nSecond student Chinese score: "<<showscore[5]<<"\nSecond student English score: "<<showscore[6];        //顯示
    cout<<"\nSecond student total score: "<<showscore[4]+showscore[5]+showscore[6];

    cout<<"\nThird student Math score: "<<showscore[7]<<"\nThird student Chinese score: "<<showscore[8]<<"\nThird student English score: "<<showscore[9];         //顯示
    cout<<"\nThird student total score: "<<showscore[7]+showscore[8]+showscore[9];

    cout<<"\nMath average score: "<<(showscore[1]+showscore[4]+showscore[7])/3;                //用 int 無法顯示小數
    cout<<"\nChinese average score: "<<(showscore[2]+showscore[5]+showscore[8])/3;
    cout<<"\nEnglish average score: "<<(showscore[3]+showscore[6]+showscore[9])/3;

    return 0;
}
