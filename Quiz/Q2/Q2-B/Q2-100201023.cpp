#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

int main()
{
    double score[4][4];
    double temp;
    string number[3] = {"First" , "Second" , "Third"};
    string type[4] = {"Math" , "Chinese" , "English" , "total"};

    srand(time(NULL));

    for(int i = 0 ; i < 3 ; ++i)
    {
        temp = 0;
        for(int j = 0 ; j < 3 ; ++j)
        {
            score[i][j] = rand() % 101; // generate random score
            temp += score[i][j]; // compute total score
        }
        score[i][3] = temp;
    }

    for(int j = 0 ; j < 3 ; ++j)
    {
        temp = 0;
        for(int i = 0 ; i < 3 ; ++i)
            temp += score[i][j]; // compute averge score
        score[3][j] = temp / 3;
    }

    for(int i = 0 ; i < 3 ; ++i) // output student score
    {
        for(int j = 0 ; j < 4 ; ++j)
            cout << number[i] << " student " << type[j] << " score: " << score[i][j] << endl;
    }

    for(int i = 0 ; i < 3 ; ++i) // output averge score
        cout << type[i] << " averge score: " << score[3][i] << endl;

    return 0;
}
