#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(NULL));//使變數能夠確實隨機產生
    int a1 = 0+rand()%100;//隨機產生範圍0~100的變數
    int a2 = 0+rand()%100;
    int a3 = 0+rand()%100;
    int b1 = 0+rand()%100;
    int b2 = 0+rand()%100;
    int b3 = 0+rand()%100;
    int c1 = 0+rand()%100;
    int c2 = 0+rand()%100;
    int c3 = 0+rand()%100;
    double math[3]= {a1,b1,c1};//將大括弧內的三個變數存入陣列中
    double chinese[3]= {a2,b2,c2};
    double english[3]= {a3,b3,c3};
    double av1=(math[1]+math[2]+math[3])/3;//將陣列的第1,2,3項除以3後的值給另一個變數
    double av2=(chinese[1]+chinese[2]+chinese[3])/3;
    double av3=(english[1]+english[2]+english[3])/3;
    cout<<"First student Math score: "<<math[1]<<endl;//顯示此陣列中的第1項
    cout<<"First student Chinese score: "<<chinese[1]<<endl;
    cout<<"First student English score: "<<english[1]<<endl;
    cout<<"First student total score: "<<math[1]+chinese[1]+english[1]<<endl;

    cout<<"Second student Math score: "<<math[2]<<endl;
    cout<<"Second student Chinese score: "<<chinese[2]<<endl;
    cout<<"Second student English score: "<<english[2]<<endl;
    cout<<"Second student total score: "<<math[2]+chinese[2]+english[2]<<endl;

    cout<<"Third student Math score: "<<math[3]<<endl;
    cout<<"Third student Chinese score: "<<chinese[3]<<endl;
    cout<<"Third student English score: "<<english[3]<<endl;
    cout<<"Third student total score: "<<math[3]+chinese[3]+english[3]<<endl;

    cout<<"Math average score: "<<av1<<endl;//顯示運算後變數的值
    cout<<"Chinese average score: "<<av2<<endl;
    cout<<"English average score: "<<av3<<endl;

    return 0;
}
