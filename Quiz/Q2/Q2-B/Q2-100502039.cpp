#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main(){

    srand(time(0));

    int Student1[3];
    int Student2[3];
    int Student3[3];

    for(int i=0; i++; i<3){
        Student1[i] = (rand()%101);
    }

    for(int j=0; j++; j<3){
        Student2[j] = (rand()%101);
    }

    for(int k=0; k++; k<3){
        Student3[k] = (rand()%101);
    }

    cout << "First student Math score: " << Student1[0] << endl;
    cout << "First student Chinese score: " << Student1[1] << endl;
    cout << "First student English score: " << Student1[2] << endl;
    cout << "First student total score: " << Student1[0]+Student1[1]+Student1[2] << endl;

    cout << "Second student Math score: " << Student2[0] << endl;
    cout << "Second student Chinese score: " << Student2[1] << endl;
    cout << "Second student English score: " << Student2[2] << endl;
    cout << "Second student total score: " << Student2[0]+Student2[1]+Student2[2] << endl;

    cout << "Third student Math score: " << Student3[0] << endl;
    cout << "Third student Chinese score: " << Student3[1] << endl;
    cout << "Third student English score: " << Student3[2] << endl;
    cout << "Third student total score: " << Student3[0]+Student3[1]+Student3[2] << endl;

    cout << "Math average score: " << (Student1[0]+Student2[0]+Student3[0])/3 << endl;
    cout << "Chinese average score: " << (Student1[1]+Student2[1]+Student3[1])/3 << endl;
    cout << "English average score: " << (Student1[2]+Student2[2]+Student3[2])/3 << endl;

    return 0;
}
