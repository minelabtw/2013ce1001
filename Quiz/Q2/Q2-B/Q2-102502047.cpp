#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    double a[3]= {},b[2]= {},c[2]= {};//宣告3個陣列
    srand(time(0));//每次隨機輸入的分數都不同
    for(int i=0; i<=2; i++)//把分數填入陣列
        a[i]=rand()%101;
    for(int i=0; i<=2; i++)
        b[i]=rand()%101;
    for(int i=0; i<=2; i++)
        c[i]=rand()%101;
    cout<<"First student Math score: "<<a[0]<<endl;//輸出陣列的分數
    cout<<"First student Chinese score: "<<a[1]<<endl;
    cout<<"First student English score: "<<a[2]<<endl;
    cout<<"First student total score: "<<a[0]+a[1]+a[2]<<endl;
    cout<<"Second student Math score: "<<b[0]<<endl;
    cout<<"Second student Chinese score: "<<b[1]<<endl;
    cout<<"Second student English score: "<<b[2]<<endl;
    cout<<"Second student total score: "<<b[0]+b[1]+b[2]<<endl;
    cout<<"Third student Math score: "<<c[0]<<endl;
    cout<<"Third student Chinese score: "<<c[1]<<endl;
    cout<<"Third student English score: "<<c[2]<<endl;
    cout<<"Third student total score: "<<c[0]+c[1]+c[2]<<endl;

    double d[3]= { (a[0]+b[0]+c[0])/3,(a[1]+b[1]+c[1])/3,(a[2]+b[2]+c[2])/3  };//各科平均陣列
    cout<<"Math average score: "<<d[0]<<endl;//輸出平均
    cout<<"Chinese average score: "<<d[1]<<endl;
    cout<<"English average score: "<<d[2]<<endl;

    for(int j=0; j<=2; j++)//以二維陣列表示
    {
        cout<<a[j]<<"  "<<b[j]<<"  "<<c[j]<<"  "<<d[j]<<endl;
    }
    cout<<a[0]+a[1]+a[2]<<" "<<b[0]+b[1]+b[2]<<" "<<c[0]+c[1]+c[2];
    return 0;
}
