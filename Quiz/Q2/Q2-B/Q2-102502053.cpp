#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    //call variables
    int maths; // for maths score
    int chi; // for chinese score
    int eng; //for english score
    double mavg; //average for maths
    double cavg; //average for chinese
    double eavg; //average for english
    int total; //for total
    srand(time(0)); //set null according to time change

    //calculate for the 1st student
    maths=rand()%101; //random for score
    chi=rand()%101; //random for score
    eng=rand()%101; //random for score
    cout<<"First student Math score: "<<maths<<endl;
    cout<<"First student Chinese score: "<<chi<<endl;
    cout<<"First student English score: "<<eng<<endl;
    total=maths+chi+eng; // add to total
    cout<<"First student total score: "<<total<<endl;
    double first[3]= {maths,chi,eng}; //call and save in a array

    //calculate for the 2nd student
    maths=rand()%101;//random for score
    chi=rand()%101;//random for score
    eng=rand()%101;//random for score
    cout<<"Second student Math score: "<<maths<<endl;
    cout<<"Second student Chinese score: "<<chi<<endl;
    cout<<"Second student English score: "<<eng<<endl;
    total=maths+chi+eng;// add to total
    cout<<"Second student total score: "<<total<<endl;
    double second[3]= {maths,chi,eng}; //call and save in a array

    //calculate for the 3rd student
    maths=rand()%101;//random for score
    chi=rand()%101;//random for score
    eng=rand()%101;//random for score
    cout<<"Third student Math score: "<<maths<<endl;
    cout<<"Third student Chinese score: "<<chi<<endl;
    cout<<"Third student English score: "<<eng<<endl;
    total=maths+chi+eng;// add to total
    cout<<"Third student total score: "<<total<<endl;
    double third[3]= {maths,chi,eng}; //call and save in a array

    //calculate the average
    mavg=(first[0]+second[0]+third[0])/3;
    cavg=(first[1]+second[1]+third[1])/3;
    eavg=(first[2]+second[2]+third[2])/3;

    //display averages
    cout<<"Math average score: "<<mavg<<endl;
    cout<<"Chinese average score: "<<cavg<<endl;
    cout<<"English average score: "<<eavg<<endl;

    return 0;
}
