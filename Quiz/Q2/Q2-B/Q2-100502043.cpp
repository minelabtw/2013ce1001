#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main()
{
    float result1[4],result2[4],result3[4];  //宣告3陣列儲存3位學生資料
    float Mas=0,Cas=0,Eas=0;

    srand(time(0));  //以time(0)做seed

    result1[1]=rand()%101;  //將隨機亂數儲存在陣列中
    result1[2]=rand()%101;
    result1[3]=rand()%101;
    result1[4]=result1[1]+result1[2]+result1[3];  //陣列第4元素用於儲存前3者之總和
    result2[1]=rand()%101;
    result2[2]=rand()%101;
    result2[3]=rand()%101;
    result2[4]=result2[1]+result2[2]+result2[3];
    result3[1]=rand()%101;
    result3[2]=rand()%101;
    result3[3]=rand()%101;
    result3[4]=result3[1]+result3[2]+result3[3];

    cout << "First studnt Math score; " << result1[1] << endl;  //輸出第一位學生各項資料
    cout << "First studnt Chinese score; " << result1[2] << endl;
    cout << "First studnt English score; " << result1[3] << endl;
    cout << "First studnt total score; " << result1[4] << endl;

    cout << "Second studnt Math score; " << result2[1] << endl;  //輸出第二位學生各項資料
    cout << "Second studnt Chinese score; " << result2[2] << endl;
    cout << "Second studnt English score; " << result2[3] << endl;
    cout << "Second studnt total score; " << result2[4] << endl;

    cout << "Third studnt Math score; " << result3[1] << endl;  //輸出第三位學生各項資料
    cout << "Third studnt Chinese score; " << result3[2] << endl;
    cout << "Third studnt English score; " << result3[3] << endl;
    cout << "Third studnt total score; " << result3[4] << endl;

    Mas=(result1[1]+result2[1]+result3[1])/3;  //定義數學平均
    Cas=(result1[2]+result2[2]+result3[2])/3;  //定義中文平均
    Eas=(result1[3]+result2[3]+result3[3])/3;  //定義英文平均

    cout << "Math average score: " << Mas << endl;  //輸出各項平均
    cout << "Chinese average score: " << Cas << endl;
    cout << "English average score: " << Eas << endl;

    return 0;
}
