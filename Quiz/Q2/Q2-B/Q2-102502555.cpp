#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int main(){
    int arraysize = 3;  //宣告陣列
    double score[arraysize][arraysize];
    double ma = 0;
    double ca = 0;
    double ea = 0;

    srand(time(0));  //設定亂數

    for(int j = 0 ; j < arraysize ; j++){  //存進陣列
        for(int i = 0 ; i < arraysize ; i++){
            score[j][i] = rand() % 101;
        }
    }


    cout << "First student Math score: " << score[0][0] << endl;  //輸出成績
    cout << "First student Chinese score: " << score[0][1] << endl;
    cout << "First student English score: " << score[0][2] << endl;
    cout << "First student total score: " << score[0][0] + score[0][1] + score[0][2] << endl;
    cout << "Second student Math score: " << score[1][0] << endl;
    cout << "Second student Chinese score: " << score[1][1] << endl;
    cout << "Second student English score: " << score[1][2] << endl;
    cout << "Second student total score: " << score[1][0] + score[1][1] + score[1][2] << endl;
    cout << "Third student Math score: " << score[2][0] << endl;
    cout << "Third student Chinese score: " << score[2][1] << endl;
    cout << "Third student English score: " << score[2][2] << endl;
    cout << "Third student total score: " << score[2][0] + score[2][1] + score[2][2] << endl;
    ma = score[0][0] + score[1][0] + score[2][0];
    ca = score[0][1] + score[1][1] + score[2][1];
    ea = score[0][2] + score[1][2] + score[2][2];
    cout << "Math average score: ";
    cout << ma / 3 << endl;
    cout << "Chinese average score: ";
    cout << ca / 3<< endl;
    cout << "English average score: ";
    cout << ea / 3;

    return 0;
}

