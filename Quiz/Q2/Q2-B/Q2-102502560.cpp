#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int sum(const int numbers[], int num);	//prototype

int main()
{
	string name[]={"First","Second","Third"};		//names of students
	string subject[]={"Math","Chinese","English"};	//names of subject

	srand(time(NULL));								//use time as seed of random

	int grade[3][3]={};								//2-dimension array for grades[studentid][subjectid]
	int subjecttotal[3]={};							//array storing total for each subjects

	for(int studentid=0;studentid<=2;studentid++){		//for each students
		for(int subjectid=0;subjectid<=2;subjectid++){		//for this student's each subjects
			grade[studentid][subjectid]=rand()%101;					//insert random score
			subjecttotal[subjectid]+=grade[studentid][subjectid];	//add this score to respective total
			cout << name[studentid] << " student " << subject[subjectid] << " score: " << grade[studentid][subjectid] << "\n";	//print this score
		}
		cout << name[studentid] << " student " << "total" << " score: " << sum(grade[studentid],3) << "\n";	//print total score
	}

	for(int subjectid=0;subjectid<=2;subjectid++){
		cout << subject[subjectid] << " average score: " << (double)subjecttotal[subjectid]/3 << "\n";	//print average score for each subjects
	}

	return 0;
}

int sum(const int numbers[], int num)	//a function that sum up an array
{
	int total=0;
	for(int i=0;i<=num-1;i++){total+=numbers[i];}
	return total;
}
