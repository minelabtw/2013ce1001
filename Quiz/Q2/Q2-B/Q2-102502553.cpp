#include<iostream>
#include<ctime>//使用time
#include<cstdlib>//使用rand和srand
using namespace std;
int main()
{
    srand(time(0));//隨機變數
    double Mathav=0;
    double Chineseav=0;
    double Englishav=0;
    int times=8;
    int s[8]= {};//陣列存放9個整數
    for(int input=0; input<=times; input++)
    {
        int score=rand()%101;//score為0到100區間
        s[input]=score;//存入score
    }
    Mathav=(s[0]+s[3]+s[6])*0.3333;
    Chineseav=(s[1]+s[4]+s[7])*0.3333;
    Englishav=(s[2]+s[5]+s[8])*0.3333;
    cout<<"First student Math score: "<<s[0]<<endl;
    cout<<"First student Chinese score: "<<s[1]<<endl;
    cout<<"First student English score: "<<s[2]<<endl;
    cout<<"First student total score: "<<s[0]+s[1]+s[2]<<endl;
    cout<<"Second student Math score: "<<s[3]<<endl;
    cout<<"Second student Chinese score: "<<s[4]<<endl;
    cout<<"Second student English score: "<<s[5]<<endl;
    cout<<"Second student total score: "<<s[3]+s[4]+s[5]<<endl;
    cout<<"Third student Math score: "<<s[6]<<endl;
    cout<<"Third student Chinese score: "<<s[7]<<endl;
    cout<<"Third student English score: "<<s[8]<<endl;
    cout<<"Third student total score: "<<s[6]+s[7]+s[8]<<endl;
    cout<<"Math average score: "<<Mathav<<endl;
    cout<<"Chinese average score: "<<Chineseav<<endl;
    cout<<"English average score: "<<Englishav<<endl;

    return 0;
}
