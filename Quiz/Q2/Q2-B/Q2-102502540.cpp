#include <iostream>
#include <cstdlib> //contain srand() and rand()
#include <ctime> //contain time()
using namespace std;

int main()
{
    srand(time(0)); //隨機變數
    int randnum=rand()%101; //宣告變數a的範圍為0到100
    float a[3]= {}; //宣告陣列
    float b[3]= {};
    float c[3]= {};
    float MAS=0;
    float CAS=0;
    float EAS=0;


    //顯示第一位學生的各科成績和總成績
    cout << "First student Math score: " << randnum << endl;
    a[1]=randnum; //使randnum值儲存進a[1]
    randnum=rand()%101; //換randnum值
    cout << "First student Chinese score: " << randnum << endl;
    a[2]=randnum;
    randnum=rand()%101;
    cout << "First student English score: " << randnum << endl;
    a[3]=randnum;
    cout << "First student total score: " << a[1]+a[2]+a[3] << endl;
    randnum=rand()%101;
    //顯示第二位學生的各科成績和總成績
    cout << "Second student Math score: " << randnum << endl;
    b[1]=randnum;
    randnum=rand()%101;
    cout << "Second student Chinese score: " << randnum << endl;
    b[2]=randnum;
    randnum=rand()%101;
    cout << "Second student English score: " << randnum << endl;
    b[3]=randnum;
    //顯示第三位學生的各科成績和總成績
    cout << "Sceond student total score: " << b[1]+b[2]+b[3] << endl;
    randnum=rand()%101;
    cout << "Third student Math score: " << randnum << endl;
    c[1]=randnum;
    randnum=rand()%101;
    cout << "Third student Chinese score: " << randnum << endl;
    c[2]=randnum;
    randnum=rand()%101;
    cout << "Third student English score: " << randnum << endl;
    c[3]=randnum;
    cout << "Third student total score: " << c[1]+c[2]+c[3] << endl;

    MAS=(a[1]+b[1]+c[1])/3; //計算數學平均
    cout << "Math average score: " << MAS << endl;
    CAS=(a[2]+b[2]+c[2])/3;
    cout << "Chinese average score: " << CAS << endl;
    EAS=(a[3]+b[3]+c[3])/3;
    cout << "English average score: " << EAS << endl;

    return 0;
}
