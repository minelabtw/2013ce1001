#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    srand(time(0));//宣告亂數由時間產生
    double A[4]={},B[4]={},C[4]={};//宣告陣列A,B,C分別記錄三位同學的數學、國文、英文、總成績
    for(int i=0;i<3;i++)//用亂數隨機產生0~100三人的國英數成績並計算出個人的總分
    {
        A[i]=rand()%101;
        B[i]=rand()%101;
        C[i]=rand()%101;
        A[3]+=A[i];
        B[3]+=B[i];
        C[3]+=C[i];
    }
    for(int j=1;j<=3;j++)//讓輸出的成績從第一位到第三位學生
    {
        for(int k=0;k<=3;k++)//讓輸出的成績順序為Math,Chinese,English,total
        {
            if(j==1)
            {
                cout << "First student ";
                if(k==0)
                    cout << "Math score: ";
                if(k==1)
                    cout << "Chinese score: ";
                if(k==2)
                    cout << "English score: ";
                if(k==3)
                    cout << "total score: ";
                cout << A[k] << "\n";
            }
            if(j==2)
            {
                cout << "Second student ";
                if(k==0)
                    cout << "Math score: ";
                if(k==1)
                    cout << "Chinese score: ";
                if(k==2)
                    cout << "English score: ";
                if(k==3)
                    cout << "total score: ";
                cout << B[k] << "\n";
            }
            if(j==3)
            {
                cout << "Third student ";
                if(k==0)
                    cout << "Math score: ";
                if(k==1)
                    cout << "Chinese score: ";
                if(k==2)
                    cout << "English score: ";
                if(k==3)
                    cout << "total score: ";
                cout << C[k] << "\n";
            }
        }
    }
    cout << "Math average score: " << (A[0]+B[0]+C[0])/3;//輸出數學三人平均
    cout << "\nChinese average score: " << (A[1]+B[1]+C[1])/3;//輸出國文三人平均
    cout << "\nEnglish average score: " << (A[2]+B[2]+C[2])/3;//輸出英文三人平均

    return 0;
}
