#include <iostream>

using namespace std;

int main()
{
    int num1,num2,tmp,mult;                          //num1,num2為兩個數字  tmp交換用  mult存放num1*num2

    do                                               //輸入num1直到在正確範圍
    {
        cout<<"Please enter first number( >0 ): ";
        cin>>num1;
        if(num1<=0)
            cout<<"Out of range!\n";
    }while(num1<=0);
    do                                               //輸入num2直到在正確範圍
    {
        cout<<"Please enter second number( >0 ): ";
        cin>>num2;
        if(num2<=0)
            cout<<"Out of range!\n";
    }while(num2<=0);

    mult=num1*num2;                                  //mult存放num1*num2

    while(num1%num2!=0)                              //輾轉相除法
    {
        tmp=num1%num2;
        num1=num2;
        num2=tmp;

    }

    cout<<"The greatest common divisor: "<<num2<<endl;   //輸出結果
    cout<<"The least common multiple: "<<mult/num2;


    return 0;
}
