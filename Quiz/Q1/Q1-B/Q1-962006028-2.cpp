#include<iostream>
using namespace std;
int main()
{
    int a=0;
    int b=0;
    int c=0;
    int d=0;
    cout << "How much money: " ;
    cin >> a ;
    while (a<0)
    {
        cout << "Out of range!" <<endl ;
        cout << "How much money: " ;
        cin >> a ;
    }

    b=a/1000;
    cout << "1000: " << b << endl ;
    a=a-1000*b;

    b=a/500;
    cout << "500: " << b << endl ;
    a=a-500*b;

    b=a/100;
    cout << "100: " << b << endl ;
    a=a-100*b;

    b=a/50;
    cout << "50: " << b << endl ;
    a=a-50*b;

    b=a/10;
    cout << "10: " << b << endl ;
    a=a-10*b;

    b=a/5;
    cout << "5: " << b << endl ;
    a=a-5*b;

    b=a/1;
    cout << "1: " << b << endl ;
    a=a-1*b;

    return 0 ;
}

