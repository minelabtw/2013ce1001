#include<iostream>
using namespace std;

int main()
{
    int number1=0,number2=0,
        n1,n2,i,gcd;
    while(number1<=0)
    {
        cout << "Please enter first number( >0 ): ";
        cin >> number1;
        if(number1<=0)
            cout << "Out of range! \n";
    }
    while(number2<=0)
    {
        cout << "Please enter second number( >0 ): ";
        cin >> number2;
        if(number2<=0)
            cout << "Out of range! \n";
    }
    n1=number1;
    n2=number2;
    while(i>0)
    {
        if(n1%n2==0)
        {
            gcd=n2;
            i=0;
        }
        if(n2%n1==0)
        {
            gcd=n1;
            i=0;
        }
        if(n1>n2)
        {
            n1=n1-n2;
            i=1;
        }
        if(n2>n1)
        {
            n2=n2-n1;
            i=2;
        }
    }

    cout << "The greatest common divisor: " << gcd << "\n";
    cout << "The least common multiple: " << number1*number2/gcd;

    return 0;
}
