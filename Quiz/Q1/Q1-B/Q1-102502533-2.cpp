#include<iostream>
using namespace std;

int main()
{
    int money;//宣告要找金額

    cout<<"How much money:";//輸入要找金額
    cin>>money;

    while( money <= 0)//限制要找金額只能大於0,否則重來
    {
        cout<<"Out of range!"<<endl;
        cout<<"How much money:";
        cin>>money;
    }

    int a=0,b=0,c=0,d=0,e=0,f=0,g=0;//宣告五個整數
    a=money/1000;//a=1000的面額
    b=(money-(1000*a))/500;//b=500的面額
    c=(money-(1000*a+500*b))/100;//c=100的面額
    d=(money-(1000*a+500*b+100*c))/50;//d=50的面額
    e=(money-(1000*a+500*b+100*c+50*d))/10;//e=10的面額
    f=(money-(1000*a+500*b+100*c+50*d+10*e))/5;//f=5的面額
    g=(money-(1000*a+500*b+100*c+50*d+10*e+5*f));//g=1的面額

    cout<<"1000: "<<a<<endl;
    cout<<"500: "<<b<<endl;
    cout<<"100: "<<c<<endl;
    cout<<"50: "<<d<<endl;
    cout<<"10: "<<e<<endl;
    cout<<"5: "<<f<<endl;
    cout<<"1: "<<g<<endl;



    return 0;
}
