#include <iostream>

using namespace std;

int main(void) {
    int num_1, num_2; //declare number_1 and number_2
    int tmp_1, tmp_2; //declare two number store number 1&2

    //input num_1
    do {
        cout << "Please enter first number( >0 ): ";
        cin >> num_1 ;
        } while ((num_1 <= 0) && (cout << "Out of range!\n"));

    //input num_2
    do {
        cout << "Please enter second number( >0 ): ";
        cin >> num_2;
        } while ((num_2 <= 0) && (cout << "Out of range!\n"));

    tmp_1 = num_1;
    tmp_2 = num_2;

    // find gcd(tmp_1, tmp_2);
    while ((tmp_1 %= tmp_2) && (tmp_2 %= tmp_1));

    cout << "The greatest common divisor: ";
    cout << (tmp_1 + tmp_2) << endl;

    cout << "The least common multiple: ";
    //note: avoid num_1 * num_2 overflow
    cout << (num_1 / (tmp_1 + tmp_2)) * num_2 << endl;

    return 0;
    }
