#include <iostream>

using namespace std ;

int main ()

{

    int first = 0 ;  //設定一變數，其名稱為first，並使其初始值為0。
    int second = 0 ;  //設定一變數，其名稱為second，並使其初始值為0。

    cout << "Please enter first number( >0 ): " ;  //輸出字串。
    cin >> first ;  //使操作者輸入第一個數字，其值丟給first。

    while (first <= 0)  //當first的值不大於0，就進入迴圈。
    {
        cout << "Out of range!\n" << "Please enter first number( >0 ): " ;  //輸出"超出範圍"，並要求操作者重新輸入。
        cin >> first ;  //將重新輸入之值丟給first。
    }

    cout << "Please enter second number( >0 ): " ;  //輸出字串。
    cin >> second ;  //使操作者輸入第二個數字，其值丟給second。

    while (second <= 0)  //當second的值不大於0，就進入迴圈。
    {
        cout << "Out of range!\n" << "Please enter second number( >0 ): " ;  //輸出"超出範圍"，並要求操作者重新輸入。
        cin >> second ;  //將重新輸入之值丟給second。
    }

    int gcd ;  //設一變數。

    while ( first != second )
    {
        if ( first > second )
            first = first - second ;
        else
            second = second - first ;
    }  //進入迴圈，使用輾轉相除法求最大公因數。
    if ( first == second )
        gcd = first ;  //當兩數字相同，gcd即為其數值。

    int lcm = first * second / gcd ;


    cout << "The greatest common divisor: " << gcd << endl ;  //輸出字串。
    cout << "The least common multiple: "  << lcm ;  //輸出字串。




    return 0 ;

}
