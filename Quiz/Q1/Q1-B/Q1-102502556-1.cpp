#include <iostream>
using namespace std;

int main ()
{
    int a = 0; //宣告型別為 整數(int) 的第一個數字(a)，並初始化其數值為0。
    int b = 0; //宣告型別為 整數(int) 的第二個數字(b)，並初始化其數值為0。
    cout << "Please enter first number( >0 ): " ; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cin >> a;
    while (a <= 0) //使用while迴圈檢測使用者所輸入的數字是否合乎標準，若不符，則要求其重新輸入。
    {
        cout << "Out of range!" << endl;
        cout << "Please enter first number( >0 ): " ;
        cin >> a;
    }
    cout << "Please enter second number( >0 ): " ;
    cin >> b;
    while (b <= 0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter second number( >0 ): " ;
        cin >> b;
    }
    int f = a; //將原本的數值(a , b)儲存在另外的變數中(f , g)。
    int g = b;
    while ( a != 0 ) //用while迴圈使其能重複做計算，直到求得最大公因數( a=0 )為止。
    {
        int e = 0 ;
        if ( a < b ) //用if指令判斷 a 和 b 的大小，若 a < b，則先讓 a 和 b 交換順序再做計算。
        {
            e = a;
            a = b;
            b = e;
        }
        int c;
        int d;
        c = a / b ;
        d = a - b * c ;
        a = d ;
    }
    cout << "The greatest common divisor: " << b << endl;
    int h = f * g / b;
    cout << "The least common multiple: " << h << endl;

    return 0;
}
