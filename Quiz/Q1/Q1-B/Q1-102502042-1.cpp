﻿#include <iostream>
using namespace std;
int find_gcd(int a,int b)   //尋找最大公因數的函式
{
    while(a%b>0)    //輾轉相除法
    {
        int r=a%b;
        a=b;
        b=r;
    }
    return b;
}
int main()
{
    ios::sync_with_stdio(0);    //優化I/O
    int number1;
    int number2;
    int gcd;        //存放最大公因數的變數
    cout<<"Please enter first number( >0 ): ";
    cin>>number1;
    while(number1<=0)   //超出範圍則重複輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter first number( >0 ): ";
        cin>>number1;
    }
    cout<<"Please enter second number( >0 ): ";
    cin>>number2;
    while(number2<=0)   //超出範圍則重複輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter second number( >0 ): ";
        cin>>number2;
    }
    gcd = find_gcd(number1,number2);
    cout<<"The greatest common divisor: "<<gcd<<endl;
    cout<<"The least common multiple: "<<number1*number2/gcd<<endl; //最大公倍數=兩數相乘/最大公因數
    return 0;
}
