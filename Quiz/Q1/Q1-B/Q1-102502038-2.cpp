#include <iostream>
using namespace std;
int main(void){
    //init area
    long long int money,deno,counter;
    int calc[7];
    int numr[7] = {1000,500,100,50,10,5,1};
    while(true){//money input area
        cout << "How much money: ";
        cin >> money;
        if(money<=0){
            cout << "Out of range!\n";
        }else{
            break;
        };
    };
    while(true){//denomination input area
        cout << "What denomination is not enough? ";
        cin >> deno;
        if((deno!=1000)&&(deno!=500)&&(deno!=100)&&(deno!=50)&&(deno!=10)&&(deno!=5)){
            cout << "Out of range!\n";
        }else{
            break;
        };
    };
    counter = 0;
    while(money!=0){
        if((numr[counter])==deno){//skip that denomination
            calc[counter] = 0;
            counter++;
            continue;
        };
        calc[counter] = money/numr[counter];
        money -= (calc[counter]*numr[counter]);//money would lesser because deno is gaven
        counter++;
    };
    for(int i=0;i<=6;i++){//echo all
        cout << numr[i] << ": " << calc[i] << "\n";
    };
}
