#include <iostream>
using namespace std;

int main()
{
    int a=0;
    int b=0; //宣告2個整數a,b，初始值為0

    while(a<=0) //a<=0時執行迴圈
    {
        cout << "Please enter first number( >0 ): "; //輸出文字
        cin >> a; //輸入a值
        if (a<=0) //如果a<=0時執行
            cout << "Out of range!\n"; //輸出文字
    }

    while(b<=0) //b<=0時執行迴圈
    {
        cout << "Please enter second number( >0 ): "; //輸出文字
        cin >> b; //輸入b值
        if (b<=0) //如果b<=0時執行
            cout << "Out of range!\n"; //輸出文字
    }

    int c=1; //宣告整數c,值為1
    for(int i=1; i<=a; i++) //宣告整數i,值為1，i<=a時執行，每執行一次i增加1
    {
        if(a%i==0 && b%i==0) //如果a除以i的餘數為0且b除以i的餘數為0時，執行
            c=i; //c值變為i
    }

    cout << "The greatest common divisor: " << c << endl; //輸出文字與c值與換行
    cout << "The least common multiple: " << a*b/c; //輸出文字與a*b/c


    return 0;
}
