/*************************************************************************
    > File Name: Q1-102502044-2.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年10月23日 (週三) 16時33分04秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

/*********************************/
/*  str means the ui message     */
/* *tmp means the point of input */
/*********************************/
void input(const char *str, int *tmp)
{
	for(;;)
	{
		printf("%s", str); // print ui

		scanf("%d", tmp);  // input number
		
		if(*tmp > 0)       // check the input is in range (0, unlimit)
			return;
		else
			puts("Out of range!");
	}
}

int main()
{
	/****************************************************/
	/*      money means the money of input              */
	/* not_enough means the coin which is not enough    */
	/*       coin means the coins which I have          */
	/* coin_types means the types of coins which I have */
	/****************************************************/
	int money, not_enough;
	int coin[7] = {1000, 500, 100, 50, 10, 5, 1};
	#define coin_types 7

	/* input all number */
	input("How much money: ", &money);
	
	/* check the not_enough is in the range */
	int flag;
	for(;;)
	{
		input("What denomination is not enough? ", &not_enough);
		
		flag = 0;
		for(int i=0 ; i<coin_types-1 ; i++)
			if(coin[i] == not_enough)
			{
				flag = 1;
				break;
			}

		if(flag)
			break;
		else
			puts("Out of range!");
	}

	for(int i=0 ; i<coin_types ; i++)
	{
		/* if one types of coins is not enough */
		/* skip the types & print 0            */
		if(coin[i] == not_enough)
		{
			printf("%d: 0\n", coin[i]);
			continue;
		}

		/* print the type of coin & print how many coins I use*/
		printf("%d: %d\n", coin[i], money/coin[i]);

		/*calc the money*/
		money %= coin[i];
	}

	return 0;
}

