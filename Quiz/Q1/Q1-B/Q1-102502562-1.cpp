#include <iostream>
using namespace std;

int main()
{
    int a=0,b=0;                                            //宣告型別為整數的a,b 當作第一個數和第二個數並初始化其值為0
    int c=0,d=0,e=0;                                        //宣告型別為整數的c,d,e 做計算並初始化其值為0
    int f=0,g=0;                                            //宣告型別為整數的f,g 用來儲存一開始輸入的a,b並初始化其值為0

    while(1)                                                //用來詢問a的迴圈
    {
        cout << "Please enter first number( >0 ): ";        //輸出"Please enter first number( >0 ): "到螢幕上
        cin >> a;                                           //輸入一個值給a
        if(a<=0)                                            //判斷a是否<=0
        {
            cout << "Out of range!\n";                      //輸出"Out of range!\n"到螢幕上
        }
        else
        {
            break;                                          //跳出迴圈
        }
    }
    while(1)                                                //用來詢問b的迴圈
    {
        cout << "Please enter second number( >0 ): ";       //輸出"Please enter second number( >0 ): "到螢幕上
        cin >> b;                                           //輸入一個值給b
        if(b<=0)                                            //判斷b是否<=0
        {
            cout << "out of range!\n";                      //輸出"Out of range!\n"到螢幕上
        }
        else
        {
            break;                                          //跳出迴圈
        }
    }
    f = a;                                                  //讓f=a
    g = b;                                                  //讓g=b
    while(1)                                                //計算最大公因數的迴圈
    {
        while(a>b)                                          //若a>b進入這個迴圈
        {
            c = a-b;                                        //計算c=a-b
            a = c;                                          //讓a=c
        }
        while(b>a)                                          //若b>a進入這個迴圈
        {
            d = b-a;                                        //計算d=b-a
            b = d;                                          //讓b=d
        }
        if(a==b)                                            //判斷a是否等於b
        {
            break;                                          //跳出迴圈
        }
    }
    cout << "The greatest common divisor: " << a << endl;   //輸出"The greatest common divisor: "和a的值到螢幕上並換行
    e = f * g;                                              //讓e=f*g
    cout << "The least common multiple: " << e / a << endl; //輸出"The least common multiple: "和e/a的值到螢幕上並換行

    return 0;
}
