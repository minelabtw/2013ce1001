#include<iostream>
using namespace std;

int main()
{
    int a,b,c,d,e,f,g;
    int x;

    cout <<"How much money: ";
    cin >>x;

    while (x<0)                                  //辦定是否大於0
    {
        cout <<"Out of range!"<<endl;
        cout <<"How much money: ";
        cin >>x;
    }
    g=x%5;                                       //設定各個變數的條件
    f=(x%10-g)/5;
    e=(x%50-g-f*5)/10;
    d=(x%100-g-f*5-e*10)/50;
    c=(x%500-g-f*5-e*10-d*50)/100;
    b=(x%1000-g-f*5-e*10-d*50-c*100)/500;
    a=(x-b*500-c*100-d*50-e*10-f*5-g)/1000;

    cout <<"1000: "<<a<<endl;                   //輸出整理過的數據
    cout <<"500: "<<b<<endl;
    cout <<"100: "<<c<<endl;
    cout <<"50: "<<d<<endl;
    cout <<"10: "<<e<<endl;
    cout <<"5: "<<f<<endl;
    cout <<"1: "<<g<<endl;

    return 0;
}
