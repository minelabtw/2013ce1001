#include <iostream>

using namespace std;

int main ()
{
    int a;//宣告變數a

    cout << "How much money: ";//輸出How much money:
    cin >> a ;//輸入a

    while ( a <= 0 )//設定條件a<=0
    {
        cout << "Out of range!" << endl << "How much money: " ;//輸出Out of range!並換行後再輸出How much money:
        cin >> a ;//再次輸入a
    }

    int b = a / 1000 ;//宣告變數b = a / 1000
    int c = ( a - b * 1000 ) / 500 ;//宣告變數c = ( a - b * 1000 ) / 500
    int d = ( a - b * 1000 - c * 500 ) / 100 ;//宣告變數d = ( a - b * 1000 - c * 500 ) / 100
    int e = ( a - b * 1000 - c * 500 - d * 100 ) / 50 ;//宣告變數e = ( a - b * 1000 - c * 500 - d * 100 ) / 50
    int f = ( a - b * 1000 - c * 500 - d * 100 - e * 50 ) / 10 ;//宣告變數f = ( a - b * 1000 - c * 500 - d * 100 - e * 50 ) / 10
    int g = ( a - b * 1000 - c * 500 - d * 100 - e * 50 - f * 10 ) / 5 ;//宣告變數g = ( a - b * 1000 - c * 500 - d * 100 - e * 50 - f * 10 ) / 5
    int h = ( a - b * 1000 - c * 500 - d * 100 - e * 50 - f * 10 - g * 5 ) / 1 ;//宣告變數h = ( a - b * 1000 - c * 500 - d * 100 - e * 50 - f * 10 - g * 5 ) / 1

    cout << "1000: " << b << endl ;//輸出1000元的個數
    cout << "500: " << c << endl ;//輸出500元的個數
    cout << "100: " << d << endl ;//輸出100元的個數
    cout << "50: " << e << endl ;//輸出50元的個數
    cout << "10: " << f << endl ;//輸出10元的個數
    cout << "5: " << g << endl ;//輸出5元的個數
    cout << "1: " << h << endl ;//輸出1元的個數

    return 0 ;

}
