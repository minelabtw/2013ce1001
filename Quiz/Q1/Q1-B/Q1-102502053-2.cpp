#include <iostream>
using namespace std;

int main()
{
    int money=0;//call variable
    int x=0; // call variable for $1000
    int y=0; // call variable for $500
    int z=0; // call variable for $100
    int o=0; // variable for $50
    int p=0; // variable for $10
    int q=0; // variable for $5
    int r=0; // variable for $1

    while(money<=0) // data validation
    {
        cout<<"How much money: ";
        cin>>money; //input //input
        if(money<=0)
        {
            cout<<"Out of range!"<<endl;
        }
    }

    while(money>0) //count for money need by subtration
    {
        if(money>=1000)
        {
            money=money-1000;
            x++;
        }else if(money>=500)
        {
            money=money-500;
            y++;
        }else if(money>=100)
        {
            money=money-100;
            z++;
        }else if(money>=50)
        {
            money=money-50;
            o++;
        }else if(money>=10)
        {
            money=money-10;
            p++;
        }else if(money>=5)
        {
            money=money-5;
            q++;
        }else if(money>=1)
        {
            money=money-1;
            r++;
        }
    }

    cout<<"1000: "<<x<<endl; //display output
    cout<<"500: "<<y<<endl;
    cout<<"100: "<<z<<endl;
    cout<<"50: "<<o<<endl;
    cout<<"10: "<<p<<endl;
    cout<<"5: "<<q<<endl;
    cout<<"1: "<<r<<endl;

    return 0;
}
