#include <iostream>

using namespace std;

int main()

{
    int number1=0;
    int number2=0;
    int x=0;       //the bigger of number1 and number2
    int y=0;       //the smaller of number1 and number2
    int m=0;       //x除以y的商
    int n=1;       //x除以y的餘數

    //number1
    cout << "Please enter first number( >0 ): ";
    cin >> number1;
    while (number1<=0)
    {
        cout << "Out of range!\n";
        cout << "Please enter first number( >0 ): ";
        cin >> number1;
    }

    //number2
    cout << "Please enter second number( >0 ): ";
    cin >> number2;
    while (number2<=0)
    {
        cout << "Out of range!\n";
        cout << "Please enter second number( >0 ): ";
        cin >> number2;
    }

    //read the bigger one into x and smaller one into y
    if (number1>=number2)
    {
        x=number1;
        y=number2;
    }
    if (number1<number2)
    {
        x=number2;
        y=number1;
    }

    //輾轉相除法
    m=x/y;
    n=x%y;
    while (n!=0)
    {
        x=y;
        y=n;
        m=x/y;
        n=x%y;
    }

    //print the result
    cout << "The greatest common divisor: " << y << endl;
    cout << "The least common multiple: " << number1*number2/y << endl;

    return 0;
}
