#include <iostream>
using namespace std;
int main()
{
    ios::sync_with_stdio(0);    //優化I/O
    int money;  //存放錢的變數
    int price[7]= {1000,500,100,50,10,5,1}; //宣告陣列存放可換的面額
    int nomoney;    //宣告變數存放沒有的面額
    bool check=true;    //檢查沒有的面額是否在範圍內
    cout<<"How much money: ";
    cin>>money;
    while(money<0)  //超出範圍則重新輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"How much money: ";
        cin>>money;
    }
    cout<<"What denomination is not enough? ";
    cin>>nomoney;
    while(check)    //超出範圍則重新輸入
    {
        for(int i=0;i<7;++i)
        {
            if(nomoney==price[i])
            {
                check=false;
                break;
            }
            else
            {
                cout<<"Out of range!"<<endl;
                cout<<"What denomination is not enough? ";
                cin>>nomoney;
                break;
            }
        }
    }
    for(int i=0; i<7; ++i)  //從陣列中逐一查找可兌換的面額
    {
        if(money>=price[i]&&price[i]!=nomoney)  //若沒有那面額的錢則條件不成立
        {
            cout<<price[i]<<": "<<money/price[i]<<endl;
            money%=price[i];
        }
        else
        {
            cout<<price[i]<<": 0"<<endl;
        }
    }
    return 0;
}
