#include <iostream>

using namespace std;
main ()
{
    int money=0;
    int out[8]; //number of 1000,500,100,50,10,5,1 and out[0] is input;
    int cut[8]={0,1000,500,100,50,10,5,1};
    //element of out >> 0;
    for(int i=0;i<8;i++)
        out[i]=0;
    //
    //input how much
    while(money<=0){
    cout << "How much money: ";
    cin >> money;
    //debug
    if(money<=0)
        cout << "Out of range!" << endl;
    }
    //
    while(out[0]==0){
        cout << "What denomination is not enough? " ;
        cin >> out[0];
        if(out[0]==1000)
            out[0]=1;
        else if(out[0]==500)
            out[0]=2;
        else if(out[0]==100)
            out[0]=3;
        else if(out[0]==50)
            out[0]=4;
        else if(out[0]==10)
            out[0]=5;
        else if(out[0]==5)
            out[0]=6;
        else{
            //debug
            cout << "Out of range!" << endl;
            out[0]=0;
        }
    }
    //計算
    for(int i=1;i<8;i++){
        if(i==out[0])
            i++;
        out[i]=(int)(money/cut[i]);
        money-=out[i]*cut[i];
    }
    //output answer
    cout << "1000: " << out[1] << endl;
    cout << "500: " << out[2] << endl;
    cout << "100: " << out[3] << endl;
    cout << "50: " << out[4] << endl;
    cout << "10: " << out[5] << endl;
    cout << "5: " << out[6] << endl;
    cout << "1: " << out[7] << endl;
    //

    return 0;

}
