#include <iostream>
using namespace std;

int main()
{
    int money;//宣告錢
    int cash[7]= {1000,500,100,50,10,5,1};//宣告面額陣列
    int mcash[7]= {0};//宣告張數陣列並將所有元素初始為0
    int deno;//宣告缺少的面額
    int cashm;//宣告缺少面額所在索引值

    //檢查money是否>0
    while(true)
    {
        cout<<"How much money: ";
        cin>>money;

        if(money<=0)
        {
            cout<<"Out of range!"<<endl;
        }
        else
            break;
    }

    //檢查缺少面額是否正確
    while(true)
    {
        cout<<"What denomination is not enough? ";
        cin>>deno;

        if(deno!=1000&&deno!=500&&deno!=100&&deno!=50&&deno!=10&&deno!=5)
        {
            cout<<"Out of range!"<<endl;
        }
        else
            break;
    }

    //找出缺少面額的索引值
    for(int i=0; i<7; i++)
    {
        if(cash[i]==deno)
        {
            cashm=i;
        }
    }

    //開始找錢
    for(int j=0; j<7; j++)
    {
        if(j!=cashm)
        {
            mcash[j]=money/cash[j];
            money=money-mcash[j]*cash[j];
        }
    }

//印出結果
    for(int k=0; k<7; k++)
    {
        cout<<cash[k]<<": "<<mcash[k]<<endl;
    }

    return 0;
}
