#include <iostream>

using namespace std;

int main ()
{
    int a,b,c,d;//宣告變數a,b,c,d

    cout << "Please enter first number( >0 ): " ;//輸出Please enter first number( >0 ):
    cin >> a ;//輸入a

    while ( a <= 0 )//設定條件a<=0
    {
        cout << "Out of range!" << endl << "Please enter first number( >0 ): ";//輸出Out of range!並換行後再輸出Please enter first number( >0 ):
        cin >> a ;//再次輸入a
    }

    cout << "Please enter second number( >0 ): " ;//輸出Please enter second number( >0 ):
    cin >> b ;//輸出b

    while ( b <= 0 )//設定條件b<=0
    {
        cout << "Out of range!" << endl << "Please enter second number( >0 ): ";//輸出Out of range!並於換行後再輸出Please enter second number( >0 ):
        cin >> b ;//再次輸入b
    }

    int i = a , j = b ;//宣告2變數i = a , j = b
    if ( i = j )//設定條件i=j
        c = i ;//符合條件時c=i
            else
        {
            while ( i - j != 0 )//設定條件i - j != 0
            {
                if ( a > b )//設定條件a > b
                {
                    i =  i - j ;
                    c = j ;//2數相減直到相等為止
                }
                else if ( a < b )
                {
                    j = j - i ;
                    c = i ;//2數相減直到相等為止
                }
            }
        }

    d = a * b / c ; //最小公倍數為2數相乘再除以最大公因數

    cout << "The greatest common divisor: " << c << endl ;//輸出最大公因數
    cout << "The least common multiple: " << d << endl ;//輸出最小公倍數

    return 0 ;
}
