#include<iostream>

using namespace std;

int main()
{
    int a=0;//宣告變數

    cout << "How much money: ";//輸出字串
    cin  >> a;//輸入變數

    while(a<=0)
    {
        cout << "Out of range!" << endl;
        cout << "How much money: ";
        cin  >> a;
    }

    cout << "1000: ";

    if(a<1000)
    {
        cout << "0" << endl;
    }
    else
    {
        cout << (a-a%1000)/1000 << endl;
    }

    while(a>1000)
    {
        a=a-1000;
    }

    cout << "500: ";

    if(a<500)
    {
        cout << "0" << endl;
    }
    else
    {
        cout << (a-a%500)/500 << endl;
    }

    while(a>500)
    {
        a=a-500;
    }

    cout << "100: ";

    if(a<100)
    {
        cout << "0" << endl;
    }
    else
    {
        cout << (a-a%100)/100 << endl;
    }

    while(a>100)
    {
        a=a-100;
    }

    cout << "50: ";

    if(a<50)
    {
        cout << "0" << endl;
    }
    else
    {
        cout << (a-a%50)/50 << endl;
    }

    while(a>50)
    {
        a=a-50;
    }

    cout << "10: ";

    if(a<10)
    {
        cout << "0" << endl;
    }
    else
    {
        cout << (a-a%10)/10 << endl;
    }

    while(a>10)
    {
        a=a-10;
    }

    cout << "1: ";

    if(a<1)
    {
        cout << "0" << endl;
    }
    else
    {
        cout << a;
    }


    return 0;
}

