#include <iostream>
using namespace std;

int main()
{
    int a=0;
    int x=0; //宣告2個整數a,x，初始值為0
    int u[7]={1000,500,100,50,10,5,1}; //宣告整數陣列u，依序為1000,500,100,50,10,5,1

    while(a<=0) //a<=0時執行迴圈
    {
        cout << "How much money: "; //輸出文字
        cin >> a; //輸入a值
        if (a<=0) //如果a<=0時執行
            cout << "Out of range!\n"; //輸出文字
    }

    while(x!=1000 && x!=500 && x!=100 && x!=50 && x!=10 && x!=5) //x不等於1000,500,100,50,10,5其中之一時，執行迴圈
    {cout << "What denomination is not enough? "; //輸出文字
    cin >> x; //輸入x值
    if(x!=1000 && x!=500 && x!=100 && x!=50 && x!=10 && x!=5 && x!=1) //如果x不等於1000,500,100,50,10,5其中之一時，執行
        cout << "Out of range!\n"; //輸出文字
    }

    for(int i=0;i<7;i++) //宣告整數i,值為0，i<7時執行，每執行一次i增加1
    {
        int y=a/u[i]; //宣告整數y，值為y=a/u[i]的整數部分
        cout << u[i] << ": "; //輸出u[i]及文字
        if(x==u[i]) //如果x值為u[i]
            cout << "0" << endl; //輸出數字0及換行
        else //非以上情況
        {
            cout << y << endl; //輸出y值及換行
            a=a-y*u[i]; //a值為a-y*u[i]
        }
    }






    return 0;
}
