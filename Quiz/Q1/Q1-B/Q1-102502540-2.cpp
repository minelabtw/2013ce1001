#include <iostream>

using namespace std;

int main()
{
    int a,b,c,d,e,f,g,h; //宣告8個整數變數

    cout << "How much money: "; //顯示How much money:
    cin >> a; //輸入a值

    while ( a < 0 )  //當a<0時進入以下迴圈
    {
        cout << "Out of range!" << endl << "How much money: "; //顯示Out of range!並換行顯示How much money:
        cin >> a;
    }

    b=a/1000; //運算
    c=(a-b*1000)/500;
    d=(a-b*1000-c*500)/100;
    e=(a-b*1000-c*500-d*100)/50;
    f=(a-b*1000-c*500-d*100-e*50)/10;
    g=(a-b*1000-c*500-d*100-e*50-f*10)/5;
    h=(a-b*1000-c*500-d*100-e*50-f*10-g*5);

    cout << "1000: " << b << endl << "500: " << c << endl << "100: " << d << endl << "50: " << e << endl << "10: " << f << endl << "5: " << g << endl << "1: " << h; //輸出

    return 0;
}
