/*
系級:資工1B
學號:102502558
姓名:賴建勳
*/
#include <iostream>

using namespace std;


// p 代表提示訊息 e 代表錯誤訊息 n 代表外部的變數
void input(const char *p,const char *e, int &n)
{
    cout << p; // 先提示使用者
    cin >> n;  // 輸入至n
    while (n <= 0) // 如果n不在合理範圍內
    {
        cout << e << endl; // 顯示錯誤訊息
        // 再次提示
        cout << p;
        cin >> n;
    }
    // 直到n 符合範圍就會跳出while
}

// 計算最大公因數
int gcd(int a,int b)
{
    // 當兩數不相等
    while ( a != b)
    {
        // 大的數字減掉小的
        if (a > b)
            a = a - b;
        else
            b = b - a;
    }
    // 當a==b就會跳出while 此時會傳 a or b 都可以
    return a;
}

int main()
{
    // n1 第一個數字
    // n2 第二個數字
    // g 暫存最大公因數
    int n1 = 0,n2 = 0,g = 0;

    // 呼叫函數請使用者輸入
    input("Please enter first number( >0 ): ","Out of range!",n1);
    input("Please enter second number( >0 ): ","Out of range!",n2);

    // 計算gcd 並用 g 存起來
    // g 在計算lcm會再用到
    g = gcd(n1,n2);
    cout << "The greatest common divisor: " << g << endl;
    cout << "The least common multiple: " << n1 * n2 / g << endl;
    return 0;
}
