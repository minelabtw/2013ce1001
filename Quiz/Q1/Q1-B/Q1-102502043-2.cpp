#include <iostream>

using namespace std;

int main()
{
    int number;                                 //宣告變數
    cout<<"How much money: ";
    cin>>number;
    while(number<=0)                            //限制輸入範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"How much money: ";
        cin>>number;
    }
    return 0;
}
