#include<iostream>
using namespace std;
int main()
{
    int a=0,b=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0;//宣告變數
    cout<<"How much money: ";//顯示字串
    cin>>a;//輸入數值
    while(a<=0)
    {
        cout<<"Out of range!"<<endl;//顯示字串 換行
        cout<<"How much money: ";//顯示字串
        cin>>a;//輸入數值
    }
    cout<<"What denomination is not enough? ";//顯示字串
    cin>>b;//輸入數值
    while(b!=1000 && b!=500 && b!=100 && b!=50 && b!=10 && b!=5)
    {
        cout<<"Out of range!"<<endl;//顯示字串 換行
        cout<<"What denomination is not enough? ";//顯示字串
        cin>>b;//輸入數值
    }
    //***************以下用來判斷**************
    if(1000!=b)//判斷值有沒有被限制去掉
    {
        if(a/1000>0)//a值有千位數
        {
            i=a/1000;//運算
            a=a-1000*i;//去掉a值
        }
    }
    //**************以下類推*****************
    if(500!=b)
    {
        if(a/500>0)
        {
            j=a/500;
            a=a-500*j;
        }
    }
    if(100!=b)
    {
        if(a/100>0)
        {
            k=a/100;
            a=a-100*k;
        }
    }
    if(50!=b)
    {
        if(a/50>0)
        {
            o=a/50;
            a=a-50*o;
        }
    }
    if(10!=b)
    {
        if(a/10>0)
        {
            n=a/10;
            a=a-10*n;
        }
    }
    if(5!=b)
    {
        if(a/5>0)
        {
            l=a/5;
            a=a-5*l;
        }
    }
    if(a/1>0)
    {
        m=a/1;
        a=a-1*m;
    }
    cout<<"1000: "<<i<<endl;
    cout<<"500: "<<j<<endl;
    cout<<"100: "<<k<<endl;
    cout<<"50: "<<o<<endl;
    cout<<"10: "<<n<<endl;
    cout<<"5: "<<l<<endl;
    cout<<"1: "<<m<<endl;
    return 0;
}
