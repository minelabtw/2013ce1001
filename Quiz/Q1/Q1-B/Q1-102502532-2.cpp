#include<iostream>
using namespace std;

int main ()
{
    int money =0;              //宣告整數


    cout <<"How much money: ";
    cin >>money;
    while (money <= 0)         //判斷數字是否正確
    {
        cout <<"Out of range!\nHow much money: ";
        cin >>money;
    }

    if(money > 1000)
        cout <<"1000: "<<money/1000<<endl;            //從最大面額開始找
    else
        cout <<"1000: 0"<<endl;              //輸出0
    if (money%1000 >= 500)
        cout <<"500: "<<money%1000/500<<endl;
    else
        cout <<"500: 0"<<endl;
    if (money%500 >= 100)
        cout <<"100: "<<money%500/100<<endl;
    else
        cout <<"100: 0"<<endl;
    if (money%100 >= 50)
        cout <<"50: "<<money%100/50<<endl;
    else
        cout <<"50: 0"<<endl;
    if (money%50 >= 10)
        cout <<"10: "<<money%50/10<<endl;
    else
        cout <<"10: 0"<<endl;
    if (money%10 >= 5)
        cout <<"5: "<<money%10/5<<endl;
    else
        cout <<"5: 0"<<endl;
    if (money%5 >= 1)
        cout <<"1: "<<money%5/1;       //輸出張數
    else
        cout <<"1: 0"<<endl;

    return 0;

}
