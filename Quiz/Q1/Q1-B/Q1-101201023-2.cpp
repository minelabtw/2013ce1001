#include <iostream>

using namespace std;

int main()
{
    int m=0;
    int a=0;
    int b=0;
    int c=0;
    int d=0;
    int e=0;
    int f=0;
    int g=0;
    int h=0;
    int i=0;
    int j=0;
    int k=0;
    int l=0;


    cout << "How much money: ";
    cin >> m;
    while(m<=0)
    {
        cout << "Out of range!" << endl;
        cout << "How much money: ";
        cin >> m;
    }


    a=m%1000;               //宣告a為m除以1000的餘數
    g=m-a;                  //宣告g為m減a
    b=a%500;
    h=a-b;
    c=b%100;
    i=b-c;
    d=c%50;
    j=c-d;
    e=d%10;
    k=d-e;
    f=e%5;
    l=e-f;


    cout << "1000: " << g*0.001 <<endl;         //輸出1000的數量
    cout << "500: " << h*0.002 <<endl;
    cout << "100: " << i*0.01 <<endl;
    cout << "50: " << j*0.02 <<endl;
    cout << "10: " << k*0.1 <<endl;
    cout << "5: " << l*0.2 <<endl;
    cout << "1: " << f <<endl;



    return 0;
}
