#include <iostream>
using namespace std;
int main()
{
    int money;//宣告金錢的變數

    do{
        cout << "How much money:";//提示輸入金錢
        cin >> money;
        if ( money <= 0)//檢查金錢是否合理
            cout << "Out of range!" << endl;//若不合理則會進行提示并換行
    }while( money <= 0);//迴圈檢查金錢是否合理

    cout << "1000:" << money / 1000 << endl;//顯示1000元張數
    cout << "500:" << money % 1000 / 500 << endl;//顯示500元張數
    cout << "100:" << money % 500 / 100 << endl;//顯示100元張數
    cout << "50:" << money % 100 / 50 << endl;//顯示50元個數
    cout << "10:" << money % 50 /10 << endl;//顯示10元個數
    cout << "5:" << money % 10 / 5 << endl;//顯示5元個數
    cout << "1:" << money %5 / 1;//顯示1元個數

return 0;//迴歸到0
}
