#include <iostream>
using namespace std;

int main()
{
    int num1;//宣告第一個數字
    int num2;//宣告第二個數字
    int mult;//宣告兩數字的積

    //檢查num1是否>0
    while(true)
    {
        cout<<"Please enter first number( >0 ): ";
        cin>>num1;

        if(num1<=0)
            cout<<"Out of range!"<<endl;
        else
            break;
    }

    //檢查num2是否>0
    while(true)
    {
        cout<<"Please enter second number( >0 ): ";
        cin>>num2;

        if(num2<=0)
            cout<<"Out of range!"<<endl;
        else
            break;
    }

    mult=num1*num2;//把num1跟num2的積存起來 等等要算最小公倍數用

    //輾轉相除法
    while(num1!=num2)
    {
        if(num1>num2)
            num1=num1-num2;
        else
            num2=num2-num1;
    }

    cout<<"The greatest common divisor: "<<num1<<endl;
    cout<<"The least common multiple: "<<mult/num1<<endl;

    return 0;
}
