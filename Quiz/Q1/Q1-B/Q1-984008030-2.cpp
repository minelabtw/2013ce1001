#include <iostream>
using namespace std;
bool isOutOfRange(int number);//判斷參數的值是否<=0
int giveMoney(int* money, int type, int disableType);//計算找開面額的張數

int main(){
    int money = 0;//宣告型態為int的金額，並初始化為0
    int disableType = 5;//宣告型態為int的無法找開面額，並初始化為5

    while(true){//輸入金額
        cout << "How much money: ";
        cin >> money;
        if(isOutOfRange(money) == true){
            //如果money小於等於零則輸出錯誤訊息並重新輸入
            cout << "Out of range!" << endl;
        }
        else{
            //跳出迴圈!
            break;
        }
    }

    while(true){//輸入不足的面額
        cout << "What denomination is not enough? ";
        cin >> disableType;
        if(disableType != 1000 && disableType != 500 &&\
            disableType != 100 && disableType != 50 &&\
            disableType != 10 && disableType != 5 ){
            //如果disableType的值皆不是我們預設找開金額或是1
            //就要重新輸入
            cout << "Not currect denomination! " << endl;
        }
        else{
            //跳出迴圈!
            break;
        }
    }

    //輸出需要找的金額
    cout <<"1000: " << giveMoney(&money, 1000, disableType) << endl;
    cout <<"500: " << giveMoney(&money, 500, disableType) << endl;
    cout <<"100: " << giveMoney(&money, 100, disableType) << endl;
    cout <<"50: " << giveMoney(&money, 50, disableType) << endl;
    cout <<"10: " << giveMoney(&money, 10, disableType) << endl;
    cout <<"5: " << giveMoney(&money, 5, disableType) << endl;
    cout <<"1: " << giveMoney(&money, 1, disableType) << endl;
}

bool isOutOfRange(int number){//判斷參數的值是否<=0
    if(number <= 0){
        return true;
    }
    else{
        return false;
    }
}

int giveMoney(int* money, int type, int disableType){
    //以 指向金額變數的指標 找開面額 無法找的金額 為參數
    //最後回傳型別為int的找開面額的張數
    int number = 0;//宣告型態為int的找開的張數，並初始化為0

    if(disableType == type){
        //若找開面額恰好為無法找的金額
        //就直接回傳0
        return 0;
    }
    else{
        number = *money / type;//計算找開張數
        *money = *money % type;//計算剩餘金額
        return number;
    }
};
