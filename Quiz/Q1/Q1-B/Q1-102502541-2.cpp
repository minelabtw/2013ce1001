#include<iostream>
using namespace std;
int main()
{
    int a = 0;
    while(a<=0)
    {
        cout << "How much money: ";
        cin >> a;
        if(a<=0)
            cout << "Out of range!" << endl;
    }
    int b = a/1000;
    int c = (a-1000*b)/500;
    int d = (a-1000*b-500*c)/100;
    int f = (a-1000*b-500*c-100*d)/50;
    int g = (a-1000*b-500*c-100*d-50*f)/10;
    int h = (a-1000*b-500*c-100*d-50*f-10*g)/5;
    int i = (a-1000*b-500*c-100*d-50*f-10*g-5*h);
    cout << "1000: " << b << endl;
    cout << "500: " << c << endl;
    cout << "100: " << d << endl;
    cout << "50: " << f << endl;
    cout << "10: " << g << endl;
    cout << "5: " << h << endl;
    cout << "1: " << i << endl;
    return 0;
}
