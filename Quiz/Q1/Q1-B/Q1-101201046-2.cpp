#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>

using namespace std;

int main(void) {
    string buf; //a buffer for input
    bool buf_check; //check input
    int num; //input number
    int no_use; //not enough denomination
    int den[7] = {1000,500,100,50,10,5,1}; //denomination
    int i, j; //just for loop

    //input money
    do {
        buf_check = true;

        cout << "How much money: ";
        cin >> buf;

        //check buf
        if (buf.length() == strspn(buf.c_str(),"0123456789"))
            buf_check = !(num = atoi(buf.c_str()));

        } while(buf_check && (cout << "Out of range!\n"));

    //input not enough denomination
    do {
        buf_check = true;

        cout << "What denomination is not enough? ";
        cin >> buf;

        //check buf
        if (buf.length() == strspn(buf.c_str(),"0123456789")) {
            no_use = atoi(buf.c_str());
            for (i = 0;i < 6;i++)
                if (den[i] == no_use) {
                    buf_check = false;
                    break;
                    }
            }

        } while(buf_check && (cout << "Out of range!\n"));

    //output
    for (j = 0;j < 7;j++)
        if (j == i)
            cout << den[j] << ": 0" << endl;
        else {
            cout << den[j] << ": " << num / den[j] << endl;
            num %= den[j];
            }

    return 0;
    }
