/*
系級:資工1B
學號:102502558
姓名:賴建勳
*/
#include <iostream>

using namespace std;

int main()
{
    // m 代表輸入的金錢
    int m = 0;
    // 提示使用者輸入
    cout << "How much money: ";
    cin >> m;
    while (m < 1)// 當錢小於1就要輸出錯誤並且請使用者再次輸入
    {
        cout << "Out of range!" << endl;
        cout << "How much money: ";
        cin >> m;
    }


    // d 代表denomination
    int d = 0;
    cout << "What denomination is not enough? ";
    cin >> d;

    // 檢查是否在範圍內
    while (d != 1000 && d != 500 && d != 100 && d != 50 && d != 10 && d != 5 && d != 1)
    {
        cout << "You can only input 1000,500,100,50,10,5,1 !" << endl;
        cout << "What demonination is not enough? ";
        cin >> d;
    }

    // 如果面額足夠
    if (d != 1000)
    {
        cout << "1000: " << m / 1000 << endl;
        m = m % 1000;
    }
    else //否則就顯示0 並且不計算
    {
        cout << "1000: 0" << endl;
    }

    // 以下做法相同

    if (d != 500)
    {
        cout << "500: " << m / 500 << endl;
        m = m % 500;
    }
    else
    {
        cout << "500: 0" << endl;
    }
    if (d != 100)
    {
        cout << "100: " << m / 100 << endl;
        m = m % 100;
    }
    else
    {
        cout << "100: 0" << endl;
    }
    if (d != 50)
    {
        cout << "50: " << m / 50 << endl;
        m = m % 50;
    }
    else
    {
        cout << "50: 0" << endl;
    }
    if (d != 10)
    {
        cout << "10: " << m / 10 << endl;
        m = m % 10;
    }
    else
    {
        cout << "10: 0" << endl;
    }
    if (d != 5)
    {
        cout << "5: " << m / 5 << endl;
        m = m % 5;
    }
    else
    {
        cout << "5: 0" << endl;
    }
    if (d != 1)
    {
        cout << "1: " << m  << endl;
    }
    else
    {
        cout << "1: 0" << endl;
    }
    return 0;
}
