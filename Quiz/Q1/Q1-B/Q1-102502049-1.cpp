#include <iostream>
using namespace std;

int main()
{
    int first=0;
    int second=0; //宣告輸入的第一數及第二數
    int T=0;
    int a=0;
    int b=0;
    int x=0;
    int p=0;
    int q=0;

    cout << "Please enter first number( >0 ): ";
    cin >> first;
    while (first <=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter first number( >0 ): ";
        cin >> first;
    }

    cout << "Please enter second number( >0 ): ";
    cin >> second;
    while (second <=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter second number( >0 ): ";
        cin >> second;
    }  //要求輸入大於零的第一束及第二數

    p=first;
    q=second; //存取第一、二數到p,q

    if (first > second)
    {
        a = first;
        b = second;
        first = b;
        second = a;
    }

    while (first%second != 0)
    {
        T = first%second;
        first = second;
        second = T;
    } //進行輾轉相除法，找出最大公因數

    x = (p*q)/T; //找出最小公倍數

    cout << "The greatest common divisor: " << T << endl;
    cout << "The least common multiple: " << x; //顯示答案

    return 0;
}
