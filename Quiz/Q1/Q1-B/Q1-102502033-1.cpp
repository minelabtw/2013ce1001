#include<iostream>

using namespace std;

int main()
{
    int f=0;//宣告第一個整數變數
    int s=0;//宣告第二個整數變數
    int x=0;//需告一個整數變數用來儲存初始輸入值(f)
    int y=0;//需告一個整數變數用來儲存初始輸入值(s)

    cout << "Please enter first number( >0 ): ";
    cin  >> f;
    while(f<=0)//當f小於等於零會執行說你出錯，並讓你重新輸入的迴圈
    {
        cout << "Out of range!\n";
        cout << "Please enter first number( >0 ): ";
        cin  >> f;
    }
    x=f;//存入初始值

    cout << "Please enter second number( >0 ): ";
    cin  >> s;
    while(s<=0)//當s小於等於零會執行說你出錯，並讓你重新輸入的迴圈
    {
        cout << "Out of range!\n";
        cout << "Please enter second number( >0 ): ";
        cin  >> s;
    }
    y=s;//存入初始值


    while(f!=s)//當f不等於s的時候，進行輾轉相除法
    {
        while(f>s)//f大於s就f存入f減s
        {
            f=f-s;
        }
        while(f<s)
        {
            s=s-f;//s大於f就s存入s減f
        }//最後會相等
    }
    cout << "The greatest common divisor: " << f << "\n";//f s都可因為相等
    cout << "The least common multiple: "   << x*y/f << "\n";//最小公倍數是初始值相乘再除以最大公因數

    return 0;
}
