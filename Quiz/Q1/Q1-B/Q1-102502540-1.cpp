#include <iostream>

using namespace std;

int main()
{
    int a,b,c,d; //宣告兩個整數變數

    cout << "Please enter first number( >0 ): " ; //顯示Please enter first number( >0 ):
    cin >> a; //從鍵盤輸入，並將輸入的值給a

    while ( a <= 0 ) //迴圈當integer1小於等於0
    {
        cout << "Out of range!" << endl << "Please enter first number( >0 ): ";//顯示First number is out of range!!並換行顯示Please enter first number( >0 ):
        cin >> a;
    }

    cout << "Please enter second number( >0 ): " ; //顯示Please enter second number( >0 ):
    cin >> b; //從鍵盤輸入，並將輸入的值給b

    while ( b <= 0 ) //迴圈當b小於等於0
    {
        cout << "Out of range!" << endl << "Please enter second number( >0 ): "; //顯示Second number is out of range!!並換行顯示Please enter second number( >0 ):
        cin >> b;
    }

    c=a; //c值=a值
    d=b; //d值=b值

    while ( a > b ) //當a>b進入以下迴圈
    {
        a=a-b;
        while ( a < b )
        {
            b=b-a;
        }
    }

    while ( b > a ) //當b>a進入以下迴圈
    {
        b=b-a;
        while ( a > b)
        {
            a=a-b;
        }
    }

    cout << "The greast common divisor: " << a << endl << "The least common multiple: " << c*d/a; //輸出

    return 0;
}



