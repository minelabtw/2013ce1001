/*************************************************************************
    > File Name: Q1-102502044-1.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年10月23日 (週三) 16時05分31秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

/* use recursive to calc the gcd   */
/* I like to write gcd in one line */
int gcd(int a, int b){return (b==0) ? a : gcd(b, a%b);}

/*********************************/
/*  str means the ui message     */
/* *tmp means the point of input */
/*********************************/
void input(const char *str, int *tmp)
{
	for(;;)
	{
		printf("%s", str); // print ui

		scanf("%d", tmp);  // input number
		
		if(*tmp > 0)       // check the input is in range (0, unlimit)
			return;
		else
			puts("Out of range!");
	}
}

/*******************************/
/* str means the ui message    */
/* tmp means the output number */
/*******************************/
void output(const char *str, int tmp)
{
	printf("%s%d\n", str, tmp); // print output
}

int main()
{
	int first, second;

	/* input all number */
	input("Please enter first number( >0 ): " , &first );
	input("Please enter second number( >0 ): ", &second);

	/* check first is greater than second */
	/* if first < second, swap them       */
	if(first < second)
	{
		int t = first;
		first = second;
		second = t;
	}

	/***************************************/
	/* output all ui message & gcd, lcm    */
	/* gcd use recursive to calc           */
	/* lcm = first * second / gcd          */
	/* first/gcd is used to avoid overflow */
	/***************************************/
	output("The greatest common divisor: ", gcd(first, second));
	output("The least common multiple: ", first / gcd(first, second) * second);

	return 0;
}

