#include <iostream>
using namespace std;

int main()
{
    int a=0;    //宣告整數a且起始值為0
    int b=0;    //宣告整數b且起始值為0
    cout << "Please enter first number( >0 ): " ;
    cin >> a ;      //輸入a
    while(a<=0)     //當a<=0時，進入while迴圈
    {
        cout << "Out of range!" << endl;
        cout << "Please enter first number( >0 ): " ;
        cin >> a ;  //再次輸入整數a
    }

    cout << "Please enter second number( >0 ): " ;
    cin >> b ;      //輸入整數b
    while(b<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter second number( >0 ): " ;
        cin >> b ;  //再次輸入整數b
    }

    int x=0;
    int y=0;
    x = a;          //使x=a
    y = b;          //使y=b

    while (a!=0)    //當a不等於0時，進入while迴圈
    {
        if (a<b)    //當a<b時，進入if迴圈
        {
            int e=0;
            e=a;        //使a和b交換
            a=b;
            b=e;
        }

        int c=0;
        int d=0;
        c = a/b;
        d = a-b*c;      //利用輾轉相除法，漸漸求出最大公因數
        a = d;
    }
    cout << "The greatest common divisor: " << b << endl;       //輸出最大公因數
    cout << "The least common multiple: " << x*y/b << endl;     //計算並輸出最小公倍數

    return 0;
}
