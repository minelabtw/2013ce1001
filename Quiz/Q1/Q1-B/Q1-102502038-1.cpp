#include <iostream>
using namespace std;
int main(void){
    long long int numA,numB;//var two int,numA and numB
    while(true){//input area 1
        cout << "Please enter first number( >0 ): ";
        cin >> numA;
        if(numA<=0){
            cout << "Out of range!\n";
        }else{
            break;
        };
    };
    while(true){//input area 2
        cout << "Please enter second number( >0 ): ";
        cin >> numB;
        if(numB<=0){
            cout << "Out of range!\n";
        }else{
            break;
        };
    };
    long long int divA = numA;//create two int,equal to numA and numB
    long long int divB = numB;
    while(divA!=divB){
        (divA>divB?divA:divB) -= (divA>divB?divB:divA);//calc common divisor
    };
    cout << "The greatest common divisor: " << divA << "\n";//echo common divisor
    cout << "The least common multiple: " << numA*(numB/divA) << "\n";//calc and echo common multiple
    return 0;
};
