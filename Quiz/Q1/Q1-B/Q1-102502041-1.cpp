#include<iostream>
using namespace std;
int main()
{
    int a,b,c,d;                                        //宣告兩個整數變數
    cout<<"Please enter first number( >0 ): ";
    cin>>a;
    while(a<=0)                                         //使用while迴圈判定a是否在預設範圍之內
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter first number( >0 ): ";
        cin>>a;
    }
    cout<<"Please enter second number( >0 ): ";
    cin>>b;
    while(b<=0)                                         //使用while迴圈判定b是否在預設範圍之內
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter second number( >0 ): ";
        cin>>b;
    }

    c=a,d=b;                //將輸入的兩個變數丟給c,d

    while(a%b>0)        //使用輾轉相除法求出最最大公因數
    {
        int r=a%b;
        a=b;
        b=r;
    }
    cout<<"The greatest common divisor: "<<b<<endl;
    cout<<"The least common multiple: "<<c*d/b;   //藉由兩數相乘再除以最大公因數來得到最小公倍數並將之輸出

    return 0;
}
