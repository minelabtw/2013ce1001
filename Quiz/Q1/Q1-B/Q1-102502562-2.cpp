#include <iostream>
using namespace std;

int main()
{
    int a;                                  //宣告型別為整數的a當作輸入的錢
    int b,c,d,e,f,g;                        //宣告型別為整數的b,c,d,e,f,g用來計算

    while(1)                                //詢問錢數的迴圈
    {
        cout << "How much money: ";         //輸出"How much money: "到螢幕上
        cin >> a;                           //輸入一個值給a
        if(a<=0)                            //判斷a是否<=0
        {
            cout << "Out of range!\n";      //輸出"Out of range!\n"到螢幕上
        }
        else
        {
            break;                          //跳出迴圈
        }
    }
    cout << "1000: " << a / 1000 << endl;   //輸出"1000: "和a/1000的值到螢幕上並換行
    b = a % 1000;                           //計算b=a%1000
    cout << "500: " << b / 500 << endl;     //輸出"500: "和b/500的值到螢幕上並換行
    c = b % 500;                            //計算c=b%500
    cout << "100: " << c / 100 << endl;     //輸出"100: "和c/100的值到螢幕上並換行
    d = c % 100;                            //計算d=c%100
    cout << "50: " << d / 50 << endl;       //輸出"50: "和d/50的值到螢幕上並換行
    e = d % 50;                             //計算e=d%50
    cout << "10: " << e / 10 << endl;       //輸出"10: "和e/10的值到螢幕上並換行
    f = e % 10;                             //計算f=e%10
    cout << "5: " << f / 5 << endl;         //輸出"5: "和f/5的值到螢幕上並換行
    g = f % 5;                              //計算g=f%5
    cout << "1: " << g / 1 << endl;         //輸出"1: "和g/1的值到螢幕上並換行

    return 0;
}
