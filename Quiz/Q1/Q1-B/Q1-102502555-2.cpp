#include <iostream>

using namespace std;

int main(){
    int money = 0;  //變數找的錢
    int temp = 0;  //變數暫存用

    while(money <=0){  //輸入金額並判斷有無超出範圍
        cout << "How much money: ";
        cin >> money;
        if(money <= 0){
            cout << "Out of range!" << endl;
        }
    }
    if(money < 1000){  //檢查是否不必找一千元
        cout << "1000: 0" << endl;
    }
    while(money >= 1000){  //看要找幾張一千
        temp = money / 1000;
        money = money % 1000;
        cout << "1000: " << temp << endl;
    }
    if(money < 500){  //檢查是否不必找五百元
        cout << "500: 0" << endl;
    }
    while(money >= 500){  //看要找幾張五百
        temp = money / 500;
        money = money % 500;
        cout << "500: " << temp << endl;
    }
    if(money < 100){  //檢查是否不必找一百元
        cout << "100: 0" << endl;
    }
    while(money >= 100){  //看要找幾張一百
        temp = money / 100;
        money = money % 100;
        cout << "100: " << temp << endl;
    }
    if(money < 50){  //檢查是否不必找五十元
        cout << "50: 0" << endl;
    }
    while(money >= 50){  //看要找幾個五十元
        temp = money / 50;
        money = money % 50;
        cout << "50: " << temp << endl;
    }
    if(money < 10){  //檢查是否不必找十元
        cout << "10: 0" << endl;
    }
    while(money >= 10){  //看要找幾個十元
        temp = money / 10;
        money = money % 10;
        cout << "10: " << temp << endl;
    }
    if(money < 5){  //檢查是否不必找五元
        cout << "5: 0" << endl;
    }
    while(money >= 5){  //看要找幾個五元
        temp = money / 5;
        money = money % 5;
        cout << "5: " << temp << endl;
    }
    if(money < 1){  //檢查是否不必找一元
        cout << "1: 0";
    }
    while(money >= 1){  //看要找幾個一元
        temp = money / 1;
        money = money % 1;
        cout << "1: " << temp << endl;
    }

    return 0;
}
