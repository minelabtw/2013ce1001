#include <iostream>
using namespace std;

int gcd(int n1,int n2) //副函式,計算最大公因數,利用輾轉相除法
{
    if(n1%n2==0)
        return n2;
    else
        return gcd(n2,n1%n2);
}

int main()
{
    int num1,num2,ans1,ans2;//宣告整數,分別為第一個輸入和第二個輸入,以及最大公因數和最小公倍數
    while(1) //處理第一個輸入,如果超過範圍,要求重新輸入
    {
        cout << "Please enter first number( >0 ): ";
        cin >> num1;
        if(num1>0)
            break;
        else
            cout << "Out of range!\n";
    }
    while(1) //處理第二個輸入,如果超過範圍,要求重新輸入
    {
        cout << "Please enter second number( >0 ): ";
        cin >> num2;
        if(num2>0)
            break;
        else
            cout << "Out of range!\n";
    }
    ans1 = gcd(num1,num2);//計算最大公因數
    ans2 = num1*num2/ans1;//計算最小公倍數
    cout << "The greatest common divisor: " << ans1 << endl;//輸出最大公因數
    cout << "The least common multiple: " << ans2 <<endl;//輸出最小公倍數
    return 0;
}
