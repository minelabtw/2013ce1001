#include <iostream>

using namespace std ;

int main ()

{
    int money = 0 ;

    cout << "How much money: " ;  //設定一變數，其名稱為money，並使其初始值為0。
    cin >> money ;  //使操作者輸入數字，其值丟給money。

    while ( money < 0 )  //當money的值小於0，就進入迴圈。
    {
        cout << "Out of range!\n" << "How much money: " ;  //輸出"超出範圍"，並要求操作者重新輸入。
        cin >> money ;  //將重新輸入之值丟給money。
    }








    return 0 ;
}
