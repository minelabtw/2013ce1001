#include <iostream>
using namespace std;

int main()
{
	int money;
	cout << "How much money: "; cin >> money;					//input the money to pay
	while(money<=0){
		//warn the user and re-input if money is not in range
		cout << "Out of range!\n";
		cout << "How much money: "; cin >> money;
	}

	int lack;
	cout << "What denomination is not enough? "; cin >> lack;	//input the denomination that is not enough
	while(lack!=1000 && lack!=500 && lack!=100 && lack!=50 && lack!=10 && lack!=5){
		//warn the user and re-input if lack is not in range
		cout << "Out of range!\n";
		cout << "What denomination is not enough? "; cin >> lack;
	}

	if(lack!=1000){
		//use $1000 to pay
		cout << "1000: " << money/1000 << "\n";
		money%=1000;
	}else{cout << "1000: " << 0 << "\n";}

	if(lack!=500){
		//use $500 to pay
		cout << "500: " << money/500 << "\n";
		money%=500;
	}else{cout << "500: " << 0 << "\n";}

	if(lack!=100){
		//use $100 to pay
		cout << "100: " << money/100 << "\n";
		money%=100;
	}else{cout << "100: " << 0 << "\n";}

	if(lack!=50){
		//use $50 to pay
		cout << "50: " << money/50 << "\n";
		money%=50;
	}else{cout << "50: " << 0 << "\n";}

	if(lack!=10){
		//use $10 to pay
		cout << "10: " << money/10 << "\n";
		money%=10;
	}else{cout << "10: " << 0 << "\n";}

	if(lack!=5){
		//use $5 to pay
		cout << "5: " << money/5 << "\n";
		money%=5;
	}else{cout << "5: " << 0 << "\n";}

	if(lack!=1){
		//use $1 to pay
		cout << "1: " << money << "\n";
	}else{cout << "1: " << 0 << "\n";}

	return 0;
}
