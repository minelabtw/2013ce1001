#include <iostream>
using namespace std;

int main()
{
    int a=0 , b=0 , c=0 , i=0;//使a,b,c,i初始值為0
    cout <<"Please enter first number( >0 ) : ";//輸出請輸入第一個數字
    cin >>a;//輸入為a
    while(a<=0)//當a<=0，則輸出不合範圍，且重新輸入一次
    {
        cout <<"Out of range!\nPlease enter first number( >0 ): ";
        cin >>a;
    }
    cout <<"Please enter second number( >0 ) : ";//輸出請輸入第二個數字
    cin >>b;//輸入為b
    while(b<=0)//當b<=0，則輸出不合範圍，且重新輸入一次
    {
        cout <<"Out of range!\nPlease enter second number( >0 ): ";
        cin >>b;
    }
    for(int i=1 ; i<=a ; i++)//使i=1，判斷i<=a時進行迴圈(且判斷到最大為止)，且i=i+1
    {
        if(a%i==0 && b%i==0)//當a/i 可整除且 b/i可整除
            c=i;//此時令c=i
    }
    cout <<"The greatest common divisor: "<<c<<endl;//輸出最大公因數
    cout <<"The least common multiple: "<<a*b/c;//輸出最小公倍數

    return 0;
}
