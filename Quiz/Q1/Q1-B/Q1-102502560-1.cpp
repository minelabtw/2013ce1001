#include <iostream>
using namespace std;

int main()
{
	int num1;
	cout << "Please enter first number( >0 ): "; cin >> num1;
	while(num1<=0){
		//warn the user and re-input if num1 is not in range
		cout << "Out of range!\n";
		cout << "Please enter first number( >0 ): "; cin >> num1;
	}

	int num2;
	cout << "Please enter second number( >0 ): "; cin >> num2;
	while(num2<=0){
		//warn the user and re-input if num2 is not in range
		cout << "Out of range!\n";
		cout << "Please enter second number( >0 ): "; cin >> num2;
	}

	int gcd;
	int minnum = (num1<num2) ? num1 : num2 ;
	for(int i=1;i<=minnum;i++){
		if(num1%i==0 && num2%i==0){gcd=i;}	//get the common divisor as big as possible
	}

	int lcm=num1*num2/gcd;
	cout << "The greatest common divisor: " << gcd << "\n";
	cout << "The least common multiple: " << lcm << "\n";

	return 0;
}
