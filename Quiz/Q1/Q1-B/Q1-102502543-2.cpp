#include <iostream>
using namespace std;
main()
{
    int a=0; //宣告變數a
    int b=0; //宣告變數b
    int c=0; //宣告變數c
    int d=0; //宣告變數d
    int e=0; //宣告變數e
    int f=0; //宣告變數f
    int g=0; //宣告變數g
    int h=0; //宣告變數h
    int i=0; //宣告變數i
    cout <<"How much money: "; //輸出
    cin >> a; //輸入
    while (a<=0) //重複判斷a是否小於等於0
    {
        cout <<"Out of range!"<<endl<<"How much money: ";
        cin >> a;
    }
    cout <<"What denomination is not enough? "; //輸出
    cin >> i; //輸入
    while (i!=1000 && i!=500 && i!=100 && i!=50 && i!=10 && i!=10 && i!=5) //重複判斷i是否超出特定的數字
    {
        cout <<"Out of range!"<<endl<<"What denomination is not enough? ";
        cin >> i;
    }
    if (i!=1000) //判斷i值
    {
        b=a/1000; //運算
    }
    if (i!=500) //判斷i值
    {
        c=(a-b*1000)/500; //運算
    }
    if (i!=100) //判斷i值
    {
        d=(a-b*1000-c*500)/100; //運算
    }
    if (i!=50) //判斷i值
    {
        e=(a-b*1000-c*500-d*100)/50; //運算
    }
    if (i!=10) //判斷i值
    {
        f=(a-b*1000-c*500-d*100-e*50)/10; //運算
    }
    if (i!=5) //判斷i值
    {
        g=(a-b*1000-c*500-d*100-e*50-f*10)/5; //運算
    }
    h=a-b*1000-c*500-d*100-e*50-f*10-g*5; //運算
    cout <<"1000: "<<b<<endl<<"500: "<<c<<endl<<"100: "<<d<<endl<<"50: "<<e<<endl<<"10: "<<f<<endl<<"5: "<<g<<endl<<"1: "<<h; //輸出
    return 0;
}
