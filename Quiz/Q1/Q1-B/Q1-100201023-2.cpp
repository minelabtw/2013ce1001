#include <iostream>
#include <string>

using namespace std;

int main()
{
    int money , notenough;
    string trash;

    while(true) // intput money
    {
        cout << "How much money: ";
        if(!(cin >> money))
        {
            cin.clear();
            getline(cin , trash);
        }
        else if(money > 0)
            break;

        cout << "Out of range!" << endl;
    }

    while(true) // input not enough denomination
    {
        cout << "What denomination is not enough? ";
        if(!(cin >> notenough))
        {
            cin.clear();
            getline(cin , trash);
        }
        else if(notenough == 1000 || notenough == 500 || notenough == 100 || notenough == 50 || notenough == 10 || notenough == 5 || notenough == 1)
            break;

        cout << "Out of range!" << endl;
    }

    int denomination[] = {1000 , 500 , 100 , 50 , 10 , 5 , 1}; // declare kind of cash
    int number[7] = {0}; // declare the number of cash

    for(int i = 0 ; i < 7 ; ++i) // comput the number of every kind of cash
    {
        static int temp = money;

        if(denomination[i] == notenough)
            continue;
        else
        {
            number[i] = temp / denomination[i];
            temp %= denomination[i];
        }
    }

    for(int i = 0 ; i < 7 ; ++i) // output result
        cout << denomination[i] << ": " << number[i] << endl;

    return 0;
}
