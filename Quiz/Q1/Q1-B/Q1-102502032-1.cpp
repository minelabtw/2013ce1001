#include <iostream>
using namespace std;

int main()
{
    //varible decalaration and initialization
    float n1 = 0;   //first number
    float n2 = 0;   //second one
    int n1t = 0;  //first temp
    int n2t = 0;  //second temp

    //ask for first number
    do
    {
        cout << "Please enter first number( >0 ): ";
        cin >> n1t;
        n1 = n1t;
    }
    while( ( n1t <= 0 ) && cout << "Out of range!" << endl );

    //ask for second number
    do
    {
        cout << "Please enter second number( >0 ): ";
        cin >> n2t;
    }
    while( n2t <= 0 && cout << "Out of range!" << endl );
    n2 = n2t;

    //comput G.C.D.
    while ( n1t != n2t )
    {
        if ( n1t < n2t )
            n2t-=n1t;
        else
            n1t-=n2t;
    }

    //outputGCD and lcm
    cout << "The greast common divisor: " << n1t << endl;
    cout << "The least common multiple: " << n1 * n2 / n1t;

    return 0;
}
