#include <iostream>

using namespace std;

int main()
{
    int w,h,a,b,c,d,e;
    cout<<"Please enter first number( >0 ): ";//輸出說明到螢幕上
    cin>>w; //輸入w.h
    while (w <= 0) //判別wh是否合格 否則重新輸入
    {
        cout << "Out of range!" << endl <<  "Please enter first number( >0 ): ";
        cin >> w;
    }
    cout<<"Please enter second number( >0 ): ";//輸出說明到螢幕上
    cin>>h; //輸入w.h
    while (h <= 0) //判別wh是否合格 否則重新輸入
    {
        cout << "Out of range!" << endl <<  "Please enter second number( >0 ): ";
        cin >> h;
    }
    if(w>=h) //判斷兩個輸入值誰大誰小
    {
        a=w;
        b=h;
    }
    else
    {
        a=h;
        b=w;
    }
    c=a%b; //利用輾轉相除法求出最大公因數
    while(c!=0)
    {
        a=b;
        b=c;
        c=a%b;
    }
    d=b; //輸出最大公因數結果
    cout <<"The greatest common divisor: "<<d<<endl;
    e=(w*h)/d; //利用最大公因數計算最小公倍數並輸出結果
    cout <<"The least common multiple: "<<e;
    return 0;
}
