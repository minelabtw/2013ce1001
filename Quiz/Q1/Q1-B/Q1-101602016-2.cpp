#include<iostream>
using namespace std;

int main()
{
    int money;//給使用者輸入金錢
    int x;//給使用者輸入，缺少的面額
    int a;//用來輸出面額量

    cout<<"How much money: ";//給使用者輸入金錢，且判定它的範圍
    cin>>money;

    while(money<0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"How much money: ";
        cin>>money;
    }

    cout<<"What denomination is not enough? ";//給使用者輸入缺少的面額
    cin>>x;
    /*
    當缺少的面額不為1000時
    輸出所需的張數
    當缺少的面額為1000時
    輸出"1000: 0"

    其餘面額同理
    */
    if(x!=1000)
    {
        a=money/1000;
        cout<<"1000: "<<a<<endl;
        money=money-a*1000;
    }
    if(x==1000)
        cout<<"1000: 0"<<endl;

    if(x!=500)
    {
        a=money/500;
        cout<<"500: "<<a<<endl;
        money=money-a*500;
    }
    if(x==500)
        cout<<"500: 0"<<endl;

    if(x!=100)
    {
        a=money/100;
        cout<<"100: "<<a<<endl;
        money=money-a*100;
    }
    if(x==100)
        cout<<"100: 0"<<endl;

    if(x!=50)
    {
        a=money/50;
        cout<<"50: "<<a<<endl;
        money=money-a*50;
    }
    if(x==50)
        cout<<"50: 0"<<endl;

    if(x!=10)
    {
        a=money/10;
        cout<<"10: "<<a<<endl;
        money=money-a*10;
    }
    if(x==10)
        cout<<"10: 0"<<endl;

    if(x!=5)
    {
        a=money/5;
        cout<<"5: "<<a<<endl;
        money=money-a*5;
    }
    if(x==5)
        cout<<"5: 0"<<endl;

    if(x!=1)
    {
        a=money;
        cout<<"1: "<<a<<endl;
    }
    if(x==1)
        cout<<"1: 0"<<endl;

    return 0;
}
