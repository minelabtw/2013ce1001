#include<iostream>
using namespace std;

int main()
{
    int money=0;                                  //宣告變數money來接收之後輸入的金額起始值為零
    while(money<=0)
    {
        cout << "How much money: ";               //當輸入的金額<=0或為第一次輸入時，輸出字串"How much money: "
        cin >> money;                             //將輸入的金額宣告給變數money
        if(money<=0)
            cout << "Out of range! \n";           //當輸入的金額不符要求時，輸出字串"Out of range!"並重頭再要求一次輸入
    }

    cout << "1000: " << money/1000 << "\n";       //輸出字串"1000: "和金額/1000的除數(也就是千元鈔的張數)
    money=money-1000*(money/1000);                //重新將變數money宣告為付完千元鈔後還剩下的金額

    cout << "500: " << money/500 << "\n";         //輸出字串"500: "和金額/500的除數(也就是五百元鈔的張數)
    money=money-500*(money/500);                  //重新將變數money宣告為付完五百元鈔後還剩下的金額


    cout << "100: " << money/100 << "\n";         //輸出字串"100: "和金額/100的除數(也就是百元鈔的張數
    money=money-100*(money/100);                  //重新將變數money宣告為付完百元鈔後還剩下的金額

    cout << "50: " << money/50 << "\n";           //輸出字串"50: "和金額/50的除數(也就是五十元的數量)
    money=money-50*(money/50);                    //重新將變數money宣告為付完五十元後還剩下的金額

    cout << "10: " << money/10 << "\n";           //輸出字串"10: "和金額/10的除數(也就是十元的數量)
    money=money-10*(money/10);                    //重新將變數money宣告為付完十元後還剩下的金額

    cout << "5: " << money/5 << "\n";             //輸出字串"5: "和金額/5的除數(也就是五元的數量)
    money=money-5*(money/5);                      //重新將變數money宣告為付完五元後還剩下的金額

    cout << "1: " << money;                       //輸出最後剩下的金額(一元的數量)

    return 0;
}
