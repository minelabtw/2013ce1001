#include<iostream>
using namespace std;

int main()
{
    int a=0;   //宣告a為整數

    cout<<"How much money: ";
    cin>>a;
    while(a<0)   //設定當a小於零時
    {
        cout<<"Out of range!\nHow much money: ";
        cin>>a;
    }
    cout<<"1000: "<<endl;
    cout<<"500: "<<endl;
    cout<<"100: "<<endl;
    cout<<"50: "<<endl;
    cout<<"10: "<<endl;
    cout<<"5: "<<endl;
    cout<<"1: "<<endl;

    return 0;
}
