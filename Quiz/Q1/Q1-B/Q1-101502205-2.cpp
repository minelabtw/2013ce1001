#include<iostream>

using namespace std;

int main() {
    int m[7] = {1000,500,100,50,10,5,1}; //An array storing type denominations
    int money;
    //inputs
    while(1){
        cout << "How much money: ";
        cin >> money;
        if(money>0) //End loop if its legal
            break;
        cout << "Out of range!" << endl;
    }

    int n,i; //n: input of the denomination isn't enough
    //i: counter
    bool flag; //used to test if following input is legal
    while(1){
        cout << "What denomination is not enough? ";
        cin >> n;
        i=0;
        flag = false; //set false means the denomination does not exist
        while(i<6){ //not including 1
            if(n == m[i]){
                flag = true; //set true when the input is legal
                break;
            }
            i++;
        }
        if(flag) //if the input is legal then end loop
            break;
        cout << "Out of range!" << endl;
    }
    //outputs
    i=0;
    while(i<7){
        if(m[i]==n){
            //if the denomination is not enought then skip
            cout << m[i] << ": 0" << endl;
            i++;
        }else{
            cout << m[i] << ": " << money/m[i] << endl;
            money %= m[i]; //how much money remain
            i++;
        }
    }
    return 0;
}
