#include <iostream>
using namespace std;

int main ()
{
    int a = 0; //宣告型別為 整數(int) 的第一個數字(a)，並初始化其數值為0。
    cout << "How much money: " ; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cin >> a;
    while ( a <= 0 ) //用while迴圈檢測使用者的輸入是否合乎標準，若不符，則要求其重新輸入。
    {
        cout << "Out of range!" << endl;
        cout << "How much money: ";
        cin >> a;
    }
    int i = 0; //宣告型別為 整數(int) 的第二個變數(i)，代表是哪個金額不能找錢。
    cout << "What denomination is not enough? ";
    cin >> i;
    while ( i != 1000 && i != 500 && i != 100 && i != 50 && i != 10 && i != 5 )
        //用while迴圈檢測使用者的輸入是否合乎標準，若不符，則要求其重新輸入。
    {
        cout << "Out of range!" << endl;
        cout << "What denomination is not enough? " ;
        cin >> i;
    }
    int b = 0; //宣告型別為 整數(int) 的第三至第九個變數(b - h)，用來記錄每項金額所需找錢的數量。。
    int c = 0;
    int d = 0;
    int e = 0;
    int f = 0;
    int g = 0;
    int h = 0;
    if ( i != 1000 ) //用if指令判斷究竟是何項金額不能找錢，再決定是哪一個金額所找錢的數量不做運算。
    {
        b = a / 1000 ;
        a = a - 1000 * b;
    }
    else
    {

    }
    if ( i != 500 )
    {
        c = a / 500 ;
        a = a - 500 * c;
    }
    else
    {

    }
    if ( i != 100 )
    {
        d = a / 100;
        a = a - 100 * d;
    }
    else
    {

    }
    if ( i != 50 )
    {
        e = a / 50 ;
        a = a - 50 * e;
    }
    else
    {

    }
    if ( i != 10 )
    {
        f = a / 10 ;
        a = a - 10 * f;
    }
    else
    {

    }

    if ( i != 5 )
    {
        g = a / 5 ;
        a = a - 5 * g;
    }
    else
    {

    }

    h = a / 1;

    cout << "1000: " << b << endl;
    cout << "500: " << c << endl;
    cout << "100: " << d << endl;
    cout << "50: " << e << endl;
    cout << "10: " << f << endl;
    cout << "5: " << g << endl;
    cout << "1: " << h << endl;

    return 0;
}
