#include<iostream>

using namespace std;

int main() {
    int first, second;

    //inputs first number
    while(1){
        cout << "Please enter first number( >0 ): ";
        cin >> first;
    //end loop if the input is legal
        if(first>0)
            break;
        cout << "Out of range!" << endl;
    }

    //inputs first number
    while(1){
        cout << "Please enter second number( >0 ): ";
        cin >> second;
    //end loop if the input is legal
        if(second>0)
            break;
        cout << "Out of range!" << endl;
    }

    int tmp,a,b; //tmp is used to swap two integers
    //a, b are the copy of input
    a = first;
    b = second;

    //consider a = bq + r;
    while(b!=0){ //when r==0 then b is the gcd
        a = a%b;
    //swap a and b;
        tmp = a;
        a = b;
        b = tmp;
    }
    cout << "The greatest common divisor: " << a << endl;
    cout << "The least common multiple: " << first*second/a << endl; //output lcm, lcm = a*b/gcd

    return 0;
}
