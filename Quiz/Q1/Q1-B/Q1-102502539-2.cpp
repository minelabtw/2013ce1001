#include <iostream>

using namespace std;

int main()
{
    int money;  //宣告金額變數

    cout << "How much money: " ;    //輸入金錢
    cin >> money;
    while ( money < 1 ) //限制範圍
    {
        cout << "Out of range!\n" << "How much money: " ;
        cin >> money;
    }

    cout << "1000: " << money / 1000 << endl;
    money = money % 1000;

    cout << "500: " << money / 500 << endl;
    money = money % 500;

    cout << "100: " << money / 100 << endl;
    money = money % 100;

    cout << "50: " << money / 50 << endl;
    money = money % 50;

    cout << "10: " << money / 10 << endl;
    money = money % 10;

    cout << "5: " << money / 5 << endl;
    money = money % 5;

    cout << "1: " << money ;
    return 0;
}
