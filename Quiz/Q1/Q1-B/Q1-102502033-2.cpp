#include <iostream>

using namespace std;

int main()
{
    int m=0;//宣告整數變數有多少錢
    int r=0;//宣告整數變數缺少的面額

    cout << "How much money: ";
    cin  >> m;
    while(m<=0)//當m小於等於零時,產生出現錯誤的迴圈
    {
        cout << "Out of range\n";
        cout << "How much money: ";
        cin  >> m;
    }

    cout << "What denomination is not enough? ";
    cin  >> r;
    while(r=1 && r!=5 && r!=10 && r!=50 && r!=100 && r!=500 && r!=1000)//當r達成上述條件時,產生出現錯誤的迴圈
    {
        cout << "Out of range!\n";
        cout << "What denomination is not enough? ";
        cin  >> r;
    }
    if(r==1000)//以下的if是當圈少哪種面額時，會產生的不同迴圈
    {
        //先從最大面額開始取餘數，最後除你所要的面額便能獲得張數，以下類推
        cout << "1000: " << 0 << "\n";
        cout << "500: "  << m/500 << "\n";
        cout << "100: "  << m%500/100 << "\n";
        cout << "50: "   << m%500%100/50 << "\n";
        cout << "10: "   << m%500%100%50/10 << "\n";
        cout << "5: "    << m%500%100%50%10/5 << "\n";
        cout << "1: "    << m%500%100%50%10%5/1 << "\n";
    }
    else if(r==500)
    {
        cout << "1000: " << m/1000 << "\n";
        cout << "500: "  << 0 << "\n";
        cout << "100: "  << m%1000/100 << "\n";
        cout << "50: "   << m%1000%100/50 << "\n";
        cout << "10: "   << m%1000%100%50/10 << "\n";
        cout << "5: "    << m%1000%100%50%10/5 << "\n";
        cout << "1: "    << m%1000%100%50%10%5/1 << "\n";
    }
    else if(r=100)
    {
        cout << "1000: " << m/1000 << "\n";
        cout << "500: "  << m%1000/500 << "\n";
        cout << "100: "  << 0 << "\n";
        cout << "50: "   << m%1000%500/50 << "\n";
        cout << "10: "   << m%1000%500%50/10 << "\n";
        cout << "5: "    << m%1000%500%50%10/5 << "\n";
        cout << "1: "    << m%1000%500%50%10%5/1 << "\n";
    }
    else if(r=50)
    {
        cout << "1000: " << m/1000 << "\n";
        cout << "500: "  << m%1000/500 << "\n";
        cout << "100: "  << m%1000%500/100 << "\n";
        cout << "50: "   << 0 << "\n";
        cout << "10: "   << m%1000%500%100/10 << "\n";
        cout << "5: "    << m%1000%500%100%10/5 << "\n";
        cout << "1: "    << m%1000%500%100%10%5/1 << "\n";
    }
    else if(r=10)
    {
        cout << "1000: " << m/1000 << "\n";
        cout << "500: "  << m%1000/500 << "\n";
        cout << "100: "  << m%1000%500/100 << "\n";
        cout << "50: "   << m%1000%500%100/50 << "\n";
        cout << "10: "   << 0 << "\n";
        cout << "5: "    << m%1000%500%100%50/5 << "\n";
        cout << "1: "    << m%1000%500%100%50%5/1 << "\n";
    }
    else if(r=5)
    {
        cout << "1000: " << m/1000 << "\n";
        cout << "500: "  << m%1000/500 << "\n";
        cout << "100: "  << m%1000%500/100 << "\n";
        cout << "50: "   << m%1000%500%100/50 << "\n";
        cout << "10: "   << m%1000%500%100%50/10 << "\n";
        cout << "5: "    << 0 << "\n";
        cout << "1: "    << m%1000%500%100%50%10/1 << "\n";
    }

    return 0;
}
