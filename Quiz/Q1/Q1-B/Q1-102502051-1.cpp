#include<iostream>

using namespace std;

int main()
{
    int a=0 , b=0 , x=0 , y=0 ;//宣告兩個變數以及設定最大公因數和最小公倍數

    cout<<"Please enter first number( >0 ) : " ; //輸入第一個數字
    cin>> a ;
     while( a <= 0 )//判斷第一個數字是否>0，若<=0則重新輸入
    {   cout<<"Out of range! "  << endl ;
        cout<<"Please enter first number( >0 ) : " ;
        cin>> a ;
    }
    cout<<"Please enter second number( >0 ) : " ; //輸入第二個數字
    cin>> b ;
    while( b <= 0 )  //判斷第二個數字是否>0，若<=0則重新輸入
 {  cout<<"Out of range! " << endl;
    cout<<"Please enter second number( >0 ) : " ;
    cin>> b ;
 }
   for(int i=1; i<=a; i++) //最大公因數不可能大於兩個變數所以只要小於a或b即可
    {if( a%i==0 && b%i==0) //兩數都被i除盡時 為兩數之因數
    x =i ;} //設x=i 進入迴圈 直到等於最後一個i 就是最大公因數
    y =(x)*(a/x)*(b/x);//最小公倍數=(最大公因數)乘(a跟b被最大公因數除後的兩互質數)
    cout<<"The greatest common divisor ： "<< x <<endl;
    cout<<"The least common divisor ： "<< y << endl ;
    return 0;


}
