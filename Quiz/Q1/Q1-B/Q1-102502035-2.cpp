#include <iostream>
using namespace std;
int main()
{
    int money =0;//宣告變數並=0
    int paper =0;//宣告變數並=0
    int none =0;//宣告變數並=0
    cout << "How much money: ";//提示輸入錢
    cin >> money;//輸入錢
    while (money<=0)//判斷數字是否符合條件
    {
        cout << "Out of range!" << endl;//提示數字不符合條件
        cout << "How much money: ";//提示輸入錢
        cin >> money;//輸入錢
    }
    cout << "What denomination is not enough? ";
    cin >> none;
    while (none!=5 && none!=10 && none!=50 && none!=100 && none!=500 && none!=1000)
    {
        cout << "Out of range!" << endl;//提示數字不符合條件
        cout << "What denomination is not enough? ";
        cin >> none;
    }
    if (none==1000);//沒錢就不換
    else
        while (money-1000>=0)//換1000元鈔
        {
            money=money-1000;
            paper++;
        }
    cout << "1000: " << paper << endl;//顯示換鈔結果
    paper =0;
    if (none==500);//沒錢就不換
    else
        while (money-500>=0)//換500元鈔
        {
            money=money-500;
            paper++;
        }
    cout << "500: " << paper << endl;//顯示換鈔結果
    paper =0;
    if (none==100);//沒錢就不換
    else
        while (money-100>=0)//換100元鈔
        {
            money=money-100;
            paper++;
        }
    cout << "100: " << paper << endl;//顯示換鈔結果
    paper =0;
    if (none==50);//沒錢就不換
    else
        while (money-50>=0)//換50元鈔
        {
            money=money-50;
            paper++;
        }
    cout << "50: " << paper << endl;//顯示換鈔結果
    paper =0;
    if (none==10);//沒錢就不換
    else
        while (money-10>=0)//換10元鈔
        {
            money=money-10;
            paper++;
        }
    cout << "10: " << paper << endl;//顯示換鈔結果
    paper =0;
    if (none==5);//沒錢就不換
    else
        while (money-5>=0)//換5元鈔
        {
            money=money-5;
            paper++;
        }
    cout << "5: " << paper << endl;//顯示換鈔結果
    paper =0;
    while (money-1>=0)//換1元鈔
    {
        money=money-1;
        paper++;
    }
    cout << "1: " << paper;//顯示換鈔結果
    return 0;
}
