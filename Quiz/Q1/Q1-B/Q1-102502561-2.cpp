#include<iostream>
using namespace std;
int main()
{
    int money;
    int a;  //1000元
    int b;  // 500元
    int c;  // 100元
    int d;  //  50元
    int e;  //  10元
    int f;  //   5元
    int g;  //   1元
    int ld; //lack denomination (不夠的鈔票面額)

    cout << "How much money: ";
    cin >> money;
    while(money<=0)         //此迴圈為確認輸入money符合題目範圍
    {
        cout << "Out of range!\n";
        cout << "How much money: ";
        cin >> money;
    }
    cout <<"what denomination is not enough? ";
    cin >> ld;
    while( ld!=1000 && ld!=500 && ld!=100 && ld!=50 && ld!=10 && ld!=5 && ld!=1)//迴圈會在輸入不夠的鈔票或硬幣面額不存在時要求從新輸入
    {
        cout << "Out of range!\n";
        cout << "what denomination is not enough? ";
        cin >> ld;
    }

    a =  money/1000;                                //money/1000的整數，即有幾張1000元鈔票
    if(ld==1000)                                    //如果不夠的面額是1000則使a=0(a是一千元鈔票張數)
        a = 0;
    b = (money-a*1000)/500;                         //money/500的整數，即有幾張500元鈔票
    if(ld==500)                                     //如果不夠的面額是500則使b=0(b是五百元鈔票張數)
        b = 0;
    c = (money-a*1000-b*500)/100;                   //money/100的整數，即有幾張100元鈔票
    if(ld==100)                                     //如果不夠的面額是100則使c=0(c是一百元鈔票張數)
        c = 0;
    d = (money-a*1000-b*500-c*100)/50;              //money/50的整數，即有幾個50元硬幣
    if(ld==50)                                      //如果不夠的面額是50則使d=0(d是五十元硬幣數)
        d = 0;
    e = (money-a*1000-b*500-c*100-d*50)/10;         //money/10的整數，即有幾個10元硬幣
    if(ld==10)                                      //如果不夠的面額是10則使e=0(e是十元硬幣數)
        e = 0;
    f = (money-a*1000-b*500-c*100-d*50-e*10)/5;     //money/5的整數，即有幾個5元硬幣
    if(ld==5)                                       //如果不夠的面額是5則使f=0(f是五元硬幣數)
        f = 0;
    g = (money-a*1000-b*500-c*100-d*50-e*10-f*5);   //money/1的整數，即有幾個1元硬幣
    cout << "1000: " << a << "\n";
    cout << "500: " << b << "\n";
    cout << "100: " << c << "\n";
    cout << "50: " << d << "\n";
    cout << "10: " << e << "\n";
    cout << "5: " << f << "\n";
    cout << "1:" << g;

    return 0;
}
