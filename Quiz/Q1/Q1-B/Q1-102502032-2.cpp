#include <iostream>
using namespace std;

int main ()
{
    //varible decalaration and initialization
    int unit[7];                      //[0]-1000 [1]-500 [2]-100 and so on
    unit[0] = 1000;
    unit[1] = 500;
    unit[2] = 100;
    unit[3] = 50;
    unit[4] = 10;
    unit[5] = 5;
    unit[6] = 1;
    int much[7];                     //save number of each unit
    for ( int i = 0; i <= 6; i ++)
        much[i] = 0;
    int input = 0;                   //how much money
    int n_enough = 0;                //which isn't enough
    int counter = 0;                 //previent inifite loop

    //ask how much money
    do
    {
        cout << "How much money: ";
        cin >> input;
    }
    while ( input <= 0 && cout << "Out of range!" << endl);

    //ask which is not enough and judge if it's out of range
    do
    {
        cout << "What denomination is not enough? ";
        cin >> n_enough;

        for ( int i = 0; i <=5; i++ )       //exclusive $1
        {
            if ( n_enough == unit[i] )
            {
                much[i] = -1;
                counter = 1001;
                break;                      //break for
            }
        }
        counter ++;
        if ( counter <= 1000)
            cout << "Out of range!" << endl;
    }
    while( counter <= 1000 );

    //compute how much does each unit need
    //output result
    for ( int i = 0; i <= 6; i ++ )
    {
        if ( much[i] == -1)
        {
            cout << unit[i] << ": 0" << endl;
            continue;
        }
        much[i] = input / unit[i];
        input -= much[i] * unit[i];
        cout << unit[i] << ": " << much[i] << endl;
    }

    return 0;
}
