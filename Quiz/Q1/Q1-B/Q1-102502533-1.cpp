#include<iostream>
using namespace std;

int main()
{
    int first_number;//宣告第一個整數
    int second_number;//需告第二個整數

    cout<<"Please enter first number( >0 ):";//要求輸入第1個數
    cin>>first_number;

    while (first_number<=0)//限制第一個數需大於0.否則重新輸入.
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>first_number;
    }

    cout<<"Please enter second number( >0 ):";//要求輸入第2個數
    cin>>second_number;

    while (second_number<=0)//限制第2個數需大於0.否則重新輸入.
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>second_number;
    }

    int a=first_number;
    int b=second_number;
    int divisor;
    int multiple;

    if(a>b)//計算最大公因數
    {
        a=a-b;
        divisor=a;
    }

    else if(b>a)
    {
        b=b-a;
        divisor=b;
    }
    else
    {
        divisor=a;
    }

    multiple=first_number * second_number / divisor;//計算最小公倍數

    cout<<"The greatest common divisor: "<<divisor<<endl;//顯示最大公因數
    cout<<"The least common multiple: "<<multiple<<endl;//顯示最小公倍數

    return 0;
}
