#include <iostream>
using namespace std;

int main()
{
    int integer1=0; //call variable
    int integer2=0; //call variable
    int x;// call variale for HCF
    int y;// call variable
    int z;//call variable for LCM

    while(integer1<=0) //data validation for 1st integer
    {
        cout<<"Please enter first number( >0 ): ";//display words
        cin>>integer1;//input number
        if(integer1<=0) //defind the input is a inproper input
        {
            cout<<"Out of range!"<<endl;
        }
    }

    while(integer2<=0)
    {
        cout<<"Please enter second number( >0 ): ";//display words
        cin>>integer2;//input
        if(integer2<=0)//defind the input is a inproper input
        {
            cout<<"Out of range!"<<endl;
        }
    }

    if(integer1>integer2) //calculate for HCF
    {
        y=integer2;
        x=integer1-integer2;
        while(integer1%x!=0&&integer2%x!=0)
        {
            y=x;
            x=y-x; //continues subtration
        }

    }
    else if(integer2>integer1)
    {
        y=integer1;
        x=integer2-integer1;
        while(integer1%x!=0&&integer2%x!=0)
        {
            y=x;
            x=y-x; //continues subtration
        }
    }
    else if(integer1==integer2)
    {
        x=integer1;
    }

    z=(integer1*integer2)/x; //calculate for LCM
    cout<<"The greatest common divisor: "<<x<<endl; //output HCF
    cout<<"The least common multiple: "<<z<<endl; //output LCM

    return 0;
}
