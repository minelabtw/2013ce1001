#include<iostream>
using namespace std;
int main()
{
    int number1;    //讀入的數字一
    int number2;    //讀入的數字二
    int Tmax;       //用來暫存的變數(會在最大公因數用到)
    int numberA;    //這是一開始讀入的數字二，原本的number2後來會被覆蓋掉
    int numberB;    //這是一開始讀入的數字一，原本的number1後來會被覆蓋掉

    cout << "Please enter first number( >0 ): ";
    cin >> number1;
    while(number1<=0)   //這個迴圈的內容會讀入數字一並確認其質在規定範圍內
    {
        cout << "Out of range!\n";
        cout << "Please enter first number( >0 ): ";
        cin >> number1;
    }

    cout << "Please enter second number( >0 ): ";
    cin >> number2;
    while(number2<=0)   //這個迴圈的內容會讀入數字二並確認其質在規定範圍內
    {
        cout << "Out of range!\n";
        cout << "Please enter second number( >0 ): ";
        cin >> number2;
    }
    numberA = number2;  //先存取number2的質，因為等等會用別的質覆蓋掉
    numberB = number1;  //先存取number1的質，因為等等會用別的質覆蓋掉
    while(number1%number2!=0)   //這迴圈會做最大公因數，利用輾轉相除法 除到number1%number2質=0才停止
    {
        Tmax = number1%number2;
        number1 = number2;
        number2 = Tmax;
    }
    cout << "The greatest common divisor: " << number2; //輸出"最大公因數"
    cout << "\n";                                       //換行
    cout << "The leastest common multiple: " << (numberA*numberB)/number2;  //輸出"最小公倍數"

    return 0;
}

