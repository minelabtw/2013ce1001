#include <iostream>
using namespace std;

int main()
{
    int money,lack;//宣告整數,分別為輸入的金錢和缺少的面額
    while(1) //處理輸入的金額,如果超過範圍,要求重新輸入
    {
        cout << "How much money: ";
        cin >> money;
        if(money>0)
            break;
        else
            cout << "Out of range!\n";
    }
    cout << "What denomination is not enough? ";//詢問缺少的面額
    cin >> lack;
    cout << "1000: ";
    if(lack==1000)//計算1000的張數,如果是缺少的面額輸出0
        cout << "0\n";
    else
    {
        cout << money/1000 <<endl;
        money %= 1000;
    }
    cout << "500: ";
    if(lack==500)//計算500的張數,如果是缺少的面額輸出0
        cout << "0\n";
    else
    {
        cout << money/500 <<endl;
        money %= 500;
    }
    cout << "100: ";
    if(lack==100)//計算100的張數,如果是缺少的面額輸出0
        cout << "0\n";
    else
    {
        cout << money/100 <<endl;
        money %= 100;
    }
    cout << "50: ";
    if(lack==50)//計算50的個數,如果是缺少的面額輸出0
        cout << "0\n";
    else
    {
        cout << money/50 <<endl;
        money %= 50;
    }
    cout << "10: ";
    if(lack==10)//計算10的個數,如果是缺少的面額輸出0
        cout << "0\n";
    else
    {
        cout << money/10 <<endl;
        money %= 10;
    }
    cout << "5: ";
    if(lack==5)//計算5的個數,如果是缺少的面額輸出0
        cout << "0\n";
    else
    {
        cout << money/5 <<endl;
        money %= 5;
    }
    cout << "1: " << money <<endl;//輸出剩餘的1元個數
}
