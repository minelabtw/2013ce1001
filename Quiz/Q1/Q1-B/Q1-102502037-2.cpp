#include <iostream>

using namespace std;

int main()
{
    int w,h,a,b,c,d,e,f,g;
    cout<<"How much money: ";//輸出說明到螢幕上
    cin>>w; //輸入w
    while (w <= 0) //判別w是否合格 否則重新輸入
    {
        cout << "Out of range!" << endl << "How much money: ";
        cin >> w;
    }
    cout<<"What denomination is not enough? "; //輸出說明到螢幕上
    cin>>h; //輸入h
    while (h != 1000 && h != 500 && h!=100 && h!=50 && h!=10 && h!=5) //判別h是否合格 否則重新輸入
    {
        cout << "Out of range!" << endl << "What denomination is not enough? ";
        cin >> h;
    }


    a=w/1000;    //將W依序分解
    b=(w-a*1000)/500;
    c=(w-a*1000-b*500)/100;
    d=(w-a*1000-b*500-c*100)/50;
    e=(w-a*1000-b*500-c*100-d*50)/10;
    f=(w-a*1000-b*500-c*100-d*50-e*10)/5;
    g=(w-a*1000-b*500-c*100-d*50-e*10-f*5);

    if(h==1000) //當某一面額紙鈔沒有時 自動由較低單位紙鈔取代
    {
        a=0;
        b=w/500;
    }
    else if(h==500)
    {
        b=0;
        c=(w-a*1000)/100;
    }
    else if(h==100)
    {
        c=0;
        d=(w-a*1000-b*500)/50;
    }
    else if(h==50)
    {
        d=0;
        e=(w-a*1000-b*500-c*100)/10;
    }
    else if(h==10)
    {
        e=0;
        f=(w-a*1000-b*500-c*100-d*50)/5;
    }
    else
    {
        f=0;
        g=(w-a*1000-b*500-c*100-d*50-e*10);
    }
    //輸出結果
    cout<<"1000: "<<a<<endl<<"500: "<<b<<endl<<"100: "<<c<<endl<<"50: "<<d<<endl<<"10: "<<e<<endl<<"5: "<<f<<endl<<"1: "<<g;//輸出結果
    return 0;
}
