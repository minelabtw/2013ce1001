#include <iostream>

using namespace std;

int main()
{
    int mon,t[7]={1000,500,100,50,10,5,1},noen,flag=false;     //mon金錢,t[7]幣值種類
    do                                                         //輸入金錢直到在正確範圍
    {
        cout<<"How much money: ";
        cin>>mon;
        if(mon<=0)
            cout<<"Out of range!\n";
    }while(mon<=0);
    do                                                         //輸入不足面額直到在正確範圍
    {
        flag=false;
        cout<<"What denomination is not enough? ";
        cin>>noen;
        for(int i=0;i<=5;i++)
        {
            if(noen==t[i])
            {
                flag=true;
                break;
            }
        }
            if(!flag)
               cout<<"Out of range!\n";

    }while(!flag);

    for(int i=0;i<=6;i++)                                      //輸出各項幣值數目
    {
        if(noen==t[i])
        {
           cout<<t[i]<<": "<<0<<endl;
        }
        else
        {
           cout<<t[i]<<": "<<mon/t[i]<<endl;
           mon=mon%t[i];
        }
    }
    return 0;
}
