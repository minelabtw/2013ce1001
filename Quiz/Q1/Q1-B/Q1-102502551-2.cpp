#include <iostream>

using namespace std;

int main()
{
    int money,trouble;                                              //宣告整數

    cout<<"How much money: ";
    cin>>money;
    while(money<=0)                                         //判斷是否在範圍內
    {
        cout<<"Out of range!"<<endl<<"How much money: ";
        cin>>money;
    }
    cout<<"What denomination is not enough? ";
    cin>>trouble;
    while(trouble!=1000 && trouble!=500 && trouble!=100 && trouble!=50 && trouble!=10 && trouble!=5)        //判斷是否在範圍內
    {
        cout<<"Out of range!"<<endl<<"What denomination is not enough? ";
        cin>>trouble;
    }

    int a=0,b=0,c=0,d=0,e=0,f=0;                            //宣告6個整數

    while(money>=1000)                                      //慢慢減
    {
        money=money-1000;
        a++;
    }
    while(money>=500)
    {
        money=money-500;
        b++;
    }
    while(money>=100)
    {
        money=money-100;
        c++;
    }
    while(money>=50)
    {
        money=money-50;
        d++;
    }
    while(money>=10)
    {
        money=money-10;
        e++;
    }
    while(money>=5)
    {
        money=money-5;
        f++;
    }
    if(trouble==1000)                                               //判斷什麼沒有並做應對
    {
        b=b+2*a;
        a=0;
    }
    if(trouble==500)
    {
        c=c+5*b;
        b=0;
    }
    if(trouble==100)
    {
        d=d+2*c;
        c=0;
    }
    if(trouble==50)
    {
        e=e+5*d;
        d=0;
    }
    if(trouble==10)
    {
        f=f+2*e;
        e=0;
    }
    if(trouble==5)
    {
        money=money+5*f;
        f=0;
    }

    cout<<"1000: "<<a<<endl<<"500: "<<b<<endl<<"100: "<<c<<endl<<"50: "<<d<<endl<<"10: "<<e<<endl<<"5: "<<f<<endl<<"1: "<<money;
    return 0;
}
