#include <iostream>

using namespace std;

int main()
{
    int integer1;                                                       //宣告變數
    int integer2;

    cout<<"Please enter first number( >0 ): ";
    cin>>integer1;
    while(integer1<=0)                                                  //限制範圍進入迴圈
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter first number( >0 ): ";
        cin>>integer1;
    }

    cout<<"Please enter second number( >0 ): ";
    cin>>integer2;
    while(integer2<=0)                                                  //限制範圍進入迴圈
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter second number( >0 ): ";
        cin>>integer2;
    }


    int x;
    int y;

    x=integer1;
    y=integer2;

    if(x>y)                                                             //判斷輸入數字大小
        while(x!=y)                                                     //判斷輸入數字不為相等即進入迴圈
        {
            if(x>y)
            {
                x=x-y;
            }
            else
            {
                y=y-x;
            }

        }
    else if(y>x)                                                        //判斷輸入數字大小
        while(x!=y)                                                     //判斷輸入數字不為相等即進入迴圈
        {
            if(x>y)
            {
                x=x-y;
            }
            else
            {
                y=y-x;
            }
        }

    else
    {
        x=integer1;
        y=integer2;
    }


    cout<<"The greastest common divisor: "<<x<<endl;
    cout<<"The least common mulltiple: "<<integer1*integer2/x<<endl;






    return 0;
}
