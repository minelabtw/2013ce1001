#include<iostream>
using namespace std;
int main()
{
    int money;                                              //輸入的數
    int denomination[7]= {1000,500,100,50,10,5,1};          //錢的面額
    int loss=-1;                                            //缺少哪種錢

    do                                                      //輸入並檢查
    {
        cout << "How much money: ";
        cin >> money;
        if(money<=0)
            cout << "Out of range!" << endl;
    }
    while(money<=0);

    while(loss == -1)
    {
        cout << "What denomination is not enough? ";
        cin >> loss;
        if( (loss !=1000 && loss != 500 && loss != 100 && loss != 50 && loss != 10 && loss != 5) || loss == 1)
            loss=-1;                                        //檢查輸入

        if(loss == -1)
            cout << "Out of range!"<< endl;
    }

    for(int i=0; i<7; i++)                                  //輸出找的錢
    {
        if(denomination[i]!=loss)
        {
            cout << denomination[i] << ": " << money/denomination[i] << endl;
            money%=denomination[i];
        }
        else
            cout << denomination[i] << ": 0" << endl;
    }


    return 0;
}
