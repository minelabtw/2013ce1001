#include<iostream>
using namespace std;

int main()
{
    int a=0;     //宣告a為整數
    int b=0;     //宣告b為整數

    cout<<"Please enter first number( >0 ): ";
    cin>>a;
    while(a<=0)   //設定當a小於等於零時
    {
        cout<<"Out of range!\nPlease enter first number( >0 ): ";
        cin>>a;
    }
    cout<<"Please enter second number( >0 ): ";
    cin>>b;
    while(b<=0)   //設定當b小於等於零時
    {
        cout<<"Out of range!\nPlease enter second number( >0 ): ";
        cin>>b;
    }
    cout<<"The greatest common divisor: "<<endl;
    cout<<"The least common multiple: ";
    return 0;
}
