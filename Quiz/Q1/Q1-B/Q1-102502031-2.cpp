#include <iostream>

using namespace std;

int main()
{
    int money=0;
    int notenough=0;
    int n=0;         //variable

    //money
    cout << "How much money: ";
    cin >> money;
    while (money<=0)
    {
        cout << "Out of range!\n";
        cout << "How much money: ";
        cin >> money;
    }

    //notenough
    cout << "What denomination is not enough? ";
    cin >> notenough;
    while (notenough!=1000&&notenough!=500&&notenough!=100&&notenough!=50&&notenough!=10&&notenough!=5)
    {
        cout << "Out of range!\n";
        cout << "What denomination is not enough? ";
        cin >> notenough;
    }

    //在不足夠的面額後印出0，其他的面額後印出該找的鈔票張數或硬幣個數
    if (notenough==1000)  //不足額的是1000
    {
        n=money;
        cout << "1000: " << "0" << endl;
        cout << "500: " << n/500 << endl;
        n=money%500;
        cout << "100: " << n/100 << endl;
        n=money%100;
        cout << "50: " << n/50 << endl;
        n=money%50;
        cout << "10: " << n/10 << endl;
        n=money%10;
        cout << "5: " << n/5 << endl;
        cout << "1: " << n%5 << endl;
    }
    else if (notenough==500)  //不足額的是500
    {
        n=money;
        cout << "1000: " << n/1000 << endl;
        n=money%1000;
        cout << "500: " << "0" << endl;
        cout << "100: " << n/100 << endl;
        n=money%100;
        cout << "50: " << n/50 << endl;
        n=money%50;
        cout << "10: " << n/10 << endl;
        n=money%10;
        cout << "5: " << n/5 << endl;
        cout << "1: " << n%5 << endl;
    }
    else if (notenough==100)  //不足額的是100
    {
        n=money;
        cout << "1000: " << n/1000 << endl;
        n=money%1000;
        cout << "500: " << n/500 << endl;
        n=money%500;
        cout << "100: " << "0" << endl;
        cout << "50: " << n/50 << endl;
        n=money%50;
        cout << "10: " << n/10 << endl;
        n=money%10;
        cout << "5: " << n/5 << endl;
        cout << "1: " << n%5 << endl;
    }
    else if (notenough==50)  //不足額的是50
    {
        n=money;
        cout << "1000: " << n/1000 << endl;
        n=money%1000;
        cout << "500: " << n/500 << endl;
        n=money%500;
        cout << "100: " << n/100 << endl;
        n=money%100;
        cout << "50: " << "0" << endl;
        cout << "10: " << n/10 << endl;
        n=money%10;
        cout << "5: " << n/5 << endl;
        cout << "1: " << n%5 << endl;
    }
    else if (notenough==10)  //不足額的是10
    {
        n=money;
        cout << "1000: " << n/1000 << endl;
        n=money%1000;
        cout << "500: " << n/500 << endl;
        n=money%500;
        cout << "100: " << n/100 << endl;
        n=money%100;
        cout << "50: " << n/50 << endl;
        n=money%50;
        cout << "10: " << "0" << endl;
        cout << "5: " << n/5 << endl;
        cout << "1: " << n%5 << endl;
    }
    else if (notenough==5)  //不足額的是5
    {
        n=money;
        cout << "1000: " << n/1000 << endl;
        n=money%1000;
        cout << "500: " << n/500 << endl;
        n=money%500;
        cout << "100: " << n/100 << endl;
        n=money%100;
        cout << "50: " << n/50 << endl;
        n=money%50;
        cout << "10: " << n/10 << endl;
        n=money%10;
        cout << "5: " << "0" << endl;
        cout << "1: " << n << endl;
    }


    return 0;
}
