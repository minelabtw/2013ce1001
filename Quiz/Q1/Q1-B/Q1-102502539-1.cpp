#include <iostream>

using namespace std;

int main()
{
    int number1;    //宣告變數
    int number2;
    int a;
    int b;

    cout << "Please enter first number( >0 ): " ;
    cin >> number1;
    while ( number1 < 1 )   //限制範圍
    {
        cout << "Out of range!\n" << "Please enter first number( >0 ): " ;
        cin >> number1;
    }

    cout << "Please enter second number( >0 ): " ;
    cin >> number2;
    while ( number2 < 1 )   //限制範圍
    {
        cout << "Out of range!\n" << "Please enter second number( >0 ): " ;
        cin >> number2;
    }

    a = number1;
    b = number2;

    if ( a != b )   //求公因數
    {
        while ( a > b )
            a = a - b;

        while ( b > a )
            b = b - a;
    }

    if ( a > b )
        a = b;

    cout << "The greatest common divisor: " << a << endl;
    cout << "The least common multiple: " << number2 * number1 / a;

    return 0;
}
