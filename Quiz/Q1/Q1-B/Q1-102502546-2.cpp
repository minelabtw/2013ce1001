#include <iostream>
using namespace std;

int main()
{
    int a=0;//使a初始值為0
    cout <<"How much money: ";//輸出多少錢
    cin >>a;//輸入a
    while(a<=0)//當a<=0時，表示超出範圍且重新輸入
    {
        cout <<"Out of range!\nHow much money: ";
        cin >>a;
    }
    cout <<"1000: "<<a/1000<<endl;//利用int不會出現小數點 則直接用a%"某值"來輸出
    cout <<"500: "<<a%1000/500<<endl;
    cout <<"100: "<<a%500/100<<endl;
    cout <<"50: "<<a%100/50<<endl;
    cout <<"10: "<<a%50/10<<endl;
    cout <<"5: "<<a%10/5<<endl;
    cout <<"1: "<<a%5/1<<endl;

    return 0;
}
