#include <iostream>
#include <string>

using namespace std;

int main()
{
    int a , b;
    int gcd , lcm;
    string trash;

    while(true) // input first number
    {
        cout << "Please enter first number( >0 ): ";
        if(!(cin >> a))
        {
            cin.clear();
            getline(cin , trash);
        }
        else if(a > 0)
            break;

        cout << "Out of range!" << endl;
    }

    while(true) // input second number
    {
        cout << "Please enter second number( >0 ): ";
        if(!(cin >> b))
        {
            cin.clear();
            getline(cin , trash);
        }
        else if(b > 0)
            break;

        cout << "Out of range!" << endl;
    }

    int a1 = a , b1 = b; // declare 2 varience to use 輾轉相除法
    while(a1 != 0 && b1 != 0) // 輾轉相除法
    {
        if(a1 - b1 >= 0)
            a1 %= b1;
        else
            b1 %= a1;
    }

    if(a1 == 0) // find the gcd
        gcd = b1;
    else
        gcd = a1;

    lcm = a * b / gcd; // compute lcm

    cout << "The greatest common divisor: " << gcd << endl;
    cout << "The least common multiple: " << lcm << endl;

    return 0;
}
