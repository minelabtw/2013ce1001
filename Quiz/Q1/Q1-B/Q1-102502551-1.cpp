#include <iostream>

using namespace std;

int main()
{
    int fn,sn;                                                             //宣告2整數

    cout<<"Please enter first number( >0 ): ";
    cin>>fn;
    while(fn<=0)                                                           //判斷是否在範圍內
    {
        cout<<"Out of range!"<<endl<<"Please enter first number( >0 ): ";
        cin>>fn;
    }

    cout<<"Please enter second number( >0 ): ";
    cin>>sn;
    while(sn<=0)
    {
        cout<<"Out of range!"<<endl<<"Please enter second number( >0 ): ";
        cin>>sn;
    }

    int a=fn,b=sn;

    while(a!=b)                                                             //找最大公因數
    {
        if(a>=b)
        {
            a=a-b;
        }
        if(b>a)
        {
            b=b-a;
        }
    }
    cout<<"The greatest common divisor: "<<a<<endl;
    cout<<"The least common multiple: "<<fn*sn/a;

    return 0;
}
