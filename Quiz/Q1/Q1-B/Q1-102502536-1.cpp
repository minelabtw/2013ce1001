#include<iostream>

using namespace std;

int main()
{
    int n1=0,n2=0;//宣告兩個變數

    cout << "Please enter first number( >0 ): ";//輸出字串
    cin  >> n1;//輸入值給n1

    while(n1<=0)//當n1小於等於時進入迴圈
    {
        cout << "Out of range!" << endl;//輸出字串並換行
        cout << "Please enter first number( >0 ): ";//輸出字串
        cin  >> n1;//輸入值給n1
    }

    cout << "Please enter second number( >0 ): ";//輸出字串
    cin  >> n2;//輸入值給n2

    while(n2<=0)//當n2小於等於0時進入迴圈
    {
        cout << "Out of range!" << endl;//輸出字串並換行
        cout << "Please enter second number( >0 ): ";//輸出字串
        cin  >> n2;//輸入值給n2
    }

    int f=n1*n2;//宣告變數f等於n1*n2

    cout << "The greatest common divisor: ";//輸出字串

    while(n1!=n2)//當n1不等於n2時進入迴圈
    {
        if(n1>=n2)//n1大於等於n2時
        {
            n1=n1-n2;//新n1是原n1-n2
        }
        else//n2大於n1時
        {
            n2=n2-n1;//新n2是原n2-n1
        }
    }

    cout << n1 << endl;//輸出重複相減後彼此相等的n1並換行
    cout << "The least common multiple: ";//輸出字串
    cout << f/n1 << endl;//輸出f除n1並換行

    return 0;

}
