#include <iostream>

using namespace std;

int main()
{
    double quiz1 = 0;//宣告一個名稱為quiz1的變數，並初始化其值為0
    double quiz2 = 0;//宣告一個名稱為quiz2的變數，並初始化其值為0
    double sum = 0;//宣告一個名稱為sum的變數，並初始化其值為0
    cout << "Quiz1 point: ";//輸出字串Quiz1 point:
    cin >> quiz1;//輸入quiz1
    while (quiz1<0||quiz1>10)//當quiz1<0或quiz1>10
    {
        cout << "Out of range!" << endl;//輸出字串Out of range!
        cout << "Quiz1 point: ";//輸出字串Quiz1 point:
        cin >> quiz1;//輸入quiz1
        if (quiz1==10)//判斷quiz1是否=10
            cout << "Result: " << "AC" << endl;//輸出字串Result: AC
        else if (quiz1==9)//判斷quiz1是否=9
            cout << "Result: " << "II/WC" << endl;//輸出字串Result: II/WC
        else if (quiz1==8)//判斷quiz1是否=8
            cout << "Result: " << "WA" << endl;//輸出字串Result: WA
        else if (quiz1==5)//判斷quiz1是否=5
            cout << "Result: " << "TLE/MLE/OLE" << endl;//輸出字串Result: TLE/MLE/OLE
        else if (quiz1==3)//判斷quiz1是否=3
            cout << "Result: " << "NA" << endl;//輸出字串Result: NA
        else if (quiz1==2)//判斷quiz1是否=2
            cout << "Result: " << "CE" << endl;//輸出字串Result: CE
        else if (quiz1==0)//判斷quiz1是否=0
            cout << "Result: " << "SE" << endl;//輸出字串Result: SE
    }
    cout << "Quiz2 point: ";//輸出字串Quiz2 point:
    cin >> quiz2;//輸入quiz2
    while (quiz2<0||quiz2>10)//當quiz2<0或quiz2>10
    {
        cout << "Out of range!" << endl;//輸出字串Out of range!
        cout << "Quiz2 point: ";//輸出字串Quiz2 point:
        cin >> quiz2;//輸入quiz2
        if (quiz2==10)//判斷quiz2是否=10
            cout << "Result: " << "AC" << endl;//輸出字串Result: AC
        else if (quiz2==9)//判斷quiz2是否=9
            cout << "Result: " << "II/WC" << endl;//輸出字串Result: II/WC
        else if (quiz2==8)//判斷quiz2是否=8
            cout << "Result: " << "WA" << endl;//輸出字串Result: WA
        else if (quiz2==5)//判斷quiz2是否=5
            cout << "Result: " << "TLE/MLE/OLE" << endl;//輸出字串Result: TLE/MLE/OLE
        else if (quiz2==3)//判斷quiz2是否=3
            cout << "Result: " << "NA" << endl;//輸出字串Result: NA
        else if (quiz2==2)//判斷quiz2是否=2
            cout << "Result: " << "CE" << endl;//輸出字串Result: CE
        else if (quiz2==0)//判斷quiz2是否=0
            cout << "Result: " << "SE" << endl;//輸出字串Result: SE
    }
    sum = (quiz1 + quiz2) / 2;//計算sum = (quiz1 + quiz2) 除以 2
    cout << "Final grade: " << sum;//輸出字串
    return 0;
}
