#include <iostream>
using namespace std;

int main()
{
    int flag=0, a1=0, d=0, n=0, p=0, input=0;//宣告變數為整數並設定初始值

    while (flag<4)//對應輸出詢問對應輸入
    {
        if (flag==0)
        {
            cout << "a1: ";
            cin >> input;
            a1=input;
        }
        else if (flag==1)
        {
            cout << "d: ";
            cin >> input;
            d=input;
        }
        else if (flag==2)
        {
            cout << "n: ";
            cin >> input;
            n=input;
        }
        else if (flag==3)
        {
            cout << "p: ";
            cin >> input;
            p=input;
        }
        if (input==0)//超出範圍發出警告並重新輸入
            cout << "Out of range!\n";
        else if ((flag==2 || flag==3) && input<0)
            cout << "Out of range!\n";
        else
            flag++;
    }
    int a=a1, i=1, j=1, ai=0, sum=0;//宣告變數為整數並設定初始值
    while (i<=n)
    {
        a=a1+d*(i-1);//等差數列
        if (i!=1)//i看第幾項
        {
            ai=a;
            while (j<p)//j看幾次方
            {
                a=a*ai;
                j++;
            }
            j=1;//初始j
        }
        sum=sum+a;
        cout << a << ",";
        i++;
    }
    cout << "sum: " << sum;
    return 0;
}
