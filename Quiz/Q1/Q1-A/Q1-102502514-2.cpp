#include <iostream>
using namespace std;

int main()
{
    int a1,d,n,an,sum; //宣告首項為a1,公差為d,項數為n,第n項為an,等差級數總和為sum
    int x=1;

    cout <<"Arithmetic progression\na1: ";
    cin >>a1;
    while (a1<=0)
    {
        cout <<"Out of range!\n";
        cout <<"a1: ";
        cin >>a1;
    }

    cout <<"d: ";
    cin >>d;
    while (d<=0)
    {
        cout <<"Out of range!\n";
        cout <<"d: ";
        cin >>d;
    }

    cout <<"n: ";
    cin >>n;
    while (n<=0)
    {
        cout <<"Out of range!\n";
        cout <<"n: ";
        cin >>n;
    }

    while (x<=n) //當x<=n時,依序印出等差級數的各項
    {
        cout <<a1+d*(x-1)<<",";
        x++;
    }
    sum=(a1+(a1+d*(n-1)))*n/2; //印出總和
    cout <<"sum: "<<sum;


    return 0;
}
