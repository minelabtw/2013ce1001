#include <iostream>

using namespace std;

int main()
{
    int number1=0;           //宣告整數變數number1
    int number2=0;
    double grade=0;          //宣告變數grade

    cout<< "Quiz1 point: ";           //輸出字串
    cin>>number1;                     //輸入number1
    while(number1>10 or number1<0)        //當符合情況時執行以下動作
    {
        cout<< "Out of range!"<<endl;
        cin>>number1;
    }
    cout<< "Result: ";
    if(number1==10)              //當number1等於10
        cout<< "AC"<<endl;
    else if(number1==9)           //當number1等於9
        cout<< "II/WC"<<endl;
    else if(number1==8)           //當number1等於8
        cout<< "WA"<<endl;
    else if(number1==5)            //當number1等於5
        cout<< "TLE/MLE/OLE"<<endl;
    else if(number1==3)           //當number1等於3
        cout<< "NA"<<endl;
    else if(number1==2)           //當number1等於2
        cout<< "CE"<<endl;
    else if(number1==0)           //當number1等於0
        cout<< "SE"<<endl;

    cout<< "Quiz2 point: ";
    cin>>number2;
    while(number2>10 or number2<0)
    {
        cout<< "Out of range!"<<endl;
        cin>>number2;
    }
    cout<< "Result: ";
    if(number2==10)
        cout<< "AC"<<endl;
    else if(number2==9)
        cout<< "II/WC"<<endl;
    else if(number2==8)
        cout<< "WA"<<endl;
    else if(number2==5)
        cout<< "TLE/MLE/OLE"<<endl;
    else if(number2==3)
        cout<< "NA"<<endl;
    else if(number2==2)
        cout<< "CE"<<endl;
    else if(number2==0)
        cout<< "SE"<<endl;

    grade=number1*0.5+number2*0.5;        //把number1乘以0.5number2乘以0.5相加的值丟到grade
    cout<< "Final grade: "<<grade;

    return 0;
}
