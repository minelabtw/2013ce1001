#include <iostream>
using namespace std;

int main()
{
    int a = 0;                                  //設定變數a,d,n
    int d = 0;
    int n = 0;

    cout << "Arithmetic progression" << endl;

    cout << "a1: ";
    cin >> a;
    while (a==0)                                //判斷a是否符合所需範圍
    {
        cout << "Out of range!" << endl;
        cout << "a1: ";
        cin >> a;
    }

    cout << "d: ";
    cin >> d;
    while (d==0)                                //判斷d是否符合所需範圍
    {
        cout << "Out of range!" << endl;
        cout << "d: ";
        cin >> d;
    }

    cout << "n: ";
    cin >> n;
    while (n<=0)                                //判斷n是否符合所需範圍
    {
        cout << "Out of range!" << endl;
        cout << "n: ";
        cin >> n;
    }

    for (int i=0;i<n;i++)                       //利用for迴圈累加等差級數並輸出
    {
        cout << (a+d*i) << ",";
    }
    cout << "sum: " << (2*a+d*(n-1))*n/2;       //利用等差級數和公式求和

    return 0;
}
