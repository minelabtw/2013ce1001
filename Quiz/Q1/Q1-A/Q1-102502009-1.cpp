#include<iostream>
using namespace std;

int main()
{
    int quiz1 = 0; // 宣告變數
    int quiz2 = 0;

    cout<<"Quiz1 point: "; //輸入quiz1
    cin>>quiz1;

    while(quiz1<0 or quiz1>10) //若quiz1不在0~10之間,則重新輸入
    {
        cout<<"Out of range!"<<endl;

        cout<<"Quiz1 point: ";
        cin>>quiz1;
    }

    if(quiz1==10) //依照"程式評分規則"輸出結果
        cout<<"Result: AC"<<endl;
    else if(quiz1==9)
        cout<<"Result: II/WC"<<endl;
    else if(quiz1==8)
        cout<<"Result: WA"<<endl;
    else if(quiz1==5)
        cout<<"Result: TLE/MLE/OLE"<<endl;
    else if(quiz1==3)
        cout<<"Result: NA"<<endl;
    else if(quiz1==2)
        cout<<"Result: CE"<<endl;
    else if(quiz1==0)
        cout<<"Result: SE"<<endl;

    cout<<"Quiz2 point: "; //輸入quiz2
    cin>>quiz2;

    while(quiz2<0 or quiz2>10) //若quiz2不在0~10之間,則重新輸入
    {
        cout<<"Out of range!"<<endl;

        cout<<"Quiz2 point: ";
        cin>>quiz2;
    }

    if(quiz2==10) //依照"程式評分規則"輸出結果
        cout<<"Result: AC"<<endl;
    else if(quiz2==9)
        cout<<"Result: II/WC"<<endl;
    else if(quiz2==8)
        cout<<"Result: WA"<<endl;
    else if(quiz2==5)
        cout<<"Result: TLE/MLE/OLE"<<endl;
    else if(quiz2==3)
        cout<<"Result: NA"<<endl;
    else if(quiz2==2)
        cout<<"Result: CE"<<endl;
    else if(quiz2==0)
        cout<<"Result: SE"<<endl;

    cout<<"Final grade: "<<quiz1*0.5+quiz2*0.5<<endl; //輸出兩次小考平均值

    return 0;
}
