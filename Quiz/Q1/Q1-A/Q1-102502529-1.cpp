#include<iostream>
using namespace std ;

int main ()
{
    int a=0 ,b=0 ,counter=0 ;                           //宣告三個變數 並設定其初始值皆為0
    while (counter<=1)                                  //當counter<=1 時進入迴圈
    {
        if(counter==0)                                  //當counter=0時進入if迴圈
        {
            cout<<"Quiz1 point: " ;                     //在螢幕上印出Quiz1 point:
            cin>>a ;                                    //輸入a值
            if(0<=a&&a<=10)                             //當a值在0~10之間 進入if 迴圈
            {
                counter++ ;                             //用來計次的counter加1 則下次輸出完畢後會進入counter=1的if迴圈
                if(a==10)                               //用來評分程式分數 若10分
                    cout<<"Result: AC"<<endl;           //輸出 Result: AC
                else if(a==9)                           //若9分
                    cout<<"Result: II/WC"<<endl;        //輸出 Result: II/WC
                else if(a==8)                           //若8分
                    cout<<"Result: WA"<<endl;           //輸出 Result: WA
                else if(a==5)                           //若5分
                    cout<<"Result: TLE/MLE/OLE"<<endl ; //輸出 Result: TLE/MLE/OLE
                else if(a==3)                           //若3分
                    cout<<"Result: NA"<<endl ;          //輸出 Result: NA
                else if(a==2)                           //若2分
                    cout<<"Result: CE"<<endl ;          //輸出 Result: CE
                else if(a==0)                           //若0分
                    cout<<"Result: SE"<<endl ;          //輸出 Result: SE
            }
            else
                cout<<"Out of range!"<<endl ;           //不符合條件則輸出 Out of range!
        }
        if(counter==1)                                  //當a值符合範圍後 進入此if迴圈
        {
            cout<<"Quiz2 point: ";                      //在螢幕上印出 Quiz2 point:
            cin>>b ;                                    //輸入b值
            if(0<=b&&b<=10)                             //此if迴圈 與第10行的if迴圈判斷方法、概念相同
            {
                counter++;                              //符合後counter加1 下列判斷結束後則跳出while迴圈
                if(b==10)
                    cout<<"Result: AC"<<endl;
                else if(b==9)
                    cout<<"Result: II/WC"<<endl;
                else if(b==8)
                    cout<<"Result: WA"<<endl;
                else if(b==5)
                    cout<<"Result: TLE/MLE/OLE"<<endl ;
                else if(b==3)
                    cout<<"Result: NA"<<endl ;
                else if(b==2)
                    cout<<"Result: CE"<<endl ;
                else if(b==0)
                    cout<<"Result: SE"<<endl ;
            }
            else
                cout<<"Out of range!"<<endl ;          //b值不符合條件則在輸入1次

        }
    }
    cout<<"Final grade: "<<a*0.5+b*0.5 ;
    return 0 ;
}
