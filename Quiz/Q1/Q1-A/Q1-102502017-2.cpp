#include<iostream>
#include<math.h>
using namespace std;
int main()
{
    int a1=0,d=0,n=0,p=0,sum=0;             //宣告型別為 整數(int) 的變數
    float an=0;                             //宣告型別為 浮點數(float) 的變數

    cout << "Arithmetic progression\n";

    while(a1==0)                            //輸入並判斷變數是否符合定義
    {
        cout << "a1: ";
        cin >> a1;
        if(a1==0)
        {
            cout << "Out of range!\n";
        }
    }
    while(d==0)
    {
        cout << "d: ";
        cin >> d;
        if(d==0)
        {
            cout << "Out of range!\n";
        }
    }
    while(n<=0)
    {
        cout << "n: ";
        cin >> n;
        if(n<=0)
        {
            cout << "Out of range!\n";
        }
    }
    while(p<=0)
    {
        cout << "p: ";
        cin >> p;
        if(p<=0)
        {
            cout << "Out of range!\n";
        }
    }
    for(int i=1; i<=n; i++)                 //計算並輸出結果
    {
        an=pow(a1+d*(i-1),p);
        sum+=an;
        cout << an << ",";
    }
    cout << "sum: " << sum;

    /*-------------------Fibonacci sequence-------------------*/
    cout << "\nFibonacci sequence\n";
    int fs[2]= {1,1};                   //宣告型別為 整數(int) 的變數陣列
    n=0;                                //將需要使用的變數歸零
    sum=0;
    while(n<=0)                         //輸入並判斷是否符合定義
    {
        cout << "n: ";
        cin >> n;
        if(n<=0)
        {
            cout << "Out of range!\n";
        }
    }
    for(int i=1,j=0; i<=n; i++)         //計算並輸入結果
    {
        if(i<3)                         //若n<3 數列為1
        {
            cout << "1,";
            sum+=1;
        }
        else                            //或n>=3 數列為前兩項之和
        {
            j=i%2;
            switch(j)
            {
            case 0:
                fs[0]=fs[0]+fs[1];
                cout << fs[0] << ",";
                break;
            case 1:
                fs[1]=fs[0]+fs[1];
                cout << fs[1] << ",";
                break;
            }
            sum+=fs[j];

        }
    }
    cout << "sum: " << sum;                 //輸出數列和
    return 0;
}
