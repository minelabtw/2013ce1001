#include <iostream>
using namespace std;
int main()
{
    float num1;   //宣告一變數
    float num2;   //宣告第二變數
    float num3;   //宣告第三變數
    int num4;     //宣告一整數變數
    float an;     //宣告一實數變數
    int x=1;      //宣告一整數變數x,其初始值為0

    cout<<"Arithmetic sequence"<<endl;   //顯示"Arithmetic sequence",並換行
    cout<<"a1: ";                        //顯示"a1"
    cin>>num1;                           //將num1輸入至電腦
    while(num1<=0)                       //當num1小於等於0,顯示"Out of range!"並重新輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"a1: ";
        cin>>num1;
    }

    cout<<"d: ";   //輸入d值
    cin>>num2;
    while(num2<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"d: ";
        cin>>num2;
    }

    cout<<"n: ";   //輸入n值
    cin>>num3;
    while(num3<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"n: ";
        cin>>num3;
    }

    cout<<"p: ";   //輸入p值
    cin>>num4;
    while(num4<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"p: ";
        cin>>num4;
    }
    while(x<=num3)     //當x小於num3,計算各數列之值
    {
        cout<<num1+num2*(x-1)<<",";
        x++;
    }

    an=num1+num2*(num3-1);
    cout<<"sum: "<<(num1+an)*(num3)*0.5;  //顯示其和
    cout<<endl;
    return 0;
}
