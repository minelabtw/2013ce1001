#include<iostream>

using namespace std;

int main()

{
    int grade1=0;       //宣告名稱為grade1的變數
    int grade2=0;       //宣告名稱為grade2的變數
    int average=0;     //宣告名稱為average的變數

    cout<< "Quiz1 point:";
    cin>> grade1;
    while(grade1>10 || grade1<0)     //迴圈  條件為grade1大於十或小於0
    {
        cout<< "Out of range!\n""Quiz1 point:";
        cin>> grade1;
    }
    if(grade1==10)         //如果grade1的值等於10的情況下
        cout<< "result: AC\n";
    else if(grade1==9)
        cout<< "result: ||/WC\n";
    else if(grade1==8)
        cout<< "result: WA\n";
    else if(grade1==5)
        cout<< "result: TLE/MLE/OLE\n";
    else if(grade1==3)
        cout<< "result: NA\n";
    else if(grade1==2)
        cout<< "result: CE\n";
    else if(grade1==0)
        cout<< "result: SE\n";

    cout<< "Quiz2 point:";
    cin>> grade2;
    while(grade2>10 || grade2<0)    //迴圈  grade2大於十或小於0的情況下
    {
        cout<< "Out of range!\n""Quiz2 point:";
        cin>> grade2;

    }
    if(grade2==10)      //如果grade2的值等於10的情況下
        cout<< "result: AC\n";
    else if(grade2==9)
        cout<< "result: ||/WC\n";
    else if(grade2==8)
        cout<< "result: WA\n";
    else if(grade2==5)
        cout<< "result: TLE/MLE/OLE\n";
    else if(grade2==3)
        cout<< "result: NA\n";
    else if(grade2==2)
        cout<< "result: CE\n";
    else if(grade2==0)
        cout<< "result: SE\n";


    average = (grade1 + grade2)*0.5;
    cout<< "Final grade:" << average << endl;    //將average的值丟給final grade的變數


    return 0;


}
