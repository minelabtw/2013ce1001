#include <iostream>
using namespace std;

int main()
{
    int number1;
    int number2;
    int sum;

    cout<<"Quiz1 point: ";              //輸出
    cin>>number1;

    while (number1>10||number1<0)       //檢測所輸入之數
    {
        cout<<"Out of range! \n";
        cout<<"Quiz1 point: ";
        cin>>number1;
    }

    if (number1==10)                    //不同情況，輸出不同結果
    {
        cout<<"AC\n";
    }
    else if (number1==9)
    {
        cout<<"II/WC\n";
    }
    else if (number1==8)
    {
        cout<<"WA\n";
    }
    else if (number1==5)
    {
        cout<<"TLE/MLE/OLE\n";
    }
    else if (number1==3)
    {
        cout<<"NA\n";
    }
    else if (number1==2)
    {
        cout<<"CE\n";
    }
    else if (number1==0)
    {
        cout<<"SE\n";
    }

    cout<<"Quiz2 point: ";              //輸出
    cin>>number2;

    while (number2>10||number2<0)       //檢測所輸入之數
    {
        cout<<"Out of range! \n";
        cout<<"Quiz2 point: ";
        cin>>number2;
    }

    if (number2==10)                    //不同情況，輸出不同結果
    {
        cout<<"AC\n";
    }
    else if (number2==9)
    {
        cout<<"II/WC\n";
    }
    else if (number2==8)
    {
        cout<<"WA\n";
    }
    else if (number2==5)
    {
        cout<<"TLE/MLE/OLE\n";
    }
    else if (number2==3)
    {
        cout<<"NA\n";
    }
    else if (number2==2)
    {
        cout<<"CE\n";
    }
    else if (number2==0)
    {
        cout<<"SE\n";
    }

    sum = (number1 + number2)/2;            //計算平均

    cout<<"Final grade: "<<sum<<endl;       //輸出




    return 0;
}
