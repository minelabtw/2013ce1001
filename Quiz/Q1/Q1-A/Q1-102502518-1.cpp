#include <iostream>

using namespace std;

int main()
{
    double first_grade=0;                    //宣告型別為整數(double)的first_grade，並初始化其數值為0。
    double second_grade=0;                   //宣告型別為整數(double)的second_grade，並初始化其數值為0。

    cout<<"Quiz1 point: ";                   //將字串"Quiz1 point: "輸出到螢幕上。
    cin>>first_grade;                        //從鍵盤輸入，並將輸入的值給first_grade。

    while (first_grade<0 or first_grade>10)  //使用while迴圈(當first_grade<0 或 first_grade>1)。
    {
        cout<<"Out of range!"<<endl;         //將字串"Out of range!"輸出到螢幕上。
        cout<<"Quiz1 point: ";               //Quiz1 point: "輸出到螢幕上。
        cin>>first_grade;                    //從鍵盤輸入，並將輸入的值給first_grade。
    }

    cout<<"Quiz2 point: ";                   //將字串"Quiz2 point: "輸出到螢幕上。
    cin>>second_grade;                       //從鍵盤輸入，並將輸入的值給second_grade。

    while (second_grade<0 or second_grade>10)//使用while迴圈(當second_grade<0 或 second_grade>10)。
    {
        cout<<"Out of range!"<<endl;         //將字串"Out of range!"輸出到螢幕上。
        cout<<"Quiz2 point: ";               //Quiz2 point: "輸出到螢幕上。
        cin>>second_grade;                   //從鍵盤輸入，並將輸入的值給second_grade。
    }

    cout<<"Final grade: "<<(first_grade+second_grade)/2;   //將字串"Final grade: "與(first_grade+second_grade)/2之運算結果輸出到螢幕上。

    return 0;
}
