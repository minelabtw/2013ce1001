#include <iostream>

using namespace std;

int main()
{


    int isOutOfRange1=0,isOutOfRange2=0;
    float finalGrade=0.0,quizPoint1=0.0,quizPoint2=0.0;

    //輸入第一個小考分數並判斷是否在合理範圍
    while(isOutOfRange1<1)
    {
        cout << "Quiz1 point: " ;
        cin >> quizPoint1 ;
        if(quizPoint1 < 11 && quizPoint1 > -1)
        {
            isOutOfRange1++;
        }
        else
        {
            cout << "Out of range! " << endl;
        }

    }

    //判斷第一個輸入的分數是屬於哪一個評分規則並印出
    if(quizPoint1 == 0)
    {
        cout << "Result: SE"  << endl;
    }
    else if(quizPoint1 == 2)
    {
        cout << "Result: CE"  << endl;
    }
    else if(quizPoint1 == 3)
    {
        cout << "Result: NA"  << endl;
    }
    else if(quizPoint1 == 5)
    {
        cout << "Result: TLE/MLE/OLE"  << endl;
    }
    else if(quizPoint1 == 8)
    {
        cout << "Result: WA"  << endl;
    }
    else if(quizPoint1 == 9)
    {
        cout << "Result: II/WC"  << endl;
    }
    else if(quizPoint1 == 10)
    {
        cout << "Result: AC"  << endl;
    }

    //輸入第二個小考分數並判斷是否在合理範圍
    while(isOutOfRange2<1)
    {
        cout << "Quiz2 point: " ;
        cin >> quizPoint2 ;
        if(quizPoint2 < 11 && quizPoint2 > -1)
        {
            isOutOfRange2++;
        }
        else
        {
            cout << "Out of range! " << endl;
        }

    }

    //判斷第二個輸入的分數是屬於哪一個評分規則並印出
    if(quizPoint2 == 0)
    {
        cout << "Result: SE"  << endl;
    }
    else if(quizPoint2 == 2)
    {
        cout << "Result: CE"  << endl;
    }
    else if(quizPoint2 == 3)
    {
        cout << "Result: NA"  << endl;
    }
    else if(quizPoint2 == 5)
    {
        cout << "Result: TLE/MLE/OLE"  << endl;
    }
    else if(quizPoint2 == 8)
    {
        cout << "Result: WA"  << endl;
    }
    else if(quizPoint2 == 9)
    {
        cout << "Result: II/WC"  << endl;
    }
    else if(quizPoint2 == 10)
    {
        cout << "Result: AC"  << endl;
    }

    //計算出平均分數並印出
    finalGrade=(quizPoint1+quizPoint2)/2 ;
    cout << "Final Grade: " << finalGrade << endl;



    return 0;
}
