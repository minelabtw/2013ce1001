#include <iostream>

using namespace std;

int main()
{
    int a1=0;                    //宣告型別為整數(int)的a1，並初始化其數值為0。
    int d=0;                     //宣告型別為整數(int)的d，並初始化其數值為0。
    int n=0;                     //宣告型別為整數(int)的n，並初始化其數值為0。
    int p=0;                     //宣告型別為整數(int)的p，並初始化其數值為0。
    int first_n=1;               //宣告型別為整數(int)的first_n，並初始化其數值為1。

    cout<<"Arithmetic progression"<<endl;  //將字串"Arithmetic progression"輸出到螢幕上。
    cout<<"a1: ";                   //將字串"a1: "輸出到螢幕上。
    cin>>a1;                       //從鍵盤輸入，並將輸入的值給a1。

    while (a1<=0)                  //使用while迴圈(當a1<=0)。
    {
        cout<<"Out of range!"<<endl;         //將字串"Out of range!"輸出到螢幕上。
        cout<<"a1: ";               //將字串"a1: "輸出到螢幕上。
        cin>>a1;                    //從鍵盤輸入，並將輸入的值給a1。
    }

    cout<<"d: ";                   //將字串"d: "輸出到螢幕上。
    cin>>d;                       //從鍵盤輸入，並將輸入的值給d。

    while (d<=0)             //使用while迴圈(當d<=0)。
    {
        cout<<"Out of range!"<<endl;         //將字串"Out of range!"輸出到螢幕上。
        cout<<"d: ";               //將字串"d: "輸出到螢幕上。
        cin>>d;                   //從鍵盤輸入，並將輸入的值給d。
    }

    cout<<"n: ";                   //將字串"n: "輸出到螢幕上。
    cin>>n;                       //從鍵盤輸入，並將輸入的值給n。

    while (n<=0)  //使用while迴圈(當n<=0)。
    {
        cout<<"Out of range!"<<endl;         //將字串"Out of range!"輸出到螢幕上。
        cout<<"n: ";               //將字串"n: "輸出到螢幕上。
        cin>>n;                    //從鍵盤輸入，並將輸入的值給n。
    }

    while(first_n<=n)             //使用while迴圈(當first_n<=n)。
    {
      cout<<(a1+d*(first_n-1))<<",";   //將(a1+d*(first_n-1))與","輸出到螢幕上。
      first_n++;                       //將first_n加一並存回first_n。
    }

    cout<<"sum: ";               //將字串"sum: "輸出到螢幕上。
    cout<<n*(2*a1+d*(n-1))/2;    //將n*(2*a1+d*(n-1))/2之運算結果輸出到螢幕。
    return 0;
}
