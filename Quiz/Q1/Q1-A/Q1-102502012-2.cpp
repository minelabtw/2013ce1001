#include<iostream>
using namespace std;
int main()
{
    int s,d,n,p,sum;
    cout<<"Arithmetic progression"<<endl;
    do  // input a1
    {
        cout<<"a1: ";
        cin>>s;
    }
    while(s<=0&&cout<<"Out of range!\n");
    do  // input d
    {
        cout<<"d: ";
        cin>>d;
    }
    while(d<=0&&cout<<"Out of range!\n");
    do  // input n
    {
        cout<<"n: ";
        cin>>n;
    }
    while(n<=0&&cout<<"Out of range!\n");
    do  // input p
    {
        cout<<"p: ";
        cin>>p;
    }
    while(p<=0&&cout<<"Out of range!\n");

    sum=0;
    for(int i=1; i<=n; i++) // 第i項
    {
        int tmp=1; // 暫存結果
        for(int j=1; j<=p; j++) // 計算p次方
            tmp*=(s+d*(i-1));
        cout<<tmp<<",";
        sum+=tmp;
    }
    cout<<"sum: "<<sum<<endl<<endl;
    cout<<"Fibonacci sequence"<<endl;
    do  // reuse and input n
    {
        cout<<"n: ";
        cin>>n;
    }
    while(n<=0&&cout<<"Out of range!\n");

    int a,b;
    a=b=1; // initialize  F1=a,F2=b ...
    sum=0;
    for(int i=1; i<=n; i++)
    {
        cout<<a<<",";
        sum+=a;
        int tmp=a+b; // F(i+2)=F(i)+F(i+1)
        a=b; // swap position
        b=tmp; // swap position
    }
    cout<<"sum: "<<sum;

    return 0;
}
