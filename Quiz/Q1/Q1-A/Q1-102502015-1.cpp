#include <iostream>
using namespace std;
int main()
{
    int a;                                      //a 第一次成績 b 第二次成績 C 學期平均
    int b;
    float c;
    cout<<"Quiz1 point: ";
    cin>>a;
    if(a<0 || a>10)                             //如果超出範圍
    {
        while(a<0 || a>10)                      //則 執行這個迴圈 直到 沒有超出範圍
        {
            cout<<"Out of range!"<<endl;
            cout<<"Quiz1 point: ";
            cin>>a;
        }
    }
    cout<<"Quiz2 point: ";
    cin>>b;
    if(b<0 || b>10)                             //如果超出範圍
    {
        while(b<0 || b>10)                      //則 執行這個迴圈 直到 沒有超出範圍
        {
            cout<<"Out of range!"<<endl;
            cout<<"Quiz1 point: ";
            cin>>b;
        }
    }
    c = a*0.5 + b*0.5;                          //計算平均
    cout<<"Final grade: "<<c;
    return 0;
}
