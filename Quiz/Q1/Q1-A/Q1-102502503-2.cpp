#include <iostream>
using namespace std;
int main()
{
    int a;  //宣告整數變數a
    int b;  //宣告整數變數b
    int c;  //宣告整數變數c
    int d;  //宣告整數變數d
    int e=1;  //宣告整數變數e並初始化其值為1
    int f;  //宣告整數變數f
    int g;  //宣告整數變數g

    cout << "初項: ";  //輸出字串
    cin >> a;  //將輸入的值給a

    while (a==0)  //使用while迴圈,若符合此條件則進行下列程式,否則跳出迴圈
    {
        cout << "Out of range! " <<endl << "初項: ";  //輸出字串
        cin >> a;
    }

    cout << "公差: ";
    cin >> b;

    while (b==0)  //使用while迴圈,若符合此條件則進行下列程式,否則跳出迴圈
    {
        cout << "Out of range! " <<endl << "公差: ";
        cin >> b;
    }

    cout << "項數: ";
    cin >> c;

    while (c<=0)  //使用while迴圈,若符合此條件則進行下列程式,否則跳出迴圈
    {
        cout << "Out of range! " <<endl << "項數: ";
        cin >> c;
    }

    cout << "等差數列: ";
    while (e<=c-1)  //使用while迴圈,若符合此條件則進行下列程式,否則跳出迴圈
    {
        cout << a+b*(e-1) << ",";
        e++;  //e遞增

    }

    cout << a+b*(c-1) << endl;  //輸出末項

    f=a+b*(c-1);  //計算末項
    cout << "等差數列和: " << (a+f)*c/2;  //輸出其和

    return 0;
}
