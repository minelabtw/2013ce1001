#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a1 ;                                      //首項
    int d ;                                       //差
    int n ;                                       //項數
    int p ;                                       //乘幾次
    int sum ;                                     //加目前加了多少
    int dot ;                                     //乘目前乘到多少
    cout << "Arithmetic progression" << endl ;
    cout << "a1: " ;                              //輸入
    cin >> a1 ;
    while (a1<=0)                                 //錯了再次輸入
    {
        cout << "Out of rangle!" << endl ;
        cout << "a1: " ;
        cin >> a1 ;
    }
    cout << "d: " ;                               //同理
    cin >> d ;
    while (d<=0)
    {
        cout << "Out of rangle!" << endl ;
        cout << "d: " ;
        cin >> d ;
    }
    cout << "n: " ;                               //同理
    cin >> n ;
    while (n<=0)
    {
        cout << "Out of rangle!" << endl ;
        cout << "n: " ;
        cin >> n ;
    }
    cout << "p: " ;                               //同理
    cin >> p ;
    while (p<=0)
    {
        cout << "Out of rangle!" << endl ;
        cout << "p: " ;
        cin >> p ;
    }
    sum=0 ;                                       //目前還沒加所以先為零
    for (int i=1; i<=n; ++i)                      //目前做到第i項
    {
        dot =1 ;                                  //還沒乘所以先為一
        for (int j=1; j<=p; ++j)                  //目前乘了j次
            dot =dot *(a1+(i-1)*d) ;
        cout << dot << "," ;
        sum=sum+dot ;                             //把目前的加起來
    }
    cout << "sum: " << sum <<endl ;               //輸出
    cout << "Fibonacci sequence: " ;
    int n_F ;
    int late1 ;                                   //前幾項1
    int late2 ;                                   //前幾項2
    int late3 ;                                   //前幾項3
    cout << "n: " ;                               //輸入
    cin >> n_F ;
    while (n_F<=0)                                //錯了再次輸入
    {
        cout << "Out of rangle!" << endl ;
        cout << "n: " ;
        cin >> n_F ;
    }
    if (n_F==1)                                   //特殊例子獨立
        cout << "1,sum; 1" ;
    else if (n_F==2)                              //特殊例子獨立
        cout << "1,1,sum; 2" ;
    else
    {
        sum=2 ;
        late1 =1 ;
        late2=1 ;
        cout << "1,1," ;
        for (int i=2 ; i<n_F; ++i)
        {
            late3=late1+late2 ;                   //算下一個
            late1=late2 ;                         //此區在用交換方法
            late2=late3 ;
            cout << late2 << "," ;
            sum=sum+late2 ;                       //加到目前的
        }
        cout << "sum: "<< sum ;                   //輸出結果
    }
    return 0;
}
