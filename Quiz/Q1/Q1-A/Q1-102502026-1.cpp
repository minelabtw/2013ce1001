//Q1-102502026-1

#include<iostream>
using namespace std;

int main()  //Start the program
{
    double number1=0;   //define number1
    double number2=0;   //define number2
    double FGrade=0;    //define the mean

    cout<<"Quiz1 point: ";  //ask number1
    cin>>number1;

    while (number1<0 | number1>10)  //do this until the number is 0~10
    {
        cout<<"Out of range! \n"; //if not, is wrong
        cout<<"Quiz1 point: ";  //ask again
        cin>>number1;
    }

    if (number1==0) //if number1 is 0
        cout<<"Result: AC\n";
    else if (number1==2)    //if number1 is 2
        cout<<"Result: CE\n";
    else if (number1==3)    //if number1 is 3
        cout<<"Result: NA\n";
    else if (number1==5)    //if number1 is 5
        cout<<"Result: TLE/MLE/OLE\n";
    else if (number1==8)    //if number1 is 8
        cout<<"Result: WA\n";
    else if (number1==9)    //if number1 is 9
        cout<<"Result: II/WC\n";
    else if (number1==10)   //if number1 is 10
        cout<<"Result: AC\n";

    cout<<"Quiz2 point: ";    //ask number2
    cin>>number2;

    while (number2<0 | number2>10)  //do it until number2 is 0~10
    {
        cout<<"Out of range! \n";   //if not, is wrong
        cout<<"Quiz2 point: ";  //ask again
        cin>>number2;
    }

    if (number2==0) //if number2 is 0
        cout<<"Result: AC\n";
    else if (number2==2)    //if number2 is 2
        cout<<"Result: CE\n";
    else if (number2==3)    //if number2 is 3
        cout<<"Result: NA\n";
    else if (number2==5)    //if number2 is 5
        cout<<"Result: TLE/MLE/OLE\n";
    else if (number2==8)    //if number2 is 8
        cout<<"Result: WA\n";
    else if (number2==9)    //if number2 is 9
        cout<<"Result: II/WC\n";
    else if (number2==10)   //if number2 is 10
        cout<<"Result: AC\n";

    FGrade=((number1+number2)/2);   //the formula to get the mean
    cout<<"Final grade:"<<FGrade;   //print the mean
    return 0;
}   //end of the program




