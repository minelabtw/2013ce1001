#include <iostream> // allows program to input and output data to the screem

using std::cout;
using std::cin; //program uses names from the std namespace

int main ()
{
    double num1 = 0; //quiz1
    double num2 = 0; //quiz2
    double grade = 0; //total


    do //input and determine
    {
        cout <<"Quiz1 points:"; //print the woird
        cin >>num1; //input the data
        if (num1<0 or num1>10) //determine the input
            cout <<"Out of range!\n"; //if out of range then output
        else if (num1==0) //condition
            cout <<"Results:SE\n"; //output the inmformation
        else if (num1==2) //condition
            cout <<"Result:CE\n"; //output the inmformation
        else if (num1==3) //condition
            cout <<"Result:NA\n"; //output the inmformation
        else if (num1==5) //condition
            cout <<"Result:TLE/MLE/OLE\n"; //output the inmformation
        else if (num1==8) //condition
            cout <<"Result:WA\n"; //output the inmformation
        else if (num1==9) //condition
            cout <<"Result:II/WC\n"; //output the inmformation
        else if (num1==10) //condition
            cout <<"Result:AC\n"; //output the inmformation
        else break; //leave the loop
    }
    while (num1<0 or num1>10);

    do //input and determine
    {
        cout <<"Quiz2 points:"; //print the words
        cin >>num2; //input the data
        if (num2<0 or num2>10) //determine the input
            cout <<"Out of range!\n"; //if out of range then output
        else if (num2==0) //condition
            cout <<"Result:SE\n"; //output the inmformation
        else if (num2==2) //condition
            cout <<"Result:CE\n"; //output the inmformation
        else if (num2==3) //condition
            cout <<"Result:NA\n"; //output the inmformation
        else if (num2==5) //condition
            cout <<"Result:TLE/MLE/OLE\n"; //output the inmformation
        else if (num2==8) //condition
            cout <<"Result:WA\n"; //output the inmformation
        else if (num2==9) //condition
            cout <<"Result:II/WC\n"; //output the inmformation
        else if (num2==10) //condition
            cout <<"Result:AC\n"; //output the inmformation
        else break; //leave the loop
    }
    while (num2<0 or num2>10);

    grade = num1*0.5 + num2*0.5; //total*0.5
    cout <<"Final grade:"<<grade; //print the data

    return 0; //indicate that program ended successfully

}
