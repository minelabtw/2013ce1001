#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int Quiz1point;//宣告整數變數quiz1point
    int Quiz2point;//宣告整數變數Quiz2point
    int finalgrade;//宣告整數變數finalgrade

    cout<<"Quiz1 point: ";
    cin>>Quiz1point;//輸入幣一個成績
    if(Quiz1point==0)
    {
        cout<<"Result: SE"<<endl;
    }
    if(Quiz1point==2)
    {
        cout<<"Result: CE"<<endl;
    }
    if(Quiz1point==3)
    {
        cout<<"Result: NA"<<endl;
    }
    if(Quiz1point==5)
    {
        cout<<"Result: TLE/MLE/OLE"<<endl;
    }
    if(Quiz1point==8)
    {
        cout<<"Result: WA"<<endl;
    }
    if(Quiz1point==9)
    {
        cout<<"Result: II/WC"<<endl;
    }
    if(Quiz1point==10)
    {
        cout<<"Result: AC"<<endl;
    }
    while(Quiz1point>10 or Quiz1point<0)
    {
        cout<<"Out of range"<<endl;
        cout<<"Quiz1 point: ";
        cin>>Quiz1point;
    }
    cout<<"Quiz2 point: ";
    cin>>Quiz2point;//輸入第二個成績
    if(Quiz2point==0)
    {
        cout<<"Result: SE"<<endl;
    }
    if(Quiz2point==2)
    {
        cout<<"Result: CE"<<endl;
    }
    if(Quiz2point==3)
    {
        cout<<"Result: NA"<<endl;
    }
    if(Quiz2point==5)
    {
        cout<<"Result: TLE/MLE/OLE"<<endl;
    }
    if(Quiz2point==8)
    {
        cout<<"Result: WA"<<endl;
    }
    if(Quiz2point==9)
    {
        cout<<"Result: II/WC"<<endl;
    }
    if(Quiz2point==10)
    {
        cout<<"Result: AC"<<endl;
    }
    while(Quiz2point>10 or Quiz2point<0)
    {
        cout<<"Out of range"<<endl;
        cout<<"Quiz2 point: ";
        cin>>Quiz2point;
    }
    finalgrade=(Quiz1point+Quiz2point)/2;
    cout<<"final grade = "<<finalgrade;//顯示兩者平均

    return 0;
}

