#include <iostream>

using namespace std;

int main()
{
    int a1=0,d=0,n=0,p=0,a1IsOutOfRange=0,dIsOutOfRange=0,nIsOutOfRange=0,pIsOutOfRange=0;

    cout << "Arithmetic progression " << endl;

    //輸入a1並判斷a1是否在合理範圍
    while(a1IsOutOfRange < 1 )
    {
        cout << "a1: " ;
        cin >> a1;
        if(a1 > 0)
        {
            a1IsOutOfRange++;

        }
        else
        {
            cout << "Out of range! " << endl;
        }

    }

    //輸入d並判斷d是否在合理範圍
    while(dIsOutOfRange < 1 )
    {
        cout << "d: " ;
        cin >> d;
        if(d > 0)
        {
            dIsOutOfRange++;

        }
        else
        {
            cout << "Out of range! " << endl;
        }

    }

    //輸入n並判斷n是否在合理範圍
    while(nIsOutOfRange < 1 )
    {
        cout << "n: " ;
        cin >> n;
        if(n > 0)
        {
            nIsOutOfRange++;

        }
        else
        {
            cout << "Out of range! " << endl;
        }

    }

    //輸入p並判斷p是否在合理範圍
    while(pIsOutOfRange < 1 )
    {
        cout << "p: " ;
        cin >> p;
        if(p > 0)
        {
            pIsOutOfRange++;
        }
        else
        {
            cout << "Out of range! " << endl;
        }
    }

    //顯示出等差數列平方的和
    int sum=0;

    for(int i = 1 ; i <= n ;i++)
    {
        int temp=0;
        temp=a1+d*(i-1);
        for(int j=1;j<p;j++)
        {
            temp=temp*temp;
        }
        cout << temp << "," ;
        sum=sum+temp;
    }
        cout << "sum: " << sum << endl ;

    cout << "Fibonacci sequence " << endl;
    int Fn=0,FnIsOutOfRange=0;

    while(FnIsOutOfRange < 1 )
    {
        cout << "n: " ;
        cin >> Fn;
        if(Fn > 0)
        {
            FnIsOutOfRange++;
        }
        else
        {
            cout << "Out of range! " << endl;
        }
    }

    int tempFn=0;
    int tempFn2=1;
    int sumFn=0;
    cout << "1" << "," ;
    for(int k=1;k< Fn ;k++)
    {

        tempFn2=tempFn2+tempFn;
        cout << tempFn2 << "," ;
        tempFn++;
        sumFn=sumFn+tempFn2+1;
    }
    cout << "sum: " << sumFn << endl ;

    return 0;
}
