#include <iostream>

using namespace std;

int main()
{
    int a;//宣告變數
    int b;

    cout << "Quiz1 point: ";//輸出字串
    cin >> a;

    while ( a > 10 or a < 0 )//使用迴圈使a值在範圍內
    {
        cout << "Out of range!" << endl;//如果超出範圍則輸出此字串
        cout << "Quiz1 point: ";
        cin >> a;
    }

    if ( a ==10 )//判斷分數1為哪一範圍
        cout << "Result: AC" << endl;
    else if ( a == 9 )
        cout << "Result: II/WC" << endl;
    else if ( a == 8 )
        cout << "Result: WA" << endl;
    else if ( a == 5 )
        cout << "Result: TLE/MLE/OLE" << endl;
    else if ( a == 3 )
        cout << "Result:NA" << endl;
    else if ( a == 2 )
        cout << "Result:CE" << endl;
    else
        cout << "Result:SE" << endl;

    cout << "Quiz2 point: ";//輸出此字串
    cin >> b;//輸入變數b

    while ( b > 10 or b < 0 )//輸入迴圈使變數b在範圍內
    {
        cout << "Out of range!" << endl;
        cout << "Quiz2 point: ";
        cin >> b;
    }

    if ( b ==10 )//判斷變數b的範圍為何
        cout << "Result: AC" << endl;
    else if ( b == 9 )
        cout << "Result: II/WC" << endl;
    else if ( b == 8 )
        cout << "Result: WA" << endl;
    else if ( b == 5 )
        cout << "Result: TLE/MLE/OLE" << endl;
    else if ( b == 3 )
        cout << "Result: NA" << endl;
    else if ( b == 2 )
        cout << "Result: CE" << endl;
    else
        cout << "Result: SE" << endl;

    cout << "Final grade: " << (a+b)/2;

    return 0;//返回初始值

}

