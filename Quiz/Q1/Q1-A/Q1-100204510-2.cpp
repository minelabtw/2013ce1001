#include <iostream>

using std::cout;
using std::cin;

int main ()
{
    int a1 = 0;
    int d = 0;
    int n = 0;
    int p = 0;
    int an = 0;
    int counter = 0;
    int sum = 0;

    cout <<"Arithmetic progression\n";

    do
    {
        cout <<"a1:";
        cin >>a1;
        if (a1==0)
            cout <<"Out of range!\n";
    }
    while (a1==0);

    do
    {
        cout <<"d:";
        cin >>d;
        if (d==0)
            cout <<"Out of range!\n";
    }
    while (d==0);

    do
    {
        cout <<"n:";
        cin >>n;
        if (n<=0)
            cout <<"Out of range!\n";
    }
    while (n<=0);

    while (counter<=n)
    {
        counter = counter + 1;
        an = a1 + d*(n-1);
        cout <<""<<an<<",";
        counter++;
    }

    return 0;
}
