#include<iostream>
using namespace std;
int main()
{
    int quiz1=-1,quiz2=-1;  //宣告型別為 整數(int) 的變數quiz1,quiz2
    float final_grade=0;    //宣告型別為 浮點數(float) 的變數final_grade

    while(quiz1<0 || quiz1>10)  //重複輸入變數並判斷是否符合定義
    {
        cout << "Quiz1 point: ";
        cin >> quiz1;
        if(quiz1<0 || quiz1>10)
        {
            cout << "Out of range!\n";
        }
    }
    cout << "Result: ";
    switch(quiz1)               //判斷輸入分數為何種
    {
    case 0:
        cout << "SE";
        break;
    case 2:
        cout << "CE";
        break;
    case 3:
        cout << "NA";
        break;
    case 5:
        cout << "TLE/MLE/OLE";
        break;
    case 8:
        cout << "WA";
        break;
    case 9:
        cout << "II/WC";
        break;
    case 10:
        cout << "AC";
        break;
    }
    cout << endl;
    while(quiz2<0 || quiz2>10)
    {
        cout << "Quiz2 point: ";
        cin >> quiz2;
        if(quiz2<0 || quiz2>10)
        {
            cout << "Out of range!\n";
        }
    }
    cout << "Result: ";
    switch(quiz2)
    {
    case 0:
        cout << "SE";
        break;
    case 2:
        cout << "CE";
        break;
    case 3:
        cout << "NA";
        break;
    case 5:
        cout << "TLE/MLE/OLE";
        break;
    case 8:
        cout << "WA";
        break;
    case 9:
        cout << "II/WC";
        break;
    case 10:
        cout << "AC";
        break;
    }
    final_grade = quiz1*0.5+quiz2*0.5;
    cout << "\nFinal grade: " << final_grade;   //輸出結果
    return 0;
}
