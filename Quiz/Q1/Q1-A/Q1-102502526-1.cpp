#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a=0;                //宣告一個整數，初始化其值為0
    int b=0;

    cout<<"Quiz1 point: ";
    cin>>a;

    while(a<0 || a>10)                    //當a<0 或 a>10時，輸出out of range、重新輸入a，並迴圈之
    {
        cout<<"Out of range!"<<endl<<cout<<"Quiz1 point: ";
        cin>>a;
    }
    if(a==10)                             //做輸入值對應的輸出
        cout<<"Result: AC";
    else if(a==9)
        cout<<"Result: II/WC"<<a;
    else if(a==8)
        cout<<"Result: WA";
    else if(a==5)
        cout<<"Result: TLE/MLE/OLE";
    else if(a==3)
        cout<<"Result: NA";
    else if(a==2)
        cout<<"Result: CE";
    else if(a==0)
        cout<<"Result: SE";

cout<<endl;

    cout<<"Quiz2 point: ";                //同a
    cin>>b;

    while(b<0 || b>10)
    {
        cout<<"Out of range!"<<endl<<cout<<"Quiz2 point: ";
        cin>>b;
    }
    if(b==10)
        cout<<"Result: AC";
    else if(b==9)
        cout<<"Result: II/WC"<<a;
    else if(b==8)
        cout<<"Result: WA";
    else if(b==5)
        cout<<"Result: TLE/MLE/OLE";
    else if(b==3)
        cout<<"Result: NA";
    else if(b==2)
        cout<<"Result: CE";
    else if(b==0)
        cout<<"Result: SE";

        cout<<endl<<"Final grade: "<<(a+b)/2;   //運算，輸出Final grade

    return 0;
}
