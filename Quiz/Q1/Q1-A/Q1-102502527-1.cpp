#include <iostream>

using namespace std;

int main()
{
    int firstgrade = 0;//宣告整數為0
    int secondgrade = 0;
    int sum = 0;

    cout << "Quiz1 point: ";
    cin >> firstgrade;
    while ( firstgrade < 0 || firstgrade > 10 )//當firstgrade在範圍外時做下列動作
    {
        cout << "Out of range!" << endl;
        cout << "Quiz1 point: ";
        cin >> firstgrade;
    }

    cout << "Quiz2 point: ";
    cin >> secondgrade;
    while ( secondgrade < 0 || secondgrade > 10 )//當secondgrade在範圍外時做下列動作
    {
        cout << "Out of range!" << endl;
        cout << "Quiz2 point: ";
        cin >> secondgrade;
    }

    sum = 0.5 * firstgrade + 0.5 * secondgrade;//將總和給sum

    cout << "Final grade: " << sum;

    return 0;
}
