#include <iostream>

using namespace std;

int main()
{

    int grade1 = -1; //成績1
    int grade2 = -1; //成績2
    int fingrdint = 0; //數
    int fingrdflt = 0; //小數

    //輸入輸出數1
    while(grade1<0)
    {
        cout << "Quize1 point: ";
        cin >> grade1;
        if (grade1<0 or grade1>10)
        {
            cout << "Out of range!\n";
            grade1 = -1;
        }
        else
        {
            if (grade1==0)cout << "Result: SE\n";
            else if(grade1==2)cout << "Result: CE\n";
            else if(grade1==3)cout << "Result: NA\n";
            else if(grade1==5)cout << "Result: TLE/MLE/OLE\n";
            else if(grade1==8)cout << "Result: WA\n";
            else if(grade1==9)cout << "Result: II/WC\n";
            else if(grade1==10)cout << "Result: AC\n";
            else ;
        }
    }

    //輸入輸出數二
    while(grade2<0)
    {

        cout << "Quize2 point: ";
        cin >> grade2;
        if (grade2<0 or grade2>10)
        {
            cout << "Out of range!\n";
            grade2 = -1;
        }
        else
        {
            if (grade2==0)cout << "Result: SE\n";
            else if(grade2==2)cout << "Result: CE\n";
            else if(grade2==3)cout << "Result: NA\n";
            else if(grade2==5)cout << "Result: TLE/MLE/OLE\n";
            else if(grade2==8)cout << "Result: WA\n";
            else if(grade2==9)cout << "Result: II/WC\n";
            else if(grade2==10)cout << "Result: AC\n";
            else ;
        }
    }

    fingrdint = grade1 * 0.5 + grade2 * 0.5;
    fingrdflt = (grade1*5 + grade2*5)%10;

    // 輸出答案
    if (fingrdflt != 0)cout << fingrdint << "." << fingrdflt ;
    else cout << fingrdflt ;

    return 0;

}
