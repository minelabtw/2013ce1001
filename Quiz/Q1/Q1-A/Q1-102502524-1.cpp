#include <iostream>
using namespace std;

int main()
{
    int a = 0;                              //設定二變數a,b為供成績輸入
    int b = 0;

    cout << "Quiz1 point: ";
    cin >> a;
    while(a<0 || a>10)                      //判斷Quiz1的成績是否符合範圍
    {
        cout << "Out of range!" << endl;
        cout << "Quiz1 point: ";
        cin >> a;
    }

    cout << "Result: ";
    if (a==10)                              //判斷並輸出Quiz1成績所相對應的Result
        cout << "AC" << endl;
    else if (a==9)
        cout << "II/WC" << endl;
    else if (a==8)
        cout << "WA" << endl;
    else if (a==5)
        cout << "TLE/MLE/OLE" << endl;
    else if (a==3)
        cout << "NA" << endl;
    else if (a==2)
        cout << "CE" << endl;
    else if (a==0)
        cout <<"SE" << endl;


    cout << "Quiz2 point: ";
    cin >> b;
    while(b<0 || b>10)                      //判斷Quiz2的成績是否符合範圍
    {
        cout << "Out of range!" << endl;
        cout << "Quiz2 point: ";
        cin >> b;
    }
    cout << "Result: ";
    if (b==10)                              //判斷並輸出Quiz2成績所相對應的Result
        cout << "AC" << endl;
    else if (b==9)
        cout << "II/WC" << endl;
    else if (b==8)
        cout << "WA" << endl;
    else if (b==5)
        cout << "TLE/MLE/OLE" << endl;
    else if (b==3)
        cout << "NA" << endl;
    else if (b==2)
        cout << "CE" << endl;
    else if (b==0)
        cout <<"SE" << endl;

    cout << "Final grade: " << (a+b)*0.5;   //計算並輸出平均成績

    return 0;
}
