#include<iostream>

using namespace std ;


int main ()
{
    // 設定變數
    int a , b ;
    double F = 0. ;
    // 輸出輸入
    cout << "Quizl point: " ;
    cin >> a ;
    // 判斷是否超出範圍
    while  ( a < 0 || a > 10)
    {
        cout << "Out of range!" << endl;
        cout << "Quizl point: " ;
        cin >> a ;
    }
    // 判斷分數落在哪一個值
    if ( a == 10 )
    {
        cout << "Result: AC" << endl ;
    }
    else if ( a == 9 )
    {
        cout << "Result: II/WC" << endl;
    }
    else if ( a == 8 )
    {
        cout << "Result: WA" << endl ;
    }
    else if ( a == 5 )
    {
        cout << "Result: TLE/MLE/OLE" << endl ;
    }
    else if ( a == 3)
    {
        cout << "Result: NA" << endl ;
    }
    else if ( a == 2 )
    {
        cout << "Result: CE" << endl ;
    }
    else if ( a == 0 )
    {
        cout << "Result: SE" << endl ;
    }


    // 輸出以及輸入第二個分數
    cout << "Quiz2 point: ";
    cin >> b ;

    while  ( b < 0 || b > 10)
    {
        cout << "Out of range!" << endl ;
        cout << "Quiz2 point: " ;
        cin >> b ;
    }

    if ( b == 10 )
    {
        cout << "Result: AC" << endl;
    }
    else if ( b == 9 )
    {
        cout << "Result: II/WC" << endl;
    }
    else if ( b == 8 )
    {
        cout << "Result: WA" << endl;
    }
    else if ( b == 5 )
    {
        cout << "Result: TLE/MLE/OLE" << endl;
    }
    else if ( b == 3)
    {
        cout << "Result: NA" << endl;
    }
    else if ( b == 2 )
    {
        cout << "Result: CE" << endl;
    }
    else if ( b == 0 )
    {
        cout << "Result: SE" << endl;
    }


// 計算最後成績
    F = a*0.5 + b*0.5 ;
// 輸出成績
    cout << "Final grade: " << F ;




    return 0 ;
}
