#include <iostream>
using namespace std;

int main()
{
    int a;    //宣告變數
    int b;
    int c;
    int d;
    int e=1;
    int f;

    cout<<"Arithmetic progression"<<endl;

    cout<<"a1: ";
    cin>>a;
    while(a<=0)    //使用迴圈判斷所輸入值是否符合要求
    {
        cout<<"Out of range!"<<endl;
        cout<<"a1: ";
        cin>>a;
    }

    cout<<"d: ";
    cin>>b;
    while(b<=0)    //使用迴圈判斷所輸入值是否符合要求
    {
        cout<<"Out of range!"<<endl;
        cout<<"d: ";
        cin>>b;
    }

    cout<<"n: ";
    cin>>c;
    while(c<=0)    //使用迴圈判斷所輸入值是否符合要求
    {
        cout<<"Out of range!"<<endl;
        cout<<"n: ";
        cin>>c;
    }

    cout<<"p: ";
    cin>>d;
    while(d<=0)    //使用迴圈判斷所輸入值是否符合要求
    {
        cout<<"Out of range!"<<endl;
        cout<<"p: ";
        cin>>d;
    }

    f=(a*2+b*(c-1))*c/2;

    while(e<=c)    //顯示數列
    {
        cout<<a<<",";
        a=a+b;
        e++;
    }

    cout<<"sum: "<<f;    //輸出結果


    return 0;
}
