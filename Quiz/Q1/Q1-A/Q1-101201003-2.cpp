#include<iostream>
using namespace std;

int a1=0;                                   //定義型別為int的變數a1，並初始化其值為0
int d=0;                                    //定義型別為int的變數d，並初始化其值為0
int n=1;                                    //定義型別為int的變數n，並初始化其值為1
int nMax=0;                                 //定義型別為int的變數nMax，並初始化其值為0
int p=0;                                    //定義型別為int的變數p，並初始化其值為0
int an=0;                                   //定義型別為int的變數an，並初始化其值為0
int sum=0;                                  //定義型別為int的變數sum，並初始化其值為0

main()
{
    cout << "Arithmetic progression" << endl << "a1:";  //顯示Arithmetic progression換行顯示a1
    cin >> a1;                                          //輸入a1
    while(a1==0)                                        //當a1=0，顯示輸入錯誤，並要求重新輸入
    {
        cout << "Out of range!" << endl << "a1:";
        cin >> a1;
    }
    cout << "d:";                                        //顯示d:
    cin >> d;                                            //輸入d
    while(d==0)                                          //當d=0，顯示輸入錯誤，並要求重新輸入
    {
        cout << "Out of range!" << endl << "d:";
        cin >> d;
    }
    cout << "n:";                                        //顯示n:，接下來和上面一樣
    cin >> nMax;
    while(nMax<=0)
    {
        cout << "Out of range!" << endl << "n:";
        cin >> nMax;
    }
    cout << "p:";
    cin >> p;
    while(p<=0)
    {
        cout << "Out of range!" << endl << "p:";
        cin >> p;
    }

    while(n<=nMax)                  //當n<=nMax進入迴圈
    {
        an=a1+d*(n-1);              //公式
        n++;                        //n每次要加一
        cout << an << "," ;         //輸出an
        sum=sum+an;                 //計算sum
    }

    cout << "sum:" << sum;          //顯示sum

return 0;
}

