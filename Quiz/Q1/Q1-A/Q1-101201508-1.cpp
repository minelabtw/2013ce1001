#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int Q1 ;                                               //第一次成績
    int Q2 ;                                               //第二次成績
    cout << "Quiz1 point: " ;
    cin >> Q1 ;
    while (Q1<0 || Q1 > 10 )                               //如果錯了就再次輸入
    {
        cout << "Out of rangle!" << endl ;
        cout << "Quiz1 point: " ;
        cin >> Q1 ;
    }
    cout <<"Result: " ;
    if (Q1==10)                                           //判斷是哪一級的
        cout << "AC" ;
    else if (Q1==9)
        cout << "II/WC" ;
    else if (Q1==8)
        cout << "WA" ;
    else if (Q1==5)
        cout << "TLE/MLE/OLE" ;
    else if (Q1==3)
        cout << "NA" ;
    else if (Q1==2)
        cout << "CE" ;
    else if (Q1==0)
        cout << "SE" ;
    cout <<endl ;
    cout << "Quiz2 point: " ;                                 //和上面同理
    cin >> Q2 ;
    while (Q2<0 || Q2 > 10 )
    {
        cout << "Out of rangle!" << endl ;
        cout << "Quiz2 point: " ;
        cin >> Q2 ;
    }
    cout <<"Result: " ;
    if (Q2==10)
        cout << "AC" ;
    else if (Q2==9)
        cout << "II/WC" ;
    else if (Q2==8)
        cout << "WA" ;
    else if (Q2==5)
        cout << "TLE/MLE/OLE" ;
    else if (Q2==3)
        cout << "NA" ;
    else if (Q2==2)
        cout << "CE" ;
    else if (Q2==0)
        cout << "SE" ;
    cout <<endl ;
    cout << "Final grade: " << (Q1+Q2)/2 ;                        //輸出結果
    return 0;
}
