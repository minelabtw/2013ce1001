#include <iostream>

using namespace std;

int main()
{
    int a1;  //首項
    int d;  //公差
    int n;  //項數
    int p;  //次方
    cout<<"Arithmetic progression"<<endl;
    cout<<"a1: ";
    cin>>a1;
    while(a1<=0)  //判斷a1是否有在範圍內
    {
        cout<<"Out of range!"<<endl;
        cout<<"a1: ";
        cin>>a1;
    }
    cout<<"d: ";
    cin>>d;
    while(d<=0)  //判斷d是否有在範圍內
    {
        cout<<"Out of range!"<<endl;
        cout<<"d: ";
        cin>>d;
    }
    cout<<"n: ";
    cin>>n;
    while(n<=0)  //判斷n是否有在範圍內
    {
        cout<<"Out of range!"<<endl;
        cout<<"n: ";
        cin>>n;
    }
    cout<<"p: ";
    cin>>p;
    while(p<=0)  //判斷p是否有在範圍內
    {
        cout<<"Out of range!"<<endl;
        cout<<"p: ";
        cin>>p;
    }
    int i,j;
    int sum=0;  //總和
    int number=a1;  //輸出的項
    int origin;  //原來項的數字
    for(i=1;i<=n;i++)
    {
        cout<<number<<',';  //輸出項
        sum+=number;
        number=a1+d*i;  //下一項
        origin=number;
        for(j=1;j<p;j++)  //把項次方
            number*=origin;
    }
    cout<<"sum: "<<sum<<endl<<endl;

    cout<<"Fibonacci sequence"<<endl;
    cout<<"n: ";
    cin>>n;
    while(n<=0)  //判斷n是否有在範圍內
    {
        cout<<"Out of range!"<<endl;
        cout<<"n: ";
        cin>>n;
    }
    int f1=1,f2=1,f3=2;  //最初Fibonacci的三項
    sum=0;
    if(n==1)
        cout<<f1<<",sum: 1"<<endl;
    else if(n==2)
        cout<<f1<<','<<f2<<",sum: 2"<<endl;
    else if(n==3)
        cout<<f1<<','<<f2<<','<<f3<<",sum: 4"<<endl;
    else
    {
        sum+=4;
        cout<<f1<<','<<f2<<','<<f3<<',';
        for(i=3;i<n;i++)
        {
            f1=f2+f3;  //改變成f3下一項
            sum+=f1;
            cout<<f1<<',';  //輸出項
            f2=f3;  //改變成f2下一項
            f3=f1;  //改變成f3下一項
        }
        cout<<"sum: "<<sum<<endl;  //輸出總和
    }
    return 0;
}
