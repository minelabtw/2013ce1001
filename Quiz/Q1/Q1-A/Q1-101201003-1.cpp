#include<iostream>
using namespace std;

int a=0;                            //定義型別為int的變數a，並初始化其值為0
int b=0;                            //定義型別為int的變數b，並初始化其值為0
double c=0;                         //定義型別為double的變數c，並初始化其值為0
main()
{
    cout << "Quiz1 point:";         //顯示Quiz1 point:
    cin >> a;                       //輸入a值
    while(a<0||a>10)                //當a<0、a>10時進入迴圈
    {
        cout << "Out of range!" << "\n" << "Quiz1 point:"; //輸出Out of range!換行並輸出Quiz1 point:
        cin >> a ;                                         //輸入a
    }
    cout << "Result:";              //顯示Result:
    if(a==10)
        cout << "AC";
    else if(a==9)
        cout << "II/WC";
    else if(a==8)
        cout << "WA";
    else if(a==5)
        cout << "TLE/MLE/OLE";
    else if(a==3)
        cout << "NA";
    else if(a==2)
        cout << "CE";
    else if(a==0)
        cout << "SE";               //判斷a的值，並輸入與其對應的結果
    cout << endl;                   //換行

    cout << "Quiz2 point:";
    cin >> b;
    while(b<0||b>10)
    {
        cout << "Out of range!" << "\n" << "Quiz2 point:";
        cin >> b ;
    }
    cout << "Result:";
    if(b==10)
        cout << "AC";
    else if(b==9)
        cout << "II/WC";
    else if(b==8)
        cout << "WA";
    else if(b==5)
        cout << "TLE/MLE/OLE";
    else if(b==3)
        cout << "NA";
    else if(b==2)
        cout << "CE";
    else if(b==0)
        cout << "SE";
    cout << endl;                   //一切與a同理
    c=a*0.5+b*0.5;                  //定義c=a*0.5+b*0.5
    cout << "Final grade:" << c;    //顯示Final grade:c的值

return 0;
}
