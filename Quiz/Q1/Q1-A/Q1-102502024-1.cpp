#include<iostream>
using namespace std;
int main()
{
    float quiz1=0,quiz2=0,finalgrade=0;  //宣告型別為整數或小數的考試分數跟最後成績
    cout<<"Quiz1 point: ";  //輸出需要的成績
    cin>>quiz1;  //輸入需要的成績
    while(quiz1<0 || quiz1>10)  //判斷是否符合條件
    {
        cout<<"Out of range!\n"<<"Quiz1 point: ";
        cin>>quiz1;
    }
    cout<<"Quiz2 point: ";  //輸出需要的成績
    cin>>quiz2;  //輸入需要的成績
    while(quiz2<0 || quiz2>10)  //判斷是否符合條件
    {
        cout<<"Out of range!\n"<<"Quiz2 point: ";
        cin>>quiz2;
    }
    finalgrade=quiz1*0.5+quiz2*0.5;  //計算最後成績
    cout<<"Final grade: "<<finalgrade;  //輸出最後成績
    return 0;
}
