#include <iostream>
#include<cmath>


using namespace std;

int main()
{
    double a1=0;
    double d=0;
    double n=0;
    double p=0;
    double sum=0;
    double an=0;
    //宣告各種變數：首項a1、公差d、相數n、次方p、總和sum，以及每一項an。都預設為0

    cout << "Arithmetic progression" << endl ;
    //這是一個等差數列
    while (a1==0)
    {
        cout << "a1: ";
        cin >> a1;
        if (a1==0)
        {
            cout << "Out of range!" << endl;
        }
    }

    while (d==0)
    {
        cout << "d: ";
        cin >> d;
        if (d==0)
        {
            cout << "Out of range!" << endl;
        }
    }

    while (n<=0)
    {
        cout << "n: ";
        cin >> n;
        if (n<=0)
        {
            cout << "Out of range!" << endl;
        }
    }

    while (p<=0)
    {
        cout << "p: ";
        cin >> p;
        if (p<=0)
        {
            cout << "Out of range!" << endl;
        }
    }
    //請使用者分別輸入首項、公差、項數，以及次方。
    //其中首項、公差不等於零；項數、次方需大於零

    for (int counter =1 ; counter <= n ; counter++) //計數器從1開始，每次用完累加1，且條件設為小於等於項數，以符合項數只有1的情形
    {
        an = a1+d*(counter-1);  //當前的項目先進行等差運算
        an = pow(an,p);         //再計算使用者決定的次方
        cout << an << ",";      //然後印出來
        sum = sum + an;         //並累加至sum中

    }

    cout << "sum: " << sum << endl << endl;
    //最後印出總和


    cout << "Fibonacci sequence" << endl ;
    //接下來的是費式數列
    n=0;        //回收利用前面的n，先將他歸零
    while (n<=0)
    {
        cout << "n: ";
        cin >> n;
        if (n<=0)
        {
            cout << "Out of range!" << endl;
        }
    }
    //請使用者輸入費式數列的項數，若小於等於零則要求重新輸入（至少顯示出一項數列）

    int left=1;         //left是該項左邊
    int leftleft=0;     //leftleft，該項左邊再左邊，也就是左邊數兩項

    cout << "1,";
    sum = 1;
    //由於至少有1項數列，所以先把左邊項=1，並直接印出1,然後再把總和設為1

    for (int counter =2 ; counter <= n ; counter++)  //從第二項開始計算
    {
        an=left+leftleft;       //當前的項等於左邊和左邊左邊的和
        leftleft=left;          //左邊左邊等於左邊（它的右邊那項）
        left=an;                //左邊那項等於當前的項
        cout << an << ",";      //印出
        sum = sum + an;         //計算總和

    }
    cout << "sum: " << sum << endl ;
    //印出總和




    return 0;
}
