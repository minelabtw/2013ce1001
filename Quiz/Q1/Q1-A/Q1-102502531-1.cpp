#include <iostream>

using namespace std;

int main ()
{
    int a=0;
    int b=0;
    float sum=0;
    do
    {
        cout << "Quiz1 point: ";
        cin>>a;  //輸入a:的值
        if (a<0||a>10)
            cout << "Out of range!"<<endl; //判斷是否合理
        else
            break;
    }
    while(a<0||a>10); //確定是否繼續迴圈


     do
    {
        cout << "Quiz2 point: ";
        cin>>b;  //輸入b:
        if (b<0||b>10)
            cout << "Out of range!"<<endl; //判斷是否合理
        else
            break;
    }
    while(b<0||b>10); //確定是否繼續迴圈

    sum=a*0.5+b*0.5;
    cout <<"Final grade: "<<sum;

    return 0;
}

