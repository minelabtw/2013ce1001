#include <iostream>
using namespace std;

int main()
{
    int a = -1;
    int b = -1;
    double c = 101;
    while (a < 0 || a > 10) // limit the range
    {
        cout << "Quiz1 point: ";
        cin >> a;
        if(a < 0 || a > 10)
        {
            cout << "Out of range!\n";
        }
        else if(a == 10)
        {
            cout << "Result: AC\n";
        }
        else if(a == 9)
        {
            cout << "Result: WC/||\n";
        }
        else if(a == 8)
        {
            cout << "Result: WA\n";
        }
        else if(a == 5)
        {
            cout << "Result: TLE/MLE/OLE\n";
        }
        else if(a == 3)
        {
            cout << "Result: NA\n";
        }
        else if(a == 2)
        {
            cout << "Result: CE\n";
        }else if(a == 0)
        {
            cout << "Result: SE\n";
        }
    }
    while (b < 0 || b > 10)// limit the range
    {
        cout << "Quiz2 point: ";
        cin >> b;
        if(b < 0 || b > 10)
        {
            cout << "Out of range!\n";
        }
        else if(b == 10)
        {
            cout << "Result: AC\n";
        }
        else if(b == 9)
        {
            cout << "Result: WC/||\n";
        }
        else if(b == 8)
        {
            cout << "Result: WA\n";
        }
        else if(b == 5)
        {
            cout << "Result: TLE/MLE/OLE\n";
        }
        else if(b == 3)
        {
            cout << "Result: NA\n";
        }
        else if(b == 2)
        {
            cout << "Result: CE\n";
        }
        else if(b == 0)
        {
            cout << "Result: SE\n";
        }
    }
    c = (a + b ) * 0.5; // calculate the grade
    cout << "Final grade: " << c; // output the grade

    cout << endl;


    return 0;
}
