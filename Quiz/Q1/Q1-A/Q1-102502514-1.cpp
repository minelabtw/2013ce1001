#include <iostream>
using namespace std;

int main()
{
    int a,b; //宣告兩次輸入的成績分別為a,b

    cout <<"Quiz1 point: ";
    cin >>a;

    while (a<0||a>10) //排除成績範圍不再0~10區間內的可能,並要求重新輸入
    {
        cout <<"Out of range!\n";
        cout <<"Quiz1 point: ";
        cin >>a;
    }
    if (a==10) //如果成績為10分,則印出AC
        cout <<"Result: AC\n";
    else if (a==9) //如果成績為9分,則印出II/WC
        cout <<"Result: II/WC\n";
    else if (a==8) //如果成績為8分,則印出WA
        cout <<"Result: WA\n";
    else if (a==5) //如果成績為5分,則印出TLE/NLE/OLE
        cout <<"Recult: TLE/NLE/OLE\n";
    else if (a==3) //如果成績為3分,則印出NA
        cout <<"Recult: NA\n";
    else if (a==2) //如果成績為2分,則印出CE
        cout <<"Recult: CE\n";
    else if (a==0) //如果成績為0分,則印出SE
        cout <<"Recult: SE\n";

    cout <<"Quiz2 point: ";
    cin >>b;

    while (b<0||b>10) //排除成績範圍不再0~10區間內的可能,並要求重新輸入
    {
        cout <<"Out of range!\n";
        cout <<"Quiz2 point: ";
        cin >>b;
    }
    if (b==10)
        cout <<"Result: AC\n"; //如果成績為10分,則印出AC
    else if (b==9)
        cout <<"Result: II/WC\n"; //如果成績為9分,則印出II/WC
    else if (b==8)
        cout <<"Result: WA\n"; //如果成績為8分,則印出WA
    else if (b==5)
        cout <<"Recult: TLE/NLE/OLE\n"; //如果成績為5分,則印出TLE/NLE/OLE
    else if (b==3)
        cout <<"Recult: NA\n"; //如果成績為3分,則印出NA
    else if (b==2)
        cout <<"Recult: CE\n"; //如果成績為2分,則印出CE
    else if (b==0)
        cout <<"Recult: SE\n"; //如果成績為0分,則印出SE

    cout <<"Final grade: "<<0.5*a+0.5*b;

    return 0;
}
