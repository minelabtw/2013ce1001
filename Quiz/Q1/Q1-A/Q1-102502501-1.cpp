
#include <iostream>
using namespace std;

int main()
{
    int number1 = 0 ;                                                    //設定一個整數變數，初始化其數值為0。
    int number2 = 0 ;                                                    //設定一個整數變數，初始化其數值為0。


    cout << "Quize1 point: ";                                            //在螢幕上顯示Quize1 point:
    cin >> number1;                                                      //請輸入一個數字
    while (number1 >10 or number1 <0)                                    //當整數變數大於十或小於零
    {
        cout << "Out of range!" << endl << "Quize1 point: ";             //當整數變數大於零或小於零時，將換行顯示Out of range!! 再換行顯示Quize1 point:
        cin >> number1;                                                  //請輸入一個數字
    }
    cout << "Quize2 point: ";                                            //在螢幕上顯示Quize2 point:
    cin >> number2;                                                      //請輸入一個數字
    while (number2 >10 or number2 <0)                                    //當整數變數大於十或小於零
    {
        cout << "Out of rang!" << endl << "Quize2 point: ";             //當整數變數大於零或小於零時，將換行顯示Out of range! 再換行顯示Quize2 point:
        cin >> number2;                                                 //請輸入一個數字
    }


    return 0;
}
