#include<iostream>

int main()
{
    int q1,q2;  //宣告

    do  //輸入Quiz1
    {
        std::cout<<"Quiz1 point: ";
        std::cin>>q1;
        if(q1<0||q1>10)
            std::cout<<"Out of range!\n";
    }while(q1<0||q1>10);

    std::cout<<"Result: ";  //輸出Quiz1結果
    switch (q1)
    {
    case 10:
        std::cout<<"AC";
        break;
    case 9:
        std::cout<<"II/WC";
        break;
    case 8:
        std::cout<<"WA";
        break;
    case 5:
        std::cout<<"TLE/MLE/OLE";
        break;
    case 3:
        std::cout<<"NA";
        break;
    case 2:
        std::cout<<"CE";
        break;
    case 0:
        std::cout<<"SE";
        break;
    }
    std::cout<<"\n";

    do  //輸入Quiz2
    {
        std::cout<<"Quiz2 point: ";
        std::cin>>q2;
        if(q2<0||q2>10)
            std::cout<<"Out of range!\n";
    }while(q2<0||q2>10);

    std::cout<<"Result: ";  //輸出Quiz2結果
    switch(q2)
    {
    case 10:
        std::cout<<"AC";
        break;
    case 9:
        std::cout<<"II/WC";
        break;
    case 8:
        std::cout<<"WA";
        break;
    case 5:
        std::cout<<"TLE/MLE/OLE";
        break;
    case 3:
        std::cout<<"NA";
        break;
    case 2:
        std::cout<<"CE";
        break;
    case 0:
        std::cout<<"SE";
        break;
    }

    std::cout<<"\nFinal grade: "<<(q1+q2+.0)/2; //輸出平均

    return 0;
}
