#include <iostream>
using namespace std;

main()
{
    int a,b;
    int c;
    int i=0;

    cout << "Quiz1 point: " ;
    cin >> a ;

    while(a>10 || a<0) //判斷a的範圍是否合理
    {
        cout << "Out of range!" << endl;
        cout << "Quiz1 point: ";
        cin >> a ;;
    }

    cout << "Quiz2 point: ";
    cin >> b ;

    while(b>10 || b<0) //判斷b的範圍是否合理
    {
        cout << "Out of range!" << endl;
        cout << "Quiz2 point: ";
        cin >> b ;
    }

    c = a*0.5 + b*0.5;
    cout << "Result: " ;

    while (i=0)
    {
        if(c=10)
            cout << "AC";
        else if(c=9)
            cout << "II/WC";
        else if(c=8)
            cout << "WA";
        else if(c=5)
            cout << "TLE/MLE/OLE";
        else if(c=3)
            cout << "NA";
        else if(c=2)
            cout << "CE";
        else if(c=0)
            cout << "SE";
    }
    cout << endl;

    cout << "Final grade: " << c;

    return 0;
}
