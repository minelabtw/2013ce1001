#include<iostream>
using namespace std ;
int main()
{
    int a1=0 ; //初值
    int d=0 ;//公差
    int n=0 ;//項數
    int p=0 ;//次方
    int sum1=1 ; //an
    int sum2=0 ; //sum
    cout << "Arithmetic progression\n";
    while(a1<=0) //輸入a1
    {
        cout << "a1: " ;
        cin >> a1 ;
        if(a1<=0)
        {
            cout << "Out of range\n" ;
        }
    }
    while(d<=0) //輸入d
    {
        cout << "d: " ;
        cin >> d ;
        if(d<=0)
        {
            cout << "Out of range\n" ;
        }
    }
    while(n<=0) //輸入n
    {
        cout << "n: " ;
        cin >> n ;
        if(n<=0)
        {
            cout << "Out of range\n" ;
        }
    }
    while(p<=0) //輸入p
    {
        cout << "p: " ;
        cin >> p ;
        if(p<=0)
        {
            cout << "Out of range\n" ;
        }
    }

    for(int i=1; i<=n; i++) //有幾項
    {
        sum1=1 ;          //每一項開始時，an先回復初執
        for(int j=1; j<=p; j++) //計算每項的值
        {
            sum1=sum1*(a1+d*(i-1)) ;
        }
        sum2=sum2+sum1 ; //將每一項加總
        cout << sum1 << "," ;
    }
    cout << "sum: " << sum2 << endl << endl ; //輸出sum

    cout << "Fibonacci sequence\n" ;
    n= 0 ; //項數
    sum1= 0 ;//暫存用
    sum2= 0 ;//前項
    int sum3 = 1;//後項
    int sum4 = 0 ;//加總
    while(n<=0)//輸入n
    {
        cout << "n: " ;
        cin >> n ;
        if(n<=0)
        {
            cout << "Out of range\n" ;
        }
    }
    for(int i=1; i<=n; i++)
    {
        cout << sum3 << "," ;//輸出後項
        sum4=sum4+sum3 ;//每項加總
        sum1=sum3;      //後項移到暫存
        sum3=sum1+sum2 ;//後巷等於前項加後項
        sum2=sum1;//前項等於後項（前）
    }
    cout << "sum: " << sum4 << endl ;
    return 0;
}
