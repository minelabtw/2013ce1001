#include <iostream>

using namespace std;

int main ()
{
    int quiz1 = 0 , quiz2 = 0 ;
    cout << "Quiz1 point : ";
    cin >>  quiz1;
    while ( quiz1 < 0 || quiz1 > 10)
    {
        cout << " Out of range ! " << endl << "Quiz1 point :" ;
        cin >> quiz1 ;
    }

    if ( quiz1==10 )
    {
        cout << "Result:" << "AC" << endl;
    }
    else if ( quiz1==9 )
    {
        cout << "Result:" << "II/WC" << endl;
    }
    else if ( quiz1==8 )
    {
        cout << "Result:" << "WA" << endl;
    }
    else if ( quiz1==5 )
    {
        cout << "Result:" << "TLE/MLE/OLE" << endl;
    }
    else if ( quiz1==3 )
    {
        cout << "Result:" << "NA" << endl;
    }
    else if ( quiz1==2  )
    {
        cout << "Result:" << "CE" << endl;
    }
    else if ( quiz1==0 )
    {
        cout << "Result:" << "SE" << endl;
    }

    cout << "Quiz2 point : ";
    cin >>  quiz2;

    while ( quiz2 < 0 || quiz2 > 10)
    {
        cout << " Out of range ! " << endl << "Quiz2 point :";
        cin >> quiz2 ;
    }
    int  y=quiz2;
    if ( quiz2==10 )
    {
        cout << "Result:" << "AC" << endl;
    }
    else if ( quiz2==9 )
    {
        cout << "Result:" << "II/WC" << endl;
    }
    else if ( quiz2==8 )
    {
        cout << "Result:" << "WA" << endl;
    }
    else if ( quiz2==5 )
    {
        cout << "Result:" << "TLE/MLE/OLE" << endl;
    }
    else if ( quiz2==3 )
    {
        cout << "Result:" << "NA" << endl;
    }
    else if ( quiz2==2  )
    {
        cout << "Result:" << "CE" << endl;
    }
    else if ( quiz2==0 )
    {
        cout << "Result:" << "SE" << endl;
    }


    float average;
    average = quiz1*0.5 + quiz2*0.5;
    cout << "Final grade:" << average;

    return 0;

}
