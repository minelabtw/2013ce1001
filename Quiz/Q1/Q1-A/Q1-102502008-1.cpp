#include<iostream>
using namespace std ;
int main()
{
    int point1=-1 ; //point1
    int point2=-1 ; //point2
    while(point1<0 || point1>10) // cin point1
    {
        cout << "Quize1 point: " ;
        cin >> point1 ;
        if(point1<0 || point1 >10) //if point1 不符合
            cout << "Out of range!" << endl ;
        else //if point1 符合
        {
            switch(point1) //判斷point1 是多少 輸出其成績
            {
            case 10 :
                cout << "Result: AC" << endl ;
                break ; //point=10
            case 9 :
                cout << "Result: II/WC" << endl ;
                break ; //point=9
            case 8 :
                cout << "Result: WA" << endl ;
                break ; //point=8
            case 5 :
                cout << "Result: TLE/MLE/OLE" << endl ;
                break ; //point=5
            case 3 :
                cout << "Result: NA" << endl ;
                break ; //point=3
            case 2 :
                cout << "Result: CE" << endl ;
                break ; //point=2
            case 0 :
                cout << "Result: SE" << endl ;
                break ; //point=0
            }
        }
    }
    while(point2<0 || point2>10)// cin point2
    {
        cout << "Quize2 point: " ;
        cin >> point2 ;
        if(point2<0 || point2 >10) //if point2 不符合
            cout << "Out of range!" << endl ;
        else //if point2 符合
        {
            switch(point2) //判斷point2 是多少 輸出其成績
            {
            case 10 :
                cout << "Result: AC" << endl ;
                break ; //point=10
            case 9 :
                cout << "Result: II/WC" << endl ;
                break ; //point=9
            case 8 :
                cout << "Result: WA" << endl ;
                break ; //point=8
            case 5 :
                cout << "Result: TLE/MLE/OLE" << endl ;
                break ; //point=5
            case 3 :
                cout << "Result: NA" << endl ;
                break ; //point=3
            case 2 :
                cout << "Result: CE" << endl ;
                break ; //point=2
            case 0 :
                cout << "Result: SE" << endl ;
                break ; //point=0
            }
        }
    }
    cout << "Final grade: " << ((float)point1+(float)point2)/2 ; // cout the final grade


    return 0;
}
