#include<iostream>
using namespace std;

int main ()
{
    int a,b;                                                  //宣告整數a,b

    cout << "Quiz1 point: ";                                  //要求輸入第一個分數
    cin >> a;                                                 //輸入第一個分數
    while(a<0 || a>10)                                        //當a超出範圍時，顯示超出範圍，並要求重新輸入
    {
        cout << "Out of Range! " << endl << "Quiz1 point: ";
        cin >> a;
    }

    cout << "Result: " ;                                      //要求顯示結果
    if(a==10)                                                 //並判斷結果屬於哪種程式評分規則
        cout << "AC";
    else if(a==9)
        cout << "II/WC";
    else if(a==8)
        cout << "WA";
    else if(a==5)
        cout << "TLE/MLE/OLE";
    else if(a==3)
        cout << "NA ";
    else if(a==2)
        cout << "CE ";
    else if(a==0)
        cout << "SE ";

    cout << endl;

    cout << "Quiz2 point: ";                                  //要求輸入第二個分數
    cin >> b;                                                 //輸入第二個分數
    while(b<0 || b>10)                                        //當b超出範圍時，顯示超出範圍，並要求重新輸入
    {
        cout << "Out of Range! " << endl << "Quiz2 point: ";  //
        cin >> b;
    }


    cout << "Result: " ;                                     //要求顯示結果
    if(b==10)                                                //並判斷結果屬於哪種程式評分規則
        cout << "AC";
    else if(b==9)
        cout << "II/WC";
    else if(b==8)
        cout << "WA";
    else if(b==5)
        cout << "TLE/MLE/OLE";
    else if(b==3)
        cout << "NA ";
    else if(b==2)
        cout << "CE ";
    else if(b==0)
        cout << "SE ";



    cout << endl << "Final grade: ";                        //要求顯示平均分數
    cout << (a+b)*0.5;                                      //顯示計算後的數值

    return 0;

}
