#include<iostream>
using namespace std;

int main()
{
    int number1=0;    //宣告一變數number1，並使其初始值為零。
    int number2=0;    //宣告一變數number2，並使其初始值為零。

    cout << "Quiz1 point: ";    //輸出Quiz1 point: 至螢幕。
    cin >> number1;    //輸入至number1。

    while(number1<0 || number1>10)    //while迴圈限制範圍在number1<0或number1>10。
    {
        cout << "Out of range!\n";    //輸出Out of range!至螢幕。
        cout << "Quiz1 point: ";    //輸出Quiz1 point: 至螢幕。
        cin >> number1;    //輸入至number1。
    }

    cout << "Result: ";    //輸出Result: 至螢幕。
    if(number1 == 10)
        cout << "AC";    //輸出AC至螢幕。
    else if(number1 == 9)
        cout << "II/WC";    //輸出II/WC至螢幕。
    else if(number1 == 8)
        cout << "WA";    //輸出WA至螢幕。
    else if(number1 == 5)
        cout << "TLE/MLE/OLE";    //輸出TLE/MLE/OLE至螢幕。
    else if(number1 == 3)
        cout << "NA";    //輸出NA至螢幕。
    else if(number1 == 2)
        cout << "CE";    //輸出CE至螢幕。
    else
        cout << "SE";    //輸出SE至螢幕。

    cout << "\nQuiz2 point: ";    //輸出Quiz2 point: 至螢幕。
    cin >> number2;    //輸入至number2。

    while(number2<0 || number2>10)    //while迴圈限制範圍在number2<0或number2>10。
    {
        cout << "Out of range!\n";    //輸出Out of range!至螢幕。
        cout << "Quiz2 point: ";    //輸出Quiz2 point: 至螢幕。
        cin >> number2;    //輸入至number2。
    }

    cout << "Result: ";    //輸出Result: 至螢幕。
    if(number2 == 10)
        cout << "AC";    //輸出AC至螢幕。
    else if(number2 == 9)
        cout << "II/WC";    //輸出II/WC至螢幕。
    else if(number2 == 8)
        cout << "WA";    //輸出WA至螢幕。
    else if(number2 == 5)
        cout << "TLE/MLE/OLE";    //輸出TLE/MLE/OLE至螢幕。
    else if(number2 == 3)
        cout << "NA";    //輸出NA至螢幕。
    else if(number2 == 2)
        cout << "CE";    //輸出CE至螢幕。
    else
        cout << "SE";    //輸出SE至螢幕。

    float sum=0;    //宣告一浮點數sum，並使其初始值為零。

    sum = (float)(number1 + number2)/2;    //運算。

    cout << "\nFinal grade: " << sum;    //輸出Final grade: 至螢幕。

    return 0;
}
