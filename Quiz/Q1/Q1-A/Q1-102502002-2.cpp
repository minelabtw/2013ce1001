#include <iostream>
using namespace std;

int main()
{
    int a1=0;     //宣告一整數變數a1，為等差數列之初項。
    int d=1;      //宣告一整數變數d，為等差數列之差。
    int n=1;      //宣告一整數變數n，為等差數列之項數。
    int N=1;      //宣告一整數變數N。
    int A=1;      //宣告一整數變數A。

    cout << "Arithmetic progression\n";   //將Arithmetic progression"輸出到螢幕。

    cout << "a1: ";                   //將"a: "輸出到螢幕。
    cin >> a1;                        //輸入a1。
    while ( a1<=0 )                   //當a1小於等於0。
    {
        cout << "Out of Range!\n" << "a1: ";    //將"Out of Range!"輸出到螢幕，並換行，重新輸出"a: "。
        cin >> a1;                   //重新輸入a1。
    }

    cout << "d: ";                 //將"d: "輸出到螢幕。
    cin >> d;                      //輸入d。
    while ( d<=0 )             //當d小於等於0。
    {
        cout << "Out of Range!\n" << "d: ";       //輸出"out of range!"，並換行重新輸出"d: "。
        cin >> d;                    //重新輸入d。
    }

    cout << "n: ";                   //輸出"n: "到螢幕。
    cin >> n;                         //輸入n。
    while ( n<=0 )                    //當n小於等於0。
    {
        cout << "Out of Range!\n" << "n: ";    //輸出"out of range!"，並換行重新輸出"n: "。
        cin >> n;                     //輸入n。
    }

    for( A=a1; A<=(a1+d*(n-1)); A+=d )       //對於A從a1到an，A加d。
        cout << A << ",";             //輸出A並以逗號分開。


    cout << "sum: " << (2*a1+d*(n-1))*n/2;      //輸出sum，數列總和。

    return 0;
}
