#include<iostream>
using namespace std;

int main()
{
    int a,d,n,p;
    cout << "Arithmetic progression" << endl;
    cout << "a1: ";
    cin >> a;
    while (a == 0)                                    //判定是否在範圍內
    {
        cout << "Out of range!"<<endl<<"a1: ";
        cin >>a;
    }
    cout << "d: ";
    cin >> d;
    while (d == 0)                                    //判定是否在範圍內
    {
        cout <<"Out of range!"<<endl<<"d: ";
        cin >> d;
    }
    cout << "n: ";
    cin >> n;
    while (n<=0)                                        //判定是否在範圍內
    {
        cout <<"Out of range!"<<endl<<"n: ";
        cin >> n;
    }
    cout << "p: ";
    cin >>p;
    while (p<=0)                                        //判定是否在範圍內
    {
        cout <<"Out of range!"<<endl<<"p: ";
        cin >>p;
    }
    int i,j,k;
    int sum = 0;
    for(i=1; i<=n; i++)                               //執行迴圈,輸出等同n的次數
    {
        int A = a;
        for(k=1; k<p; k++)                            //計算a的p次方
        {
            A = A * a;
        }
        sum = sum + A;                                //計算總和
        cout << A<< ",";
        a = a + d;                                     //等差
    }
    cout << "sum: " << sum << endl;

    int n2;
    int f1=1;
    int s,f2 = 0;
    cout <<endl<<"Fibonacci sequence"<<endl<<"n: ";
    cin >> n2;
    while (n<=0)                                     //判定是否在範圍內
    {
        cout <<"Out of range!"<<endl<<"n: ";
        cin >> n2;
    }
    for(j=1; j<=n2; j++)                          //計算費式數列
    {
        if(f1>f2)
        {
            cout << f1 <<",";
            f2 = f2 + f1;
        }
        else
        {
            cout << f2<<",";
            f1 = f1+f2;
        }



    }
    return 0;
}
