#include<iostream>
using namespace std;
int main()
{
    int grade1,grade2;
    double totalg;
    do
    {
        cout<<"Quiz1 point: ";
        cin>>grade1;
        if(grade1<0||grade1>10)
        {
            cout<<"Out of range!\n";
        }
        else if(grade1==10)
            cout<<"Result: AC\n";
        else if(grade1==9)
            cout<<"Result: II/WC\n";
        else if(grade1==8)
            cout<<"Result: WA\n";
        else if(grade1==5)
            cout<<"Result: TLE/MLE/OLE\n";
        else if(grade1==3)
            cout<<"Result: NA\n";
        else if(grade1==2)
            cout<<"Result: CE\n";
        else if(grade1==0)
            cout<<"Result: SE\n";
    }
    while(grade1<0||grade1>10);
    do
    {
        cout<<"Quiz2 point: ";
            cin>>grade2;
        if(grade2<0||grade2>10)
        {
            cout<<"Out of range!\n";
        }
        else if(grade2==10)
            cout<<"Result: AC\n";
        else if(grade2==9)
            cout<<"Result: II/WC\n";
        else if(grade2==8)
            cout<<"Result: WA\n";
        else if(grade2==5)
            cout<<"Result: TLE/MLE/OLE\n";
        else if(grade2==3)
            cout<<"Result: NA\n";
        else if(grade2==2)
            cout<<"Result: CE\n";
        else if(grade2==0)
            cout<<"Result: SE\n";
    }
    while(grade2<0||grade2>10);

        totalg=(grade1+grade2)*0.5;
        cout<<"Final grade: "<<totalg;
            return 0;
}
