#include <iostream>

using namespace std;

int main()
{
     double num1;  //設定變數
     double num2;
     double product;

    cout << "Quiz1 point: ";  //輸入變數一
    cin >> num1;

    while ( num1 < 0 || num1 > 10 )  //判斷變數是否超出範圍,若超出就輸出out of range! 然後重新輸入
    {
        if ( num1 < 0 || num1 > 10 )
        {
            cout << "out of range!" << endl;
            cout << "Quiz1 point: ";

            cin >> num1;
        }
    }
        if ( num1 == 10 )  //判斷輸入的分數在輸出原因
        {
            cout << "Result: AC" << endl;
        }
        if ( num1 == 9 )
        {
            cout << "Result: II/WC" << endl;
        }
        if ( num1 == 8 )
        {
            cout << "Result: WA" << endl;
        }
        if ( num1 == 5 )
        {
            cout << "Result: TLE/MLE/OLE" << endl;
        }
        if ( num1 == 3)
        {
            cout << "Result: NA" << endl;
        }
        if ( num1 == 2 )
        {
            cout << "Reault: CE" << endl;
        }
        if ( num1 == 0 )
        {
            cout << "Result: SE" << endl;
        }

    cout << "Quiz2 point: ";
    cin >> num2;

    while ( num2 < 0 || num2 > 10 )
    {
        if ( num2 < 0 || num2 > 10 )
        {
            cout << "out of range!" << endl;
            cout << "Quiz1 point: ";

            cin >> num2;
        }
    }
        if ( num2 == 10 )
        {
            cout << "Result: AC" << endl;
        }
        if ( num2 == 9 )
        {
            cout << "Result: II/WC" << endl;
        }
        if ( num2 == 8 )
        {
            cout << "Result: WA" << endl;
        }
        if ( num2 == 5 )
        {
            cout << "Result: TLE/MLE/OLE" << endl;
        }
        if ( num2 == 3)
        {
            cout << "Result: NA" << endl;
        }
        if ( num2 == 2 )
        {
            cout << "Reault: CE" << endl;
        }
        if ( num2 == 0 )
        {
            cout << "Result: SE" << endl;
        }

    product = ( num1 + num2 ) / 2;  //將兩數相加後除以2為平均
    cout << "Final grade: " << product << endl;  //輸出平均

    return 0;
}
