#include<iostream>
using namespace std;

int main()
{

    int a1=0;
    int d=0;
    int n=0;
    int an=0;
    int s=0;
    int n2=0;


    cout<<"Arithmetic progression"<<endl;
    cout<<"a1: ";
    cin>>a1;

    while(a1<=0)  //確保首項為正整數
    {
        cout<<"Out of range!"<<endl<<"a1: ";
        cin>>a1;
    }

    cout<<"d: ";
    cin>>d;

    while(d<=0)  //判定輸入之公差為正整數
    {
        cout<<"Out of range!"<<endl<<"d: ";
        cin>>d;
    }

    cout<<"n: ";
    cin>>n;

    while(n<=0)  //判定輸入項數為正整數
    {
        cout<<"Out of range!"<<endl<<"n: ";
        cin>>n;
    }

    an=a1 + d * (n-1);  //計算末項
    s=((a1+an)*n)/2;   //計算總合
    for(int a=a1; a<=an; a+=d)  //輸出等差為d的等差數列
    {
        cout<<a<<",";
    }

    cout<<"sum: "<<s;  //顯示出總和


    return 0;

}
