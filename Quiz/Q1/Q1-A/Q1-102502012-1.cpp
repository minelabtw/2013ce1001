#include<iostream>
using namespace std;
void judge(int score)  // judge the score
{
    cout<<"Result: ";
    if( score==10 )
        cout<<"AC"<<endl;
    else if( score==9 )
        cout<<"II/WC"<<endl;
    else if( score==8 )
        cout<<"WA"<<endl;
    else if( score==5 )
        cout<<"TLE/MLE/OLE"<<endl;
    else if( score==3 )
        cout<<"NA"<<endl;
    else if( score==2 )
        cout<<"CE"<<endl;
    else if( score==0 )
        cout<<"SE"<<endl;
}
int main()
{
    int a,b;
    do  // input a
    {
        cout<<"Quiz1 point: ";
        cin>>a;
    }
    while((a<0||a>10)&&cout<<"Out of range!\n");
    judge(a);
    do  // input b
    {
        cout<<"Quiz2 point: ";
        cin>>b;
    }
    while((b<0||b>10)&&cout<<"Out of range!\n");
    judge(b);
    cout<<"Final grade: "<<(double)(a+b)/2; // average
    return 0;
}
