#include <iostream>

using namespace std;

int main ()
{
    int k=0;
    int input=0;
    int sum=0;

    cout << "Arithmetic progression"  << endl;

    while (k<3)
    {
        if (k==0)
        {
            cout << "a1: ";
        }

        else if (k==1)
        {
            cout << "d: ";
        }

        else if (k==2)
        {
            cout << "n: ";
        }

        cin >> input;

        if (k<=0)
        {
            cout << "Out of range!" << endl;
        }

        else
        {
            k++;
        }
    }

}
