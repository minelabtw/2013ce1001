#include <iostream>

using namespace std;

int main()
{
    int a1 ;                                        //宣告整數變數，首項
    int d ;                                         //宣告整數變數，公差
    int n ;                                         //宣告整數變數，項數
    int p ;                                         //宣告整數變數，次方
    int c = 1 ;                                     //宣告整數變數
    int sum = 0;                                    //宣告整數變數
    int number = 0;                                 //宣告整數變數
    int multi = 1 ;
    int i = 1 ;



    cout << "Arithmetic progression" << endl;       //顯示字串
    cout << "a1: " ;
    cin  >> a1 ;                                    //將輸入值給a1

    while (a1<=0)                                   //製造迴圈，使輸入值大於0，否則繼續執行下列動作
    {
        cout << "Out of range!" << endl;
        cout << "a1: " ;
        cin  >> a1 ;
    }

    cout << "d: " ;
    cin  >> d ;

    while (d<=0)                                    //製造迴圈，使輸入值大於0，否則繼續執行下列動作
    {
        cout << "Out of range!" << endl;
        cout << "d: " ;
        cin  >> d ;
    }

    cout << "n: " ;
    cin  >> n ;

    while (n<=0)                                    //製造迴圈，使輸入值大於0，否則繼續執行下列動作
    {
        cout << "Out of range!" << endl;
        cout << "n: " ;
        cin  >> n ;
    }

    cout << "p: " ;
    cin  >> p ;


   while (p<=0)                                    //製造迴圈，使輸入值大於0，否則繼續執行下列動作
    {
        cout << "Out of range!" << endl;
        cout << "p: " ;
        cin  >> p ;
    }

    while (c<=n)                                    //製造迴圈條件(規定它次數)，繼續動作
    {
        number = a1+d*(c-1) ;                       //算出某項的公差數字
        while(i<=p)                                 //製造迴圈條件(利用i產生p次方)
        {
            multi = multi*number ;                  //不斷乘上該項數字自己，直到次數超過輸入值p
            i++;                                    //i+1，並回到迴圈，以利迴圈持續
        }
        cout << multi << "," ;                      //輸出某項的p次方數字
        sum = sum + multi ;                         //將算出的值不斷加入到一變數
        multi = 1;                                  //將整數變數初始化為1，以利迴圈繼續執行
        i = 1 ;                                     //同上
        c++;                                        //c+1以利出現n個數字
    }

    cout << "sum: " << sum ;                        //輸出最終總和



    return 0 ;
}
