#include <iostream>

using namespace std;

int main()
{
    int a = -1; //宣告型別為int
    int b = -1; //宣告型別為int
    float sum = 0; //宣告型別為float

    while (a<0 or a>10) //當超出範圍時輸出Out of range!
    {
        cout << "Quiz1 point: ";
        cin >> a;
        if (a<0 or a>10)
            cout << "Out of range!" << endl;
    }

    if (a==10) //依輸入條件輸出對應成績
        cout << "Result: AC" << endl;
    else if (a==9)
        cout << "Result: II/WC" << endl;
    else if (a==8)
        cout << "Result: WA" << endl;
    else if (a>=5 and a<8)
        cout << "Result: TLE/MLE/OLE" << endl;
    else if (a>=3 and a<5)
        cout << "Result: NA" << endl;
    else if (a==2)
        cout << "Result: CE" << endl;
    else if (a>=0 and a<2)
        cout << "Result: SE" << endl;

    while (b<0 or b>10) //當超出範圍時輸出Out of range!
    {
        cout << "Quiz2 point: ";
        cin >> b;
        if (b<0 or b>10)
            cout << "Out of range!" << endl;
    }

    if (b==10) //依輸入條件輸出對應成績
        cout << "Result: AC" << endl;
    else if (b==9)
        cout << "Result: II/WC" << endl;
    else if (b==8)
        cout << "Result: WA" << endl;
    else if (b>=5 and b<8)
        cout << "Result: TLE/MLE/OLE" << endl;
    else if (b>=3 and b<5)
        cout << "Result: NA" << endl;
    else if (b==2)
        cout << "Result: CE" << endl;
    else if (b>=0 and b<2)
        cout << "Result: SE" << endl;

    sum = (a + b) /2;
    cout << "Final grade: " << sum; //輸出平均

    return 0;
}
