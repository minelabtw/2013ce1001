#include <iostream>

using namespace std;

int main()
{
    int a;//宣告初項變數
    int d;//宣告公差變數
    int n;//宣告項數變數
    int p;//宣告次方變數
    int x = 1;//宣告迴圈變數
    int sum1;//宣告總和
    int sum2 = 1;
    int integer1 = 1;//宣告費氏數列變數1，初始值為1
    int integer2 = 1;//宣告費氏數列變數2
    int w;//費氏數列的項數
    int y = 1;//費氏數列迴圈變數

    cin >> a;//輸入a
    while ( a == 0 )//限制a的範圍
    {
        cout << "Out of range!" << endl;
        cin >> a;
    }

    cin >> d;
    while ( d == 0 )
    {
        cout << "Out of range!" << endl;
        cin >> d;
    }

    cin >> n;
    while ( n <= 0 )
    {
        cout << "Out of range!" << endl;
        cin >> n;
    }

    sum1 = a+d*(n-1);//計算總和的數值

    cin >> p;//輸入次方
    while ( x <= p )
    {
        sum2 = sum2*sum1;
        x++;
    }

    cout << sum2 << "=" << "(" << a << "+" << d << "*" << "(" << n << "-1)" << ")" << p << endl;//輸出等差數列的次方算式與集合

    cout << "費氏數列項數: ";//輸出字串
    cin >> w;//輸入變數

    if ( w%2 == 1 )
        w--;

    while ( y <= w/2 )//設定費氏數列迴圈
    {
        cout << integer1 << "," << integer2 << ",";//輸出變數1和變數2
        integer1 = integer1+integer2;//重新計算變數1
        integer2 = integer1+integer2;
        y++;//y+1
    }

    return 0;//返回初始值

}
