#include <iostream>

using namespace std;

int main()
{
    float a=-1;//設兩變數
    float b=-1;

    while (a<0 || a>10)//當a<0或a>10則進入迴圈
    {
        cout << "Quizl point: ";
        cin >> a;

        if(a<0 || a>10)//若輸入的a<0或a>10則進行下面動作
            cout << "Out of range!"<<endl;
        else//若if不成立,則進行下面動作
        {
            if (a==10)
                cout <<"Result: AC"<<endl;
            if (a==9)
                cout <<"Result: ||/WC"<<endl;
            if (a==8)
                cout <<"Result: WA"<<endl;
            if (a==5)
                cout <<"Result: TLE/MLE/OLE"<<endl;
            if (a==3)
                cout <<"Result: NA"<<endl;
            if (a==2)
                cout <<"Result: CE"<<endl;
            if (a==0)
                cout <<"Result: SE"<<endl;
        }
    }

    while (b<0 || b>10)
    {
        cout << "Quiz2 point: ";
        cin >> b;

        if(b<0 || b>10)
            cout << "Out of range!"<<endl;
        else
        {
            if (b==10)
                cout <<"Result: AC"<<endl;
            if (b==9)
                cout <<"Result: ||/WC"<<endl;
            if (b==8)
                cout <<"Result: WA"<<endl;
            if (b==5)
                cout <<"Result: TLE/MLE/OLE"<<endl;
            if (b==3)
                cout <<"Result: NA"<<endl;
            if (b==2)
                cout <<"Result: CE"<<endl;
            if (b==0)
                cout <<"Result: SE"<<endl;
        }
    }
    cout <<"Final grade: "<<(a+b)/2;//列出最終成績

    return 0;
}




