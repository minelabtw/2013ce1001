#include<iostream>
using namespace std;
int main()
{
    int a1,d,n;//宣告首項，公差，項數
    int x;//宣告一變數x
    int number;//宣告值number
    int sum=0;//宣告和sum
    cout <<"Arithmetic progression"<<endl;
    cout <<"a1: ";
    cin >>a1;
    while (a1<=0)//做迴圈使得a1>0
    {
        cout <<"Out of range!"<<endl;
        cout <<"a1: ";
        cin >>a1;
    }
    cout <<"d: ";
    cin >>d;
    while (d<=0)//做迴圈使得d>0
    {
        cout <<"Out of range!"<<endl;
        cout <<"d: ";
        cin >>d;
    }
    cout <<"n: ";
    cin >>n;
    while (n<=0)//做迴圈使得n>0
    {
        cout <<"Out of range!"<<endl;
        cout <<"n: ";
        cin >>n;
    }
    for (x=a1;x<=n;x++)//做迴圈印出等差級數
    {
        number=number+d;
        cout <<number<<",";
    }
    sum=(n*(2*a1+(n-1)*d))/2;//級數和
    cout <<"sum: "<<sum;
    return 0;
}
