#include <iostream>
using namespace std;

int main()
{
    double a;                 //宣告變數
    double b;
    double c;

    cout<<"Quiz1 point: ";
    cin>>a;
    while(a<0||a>10)          //使用迴圈判斷所輸入值是否符合要求
    {
        cout<<"Out of range!"<<endl;
        cout<<"Quiz1 point: ";
        cin>>a;
    }
    cout<<"Result: ";
    if(a==10)                 //使用if判斷"程式評分規則"
    {
        cout<<"AC"<<endl;
    }
    else if(a==9)
    {
        cout<<"II/WC"<<endl;
    }
    else if(a==8)
    {
        cout<<"WA"<<endl;
    }
    else if(a==5)
    {
        cout<<"TLE/MLE/OLE"<<endl;
    }
    else if(a==3)
    {
        cout<<"NA"<<endl;
    }
    else if(a==2)
    {
        cout<<"CE"<<endl;
    }
    else if(a==0)
    {
        cout<<"SE"<<endl;
    }

    cout<<"Quiz2 point: ";
    cin>>b;
    while(b<0||b>10)          //使用迴圈判斷所輸入值是否符合要求
    {
        cout<<"Out of range!"<<endl;
        cout<<"Quiz2 point: ";
        cin>>b;
    }
    cout<<"Result: ";
    if(b==10)                 //使用if判斷"程式評分規則"
    {
        cout<<"AC"<<endl;
    }
    else if(b==9)
    {
        cout<<"II/WC"<<endl;
    }
    else if(b==8)
    {
        cout<<"WA"<<endl;
    }
    else if(b==5)
    {
        cout<<"TLE/MLE/OLE"<<endl;
    }
    else if(b==3)
    {
        cout<<"NA"<<endl;
    }
    else if(b==2)
    {
        cout<<"CE"<<endl;
    }
    else if(b==0)
    {
        cout<<"SE"<<endl;
    }

    c=(a+b)/2;                //求平均值

    cout<<"Final grade: "<<c; //輸出結果

    return 0;
}
