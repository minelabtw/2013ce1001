#include<iostream>
using namespace std;

int main()
{

    int grade1 = 0;
    int grade2 = 0;
    float sum = 0;


    cout<<"Quiz1 point: ";
    cin>>grade1;


    while(grade1<0 or grade1>10)  //判斷第一項成績輸入是否在範圍內
    {
        if(grade1<0 or grade1>10)
        {
            cout<<"Out of range!"<<endl<<"Quiz1 point: ";
            cin>>grade1;
        }

    }

    cout<<"Result: ";

    if(grade1==10)  //就第一項成績判定屬於哪一個等級
    {
        cout<<"AC";
    }
    else if(grade1==9)
    {
        cout<<"II/WC";
    }
    else if(grade1==8)
    {
        cout<<"WA";
    }
    else if(grade1==5)
    {
        cout<<"TLE/MLE/OLE";
    }
    else if(grade1==3)
    {
        cout<<"NA";
    }
    else if(grade1==2)
    {
        cout<<"CE";
    }
    else if(grade1==0)
    {
        cout<<"SE";
    }
    else
    {
        cout<<" ";
    }

    cout<<endl;


    cout<<"Quiz2 point: ";
    cin>>grade2;

    while(grade2<0 or grade2>10)  //判斷第二項成績是否在範圍內
    {
        if(grade2<0 or grade2>10)
        {
            cout<<"Out of range!"<<endl<<"Quiz2 point: ";
            cin>>grade2;
        }

    }

    cout<<"Result: ";

    if(grade2==10)  //判定第二項成績符合哪一個等級並輸出
    {
        cout<<"AC";
    }
    else if(grade2==9)
    {
        cout<<"II/WC";
    }
    else if(grade2==8)
    {
        cout<<"WA";
    }
    else if(grade2==5)
    {
        cout<<"TLE/MLE/OLE";
    }
    else if(grade2==3)
    {
        cout<<"NA";
    }
    else if(grade2==2)
    {
        cout<<"CE";
    }
    else if(grade2==0)
    {
        cout<<"SE";
    }
    else
    {
        cout<<endl;
    }

    cout<<endl;

    sum=grade1 * 0.5 + grade2 * 0.5;  //兩個成績各占50%以算出總成績
    cout<<"Final grade: "<<sum<<endl;

    return 0;


}
