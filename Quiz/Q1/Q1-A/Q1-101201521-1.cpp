#include <iostream>
using namespace std;

int main()
    {
        double flag=0, p1=0, p2=0, input=0;//宣告變數為實數並設定初始值0

        while (flag<2)//輸入2個成績跳出while
        {
            if (flag==0)
            {
                cout << "Quiz1 point: ";//成績1
                cin >> input;
                p1=input;
            }
            else
            {
                cout << "Quiz2 point: ";//成績2
                cin >> input;
                p2=input;
            }
            if (input<0 || input>10)//超出範圍發出警告並重新詢問
            {
                cout << "Out of range!\n";
            }
            else
            {
                if (input==10)//對應分數輸出對應輸出
                    cout << "Result: AC\n";
                else if (input==9)
                    cout << "Result: II/WC\n";
                else if (input==8)
                    cout << "Result: WA\n";
                else if (input==5)
                    cout << "Result: TLE/MLE/OLE\n";
                else if (input==3)
                    cout << "Result: NA\n";
                else if (input==2)
                    cout << "Result: CE\n";
                else if (input==0)
                    cout << "Result: SE\n";
                flag++;
            }
        }
        cout << "Final grade: " << (p1+p2)/2;

        return 0;
    }
