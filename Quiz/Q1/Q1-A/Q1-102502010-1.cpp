#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    double quiz1; //第一次小考分數
    double quiz2; //第二次小考分數
    cout<<"Quiz1 point: ";
    cin>>quiz1;
    while(quiz1<0 || quiz1>10)  //判斷Q1是否在範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"Quiz1 point: ";
        cin>>quiz1;
    }
    if(quiz1==10)   //輸出Q1程式評分規則
        cout<<"Result: AC"<<endl;
    else if(quiz1==9)
        cout<<"Result: II/WC"<<endl;
    else if(quiz1==8)
        cout<<"Result: WA"<<endl;
    else if(quiz1==5)
        cout<<"Result: TLE/MLE/OLE"<<endl;
    else if(quiz1==3)
        cout<<"Result: NA"<<endl;
    else if(quiz1==2)
        cout<<"Result: CE"<<endl;
    else if(quiz1==0)
        cout<<"Result: SE"<<endl;
    cout<<"Quiz2 point: ";
    cin>>quiz2;
    while(quiz2<0 || quiz2>10)  //判斷Q2是否在範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"Quiz2 point: ";
        cin>>quiz2;
    }
    if(quiz2==10)  //輸出Q2程式評分規則
        cout<<"Result: AC"<<endl;
    else if(quiz2==9)
        cout<<"Result: II/WC"<<endl;
    else if(quiz2==8)
        cout<<"Result: WA"<<endl;
    else if(quiz2==5)
        cout<<"Result: TLE/MLE/OLE"<<endl;
    else if(quiz2==3)
        cout<<"Result: NA"<<endl;
    else if(quiz2==2)
        cout<<"Result: CE"<<endl;
    else if(quiz2==0)
        cout<<"Result: SE"<<endl;
    cout<<"Final grade: "<<setprecision(1)<<fixed<<(quiz1+quiz2)/2<<endl;  //輸出最後成績
    return 0;
}
