#include <iostream>
using namespace std;
int main()
{
    int num1;       //宣告一整數變數
    int num2;       //宣告另一整數變數
    float total=0;  //宣告一實數變數total

    cout<<"Quiz1 point: ";   //將字串"Quiz1 point: "顯示於螢幕上
    cin>>num1;               //將num1輸入至電腦

    if (num1>10 || num1<0)   //若num1大於10或小於0則顯示"Out of range!"並重新輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Quiz1 point: ";
        cin>>num1;
    }
    else if (num1==10)         //若num1為10,顯示"AC"
        cout<<"Result: AC";
    else if (num1==9)          //若num1為9,顯示"II/WC"
        cout<<"Result: II/WC";
    else if (num1==8)          //若num2為8,顯示"WA"
        cout<<"Result: WA";
    else if (num1==5)          //若num1為5,顯示"TEL/MLE/OLE"
        cout<<"Result: TEL/MLE/OLE";
    else if (num1==3)          //若num1為3,顯示"NA"
        cout<<"Result: NA";
    else if (num1==2)          //若num1為2,顯示"CE"
        cout<<"Result: CE";
    else if (num1==0)          //若num1為0,顯示"SE"
        cout<<"Result: SE";

    cout<<endl;

    cout<<"Quiz2 point: ";
    cin>>num2;
    if (num2>10 || num2<0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Quiz2 point: ";
        cin>>num2;
    }
    else if (num2==10)
        cout<<"Result: AC";
    else if (num2==9)
        cout<<"Result: II/WC";
    else if (num2==8)
        cout<<"Result: WA";
    else if (num2==5)
        cout<<"Result: TEL/MLE/OLE";
    else if (num2==3)
        cout<<"Result: NA";
    else if (num2==2)
        cout<<"Result: CE";
    else if (num2==0)
        cout<<"Result: SE";

    cout<<endl;

    total=num1*0.5+num2*0.5;      //Final grade=Quiz1*0.5+Quiz2*0.5
    cout<<"Final grade: "<<total; //將"Final grade: "顯示於螢幕,並顯示出total計算出之值
    return 0;
}
