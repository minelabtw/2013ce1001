#include <iostream>

using namespace std;

int main()
{
    int number1=0,number2=0,x=0;                      //宣告變數並設期使值為零

    cout <<"Quiz1 point: ";                           //輸出字元
    cin >>number1;                                    //輸入字元

    while(number1<0 || number1>10)                    //while迴圈(條件)
    {
        cout <<"Out of range!\n"<< "Quiz1 point: " ;
        cin >>number1;
    }
    if(number1==10)                                   //if判斷
    {
        cout <<"Result: AC\n";
    }
    else if(number1==9)
    {
        cout <<"Result: II/WC\n";
    }
    else if(number1==8)
    {
        cout <<"Result: WA\n";
    }
    else if(number1==5)
    {
        cout <<"Result: TLE/MLE/OLE\n";
    }
    else if(number1==3)
    {
        cout <<"Result: NA\n";
    }
    else if(number1==2)
    {
        cout <<"Result: CE\n";
    }
    else if(number1==0)
    {
        cout <<"Result: SE\n";
    }
    cout <<"Quiz2 point: ";
    cin >>number2;
    while(number2<0 || number2>10)
    {
        cout <<"Out of range!\n"<< "Quiz2 point: " ;
        cin >>number2;
    }
    if(number2==10)
    {
        cout <<"Result: AC\n";
    }
    else if(number2==9)
    {
        cout <<"Result: II/WC\n";
    }
    else if(number2==8)
    {
        cout <<"Result: WA\n";
    }
    else if(number2==5)
    {
        cout <<"Result: TLE/MLE/OLE\n";
    }
    else if(number2==3)
    {
        cout <<"Result: NA\n";
    }
    else if(number2==2)
    {
        cout <<"Result: CE\n";
    }
    else if(number2==0)
    {
        cout <<"Result: SE\n";
    }

    x=number1*0.5+number2*0.5;                         //計算
    cout <<"Final grade: ";
    cout <<x;

    return 0;

}
