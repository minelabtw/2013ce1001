#include <iostream>

using namespace std;

int main()
{

    int a1 =0; //初項
    int d =0; // 公差
    int n =0; // 項數
    int p =0; //次方
    int ansum =0 ; // 單項
    int product =0; // 項數乘機
    int sum =0 ; //總和

    cout << "Arithmetic progression\n";
    //輸入初項
    while(a1==0)
    {
        cout << "a1: ";
        cin >> a1;
        if(a1==0)
        {
            cout << "Out of range!\n";
        }
    }
    //輸入公差
    while(d==0)
    {
        cout << "d: ";
        cin >> d;
        if(d==0)
        {
            cout << "Out of range!\n";
        }
    }
    //輸入項數
    while(n<1)
    {
        cout << "n: ";
        cin >> n;
        if(n<1)cout << "Out if range!\n";
    }
    //輸入次方
    while(p<1)
    {
        cout << "p: ";
        cin >> p;
        if(p<1)cout << "Out if range!\n";
    }

    // 輸出
    for(int i=1; i<=n; i++)
    {
        ansum = a1 + d*(i-1);
        product = ansum;
        for(int j=1; j<p; j++)
        {
            product =  product * ansum;
        }
        sum = sum + product;
        cout << product << ",";
    }

    cout << "sum: "<< sum << endl << endl;

    cout << "Fibonacci sequence\n";
    int n2 = 0; //橡樹二
    //輸入項數二
    while(n2<1)
    {
        cout << "n: ";
        cin >> n2;
        if(n2<1)cout << "Out if range!\n";
    }

    int num1 =1,num2 =0;
    int sum2 =0;
    int sum3 =0;
    // 輸出
    for(int k=1; k<=n2; k++)
    {

        sum2 = num1 + num2;
        cout << sum2 << " ";
        sum3 = sum3 + sum2;
        num1 = num2;
        num2 = sum2;
    }
    cout << "sum: " << sum3;

    return 0;

}
