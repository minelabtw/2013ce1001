#include <iostream>
using namespace std;
int main()

{
    int a;  //宣告整數變數a
    int b;  //宣告整數變數b
    double c;  //宣告變數c

    cout << "Quiz1 point: ";  //輸出字串
    cin >> a;  //將輸入的值給a

    while (a<0 or a>10)  //使用while迴圈,若符合此條件則進行下列程式,否則跳出迴圈
    {
        cout << "Out of range !" << endl << "Quiz1 point: ";
        cin >> a;
    }

    if (a==10)  //若a值為10則輸出Result: AC
        cout << "Result: AC" << endl;
    else if (a==9)
        cout << "Result: II/WC" << endl;
    else if (a==8)
        cout << "Result: WA" << endl;
    else if (a==5)
        cout << "Result: TLE/MLE/OLE" << endl;
    else if (a==3)
        cout << "Result: NA" << endl;
    else if (a==2)
        cout << "Result: CE" << endl;
    else if (a==0)
        cout << "Result: SE" << endl;

    cout << "Quiz2 point: ";
    cin >> b;

    while (b<0 or b>10)  //使用while迴圈,若符合此條件則進行下列程式,否則跳出迴圈
    {
        cout << "Out of range !" << endl << "Quiz2 point: ";
        cin >> b;
    }

    if (b==10)  //若b值為10則輸出Result: AC
        cout << "Result: AC" << endl;
    else if (b==9)
        cout << "Result: II/WC" << endl;
    else if (b==8)
        cout << "Result: WA" << endl;
    else if (b==5)
        cout << "Result: TLE/MLE/OLE" << endl;
    else if (b==3)
        cout << "Result: NA" << endl;
    else if (b==2)
        cout << "Result: CE" << endl;
    else if (b==0)
        cout << "Result: SE" << endl;

    c=a*0.5+b*0.5;  //c為平均值
    cout << "Final grade: " << c;

    return 0;
}
