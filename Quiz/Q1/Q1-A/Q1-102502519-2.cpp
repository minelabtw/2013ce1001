#include<iostream>
using namespace std;

int main()
{

    int number1=0;    //宣告一變數number1=0
    int number2=0;    //宣告一變數number2=0
    int number3=0;    //宣告一變數number3=0
    int number4=0;    //宣告一變數number4=0

    cout << "Arithmetic progression\n";    //輸出Arithmetic progression

    cout << "a1: ";    //輸出a1:
    cin >> number1;    //輸入至number1

    while(number1<=0)
    {
        cout << "Out of range!\n";    //輸出Out of range!
        cout << "a1: ";    //輸出a1:
        cin >> number1;    //輸入至number1
    }

    cout << "d: ";    //輸出d:
    cin >> number2;    //輸入至number2

    while(number2<=0)
    {
        cout << "Out of range!\n";    //輸出Out of range!
        cout << "d: ";    //輸出d:
        cin >> number2;    //輸入至number2
    }

    cout << "n: ";    //輸出n:
    cin >> number3;    //輸入至number3

    while(number3<=0)
    {
        cout << "Out of range!\n";    //輸出Out of range!
        cout << "n: ";    //輸出n:
        cin >> number3;    //輸入至number3
    }

    cout << "p: ";    //輸出p:
    cin >> number4;    //輸入至number4

    while(number4<=0)
    {
        cout << "Out of range!\n";    //輸出Out of range!
        cout << "p: ";    //輸出p:
        cin >> number4;    //輸入至number4
    }

    int sum=0;    //宣告一變數sum=0

    for(int j=1 ; j<=number3 ; ++j)
    {
        cout << (number1+number2*(j-1))*(number1+number2*(j-1)) << ",";    //輸出運算值
        sum = sum + (number1+number2*(j-1))*(number1+number2*(j-1));
    }

    cout << "sum: " << sum;    //輸出sum:

    return 0;
}
