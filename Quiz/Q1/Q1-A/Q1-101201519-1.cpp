#include <iostream>
using namespace std;

int main()
{
    int point1,point2;//�O���Z1,���Z2
    double score;

    cout <<"Quiz1 point: ";
    cin >> point1;
    while(point1<0 || point1>10)//�P�_���Z1�d��
    {
        cout << "Out of range!" << endl;
        cout <<"Quiz1 point: ";
        cin >> point1;
    }
    if(0<=point1<=10)//�P�_���Z1�϶�
    {
        if(point1==0)
            cout << "Result: SE";
        else if(point1==2)
            cout << "Result: CE";
        else if(point1==3)
            cout << "Result: NA";
        else if(point1==5)
            cout << "Result: TLE/MLE/OLE";
        else if(point1==8)
            cout << "Result: WA";
        else if(point1==9)
            cout << "Result: II/WC";
        else if(point1==10)
            cout << "Result: AC";
    }
    cout << endl;
    cout <<"Quiz2 point: ";
    cin >> point2;
    while(point2<0 || point2>10)//�P�_���Z2�d��
    {
        cout << "Out of range!" << endl;
        cout <<"Quiz2 point: ";
        cin >> point2;
    }
    if(0<=point2<=10)//�P�_���Z2�϶�
    {
        if(point2==0)
            cout << "Result: SE";
        else if(point2==2)
            cout << "Result: CE";
        else if(point2==3)
            cout << "Result: NA";
        else if(point2==5)
            cout << "Result: TLE/MLE/OLE";
        else if(point2==8)
            cout << "Result: WA";
        else if(point2==9)
            cout << "Result: II/WC";
        else if(point2==10)
            cout << "Result: AC";
    }
    cout << endl;
    score=(point1+point2)/2.0;//��X����
    cout << "Final grade: " << score;

    return 0;

}
