#include <iostream>

using namespace std;

int main()
{
    int a = 0;
    int b = 0;
    int d = 0;
    int e = 1;
    int n = 0;
    int i = 0;
    int sum = 0;

    cout << "Arithmetic progression" << endl;//輸出字彙後並將a的值輸入
    cout << "a1: ";
    cin >> a;
    while ( a == 0 )//當a=0時做出以下動作
    {
        cout << "Out of range!" << endl;
        cout << "a1: ";
        cin >> a;
    }

    cout << "d: ";
    cin >> d;
    while ( d == 0 )//當d=0時做出以下動作
    {
        cout << "Out of range!" << endl;
        cout << "d: ";
        cin >> d;
    }

    cout << "n: ";//當n小時0時做出以下動作
    cin >> n;
    while ( n <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "n: ";
        cin >> n;
    }

    b = a + d * ( n - 1 );//將數列的最大值給b
    sum = ( a + b ) * n * 0.5;//將數列總和給sum

    for ( int i = a ; i <= b ; i++ )//定義i初始為a,最大值為b,i做持續增加動作
    {
        while ( e <= n && i == a + d * ( e - 1 ) )//當e比n小且i等於算式結果時輸出字彙,e做持續增加動作
        {
            cout << i << ",";
            e++;
        }
    }
    cout << "sum: " << sum;//將字彙打在後面並加入sum的值

    return 0;
}
