# include <iostream>

using namespace std;

int main ()
{
    int a=0;
    double b=0.5;
    int input=0;
    double total=0;

    while (a<2)
    {
        if (a==0)
        {
            cout << "Quiz1 point: ";
        }

        else if (a==1)
        {
            cout << "Quiz2 point: ";
        }

        cin >> input;

        if (input>10 || input <0)
        {
            cout << "Out of range!\n";
        }

        else
        {
            a++;
            total = total + input*b;
            cout << "Result: ";
            if(input==10)
            {
                cout << "AC";
            }

            else if(input==9)
            {
                cout << "II/WC";
            }

            else if(input==8)
            {
                cout << "WA";
            }

            else if(input==5)
            {
                cout << "TLE/MLE/OLE";
            }

            else if(input==3)
            {
                cout << "NA";
            }

            else if(input==2)
            {
                cout << "CE";
            }

            else if(input==0)
            {
                cout << "SE";
            }

            cout << endl;
        }



    }

    cout << "Final grade: " << total;

    return 0;
}
