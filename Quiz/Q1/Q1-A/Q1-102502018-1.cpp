#include <iostream>
using namespace std;

int main()
{
    float a;
    float b;                         //宣告變數a.b
    cout<<"Quiz1 point: :";
    cin>>a;
    while(a<0 || a>10)               //判斷a是否符合範圍
    {
        cout<<"Out of range!\n";
        cout<<"Quiz1 point: :";
        cin>>a;
    }
    if(a==10)                        //判斷成績的評分規則
    {
        cout<<"Result: AC\n";
    }
    else if(a==9)
    {
        cout<<"Result: II/WC\n";
    }
    else if(a==8)
    {
        cout<<"Result: WA\n";
    }
    else if(a==5)
    {
        cout<<"Result: TLE/MLE/OLE\n";
    }
    else if(a==3)
    {
        cout<<"Result: NA\n";
    }
    else if(a==2)
    {
        cout<<"Result: CE\n";
    }
    else if(a==0)
    {
        cout<<"Result: SE";
    }
    else
    {

    }
    cout<<"Quiz2 point: :";
    cin>>b;
    while(b<0 || b>10)              //判斷b是否符合範圍
    {
        cout<<"Out of range!\n";
        cout<<"Quiz2 point: :";
        cin>>b;
    }
    if(b==10)                       //判斷成績的評分規則
    {
        cout<<"Result: AC\n";
    }
    else if(b==9)
    {
        cout<<"Result: II/WC\n";
    }
    else if(b==8)
    {
        cout<<"Result: WA\n";
    }
    else if(b==5)
    {
        cout<<"Result: TLE/MLE/OLE\n";
    }
    else if(b==3)
    {
        cout<<"Result: NA\n";
    }
    else if(b==2)
    {
        cout<<"Result: CE\n";
    }
    else if(b==0)
    {
        cout<<"Result: SE\n";
    }
    else
    {

    }
    cout<<"Final grade: "<<(a+b)/2;       //計算最後平均



    return 0;

}
