#include <iostream>

using namespace std;
string judge_score(int score)//判斷成績
{
    if(score==10)
        return "AC";
    else if(score==9)
        return "II/WC";
    else if(score==8)
        return "WA";
    else if(score>=5)
        return "TLE/MLE/OLE";
    else if(score>=3)
        return "NA";
    else if(score>=2)
        return "CE";
    else
        return "SE";
}
int main()
{
    double quiz1=-1;
    double quiz2=-1;
    while(quiz1<0||quiz1>10)
    {
        cout << "Quiz1 point: ";
        cin >> quiz1;
        if(quiz1<0||quiz1>10)//判斷合理性
        {
            cout << "Out of range!" << endl;
        }
    }
    cout << "Result: " << judge_score(quiz1) << endl;
    while(quiz2<0||quiz2>10)
    {
        cout << "Quiz2 point: ";
        cin >> quiz2;
        if(quiz2<0||quiz2>10)//判斷合理性
        {
            cout << "Out of range!" << endl;
        }
    }
    cout << "Result: " << judge_score(quiz2) << endl;
    cout << "Final grade: " << (quiz1+quiz2)/2 << endl;
    return 0;
}
