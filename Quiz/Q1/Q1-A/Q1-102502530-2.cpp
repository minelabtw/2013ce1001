#include<iostream>

int pow(int x, int k)   //計算次方
{
    if(k==1)
        return x;
    return pow(x*x,k/2)*(k&1?x:1);
}

int main()
{
    int a1,d,n,p,sum,temp,f[2]={1,0};   //宣告

    std::cout<<"Arithmetic progression\n";  //等差數列

    do  //輸入a1
    {
        std::cout<<"a1: ";
        std::cin>>a1;
        if(a1<=0)
            std::cout<<"Out of range!\n";
    }while(a1<=0);

    do  //輸入d
    {
        std::cout<<"d: ";
        std::cin>>d;
        if(d<=0)
            std::cout<<"Out of range!\n";
    }while(d<=0);

    do  //輸入n
    {
        std::cout<<"n: ";
        std::cin>>n;
        if(n<=0)
            std::cout<<"Out of range!\n";
    }while(n<=0);

    do  //輸入p
    {
        std::cout<<"p: ";
        std::cin>>p;
        if(p<=0)
            std::cout<<"Out of range!\n";
    }while(p<=0);

    std::cout<<a1<<','; //輸出結果
    sum=a1;
    for(int i=1;i!=n;i++)
    {
        temp=pow(a1+d*i,p);
        std::cout<<temp<<',';
        sum+=temp;
    }
    std::cout<<"sum: "<<sum<<'\n';



    std::cout<<"\nFibonacci sequence\n";    //費氏數列

    do  //輸入n
    {
        std::cout<<"n: ";
        std::cin>>n;
        if(n<=0)
            std::cout<<"Out of range!\n";
    }while(n<=0);

    sum=0;
    for(int i=0;i!=n;i++)   //輸出結果
    {
        f[i%2]=f[0]+f[1];
        std::cout<<f[i%2]<<',';
        sum+=f[i%2];
    }
    std::cout<<"sum: "<<sum;

    return 0;
}
