#include <iostream>
using namespace std;

int main()
{
    int q1=0;   //宣告一整數變數q1，並令其初始值為0。
    int q2=0;   //宣告一整數變數q2，並令其初始值為0。

    cout << "Quiz1 point: ";      //將"Quiz1 point: "輸出至螢幕。
    cin >> q1;                    //輸入q1值。
    while ( q1>10 || q1<0 )       //當q1大於10或小於0
    {
        cout << "Out of Range!\n" << "Quiz1 point: ";   //將"Out of Range!"輸出至螢幕，並換行再次輸出"Quiz1 point: "。
        cin >> q1;                //重新輸入q1值。
    }
    if (q1==10)                          //若q1等於10
        cout << "Result: AC\n";          //輸出"Result: AC"，後換行。
    else if (q1==9)                      //若q1等於9
        cout << "Result: II/WC\n";       //輸出"Result: II/WC"，後換行。
    else if (q1==8)                      //若q1等於8
        cout << "Result: WA\n";          //輸出"Result: WA"，後換行。
    else if (q1==5)                      //若q1等於5
        cout << "Result: TLE/MLE/OLE\n"; //輸出"Result: TLE/MLE/OLE"，後換行。
    else if (q1==3)                      //若q1等於3
        cout << "Result: NA\n";          //輸出"Result: NA"，後換行。
    else if (q1==2)                      //若q1等於2
        cout << "Result: CE\n";          //輸出"Result: CE"，後換行。
    else if (q1==0)                      //若q1等於0
        cout << "Result: SE\n";          //輸出"Result: SE"，後換行。
    else;

    cout << "Quiz2 point: ";
    cin >> q2;
    while ( q2>10 || q2<0 )
    {
        cout << "Out of Range!\n" << "Quiz2 point: ";
        cin >> q2;
    }
    if (q2==10)                          //若q2等於10
        cout << "Result: AC\n";          //輸出"Result: AC"，後換行。
    else if (q2==9)                      //若q2等於9
        cout << "Result: II/WC\n";       //輸出"Result: II/WC"，後換行。
    else if (q2==8)                      //若q2等於8
        cout << "Result: WA\n";          //輸出"Result: WA"，後換行。
    else if (q2==5)                      //若q2等於5
        cout << "Result: TLE/MLE/OLE\n"; //輸出"Result: TLE/MLE/OLE"，後換行。
    else if (q2==3)                      //若q2等於3
        cout << "Result: NA\n";          //輸出"Result: NA"，後換行。
    else if (q2==2)                      //若q2等於2
        cout << "Result: CE\n";          //輸出"Result: CE"，後換行。
    else if (q2==0)                      //若q2等於0
        cout << "Result: SE\n";          //輸出"Result: SE"，後換行。
    else;                                //其餘例外不做任何事。

    cout << "Final Grade: " << q1*0.5+q2*0.5;   //輸出"Final Grade: "，最終成績等於q1(50%)+q2(50%)

    return 0;
}
