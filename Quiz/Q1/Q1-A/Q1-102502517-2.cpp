#include <iostream>

using namespace std;

int main()
{
    int a1 = 0; //宣告型別為int
    int d = 0; //宣告型別為int
    int n = 0; //宣告型別為int

    cout << "Arithmetic progression" << endl;

    while (a1==0) //當超出範圍時輸出Out of range!
    {
        cout << "a1: ";
        cin >> a1;
        if (a1==0)
            cout << "Out of range!" << endl;
    }

    while (d==0) //當超出範圍時輸出Out of range!
    {
        cout << "d: ";
        cin >> d;
        if (d==0)
            cout << "Out of range!" << endl;
    }

    while (n<=0) //當超出範圍時輸出Out of range!
    {
        cout << "n: ";
        cin >> n;
        if (n<=0)
            cout << "Out of range!" << endl;
    }

    int n_min = -1;
    int sum = a1;

    cout << a1 << ",";

    while(n_min<(n-2)) //輸出
    {
        a1 = a1 + d;
        sum=a1 + sum;
        cout << a1 << ",";
        n_min++;
    }

    cout << "sum: " << sum << endl;

    cout << "Fibonacci sequence" << endl;

    int a0 = 1;
    int a2 = 1;
    int a3 = 0;
    int n1 = 0;
    int n1_min = 3;
    int sum1 = 2;

    while (n1<=0)
    {
        cout << "n: ";
        cin >> n1;
        if (n1<=0)
            cout << "Out of range!" << endl;
    }

    cout << "1,1,";

    while(n1_min<=n1)
    {
        a3 = a2 + a0;
        cout << a3 << ",";
        sum1 = sum1 + a3;
        a0 = a2;
        n1_min++;
    }

    cout << "sum: " << sum1;
}
