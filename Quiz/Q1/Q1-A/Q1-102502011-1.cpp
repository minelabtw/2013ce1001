#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    double number1 = 0 , number2 = 0 ; //宣告兩數並數值化為零

    cout << "Quiz1 point: " ; //輸入第一個數
    cin >> number1 ;

    while(number1<0 || number1>10) //超出範圍的數的情況
    {
        cout << "Out of range!" << endl ;
        cout << "Quiz1 point: " ;
        cin >> number1 ;
    }

    if(number1==10)
        cout << "Result: AC" << endl ;        //當分數為下列數字的情況
    else if(number1==9)
        cout << "Result: II/WC" << endl ;
    else if(number1==8)
        cout << "Result: WA" << endl ;
    else if(number1==5)
        cout << "Result: TLE/MLE/OLE" << endl ;
    else if(number1==3)
        cout << "Result: NA" << endl ;
    else if(number1==2)
        cout << "Result: CE" << endl ;
    else if(number1==0)
        cout << "Result: SE" << endl ;

    cout << "Quiz2 point: " ; //數入第二個數
    cin >> number2 ;

    while(number2<0 || number2>10)
    {
        cout << "Out of range!" << endl ;
        cout << "Quiz2 point: " ;
        cin >> number2 ;
    }

    if(number2==10)
        cout << "Result: AC" << endl ;
    else if(number2==9)
        cout << "Result: II/WC" << endl ;
    else if(number2==8)
        cout << "Result: WA" << endl ;
    else if(number2==5)
        cout << "Result: TLE/MLE/OLE" << endl ;
    else if(number2==3)
        cout << "Result: NA" << endl ;
    else if(number2==2)
        cout << "Result: CE" << endl ;
    else if(number2==0)
        cout << "Result: SE" << endl ;

    double Final = 0 ; //計算結果
    Final = ( number1 + number2 ) / 2 ; //公式

    cout << "Final grade: " << Final ; //輸出結果

    return 0 ;
}
