#include <iostream>
using namespace std;
int main()
{
    int a;                                      //a 初項 b 公差 c 項數 d 次方 f 每一個項的值 h 次方項的值(未次方) k 次方項的值
    int b;
    int c;
    int d;
    int f;
    int h;
    int k=1;
    int j=1;                                    //費氏 一二項 = j l
    int l=1;
    int m;                                      // 費氏項數值(三項起)
    int sum=0;                                  // 各數列的和
    int sum2=0;
    int sum3=2;                                 //從第三項開始加
    cout<<"Arithmetic progression"<<endl;
    cout<<"a1: ";
    cin>>a;
    while(a<=0)
    {
        cout<<"Out of range!"<<endl;            //超出範圍則重輸入
        cout<<"a1: ";
        cin>>a;
    }
    cout<<"d: ";
    cin>>b;
    while(b<=0)
    {
        cout<<"Out of range!"<<endl;            //超出範圍則重輸入
        cout<<"d: ";
        cin>>b;
    }
    cout<<"n: ";
    cin>>c;
    while(c<=0)
    {
        cout<<"Out of range!"<<endl;            //超出範圍則重輸入
        cout<<"n: ";
        cin>>c;
    }
    cout<<"p: ";
    cin>>d;
    while(d<=0)
    {
        cout<<"Out of range!"<<endl;            //超出範圍則重輸入
        cout<<"p: ";
        cin>>d;
    }
    for (int e=1; e<=c; e++)                    //迴圈 項數次
    {
        f = a+b*(e-1);                          //計算各項值
        sum=sum+f;                              //數列和
        cout<<f<<",";
    }
    cout<<"sum: "<<sum<<endl;
    for (int g=1; g<=c; g++)                    //迴圈 項數次
    {
        h =(a+b*(g-1));                         //計算項數值
        for (int i=1; i<=d; i++)                //項數值 乘次方
        {
            k = k *h;
        }
        sum2 =sum2+k;                           //數列和
        cout<<k<<",";
        k=1;                                    //還原值
    }
    cout<<"sum: "<<sum2<<endl;
    cout<<endl<<"Finbonacci sequence"<<endl;
    cout<<"n: ";
    cin>>c;
    while(c<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"n: ";
        cin>>c;
    }
    for (int n=1; n<=c; n++)                    //迴圈項數次
    {

        if(n==1 || n==2)                        //第一 第二項 輸出1
        {
            cout<<"1,";
        }
        else
        {
            m=j+l;                              //計算前兩項和
            j=l;                                //給第一項第二項的值(給後一項的值)
            l=m;                                //給第二項第三項的值(給後一項的值)
            sum3 = sum3+m;                      //數列和
            cout<<m<<",";
        }

    }
    cout<<"sum: "<<sum3<<endl;
    return 0;
}
