#include <iostream>
using namespace std;
int main()
{
    double Quiz1=-1 , Quiz2=-1 , Finalgrade=0;
    while(Quiz1<0||Quiz1>10)
    {
        cout << "Quiz1 point: ";
        cin >> Quiz1;
        if(Quiz1<0||Quiz1>10)
            cout << "Out of range!\n";
        else if(Quiz1==10)                                             //成績分級
            cout << "Result: AC\n";
        else if(Quiz1==9)
            cout << "Result: II/WC\n";
        else if(Quiz1==8)
            cout << "Result: WA\n";
        else if(Quiz1==5)
            cout << "Result: TLE/MLE/OLE\n";
        else if(Quiz1==3)
            cout << "Result: NA\n";
        else if(Quiz1==2)
            cout << "Result: CE\n";
        else if(Quiz1==0)
            cout << "Result: SE\n";
    }
    while(Quiz2<0||Quiz2>10)
    {
        cout << "Quiz2 point: ";
        cin >> Quiz2;
        if(Quiz2<0||Quiz2>10)
            cout << "Out of range!\n";
        else if(Quiz2==10)
            cout << "Result: AC\n";
        else if(Quiz2==9)
            cout << "Result: II/WC\n";
        else if(Quiz2==8)
            cout << "Result: WA\n";
        else if(Quiz2==5)
            cout << "Result: TLE/MLE/OLE\n";
        else if(Quiz2==3)
            cout << "Result: NA\n";
        else if(Quiz2==2)
            cout << "Result: CE\n";
        else if(Quiz2==0)
            cout << "Result: SE\n";
    }
    Finalgrade = Quiz1*0.5 + Quiz2*0.5;                                //算其平均
    cout << "Final grade: " << Finalgrade;

    return 0;
}
