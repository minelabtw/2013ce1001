#include <iostream>

using namespace std;

int main()
{
    int number1 ;                                       //宣告整數變數，輸入的第一個值
    int number2 ;                                       //宣告整數變數，輸入的第二個值
    double sum ;                                        //宣告實數變數
    double grade ;                                      //宣告實數變數

    cout << "Quiz1 point: " ;                           //顯示字串
    cin  >> number1 ;                                   //將輸入值給number1


    while (number1>10 || number1<0)                     //設定迴圈條件，執行至輸入值介於0-10
    {
        cout << "Out of range!" << endl ;               //符合條件時執行的動作
        cout << "Quiz1 point: " ;
        cin  >> number1 ;
    }

    if (number1==10)                                    //利用if else去選擇符合條件時該執行的動作
    {
        cout << "Result: AC" << endl;
    }

    else if (number1==9)
    {
        cout << "Result: II/WC" << endl;
    }

    else if (number1==8)
    {
        cout << "Result: WA" << endl;
    }

    else if (number1==5)
    {
        cout << "Result: TLE/MLE/OLE" << endl;
    }

    else if (number1==3)
    {
        cout << "Result: NA" << endl;
    }

    else if (number1==2)
    {
        cout << "Result: CE" << endl;
    }

    else if (number1==0)
    {
        cout << "Result: SE" << endl;
    }


    cout << "Quiz2 point: " ;                                   //顯示字串
    cin  >> number2 ;                                           //將輸入值給number2

    while (number2>10||number2<0)                               //製造迴圈條件，執行動作至輸入值符合0-10之間
    {
        cout << "Out of range!" << endl;                        //顯示字串
        cout << "Quiz2 point: " ;
        cin  >> number2 ;
    }

    if (number2==10)                                            //同number1利用if else去選擇該執行的動作
    {
        cout << "Result: AC" << endl;
    }

    else if (number2==9)
    {
        cout << "Result: II/WC" << endl;
    }

    else if (number2==8)
    {
        cout << "Result: WA" << endl;
    }

    else if (number2==5)
    {
        cout << "Result: TLE/MLE/OLE" << endl;
    }

    else if (number2==3)
    {
        cout << "Result: NA" << endl;
    }

    else if (number2==2)
    {
        cout << "Result: CE" << endl;
    }

    else if (number2==0)
    {
        cout << "Result: SE" << endl;
    }

    sum = (number1 + number2) ;                                     //利用sum將數字總和
    grade = sum/2 ;                                                 //利用實數變數的sum和grade去顯示出小數點後的數字
    cout << "Final grade: " << grade ;                              //顯示出字串及最終平均分數

    return 0 ;
}
