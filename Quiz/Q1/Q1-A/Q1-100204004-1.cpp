#include<iostream>

using namespace std;

int main()
{
    int q1;//quiz1 point
    int q2;//quiz1 point
    double fg=0;//final grade

    while(1)//限制Quiz1分數一定要符合範圍，否則重新輸入
    {
        cout<<"Quiz1 point: ";
        cin>>q1;
        if(q1<0||q1>10)
            cout<<"Out of range!"<<endl;
        else if(q1==10)//10分為AC
        {
            cout<<"Result: AC"<<endl;
            break;
        }

        else if(q1==9)//9分為II/WC
        {
            cout<<"Result: II/WC"<<endl;
            break;
        }
        else
            break;
    }
    while(1)//限制Quiz2分數一定要符合範圍，否則重新輸入
    {
        cout<<"Quiz2 point: ";
        cin>>q2;
        if(q2<0||q2>10)
            cout<<"Out of range!"<<endl;
        else if(q2==10)//10分為AC
        {
            cout<<"Result: AC"<<endl;
            break;
        }

        else if(q2==9)//9分為II/WC
        {
            cout<<"Result: II/WC"<<endl;
            break;
        }

        else
            break;
    }
    fg=q1*0.5+q2*0.5;//Final grade算法
    cout<<"Final grade: "<<fg;
    return 0;

}
