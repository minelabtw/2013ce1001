#include<iostream>
using namespace std;

int main()
{
    int a=0; //輸入的第一個小考成績
    int b=0; //輸入的第二個小考成績
    int c=0; //最後總平均成績

    cout << "Quiz1 point: " ;
    while (cin>>a) //判斷a成績所對應的評分規則
    {
        if(a<0 || 10<a)
        {
            cout << "Out of range!!" << endl << "Quiz1 point: " ;
        }
        else if(a==10)
        {
            cout << "Result: AC" << endl ;
            break;
        }
        else if(a==9)
        {
            cout << "Result: II/WC" << endl ;
            break;
        }

        else if(a==8)
        {
            cout << "Result: WA" << endl ;
            break;
        }
        else if(a==5)
        {
            cout << "Result: TLE/MLE/OLE" << endl ;
            break;
        }
        else if(a==3)
        {
            cout << "Result: NA" << endl ;
            break;
        }
        else if(a==2)
        {
            cout << "Result: CE" << endl;
            break;
        }
        else if(a==0)
        {
            cout << "Result: SE" << endl;
            break;
        }
        else
            break ;
    }

    cout << "Quiz2 point: " ;
    while (cin>>b) //判斷b成績所對應的評分規則
    {
        if(b<0 || 10<b)
        {
            cout << "Out of range!!" << endl << "Quiz2 point: " ;
        }
        else if(b==10)
        {
            cout << "Result: AC" << endl ;
            break;
        }
        else if(b==9)
        {
            cout << "Result: II/WC" << endl ;
            break;
        }

        else if(b==8)
        {
            cout << "Result: WA" << endl ;
            break;
        }
        else if(b==5)
        {
            cout << "Result: TLE/MLE/OLE" << endl ;
            break;
        }
        else if(b==3)
        {
            cout << "Result: NA" << endl ;
            break;
        }
        else if(b==2)
        {
            cout << "Result: CE" << endl;
            break;
        }
        else if(b==0)
        {
            cout << "Result: SE" << endl;
            break;
        }
        else
            break ;
    }

    c = (a+b)*0.5; //最後總平均成績
    cout << "Final grade: " << c << endl ;

    return 0;
}
