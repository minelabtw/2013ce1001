#include <iostream>
using namespace std;

void Square(); //function prototype
void Cube();
void Factorial();
int x,Fac;

int main()
{
    cout << "Please enter a number( >0 ): ";
    cin >> x;
    while (x<=0) //讓使用者輸入一個大於0的數字
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ): ";
        cin >> x;
    }
    cout << "Square: ";
    Square();
    cout << "Cube: ";
    Cube();
    cout << "Factorial: ";
    Factorial();
    return 0;
}

void Square() //利用function運算輸出該數字的平方
{
    Fac = x*x; //重新定義Fac
    cout << Fac << endl;
}

void Cube() //利用function運算輸出該數字的立方
{
    Fac = x*x*x; //重新定義Fac
    cout << Fac << endl;
}

void Factorial() //利用function運算輸出該數字的階層
{
    Fac = 1; //重新定義Fac
    for(int i=1; i<=x; i++)
    {
        Fac = Fac * i;
    }
    cout << Fac << endl;
}
