#include <iostream>

using namespace std;

double Square(double number)                                        //計算平方之function
{
    return number*number ;                                          //回傳算出之平方值
}

double Cube(double number)                                          //計算立方function
{
    return number*number*number ;                                   //回傳算出之立方值
}

int Factorial(int number)                                           //計算階層function
{
    int i = 1 ;                                                     //設定一變數，並初始化其值為1
    while(number>=1)                                                //設定迴圈條件，計算階層
    {
        i=i*number ;                                                //將輸入的值計算不斷乘上減一的值，直到1
        number-- ;                                                  //計算直到1
    }
    return i ;                                                      //回傳最後計算出的i階層值
}



int main()
{
    double number ;
    do                                                               //使輸入值直到大於0
    {
        cout << "Please enter a number( >0 ): " ;
        cin  >> number ;
        if (number <= 0)
        {
            cout << "Out of range!" << endl;
        }
    }
    while (number <= 0);

    cout << "Square: " << Square(number) << endl ;                   //輸出字串和return回傳的值
    cout << "Cube: " << Cube(number) << endl ;
    cout << "Factorial: " << Factorial(number) << endl ;

    return 0;
}


