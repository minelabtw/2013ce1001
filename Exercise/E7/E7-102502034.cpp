#include<iostream>
using namespace std;
double Square(double) ; //宣告函式
double Cube (double) ;
double Factorial(double) ;
int main               ()
{
    double num ;//宣告變數num
    cout << "Please enter a number( >0 ): "  ;//輸出Please enter a number( >0 ):
    cin >> num ; //輸入num
    while (num <=0)//當num小於等於0時
    {
        cout << "Out of range!" <<endl;//輸出Out of range!
        cout << "Please enter a number( >0 ): " ;//輸出Please enter a number( >0 ):
        cin >> num;//輸入num
    }
    cout << "Square: " << Square (num) <<endl;//輸出Square: num的平方
    cout << "Cube: " << Cube (num)<<endl; //輸出Cube: num的平方
    cout << "Factorial: " << Factorial (num)<<endl;//輸出Factorial: num的階層

    return 0;
}
double Square(double x)//Square 函式
{
    return x*x ;//回傳值為x*x
}
double Cube (double y) //Cube函式
{
    return y*y*y ;//回傳值為y*y*y
}
double Factorial (double z)//Factorial函式
{
    int i=z-1 ;//宣告變數i=z-1

    while (i!=0)//當i不等於0時
    {
        z=z*i;//z=z*z-1
        i--;//i=i-1

    }
    return z ;//回傳值為z
}
