#include <iostream>

using namespace std;
double number = 0;//數字
int sum = 1;//結果
void Square()//平方函式
{
    cout << "Square: " << number * number << endl;
    return;
}
void Cube()//立方函式
{
    cout << "Cube: " << number * number * number << endl;
    return;
}
void Factorial()//階層函式
{
    for (int x=1; x<=number; x++)
        sum *= x;
    cout << "Factorial: " << sum << endl;
    return;

}

int main()
{
    cout << "Please enter a number( >0 ): ";//輸出字串Please enter a number( >0 ):
    cin >> number;//輸入數字
    while (number<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ): ";
        cin >> number;
    }
    Square();
    Cube();
    Factorial();
    return 0;
}
