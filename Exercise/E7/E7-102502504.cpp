#include <iostream>
#include <cmath>

using namespace std;

double sum=1;
double num;

double square() //計算square
{
    return pow(num,2);
}

double cube()  //計算cube
{
    return pow(num,3);
}

double factorial() //計算factorial
{
    for(int x=1; x<=num; x++)
    {
        sum=sum*x;
    }
    return sum;
}

int main()
{
    cout << "Please enter a number( >0 ): ";
    cin >> num;
    while(num<=0) //利用while迴圈使輸入值大於0，若無則顯示out of range，並要求重新輸入
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ): ";
        cin >> num;

    }

    cout << "Square: " << square() << endl; //顯示square的值
    cout << "Cube: " << cube() << endl; //顯示cube的值
    cout << "Factorial: " << factorial(); //顯示factorial的值


    return 0;
}
