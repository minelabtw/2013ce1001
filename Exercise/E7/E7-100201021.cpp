#include<iostream>

using namespace std;
////
void Square(int n)
{
    cout << "Square: " << n*n << endl;
}
////
void Cube(int n)
{
    cout << "Cube: " << n*n*n << endl;
}
////
void Factorial(int n)
{
    int fact=1;
    for(int i=1;i<=n;i++)
    {
        fact*=i;
    }
    cout << "Factorial " << fact << endl;
}

//////main function ///////////
int main()
{
    int num=0;
    //input
    while(true)
    {
        cout << "Please enter a number( >0 ): " ;
        cin >> num;
        if(num>0)
            break;
        else
            cout << "Out of range!" << endl;
    }
    //output answer;
    Square(num);   //n^2
    Cube(num);      //n^3
    Factorial(num); //n!

    return 0;
}

