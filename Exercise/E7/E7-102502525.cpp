#include<iostream>
#include<cmath>
using namespace std;
double square(double a)
{
    return pow(a,2);
}
double factorial(double y)
{
    int fac=1;
    for(int x=1; x<=y; x++)
    {
        fac=fac*x;
    }
    y=fac;
    return y;
}
int main()
{
    double a;
    do
    {
        cout <<"Please enter a number( >0 ): ";
        cin >>a;
        if(a<=0)
            cout <<"Out of range!"<<endl;
    }
    while (a<=0);
    cout <<"Square: "<<square(a)<<endl;
    cout <<"Cube: "<<square(a)*a<<endl;
    cout <<"Factorial: "<<factorial(a)<<endl;
    return 0;
}

