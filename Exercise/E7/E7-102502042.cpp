#include <iostream>
using namespace std;
//函示原型
int input();
int Square(int );
int Cube(int );
int Factorial(int );
int main()
{
    int number;
    number=input(); //輸入
    cout<<"Square: "<<Square(number)<<endl;
    cout<<"Cube: "<<Cube(number)<<endl;
    cout<<"Factorial: "<<Factorial(number)<<endl;
    return 0;
}
int input() //輸入函式
{
    int temp;
    cout<<"Please enter a number( >0 ): ";
    cin>>temp;
    while(temp<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>temp;
    }
    return temp;
}
int Square(int n)   //平方函式
{
    return n*n;
}
int Cube(int n) //立方函式
{
    return n*n*n;
}
int Factorial(int n)    //階層函式
{
    int temp=1;
    for(int i=2;i<=n;++i)
        temp*=i;
    return temp;
}
