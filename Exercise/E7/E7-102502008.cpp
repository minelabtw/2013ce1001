#include<iostream>
#include<cmath>
using namespace std ;
void Square(int) ; //function prototype
void Cube(int) ; //function prototype
void Factorial(int) ; //function prototype
int main()
{
    int number=0 ;
    do
    {
        cout << "Please enter a number( >0 ): " ;
        cin >> number ;
        if(number<=0)
            cout << "Out of range!\n" ;
    }while(number <=0 ) ; //讀取大於0的數
    Square(number);
    Cube(number) ;
    Factorial(number) ;
    return 0 ;
}

void Square(int number) //平方
{
    cout << "Square: " << pow(number,2) << endl ;
}
void Cube(int number) //立方
{
    cout << "Cube: " << pow(number,3) << endl ;
}
void Factorial(int number) //階層
{
    int sum=1 ;
    for(int i=1;i<=number;i++) //1*2*.....*n
    {
        sum *=i ;
    }
    cout << "Factorial: " <<sum << endl ;
}
