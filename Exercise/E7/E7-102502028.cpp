#include <iostream>
#include <math.h>
using namespace std ;

double Square (double) ;           // function prototype
double Cube (double) ;
double Factorial (double) ;

int main ()
{
    double i = 0 ;
    do
    {
        cout << "Please enter a number( >0 ): " ;
        cin >> i ;
    }
    while (i <= 0 && cout << "Out of range!" << endl) ;

    cout << "Square: " << Square (i) << endl ;      //輸出平方
    cout << "Cube: " << Cube (i) << endl ;          //輸出立方
    cout << "Factorial: " << Factorial (i) ;        //輸出階層

    return 0 ;
}

double Square (double x)               //計算並回傳
{
    return pow(x,2) ;
}

double Cube (double x)
{
    return pow(x,3) ;
}

double Factorial (double x)
{
    int yy = 1 ;
    int y = 1 ;
    for ( ; y <= x ; y++)
    {
        yy = yy*y ;
    }
    return yy ;
}
