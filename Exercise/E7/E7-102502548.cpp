#include<iostream>

using namespace std ;

int Square (int) ;

int Cube (int) ;

int Factorial (int) ;

int Square (int a)
{
    return a*a ;
}

int Cube (int a)
{
    return a*a*a ;
}

int Factorial(int a)
{
    int b=0 ;

    for (int x=a-1;x>=1;x--)
    {
        b=a*x ;

        a=b ;
    }

    return a ;
}

int main ()
{
    int a=0 ,b=0 ,c=a ;

    while (true)
    {
        cout << "Please enter a number( >0 ): " ;

        cin >> a ;

        if (a<=0)
        {
            cout << "Out of range!\n" ;
        }

        else break ;
    }

    cout << "Square: " << Square (a) << endl ;

    cout << "Cube: " << Cube (a) << endl ;

    cout << "Factorial: " << Factorial (a) ;

    return 0 ;
}
