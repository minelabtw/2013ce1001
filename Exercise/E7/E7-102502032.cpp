#include <iostream>
#include <cmath>
using namespace std;

//prototype
int Square ( int );
int Cube ( int );
int Factorial ( int );

int main()
{
    //varible decalaration and initialization
    int input =0;

    do
    {
        //ask for a number
        cout << "Please enter a number( >0 ): ";
        cin >> input;

        //judge leagle or not and output
        if ( input <= 0 )
            cout << "Out of range!" << endl;
        else
        {
            cout << "Square: " << Square( input ) << endl;
            cout << "Cube: " << Cube( input ) << endl;
            cout << "Factorial: " << Factorial( input ) << endl;
        }
    }
    while ( input <= 0 );

    return 0;
}

//function: num * num
int Square( int num )
{
    return pow( num, 2 );
}
//function: num * num * num
int Cube( int num )
{
    return pow( num, 3 );
}
//function: 1 * 2 * .... * num
int Factorial( int num )
{
    //local varible decalaration
    int temp = 1;

    for ( int i = 1; i<=num; i ++ )
        temp *= i;

    return temp;
}
