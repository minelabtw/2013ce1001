#include<iostream>

using namespace std;

int Square(int);//function prototype
int Cube(int);
int Factorial(int);

int main()
{
    int x;

    cout << "Please enter a number( >0 ): ";
    cin  >> x;
    while(x<=0)//輸入需大於零的迴圈
    {
        cout << "Out of range!";
        cout << "Please enter a number( >0 ): ";
        cin  >> x;
    }
    cout << "Square: ";
    cout << Square(x) << "\n";

    cout << "Cube: ";
    cout << Cube(x) << "\n";

    cout << "Factorial: ";
    cout << Factorial(x) << "\n";

    return 0;
}

int Square(int a)
{
    return a*a;//傳回平方值
}

int Cube(int b)
{
    return b*b*b;//傳回立方值
}

int Factorial(int c)
{
    int total=0;
    total=c;//存入的值
    while(c>1)//跑迴圈一個一個乘
    {
        total=total*(c-1);
        c--;
    }
    return total;//傳回階層值
}
