#include<iostream>
using namespace std;

void Square(int number);
void Cube(int number);
void Factorial(int number);

int main()
{
    int number=0;//宣告一個輸入值

    cout<<"Please enter a number( >0 ): ";//要求輸入一個值
    cin>>number;
    while(number <=0)//值須大於0.否則重新輸入
    {
        cout<<"Out of range!"<<"\nPlease enter a number( >0 ): ";
        cin>>number;
    }
    Square(number);//導入平方函數
    Cube(number);//導入立方函數
    Factorial(number);//導入階層函數

    return 0;
}
void Square(int number)
{
    int x=number *number;
     cout<<"Square: "<<x<<endl;
}
void Cube(int number)
{
    int y =number*number*number;
    cout<<"Cube: "<<y<<endl;
}
void Factorial(int number)
{
    int z=1;
    int a=1;
    while(a<=number)
    {
        z=z*a;
        a++;
    }
    cout<<"Factorial: "<<z<<endl;


}
