#include <iostream>
using namespace std;

int square ( int input1 ) ; //function prototype
int cube ( int input2 ) ;
int factorial ( int input3 ) ;

int main ()
{
    int num1 = 0; //宣告型別為 整數(int) 的第一個變數(num1) ，並初始化其數值為0。
    cout << "Please enter a number( >0 ): " ; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cin >> num1;
    while ( num1 <= 0 ) //用while迴圈檢測使用者所輸入的數字是否合乎標準，如不符，則要求其重新輸入。
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ): " ;
        cin >> num1;
    }
    cout << "Square: " << square( num1 ) << endl
         << "Cube: " << cube( num1 ) << endl
         << "Factorial: " << factorial( num1 ) ;
    return 0;
}

int square ( int input1 ) //平方運算
{
    return input1 * input1 ;
}
int cube ( int input2 ) //立方運算
{
    return input2 * input2 * input2 ;
}
int factorial ( int input3 ) //階層運算
{
    int i = 1;
    for ( int k = 1 ; k <= input3 ; k++ )
    {
        i = i * k ;
    }
    return i;
}
