#include<iostream>
using namespace std;

int Square(int x)//宣告平方程式
{
    cout << "Square: ";//輸出"Square: "x^2
    return x*x;//回傳數字x^2
}

int Cube(int y)//宣告立方程式
{
    cout << "Cube: ";//輸出"Cube: "y^3
    return y*y*y;//回傳數字y^3
}
int Factorial(int z)//宣告階層程式
{
    int fac=1;//宣告變數fzc來進行階層運算
    cout << "Factorial: ";//輸出"Factorial: "和運算過後的fac=z!
    for(int i=2;i<=z;i++)//fac=fac*2*3*....*z=z!
        fac=fac*i;
    return fac;//回傳數字z!
}

int main()
{
    int number=0;//宣告變數number來接收輸入的數字
    while(number<=0)//當為第一次輸入或輸入數字不符條件時執行程式
    {
        cout << "Please enter a number( >0 ): ";//輸出字串"Please enter a number( >0 ): "
        cin >> number;//將數字宣告給number
        if(number<=0)//若輸入的數字<=0則輸出字串"Out of range!"並要求重新輸入
            cout << "Out of range!\n";
    }
    cout << Square(number) << "\n";//輸出"Square: "和x^2
    cout << Cube(number) << "\n";//輸出"Cube: "和y^3
    cout << Factorial(number);//輸出"Factorial: "和z!

    return 0;
}
