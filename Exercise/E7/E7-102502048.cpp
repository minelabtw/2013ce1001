#include<iostream>
using namespace std;
int Square(int number);//prototype
int Cube(int number);//prototype
int Factorial(int number);//prototype
int main()
{
    int number=0;
    cout<<"Please enter a number( >0 ): ";//顯示字串
    cin>>number;//輸入
    while(number<=0)//判斷有無大於0
    {
        cout<<"Out of range!\n"<<"Please enter a number( >0 ): ";//顯示字串
        cin>>number;//輸入
    }
    cout<<"Square: "<<Square(number)<<endl;//顯示字串
    cout<<"Cube: "<<Cube(number)<<endl;//顯示字串
    cout<<"Factorial: "<<Factorial(number)<<endl;//顯示字串
    return 0;
}
int Square(int number)//Square函數
{
    return number*number;//回傳值
}
int Cube(int number)//Cube函數
{
    return number*number*number;//回傳值
}
int Factorial(int number)//Factorial函數
{
    int f=1;//宣告
    for(int F=1;F<=number;F++)//計算階層
    {
        f=f*F;//計算
    }
    return f;//回傳值
}
