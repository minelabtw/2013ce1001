#include<iostream>
using namespace std;

int Square (int x);                            //function的prototype
int Cube (int x);
int Factorial (int x);

int main()
{
    int x;

    cout << "Please enter a number( >0 ): " ;  //要求使用者輸入數值。
    cin >> x;

    while (x<=0)                               //若數值不符條件，則要求使用者重新輸入。
    {
        cout << "Out of range!" << endl << "Please enter a number( >0 ): ";
        cin >> x;
    }

    cout << "Square: " << Square(x) << endl;  //輸出結果。
    cout << "Cube: " << Cube(x)<< endl;
    cout << "Factorial: " << Factorial(x)<<endl;

    return 0;
}

int Square(int x)
{
    int s;
    s=x*x;
    return s;
}

int Cube(int x)
{
    int c;
    c=x*x*x;
    return c;
}

int Factorial(int x)
{
    int i,f=1;
    for (i=1; i<=x; i++)
    {
        f=f*i;
    }
    return f;
}
