#include <iostream>
using namespace std;
int Square(int);
int Cube(int);
long long int Factorial(int); //宣告3個函式Square,Cube,Factorial

int main()
{
    int a=0;
    do
    {
        cout << "Please enter a number( >0 ): ";
        cin >> a;
        if(a<=0)
            cout << "Out of range!\n";
    }
    while(a<=0); //輸入a值，a不大於0時重複輸入
    cout << "Square: " << Square(a) << "\nCube: " << Cube(a) << "\nFactorial: " << Factorial(a);
    return 0;
}

int Square(int x)
{
    return x*x; //回傳x*x
}

int Cube(int x)
{
    return x*x*x; //回傳x*x*x
}

long long int Factorial(int x)
{
    long long int b=1;
    for(int i=1; i<=x; i++) //計算1*2*3*...*x
    {
        b=b*i;
    }
    return b; //回傳b值
}
