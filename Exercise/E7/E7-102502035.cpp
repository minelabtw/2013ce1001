#include <iostream>
using namespace std;
int Square (int);//Square prototype
int Cube (int);//Cube prototype
int Factorial (int);//Factorial prototype
    int main ()//主函式
{
    int int1 =0;//宣告變數並=0
    cout << "Please enter a number( >0 ): ";//提示輸入
    cin >> int1;//輸入
    while (int1 <=0)
    {
        cout << "Out of range!" << endl << "Please enter a number( >0 ): ";//提示錯誤再提示輸入
        cin >> int1;//輸入
    }
    cout << "Square: " << Square (int1) << endl;//輸出結果
    cout << "Cube: " << Cube (int1) << endl;//輸出結果
    cout << "Factorial: " << Factorial (int1);//輸出結果
    return 0;
}
int Square (int a)//Square函式
{
    return a *a;//回傳平方
}
int Cube (int a)//Cube函式
{
    return a *a *a;//回傳立方
}
int Factorial (int a)//Factorial函式
{
    int b=1;//宣告變數並=1
    while (a>1)//當>1繼續進行階乘
    {
        b =a *b;//成下一個
        a--;
    }
    return b;//回傳結果
}
