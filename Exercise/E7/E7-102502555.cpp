#include <iostream>

using namespace std;

int Square(int input);  //宣告平方的函式
int Cube(int input);  //宣告立方的函式
int Factorial(int input);  //宣告階層的函式

int main(){
    int input;  //宣告變數輸出

    do{  //輸入數字並判斷有無超出範圍
        cout << "Please enter a number( >0 ): ";
        cin >> input;
        if(input <= 0){
            cout << "Out of range!" << endl;
        }
    }while(input <= 0);

    cout << "Square: " << Square(input) << endl;  //輸出平方
    cout << "Cube: " << Cube(input) << endl;  //輸出立方
    cout << "Factorial: " << Factorial(input) << endl;  //輸出階層

    return 0;
}

int Square(int input){  //函式平方
    int square = 0;
    square = input * input;
    return square;
}

int Cube(int input){  //函式立方
    int cube = 0;
    cube = input * input * input;
    return cube;
}

int Factorial(int input){  //函式階層
    int factorial = 1;
    for(int i = 1 ; i <= input ; i++){
        factorial *= i;
    }
    return factorial;
}
