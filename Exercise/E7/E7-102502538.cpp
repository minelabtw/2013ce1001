#include<iostream>
using namespace std;

int Square(int);//設定所需要的函式
int Cube(int);
int Factorial(int);

int main()
{
    int input;
    int Square1,Cube1,Factorial1;

    cout <<"Please enter a number( >0 ): ";
    cin >>input;
    while(input<=0)//判定input的範圍
    {
        cout <<"Out of range!"<<endl;
        cout <<"Please enter a number( >0 ): ";
        cin >>input;
    }

    Square1=Square(input);
    Cube1=Cube(input);
    Factorial1=Factorial(input);

    cout <<"Square: "<<Square1<<endl;//輸出函式的計算結果
    cout <<"Cube: "<<Cube1<<endl;
    cout <<"Factorial: "<<Factorial1;

    return 0;
}
int Square(int a)//設定函式"square"
{
    return a*a;
}
int Cube(int a)//設定函式"cude"
{
    return a*a*a;
}
int Factorial(int a)//設定函式"Factorial"
{
    int b=1 ,c=1;
    for (b=1;b<=a;b++)
    {
        c*=b;
    }
       return c ;
}
