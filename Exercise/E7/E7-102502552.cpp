#include <iostream>
using namespace std;

int square(int);//呼叫平方函式
int cube(int);//呼叫立方函式
int factorial(int);//呼叫階層函式

int main()
{
    int input;//宣告輸入變數

    do{
        cout << "Please enter a number( >0 ):";
        cin >> input;
        if(input <= 0)
            cout << "Out of range!" << endl;
    }while(input <= 0);//檢查變數是否符合要求

    cout << "Square:" << square(input) << endl;//輸出平方
    cout << "Cube:" << cube(input) << endl;//輸入立方
    cout << "Factorial:" << factorial(input);//輸入階層

    return 0;//回傳
}
int square(int input)
{
    return input * input;
}//自定義平方函式
int cube(int input)
{
    return input * input * input;
}//自定義立方函式
int factorial(int input)
{
    int value = 1;

    for(int i = input;i >= 1;i--)
        value *= i;

    return value;
}//自定義階層函式
