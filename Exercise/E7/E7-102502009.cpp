#include<iostream>
using namespace std;

int Factorial0=1;
int number=0;

void Square()
{
    cout<<"Square: "<<number*number<<endl;
}
void Cube()
{
    cout<<"Cube: "<<number*number*number<<endl;
}
void Factorial()
{
    for(int i=1; i<=number; i++)
    {
        Factorial0=Factorial0*i;
    }
    cout<<"Factorial: "<<Factorial0<<endl;
}

int main()
{
    cout<<"Please enter a number( >0 ): ";
    cin>>number;
    while(number<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>number;
    }

    Square();
    Cube();
    Factorial();

    return 0;
}
