#include <iostream>
using namespace std ;
int Square(int);//宣告函式
int Cube(int);//同上
int Factorial(int);//同上

int Square(int x)//計算平方
{
    return x*x ;
}
int Cube(int x)//計算立方
{
    return x*x*x ;
}
int Factorial(int x)//計算階層
{
    int b=x ;
    while(b>=2)
    {
        x=x*(b-1) ;
        b-- ;
    }
    return x ;
}
int main()
{
    int a=0 ;//宣告變數

    cout << "Please enter a number( >0 ): " ;//輸出Please enter a number( >0 ):
    cin >> a ;//輸入a
    while (a<=0)//當a小於等於0進入迴圈
    {
        cout << "Out of range!" << endl << "Please enter a number( >0 ): " ;
        cin >> a ;
    }
    cout << "Square: " << Square(a) << endl ;//輸出Square:
    cout << "Cube: " << Cube(a) << endl ;//輸出Cube:
    cout << "Factorial: " << Factorial(a) ;//輸出Factorial:
    return 0 ;
}


