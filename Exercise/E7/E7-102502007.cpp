#include<iostream>
#include<cmath>
using namespace std;
double Square(double number)
{
    return pow(number,2);
}//set up a function to compute square
double Cube(double number)
{
    return pow(number,3);
}//set up a functino to compute cube
double Factorial(double number)
{
    int a=1;
    for(int i=1;i<=number;i++)
        a*=i;
    return a;
}//set up a function to compute Factorial
int main()
{
    int number;
    do
    {
    cout<<"Please enter a number( >0 ): ";
    cin>>number;
    if(number<=0)
        cout<<"Out of range!"<<endl;
    }//loop until the correct number is input.
    while(number<=0);

    cout<<"Square: "<<Square(number)<<endl;
    cout<<"Cube: "<<Cube(number)<<endl;
    cout<<"Factorial: "<<Factorial(number);//call the function
    return 0;
}
