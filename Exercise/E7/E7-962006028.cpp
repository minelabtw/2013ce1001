#include <iostream>
using namespace std;

int Square(int x)
{
    return x*x;
}
int Cube(int x)
{
    return x*x*x;
}
int Factorial(int x)
{
    int temp=1;
    for (x;x>=1;x--)
    {
        temp*=x;
    }
    return temp ;

}
int input=0;

int main()
{
    cout << "Please enter a number( >0 ): ";
    cin >> input;
    while (input<=0 )
    {
        cout << "Out of range!" <<endl ;
        cout << "Please enter a number( >0 ): ";
        cin >> input;
    }
    cout << "Square: " << Square(input) << endl;
    cout << "Cube: " << Cube(input) << endl;
    cout << "Factorial: " << Factorial(input) << endl;
    return 0;
}



