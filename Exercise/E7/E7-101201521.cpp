#include <iostream>
using namespace std;
//prototype
int Square(int);
int Cube(int);
int Factorial(int);

int main()
{
    int input=0;
    //input
    do
    {
        cout << "Please enter a number( >0 ): ";
        cin >> input;
    }
    while(input<=0 && cout << "Out of range!\n");
    //output
    cout << "Square: " << Square(input) << endl;
    cout << "Cube: " << Cube(input) << endl;
    cout << "Factorial: " << Factorial(input) << endl;

    return 0;
}
//functions
int Square(int x)
{
    return x*x;
}

int Cube(int x)
{
    return x*x*x;
}

int Factorial(int x)
{
    return (x ? x*Factorial(x-1) : 1);
}
