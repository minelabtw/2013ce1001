#include<iostream>

int Square(int);   //函數原型
int Cube(int);
int Factorial(int);

int main()
{
    int num;   //宣告

    do   //輸入
    {
        std::cout<<"Please enter a number( >0 ): ";
        std::cin>>num;
        if(num<=0)
            std::cout<<"Out of range!\n";
    }while(num<=0);

    std::cout<<"Square: "<<Square(num)<<"\nCube: "<<Cube(num)<<"\nFactorial: "<<Factorial(num);    //輸出

    return 0;
}

int Square(int x){return x*x;}   //平方
int Cube(int x){return x*x*x;}   //立方
int Factorial(int x){return x==1?1:x*Factorial(x-1);}    //階乘
