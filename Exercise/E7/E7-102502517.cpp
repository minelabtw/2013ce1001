#include <iostream>

using namespace std;

int square (int);
int cube (int);
int factorial (int);

int main()
{
    int number = 0; //宣告變數

    do //輸入變數
    {
        cout << "Please enter a number( >0 ): ";
        cin >> number;
        if (number<=0)
            cout << "Out of range!" << endl;
    }
    while (number<=0);

    cout << "Square: " << square(number) << endl; //輸出
    cout << "Cube: " << cube(number) << endl; //輸出
    cout << "Factorial: " << factorial(number); //輸出

    return 0;
}

int square (int x) //計算平方
{
    x=x*x;
    return x;
}

int cube (int x) //計算立方
{
    x=x*x*x;
    return x;
}

int factorial (int x) //計算階層
{
    int y_min = 1;
    int y = x;

    while (y_min<x)
    {
        y=y_min*y;
        y_min++;
    }
    return y;
}
