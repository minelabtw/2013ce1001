#include <iostream>

using namespace std;

void square(int a) // 平方
{
    cout << "Square: " << a*a << endl;
}
void cube(int b) // 立方
{
    cout << "Cube: " << b*b*b << endl;
}
void factorial(int c) // 階層
{
    int pro =1;
    for(int i=1; i<=c; i++)
    {
        pro = pro * i;
    }
    cout << "Factorial: " << pro << endl;
}

int main()
{
    int num =0; // 數字

    while(num<1) // 判斷輸入
    {
        cout << "Please enter a number( >0 ): ";
        cin >> num;
        if(num<1)cout << "Out of range!\n";
    }

    square(num);
    cube(num);
    factorial(num);

    return 0;
}
