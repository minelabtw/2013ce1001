#include<iostream>
using namespace std;

int square(int);//function prototype
int cube(int);
int factorial(int);

int main()
{
    int number=0;//�ŧi�ܼ�

    cout<<"Please enter a number( >0 ): ";
    cin>>number;
    while(number<=0)
    {
        cout<<"Out of range!"<<endl<<"Please enter a number( >0 ): ";
        cin>>number;
    }//end main

    cout<<"Square: "<<square(number)<<endl<<"Cube: "<<cube(number)<<endl<<"Factorial: "<<factorial(number);//function call

    return 0;
}

//function definition
int square(int x)
{
    return x*x;
}
int cube(int x)
{
    return x*x*x;
}
int factorial(int x)
{
    int t=1;
    for(int i=1; i<=x; i++)
    {
        t=t*i;
    }
    return t;

}

