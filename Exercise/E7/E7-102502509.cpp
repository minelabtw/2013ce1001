#include <iostream>
using namespace std;
// Definition of the functions.
int square( int );
int cube( int );
int factorial( int );

int main()
{
    int number = 0;
    do
    {
        cout << "Please enter a number( >0 ): ";
        cin >> number;
        if(number <= 0)
           cout << "Out of range!\n";
    }
    while(number <= 0); // To avoid the number which we cin out of range.

    cout << "Square: " << square ( number ) << " ";
    cout << endl;

    cout << "Cube: " << cube ( number ) << " "; // call out the function.
    cout << endl;

    cout << "Factorial: " << factorial ( number ) << " ";
    cout << endl;
}
// functions' contents.
int square ( int a )
{
    return a * a;
}
int cube( int b )
{
    return b * b * b;
}
int factorial( int c )
{
    int a = 1;
    while( c )
        {
            a *= c;
            --c;
        }
    return a;
}
