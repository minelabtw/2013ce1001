#include<iostream>
using namespace std;
int Square( int );
int Cube ( int );
int Factorial ( int );

int main()
{
    int input;
    while(1)                                        //使程式順利進入迴圈
    {
        cout<<"Please enter a number( >0 ): ";
        cin>>input;
        if(input>0)                                 //如果input在我要的範圍之內,即跳出此迴圈,倘若不再我要的範圍之內,那就讓迴圈繼續跑下去
            break;
        cout<<"Out of range!"<<endl;
    }
    cout<<"Square: "<<Square(input)<<endl;
    cout<<"Cube: "<<Cube(input)<<endl;
    cout<<"Factorial: "<<Factorial(input)<<endl;
    return 0;
}

int Square( int x )
{
    return x*x;
}

int Cube( int x )
{
    return x*x*x;
}

int Factorial( int x )
{
    int factorial;
    for(factorial=1; x>=1; --x )
    {
        factorial*=x;                                   //factorial=factorial*x
    }
    return factorial;
}
