#include <iostream>

using namespace std;

int Square( int );//宣告3個函數
int Cube( int );
int Factorial( int );

int main()
{
    int x=0;
    cout<<"Please enter a number( >0 ): ";
    cin>>x;
    for(; x<=0;)//輸入大於0的整數
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>x;
    }
    cout<<"Square: "<<Square(x)<<endl;
    cout<<"Cube: "<<Cube(x)<<endl;
    cout<<"Factorial: "<<Factorial(x);
    return 0;
}
int Square( int y )//定義3個函數
{
    return y*y;
}
int Cube( int y )
{
    return y*y*y;
}
int Factorial( int y )
{
    int k=1;
    for(int a=1; a<=y; a++)
        k=k*a;
    return k;
}
