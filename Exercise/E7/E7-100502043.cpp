#include<iostream>

using namespace std;

int square (int num)  //自訂平方函式
{
    return num * num;  //回傳平方值
}

int cube (int num)  //自訂立方函式
{
    return num * num * num;  //回傳立方值
}

int factorial (int num)  //自訂階層函式
{
    if (num<=1)  //若變數等於1，則回傳1
        return 1;
    else
        return num * factorial(num-1);  //若不等於1，則使用遞迴算出階層並回傳
}

int main()
{
    int num=0;

    while (num<1)
    {
        cout << "Please enter a number( >0 ): ";
        cin >> num;
        if (num<1)
            cout << "Out of range!";
    }

    cout << "Square: " << square(num) << endl;
    cout << "Cube: " << cube(num) << endl;
    cout << "Factorial: " << factorial(num) << endl;  //輸出三個自訂函式值

    return 0;
}
