#include <iostream>
using namespace std;

int Square(int s)
{
    int sq=0;
    sq = s*s;
    return sq;
}

int Cube(int c)
{
    int cu=0;
    cu = c*c*c;
    return cu;
}

int Factorial(int f)
{
    int fa=1;
    int f1=1;
    int f2=1;
    while (f1<=f)
    {
        fa=f1*f2;
        f2=fa;
        f1++;
    }
    return fa;
}

int main()
{
    int n=0;

    cout << "Please enter a number( >0 ): ";
    cin >> n;
    while (n<=0)
    {
        cout << "Out of range!\nPlease enter a number( >0 ): ";
        cin >> n;
    }
    cout << "Square: " << Square(n) << endl;
    cout << "Cube: " << Cube(n) << endl;
    cout << "Factorial: " << Factorial(n);

    return 0;
}
