#include<iostream>
#include<cmath>
using namespace std;
double Square(int);//宣告函式Square 參數為整數
double Cube(int);//宣告函式Cube 參數為整數
double Factorial(int);//宣告函式Factorial 參數為整數
int main()
{
    int number = 0;//宣告變數number
    do
    {
        cout << "Please enter a number( >0 ): ";//輸出Please enter a number( >0 ):
        cin >> number;//輸入number
        if(number<=0)//當number小於等於0
            cout << "Out of range!" << endl;//輸出Out of range! 換行
    }
    while(number<=0);//當number小於等於0 迴圈
    cout << "Square: " << Square(number) << endl;//輸出Square:  Square(number) 換行
    cout << "Cube: " << Cube(number) << endl;//輸出Cube: Cube(number) 換行
    cout << "Factorial: " << Factorial(number) << endl;//輸出Factorial: Factorial(number) 換行
    return 0;
}
double Square(int x)//宣告函式Square 參數為整數x
{
    return pow(x,2);//回傳x平方的值
}
double Cube(int x)//宣告函式Cube 參數為整數x
{
    return pow(x,3);//回傳x立方的值
}
double Factorial(int x)//宣告函式Factorial 參數為整數x
{
    int c =1;//宣告整數c等於1
    for(int y=1; y<=x; y++)//宣告y=1 y<=x y遞增
    {
        c*=y;//c*y的值傳給c
    }
    return c;//回傳c的值
}

