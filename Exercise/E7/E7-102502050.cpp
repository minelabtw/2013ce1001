#include<iostream>
using namespace std;
void Square(int);
void Cube(int);
void Factorial(int);                                      //prototype

int main()
{
    int number;
    do                                                    //input
    {
        cout << "Please enter a number( >0 ): ";
        cin >> number;
    }
    while(number<=0 && cout << "Out of range!\n");
    Square(number);                                      //call function
    Cube(number);
    Factorial(number);
    return 0;
}
void Square(int number)
{
    cout << "Square: " <<number*number << endl;          //compute square and output
}
void Cube(int number)
{
    cout << "Cube: " <<number*number*number << endl;     //compute cube   and output
}
void Factorial(int number)
{
    int total=1;
    for(int i=1;i<=number;i++)                           //compute factorial and output
    total*=i;
    cout << "Factorial: " << total << endl;

}
