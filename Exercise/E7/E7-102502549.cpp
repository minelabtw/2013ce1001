#include <iostream>
using namespace std;

int Square(int);//宣告Square原型
int Cube(int);//宣告Cube原型
int Factorial(int);//宣告Factorial原型

int main()
{
    int n;

//就檢查阿
    do
    {
        cout<<"Please enter a number( >0 ): ";
        cin>>n;

        if(n<=0)
            cout<<"Out of range!"<<endl;
    }
    while(n<=0);

//印結果囉
    cout<<"Square: "<<Square(n)<<endl;
    cout<<"Cube: "<<Cube(n)<<endl;
    cout<<"Factorial: "<<Factorial(n);

    return 0;
}

//以下實作函數
int Square(int x)
{
    return x*x;
}

int Cube(int x)
{
    return x*x*x;
}

int Factorial(int x)
{
    int temp=1;

    for(int i=1; i<=x; i++)
        temp*=i;

    return temp;
}
