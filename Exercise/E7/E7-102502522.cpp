#include<iostream>
#include <math.h>
using namespace std;

double  number;

double Square(double a)//定義square此方程式
    {
        return a*a;//回報此值
    }
double Cube(double a)//定義Cube此方程式
    {
        return a*a*a;//回報此值
    }
int Factorial(int a)//定義Factorial此方程式
    {
        int c;
        int b=1;
        for(c=1;c<=a;c++)
        {
         b=b*c;
        }
        return b;//回報此值
    }
int main()
{
    cout<<"Please enter a number( >0 ): ";
    cin>>number;

    while(number<=0)//當輸入值不符合規定時
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>number;
    }

    cout<<"Square: "<<Square(number)<<endl<<"Cube: "<<Cube(number)<<endl<<"Factorial: "<<Factorial(number);

    return 0;
}
