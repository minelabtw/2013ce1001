#include<iostream>
using namespace std;
int Square (int);
int Cube (int);
int Factorial(int);
int main()
{
    int input=0;
    cout << "Please enter a number( >0 ): " ;
    cin >> input ; //輸入整數input
    while( input <= 0 ) //當input小於等於0時，進入while迴圈
    {
        cout << "Out of range!" << endl
             << "Please enter a number( >0 ): " ;
        cin >> input ; //再次輸入input
    }
    cout << "Square: " << Square (input) << endl; //計算並印出Square
    cout << "Cube: " << Cube (input) << endl; //計算並印出Cube
    cout << "Factorial: " << Factorial (input) << endl; //計算並印出Factorial

    return 0;
}
int Square (int a) //Square 函數
{
    return a*a;
}
int Cube (int a) //Cube 函數
{
    return a*a*a;
}
int Factorial (int a) //Factorial 函數
{
    int x=1;
    for (int i=1 ; i<=a ; i++)
        x *= i;
    return x;
}
