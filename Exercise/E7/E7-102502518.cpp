#include <iostream>

using namespace std;
int calacSquare(int);
int calacCube(int);
int calacFactorial(int);
int main()
{
    int number=0;
    do
    {

        cout<<"Please enter a number( >0 ): ";
        cin>>number;
        if(number<=0)
            cout<<"Out of range!"<<endl;
    }
    while(number<=0);

    int Square=calacSquare(number);
    cout<<"Square: "<<Square<<endl;

    int Cube=calacCube(number);
    cout<<"Cube: "<<Cube<<endl;

    int Factorial=calacFactorial(number);
    cout<<"Factorial: "<<Factorial<<endl;

    return 0;
}

int calacSquare(int number)
{
    return number*number;
}

int calacCube(int number)
{
    return number*number*number;
}

int calacFactorial(int number)
{
    int Factorial=1;
    for(int calac=number; calac>0; calac--)
    {
        Factorial=calac*Factorial;
    }
    return Factorial;
}

