#include <iostream>
#include <cmath>
using namespace std;
int square ( int );//宣告函數
int cube ( int );
int factorial ( int );
int main()
{
    int x;
    cout<<"Please enter a number( >0 ): ";//輸出Please enter a number( >0 ):
    cin>>x;//輸入x的值
    while (x<=0)//當x<=0
    {
        cout<<"Out of range!"<<endl<<"Please enter a number( >0 ): ";//輸出Out of range! 否則輸出Please enter a number( >0 ):
        cin>>x;//輸入x的值
    }
    cout<<"Square: "<<square( x )<<endl;//輸出Square:
    cout<<"Cube: "<<cube( x )<<endl;//輸出Cube:
    cout<<"Factorial: "<<factorial( x );//輸出Factorial:
}
int square ( int x )//函數運算
{
    return x*x;
}
int cube ( int x )
{
    return x*x*x;
}
int factorial( int x )
{
    int temp=1;
    int k=1;
    for(k=1;k<=x;k++){
        temp*=k;
    }
    return temp;
}
