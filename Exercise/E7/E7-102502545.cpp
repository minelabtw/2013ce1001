#include <iostream>
using namespace std;

int Square(int x);//prototype
int Cube(int x);//prototype
int Factorial(int x);//prototype

int main()
{
    int x;
    cout<<"Please enter a number( >0 ): ";
    cin>>x;
    while(x<=0)//設置一個迴圈來限定x的範圍
    {
        cout<<"Out of range!\n";
        cout<<"Please enter a number( >0 ): ";
        cin>>x;
    }
    cout<<"Square: "<<Square(x)<<endl;
    cout<<"Cube: "<<Cube(x)<<endl;
    cout<<"Factorial: "<<Factorial(x)<<endl;

    return 0;
}

int Square(int x)//Square函式
{
    return x*x;//把x的值回傳
}

int Cube(int x)//Cube函式
{
    return x*x*x;//把x的值回傳
}

int Factorial(int x)//Factorial函式
{
    int y=1,z=1;
    while(y<=x)//計算出階層
    {
        z=z*y;
        y++;
    }

    return z;
}








