#include <iostream>
#include <stdlib.h>
using namespace std;

void square(int);
void cube(int);
void factorial(int);

int main(){
    int num;//宣告整數,num為輸入的數字 
    do{
        cout << "Please enter a number( >0 ): ";
        cin >> num;
    }while(num<=0 && cout << "Out of range!\n");//如果超出範圍要求重新輸入 
    square(num);//呼叫函式計算平方 
    cube(num);//呼叫函式計算立方 
    factorial(num);//呼叫函式計算階乘 
    return 0;
}

void square(int n){//計算平方,並輸出 
    cout << "Square: " << n*n <<endl;
}

void cube(int n){//計算立方 ,並輸出 
    cout << "Cube: " << n*n*n <<endl;
}

void factorial(int n){//計算階乘,並輸出 
    int i,ans=1;
    for(i=1;i<=n;i++){
        ans *= i;
    }
    cout << "Factorial: " << ans <<endl;
}
