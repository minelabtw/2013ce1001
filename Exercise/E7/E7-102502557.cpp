//階乘除了用遞迴，還有別的寫法嗎？

#include <iostream>
using namespace std;

int Square(int);//平方用回傳值方式寫
void Cube(int); //立方用不能回傳值的void寫
int Factorial(int);

int main()
{
    int a;
    cout<<"Please enter a number( >0 ): ";
    cin>>a;
    if (a<=0)
    {
        cout<<"Out of range!";
        cin>>a;
    }
    cout<<Square(a);//若是用回傳值 則要用cout的方式，就像是cout<<a一樣
    cout<<endl;//那就要在這裡換行，因為他只會回傳值，並不會回傳換行
    Cube(a);//立方則是完全在自定義函數中把所有工作完成
    cout<<"Factorial: ";
    cout<<Factorial(a);
    
    return 0;
}

int Square(int a)
{
    cout<<"Squre=: ";
    return a*a;
}

void Cube(int b)
{
    cout<<"Cube: ";
    cout<< b*b*b;
    cout<<endl;
}

int Factorial(int c)
{
    if (c>0)
    {
        return (c*Factorial(c-1));//階乘：用遞迴寫 6-34洪維恩博士
    }
    else
        return 1;
}
