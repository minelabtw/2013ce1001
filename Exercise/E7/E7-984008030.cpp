#include <iostream>
using namespace std;
bool IsNotPositive(int number);//檢查number是否為正
int Squre(int number);//以int型態的number為輸入，回傳其平方
int Cube(int number);//以int型態的number為輸入，回傳其次方
int Factorial(int number);//以int型態的number為輸入，回傳其階乘

int main(){
    int inputNumber = 0;//宣告型態為int的模式變數，並初始化為0

    while(1){
        cout << "Please enter a number( >0 ): ";
        cin >> inputNumber;
        if(!IsNotPositive(inputNumber)){//如果大於0就跳出迴圈
            break;
        }
    }
    cout << "Square: " << Squre(inputNumber) << endl \
         << "Cube: " << Cube(inputNumber) << endl \
         << "Factorial: " << Factorial(inputNumber) << endl;
    return 0;
}

bool IsNotPositive(int number){//檢查number是否為正
    if(number <= 0){
        cout << "Out of range!" << endl;
        return true;
    }
    else{
        return false;
    }
}

int Squre(int number){//以int型態的number為輸入，回傳其平方
    return number * number;
}

int Cube(int number){//以int型態的number為輸入，回傳其次方
    return number * number * number;
}

int Factorial(int number){//以int型態的number為輸入，回傳其階乘
    if(number == 1){//當number為1，回傳1
        return 1;
    }
    else{//回傳Factorial(number - 1) * number
        return Factorial(number - 1) * number;
    }
}
