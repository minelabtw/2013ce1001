#include<iostream>
using namespace std;

int Square(int);//宣告Square函數
int Cube(int);//宣告Cube函數
int Factorial(int);//宣告Factorial函數

int main()
{
    int n;
    do//輸入大於0的數字
    {
        cout << "Please enter a number( >0 ): ";
        cin >> n;
        if(n<=0)
            cout << "Out of range!\n";
    }
    while(n<=0);

    cout << "Square: " << Square(n) << endl;//呼叫Square(n)並輸出結果
    cout << "Cube: " << Cube(n) << endl;//呼叫Cube(n)並輸出結果
    cout << "Factorial: " << Factorial(n) << endl;//呼叫Factorial(n)並輸出結果

    return 0;
}
int Square(int n)//計算平方
{
    return n * n;
}
int Cube(int n)//計算立方
{
    return n * n * n;
}
int Factorial(int n)//計算階層
{
    int a=1;
    for(int i=1; i<=n; i++)
    {
        a *= i;
    }
    return a;
}
