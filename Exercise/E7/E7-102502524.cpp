#include <iostream>
using namespace std;

double Square(int x)                                        //設定平方函數
{
    return x*x;
}
double Cube(int x)                                          //設定立方函數
{
    return x*x*x;
}
double Factorial(int x)                                     //設定階層函數
{
    int a = 1;
    for(int i=1; i<=x; i++)
        a=a*i;
    return a;
}
int main()
{
    int x = 0;

    cout << "Please enter a number( >0 ): ";
    while(cin >> x)                                         //輸入數字並判斷是否超出範圍
    {                                                       //若無超出，則輸出結果
        if (x<=0)
        {
            cout << "Out of range!" << endl;
            cout << "Please enter a number( >0 ): ";
        }
        else
        {
            cout << "Square: " << Square(x) << endl;
            cout << "Cube: " << Cube(x) << endl;
            cout << "Factorial: " << Factorial(x) << endl;
            break;
        }
    }
    return 0;
}
