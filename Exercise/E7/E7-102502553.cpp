#include<iostream>
using namespace std;

int Square(int x);//先宣告一個function，名字是Square，參數為整數x
int Cube(int x);
int Factorial(int x);

int main()
{
    int y=0;
    do
    {
        cout<<"Please enter a number( >0 ): ";
        cin>>y;
        if(y<=0)
            cout<<"Out of range!"<<endl;
    }
    while(y<=0);
    cout<<"Square: "<<Square(y)<<endl;//呼叫Square的function
    cout<<"Cube: "<<Cube(y)<<endl;
    cout<<"Factorial: "<<Factorial(y);

    return 0;
}

int Square(int x)//計算平方
{
    return x*x;
}

int Cube(int x)//計算次方
{
    return x*x*x;
}

int Factorial(int x)//計算階層
{
    int a=1;
    for(int b=1; b<=x; b++)
    {
        a=a*b;
    }
    return a;
}
