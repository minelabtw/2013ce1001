#include<iostream>

using namespace std;

int Square(int x);//宣告函式
int Cube(int x);
int Factorial(int x);

int main()
{
    int a=0;//宣告變數

    do
    {
        cout << "Please enter a number( >0 ): ";//輸出字串
        cin  >> a;//輸入值給變數a

        if(a<=0)//a<=0時輸出字串
            cout << "Out of range!";
    }
    while(a<=0);//a<=0時重新進入迴圈

    cout << "Square: " << Square(a) << endl;//輸出字串及函數計算結果並換行
    cout << "Cube: " << Cube(a) << endl;
    cout << "Factorial: " << Factorial(a) << endl;

    return 0;
}

int Square(int x)//平方
{
    int temp=0;

    temp=x*x;

    return temp;
}

int Cube(int x)//立方
{
    int temp=0;

    temp=x*x*x;

    return temp;
}

int Factorial(int x)//階層
{
    int temp=1;
    int i=1;

    for(i=1; i<=x; i++)
    {
        temp*=i;
    }

    return temp;
}

