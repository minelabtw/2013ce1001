#include <iostream>
using namespace std;

int Square(int a) //計算平方
{
    return a*a;
}

int Cube(int a) //計算立方
{
    return a*a*a;
}

int Factorial(int a) //計算階層
{
    int i=1;
    int product=1;

    for(int i=1; i<=a; i++)
    {
        product=product*i;
    }
    return product;
}

int main()
{
    int X=0; //要輸入變數

    cout<<"Please enter a number( >0 ): ";
    cin>>X;
    while(X<=0)                                                             //確認year1不可以<0，若<0則重新輸入第一個年分
    {
        cout<<"Out of range!"<<endl
            <<"Please enter a number( >0 ): ";
        cin>>X;
    }

    cout<<"Square: "<<Square(X)<<endl;
    cout<<"Cube: "<<Cube(X)<<endl;
    cout<<"Factorial: "<< Factorial(X)<<endl;

    return 0;
}
