#include <iostream>

using namespace std;

int Square(int);                                                   //函數原型
int Cube(int);
int Factorial(int);

int main()
{
    int a=0;
    do
    {
        cout<<"Please enter a number( >0 ): ";                     //重複輸入
        cin>>a;
    }
    while(a<=0 && cout<<"Out of range!"<<endl);


    cout<<"Square: "<<Square(a)<<endl;                             //輸出結果
    cout<<"Cube: "<<Cube(a)<<endl;
    cout<<"Factorial: "<<Factorial(a)<<endl;

    return 0;
}
int Square(int x)                                                  //函數內容
{
    return x*x;
}
int Cube(int x)
{
    return x*x*x;
}
int Factorial(int x)
{
    if(x==1)
        return 1;
    else
        return x*Factorial(x-1);
}

