#include <iostream>
using namespace std;

int n;

void square()
{
    cout<<"Square: "<<n*n<<endl;
    return;
}

void cube()
{
    cout<<"Cube: "<<n*n*n<<endl;
    return;
}

void factorial()
{
    int i,ans=1;
    for(i=1; i<=n; i++)
        ans*=i;
    cout<<"Factorial: "<<ans<<endl;
    return;
}

int main()
{
    cout<<"Please enter a number( >0 ): ";
    cin>>n;
    while(n<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>n;
    }
    square();
    cube();
    factorial();
    return 0;
}
