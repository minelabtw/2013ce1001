#include <iostream>
using namespace std;
double square(int); //宣告函式
double cube(int);
double factorial(int);
int main()
{
    int a=0; //宣告變數
    cout <<"Please enter a number( >0 ): ";
    cin >> a;
    while (a<=0) //迴圈
    {
        cout <<"Out of range!"<<endl<<"Please enter a number( >0 ): ";
        cin >> a;
    }
    cout <<"Square: "<<square (a)<<endl; //輸出結果
    cout <<"Cube: "<<cube (a)<<endl;
    cout <<"Factorial: "<<factorial (a)<<endl;
    return 0; //結束
}
double square (int x) //函式
{
    return x*x;
}
double cube (int x) //函式
{
    return x*x*x;
}
double factorial (int x) //函式
{
    for (int b=x-1; b>=1; b--) //階層運算
    {
        x=x*b;
    }
    return x;
}
