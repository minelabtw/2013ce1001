#include <iostream>
#include <cmath>
using namespace std;

//function prototype
double square(double);
double cube(double);
double factorial(double);

int main()
{
    double integer=0; //call variable

    // data validation
    while(integer<=0)
    {
        cout<<"Please enter a number( >0 ): ";
        cin>>integer;
        if(integer<=0)
        {
            cout<<"Out of range!";
        }
    }
    //output result by calling function
    cout<<"Square: "<<square(integer)<<endl;
    cout<<"Cube: "<<cube(integer)<<endl;
    cout<<"Factorial: "<<factorial(integer)<<endl;

    return 0;
}

//function for calculating the square of input number
double square(double x)
{
    return pow(x,2);
}//end function

//function for calculating the cube of the input number
double cube(double x)
{
    return pow(x,3);
}//end function

//function for calculating the factorial of input number
double factorial(double x)
{

    double ans=x;
    do
    {
        x=x-1;
        ans=ans*x;
    }while(x>1);

    return ans;
}//end function

