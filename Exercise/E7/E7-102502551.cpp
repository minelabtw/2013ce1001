#include <iostream>

using namespace std;

int Square(int);
int Cube(int);
int Factorial(int);

int main()
{
    int a;

    cout<<"Please enter a number( >0 ): ";                     //檢查是否在範圍內
    cin>>a;

    while(a<=0)
    {
        cout<<"Out of range!"<<endl<<"Please enter a number( >0 ): ";
        cin>>a;
    }

    cout<<"Square: "<<Square(a)<<endl;
    cout<<"Cube: "<<Cube(a)<<endl;
    cout<<"Factorial: "<<Factorial(a)<<endl;

    return 0;
}
int Square(int x)                                                 //設定函數
{
    return x*x;
}
int Cube(int x)
{
    return x*x*x;
}
int Factorial(int x)
{
    if(x==1)
        return 1;
    else
        return x*Factorial(x-1);
}

