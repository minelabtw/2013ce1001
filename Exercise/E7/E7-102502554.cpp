#include<iostream>
#include <math.h>
using namespace std;

int number;//宣告整數變數number

int Square ()//計算數值之平方
{
    do
    {
        cout << "Please enter a number( >0 ): ";//輸出Please enter a number( >0 ):
        cin >> number;//輸入數值

        if ( number <= 0 )
            cout<<"Out of range!"<<endl;//當條件不符時輸出Out of range!
    }
    while ( number <=0 );//條件不符時重新輸入
    return number * number;//回傳數值的平方
}

int Cube ()//計算數值的立方
{
    return number * number * number;//回傳數值的立方
}

int Factorial ()//計算階層
{
    int factorial;//宣告整數變數factorial

    for ( int i = number - 1; i > 0; i--)
    {
        number *= i;
        factorial = number;
    }//設立迴圈計算階層

    return factorial;//回傳階層值
}


int main ()
{
    int square = Square ();//呼叫Square
    int cube = Cube ();//呼叫Cube
    int factorial = Factorial ();//呼叫Factorial

    cout << "Square: " << square << endl;
    cout << "Cube: " << cube << endl;
    cout << "Factorial: " << factorial << endl;//輸出所計算的值

    return 0;
}
