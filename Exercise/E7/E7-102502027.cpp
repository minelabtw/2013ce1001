#include <iostream>
#include<cmath>

using namespace std;

double square(double number);
double cube(double number);
double factorical(double number);

int main()
{
    int number; //宣告number
    do
    {
        cout <<"Please enter a number( >0 ): ";//輸出
        cin>>number;//輸入
    }
    while(number<=0 && cout<<"Out of range!"<<endl);

    cout<<"Square: "<<square(number)<<endl;
    cout<<"Cube: "<<cube(number)<<endl;
    cout<<"Factorial: "<<factorical(number)<<endl;

    return 0;
}
double square(double number)
{
    return number*number; //回傳給square
}
double cube(double number)
{
    return number*number*number;
}
double factorical(double number)
{
    int k=1;

    for(int i=1;i<=number;i++)
    {
       k=k*i;
    }
    return k;
}
