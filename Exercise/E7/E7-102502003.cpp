#include<iostream>
using namespace std;

double square(double n)  //宣告函式
{
    double s=n*n;  //平方
    return s;  //回傳值s
}

double cube(double n)  //宣告函式
{
    double c=n*n*n;  //立方
    return c;  //回傳值c
}

double factorial(double n)  //宣告函式
{
    double f=1;
    while(n)  //n階乘
    {
        f=f*n;
        n--;
    }
    return f;  //回傳值f
}

int main()
{
    double a=0;
    while(a<=0)  //判定輸入值大於0
    {
        cout<<"Please enter a number( >0 ):";
        cin>>a;
        if(a<=0)
            cout<<"Out of range!"<<endl;
    }
    cout<<"Square: "<<square(a)<<endl;
    cout<<"Cube: "<<cube(a)<<endl;
    cout<<"Factorial: "<<factorial(a)<<endl;
    return 0;

}
