#include<iostream>
using namespace std;

int Square(int);                            //function prototype  先打
int Cube(int);
int Factorial(int);

int main()
{
    int input =0;

    cout<<"Please enter a number( >0 ): ";
    cin>>input;
    while (input <= 0)
    {
        cout<<"Out of range!\nPlease enter a number( >0 ): ";
        cin>>input;
    }
    cout<<"Square: "<<Square(input)<<endl<<"Cube: "<<Cube(input)<<endl<<"Factorial: "<<Factorial(input);

    return 0;
}

int Square(int input)                         //3個function為平方、立方、階層
{
    return input*input;
}
int Cube(int input)
{
    return input*input*input;
}
int Factorial(int input)                  //階層function的input=main裡的input值
{
    int y =1;                               //另外用y運算
    for (int i = 1; i <= input ; i++)
        y = y*i;
    return y;                                     //回傳y值
}



