#include<iostream>
using namespace std;

double square(double a) //宣告1個function
{
    a=a*a;  //計算平方
    return a;
}

double cube(double a)
{
    a=a*a*a; //計算立方
    return a;
}

double factorial(double a)  // 算接乘
{
    double c=1;
    while(a)
    {
        c=c*a;
        --a;
    }
    return c;
}

int main()
{
    double b=0;
    while(b<=0)  //小於0迴圈
    {
        cout << "Please enter a number( >0 ): ";
        cin >> b;
        if(b<=0)
            cout << "Out of range!" << endl;
    }


    cout << "Square: " << square(b) << endl;    //把結果印出
    cout << "Cube: " << cube(b) << endl;
    cout << "Factorial: " << factorial(b) << endl;

    return 0;

}
