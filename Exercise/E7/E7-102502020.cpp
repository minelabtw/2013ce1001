#include <iostream>
#include <math.h>

using namespace std;

int Square(int y)             //宣告函式
{
    return y*y;               //回傳平方的值
}
int Cube(int y)
{
    return y*y*y;             //回傳立方的值
}
int Factorial(int y)
{
    int z=1;
    for(int n=1; n<=y; n++)
    {
        z=z*n;
    }
    return z;                 //回傳階乘的值
}

int main()
{
    int x;
    do                                              //先執行一次以下動作
    {
        cout << "Please enter a number( >0 ): ";
        cin >> x;
        if(x<=0)
            cout << "Out of range!" << endl;
    }
    while(x<=0);
    cout << "Square: " << Square(x) << endl         //輸出字串和函式的值
         << "Cube: " << Cube(x) << endl
         << "Factorial: " << Factorial(x);

    return 0;
}
