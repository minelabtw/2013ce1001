#include <iostream>
using namespace std;
int Square ( int );  //宣告平方函數
int Cube ( int );  //宣告立方函數
int Factorial ( int );  //宣告階層函數
main()
{
    int num;  //輸入用變數

    do{
        cout << "Please enter a number( >0 ): ";  //指示
        cin >> num;
    }while ( num<=0 && cout << "Out of range!\n");  //判斷合理性
    cout << "Square: " << Square ( num ) << endl;  //顯示平方
    cout << "Cube: " << Cube ( num ) << endl;  //顯示立方
    cout << "Factorial: " << Factorial ( num );  //顯示階層

    return 0;
}
int Square ( int x )
{
    return x*x;  //計算平方
}
int Cube ( int y )
{
    return y*y*y;  //計算立方
}
int Factorial ( int z )
{
    int b=z-1;
    while ( b>=1 )  //計算階層
    {
        z=z*b;
        b--;
    }
    return z;
}
