#include <iostream>;
using namespace std;

int Square(int x)//
{
    return x*x;
}
int Cube(int x)
{
    return x*x*x;
}
int Factorial(int x)
{
    return (x ? x*Factorial(x-1) : 1);
}

int main()
{
    int n=0;
    while(n<=0)
    {
        cout << "Please enter a number( >0 ): ";
        cin >> n;
        if (n<=0)
            cout << "Out of range!\n";
    }
    cout << "Square: " << Square(n) << endl;
    cout << "Cube: " << Cube(n) << endl;
    cout << "Factorial: " << Factorial(n) <<endl;

    return 0;
}
