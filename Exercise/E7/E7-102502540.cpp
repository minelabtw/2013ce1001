#include <iostream>
using namespace std;

int Square(int);  //宣告函示Square
int Cube(int);
int Factorial(int);

int main()
{
    int number; //宣告一個整數變數
    cout << "Please enter a number( >0 ): "; //顯示Please enter a number( >0 ):
    cin >> number; //輸入number值
    while ( number <= 0 ) //當number<=0進入以下迴圈
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ): ";
        cin >> number;
    }
    cout << "Square: " << Square (number) << endl;
    cout << "Cube: " << Cube (number) << endl;
    cout << "Factorial: " << Factorial (number) << endl;

    return 0;
}
int Square( int x ) //實作函示Square
{
    return x*x;
}
int Cube( int y )
{
    return y*y*y;
}
int Factorial( int z )
{
    int i=z-1;

    while ( i != 0 )
    {
        z=z*i;  //z*i值指派給z
        i--; //i值遞減1
    }
    return z;
}
