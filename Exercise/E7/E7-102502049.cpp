#include<iostream>
using namespace std;

int Square (int);
int Cube (int);
int Factorial (int); //宣告三個回傳值為整數的函式
int main()
{
    int Square1;
    int Cube1;
    int Factorial1; //宣告三個整數變數儲存平方.立方.階層
    int number;

    do
    {
        cout << "Please enter a number( >0 ): ";
        cin >> number;
    }
    while (number<=0 && cout << "Out of range!\n"); //顯示題目與要求>0

    Square1 = Square (number);
    Cube1 = Cube (number);
    Factorial1 = Factorial (number); //進入函式運算

    cout << "Square: " << Square1 << endl;
    cout << "Cube: " << Cube1 << endl;
    cout << "Factorial: " << Factorial1; //顯示結果

    return 0 ;
}

int Square (int x)
{
    return x*x; //平方函式
}

int Cube (int x)
{
    return x*x*x; //立方函式
}

int Factorial (int x)
{
    int y=1;
    for (; x>0; x--)
    {
        y *= x;
    }
    return y; //階層函式
}
