#include<iostream>
using namespace std;
int square(int); //宣告函數型態
int cube(int);
int factorial(int);

int main()
{
    int a;
    cout<<"Please enter a number( >0 ):";
    cin>>a;
    while(a<=0) //假如輸入給a的數值小於或等於0,進入迴圈要求重新輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ):";
        cin>>a;
    }
    cout<<"Square: "<<square(a)<<endl;//顯示函數回傳的數值
    cout<<"Cube: "<<cube(a)<<endl;
    cout<<"Factorial: "<<factorial(a)<<endl;
}

int square(int a) //定義函數內容
{
    return a*a; //將a平方後回傳
}

int cube(int a)
{
    return a*a*a;
}
int factorial(int a)
{
    if (a>0)
    {
        return (a*factorial(a-1));//階乘的數值然後回傳
    }
    else
        return 1;
}
