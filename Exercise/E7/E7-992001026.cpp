#include <iostream>

using namespace std;

// 設定函數
int square( int n )
{
    return n*n;
}

int cube( int n )
{
    return n*n*n;
}

int factorial( int n )
{
    return (n<2 ? 1 : factorial(n-1)*n);
}
//主涵式
int main()
{
    int n = 1;
    do
    {
        cout << (n<=0 ? "Out of range!\n" : "" ) << "Please enter a number( >0 ): ";
        cin >> n;

    }
    while( n<=0 );
//輸出
    cout << "Square: " << square(n) << endl;
    cout << "Cube: " << cube(n) << endl;
    cout << "Factorial: " << factorial(n) << endl;
    return 0;
}
