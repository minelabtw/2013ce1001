#include <iostream>
#include <cmath>
#include <limits>
using namespace std;
//為了可以算更大的數字，通通使用unsigned long long
unsigned long long Square ( unsigned long long );
unsigned long long Cube ( unsigned long long );
unsigned long long Factorial ( unsigned long long );

int main()
{
    unsigned long long input;
    do
    {
        cout << "Please enter a number( >0 ): ";                //提示使用者輸入何種長度
        cin >> input;
        if ( input <= 0 || !(cin) || input != int (input) )     //若cin進非零負數、讓cin錯誤、非整數時
        {
            cin.clear();                                        //先清除cin的錯誤狀態
            cin.ignore(numeric_limits<streamsize>::max(), '\n');    //刪除緩衝區所有的資料
            cout << "Out of range!\n";          //提示錯誤
            input = -1;                         //將input直接改成-1，以節省while的再次判斷
        }
    }
    while ( input <= 0 );
    cout << "Square: " << Square ( input ) << endl
         << "Cube: " << Cube ( input ) << endl
         << "Factorial: " << Factorial ( input );       //輸出各種計算結果

    return 0;
}

unsigned long long Square ( unsigned long long inputSquare )        //計算平方的函式
{
    return inputCube*inputCube;     //為了可以計算更大的數，不使用pow
}

unsigned long long Cube ( unsigned long long inputCube )            //計算立方的函式
{
    return inputCube*inputCube*inputCube;
}
unsigned long long Factorial ( unsigned long long inputFactorial )  //計算階層的函式
{
    unsigned long long outputFactorial = 1;
    for ( unsigned long long counter = 1 ; counter <= inputFactorial ; counter ++)
    {
        outputFactorial *= counter ;
    }
    return outputFactorial;
}


