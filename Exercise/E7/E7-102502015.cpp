#include <iostream>
#include <cmath>
using namespace std;
float a;                                            //a 輸入的數
float c=1;                                          //階層值
float Square(float);                                //宣告三個函數 輸入FLOAT 輸出FLOAT
float Cube(float);
float Factorial(float);
int main()
{
    cout<<"Please enter a number( >0 ): ";
    cin>>a;
    while(a<=0)                                     //超過範圍重輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>a;
    }
    cout<<"Square: "<<Square(a)<<endl;              //運算三個函數
    cout<<"Cube: "<<Cube(a)<<endl;
    cout<<"Factorial: "<<Factorial(a)<<endl;
    return 0;
}
float Square(float y)
{
    return y*y;                                     //回傳平方
}
float Cube(float y)
{
    return y*y*y;                                   //回傳立方
}
float Factorial(float y)
{
    for (int b=1; b<=y; b++)                        //運算階層
    {
        c=c*b;
    }
    return c;                                       //回傳值
}
