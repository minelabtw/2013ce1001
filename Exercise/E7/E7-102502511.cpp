#include <iostream>
using namespace std;
double Square ( double ); //設fuction Square
double Cube ( double ); //設fuction Cube
double Factorial ( double ); //設fuction Factorial
int main()
{
    double a; //設一個變數a
    cout << "Please enter a number( >0 ): ";
    cin >> a;
    while(a<=0) //判定變數a是否符合定義
    {
        cout << "Out of range!" <<endl;
        cout << "Please enter a number( >0 ): ";
        cin >> a;
    }

    cout << "Square: " << Square ( a ) << endl; //輸出當Square函式帶a時的數字
    cout << "Cube: " << Cube ( a ) << endl; //輸出當Cube函式帶a時的數字
    cout << "Factorial: " << Factorial ( a ); //輸出當Factorial函式帶a時的數字
return 0;
}

double Square ( double b ) //定義Square
{
    return b*b; //Square的回傳值 b*b
}

double Cube ( double c ) //定義Cube
{
    return c*c*c; //Cube的回傳值 c*c*c
}

double Factorial ( double d ) //定義Factorial
{
    int j=1; //設j的初始值為1
    for(int i=1;i<=d;i++) //i從1開始遞增 直到i=d
    {
        j=i*j; //j為i*j 不停乘 直到跳出for
    }
    return j; //Factorial的回傳值為j
}
