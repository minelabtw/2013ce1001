#include<iostream>
using namespace std;
void Square (int);
void Cube(int);
void Factorial(int);
int main()

{
    int x=0;//宣告變數
    while(x<=0)
    {
        cout<<"Please enter a number(>0): ";//輸入數字
        cin>>x;
        if(x<=0)
            cout<<"Out of range!"<<endl;//跑出警告
    }
    Square(x);
    Cube(x);
    Factorial(x);
    return 0;
}
void Square(int x)//計算平方
{
    cout<<"Square: "<<x*x<<endl;
}
void Cube(int x)//計算立方
{
    cout<<"Cube: "<<x*x*x<<endl;
}
void Factorial(int x)//計算階層
{
    int sum=1;
    for(x; x>0; x=x-1)
        sum *=x;
    cout<<"Factorial: "<<sum<<endl;
}
