#include<iostream>
#include<iomanip>
#include<math.h>
using std::cout;
using std::cin;
using std::endl;
double Square(int number)
{
    return pow(number,2) ;
}
double Cube(int number)
{
    return pow(number,3) ;
}
int Factorial(int number)
{
    int dot=1 ;
    for (int i=1; i<=number; ++i)
        dot =dot*i;
    return dot ;
}
int main()
{
    int number ;
    do
    {
        cout << "Please enter a number( >0 ): " ;
        cin >> number ;
        if (number <=0)
            cout << "Out of range!" ;
    }
    while (number <=0);
    cout << "Square: " <<Square(number)<<endl;
    cout << "Cube: " << Cube(number)<<endl;
    cout << "Factorial: " << Factorial(number);
    return 0;
}
