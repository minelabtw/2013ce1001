#include <iostream>
using namespace std;

int square(int); // function prototype of suqare
int cube(int); // function prototype of cube
unsigned long factorial(unsigned long); // function prototype of factorial


int main()
{
    int number; // initialize number
    do
    {
        cout << "Please enter a number ( >0 ): "; // input number
        cin >> number;
        if (number<=0)
            cout << "Out of range!\n";
    }
    while (number <=0); // do...while loop to check number is on demand

    cout << "Square: " << square(number) << endl << "Cube: " << cube(number) << endl
         << "Factorial: " << factorial(number);

    return 0;
} // end main

int square(int number)
{
    return number*number; // calculate square and return result
} // end function square

int cube(int number)
{
    return number*number*number; // calculate cube and return result
} // end function cube

unsigned long factorial(unsigned long number)
{
    if (number<=1)
        return 1;
    else
        return number*factorial(number - 1);
} // end function factorial


