#include <iostream>
#include <math.h>
using namespace std;

double integer;

double Square()//計算平方
{
    return pow(integer,2);//回傳平方值
}
double Cube()//計算立方
{
    return pow(integer,3);//回傳立方值
}
double Factorial()//計算階層
{
    int f = 1;//定義起始值
    for ( int a=1 ; a <= integer ; a++ )//利用迴圈
        f*=a;//計算階層

    return f;//回傳算出的值
}

int main()
{
    cout << "Please enter a number( >0 ): ";//輸出字串
    cin >> integer;//輸入變數

    while ( integer <= 0 )//加入迴圈，使變數遵循範圍
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ): ";
        cin >> integer;
    }

    cout << "Squre: " << Square() << endl;//輸出字串，並叫出Square函式
    cout << "Cube: " << Cube() << endl;//輸出字串，並叫出Cube函式
    cout << "Factorial: " << Factorial();//輸出字串。並叫出Factorial函式

    return 0;//返回初始值
}
