#include <iostream>

using namespace std;

double Square(double x); //define function Square
double Cube(double x); // define function Cube
double Factorial(double x); // define function Factorial

int main()
{
	double input;

	while(true) // input number
	{
		cout << "Please enter a number( >0 ): ";
		cin >> input;
		if(input > 0)
			break;
		cout << "Out of range!" << endl;
	}

	cout << "Square: " << Square(input) << endl; // output Square
	cout << "Cube: : " << Cube(input) << endl; // output Cube
	cout << "Factorial: " << Factorial(input) << endl; // output Factorial

	system("pause");

	return 0;
}

double Square(double x) //implement function Square
{
	return x * x;
}

double Cube(double x) // implement function Cube
{
	return x * x * x;
}

double Factorial(double x) // implement function Factorial
{
	double ans = 1;

	for(int i = 1 ; i <= x ; ++i)
		ans *= i;

	return ans;
}
