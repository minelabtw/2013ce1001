#include <iostream>
#include <math.h>
using namespace std ;

int Square ( int x ) ;  //宣告函式Square，

int Cube ( int x ) ;  //宣告函式Cube。

int Factorial ( int x ) ;  //宣告函式Factorial。

int main ()
{
    int number = 0 ;

    cout << "Please enter a number( >0 ): " ;
    cin >> number ;
    while ( number <= 0 )
    {
        cout << "Out of range!" << endl << "Please enter a number( >0 ): " ;
        cin >> number ;
    }  //使操作者輸入值，並限制其範圍。

    cout << "Square: " << Square ( number ) << endl ;
    cout << "Cube: " << Cube ( number ) << endl ;
    cout << "Factorial: " << Factorial ( number ) << endl ;

    return 0 ;
}

int Square ( int x )
{
    int square = x * x ;
    return square ;
}  //定義Square函數值為輸入值的平方。

int Cube ( int x )
{
    int cube = x * x * x ;
    return cube ;
}  //定義Cube函數值為輸入值的立方。。

int Factorial ( int x )
{
    int factorial = 1 ;
    for ( int i = 1 ; i <= x ; i ++ )
    {
        factorial *= i ;
    }
    return factorial ;
}  //定義Factorial函數值為輸入值的階層。
