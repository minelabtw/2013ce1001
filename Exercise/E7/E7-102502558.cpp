#include <iostream>
using namespace std;
// Prototype
int Square(int);
int Cube(int);
int Factorial(int);
int main()
{
    int n = 0;
    // input the number
    do
    {
        cout << "Please enter a number( >0 ): ";
        cin >> n;
    } while (n <=0 && cout << "Out of range!" << endl);

    // call functions
    cout << "Square: " << Square(n) << endl;
    cout << "Cube: " << Cube(n) << endl;
    cout << "Factorial: " << Factorial(n) << endl;
    return 0;
}

int Square(int n)
{
    return n*n; // square = n*n
}

int Cube(int n)
{
    return n*n*n; // cube = n*n*n
}

int Factorial(int n)
{
    int r = 1;
    for (int i=1;i<=n;i++)
        r *= i;
    return r;
}
