#include <iostream>
using namespace std;
float Square(float);                     //宣告函數
float Cube(float);
float Factorial(float);
int d=1;


int main()
{
    float a;                            //輸入的數為a
    cout<<"Please enter a number( >0 ): ";
    cin>>a;
    while (a<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>a;
    }

    cout<<"Square: "<<Square(a)<<endl;  //運算函數
    cout<<"Cube: "<<Cube(a)<<endl;
    cout<<"Factorial: "<<Factorial(a)<<endl;

    return 0;
}
float Square(float b)                   //計算平方
{
    return b*b;

}
float Cube(float b)                     //計算立方
{
    return b*b*b;
}
float Factorial(float b)                //計算階層
{
    for(int c=1; c<=b; c++)
    {
        d=d*c;
    }
    return d;
}


