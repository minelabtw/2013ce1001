#include <iostream>
#include <math.h>

using namespace std;

int num;    //輸入變數

double square()    //宣告函數
{
    do    //判斷變數是否合理
    {
        cout<<"Please enter a number( >0 ): ";
        cin>>num;
        if(num<=0)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(num<=0);

    return pow(num,2);
}

double cube()    //宣告函數
{
    return pow(num,3);
}

double factorial()    //宣告函數
{
    int one=1;    //宣告變數
    int ans=1;

    while(one<=num)    //階層運算
    {
        ans=ans*one;
        one++;
    }

    return ans;
}

int main()
{
    double anssquare=square();    //宣告變數
    double anscube=cube();
    double ansfactorial=factorial();

    cout<<"Square: "<<anssquare<<endl;
    cout<<"Cube: "<<anscube<<endl;
    cout<<"Factorial: "<<ansfactorial;

    return 0;
}
