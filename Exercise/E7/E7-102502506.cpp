#include <iostream>

using namespace std;

int num;
int fac = 1;
int Square( int num )
{
    return num * num;
}
int Cube( int num )
{
    return num * num * num;
}
int Factorial( int num )
{
    for (int i = 1; i <= num; i++ )
    {
        fac = fac * i;
    }
    return fac;
}

int main ()
{
    cout << "Please enter a number( >0 ): ";
    cin >> num;

    while ( num <= 0 )
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ): ";
        cin >> num;
    }
    cout << "Square: " << Square( num ) << endl;
    cout << "Cube: " << Cube( num ) << endl;
    cout << "Factorial: " << Factorial( num );

    return 0;
}
