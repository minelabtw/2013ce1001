/*************************************************************************
    > File Name: E7-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年11月06日 (週三) 16時16分26秒
 ************************************************************************/

#include <stdio.h>

/* function to check input */
inline int check_int(int x);

/* function to output */
inline void Square(int x);
inline void Cube(int x);
inline void Factorial(int x);

/* function to input number */
inline void input_int(const char *str, const char *error, int *tmp,int (*check)(int));

int main()
{
	/* declare all variables */
	int num;

	/* input all variable */
	input_int("Please enter a number( >0 ): ", "Out of range!", &num, check_int);

	/* output */
	Square(num);
	Cube(num);
	Factorial(num);

	return 0;
}

/* x means the number will be check */
inline int check_int(int x)
{
	/* check x is in range (0, unlimit) */
	return x > 0;
}

/*******************************************/
/* this function is used to input double   */
/*	                                       */
/*   str means the ui message			   */
/* error means the error message		   */
/*  *tmp means the point of input		   */
/* check means the function to check input */
/*******************************************/
inline void input_int(const char *str, const char *error, int *tmp, int (*check)(int))
{
	for(;;)
	{
		printf("%s", str);	// print ui

		scanf("%d", tmp);	// input number

		if(check(*tmp))		// check the input is in range
			return;
		else
			puts(error);
	}
}

/* x means the input number */
inline void Square(int x)
{
	/* output answer */
	printf("Square: %d\n", x*x);
}

/* x means the input number */
inline void Cube(int x)
{
	/* output answer */
	printf("Cube: %d\n", x*x*x);
}

/* x means the input number */
inline void Factorial(int x)
{
	int ans = x;

	/* calculate Factorial */
	while(--x)
		ans *= x;

	/* output answer */
	printf("Factorial: %d\n", ans);
}

