#include<iostream>
using namespace std;
long int Square(int x)          //計算並輸出平方的function
{
    x*=x;
    return x;
}
long int Cube(int x)            //計算並輸出立方的Function
{
    x*=x*x;
    return x;
}
long long int Factorial(int x)  //計算並輸出階層的function
{
    long long int sum=1;
    for(int i=1; i<=x; i++)
    {
        sum*=i;
    }
    return sum;
}
int main()
{
    int i=0;
    int input;
    while(i!=-1)                //說明並讀入input
    {
        cout << "Please enter a number( >0 ): ";
        cin >> input;
        if(input<=0)
        {
            cout << "Out of range!\n";
        }
        else
            i=-1;
    }
    cout << "Square: " << Square(input) << endl;        //輸出結果
    cout << "Cube: " << Cube(input) << endl;
    cout << "Factorial: " << Factorial(input) << endl;
    return 0;
}
