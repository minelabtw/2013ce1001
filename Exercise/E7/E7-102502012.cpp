#include<iostream>
using namespace std;
int Square(int n)
{
    return n*n;
}
int Cube(int n)
{
    return n*n*n;
}
int Factorial(int n)
{
    int ans=1; // store n!
    for(int i=1; i<=n; i++) // calculate factorial
        ans*=i;
    return ans;
}
int main()
{
    int n;
    do
    {
        cout<<"Please enter a number( >0 ): ";
        cin>>n;
    }
    while( n<=0&&cout<<"Out of range!"<<endl );
    cout<<"Square: "<<Square(n)<<endl;
    cout<<"Cube: "<<Cube(n)<<endl;
    cout<<"Factorial: "<<Factorial(n)<<endl;
    return 0;
}
