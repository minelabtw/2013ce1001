#include <iostream>
using namespace std;

double Square( double a);
double Cube( double a);
double Factorial( double a);

int main()
{
    double a =0;
    do
    {
        cout << "Please enter a number ( >0 ):";
        cin >> a;
        if ( a<0 )
        {
            cout << " Out od range! ";
        }
    }
    while( a<0 );
    cout << "Square:" << Square(a) << endl;
    cout << "Cube:" << Cube (a) <<endl;
    cout << "Factorial: "<< Factorial(a) << endl;
    return 0;
}
double Square( double a)
{
    return a*a;
}
double Cube ( double a)
{
    return a*a*a;
}
double Factorial( double a)
{
    double sum=1;
    for ( double b=1;b<=a;b++)
    {
        sum *= b ;
    }
    return sum;
}
