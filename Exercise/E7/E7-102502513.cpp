#include <iostream>
using namespace std;

double Square(double number) //平方函式
{
    return number*number;
}

double Cube(double number)  //立方函式
{
    return number*number*number;
}

double Factorial(double number, double factorial=1)  //階層函式
{
    for(double x=1; x<=number; x++)
    {
        factorial *= x;
    }
    return factorial;
}

int main()
{
    double number=0, result=0;

    while(number<=0)
    {
        cout << "Please enter a number( >0 ): ";
        cin >> number;

        if(number<=0)
            cout << "Out of range!\n";
    }

    result=Square(number);  //呼叫平方函式
    cout << "Square: " << result << endl;
    result=Cube(number);  //呼叫立方函式
    cout << "Cube: " << result << endl;
    result=Factorial(number);  //呼叫階層函式
    cout << "Factorial: " << result << endl;

    return 0;
}
