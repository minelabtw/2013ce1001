#include<iostream>
#include <math.h>
using namespace std;
double square(double);
double cube(double);
int factorial(int);


double square(double input)          //定義function
{
    return(input*input);             //計算平方值
}

double cube(double input)            //定義function
{
    return(input*input*input);       //計算次方值
}

int factorial(int input)             //定義function
{
    int result=1;
    for(int a=1;a<=input;a++)
    {
        result=result*a;
    }                                //計算階層
    return result;
}

int main()
{
    int input=0;
    do
    {
        cout<<"Please enter a number( >0 ): ";
        cin>>input;
        if(input<=0)
            cout<<"Out of range!"<<endl;
    }
    while(input<=0);                 //判斷輸入的值

    cout<<"Square: "<<square(input)<<endl
        <<"Cube: "<<cube(input)<<endl
        <<"Factorial: "<<factorial(input);
                                     //輸出函數計算的值
    return 0;
}
