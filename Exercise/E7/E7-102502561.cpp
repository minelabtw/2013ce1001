#include<iostream>
using namespace std;

int square(int);    //宣告原形
int cube(int);      //宣告原形
int factorial(int); //宣告原形

int main()
{
    int num;    //輸入的數字

    cout << "Please enter a number( >0 ): ";    //題意
    cin >> num;
    while(num<=0)   //確認輸入的數字符合題意
    {
        cout << "Out of range!\n";
        cout << "Please enter a number( >0 ): \n";
        cin >> num;
    }
    cout << "Square: " << square(num) << "\n";  //呼叫square
    cout << "Cube: " << cube(num) << "\n";      //呼叫cube
    cout << "Factorial: " << factorial(num);    //呼叫factorial

    return 0;
}

int square(int x)
{
    return x*x;
}

int cube(int x)
{
    return x*x*x;
}

int factorial(int x)
{
    for(int i=x-1;i>0;i--){x = x*i;}
    return x;
}
