//E7-102502026
//Using function for square, cube, factorial
#include<iostream>
using namespace std;

double square (double); //use function prototype square
double cube (double);   //use function prototype cube
int factorial (int);    //use function prototype factorial

int main()  //start program
{
    double number=0;    //define number
    cout<<"Please enter a number( >0 ): ";
    cin>>number;        //ask number

    while(number<=0)    //do until number >0
    {
        cout<<"Out of range!\n";    //wrong
        cout<<"Please enter a number( >0 ): ";
        cin>>number;    //ask again number
    }
    cout<<"Square: "<< square (number)<<"\n";       //print answer for square
    cout<<"Cube: "<<cube(number)<<"\n";             //print answer for cube
    cout<<"Factorial: "<<factorial(number)<<"\n";   //print answer for factorial

    return 0;
}   //end program

double square (double x)    //doing this of square (number) Resolving
{
    return x*x;
}

double cube (double x)      //doing this of cube (number) Resolving
{
    return x*x*x;
}

int factorial (int x)       //doing this of factorial (number) Resolving
{
    if (x<=1)
        return 1;
    else
        return x * factorial(x-1);
}

