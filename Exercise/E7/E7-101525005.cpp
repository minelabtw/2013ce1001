#include <iostream>

using namespace std;

//宣告protoType
int CalculateSquare(int);
int CalculateCube(int );
int CalculateFactorial(int );

int main()
{
    int num,numIsValid=0;



    while(numIsValid==0)
    {
        cout << "Please enter a number( >0 ): " ;
        cin >> num ;
        if(num>0)
        {

            cout << "Square: " << CalculateSquare(num) << endl;
            cout << "Cube: " << CalculateCube(num) << endl;

            //計算出階層值並印出
            cout << "Factorial: " << CalculateFactorial(num) << endl;

        }
        else
        {
            cout << "Out of range!" << endl;
        }
    }

    return 0;
}

int CalculateSquare(int num)
    {
        int square=0;
        square=num*num;
        return square;
    }
    int CalculateCube(int num)
    {
        int cube=0;
        cube=num*num*num;
        return cube;
    }
    int CalculateFactorial(int num)
    {
        int factorial=1;
        for(int i=1;i<=num;i++)
        {
            factorial*=i;
        }
        return factorial;
    }
