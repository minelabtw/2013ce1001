#include <iostream>

using namespace std;

//function declartion
int Square (int input);
int Cube (int input);
int Factorial (int input);

int main()
{
    int number=0;//be used as input

    do//prompt for data
    {
        cout << "Please enter a number: ";
        cin >> number;
    }
    while (number<=0&&cout << "Out of range!\n");//reask for legal input

    //result
    cout << "Square: " << Square (number) << endl;
    cout << "Cube: " << Cube (number) << endl;
    cout << "Factorial: " << Factorial (number) << endl;

    return 0;
}

int Square (int input)
{
    int square=0;
    square=input*input;
    return square;
}

int Cube (int input)
{
    int cube=0;
    cube=input*input*input;
    return cube;
}

int Factorial (int input)
{
    int factorial=1;
    for (int x=1; x<=input; x=x+1)
    {
        factorial=factorial*x;
    }
    return factorial;
}
