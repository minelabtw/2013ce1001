#include <iostream>
using namespace std;

double number;  //宣告number為一個變數

double Square(double a)  //定義Square function
{
    return a*a;  //回傳平方值
}

double Cube(double a)  //定義Cube function
{
    return a*a*a;  //回傳立方值
}

int Factorial(int a)  //定義Factorial function
{
    int b=1;
    int c=1;
    while (b<=a)  //計算階層值
    {
        c*=b;
        b++;
    }
    return c;  //回傳階層值
}

int main()
{
    cout << "Please enter a number( >0 ): ";
    cin >> number;

    while (number<=0)  //判斷number為一個大於0的數
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ): ";
        cin >> number;
    }


    cout << "Square: " << Square (number) << endl;  //呼叫Square function
    cout << "Cube: " << Cube (number) << endl;  //呼叫Cube funcion
    cout << "Factorial: " << Factorial (number);  //呼叫Factorial funcion

    return 0;
}
