#include<iostream>
#include <math.h>
using namespace std;
int number;
int Square()  //計算平方
{
    return number*number;
}
int Cube()  //計算立方
{
    return number*number*number;
}
int Factorial()  //計算階層
{
    int factorial=1;
    for(int x=1; x<=number; x++)
    {
        factorial=factorial*x;
    }
    return factorial;
}
int main()
{
    cout<<"Please enter a number( >0 ): ";  //輸出題目
    cin>>number;  //輸入數字
    while(number<=0)  //判斷是否符合條件
    {
        cout<<"Out of range!\n";
        cout<<"Please enter a number( >0 ):";
        cin>>number;
    }
    int square=Square();  //呼叫Square()
    cout<<"Square: "<<square;
    cout<<endl;
    int cube=Cube();  //呼叫Cube()
    cout<<"Cube: "<<cube;
    cout<<endl;
    int factorial=Factorial();  //呼叫Factorial()
    cout<<"Factorial: "<<factorial;
    cout<<endl;
    return 0;
}
