#include <iostream>
#include <iomanip>
using namespace std;
int Square(int);
int Cube(int);
int Factorial(int);
int main()
{
    int x;
    cout<<"Please enter a number( >0 ): ";
    cin>>x;
    while(x<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>x;
    }
    int A=Square(x);
    int B=Cube(x);
    int C=Factorial(x);
    cout<<"Square: "<<A<<endl;
    cout<<"Cube: "<<B<<endl;
    cout<<"Factorial: "<<C<<endl;

    return 0;
}
int Square(int x)
{
    return x*x;
}
int Cube(int x)
{

    return x*x*x;
}
int Factorial(int x)
{
    int y,factorial=1;
    for (y=1 ; y<=x ; y++)
    {
        factorial=y*factorial;
    }
    return factorial;
}
