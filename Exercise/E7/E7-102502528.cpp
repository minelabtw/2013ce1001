#include<iostream>
#include <math.h>
using namespace std;

double Square(double a)               //計算平方
{
    return a*a;
}

double Cube(double a)                //計算立方
{
    return pow(a,3);
}

double Factorial(double a)            //計算階層
{
    double Fac= 1;
    for(int i=1; i<=a; i++)
    {
        Fac = Fac * i;
    }
    return Fac;
}

int main()
{
    double n;
    cout << "Please enter a number( >0 ): ";
    cin >> n;
    while(n<=0)
    {
        cout <<"Out of range!"<<endl<<"Please enter a number( >0 ): ";
        cin >> n;
    }
    cout << "Square: " <<Square(n)<<endl;           //呼叫Square()
    cout << "Cube: "<<Cube(n)<<endl;              //呼叫Cube()
    cout <<"Factorial: "<<Factorial(n);          //呼叫Factorial()
    return 0;
}

