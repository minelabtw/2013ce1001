#include <iostream>
using namespace std;

//declare function's prototype
int Square(int);
int Cube(int);
int Factorial(int);

int main()
{
	int number;
	while(1){
		cout << "Please enter a number( >0 ): "; cin >> number;
		if(number<=0){cout << "Out of range!\n"; continue;}		//redo if number is out of range
		break;
	}

	cout << "Square: " << Square(number) << "\n"
		<< "Cube: " << Cube(number) << "\n"
		<< "Factorial: " << Factorial(number) << "\n";

	return 0;
}

int Square(int n)		//calculate Square of number
{
	return n*n;
}

int Cube(int n)			//calculate Cube of number
{
	return n*n*n;
}

int Factorial(int n)	//calculate Factorial of number
{
	if(n==1||n==0){return 1;}
	return n*Factorial(n-1);	//recursion: n!=n*(n-1)!
}
