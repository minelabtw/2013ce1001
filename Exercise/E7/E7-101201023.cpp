#include <iostream>

using namespace std;

int Square(int n);                   //宣告方程式
int Cube(int n);
int Factorial(int n);


int Square(int n)                    //設置方程式的入n的執行
{
    return n*n;
}

int Cube(int n)
{
    return n*n*n;
}

int Factorial(int n)
{
    int i=1;
    int p=0;
    int m=1;
    while(i<n)
    {
        p=(i+1)*m;
        m=p;
        i++;
    }

    return p;
}

int main()
{
    int n=0;

    cout << "Please enter a number( >0 ):";
    cin >> n;
    while(n<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Please enter a number( >0 ):";
        cin >> n;
    }
    cout << "Square: " << Square(n) << endl;              //輸出 Square: 並將n帶入方程式Square()再輸出
    cout << "Cube: " << Cube(n) << endl;
    cout << "Factorial: " << Factorial(n);

    return 0;
}
