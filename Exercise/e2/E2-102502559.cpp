#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    int num1; //宣告整數變數num1
    int num2; //宣告整數變數num2

    cin >> num1 >> num2; //將先後輸入的值分別配給num1及num2

    cout << num1 << "+" << num2 << "=" << num1+num2 << endl; //顯示"num1的值"+"num2的值"=num1+num2的計算結果,然後換行
    cout << num1 << "-" << num2 << "=" << num1-num2 << endl; //顯示"num1的值"-"num2的值"=num1-num2的計算結果,然後換行
    cout << num1 << "*" << num2 << "=" << num1*num2 << endl; //顯示"num1的值"*"num2的值"=num1*num2的計算結果,然後換行
    system ("pause");
    return 0;
}

