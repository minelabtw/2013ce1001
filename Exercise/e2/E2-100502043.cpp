#include<iostream>

using std::cout;
using std::cin;
using std::endl;

    int main()
{
    int num1 = 0,num2 = 0;  //宣告兩變數，並使其初始化為0
    int sum = 0;  //宣告兩數相加之值，並使其初始化為0
    int difference = 0;  //宣告兩數相減之值，並使其初始化為0
    int product = 0;  //宣告兩數相乘之值，並使其初始化為0

    cin >> num1; //輸入變數1
    cin >> num2;  //輸入變數2

    sum = num1 + num2;
    cout << num1 << "+" << num2 << "=" << sum << endl;  //輸出兩數之和

    difference = num1 - num2;
    cout << num1 << "-" << num2 << "=" << difference << endl;  //輸出兩數之差

    product = num1 * num2;
    cout << num1 << "*" << num2 << "=" << product; //輸出兩數之積

    return 0;
}
