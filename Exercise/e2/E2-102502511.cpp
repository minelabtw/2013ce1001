#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1 = 0; //宣告第一個數字,並初始化其數值為0
    int integer2 = 0; //宣告第二個數字,並初始化其數值為0
    int sum = 0; //宣告型別為整數,並初始化其數值為0
    int reduce = 0; //宣告型別為整數,並初始化其數值為0
    int multiply = 0; //宣告型別為整數,並初始化其數值為0

    cout << "請輸入第一個數字:"; //將字串"請輸入第一個數字："輸出到螢幕上。
    cin >> integer1; //從鍵盤輸入，並將輸入的值給integer1。

    cout << "請輸入第二個數字:"; //將字串"請輸入第二個數字："輸出到螢幕上。
    cin >> integer2; //從鍵盤輸入，並將輸入的值給integer2。

    sum = integer1 + integer2; //計算數字1+數字2等於
    cout << integer1<< "+" << integer2 << "=" << sum << endl;

    reduce = integer1 - integer2; //計算數字1減數字2等於
    cout << integer1<< "-" << integer2 << "=" << reduce << endl;

    multiply= integer1 * integer2; //計算數字1乘數字2等於
    cout << integer1<< "*" << integer2 << "=" << multiply;

    return 0;
}
