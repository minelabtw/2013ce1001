#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int integer1 ;  //宣告第一個變數
    int integer2 ;  //宣告第二個變數
    int sum;        //宣告兩數之和
    int product;    //宣告兩數之積
    int difference; //宣告兩數之差




    cout << "請輸入第一個整數:" ;
    cin >> integer1;                //把使用者輸入的第一個數字存到integer1變數

    cout << "請輸入第二個整數:" ;
    cin >> integer2;                //第二個數則給integer2

    sum = integer1 + integer2;      //進行兩數相加，並把和給sum變數
    product = integer1 * integer2;  //相乘後的積丟給product
    difference = integer1 - integer2;   //兩數的差則給difference變數

    cout << integer1 << "+" << integer2 <<"="<< sum<<endl;    //把和輸出到螢幕上
    cout << integer1 << "-" << integer2 <<"="<< difference<<endl;    //把差輸出到螢幕上
    cout << integer1 << "×" << integer2 <<"="<< product<<endl;    //把積輸出到螢幕上

    return 0;
}

