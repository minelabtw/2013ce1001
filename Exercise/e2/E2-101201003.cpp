#include <iostream>
using std::cout;
using std::cin;

int main()
{
    int integer1=0;          //宣告型別為 整數(int) 的整數一，並初始化其數值為0
    int integer2=0;          //宣告型別為 整數(int) 的整數二，並初始化其數值為0
    int addition=0;          //宣告型別為 整數(int) 的和，並初始化其數值為0
    int subtraction=0;       //宣告型別為 整數(int) 的差，並初始化其數值為0
    int multiplication=0;    //宣告型別為 整數(int) 的積，並初始化其數值為0

    cin >>integer1;           //輸入數字，並將值給integer1
    cin >>integer2;           //輸入數字，並將值給integer2

    addition=integer1+integer2;       //將整數一及整數二相加並送到addition
    subtraction=integer1-integer2;    //將整數一及整數二相減並送到subtraction
    multiplication=integer1*integer2; //將整數一及整數二相乘並送到multiplication

    cout << integer1 << "+" << integer2 << "=" << addition << "\n";     //顯示算式並換行
    cout << integer1 << "-" << integer2 << "=" << subtraction << "\n";  //顯示算式並換行
    cout << integer1 << "*" << integer2 << "=" << multiplication;       //顯示算式

    return 0;
}
