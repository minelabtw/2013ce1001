#include <iostream>
using namespace std;

int main()
{
    int number1;    //宣告型別為 整數(int) 的第一數
    int number2;    //宣告型別為 整數(int) 的第二數

    cin >> number1 >> number2;                                              //從鍵盤輸入，並將輸入的值給第一數和第二數
    cout << number1 << "+" << number2 << "=" << number1+number2 << endl;    //計算number1+number的結果並輸出到螢幕上
    cout << number1 << "-" << number2 << "=" << number1-number2 << endl;    //計算number1-number的結果並輸出到螢幕上
    cout << number1 << "*" << number2 << "=" << number1*number2 << endl;    //計算number1*number的結果並輸出到螢幕上

    return 0;
}
