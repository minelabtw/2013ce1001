#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;  //宣告兩個整數變數
    int sum;                //宣告名稱為sum的整數變數
    int difference;         //宣告名稱為difference的整數變數
    int product;            //宣告名稱為product的整數變數

    cout << "第一個整數：";   //將"第一個整數"輸出到螢幕上
    cin >> integer1;          //輸入值給integer1

    cout << "第二個整數：";   //"將"第二個整數"輸出到螢幕上
    cin >> integer2;          //輸入值給integer2

    sum = integer1 + integer2 ;  //將兩個數相加,並把值給sum
    cout << "相加後結果為：" << sum << endl;

    difference = integer1 - integer2 ;  //將兩個數相減,並將值給difference
    cout << "相減後結果為：" << difference << endl;

    product = integer1 * integer2 ;  //將兩個數相乘,並將值給product
    cout << "相乘的結果為 : " << product ;

    return 0;
}
