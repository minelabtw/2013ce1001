#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int integer1,integer2;
    int addition;
    int subtraction;
    int multiplication;

    cin >> integer1;
    cin >> integer2;

    addition = integer1 + integer2;  //計算相加
    cout << integer1 << "+" << integer2 << "=" << addition << endl;

    subtraction = integer1 - integer2;  //計算相減
    cout << integer1 << "-" << integer2 << "=" << subtraction << endl;

    multiplication = integer1*integer2;  //計算相乘
    cout << integer1 << "*" << integer2 << "=" << multiplication;
    return 0;
}
