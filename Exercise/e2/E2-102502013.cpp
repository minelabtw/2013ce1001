#include <iostream>

using namespace std;
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1; //宣告第一個整數變數
    int integer2; //宣告第二個整數變數
    int sum1; //宣告第一個運算結果
    int sum2; //宣告第二個運算結果
    int multiply; //宣告第三個運算結果

    cin >> integer1; //輸入第一個整數
    cin >> integer2; //輸入第二個整數

    sum1 = integer1 + integer2;
    cout << integer1 << "+" << integer2 << "=" << sum1 << endl; //輸出integer1+integer2的結果

    sum2 = integer1 - integer2;
    cout << integer1 << "-" << integer2 << "=" << sum2 << endl; //輸出integer1-integer2的結果

    multiply = integer1 * integer2;
    cout << integer1 << "*" << integer2 << "=" << multiply << endl; //輸出integer1*integer2的結果

    return 0;
}
