#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1 = 0;   //宣告型別為 整數(int) 的變數，並初始化其數值為0。
    int integer2 = 0;
    int value1 = 0;     //宣告型別為 整數(int) 的數值，並初始化其數值為0。
    int value2 = 0;
    int value3 = 0;


    cin >> integer1;  //輸入一個整數

    cin >> integer2;

    value1 = integer1 + integer2;  //計算兩數相加
    cout << integer1 << "+" << integer2 << "=" << value1 << endl;  //輸出兩數相加的方程式

    value2 = integer1 - integer2;  //計算兩數相減
    cout << integer1 << "-" << integer2 << "=" << value2 << endl;  //輸出兩數相減的方程式

    value3 = integer1 * integer2;  //計算兩數相乘
    cout << integer1 << "*" << integer2 << "=" << value3;  //輸出兩數相乘的方程式

    return 0;
}
