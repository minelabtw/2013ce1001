#include <iostream>

using namespace std;

int main()
{
    int integral1 = 0; //宣告第一個整數
    int integral2 = 0; //宣告第二個整數
    int sum = 0; //宣告和
    int difference = 0; //宣告差
    int product = 0; //宣告積

    cin >> integral1; //輸入第一個整數

    cin >> integral2; //輸入第二個整數

    sum = integral1 + integral2; //第一個整數加第二個整數等於和
    cout << integral1 << "+" << integral2 << "=" << sum << endl; //輸出和 換行

    difference = integral1 - integral2; //第一個整數減第二個整數等於差
    cout << integral1 << "-" << integral2 << "=" << difference << endl; //輸出差 換行

    product = integral1 * integral2; //第一個整數乘於第二個整數等於積
    cout << integral1 << "*" << integral2 << "=" << product; //輸出積

    return 0;
}
