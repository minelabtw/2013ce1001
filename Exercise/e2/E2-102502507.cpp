#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1 = 0, integer2 = 0;     //宣告型別為 整數(int) 的長度及寬度，並初始化其數值為0。
    int sum = 0;            //宣告型別為 整數(int) 的周長，並初始化其數值為0。
    int difference = 0;                 //宣告型別為 整數(int) 的面積，並初始化其數值為0。
    int product = 0;
    cout << "請輸入第一個整數：";
    cin >> integer1;

    cout << "請輸入第二個整數：";
    cin >> integer2;

    sum =  integer1+integer2 ;     //計算周長
    cout << "兩數相加為" << sum << endl;
    difference =  integer1-integer2 ;                 //計算面積
    cout << "兩數相減為" << difference<< endl;
    product = integer1*integer2;
    cout << "兩數相乘為" <<product;

    return 0;
}
