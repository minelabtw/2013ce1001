#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1 = 0;   //宣告型別為 整數(int)的number1，並初始化其數值為0。
    int number2 = 0;   //宣告型別為 整數(int)的number2，並初始化其數值為0。
    int sum1 = 0;      //宣告型別為 整數(int)的sum1，並初始化其數值為0。
    int sum2 = 0;      //宣告型別為 整數(int)的sum2，並初始化其數值為0。
    int sum3 = 0;      //宣告型別為 整數(int)的sum3，並初始化其數值為0。

    cout << "請輸入第一個整數:";   //將"請輸入第一個整數:"輸出至螢幕。
    cin >> number1;                //輸入至number1。
    cout << "請輸入第二個整數:";   //將"請輸入第二個整數:"輸出至螢幕。
    cin >> number2;                //輸入至number2。

    sum1 = number1 + number2;  //計算加法。
    cout << number1 << "+" << number2 << "=" << sum1 << endl;  //將number1+number2=sum1輸出至螢幕。
    sum2 = number1 - number2;  //計算減法。
    cout << number1 << "-" << number2 << "=" << sum2 << endl;  //將number1-number2=sum2輸出至螢幕。
    sum3 = number1 * number2;  //計算乘法。
    cout << number1 << "*" << number2 << "=" << sum3;  //將number1*number2=sum3輸出至螢幕。

    return 0;
}
