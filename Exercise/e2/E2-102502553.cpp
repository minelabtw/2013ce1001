#include<iostream>
using namespace std;

int main()
{
    int number1 = 0; //輸入一個整數變數，並初始化為0
    int number2 = 0; //輸入另一個整數變數，並初始化為0

    cout << "請輸入一個數字"; //輸入字串，告訴使用者輸入數字顯示在螢幕上
    cin >> number1; //使用者輸入數字

    cout << "再輸入一個數字";
    cin >> number2;

    cout << number1 << "+" << number2 << "=" << number1 + number2 << endl; //在螢幕上顯示出，把使用者輸入的兩個數字相加，並換行
    cout << number1 << "-" << number2 << "=" << number1 - number2 << endl; //在螢幕上顯示出，把使用者輸入的兩個數字相減，並換行
    cout << number1 << "X" << number2 << "=" << number1 * number2 << endl; //在螢幕上顯示出，把使用者輸入的兩個數字相乘

    return 0; //結束程式
}
