#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1, number2;
    //Preface
    cout << "Please enter two numbers and we will provide you the addition," << endl
    << " subtraction and mutiplication of the two numbers."
    << endl << endl << "Please enter number1, space and number2" << endl;
    //Input numbers
    cin >> number1 >> number2;
    //Output results
    cout << "The addition: " << number1 << " + " << number2 << " = " << number1 + number2 << endl;
    cout << "The subtraction: " << number1 << " - " << number2 << " = " << number1 - number2 << endl;
    cout << "The mutiplication: " << number1 << " * " << number2 << " = " << number1 * number2 << endl;

    return 0;


}
