#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()

{
    int number1 = 0,number2 = 0,sum = 0,subtraction = 0,product = 0;//宣告型別為 整數(int) 的數字1、數字2、加及乘，並初始化其數值為0。


    cin >> number1;
    cin >> number2;

    sum = number1+number2;                                         //計算加

    cout << number1  <<"+"  <<number2 << "=" << sum << endl;

    subtraction = number1-number2;                                 //計算減

    cout << number1 <<"-" <<number2 << "=" << subtraction << endl;

    product = number1*number2;                                     //計算乘

    cout << number1 <<"*" <<number2 << "=" << product;

    return 0;
}
