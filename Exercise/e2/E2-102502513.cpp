#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1=0 , integer2=0;
    int sum=0;
    int difference=0;
    int product=0;

    cin >> integer1;
    cin >> integer2;

    sum = integer1 + integer2;                                         //let the value of integer1+integer2 store in sum
    cout << integer1 << "+" << integer2 << "=" << sum << "\n" ;

    difference = integer1 - integer2;                                  //let the value of integer1-integer2 store in difference
    cout << integer1 << "-" << integer2 << "=" << difference << "\n";

    product = integer1 * integer2;                                     //let the value of integer1*integer2 store in product
    cout << integer1 << "*" << integer2 << "=" << product << "\n";

    return 0;
}

