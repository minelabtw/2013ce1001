#include<iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int number_1;//宣告變數(整數)1
    int number_2;//宣告變數(整數)2

    cin >> number_1;//讀入數值至變數1
    cin >> number_2;//讀數數值至變數2

    cout << number_1 << "+" << number_2 << "=" << number_1+number_2 << endl;//計算加法並列印出結果
    cout << number_1 << "-" << number_2 << "=" << number_1-number_2 << endl;//計算減法並列印出結果
    cout << number_1 << "*" << number_2 << "=" << number_1*number_2 << endl;//計算乘法並列印出結果

    return 0;
}
