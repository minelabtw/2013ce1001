#include <iostream>
using namespace std;//讓之後的STD::都可省略不寫

int main()
{
    int number1 = 0;//宣告整數變數
    int number2 = 0;//宣告整數變數
    int sum = 0;//存入的和
    int difference = 0;//存入的差
    int product = 0;//存入的積

    cin  >> number1;//輸入第一個數字
    cin  >> number2;//輸入第二個數字

    sum = number1 + number2;//使數字1和數字2相加後存入SUM裡
    difference = number1 - number2;//使數字1減數字2後存入difference中
    product = number1*number2;//使數字1和數字2相乘後存入product

    cout << number1 << "+" << number2 << "=" << sum << "\n";//螢幕顯示相加的過程和結果
    cout << number1 << "-" << number2 << "=" << difference << "\n";//螢幕顯示相減的過程和結果
    cout << number1 << "*" << number2 << "=" << product << "\n";//螢幕顯示相乘的過程和結果

    return 0;

}
