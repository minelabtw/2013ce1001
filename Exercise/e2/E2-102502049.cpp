#include<iostream>
using namespace std;

int main()
{
    int integer1 = 0;//宣告變數型別為 整數 的第一個變數，並初始化其值為0
    int integer2 = 0;//宣告變數型別為 整數 的第二個變數，並初始化其值為0
    int sum = 0;//宣告變數型別為 整數 的兩數之和，並初始化其值為0
    int subtraction = 0;//宣告變數型別為 整數 的兩數之差，並初始化其值為0
    int product = 0;//宣告變數型別為 整數 的兩數乘積，並初始化其值為0

    cin >> integer1;//輸入第一個變數
    cin >> integer2;//輸入第二個變數

    sum = integer1 + integer2;//將兩變數之和其值給sum
    subtraction = integer1 - integer2;//將兩數之差其值給subtraction
    product = integer1 * integer2;//將兩數乘積其值給product

    cout << integer1 << "+" << integer2 << "=" << sum << endl;//顯示出 變數一+變數二=兩數之和
    cout << integer1 << "-" << integer2 << "=" << subtraction << endl;//顯示出 變數一-變數二=兩數之差
    cout << integer1 << "*" << integer2 << "=" << product << endl;//顯示出 變數一*變數二=兩數乘積

    return 0;
}
