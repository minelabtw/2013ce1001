/*************************************************************************
    > File Name: E2-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年10月02日 (週三) 14時57分21秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

int main()
{
	int n, m;

	scanf("%d%d", &n, &m); //input 2 integer n, m
	printf("%d+%d=%d\n", n, m, n+m); //output n+m=
	printf("%d+%d=%d\n", n, m, n-m); //output n-m=
	printf("%d+%d=%d\n", n, m, n*m); //output n*m=
	
	return 0;
}

