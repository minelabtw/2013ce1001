#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2 ;                                             //宣告兩個整數變數
    int sum ;                                                           //宣告兩個整數的和
    int minus ;                                                         //宣告兩個整數的差
    int multiply ;                                                      //宣告兩個整數的積

    cout    <<"請輸入兩個數字：\n";                                       //將字串顯示到螢幕
    cin     >> integer1;                                                //從鍵盤輸入，並將輸入的值給integer1
    cin     >> integer2;                                                //從鍵盤輸入，並將輸入的值給integer2


    sum = integer1 + integer2;                                          //將兩個數字相加，並將值給sum
    minus = integer1 - integer2;                                        //將兩個數字相減，並將值給minus
    multiply = integer1 * integer2;                                     //將兩個數字相乘，並將值給multiply


    cout     <<integer1 << "+" << integer2 << "=" << sum << endl;
    cout     <<integer1 << "-" << integer2 << "=" << minus << endl;
    cout     <<integer1 << "*" << integer2 << "=" << multiply << endl;




    return 0;

}
