#include <iostream>

using namespace std;

int main(void) {
    int number_1, number_2; // declare number_1 and number_2

    cin >> number_1 >> number_2; //input number_1 and number_2

    cout << number_1 << "+" << number_2 << "=" << (number_1 + number_2) << endl; //ouput number_1 + number_2

    cout << number_1 << "-" << number_2 << "=" << (number_1 - number_2) << endl; //ouput number_1 - number_2

    cout << number_1 << "*" << number_2 << "=" << (number_1 * number_2) << endl; //ouput number_1 * number_2
    return 0;
    }
