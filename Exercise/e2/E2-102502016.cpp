#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a = 0;//a,b為變數,c,d,e分別為加法,減法,乘法的結果
    int b = 0;
    int c = 0;
    int d = 0;
    int e = 0;

    cin >> a;
    cin >> b;

    c = a + b;
    cout << a <<"+"<< b <<"="<< c<<endl;//輸入兩變數並列出結果(加法)

    d = a - b;
    cout << a <<"-"<< b <<"="<< d<<endl;//輸入兩變數並列出結果(減法)

    e = a * b;
    cout << a <<"*"<< b <<"="<< e;//輸入兩變數並列出結果(乘法)

    return 0;
    }
