#include <iostream>
using namespace std ;
int main ()
{
    int num1 ;  //宣告第一個數
    int num2 ;  //宣告第二個數
    int sum ;   //宣告和
    int dif ;  //宣告差
    int product ; //宣告積

    cin >> num1 ; //輸入第一個數
    cin >> num2 ; //輸入第二個數

    sum = num1 + num2     ; //計算和
    dif = num1 - num2     ; //計算差
    product = num1 * num2 ; //計算積

    cout << num1 << "+" << num2 << "=" << sum << endl  ; //輸出 第一個數+第二個數=和

    cout << num1 << "-" << num2 << "=" << dif << endl ; //輸出 第一個數+第二個數=差

    cout << num1 << "*" << num2 << "=" << product ; //輸出第一個數+第二個數=積
    return 0 ;
}
