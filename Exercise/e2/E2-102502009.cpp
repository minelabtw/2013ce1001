#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1; //宣告一個整數變數number1
    int number2; //宣告一個整數變數number2
    int sum=0;   //宣告一個整數變數sum，並初始化為0
    int subtraction=0; //宣告一個整數變數subtraction，並初始化為0
    int product=0;     //宣告一個整數變數product，並初始化為0

    cin>>number1; //輸入number1的值

    cin>>number2; //輸入number1的值

    sum = number1 + number2; //把number1與number2相加，並把得到的值給sum

    cout<<number1<<" + "<<number2<<" = "<<sum<<endl; //輸出number1+number2=sum

    subtraction = number1 - number2; //把number1與number2相減，並把得到的值給subtraction

    cout<<number1<<" - "<<number2<<" = "<<subtraction<<endl; //輸出number1-number2=subtraction

    product = number1 * number2; //把number1與number2相乘，並把得到的值給product

    cout<<number1<<" * "<<number2<<" = "<<product<<endl; //輸出number1*number2=product

    return 0;

}
