#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1 = 0;                            //宣告名稱為integer1的整數變數，並初始化其數值為0。
    int integer2 = 0;                            //宣告名稱為integer2的整數變數，並初始化其數值為0。
    int sum = 0;                                 //宣告名稱為sum的整數變數，用來儲存兩數和並初始化其數值為0。
    int difference = 0;                          //宣告名稱為difference的整數變數，用來儲存兩數差並初始化其數值為0。
    int product = 0;                             //宣告名稱為product的整數變數，用來儲存兩數積並初始化其數值為0。

    cin >> integer1;                             //將鍵盤輸入的值貯存到integer1。
    cin >> integer2;                             //將鍵盤輸入的值貯存到integer2。

    sum = integer1 + integer2 ;                  //將integer1和integer2相加，並將和貯存在sum。
    cout << integer1 << "+" << integer2 << "=" << sum << endl;            //將兩數和輸出至銀幕上。

    difference = integer1 - integer2 ;           //將integer1和integer2相減，並將差貯存在sum。
    cout << integer1 << "-" << integer2 << "=" << difference << endl;     //將兩數和輸出至銀幕上。

    product = integer1 * integer2 ;              //將integer1和integer2相乘，並將積貯存在sum。
    cout << integer1 << "*" << integer2 << "=" << product << endl;       //將兩數和輸出至銀幕上。


    return 0;
}
