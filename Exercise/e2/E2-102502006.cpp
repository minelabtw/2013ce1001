#include <iostream>

using std::cin;
using std::cout;

int main()
{

    int fstnum; // 第一個數
    int scdnum; // 第二個數
    int sum = 0; // 加
    int diff = 0; // 減
    int mul = 0; // 乘

    cin >> fstnum;
    cin >> scdnum;

    sum = fstnum + scdnum; // 加法
    diff = fstnum - scdnum; // 減法
    mul = fstnum * scdnum; // 乘法

    cout << fstnum << "+" << scdnum << "=" << sum << "\n";
    cout << fstnum << "-" << scdnum << "=" << diff << "\n";
    cout << fstnum << "*" << scdnum << "=" << mul << "\n";



    //while(1);
    return 0;

}
