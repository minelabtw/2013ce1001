#include <iostream>

using namespace std ;

int main()
{
    int integer1 =0 ;  //宣告第一個整數變數。
    int integer2 =0 ;  //宣告第二個整數變數。

    cin >> integer1 ;  //操作者輸入第一個整數並給integer1。
    cin >> integer2 ;  //操作者輸入第二個整數並給integer2。

    cout << integer1 << "+" << integer2 << "=" << integer1 + integer2 << endl ;  //將算式和答案顯示在螢幕上。以下亦同。
    cout << integer1 << "-" << integer2 << "=" << integer1 - integer2 << endl ;
    cout << integer1 << "*" << integer2 << "=" << integer1 * integer2 << endl ;

    return 0 ;
}
