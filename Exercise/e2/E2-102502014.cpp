#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int int1;      //宣告兩變數int1和int2
    int int2;
    int sum= 0;    //宣告變數sum為int1跟int2之和
    int minus=0;   //宣告變數minus為int1跟int2之差
    int times=0;   //宣告變數times為int1跟int2之乘積

    cin >> int1;
    cin >> int2;

    sum = int1 + int2;   //輸出sum之結果
    cout <<int1<<"+"<<int2<<"="<<sum<< endl;

    minus = int1 - int2;   //輸出minus之結果
    cout <<int1<<"-"<<int2<<"="<<minus<<endl;

    times = int1*int2;   //輸出times之結果
    cout <<int1<<"*"<<int2<<"="<<times<<endl;

    return 0;
}
