// 10/2 Exercise 2
// simple mathematical operation

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1 = 0;
    int integer2 = 0;
    int plus = 0;
    int minus = 0;
    int multiplies = 0;

    cin >> integer1;
    cin >> integer2;

    plus = integer1 + integer2;
    cout << integer1 << "+" << integer2 << "=" << plus << endl; // isolate the number and symbol

    minus = integer1 - integer2;
    cout << integer1 << "-" << integer2 << "=" << minus << endl;

    multiplies = integer1 * integer2;
    cout << integer1 << "*" << integer2 << "=" << multiplies << endl;

    return 0; // end successfully
}
