#include<iostream>
using std::cout;
using std::cin;
using std::endl;



int main()
{
    int number_1; //宣告整數的變數number_1。
    int number_2; //宣告整數的變數number_2。
    int sum=0;  //宣告名稱為sum的整數變數，並初始化其數值為0。
    int subtract=0; //宣告名稱為subtract的整數變數，並初始化其數值為0。
    int multiply=0; //宣告名稱為multiply的整數變數，並初始化其數值為0。

    cin >> number_1; //從鍵盤輸入，並將輸入的值給number_1。

    cin >> number_2; //從鍵盤輸入，並將輸入的值給number_2。

    sum = number_1 + number_2;  //將number_1 + number_2並儲存於sum裡。
    cout <<number_1 << "+" << number_2 << "=" << sum <<endl; //將輸入進去的number_1.number_2相加。

    subtract = number_1 - number_2;//將number_1 - number_2並儲存於subtract裡。
    cout <<number_1 << "-" << number_2 << "=" << subtract <<endl; //將輸入進去的number_1.number_2相減。

    multiply = number_1 * number_2;//將number_1 * number_2並儲存於multiply裡。
    cout <<number_1 << "*" << number_2 << "=" << multiply;//將輸入進去的number_1.number_2相乘。


    return 0;
}
