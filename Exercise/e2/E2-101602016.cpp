#include <iostream>
using std::cin;
using std::cout;
using std::endl;


int main ()
{
    int int1=0;             //宣告型別為 整數(int) 的整數一，並初始化其數值為0
    int int2=0;             //宣告型別為 整數(int) 的整數二，並初始化其數值為0
    int sum=0;              //宣告型別為 整數(int) 的和，並初始化其數值為0
    int difference=0;       //宣告型別為 整數(int) 的差，並初始化其數值為0
    int product=0;          //宣告型別為 整數(int) 的積，並初始化其數值為0

    cout << "請輸入兩個整數" << endl;     //在螢幕上顯示"請輸入兩個整數"
    cin >>int1;                           //輸入數字，並將值給int1
    cin >>int2;                           //輸入數字，並將值給int2

    sum = int1 + int2;                 //將整數一及整數二相加並送到sum
    difference = int1 + (-1)*int2;    //將整數一及整數二相減並送到difference
    product = int1 * int2;            //將整數一及整數二相乘並送到product

    cout << int1 << "+" << int2 << "=" << sum << endl;         //顯示算式並換行
    cout << int1 << "-" << int2 << "=" << difference << endl;  //顯示算式並換行
    cout << int1 << "*" << int2 << "=" << product;             //顯示算式

    return 0;
}
