// E2-102502026-李俊賢
// sum, rest, multiplicacion

#include <iostream>
using std::cout;
using std::cin;

int main() // start the program

{
    int number1;// defining number1
    int number2;// defining number2
    int sum=0;
    int rest=0;
    int mult=0;

    cout<<"輸入第一個整數:";//ask the number1
    cin>> number1;

    cout<<"輸入第二個整數:";//ask the number2
    cin>>number2;

    sum= number1+number2;
    cout<< number1<<"+"<<number2<<"="<<sum<<"\n";//sum 2 numbers
    rest= number1-number2;
    cout<<number1<<"-"<<number2<<"="<<rest<<"\n";//rest 2 numbers
    mult= number1*number2;
    cout<<number1<<"*"<<number2<<"="<<mult<<"\n";//multiplicates 2 numbers

    return 0;
}//end program
