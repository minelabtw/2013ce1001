#include <iostream>
using std:: cout;
using std:: cin;
using std:: endl;

int main ()
{
    int integer1 = 0, integer2 = 0;   //宣告 整數1&2，並使其初始化為0
    int plus = 0, minus = 0;         //宣告 兩數相加之和，兩數相減之差，並使其初始化為0
    int multiple = 0;                //宣告 兩數相乘之積，並使其初始化為0


    cin >> integer1 >> integer2;

    plus =  integer1 + integer2;
    minus = integer1 - integer2;
    multiple = integer1 * integer2;

    cout << integer1 << "+" << integer2 << "=" << plus << endl;    //計算和
    cout << integer1 << "-" << integer2 << "=" << minus << endl;   //計算差
    cout << integer1 << "*" << integer2 << "=" << multiple;       //計算積

   return 0 ;

}
