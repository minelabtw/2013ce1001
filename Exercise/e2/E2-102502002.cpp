#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
  int integer1;   //宣告第一個整數。
  int integer2;   //宣告第二個整數。
  int sum;        //宣告兩整數的和，為integer1+integer2。
  int sub;        //宣告兩整數的差，為integer1-integer2。
  int mult;       //宣告兩整數的積，為integer1*integer2。

  cout << "請輸入一整數:" ;    //將"請輸入一整數:"由螢幕輸出。
  cin >> integer1;             //從鍵盤輸入，並將輸入的值給integer1。

  cout << "請輸入另一整數:" ;  //將"請輸入另一整數:"由螢幕輸出。
  cin >> integer2;             //從鍵盤輸入，並將輸入的值給integer2。

  sum = integer1 + integer2 ;               //將兩個整數相加，並將相加後的值丟給sum。
  cout << "" << integer1 << "+" << "" << integer2 << "=" << sum << endl;    //將兩數相加的值輸出到螢幕。

  sub = integer1 - integer2 ;               //將兩個整數相減，並將相減後的值丟給sub。
  cout << "" << integer1 << "-" << "" << integer2 << "=" << sub << endl;    //將兩數相減的值輸出到螢幕。

  mult = integer1*integer2;                 //將兩個整數相乘，並將相乘後的值丟給mult。
  cout << "" << integer1 << "*" << "" << integer2 << "=" << mult << endl;   //將兩數相乘的值輸出到螢幕。

  return 0;
}
