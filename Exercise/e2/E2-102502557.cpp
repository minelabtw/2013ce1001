#include <iostream>                              //含括iostream檔案
#include <cstdlib>                               //含括cstdlib檔案
using namespace std;                             //使用std名稱空間
main()
{
    int num1;                                    //宣告整數變數num1
    int num2;                                    //宣告整數變數num2

    cin>>num1>>num2;                             //輸入num1還有num2

    cout<<num1<<"+"<<num2<<"="<<num1+num2<<endl; //把num1和num2相加，並輸出
    cout<<num1<<"-"<<num2<<"="<<num1-num2<<endl; //把num1和num2相減，並輸出
    cout<<num1<<"*"<<num2<<"="<<num1*num2<<endl; //把num1和num2相乘，並輸出
    system ("pause");
    return 0;
}
