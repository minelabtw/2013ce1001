#include <iostream>
using namespace std;

int main()
{
    int integer1,integer2; //宣告兩個整數變數
    int sum = 0;           //宣告名稱為sum的整數變數並初始化其數值為0
    int minus = 0;         //宣告名稱為minus的整數變數並初始化其數值為0
    int multiplies = 0;    //宣告名稱為multiplies的整數變數並初始化其數值為0

    cin >> integer1;       //從鍵盤輸入，並將輸入的值給integer1
    cin >> integer2;       //從鍵盤輸入，並將輸入的值給integer2

    sum = integer1 + integer2;  //將兩個整數相加，並將相加後的值丟給sum
    cout << integer1 << "+" << integer2 << "=" << sum << endl;

    minus = integer1 - integer2;  //將兩個整數相減,並將其值丟給minus
    cout << integer1 << "-" << integer2 << "=" << minus << endl;

    multiplies = integer1 * integer2;  //將兩個整數相乘<並將其值丟給multiplies
    cout << integer1 << "*" << integer2 << "=" << multiplies << endl;

    return 0;
}
