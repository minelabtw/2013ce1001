#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1 ;                       //宣告一個整數變數
    int integer2 ;                       //宣告一個整數變數。
    int sum ;                            //宣告名稱為sum的整數變數
    int differ ;                         //宣告名稱為differ的整數變數
    int product ;                        //宣告名稱為product的整數變數

    cin >> integer1;                     //從鍵盤輸入，並將輸入的值給integer1
    cin >> integer2;                     //從鍵盤輸入，並將輸入的值給integer2

    sum = integer1 + integer2;           //將兩個整數相加，並將相加後的值丟給sum
    differ = integer1 - integer2;        //將兩個整數相減，並將相減後的值丟給differ
    product = integer1 * integer2;       //將兩個整數相乘，並將相乘後的值丟給product

    cout << integer1<<"+"<<integer2<<"="<<sum<<endl;
    cout << integer1<<"-"<<integer2<<"="<<differ<<endl;
    cout << integer1<<"*"<<integer2<<"="<<product;
    return 0;
}
