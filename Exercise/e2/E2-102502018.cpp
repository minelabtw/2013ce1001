#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1;
    int integer2;
    int sum1 = 0;
    int sum2 = 0;
    int sum3 = 0;                          //宣告名稱為sum1.2.3的整數變數，並初始化其數值為0。
    cin >> integer1;                       //從鍵盤輸入，並將輸入的值給integer1。
    cin >> integer2;                       //從鍵盤輸入，並將輸入的值給integer2。
    sum1=integer1+integer2;                //兩個整數相加後的值代入sum1
    sum2=integer1-integer2;                //兩個整數相減後的值代入sum2
    sum3=integer1*integer2;                //兩個整數相乘後的值代入sum3
    cout << integer1<<"+"<<integer2<<"="<<sum1<<endl;
    cout << integer1<<"-"<<integer2<<"="<<sum2<<endl;
    cout << integer1<<"*"<<integer2<<"="<<sum3;


    return 0;
}
