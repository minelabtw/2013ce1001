#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main ()

{
    int a,b;//輸入兩整數
    int sum = 0;//將輸入的兩整數相加
    int sub = 0;//將輸入的兩整數相減
    int pro = 0;//將輸入的兩整數相乘

    cout << " ";//輸入第一個整數
    cin >> a;

    cout << " ";//輸入第二個整數
    cin >> b;

    sum = ( a + b );
    cout << a << " + " << b << "=" << sum <<endl;//計算兩數總和
    sub = ( a - b );
    cout << a << " - " << b << "=" << sub <<endl;//計算兩數差
    pro = ( a * b );
    cout << a << " * " << b << "=" << pro <<endl;//計算兩數積

    return 0;


}
