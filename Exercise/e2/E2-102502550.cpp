#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int interger1 = 0;           //宣告型別為 整數(int) 的一個數，並初始化其數值為0。
    int interger2 = 0;           //宣告型別為 整數(int) 的一個數，並初始化其數值為0。
    int S = 0;                   //宣告型別為 整數(int) S作為和的變數，並初始化其數值為0。
    int D = 0;                   //宣告型別為 整數(int) D作為差的變數，並初始化其數值為0。
    int P = 0;                   //宣告型別為 整數(int) P作為積的變數，並初始化其數值為0。

    cin >> interger1;          //輸入變數1
    cin >> interger2;          //輸入變數2

    S = interger1 + interger2;            //計算和
    cout << interger1 << "+" << interger2 << "=" << S << endl;

    D = interger1 - interger2;              //計算差
    cout << interger1 << "-" << interger2 << "=" << D << endl;

    P = interger1 * interger2;              //計算積
    cout << interger1 << "*" << interger2 << "=" << P << endl;

    return 0;
}
