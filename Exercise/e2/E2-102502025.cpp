#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()

    {
        int number1,number2;                //宣告兩個變數整數。
        int sum = 0;                        //宣告整數的加法，並初始化其數值為0。
        int redu = 0;                       //宣告整數的減法，並初始化其數值為0。
        int mult = 0;                       //宣告整數的乘法，並初始化其數值為0。

        cout << "請輸入第一個整數：";
        cin >> number1;

        cout << "請輸入第二個整數：";
        cin >> number2;

        sum = number1 + number2;            //計算兩個整數相加。
        cout << number1 << "+"  << number2  << "=" <<sum << endl;       //顯示加法。

        redu = number1 - number2;           //計算兩個整數相減。
        cout << number1 << "-" << number2 << "="  << redu << endl;      //顯示減法。

        mult = number1 * number2;           //計算兩個整數相乘。
        cout << number1 << "*" << number2 << "=" << mult <<endl ;       //顯示乘法。

        return 0;
    }
