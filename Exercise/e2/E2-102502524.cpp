#include <iostream>
using namespace std;

int main()
{
    int num1 = 0;
    int num2 = 0;
    int add = 0;
    int minu = 0;
    int mult = 0;

    cin >> num1;
    cin >> num2;

    add = num1+num2;
    minu = num1-num2;
    mult = num1*num2;

    cout << num1 << "+" << num2 << "=" << add << endl;
    cout << num1 << "-" << num2 << "=" << minu << endl;
    cout << num1 << "*" << num2 << "=" << mult << endl;
}
