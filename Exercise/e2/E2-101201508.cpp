#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a = 0,b = 0;             //宣告型別為 整數(int) 為a,b，並初始化其數值為0。


    cin >> a;                    //和使用者要第一個數字
    cin >> b;                    //和使用者要第二個數字

    cout <<  a+b << endl;        //做加運算並輸出跳行
    cout <<  a-b << endl;        //做減運算並輸出跳行
    cout <<  a*b << endl;        //做乘運算並輸出跳行


    return 0;
}
