#include <iostream>

using std::cout;
using std::cin;
using std::endl;


int main()
{
    int number1;                          //定義一整數
    int number2;                          //定義一整數

    int sum1;                             //定義一整數
    int sum2;                             //定義一整數
    int sum3;                             //定義一整數

    cout<<"請輸入一個整數!!\n";           //輸出並換行
    cin>>number1;                         //輸入

    cout<<"請輸入另一個整數!!\n";         //輸出並換行
    cin>>number2;                         //輸入

    sum1=number1+number2;                 //相加
    cout<<"兩數相加的結果為:"<<sum1<<endl;//輸出並換行

    sum2=number1-number2;                 //相減
    cout<<"兩數相減的結果為:"<<sum2<<endl;//輸出並換行

    sum3=number1*number2;                 //相乘
    cout<<"兩數相成的結果為:"<<sum3<<endl;//輸出並換行

    return 0;
}
