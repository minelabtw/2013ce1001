#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1; //宣告型別為int，並初始化其數值為0
    int integer2; //宣告型別為int，並初始化其數值為0
    int sum = 0; //宣告型別為int的和，並初始化其數值為0
    int difference = 0; //宣告型別為int的差，並初始化其數值為0
    int product = 0; //宣告型別為int的積，並初始化其數值為0

    cin >> integer1;
    cin >> integer2;

    sum = integer1 + integer2; //計算和
    cout << integer1 << "+" << integer2 << "=" << sum << endl;

    difference = integer1 - integer2; //計算差
    cout << integer1 << "-" << integer2 << "=" << difference << endl;

    product = integer1 * integer2; //計算積
    cout << integer1 << "*" << integer2 << "=" <<product;

    return 0;
}
