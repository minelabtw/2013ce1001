#include <iostream>

using  namespace std;



int main()
{
    int integral1;//宣告第一個整數
    int integral2;//宣告第二個整數
    int sum = 0;//宣告和
    int minus = 0;//宣告差
    int multiplies = 0;//宣告積

    cin >> integral1;//輸入第一個整數
    cin >> integral2;//輸入第二個整數

    sum = integral1 + integral2;//和是整數一加整數二
    cout << integral1 << "+" << integral2 << "=" << sum << endl;//輸出整數一加整數二等於和

    minus = integral1 - integral2;//和是整數一減整數二
    cout << integral1 << "-" << integral2 << "=" << minus << endl;//輸出整數一減整數二等於差

    multiplies = integral1 * integral2;//和是整數一乘整數二
    cout << integral1 << "*" << integral2 << "=" << multiplies;//輸出整數一乘整數二等於積

    return 0;

}
