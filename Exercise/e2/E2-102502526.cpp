#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;  //宣告兩個整數變數
    int sum1=0;             //宣告名稱為sum1的整數變數，並初始化其值為0，用來存放integer1+integer2
    int sum2=0;             //宣告名稱為sum2的整數變數，並初始化其值為0，用來存放integer1-integer2
    int sum3=0;             //宣告名稱為sum2的整數變數，並初始化其值為0，用來存放integer1*integer2

    cout << "請輸入第一個整數:";
    cin >> integer1;

    cout << "請輸入第二個整數:";
    cin >> integer2;

    sum1 = integer1 + integer2 ;
    cout << integer1 << "+" << integer2 << "=" << sum1 << endl;

    sum2 = integer1 - integer2 ;
    cout << integer1 << "-" << integer2 << "=" << sum2 <<endl;

    sum3 = integer1 * integer2 ;
    cout << integer1 << "*" << integer2 << "=" << sum3 <<endl;

    return 0;
}
