#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
  int a= 0, b= 0;   //宣告型別為 整數(int) 的 a與b
  cin >> a >> b;
  cout<< a << "+" << b << "=" <<a+b<< endl;//計算a+b
  cout<< a << "-" << b << "=" <<a-b<< endl;//計算a-b
  cout<< a << "*" << b << "=" <<a*b<< endl;//計算a*b
  return 0;
}
