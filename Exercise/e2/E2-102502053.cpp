#include<iostream>
using namespace std;

int main()
{
    int integer1 =0; //call a variable
    int integer2 =0; //call a variable
    int sum =0; // variable for sum
    int difference =0; // variable for difference
    int product =0; // variable for product

    cin >> integer1;// input a number
    cin >> integer2;//input a number

    sum = integer1+integer2; // calculate for addition
    difference = integer1-integer2; // calculate for subtraction
    product = integer1*integer2; // calculate for multiplication

    cout << integer1 << "+" << integer2 << "=" << sum << endl; //display result of sum
    cout << integer1 << "-" << integer2 << "=" << difference << endl;//display result of difference
    cout << integer1 << "*" << integer2 << "=" << product;//display result of product

    return 0;
}
