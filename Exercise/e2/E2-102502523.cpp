#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int number1;//宣告第一個數字
    int number2;//宣告第二個數字
    int answer1;//宣告答案1
    int answer2;//宣告答案2
    int answer3;//宣告答案3
    cout<<"請輸入兩個整數\n";//將字串"請輸入兩個整數"輸出到螢幕上
    cin>>number1;//將第一個輸入的數字的值給number1
    cin>>number2;//將第二個輸入的數字的值給number2
    answer1=number1+number2;//將number1加number2的值給answer1
    answer2=number1-number2;//將number1減number2的值給answer2
    answer3=number1*number2;//將number1乘number2的值給answer3
    cout<<number1<<"+"<<number2<<"="<<answer1<<endl;//輸出結果然後換行
    cout<<number1<<"-"<<number2<<"="<<answer2<<endl;//輸出結果然後換行
    cout<<number1<<"*"<<number2<<"="<<answer3<<endl;//輸出結果然後換行


    return 0;
}
