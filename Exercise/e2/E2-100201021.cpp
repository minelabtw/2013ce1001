#include <iostream>

using namespace std;

main (){
    int x,y;    //def x&y
    //input
    cin >> x;
    cin >> y;
    //output
    cout << x << "+" << y << "=" << x+y << endl;    //x+y
    cout << x << "-" << y << "=" << x-y << endl;    //x-y
    cout << x << "*" << y << "=" << x*y << endl;    //x*y

    return 0;
}
