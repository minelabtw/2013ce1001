#include <iostream>

using namespace std;

int main()
{
    int num1 = 0; //宣告名為"num1"的變數。
    int num2 = 0; //宣告名為"num2"的變數。

    cin >> num1; //從鍵盤讀取的數字存到"num1"。
    cin >> num2; //從鍵盤讀取的數字存到"num2"。

    cout << num1 << "+" << num2 << "=" << num1 + num2 << "\n"; //將計算結果輸出到螢幕上。
    cout << num1 << "-" << num2 << "=" << num1 - num2 << "\n"; //將計算結果輸出到螢幕上。
    cout << num1 << "*" << num2 << "=" << num1 * num2 << "\n"; //將計算結果輸出到螢幕上。

    return 0;
}
