#include <iostream>         //將輸入/輸出函式庫給含括進來
using namespace std;        //指名使用std這個名稱空間
int main()
{
    int integer1;           //宣告第一個整數變數
    int integer2;           //宣告第二個整數變數
    int sum = 0;            //宣告兩整數之和並將其初始化
    int difference = 0;     //宣告兩整數之差並將其初始化
    int product = 0;        //宣告兩整數之積並將其初始化
    cin >> integer1;
    cin >> integer2;

    sum = integer1 + integer2;          //將兩數相加並將其值丟回給sum這個整數變數
    difference = integer1 - integer2;   //將兩數相減並將其值丟回給difference這個整數變數
    product = integer1 * integer2;      //將兩數相乘並將其值丟回給product這個整數變數

    cout << integer1 <<"+"<< integer2 <<"="<< sum <<endl;           //輸出兩數相加及其和至螢幕上
    cout << integer1 <<"-"<< integer2 <<"="<< difference <<endl;    //輸出兩數相減及其差至螢幕上
    cout << integer1 <<"*"<< integer2 <<"="<< product <<endl;       //輸出兩數相乘及其積滯螢幕上
    return 0;


}
