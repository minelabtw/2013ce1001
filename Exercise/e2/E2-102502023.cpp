#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2,a,b,c; //宣告型別為 整數(int)
    cin >> integer1;
    cin >> integer2;
    a=integer1 + integer2; //計算二數和
    cout << integer1 << "+" << integer2 << "="  << a << endl;
    b=integer1 - integer2; //計算二數差
    cout << integer1 << "-" << integer2 << "=" << b << endl;
    c=integer1 * integer2; //計算二數積
    cout << integer1 << "*" << integer2 << "=" << c << endl;

    return 0;

}
