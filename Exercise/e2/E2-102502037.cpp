#include <iostream>
using namespace std;

int main()
{
    int integer1,integer2; //宣告第一.第二個變數
    int sum = 0; //宣告總合
    int difference = 0; //宣告差
    int product = 0; //宣告乘積

    cin >> integer1; //輸入第一個變數

    cin >> integer2; //輸入第二個變數

    sum = integer1 + integer2; //相加總合
    cout << integer1 << "+" << integer2 << "=" << sum << endl;

    difference = integer1 - integer2; //相減之差
    cout << integer1 << "-" << integer2 << "=" << difference << endl;

    product = integer1 * integer2; //相乘之積
    cout << integer1 << "*" << integer2 << "=" << product << endl;

    return 0;
}
