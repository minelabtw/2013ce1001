#include <iostream>

using namespace std;

int main()
{
    int int1 = 0,int2 =0; //宣告型別為整數(int) 的int1和int2，並初始化其數值為0。

    cin >> int1 ; //從鍵盤輸入，並將數值給int1
    cin >> int2 ; //從鍵盤輸入，並將數值給int2

    cout << int1 << "+" << int2 << "=" << int1 + int2 << endl; //輸出int1+int2的值
    cout << int1 << "-" << int2 << "=" << int1 - int2 << endl; //輸出int1-int2的值
    cout << int1 << "X" << int2 << "=" << int1 * int2 ;        //輸出int1Xint2的值

    return 0;
}
