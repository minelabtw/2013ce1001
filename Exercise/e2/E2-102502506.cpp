#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2;              //宣告兩個變數
    int sum1 = 0;                       //宣告一個名為sum1的整數變數,並初始值為0
    int sum2 = 0;                       //宣告一個名為sum2的整數變數,並初始值為0
    int product1;                       //宣告名為product1的整數變數 用來存放兩個整數乘積

    cout << "請輸入兩個整數:";
    cin >> integer1 >> integer2;

    sum1 = integer1 + integer2;          //將兩個整數相加之值丟入sum1
    cout << integer1 << "+" << integer2 << "=" << sum1 << endl;

    sum2 = integer1 - integer2;
    cout << integer1 << "-" << integer2 << "=" << sum2 << endl;

    product1 = integer1 * integer2;      //將兩個整數乘積丟入product1
    cout << integer1 << "*" << integer2 << "=" << product1 <<endl;

    return 0;
}
