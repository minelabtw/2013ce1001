#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2; //宣告兩個整數變數
    int sum = 0;           //宣告名稱為sum的整數變數，並使其原數值為0
    int subtract = 0;      //宣告名稱為subtract的整數變數，並使其原數值為0
    int multiple = 0;      //宣告名稱為multiple的整數變數，並使其原數值為0

    cout << "請輸入任意兩個整數:";   //將字串"請輸入任意兩個整數:"輸出到螢幕上
    cin >> integer1;                 //從鍵盤輸入，並將輸入的值給integer1
    cin >> integer2;                 //從鍵盤輸入，並將輸入的值給integer2

    sum = integer1 + integer2;                          //將兩個整數相加，並將相加後的值給sum
    cout << "兩個整數相加後結果為:" << sum << endl;

    subtract = integer1 - integer2;                     //將兩個整數相減，並將相加後的值丟給subtract
    cout << "兩個整數相減後結果為:" << subtract << endl;

    multiple = integer1 * integer2;                     //將兩個整數相乘，並將相加後的值丟給multiple
    cout << "兩個整數相乘後結果為:" << multiple;

    return 0;
}
