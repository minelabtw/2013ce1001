#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int i1;   //兩個變數 一個和 一個相減 一個相乘
    int i2;
    int sum = 0;
    int minus = 0;
    int times = 0;

    cin >> i1;                    //儲存第一個變數
    cin >> i2;                    //儲存第二個變數

    sum = i1 + i2;              //定義SUM=兩個變數相加
    cout << i1 <<"+"<<i2<<"="<< sum << endl;

    minus = i1 - i2;            //定義minus=兩個變數相減
    cout << i1 <<"-"<<i2<<"="<< minus << endl;

    times = i1 * i2;            //定義times=兩個變數相乘
    cout << i1 <<"*"<<i2<<"="<< times;

    return 0;
}
