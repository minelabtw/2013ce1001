#include <iostream>
using namespace std ;
int main()
{
    int number1=0 ; // 第一個輸入的數
    int number2=0 ; // 第二個輸入的數
    int sum=0 ; // 和
    int difference=0 ; // 差
    int product=0 ; // 積
    cin >> number1 ; // 輸入第一個數
    cin >> number2 ; // 輸入第二個數
    sum = number1 + number2 ; // 將第一個輸入的數和第二個輸入的數的和丟給左邊
    difference = number1 - number2 ; // 將第一個輸入的數和第二個輸入的數的差丟給左邊
    product = number1 * number2 ; // 將第一個輸入的數和第二個輸入的數的積丟給左邊
    cout << number1 << "+" << number2 << "=" << sum << endl ; // 輸出第一個數與第二個數等於和 換行
    cout << number1 << "-" << number2 << "=" << difference << endl ; // 輸出第一個數與第二個數等於差 換行
    cout << number1 << "*" << number2 << "=" << product ; // 輸出第一個數與第二個數等於積
    return 0 ;

}
