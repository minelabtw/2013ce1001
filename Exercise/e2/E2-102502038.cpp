#include <iostream>//Include iostream for cin and cout.
using namespace std;//Use std namespace.
int main(int argc, char* argv[]){//Var a function which name main as a enter point.
    long long int alufa,beta;//Var two int,alufa and beta.
    cin >> alufa >> beta;//Catch number from CLI.
    cout << alufa << "+" << beta << "=" << (alufa+beta) << "\n";//Output (alufa+beta).
    cout << alufa << "-" << beta << "=" << (alufa-beta) << "\n";//Output (alufa-beta).
    cout << alufa << "*" << beta << "=" << (alufa*beta) << "\n";//Output (alufa*beta).
    return 0;
}
