#include<iostream>

using namespace std;

int main()
{
    cout<<"please enter 2 integers"<<endl;

    int a = 0;  //宣告型別為 整數(int) 的數值，並初始化其數值為0
    int b = 0;  //宣告型別為 整數(int) 的數值，並初始化其數值為0

    cin>>a; //從鍵盤輸入，並將輸入的值給a
    cin>>b; //從鍵盤輸入，並將輸入的值給b

    cout<<a<<"+"<<b<<"="<<a+b<<endl;    //利用cout將字串輸出到螢幕上，並使用endl進行換行
    cout<<a<<"-"<<b<<"="<<a-b<<endl;    //利用cout將字串輸出到螢幕上，並使用endl進行換行
    cout<<a<<"*"<<b<<"="<<a*b<<endl;    //利用cout將字串輸出到螢幕上，並使用endl進行換行

    return 0;
}
