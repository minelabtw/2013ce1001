﻿#include <iostream>

using namespace std;

int main()
{
    int number1 = 0;//宣告一個變數數字一
    int number2 = 0;//宣告一個變數數字二

    cin >> number1;//輸入數字一
    cin >> number2;//輸入數字二

    cout << number1 << "+" << number2 << "=" << number1 + number2 << endl;//輸出 數字一加數字二
    cout << number1 << "-" << number2 << "=" << number1 - number2 << endl;//輸出 數字一減數字二
    cout << number1 << "*" << number2 << "=" << number1 * number2 << endl;//輸出 數字一乘數字二

    return 0;

}
