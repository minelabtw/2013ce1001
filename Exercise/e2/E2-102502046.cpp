#include <iostream>

using std::endl;
using std::cin;
using std::cout;

int main()
{
    int a=0; //宣告型別為 整數(int)，並初始化其數值為0。
    int b=0; //宣告型別為 整數(int)，並初始化其數值為0。

    cin >> a; //輸入第一個整數。
    cin >> b; //輸入第二個整數。

    cout << a << "+" << b << "=" << a+b << endl;  //計算爾個整數相加並輸出。
    cout << a << "-" << b << "=" << a-b << endl;  //計算爾個整數相減並輸出。
    cout << a << "*" << b << "=" << a*b << endl;  //計算爾個整數相乘並輸出。


    return 0;

}
