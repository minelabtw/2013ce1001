#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1;
    int integer2;
    int addition = 0; //宣告相加的結果，並使初始化數值為零
    int subtraction = 0; //宣告相減的結果
    int multiplication = 0; //宣告相乘的結果

    cin >> integer1;
    cin >> integer2;

    addition = integer1 + integer2; //做相加的運算
    cout << integer1 <<"+" << integer2 << "=" << addition << endl; //輸出字串和結果

    subtraction = integer1 - integer2; //做相減的運算
    cout << integer1 << "-" << integer2 << "=" << subtraction << endl;

    multiplication = integer1*integer2; //做相乘的運算
    cout << integer1 << "*" << integer2 << "=" << multiplication;

    return 0;
    }
