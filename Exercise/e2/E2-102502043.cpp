#include <iostream>

using namespace std;

int main()
{
    int integer1;                                                           //宣告變數1
    int integer2;                                                           //宣告變數2
    cin >> integer1;                                                        //輸入變數1
    cin >> integer2;                                                        //輸入變數2
    cout << integer1 << "+" << integer2 << "=" << integer1+integer2 << endl;//變數1+變數2
    cout << integer1 << "-" << integer2 << "=" << integer1-integer2 << endl;//變數1-變數2
    cout << integer1 << "*" << integer2 << "=" << integer1*integer2 << endl;//變數1*變數2
    cout << integer1 << "/" << integer2 << "=" << integer1/integer2 << endl;//變數1/變數2
    return 0;
}
