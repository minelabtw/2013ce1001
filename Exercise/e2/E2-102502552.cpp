#include<iostream>
using namespace std;
int main()
{
    int number1 =0;//宣告一個整數變數
    int number2 =0;//宣告另一整數變數
    int sum;//宣告一個和的整數變數
    int sub;//宣告一個差的整數變數
    int product;//宣告一個積的整數變數

    cout << "請輸入一個數字";//提示輸入
    cin >> number1;//輸入一個整數

    cout << "再輸入一個數字";//提示輸入
    cin >> number2;//輸入另一個整數

    sum = number1 + number2;//將和計算出來
    cout << number1 << "+" << number2 << "=" << sum <<endl;//顯示出算式與和的值並換行

    sub = number1 - number2;//將差計算出來
    cout << number1 << "-" << number2 << "=" << sub <<endl;//顯示出算式與差的值並換行

    product = number1 * number2;//將積計算出來
    cout << number1 << "X" << number2 << "=" << product;//顯示出算式與積的值

    return 0;//結束程式
}//結束主程式
