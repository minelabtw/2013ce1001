#include <iostream>
using namespace std;

int main(){

    int num1,num2; //宣告兩個整數變數

    cin >> num1; //從鍵盤輸入第一個整數給num1
    cin >> num2; //從鍵盤輸入第一個整數給num2

    cout << num1 << "+" << num2 << "=" << num1+num2 <<endl; //計算及輸出兩整數之和到螢幕上

    cout << num1 << "-" << num2 << "=" << num1-num2 <<endl; //計算及輸出兩整數之差(前減後)到螢幕上

    cout << num1 << "*" << num2 << "=" << num1*num2 <<endl; //計算及輸出兩整數之積到螢幕上

    return 0;
}
