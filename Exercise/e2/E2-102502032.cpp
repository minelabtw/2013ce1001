#include <iostream>
using namespace std;

int main()
{
    //varible dacalaration start
    int number[2] = {0,0};     //an integer array named number, has two elements and initialize all to 0
    int sum = 0;               //an integer varible named sum and initialize to 0
    int difference = 0;        //an integer varible named difference and initialize to 0
                               //difference is used as a varible saving minus result
    int product = 0;           //an integer varible named product and initialize to 0
    //varible decalaration end

    while(cin >> number[0] and cin >> number[1]) //ask for two integers
                                                 //put first integer into number[0]
                                                 //put second integer into number[1]
    {
        //compute the result of number[0] and number[1], start
        sum = number[0] + number[1];              //summation
        difference = number[0] - number[1];       //minus ([0]-[1])
        product = number[0] * number[1];          //product
        //compute end

        //output results start (number[0] and number[1])
        cout << number[0] << "+" << number[1] << "=" << sum << endl;          //output summation
        cout << number[0] << "-" << number[1] << "=" << difference << endl;   //output difference ([0]-[1])
        cout << number[0] << "*" << number[1] << "=" << product << endl;      //output product
        //output results end
    }

    return 0;
}
