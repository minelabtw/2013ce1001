#include <iostream>
using std::cout;
using std::cin;
using std::endl;


int main()
{
    int number1;  //宣告一個名為number1的整數
    int number2;  //宣告一個名為number2的整數
    int sum1 = 0; //宣告名稱為sum1的整數變數，其初始值為0。
    int sum2 = 0; //宣告名稱為sum2的整數變數，其初始值為0。
    int sum3 = 0; //宣告名稱為sum3的整數變數，其初始值為0。


    cout << "請輸入一個整數";
    cin >> number1;

    cout << "再輸入一個整數";
    cin >> number2;

    sum1 = number1 + number2;  //把兩個數字相加並將結果交給sum1。
    cout << number1 << "+" << number2 << "=" << sum1 << endl;

    sum2 = number1 - number2;  //第一個數字減掉第二個數字並交結果丟給sum2。
    cout << number1 << "-" << number2 << "=" << sum2 << endl;

    sum3 = number1 * number2;  //將兩個數字相乘並將結果交給sum3。
    cout << number1 << "*" << number2 << "=" << sum3 << endl;


    return 0;
 }


