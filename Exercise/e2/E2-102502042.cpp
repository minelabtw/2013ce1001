#include <iostream>
using namespace std;
int main()
{
    int integer1; //宣告第一個整數型別變數
    int integer2; //宣告第二個整數型別變數
    cin >> integer1 >> integer2; //用cin輸入兩個整數
    cout << integer1 << "+" << integer2 << "=" << integer1 + integer2 << endl; //輸出相加後結果
    cout << integer1 << "-" << integer2 << "=" << integer1 - integer2 << endl; //輸出相減後結果
    cout << integer1 << "*" << integer2 << "=" << integer1 * integer2 << endl; //輸出相乘後結果
    return 0;
}
