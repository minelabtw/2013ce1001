#include <iostream>
using namespace std;

int main()
{
    int num1, num2; //declare two integer variables num1,num2 to store the input numbers
    cin >> num1; //input first number to num1
    cin >> num2; //input second number to num2
    cout << num1 << "+" << num2 << "=" << num1 + num2 << "\n";  //output the addition result to the screen
    cout << num1 << "-" << num2 << "=" << num1 - num2 << "\n";  //output the subtraction result to the screen
    cout << num1 << "*" << num2 << "=" << num1 * num2 << "\n";  //output the multiplication result to the screen

    return 0;   //return 0 to tell the system that the program ended sucessfully
}
