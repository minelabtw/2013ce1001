#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{

    int number1 = 0, number2 = 0 ; //宣告第一及第二的變數 , 並使其數值化為0
    int sum = 0 ;                  //宣告加法結果,並使其數值化為0
    int minus = 0 ;                //宣告減法結果,並使其數值化為0
    int multiple = 0 ;             //宣告乘法結果,並使其數值化為0


    cin >> number1 >> number2 ;    //輸入number1及number2

    sum = number1 + number2 ;      //加法公式
    minus = number1 - number2 ;    //減法公式
    multiple = number1 * number2 ; //乘法公式

    cout << number1 << "+" << number2 << "=" << sum  << endl ;
    cout << number1 << "-" << number2 << "=" << minus << endl ;
    cout << number1 << "*" << number2 << "=" << multiple << endl ; //最後將計算結果排列出來

    return 0;
}
