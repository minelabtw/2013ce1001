#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main()
{
    int number1 = 0; //宣告型別為 整數(int) 的第一個數字，並初始化其數值為0。
    int number2 = 0; //宣告型別為 整數(int) 的第二個數字，並初始化其數值為0。
    cin >> number1 >> number2;  //利用鍵盤來輸入第一和第二個數字的數值。
    cout << number1 << "+" << number2 << "=" << number1+number2 << endl;
    //利用cout使螢幕輸出兩整數做加法的算式，並用endl使其換行。
    cout << number1 << "-" << number2 << "=" << number1-number2 << endl;
    //利用cout使螢幕輸出兩整數做減法的算式，並用endl使其換行。
    cout << number1 << "*" << number2 << "=" << number1*number2;
    //利用cout使螢幕輸出兩整數做乘法的算式。

    return 0;
}
