#include <iostream>
using namespace std;

int main()
{
    int number1,number2;                                                        //宣告兩個變數來接收接下來輸入的數字
    cin >> number1;                                                             //接收第一個變數並傳給number1
    cin >> number2;                                                             //接收第一個變數並傳給number2

    cout << number1 << "+" << number2 << "=" << number1+number2 << "\n";        //"number1+number2="輸出到螢幕上並計算number1+number2並輸出到螢幕上
    cout << number1 << "-" << number2 << "=" << number1-number2 << "\n";        //"number1-number2="輸出到螢幕上並計算number1-number2並輸出到螢幕上
    cout << number1 << "*" << number2 << "=" << number1*number2;                //"number1*number2="輸出到螢幕上並計算number1*number2並輸出到螢幕上

    return 0;
}
