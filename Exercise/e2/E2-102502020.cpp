#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1 = 0;        //宣告名稱為number1的整數變數，並初始化其數值為0。
    int number2 = 0;        //宣告名稱為number2的整數變數，並初始化其數值為0。
    int Plus = 0;           //宣告名稱為Plus的整數變數，並初始化其數值為0。
    int Minus = 0;          //宣告名稱為Minus的整數變數，並初始化其數值為0。
    int Multiply = 0;       //宣告名稱為Multiply的整數變數，並初始化其數值為0。


    cin >> number1;         //從鍵盤輸入，並將輸入的值給number1。

    cin >> number2;         //從鍵盤輸入，並將輸入的值給number2。

    Plus = number1 + number2;          //將兩個整數相加，並將相加後的值丟給Plus。

    cout <<number1<<"+"<<number2<<"="<<Plus<<endl;

    Minus = number1 - number2;         //將兩個整數相減，並將相減後的值丟給Minus。
    cout <<number1<<"-"<<number2<<"="<<Minus<<endl;

    Multiply = number1 * number2;      //將兩個整數相乘，並將相乘後的值丟給Multiply。
    cout <<number1<<"*"<<number2<<"="<<Multiply;


    return 0;
}


