#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std ;

int main ()
{
    ofstream outfile("102502028.txt") ;   //宣告

    if (!outfile)                         //找不到檔案的情況
    {
        cerr << "File could not be opened" << endl ;
        exit(1) ;
    }

    for (int a = 0 ; a < 4 ; a++)         //印出聖誕樹
    {
        for (int b = 0 ; b < 3-a ; b++)
        {
                outfile << " " ;
        }
        for (int c = 0 ; c < 2*a+1 ; c++)
        {
            outfile << "*" ;
        }
        outfile << endl ;
    }
    outfile << "  ***" ;

    outfile.close() ;                      //結束outfile

    return 0 ;
}
