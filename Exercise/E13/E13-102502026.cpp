//E13-102502026
#include <iostream>     //for cout
#include <fstream>      //to create txt
#include <iomanip>      //to use setw
using namespace std;
int main()      //start program
{
    ofstream File ("102502026.txt",ios::out);   //create txt
    int s=15;   //use for setw
    int z=1;    //for counter doing the part 2 and 3 of the tree
    for(int a=0; a<3; a++)
    {
        for (int b=z; b<11; b++)    //doing the tree
        {
            cout<<setw(s);         //print on program
            File<<setw(s);        //print on txt
            for (int c=1; c<(2*b); c++) //put how many * with the condition
            {
                cout<<"*";          //print on program
                File<<"*";        //print on txt
            }
            s--;
            cout<<endl;             //print on program
            File<<endl;           //print on txt
        }
        s=11; //for the las 5 part of the first one so is (20-4)=11 lines
        z=5;   //return z for the last 5 lines
    }

    for(int d=1; d<6; d++)  //doing the trunk
    {
        s=16;
        cout<< setw(s)<<"***"<<endl;        //print on program
        File << setw(s)<<"***"<<endl;       //print on txt
    }
    return 0;
}   //end program
