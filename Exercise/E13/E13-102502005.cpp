#include<fstream>
#include<iostream>
#include<cstdlib>
using namespace std;

int main()
{
    ofstream outClientFile("Christmastree.txt",ios::out);   //開啟檔案。

    if(!outClientFile)                                      //若無法開啟檔案，則印出錯誤訊息。
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }

    for(int i=8; i>=1; i--)                                 //利用兩個for迴圈印出聖誕樹的上半三角形。
    {
        for(int j=1; j<=i-1; j++)
        {
            outClientFile << " ";
        }
        for(int k=0; k<=16-i*2; k++)
        {
            outClientFile << "*" ;
        }
        outClientFile << endl;
    }
    outClientFile << " ************* ";                     //最後一行手動輸入。

    return 0;
}
