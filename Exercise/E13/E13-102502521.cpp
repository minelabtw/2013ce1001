#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

int main()
{
    ofstream pic("102502521.txt",ios::out);    //ofstream constructor opens file
    if(!pic)    //如果不能開啟檔案就跳出程式
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }

    pic<<"     *     "<<endl<<"    ***    "<<endl<<"   *****   "<<endl<<"  *******  "<<endl<<" ********* "<<endl<<"***********"<<endl<<" ********* ";    //輸入

    return 0;
}
