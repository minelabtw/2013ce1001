#include <iostream>
#include <cstdlib>
#include <fstream>
#include <ctime>
using namespace std;

int main()
{
    //ofstream consturctor opens file
    ofstream xMasTree("102502032.txt", ios::out);
    if (!xMasTree)
    {
        cerr << "Couldn't open file";
        exit(1);
    }

    //define the tree's size
    srand(time(NULL));
    int treeSize = rand() % 20 + 4;

    //data array (temporary)
    char tree[treeSize * 2 + 1][treeSize + 4];
    for (int i = 0; i < treeSize * 2 + 1; i ++)     //initialize data array
        for (int j = 0; j < treeSize + 4; j ++)
            tree[i][j] = ' ';

    //draw thetree's crown
    for (int j = 0; j < treeSize + 1; j ++)
        for (int i = 0; i < treeSize * 2 + 1; i ++)
            if (i >= treeSize - j and i <= treeSize + j)
                tree[i][j] = '*';

    //draw the middle part
    for (int i = 0; i < treeSize * 2 + 1; i ++)
        if (i > 0 and i < treeSize * 2 )
            tree[i][treeSize + 1] = '*';

    //draw the tree trunk
    for (int j = treeSize + 2; j <= treeSize + 3; j ++)
    {
        tree[treeSize][j] = '*';
        tree[treeSize - 1][j] = '*';
        tree[treeSize + 1][j] = '*';
    }

    //output to file
    for (int j = 0; j < treeSize + 4; j ++)
        for (int i = 0; i < treeSize * 2 + 1; i ++)
        {
            xMasTree << tree[i][j];
            if (i == treeSize * 2)
                xMasTree << endl;
        }
    xMasTree.close();

    return 0;
}
