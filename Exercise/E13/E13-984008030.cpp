#include <iostream>
#include <fstream>
using namespace std;
const int TREE_SIZE = 10;
int main(){
    ofstream fout("984008030.txt");
    if(!fout){
        cout << "Cannot open 984008031.txt. Exit." << endl;
        return 1;
    }
    //樹的TREE_SIZE-1是樹的上半身 剩下的是樹幹部分
    for(int i = 1; i <= TREE_SIZE - 1; i++){//印出樹上半部
        for(int blank = 0; blank < TREE_SIZE - 1 -i; blank++){//印出
            //樹葉最大為(TREE_SIZE - 1)*2 -1=2*TREE_SIZE - 3，其中心點恰好為(2*TREE_SIZE - 3 + 1)/2 = TREE_SIZE - 1
            //但真正的空格數還要扣除中心點+左邊樹葉的數量 也就是i
            fout << " ";
        }
        for(int leaf = 0; leaf < 2 * i - 1; leaf++){//印出樹葉
            fout << "*";
        }
        fout << endl;
    }
    fout << " ";
    for(int trunk = 0; trunk < 2*TREE_SIZE - 5; trunk++){//印出樹幹
        fout << "*";
    }
    fout.close();
    return 0;
}
