#include<iostream>
#include<fstream>
#include<iomanip>
using namespace std;

int main()
{
    ofstream outFile("100502043.txt");  //開啟檔案以供輸出

    //輸出檔案內容
    outFile << setw(5) << "*" << endl
            << setw(6) << "***" << endl
            << setw(7) << "*****" << endl
            << setw(8) << "*******" << endl
            << setw(9) << "*********" << endl
            << setw(6) << "***";

    return 0;
}
