#include <iostream>
#include <fstream>
using namespace std;

int main()
{

    ofstream tree("tree.txt",ios::out); // 建立檔案

    if(!tree) // 開啟失敗
    {
        cout << "Error!" << endl;
        return 1;
    }

    tree << "             ☆                 " << endl
         << "           / | \\              " << endl
         << "          //   ★\\             " << endl
         << "         / ★ | / \\            " << endl
         << "         ＄  \ ＊ \\           " << endl
         << "        /^ / ♠ ^^\\ \\         " << endl
         << "        ＊。－。－~＊          " << endl
         << "       ^/ ★ ^  \\ ^  |♠        " << endl
         << "      /＊ | / ＄ |★| ^\        " << endl
         << "    ＊~^^^。－。－^~~~＊       " << endl
         << "      / / / ▇  \\ \\ \\\\     " << endl
         << "           ▇ ▋               " << endl
         << "           █▉▋               " << endl ;

         tree << endl << "我是一顆 聖誕樹 噢!!≧▽≦y" << endl;
         return 0;
     }
