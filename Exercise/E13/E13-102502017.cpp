#include<fstream>

using namespace std;

int main(void){

    fstream file("102502017.txt",ios::out);
    //宣告一個fstream物件，名稱為file；對檔案 102502017.txt 執行寫入(ios::out) 動作

    file << "/*font:MS Gothic*/" << endl;       //此排版係(?)使用MS Gothic字型
    file << endl;
    file << "*   * *   *  **   ***  *** **   *** ***  ***" << endl;
    file << " * *  ** ** *  * *      *  * *  *   *   *   " << endl;
    file << "  *   * * * ****  **    *  **   *** ***  ** " << endl;
    file << " * *  *   * *  *    *   *  * *  *   *      *" << endl;
    file << "*   * *   * *  * ***    *  *  * *** *** *** " << endl;
    file << endl;
    file << "  *     *     *     *     *     *     *     *  " << endl;
    file << " ***   ***   ***   ***   ***   ***   ***   *** " << endl;
    file << "***** ***** ***** ***** ***** ***** ***** *****" << endl;
    file << "  *     *     *     *     *     *     *     *  " << endl;
    file << "  *     *     *     *     *     *     *     *  " << endl;

    file.close();                               //關閉檔案
    return 0;
}
