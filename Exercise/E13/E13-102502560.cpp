#include <iostream>
#include <fstream>
using namespace std;

void treelayer(int wid,int maxw);

ofstream OUT("102502560.txt");					//open file to write

int main()
{
	int maxwidth=30;
	int tree[]={1,5,9,13,7,11,15,19,13,17,21,25,5,5,5};		//width of each tree layer

	for(int i=0;i<sizeof(tree)/sizeof(int);i++)treelayer(tree[i],maxwidth);

	OUT.close();								//close file

	return 0;
}

void treelayer(int wid,int maxw)
{
	for(int i=1;i<=(maxw-wid)/2;i++)OUT << " ";		//blank on the left
	for(int i=1;i<=wid;i++)OUT << "*";				//layer of tree
	OUT << endl;
}
