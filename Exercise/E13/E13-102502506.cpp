#include <fstream>
#include <iostream>
using namespace std;
int main()
{
    int j = 1;
    ofstream file("102502506.txt",ios::out);  //以輸出方式開檔案 102502506.txt
    for (int i = 7; i >= 0; i--)  //畫樹上8行
    {
        for (int k = i ; k >= 1; k--)
        {
            file << " ";   //把空白丟入檔案
        }
        for (int l = 2 * j - 1; l >= 1; l--)
        {
            file << "*";  //把*丟入檔案
        }
        file << endl;  //檔案裡換行
        j++;
    }
    file << " *************";  //把最後一排丟入檔案
    file.close();  //關閉檔案
    return 0;
}
