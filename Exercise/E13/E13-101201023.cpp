#include<iostream>
#include<fstream>

using namespace std;

int main()
{
    ofstream tree("101201023.txt");                     //宣告一個檔，檔名為101201023的txt
    int x=-4;
    int y=3;

    while(y>=-3)
    {
        while(x<=4)
        {
            if(x==0)
            {
                tree << "*";
            }

            else if(x==1 && -1<=y && y<=2)
            {
                tree << "*";
            }

            else if(x==2 && -1<=y && y<=1)
            {
                tree << "*";
            }

            else if(x==3 && y==-1)
            {
                tree << "*";
            }

            else if(x==3 && y==1)
            {
                tree << "*";
            }

            else if(x==4 && y==-1)
            {
                tree << "*";
            }

            else if(x==-1 && -1<=y && y<=2)
            {
                tree << "*";
            }

            else if(x==-2 && -1<=y && y<=1)
            {
                tree << "*";
            }

            else if(x==-3 && y==-1)
            {
                tree << "*";
            }

            else if(x==-3 && y==1)
            {
                tree << "*";
            }

            else if(x==-4 && y==-1)
            {
                tree << "*";
            }

            else
                tree << " ";
            x++;
        }
        tree << endl;
        x=-4;
        y--;
    }

    tree.close();                                          //結束檔案
    return 0;
}
