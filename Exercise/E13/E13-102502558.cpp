#include <iostream>
#include <fstream>

using namespace std;

const int _size = 30;
const char foreground = '*';
const char background = ' ';

fstream fout;
void init()
{
    fout.open("102502558.txt", ios::out);
}

void cleanup()
{
    fout.close();
}

void print_tree(int n, int margin,int del)
{
    for (int i=1+del;i<=n;i++)
    {
        for (int j=1;j<=n-i+margin;j++)
            fout << background;
        for (int j=1;j<i*2;j++)
            fout << foreground;
        for (int j=1;j<=n-i+margin;j++)
            fout << background;
        fout << endl;
    }
}

void print_root(int x,int y,int margin)
{
    for (int i=0;i<y;i++)
    {
        for (int j=0;j<margin;j++)
            fout << background;
        for (int j=0;j<x;j++)
            fout << foreground;
        for (int j=0;j<margin;j++)
            fout << background;
        fout << endl;
    }
}

int main()
{
    init();

    for (int i=1;i<_size;i++)
        print_tree(i*2,(_size - i)*2,i-1);
    print_root(_size,_size/2,_size+_size/2);

    cleanup();
    return 0;
}
