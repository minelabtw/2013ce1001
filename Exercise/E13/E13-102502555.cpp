#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

int main(){
    ofstream outdata("102502555.txt" , ios::out);  //創一個檔案

    if(!outdata){  //如果不能開檔案就離開
        cerr << "File could not be opened" << endl;
        exit(1);
    }

    for(int i = 1 ; i <= 5 ; i++){  //列印樹
        for(int j = 5 - i ; j > 0 ; j--){
            outdata << " ";
        }
        for(int j = 2 * i - 1 ; j > 0 ; j--){
            outdata << "*";
        }

        outdata << endl;
    }
    for(int i = 0 ; i < 2 ; i++){  //列印樹幹
        for(int j = 3 ; j > 0 ; j--){
            outdata << " ";
        }
        for(int j = 3 ; j > 0 ; j--){
            outdata << "*";
        }
        outdata << endl;
    }

    outdata.close();  //關檔案

    return 0;
}
