#include <string>
#include <fstream>
#include <stdlib.h>  //使用exit須載入stdlib標頭檔
using namespace std;

int main()                      //主程式開始
{
    fstream file;
    char *str[8] = {"    *"," 　****","  ******"," *********","   *****"," *********","     **","     **"};  //宣告字串指標陣列
    file.open("102502002.txt", ios::out);      //開啟檔案

    for(int i = 0; i < 8; i++)
    {
        file << str[i] << endl;
    }      //將資料輸出至檔案
    return 0;
}      //主程式結束
