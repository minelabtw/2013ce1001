#include<iostream>
#include<fstream>
#include<cstdlib>
using namespace std ;

int main ()
{
    ofstream outClientFile("101201005.txt", ios::out) ;
    if (!outClientFile)
    {
        cerr << "File could not be opened" << endl ;
        exit(1) ;
    }

    outClientFile << "      *      " <<endl;
    outClientFile << "     ***     " <<endl;
    outClientFile << "    *****    " <<endl;
    outClientFile << "   *******   " <<endl;
    outClientFile << "  *********  " <<endl;
    outClientFile << " *********** " <<endl;
    outClientFile << "    ****     ";

    return 0 ;
}
