#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

int main ()
{
    fstream file; //宣告 fstream 物件(file)
    file.open( "102502556.txt" , ios::out ); //開啟檔案(102502556.txt)為輸出狀態
    if ( !file ) //檢查檔案能否開啟，否則回傳錯誤訊息並結束。
    {
        cerr << "Can't open file!" << endl;
        exit(1);
    }
    int space = 7; //宣告型別為 int 的變數(space)，用來計算前方空白大小。
    int star = 1; //宣告型別為 int 的變數(star)，用來計算星星數量。
    while ( space >= 0 ) //輸出聖誕樹
    {
        for ( int i = 0 ; i < space ; i++ )
        {
            file << " ";
        }
        for ( int j = 0 ; j < 2 * star - 1 ; j++ )
        {
            file << "*";
        }
        file << endl;
        space--;
        star++;
    }
    space += 2;
    star -= 2;
    for ( int k = 0 ; k < space ; k++ )
    {
        file << " ";
    }
    for ( int l = 0 ; l < 2 * star - 1 ; l++ )
    {
        file << "*";
    }
    file.close(); //關閉檔案
    return 0;
}
