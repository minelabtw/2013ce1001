#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

int main()
{
    ofstream outfile ("102502511.txt", ios::out); //須先宣告

    int y = 0;
    while(y<10)
    {
        for(int space = 0; space < 10-y-1 ; space++)
        {
            outfile << " "; //將原先的out 都改成 outfile
        }
        for(int left1 = 0 ; left1 <= y ; left1++)
        {
            outfile << "*";
        }
        for(int right1 = 0 ; right1 <= y-1 ; right1++)
        {
            outfile << "*";
        }

        outfile << endl;

        y++;
    }

    outfile << " ";
    for(int left2 = 1 ; left2 < y ; left2++)
    {
        outfile << "*";
    }
    for(int right2 = 1 ; right2 < y-1 ; right2++)
    {
        outfile << "*";
    }
    return 0;
}
