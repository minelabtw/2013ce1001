#include<iostream>
#include<fstream>
using namespace std;

int main()
{
    fstream file;//創造一個檔案
    file.open("101201524.txt", ios::out);//開啟txt檔案狀態為寫入
    //寫入一個聖誕樹圖案
        file << "            *" << endl;
        file << "           ***" << endl;
        file << "          *****" << endl;
        file << "         *******" << endl;
        file << "          *****" << endl;
        file << "         *******" << endl;
        file << "        *********" << endl;
        file << "       ***********" << endl;
        file << "        *********" << endl;
        file << "       ***********" << endl;
        file << "      *************" << endl;
        file << "     ***************" << endl;
        file << "           | |" << endl;
        file << "           | |" << endl;
        file << "           |_|" << endl;

    file.close();//關閉檔案
    file.open("101201524.txt",ios::in);//開啟txt檔案狀態為讀取
    cout << file.rdbuf();//讀取檔案內容
    file.close();//關閉檔案

    return 0;
}
