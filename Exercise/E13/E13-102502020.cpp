#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ofstream outFile( "102502020.txt");

    for(int i=0; i<19; i++)
    {
        if(i<=4)
        {
            for(int j=1; j<=15; j++)
            {
                if(8-i<=j && j<=8+i)
                    outFile << "*";
                else
                    outFile << " ";
            }
            outFile << endl;
        }
        else if(i<=9)
        {
            for(int j=1; j<=15; j++)
            {
                if(8-(i-3)<=j && j<=8+(i-3))
                    outFile << "*";
                else
                    outFile << " ";
            }
            outFile << endl;
        }
        else if(i<=14)
        {
            for(int j=1; j<=15; j++)
            {
                if(8-(i-7)<=j && j<=8+(i-7))
                    outFile << "*";
                else
                    outFile << " ";
            }
            outFile << endl;
        }
        else if(i<19)
        {
            for(int j=1;j<=15;j++)
            {
                if(6<=j && j<=10)
                    outFile << "*";
                else
                    outFile << " ";
            }
            outFile << endl;
        }
    }
    return 0;
}
