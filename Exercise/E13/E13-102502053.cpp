#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

int main()
{
    ofstream textfile("102502053.txt"); //ofstream constructor open file
    //call variables
    int x=0;
    int a=1;
    int b=10;
    int y;
    while(x<10) //loop to display rows
    {
        y=0;
        textfile<<setw(b);
        while(y<a) //loop to display columns
        {
            textfile<<"*";
            y++;
        }
        textfile<<endl;
        a=a+2;
        x++;
        b--;
    }
    if(x==10) //display for the last line
    {
        textfile<<setw(b+1+1);
        x--;
        a=a-2-2;
        y=0;
        while(y<a)
        {
            textfile<<"*";
            y++;
        }
    }
    return 0;
}
