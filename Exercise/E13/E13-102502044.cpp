/*************************************************************************
    > File Name: E13-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年12月25日 (週三) 15時11分00秒
 ************************************************************************/

#include <stdio.h>

#define lh 5
#define lw 12
#define ll 3
#define wh 4
#define ww 3

FILE *fout;

void put_leaf(int d, int w)
{
	while(d--)
		fprintf(fout, " ");
	fprintf(fout, "◢");

	for(int k=0 ; k<w*2 ; k++)
		fprintf(fout, "█");

	fprintf(fout, "◣");
	fprintf(fout, "\n");

}

void put_wood(int d, int w)
{
	while(d--)
		fprintf(fout," ");

	for(int k=0 ; k<w*2 ; k++)
		fprintf(fout, "█");

	fprintf(fout, "\n");
}

int main()
{
	fout = fopen("102502044.txt", "w+");


	for(int i=0 ; i<lw ; i++)
		fprintf(fout, " ");
	fprintf(fout, "★\n");

	for(int i=ll-1 ; i>=0 ; i--)
	{
		if(i == ll-1)
			for(int j=0 ; j<lw-lh-i*2 ; j++)
				put_leaf(lw-j-1, j);

		for(int j=lw-lh ; j<lw ; j++)
			put_leaf(lw-j-1+i*2, j-i*2);
	}

	for(int i=0 ; i<wh ; i++)
		put_wood(lw-ww, ww);

	fclose(fout);

	return 0;
}

