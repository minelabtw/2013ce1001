#include<iostream>
#include<fstream>
#include<cstdlib>
#include<iomanip>
using namespace std;
int main()
{
    int j=10;
    ofstream christmastree("christmastree.txt",ios::out);//輸出 christmastree.txt
    if(!christmastree)//如果無法開啟christmastree 結束
    {
        exit(1);
    }
    for(int i=0; i<10; i++)
    {
        christmastree << setw(2*j);
        j--;
        for(int k=0; k<2*i+1; k++)
        {
            christmastree << "*";
        }
        christmastree << endl;

    }
    christmastree << setw(12);
    for(int l=0; l<7; l++)
    {
        christmastree << "*";
    }
    return 0;
}
