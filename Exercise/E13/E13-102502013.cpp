#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main()
{
    ofstream outclientfile("102502013.txt",ios::out);
    if(!outclientfile)
    {
        cerr << "file could not be opened" << endl;
        exit(1);
    }
    int x=4, n=0, m=0;
    for (int j=0; j<6; j++)
    {
        for (int i=0; i<9; i++)
        {
            n=x-j;
            m=x+j;
            if (j==5&&i>=1&&i<=7)
                outclientfile << "*";
            else if (j<5&&i>=n&&i<=m)
                outclientfile << "*";
            else
                outclientfile << " ";
        }
        outclientfile << endl;
    }
    return 0;
}
