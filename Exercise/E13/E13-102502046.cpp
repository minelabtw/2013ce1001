#include<fstream>
using namespace std;

int main()
{
    ofstream outfile("102502046.txt"); /*宣告輸出檔名*/

    outfile << endl ;
    for (int n=0 ; n<5 ; n++)       //印出樹葉部分
    {
        for(int i=0 ; i<2*n+4 ; i++)
        {
            for(int j=50-i-3*n ; j>0 ; j--)
                outfile << " " ;
            for(int j=2*i-1+6*n ; j>0 ; j--)
                outfile << "*";
            outfile << endl ;
        }
    }
    for (int i=9 ; i>0 ; i--)       //印出樹幹部分
    {
        for (int j=47 ; j>0 ; j--)
            outfile << " ";
        for (int j=5 ; j>0 ; j--)
            outfile << "*";
        outfile << endl;
    }
    for (int i=1 ; i<=6 ; i++)      //印出盆栽部分
    {
        for (int j=44 ; j>0 ; j--)
            outfile << " ";
        for (int j=11 ; j>0 ; j--)
            outfile << "*";
        outfile << endl;
    }
    for (int n=2 ; n>0 ; n--)       //輸出草地部分
    {
        for (int i=101 ; i>0 ; i--)
            outfile << "*";
        outfile << endl;
    }
}
