#include <iostream>
#include <fstream>      //fstream的層下指令
using namespace std;
int main()
{
    ofstream myfile;        //創一個file
    myfile.open ("102502025.txt");      //開啟並命名txt
    myfile  << "         *\n"       //輸入
            << "        ***\n"
            << "       *****\n"
            << "      *******\n"
            << "        ***\n"
            << "       *****\n"
            << "      *******\n"
            << "     *********\n"
            << "       *****\n"
            << "      *******\n"
            << "     *********\n"
            << "    ***********\n"
            << "        ***\n"
            << "        ***\n"
            << "        ***\n"
            << "        ***\n";
    myfile.close();     //關閉
    return 0;
}
