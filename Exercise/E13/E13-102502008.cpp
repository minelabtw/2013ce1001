#include <iostream>
#include <cstdlib>
#include <fstream>
#define Max 10
using namespace std ;
int main()
{
    ofstream txt1("102502008.txt",ios::out) ; //build the file
    if(txt1.is_open()) //the file is open
    {
        for(int i=1 ; i<=Max ; i++) //行
        {
            for(int j=1 ; j<=Max+i-1 ; j++) //列
            {
                if(j<=Max-i)            //空白
                    txt1 << " " ;
                else                    //*
                    txt1 << "*" ;

            }
            txt1 << endl ;
            if(i==Max)                  //最後一行
            {
                txt1 << " " ;
                for(int j=1 ; j<=Max+i-1 ; j++)
                    if(j>Max-i+2)
                        txt1 << "*" ;
            }
        }
    }
    txt1.close();
    return 0 ;
}
