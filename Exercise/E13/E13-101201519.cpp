#include<iostream>
#include<iomanip>
#include<fstream>
using namespace std;

void tree(ofstream&, int);

int main()
{
    int width=19;
    ofstream xmastree("101201519.txt",ios::out);
    tree(xmastree,width);
    xmastree.close();
    return 0;
}
void tree(ofstream& xmastree, int w)
{
    //由於此聖誕樹為鋸齒狀,共有3片
    int cy=w/6, check=0;//cy為分割長度, check為檢查目前y軸迴朔幾次
    for(int y=w; y>=0; y--)//y軸從上而下為w到0
    {
        if(check==0 && y==w-4*cy)//第一次迴朔
            y=y+2*cy,check++;
        else if(check==1 && y==w-5*cy)//第二次迴朔
            y=y+2*cy, check++;

        for(int x=-w; x<=w; x++)//x軸從左而右為-w到w
        {
            if((y<-x+w && y<x+w)|| y==-x+w || y==x+w)//以2個線性方程式判定聖誕樹形狀
                xmastree << "*";
            else
                xmastree << " ";
        }
        xmastree << endl;
    }
    for(int i=1; i<=w/5; i++)//輸出樹幹
    {
        xmastree << setw(w/5) << " ";//調整樹幹與樹葉銜接位置
        for(int j=0; j<=2*w-2*(w/5); j++)
            xmastree << "*";
        xmastree << setw(w/5) << " "<< endl;
    }
}
