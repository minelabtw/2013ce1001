#include<iostream>
#include<fstream>
int main()
{
   std::fstream fs("102502530.txt",std::fstream::out);   //declaration

   fs<<"                       *"<<std::endl<<  //output
      "                 *    * *    *"<<std::endl<<
      "                * *  * * *  * *"<<std::endl<<
      "               *   **     **   *"<<std::endl<<
      "                *      *      *"<<std::endl<<
      "                 *    * *    *"<<std::endl<<
      "            *    *  **   **  *    *"<<std::endl<<
      "           * *  * **  ***  ** *  * *"<<std::endl<<
      "          *   ** *           * **   *"<<std::endl<<
      "           *        * * * *        *"<<std::endl<<
      "            *  *   *  * *  *   *  *"<<std::endl<<
      "       *    * *   *    *    *   * *    *"<<std::endl<<
      "      * *  *   * *  *     *  * *   *  * *"<<std::endl<<
      "     *   **  *  *  *  * *  *  *  *  **   *"<<std::endl<<
      "      *     *     *  *   *  *     *     *"<<std::endl<<
      "       *   *    * * * * * * * *    *   *"<<std::endl<<
      "  *    *  *   **   *  * *  *   **   *  *    *"<<std::endl<<
      " * *  *  *  **         *         **  *  *  * *"<<std::endl<<
      "*   **    **  *   * *     * *   *  **    **   *"<<std::endl<<
      " *     *  *   *  *  *  *  *  *  *   *  *     *"<<std::endl<<
      "  *** * **   * **  * ***** *  ** *   ** * ***"<<std::endl<<
      "     ***     **    ** *** **    **     ***"<<std::endl<<
      "                      ***";

   fs.close();    //close filestream

   return 0;   //exit
}
