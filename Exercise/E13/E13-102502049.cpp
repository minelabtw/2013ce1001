#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    ofstream tree("102502049.txt",ios::out); //建立tree.txt文件

    int j=7; //空白(迴圈)
    int jp=j; //空白每次遞減1
    int star=1;
    for (int i=0; i<8; i++)
    {
        j=jp; //每次減一

        while ( j>0 ) //輸出空白
        {
            tree << " ";
            j--;
        }

        int k=0; //輸出星星
        while ( k<star )
        {
            tree << "*";
            k++;
        }
        tree << endl;

        jp--; //換行空白減一
        star += 2; //換行星星+2
    }

    tree << " *************"; //最後一列書出

    tree.close(); //關閉檔案

    return 0;
}
