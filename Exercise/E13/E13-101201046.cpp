#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(void) {
    ofstream out("101201046.txt"); //open file

    out << string(12, ' ') <<   "A"   << endl // output star
        << string(10, ' ') << "<=+=>" << endl
        << string(11, ' ') <<  "\\^/" << endl;

    for (int j = 0; j < 3; j++)
        for (int i = j*2 + 1; i < (6 + j - !j); i++)
            out << string(12 - i, ' ') << string( i*2 + 1, '*') << endl;

    for (int i = 0; i < 3; i++)
        out << string(11, ' ') << "***" << endl;

    out.close(); //close file
    return 0;
    }
