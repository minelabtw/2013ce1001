#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	ofstream fout("100201023.txt");

	for(int i = 0 ; i < 10 ; ++i)
	{
		for(int j = 0 ; j < 2 * (9 - i) ; ++j) // output left side space
			fout << " ";

		for(int j = 0 ; j < i * 2 + 1 ; ++j) // output tree
			fout << "*";

		fout << endl;
	}
	
	for(int i = 0 ; i < 5 ; ++i) // output tree trunk
	{
		for(int j = 0 ; j < 18 ; ++j)
			fout << " ";
		fout << "*" << endl;
	}

	return 0;
}
