#include <iostream>
#include <fstream>
using namespace std;
int main()
{
    fstream file;//建立檔案輸入，輸出物件
    file.open("102502007.txt",ios::out);//輸出
    int i,j,k,l,line=20;//樹上半部高
    int bottomhalf=line/3+1;//樹下半部行數為上半部的(1/3)+1行
    int a=line-bottomhalf;
    //下半部由左至右的最短間距
    //兩者相減，即為下半部的最短間距
    for(i=1; i<=line; i++)//上半部行數
    {
        {
            for(j=line; j>i; j--)
                file << " ";//由上往下，間距越來越少
            for(k=1; k<=2*i-1; k+=1)
                file << "*";//第k行有2k-1個星號
        }
        file << endl;//換行
    }
    for(l=1; l<=bottomhalf; l++)//下半部行數
    {
        {
            for(int x=1 ; x<=a; x++)//先製造出最短間距
                file << " " ;
            for(int y=1 ; y < l; y++)
                file << " " ;//由上往下，間距越來越大
            for(int z=1 ; z<=2*(bottomhalf-l+1)-1; z++)
                file <<"*" ;
            //由上往下，l越大，星號越來越少
            //l=bottomhalf時，即為最後一行
        }
        file << endl;//換行
    }
    file.close();//關閉檔案
    return 0;
}
