#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

void repeat(ofstream&, int);//重複輸出聖誕樹功能
void printtree(ofstream&, int);//輸出單一聖誕樹

int main()
{

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~試用額外功能請將ad改成1~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    int ad=0, w=18;// ad看是否用額外功能, w為聖誕樹寬度
    ofstream tree("101201521.txt", ios::out);
    if(ad==0)
        printtree(tree, w);
    else if(ad==1)
        repeat(tree, w);
    tree.close();//關閉tree
    return 0;
}
void repeat(ofstream& tree, int w)
{
    while((cout << "Enter the width of the tree you want(>=13):")//實際寬度可能少1
        && (cin >> w))
    {
        w=(w-1)/2;//將w改為聖誕樹寬度的一半
        if(w<6)
        {
            cout << "Out of range!" << endl;
            continue;
        }
        printtree(tree, w);
    }
}
void printtree(ofstream& tree, int w)
{
    //由於此聖誕樹為鋸齒狀,共有3片
    int cy=w/6, check=0;//cy為分割長度, check為檢查目前y軸迴朔幾次
    for(int y=w; y>=0; y--)//y軸從上而下為w到0
    {
        if(check==0 && y==w-4*cy)//第一次迴朔
            y=y+2*cy,check++;
        else if(check==1 && y==w-5*cy)//第二次迴朔
            y=y+2*cy, check++;

        for(int x=-w; x<=w; x++)//x軸從左而右為-w到w
        {
            if((y<-x+w && y<x+w)|| y==-x+w || y==x+w)//以2個線性方程式判定聖誕樹形狀
                tree << "*";
            else
                tree << " ";
        }
        tree << endl;
    }
    for(int i=1; i<=w/5; i++)//輸出樹幹
    {
        tree << setw(w/5) << " ";//調整樹幹與樹葉銜接位置
        for(int j=0; j<=2*w-2*(w/5); j++)
            tree << "*";
        tree << setw(w/5) << " "<< endl;
    }
}
