#include <iostream>
#include <fstream>

using namespace std ;

int main ()
{

// 設定輸出檔案
    ofstream fout ("992001026.txt") ;

// 在檔案裡印入以下內容
    fout << "     *     " << endl ;
    fout << "    ***    " << endl ;
    fout << "   *****   " << endl ;
    fout << "  *******  " << endl ;
    fout << " ********* " << endl ;
    fout << "     *     " << endl ;
    fout << "     *     " << endl ;

// 釋放檔案資源
    fout.close();

    return 0 ;
}

