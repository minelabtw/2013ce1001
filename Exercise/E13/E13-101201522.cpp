#include <iostream>
#include <fstream>
using namespace std;

string printspace(int _size){//輸出_size個空白
    int i;
    string s="";
    for(i=0;i<_size;i++)
        s += " ";
    return s;
}

string printstar(int _size){//輸出_size個*
    int i;
    string s="";
    for(i=0;i<_size;i++)
        s += "*";
    return s;
}

void star(int _size,fstream &file){//印出樹上的星星
    file << printspace(_size) << printstar(1) << endl
         << printspace(_size-2) << printstar(1) << printspace(3) << printstar(1) << endl
         << printspace(_size-1) << printstar(1) << printspace(1) << printstar(1) << endl;
}

void tree(int n,fstream &file){//印出樹
    int i,j,k=1,_size = (1+n)*n/2+2;//i,j為迴圈用變數,k為樹寬
    for(i=0;i<n;i++){
        for(j=0;j<3+i;j++){
            file << printspace(_size-k) << printstar(2*k+1) << endl;
            k++;
        }
        k -= 2;
    }
    for(i=0;i<((_size-2)+2*n)/5;i++){
        file << printspace(_size-k/8) << printstar(k/8*2+1) << endl;
    }
}

int main(){
    int n=5,i,_size;//n為樹葉的層數預設為5,i為迴圈用變數,_size為樹寬度
    fstream file;
    file.open("101201522.txt",ios::out);//開檔
    _size = (1+n)*n/2+2;
    star(_size,file);//印出星星
    tree(n,file);//印出樹
    file.close();//關檔
    return 0;
}
