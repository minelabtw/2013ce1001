#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
void function0(int &num, int &Max ,int &Min, int &randNum);//函式
int main()
{
    srand(time(0));//設定變數
    int randNum = (rand() % 101);
    int num;
    int Max=0;
    int Min=100;
    do
    {
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>num;
        if (num<0||num>100)//檢驗輸入之數
        {
            cout<<"Out of range!\n";
        }
    }
    while (num<0||num>100);

    while(num!=randNum)//沒猜對進入while
    {
        function0(num, Max, Min,randNum);//呼叫函式
        if (Max==Min)//輸了，結束wile
        {
            cout<<"You lose! Answer is "<<randNum;
            break;
        }
        if (num!=randNum)//更改範圍
        {
            cout<<"Enter number("<<Max<<"<=number<="<<Min<<")?: ";
            cin>>num;
        }

        if (num<Max||num>Min)//輸出超出範圍
        {
            cout<<"Out of range!\n";
        }


    }
    if (num==randNum)//猜對，獲勝
    {
        cout<<"You win!";
    }
    return 0;
}

void function0(int &num, int &Max,int &Min,int &randNum)//函式
{
    if(num > Min)//輸入超出範圍，不做任何事
    {

    }
    else if (num>randNum)//輸入在範圍內，更改Min
    {
        Min = num - 1;
    }
    if(num < Max)//輸入超出範圍，不做任何事
    {

    }
    else if (num<randNum)//輸入在範圍內，更改Max
    {
        Max = num + 1;
    }
}

