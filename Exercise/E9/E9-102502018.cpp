#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;

void function0(int &a,int &b,int &c,int &number);

int main()
{
    int a=-1,b=0,c=100;
    int number;
    srand(time(0));
    number=rand()%101;
    while(a<0||a>100)                                     //判斷a是否符合範圍
    {
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>a;
        if(a<0||a>100.)
        {
            cout<<"Out of range!\n";
        }
    }
    while(a!=number)
    {
        function0(a,b,c,number);                          //呼叫函示
        if(b==c)                                          //若a剩最後兩個數猜錯時則輸
        {
            cout<<"You lose! Answer is "<<number;
            break;
        }
        cout<<"Enter number("<<b<<"<=number<="<<c<<"?: ";
        cin>>a;
        if(a<b||a>c)                                      //判斷a符不符合當前範圍
        {
            cout<<"Out of range!\n";
        }
        if(a==number)
        {
            cout<<"You win!";
            break;
        }
    }
    return 0;
}

void function0(int &a,int &b,int &c,int &number)
{
    if(a<b)                                   //會先跳過前面兩個if，先修改b與c的數值，之後判斷a是否超出b與c的範圍
    {                                         //如果a不再b與c範圍內則不輸出，若在範圍內則在修改範圍數值

    }
    else if(a>c)
    {

    }
    else if(a<=number)
    {
        b=a+1;
    }
    else if(a>=number)
    {
        c=a-1;
    }
}
