#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));  //隨機變數
    int number=rand()%101; //設定一個亂碼
    int guess=0;  //遊戲者所輸入的數
    int Max=100; //規範範圍
    int Min=0;

    while(guess!=number)
    {

        if(guess<Min||guess>Max)  //輸入數字不能超出範圍
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>guess;
        }

        else if(guess<number)  //改變下限
        {
            Min=guess+1;
            cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>guess;
        }
        else if(guess>number)  //改變上限
        {
            Max=guess-1;
            cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>guess;
        }


    }

    if(Max==Min)  //只剩一個數可猜
        cout<<"You lose! Answer is "<<number<<endl;
    else
        cout<<"You win!"<<endl;


    return 0;
}
