#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main ()
{
    srand ( time( 0 ) );

    int randNum = rand()%101;
    int number = 0;
    int Number = 0;
    int MAX = 100;
    int MIN = 0;

    cout << "Enter number(0<=number<=100)?: ";
    cin >> number;
    while ( number != randNum )
    {
        if ( number > MAX || number < MIN )
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?: ";
            cin >> number;
        }
        else if ( number > randNum )
        {
            MAX = number - 1;
            if ( MAX == MIN )
            {
                cout << "You lose! Answer is " << randNum;
                break;
            }
            cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?: ";
            cin >> number;
        }
        else if ( number < randNum )
        {
            MIN = number + 1;
            if ( MAX == MIN )
            {
                cout << "You lose! Answer is " << randNum;
                break;
            }
            cout << "Enter number(" << MIN << "<=number<=" << MAX << ")?:";
            cin >> number;
        }
    }
    if ( number == randNum )
    {
        cout << "You win!";
    }
    return 0;
}

