#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std ;
int main()//本次作業目的在於運用亂數的產生和迴圈來進行簡單的猜數字活動
{
    int guess=-1 ;
    int target=0 ;
    int TB=100 ;
    int TS=0   ;
    srand (time(NULL));
    target=rand()%101 ;


    while(guess<TS || guess>TB)   //利用迴圈來防止使用者的輸入超出提示範圍
    {
        cout<<"Enter number(0<=number<=100)?: " ;
        cin>>guess ;
        if(guess<TS || guess>TB)
        {
            cout<<"Out of range!"<<endl ;
        }

    }

    while(guess!=target )//利用迴圈來進行提示範圍的變換
    {
        if(guess>target)
        {
            TB=guess-1 ;
            if(TB==TS)
            {
                cout<<"You lose!Answer is "<<target<<endl ;
                cout<<endl ;
                return 0;
            }
            cout<<"Enter number("<<TS<<"<=number<="<<TB<<")?: "  ;
            cin>>guess ;
            while(guess>TB ||guess<TS)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"Enter number("<<TS<<"<=number<="<<TB<<")?: "  ;
                cin>>guess ;
            }

        }

        if(guess<target)
        {
            TS=guess+1 ;
            if(TB==TS)
            {
                cout<<"You lose!Answer is "<<target<<endl ;
                cout<<endl ;
                return 0;
            }
            cout<<"Enter number("<<TS<<"<=number<="<<TB<<")?: " ;
            cin>>guess ;
            while(guess>TB||guess<TS)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"Enter number("<<TS<<"<=number<="<<TB<<")?: " ;
                cin>>guess ;
            }


        }
    }


    if(guess==target)
    {
        cout<<"You win!" ;
    }

    return 0 ;





}
