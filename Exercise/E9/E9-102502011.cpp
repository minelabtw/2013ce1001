#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int number = 0 , MaxNum = 100 , MinNum = 0 ; //宣告最大值=100,最小值=0

    srand( time(0) ) ;
    int randNum = rand() % 101 ; //隨機亂數,讓亂數出現在0~100之間

    do
    {
        do
        {
            cout << "Enter number(" << MinNum << "<=number<=" << MaxNum << ")?: " ;
            cin >> number ;
            if ( number < MinNum || number > MaxNum )
                cout << "Out of range!" << endl ;
        }
        while ( number < MinNum || number > MaxNum ) ;

        if ( number < randNum )//當輸入小於答案的情況
            MinNum = number +1 ;

        else if ( number > randNum ) //當輸入大於答案的情況
            MaxNum = number -1 ;

         if ( MinNum == MaxNum )
        {
            cout << "You lose! Answer is " << randNum ; //輸的結果
            break ;
        }

        else if ( number == randNum ) //贏的結果
            cout << "You win!" ;
    }
    while ( number != randNum ) ;



    return 0;
}
