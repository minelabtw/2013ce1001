#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int answer = 0;
    int number = 0;
    int maximum = 100;
    int minium = 0;

    srand(time(NULL));//設定亂數
    answer = (rand()%101);

    cout << "Enter number(0<=number<=100)?: ";
    while ( cin >> number )//當輸入時做下列動作
    {
        if ( number > maximum || number < minium )
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << minium << "<=number<=" << maximum << ")?: ";
        }

        else if ( number == answer )
        {
            cout << "You win!" <<endl;
            break;
        }

        else if ( number > answer )//當數字比答案大時更改範圍
        {
            maximum = number - 1;
            cout << "Enter number(" << minium << "<=number<=" << maximum << ")?: ";
            if ( maximum == minium )
            {
                cout << "You lose! Answer is " << answer;
                break;
            }
        }

        else if ( number < answer )//當數字比答案小時更改範圍
        {
            minium = number + 1;
            cout << "Enter number(" << minium << "<=number<=" << maximum << ")?: ";
            if ( maximum == minium )
            {
                cout << "You lose! Answer is " << answer;
                break;
            }
        }
    }

    return 0;
}
