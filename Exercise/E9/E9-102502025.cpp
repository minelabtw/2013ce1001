#include <iostream>
#include <stdlib.h>//賦予srand有指令
#include <ctime>//賦予time有指令
using namespace std;

int main()
{
    srand(time(0));//srand重啟時，令time為0
    int random = rand()%101;//令隨便整數，但不過101
    int MIN=0;
    int MAX=100;
    int input=0;

    cout << "Enter number(" << MIN << "<=number=<" << MAX << ")?: ";
    cin >> input;

    while(input!=random)//當不等於random時跳出迴圈
    {

        if(input<MIN || input>MAX)
        {
            cout << "Out of range!\n";
            cout << "Enter number(" << MIN << "<=number=<" << MAX << ")?: ";
            cin >> input;
        }
        else
        {
            if(input<random)//假如輸入值小於random時進行MIN縮短
            {
                MIN = input + 1;
            }
            else//假如輸入值大於random時進行MMAX縮短
            {
                MAX = input - 1;
            }

            if(MIN==MAX)//假如MIN=MAX時，輸出並跳出
            {
                cout << "You lose! Answer is " << random << endl;
                return 0;
            }

            cout << "Enter number(" << MIN << "<=number=<" << MAX << ")?: ";
            cin >> input;
        }
    }

    cout << "You win!";

    return 0;
}
