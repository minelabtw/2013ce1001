#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
	int answer , input , min , max;

	srand(time(NULL));
	answer = rand() % 101; // set rnadom number

	min = 0; // set default boundary
	max = 100;

	while(true)
	{
		cout << "Enter number(" << min << "<=number<=" << max << ")?: "; // input the value
		cin >> input;
		
		if(input >= min && input <= max) // determine if input between the boundary
		{
			if(input == answer) // the correct answer
			{
				cout << "You lose! Answer is " << answer << endl; // output answer
				break;
			}

			if(input > answer) // set boundary
				max = input - 1;
			else
				min = input + 1;
		}
		else
			cout << "Out of range!" << endl;
	}

	return 0;
}
