#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));
    int randnum=rand()%101;
    int number=0;
    int randmax=100;
    int randmin=0;

    cout<<"Enter number("<<randmin<<"<=number<="<<randmax<<")?: ";
    cin>>number;
    while(number<randmin || number>randmax)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter number("<<randmin<<"<=number<="<<randmax<<")?: ";
        cin>>number;
    }

    while(number!=randnum && randmax!=randmin)
    {
    if(number>randnum)
    {
        randmax=number-1;
        if(randmax==randmin)
        break;
        cout<<"Enter number("<<randmin<<"<=number<="<<randmax<<")?: ";
        cin>>number;
        while(number<randmin || number>randmax)
        {
        cout<<"Out of range!"<<endl;
        cout<<"Enter number("<<randmin<<"<=number<="<<randmax<<")?: ";
        cin>>number;
        }
    }
    else if(number<randnum)
    {
        randmin=number+1;
        if(randmax==randmin)
        break;
        cout<<"Enter number("<<randmin<<"<=number<="<<randmax<<")?: ";
        cin>>number;
        while(number<randmin || number>randmax)
        {
        cout<<"Out of range!"<<endl;
        cout<<"Enter number("<<randmin<<"<=number<="<<randmax<<")?: ";
        cin>>number;
        }
    }
    }

    if(number==randnum)
    {
        cout<<"You win!";
    }
    else if(randmax==randmin)
    {
        cout<<"You lose! Answer is "<<randnum;
    }

    return 0;
}
