#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0));              //隨機取變數
    int randnum=rand()%101;      //將任意數除以101,餘數會介於0~100之間
    int num1;                    //宣告一整數變數
    int max=100;                 //宣告一初始最大值=100
    int min=0;                   //宣告一初始最小值=0

    cout<<"Enter number(0<=number<=100)?: ";
    cin>>num1;                   //輸入變數

    while(num1)
    {
        if(num1==min+2 or num1==max-2)   //若變數為最小值加二或最大值減二則輸
        {
            cout<<"You lose! Answer is "<<randnum;
            break;
        }
        if(num1<min or num1>max)        //若不再範圍內則顯示"Out of range!"並重新輸入
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number(0<=number<=100)?: ";
            cin>>num1;
        }
        else if(num1==randnum)         //猜中數字則贏
        {
            cout<<"You win!";
            break;
        }
        if(num1<randnum)               //若變數小於隨機變數則使min=num1並重新顯示範圍
        {
            min=num1;
            cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
            cin>>num1;
        }

        if(num1>randnum)              //若變數大於隨機變數則使max=num1並重新顯示範圍
        {
            max=num1;
            cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
            cin>>num1;
        }
    }
    return 0;
}

