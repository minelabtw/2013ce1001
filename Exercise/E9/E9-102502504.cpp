#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int main()
{
    srand(time(0)); //以time(0)做種籽
    rand();
    int number=(rand()%101)+0; //把任何數除以101，餘數便會是被除數本身
    int a,maximun,minimun;

    cout << "Enter number(0<=number<=100)?: " ;
    cin >> a;
    while(a<0||a>100) //使輸入值介於0~100之間，若不是則顯示錯誤訊息
    {
        cout << "Out of range!"<<endl;
        cout << "Enter number(0<=number<=100)?: " ;
        cin >> a;
    }

    if(a<number) //若輸入數值小於亂數數值則進入此，並修改使用者可輸入之範圍的上下限
    {
        minimun=a+1;
        maximun=100;
    }

    else if(a>number) //若輸入數值大於亂數數值則進入此，並修改使用者可輸入之範圍的上下限
    {
        maximun=a-1;
        minimun=0;
    }

    while(a!=number) //若輸入值不等於亂數值，則進入此迴圈
    {
        if(a<number)
        {
            cout << "Enter number(" << minimun << "<=number<=" << maximun << ")?: " ;
            cin >> a;
        }
        else if(a>number)
        {
            cout << "Enter number(" << minimun << "<=number<=" << maximun << ")?: ";
            cin >>a;
        }

        if(a<minimun||a>maximun) //若使用者輸入範圍外之數值，則顯示錯誤訊息
        {
            cout<< "Out of range!"<<endl;
        }

        if(a<number&&a<=maximun&&a>=minimun) //若輸入數值小於亂數數值則進入此，並修改使用者可輸入之範圍的下限
        {
            minimun=a+1;
        }
        else if(a>number&&a<=maximun&&a>=minimun) //若輸入數值大於亂數數值則進入此，並修改使用者可輸入之範圍的上限
        {
            maximun=a-1;
        }
        if(maximun==minimun) //若上限等於下限，則進入，並顯示失敗訊息
        {
            cout<< "You lose! Answer is " << number <<endl;
            break;
        }
    }

    if(a==number) //猜中亂數數值進入此，並顯示成功訊息
    {
        cout<<"You win!";
    }
    return 0;
}
