#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
	srand(time(NULL));				//use now time as seed
	int rmin=0,rmax=100;			//set the initial range
	int number=rand()%101;			//get the random number 0~100
	while(1){
		int mynum;
		cout << "Enter number(" << rmin << "<=number<=" << rmax << ")?: ";
		cin >> mynum;

		if(mynum<rmin || mynum>rmax){
			cout << "Out of range!\n";
		}else if(mynum<number){
			rmin=mynum+1;			//modifiy the range
		}else if(mynum>number){
			rmax=mynum-1;			//modifiy the range
		}else{
			cout << "You win!\n"; return 0;		//win
		}

		if(rmin==rmax){cout << "You lose! Answer is " << number << "\n"; return 0;}		//lose
	}
	//return 0;
}
