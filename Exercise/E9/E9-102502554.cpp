#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0));//以時間來打亂亂數表
    int number = (rand()%101);//以亂數表取餘數,故number會是0~100其中一數
    int  a = 0;//設定最小值
    int  b = 100;//設定最大值
    int guess;//猜測之數值

    do
    {
        cout << "Enter number(" << a << "<=number<=" << b << ")?: ";
        cin >> guess;
        while ( guess < a || guess > b )
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << a << "<=number<=" << b << ")?: ";
            cin >> guess;
        }//輸出猜測之範圍,若不符合可重新輸入

        if ( guess >= a && guess <= b)
        {
            if ( guess > number )
            {
                b = guess - 1;
            }//輸入一數若大於number則令最大值為輸入之數值減1
            else if ( guess < number )
            {
                a = guess + 1;
            }//輸入一數若小於number則令最小值為輸入之數值加1

            if ( guess == number )
            {
                cout << "you win!";
            }//若猜中則贏了
            else if ( a == b )
            {
                cout << "You lose! Answer is " << number;
            }//若答案顯示則輸了
        }
    }
    while ( guess != number && a != b);//重複輸入


    return 0;
}
