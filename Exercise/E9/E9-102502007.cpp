#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;
void input(int &a , int &b , int &c)
{
    do
    {
        cout<<"Enter number("<<a<<"<=number<="<<b<<")?: ";
        cin>>c;
        if(c<a or c>b)
            cout<<"Out of range!"<<endl;
    }
    while(c<a or c>b);
}//輸入數字的函數，利用call by reference，將每次
int main()
{
    int number,a=0,b=100,c;//猜數字，下限設為0，上限設為100，c為輸入值
    srand(time(NULL));//使用亂數
    number=rand()%101+0 ;//亂數的範圍設在0~100
    input(a,b,c);//丟入函式，第一次判斷
    do
    {
        if(number<c)
        {
            b=c-1;//如果輸入值大於數字，將上限改為輸入值-1
            if (a==b)
            {
                cout<<"You lose! Answer is "<<number;
                break;//如果上下限相等，則玩家輸了，結束遊戲並跳出迴圈
            }
            input(a,b,c);//修改上限後丟入函式，重新給出提示
        }
        else if(number>c)
        {
            a=c+1;//如果輸入值小於數字，將下限改為輸入值+1
            if (a==b)
            {
                cout<<"You lose! Answer is "<<number;
                break;//如果上下限相等，則玩家輸了，結束遊戲並跳出迴圈
            }
            input(a,b,c);//修改下限後丟入函式，重新給出提示
        }
        else
        {
            cout<<"You win!"<<endl;
            break;//如果輸入值等於數字，則玩家贏了，結束遊戲並跳出迴圈
        }
    }
    while(1);//只要輸入值不等於數字，就一直跑這個迴圈，不斷修改上下限
    return 0;
}
