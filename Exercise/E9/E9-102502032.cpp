#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    //decalaration and initialization
    int num = 0;
    int bound[2] = { 0, 100 };
    int temp = -1;              //temperlate

    //set answer
    srand( time(NULL) );
    num = rand() % 101;

    while ( bound[0] >= 0 and bound[1] <= 100 )
    {
        //ask for a number
        do
        {
            cout << "Enter number(" << bound[0] << "<=number<=" << bound[1] << ")?: ";
            cin >> temp;
        }
        while ( ( temp < bound[0] or temp > bound[1] ) and cout << "Out of range!" << endl );

        //argue whether the number inputed is answer
        //if not, change the boundary
        if ( temp == num )
        {
            cout << "You win!";
            break;
        }
        else if ( temp > num )
        {
            if ( bound[1] == temp )
                bound[1] --;
            else
                bound[1] = temp;
        }
        else        //if temp < num
        {
            if ( bound[0] == temp )
                bound[0] ++;
            else
                bound[0] = temp;
        }

        //if there's only one possible number on the interval (bound[0],bound[1])
        if ( bound[0] == num - 1 and bound[1] == num + 1 )
        {
            cout << "You lose! Answer is " << num;
            break;
        }

    }

    return 0;
}
