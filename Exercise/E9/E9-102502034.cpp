#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    int number , guess, upper=101, lower=-1;//宣告變數
    srand(time(NULL));
    number = rand() % 101 ;//決定變數
    cout << "Enter number(0<=number<=100)?: " ;//輸出輸入題目所需
    cin >> guess ;
    while(guess>100 or guess<0)
    {
        cout <<"Out of range!" <<endl;
        cout <<"Enter number(0<=number<=100)?: ";
        cin >>guess ;
    }
    while(guess != number)//當guess不等於number
    {

        if (guess>number)//若guess > number
        {
            upper=guess;//upper會等於guess
        }
        else if(guess <number)//若guess < number
        {
            lower=guess ;//lower 會等於 guess
        }
        if (upper-1 == lower+1 )//若upper-1等於lower+1
        {
            cout <<"You lose! Answer is "<<number;//輸出你輸了
            break;//跳脫迴圈
        }
       cout << "Enter number(" << lower+1 << "<=number<=" << upper-1 << ")?: ";//輸出提示
       cin >> guess ;//輸入guess
       while(guess>=upper or guess<=lower)//輸出out of range
       {
           cout <<"Out of range!" <<endl;
           cout <<"Enter number(" << lower+1 << "<=number<=" << upper-1 << ")?: ";
           cin >> guess;
       }
    }
    if(guess==number)//猜贏的狀況
    {
        cout <<"You Win!";
    }
    return 0;
}
