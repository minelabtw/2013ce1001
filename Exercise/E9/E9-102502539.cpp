#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int number;
    int answer;
    int celling = 100 ; //最大數字
    int bottom = 0 ;    //最小數字

    answer = rand() % 101 ;     //對 101 取餘數，使得區間位於 0 ~ 100

    do
    {
        if ( answer == celling && answer == bottom )    //判斷數字
        {
            cout << "You lose! Answer is " << answer;
            return 0;
        }
        cout << "Enter number(" << bottom << "<=number<=" << celling << ")?: " ;
        cin >> number;
        if ( number < bottom || number > celling )
            cout << "Out of range!" << endl ;
        else if ( number < answer )
            bottom = number + 1;
        else if ( number > answer )
            celling = number - 1;
    } while ( number != answer ) ;

    cout << "You win!" ;

    return 0;
}
