#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(NULL)); //隨機變數
    int a=(0+rand()%101); //宣告變數a的範圍為0到100
    int b; //宣告變數
    int c=0;
    int d=100;
    do
    {
        cout << "Enter number(" << c << "<=number<=" << d << ")?: "; //顯示
        cin >> b; //輸入
        if (b<c or b>d) //判斷變數
            cout << "Out of range!" << endl;
        else if (b<a && b!=d-1)
            c=b+1;
        else if (b>a && b!=c+1)
            d=b-1;
    }
    while (b!=d-1 && b!=c+1 && b!=a); //迴圈
    if (a==b) //判斷變數
        cout << "You win!";
    else if (b==d-1 or b==c+1)
        cout << "You lose! Answer is " << a;
    return 0;
}
