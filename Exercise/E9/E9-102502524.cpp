#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int main()
{
    int q = 0;                                                              //設定變數
    int a = 0;
    int m = 100;
    int n = 0;

    srand(time(NULL));                                                      //取得亂數值
    q=(rand()%101);                                                         //範圍定於0~100之間，存於q

    cout << "Enter number(" << n << "<=number<=" << m << ")?: ";            //開始終極密碼迴圈
    while(cin >> a)
    {
        if (a<n || a>m)                                                     //檢查有無超出範圍
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << n << "<=number<=" << m << ")?: ";
        }
        else if (a==q)                                                      //答對則宣告勝利並跳出迴圈
            {
            cout << "You win!" <<endl;
            break;
            }
        else if (a>q)                                                       //大於答案則縮小範圍
        {
            m=a-1;
            if (m==n)                                                       //若範圍內只剩答案，則宣告失敗並輸出答案
            {
                cout << "You lose! Answer is " << q;
                break;
            }
            cout << "Enter number(" << n << "<=number<=" << m << ")?: ";
        }
        else if (a<q)                                                       //小於答案則縮小範圍
        {
            n=a+1;
            if (m==n)                                                       //若範圍內只剩答案，則宣告失敗並輸出答案
            {
                cout << "You lose! Answer is " << q;
                break;
            }
            cout << "Enter number(" << n << "<=number<=" << m << ")?: ";
        }
    }

    return 0;
}
