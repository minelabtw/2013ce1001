#include<iostream>
#include<time.h>
#include<stdlib.h>
using namespace std;
int main()
{
    srand(time(0)  );
    int number = rand()%101;//正確答案
    int correct=0;          //決定迴圈是否結束
    int lbound=0,rbound=100;//下限和上限
    int input;              //輸入
    while(!correct)
    {
       do
       {
           cout << "Enter number(" << lbound << "<=number<=" << rbound << ")?: ";
           cin >> input;
       }//輸入並檢查
       while( (input<lbound || input>rbound) && cout << "Out of range!\n"  );
       if(input==number)//猜中數字，跳出迴圈
       {
           cout << "You win!\n";
           correct=1;
       }
       else if(input <number)
           lbound=input+1;
       else
           rbound=input-1;
       //沒有猜中則改變上限或下限

       if(lbound==rbound)
       {
           //上限與下限相等，跳出迴圈
           cout << "You lose! Answer is " << number << endl;
           correct=1;
       }

    }
    return 0;
}
