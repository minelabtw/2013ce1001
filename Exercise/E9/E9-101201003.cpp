#include<iostream>
#include<ctime>
#include<stdlib.h>
using namespace std;

int main()
{
    int a=0;//定義電腦選定的變數，使其初始值為0
    int input=0;//定義輸入的變數，使其初始值為0
    int least=0;//定義最小值變數，使其初始值為0
    int large=100;//定義最大值變數，使其初始值為100
    srand(time(NULL));//讓變數為亂數
    a=(rand()%100)+1;//亂數的範圍為0~100
    cout<<"Enter number(0<=number<=100)?: ";//
    cin>>input;//遊戲開始

    while(input!=a)//當輸入直不等於亂數的時候進入迴圈
    {
        while(input<least||input>large)
        {
            cout<<"Out of range!"<<endl<<"Enter number("<<least<<"<=number<="<<large<<")?: ";
            cin>>input;
        }//輸入範圍錯誤，要求重新輸入
        if(input<a)//當輸入數值小於亂數的時候，下界為輸入值+1
            least=input+1;
        else if(input>a)//當輸入數值大於亂數的時候，上界為輸入值-1
            large=input-1;

        if(least!=large)//當上下界值不相同的時候，執行此指令
        {
            cout<<"Enter number("<<least<<"<=number<="<<large<<")?: ";
            cin>>input;
        }
        if(large==a&&least==a)//當上下界值相同時就輸了
        {
            cout<<"You lose! Answer is "<<a;
            break;
        }
    }
    if(input==a)//當猜中的時候顯示贏了
    {
        cout<<"You win!";
    }
    return 0;
}
