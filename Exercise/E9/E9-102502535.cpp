#include <iostream>
#include <stdlib.h>
#include <ctime>

using namespace std ;

int main ()
{
    srand(time(0)) ;

    int number ;
    int n1 = 0 ;
    int n2 = 100 ;  //設變數。
    int answer = rand()%101;  //隨機找變數。

    cout << "Enter number(0<=number<=100)?: " ;
    cin >> number ;

    while ( number < 0 || number > 100 )
    {
        cout << "Out of range!" << endl << "Enter number(0<=number<=100)?: " ;
        cin >> number ;
    }  //判斷合理與否。

    do
    {
        if ( number < answer )
        {
            n1 = number + 1 ;
        }

        else if ( number > answer )
        {
            n2 = number - 1 ;
        }

        if ( n1 == n2 )
            {
            cout << "You lose! Answer is " << answer ;
            return 0 ;
            }

        cout << "Enter number(" << n1 << "<=number<=" << n2 << ")?: " ;
        cin >> number ;

        while ( number < n1 || number > n2 )
        {
            cout << "Out of range!" << endl << "Enter number(" << n1 << "<=number<=" << n2 << ")?: " ;
            cin >> number ;
        }
    }
    while ( number != answer ) ;  //猜數字。

    if ( number = answer )
    {
        cout << "You win!" ;
    }

    return 0 ;
}
