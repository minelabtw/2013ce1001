#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{
    int answer = 0, number = 0, l = 0, h = 100;
    srand(time(0));
    answer = (rand()%101);
    l = 0;//最大值
    h = 100;//最小值
    cout << "Enter number(0<=number<=100)?: ";
    cin >> number;
    while (true)
    {
        if (number<0||number>100)
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(0<=number<=100)?: ";
            cin >> number;
        }
        else if (answer<number&&number<=h)//輸入值介於答案和最大值之間
        {
            h = number - 1;
            if (l==h)
            {
                cout << "You lose! Answer is " << answer << endl;
                break;
            }
            cout << "Enter number(" << l << "<=number<=" << h << ")?: ";
            cin >> number;
        }
        else if (l<=number&&number<answer)//輸入值介於答案和最小值之間
        {
        {
            l = number + 1;
            if (l==h)
            {
                cout << "You lose! Answer is " << answer << endl;
                break;
            }
            cout << "Enter number(" << l << "<=number<=" << h << ")?: ";
            cin >> number;
        }
        else if (number==answer)
            cout << "You win!";
    }

    return 0;
}
