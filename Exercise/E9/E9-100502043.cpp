#include<iostream>
#include<cstdlib>  //使用包含rand函式的標頭檔

using namespace std;

int main()
{
    int i,n;
    int lb=0,ub=100;  //預設lower bound=0、upper bound=100

    i = rand()%101;  //用rand亂數使i=0~100中一數

    cout << "Enter number(" << lb << "<=number<=" << ub << ")?: ";

    while (1)
    {
        cin >> n;
        if (n<lb||n>ub)
        {
            cout << "Out of range!" << endl;
            while (n<lb||n>ub)
            {
                cout << "Enter number(" << lb << "<=number<=" << ub << ")?: ";
                cin >> n;
            }
        }

        if (n<i)  //若輸入值小於i，取代下界
        {
            cout << "Enter number(" << n+1 << "<=number<=" << ub << ")?: ";
            lb = n+1;
        }
        else if (n>i)  //若輸入值大於i，取代上界
        {
            cout << "Enter number(" << lb << "<=number<=" << n-1 << ")?: ";
            ub = n-1;
        }
        else if (n==i)  //輸入值等於i，遊戲勝利並結束程式
        {
            cout << "You win!";
            break;
        }

        if (lb==i&&ub==i)  //若只剩一個數可選，遊戲失敗並結束程式
        {
            cout << "\nYou lose! Answer is " << i;
            break;
        }
    }

    return 0;
}
