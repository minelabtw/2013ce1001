#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    int number;//number輸入,low,high最小最大
    int low=0;
    int high=100;
    int answer;
    answer = (rand()%100)+1;//亂數
    do
    {
        cout<<"Enter number("<<low<<"<=number<="<<high<<")?: ";
        cin>>number;
        if (number<low || number>high)
            cout<<"Out of range!"<<endl;
        else
        {
            if (number<answer)
            {
                low = number+1;
            }
            if (number>answer)
            {
                high = number-1;
            }
        }
    }
    while(low!=high && answer!=number);

    if(low==high)
        cout<<"You lose! Answer is "<<answer;
    else
        cout<<"You win!";

    return 0;
}

