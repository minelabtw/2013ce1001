#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int a,number; // initialize integer a and number
    int Min = 0; // initialize integer Min and set it to 0
    int Max = 100; // initialize integer Max and set it to 0
    srand( time ( 0 ) ); // use time as seed to set up random number
    number = rand() % 101; // sign a value to number between 0 to 100
    cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
    cin >> a;
    while ( a < 0 || a > 100 ) // while loop to check whether a is on demand
    {
        cout << "Out of range!" << endl << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
        cin >> a;
    }

    while ( 1 ) // while loop to let user guess the number
    {
        if ( a < number )
        {
            Min = a; // sign a value to Min
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
            cin >> a;

            while ( a < Min || a > Max )
            {
                cout << "Out of range!\n" << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
                cin >> a;
            }
        }
        else if ( a > number )
        {
            Max = a; // sign a value to Max
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
            cin >> a;

            while ( a < Min || a > Max )
            {
                cout << "Out of range!\n" << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
                cin >> a;
            }
        }
        else if ( Min == Max )
        {
            cout << "You lose! Answer is " << number;
            break;
        }
        else
        {
            cout << "You win!";
            break;
        }
    }
    return 0;

}

