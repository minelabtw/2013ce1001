#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    int rangemax=100;               //範圍最大值
    int rangemin=0;                 //範圍最小值
    int a;                          //答案
    int guess;                      //猜的數字
    srand(time(0));                 //給rand 當前時間當種子
    a=(rand()%100)+1;               //a=1-100亂數
    do
    {
        cout<<"Enter number("<<rangemin<<"<=number<="<<rangemax<<")?: ";   //列出範圍
        cin>>guess;
        if(guess>rangemax || guess<rangemin)                               //超出範圍則重輸入
        {
            cout<<"Out of range!"<<endl;
        }
        else
        {
            if(guess>a)                                                    //猜的答案比答案大時 更改 最大範圍
            {
                rangemax=guess-1;
            }
            if(guess<a)                                                    //猜的答案比答案小時 更改 最小範圍
            {
                rangemin=guess+1;
            }
        }
    }
    while(a!=guess && rangemin!=rangemax);                                 //迴圈直到 猜到答案 或者是 只剩一個數字
    if(rangemin==rangemax)                                                 //判斷輸贏
    {
        cout<<"You lose! Answer is "<<a;
    }
    else
    {
        cout<<"You win!";
    }
    return 0;
}
