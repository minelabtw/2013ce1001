#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main ()
{
    int isecret,inumber;                                                                     //初始化密碼和猜的數字
    int a=0,b=100;                                                                           //設a的初始值為零，b的初始值為一百
    srand (time(NULL));                                                                      //初始亂數種子
    isecret = rand ()%101;                                                                   //產生1~100的亂數
    cout << "Enter number(0<=number<=100)?: ";
    cin >> inumber;
    while (inumber!=isecret)                                                                 //當輸入的值不等於密碼時
    {
        while (inumber < a || inumber > b)                                                   //當輸入的值不在範圍內時
        {
            cout << "Out of range!\n" << "Enter number("<< a << "<=number<=" << b << ")?: "; //顯示超出範圍並要求再次輸入
            cin >> inumber;
        }
        if (b == isecret && inumber < b)                                                     //如果上限等於密碼且輸入的值小於密碼時
        {
            cout << "You lose! Answer is " << isecret;
            break;                                                                           //顯示輸和正解
        }
        if (a == isecret && a < inumber)                                                     //如果下限等於密碼且輸入的值大於密碼時
        {
            cout << "You lose! Answer is " << isecret;
            break;                                                                           //顯示輸和正解
        }
        if (a!=b)                                                                            //如果前後的值不相等時
        {
            if (inumber > isecret && inumber <= b)                                           //如果輸入的值大於密碼且小於等於上限時
            {
                b=inumber-1;
                cout << "Enter number("<< a << "<=number<=" << b << ")?: ";                  //給的範圍提示中上限要減1
            }
            else if (a <= inumber && inumber < isecret)                                      //如果輸入的值小於密碼且大於等於下限時
            {
                a=inumber+1;
                cout << "Enter number("<< a << "<=number<=" << b << ")?: ";                  //給的範圍提示中範圍中下限要加1
            }
        }
        cin >> inumber;
    }
    if (inumber == isecret)                                                                  //當輸入的值等於密碼時
    {
        cout << "You win!";                                                                  //顯示贏
    }
    return 0;
}
