#include<iostream>
#include<cstdlib>                 //contain srand()  rand()
#include<ctime>             //contain time()
using namespace std;

int main()
{
    srand(time(0));                 //亂數 時間

    int rnumber = rand()%101;
    int input =0;
    int maxnum =100;
    int minnum =0;

    cout<<"Enter number(0<=number<=100)?: ";
    while(input != rnumber)                             //while 可以一直跑
    {
        cin>>input;                          //一直輸入

        if (input >maxnum or input <minnum)
        {
            cout<<"Out of range!\nEnter number("<<minnum<<"<=number<="<<maxnum<<")?: ";
        }
        else if (input < rnumber)                            //改變最小值
        {
            minnum = input+1;
            cout<<"Enter number("<<minnum<<"<=number<="<<maxnum<<")?: ";
        }
        else if (input > rnumber)                          //改變最大值
        {
            maxnum = input-1;
            cout<<"Enter number("<<minnum<<"<=number<="<<maxnum<<")?: ";
        }

        if (maxnum == minnum)                        //判斷輸贏
        {
            cout<<"You lose! Answer is "<< rnumber;
        }
        else if(input == rnumber )
            cout<<"You win!";
    }

    return 0;
}
