#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

int randnum;//亂數
int minnum=0;//下限
int maxnum=100;//上限
int x;//輸入的數

int main()
{
    srand(time(0));//以time(0)做種子
    randnum = rand()%101;//把任何數除以101取餘數，就可以把隨機數控制在100以內
    cout << "Enter number(0<=number<=100)?: ";//輸出字串
    cin >> x;
    while ( x>100 or x<0 )//設迴圈使輸入值在0到100之間
    {
        cout << "Out of range!" << endl;
        cout << "Enter number(0<=number<=100)?: ";
        cin >> x;
    }

    while ( x!=randnum )//當輸入值不等於亂數時會進入此迴圈
    {
        if  ( x>maxnum or x<minnum )//當輸入值超出範圍時輸出下列
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << minnum << "<=number<=" << maxnum << ")?: ";
            cin >> x;
        }
        else if ( x+1==maxnum or x-1==minnum )//當輸入的數剛好使範圍只剩下答案時則輸了
        {
            cout << "You lose! Answer is " << randnum;
            break;
        }
        else if ( randnum<=maxnum && randnum>x )//當範圍還不到答案時則跑出下列
        {
            minnum = x+1;//把下限縮小
            cout << "Enter number(" << minnum << "<=number<=" << maxnum << ")?: ";
            cin >> x;
        }
        else if ( randnum>=minnum && randnum<x )
        {
            maxnum = x-1;//把上限縮小
            cout << "Enter number(" << minnum << "<=number<=" << maxnum << ")?: ";
            cin >> x;
        }

    }

    if ( x==randnum )//假如輸入的數危亂數時，則贏
    {
        cout << "You win!";
    }

    return 0;//返回初始值
}
