#include<iostream>
#include<cstdlib>//rank的使用
using namespace std;

int main()
{
    int number=0;
    int answer=(0+rand()%100);//從0到100選唷個整數當作answer的初始值
    int a=0;
    int b=100;
    do
    {
        cout<<"Enter number("<<a<<"<=number<="<<b<<")?: ";
        cin>>number;
        if(answer>number&&number>=a)
        {
            a=number+1;
        }
        else if(answer<number&&number<=b)
        {
            b=number-1;
        }
        else if(number>b||number<a)
        {
            cout<<"Out of range!"<<endl;
        }

        if(a==b)//如果只剩下答案可以猜，顯示你輸了
        {
            cout<<"You lose! Answer is "<<answer;
            number=answer;//讓輸入的值=答案以強制結束
        }
        else if(answer==number)
        {
            cout<<"You win!";
        }
    }
    while(number!=answer);

    return 0;
}
