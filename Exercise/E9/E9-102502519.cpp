#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));    //亂數
    int num1=rand()%101,num2=0,num3=100,num4=-1;    //取0~100的整數

    while(num4!=num1)    //運算
    {
        cout << "Enter number(" << num2 << "<=number<=" << num3 << ")?: ";
        cin >> num4;
        if(num4>num3 || num4<num2)
            cout << "Out of range!" <<endl;
        else if(num4>num1)
            num3=num4-1;
        else if(num4<num1)
            num2=num4+1;
        else if(num2==num3)
            cout << "You lose! Answer is " << num1;
        else
            cout << "You win!";
    }

    return 0;
}
