#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int Game(int &random,int &number,int m,int Ma);    //prototype

int main()
{
    int random;    //設定變數
    int number;
    int m=0;
    int Ma=100;

    srand(time (0));    //亂數函數

    random=rand()%101;

    do    //判斷輸入的值是否合理
    {
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>number;
        if(number>100||number<0)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(number>100||number<0);

    Game(random,number,m,Ma);    //呼叫函數

    return 0;
}

int Game(int &random,int &number,int m,int Ma)    //定義函數
{
    while(m!=Ma)    //迴圈
    {
        if(random==number)    //判斷贏
        {
            cout<<"You win!";
            m=Ma;
        }

        else if(random>number)
        {
            m=number+1;

            if(m==Ma)    //判斷最大跟最小值是否一樣，若一樣就輸了
            {
                cout<<"You lose! Answer is "<<random;
            }

            else
            {
                do    //判斷值是否在最大跟最小之間
                {
                    cout<<"Enter number("<<m<<"<=number<="<<Ma<<")?: ";
                    cin>>number;
                    if(number>Ma||number<m)
                    {
                        cout<<"Out of range!"<<endl;
                    }
                }
                while(number>Ma||number<m);
            }
        }

        else if(random<number)
        {
            Ma=number-1;

            if(m==Ma)
            {
                cout<<"You lose! Answer is "<<random;
            }

            else
            {
                do
                {
                    cout<<"Enter number("<<m<<"<=number<="<<Ma<<")?: ";
                    cin>>number;
                    if(number>Ma||number<m)
                    {
                        cout<<"Out of range!"<<endl;
                    }
                }
                while(number>Ma||number<m);
            }
        }
    }
}
