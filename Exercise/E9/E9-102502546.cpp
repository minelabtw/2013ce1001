#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

int main()
{
    int number , answer ;
    int temp1=0 ;
    int temp2=100 ;//宣告變數
    srand(time(0)) ;//隨機變數

    answer = rand()%101 ;//亂數
    cout << "Enter number(0<=number<=100)?: " ;
    cin >> number ;
    while(number<0 || number >100)
    {
        cout << "Out of range!" << endl ;
        cout << "Enter number(0<=number<=100)?: " ;
        cin >> number ;
    }
    while(answer!=number && temp1!=temp2)//若輸入與亂數相同 則結束，或已可確定答案
    {

        if(number>answer)
        {
            temp2=number-1 ;
            if(temp1==temp2)
            {
                break ;//只要一相等，則結束
            }
            cout << "Enter number(" << temp1 << "<=number<=" << temp2 << ")?: " ;
            cin >> number ;
        }
        else if(number<answer)
        {
            temp1=number+1 ;
            if(temp1==temp2)
            {
                break ;
            }
            cout << "Enter number(" << temp1 << "<=number<=" << temp2 << ")?: " ;
            cin >> number ;
        }
        while(number<temp1 || number>temp2)
        {
            cout << "Out of range!" << endl ;
            cout << "Enter number(" << temp1 << "<=number<=" << temp2 << ")?: " ;
            cin >> number ;
        }
    }
    if(temp1==temp2)//符合條件，則輸出輸了
    {
        cout << "You lose! Answer is " << answer ;
    }
    if(number=answer)//直接猜中 則贏
    {
        cout << "You win!" ;
    }
    return 0 ;
}
