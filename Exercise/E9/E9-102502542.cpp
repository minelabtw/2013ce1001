#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std ;

int main()
{
    int number=0 ; //宣告變數
    int rain=0 ;
    int a=0 ;
    int b=100 ;
    srand(time(0)); //做亂數
    rain=rand()%101 ; // 隨機挑取亂數
    cout << "Enter number(0<=number<=100)?: " ; //輸出
    cin >> number ; //輸入
    while (number<0 or number>100) // 當number<0 or number>100 進入迴圈
    {
        cout << "Out of range!" << endl << "Enter number(0<=number<=100)?: " ;
        cin >> number ;
    }
    while (number!=rain) //當number!=rain進入迴圈
    {

        if (number>rain) //判斷式
        {
            b=number-1 ;

        }
        else if (number<rain) //同上
        {
            a=number+1 ;

        }
        if (a==b) //同上
        {
            cout << "You lose! Answer is " << rain ;
            break ; //離開
        }
        cout << "Enter number(" << a << "<=number<=" << b << ")?: " ; //輸出
        cin >> number ; //輸入
        while (number<a or number>b) //如果number<a or number>b 進入以下迴圈
        {
            cout << "Out of range!" << endl << "Enter number(" << a << "<=number<=" << b << ")?: " ;
            cin >> number ;
        }
    }
    if (number==rain) //判斷式.
        cout << "You win!" ;
    return 0 ;
}
