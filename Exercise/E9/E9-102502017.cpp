#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std;

int main(void)
{
    srand(time(NULL));                                          //製造亂數表
    int number=rand()%101;                                      //定義答案，範圍在 0~100
    int input=0;
    int _max=100,_min=0;                                        //最大值=100 , 最小值=0

    while(true)
    {
        cout << "Enter number(" << _min << "<=number<=" << _max << ")?: ";
        cin >> input;
        if(input<_min || input>_max)cout << "Out of range!\n";  //判斷範圍
        else if(input<number)_min=input+1;
        else if(input>number)_max=input-1;
        else
        {
            cout << "You win!";
            break;                                          //U win ,end loop
        }
        if((_max-_min)==0)
        {
            cout << "You lose! Answer is " << number;
            break;                                          //U lose ,end loop
        }
    }
    return 0;
}
