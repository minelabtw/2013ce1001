#include<iostream>
#include<cstdlib>
#include<ctime>
int main()
{
    srand(time(0));//時間亂數
    int guess=102,l=0,h=100,temp=rand()%101;//宣告
    while((guess<l || guess>h)&&(guess!=temp && h!=l))//判斷
    {
        std::cout<<"Enter number("<<l<<"<=number<="<<h<<")?: ";
        std::cin >> guess;
        if(guess<l || guess>h)//判斷有無超出範圍
            std::cout<<"Out of range!\n";
        h=(temp<=guess&&guess<=h)?guess-1:h;//改變頂值
        l=(temp>=guess&&guess>=l)?guess+1:l;//改變底值
    }
    (guess==temp)?std::cout<<"You win!":std::cout<<"You lose! Answer is "<<temp;//判斷顯示
    return 0;
}
