#include<iostream>
#include<time.h>
#include<stdlib.h>

using namespace std;

void judge(int &a,int &b,int ans,int in);       //prototype 用來改變上下值

int main()
{
    int number=0,input=0 ;
    int uper=100,down=0 ;
    int a=0;
    srand(time(NULL));                          //淨空
    number=rand()%101+0 ;                       //令答案的值

    cout<<"Enter number(0<=number<=100)?: " ;
    cin>>input ;
    while(a==0)
    {
        if(input==number)
        {
        cout<<"You win!" ;
        break;
        }
        while(input<down||input>uper)
        {
            cout<<"Out of range!\n";
            cout<<"Enter number("<<down<<"<=number<="<<uper<<")?: " ;
            cin>>input;
        }
        judge(down,uper,number,input);

        while(down<=input<=uper)                                        //正確的範圍內 輸入不一定正確 所以要再放一次判斷正確與否
        {
        cout<<"Enter number("<<down<<"<=number<="<<uper<<")?: " ;
        cin>>input;
        while(input<down||input>uper)                                       //判斷正確與否
        {
            cout<<"Out of range!\n";
            cout<<"Enter number("<<down<<"<=number<="<<uper<<")?: " ;
            cin>>input;
        }
        judge(down,uper,number,input);
        if(input==number)                                                   //正確則跳出
        break;
        if(uper-down==0)                                                    //猜輸判斷
        {
            cout<<"You lose! Answer is "<<number;
            a=1;
            break;
        }
        }
    }
}

void judge(int &a,int &b,int ans,int in)            //改變上下值
{
    if(in<ans)
    a=in+1;
    if(in>ans)
    b=in-1;
}
