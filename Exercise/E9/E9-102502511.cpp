#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int i;
    int l;
    int u;
    int a;
    srand(time(0)); //以time(0)為種子
    i=rand()%101; //i為0~100的其中一個數
    l=0; //設底一開始為0
    u=100; //設u一開始為0

    cout << "Enter number(" << l << "<=number<=" << u << ")?: ";
    cin >> a;

    while(a!=i) //當a不等於答案時 跳入
    {
        if(a<l||a>u) //a超過範圍
        {
            cout << "Out of range!" <<endl;
            cout << "Enter number(" << l << "<=number<=" << u << ")?: ";
            cin >> a;
        }
        if(l<=a&&a<=i)
        {
            l=a+1;
            if(l==u) //此內容夾在變化完後的l後面，使此內容能第一時間反應
            {
                cout << "You lose! Answer is " << i << endl;
                break; //breal使他跳出外圈的if
            }
            cout << "Enter number(" << l << "<=number<=" << u << ")?: ";
            cin >> a;
        }
        if(i<=a&&a<=u)
        {
            u=a-1;
            if(l==u)
            {
                cout << "You lose! Answer is " << i << endl;
                break;
            }
            cout << "Enter number(" << l << "<=number<=" << u << ")?: ";
            cin >> a;
        }
    }
    if(a==i) //當a等於答案
    {
        cout << "You win!";
    }

    return 0;
}

