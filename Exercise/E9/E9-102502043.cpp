#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

int main()
{
    int number;                                                          //宣告變數
    int a;
    int b=100;                                                           //設定上界值
    int c=0;                                                             //設定下界
    srand(time(0));
    a=(rand()%101);
    cout<<"Enter number(0<=number<=100)?: ";
    cin>>number;
    while(number<=0&&number>=100)                                        //設定範圍
    {
        cout<<"Out of range!!"<<endl;
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>number;
    }
    while(number!=a)                                                     //判斷答案值
    {
        if(number>a)                                                     //改變上界
        {
            b=number;
            if(b-1==c)
            {
                cout<<"You lose! Answer is "<<a;
                break;
            }
            cout<<"Enter number("<<c<<"<=number<="<<(b-1)<<")?: ";
            b=b-1;
            cin>>number;
            while(number>b||number<c)
            {
                cout<<"Out of range!!"<<endl;
                cout<<"Enter number("<<c<<"<=number<="<<b<<")?: ";
                cin>>number;
            }
        }
        if(number<a)
        {
            c=number;
            if(b==c+1)
            {
                cout<<"You lose! Answer is "<<a;
                break;
            }
            cout<<"Enter number("<<(c+1)<<"<=number<="<<b<<")?: ";   //改變下界
            c=c+1;
            cin>>number;
            while(number<c||number>b)
            {
                cout<<"Out of range!!"<<endl;
                cout<<"Enter number("<<c<<"<=number<="<<b<<")?: ";
                cin>>number;
            }
        }
        if(number==a)
        {
            cout<<"You win!";
            break;
        }

    }

    return 0;
}
