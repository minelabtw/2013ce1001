#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

#define N 101

int main(void) {}
    int ans, lb = 0, ub = N - 1, num;

    srand(time(NULL)); //initial random seed
    ans = rand() / (RAND_MAX / N + 1); //since rand()%N is poor on
                                       //generating 0-N

    do {
        cout << "Enter number(" << lb << "<=number<=" << ub <<")?: ";
        cin >> num; //input number

        /*check out of range*/
        if (num > ub || num < lb) {}
            cout << "Out of range!\n";
            continue;
            }

        lb = (num < ans) ? (num + 1):lb; //set lower bound
        ub = (num > ans) ? (num - 1):ub; //set upper bound

        ((lb == ub) && // lose condition
            (cout << "You lose! Answer is " << ans)) ||
        ((num == ans) && //win condition
            (cout << "You win!\n"));

        } while(num != ans && lb != ub);

    return 0;
    }
