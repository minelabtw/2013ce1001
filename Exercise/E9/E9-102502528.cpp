#include<iostream>
#include<cstdlib>
#include <time.h>
using namespace std;

int main()
{
    srand(time(NULL));                         //依時間出亂數
    int ans = 1 + (rand()%100),input;           //指定一個介於1-100的亂數
    int Min = 1,Max = 100;
    int mode = 1;
    do
    {
        cout << "Enter number("<< Min<< "<=number<=" << Max << ")?: ";
        cin >> input;
        while(input < Min || input > Max)         //判定輸入範圍
        {
            cout << "Out of range!" <<endl;
            cout << "Enter number("<< Min<< "<=number<=" << Max << ")?: ";
            cin >> input;
        }
        if(input == ans)         //正解，跳出循環
        {
            cout << "You win!";
            mode = 0;
        }
        else if(input > ans)
        {
            Max = input - 1;
            if(ans == Max && ans == Min)           //失敗，顯示答案並跳出循環
            {
                cout << "You lose! Answer is " << ans;
                mode = 0;
            }
            else
                cout << "Out of range!" << endl;

        }
        else
        {
            Min = input + 1;
            if(ans == Max && ans ==Min)
            {
                cout << "You lose! Answer is " << ans;        //失敗，顯示答案並跳出循環
                mode = 0;
            }
            else
                cout << "Out of range!" << endl;
        }
    }
    while(mode == 1);       //循環

    return 0;
}

