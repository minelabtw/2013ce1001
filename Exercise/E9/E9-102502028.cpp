#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>

using namespace std ;

int main ()
{
    srand (time(0)) ;   //找亂數
    int number =  rand() % 100 + 1 ;   //亂數介在0跟100之間
    int x = 0 ;
    int Max = 100 ;
    int Min = 0 ;

    while (x != number)   // 還沒有結果前繼續迴圈
    {
        do     // 要求輸入範圍
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: " ;
            cin >> x ;
            if(x < Min || x > Max)
            {
               cout << "Out of range!" << endl;
            }
        }
        while (x < Min || x > Max) ;

        if (x == number)     //各種條件繼續猜or贏or輸的條件
        {
            cout << "You win!" ;
            break ;
        }
        else if (Min <= x && x <= number)
            Min = x + 1 ;
        else if (number <= x && x <= Max)
            Max = x - 1 ;
        if (Max - Min == 0)
        {
            cout << "You lose! Answer is " << number ;
            break ;
        }
    }
    return 0 ;
}
