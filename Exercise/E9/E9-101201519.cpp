#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));//讓種子隨著時間變化
    int ans = rand()%101;
    int guess=0,min=0,max=100;
    do
    {
        cout << "Enter number("<< min <<"<=number<=" << max <<")?: ";
        cin >> guess;
        if (guess<min || guess>max)//判斷猜的範圍是否正確
            cout << "Out of range!\n";
        else if (guess<ans)//若沒猜到正確數字則改變猜測的範圍
            min=guess+1;
        else if (guess>ans)
            max=guess-1;
        else if (guess==ans)
        {
            cout << "You win!\n";
            return 0;
        }
    }
    while(max!=min);
    cout << "You lose! Answer is " << ans << endl;
    return 0;

}
