#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0)); //以time(0)做seed
    int number=rand()%101;
    int choose=-1;
    int Max=100, Min=0;
    int a;

    while(choose<0||choose>100)
    {
        cout << "Enter number(0<=number<=100)?: ";
        cin >> choose;

        if(choose<0||choose>100)
            cout << "Out of range!\n";
    }

    while(true)
    {
        if(choose==number)
        {
            cout << "You win!\n";
            break;
        }

        else if((choose-number)==1||(choose-number)==-1)
        {
            cout << "You lose! Answer is " << number;
            break;
        }

        else if(choose<number&&(choose-number)!=-1)
        {
            do
            {
                cout << "Enter number(" << choose+1 << "<=number<=" << Max << ")?: ";
                a=choose+1;
                cin >> choose;
                if(choose<a||choose>Max)
                {
                    cout<<"Out of range\n";
                    choose=a-1;
                }


            }
            while(choose<a||choose>Max);
            Min=a;
        }

        else if(choose>number&&(choose-number)!=1)
        {
            do
            {
                cout << "Enter number(" << Min << "<=number<=" <<  choose-1<< ")?: ";
                a=choose-1;
                cin >> choose;
                if(choose<Min||choose>a)
                {
                    cout<<"Out of range\n";
                    choose=a+1;
                }
            }
            while(choose<Min||choose>a);

            Max=a;
        }
    }

    return 0;
}
