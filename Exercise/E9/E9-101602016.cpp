#include <iostream>
#include <cstdlib>  //contain srand() and rand()
#include <ctime>    //contain times()
using namespace std;
int main()
{
    srand(time(NULL));//使系統的起使值為亂數
    int number=(rand()%100)+1;//使number這個變數為亂數
    int x=0,s=0,m=100;//宣告使用者輸入的數，下界，上界


    do//設定遊戲結束條件為 : 1.使用者輸入數字等於number 或 2.上界等於下界
    {
        if(s!=number||m!=number)//當上界不等於下界時，使用者可以繼續遊戲
            cout<<"Enter number("<<s<<"<=number<="<<m<<")?: ";//顯示每次遊戲的範圍
        cin>>x;//開始遊戲(猜數字)

        while(x<s||m<x)//當數字超出範圍時，請使用者重新輸入
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number("<<s<<"<=number<="<<m<<")?: ";
            cin>>x;
        }

        if(x<number)//當x小於number
        {
            s=x+1;//將下界變為x+1
        }
        else if(number<x)//當x大於number
        {
            m=x-1;//將上界變為x-1
        }
    }
    while(s!=m&&x!=number);//當符合"1.使用者輸入數字等於number 或 2.上界等於下界"任一條件時，游戲結束

    if(x==number)//猜到數字
        cout<<"You win!";//使用者贏了
    else if(s==m)//上界等於下界
        cout<<"You lose! Answer is "<<number;//使用者輸了

    return 0;
}
