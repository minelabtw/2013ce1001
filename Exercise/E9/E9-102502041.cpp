#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    int x,input,minimum=0,maximum=100;
    srand(time(0));
    x=rand()%101;
    cout<<"Enter number(0<=number<=100)?: ";
    do
    {
        cin>>input;
        if(input>100||input<0)
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number("<<minimum<<"<=number<="<<maximum<<")?: ";
        }
        else if(input==x)                                                   //猜中的話就直接跳出迴圈
            break;
        else if(input<x)
        {
            minimum=input+1;
            if(minimum==maximum)                                            //如果出現這種情況就直接輸
                break;
            cout<<"Enter number("<<minimum<<"<=number<="<<maximum<<")?: ";
        }
        else if(input>x)
        {
            maximum=input-1;
            if(minimum==maximum)                                            //如果出現這種情況就直接輸
                break;
            cout<<"Enter number("<<minimum<<"<=number<="<<maximum<<")?: ";
        }
    }
    while(input!=x);
    if(minimum==maximum)
        cout<<"You lose! Answer is "<<x;
    else
        cout<<"You win!";

    return 0;
}
