#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));//讓種子隨時間改變
    int ans=rand()%101, lbd=0, ubd=100, guess=0;//宣告變數為整數，ans為答案、lbd為下界、ubd為上界、guess為猜測答案

    do
    {
        cout << "Enter number(" << lbd << "<=number<=" << ubd << ")?: ";
        cin >> guess;
        if(guess<lbd || guess>ubd)//檢查是否在範圍內
        {
            cout << "Out of range!\n";
            continue;
        }
        if(guess==ans)//贏
        {
            cout << "You win!\n";
            return 0;
        }
        else if(guess<ans)//改變範圍
            lbd=guess+1;
        else if(guess>ans)
            ubd=guess-1;
    }
    while(lbd!=ubd);
    //輸
    cout << "You lose! Answer is " << ans << endl;

    return 0;
}
