#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    int a=0;
    int b=100;
    int c=(rand()%101); //c值為亂數
    int x;

    do
    {
        cout << "Enter number(" << a << "<=number<=" << b << ")?: ";
        cin >> x;
        if(x<a || x>b)
            cout << "Out of range!\n";
        if(x<c)
            a=x+1;
        if(x>c)
            b=x-1; //猜錯時縮小範圍
        if(a==b)
            cout << "You lose! Answer is " << c; //如果a=b，失敗，並輸出c值
        if(x==c)
        {
            cout << "You win!"; //如果x=c時，獲勝
            a=c;
            b=c;
        }
    }
    while(a!=b); //a不等於b時執行迴圈

    return 0;
}
