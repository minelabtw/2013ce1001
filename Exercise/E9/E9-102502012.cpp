#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;
int main(){
	srand(time(NULL));
	int num = rand() % 101; // get 0 ~ 100
	int L, R;
	int guess;

	L = 0, R = 100;
	do{
		do{
			cout << "Enter number(" << L << "<=number<=" << R<<") ? : ";
			cin >> guess;
		} while ((guess > R || guess < L) && cout << "Out of range!" << endl);
		if (guess > num) // reduce range
			R = guess - 1;
		if (guess < num) // reduce range
			L = guess + 1;
	} while (guess != num&&R != L);
	if (R != L)
		cout << "You win!" << endl;
	else
		cout << "You lose! Answer is " << R << endl;

	return 0;
}