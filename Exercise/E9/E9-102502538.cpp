#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    int number1,number2=100,number3=0;
    int random;

    srand(time(NULL));         //設定亂數
    random = rand()%101;

    cout <<"Enter number(0<=number<=100)?: ";
    cin >>number1;
    while(number1<0||number1>100)//檢驗number1的值
    {
        cout <<"Out of range!"<<endl;
        cout <<"Enter number(0<=number<=100)?: ";
        cin >>number1;
    }
    while(number1)
    {
        if(random-number1<0)   //替換最大值和最小值
            number2=number1-1;
        else
            number3=number1+1;
        cout <<"Enter number("<<number3<<"<=number<="<<number2<<")?: ";
        cin >>number1;
        while(number1<number3||number1>number2)
        {
            cout <<"Out of range!"<<endl;
            cout <<"Enter number(0<=number<=100)?: ";
            cin >>number1;
        }
        if(number2-number3==0)//判斷輸贏條件
        {
            cout <<"You lose! Answer is "<<random;
            break;
        }
        else if(number1==random)
        {
            cout <<"You win!";
            break;
        }
    }
    return 0;
}
