#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{

    int uplimit = 100; // 上限
    int downlimit = 0; // 下限
    int guess = -1; // 猜測值
    int num; // 答案

    srand(time(0)); // 建立表格
    num = rand() % 101; // 抽取亂數

    while(guess != num)
    {
        if(uplimit == downlimit) // 輸
        {
            cout << "You lose! Answer is " << num << endl;
            break;
        }
        cout << "Enter number(" << downlimit << "<=number<=" << uplimit << ")?: ";
        cin >> guess;
        if(guess < downlimit || guess > uplimit)cout << "Out of range!\n";
        else if(guess==num)cout << "You win!\n"; //贏
        else
        {
            guess < num ? downlimit = guess+1 : uplimit = guess-1; // 改變上下限
        }
    }

    return 0;

}
