#include<iostream>
#include<stdlib.h>
using namespace std;

int main()
{
    int answer = rand() % 100 + 1;//宣告答案為一1-100的隨機變數
    int input;//宣告輸入數
    int small = 0;//宣告較小數
    int big = 100;//宣告較大數

    do{
        cout << "Enter number(" << small << "<=number<=" << big << ")?:";//提示輸入範圍
        cin >> input;
        if (input < small || input > big)
            cout << "Out of range!" << endl;//提示超圍
       else if(input == answer)
           {cout << "You win!";
            break;}//若答對即結束
       else if (input < answer && input > small)
           {small = input + 1;}//若比答案小且最為靠近答案,複製比輸入數大1的數至較小數
       else if (input > answer && input < big)
           {big = input - 1;}//若比答案大且最為靠近答案,複製比輸入數小1的數至較大數
       if (big == small)
           {cout << "You lose! Answer is " << answer;}//若較大數已等於較小數,提示輸
       }while(big != small);//較大數等於較小數跳出迴圈
return 0;//回傳
}
