#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main()
{
    srand(time(0));
    rand();
    int number;
    int answer=rand()%101;  //用rand設定答案值為0~100
    int high=101;  //設定Max值
    int low=-1;  //設定min值

    cout << "Enter number(0<=number<=100)?: ";
    cin >> number;
    while(number>100 or number<0)  //用while迴圈判斷輸入的值是否在範圍內
    {
        cout << "Out of range!" << endl << "Enter number(0<=number<=100)?: ";
        cin >> number;
    }

    while (number!=answer && low+1!=high-1)  //當輸入的值不等於答案或是Max值不等於min值則進行下列程式,否則停止
    {
        if (number>answer)  //若輸入的值大於答案
        {
            high=number;  //將最大值改為輸入的值
            if (low+1==high-1)
            {
                cout << "You lose! Answer is " << answer;
            }
            else  //輸出新範圍
            {
                cout << "Enter number(" << low+1 << "<=number<=" << high-1 << ")?: ";
                cin >> number;
            }
        }


        if (number<answer)  //若輸入的值小於答案
        {
            low=number;  //將最小值改為輸入的值
            if (low+1==high-1)
            {
                cout << "You lose! Answer is " << answer;
            }
            else  //輸出新範圍
            {
                cout << "Enter number(" << low+1 << "<=number<=" << high-1 << ")?: ";
                cin >> number;
            }
        }
        if (number==answer)  //若輸入的值等於答案的值
        {
            cout << "You win!";
        }

    }
    return 0;
}
