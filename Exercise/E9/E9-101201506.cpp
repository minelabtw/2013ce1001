#include <iostream>
#include <cstdlib>  //contain srand() and rand()
#include <ctime>    //contain times()
using namespace std;
int main()
{
    int x=0; //輸入的數
    int y=0; //隨機的答案
    int a=100; //上界起始質為100
    int b=0; //下界起始質為0

    srand((unsigned) time(NULL)); //亂數產生

    y=rand()%100+0; //產生0-100的亂數

    cout<<y<<endl;

    cout<<"Enter number(0<=number<=100)?: ";
    cin>>x;

    while(a!=y && b!=y) //上界和下界都不等於y時
    {


        if(x>a || x<b)
            cout<<"Out of range!"<<endl<<"Enter number("<<b<<"<=number<="<<a<<")?: ";

        else
        {
            if(x>y)
            {
                a=x-1;
                cout<<"Enter number("<<b<<"<=number<="<<a<<")?: ";
            }
            else if(x<y)
            {
                b=x+1;
                cout<<"Enter number("<<b<<"<=number<="<<a<<")?: ";
            }
            else if(x==y)
            {
                cout<<"You win!";
                a=y;
                b=y;
            }
        }
        cin>>x;
    }

    while(a==y&&b!=y) //上界等於y時
    {
        if(x>a || x<b)
            cout<<"Out of range!"<<endl<<"Enter number("<<b<<"<=number<="<<a<<")?: ";
        else
        {
            b=x+1;
            if(x==y)
            {
                cout<<"You win!";
                b=y;
            }
            else if(b==y)
            {
                cout<<"You lose! Answer is "<<y<<endl;
            }
            else
                cout<<"Enter number("<<b<<"<=number<="<<a<<")?: ";
        }
        cin>>x;
    }

    while(b==y&&a!=y) //下界等於y時
    {
        if(x>a || x<b)
            cout<<"Out of range!"<<endl<<"Enter number("<<b<<"<=number<="<<a<<")?: ";
        else
        {
            a=x-1;
            if(x==y)
            {
                cout<<"You win!";
                a=y;
            }
            else if(a==y)
            {
                cout<<"You lose! Answer is "<<y<<endl;
            }
            else
                cout<<"Enter number("<<b<<"<=number<="<<a<<")?: ";
        }
        cin>>x;
    }

    return 0;
}
