#include <iostream>
#include <ctime>
#include <stdlib.h>
using namespace std;

int main(){

    srand(time(0));
    int target = rand()%101;
    int input, status = 0;
    int upperbound = 100;
    int lowerbound = 0;

    do{
        cout << "Enter number(" << lowerbound << "<=number<=" << upperbound << "): ";
        cin >> input;
        if(input<lowerbound || input>upperbound)
            cout << "Out of range!" << endl;
        else if(input == target){
            cout << "You win!";
            status = 1;
        }
        else if(input < target){
             if(target==upperbound && input==(target-1)){
                 cout << "You lose! The answer is : " << target;
                 status = 1;
             }
             lowerbound = input+1;
        }
        else if(input > target){
             if(target==lowerbound && input==(target+1)){
                 cout << "You lose! The answer is : " << target;
                 status = 1;
             }
             upperbound = input-1;
        }
    }while(status == 0);

    return 0;
}
