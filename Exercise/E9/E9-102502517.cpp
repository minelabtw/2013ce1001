#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    srand(time(0)); //宣告以時間為seed的隨機變數
    int number = rand()%101; //宣告變數為101的餘數
    int guess_number = 0;
    int min_number = 0;
    int MAX_number = 100;

    do
    {
        cout << "Enter number(" << min_number << "<=number<=" << MAX_number << ")?: ";
        cin >> guess_number;
        if (guess_number<min_number or guess_number>MAX_number) //當輸入不在範圍內輸出Out of range!
            cout << "Out of range!" << endl;
        else if (guess_number==number) //當輸入為number時輸出You win!並結束迴圈
            cout << "You win!";
        else if (guess_number>=min_number and guess_number<=MAX_number and guess_number<number) //當輸入小於number時使範圍縮小
            min_number = guess_number+1;
        else if (guess_number>=min_number and guess_number<=MAX_number and guess_number>number) //當輸入大於number時使範圍縮小
            MAX_number = guess_number-1;

        if (min_number==MAX_number) //當範圍無法縮小時輸出正確答案並結束迴圈
        {
            cout << "You lose! Answer is " << number;
            guess_number = number;
        }

    }
    while (guess_number!=number);

    return 0;
}
