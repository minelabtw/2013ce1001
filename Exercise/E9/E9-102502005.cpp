#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));                                                         //指定rand的seed。
    int randnum = rand () % 101, upbound = 100, lowbound = 0, ans = -1 ;    // 宣告變數。

    do
    {
        cout << "Enter number(" << lowbound << "<=number<=" << upbound <<")?: ";//要求使用者輸入答案。
        cin >> ans;

        if (ans > upbound || ans < lowbound)                                //若輸入的答案不符範圍，輸出字串。
        {
            cout << "Out of range!" << endl;
        }

        if (ans < randnum && ans >= lowbound)                               //若輸入的答案小於真正的答案，修改下限。
        {
            lowbound = ans + 1;
        }
        else if(ans > randnum && ans <= upbound)                            //若輸入的答案大於真正的答案，修改上限。
        {
            upbound = ans - 1;
        }
        if(upbound == lowbound)                                             //若最後沒有猜出答案，輸出字串。
        {
            cout << "You lose! Answer is " << randnum;
            break;
        }
    }
    while(ans != randnum);

    if (ans == randnum)                                                     //若使用者猜出正確的答案，輸出字串。
    {
        cout << "You win!" ;
    }
    return 0;
}
