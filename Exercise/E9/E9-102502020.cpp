#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    int a=0;
    int b=100;
    int number=0;                //你猜的數字
    int answer=0;                //正確答案

    srand(time(0));              //以time(0)的隨機變數當作seed
    answer=rand()%100+1;         //隨機變數的範圍在0到100之間
    //cout << answer << endl;
    do
    {
        if(a!=b)                                                             //當a不等於b
            cout << "Enter number(" << a << "<=number<=" << b <<" )?: ";     //輸出正確答案在哪個範圍裡
        else                                                                 //當二選一你猜錯時
        {
            cout << "You lose! Answer is " << answer;                        //真可惜~你猜錯啦!!
            return 0;
        }
        cin >> number;                                                       //請猜個數字
        if(number<a or number>b)                                             //當猜的數字超出範圍時
            cout << "Out of range!" << endl;                                 //超出範圍囉~~
        else if(number<answer)                                               //當猜的數字比正確答案小時
            a=number+1;                                                      //調整最小值範圍
        else                                                                 //當猜的數字比正確答案大時
            b=number-1;                                                      //調整最大值範圍
    }
    while(number!=answer);                                                   //當沒猜中正確答案時
    cout << "You win!";                                                      //恭喜~你猜對啦!!

    return 0;
}
