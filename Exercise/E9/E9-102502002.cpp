#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand(time(0));                         //choose a random number (changes through time).
    int randnum=rand()%100+1;               //the range of randnum is from 1 to 100.
    int number=0;                           //declare a variable called "number".
    int n1=100;                             //the upper limit
    int n2=0;                               //the lower limit

    cout << "Enter number(0<=number<=100)?: ";            //enter a number between 0 and 100.
    cin >> number;
    while(number)
    {
        if(n1==number+2||n2==number-2)                    //if you don't guess the right number by the end of the game, output the answer.
        {
            cout << "You lose! Answer is " << randnum;
            break;
        }
        if(number>n1||number<n2)                          //if number is bigger than the upper limit or smaller than the lower limit output out of range.
        {
            cout << "Out of range!\n" << "Enter number(" << n2 << "<=number<=" << n1 << ")?: ";
            cin >> number;
        }
        else if(number==randnum)                          //if you guess the correct number output you win.
        {
            cout << "You win!";
            break;
        }
        else if(number<randnum)                           //if number is smaller than randnum, change the upper limit to number.
        {
            n2=number;
            cout << "Enter number(" << number << "<=number<=" << n1 << ")?: ";
            cin >> number;
        }
        else if(number>randnum)                           //if number is bigger than randnum, change the lower limit to number.
        {
            n1=number;
            cout << "Enter number(" << n2 << "<=number<=" << number << ")?: ";
            cin >> number;
        }
    }
    return 0;
}
