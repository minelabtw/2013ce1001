#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* tmp is the point of input         */
/* upper is the upper bound of range */
/* lower is the lower bound of range */
void input(int *tmp, int upper, int lower)
{
	for(;;)
	{
		/* ui message */
		printf("Enter number(%d<=number<=%d)?: ", lower, upper);

		/* input one number */
		scanf("%d", tmp);

		/* check input is in range[lower, upper] */
		if(lower <= *tmp && *tmp <= upper)
			break;
		else
			puts("Out of range!"); // error message
	}
}

int main()
{
	/* num is the input */
	/* ans is the anser of the game */
	int num, ans;

	/* the upper bound and lower bound of the range */
	int upper, lower;

	/* init the game */
	srand(time(NULL));
	ans = rand() % 101;
	upper = 100;
	lower = 0;

	/* game start */
	for(;;)
	{
		/* input a number */
		input(&num, upper, lower);

		/* guess number right */
		if(num == ans)
		{
			puts("You win!");
			break;
		}
		/* guess number wrong*/
		else
		{
			/* update the range */
�@�@�@�@�@�@(num < ans) ? (lower = num+1) : (upper=num-1);

			/* if only one number can be guessed */
			if(upper == lower)
			{
				printf("You lose! Answer is %d\n", ans);
				break;
			}
		}
	}
	return 0;
}
