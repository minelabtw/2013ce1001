#include<iostream>
#include<cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int guess;                    //猜的數字
    int min=0;                    //猜數字的最小值
    int max=100;                  //猜數字的最大值
    srand(time(0));               //rand依當前時間來跑
    int a=(rand()%100)+1;         //答案,為1~100間的亂數

    do
    {
        cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
        cin>>guess;

        if(guess>max || guess<min)
        {
            cout<<"Out of range!"<<endl;
        }
        else
        {
            if(guess>a)
            {
                max=guess-1;                                     //猜的數字比答案大時,更改最大範圍
            }
            if(guess<a)
            {
                min=guess+1;                                     //猜的數字比答案小時,更改最小範圍
            }
        }
    }
    while(a!=guess&&min!=max);                                   //迴圈直到猜到答案或只剩一個數字
    if(min==max)
    {
        cout<<"You lose! Answer is "<<a;                         //只剩一個數便輸了
    }
    else
    {
        cout<<"You win!";
    }
    return 0;
}
