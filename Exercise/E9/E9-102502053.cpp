#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    //call variables
    int first=0;
    int last=100;
    int number;
    int terminator=0+rand()%100;
    srand(time(NULL)); //make it to get different random number according to time change

    do
    {
        do// data validation
        {
            cout<<"Enter number("<<first<<"<=number<="<<last<<")?: ";
            cin>>number;
            if(number<first || number>last)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(number<first || number>last);

        //calculate for the range after input
        if(number<terminator)
        {
            first=number+1;
        }
        else if(number>terminator)
        {
            last=number-1;
        }

        //to check whether you win or not
        if(number==terminator)
        {
            cout<<"You win!";
        }
        else if(first==last)
        {
            cout<<"You lose! Answer is "<<terminator;
        }
    }
    while (number!=terminator && first!=last);
    return 0;
}
