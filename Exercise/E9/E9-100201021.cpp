#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;
int main()
{
    // lower bound, upper bound ,random number and input number;
    int lbd=0,ubd=100,randnum,inp=-1;
    //set random number
    srand(time(NULL));
    randnum=rand()%101;
    //
    //cout << randnum << endl;

    //gamestrat
    while(true)
    {

        cout << "Enter number(" << lbd << "<=number<=" << ubd << ")?: ";
        cin >> inp;

        //process
        if(inp>ubd || inp<lbd )
        {
            cout << "Out of range!" << endl;
        }
        else if(inp > randnum)
            ubd=inp-1;
        else if(inp < randnum)
            lbd=inp+1;

        //input = answer
        if(randnum==inp)
        {
            cout << "You win!" << endl;
            break;
        }
        //lose,
        if(lbd==ubd)
        {
            cout << "You lose! Answer is " << randnum << endl;
            break;
        }
    }
    return 0;
}
