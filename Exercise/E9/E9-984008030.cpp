#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main(){
    int number = 0;//宣告型態為int的數字，並初始化為0
    int input = 0;//宣告型態為int的輸入，並初始化為0
    int lowerBound = 0;//宣告型態為int的下限，並初始化為0
    int upperBound = 100;//宣告型態為int的上限，並初始化為100

    srand (time(NULL));//用現在時間初始化pseudo-random number generator
    number = rand() % 101;//number得到介於0~100的正整數
    while(1){
        cout << "Enter number(" << lowerBound << "<=number<=" << upperBound << ")?: ";
        cin >> input;
        if(input < lowerBound || input > upperBound){//檢查輸入是否超出範圍
            cout << "Out of range!" << endl;
        }
        else{
            if(input == number){//當輸入等於答案
                cout << "You win!" << endl;
                break;
            }
            else{//當輸入不等於答案
                if(input > number){//輸入大於答案則上限為輸入減一
                    upperBound = input - 1;
                }
                else{//輸入小於答案則下限為輸入加一
                    lowerBound = input + 1;
                }
                if(lowerBound == number && upperBound == number){
                    //如果答案等於上限和下限 輸掉
                    cout << "You lose! Answer is " << number << endl;
                    break;
                }
            }
        }
    }
    return 0;
}
