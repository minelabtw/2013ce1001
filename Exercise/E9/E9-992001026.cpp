#include <iostream>
#include <ctime>
#include <cstdlib>


using namespace std ;

int main ()
{
    int a , b , n , m;
    srand( static_cast<unsigned>( time(NULL) ) );
// 設定變數為 1-100
    m = rand()%100 +1 ;
    a = 0;
    b = 100 ;
    cout << "Enter number("<< a <<"<=number<=" << b << ")?: " ;
    cin >> n ;

//判斷 假如 n 不等於終極密碼
    while ( n != m )
    {

        while ( n > b || n < a )
        {
            cout << "Out of range!" << endl ;
            cout << "Enter number("<< a <<"<=number<=" << b << ")?: " ;
            cin >> n ;

        }

// 判斷輸入的數字 以及改變終極密碼問題的範圍
        if ( n > m && n-a != 1 )
        {
            b = n-1 ;
            cout << "Enter number("<< a <<"<=number<=" << b << ")?: " ;
            cin >> n ;
        }
        else if ( n < m && b-n != 1 )
        {
            a = n+1 ;
            cout << "Enter number("<< a <<"<=number<=" << b << ")?: " ;
            cin >> n ;
        }
        else if ( n == m+1 || n == m-1  )
        {
            cout << "You lose! Answer is " << m ;
            break ;
        }
    }
    if ( n ==  m )
    {
        cout << "You win!" ;
    }
    return 0 ;
}
