#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0));//設定每一次執行程式時出現亂數皆不同
    int ans=rand()%101;
    int a=0;//輸入
    int Max=100;//宣告上限與下限
    int Min=0;

    while(a != ans && Min != Max )
    {
        cout << "Enter number(" << Min << "<=number<=" << Max << ")?: " ;//輸出字元
        cin >> a;
        while(a<Min || a>Max)//當程式尚未執行結束時，重複以下指令
        {
            cout << "Out of range!" << endl ;
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: " ;
            cin >> a;
        }
        if (a>ans)//當輸入大於亂數，上限等於輸入-1
        {
            Max=a-1;
        }
        else if (a<ans)//當輸入大於亂數，下限等於輸入+1
        {
            Min=a+1;
        }
        if (Min == Max)//若是上限與下限相同，則顯示字元，程式結束
        {
            cout << "You lose! Answer is " << ans;
            break;
        }
        else if(a == ans)//若是輸入等於亂數，則顯示字元，程式結束
        {
            cout << "You win!";
            break;
        }

    }
    return 0;
}
