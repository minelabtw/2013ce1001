#include <iostream>
#include <cstdlib>

using namespace std;

int main(){
    int answer = rand() % 101;  //變數答案
    int input;  //變數輸入的數字
    int Min = 0;  //變數最小值
    int Max = 100;  //變數最大值

    do{
        do{  //輸入數字並判斷有無超出範圍
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: ";
            cin >> input;
            if(input > Max || input < Min){
               cout << "Out of range!" << endl;
            }
        }while(input > Max || input < Min);

        if(input > answer){  //依據輸入變更最大最小值
            Max = input - 1;
        }else if(input < answer){
            Min = input + 1;
        }

        if(Max == Min){  //判斷輸贏
            cout << "You lose! Answer is " << answer;
            input = answer;
        }else if(input == answer){
            cout << "You win!";
        }
    }while(input != answer);

    return 0;
}
