#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int main(void){
  int ram,num,header = 100,footer = 0; 
  srand(time(NULL));  //rand init
  ram = (rand() % 101);  //call rand
  while(1){  //while loop
    cout << "Enter number(" << footer << "<=number<=" << header << ")?: ";
    cin >> num;
    if(!((num <= header)&&(num >= footer))){
      cout << "Out of range!\n";
    }else if(num == ram){
      cout << "You win!\n";  //end game
      break;
    }else{
      if(header == footer){
	cout << "You lose! Answer is " << ram << "\n";  //end game
	break;
      }
      ((ram > num) ? footer : header) = ((ram > num) ? (num + 1) : (num - 1)); //adjust header or footer
    }
  }
  return 0;
}
