#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;


int main()
{
    int number;//用亂數表製造一個值
    int guess;//猜的數
    int Max=100;//最大值
    int Min=0;//最小值
    srand(time(0)) ;//用時間改變亂數表
    number=rand()%101;

    cout<<"Enter number(0<=number<=100)?: ";//輸入猜測值
    cin>>guess;
    while(guess > 100 || guess < 0)//超出範圍者重新輸入
    {
        cout<<"Out of range!"<<"\nEnter number(0<=number<=100)?: ";
        cin>>guess;
    }
    while(guess!=number)//當不等於預設值.縮小範圍
    {
        if(Max-Min==2)
        {
            cout<<"You lose! Answer is "<<number;
        }
        else if(guess > Max || guess < Min)
        {
            cout<<"Out of range!"<<"\nEnter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>guess;
        }
        else if(guess < number)
        {
            Min=guess+1;
            cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>guess;
        }
        else if(guess > number)
        {
            Max=guess-1;
            cout<<"Enter number("<<Min<<"<=number<="<<Max<<")?: ";
            cin>>guess;
        }

    }
    if(guess==number)//當猜測值等於預設值 贏了
    {
        cout<<"You win!";
    }

    return 0;
}
