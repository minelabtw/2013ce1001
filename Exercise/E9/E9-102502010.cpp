#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{
    int ans,x,l,h;
    srand(time(0));
    ans=(rand()%101);
    l=0;
    h=100;
    cout<<"Enter number("<<l<<"<=number<="<<h<<")?: ";
    while(cin>>x)
    {
        while(x<l || x>h)
        {
            cout<<"Out of range!"<<endl;
            cout<<"Enter number("<<l<<"<=number<="<<h<<")?: ";
            cin>>x;
        }
        if(x==ans)
        {
            cout<<"You win!"<<endl;
            break;
        }
        else
        {
            if(ans>x)
                l=x+1;
            else if(ans<x)
                h=x-1;
            if(l==h)
            {
                cout<<"You lose! Answer is "<<ans<<endl;
                break;
            }
            else
                cout<<"Enter number("<<l<<"<=number<="<<h<<")?: ";
        }
    }
    return 0;
}
