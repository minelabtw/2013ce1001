#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    srand (time(0));
    int number = rand() % 101; //產生亂數
    int a,b;
    int M=100;
    int m=0;
    int lost=0;
    cout << "Enter number(0<=number<=100)?: ";
    cin >> a;
    while (a<m || a>M)
    {
        cout << "Out of range!" << endl ;
        cout << "Enter number(0<=number<=100)?: ";
        cin >> a;
    }
    while(true)
    {
        if (a==number)
        {
            cout << "You win!";
            break;
        }
        else if(a>M || a<m)
            cout << "Out of range!" << endl;
        else if (a<number) //讓a調整最小值
            m=a+1;
        else if (a>number) //由a調整最大值
            M=a-1;
        if (m==M)
        {
            cout <<"You lose! Answer is "<< number ;
            break;
        }
        else
        {
            cout << "Enter number(" << m <<"<=number<=" << M <<")?: ";
            cin >> a;
        }
    }
    return 0;
}
