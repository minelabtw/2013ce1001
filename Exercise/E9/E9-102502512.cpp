#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;
int main()
{
    int qus, ans;                                       //Line 7~9 define the varibles
    int control;
    int boundt=100, boundb=0;
    srand(time(0));                                     //Line 10~11 make a random number
    qus=rand()%101;
    do                                                  //line 12~19 let user enter number for first time
    {
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>ans;
        if(ans<0||ans>100)
            cout<<"Out of range!\n";
    }
    while(ans<0||ans>100);
    do                                                   //Line 20~72 let user guess the number
    {
        if(ans==qus)
        {
            cout<<"You win!";
        }
        else if((qus-ans)==1||(qus-ans)==-1)             //Line 26 to 30 when the number that user guessed has only one unit distance form answer
        {                                                //print that user lose
            cout<<"You lose! Answer is "<<qus;
            break;
        }
        else if(ans>qus&&(qus-ans)!=-1)                  //Line 31 to 70 give the user some hint when he or she doesn't guess the right number
        {
            do
            {
                cout<<"Enter number("<<boundb<<"<=number<="<<ans-1<<")?: ";
                control=ans-1;
                cin>>ans;
                if(ans<boundb||ans>control)
                {
                    cout<<"Out of range!\n";
                    ans=control+1;
                }
                else if(ans==qus)
                {
                    cout<<"You win!";
                }
            }
            while(ans<boundb||ans>control);
            boundt=control;
        }
        else if(ans<qus&&(qus-ans)!=1)
        {
            do
            {
                cout<<"Enter number("<<ans+1<<"<=number<="<<boundt<<")?: ";
                control=ans+1;
                cin>>ans;
                if(ans<control||ans>boundt)
                {
                    cout<<"Out of range!\n";
                    ans=control-1;
                }
                else if(ans==qus)
                {
                    cout<<"You win!";
                }
            }
            while(ans<control||ans>boundt);
            boundb=control;
        }
    }
    while(qus!=ans);
    return 0;
}
