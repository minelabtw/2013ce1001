#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    int a=0,b=0,c=0,d=100;//宣告4個數字
    srand (time(0));//設隨機變數
    a=rand()%101;
    cout<<"Enter number(0<=number<=100)?: ";
    cin>>b;
    for(; b>100||b<0;)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter number(0<=number<=100)?: ";
        cin>>b;
    }
    for(; a!=b&&c!=d;)
    {
        if(b<a)
            c=b+1;
        if(a<b)
            d=b-1;
        if(c!=d)
        {
            cout<<"Enter number("<<c<<"<=number<="<<d<<")?: ";
            cin>>b;
            for(; b>d||b<c;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"Enter number("<<c<<"<=number<="<<d<<")?: ";
                cin>>b;
            }
        }
        if(c==d)
            cout<<"You lose! Answer is "<<c;
    }
    if(a==b)//猜對
    {
        cout<<"You win!";
    }
    return 0;
}
