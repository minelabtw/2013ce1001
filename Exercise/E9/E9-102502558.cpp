#include <iostream>
#include <cstdlib>

using namespace std;

// input function
void input(int &n, const int &range_min, const int &range_max)
{
	// show ui
	do
	{
		cout << "Enter number(" << range_min << "<=number<=" << range_max << ")?: ";
		cin >> n;
	} while ((n < range_min || n > range_max) && cout << "Out of range!" << endl);
}

int main()
{
	// init random seed
	srand(time(NULL));
	
	int ans = rand() % 101; // random number
	int guess = -1;
	int _min = 0;
	int _max = 100;
	bool win = true; // flag
	while (guess != ans)
	{
		input(guess, _min, _max);
		if (guess > ans)
			_max = guess - 1;
		else if (guess < ans)
			_min = guess + 1;
		if (_min == _max)
		{
			cout << "You lose! Answer is " << _min << endl;
			win = false;
			break;
		}
	}
	// check win
	if (win)
		cout << "You win!" << endl;
	return 0;
}
