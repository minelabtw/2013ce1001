#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int a,min=0,max=100,b,c=0;

    srand(time(NULL));                                                                              //亂數
    a=(rand()%100)+1;

    while(c==0)                                                                                     //讓他持續做
    {
        cout<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
        cin>>b;
        while(b>max || b<min)                                                                       //判斷是否在範圍內
        {
            cout<<"Out of range!"<<endl<<"Enter number("<<min<<"<=number<="<<max<<")?: ";
            cin>>b;
        }                                                                                           //判斷各情況
        if(b>a)
        {
            max=b-1;
        }
        if(b<a)
        {
            min=b+1;
        }
        if(b==a)
        {
            cout<<"You win!";
            c=1;
        }
        if(min==a && max==a)
        {
            cout<<"You lose! Answer is "<<a;
            c=1;
        }
    }
    return 0;
}
