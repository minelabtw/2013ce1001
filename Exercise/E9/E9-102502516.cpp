#include <iostream>
#include <cstdlib>
#include <ctime>
#include <limits>

using namespace std;

int main()
{
    srand (time (0));   //用時間當亂數種子
    int userGuess , Min=0, Max=100 , number=rand()%101;     //userGuess是使用者每次猜的數字，Min是最小值，Max是最大值，number是這次遊戲的密碼
    do
    {
        do      //這個回圈用來取得正確的使用者猜測數字
        {
            cout << "Enter number(" << Min << "<=number<=" << Max << ")?: " ;
            cin >> userGuess;
            if (userGuess>Max || userGuess<Min || !(cin))   //若比Min小、比Max大、或是非整數時
            {
                cin.clear();        //先清除cin的錯誤狀態
                cin.ignore(numeric_limits<streamsize>::max(), '\n');        //刪除緩衝區所有的資料
                cout << "Out of range!\n";      //提示錯誤
            }
        }
        while (userGuess>Max || userGuess<Min || !(cin));

        if (userGuess<number)
            Min = userGuess+1;      //猜的數字比密碼小時，Min更改
        if (userGuess>number)
            Max = userGuess-1;      //反之更改Max
        if (userGuess==number)
            cout << "You win!";     //猜中了~
        if (Max==Min)
            cout << "You lose! Answer is " << number;   //最大等於最小就沒啥好猜了
    }
    while (userGuess!=number && Max!=Min);
    return 0;
}
