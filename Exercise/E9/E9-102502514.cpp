#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
    int number;
    int top=100;  //設定最大值初始值為100
    int bottom=0;  //設定最小值初始值為0
    srand(time(0));
    int randNum=rand()%101;  //宣告亂數為0~100的整數
    cout <<"Enter number(0<=number<=100)?: ";
    cin >>number;
    while (number<0||number>100)
    {
        cout <<"Out of range!\nEnter number(0<=number<=100)?: ";
        cin >>number;
    }
    while (number!=randNum)
    {
        if (0<randNum&&randNum<number)
        {
            top=number-1;  //如果randNum<number,將number設定為最大值,繼續猜
            if (top==bottom)
            {
                cout <<"You lose! Answer is "<<randNum;  //當最大值等於最小值,印出遊戲失敗,並跳出遊戲,否則繼續猜測
                break;
            }
            else
            {
                cout <<"Enter number("<<bottom<<"<=number<="<<top<<")?: ";
                cin >>number;
            }
        }
        if (number<randNum&&randNum<=100)
        {
            bottom=number+1;  //如果randNum>number,將number設定為最小值,繼續猜
            if (top==bottom)
            {
                cout <<"You lose! Answer is "<<randNum;  //當最大值等於最小值,印出遊戲失敗,並跳出遊戲,否則繼續猜測
                break;
            }
            else
            {
                cout <<"Enter number("<<bottom<<"<=number<="<<top<<")?: ";
                cin >>number;
            }
        }
        if (number<bottom||number>top)  //當輸入的number在任一時刻不符合最大最小值所在的區間,則印出Out of range!
        {
            cout <<"Out of range!"<<endl;
            cout <<"Enter number("<<bottom<<"<=number<="<<top<<")?: ";
            cin >>number;
        }
    }
    if (number==randNum)  //當number==randNum則跳出while迴圈,並印出遊戲勝利
        cout <<"You win!";

    return 0;
}
