#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;
int main()
{
    int answer = rand()%101; //跑出隨機數字
    int number; //宣告
    int a=100;
    int b=0;
    do //執行
    {
        cout<<"Enter number("<<b<<"<=number<="<<a<<")?: "; //輸出
        cin>>number; //輸出
        if(number<b || number>a) //條件
            cout<<"Out of range!\n";
        if(number>answer && number<a)
            a=number-1;
        if(number<answer && number>b)
            b=number+1;
        if(number==answer)
        {
            cout<<"You win!";
            break; //跳出迴圈
        }
        if(a==b)
            cout<<"You lose! Answer is "<<answer;
    }while(a!=b);
    return 0;
}
