#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    srand(time(0));
    int up_limit=100;
    int down_limit=0;
    int number=rand()%101;  //randon number in the range [0,100]
    int x=0;                //input from user

    do
    {
        if (up_limit==down_limit)
        {
            cout << "You lose! Answer is " << number;  //user is lose while the range have only a number
            x=number;       //end while
        }
        else
        {
            cout << "Enter number(" << down_limit << "<=number<=" << up_limit << ")?: ";
            cin >> x;
            if (x==number)
                cout << "You win!" << endl;         //user is win while input is number
            else if (x<number&&x>=down_limit)
                down_limit=x+1;   //change the down limit while user guess wrong number and the range have still two number or above
            else if (x>number&&x<=up_limit)
                up_limit=x-1;     //change the up limit while user guess wrong number and the range have still two number or above
            else
                cout << "Out of range!" << endl;    //reprompt x while input is out of range [down_limit,up_limit]
        }
    }
    while (x!=number);  //end while if user get number value not matter he is win or lose

    return 0;
}
