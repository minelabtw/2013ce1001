#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
    srand(time(0)); //隨機設定一個整數
    int mini=0;
    int maxi=100;
    int guess;
    int number = rand()%101; //隨機設定number，範圍為0<=number<=100。
    do
    {
        cout << "Enter number(" << mini << "<=number<=" << maxi << ")?: "; //玩家輸入整數來猜數字
        cin >> guess;
        while (guess<mini||guess>maxi) //輸入超出提示字的範圍則顯示”Out of range!”。
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << mini << "<=number<=" << maxi << ")?: ";
            cin >> guess;
        }
        if (guess>number) //若沒猜中就會更改提示字範圍並繼續遊戲
            maxi = guess-1;
        else
            mini = guess+1;
    }
    while (guess!=number&&maxi!=mini);
    if (guess==number) //若猜中就顯示”You win!”
        cout << "You win!";
    else //若剩一個數字可猜，則直接顯示”You lose! Answer is ”和正確答案
        cout << "You lose! Answer is " << number;
    return 0;
}
