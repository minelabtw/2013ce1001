#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    int b=0;
    int c=100;
    int a;
    int d; //設定變數
    srand(time(0)); //產生一個隨機數字
    a=rand()%101+1; //使他介於1~100間

    do //開始猜
    {
        if (b!=c) //如果兩數不一樣就繼續猜
        {
            cout<<"Enter number("<<b<<"<=number<="<<c<<")?: ";
            cin>>d;
        }
        else //兩數一樣就輸了
        {
            cout<<"You lose! Answer is "<<b;
            break;
        }
        if (d>c||d<b)
        {
            cout<<"Out of range!"<<endl;
        }
        else if (d>a)
        {
            c=d-1;
        }
        else if (d<a)
        {
            b=d+1;
        }
        else if (d=a) //猜對就贏了
        {
            cout<<"You win!";
            break;
        }
    }
    while (d!=a||b!=c);

    return 0;
}
