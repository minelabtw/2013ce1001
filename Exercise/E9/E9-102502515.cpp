#include <iostream>
#include <cstdlib>//use rand/srand
#include <ctime>//use time

using namespace std;

int main()
{
    int answer;//the number of srand
    int a;//input
    int Rmax=100;//declare Range max
    int Rmin=0;//declare Range min

    srand (time(0));//use srand to produce a uncertain number
    answer = rand()%101;//make the rand in the range of 0 to 100

    cout << "Enter number(0<=number<=100)?: " ;
    cin  >> a;

    while (a<0||a>100)//use condition to make the input in the range
    {
        cout << "Out of range!" << endl;
        cout << "Enter number(0<=number<=100)?: " ;
        cin  >> a;
    }
    while (a!=answer&&Rmax!=Rmin)//if input is not equal to the answer,then repeat
    {
        while (a<Rmin || a>Rmax)//use a condition to make input in the range
        {
            cout << "Out of range!" << endl;
            cout << "Enter number(" << Rmin << "<=number<=" << Rmax << ")?: ";
            cin  >> a;
        }

        if (a<answer)
        {
            Rmin = a+1;//make the input be the Range min
        }
        else if (a>answer)
        {
            Rmax = a-1;//make the input be the Range max
        }
        if(Rmax!=Rmin)
        {
            cout << "Enter number(" << Rmin << "<=number<=" << Rmax << ")?: ";//output the sentence and the new range
            cin  >> a;
        }
    }
    if (a==answer)
    {
        cout << "You win!" ;//output the sentence and the answer
    }
    else if (Rmin==Rmax)
    {
        cout << "You lose! Answer is " << answer ;
    }
    return 0;
}
