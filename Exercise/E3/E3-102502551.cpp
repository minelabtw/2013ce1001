#include <iostream>

using namespace std;

int main()
{
    int a,b;


    cout<<"Complete this linear function and draw a diagram."<<endl<<"f(x)=ax+b.(a=/=0)"<<endl<<"a:";
    cin>>a;
    while(a<-10 || a>10 || a==0)
        {
            cout<<"illegal input!"<<endl;
            cout<<"a:";
            cin>>a;
        }
    cout<<"b:";
    cin>>b;
    while(b<-10 || b>10)
        {
            cout<<"illegal input!"<<endl;
            cout<<"b:";
            cin>>b;
        }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;
    for(int y=10;-10<=y;y--)
        {
            for(int x=-10;x<=10;x++)
            {
                if (y==a*x+b)
                {
                    cout<<"*";
                }
                else if (x==0 && y==0)
                {
                    cout<<"+";
                }
                else if (x==0)
                {
                    cout<<"|";
                }
                else if (y==0)
                {
                    cout<<"-";
                }
                else
                {
                    cout<<" ";
                }
            }
            cout<<endl;

        }
    return 0;
}
