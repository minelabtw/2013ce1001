#include<iostream>
using namespace std;

int main()
{
    int xMax = 10;
    int yMax = 10;
    int a = 0;
    int b = 0;
    int x = -10;
    int y = -10;

    cout <<"Complete this quadratic function and draw a diagram"<<endl;
    cout <<"f(x)=ax+b   a=/=0"<<endl;
    cout <<"a=";
    cin >> a;
    while (a>10 || a<-10)                            //判定a的範圍是否介於10和-10
    {
        cout <<"illegal input!"<<endl;
        cout <<"a=";
        cin >> a;
    }
    cout <<"b=";
    cin >> b;
    while (b>10 || b<-10)                            //判定a的範圍是否介於10和-10
    {
        cout <<"illegal input!"<<endl;
        cout <<"b=";
        cin >>b;
    }
    cout <<"f(x)="<<a<<"x+"<<b<<endl;


    while(y<=yMax)
    {
        while(x<=xMax)
        {
            if (y==a*x+b)                            //當y為函數值時輸出"*"
                cout <<"*";
            else if (x==0 && y==0)                   //當原點時輸出"+"
                cout << "+";
            else if (x==0)                           //當為y軸時輸出"|"
                cout << "|";
            else if (y==0)                           //當為x軸時輸出"-"
                cout << "-";
            else                                     //除此之外都輸出空白
                cout << " ";
            x++;                                     //將x的值+1
        }
        cout << endl;
        x = -10;
        y++;

    }
    return 0;

}
