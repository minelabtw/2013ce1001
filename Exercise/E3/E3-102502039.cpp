#include<iostream>
using namespace std;
int main()
{
    int a,b;//宣告型別為整數(int)的a,b。
    cout<<"Complete this quadratic function and draw a diagram."<<endl<<"f(x)=ax+b.(a=/=0)"<<endl<<"a:"<<endl;
    cin>>a;//輸出"Complete this quadratic function and draw a diagram."和 "f(x)=ax+b.(a=/=0)" 以及"a:"。
    while (a==0 || a<-10 || a>10)//當輸入的a=0 或a<-10或a>10時，
    {
        cout<<"illegal input!"<<endl<<"a:";//輸出"illegal input!"。
        cin>>a;//輸入a的值。
    }
    cout<<"b:"<<endl;//輸出"b:"。
    cin>>b;//輸入b的值。
    while (b<-10 || b>10)//當輸入的b<-10或b>10時，
    {
        cout<<"illegal input!"<<endl<<"b:";//輸出"illegal input!"。
        cin>>b;//輸入b的值。
    }
    cout << "f(x)=" << a << "x" << "+" << b << "." << "(a=/=0)" << endl;//輸出"f(x)=" "x" "+" "." "(a=/=0)"。
    int xMax=10;//宣告型別為整數(int)的xMax，並初始化其數值為10。
    int yMin=-10;//宣告型別為整數(int)的yMax，並初始化其數值為-10。
    int x=-10;//宣告型別為整數(int)的x，並初始化其數值為-10。
    int y=10;//宣告型別為整數(int)的y，並初始化其數值為10。
    while(y>=yMin)//當y>=yMin時，
    {
        if (y==0)//若y=0
        {
            while(x<=xMax)//當x<=xMax
            {
                if (y==a*x+b)//若y=ax+b
                    cout <<"*";//輸出"*"
                else if (x==0)//若x=0
                {
                    cout<<"+";//輸出"+"
                }
                else//否則
                {
                    cout<<"-";//輸出"-"
                }
                x++;
            }
        }else{
            while(x<=xMax)//當x<=xMax
            {
                if (y==a*x+b)//若y=ax+b
                    cout <<"*";//輸出"*"
                else if (x==0)//若x=0
                {
                    cout<<"|";//輸出"|"
                }
                else//否則
                {
                    cout<<" ";//輸出空白
                }
                x++;
            }
        }

        cout<<endl;
        x=-10;
        y--;

    }





    return 0;
}
