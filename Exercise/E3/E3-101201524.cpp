#include<iostream>
using namespace std;

int main()
{
    int y = 10,                                                     //宣告四個變數y,ymin,x,xmax分別用來設定顯示時的x,y軸的上,下,左,右的邊界
        ymin = -10,                                                 //設定x的顯示範圍為-10~10,y是-10~10
        x = -10,
        xmax = 10,
        a,b;                                                        //宣告兩個變數a,b來接收之後輸入的方程式的兩個整數項系數

    cout << "Complete this linear function and draw a diagram.\n";  //將字串"Complete this linear function and draw a diagram."輸出，並空行
    cout << "f(x)=ax+b.(a=/=0)\n";                                  //將字串"f(x)=ax+b.(a=/=0)\n"輸出，並空行
    cout << "a: ";                                                  //將字串"a: "輸出
    cin >> a;                                                       //將輸入的數字宣告給a

    while( a>10 || a<-10 || a==0 )                                  //當輸入的a不在設定的範圍(-10~10,a=/=0)時，執行以下動作
    {
        cout << "illegal input!\n";                                 //將字串"illegal input!"輸出,並空行,再輸出"a: "，最後重新接收一次新變數給a
        cout << "a: ";
        cin >> a;
    }

    cout << "b: ";                                                  //將字串"b: "輸出
    cin >> b;                                                       //將輸入的數字宣告給b

    while( b>10 || b<-10 )                                          //同a的方法,當輸日的數字不在範圍內實執行以下動作
    {
        cout << "illegal input!\n";                                 //將字串"illegal input!"輸出,並空行,再輸出"b: "，最後重新接收一次新變數給b
        cout << "b: ";
        cin >> b;
    }
    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)\n";              //將字串"f(x)=ax+b.(a=/=0)\n"輸出(a,b是顯示a,b所宣告到的數字)，並空行

    while(y>=ymin)                                                  //當新的y(10以下)>=ymin=-10時(-10~10之間時)執行以下動作
    {
        while(x<=xmax)                                              //當新的x(-10以上)<=xmin=10時(-10~10之間時)執行以下動作，形成一個座標系統
        {
            if(x==0 & y==0)                                         //若座標在原點(0,0)顯示"+"
                cout << "+";
            else if(y==a*x+b)                                       //若座標在方程式y=ax+b上，顯示"*"
                cout << "*";
            else if(x==0)                                           //若座標在y軸上，顯示"|"
                cout << "|";
            else if(y==0)                                           //若座標在x軸上，顯示"-"
                cout << "-";
            else                                                    //若以上皆非(不是特別的座標)則空白
                cout << " ";
            x=x+1;                                                  //做完以上判斷後，移到下一個座標(y,x+1)繼續做判斷直到x=10
        }
        cout << "\n";                                               //判斷完一排的x後空到下一行(y=y-1)，x重新回歸到-10(x=-10)，繼續做判斷直到y=-10
        x=-10;
        y=y-1;

    }

    return 0;
}
