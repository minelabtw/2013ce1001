#include<iostream>
using namespace std;

int main()
{
    cout << "Complete this linear function and draw a diagram.\nf(x)=ax+b.(a=/=0)" << endl;    //將Complete this linear function and draw a diagram.\nf(x)=ax+b.(a=/=0)輸出至螢幕。

    int x=-10;    //宣告一變數x其值=-10。
    int y=10;     //宣告一變數y其值=10。
    int a=0;      //宣告一變數a其值=0。
    int b=0;      //宣告一變數b其值=0。

    cout << "a=";    //將a=輸出至螢幕。
    cin >> a;        //將值輸入至a變數。

    while(a == 0 || a > 10 || a < -10)    //while迴圈限制範圍在a=0或a>10或a<-10。
    {
        cout << "illegal input!" << endl;    //將illegal input!輸出至螢幕並換行。
        cout << "a=";    //將a=輸出至螢幕。
        cin >> a;        //將值輸入至a變數。
    }

    cout << "b=";    //將b=輸出至螢幕。
    cin >> b;        //將值輸入至b變數。

    while(b > 10 || b < -10)    //while迴圈限制範圍在b>10或b<-10。
    {
        cout << "illegal input!" << endl;    //將illegal input!輸出至螢幕並換行。
        cout << "b=";    //將b=輸出至螢幕。
        cin >> b;        //將值輸入至b變數。
    }

    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;    //將f(x)=ax+b.(a=/=0)輸出至螢幕。

    int xMAX=10;     //宣告一變數xMAX其值為10。
    int xMIN=-10;    //宣告一變數xMIN其值為-10。
    int yMAX=10;     //宣告一變數yMAX其值為10。
    int yMIN=-10;    //宣告一變數yMIN其值為-10。

    while(y >= yMIN)    //while迴圈限制範圍不可小於yMIN。
    {
        while(x <= xMAX)    //while迴圈限制範圍不可大於xMAX。
        {
            if(y == a*x+b)    //運算
            {
                cout << "*";
                ++x;
            }
            else if(x == 0 && y == 0)
            {
                cout << "+";
                ++x;
            }

            else if(x == 0)
            {
                cout << "|";
                ++x;
            }

            else if(y == 0)
            {
                cout << "-";
                ++x;
            }

            else
            {
                cout << " ";
                ++x;
            }
        }
        cout << endl;
        x=-10;
        y--;
    }
    return 0;
}
