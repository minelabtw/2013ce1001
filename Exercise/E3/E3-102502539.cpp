#include <iostream>

using namespace std;

int main()
{
    int x = -10;    //宣告變數 x
    int xMax = 10;
    int y = -10;    //宣告變數 y
    int yMax = 10;
    int a , b;      //宣告變數 a , b

    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b. (a=/=0)" << endl;
    cout << "a: " ;
    cin >> a;
    while ( a > 10 || a < -10 ) //限制 a 範圍
    {
        cout << "illegal input!\n" << "a: " ;
        cin >> a;
    }
    cout << "b: ";
    cin >> b;
    while ( b > 10 || b < -10 ) //限制 b 範圍
    {
        cout << "illegal input!\n" << "b: " ;
        cin >> b;
    }
    cout << "f(x)=" << a << "x+" << b << ". (a=/=0)" << endl << endl;

    while ( y <= yMax )
    {

        while ( x <= xMax )
        {
            if ( -y == a * x + b )
                cout << "*";
            else if ( x == 0 && y == 0 )
                cout << "+";    //原點
            else if ( x == 0 )
                cout << "|";    // x 軸
            else if ( y == 0 )
                cout << "-";    // y 軸
            else
                cout << " ";

            x++;
        }
        cout << endl;
        x = -10;
        y++;
    }

    return 0;
}





