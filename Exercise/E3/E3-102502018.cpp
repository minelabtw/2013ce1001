#include <iostream>
using namespace std;

int main()
{
    int x=-10;                 //宣告變數X初始值為-10
    int y=10;                  //宣告變數y初始值為10
    int a=0;                   //宣告變數a初始值為0
    int b=0;                   //宣告變數b初始值為0

    cout<<"Complete this quadratic function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)\n";
    cout<<"a:";
    cin >>a;
    while(a>10 || a<-10 || a==0)                 //迴圈判斷-10<=a<=10 且a不等於0
    {
       cout<<"illegal input!\n";
       cout<<"a:";
       cin>>a;
    }
    cout<<"b:";
    cin>>b;
    while(b>10||b<-10)                           //迴圈判斷-10<=b<=10
    {
       cout<<"illegal input!\n";
       cout<<"b:";
       cin>>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)\n";

    while(y>=-10 && y<=10)                       //用迴圈跑y從-10到10
    {
        while(x<=10 && x>=-10)                   //用迴圈跑x從10~-10
        {
            if(y==a*x+b)                         //判斷該位置輸出的符號
            {
                cout<<"*";
            }
            else if(x==0 && y==0)
            {
                cout<<"+";
            }
            else if(x==0)
            {
                cout<<"|";
            }
            else if(y==0)
            {
                cout<<"-";
            }
            else
            {
                cout<<" ";
            }
            x++;      //x等於x+1
        }
        cout<<endl;
        x=-10;        //使x的值回到-10
        y--;          //y等於y+1
    }

}
