
#include <iostream>

using namespace std;

int main()
{
 int a=0,b=0,x=-10,y=10;
 cout<<"Complete this linear function and draw a diagram."<<endl;//從螢幕印出句子
 cout<<"f(x)=ax+b.(a=/=0)"<<endl;
 cout<<"a:";
 cin>>a;                                                         //從鍵盤輸入a
 cout<<endl;

 while(a<-10||a==0||a>10)                          //規定a的範圍在(-10,10)但不等於0
 {
  cout<<"illegal input!"<<endl;                    //若a超出此範圍
  cout<<"a:";                                      //輸出 illegal input!
  cin>>a;
  cout<<endl;
 }
  cout<<"b:";
  cin>>b;

 while(b<-10||b>10)
 {
  cout<<"illegal input!"<<endl;
  cout<<"b:";
  cin>>b;
  cout<<endl;
 }
 cout<<"f(x)="<<a<<"x+"<<b<<"(a=/=0)"<<endl;        //輸出方程式

 while (y>=-10)                                     //設+號處為原點
 {                                                  //所以從最左上(x=-10,y=10)
  while(x<=10)                                      //開始輸出
  {
    if(y==a*x+b)                                    //如果在方程式上
     cout<<"*";                                     //印出*
    else if(x==0&&y==0)                             //如果在原點
     cout<<"+";                                     //印出+
    else if(x==0)                                   //印出|做出y軸
     cout<<"|";
    else if(y==0)                                   //印出-做出x軸
     cout<<"-";
    else
     cout<<" ";
    x++;
  }

  cout<<endl;
  x=-10;
  y--;
  }
  return 0;
}
