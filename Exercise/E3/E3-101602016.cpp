#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a;
    int b;
    cout << "Complete this quadratic function and draw a diagram." << endl
    << "f(x)=ax+b.(a=/=0)" << endl << "a:" ;//顯示函式以及要求輸入a

    cin >> a ;

    while (a < -10 || a > 10 )
    {
        cout << "illegal input!" << endl <<  "a:";
        cin >> a;
    }
    //給定a的範圍，如果超出則顯示"illegal input!"

    cout << "b:" ;
    cin >> b ;
    while (b < -10 || b > 10)
    {
        cout << "illegal input!" << endl <<  "b:";
        cin >> b;
    }
    cout << "f(x)=" << a << "x" << "+" << b << "." << "(a=/=0)" << endl;
    //b同理a

    int xMax=10;
    int yMax=10;
    int x=-10;
    int y=-10;

    while(y<=yMax)
    {
       while(x<=xMax)
       {
           if (-y==a*x+b)//因為y從上往下數，所以藥加負號
           {
               cout <<"*";//當函式等號成立時，打出星號
           }
           else if (y==0 && x==0)
                cout << "+";//設定原點為+
           else if (y==0)
                cout << "-";//畫出X軸
           else if (x==0)
                cout << "|";//畫出Y軸
           else
                cout << " ";//填滿空格
        x++;//將X加一
       }
       cout << endl;//換行
        x=-10;//重新定義x的起始值
        y++;//將y加一
    }
    return 0;
}

