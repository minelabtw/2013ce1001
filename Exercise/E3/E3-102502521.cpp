#include<iostream>
using namespace std;

int main()
{
    int x=-10;           //宣告名稱為x的整數變數,將其值設為-10
    int y=10;            //宣告名稱為y的整數變數,將其值設為10
    int a=0;             //宣告名稱為a的整數變數,將其值設為0
    int b=0;             //宣告名稱為b的整數變數,將其值設為0
    int xMax=10;         //宣告名稱為xMax的整數變數,將其值設為10
    int yMax=-10;        //宣告名稱為yMax的整數變數,將其值設為-10

    cout<<"Complete this linear function and draw a diagram"<<endl; //將字串"Complete this linear function and draw a diagram"輸出到螢幕上並換行
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;       //將字串"f(x)=ax+b.(a=/=0)"輸出到螢幕上並換行

    cout<<"a: ";                           //將字串"a: "輸出到螢幕上
    cin>>a;                                //從鍵盤輸入，並將輸入的值給a
    while(a==0||a>10||a<-10)               //使用while迴圈，若a數值小於-10或大於10或等於0就會一直重複該程式碼內的動作
    {
        cout<<"illegal input!"<<endl;      //將字串"illegal input!"輸出到螢幕上並換行
        cout<<"a: ";                       //將字串"a: "輸出到螢幕上
        cin>>a;                            //從鍵盤輸入，並將輸入的值給a
    }

    cout<<"b: ";                           //將字串"b: "輸出到螢幕上
    cin>>b;                                //從鍵盤輸入，並將輸入的值給b
    while(b>10||b<-10)                     //使用while迴圈，若b數值小於-10或大於10就會一直重複該程式碼內的動作
    {
        cout<<"illegal input!"<<endl;      //將字串"illegal input!"輸出到螢幕上並換行
        cout<<"b: ";                       //將字串"b: "輸出到螢幕上
        cin>>b;                            //從鍵盤輸入，並將輸入的值給b
    }

    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;  //將字串"f(x)=a之值x+b之值.(a=/=0)"輸出到螢幕上並換行

    while(y>=yMax)                         //使用while迴圈，若y大於等於yMax就會一直重複該程式碼內的動作
    {
        while(x<=xMax)                     //使用while迴圈，若x小於等於xMax就會一直重複該程式碼內的動作
        {
            if(x==(y-b)/a)                 //利用if,else if,else判斷什麼地方應該放什麼符號
            {
                cout<<"*";                 //將字串"*"輸出到螢幕上
            }
            else if(y==0)
            {
                if(x==0)
                {
                    cout<<"+";             //將字串"+"輸出到螢幕上
                }
                else
                {
                    cout<<"-";             //將字串"-"輸出到螢幕上
                }
            }
            else if(x>0||x<0)
            {
                cout<<" ";                 //將字串" "輸出到螢幕上
            }
            else
            {
                cout<<"|";                 //將字串"|"輸出到螢幕上
            }
            x++;                           //讓x之值遞增,公差為1
        }
        cout<<endl;                        //換行
        x=-10;                             //重新設定x為-10
        y--;                               //讓y之值遞減,公差為1
    }
    return 0;
}
