#include <iostream>

using namespace std;

int main()
{
    int a;//變數a
    int b;//變數b
    cout << "Comlete this quadratic function and draw a diagram." << endl;//請使用者完成方程式
    cout << "f(x)=ax+b.(a=/=0)" << endl;//輸出方程式格式
    do{
        cout << "a: ";//填入數字a
        cin >> a;//輸入數字a
        if(a>10||a<-10){//當a超出範圍
            cout << "illegal input!" << endl;//提醒使用者超出範圍
        }
    }while(a>10||a<-10);//檢查a有無超出範圍
    do{
        cout << "b: ";//填入數字b
        cin >> b;//輸入數字b
        if(b>10||b<-10){//當b超出範圍
            cout << "illegal input" << endl;//提醒使用者超出範圍
        }
    }while(b>10||b<-10);//檢查b有無超出範圍

    cout << "f(x)=" << a << "x" << (b>0?"+":"-") << (b>0?b:-b) << ".(a=/=0)" << endl;//輸出完整方程式
    int x = -10;//設定x初始值
    int y = 10;//設定y初始值

    for(y=10;y>-10;y--){
        for(x=-10;x<=10;x++){//輸出圖形
            if(y==a*x+b){
                cout << "*";//如果y值等於a乘x加b就輸出*
            }else if(y==0&&x==0){
                cout << "+";//如果y值等於0而且x值等於0就輸出+
            }else if(y==0){
                cout << "-";//如果y值等於0就輸出-
            }else if(x==0){
                cout << "|";//如果x值等於0就輸出|
            }else{
                cout << " ";//如果都不是就輸出空白
            }
            if(x==10){
                cout << endl;//如果x等於10換行
            }
        }
    }
    return 0;
}
