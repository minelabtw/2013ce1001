#include<iostream>
using namespace std;
int main()
{
    int a=0;
    int b=0;
    int i=0;
    int j=0;
    cout<<"Complete this quadratic function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;
    cout<<"a: ";  //輸入a
    cin>>a;
    while(a<-10||a>10||a==0) //輸入的a大於10或小於-10或=0,則輸出illegal input
    {
        cout<<"illegal input!"<<endl;
        cout<<"a: ";  //重新輸入a
        cin>>a;
    }
    cout<<"b: ";  //輸入b
    cin>>b;
    while(b<-10||b>10) //輸入的b大於10或小於-10,則輸出illegal input
    {
        cout<<"illegal input!"<<endl;
        cout<<"b: ";  //重新輸入b
        cin>>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;
    for(j=10;j>=-10;j--) //j從10開始直到-10,迴圈共進行21次
    {
        for(i=-10;i<=10;i++) //i從-10開始直到10,迴圈共進行21次
        {
            if ( a*i+b == j ) //若符合直線y=ax+b則輸出*
                cout << "*" ;
            else if(j==0&i==0) //原點輸出+
                cout << "+" ;
            else if(j==0) //x軸輸出-
                cout << "-" ;
            else if(i==0) //y軸輸出|
                cout << "|" ;
            else
                cout <<" " ; //其他都空白
        }
        cout << endl ; //執行完一排x就換行,共換行21次
    }
    return 0;
}
