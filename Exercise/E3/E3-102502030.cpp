#include <iostream>
using namespace std;
int main()
{
    int ymin=-10; //y的最小值
    int xmax=10;  //x的最大值
    int y=10;  //y的初始值
    int x=-10;  //x的初始值
    int a;  //斜率
    int b;  //垂直平移


    cout<<"Complete this quadratic function and draw a diagram.\n";  //輸入提示
    cout<<"f(x)=ax+b.(a=/=0)\n";
    cout<<"a: ";
    cin>>a;
    while(a<-10 || a>10 || a==0)  //檢測是否符合
       {
        cout<<"illegal input!\n";
        cin>>a;
       }
    cout<<"b: ";
    cin>>b;
     while(b<-10 || b>10)
       {
        cout<<"illegal input!\n";
        cin>>b;
       }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)\n";


    while(y>=ymin) //從上往下
    {
        while(x<=xmax)  //從左往右
            {
                if(x==(y-b)/a && (y-b)%a==0)  //顯示函數
                {
                cout<<"*";
                x++;
                }
                else if(x==0 && y!=0)  //顯示x軸
                {
                cout<<"|";
                x++;
                }
                else if(y==0 && x!=0)  //顯示y軸
                {
                 cout<<"-";
                 x++;
                }
                else if(x==0 && y==0)//顯示原點
                {
                 cout<<"+";
                 x++;
                }
                else  //空格補滿
                {cout<<" ";
                x++;
                }
            }


        cout<<"\n";
        x=-10;  //至最左後向下一行
        y--;

    }



    return 0;



}
