#include <iostream>
using namespace std;

int main()
{
    int a;      //使用者定義的斜率
    int b;      //使用者定義的y截距
    int y;      //f(x)的值，同時也拿來當作迴圈的計數器
    int x;      //方程式中的x
    string line("          |          ");       //除了x軸以外的每一列
    string x_axis ("----------+----------");    //x軸以及原點
    string line_y = line;       //宣告一個與line相同的變數，使其在每次被改變後能夠藉由line_y = line還原

    cout<<"Complete this quadratic function and draw a diagram."<<endl<<
        "f(x)=ax+b.  (a=/=0)"<<endl<<
        "a: ";
    cin >> a;               //印出題目，以及請使用者輸入斜率a

    while(a==0 || a>10 || a< -10)
    {
        cout << "Illegal input!" << endl << "a: ";
        cin >> a;
    }                       //判斷a值是否符合規則

    cout << "b: ";
    cin >>b;               //請使用者輸入截距b
    while (b==0 || b>10 || b< -10 )
    {
        cout << "Illegbl input!" << endl << "b: ";
        cin >> b;
    }                       //判斷b值是否符合規則

    if (b<0)
        cout << "f(x)="<< a<<"x"<<b;
    else
        cout << "f(x)="<< a<<"x+"<<b;
    cout << ".  (a=/=0)"<< endl<< endl; //使b為負值時也能正確顯示（若a=1,b=-3則顯示f(x)=1x-3而非f(x)=1x+-3）


    y=10;       //讓函數圖形從y=10開始往y負方向印出

    while(y>=-10)   //讓圖形印到y=-10就結束
    {
        x=(y-b)/a;  //y=ax+b的移項
        y--;        //計數器y每次減1
        x=x+11;     //由於是從最左邊開始數，加上原點及x負共11單位，使x的意義變成從左向右數的第幾個

        if (y == 0)     //發生在x軸上
        {
            x_axis.replace(x,1,"*");
            cout << x_axis << endl;     //把*替換到相對應的位置，並印出
        }
        else            //在x軸以外的地方
        {
            line_y.replace(x,1,"*");
            cout << line_y << endl;     //把*替換到相對應的位置，並印出
            line_y = line;              //利用line還原被修改後的line_y供下次使用
        }
    }
}
