#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    cout << "Complete this quadratic function and draw a diagram." << endl ;
    cout << "f(x)=ax+b.(a=/=0)" << endl ;

    int a = 0 , b = 0 ;

    cout << "a: " ;
    cin >> a ;

    while(a > 10 || a < -10 || a == 0 )
    {
        cout << "illegal input!" << endl ;
        cout <<  "a: " ;
        cin >> a ;
    }

    cout << "b: " ;
    cin >> b ;

    while ( b > 10 || b < -10 )
    {
        cout << "illegal input!" << endl ;
        cout <<  "b: " ;
        cin >> b ;
    }

    cout << "f(x)="<< a << "x+" << b << ".(a=/=0)" << endl ;

    int xMax = 10 ;
    int yMax = -10 ;
    int x = -10 ;
    int y = 10 ;

    while ( y >= yMax )
    {

        while( x <= xMax )
        {
            if (y== a*x + b)
                cout << "*" ;

            else if ( x == 0 && y == 0 )
                cout << "+" ;

            else if (x == 0 )
                cout << "|" ;

            else if ( y == 0 )
                cout << "-" ;

            else
                cout << " " ;
            x++;
        }

        cout << endl ;
        x = -10 ;
        y-- ;


    }
    return 0;
}
