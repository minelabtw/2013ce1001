#include<iostream>

using namespace std;

main(){
    int a=0,b=0;    //def a,b
    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;
    //input a
    while(a==0){
        cout << "a=";
        cin >> a;
        if(a==0)
            cout << "illegal input!" << endl;   //when a=0;
    }
    //input b
    cout << "b=";
    cin >> b;
    //output plot of function
    for(int y=10;y>-11;y--){        //y;
        for(int x=-10;x<11;x++){    //x
            if(a*x+b==y)
                cout << "*";
            else{
                if(x==0 && y==0)
                    cout << "+";
                else if (y==0)
                    cout << "-";
                else if (x==0)
                    cout << "|";
                else
                    cout << " ";

            }


        }
        cout << endl;
    }

    return 0;

}
