#include <iostream>

using namespace std;

int main()
{
    int a,b;    //宣告整數a跟b
    int x=-10;  //宣告x初始為-10
    int y=10;   //宣告y初始為10

    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;

    cout << "a: ";
    cin >> a;   //將a的值輸入

    while ( a > 10 || a < -10 || a == 0)          //當a大於10,a小於-10,a=0
    {
        cout << "illegal input!" << endl << "a: ";//輸出字彙並換行
        cin >> a;
    }

    cout << "b: ";
    cin >> b;   //將b的值輸入

    while ( b > 10 || b < -10)   //當b大於10,b小於-10
    {
        cout << "illegal input!" << endl << "b: ";//輸出字彙並換行
        cin >> b;
    }

    if( b > 0 )     //當b大於0,等於0以及其他的狀況時輸出不同字彙
        cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;
    else if( b == 0 )
        cout << "f(x)=" << a << "x.(a=/=0)" << endl;
    else
        cout << "f(x)=" << a << "x" << b << ".(a=/=0)" << endl;

    while (y >= -10)
    {
        while (x <= 10)//當y大於等於-10,x小於等於10
        {
           if ( y == a * x + b)         //當y=ax+b時輸出*
              cout << "*";
           else if ( y == 0 && x == 0 ) //當y=0,x=0時輸出+
              cout << "+";
           else if ( x == 0 )           //當x=0時輸出|
              cout << "|";
           else if ( y == 0 )           //當y=0時輸出-
              cout << "-";
           else                         //當其他狀況時輸出空格
              cout << " ";
           x++;                         //x做向下一位
        }
        cout << endl;                   //當到底時換行
        x=-10;                          //x回到-10
        y--;                            //y做向下一行
    }

    return 0;
}
