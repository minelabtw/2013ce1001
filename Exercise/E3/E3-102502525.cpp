#include<iostream>
using namespace std;

int main()
{
    int x=-10;//設x從-10開始印。
    int y=10;//設y從10開始印。
    int xMax=10;//設x的極大值為10。
    int yMax=-10;//設y的極小值為-10，因為從上而下印。
    int a,b;

    cout <<"Complete this linear function and draw a diagram."<<endl;
    cout <<"f(x)=ax+b(a=/=0)"<<endl;
    cout <<"a: ";
    cin >>a;
    while (a>10 || a<-10 || a==0)//當a>10或a<-10或a=0，超出範圍還有a不能等於0，跑出"illegal input!"。
    {
        cout <<"illegal input!"<<endl;
        cout <<"a: ";
        cin >>a;
    }
    cout <<"b: ";
    cin >>b;
    while (b>10 || b<-10)//當b>10或b<-10時超出範圍，跑出"illegal input!"。
    {
        cout <<"illegal input!"<<endl;
        cout <<"b: ";
        cin >>b;
    }
    if (b==0)
        cout <<"f(x)="<<a<<"x"<<".(a=/=0)"<<endl;//當b=0時，x後面不能有"+"號。
    else if (b<0)
        cout <<"f(x)="<<a<<"x"<<b<<".(a=/=0)"<<endl;//當b<0時，x後面不能有"+"號。
    else
        cout <<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;//當b>0時，可以有"+"號。

    while (y>=yMax)//當y在範圍內時
    {
        while (x<=xMax)//當x在範圍內時
        {
            if (y==a*x+b)
                cout <<"*";//如果y=ax+b，印出"*"
            else if (x==0 && y==0)
                cout <<"+";//如果x=y，印出"+"
            else if (x==0)
                cout <<"|";//如果x=0，印出"|"
            else if (y==0)
                cout <<"-";//如果y=0，印出"-"
            else
                cout <<" ";//至於其他部分，印出空白
            x++;
        }
        cout <<endl;
        x=-10;
        y--;

    }
    return 0;

}
