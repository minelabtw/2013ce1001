#include <iostream>
using namespace std;

int main()
{
    int a=0,b=0,x=-10,y=10;                                             //宣告型別為整數的a,b,x,y 並初始化其值為0,0,-10,10

    cout << "Complete this linear function and draw a diagram.\n";      //輸出"Complete this linear function and draw a diagram.\n"到螢幕上
    cout << "f(x)=ax+b.(a=/=0)\n";                                      //輸出"f(x)=ax+b.(a=/=0)\n"到螢幕上

    while(1)                                           //詢問a的迴圈
    {
        cout << "a:";                                  //輸出"a:"到螢幕上
        cin >> a;                                      //輸入一個值給a
        if (a<-10 or a>10 or a==0)                     //判斷輸入的a是否符合範圍
        {
            cout << "illegal input!\n";                //輸出"illegal input!\n"到螢幕上
        }
        else
        {
            break;                                     //跳出迴圈
        }
    }
    while (1)                                          //詢問b的迴圈
    {
        cout << "b:";                                  //輸出"b:"到螢幕上
        cin >> b;                                      //輸入一個值給b
        if (b<-10 or b>10)                             //判斷輸入的b是否符合範圍
        {
            cout << "illegal input!\n";                //輸出"illegal input!\n"到螢幕上
        }
        else
        {
            break;                                     //跳出迴圈
        }
    }
    cout << "f(x)=" << a << "x+" << b <<".(a=/=0)\n";  //輸出"f(x)=" << a << "x+" << b <<".(a=/=0)\n"到螢幕上
    for(y=10;y>=-10;y--)                               //y重覆累加並執行
    {
        for (x=-10;x<=10;x++)                          //x重覆累加並執行
        {
            if (y==a*x+b)                              //判斷y==a*x+b
            {
                cout << "*";                           //輸出"*"到螢幕上
            }

            else if (x==0 and y==0)                    //判斷x,y是否等於0
            {
                cout << "+";                           //輸出"+"到螢幕上
            }
            else if (x==0)                             //判斷x是否等於0
            {
                cout << "|";                           //輸出"|"到螢幕上
            }
            else if (y==0)                             //判斷y是否等於0
            {
                cout << "-";                           //輸出"-"到螢幕上
            }
            else
            {
                cout << " ";                           //輸出" "到螢幕上
            }
        }
        cout << endl;                                  //輸出換行
    }

    return 0;
}
