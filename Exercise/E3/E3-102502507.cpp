#include <iostream>
using namespace std;
int main()
{
    int a;//宣告變數
    int b;
    int x=0;
    int y=10;
    int xmax=0;
    int ymax=10;
    cout<<"Complete this linear function and draw a diagram."<<endl;
    cout <<"f(x)=ax+b.(a=/=0)"<<endl;//輸出f(x)=ax+b,a不等於0
    cout <<"a:";//輸出a:
    cin >>a;//輸入a
    while(a>10 or a<-10 or a==0)//當此情形發生時,進入迴圈
    {
        cout<<"illegal input!"<<endl<<"a:";//輸出illegal input!,換行跑出a:
        cin>>a;//輸入a
    }
    cout<<"b:";//同上
    cin>>b;
    while(b>10 or b<-10)
    {
        cout<<"illegal input!"<<endl<<"b:";
        cin>>b;
    }
    if(b>=0)
    {
        cout<<"f(x)="<<a<<"x"<<"+"<<b<<endl;
    }
    else
    {
        cout<<"f(x)="<<a<<"x"<<b<<endl;
    }
    while (y>= -10)
    {
        x=-10;
        while(x<=10)
        {
            if(y==a*x+b)//當式子成立時,跑出*
                cout<<"*";
            else if(x!=0 and y==0)//當x不等於0且Y=0時,顯示-
                cout<<"-";
            else if(x==0 and y!=0)//當x等於0且Y等於0時,顯示|
                cout<<"|";
            else if(x==0 and y==0)//原點顯示
                cout<<"+";
            else if(y!=a*x+b)//以上不成立時,空白
                cout<<" ";
            x++;//使x跑下去
        }
        y--;//使y跑下去
        cout<<endl;
    }
    return 0;
}
