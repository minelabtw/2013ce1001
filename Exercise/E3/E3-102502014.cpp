#include <iostream>
using namespace std;

int main(){
    int a;                  //宣告變數a,b為整數
    int b;
    double ans;             //宣告變數ans為浮點數

    cout<<"Complete this quadratic function and draw a diagram."<<endl;
        cout<<"f(x)=ax+b.(a=/=0)"<<endl;
    while(true) {           //寫一個while迴圈限制a與b的值在+10到-10之間 否則會在螢幕印出"Illegal Input!"的字樣
        cout<<"a:";
        cin>>a;
        if(a<=-10||a>=10||a==0) {
            cout<<"Illegal Input!"<<endl;
            continue;
        }
        cout<<"b:";
        cin>>b;
        if(b<=-10||b>=10) {
            cout<<"Illegal Input!"<<endl;
        }else{
             break;
             }
    }
        cout<<"f(x)="<<a<<"x+("<<b<<").(a=/=0)"<<endl;

    for(int y=10;y>=-10;y--) {            //使用兩層的for迴圈畫出21*21的圖形
        ans = (double)(y-b)/a;            // (y-b)/a要轉型為浮點數以與ans作比較
        for(int x=-10;x<=10;x++) {
            if(x==ans) {
                cout<<"*";                //若x=ans 印出"*"
            } else if(x==0) {             //否則
                if(y!=0) {
                    cout<<"|";}           //若x=0,y=/=0 印出"|"(y軸)
                else {
                    cout<<"+";            //若x=0,y=0 印出"+"(原點)
                       }
            } else {
                if(y!=0) {                //若x=/=0,y=/=0 印出" "(空格)
                    cout<<" ";}
                    else {
                    cout<<"-";}           //若x=/=0,y=0 印出"-"(x軸)
                   }
                       }
        cout<<endl;                       //使內迴圈都跑完後換行
    }
    return 0;
}
