#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a=0; //係數a
    int b=0; //係數b

    cout << "Complete this quadratic function and draw a diagram." << endl << "f(x)=ax+b.(a=/=0)" << endl << "a:"   ;
    cin >> a ;

    while (a < -10 || a > 10 )
    {
        cout << "illegal input!" << endl <<  "a:";
        cin >> a;
    }
    cout << "b:" ;
    cin >> b ;
    while (b < -10 || b > 10)
    {
        cout << "illegal input!" << endl <<  "b:";
        cin >> b;
    }
    cout << "f(x)=" << a << "x" << "+" << b << "." << "(a=/=0)" << endl;

    int v ;
    int w ;
    for (v=-10; v<11; ++v)
    {
        for (w=-10; w<11; ++w)
        {
            if (-1*v==a*w+b)
                cout << "*" ; //方程式*
            else if (v==0 && w==0)
                cout << "+"; //在原點是+
            else if (v==0)
                cout << "-"; //X軸
            else if (w==0)
                cout << "|"; //Y軸
            else
                cout << " ";
        }
        cout << "\n" ;
    }



}

