#include <iostream>
using namespace std;

int main()
{
    int a=0,b=0;
    cout << "Complete this quadratic function and draw a diagram.\n";
    cout << "f(x)=ax+b.(a=/=0)\n";
    cout << "a: ";
    cin >> a;
    while (a<-10 || a>10 || a==0)//判斷a的範圍是否符合條件
    {
        cout << "illegal input!" << endl << "a: ";
        cin >> a;
    }
    cout << "b: ";
    cin >> b;
    while (b<-10 || b>10)//判斷b的範圍是否符合條件
    {
        cout << "illegal input!" << endl << "b: ";
        cin >> b;
    }

    cout << "f(x)=" << a << "x+" << b << "." << "(a=/=0)" << endl;

    int x=-10;//給定x的初始值
    int y=10;//給定y的初始值
    int xMax=10;//跑迴圈時x的上限
    int yMax=-10;//跑迴圈時y的下限

    while(y>=yMax)
    {
        while(x<=xMax)
        {
            if(y==a*x+b)
                cout << "*";//若y=a*x+b則印出一個*
            else if(y==0 &&x==0)
                cout << "+";//印出原點
            else if(x==0)
                cout << "|";//印出y軸
            else if(y==0)
                cout << "-";//印出x軸
            else
                cout << " ";//除了上面的條件之外印出一個空白
            x++;//x值+1後再跑一次迴圈

        }
        cout << endl;//換下一行繼續執行上面的迴圈
        x=-10;//使x值回歸原本的初始值
        y--;//y值-1後再跑一次迴圈

    }

    return 0;



}
