#include<iostream>
using namespace std;
int main()
{
    int a = 0;
    int b = 0;
    int x = 0;
    int y = 0;
    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;
    cout << "a:";  //輸入a的值
    cin >> a;
    while ( a < -10 || a > 10 || a == 0 ) //如果a的值不在-10~10的範圍內且等於0，則重新輸入
    {
        cout << "illegal input!" << endl;
        cout << "a:";
        cin >> a;
    }
    cout << "b:";  //輸入b的值
    cin >> b;
    while ( b < -10 || b > 10 ) //如果b的值不在-10~10的範圍內，則重新輸入
    {
        cout << "illegal input!" << endl;
        cout << "b:";
        cin >> b;
    }
    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;
    for ( y = 10; y >= -10; y -- ) //y座標從10開始，結束為-10，每次迴圈y值減1
    {
        for( x = -10; x <= 10; x++ ) //x座標從-10開始，結束為10，每次迴圈x值加1
        {
            if ( a * x + b == y ) //若y=a*x+b，輸出*
                cout << "*" ;
            else if( y == 0 & x == 0 ) //若y=0且x=0，輸出+
                cout << "+" ;
            else if( y == 0 ) //若y=0，輸出-
                cout << "-" ;
            else if( x == 0 ) //若x=0，輸出+
                cout << "|" ;
            else
                cout << " " ; //剩下全為空白
        }
        cout << endl ; //x每跑完一行就換行然後繼續執行以上迴圈至跑完全部行
    }
    return 0;
}
