#include <iostream>

using namespace std;

int main()
{
	double x , y; //declare the x , y of funtion
	int a , b; //declare the a , b of funtion

	cout << "Complete this quadratic function and draw a diagram." << endl << "f(x)=ax+b.(a=/=0)" << endl; //output message

	while(true) //input a
	{
		cout << "a: ";
		cin >> a;
		if(a >= -10 && a <= 10 && a != 0)
			break;
		cout << "illegal input!" << endl;
	}

	while(true) //input b
	{
		cout << "b: ";
		cin >> b;
		if(b >= -10 && b <= 10)
			break;
		cout << "illegal input!" << endl;
	}

	cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;

	for(int cory = 10 ; cory >= -10 ; --cory) //y-cordinate
	{
		y = cory;
		for(int corx = -10 ; corx <= 10 ; ++corx) //x-cordinate
		{
			x = (y - b) / a;
			
			if(x == corx)
				cout << "*";
			else if(corx == 0 || cory == 0)
			{
				if(corx == 0 && cory == 0)
					cout << "+";
				else if(corx == 0)
					cout << "|";
				else
					cout << "-";
			}
			else
				cout << " ";
		}
		cout << endl;
	}

	return 0;
}
