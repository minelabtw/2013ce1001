#include <iostream>
using namespace std;
int main()
{
    int a = 0; //宣告變數a
    int b = 0; //宣告變數b

    cout <<"Complete this linear function and draw a diagram."<< endl <<"f(x)=ax+b.(a=/=0)"<< endl <<"a: "; //輸出文字
    cin >> a; //輸入變數a
    while (a < -10 || a > 10 || a == 0) //重複判斷a是否超出範圍
    {
        cout <<"illegal input!"<< endl <<"a: ";
        cin >> a;
    }

    cout << "b: ";
    cin >> b; //輸入變數b
    while (b < -10 || b > 10 ) //重複判斷b是否超出範圍
    {
        cout <<"illegal input!"<< endl <<"b: ";
        cin >> b;
    }
    if (b >= 0) //判斷變數b是否大於等於零
    {
        cout <<"f(x)="<< a <<"x+"<< b <<".(a=/=0)"<< endl;
    } //輸出(含加號)
    else
    {
        cout <<"f(x)="<< a <<"x"<< b <<".(a=/=0)"<< endl;
    } //輸出(不含加號)

    int x = -10; //宣告x起始值-10
    int y = 10; //宣告y起始值10
    int xmax = 10; //宣告xmax的值為10
    int ymin = -10; //宣告ymin的值為-10

    while (y >= ymin) //重複判斷y是否在範圍內
    {
        while (x <= xmax) //重複判斷x是否在範圍內
        {
            if (y == a * x + b) //判斷是否符合式子y=ax+b
            {
                cout <<"*";
            }
            else if (x == 0 && y == 0) //判斷是否為原點
            {
                cout <<"+";
            }
            else if (x == 0) //判斷x是否為零
            {
                cout <<"|";

            }
            else if (y == 0) //判斷y是否為零
            {
                cout <<"-";
            }
            else
            {
                cout <<" ";
            }
            x++; //x跳至x+1
        }
        cout << endl; //換行
        x = -10; //使x回到起始值-10
        y--; //y跳至y-1
    }
    return 0;
}
