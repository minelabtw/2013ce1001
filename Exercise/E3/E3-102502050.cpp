#include<iostream>
using namespace std;
int main()
{
    int x ;
    int y ;     //座標的值

    int a;
    int b;      //題目要求的輸入

    int input = 0;  //index

    cout << "Complete this quadratic function and draw a diagram." << endl ;
    cout << "f(x)=ax+b.(a=/=0)" << endl ;

    for(;input<2;)
    {
        if(input)   //i非零時( i=1 )
        {
            cout << "b: " ;
            cin  >> b;
            if(b< 11 && b> -11)input++;              //檢查輸入
            else               cout << "illegal input!" << endl ;
        }
        else        //i為零時
        {
            cout << "a: " ;
            cin  >> a;
            if(a< 11 && a> -11 && a != 0)input++;                 //檢查輸入
            else                         cout << "illegal input!" << endl ;
        }
    }//輸入

    cout << "f(x)=" << a << "x"  ;

    if(b>0)  cout << "+" ;      //判斷b的正負

    if(b!=0) cout << b ;
    cout << ".(a=/=0)" << endl ;

    for(y=10;y>-11;y--)
    {

        for(x=-10;x<11;x++)
        {
            if( x * a + b == y)           //判斷此點是否符合方程式
                cout << "*";
            else if( x==0 && y==0)        //判斷X和Y軸
                cout << "+";
            else if(x==0)
                cout << "|";
            else if(y==0)
                cout << "-";
            else
                cout << " ";                //輸出空白
        }

        cout << endl ;
    }

    return 0;
}
