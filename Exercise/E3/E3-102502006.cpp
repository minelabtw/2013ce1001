#include <iostream>

using namespace std;

int main()
{

    cout << "Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)\n";

    int a=0; // 斜率
    int b=0; // 常數

    cout << "a:"; // 判斷斜率
    for(int i=0 ; i<1; i++)
    {
        cin >> a;
        if (a==0 or a<-10 or a>10)
        {
            cout << "illegal input!\na:";
            i--;
        }
    }

    cout << "b:"; // 判斷常數
    for (int i=0; i<1; i++)
    {
        cin >> b;
        if (b<-10 or b>10)
        {
            cout << "illegal input!\nb:";
            i--;
        }
    }

    cout << "f(x)=" << a << "x+" << b << "\n";  // 輸出函式

    // 印出圖表
    for(int j=10; j>=-10; j--)
    {
        for(int k=-10; k<=10; k++)
        {
            if(j==a*k+b)
            {
                cout << "*";
            }
            else
            {
                if(j==0 and k==0)cout << "+";
                else if(j==0)cout << "-";
                else if(k==0)cout << "|";
                else cout << " ";
            }

        }
        cout << "\n";
    }




    return 0;
}
