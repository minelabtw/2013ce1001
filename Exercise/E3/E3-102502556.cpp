#include <iostream>
using namespace std;

int main()
{
    int a = 0; //宣告型別為 整數(int) 的第一位數字(斜率)，並初始化其數值為0。
    int b = 0; //宣告型別為 整數(int) 的第二位數字(常數)，並初始化其數值為0
    cout << "Complete this linear function and draw a diagram." << endl; //使用cout將欲顯示的文字輸出在螢幕上。
    cout << "f(x)=ax+b.(a=/=0)" << endl;
    cout << "a:";
    cin >> a; //讓使用者輸入第一位數字(斜率)，並用while判斷數字合理性，否則要求使用者重新輸入。
    while (a < -10 || a == 0 || a > 10)
    {
        cout << "illegal input!" << endl;
        cout << "a:";
        cin >> a;
    }
    cout << "b:";
    cin >> b; //讓使用者輸入第二位數字(常數)，並用while判斷數字合理性，否則要求使用者重新輸入。
    while (b < -10 || b > 10)
    {
        cout << "illegal input!" << endl;
        cout << "b:";
        cin >> b;
    }
    cout << "f(x)=" << a << "x" << "+" << b << ".(a=/=0)" << endl;

    int x = -10; //宣告型別為 整數(int) 的第三位數字(x變數)，並初始化其數值為-10。
    int y = 10;  //宣告型別為 整數(int) 的第四位數字(y變數)，並初始化其數值為10。
    int xmax = 10; //宣告型別為 整數(int) 的第五位數字(x的最大值)，並初始化其數值為10。
    int ymin = -10;//宣告型別為 整數(int) 的第六位數字(y的最小值)，並初始化其數值為-10。
    while (y >= ymin) //用while迴圈來讓y能夠重複計算不同的狀況，計算到最小值時結束。
    {
        while (x <= xmax) //用while迴圈來讓x能夠重複計算不同的狀況，計算到最大值時結束。
        {
            if (y == a * x + b) //用if和else來判斷不同的狀況，並使螢幕輸出在某種狀況時所對應的符號。
                cout << "*";
            else if (x == 0 && y == 0)
                cout << "+";
            else if (x == 0)
                cout << "|" ;
            else if (y == 0)
                cout << "-" ;
            else
                cout << " ";
            x++;
        }
        cout << endl; //換行。
        x = -10; //計算完後，使x回到初始值(-10)，方便做下一次的運算。
        y--;
    }
    return 0;
}
