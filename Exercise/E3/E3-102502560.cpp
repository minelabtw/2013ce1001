#include <iostream>
using namespace std;

int main()
{
	cout << "Complete this quadratic function and draw a diagram.\n";
	cout << "f(x)=ax+b.(a=/=0)\n";

	int a, b;	//a is the slope of f(x)
				//b is the y-intercept of f(x)

	cout << "a: "; cin >> a;
	while(a<-10||a>10||a==0)
	{
		//warn the user and re-input if "a" is not in range
		cout << "illegal input!\n";
		cout << "a: "; cin >> a;
	}

	cout << "b: "; cin >> b;
	while(b<-10||b>10)
	{
		//warn the user and re-input if "b" is not in range
		cout << "illegal input!\n";
		cout << "b: "; cin >> b;
	}

	cout << "f(x)=" << a << "x"; if(b>=0){cout << "+";} cout << b << ".(a=/=0)\n";
	//print the Linear equation f(x)

	for(int y=10;y>=-10;y--)				//for each line from y=+10 to y=-10:
	{
		if(!y){								//if on the X-axis
			for(int x=-10;x<=10;x++)		//for each position in line from x=-10 to x=+10
			{
				if(a*x+b==y){cout<<"*";}	//a point of linear equation
				else if(!x){cout<<"+";}		//the origin
				else{cout<<"-";}			//the X-axis
			}
		}else{								//if not on the X-axis
			for(int x=-10;x<=10;x++)		//for each position in line from x=-10 to x=+10
			{
				if(a*x+b==y){cout<<"*";}	//a point of linear equation
				else if(!x){cout<<"|";}		//the Y-axis
				else{cout<<" ";}			//a empty space
			}
		}
		cout << "\n";
	}
}
