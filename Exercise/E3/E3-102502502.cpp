#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a,b;                                           // 宣告a,b
    cout << "Complete this linear function and draw a diagram." << endl << "f(x)=ax+b.(a=/=0)" << endl;
    cout << "a:";                                     //宣告a，超出範圍者，重新輸入
    cin >> a;
    while (a < -10 || a > 10 )
        {
            cout << "illegal input!" << endl << "a:";
            cin >> a;
        }
    cout << "b:";                                    //宣告b，超出範圍者，重新輸入
    cin >> b;
    while (b < -10 || b > 10)
        {
            cout << "illegal input!" << endl << "b:";
            cin >> b;
        }

    cout << "f(x)=" << a << "x" << "+" << b << "." << "(a=/=0)" << endl;

    int i, j;                                      // 宣告i,j
	for(int i=-10;i<=10;i++)
    {
        for(int j=10;j>=-10;j--)
        {
            if(j==i*a+b)
                cout<<'*';
            else if(j==0 && i==0)
                cout<<'+';
            else if(j==0)
                cout<<'|';
            else if(i==0)
                cout<<'-';
            else
                cout<<' ';
        }
        cout << endl;
    }
}
