#include <iostream>
using namespace std;
int main()
{
    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;
    int a=0;
    int b=0;
    cout << "a: ";
    while(cin >> a)
    {
        if(a>=-10&&a<=10&&a!=0)
        {
            break;
        }
        cout << "illegal input!" << endl;
        cout << "a: ";
    }
    cout << "b: ";
    while(cin >> b)
    {
        if(b>=-10&&b<=10)
        {
            break;
        }
        cout << "illegal input!" << endl;
        cout << "b: ";
    }
    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;
    for(int y=10; y>0; --y)
    {
        for(int x=-10; x<=10; ++x)
        {
            if(y==a*x+b)
            {
                cout << "*";
            }
            else if(x==0)
            {
                cout << "|";
            }
            else
            {
                cout << " ";
            }
        }
        cout << endl;
    }
    for(int x=-10; x<=10; ++x)
    {
        if(0==a*x+b)
        {
            cout << "*";
        }
        else if(x==0)
        {
            cout << "+";
        }
        else
        {
            cout << "-";
        }
    }
    cout << endl;
    for(int y=-1; y>=-10; --y)
    {
        for(int x=-10; x<=10; ++x)
        {
            if(y==a*x+b)
            {
                cout << "*";
            }
            else if(x==0)
            {
                cout << "|";
            }
            else
            {
                cout << " ";
            }
        }
        cout << endl;
    }
    return 0;
}
