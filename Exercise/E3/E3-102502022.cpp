#include <iostream>

using namespace std;

int main()
{

    int integer1=0;      //宣告名稱為integer1的變數'初始值為0
    int integer2=0;      //宣告名稱為integer2的變數'初始值為0


    cout<< "Complete this linear function and draw a diagram.\n""f(x)=ax+b.(a=/=0)\n";

    cout<< "a:";
    cin>> integer1;                                              //輸入integer1的值並丟給a
    while(integer1>10 ||integer1<-10 ||integer1==0)
    {
        cout<< "illegal input!\n""a:";
        cin>> integer1;
    }

    cout<< "b:";
    cin>> integer2;                                             //輸入integer2的值並丟給b                                            //
    while(integer2>10 ||integer2<-10)
    {
        cout<<"illegal input!\n""b:";
        cin>> integer2;
    }

    cout<< "f(x)=" <<integer1<<"x+"<<integer2<<".(a=/=0)" <<endl;

    int xMAX=10;       //宣告名稱為xMAX的變數'初始值為10
    int yMAX=10;       //宣告名稱為yMAX的變數'初始值為10
    int xMIN=-10;      //宣告名稱為xMIN的變數'初始值為-10
    int yMIN=-10;      //宣告名稱為yMIN的變數'初始值為-10
    int x=-10;         //宣告名稱為x的變數'初始值為-10
    int y=10;          //宣告名稱為y的變數'初始值為10
    while(y>=yMIN)     //迴圈'條件為y大於或等於y的最小值
    {
        while(x<=xMAX) //迴圈'條件為x小魚或等於x的最大值
        {
            if(y==integer1*x+integer2)      //假設點在線上
            {
                cout<< "*";
            }
            else if(x==0 && y==0)           //假設點在原點
            {
                cout<< "+";
            }
            else if(x==0)                  //假設在x座標等於0時
            {
                cout<< "|";
            }
            else if(y==0)                  //假設在y座標等於0時
            {
                cout<< "-";

            }
            else
            {
                cout<< " ";                //其餘情況輸出空白


            }
             x++;                         //代表x+1
        }
    cout<< endl;
    x=-10;                                //回復到-10的初始值
    y--;                                  //代表y-1


    }

    return 0;

}

