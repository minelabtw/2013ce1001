
#include <iostream>

using namespace std;

int main()
{
 int a=0;
 int b=0;
 int x=-10;
 int y=10;
 cout<<"Complete this linear function and draw a diagram."<<endl;//從螢幕輸出句子
 cout<<"f(x)=ax+b.(a=/=0)"<<endl;                                //從螢幕輸出句子
 cout<<"a:";
 cin>>a;                                                         //從鍵盤輸入a
 cout<<endl;

 while(a<-10||a==0||a>10)                          //限定a的範圍
 {
  cout<<"illegal input!"<<endl;                    //若a違反此範圍
  cout<<"a:";                                      //輸出 illegal input!
  cin>>a;                                          //並要求重新輸入
  cout<<endl;
 }
  cout<<"b:";                                      //b的同上處理
  cin>>b;
 while(b<-10||b>10)
 {
  cout<<"illegal input!"<<endl;
  cout<<"b:";
  cin>>b;
  cout<<endl;
 }
 cout<<"f(x)="<<a<<"x+"<<b<<"(a=/=0)"<<endl;        //輸出方程式
 while (y>=-10)                                     //設+號處為原點
 {                                                  //所以從最左上(x=-10,y=10)
  while(x<=10)                                      //開始輸出
  {
    if(y==a*x+b)                                    //如果在方程式上
     cout<<"*";                                     //印出*
    else if(x==0&&y==0)                             //如果在原點
     cout<<"+";                                     //印出+
    else if(x==0)                                   //在y軸印出|
     cout<<"|";
    else if(y==0)                                   //在x軸印出-
     cout<<"-";
    else
     cout<<" ";                                     //其他地方印出空白
    x++;
  }
  cout<<endl;
  x=-10;
  y--;                                              //y--是因為從上到下，y值越來越小
  }
  return 0;
}
