#include<iostream>
using namespace std;
int main()
{
    int a=0;//declare a variable and initial as 0
    int b=0;//declare a variable and initial as 0

    cout<<"Complete this quadratic function and ";
    cout<<"draw a diagram."<<endl;
    cout<<"f(x)= ax + b. (a=/=0)"<<endl;

    do
    {
        cout<<"a: ";
        cin>>a;
        if(a>10 or a<-10 or a==0)
            cout<<"illegal input!"<<endl;

    }
    while(a>10 or a<-10 or a==0);//loop until
    // the correct number is input , and a
    //represent the slope

    do
    {
        cout<<"b: ";
        cin>>b;
        if(b>10 or b<-10 )
            cout<<"illegal input!"<<endl;

    }
    while(b>10 or b<-10);//loop until the
    //correct number is input and b represent
    //the intercept

    cout<<"f(x) = "<<a<<"x+"<<b<<".(a=/=0)"<<endl;

    int j=0;//declare a variable and initial as 0
    // j represent the vertical coordinate
    int i=0;//declare a variable and initial as 0
    // i represent the horizontal coordinate
    for(j=10; j>=-10; j = j-1)//because the
        //program runs from left to right,
        //up to down, so I set j=10 and i=-10
    {
        for(i=-10; i<=10; i = i+1)
        {
            if(a*i+b == j)
                cout<<"*";
            //as the given function y=ax+b
            //if the point (i,j) is in the
            //line , and then print *
            if  (i==0 and j==0)
                cout<<"+";
            //print + at the origin if
            //the function doesn't pass
            //through the origin
            if(i==0)
                cout<<"|";
            //print | at the y-axis if
            //the function doesn't pass
            //through the y-axis
            if(j==0)
                cout<<"-";
            //print - at the x-axis if
            //the function doesn't pass
            //through the x-axis
            else
                cout<<" ";
            //print a blank where the
            //function , y-axis and x-axis
            //don't pass through
        }
        cout<<endl;
    }
    return 0;
}
