#include <iostream>

using namespace std;

int main()
{
    int a=0;                                                                //宣告整數變數a並將其初始化
    int b=0;                                                                //宣告整數變數b並將其初始化

    cout << "Complete this quadratic function and draw a diagram."<<endl;
    cout << "f(x)=ax+b.(a=/=0)"<< endl;
    cout << "a:";
    cin >> a ;
    while(a==0||a>10||a<-10)                                                //限制整數變數a的範圍
    {
        cout<<"illegal input!"<<endl;
        cout << "a:";
        cin >> a ;
    }
    cout << "b:";
    cin >> b ;
    while(b>10||b<-10)                                                      //限制整數變數b的範圍
    {
        cout<<"illegal input!"<<endl;
        cout << "b:";
        cin >> b ;
    }
    cout << "f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;

    int x=-10,y=10;                                                         //將起始位置定義為座標(-10,10)
    while(y>-11)                                                            //將y限制在10~-10之間
    {
        int x=-10;                                                          //需在每次換行之後將x座標重新定義為-10
        while(x<11)                                                         //將x限制在-10~10之間
        {
            if(y==a*x+b)                                                    //第一優先條件:如果此座標符合函數,則輸出"*"
                cout<<"*";
            else if(x==0&&y==0)                                             //第二優先條件:如果此座標為原點,則輸出"+"
                cout<<"+";
            else if(x==0)                                                   //第三優先條件:如果此座標非原點,且x座標=0,則輸出"|"
                cout<<"|";
            else if(y==0)                                                   //第四優先條件:如果此座標非原點,且y座標=0,則輸出"-"
                cout<<"-";
            else                                                            //第五優先條件:若未符合上述任一條件,則輸出" "
                cout<<" ";
            x++;                                                            //每完成一個動作之後,游標將向右跳至下一格,乃須將x座標給予新的值且為本身再加一
        }
        cout<<endl;                                                         //當第一行滿了之後,跳出迴圈並向下換一行
        y--;                                                                //向下換了一行之後,y座標需扣一
    }

    return 0;
}
