#include <iostream>
using namespace std;
int main()
{
	int a, b;

	while (cin >> a >> b) {
		if (a == 0 || a >= 10 || a <= -10) {
			cout << "illegal input!" << endl;
			continue;
		}
		if (b >= 10 || b <= -10) {
			cout << "illegal input!" << endl;
			continue;
		}

		if (a > 0) {

			int centerX = a/2+b;
			int centerY = a/2;


			for (int i = 1; i <= a; i++) {
				for (int j = 1; j <= a-i; j++) {
					if (j == centerY) {
						cout << "|";
						continue;
					}
					if (i == centerX) {
						cout << "-";
						continue;
					}
					cout << " ";
				}

				if (i == centerX) {
					cout << "*";
					for (int j = 1; j <= centerX; j++) {
						cout << "-";
					}
					cout << endl;
					continue;
				}

				if (i <= centerX) {
					/*if (i > a/2) {
						cout << "*";
						for (int j = 1; j < i-(a/2); j++)
							cout << " ";
						cout << "|" << endl;
					}
					else */

					cout << "*" << endl;


				}
				else if (i > centerX) {
					cout << "*";
					/*for (int j = 1; j < i-centerX; j++)
							cout << " ";
					cout << "|" << endl;*/
				}
			}
		}
		else {
			a = (-1)*a;
			int centerX = a/2+b;
			int centerY = a/2;
			for (int i = 1; i <= a; i++) {
				for (int j = 1; j <= i; j++) {
					if (j == centerY) {
						cout << "|";
						continue;
					}
					if (i == centerX) {
						cout << "-";
						continue;
					}
					cout << " ";
				}
				cout << "*" << endl;
			}
		}

	}
return 0;
}
