#include <iostream>
using namespace std;
int main()
{
    int a;                                                                //宣告變數A
    int b;                                                                //宣告變數B
    cout<<"Complete this quadratic function and draw a diagram."<<endl;   //顯示文字
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;                                      //顯示方程式
    cout<<"a: ";                                                          //顯示A
    cin>>a;                                                               //輸入A
    while(a>10||a<-10||a==0)                                              //當A超出範圍
        {cout<<"illegal input!"<<endl;                                    //顯示錯誤
         cout<<"a; ";                                                      //顯示變數A
         cin>>a;}                                                         //輸入變數A
    cout<<"b: ";                                                          //顯示變數B
    cin>>b;                                                               //輸入變數B
    while(b>10||b<-10)                                                    //當B超出範圍
        {cout<<"illegal input!"<<endl;                                    //顯示錯誤
         cout<<"b: ";                                                     //顯示變數B
         cin>>b;}                                                         //輸入變數B
    cout<<"f(x)="<<a<<"x+"<<b<<endl;                                      //顯示方程式
    int x=-10;                                                            //宣稱X
    int y=10;                                                             //宣稱Y
    int xMax=10;                                                          //宣稱XMAX
    int yMax=-10;                                                         //宣稱YMAX
    while(y>=yMax)                                                        //Y大於等於YMAX進入迴圈
    {
        x=-10;                                                            //X起點
        while(x<=xMax)                                                    //X小XMAX進入迴圈
        {
            if(y==a*x+b)                                                  //符合函數時
                cout<<"*";                                                //顯示*
            else if(x==0&&y==0)                                           //原點時
                cout<<"+";                                                //顯示+
            else if(y==0)                                                 //X軸時
                cout<<"-";                                                //顯示-
            else if(x==0)                                                 //Y軸時
                cout<<"|";                                                //顯示|
            else if(x!=0&&y!=0)                                           //其他點
                cout<<" ";                                                //顯示空白
            x++;                                                          //X累加
        }
            cout<<endl;                                                   //換行
        y--;                                                              //Y累減
    }
    return 0;
}
