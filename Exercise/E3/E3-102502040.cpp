#include<iostream>

using namespace std;

int main(void)
{
    int aMax=10;//設定a的最大值為10
    int bMax=10;//設定b的最大值為10
    int a=0;//初始化a值=0
    int b=0;//初始化b值=0

    cout << "Complete this quadratic function and draw a diagram." <<endl;//設定執行時出現的文字
    cout << "f(x)=ax+b.(a=/=0)" << endl;//顯示出所執行方程式
    cout << "a:";
    cin >> a ;//輸入a
    while(a==0||a>10||a<-10)//設定a的區域值,且其值不能=0
    {
        cout << "illegal input!" << endl;//若是a超出範圍,顯示出illegal input!
        cout << "a:";
        cin >> a;
    }
    cout << "b:";
    cin >> b ;
    while(b>10||b<-10)//設定b的區域值
    {
        cout << "illegal input!" << endl;
        cout << "b:";
        cin >> b;
    }
    cout << "f(x)="<< a <<"x+"<<b<<endl;


    int x=-10,y=10;//設定起始座標
    while (y>-11)//設定圖形範圍,定義長度
    {
        int x=-10;//定義圖形寬度
        while (x<11)//設定圖形範圍
        {
            if(y==a*x+b)
                cout << "*" ;//若是符合方程式,則顯示出*表示點上
            else if(x==0&&y==0)
                cout << "+";//用+表示圖形無通過原點
            else if(x==0)
                cout << "|" ;//用|表示y軸
            else if(y==0)
                cout << "-";//用-表示x軸
            else
                cout << " ";//其餘點用空白鍵代替
            x++;//x的值為向右遞增


        }
        cout<<endl;//定義執行區域換行
        y--;//y的值為向下遞減


    }


    return 0;
}
