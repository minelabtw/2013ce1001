#include <iostream>
using namespace std;

int main()
{
    int a;
    int b;
    int x;
    char xline[]="          |          ";                                   //宣告字元陣列 xline=非y=0的位置 xline2 y=0 的位置
    char xline2[]="----------+----------";
    string xline3;                                                          //宣告字串 xline3 = 可變字元陣列

    cout<<"Complete this quadratic function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;
    cout<<"a:";                                                             //輸入a值
    cin>>a;
    while(a==0 || a>10 || a<-10)                                            //超出範圍則循環
    {
        cout<<"illegal input!"<<endl;
        cout<<"a:";
        cin>>a;
    }
    cout<<"b:";                                                             //輸入b值
    cin>>b;
    while(b>10 || b<-10)                                                    //超出範圍則循環
    {
        cout<<"illegal input!"<<endl;
        cout<<"b:";
        cin>>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;
    for (int c=10; c>-11; c--)                                              //迴圈20次 c=y的位置 由10往下減到-10
    {
        x=(c-b)/a;                                                          //判斷x的位置
        if ((c-b)%a==0)                                                     //判斷X是否整除
        {
            if(x>=-10 && x<=10)                                             //若x 在 10~-10 則
            {
                x=x+10;                                                     //把x位置 變換到實際 字串 的位置
                if(c!=0)                                                    //當 不是 y=0 的時候
                {
                    xline3 = xline;                                         //把xline字元陣列 給定 xline3字串
                    xline3[x]='*';                                          //替換字串中 x的位置的字元為*
                    cout<<xline3;                                           //印出換行
                    cout<<endl;
                }
                else
                {
                    xline3 = xline2;                                        //替換當y=0的時候的 字串
                    xline3[x]='*';
                    cout<<xline3;
                    cout<<endl;
                }

            }
            else                                                            //x整除 但是超出範圍 則直接印出xline 的字串
            {
                if (c==0)
                {
                    cout<<xline2;
                    cout<<endl;
                }
                else
                {
                    cout<<xline;
                    cout<<endl;
                }
            }
        }
        else                                                                //x不整除 即使在範圍內 也直接印出xline 的字串
        {
            if (c==0)
            {
                cout<<xline2;
                cout<<endl;
            }
            else
            {
                cout<<xline;
                cout<<endl;
            }
        }
    }
    return 0;
}

