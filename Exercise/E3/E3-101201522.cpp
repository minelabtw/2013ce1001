#include <iostream>
using namespace std;

int main(){
    int i,j,a,b;//宣告為整數，i為y軸的值，j為x軸的值，a、b為輸入要求

    cout << "Complete this quadratic function and draw a diagram.\n";
    cout << "f(x)=ax+b.(a=/=0)\n";
    while(1){
        cout << "a: ";
        cin >> a;
        if(a<-10 || a==0 || a>10)//如果不符合輸入要求，則輸出輸入錯誤，並重複詢問
            cout << "illegal input!\n";
        else//符合輸入要求即跳出迴圈
            break;
    }
    while(1){
        cout << "b: ";
        cin >> b;
        if(b<-10 || b>10)//如果不符合輸入要求，則輸出輸入錯誤，並重複詢問
            cout << "illegal input!\n";
        else//符合輸入要求即跳出迴圈
            break;
    }
    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)\n";

    for(i=10;i>=-10;i--){//用迴圈跑y軸的值[-10,10]
        for(j=-10;j<=10;j++){//用迴圈跑x軸的值[-10,10]
            if(i == j*a+b)//如果符合方程式，輸出*
                cout << "*";
            else if(!(i || j))//如果不符合但為原點，輸出+
                cout << "+";
            else if(!i)//如果不符合但為x軸，輸出-
                cout << "-";
            else if(!j)//如果不符合但為y軸，輸出|
                cout << "|";
            else//其餘輸出空白
                cout << " ";
        }
        cout << "\n";
    }

    return 0;
}
