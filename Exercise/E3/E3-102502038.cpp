//============================================================================
// Name        : E3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int alufa,beta;
int check(int a,int b){
	if(a*alufa + beta == b){return 1;}
	else if(a==0&&b==0){return 4;}
	else if(a==0){return 2;}
	else if(b==0){return 3;}
	else{return 0;};
}
int main(void) {
	int alufa_check = 1;
	int beta_check = 1;
	cout << "Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)\n";
	while(alufa_check){
		if(alufa_check == 2)cout<<"illegal input!";
		cout << "a: ";
		cin >> alufa;
		alufa_check = (alufa==0||alufa>10||alufa<-10)?2:0;
	};
	while(beta_check){
		if(beta_check == 2)cout<<"illegal input!\n";
		cout << "b: ";
		cin >> beta;
		beta_check = (beta>10||beta<-10)?2:0;
	};
	cout << "f(x)=" << alufa << "x+" << beta <<".(a=/=0)\n";
	//draw
	for(int i = 10;i>=-10;i--){
		for(int j = -10;j<=10;j++){
			switch(check(j,i)){
			case 0:
				cout << ' ';
				break;
			case 1:
				cout << '*';
				break;
			case 2:
				cout << '|';
				break;
			case 3:
				cout << '-';
				break;
			case 4:
				cout << '+';
				break;
			default:
				break;
			};
		};
		cout << "\n";
	};
	return 0;
};
