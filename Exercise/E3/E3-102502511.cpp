#include <iostream>
using namespace std;

int main()
{
    int a=0; //使a的初始值為0
    int b=0; //使b的初始值為0
    int x=-10; //使x的初始值為-10,讓x能進入if以及while的作用過程
    int y=10; //使y的初始值為10,讓x能進入if以及while的作用過程

    cout << "Complete this linear function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a≠0)" << endl;

    cout << "a:";
    cin >> a;
    if(a>=-10 && a<=10&&a!=0) //當-10≦a≦10和a≠0時,不做任何動作
        cout << "";
    else
        while(a>10||a<-10||a==0) //當a>10或a<-10或a=0時,做出下列指令
        {
            cout << "Illegal input!" << endl;
            cout << "a:";
            cin >> a;
        }

    cout << "b:";
    cin >> b;
    if(b>=-10&&b<=10) //當-10≦b≦10時,不做任何動作
        cout << "";
    else
        while(b>10||b<-10) //當b>10或b<-10時,做出下列指令
        {
            cout << "Illegal input!" << endl;
            cout << "b:";
            cin >> b;
        }

    cout << "f(x)=" << a << "x" << "+" << b << "(a≠0)" << endl;
    while(y>=-10) //當y≧-10時做出下列動作
    {
        while(x<=10) //x≦-10時做出下列動作
        {
            if(y==a*x+b) //當(x,y)符合方程式時,出現*
                cout << "*";
            else if(x==0&&y==0) //當(x,y)=(0,0)時,出現+
                cout << "+";
            else if(x==0) //當x=0時,出現|
                cout << "|";
            else if(y==0) //當y=0時,出現-
                cout << "-";
            else //其餘皆以空白呈現
                cout << " ";
            x++; //x持續遞增值到不符合while的條件
        }
        cout << endl;
        x=-10; //使x回復到-10的地方,即回到最左邊
        y--; //y持續遞減值到不符合while的條件
    }

    return 0;
}
