#include<iostream>
using namespace std;

int main ()
{
    int x = -10;        //宣告座標初始值(-10,10)
    int y = 10;
    int a = 0;
    int b = 0;
    int yMax = 10;         //宣告座標最大最小值
    int xMax = 10;
    int yMin = -10;
    int xMin = -10;

    cout <<"Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)"<<endl;
    cout <<"a: ";
    cin >> a;
    while ( a > 10 or a < -10 or a == 0)      //a的範圍為-10~10(包括10,-10)但非0的整數
    {
        cout <<"illegal input!"<<endl;
        cout <<"a: ";
        cin >> a;
    }

    cout <<"b: ";
    cin >> b;
    while ( b > 10 or b < -10 )      //範圍為-10~10(包括10,-10)
    {
        cout <<"illegal input!"<<endl;
        cout <<"b: ";
        cin >> b;
    }
    cout << "f(x)=" << a << "x" << "+" << b << ".(a=/=0)" << endl;

    while (y>=yMin)
    {
        while (x<=xMax)
        {
            if (x == (y-b)/a )

                cout<<"*";

            else if(x == 0 and y == 0  )
                cout<<"+";                     //座標原點若線條沒經過則用+號表示

            else if(x == 0 )
                cout<<"|";                    //X座標
            else if(y == 0 )
                cout<<"-";                    //y座標
            else
                cout <<" ";


            x++;                         //x+1
        }

        cout<< endl;

        y--;                            //y-1
        x = xMin;                       //x=-10
    }

    return 0;
}
