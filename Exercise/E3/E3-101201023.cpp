#include <iostream>

using namespace std;

int main()
{
    int xMax=10,xMin=-10;
    int yMax=10,yMin=-10;
    int x=-10;
    int y=10;
    int a=0;
    int b=0;

    cout <<"Complete this linear function and draw a diagram."<< endl;

    cout <<"f(x)=ax+b.(a=/=0)"<< endl;

    cout <<"a:";
    cin >> a;
    while(a>10 or a<-10 or a==0)            //當a大於十 或 小於負十 或 等於十 一直循環
    {
       cout << "illegal input!" << endl;
       cout <<"a:";
       cin >> a;
    }

    cout <<"b:";
    cin >> b;
    while(b>10 or b<-10)
    {
       cout << "illegal input!" << endl;
       cout <<"b:";
       cin >> b;
    }



    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;

    while(yMin<=y && y<=yMax)                          //當y界於10與-10之間(包含10和-10)
    {
        while(xMin<=x && x<=xMax)
        {
            if(y==a*x+b)                               //全部範圍，如果y值等於ax+b
            {
                cout << "*";
            }

            else if(x==0 && y==0)                      //剩餘之中，如果x=y=0
            {
                cout << "+";
            }

            else if(x==0)                              //剩餘之中，如果x=0
            {
                cout << "|";
            }

            else if(y==0)                              //剩餘之中，如果y=0
            {
                cout << "-";
            }

            else                                       //其餘全部
            {
                cout << " ";
            }
            x++;                                       //將x傳送到x+1(設為新的x)再跑一次
        }
        cout << endl;
        x=-10;
        y--;
    }
    return 0;
}
