#include <iostream>

using namespace std;

int main()
{
    int a = 0; //宣告整數a
    int b = 0; //宣告整數b

    cout << "Complete this quadratic function and draw a diagram." << endl << "f(x)=ax+b.(a=/=0)" << endl << "a:"; //顯示Complete this quadratic function and draw a diagram.換行"f(x)=ax+b.(a=/=0)"換行 "a:"
    cin >> a; //輸入整數a

    while ( a < -10 || a > 10 || a == 0 ) //迴圈當a小於-10或a大於10或a等於0

    {
        cout << "illegal input!" << endl <<  "a:"; //顯示illegal input!換行a
        cin >> a; //輸入整數a
    }

    cout << "b:"; //顯示整數b
    cin >> b; //輸入整數b

    while ( b < -10 || b > 10 ) //迴圈當b小於-10或b大於10
    {
        cout << "illegal input!" << endl <<  "b:";  //顯示illegal input!換行b
        cin >> b; //輸入整數b
    }
    if ( b >= 0) //判斷b大於等於0時
    {
        cout << "f(x)=" << a << "x" << "+" << b << "." << "(a=/=0)" << endl; //顯示f(x)=ax+b.(a=/=0)換行
    }
    else //其餘
    {
        cout << "f(x)=" << a << "x" <<  b << "." << "(a=/=0)" << endl; //顯示f(x)=ax b.(a=/=0換行
    }

    int xMax = 10; //宣告xMax等於10
    int yMax = -10; //宣告yMax等於-10
    int x = -10; //宣告x等於-10
    int y = 10;//宣告y等於10

    while ( y >= yMax ) //迴圈當y大於等於yMax

    {
        while( x <= xMax ) //迴圈x小於等於xMax
        {
            if( y == a*x+b ) //判斷當y等於a*x+b時
            {
                cout << "*"; //顯示*

            }
            else if( x == 0 && y ==0 ) //判斷當x等於0且y等於0時
            {
                cout << "+"; //顯示+

            }
            else if( x == 0 ) //判斷當x等於0
            {
                cout << "|"; //顯示|

            }
            else if( y == 0 ) //判斷當y等於0時
            {
                cout << "-"; //顯示-

            }

            else //其餘
            {
                cout << " "; //顯示

            }
            x++; //使x值加1

        }
        cout << endl; //換行
        x = -10; //使x等於-10
        y--; //使y值減1
    }
    return 0;
}

