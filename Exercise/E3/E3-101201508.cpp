#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a ;                           //宣告使用者輸入的a
    int b ;                           //宣告使用者輸入的b
    int i ;                           //畫圖時的y座標
    int j ;                           //畫圖時的x座標
    cout << "Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)\n";
    cout << "a:";
    cin >> a ;                        //使用者輸入a
    while (a<-10 || a>10 || a==0){    //如果有錯就進去直到成功之後出來
        cout << "illegal input\n";
        cout << "a:";
        cin >> a ;
    }
    cout << "b:";
    cin >> b ;
    while (b<-10 || b>10 || b==0){    //如果有錯就進去直到成功之後出來
        cout << "illegal input\n";
        cout << "b:";
        cin >> b ;
    }
    if (b>0){
        cout <<"f(x)=" << a << "x+" << b <<".(a=/=0)\n";   //輸出方程式
    }
    else if (b=0){
        cout <<"f(x)=" << a << "x" <<".(a=/=0)\n";
    }
    else{
        cout <<"f(x)=" << a << "x-" << (-1)*b <<".(a=/=0)\n";
    }
    for (i=-10;i<11;++i){
        for (j=-10;j<11;++j){
            if (-1*i==a*j+b)          //如果是在那整數點上就輸出*
                cout << "*";
            else if (i==0 && j==0)    //如果在原點的話就輸出+
                cout << "+";
            else if (j==0)            //如果是x軸的話就是j=0的時候。就輸出|
                cout << "|";
            else if (i==0)            //如果是y軸的話就是i=0的時候。就輸出-
                cout << "-";
            else                      //沒有要輸入符號的時候就用" "把它填住
                cout << " ";
        }
        cout <<endl;                  //每行做完要換行
    }
    return 0;
}
