#include<cstdio>
void input(int *x)              //輸入優化
{
    char cha;                   //宣告暫存輸入的字元
    int neg=1;                  //宣告正負狀態的變數
    *x^=*x;                     //初始化
    while(cha=getchar())        //跳過' '及'\n'
        if(cha!=' '&&cha!='\n')
            break;
    if(cha=='-')                //如果是負數
        neg=-1;                 //設定為負數
    else
        *x=cha-48;
    while(cha=getchar())        //讀入字元
    {
            if(cha==' '||cha=='\n') //輸入結束則跳出迴圈
                break;
            *x=*x*10+cha-48;        //存入x
        }
    *x*=neg;                    //設定正負狀態
}
int main()
{
    int a,b;                                //宣告a&b;
    puts("Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)");    //輸出敘述
A:
    printf("a: ");                          //輸出輸入提示
    input(&a);                              //輸入a
    if(a<-10||a>10||a==0)                   //如果格式不符
    {
            puts("illegal input!");             //輸出"illegal input!"
            goto A;                             //重新輸入a
        }
B:
    printf("b: ");                          //輸出輸入提示
    input(&b);                              //輸入b
    if(b<-10||b>10)                         //如果格式不符
    {
            puts("illegal input!");             //輸出"illegal input!"
            goto B;                             //重新輸入b
        }
    printf("f(x)=%dx+%d.(a=/=0)\n",a,b);    //輸出敘述
    for(int i=10; i!=-11; i--)              //進入迴圈
    {
            for(int j=-10; j!=11; j++)
            {
                        if(j*a+b==i)                    //如果圖形經過這點
                            putchar('*');               //輸出'*'
                        else if(i==0)                   //如果y等於0
                        {
                                        if(j==0)                    //如果x也等於0
                                            putchar('+');           //輸出'+'
                                        else
                                            putchar('-');           //輸出'-'
                                    }
                        else if(j==0)                   //如果x等於0
                            putchar('|');               //輸出'|'
                        else
                            putchar(' ');               //輸出' '
                    }
            puts("");                           //換行
        }
}

