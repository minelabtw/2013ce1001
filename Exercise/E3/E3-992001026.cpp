#include <iostream>

using namespace std;

int main() {
    //提示使用者
    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;
    int a, b;

    //輸入 a, b
    cout << "a: ";
    cin >> a;
    cout << "b: ";
    cin >> b;

    //輸入錯誤的話重新輸入
    while( a == 0 || a > 10 || a < -10 || b > 10 || b < -10 ) {
        cout << "illegal input!" << endl;
        cout << "a: ";
        cin >> a;
        cout << "b: ";
        cin >> b;
    }

    //另出陣列儲存要輸出的 hcar
    char graphic[21][21] = {'\0'};
    //畫 x , y 軸
    for( int i=0 ; i<21 ; ++i ) {
        graphic[10][i] = '-';
        graphic[i][10] = '|';
    }
    //畫原點
    graphic[10][10] = '+';
    //畫方程式
    for( int i=0 ; i<21 ; ++i ) {
        for( int j=0 ; j<21 ; ++j ) {
            if( j-10 == (10-i)*a - b ) {
                graphic[i][j] = '*';
            }
        }
    }
    //把方程式印出來
    cout << "f(x) = " << a << "x + " << b << "." << endl;
    //把陣列印出來
    for( int i=0 ; i<21 ; ++i ) {
        for( int j=0 ; j<21 ; ++j ) {
            cout << graphic[i][j];
        }
        cout << endl;
    }

    return 0;
}
