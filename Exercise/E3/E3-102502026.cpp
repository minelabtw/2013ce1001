//E3-102502026
// Linear Function

#include <iostream>
using namespace std;

int main ()//start program
{
    int a;// define a
    int b;//define b
    int x; //define x
    int y; //defince y

    cout<<"Complete this linear function and draw a diagram\n";// tell about for what is this program
    cout<<"f(x)= ax+b. (a=/=0)\n"; //tell about the function
    cout<<"a:";//ask a
    cin>>a;
    while (a==0 or a>10 or a<-10)//loop when a is equal to 0, greater than 10 and smaller than -10
    {
        cout<<"illegal input!\n";// wrong
        cout<<"a:";//ask again a
        cin>>a;
    }

    cout<<"b:";//ask b
    cin>>b;
    while (b>10 or b<-10)//loop when b is greater than 10 and smaller than -10
    {
        cout<<"illegal input!\n";//wrong
        cout<<"b:";//ask again b
        cin>>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)\n"; //this is the result of the function

    for (y=10; y>=-10; y--) //define y=10 and needs to go to -10, going -1... ex: 10,9,8...
    {
        for (x=-10; x<=10; x++) //define x=-10 and needs to go to 10, going 1... ex: -10,-9,-8...
        {
            if (a*x+b==y) // formula to draw the function
                cout<<"*";
            else if (x==0 and y==0) //when x and y equal to 0 is the origin
                cout<<"+";
            else if (x==0) //when x equal to 0 it draws the y axis
                cout<<"|";
            else if (y==0) //when y equal to 0 it draws the x axis
                cout<<"-";
            else //when is nothing...
                cout<<" ";
        }
        cout<<"\n";

    }
    return 0;

}
