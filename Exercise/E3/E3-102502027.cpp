#include <iostream>

using namespace std;

int main()
{
    int A ;
    int B ;

    cout<<" Complete this linear function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)" <<endl;
    cout<<"a :" ;
    cin>> A ;

    cout<<"b :" ;
    cin>> B ;
    cout<<"f(x)="<<A<<"x+"<< B <<".(a=/=0)"<<endl;


    //while(cin>>A>>B)
    //{
        if(A<-10 || A>10 || A==0 || B<-10 || B>10)
        {
            cout<<"illegal input!"<<endl;
            //continue;
        }
        for(int y=10; y>=-10; y--,cout<<endl) // y-axis
            for(int x=-10; x<=10; x++) // x-axis
            {
                if(y==A*x+B) // on the y=A*x+B
                    cout<<"*";
                else if(x==0 && y==0) // origin
                    cout<<"+";
                else if(x==0) // on the y-axis
                    cout<<"|";
                else if(y==0) // one the x-axis
                    cout<<"-";
                else // other
                    cout<<" ";
            }
    //}


    return 0;
}


