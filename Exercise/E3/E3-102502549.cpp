#include <iostream>
using namespace std;

int main()
{
    int x=-10;//宣告x初始座標為-10
    int y=10;//宣告y初始座標為-10
    int a=0;//宣告一次項係數，並初始為0
    int b=0;//宣告零次項係數，並初始為0

    cout<<"Complete this linear function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;

    while(true)//檢驗a的範圍是否合理
    {
        cout<<"a:";
        cin>>a;
        if(a<-10||a>10)
            cout<<"illegal input!"<<endl;
        else
            break;

    }

    while(true)//檢驗b的範圍是否合理
    {
        cout<<"b:";
        cin>>b;
        if(b<-10||b>10)
            cout<<"illegal input!"<<endl;
        else
            break;

    }

    while(y>=-10)//開始畫圖啦!
    {
        while(x<=10)
        {
            if(x==0&&y!=0)
                cout<<"|";
            else if(y==0&x!=0&&a*x+b!=y)
                cout<<"-";
            else if(x==0&&y==0&&a*x+b!=y)
                cout<<"+";
            else if(a*x+b==y)
                cout<<"*";
            else
                cout<<" ";
            x++;

        }
        cout<<endl;
        y--;
        x=-10;//記得把x值變回-10
    }

    return 0;
}
