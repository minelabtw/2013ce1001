#include <iostream>
using namespace std;

int main()
{
    int a = 0;                                                                  //設定a,b為方程式所需變數
    int b = 0;
    int x = -10;                                                                //設定x,y為繪圖用變數
    int y = 10;

    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;

    cout << "a:";
    cin >> a;
    while (a<-10 || a==0 || a>10)                                               //判斷a是否符合所需範圍
    {
        cout << "illegal input!" << endl;
        cout << "a:";
        cin >> a;
    }

    cout << "b:";
    cin >> b;
    while (b<-10 || b>10)                                                       //判斷b是否符合所需範圍
    {
        cout << "illegal input!" <<endl;
        cout << "b:";
        cin >> b;
    }

    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;                    //把所得的方程式輸出

    while (y>=-10)                                                              //設定繪圖的高從y=10~-10
    {
        while (x<=10)                                                           //設定繪圖的寬從x=10~-10
        {
            if (y==a*x+b)                                                       //如果該座標為方程式的圖形，用*顯示
                cout << "*";
            else if (x==0 && y==0)                                              //如果該座標為原點(0,0)，用+顯示
                cout << "+";
            else if (x==0)                                                      //如果該座標為Y軸(x=0)，用|表示
                cout << "|";
            else if (y==0)                                                      //如果該座標為X軸(y=0)，用-表示
                cout << "-";
            else                                                                //其餘皆以空格顯示
                cout << " ";
            x++;                                                                //重複累加直到x=10
        }
        x=-10;                                                                  //離開while後，重置變數x的值回-10
        y--;                                                                    //重複向下繪圖直到y=-10
        cout << endl;
    }
    return 0;
}
