#include<iostream>
using namespace std;

int main()
{
    int x=-10; //宣告x座標，起始為-10
    int y=10;  //宣告y座標，起始為10
    int a,b;     //宣告型別為整數的斜率a、y截距b

    cout<<"Complete this linear function and draw a diagram."<<endl;
    //輸出"Complete this linear function and draw a diagram."
    cout<<"f(x)=ax+b.(a=/=0)"<<endl; //輸出"f(x)=ax+b.(a=/=0)"
    cout<<"a: "; //輸出"a: "
    cin>>a;      //輸入變數a
    while(a<(-10)|a>10|a==0) //當a不符合條件，也就是不在10到-10的範圍或等於0
    {
        cout<<"illegal input!"<<endl; //輸出"illegal input!"
        cout<<"a: ";  //輸出"a: "
        cin>>a;       //重新輸入變數a
    }
    cout<<"b: "; //輸出"b: "
    cin>>b;      //輸入變數b
    while(b<(-10)|b>10) //當a不符合條件，也就是不在10到-10的範圍
    {
        cout<<"illegal input!"<<endl; //輸出"illegal input!"
        cout<<"b: ";  //輸出"a: "
        cin>>b;       //重新輸入變數a
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl; //輸出函數的方程式


    while(y>0) //當y座標>0
    {
        while(x<0) //當x座標<0
        {
            //判斷此座標是否是函數上的點
            if(y==a*x+b) //若是函數上的點，輸出"*"
                cout<<"*";
            else         //若不是函數上的點，輸出" "
                cout<<" ";
            x++; //將x座標加1
        }        //進行下一次x座標的判斷
        //y軸上
        if(y==a*x+b)
            cout<<"*";
        else
            cout<<"|";
        x++;
        while(x<=10) //當x座標>0且<=10
        {
            if(y==a*x+b)
                cout<<"*";
            else
                cout<<" ";
            x++;
        }
        cout<<endl; //換行
        x=-10;      //重新將x座標定為-10
        y--;        //y座標減1
    }               //進行下一次y座標判斷

    //x軸上
    while(x<0)
    {
        if(y==a*x+b)
            cout<<"*";
        else
            cout<<"-";
        x++;
    }
    //原點
    if(y==a*x+b)
        cout<<"*";
    else
        cout<<"+";
    x++;
    while(x<=10)
    {
        if(y==a*x+b)
            cout<<"*";
        else
            cout<<"-";
        x++;
    }
    cout<<endl;
    x =-10;
    y--;

    while(y>=-10)
    {
        while(x<0)
        {
            if(y==a*x+b)
                cout<<"*";
            else
                cout<<" ";
            x++;
        }
        if(y==a*x+b)
            cout<<"*";
        else
            cout<<"|";
        x++;
        while(x<=10)
        {
            if(y==a*x+b)
                cout<<"*";
            else
                cout<<" ";
            x++;
        }
        cout<<endl;
        x=-10;
        y--;
    }
    return 0;
}
