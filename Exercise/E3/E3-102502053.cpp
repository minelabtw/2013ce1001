#include <iostream>
using namespace std;

int main()
{
    int a; // set variable to be input
    int b; // set variable to be input// display "+" in origin
    int x=-10; // call a variavle//limit the looping in between -10 to 10
    int y=10;  // call a variable

    cout << "Complete this linear function and draw a diagram." << endl; // display words
    cout << "f(x)=ax+b.(a=/=0)" << endl; // display words

    do
    {
        cout << "a= "; // input number
        cin >> a;
        if(a>10 || a<-10 || a==0) // data validation
        {
            cout << "illegal input!" <<endl; //display words for improper input
        }
    }
    while(a>10 || a<-10 || a==0);

    do
    {
        cout << "b= ";
        cin >> b; // input number for b
        if(b>10 || b<-10) //data validation
        {
            cout << "illegal input!" <<endl; //display words for improper input
        }
    }
    while(b>10 || b<-10); // looping for nuber of lines

    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl; //display the formula

    while (y>=-10) //counting for y axis
    {
        x=-10;
        while (x<=10) //counting for x axis
        {
            if (y==a*x+b)
            {
                cout << "*"; //show "*" as where the equation display
            }
            else if (x==0 && y==0)
            {
                cout << "+"; // display "+" at origin
            }
            else if (y==0)
            {
                cout << "-"; //display "-" as x axis
            }
            else if (x==0 )
            {
                cout << "|"; //display "|" as y axis
            }
            else
                cout <<" "; //for nothing display in the graph

            x++; //limit the looping in between -10 to 10 (x axis)
        }
        y--; //limit the looping in between -10 to 10 (y axis)
        cout << endl ;
    }
    return 0;
}
