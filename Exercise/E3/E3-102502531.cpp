#include <iostream>

using namespace std;

int main ()
{
    int a=0; //宣告一個變數a,並初始化其數值為0。
    int b=0; //宣告一個變數b,並初始化其數值為0。
    int xmax=10; //宣告一個數xmax,並初始化其數值為10。
    int ymax=-10; //宣告一個數ymax,初始化其數值為-10。
    int x=-10;  //宣告一個變數x,初始化其數值為-10。
    int y=10;  //宣告一個變數y,初始化其數值為-10。


    cout<<"Complete this quadratic function and draw a diagram."<<endl; //將"Complete this quadratic function and draw a diagram."儲存於cout,並印在銀幕上。
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;   //將"f(x)=ax+b.(a=/=0)"儲存於cout,並印在銀幕上。
    do //進入此一迴圈
    {
        cout << "a:"; //將"a:"儲存於cout,並印在銀幕上。
        cin>>a; "f(x)=ax+b.(a=/=0)"; //輸入變數a的值。
        if (a>10||a<-10||a==0)
            cout << "illegal input!"<<endl; //如果a>10||a<-10||a==0則將"illegal input!"印出螢幕。
    }
    while(a>10||a<-10||a==0);  //判斷是否還需不需繼續迴圈。

    do //進入此一迴圈
    {
        cout << "b:"; //將"b:"儲存於cout,並印在銀幕上。
        cin>>b; //輸入變數b的值。
        if (b>10||b<-10)
            cout << "illegal input!"<<endl; //如果b>10||b<-10則將"illegal input!"印出螢幕。
    }
    while(b>10||b<-10);   //判斷是否還需不需繼續迴圈。

    while(y>=ymax) //進入此一迴圈(第一層)。
    {
        while(x<=xmax) //進入此一迴圈(第二層)。
        {
            if(y==a*x+b) //如果y==a*x+b,則在銀幕顯示"*"。
                cout<<"*";
            else if (y==0&&x==0) //或如果y==0&&x==0,則在銀幕顯示"+"。
                cout<<"+";
            else if (x==0&&y!=0) //或如果y==0&&y!=0,則在銀幕顯示"|"。
                cout<<"|";
            else if(y==0) //或如果y==0,則在銀幕顯示"-"。
                cout<<"-";
            else if (y!=0&&x!=0) //或如果y!=0&&x!=0,則在銀幕顯示" "。
                cout<<" ";
            x++; //每次x都向上加1。
        }
        cout<<endl; //換到下一行。
        x=-10; //使重新回到x=-10。
        y--; //每次y都向下減1。

    }

}


