#include <iostream>

using namespace std;

int main()
{
    int numbera ; //宣告一個變數叫做numbera

    int numberb ; //宣告一個變數叫做numberb


    cout<<"Complete this linear function and draw a diagram."<<endl<<"f(x)=ax+b.(a=/=0)\n"<<"a:";
    cin>>numbera;
    while(numbera<-10 || numbera>10 || numbera==0) //使用迴圈並限制條件
        {
            cout<<"illegal input!"<<endl;
            cout<<"a:";
            cin>>numbera;
        }
    cout<<"b:";
    cin>>numberb;
    while(numberb<-10 || numberb>10)
        {
            cout<<"illegal input!"<<endl;
            cout<<"b:";
            cin>>numberb;
        }
    cout<<"f(x)="<<numbera<<"x+"<<numberb<<".(a=/=0)\n";
    for(int y=10;-10<=y;y--) //使用迴圈
        {
            for(int x=-10;x<=10;x++)
            {
                if (y==numbera*x+numberb)
                {
                    cout<<"*";
                }
                else if (x==0 && y==0)
                {
                    cout<<"+";
                }
                else if (x==0)
                {
                    cout<<"|";
                }
                else if (y==0)
                {
                    cout<<"-";
                }
                else
                {
                    cout<<" ";
                }
            }
            cout<< "\n";

        }
    return 0;
}
