#include <iostream>

using namespace std;

int main()
{
    //variable declaration
    int x=-10; //the varible of Cartesian coordinate
    int y=10;  //the varible of Cartesian coordinate
    int a=1;   //the varible form user
    int b=0;   //the varible from user

    cout << "Complete this linear function and draw a diagram.\n"; //show user the purpose of the program
    cout << "f(x)=ax+b, (a=/=0)\n";                                //show user the function

    cout << "a = ";                             //promt user for data
    cin >> a;                                   //read the first integer from user into a
    while (a>10 || a<-10 || a==0)               //Check if a is in the range or not. If not, promt for the data again. If yes, promt for the next data.
    {
        while (a==0)
        {
            cout << "a can't be 0!!!\n";        //If the data of a from user is 0, tell user a must be a nonzero interger.
            cout << "a = ";                     //prompt user for data again
            cin >> a;                           //read the first integer from user into a again
        }
        while (a>10 || a<-10)
        {
            cout << "a is out of range!!!\n";   //If the data of a from user is out of interval [10,10], tell user a must stay in the range.
            cout << "a = ";                     //prompt user for data again
            cin >> a;                           //read the first integer from user into a again
        }
    }
    cout << "b = ";                             //promt user for data
    cin >> b;                                   //read the second integer from user into b
    while (b>10 || b<-10)                       //Check if b is in the range or not. If not, promt for the data again. If yes, draw the diagram.
    {
        cout << "b is out of range!!!\n";       //If the data of b from user is out of interval [10,10], tell user b must stay in the range.
        cout << "b = ";                         //prompt user for data again
        cin >> b;                               //read the first integer from user into a again
    }

    //graph the function and Cartesian coordinate
    while (y>=-10)                //run instructions on (x,y)
    {
        if (y==0)                 //run instructions on (x,0)
        {
            while (x<=10)         //run instructions on x-axis
            {
                if (y==a*x+b)
                    cout << "*";  //show the point of the function on x-axis
                else if (x==0)
                    cout << "+";  //show the orgin
                else
                    cout << "-";  //show x-axis
                x=x+1;            //run the next value of x
            }
        }
        else                      //run instructions on (x,y), and y can't be 0
        {
            while (x<=10)         //run instructions out of x-axis
            {
                if (y==a*x+b)
                    cout << "*";  //show the point of the function out of x-axis
                else if (x==0)
                    cout << "|";  //show y-axis
                else
                    cout << " ";  //show the point out of function
                x=x+1;            //run the next value of x
            }
        }
        cout << endl;             //make a newline after finish a line
        x=-10;                    //x return to -10 after make a newline
        y=y-1;                    //run the next value of y, that is, make a newline
    }
    cout << "f(x)=(" << a << ")x+(" << b << ")" << endl;  //illustrate of result
    return 0;
}
