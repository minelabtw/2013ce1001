#include <iostream>

using namespace std;

int main()

{
    int a=0;                                 //變數整數。
                                            //變數整數。
    int b=0;


    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b. (a=/=0)" << endl;

    cout << "a:";
    cin >> a;
    while (a>10||a<-10||a==0 )                  //以題意限制範圍與不可以等於零。
    {
        cout << "illegal input!" << endl << "a:";       //假如超出範圍或等於零將輸出警告。
        cin >> a;
    }

    cout << "b:";
    cin >> b;
    while (b>10||b<-10)
    {
        cout << "illegal input!" << endl << "b:";
        cin >> b;
    }

    cout << "f(x)=" << a << "x+" << b << ". (a=/=0)" << endl ;      //顯示輸入後改變的函數。

    int x=-10;      //宣告x軸變數整數
    int y=10;       //宣告y變數整數

    while (y>=-10)      //使用迴旋來做y軸坐標
    {
        while (x<=10)       //使用迴旋來做x軸坐標
        {
            if (y==(a*x+b))     //宣告條件顯示
            {
                cout << "*";
            }
             else if (y==0 && x==0)
            {
                cout << "+";
            }
            else if (x==0)
            {
                cout << "|";
            }
            else if (y==0)
            {
                cout << "-";
            }

            else
            {
                cout << " ";
            }
            x++;

        }
    cout << endl;
    x=-10;
    y--;
    }

    return 0;
}
