#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int a=0;
    int b=0;
    int x=-10;
    int y=10;

    cout<<"請輸入x之係數a:";
    cin>>a;
    while(a<-10 ||a>10 || a==0 )
    {
        cout<<"illegal input";
        cout<<endl;
        cout<<"請輸入x之係數a:";
        cin>>a;
    }

    cout<<"請輸入常數b:";
    cin>>b;
    while(b<-10 || b>10 )
    {
        cout<<"illegal input";
        cout<<endl;
        cout<<"請輸入常數b";
        cin>>b;
    }

    while(y>=-10)
    {
        while(x<=10)
        {
            if(y==a*x+b)
                cout<<"*";
            else if(x==0 && y==0)
                cout<<"+";
            else if(x==0)
                cout<<"|";
            else if(y==0)
                cout<<"-";
            else
                cout<<" ";
            x++;
        }
        cout<<endl;
        x=-10;
        y--;
    }
    cout<<endl;
    cout<<"f(x)="<<a<<"x+"<<b;
    return 0;
}
