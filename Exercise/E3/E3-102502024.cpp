#include <iostream>
using namespace std;
int main()
{
    int xMax=10;  //宣告型別為整數的座標和方程式係數
    int x=-10;
    int y=10;
    int a,b;

    cout<<"Complete this quadratic function and draw a diagram.\n";  //輸入題目
    cout<<"f(x)=ax+b.(a=/=0)\n";
    cout<<"a:";
    cin>>a;  //輸入係數
    while (a>10 || a<-10 || a==0)  //判斷是否符合條件
    {
        cout<<"illegal input!\n"<<"a:";
        cin>>a;
    }
    cout<<"b:";
    cin>>b;  //輸入係數
    while (b>10 || b<-10)  //判斷是否符合條件
    {
        cout<<"illegal input!\n"<<"b:";
        cin>>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)\n";
    while (y>=-10)  //產生圖形
    {
        while (x<=10)
        {
            if (y==a*x+b)
            {
                cout<<"*";
            }

            else if (x==0 && y==0)
            {
                cout<<"+";
            }
            else if (x==0)
            {
                cout<<"|";
            }
            else if (y==0)
            {
                cout<<"-";
            }
            else if (y!=a*x+b)
            {
                cout<<" ";

            }


            x++;

        }

        cout<<endl;
        x=-10;
        y--;

    }
    return 0;
}
