#include <iostream>

using namespace std;

int main()
{
    int integer1;                                                                     //宣告名稱為integer1的整數變數
    int integer2;                                                                     //宣告名稱為integer2的整數變數
    int x = -10;                                                                      //將整數變數x的值初始化為-10
    int y = 10;                                                                       //將整數變數y的值初始化為10

          cout << "Complete this quadratic function and draw a diagram." << endl;     //輸出字串Complete this quadratic function and draw a diagram.
          cout << "f(x)=ax+b.(a=/=0)" << endl;                                        //輸出字串f(x)=ax+b.(a=/=0)
          cout << "a: ";                                                              //輸出字串a:
          cin >> integer1;                                                            //輸入變數
    while (integer1<-10||integer1>10||integer1==0){                                   //當integer1<-10||integer1>10||integer1==0
          cout << "illegal input!" << endl;                                           //輸出字串illegal input!
          cout << "a: ";                                                              //輸出字串a:
          cin >> integer1;                                                            //輸入變數
    }

          cout << "b: ";                                                              //輸出字串b:
          cin >> integer2;                                                            //輸入變數
    while (integer2<-10||integer2>10){                                                //當integer2<-10||integer2>10
          cout << "illegal input!" << endl;                                           //輸出字串illegal input!
          cout << "b: ";                                                              //輸出字串b:
          cin >> integer2;                                                            //輸入變數
    }
          cout << "f(x)=" << integer1 << "x" << "+" << integer2 << ".(a=/=0)" << endl;//輸出字串f(x)=integer1x+integer2.(a=/=0)
    while (y>=-10){
        while (x<=10){
               if(y==integer1 * x + integer2)                                         //判斷y==integer1 * x + integer2
                  cout << "*" ;                                                       //輸出字串*
               else if (y==0&&x==0)                                                   //判斷y==0&&x==0
                  cout << "+" ;                                                       //輸出字串+
               else if (x==0)                                                         //判斷x==0
                  cout << "|" ;                                                       //輸出字串|
               else if (y==0)                                                         //判斷y==0
                  cout << "-" ;                                                       //輸出字串-
               else
                  cout << " " ;                                                       //輸出字串
                  x++;                                                                //x+1

        }
          cout << endl;
          x=-10;                                                                      //將x拉回-10
          y--;                                                                        //y+1
    }
    return 0;
}
