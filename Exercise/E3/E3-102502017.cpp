#include<iostream>

using namespace std;

int main()
{
    int a=0,b=0;                //宣告型別為 整數(int) 的a,b
    int fx[21];                 //宣告型別為 整數(int) 的F(x)
    int x=0,y=0;                //宣告型別為 整數(int) 的座標(x,y)
    int correct=0;

    string graph[21][21]={};    //宣告圖形(21x21)，型別為字串(string)

    cout << "Complete this quadratic function and draw a diagram."<<endl;
    cout << "f(x)=ax+b.(a=/=0)"<<endl;

    while(correct!=1)           //輸入a並判斷是否符合-10~10之間並不等於0
        {
        cout << "a: ";
        cin  >> a;
        if(a>10 || a<-10 || a==0) cout <<"illegal input!\n";
        else correct=1;
        }
    correct=0;

    while(correct!=1)           //輸入b並判斷是否符合-10~10之間
        {
        cout << "b: ";
        cin  >> b;
        if(b>10 || b<-10) cout << "illegal input!\n";
        else correct=1;
        }

    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)\n";

    for(x=0;x<21;x++)
        {
        if(x==10)               //將y軸存入graph中
            {
            while(y<21)
                {
                graph[x][y]="|";
                y++;
                }
            }
        else {
            while(y<21)
                {
                if(y==10)       //將x軸存入graph中
                    {
                    graph[x][y]="-";
                    }
                else            //存入空白至graph中
                    {
                    graph[x][y]=" ";
                    }
                y++;
                }
            }
        y=0;
        }

    graph[10][10]="+";          //存入原點至graph中

    x=0;
    while(x<21)                         //計算f(x)之值並判斷是否在-10~10之間
        {
        fx[x] = a*(x-10)+b;
        if(fx[x]>=-10 && fx[x]<=10)
            {
            graph[x][fx[x]+10]="*";     //f(x)之值在，將點(*)存入graph中
            }
        x++;
        }

    x=0;
    y=20;                               //由於繪圖是由 左至右，上至下 的順序 y要先設為座標系中的10
    while(y>=0)
        {
        while(x<21)
            {
            cout << graph[x][y];        //畫出圖形
            x++;
            }
        cout <<"\n";
        x=0;
        y--;
        }

    return 0;
}
