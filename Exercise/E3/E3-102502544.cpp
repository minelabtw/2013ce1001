#include<iostream>
using namespace std;

int main()
{
    int a=0;         //宣告型別為整數,並初始化
    int b=0;         //宣告型別為整數,並初始化
    int xMAX=10;     //宣告型別為整數,並初始化
    int yMAX=-10;    //宣告型別為整數,並初始化
    int x=-10;       //宣告型別為整數,並初始化
    int y=10;        //宣告型別為整數,並初始化

    cout<<"Complete this linear function and draw a diagram.\nf(x)=ax+b.(a=/=0)\na:";
    cin>>a;
    while(a<-10 || a>10 || a==0)   //重覆循環並設定範圍
    {
        cout<<"illegal input!\na:";
        cin>>a;
    }
    cout<<"b:";
    cin>>b;
    while(b<-10 || b>10)       //重覆循環並設定範圍
    {
        cout<<"illegal input!\nb:";
        cin>>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)\n";

    while(y>=yMAX)    //重覆循環並設定範圍
    {
        while(x<=xMAX)   //重覆循環並設定範圍
        {
            if(x==(y-b)/a)
            {
                cout<<"*";
            }
            else if(x==0 && y==0)
            {
                cout<<"+";
            }
            else if(x==0)
            {
                cout<<"|";
            }
            else if(y==0)
            {
                cout<<"-";
            }
            else if(x<0 || x>0)
            {
                cout<<" ";
            }
            x++;
        }
        cout<<endl;
        x=-10;
        y--;
    }
    return 0;
}
