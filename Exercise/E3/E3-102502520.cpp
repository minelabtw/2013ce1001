#include <iostream>
using namespace std;

int main()
{
    int a;                                                         //定義一個整數a
    int b;                                                         //定義一個整數b

    cout<<"Complete this quadratic function and draw a diagram.\n";//輸出"Complete this quadratic function and draw a diagram."然後換行
    cout<<"f(x)=ax+b.(a=/=0)\n";                                   //輸出"f(x)=ax+b.(a=/=0)"然後換行
    cout<<"a:";                                                    //輸出"a:"
    cin>>a;                                                        //輸入一數，取代a的位置

    while (a>10||a<-10||a==0)                                      //檢驗a的範圍
    {
        cout<<"illeagal input!"<<endl;                             //若不符合範圍，輸出"illeagal input!"
        cout<<"a:";                                                //再重新輸入一次
        cin>>a;
    }

    cout<<"b:";                                                    //輸出"b:"
    cin>>b;                                                        //輸入一數，取代b的位置

    while (b>10||b<-10)                                            //檢驗b的範圍
    {
        cout<<"illeagal input!"<<endl;                             //若不符合範圍，輸出"illeagal input!"
        cout<<"b:";                                                //再重新輸入一次
        cin>>b;
    }


    for(int y = 10 ; y >= -10 ; y--)                               //設定y的初始值為10，並從y=10開始檢驗，再往下檢驗
    {
        for(int x = -10 ; x <= 10 ; x++)                           //設定x的初始值為-10，並從x=-10開始檢驗，再往上檢驗
        {
            if(y==a*x+b)                                           //若符合y=ax+b，則輸出"*"
            {cout << "*";}
            else
            {
                if(x==0 && y==0)                                   //若x跟y都為零食為原點，則輸出"+"
                {cout << "+";}
                else if(x==0)                                      //若x=0，輸出"|"，作為y軸
                {cout << "|";}
                else if(y==0)                                      //若y=0，輸出"-"，作為x軸
                {cout << "-";}
                else                                               //其他皆為空白
                {cout << " ";}
            }
        }
        cout<<endl;                                                //每次檢驗一行後，換行
    }

    return 0;
    }


























