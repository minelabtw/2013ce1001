#include<iostream>
using namespace std;

int main()
{
    int a;
    int b;
    int x =-10;
    int y = 10;

    cout <<"Complete this linear function and draw a diagram.\n";       //列出題目
    cout <<"f(x)=ax+b.(a=/=0)\n";                                       //同上

    //以下:輸入a,b值,且值必須符合限定範圍
    cout << "a:";
    cin >> a ;
    while(a>10||a<-10||a==0)
    {
        cout << "illegal input!\n";
        cout << "a:";
        cin >> a;
    }
    cout << "b:";
    cin >> b ;
    while(b>10||b<-10)
    {
        cout << "illegal input!\n";
        cout << "b:";
        cin >> b;
    }
    //以上:輸入a,b值,且值必須符合限定範圍

    while(y>=-10&&y<=10)
    {
        while(x<=10&&x>=-10)
        {
        //關鍵ˇ
            if(y==a*x+b)                    //跑到有焦點的位置時
            {
                    cout << "*";                 //輸出"*"
            }
            else                            //其他情況時(就是沒有交點的時候)
            {
               if(x==0&&y==0)                    //當在原點位置上,且方程式與原點沒有交集
               {
                   cout << "+";                  //輸出"+"
               }
               else if(x==0&&y!=b)               //當在y軸上,且方程式與y軸沒有交集
               {
                   cout << "|";                  //輸出"|"
               }
               else if(y==0&&y!=a*x+b)           //當在x軸上,且方程式與x軸沒有交集
               {
                   cout << "-";                  //輸出"-"
               }
               else                              //其他情況(就是不再x,y軸上,和方程式也沒有焦點)
               {
                   cout << " ";                  //輸出" "
               }
        }
        //關鍵^
        x++;
        }
        cout << "\n";                           //單純換行用
        x=-10;
        y--;
    }
        return 0;
}
