﻿#include <iostream>

using namespace std;

int main ()
{
    int a,b;//設定變數a和b
    cout << "Complete this linear function and draw a diagram." << endl ;//輸出Complete this linear function and draw a diagram.
    cout << "f(x)=ax+b.(a=/=0)" << endl ;//輸出f(x)=ax+b.(a=/=0)

    cout << "a:" ;//輸出a:
    cin >> a ;//輸入a之值

    while (a < -10 || a > 10 )//設定條件
    {
        cout << "illegal input!" << endl ;//輸出illegal input!並換行
        cout <<  "a:";//重新輸出a:
        cin >> a;//重新輸入a之值
    }//當符合條件時執行大括號內之內容

    cout << "b:" ;//輸出b:
    cin >> b ;//輸入b之值
    while (b < -10 || b > 10)//設定條件
    {
        cout << "illegal input!" << endl ;//輸出illegal input!並換行
        cout <<  "b:";//重新輸出b:
        cin >> b;//重新輸入b之值
    }

    cout << "f(x)=" << a << "x+" << b << ". (a=/=0)" << endl;//輸出f(x)=ax+b.(a=/=0)並將a和b之值帶入
    int xMax=10;//設定xMax之值為10
    int yMax=10;//設定yMax之值為10
    int xMin =-10;//設定xMin之值為-10
    int yMin =-10;//設定yMin之值為-10
    int x = -10;//設定x之值為-10
    int y = 10;//設定y之值為10

    while (y >= yMin)//設定條件
    {
        while ( x <= xMax)//設定條件
        {
        if (y == a * x + b)//當符合方程式時
        cout << "*";//使方程式上的點輸出*
        else if ( x == 0 && y == 0)//當位於原點時
        cout <<"+";//輸出+
        else if ( x == 0 )//當在y軸上時
        cout << "|";//輸出|
        else if ( y == 0 )//當在x軸上時
        cout << "-";//輸出-

        else
        cout << " ";//其他位置則空白
            x++;//x逐漸增加1加到10為止
        }
        cout << endl;//換行
        x = xMin;//讓x回到-10
        y--;//y之值逐漸減少1
    }

    return 0;
}

