#include<iostream>                   //導入iostream函數庫 
using namespace std;                 //使用名字空間std

int main()
{
	int a=0,b=-100; //宣告數值a，並初始化為0；數值 b，並初始化為-100 
	int x,y=10; //宣告數值x和y，並初始化y為10 
	int c; //宣告數值c 
	
	cout<<"Please enter the number a,b"<<endl; 
	
    while(a<-10||a>10||a==0) //運用while迴圈，判斷a是否符合"-10<=a<=10且a不等於0"，若是則跳出迴圈，若否則重新跑此迴圈 
    {
	cout<<"a= ";
	cin>>a;  
	if(a<-10||a>10||a==0) //若a<-10或a>10或a=0的時候，顯示 illegal input!
	cout<<"illegal input!"<<endl;
	}
	
	while(b<-10||b>10) // //運用while迴圈，判斷b是否符合"-10<=b<=10"，若是則跳出迴圈，若否則重新跑此迴圈 
    {
	cout<<"b= ";
	cin>>b;
	if(b<-10||b>10) //若b<-10或b>10的時候，顯示 illegal input!
	cout<<"illegal input!"<<endl;
	}
	
	cout<<"Complete this linear function and draw a diagram."<<endl;
	cout<<"f(x)=ax+b.(a=/=0)"<<endl;
	cout<<"a: "<<a<<endl<<"b: "<<b<<endl;
	cout<<"f(x)="<<a<<"x";
	if(b>=0) //若b>=0則顯示"+" 
	cout<<"+";
	cout<<b<<".(a=/=0)"<<endl;
	
	while(y>=-10&&y<=10) //若y>=-10或y<=10時，跑此迴圈；若否則跳出此迴圈。 
	{
		
		for(x=-10;x<=10;x++)  
		{
			c=a*x+b; //計算把x值帶入方程式後所得之f(x) 
			if(c==y) //若f(x)等於y值時，標出*點 
			cout<<"*";
			else if(y==0&&x!=0) //此迴圈用來標出x軸之軸線，但排除原點 
			cout<<"-";
			else if(y==0&&x==0) //此迴圈用來標示出原點"+" 
			cout<<"+";
			else if(c!=y&&x==0) //此迴圈用來標出y軸之軸線 
			cout<<"|";
			else //若不符合上述if及else if的條件時，輸出空白 
			cout<<" ";	
		}
		cout<<endl;
		y--; //將y值減1 
	}
	

	return 0;
}
