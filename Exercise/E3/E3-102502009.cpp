#include<iostream>
using namespace std;

int main()
{
    int a=0;
    int b=0;
    int squarex=0;
    int squarey=0;

    cout<<"Complete this quadratic function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;

    cout<<"a: ";  //輸入a
    cin>>a;

    while(a<-10||a>10) //如果輸入的a不在-10~10的範圍內，則重新輸入
    {
        cout<<"illegal input!"<<endl;

        cout<<"a: ";
        cin>>a;
    }

    cout<<"b: ";  //輸入b
    cin>>b;

    while(b<-10||b>10) //如果輸入的b不在-10~10的範圍內，則重新輸入
    {
        cout<<"illegal input!"<<endl;

        cout<<"b: ";
        cin>>b;
    }

    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;

    for(squarey=10;squarey>=-10;squarey--) //y座標從10開始，每跑一次迴圈就減1，直到y座標變為-10
    {
        for(squarex=-10;squarex<=10;squarex++) //x座標從-10開始，每跑一次迴圈就加1，直到x座標變為10
        {
            if ( a*squarex+b == squarey ) //假設y=a*x+b，則輸出*,程式由上而下執行
                cout << "*" ;
            else if(squarey==0&squarex==0) //假設y=0且x=0，則輸出+
                cout << "+" ;
            else if(squarey==0) //假設y=0，則輸出-
                cout << "-" ;
            else if(squarex==0) //假設x=0，則輸出+
                cout << "|" ;
            else
                cout <<" " ; //其餘輸出空白
        }
        cout << endl ; //x座標每跑完1次，就換1行，繼續執行迴圈內容
    }

    return 0;
}


