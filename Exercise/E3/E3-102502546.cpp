#include<iostream>
using namespace std;

int main()
{
    int a=0;
    int b=0;

    cout <<"Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)"<<endl;

    cout <<"a: ";
    cin >>a;
    while(a<-10 || a >10 || a==0)//當a<-10 or a>10 or a=0
    {
        cout<<"illegal input!\na: ";
        cin >>a;
    }

    cout <<"b: ";
    cin >>b;
    while(b<-10 || b >10 || b==0)
    {
        cout<<"illegal input!\nb: ";
        cin >>b;
    }
    cout <<"f(x)="<<a<<"x"<<"+"<<b<<"y.(a=/=0)"<<endl;

    int y=10;
    while (y>-11)//當y>-11時，即執行迴圈
    {
        int x=-10;
        while (x<11)//當x<11時，即執行迴圈
        {
            if(y == a*x+b)//如果y=a*x+b
                cout <<"*";
            else if(x==0 && y==0)//非以上情況，先顯示原點為+
                cout <<"+";
            else if(x==0)//非原點、函數，顯示y軸為|
                cout <<"|";
            else if(y==0)//非原點、函數、x=0，顯示x軸為-
                cout <<"-";
            else if(y != 0 && x !=0)//其餘皆顯示為空白
                cout <<" ";

            x++;
        }
        cout<<endl;
        y--;

    }

    return 0;
}

