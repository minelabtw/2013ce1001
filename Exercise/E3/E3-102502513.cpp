#include <iostream>
using namespace std;
int main()
{
    int a=50 , b=50;
    int x=-10 , y=10;

    cout << "Complete this quadratic function and draw a diagram.\n";
    cout << "f(x)=ax+b.(a=/=0)\n";

    while(a<-10||a>10||a==0)
    {
        cout << "a:";
        cin >> a;
        if (a<-10||a>10||a==0)
            cout << "illegal input!\n";
        else
        {
        }
    }

    while(b<-10||b>10)
    {
        cout << "b:";
        cin >> b;
        if (b<-10||b>10)
            cout << "illegal input!\n";
        else
        {
        }
    }

    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)\n";

    while(y>=-10)                                        //使 y 由10到-10循環(由上到下)
    {
        while(x<=10)                                     //使 X 由-10到10循環(由左至右)

        {
            if(y==a*x+b)                                 //使位於方程式的點用米號印於上
                cout << "*";
            else if(x==0&&y==0)                          //用 "+" 代表原點位置
                cout << "+";
            else if(y==0)                                //用 "-" 將X軸畫出
                cout << "-";
            else if(x==0)                                //用 "|" 將Y軸畫出
                cout << "|";
            else                                         //其餘點留白
                cout << " ";

            x++;
        }

        cout << endl;                                    //換行,使程式再重新跑一次
        x=-10;                                           //將"x"初始為-10讓其能再次的由左至右重新跑一次
        y--;
    }

    return 0;
}
