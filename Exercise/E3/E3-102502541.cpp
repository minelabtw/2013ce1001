#include <iostream>

using namespace std;

int main()
{
    int a = 0;//宣告整數a
    int b = 0;//宣告整數b

    cout << "Complete this quadratic function and draw a diagram." << endl;//輸出Complete this quadratic function and draw a diagram. 換行
    cout << "f(x)=ax+b.(a=/=0)" << endl;//輸出f(x)=ax+b.(a=/=0) 換行
    cout << "a:";//輸出a:
    cin >> a;//輸入a
    while (a < -10 || a > 10 || a == 0)//迴圈 當a小於-10或 a大於10 或 a等於0
    {
        cout << "illegal input!" << endl << "a:";//輸出illegal input! 換行 輸出a:
        cin >> a;//輸入a
    }
    cout << "b:";//輸出b:
    cin >> b;//輸入b
    while (b < -10 || b >10)//迴圈 當b小於-10 或 b大於10
    {
        cout << "illegal input!" << endl << "b:";//輸出illegal input! 換行 輸出b:
        cin >> b;//輸入b
    }
    cout << "f(x)=" << a <<"x+" << b <<".(a=/=0)" << endl;//輸出f(x)=變數ax+變數b.(a=/=0)
    int x = -10;//宣告整數x=-10
    int y = 10;//宣告整數y=10 迴圈起點
    while (y >= -10)//迴圈 當y大於等於-10
    {

        while(x <= 10 )//迴圈 當x小於等於10
        {
            if (y == a * x + b) //條件一 y=a*x+b
                cout << "*";//輸出*

            else if (y == 0 && x != 0)//條件二 y等於0 且 x不等於0
                cout << "-";//輸出-

            else if (x == 0 && y !=0)//條件三 x等於0 且 y不等於0
                cout << "|";//輸出|

            else if (x == 0 && y == 0)//條件四 x等於0 且 y等於0
                cout << "+";//輸出+
            else//其餘
                cout << " ";//輸出空白
			x++;//x遞增
		}
        x=-10;//x等於-10 迴圈起點
        y--;//y遞減
        cout << endl;//輸出 換行
    }
    return 0;
}

