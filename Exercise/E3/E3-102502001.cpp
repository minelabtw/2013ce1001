#include <iostream>
using namespace std ;

int main()
{
    int integer1;
    int integer2;
    int xMax=10 ;
    int yMax=10 ;
    int x=-10 ;
    int y=-10 ;

    cout << "Complete this quadratic function and draw a diagram." <<endl ;
    cout << "f(x)=ax+b.(a=/=0)" <<endl ;
    cout << "a:";
    cin >> integer1 ;
    while (integer1>10 or integer1<-10 or integer1==0)
    {
        cout <<"illegal input!"<< endl ;
        cout << "a:";
        cin >> integer1 ;
    }

    cout << "b:" ;
    cin >> integer2 ;
    while (integer2>=10 or integer2<=-10)
    {
        cout <<"illegal input!"<< endl ;
        cout << "b:";
        cin >> integer2 ;
    }

    cout <<"f(x)="<<""<<integer1<<"x"<<"+"<<"("<<""<<integer2<<")"<<endl ;

    while (x<=xMax)
    {
        while(y<=yMax)
        {
            if(y==integer1*x+integer2)
            {
                cout<<"*";
                x++;
            }
            else if(y==0)
            {
                cout<<"-";
                y++;
            }
            else if(x==0)
            {
                cout <<"|";
                x++;
            }
            else if(x==0 and y==0)
            {
                cout<<"+";
                x++;
            }
            else
            cout<<" ";


            cout << endl ;
            x = 0 ;
            y++;

        }
    }
    return 0 ;
}
