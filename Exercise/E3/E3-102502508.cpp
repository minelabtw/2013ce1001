#include<iostream>
using namespace std ;
int main()
{

    // 這次的主題是利用寫程式來算出定義域有限的直線方程式的解且劃出該函數圖形
    cout<<"Complete this linear function and draw a diagram."<<endl;
    int a=0;
    int b=0;

    cout << "f(x)=ax+b.(a=/=0)"<<endl;
    cout << "a:";
    cin >> a;
    while (a<-10 or a>10 or a==0)
    {
        cout << "illegal input!"<<endl ;
        cout << "a:";
        cin  >> a ;

    }
    cout << "b:";
    cin >> b ;
    while (b<-10 or b>10)
    {
        cout << "illegal input!"<<endl ;
        cout << "b:";
        cin >> b ;
    }
    cout <<"f(X)="<< a << "x"<< "+" << b <<" . "<<"(a=0/=0)"<<endl ;


    int   p   = 0;
    int   q   = 0;

    for(q=10 ; q>=-10 ; q=q-1)
    {
        for(p=-10 ; p<=10 ; p=p+1)
        {
            if(a*p+b==q)
                cout<<"*" ;         //  * 這個符號代表的是方程式中的線條

            else if(p==0 and q==0)
                cout<<"+" ;          // + 這個符號代表的是此方程式未經過原點

            else if (p==0)
                cout<<"|" ;         // | 這個符號表示的是方程式圖形中的y軸

            else if (q==0)
                cout<<"-" ;         // -  這個符號代表的是方程式中的x軸

            else
                cout<<" " ;


        }
        cout<<endl;
    }
    return 0 ;
}
