#include <iostream>
using namespace std;
int main()
{
    int a=0;//宣告整數變數a
    int b=0;//宣告整數變數b
    int x=-10;//宣告整數變數x
    int y=10;//宣告整數變數y
    int xmax=10;//宣告整數變數X可達的最達值
    int ymin=-10;//宣告整數變數y可達的最小直

    cout<<"Complete this quadratic function and draw a diagram.""\n";
    cout<<"f(x)=ax+b.(a=/=0)""\n";
    cout<<"a:";
    cin >>a;
    while(a>10 || a<-10)//在輸入a之後，當a的值符合條件時，產生迴圈的效果
    {
        cout<<"illegal input!"<<"\n";
        cout<<"a:";
        cin >>a;
    }
    cout<<"b:";
    cin >>b;
    while(b>10 || b<-10)//在輸入b之後，當b的值符合條件時，產生迴圈的效果
    {
        cout<<"illegal input!"<<"\n";
        cout<<"b:";
        cin >>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<"\n";

    while(y>=ymin)//以下是讓座標一個點一個點的判定的迴圈，從(-10,10)到(10,-10)
    {
        while(x<=10)
        {
            if(y==a*x+b)//判定的順序是y=ax+b這條線>原點>XY軸>空白
            {
                cout<<"*";
            }

            else if(x==0 && y==0)
            {
                cout<<"+";
            }

             else if(x==0)
            {
                cout<<"|";
            }

            else if(y==0)
            {
                cout<<"-";
            }
            else
            {
                cout<<" ";
            }

            x++;//一個點叛定完 X+1繼續判定
        }
        cout<<"\n";//結束一行21個點後換行
        x=-10;//從最左邊重新開始
        y=y-1;//因為換行所以y要減1

    }
    return 0;
}
