#include <iostream>

using namespace std;

int main()
{
    int a =0;                                       //宣告整數變數
    int b =0;                                       //宣告整數變數

    cout << "Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)" << endl;  //將字串顯現在螢幕
    cout << "a:" ;                                  //將字串顯現在螢幕
    cin  >> a;                                      //將輸入的值給a

    while (a<-10 || a>10 || a==0)                   //設定題目條件，並在符合題目條件下執行下列動作，
    {
        cout << "illegal input!" << endl;           //將字串顯示於螢幕
        cout << "a:" ;                              //將字串顯示於螢幕
        cin >> a;                                   //將數值給a
    }

    cout << "b:" ;                                  //將字串顯示於螢幕
    cin  >> b ;                                     //將數值給b

    while (b<-10 || b>10 )                          //設定題目條件，並符合題目條件下執行下列動作
    {
        cout << "illegal input!" << endl;           //將字串顯示於螢幕
        cout << "b:" ;                              //將字串顯示於螢幕
        cin  >> b ;                                 //將數值給b
    }


    cout << "f(x)=" << a << "x+" << b << endl;      //將輸入的值放進顯示出的方程式




    int xMin = -10;                                 //宣告整數變數，並將其值初始化為-10
    int yMin = -10;                                 //宣告整數變數，並將其值初始化為-10
    int x = 10;                                     //宣告整數變數，並將其值初始化為10
    int y = 10;                                     //宣告整數變數，並將其值初始化為10

    while(y>=yMin)                                  //設定條件，製造迴圈
    {
        while(x>=xMin)                              //設定條件，製造迴圈
        {

            if (y==(-1)*x*a+b)                      //在條件下，執行下列動作
            {
                cout << "*" ;                       //顯示字串
            }

            else if (x==0 && y!=0)                  //符合條件下，執行下列動作
            {
                cout << "|";                        //顯示字串
            }

            else if (x==0 && y==0)                  //符合條件下，執行下列動作
            {
                cout << "+" ;                       //顯示字串
            }

            else if (y==0 && x!=0)                  //符合條件下，執行下列動作
            {
                cout << "-" ;                       //顯示字串
            }
            else                                    //無論以上條件是否執行，皆會執行下列動作
                cout << " " ;                       //顯示字串

            x--;                                    //將設定的值減1，以致迴圈持續至不符條件
        }


    cout << endl;                                   //每一次迴圈完成後，跳下一行繼續迴圈
    x=10;                                           //將上一迴圈使用的值，初始化至10，並重新執行迴圈
    y=y-1;                                          //將設定的值在每一迴圈執行完後，減1以便執行迴圈至不符條件
    }

    return 0;
}





