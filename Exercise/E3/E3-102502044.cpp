/*************************************************************************
    > File Name: E3-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年10月09日 (週三) 15時44分18秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

char map[21][22], *px[21], **ori;

/************************************/
/*  str means an string for ui      */
/* *tmp means a point to save score */
/* flag= 1 means that *tmp can be 0 */
/************************************/
void input(const char *str, int *tmp, int flag)
{
	for(;;)
	{
		printf(str); //input score
		scanf("%d", tmp);

		if(*tmp == 0) //check input can be 0
		{
			if(flag)
				break;
			else
				puts("illegal input!");
		}
		else if(-10<=*tmp && *tmp<=10) //check input is in range [-10, 10]
			break;
		else
			puts("illegal input!");
	}
}

void init_map()
{
	for(int i=0 ; i<=20 ; i++)
		px[i] = map[i] + 10;

	ori = px + 10;

	for(int x=-10 ; x<=10 ; x++)
		for(int y=-10 ; y<=10 ; y++)
			ori[x][y] = ' ';

	for(int i=-10 ; i<=10 ; i++)
	{
		ori[i][0] = '|';
		ori[0][i] = '-';
	}
	ori[0][0] = '+';
}

int main()
{
	
	/* a b means two integer for f(x) = ax + b */
	int a, b;

	/* print ui message */
	puts("Complete this quadratic function and draw a diagram.");
	puts("f(x)=ax+b.(a=/=0)");
	
	/* input all variable */
	input("a: ", &a, 0);
	input("b: ", &b, 1);

	/* print the f(x) */
	printf("f(x)=%dx+%d.\n", a, b);

	init_map();

	for(int i=-10 ; i<=10 ; i++)
		for(int j=-10 ; j<=10 ; j++)
			if(a*i+b == j)
				ori[j][i] = '*';
	
	for(int i=20 ; i>=0 ; i--)
		puts(map[i]);

	return 0;
}

