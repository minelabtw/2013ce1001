#include <iostream>
using namespace std;

int main()
{
    int a=0; //宣告整數a,數值為0
    int b=0; //宣告整數b,數值為0

    cout << "Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)\n";
    cout << "a= ";
    cin >> a;

    while(a<-10 || a>10 || a==0) //當a<-10或a>10或a=0
    {
        cout << "illegal input!\na= ";
        cin >> a;
    }

    cout << "b= ";
    cin >> b;
    while(b<-10 || b>10) //當b<-10或b>10
    {
        cout << "illegal input!\nb= ";
        cin >> b;
    }

    cout << "f(x)="<<a<<"x+"<<b<<".(a=/=0)\n";

    for(int y=10;y>-11;y--) //執行迴圈,並宣告整數y,數值為10,在y>-11時執行,每執行一次y就減1
   {for(int x=-10;x<11;x++) //執行迴圈,並宣告整數x,數值為-10,在x<11時執行,每執行一次x就加1
   {if(y == a*x+b) //如果y=a*x+b
   cout << "*";
   else if(x==0 && y==0) //非以上情況,如果x=0且y=0
       cout << "+";
       else if(x==0) //非以上情況,如果x=0
        cout << "|";
       else if(y==0) //非以上情況,如果y=0
        cout << "-";
       else          //非以上情況
        cout << " ";
   }
   cout<<"\n";
   }



    return 0;
}
