#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a;
    int b;
    cout << "Complete this quadratic function and draw a diagram." << endl
    << "f(x)=ax+b.(a=/=0)" << endl << "a:" ;//將函數顯示在螢幕上，並請求輸入a

    cin >> a ;

    while (a < -10 || a > 10 )
    {
        cout << "illegal input!" << endl <<  "a:";
        cin >> a;
    }
    //將a的範圍定義在-10到10之間，如果超出範圍則顯示"illegal input!"

    cout << "b:" ;
    cin >> b ;
    while (b < -10 || b > 10)
    {
        cout << "illegal input!" << endl <<  "b:";
        cin >> b;
    }
    cout << "f(x)=" << a << "x" << "+" << b << "." << "(a=/=0)" << endl;
    //b與a相似

    int xMax=10;
    int yMax=10;
    int x=-10;
    int y=-10;

    while(y<=yMax)
    {
       while(x<=xMax)
       {
           if (-y==a*x+b)//y以上方為-10往下累加，為了將圖形倒置，所以需要加負號
           {
               cout <<"*";//當等號成立的時候，顯示"*"
           }
           else if (y==0 && x==0)
                cout << "+";//將座標的原點設定為+
           else if (y==0)
                cout << "-";//顯示出座標的X軸
           else if (x==0)
                cout << "|";//顯示出座標的Y軸
           else
                cout << " ";//將其餘空格處填滿
        x++;//在每次迴圈結束後，將x的值加1
       }
       cout << endl;//換行
        x=-10;//重新定義x的值為-10
        y++;//在每次迴圈結束後，將y的值加1
    }
    return 0;
}

