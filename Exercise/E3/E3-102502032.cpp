/*
make a imagine array
substitute each x into the function
then set integer solution point as "*"
finally, output array as an imagine
*/

#include <iostream>
using namespace std;

//this func. is used to initialize the inputed array[21][21]
void initialize ( string arr[21][21] )
{
    //initialize all element to space
    for ( int i = 0; i <= 20; i ++ )
        for ( int j = 0; j <= 20; j ++ )
            arr[i][j] = " ";
    //set x-axis and y-axis
    for ( int i = 0; i <= 20; i ++ )
    {
        arr[10][i] = "-";
        arr[i][10] = "|";
    }
    //set orig. point
    arr[10][10] = "+";
}

int main ()
{
    //varible decalaration and initialization
    string figure[21][21];          //a char array; saving data of figure
    initialize ( figure );          //initialize the array: figure
    int a = 0;                      //an integer varible: save the slope
    int b = 0;                      //an integer varible: save the constant

    //explain what function this program is and ask for coefficients
    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;
    cout << "a: ";                          //ask for the coefficient of x
    cin >> a;
    cout << "b: ";                          //ask for the
    cin >> b;

    //output the func.
    if (b >= 0)
        cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;
    else
        cout << "f(x)=" << a << "x" << b << ".(a=/=0)" << endl;

    //set integer points on the line as "*" to relative elements
    for ( int i = -10; i <= 10; i++)
    {
        if( a*i+b <=10 and a*i+b >= -10)        //i is the same to x, just different name
            figure[10-(a*i+b)][i+10] = "*";
    }

    //output result
    for ( int i = 0; i <= 20; i ++ )
    {
        for ( int j = 0; j <= 20; j ++)
            cout << figure[i][j] << " ";
        cout << endl;
    }
}

