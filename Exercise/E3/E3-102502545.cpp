  #include <iostream>

  using namespace std;

  int main()
  {
      int a=0;                      //宣告一變數a。
      int b=0;                      //宣告一變數b。

      cout<<"Complete this quadratic function and draw a diagram.\n";
      cout<<"f(x)=ax+b.(a=/=0)\n";
      cout<<"a:";
      cin>>a;

      while(a<-10||a>10||a==0)       //設一個迴圈來判定a的範圍。
      {
          cout<<"illegal input!\n";
          cout<<"a:";
          cin>>a;
      }

      cout<<"b:";
      cin>>b;

      while(b<-10||b>10||b==0)        //設一個迴圈來判定b的範圍。
      {
          cout<<"illegal input!\n";
          cout<<"b:";
          cin>>b;
      }

      cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)\n";


      int y=10;                               //設一變數y。
      while(y>-11)
      {
          int x=-10;                           //設一變數x。
          while(x<11)
          {
             if(y==a*x+b)
                cout<<"*";
             else if(y==0&&x!=0)
                cout<<"-";
             else if(x==0&&y!=0)
                cout<<"|";
             else if(y==0&&x==0)
                cout<<"+";
             else if(y!=a*x+b)
                cout<<" ";
                x++;
          }
          cout<<endl;
          y--;

      }



        return 0;

  }
