#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer_a;//宣告變數
    int x=0;//宣告x從零開始
    int integer_x;
    int integer_b;
    int y=0;
    int xmax=21;
    int ymax=21;

    cout << "Complete this quadratic function and draw a diagram." << endl;//輸出字串
    cout << "f(x)=ax+b.(a=/=0)" << endl;

    cout << "a: ";
    cin >> integer_a;//輸入變數a
        while ( integer_a > 10 or integer_a < -10 )//加入條件a不能大於十也不能小於負十
        {
            cout << "illegal input!" << endl;
            cout << "a: ";
            cin >> integer_a;
        }

    cout << "b: ";
    cin >> integer_b;
        while ( integer_b > 10 or integer_b < -10 )
        {
            cout << "illegal input!" << endl;
            cout << "b: ";
            cin >> integer_b;
        }


    cout << "f(x)=" << integer_a << "x+" << integer_b << ".(a=/=0)" << endl;//輸出字串


    while ( y <= 10 )//加入y的迴圈
    {
        x = 0;
        while ( x <= 10 )//加入x的迴圈
        {
            integer_x  == ( y - integer_b )/integer_a +10;//計算integer_x的數，並調整為座標系的座標
            if ( integer_x != x )//假如integer_x的數不等於x則輸出空格
                { cout << " "; }
            else//反之則輸出星號
                { cout << "*"; }
                 x++;//x加1再遞迴上去
        }
            integer_x == ( y - integer_b )/integer_a +10;
            if ( integer_x != x )//假如integer_x的數不等於x則輸出空格
                { cout << "|"; }
            else//反之則輸出星號
                { cout << "*"; }
            x++;

         while ( x <= xmax )
        {
            integer_x == ( y - integer_b )/integer_a +10;
            if ( integer_x != x )
                { cout << " "; }
            else
                { cout << "*"; }
                 x++;
        }
        cout << endl;
        x = 0;
        y++;
    }


    cout << " ----------+----------" << endl;//輸出圖樣
    y++;


    while ( y<= ymax )//輸出y從11到21的圖樣
    {
        x = 0;
        while ( x <= 10 )
        {
            integer_x == ( y - integer_b )/integer_a +10;
            if ( integer_x != x )//加入條件
                { cout << " "; }
            else
                { cout << "*"; }
                 x++;
        }
        cout << "|";
        x++;
         while ( x <= xmax )
        {
            integer_x == ( y - integer_b )/integer_a +10;
            if ( integer_x != x )
                { cout << " "; }
            else
                { cout << "*"; }
                 x++;
        }
        cout << endl;
        x = 0;
        y++;
    }



    return 0;
}

