#include <iostream>

using namespace std;

int main()
{
    int a = 0;
    int b = 0;

    cout << "Complete this linear function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;
    cout << "a: ";
    cin >> a;

    while (a<-10 or a==0 or a>10)
    {
        cout << "illegal input!" << endl;
        cout << "a: ";
        cin >> a;
    }

    cout << "b: ";
    cin >> b;

    while (b<-10 or b>10)
    {
        cout << "illegal input!" << endl;
        cout << "b: ";
        cin >> b;
    }

    if (b>0)
    {
        cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;
    }

    else if (b==0)
    {
        cout << "f(x)=" << a << "x.(a=/=0)" << endl;
    }

    else if (b<0)
    {
        cout << "f(x)=" << a << "x" << b << ".(a=/=0)" << endl;
    }

    int xMax = 10;
    int yMax = -10;
    int x = -10;
    int y = 10;

    while (y>=yMax)
    {
        while (x<=xMax)
        {
            if (y==a*x+b)
            {
                cout << "*";
                x++;
            }
            else if (x==0 and y==0)
            {
                cout << "+";
                x++;
            }
            else if (x==0)
            {
                cout << "|";
                x++;
            }
            else if (y==0)
            {
                cout << "-";
                x++;
            }
            else
            {
                cout << " ";
                x++;
            }
        }
        cout << endl;
        x = -10;
        y--;
    }
    return 0;
}
