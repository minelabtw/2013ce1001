#include<iostream>
using namespace std;
int main()
{
    int xMax=10;//宣告x最大值
    int yMax=-10;//宣告y最小值
    int x=-10;//宣告x並=0
    int y=10;//宣告y並=0
    int a=0;//宣告斜率
    int b=0;//宣告差值
    cout << "Complete this quadratic function and draw a diagram." <<endl;
    cout << "f(x)=ax+b.(a=/=0)" <<endl;
    cout << "a: ";//提示輸入a
    cin >> a;//輸入a

    while (a>10 || a<-10 || a==0)//判斷a是否符合
    {
        cout << "illegal input!"<<endl;
        cout << "a: ";//提示輸入a
        cin >> a;//輸入a
    }
    cout << "b: ";//提示輸入b
    cin >> b;//輸入b
    while (b>10 || b<-10)//判斷b是否符合
    {
        cout << "illegal input!"<<endl;
        cout << "b: ";//提示輸入b
        cin >> b;//輸入b
    }
    if (b>=0)
        cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;//輸出題目要求提示與輸入
    else
        cout << "f(x)=" << a << "x" << b << ".(a=/=0)" << endl;//輸出題目要求提示與輸入

    while (y>=yMax)//迴圈行
    {
        while (x<=xMax && y!=0)//不在y=0時
        {
            if (y==a*x+b)//判斷是否符合方程式
                cout<< "*";
            else if (x==0)//x=0時
                cout << "|";
            else//x!=0時
                cout << " ";
            x++;
        }
        while (x<=xMax && y==0)//在y=0時
        {
            if (y==a*x+b)//判斷是否符合方程式
                cout << "*";
            else if (x==0)//x=0時
                cout << "+";
            else//x!=0時
                cout << "-";
            x++;
        }
        cout<<endl;
        x=-10;
        y--;
    }
    return 0;
}
