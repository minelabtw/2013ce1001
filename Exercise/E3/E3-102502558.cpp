﻿#include <iostream>

using namespace std;

int main()
{
    const int size = 10; // the size of the graph

    int a,b; // a and b to decide a line
    cout << "Complete this linear function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;

    cout << "a: ";cin >> a; // prompt a: and enter a
    cout << "b: ";cin >> b; // prompt b: and enter b

    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl; // show the equation
    for (int y=size;y>=-size;y--)
    {
        for (int x=-size;x<=size;x++)
        {
            if (y == a * x + b) // if x,y is on the line shows *
                cout << '*';
            else if (y == 0 && x == 0) // if x,y = 0,0 shows +
                cout << '+';
            else if (y == 0) // if y == 0 shows -
                cout << '-';
            else if (x == 0) // if x == 0 shows |
                cout << '|';
            else
                cout << ' '; // otherwise prints a space
        }
        cout << endl; // next line
    }
    return 0;
}
