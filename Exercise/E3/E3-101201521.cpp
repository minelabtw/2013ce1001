#include <iostream>
using namespace std;

int main()
{
        //Give the variables initial value.
        //a is input1, b is input2, input is to take the value of a or b,
        //x is the x-axis and the lower bound is -10, y is the y-axis and the upper bound is 10,
        //xp is the x coordinate of thepoint on the line of the given function
    int a=0, b=0, input=0, check=0, x=-10, y=10, xp=0;

    cout << "Complete this linear function and draw a diagram.\n";
    cout << "f(x)=ax+b.(a=/=0)\n";

    while(check<2)
    {
        if(check==0)    //Make sure that the variable input is a or b.
        {
            cout << "a: ";
            cin >> a;   //Input a.
            input = a;
        }
        else
        {
            cout << "b: ";
            cin >> b;   // Input b.
            input = b;
        }
            // Use the variable input replace of a and b to check the ranges of a and b.
            // If a is in the range, change the value of check that it goes to handle b.
            // If a is not in the range, give a warning and ask a new input of a again.
        if (input <=10 && input >= -10 && input != 0)   // The range of a and part of the range of b.
            check++;
        else if (check == 1 && input == 0)  // The else of the range of b; b=0 is also in the range.
            check++;
        else
            cout << "illegal input!\n";

    }
    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)\n";
    while (y>=-10)  //The lower bound of y.
    {
        if ( (y-b)%a == 0) // To make sure xp is an integer, so that we can plot the point.
            xp=(y-b)/a;
        else
            xp=11;  // If xp is not a integer, give it a value that it will out of the range of x.
        if (y!=0) // The situation except y==0.
        {
            while (x<=10)   // The upper bound of x.
            {
                if (x==xp)  // When x is equal xp print *.
                    cout << "*";
                else if (x==0)  //Plot the y-axis.
                    cout << "|";
                else    //Plot blank spaces.
                    cout << " ";
                x++;
            }
        }
        else //y==0.
        {
            while (x<=10)   // The upper bound of x.
            {
                if (x==xp)  // When x is equal xp print *.
                    cout << "*";
                else if (x==0)
                    cout << "+";    //Plot the origin.
                else
                    cout << "-";    //Plot the x-axis.
                x++;    // Chage the x from -10 to 10
            }
        }
        x=-10; // Let x return to the initial value.
        cout << endl;   //Chage the line so that the plot will not stick together.
        y--;    // Change the y from 10 to -10.
    }
    return 0;
}
