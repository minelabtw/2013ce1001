#include <iostream>
using namespace std;

int main()
{
    int a;  //宣告名稱為a的整數變數
    int b;  //宣告名稱為b的整數變數

    cout <<"Complete this linear function and draw a diagram." <<endl <<"f(x)=ax+b.(a=/=0)"<<endl << "a:"; //輸出字串
    cin >>a;  //將輸入的值給a

    while(a > 10 or a < -10 or a == 0) //使用while迴圈，若a數值小於-10或大於10或等於0就會一直重複該程式碼內的動作
    {
        cout <<"illegal input!" <<endl <<"a:";  //將字串"illegal input!"輸出並換行輸出字串"a:"
        cin >> a;
    }

    cout <<"b:";
    cin >>b;

    while(b>10 or b<-10)  //使用while迴圈，若b數值小於-10或大於10就會一直重複該程式碼內的動作
    {
        cout <<"illegal input!" <<endl <<"b:";
        cin >> b;
    }

    cout <<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;  //輸出方程式

    int xMax=10; //宣告名稱為xMax的整數變數,將其值設為10
    int xMin=-10; //宣告名稱為xMin的整數變數,將其值設為-10
    int yMax=10; //宣告名稱為yMax的整數變數,將其值設為10
    int yMin=-10;  //宣告名稱為yMin的整數變數,將其值設為-10
    int x=-10;    //宣告名稱為x的整數變數,將其值設為-10
    int y=10;  //宣告名稱為y的整數變數,將其值設為10

    while(y >= yMin)//使用while迴圈，若y大於等於yMax就會一直重複該程式碼內的動作
    {


        while(x <= xMax)  //使用while迴圈，若x小於等於xMax就會一直重複該程式碼內的動作
        {
            if(x==(y-b)/a)
                cout << "*";  //點通過輸出"*"
            else if(x == 0 && y == 0)
                cout <<"+";  //若點沒通過原點，原點則輸出"+"
            else if(y == 0)
                cout <<"-";  //X軸
            else if(x == 0)
                cout <<"|";  //Y軸
            else
                cout <<" ";
            x++;  //讓x之值遞增

        }
        y--;  //讓y之值遞減
        cout <<endl;
        x=-10;  //重新設定x為-10
    }
    return 0;
}
