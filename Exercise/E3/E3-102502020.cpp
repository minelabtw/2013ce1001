#include <iostream>

using namespace std;

int main()
{
    int a=0;      //宣告名稱為a的整數，並初始化其數值為0。
    int b=0;      //宣告名稱為b的整數，並初始化其數值為0。
    int x=-10;    //宣告名稱為x的整數，並初始化其數值為-10。
    int y=10;     //宣告名稱為y的整數，並初始化其數值為10。

    cout << "Complete this quadratic function and draw a diagram." << endl;    //將"Complete this quadratic function and draw a diagram."輸出到螢幕上，並換行。
    cout << "f(x)=ax+b.(a=/=0)" << endl;                                       //將"f(x)=ax+b.(a=/=0)"輸出到螢幕上，並換行。
    cout << "a:";                                                              //將"a:"輸出到螢幕上。
    cin >> a;                                                                  //輸入整數a
    while (a<-10 or a>10 or a==0)              //當a<-10或a>10或a=0時，執行以下動作
    {
        cout << "illegal input!" << endl;      //將"illegal input!"輸出到螢幕上，並換行。
        cout << "a:";
        cin >> a;
    }
    cout << "b:";                              //將"b:"輸出到螢幕上。
    cin >> b;                                  //輸入整數b

    while (b<-10 or b>10)                      //當b<-10或b>10時，執行以下動作
    {
        cout << "illegal input!" << endl;
        cout << "b:";
        cin >> b;
    }

    if (b>=0)                                                        //如果b大於等於0時，執行以下動作
        cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;     //將f(x)=ax+b.(a=/=0)輸出到螢幕上，並換行。
    else                                                             //除非b小於0時，執行以下動作
        cout << "f(x)=" << a << "x" << b << ".(a=/=0)" << endl;      //將f(x)=axb.(a=/=0)輸出到螢幕上，並換行。

    while (y>=-10)                     //當y大於等於-10時，執行以下動作。
    {
        while (x<=10)                  //當x小於等於10時，執行以下動作。
        {
            if (y==a*x+b)              //如果y=a*x+b時，執行以下動作。
                cout<< "*";            //輸出*到螢幕上
            else if (x==0&&y==0)       //除非在y=/=a*x+b的情形下，如果x=0而且y=0時，執行以下動作。
                cout<< "+";            //輸出+到螢幕上
            else if (x==0)             //除非在y=/=a*x+b且y=/=0的情形下，如果x=0時，執行以下動作。
                cout<< "|";            //輸出|到螢幕上
            else if (y==0)             //除非在y=/=a*x+b且x=/=0的情形下，如果y=0時，執行以下動作。
                cout<< "-";            //輸出-到螢幕上
            else                       //除非在y=/=a*x+b且x=/=0且y=/=0的情形下時，執行以下動作。
                cout<< " ";            //輸出 到螢幕上
            x++;                       //x從起始值開始逐1往上遞增。
        }
        cout << endl;                  //當x>10時換行
        x=-10;                         //換行後x的起始值為-10
        y--;                           //y從起始值開始逐1往下遞減。
    }


    return 0;
}
