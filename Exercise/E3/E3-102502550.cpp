#include <iostream>
#include <cstring>
using namespace std;

int main()
{
   char matrix[22][22];                    //宣告陣列 
   int a=0,b=0;
   memset(matrix,0,sizeof(matrix));        //初始化陣列 
   
   cout<<"a:";
   cin>>a;
   while(a<-10 || a>10)                    //輸入a直到在範圍內 
   {
            
      cout<<"illegal input!\n"<<"a:";
      cin>>a;
   }
   cout<<"b:";
   cin>>b;
   while(b<-10 || b>10)                    //輸入b直到在範圍內 
   {
      cout<<"illegal input!\n"<<"b:";
      cin>>b;
   }
      
   for(int i=1;i<=21;i++)                  //在陣列上填入對應符號 
   {
      for(int j=1;j<=21;j++)
      {      
          
          if(j==11 && matrix[j][i]!='*')
          {
              matrix[j][i]='|';
          }
          if(i==11 && matrix[j][i]!='*')
          {
              matrix[j][i]='-';
          }
          if(i==11 && j==11)
          {
              matrix[j][i]='+';     
          }   
          if((11-i)==((j-11)*a+b))
          {
              matrix[j][i]='*';
          }
          
            
          
      }
   }
   for(int i=1;i<=21;i++)                   //印出陣列 
   {
      for(int j=1;j<=21;j++)
      {

          if(matrix[j][i]!='0')
            cout<<matrix[j][i];

      }
      cout<<endl;
   }
system("pause");

}
