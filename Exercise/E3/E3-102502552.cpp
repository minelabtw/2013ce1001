#include <iostream>
using namespace std;
int main()
{
    float xMax =10;
    float yMax =10;
    float xMin =-10;
    float yMin =-10;
    float y =10;
    float x =-10;
    float a;
    float b;         //宣告變數,定出x與y的範圍以及方程的變量

    cout << "Complete this linear function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;//提示使用者目的
    do{
        cout << "a=";
        cin >> a;//輸入變數a
        {if ( a > 10 || a == 0 || a < -10)//程式將檢查輸入的值是否符合要求
            cout << "illegal input!" << endl;}//若否,提示有誤並要求重新輸入
    }while( a > 10 || a == 0 || a < -10);

     do{
        cout << "b=";
        cin >> b;//輸入變數b
        {if ( b > 10 || b < -10)//程式將檢查輸入的值是否符合要求
            cout << "illegal input!" << endl;}//若否,提示有誤並要求重新輸入
    }while( b > 10 || b < -10);

    cout << "f(x)=" << a << "x+" << b << "(a=/=0)"<< endl;//顯示使用者輸入後的方程式


    while ( y >= yMin )//進入判斷y的迴圈
    {
        while ( x <= xMax )//進入判斷x的迴圈
        {
           if ( y == a * x + b )//判斷點是否在函數上
                cout << "*";//在符合方程的點上印上*
           else if ( x == 0 && y == 0)//判斷點是否在原點
            cout << "+";//在原點印上+
            else if ( y == 0)//判斷點是否在x軸
                cout << "-";//在x軸印上-
            else if ( x == 0)//判斷點是否在y軸
                cout << "|";//在y軸印上|
                else cout << " ";//其餘的點將用空格補滿以維持圖形
            x++;//x橫向走

        }
        x = xMin;//每當x回到最小值
        y--;//y從上往下走
        cout << endl;//到x極值時換行以維持圖形
    }

    return 0;//回歸到0
}//結束主程式
