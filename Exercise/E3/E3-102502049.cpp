#include <iostream>
using namespace std;

int main()
{
    int xmax=10;  //宣告變數型別為整數的xmax並初始化其為10作為迴圈的限制
    int ymin=-10;  //宣告變數型別為整數的ymin並初始化其為-10作為迴圈的限制
    int a=0;
    int b=0;  //宣告兩個變數型別為整數a.b作為係數
    int x=-10;
    int y=10;  //宣告兩個變數型態為整數的x.y分別初始化為-10.10代表座標

    cout << "Complete this quadratic function and draw a diagram." << endl << "f(x)=ax+b.(a=/=0)" << endl << "a:"   ;
    cin >> a ; //顯示文字並提示出入a值

    while (a < -10 || a > 10 || a == 0 )
    {
        cout << "illegal input!" << endl <<  "a:";
        cin >> a;
    }
    cout << "b:" ;
    cin >> b ;
    while (b < -10 || b > 10)
    {
        cout << "illegal input!" << endl <<  "b:";
        cin >> b;
    } //限制a.b係數的的範圍為-10~10且a不等於0，若不符則顯示要求重新輸入

    if ( b>=0 )
    {
        cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" << endl;
    }
    else
    {
        cout << "f(x)=" << a << "x" << b << ".(a=/=0)" << endl;
    } //為了使其不出現+-b的狀況

    while(y>=ymin) //由上而下做迴圈使其從y=10到y=-10
    {
        while(x<=xmax) //由左而右做迴圈使其從x=-10到x=10
        {
            if ( y == a*x+b )
            {
                cout << "*";
            }
            else if ( x == 0 && y == 0 )
            {
                cout << "+";
            }
            else if ( x == 0 )
            {
                cout << "|";
            }
            else if ( y == 0 )
            {
                cout << "-";
            }
            else
            {
                cout << " ";
            } //顯示圖形包含X軸.Y軸及線

            x++; //使x每次+1滿足迴圈
        }
        cout<<endl;
        x=-10; //當每執行完x迴圈後重新初始化x為0
        y--;  //使y每次-1使其滿足迴圈
    }
    return 0;
}
