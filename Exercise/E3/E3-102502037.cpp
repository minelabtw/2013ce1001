#include <iostream>

using namespace std;

int main()
{
    int a,b,x,y;
    int Q,K;
    cout<<"Complete this quadratic function and draw a diagram.";
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;
    cout<<"a:";
    cin>>a;
    while (a < -10 || a > 10 || a==0 )
    {
        cout << "illegal input!" << endl <<  "a:";
        cin >> a;
    }
    cout << "b:" ;
    cin >> b ;
    while (b < -10 || b > 10)
    {
        cout << "illegal input!" << endl <<  "b:";
        cin >> b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<"(a=/=0)"<<endl;
 for(y=10 ; y>=-10 ; y--)
    {
        for(x=-10 ; x<=10 ; x++)
        {   Q=a*x+b;
            if(y==Q)
                cout<<"*";
            else if(x==0 && y==0)
                cout<<"+";
            else if(x==0)
                cout<<"|";
            else if(y==0)
                cout<<"-";
            else
                cout<<" ";
        }
        cout<<endl;
    }

    return 0;
}
