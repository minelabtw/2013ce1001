#include<iostream>
using namespace std;

int main()
{
    int xMAX=10;    //宣告一整數xMAX，起始值為10
    int yMIN=-10;   //宣告一整數yMIN，起始值為-10
    int x=-10;      //宣告一整數x，起始值為-10
    int y=10;       //宣告一整數y，起始值為10
    int a=0;        //宣告一整數a，起始值為-10
    int b=0;        //宣告一整數b，起始值為-10

    cout << "a=";           //印出"a="
    cin >> a;               //鍵盤輸入整數a
    while (a>10||a<-10||a==0)   //當a>10或a<-10或a==0時進入while迴圈
    {
        cout << "illegal input!" <<endl;    //印出"illegal input!"
        cout << "a=";                       //印出"a="
        cin >> a;                           //再次輸入a
    }

    cout << "b=";
    cin >> b;
    while (b>10||b<-10||b==0)
    {
        cout << "illegal input!" <<endl;
        cout << "b=";
        cin >> b;
    }
    cout << endl;
    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)"<<endl;
    cout << "a=" << a <<endl;
    cout << "b=" << b <<endl;
    cout << "f(x)=" << a << "x+" << b << ".(a=/=0)" <<endl;


    while (y>=yMIN)         //當y>=yMAX進入while迴圈
    {
        while (x<=xMAX)     //當x<=xMIN進入while迴圈
        {
            if (y==a*x+b)           //當y==a*x+b時印出"*"
            cout << "*";
            else if (y==0&&x==0)    //當y==0且x==0時印出"+"
            cout << "+";
            else if (y==0)          //當y==0時印出"-"
            cout << "-";
            else if (x==0)          //當x==0時印出"|"
            cout << "|";
            else
            cout << " ";            //其他印出" "

            x++;

        }
            cout <<endl;
            x=-10;                  //使x恢復為-10
            y--;                    //讓y-1


    }
return 0;

}
