#include<iostream>
using namespace std ;

int main()
{
    int a=0 ; //宣告變數
    int b=0 ; //宣告變數
    int x=-10 ; //宣告變數
    int y=10 ; //宣告變數

    cout << "Complete this linear function and draw a diagram." << endl ; //完成函數圖形並畫圖
    cout << "f(x)=ax+b.(a=/=0)" << endl ; // 輸出 f(x)=ax+b.(a=/=0) 並換行
    cout << "a:" ; // 輸出 a:
    cin >> a ; // 輸入 a


    while (a>10 or a<-10 or a==0) //當 a>10 or a<-10 or a==0 執行下列東西
    {
        cout << "illegal input!" << endl << "a:" ; // 輸出 illegal input! 並換行再輸出 a:
        cin >> a ; // 輸入 a
    }

    cout << "b:" ; // 輸出 b:
    cin >> b ; // 輸入 b
    while (b>10 or b<-10) //當 b>10 or b<-10 執行下列東西
    {
        cout << "illegal input!" << endl << "b:" ; //輸出 illegal input! 並換行再輸出 b:
        cin >> b ; // 輸入 b
    }
    if(b>=0) // 判斷當b>=0 執行下列東西
    {
        cout << "f(x)=" << a << "x" << "+" << b << endl ;
    }
    else if (b<0)  // 判斷當b<0 執行下列東西
    {
        cout << "f(x)=" << a << "x" << b << endl ;
    }



    while (y>=-10) // 判斷當 y>=-10 執行下列迴圈
    {
    x=-10; // 重置x值
        while (x<=10) // 當x<=10 執行下列迴圈
        {
            if (y==a*x+b) //判斷當 y=a*x+b 時
                cout << "*" ; //輸出 *
            else if (x==0 and y==0) // 判斷當 x=0 and y=0 時
                cout << "+" ; // 輸出 +
            else if (y==0) // 判斷當 y=0 時
                cout << "-" ; // 輸出 -
            else if (x==0) // 判斷當 x=0 時
                cout << "|" ; // 輸出 |
            else // 判斷當條件不是以上那些時
                cout << " " ; // 輸出
            x++ ; // 使迴圈能結束
        }
        cout << endl ; // 換行
        y-- ; // 使迴圈結束

    }


    return 0 ;


}
