#include <iostream>
using namespace std;
int main()
{
    ios::sync_with_stdio(0);                                                //取消同步，加快輸入輸出效率
    int a;                                                                  //宣告一個變數a代表x的係數
    int b;                                                                  //宣告一個變數b代表方程式常數項
    cout<<"Complete this linear function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;
    cout<<"a: ";
    cin>>a;                                                                 //輸入一個整數存到a中
    while(a>10||a<-10||a==0)                                                //如果a不符合範圍則進入迴圈
    {
        cout<<"illegal input!"<<endl;
        cout<<"a: ";
        cin>>a;
    }
    cout << "b: " ;
    cin>>b;                                                                 //輸入一個整數存到b中
    while(b>10||b<-10)                                                      //如果b不符合範圍則進入迴圈
    {
        cout<<"illegal input!"<<endl;
        cout<<"b: ";
        cin>>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;
    for(int i=10;i>=-10;--i){                                               //i代表y座標 從10跑到-10
        for(int j=-10;j<=10;++j)                                            //j代表x座標 從-10跑到10
            if(a*j+b==i)cout<<'*';                                          //若(j,i)符合方程式輸出*
            else if(i==0&&j==0)cout<<'+';                                   //若(0,0)輸出+
            else if(i==0)cout<<'-';                                         //若i為0輸出-
            else if(j==0)cout<<'|';                                         //若j為0輸出|
            else cout<<' ';
        cout<<endl;                                                         //每列輸出完後要換行
    }
    return 0;
}
