/*方程式繪圖
請寫一個程式用於繪畫方程式圖形
方程式樣板:f(x)=ax+b.*/

#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int xmax=10;            //宣告整數
    int ymin=-10;
    int x=-10;
    int y=10;
    int a;
    int b;

    cout << "Complete this linear function and draw a diagram." << endl;    //將字串印出螢幕上
    cout <<"f(x)=ax+b.(a=/=0)" << endl;

    do
    {
        cout << "a:" ;
        cin >> a ;
        if(a==0 || a>10 || a<-10)                   //如果在(a==0 || a>10 || a<-10)條件
        {
            cout << "illegal input!" << endl;       //就印出字串
        }
    }
    while (a==0 || a>10 || a<-10);                  // 當在條件內就迴圈做do

    do
    {
        cout << "b:" ;
        cin >> b;
        if(b>10 || b<-10)
        {
            cout << "illegal input!" << endl;
        }
    }
    while ( b>10 || b<-10);
    cout << "f(x)=" << a << "x + (" << b << "). (a=/=0)" << endl;

    while(y>=ymin)                  //while迴圈, 當y在10至-10之間就迴圈
        {
            while(x<=xmax)          //當x在10~(-10)之間就迴圈
            {
                if(y==a*x+b)            //我們當是一個表格, 先判斷第一格, 如果在條件
                {
                    cout << "*";        //印出 *, 不然就判斷
                }
                else if (x==0 && y==0)  //x&y有沒有 = 0
                {
                    cout << "+";        //如果=0, 就印出 + , 不然就判斷
                }
                else if (x==0)          //x是不是=0, 如果是
                {
                    cout << "|";        // 就印出 |; 不實的話就繼續判斷
                }
                else if (y==0)          //如果y=0; 就
                {
                    cout << "-";        //印出 -
                }
                else                    //都沒有在上面的條件, 就
                {
                    cout << " ";        //印出空白
                }
                x++;                    //x+1, 所以這裡 x=1

            }
            cout << endl;
            x=-10;                      //這裡的x會從第一格開始
            y--;                        //y會跳下一行然後繼續迴圈
        }
        return 0;

}
