#include <iostream>
using namespace std;
const float MAX_SIZE = 10.0;//宣告型別為const float的範圍，並初始化其數值為10.0
bool isOutOfRange(float number);//檢查number是否帶小數
bool isFloat(float number);//檢查number是否帶小數

int main(){
    float a = 0.0;//宣告型別為float的a，並初始化其數值為0.0
    float b = 0.0;//宣告型別為float的b，並初始化其數值為0.0
    int sourceType = 0;//宣告型別為int的輸入來源類別，並初始化其數值為0.0
    float* sourcePtr = 0;//宣告型別為指向float的來源指標，並初始化其數值為0(null)

    cout << "Complete this quadratic function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;

    while(sourceType < 2){
        if(sourceType == 0){//sourceType為a
            cout << "a: ";
            sourcePtr = &a;
        }
        else if(sourceType == 1){//sourceType為b
            cout << "b: ";
            sourcePtr = &b;
        }
        else{
            //不做任何事
        }

        cin >> (*sourcePtr);
        if(isOutOfRange(*sourcePtr) || isFloat(*sourcePtr) || a == 0){
            //sourcePtr超出範圍 或是 不是整數 或是 a=0
            //則重新輸入
            cout << "illegal input!" << endl;
        }
        else{//輸入下一個值
            sourceType++;
        }
    }

    cout << "f(x)=" << a <<"x+" << b <<".(a=/=0)" << endl;

    for(float y = MAX_SIZE; y >= -MAX_SIZE; y--){//沿著y = MAX_SIZE畫到y = -(MAX_SIZE)
        float x = (int)((y - b)/a);//計算出x值
        for(int blank = -MAX_SIZE; blank <= MAX_SIZE; blank++){//沿著x = -MAX_SIZE畫到x = MAX_SIZE
            if(blank == (int)x){//如果blank等於x值則輸出*
                cout << "*";
            }
            else if(blank == 0){//如果blank等於=值則輸出+或|
                if(y == 0){     //(blank, y)=(0,0)輸出+
                    cout << "+";
                }
                else{           //blank = 0 但 y != 0 輸出|
                    cout << "|";
                }
            }
            else{               //輸出-或空白
                if(y == 0){     //y=0 輸出-
                    cout << "-";
                }
                else{           //y=0 輸出空白
                    cout << " ";
                }
            }
        }
        cout << endl;
    }
    return 0;
}

bool isOutOfRange(float number){//檢查number是否超過正負MAX_SIZE
    if(number > MAX_SIZE || number < -MAX_SIZE)
        return true;
    else
        return false;
}

bool isFloat(float number){//檢查number是否帶小數
    float intNumber = (int)number;//intNumber放入number整數部分
    if(intNumber != number)//如果不相等則必帶小數點
        return true;
    else
        return false;
}
