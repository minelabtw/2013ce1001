#include <iostream>
using namespace std;

int main()
{

    int x=-10; //起始值設為-10,依序由左往右輸出
    int y=10; //起始值設為10,依序由上往下輸出
    int a,b;

    cout <<"Complete this quadratic function and draw a diagram.\nf(x)=ax+b.(a=/=0)\na:"; //印出題目並請求輸入a值
    cin >>a;

    while (a>10||a<-10||a==0) //a必須為>10&<-10&0以外的值,否則跳回重跑
    {
        cout <<"Illegal input!\na:";
        cin >>a;
    }

    cout <<"b="; //請求輸入b值
    cin >>b;
    while (b>10||b<-10) //b必須為>10&<-10以外的值,否則跳回重跑
    {
        cout <<"Illegal input!\nb:";
        cin >>b;
    }

    cout <<"f(x)"<<"="<<a<<"x"<<"+"<<b<<"."<<"(a=/=0)" << endl; //印出剛剛輸入的方程式

    while (y>=-10) //因為y的起始值設為10,依序由上往下印出,所以這邊的迴圈範圍設定為>=-10
    {
        while (x<=10) //因為x的起始值設為-10,依序由左往右印出,所以這邊的迴圈範圍設定為<=10
        {
            if(y==a*x+b)
                cout <<"*"; //將方程式的整數解用*號表示
            else if(y==0&&x==0)
                cout <<"+"; //將原點印出+
            else if(x==0)
                cout <<"|"; //Y軸
            else if(y==0)
                cout <<"-"; //X軸
            else
                cout <<" "; //空白
            x++;
        }
        cout <<endl;
        x=-10; //回到x的起始值,y由10~-10繼續跑迴圈
        y--; //y由上至下依序印出
    }
    return 0;
}
