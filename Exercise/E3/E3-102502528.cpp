#include<iostream>

using namespace std;

int main()
{
    int a,b;
    int x = -10;
    int y = 10;
    int i = 0;

    cout << "Complete this linear function and draw a diagram." << endl;
    cout << "f(x)=ax+b.(a=/=0)" << endl;

    cout << "a:";
    cin >> a ;

    while (a < -10 || a > 10 || a == 0)                                 //用來判定a是否符合範圍
    {
        cout << "illegal input!" << endl <<  "a:";
        cin >> a;
    }
    cout << "b:" ;
    cin >> b ;
    while (b < -10 || b > 10 || b == 0)                                 //用來判定a是否符合範圍
    {
        cout << "illegal input!" << endl <<  "b:";
        cin >> b;
    }
    while(y>=-10)                                                       //用來表示縱向的位置
    {
        while(x<=10)                                                    //用來表示橫向的位置
        {
            if(y==a*x+b)
            {
                cout << "*";
                x++;
            }
            else if(x==0 && y==0)                                       //標示原點
            {
                cout <<"+";
                x++;
            }
            else if(x==0 )                                              //標示Y軸
            {
                cout << "|";
                x++;
            }
            else if(y==0 )                                              //標示X軸
            {
                cout << "-";
                x++;
            }

            else
            {
                cout << " ";
                x++;
            }
        }
        cout << endl;
        x = -10;
        y --;                                                           //移向下一列
    }
    return 0;
}
