#include <iostream>

using namespace std;

int main()
{
    int a,b;
    cout<<"Complete this quadratic function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;
    cout<<"a: ";
    cin>>a;
    cout<<"b: ";
    cin>>b;
    while(a<-10 || a>10 || a==0 || b<-10 || b>10)
    {
        cout<<"illegal input!"<<endl;
        cout<<"a: ";
        cin>>a;
        cout<<"b: ";
        cin>>b;
    }
    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;
    for(int i=-10;i<=10;i++)
    {
        for(int j=10;j>=-10;j--)
        {
            if(j==i*a+b)
                cout<<'*';
            else if(j==0 && i==0)
                cout<<'+';
            else if(j==0)
                cout<<'|';
            else if(i==0)
                cout<<'-';
            else
                cout<<' ';
        }
        cout<<endl;
    }
    return 0;
}
