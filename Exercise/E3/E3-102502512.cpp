#include<iostream>

using namespace std;
int main()
{
    int a=0,b=0;                                                                 //define varibles a and b
    int x=-10,y=10;                                                              //make varible x be -10 and be 10
                                                                                 //because the value of x-axis is staring at
                                                                                 //-10 from right to left, y-axis staring at 10 from top to bottom

    cout<<"Complete this quadratic function and draw a diagram."<<endl;
    cout<<"f(x)=ax+b.(a=/=0)"<<endl;                                             //Telling user to complete the function

    do
    {                                                                            //Line 14~29:To make sure number doesn't out of range
        cout<<"a= ";
        cin>>a;
        if(a>10 || a<-10||a==0)
            cout<<"The number is illegal!"<<endl;
    }
    while(a>10 || a<-10||a==0);
    do
    {
        cout<<"b= ";
        cin>>b;
        if(b>10 || b<-10)
            cout<<"The number is illegal!"<<endl;
    }
    while(b>10 || b<-10);

    cout<<"f(x)="<<a<<"x+"<<b<<".(a=/=0)"<<endl;

    while(x<=10)                                                  //Line 33~62:Printing the graph of function
    {
        while(y>=-10)
        {
            if(y==a*x+b)
            {
                cout<<"*";
            }
            else if(y==0&&x==0)
            {
                cout<<"+";
            }
            else if(x==0)
            {
                cout<<"-";
            }
            else if(y==0)
            {
                cout<<"|";
            }
            else
            {
                cout<<" ";
            }
            y--;
        }
        cout<<endl;
        y=10;
        x++;
    }
    return 0;
}
