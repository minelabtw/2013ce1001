#include<iostream>

using namespace std;

double newFibonacci(double number)
{
    if (number == 1)
        return 1;
    else if (number == 2)
        return 2;
     else if (number == 3)
        return 3;
    else
    {
        return newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);   //輸出的值等於前面3項相加之和
    }
}

int main ()
{
    double i;
    for (i = 1; i < 11; i++)
    {
        cout << "newfabonacci(" << i << ") = " << newFibonacci(i) << endl;           //輸出前十項
    }
    cout << "newfabonacci(" << 20 << ") = " << newFibonacci(20) << endl;             //輸出第二十項
    cout << "newfabonacci(" << 30 << ") = " << newFibonacci(30) << endl;             //輸出第三十項
    cout << "newfabonacci(" << 40 << ") = " << newFibonacci(40);                     //輸出第四十項
    return 0;
}
