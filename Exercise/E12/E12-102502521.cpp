#include <iostream>

using namespace std;

double newFibonacci(double number);    //宣告函數

int main()
{
    double number=1;    //宣告變數

    for(int a=40;number<=a;number++)    //列印
    {
        if(number>0&&number<=10||number==10||number==20||number==30||number==40)
        {
            cout<<"newfibonacci("<<number<<") = "<<newFibonacci(number)<<endl;
        }
    }

    return 0;
}

double newFibonacci(double number)    //使用遞迴囉
{
    if(number==1)
    {
        return 1;
    }

    else if(number==2)
    {
        return 2;
    }

    else if(number==3)
    {
        return 3;
    }

    else
    {
        return newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);
    }

}
