#include<iostream>
using namespace std;

int main()
{
    double newfibonacci(double number);
    for(double i=1; i<11; i++)
    {
        cout << "newfibonacci(" << i << ") = " << newfibonacci(i) << endl;  //輸出每個數列的值
    }
    return 0;
}

double newfibonacci(double number)
{

    if((number == 1)||(number == 2)||(number == 3))     //前三項慧回傳自己
    {
        return number;
    }
    else
    {
        return newfibonacci(number-1) + newfibonacci(number-2) + newfibonacci(number-3);    //費氏數列
    }
}
