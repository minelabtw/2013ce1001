#include <iostream>
using namespace std;

double newFibonacci(double number);
int a[30];              //宣告陣列

int main()
{
    a[0]=1;              //定前陣列三項
    a[1]=2;
    a[2]=3;

    newFibonacci(a[0]);
    for(int i=0; i<30; i++)
        if(i<10||i==19||i==29)                //輸出前十項及第20和30項
            cout << "newfibonacci(" << i+1 << ") = " << a[i] << endl;

    return 0;
}

double newFibonacci(double number)
{
    for(int i=0; i<30-3; i++)                  //用遞迴計算費波那契數列
    {
        a[i+3]=a[i]+a[i+1]+a[i+2];
        number=a[i+3];
    }
    return number;
}
