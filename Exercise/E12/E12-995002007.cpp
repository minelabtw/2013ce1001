#include<iostream>
using namespace std;

double newFibonacci(double number)
{
    if(number==1)  //第一項為1
        return 1;
    else if(number==2)  //第一項為2
        return 2;
    else if(number==3)  //第一項為3
        return 3;
    else                //遞迴呼叫涵式 第N項為(n-3)+ (n-2)+(n-1)
        return(newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1));
}

int main()
{

    for ( int i=0;i<10;i++)
    {
        cout << "newfibonacci(" << i+1 << ") = " << newFibonacci(i+1) << endl; //把1~10的Fibonacci列印出來
    }

    return 0;
}
