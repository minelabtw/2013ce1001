#include <iostream>

using namespace std;

double newFibonacci (double);
void  output (double);

int main()
{
    output (1);
    output (2);
    output (3);
    output (4);
    output (5);
    output (6);
    output (7);
    output (8);
    output (9);
    output (10);
    output (20);
    output (30);
    output (40);

    return 0;
}

double newFibonacci (double number)
{
    if (number<=3)
        return number;
    else
        return newFibonacci (number-1) + newFibonacci (number-2) + newFibonacci (number-3);
}

void  output (double number)
{
    cout << "newfibonacci(" << number << ") = " << newFibonacci (number) << endl;
}
