#include <iostream>

using namespace std;


double newFibonacci(double number)
{
   if (number==1)
    return 1 ;
   else if (number==2)
    return 2 ;
   else if (number==0)
    return 0 ;
   else
    return newFibonacci(number-3) + newFibonacci(number-2) +  newFibonacci (number -1);
}

int main()
{

    for (int i=1; i<11 ; i++)
    {
        cout << "newfibonacci(" << i << ") = " <<newFibonacci (i) <<endl;
    }

    for (int i=1; i<5; i++)
    {
        cout << "newfibonacci(" << i*10 << ") = " <<newFibonacci (i*10) <<endl;
    }

    return 0;
}
