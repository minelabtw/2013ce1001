#include <iostream>
using namespace std;

double f(double); //宣告函式

int main()
{
    for (int a=1; a<=10; a++)
    {
        cout << "newfibonacci(" << a << ") = " << f(a) << endl; //輸出
    }
    return 0;
}
double f(double n) //遞回函式
{
    if(n<=3)
        return n;
    else
        return (f(n-1)+f(n-2)+f(n-3));
}
