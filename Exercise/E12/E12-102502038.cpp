//============================================================================
// Name        : E12-102502038.cpp
// Author      : catLee
// Version     : 0.2
// Description : NCU CE1001 E12
//============================================================================
#include <iostream>

using namespace std;

double newFibonacci(double);
void fibInit(void);
double arr[40];
int main(void){
  fibInit();
  newFibonacci(1);
  return 0;
}
double newFibonacci(double number){
  if(number <= 40){
    if((number <= 10 && number >= 1) || ((int)number)%10 == 0){
      cout << "newfibonacci(" << number << ") = " << arr[(int)number-1] << endl;
    }
    newFibonacci(number + 1);
  }
  return 0;
}
void fibInit(void){
  for(int i=0;i<40;i++){
    arr[i] = (i<3?i:(arr[i-3]+arr[i-2]+arr[i-1]));
    //遞迴只應天上有，凡人應當用迴圈
  }
}
