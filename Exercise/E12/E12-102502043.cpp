#include <iostream>

using namespace std;

double newFibonacci(double number);
int main()
{
    double number=1;
    for(int i=number;i<=10;i++)
    {
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl;                            //進入迴圈呈現計算後的值
    }
    cout<<"newfibonacci(20) = "<<newFibonacci(20)<<endl;
    cout<<"newfibonacci(30) = "<<newFibonacci(30)<<endl;
    cout<<"newfibonacci(40) = "<<newFibonacci(40)<<endl;
    return 0;
}
double newFibonacci(double number)
{
    if(number==1)                                                                           //分別計算地回中的值
        return 1;
    else if(number==2)
        return 2;
    else if(number==3)
        return 3;
    else
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
}
