#include <iostream>

using namespace std ;
double newFibonacci(double number) ;
int main ()
{
    for (int i = 1 ; i <= 10 ; i ++)                                                   //輸出1~10項
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci (i) << endl ;
    }

    for (int j = 2 ; j <= 4 ; j ++)                                                    //輸出第20 30 40 項
    {
        cout << "newfibonacci(" << j*10 << ") = " << newFibonacci (j*10) << endl ;
    }

    return 0 ;
}

double newFibonacci(double number)                                                     //新費式遞迴數列
{
    if (number <= 3)
        return number ;
    else
        return newFibonacci(number - 1) + newFibonacci (number - 2) + newFibonacci (number - 3) ;
}
