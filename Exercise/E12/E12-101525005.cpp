#include <iostream>

using namespace std;

double newFibonacci(double number);

int main()
{

    cout << "newfibonacci(1) = "<<newFibonacci(1) << endl;
    cout << "newfibonacci(2) = "<<newFibonacci(2) << endl;
    cout << "newfibonacci(3) = "<<newFibonacci(3) << endl;
    cout << "newfibonacci(4) = "<<newFibonacci(4) << endl;
    cout << "newfibonacci(5) = "<<newFibonacci(5) << endl;
    cout << "newfibonacci(6) = "<<newFibonacci(6) << endl;
    cout << "newfibonacci(7) = "<<newFibonacci(7) << endl;
    cout << "newfibonacci(8) = "<<newFibonacci(8) << endl;
    cout << "newfibonacci(9) = "<<newFibonacci(9) << endl;
    cout << "newfibonacci(10) = "<<newFibonacci(10) << endl;
    cout << "newfibonacci(20) = "<<newFibonacci(20) << endl;
    cout << "newfibonacci(30) = "<<newFibonacci(30) << endl;
    cout << "newfibonacci(40) = "<<newFibonacci(40) << endl;

    return 0;
}
double newFibonacci(double number)
{
    //f3=f2+f1=f1+f0+f1=3
    //0 1 2 3 4 5
    //1 1 2 3 5 8

    double fn;

    if(number==1 )
    {
        return 1;
    }
    else if(number==2)
    {
        return 2;
    }
    else if(number==3)
    {
        return 3;
    }
    else
    {
        fn=newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
        return fn;
    }
}
