#include<iostream>
using namespace std;
double numberarray[40]={1,2,3};
/*
定義初始值
[0]=1
[1]=2
[2]=3
其餘為0
0表示未計算
*/
double newFibonacci(int number)
{
    if(number<4)//有初始值
        return numberarray[number-1];
    if(numberarray[number-1-3]==0)
        numberarray[number-1-3]=newFibonacci(number-3);
    if(numberarray[number-1-2]==0)
        numberarray[number-1-2]=newFibonacci(number-2);
    if(numberarray[number-1-1]==0)
        numberarray[number-1-1]=newFibonacci(number-1);
    //陣列的值若未計算，則呼叫函式計算

    return numberarray[number-1-3]+numberarray[number-1-2]+numberarray[number-1-1];
}
int main()
{
    for(int i=1;i<=40;i++)
        if(i<=10 || i%10==0)
            cout << "newfibonacci(" << i << ") = "<<newFibonacci(i)<<endl;
    return 0;
}
