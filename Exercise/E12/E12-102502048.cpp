#include<iostream>
using namespace std;
double newFibonacci(int number)//遞迴實作費氏數列
{
    if(number==0)//判斷number值
        return 1;
    else if(number==1)
        return 2;
    else if(number==2)
        return 3;
    else//遞迴
        return newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);
}
int main()
{
    for(int i=0; i<40; i++)
        if(i<10 || i%10==9)
            cout<<"newfibonacci("<<i+1<<") = "<<newFibonacci(i)<<endl;//顯示數列
    return 0;
}
