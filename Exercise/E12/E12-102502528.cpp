#include<iostream>
using namespace std;

double newFibonacci(double number)
{
    if(number ==1||number ==2||number ==3)         //定義前三項
        return number;
    else
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);       //遞迴
}

int main(void)
{
    for(int i=1; i<=40; i<10? i++ : i+=10)                     //輸出
        cout << "newfibonacci("<<i<<") = "<<newFibonacci(i)<< endl;
    return 0;
}
