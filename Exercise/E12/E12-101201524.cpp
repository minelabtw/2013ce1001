#include<iostream>
using namespace std;

double newFibonacci(double number)//宣告涵式來計算費氏數列
{
    if(number<=3)//若為前三項則輸出該項的數字(1,2,3)
        return number;
    else if(number==4)//另外判斷第四項
        return 6;
    else
        return 2*newFibonacci(number-1)-newFibonacci(number-4);//若在第四項之後,則用公式算出
}

int main()
{
    double i;//宣告變數i用來記錄次數
    for(i=1;i<=10;i++)
    cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl ;//輸出一到十項
    cout << "newfibonacci(20) = " << newFibonacci(20) << endl;//輸出第二十項
    cout << "newfibonacci(30) = " << newFibonacci(30) << endl;//輸出第三十項
    cout << "newfibonacci(40) = " << newFibonacci(40);//輸出第四十項

    return 0;
}
