/*************************************************************************
    > File Name: E12-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年12月18日 (週三) 16時07分07秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#define nF newFibonacci

double nF(int number, double *dp);

int main()
{
	double dp[100];
	for(int i=0 ; i<100 ; i++)
		dp[i] = 0.0;

	for(int i=1 ; i<=10 ; i++)
		printf("newfibonacci(%d) = %d\n", i, nF(i, dp));

	printf("newfibonacci(20) = 20\n", nF(20, dp));
	printf("newfibonacci(30) = 30\n", nF(30, dp));
	printf("newfibonacci(40) = 40\n", nF(40, dp));

	return 0;
}

double nF(int number, double *dp)
{
	if(number <= 3)
		return number;
	if(dp[number] != 0.0)
		return dp[number];

	return dp[number] = nF(number-1, dp) + nF(number-2, dp) + nF(number-3, dp);
}

