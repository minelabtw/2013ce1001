#include <iostream>
using namespace std;

double newFibonacci(double number) // 遞迴法
{
    if (number == 1) // 定義第1, 2, 3項
        return 1;
    if (number == 2)
        return 2;
    if (number == 3)
        return 3;
    else // 召喚此函式進行遞迴
        return (newFibonacci (number - 1) +  newFibonacci(number - 2) +  newFibonacci(number - 3) );
}

int main()
{
    for (int change = 1; change < 11; change++)
    {
        cout << "newfibonacci(" << change << ") = " << newFibonacci(change) << endl;
    }
    for (int i = 20; i < 41; i += 10) // 項數加10!!
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    }

    return 0;
}

