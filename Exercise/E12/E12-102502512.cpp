#include<iostream>
using namespace std;
double newf(double);
int main()
{
    for(int i=0; i<10; i++)                                                 //line 6-9: print the newfibonacci number from one to ten
    {
        cout<<"newfibonacci("<<i+1<<") = "<<newf(i)<<endl;
    }
    return 0;
}
double newf(double a)                                                       //line 12-22:define the function newf by recursive
{
    if(a==0)
        return 1;
    else if(a==1)
        return 2;
    else if(a==2)
        return 3;
    if(a>=3)
        return newf(a-1)+newf(a-2)+newf(a-3);
}
