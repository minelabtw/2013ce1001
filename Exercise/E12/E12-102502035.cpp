#include <iostream>
using namespace std;
double newFibonacci (double number);//宣告函式
int main ()
{
    for (double i =1; i<=10; i++) //迴圈十次
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci (i) << endl;//個別輸出
    }
    cout << "newfibonacci(20) = " << newFibonacci (20) << endl;//輸出第20
    cout << "newfibonacci(30) = " << newFibonacci (30) << endl;//輸出第30
    cout << "newfibonacci(40) = " << newFibonacci (40);//輸出第40
    return 0;
}
double newFibonacci (double number)//函式
{
    if (number >3)//若須由計算求得
    {
        return newFibonacci (number -2) *2 +newFibonacci (number -3) *2 +newFibonacci (number -4);//回傳計算結果
    }
    else if (number ==3)//第三項
    {
        return 3;
    }
    else if (number ==2)//第二項
    {
        return 2;
    }
    else if (number ==1)//第一項
    {
        return 1;
    }
    else if (number ==0)//第零項
    {
        return 0;
    }
}
