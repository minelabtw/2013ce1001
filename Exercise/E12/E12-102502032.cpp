#include <iostream>
using namespace std;

/*
f(1)=1
f(2)=2
f(3)=3
f(n+3)=f(n)+f(n+1)+f(n+2)
*/
double newFibonacci(double number)
{
    if (number <= 0)        //exception:number must be larger than 0
        return 0;
    else if (number <= 3)   //domain
        return number;
    else
        return newFibonacci(number - 1) + newFibonacci(number - 2) + newFibonacci(number - 3);
}

int main()
{
    //output
    for (int i = 1; i <= 10; i ++)
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    cout << "newfibonacci(20) = " << newFibonacci(20) << endl;
    cout << "newfibonacci(30) = " << newFibonacci(30) << endl;
    return 0;
}
