#include <iostream>

using namespace std ;

double newFibonacci( double number ) ;  //宣告函式。

int main ()
{
    for ( int i = 1 ; i < 10 ; i ++ )
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci( i ) << endl ;
    }  //輸出1到9的費氏數列。

    for ( int i = 10 ; i <= 40 ; i = i + 10 )
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci( i ) << endl ;
    }  //輸出10、20、30和40的費氏數列。

    return 0 ;
}

double newFibonacci( double number )
{
    int a = 0 ;

    if ( number == 1 )
        a = 1 ;
    else if ( number == 2 )
        a = 2 ;
    else if ( number == 3 )
        a = 3 ;
    else if ( number > 3 )
        a = newFibonacci( number - 3 ) + newFibonacci( number - 2 ) + newFibonacci( number - 1 ) ;

    return  a ;
}  //定義費氏數列之計算：第一、二和三項分別是1、2和3；第三項之後的項，為往前數三項之和。
