#include <iostream>
using namespace std;

double newFibonacci(double number);  //費式數列函式


int main(){
    for(int i = 1 ; i <= 10 ; i++){  //輸出費式數列一到十
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    }
    for(int i = 2 ; i <= 4 ; i++){  //輸出費式數列二十到四十
        int number = i * 10;
        cout << "newfibonacci(" << number << ") = " << newFibonacci(number) << endl;
    }
    return 0;
}

double newFibonacci(double number){  //費式數列函式
    if(number == 1){
        return 1;
    }else if(number == 2){
        return 2;
    }else if(number == 3){
        return 3;
    }else{
        return newFibonacci(number - 3) + newFibonacci(number - 2) + newFibonacci(number - 1);
    }
}
