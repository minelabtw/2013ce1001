#include <iostream>
using namespace std;

double F(double n);                                             //宣告遞迴函式

int main()
{
    double n = 1;
    for (n; n<=40; n++)                                         //輸出遞迴的值
    {
        if(n==11)
            n=20;
        else if(n==21)
            n=30;
        else if(n==31)
            n=40;
        cout << "newfibonacci(" << n << ") = " << F(n) << endl;
    }
    return 0;
}

double F(double n)                                              //新遞迴函式→F(n) = F(n-1) + F(n-2) + F(n-3)
{
    if(n==1)
        return 1;
    else if(n==2)
        return 2;
    else if(n==3)
        return 3;
    else
        return (F(n-1) + F(n-2) + F(n-3));                      //如果n大於3，則會將F(n-1)、F(n-2)、F(n-3)都執行一遍
}
