#include<iostream>
using namespace std;
double newFibonacci(double number)
{
    double f1=1,f2=2,f3=3;
    double fn=0;
    for(int i=0; i<number; i++)
    {
        fn=f1+f2+f3;
        f1=f2;
        f2=f3;
        f3=fn;
    }
    return fn;
}
int main()
{
    cout<<"newfibonacci(1) = 1"<<endl;
    cout<<"newfibonacci(2) = 2"<<endl;
    cout<<"newfibonacci(3) = 3"<<endl;
    for(int i=4; i<=10; i++)
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i-3)<<endl;
    for(int i=20; i<=40; i+=10)
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i-3)<<endl;
    return 0;
}
