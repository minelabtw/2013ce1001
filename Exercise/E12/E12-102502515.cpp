#include <iostream>
using namespace std;

double newFibonacci(double number);//function

int main()
{
    for (int a=1; a<=10; a++)//for迴圈，重複輸出
    {
        cout << "newfibonacci(" << a << ") = " << newFibonacci(a) << endl;//call function
    }
    return 0;
}

double newFibonacci(double number)
{
    if (number==1)
        return 1;
    else if (number==2)
        return 2;
    else if (number==3)
        return 3;
    else
        return newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);//重複呼叫自己
}
