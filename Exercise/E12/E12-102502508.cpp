#include<iostream>
using namespace std ;
double newFibonacci(double number);
int main()//這次練習的目的在於運用遞迴的方式來進行費是數列的運算
{
    for(int i=1 ; i<=10 ; i=i+1 )
    {
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl ;  //利用回傳值顯示答案
    }
    return 0 ;

}
double newFibonacci(double number)
{   //在函式中利用主程式中的number值進行判斷並將回傳值傳回主程式

    if((number==1) || (number==2) || (number==3))
    {
        return number ;
    }
    else
    {

        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
    }



}
