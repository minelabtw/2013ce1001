#include<iostream>
using namespace std;

double newFibonacci(double number){
    if(number<=3)return number;
    else return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
}

int main(){
    while(true){
        static double i=1;
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
        if(i==40)break;
        i = (i<10 ? i+=1 : i+=10);
    }
    return 0;
}
