#include <iostream>
using namespace std;

double newFibonacci(int num){
    static double fib[10001]={1,1,2,3};//用陣列存費氏數列的數字,前三項初始化為1,2,3
    if(fib[num])//如果該項數字位於陣列內,則直接回傳
        return fib[num];
    else//否則回傳遞迴前三項相加
        return (fib[num] = newFibonacci(num-1)+newFibonacci(num-2)+newFibonacci(num-3));
}

int main(){
    int i;//陣列用變數
    for(i=1;i<=10;i++){//輸出第1~10項外加20,30,40項
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    }
    cout << "newfibonacci(" << 20 << ") = " << newFibonacci(20) << endl;
    cout << "newfibonacci(" << 30 << ") = " << newFibonacci(30) << endl;
    cout << "newfibonacci(" << 40 << ") = " << newFibonacci(40) << endl;
    return 0;
}
