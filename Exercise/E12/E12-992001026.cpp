#include <iostream>

using namespace std ;

// 新費波納契數列
double newFibonacci(double m) {
    if ( m == 1 )
        return 1 ;
    if ( m == 2 )
        return 2 ;
    if ( m == 3 )
        return 3 ;
    else
        return newFibonacci(m-1)+ newFibonacci(m-2)+ newFibonacci(m-3) ;

}

// 主函式
int main () {

    int n = 11 ;

    cout << "newfibonacci(1) = 1" << endl ;
    cout << "newfibonacci(2) = 2" << endl ;
    cout << "newfibonacci(3) = 3" << endl ;

    for ( int i = 4 ; i < n ; ++i ) {
        cout << "newfibonacci("<< i << ") = " ;
        cout << newFibonacci(i) <<endl ;
    }
    return 0 ;
}
