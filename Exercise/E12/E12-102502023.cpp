#include <iostream>
using namespace std;

double newFibonacci(double number); // function prototype

int main()
{
    for ( int i = 1; i <= 10; i++ ) // for loop to output the result
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci( i ) << endl;
    }

    for ( int j = 20; j <= 40; j += 10 ) // for loop to output the result
    {
        cout << "newfibonacci(" << j << ") = " << newFibonacci( j ) << endl;
    }

    return 0;
}

double newFibonacci(double number)
{
    if ( number == 1 )
        return 1;
    else if ( number == 2 )
        return 2;
    else if ( number == 3 )
        return 3;
    else
        return newFibonacci( number - 1 ) + newFibonacci( number - 2 ) + newFibonacci( number -3 );
}
