#include<iostream>

using namespace std;

double newFibonacci(double);

double newFibonacci(double number)
{
    if(number==1 || number==2 || number==3)                                                //讓前三項為本身項數值
        return number;

    else
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);       //第四項後為本身前三項相加
}

int main()
{
    for(int number=1 ; number<=10 ; number++)                                              //輸出新費氏數列一到十項
    {
        cout << "newfibonacci("<< number << ") = " << newFibonacci(number) << endl;
    }

    return 0;
}
