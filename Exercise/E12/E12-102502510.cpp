#include <iostream>

using namespace std;
double newFibonacci(double number)//function
{
    if(number<=3)
    {
        return number;
    }
    else
    {
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
    }

}
int main()
{
    for(double i=0; i<10; ++i)
    {
        cout << "newfibonacci(" << i+1 << ") = " << newFibonacci(i+1) << endl;
    }
    cout << "newfibonacci(" << 20 << ") = " << newFibonacci(20) << endl;
    cout << "newfibonacci(" << 30 << ") = " << newFibonacci(30) << endl;
    cout << "newfibonacci(" << 40 << ") = " << newFibonacci(40) << endl;

    return 0;
}
