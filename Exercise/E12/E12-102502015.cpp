#include <iostream>
using namespace std;
double newFibonacci(double number)
{
    if (number<1) //  小於1回傳0            1 2 3 分別回傳 1 2 3
    {
        return 0;
    }
    else if (number==1)
    {
        return 1;
    }
    else if (number==2)
    {
        return 2;
    }
    else
    {
        return (newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1));         //其於等於前三項相加
    }
}
int main()
{
for (int i=1;i<=10;i++)
{
    cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl;
}
cout<<"newfibonacci(20) = "<<newFibonacci(20)<<endl;
cout<<"newfibonacci(30) = "<<newFibonacci(30)<<endl;
cout<<"newfibonacci(40) = "<<newFibonacci(40)<<endl;

    return 0;
}
