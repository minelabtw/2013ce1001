#include <iostream>

using namespace std ;

double newFibonacci(double n)//宣告函式
{
    if (n==1)
    {
        return 1 ;
    }

    else if (n==2)
    {
        return 2 ;
    }

    else if (n==3)
    {
        return 3 ;
    }

    else
        return newFibonacci(n-1)+newFibonacci(n-2)+newFibonacci(n-3) ;//呼叫自己

}

int main ()
{
    for (int x=1; x<=10; x++)//用迴圈重複輸出
    {
        cout << "newfibonacci(" << x << ") = " << newFibonacci(x) << endl ;
    }

    for (int y=20;y<=40;y=y+10)
    {
        cout << "newfibonacci(" << y << ") = " << newFibonacci(y) << endl ;
    }

    return 0 ;
}
