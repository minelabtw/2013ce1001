#include <iostream>
using namespace std;

double newFibonacci(double number);//宣告newFibonacci原型

int main()
{
    //整個main都在印結果
    for(int i=1; i<=10; i++)
    {
        if(i==10)
            cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i);
        else
            cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl;
    }

    return 0;//結束
}

//實作newFibonacci
double newFibonacci(double number)
{
    if(number==1)
        return 1;
    else if(number==2)
        return 2;
    else if(number==3)
        return 3;
    else
        return newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);
}

