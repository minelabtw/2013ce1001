#include <iostream>
using namespace std;
double newFibonacci(double);//宣告遞迴函式
int main()
{
    for(double i = 1 ; i <= 10 ; i++)
    {
        cout<< "newfibonacci(" << i << ") = ";
        cout<< newFibonacci(i) <<endl;
    }//for迴圈將1~10先輸出出來
    cout<< "newfibonacci(20) = ";
    cout<< newFibonacci(20.0) <<endl;
    cout<< "newfibonacci(30) = ";
    cout<< newFibonacci(30.0) <<endl;
    cout<< "newfibonacci(40) = ";
    cout<< newFibonacci(40.0) <<endl;//再輸出10、20及30

    return 0;
}
double newFibonacci(double n)
{
    if( n == 1 ) return 1;//若變數為1則輸出1
    if( n == 2 ) return 2;//若變數為2則輸出2
    if( n == 3 ) return 3;//若變數為3則輸出3
    return newFibonacci(n-1)+newFibonacci(n-2)+newFibonacci(n-3);//其值為前三項相加
}
