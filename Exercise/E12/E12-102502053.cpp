#include <iostream>
using namespace std;

double newFibonacci(double); // fuction prototype

int main()
{
    for(int n=1;n<=10;n++)
    {
        cout<<"newfibonacci("<<n<<") = "<<newFibonacci(n)<<endl; //display 1-10 of the sequence
    }
    for(int n=20;n<=40;n=n+10)
    {
        cout<<"newfibonacci("<<n<<") = "<<newFibonacci(n)<<endl; //display 20-40 of the sequence
    }

    return 0;
}// end of main

//recursive function of this sequence
double newFibonacci(double n)
{
    if(n==1) //base cause
    {
        return 1;
    }
    else if(n==2) // base cause
    {
        return 2;
    }
    else if(n==3) // base cause
    {
        return 3;
    }
    else //recursion step
    {
        return newFibonacci(n-1)+newFibonacci(n-2)+newFibonacci(n-3);
    }
}
