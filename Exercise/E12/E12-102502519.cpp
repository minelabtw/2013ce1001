#include<iostream>
using namespace std;

double newFibonacci(double number);

int main()
{
    int num=1;

    newFibonacci(num);

    for(int i=1; i<=10; ++i)    //輸出newfibonacci 1~10的數值
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci(num) << endl;
        num++;
    }

    cout << "newfibonacci(20) = " << newFibonacci(20) << endl;    //輸出newfibonacci 20的數值
    cout << "newfibonacci(30) = " << newFibonacci(30) << endl;    //輸出newfibonacci 30的數值

    return 0;
}
double newFibonacci(double number)    //運算
{
    if(number==1)
        return 1;
    else if(number==2)
        return 2;
    else if(number==3)
        return 3;
    else
        return newFibonacci(number-1) + newFibonacci(number-2) + newFibonacci(number-3);
}

