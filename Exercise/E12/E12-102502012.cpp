#include <iostream>
#define MAX 13
using namespace std;
double F(double num){
	if (num == 1)
		return 1;
	if (num == 2)
		return 2;
	if (num == 3)
		return 3;
	return F(num - 1) + F(num - 2) + F(num - 3);
}
int main(){
	double ask[MAX] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40 };
	for (int i = 0; i < MAX; i++)
		cout << "newfibonacci(" << ask[i] << ") = " << F(ask[i]) << endl;
	return 0;
}