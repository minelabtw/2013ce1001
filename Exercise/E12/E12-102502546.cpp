#include<iostream>

using namespace std;

int newfibonacci(int number)//費氏數列
{
    if(number==1 || number==2 || number==3)//若是1.2.3 則顯示1.2.3
    {
        return number ;
    }
    else//除此之外為前三項和
    {
        return newfibonacci(number-1) + newfibonacci(number-2) + newfibonacci(number-3) ;
    }
}
int  main()
{
    for(int i=1; i<=10; i++)//從1到10，依序顯示費氏數列的結果
    {
        cout << "newfibonacci(" << i << ") = " << newfibonacci(i) << endl ;
    }
    return 0;
}
