#include<iostream>
using namespace std;

double newFibonacci(double number)
{
    if(number==0||number==1||number==2)
        return number;
    else
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3); //利用遞迴實作費氏數列
}


int main()
{
    for(int counter=1;counter<=10;counter++) //輸出一到十項
        cout<<"newfibonacci("<<counter<<") = "<<newFibonacci(counter)<<endl;
    cout<<"newfibonacci(20) = "<<newFibonacci(20)<<endl;
    cout<<"newfibonacci(30) = "<<newFibonacci(30)<<endl;
    cout<<"newfibonacci(40) = "<<newFibonacci(40)<<endl;
}
