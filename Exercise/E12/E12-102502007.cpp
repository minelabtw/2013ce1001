#include <iostream>
using namespace std;
double newFibonacci(double number)
{
    if(number<=3)
        return number;//分別回傳1,2,3
    else
        return newFibonacci(number-3) + newFibonacci(number-2) + newFibonacci(number-1);//回傳f(n-3)+f(n-2)+f(n-1)
}//遞迴函式
int main()
{
    for(double i=1 ; i<=10 ; i=i+1)
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    cout << "newfibonacci(" << 20 << ") = " << newFibonacci(20) << endl;
    cout << "newfibonacci(" << 30 << ") = " << newFibonacci(30) << endl;
    cout << "newfibonacci(" << 40 << ") = " << newFibonacci(40) << endl;
    return 0;
}
