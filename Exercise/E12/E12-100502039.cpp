#include <iostream>
using namespace std;

unsigned int newFibonacci(int number);

int main(){

    for(int i = 1; i<=10; i++){
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    }
    cout << "newfibonacci(20) = " << newFibonacci(20) << endl;
    cout << "newfibonacci(30) = " << newFibonacci(30) << endl;
    cout << "newfibonacci(40) = " << newFibonacci(40) << endl;

    return 0;
}

unsigned int newFibonacci(int number){
    if(number == 1)
        return 1;
    else if (number == 2)
        return 2;
    else if (number == 3)
        return 3;
    else
        return (newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3));
}
