#include <iostream>
using namespace std;

double newFibonacci(double );
int main()
{

    double number;
    for(int n=1; n<=40; n++)
    {
        number=newFibonacci(n);
        if(n<=10)
        {
            cout<<"newfibonacci("<<n<<") = "<<number<<endl;
        }
    }
    return 0;
}


double newFibonacci(double n)
{
    if(n==1)
        return 1;
    else if(n==2)
        return 2;
    else if(n==3)
        return 3;
    else
        return newFibonacci(n-3)+newFibonacci(n-2)+newFibonacci(n-1);
}
