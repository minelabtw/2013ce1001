#include <iostream>

using namespace std;

double answer[41] = {}; //宣告型別為 double 的一維陣列(answer)，用來儲存已經計算過的數值。

double newFibonacci ( int ); //function prototype

int main ()
{
    answer[0] = -1; //初始化 answer[0] ~ answer[3] 的值。
    answer[1] = 1;
    answer[2] = 2;
    answer[3] = 3;
    for ( int k = 4 ; k <= 40 ; k++ ) //用for迴圈初始化陣列裡其他元素的值為 -1 。
    {
        answer[k] = -1;
    }
    for ( int i = 1 ; i <= 10 ; i++ ) //用for迴圈輸出第 1 ~ 10 項的值。
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci( i ) << endl;

    }
    cout << "newfibonacci(20) = " << newFibonacci( 20 ) << endl; //輸出第 20 , 30 和 40 項的值。
    cout << "newfibonacci(30) = " << newFibonacci( 30 ) << endl;
    cout << "newfibonacci(40) = " << newFibonacci( 40 ) << endl;

    return 0;

}

double newFibonacci ( int number ) //費式數列
{
    if ( answer[number] != -1 ) //如果內部元素的值為不為 -1 ，則直接回傳內部所儲存的值。
    {

    }
    else
    {
        answer[number] = newFibonacci( number - 1 ) + newFibonacci ( number - 2 ) + newFibonacci( number - 3 );
        //計算完後，先儲存在陣列裡，再回傳計算後的值。
    }
    return answer[number];
}
