#include <iostream>
using namespace std;

double newFibonacci(double number); //設函式

int main()
{
    for(int i = 0 ; i < 10 ; i++)
    {
        cout << "newfibonacci(" << i+1 << ") = " << newFibonacci(i) << endl;
    }
    for(int j = 19 ; j < 40 ; j+=10) //每做完一次j+10
    {
        cout << "newfibonacci(" << j+1 << ") = " << newFibonacci(j) << endl;
    }
    return 0;
}

double newFibonacci(double number)
{
    if(number == 0)
        return 1;
    else if(number == 1)
        return 2;
    else if(number == 2)
        return 3;
    if(number >= 3) //將做完的函式再次做一次 不停重複 遞迴
        return newFibonacci(number-1) + newFibonacci(number-2) + newFibonacci(number-3);
}
