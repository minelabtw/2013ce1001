#include <iostream>

using namespace std;
double newFibonacci(double number); // function prototype

int main()
{
    for(int number=1;number<=10;number++) // calculate the Fibonacci value of 1 through 10
         cout<<"newfibonacci("<<number<<") = "<<newFibonacci(number)<<endl;

         cout<<"newfibonacci(20) = "<<newFibonacci(20)<<endl; //display higher Fibonacci values
         cout<<"newfibonacci(30) = "<<newFibonacci(30)<<endl;
         cout<<"newfibonacci(40) = "<<newFibonacci(40)<<endl;
         return 0;
}
double newFibonacci(double number) //recursive function Fibonacci
{
   if(number==1 ||number==2 || number==3)
      return number;
   else
      return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
}
