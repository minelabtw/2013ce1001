#include <iostream>
using namespace std;

double newFibonacci(double );
int main()
{

    for (int a=1;a<=10;a++)
    {
        cout << "newFibonacci(" << a << ") = ";
        cout << newFibonacci(a) << endl;
    }
}
double newFibonacci(double a)
{
    int data[10] = {};
    if(a==1)
    {
        return 1;
    }
    if (a==2)
    {
        return 2;
    }
    if(a==3)
    {
        return 3;
    }
    else
    {
        return newFibonacci(a-3) + newFibonacci(a-1) + newFibonacci(a-2);
    }
}
