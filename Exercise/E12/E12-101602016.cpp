#include <iostream>
using namespace std;

double newFibonacci(double number)//新費氏數列
{
    double a=1,b=2,c=3,s=0,i=0;//定義首項=1，二項=2，三項=3
    if(number==1)//當輸入第一項
        s=a;//輸出第一項
    else if(number==2)//當輸入第二項
        s=b;//輸出第二項
    else if(number==3)//當輸入第三項
        s=c;//輸出第三項
    else//當輸入第四項以上時
    {
        for(i=1;i<=(number-3);i++)//需運算項數減三次
        {
            s=a+b+c;//令s等於前三項相加
            a=b;//將每一項往後移一位
            b=c;
            c=s;
        }
    }
    return s;//輸出此項
}

int main()
{
    int i=0;
    for(i=1;i<=9;i++)//輸出項數1~9項
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl;
    for(i=1;i<=4;i++)//輸出10,20,30,40項
        cout<<"newfibonacci("<<i*10<<") = "<<newFibonacci(i*10)<<endl;
    return 0;
}
