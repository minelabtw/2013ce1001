#include <iostream>

using namespace std;

double newFibonacci(double number)
{
    if(number==1)
        return 1;
    if(number==2)
        return 2;
    if (number==3)
        return 3;
    else
        return newFibonacci(number-1) + newFibonacci(number-2) + newFibonacci(number-3);

}
int main()
{
    double ans1=0, ans2=0, ans3=0;//ans1為到20的結果，ans2為到30的結果，ans3為到40的結果
    for (int i=1; i<=10; i++)
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    for (int i=1; i<=20; i++)
        ans1=newFibonacci(i);
    cout << "newfibonacci(" << "20" << ") = " << ans1 << endl;
    for (int i=1; i<=30; i++)
        ans2=newFibonacci(i);
    cout << "newfibonacci(" << "30" << ") = " << ans2 << endl;
    for (int i=1; i<=40; i++)
        ans3=newFibonacci(i);
    cout << "newfibonacci(" << "40" << ") = " << ans3 << endl;
    return 0;
}
