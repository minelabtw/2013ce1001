#include <iostream>
using namespace std;

double newFibonacci(double number)
{
    if(number==1)
        return 1;
    else if(number==2)
        return 2;
    else if(number==3)
        return 3;
    return newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);
}

int main()
{
    double n;
    while(cin>>n)
    {
        cout<<"newfibonacci("<<n<<") = "<<newFibonacci(n)<<endl;
    }
    return 0;
}
