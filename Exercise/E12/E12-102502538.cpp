#include<iostream>
using namespace std;

double newFibonacci(double number)//設定遞迴函數

{
    if(number==1)//定義1,2,3時的值
        return 1;
    else if(number==2)
        return 2;
    else if(number==3)
        return 3;
    else if(number>3)
        return newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);
}

int main()
{
    for(int i=1; i<11; i++)//輸出結果
    {
        cout<<"newFibonacci("<<i<<") = "<<newFibonacci(i)<<endl;
    }
    cout<<"newFibonacci("<<"20"<<") = "<<newFibonacci(20)<<endl;
    cout<<"newFibonacci("<<"30"<<") = "<<newFibonacci(30)<<endl;
    cout<<"newFibonacci("<<"40"<<") = "<<newFibonacci(40);

    return 0;
}
