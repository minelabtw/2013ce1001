#include <iostream>
using namespace std;
double fib[500]; // save fib number
double newFibonacci(int number)
{
    if (fib[number] == -1) // if number haven't calc
        fib[number] = newFibonacci(number-3) + newFibonacci(number-2) + newFibonacci(number-1);
    return fib[number];
}
int main(int argc, char *argv[])
{
    // init
    for (int i=0;i<50;i++)
        fib[i] = -1;
    fib[1] =1;
    fib[2] =2;
    fib[3] =3;
    // print 1 - 9 10 - 40
    for (int i=1;i<=40;i += (i>=10) ? 10 : 1)
        cout << "newfibonacci(" << i << ") = " << newFibonacci((double)i) << endl;
    return 0;
}
