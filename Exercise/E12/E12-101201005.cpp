/*利用遞迴實作費氏數列
f(1)=1
f(2)=2
f(3)=3
f(n+3)=f(n)+f(n+1)+f(n+2)
*/
#include<iostream>
using namespace std;

double newFibonacci(double number) //費氏數列
{
    if (number==1)
        return 1;
    else if (number==2)
        return 2;
    else if (number==3)
        return 3;
    else
        return newFibonacci(number-3) + newFibonacci(number-2) + newFibonacci(number-1);
}

int main()
{

    for (int i=1;i<=10;i++) //數列1-10
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    for (int i=20;i<50;i=i+10) //數列20.30.40
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;


    return 0;

}
