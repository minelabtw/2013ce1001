#include <iostream>
using namespace std;
double suppArray[100] = {0.0, 1.0, 2.0, 3.0};//支援用陣列 用來儲存已經計算過的費氏數列

double newFibonacci(double number);//回傳費氏數列第number項

int main(){
    for(int i = 1; i < 10; i++){
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    }
    for(int i = 1; i <= 4; i++){
        cout << "newfibonacci(" << i * 10 << ") = " << newFibonacci(i * 10) << endl;
    }
    return 0;
}

/*double newFibonacci(double number){//回傳費氏數列第number項
    if(number == 1.0){
        return 1.0;
    }
    else if(number == 2.0){
        return 2.0;
    }
    else if(number == 3.0){
        return 3.0;
    }
    else{
        return newFibonacci(number - 1) + newFibonacci(number - 2) + newFibonacci(number - 3);
    }
}*/

double newFibonacci(double number){//回傳費氏數列第number項
    if(number == 1.0){
        return 1.0;
    }
    else if(number == 2.0){
        return 2.0;
    }
    else if(number == 3.0){
        return 3.0;
    }
    else{
        //如果suppArray的number - 1、number - 2、number - 3個等於0 代表尚未計算，
        //必須用遞迴求得 否則之前就已經算過 可以不必算
        if(suppArray[(int)(number - 1)] == 0.0){
            suppArray[(int)(number - 1)] = newFibonacci(number - 1);
        }
        if(suppArray[(int)(number - 2)] == 0.0){
            suppArray[(int)(number - 2)] = newFibonacci(number - 2);
        }
        if(suppArray[(int)(number - 3)] == 0.0){
            suppArray[(int)(number - 3)] = newFibonacci(number - 3);
        }
        return suppArray[(int)(number - 1)] + suppArray[(int)(number - 2)] + suppArray[(int)(number - 3)];
    }
}
