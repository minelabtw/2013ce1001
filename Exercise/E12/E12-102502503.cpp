#include <iostream>
using namespace std;
double newFibonacci(double number)
{
    if(number == 1 || number == 2 || number==3)  //若number值為1或2或3回傳number值
        return number;
    else
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3) ;  //若number為其餘值使用遞迴
}
int main()
{
    for (int n=1; n<=10; n++)
    {
        cout << "newfibonacci("<< n << ") = " << newFibonacci(n) << endl;  //輸出1~10的費氏數列
    }
    cout << "newfibonacci("<< 20 << ") = " << newFibonacci(20) << endl;
    cout << "newfibonacci("<< 30 << ") = " << newFibonacci(30);
    return 0;
}
