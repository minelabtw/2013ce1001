#include<iostream>
using namespace std;

double newfibonacci(double x)//算費氏數列
{
    if( (x==1) || (x==2) || (x==3))
        return x;//回傳相同的數字
    else
        return newfibonacci(x-1) + newfibonacci(x-2) + newfibonacci(x-3);//項回傳前三項的合
}

int  main()
{
    for(double n=1 ; n<= 10 ; n++)//輸出前10項
        cout<<"newfibonacci("<<n<<") = "<<newfibonacci(n)<<endl;

    cout<<"newfibonacci(20) = "<<newfibonacci(20)<<endl;//第20項
    cout<<"newfibonacci(30) = "<<newfibonacci(30)<<endl;//第30項
    cout<<"newfibonacci(40) = "<<newfibonacci(40)<<endl;//第40項

    return 0;
}
