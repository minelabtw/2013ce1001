#include <iostream>

using namespace std;

double newFibonacci(double number);

int main()
{
    double x=1; //項數
    while(x<=10) //當X<=10的時候，執行此迴圈
    {
        cout << "newfibonacci(" << x << ") = " << newFibonacci(x) << endl;
        x++;
    }
    while(x<=30)
    {
        if(x==20) //當x跑到20的時候，輸出下方
        {
            cout <<  "newfibonacci(20) = " << newFibonacci(20) << endl;
        }

        if(x==30) //當x跑到30的時候，輸出下方
        {
            cout << "newfibonacci(30) = " << newFibonacci(30) << endl;
        }
        x++;
    }


    return 0;
}

double newFibonacci(double number)
{
    int fi; //值

    if(number<=3)
    {
        fi=number; //前三項的值就是項數
    }
    else if(number>3) //第四項(含)之後，值就是前三項的合
    {
        fi=newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
        number++;

    }
    return fi; //回傳fi
}
