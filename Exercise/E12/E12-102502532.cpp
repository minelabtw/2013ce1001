#include<iostream>
using namespace std;

double newFibonacci(double number);           //用 函式

int main()
{
    for (int n =1; n<=10; n++)
        cout<<"newfibonacci("<<n<<") = "<<newFibonacci(n)<<endl;             //此不用0

    cout<<"newfibonacci(20) = "<<newFibonacci(20)<<endl;
    cout<<"newfibonacci(30) = "<<newFibonacci(30)<<endl;
    cout<<"newfibonacci(40) = "<<newFibonacci(40)<<endl;

    return 0;
}

double newFibonacci(double number)
{
    if (number ==1 or number ==2 or number ==3)         //base cases
        return number;
    else
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);         //可 return 函式值
}
