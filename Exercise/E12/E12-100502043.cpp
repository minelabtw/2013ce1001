#include<iostream>
using namespace std;

double newFibonacci(double number);  //function prototype

int main()
{
    for (int i=1;i<=10;i++)
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;

    return 0;
}

double newFibonacci(double number)
{
    //定義前三項分別為1、2、3
    if (number<4)
        return number;
    //第n項=前三項之和(n>3)
    else
        return newFibonacci(number-1) + newFibonacci(number-2) + newFibonacci(number-3);
}
