#include <iostream>
using namespace std;

double newFibonacci(double number) //費氏數列遞迴
{
    if (number==1)
        return 1;
    else if (number==2)
        return 2;
    else if (number==3)
        return 3;
    else
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
}

int main()
{
    for (int i=1; i<=10; i++)
    {
        cout << "newFibonacci" << "(" << i << ")" << " = " << newFibonacci(i) << endl;
    }
    cout << "newFibonacci(20) = " << newFibonacci(20) << endl;
    cout << "newFibonacci(30) = " << newFibonacci(30) << endl;
    cout << "newFibonacci(40) = " << newFibonacci(40) << endl; //輸出結果

    return 0;
}
