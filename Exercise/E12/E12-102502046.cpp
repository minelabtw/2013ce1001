#include<iostream>
using namespace std;
double fib[41]={};		//宣告fib陣列

double newFibonacci(int number)
{
	if(fib[number]==-1)
		fib[number]=newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);
	return fib[number];
}

int main()
{
	int b[13]={1,2,3,4,5,6,7,8,9,10,20,30,40};
	for (int i=1 ; i<=40 ; i++)	//初始fib陣列==-1
		fib[i]=-1;

	fib[1]=1;		//設定前3項的值
	fib[2]=2;
	fib[3]=3;

	for (int i=0 ; i<=12 ; i++)		//印出結果
		cout << "newfibonacci(" << b[i] << ") = " <<  newFibonacci(b[i]) << endl;
	return 0;
}

