#include <iostream>
using namespace std;

double newFibonacci(double number)                 //宣告一個遞迴函式
{
    if (number<1)
    {
        return 0;
    }
    if (number==1)
    {
        return 1;
    }
    if (number==2)
    {
        return 2;
    }
    else                                          //當n大於等於3之後 回傳前三項的和
    {
        return (newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1));
    }
}

int main()
{
    for (int i=1;i<=10;i++)                       //迴圈列印n=1~n=10
    {
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl;
    }
    cout<<"newfibonacci(20) = "<<newFibonacci(20)<<endl;
    cout<<"newfibonacci(30) = "<<newFibonacci(30)<<endl;
    cout<<"newfibonacci(40) = "<<newFibonacci(40)<<endl;
    return 0;
}
