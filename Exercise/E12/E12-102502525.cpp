#include<iostream>
using namespace std;
double newfibonacci(double number)//計算費氏數列
{
    if( (number == 1) || (number == 2) || (number == 3))
        return number;//前三項一樣，不變
    else
        return newfibonacci(number-1) + newfibonacci(number-2) + newfibonacci(number-3);//重複計算
}
int main()
{
    for(double counter = 1 ; counter <= 10 ; counter++)//輸出前10項
        cout << "newfibonacci(" << counter << ") = " << newfibonacci(counter) << endl;
        cout << "newfibonacci(20) = "<< newfibonacci(20) << endl;//輸出第20項
        cout << "newfibonacci(30) = "<< newfibonacci(30) << endl;//輸出第30項
        cout << "newfibonacci(40) = "<< newfibonacci(40) << endl;//輸出第40項
    return 0;
}
