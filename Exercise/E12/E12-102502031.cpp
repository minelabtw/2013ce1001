#include <iostream>

using namespace std;

void output(int first_number, int last_number, int differencee);
double newFibonacci(double number);

int main(void)
{
    output(1,9,1);     //output 1,2,3,4,5,6,7,8,9
    output(10,40,10);  //output 10,20,30,40
    return 0;
}

void output(int first_number, int last_number, int difference)
{
    for (int i=first_number; i<=last_number; i=i+difference)
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
}

double newFibonacci(double number)
{
    if (number<=3)
        return number;    //return number itself if number is 1,2, or 3
    else if (number>3)
        return newFibonacci(number-3)+newFibonacci(number-2)+newFibonacci(number-1);    //Recursion
}
