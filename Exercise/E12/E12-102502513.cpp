//費式數列
#include <iostream>
using namespace std;

double newFibonacci(double );

int main()
{
    for(int i=1; i<=10; i++)
    {
        cout << "newfibonacci(" << i << ") = " << newFibonacci(i) << endl;
    }
    return 0;
}

double newFibonacci(double number)  //用遞廻函式寫費式數列
{
    if (number==1)
        return 1;
    else if (number==2)
        return 2;
    else if (number==3)
        return 3;
    else
        return newFibonacci(number-1)+newFibonacci(number-2)+newFibonacci(number-3);
}
