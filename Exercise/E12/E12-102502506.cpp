#include <iostream>
using namespace std;
double newFibonacci(double i)
{
    double f1 = 1, f2 = 2, f3 = 3;
    double fn = 0;
    for ( int y = 0; y < i; y++ )
    {
        fn = f1 + f2 + f3;
        f1 = f2;
        f2 = f3;
        f3 = fn;
    }
    return fn;
}
int main ()
{
    cout << "newfibonacci(1) = 1" << endl;
    cout << "newfibonacci(2) = 2" << endl;
    cout << "newfibonacci(3) = 3" << endl;
    for ( int y = 4;y <= 10;y ++ )
    {
        cout << "newfibonacci(" << y << ") = " << newFibonacci(y-3) << endl;
    }
    for ( int z = 20; z <= 40; z+=10 )
    {
        cout << "newfibonacci(" << z << ") = " << newFibonacci(z-3) << endl;
    }
    return 0;
}
