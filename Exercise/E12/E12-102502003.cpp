#include<iostream>
#include<cmath>
using namespace std;

double newFibonacci(double number);  //費式數列宣告

int main()
{
    for(int i=1; i<=10; i++)  //從第一項到第十項
    {
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl;
    }

    for(int i=20; i<=40; i+=10)  //輸出第20、30、40項
    {
        cout<<"newfibonacci("<<i<<") = "<<newFibonacci(i)<<endl;
    }


    return 0;
}

double newFibonacci(double n)  //費式數列 = 後項為前三項之和
{
    if(n==1)  //第一項
        return 1;
    else if(n==2)  //第二項
        return 2;
    else if(n==3)  //第三項
        return 3;
    else  //從第四項開始
        return newFibonacci(n-2)+newFibonacci(n-1)+newFibonacci(n-3);
}
