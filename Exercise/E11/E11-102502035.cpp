#include <iostream>
using namespace std;
void bubblesort(int arraya[] , int sizea);
int main ()
{
    int sizea;//宣告變數並=0
    int arraya[20] = {};
    cout << "Enter array size (5-20): " ;//提示輸入
    cin >> sizea;//輸入
    while (sizea >20 ||sizea <5)
    {
        cout << "Out of range!" << endl << "Enter array size (5-20): ";//提示錯誤輸入
        cin >> sizea;//輸入
    }
    for (int i =0; i <sizea; i++)
    {
        cout << "The " << i+1 << " element (0-100): ";//提示輸入
        cin >> arraya[i];//輸入
        while (arraya[i] <0 ||arraya[i] >100)
        {
            cout << "Out of range!" << endl <<"The " << i+1 << " element (0-100): " ;//提示錯誤輸入
            cin >> arraya[i];//輸入
        }
    }
    cout << "Before sort" << endl;//輸出題目要求
    for (int i =0; i <sizea; i++)
    {
        cout << "The " << i+1 << " element: " << arraya[i] << endl;//輸出題目要求
    }
    bubblesort(arraya , sizea);//使用函式
    cout << "After sort" << endl;//輸出題目要求
    for (int i =0; i <sizea; i++)
    {
        cout << "The " << i+1 << " element: " << arraya[i] << endl;//輸出最後結果
    }
    return 0;
}
void bubblesort(int arraya[] , int sizea)//函式
{
    int temp =0;//宣告變數並=0
    for (int i =sizea; i >0; i--)//重複個數次
    {
        for (int j =0; j <i-1; j++)//確保往後移次
        {
            if (arraya[j] >arraya[j+1])
            {
                temp =arraya[j];//先轉存
                arraya[j] =arraya[j+1];//換一個
                arraya[j+1] =temp;//存回來
                temp =0;
            }
        }
    }
}
