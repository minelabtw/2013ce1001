#include<iostream>
using namespace std;
void bubblesort(int* arr,int _size);//宣告排列用的函式
int main()
{
    int _size;//宣告輸入的次數
    int num[20]= {}; //宣告陣列
    cout<<"Enter array size (5-20): ";
    cin>>_size;
    while(_size<5||_size>20)//判斷範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter array size (5-20): ";
        cin>>_size;
    }
    for(int a=0; a<_size; a++)//用迴圈輸入陣列
    {
        cout<<"The "<<a+1<<" element (0-100): ";
        cin>>num[a];
        while(num[a]<0||num[a]>100)
        {
            cout<<"Out of range!"<<endl;
            cout<<"The "<<a+1<<" element (0-100): ";
            cin>>num[a];
        }
    }
    cout<<"Before sort"<<endl;
    for(int b=0; b<_size; b++)//輸出原陣列
    {
        cout<<"The "<<b+1<<" element: "<<num[b]<<endl;
    }
    cout<<"After sort"<<endl;
    bubblesort(num,_size);
    for(int c=0; c<_size; c++)//輸出轉換後的陣列
    {
        cout<<"The "<<c+1<<" element: "<<num[c]<<endl;
    }
}
void bubblesort(int* arr,int _size)
{
    for(int f=0; f<_size; f++)//用迴圈排序
    {
        for(int e=0; e<_size-1; e++)
        {
            if(arr[e]>arr[e+1])
            {
                int a=arr[e];
                int b=arr[e+1];
                arr[e]=b;
                arr[e+1]=a;
            }
        }
    }
}
