#include <iostream>

using namespace std;

void bubblesort( int * , int ); //function prototype

int main ()
{
    int ArraySize = 0; //宣告型別為 int 的變數(ArraySize)，用來儲存陣列的大小。
    int element[20] = {}; //宣告型別為 int 的一維陣列(element)，用來儲存所有元素的值。
    cout << "Enter array size (5-20): ";
    cin >> ArraySize;
    while ( ArraySize < 5 || ArraySize > 20 ) //用while迴圈檢測使用者的輸入是否合乎標準，若不符，則要求其重新輸入。
    {
        cout << "Out of range!" << endl;
        cout << "Enter array size (5-20): ";
        cin >> ArraySize;
    }
    for ( int i = 0 ; i < ArraySize ; i++ ) //用for迴圈使每一次的輸入能儲存到陣列裡所對應元素中。
    {
        int input = 0; //宣告型別為 int 的變數(input)，用來暫時儲存所輸入的數值。
        cout << "The " << i + 1 << " element (0-100): ";
        cin >> input;
        while ( input < 0 || input > 100 ) //用while迴圈檢測使用者的輸入是否合乎標準，若不符，則要求其重新輸入。
        {
            cout << "Out of range!" << endl;
            cout << "The " << i + 1 << " element (0-100): " ;
            cin >> input;
        }
        element[i] = input;
    }
    cout << "Before sort" << endl;
    for ( int j = 0 ; j < ArraySize ; j++ ) //輸出未排序前的陣列。
    {
        cout << "The " << j + 1 << " element: " << element[j] << endl;
    }
    bubblesort( element , ArraySize ); //將整個陣列和陣列大小的值傳入副函式中，做泡泡排序法。
    cout << "After sort" << endl;
    for ( int j = 0 ; j < ArraySize ; j++ ) //輸出排序後的陣列。
    {
        cout << "The " << j + 1 << " element: " << element[j] << endl;
    }

    return 0;
}

void bubblesort ( int *Array , int ArraySize ) //泡泡排序法
{
    while ( ArraySize - 1 > 0 )
    {
        for( int i = 0 ; i < ArraySize - 1 ; i++ )
        {
            if ( Array[i] > Array[i+1] )
            {
                int mid = 0;
                mid = Array[i+1];
                Array[i+1] = Array[i];
                Array[i] = mid;
            }
        }
        ArraySize--;
    }
}
