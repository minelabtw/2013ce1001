#include<iostream>
using namespace std;

int bubblesort(int *, int ); //宣告函式

int main()
{
    int a=0; //宣告變數
    int c=0;
    do
    {
        cout << "Enter array size (5-20): "; //輸出Enter array size (5-20):
        cin >> a; //輸入a
        if (a<5 or a>20) //判斷a<5或a>20時
            cout << "Out of range!" << endl;
    }
    while (a<5 or a>20); //當a<5或a>20進入迴圈

    int b[a]; //宣告陣列
    do
    {
        cout << "The " << c+1 << " element (0-100): ";
        cin >> b[c];
        if (b[c]>=0 and b[c]<=100)
            c++;
        else
            cout << "Out of range!" <<endl;
    }
    while (c<a);
    cout << "Before sort" << endl;
    for (int d=0; d<a; d++) //輸出陣列內容
    {
        cout << "The " << d+1 << " element: " << b[d] << endl;
    }
    cout << "After sort";
    bubblesort(b,a);  //進入函式
    for (int d=0; d<a; d++)  //輸出陣列內容
    {
        cout << endl << "The " << d+1 << " element: " << b[d];
    }
    return 0;
}

int bubblesort(int* array, int size) //函式
{
    for (int i=0; i<size-1 ; i++)
    {
        int n=i;
        int g=array[i+1];
        if(g<array[i])
            while (g<array[n] and n>=0)
            {
                array[n+1]=array[n];
                n--;
            }
        array[n+1]=g;
    }
}
