#include <iostream>
using namespace std;
void bubblesort(int Array0[20] , int Size)
{
    for ( int i = 0; i < Size; i++ )
    {
        for ( int j = 0; j < Size; j++ )
        {
            if ( Array0[j] > Array0[j+1])
            {
                int X;
                X = Array0[j];
                Array0[j] = Array0[j+1];
                Array0[j+1] = X;
            }
        }
    }
    for ( int i = 0; i < Size; i++ )
    {
        cout << "The " << i + 1 << " element: " << Array0[i] << endl;
    }
}
int main()
{
    int Array [20];
    int s;
    do
    {
        cout << "Enter array size (5-20): ";
        cin >> s;
        if ( s < 5 || s > 20 )
            cout << "Out of range!" << endl;
    }
    while ( s < 5 || s > 20 );
    for ( int i = 0; i < s; i++ )
    {
        cout << "The " << i + 1 << " element (0-100): ";
        cin >> Array [i];
        if ( Array [i] < 0 || Array [i] > 100 )
        {
            cout << "Out of range!" << endl;
            i--;
            continue;
        }
    }
    cout << "Before sort" << endl;
    for ( int i = 0;i < s;i++)
    {
        cout << "The " << i + 1 << " element: " << Array [i] << endl;
    }
    cout << "After sort" << endl;
    bubblesort( Array,s );
    return 0;
}
