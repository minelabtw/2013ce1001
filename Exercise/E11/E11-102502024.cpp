#include<iostream>
using namespace std;
int main()
{
    int L=0,element=0,_size=0;  //宣告變數
    int array[_size];
    int bubblesort(int* array ,int _size);
    cout<<"Enter array size (5-20): ";  //輸出題目
    cin>>L;  //輸入數字
    while(L>20 || L<5)  //判斷是否符合條件
    {
        cout<<"Out of range!\n";
        cout<<"Enter array size (5-20): ";
        cin>>L;
    }
    _size=L;
    for(int i=0; i<L; i++)
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        cin>>element;  //輸入數字
        while(element>100 || element<0)  //判斷是否符合條件
        {
            cout<<"Out of range!\n";
            cout<<"The "<<i+1<<" element (0-100): ";
            cin>>element;
        }
        array[i]=element;  //存入陣列
    }
    cout<<"Before sort\n";
    for(int m=0; m<L; m++)  //輸出陣列
    {
        cout<<"The "<<m+1<<" element (0-100): "<<array[m]<<endl;
    }
    cout<<"After sort\n";
    bubblesort(array,_size);  //泡泡排法
    return 0;
}
int bubblesort(int* array,int _size)  //宣告函式
{
    int temp=0;
    for(int v=0; v<_size; v++)
    {
        for(int n=v; n<_size; n++)
        {
            if(array[v]>array[n+1])
            {
                temp=array[v];
                array[v]=array[n+1];
                array[n+1]=temp;
            }
        }
    }
    for(int c=0; c<_size; c++)
    {
        cout<<"The "<<c+1<<" element (0-100): "<<array[c+1]<<endl;
    }
    return 0;
}


