#include <iostream>
using namespace std;

//ensure the functions' sequence won't make CE
void exchange(int *, int *); //exchange two numbers' values
void bubblesort(int, int *); //bubble sort increasing

//optional functions
/*
void selectionsort(int, int *); //selection sort increasing
void selectionsort(int arr[], int *arr_size) //selection sort
{
    int temp = 0; //temporary
    for (int i = 0; i < *arr_size-1; i ++)
    {
        temp = i;
        for (int j = i + 1; j < *arr_size; j ++)
            if (arr[j] < arr[temp])
                temp = j;
        exchange(&arr[i], &arr[temp]);
    }
}
*/
/*
void inverseselectionsort(int, int *); //selection sort decreasing
void inverseselectionsort(int arr[], int *arr_size) //selection sort
{
    int temp = 0; //temporary
    for (int i = 0; i < *arr_size-1; i ++)
    {
        temp = i;
        for (int j = i + 1; j < *arr_size; j ++)
            if (arr[j] > arr[temp])
                temp = j;
        exchange(&arr[i], &arr[temp]);
    }
}
*/
/*
void inversebubblesort(int, int *) //bubble sort decreasing
void inversebubblesort(int arr[], int *arr_size) //bubble sort
{
    //sortint
    for (int i = 0; i < *arr_size-1; i ++)
        for (int j = 0; j < *arr_size-1; j ++)
        {
            if (arr[j] > arr[j+1])
                exchange(&arr[j], &arr[j + 1]);
            else if (arr[i] < arr[j + 1])
                break;
        }

    //check whether result is right or not (exceptially the last element in orig. array)
    for (int i = 0; i < *arr_size - 1; i ++)
        if (arr[i] > arr[i + 1])
        {
            bubblesort(arr, arr_size);
            break;
        }
}
*/

void bubblesort(int arr[], int *arr_size) //bubble sort
{
    //sortint
    for (int i = 0; i < *arr_size-1; i ++)
        for (int j = 0; j < *arr_size-1; j ++)
        {
            if (arr[j] > arr[j+1])
                exchange(&arr[j], &arr[j + 1]);
            else if (arr[i] < arr[j + 1])
                break;
        }

    //check whether result is right or not (exceptially the last element in orig. array)
    for (int i = 0; i < *arr_size - 1; i ++)
        if (arr[i] > arr[i + 1])
        {
            bubblesort(arr, arr_size);
            break;
        }
}

void exchange(int *no_1, int *no_2) //exchange two numbers' values
{
        int temp = 0; //temporary
        temp = *no_1;
        *no_1 = *no_2;
        *no_2 = temp;
}

int main()
{
    //declar a varible to save the sizee and ask for arrray size
    int NumSize = 0;
    do
    {
        cout << "Enter array size (5-20): ";
        cin >> NumSize;
        if (NumSize < 5 or NumSize > 20)
            cout << "Out of range!" << endl;
    }
    while (NumSize < 5 or NumSize > 20);

    //decalar array to save data
    int number[NumSize];
    number[NumSize] = {};

    //ask for data
    for(int i = 0; i < NumSize; i ++)
    {
        cout << "The " << i + 1 << " element (0-100): ";
        cin >> number[i];
        if (number[i] < 0 or number[i] > 100)
        {
            cout << "Out of range!" << endl;
            i --;
        }
    }

    //output orig. data
    cout << "Before sort" << endl;
    for(int i = 0; i < NumSize; i ++)
        cout << "The " << i + 1 << " element): " << number[i] << endl;

    //sorting data
    bubblesort(number, &NumSize);
    //other choice
    //inser bubblesort(number, &NumSize);
    //selectionsort(number, &NumSize);
    //inverseselectionsort(number, &NumSize);

    //output results
    cout << "After sort" << endl;
    for(int i = 0; i < NumSize; i ++)
        cout << "The " << i + 1 << " element): " << number[i] << endl;

    return 0;
}
