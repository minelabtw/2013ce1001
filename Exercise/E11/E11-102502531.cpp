#include <iostream>

using namespace std;

int main()
{
    int arraysize=0;
    void bubblesort(int* array , int size);

    do
    {
        cout << "Enter array size (5-20): " ;
        cin >> arraysize;
        if (arraysize<5||arraysize>20)
            cout<<"Out of range!"<<endl;
    }
    while (arraysize<5||arraysize>20); //輸入幾行陣列
    int array_1[arraysize];
    for (int i=1,z=arraysize; i<=z;i++)
    {
        cout<<"The "<<i<<" element (0-100): ";
        cin>>array_1[i-1];
        while(array_1[i-1]<0||array_1[i-1]>100)
        {
            cout<<"Out of range!"<<endl;
            cout<<"The "<<i<<" element (0-100): ";
            cin>>array_1[i-1];
        }

    }//輸入陣列裡的數值
    cout<<"Before sort"<<endl;
    for (int i=1,z=arraysize; i<=z; i++)
    {
        cout<<"The "<<i<<" element (0-100): "<<array_1[i-1]<<endl;
    }//印出原先的值
    cout<<"After sort"<<endl;
    bubblesort( array_1 , arraysize); //經過排列
    for (int i=1,z=arraysize; i<=z; i++)
    {
        cout<<"The "<<i<<" element (0-100): "<<array_1[i-1]<<endl;
    } //輸出排列過後的值

return 0;
}

void bubblesort(int* array_1 , int arraysize) //如何排列bubblesort
{
    int i, j, temp;
    for (i=arraysize-1; i>0; i--)
    {
        for (j=0; j<i; j++)
        {
            if (array_1[j]>array_1[j+1])
            {
                temp=array_1[j];
                array_1[j]=array_1[j + 1];
                array_1[j+1]=temp;
            }
        }
    }
}

