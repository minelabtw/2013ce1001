#include<iostream>

using namespace std;

int bubblesort(int* num , int L);  //宣告函式

int main()
{
    int L=0;  //宣告變數

    cout << "Enter array size (5-20): ";  //輸出字串
    cin  >> L;                            //輸入項數

    while(L<5 || L>20)  //限定項數
    {
        cout << "Out of range!" << endl << "Enter array size (5-20): ";
        cin  >> L;
    }

    int num[L];  //宣告陣列

    for(int i=0;i<L;i++)  //初始陣列
    {
        num[i]=-1;
    }

    for(int i=0;i<L;i++)  //輸入值給陣列
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin  >> num[i];

        if(num[i]<0 || num[i]>100)
        {
            cout << "Out of range!" << endl << "The " << i+1 << " element (0-100): ";
            cin  >> num[i];
        }
    }

    cout << "Before sort" << endl;  //輸出陣列元素

    for(int i=0;i<L;i++)
    {
        cout << "The " << i+1 << " element (0-100): " << num[i] << endl;
    }

    cout << "After sort" << endl;  //輸出轉換過的陣列元素

    bubblesort(num , L);

    for(int i=0;i<L;i++)
    {
        cout << "The " << i+1 << " element (0-100): " << num[i] << endl;
    }

    return 0;
}

int bubblesort(int* num , int L)  //轉換數值
{
    int a=0;

    for(int i=L-1;i>0;i--)
    {
        for(int j=0;j<i;j++)
        {
            if(num[j]>num[j+1])
            {
                a=num[j+1];
                num[j+1]=num[j];
                num[j]=a;
            }

        }
    }

    return num[L];
}
