#include <iostream>

using namespace std;

void bubblesort(int* arrayx , int sizeofx)//將陣列裡的值從小排序到大
{
    for (int i=1;i<sizeofx;i++)
        for (int i=1;i<sizeofx;i++)
    {
        if (arrayx[i-1]>arrayx[i])//如果前一位大於後一位，位置交換
            swap(arrayx[i-1],arrayx[i]);
    }
}
int main()
{
    int x[20]= {}, L=0;
    cout << "Enter array size (5-20): ";
    while (cin >> L)//輸入陣列有幾個數字
    {
        if (L>=5&&L<=20)
        {
            for (int i=0; i<L; i++)
            {
                cout << "The " << i+1 << " element (0-100): ";
                cin >> x[i];
                if (x[i]<0||x[i]>100)//不在範圍內
                {
                    cout << "Out of range!" << endl;
                    i--;
                }
            }
        }
        else//不在範圍內
        {
            cout << "Out of range!" << endl;
            cout << "Enter array size (5-20): ";
        }
        cout << "Before sort" << endl;
        for (int i=0; i<L; i++)
            cout << "The " << i+1 << " element: " << x[i] << endl;
        bubblesort(x,L);
        cout << "After sort" << endl;
        for (int i=0;i<L;i++)
            cout << "The " << i+1 << " element: " << x[i] << endl;
            break;
        }
    return 0;
}
