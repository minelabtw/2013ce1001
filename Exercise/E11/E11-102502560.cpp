#include <iostream>
using namespace std;

void bubblesort(int*, int);
void printarr(int [], int);

int main()
{
	int L;
	while(1){
		cout << "Enter array size (5-20): "; cin >> L;					//input size
		if(L<5||L>20){cout << "Out of range!\n"; continue;}
		break;
	}

	int arr[L];	int cnt=0;
	while(cnt<L){
		int tmp;
		cout << "The " << cnt+1 << " element (0-100): "; cin >> tmp;	//input elements
		if(tmp<0||tmp>100){cout << "Out of range!\n"; continue;}
		arr[cnt++]=tmp;
	}

	cout << "Before sort\n";
	printarr(arr,cnt);

	bubblesort(arr,cnt);	//sort

	cout << "After sort\n";
	printarr(arr,cnt);

	return 0;
}

void bubblesort(int* arrptr, int siz)		//normal bubblesort, needless to explain
{
	for(int endidx=siz-2;endidx>=0;endidx--){
		for(int i=0;i<=endidx;i++){
			if(arrptr[i]>arrptr[i+1]){
				int tmp=arrptr[i+1];
				arrptr[i+1]=arrptr[i];
				arrptr[i]=tmp;
			}
		}
	}
}

void printarr(int arr[], int siz)			//print out array
{
	for(int i=0;i<siz;i++){cout << "The " << i+1 << " element: " << arr[i] << "\n";}
}
