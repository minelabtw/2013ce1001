#include <iostream>
using namespace std;

void bubblesort(int* array , int size);
void printArray(int* array , int size);

int main(){
    int inputArray[20] = {};//宣告型別為int元素為20個的輸入陣列，並初始化為0
    int L = 5;//宣告型別為int的陣列長度，並初始化為5

    while(1){
        cout << "Enter array size (5-20): ";
        cin >> L;
        if(L < 5 || L > 20){
            cout << "Out of range!" << endl;
        }
        else{
            break;
        }
    }

    for(int i = 0; i < L;){
        int input = 0;
        cout << "The " << i + 1 << " element (0-100): ";
        cin  >> input;
        if(input < 0 || input > 100){
            cout << "Out of range!" << endl;
        }
        else{
            inputArray[i++] = input;
        }
    }
    cout << "Before sort" << endl;
    printArray(inputArray , L);
    bubblesort(inputArray , L);
    cout << "After sort" << endl;
    printArray(inputArray , L);
    return 0;
}

void bubblesort(int* array , int size){
    for(int i = size; i > 0; i--){
        for(int j = 1; j < i; j++){
            int temp = 0;
            if(array[j - 1] > array[j]){
                temp = array[j - 1];
                array[j - 1] = array[j];
                array[j] = temp;
            }
        }
    }
}

void printArray(int* array , int size){
    for(int i = 0; i < size; i++){
        cout << "The " << i + 1 << " element: " << array[i] << endl;
    }
}
