#include <iostream>
using namespace std;

void bubblesort(int* array , int size);

int main()
{
    int L;//宣告陣列長度

    do//輸入並檢查陣列長度
    {
        cout<<"Enter array size (5-20): ";
        cin>>L;

        if(L<5||L>20)
            cout<<"Out of range!"<<endl;
    }
    while(L<5||L>20);

    int arr[L];//宣告長度L的陣列

    for(int i=0; i<L; i++)//輸入陣列元素並檢查
    {
        do
        {
            cout<<"The "<<i+1<<" element (0-100): ";
            cin>>arr[i];

            if(arr[i]<0||arr[i]>100)
                cout<<"Out of range!"<<endl;
        }
        while(arr[i]<0||arr[i]>100);
    }

    cout<<"Before sort"<<endl;

    for(int i=0; i<L; i++)//印出排序前結果
    {
        cout<<"The "<<i+1<<" element: "<<arr[i]<<endl;
    }

    bubblesort(arr,L);//呼叫bubblesort函式

    cout<<"After sort"<<endl;

    for(int i=0; i<L; i++)//印出排序後結果
    {
        if(i==L-1)
            cout<<"The "<<i+1<<" element: "<<arr[i];
        else
            cout<<"The "<<i+1<<" element: "<<arr[i]<<endl;
    }

    return 0;//結束
}

//實作bubblesort函式每次比較兩個元素，最大的會跑到最後，跑L-1次後即排序完成
void bubblesort(int* array , int size)
{
    int temp;//拿來存較小值

    for(int i=0; i<size-1; i++)
    {
        for(int j=0; j<size-1-i; j++)
        {
            if(array[j]>array[j+1])
            {
                temp=array[j+1];
                array[j+1]=array[j];
                array[j]=temp;
            }
        }
    }
}
