#include <iostream>

using  namespace std;

void bubblesort(int* array , int size);

int main()
{
    int l=0;    //size
    int arr[20];    //array
    //enter size
    while(true)
    {
        cout << "Enter array size (5-20): ";
        cin >> l;
        if(l>20 || l<5)
            cout << "Out of range!" << endl;
        else
            break;
    }
    //enter element
    for(int i=0;i<l;i++)
    {
        while(true)
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> arr[i];
            if(arr[i]>100 || arr[i]<0)
                cout << "Out of range!" << endl;
            else
                break;
        }


    }
    //output array
    cout << "Before sort" << endl;
    for(int i=0;i<l;i++)
    {
        cout <<  "The " << i+1 << " element (0-100): "<< arr[i] << endl;
    }
    //sort
    cout << "After sort" << endl;
    bubblesort(arr ,l);
    //after sort,output array
    for(int i=0;i<l;i++)
    {
        cout <<  "The " << i+1 << " element (0-100): "<< arr[i] << endl;
    }

    return 0;
}

    //function for sort
void bubblesort(int* array , int size)
{
    int t=0;
    for(int j=0;j<size;j++)
        for(int i=1;i<size;i++)
        {
            if(array[i-1] > array[i])
            {
                t=array[i-1];
                array[i-1]=array[i];
                array[i]=t;
            }
        }
}
