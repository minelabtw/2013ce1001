#include <iostream>
#include <iomanip>
#include <algorithm>
#define arrsize 20
using namespace std;
void bubblesort(int* arr , int _size);
int arr[arrsize] = {};//定義陣列
int main()
{
    int _size;//定義整數
    int element;
    cout<<"Enter array size (5-20): ";//輸出
    cin>>_size;//輸入
    while (_size<5||_size>20)//檢驗
    {
        cout<<"Out of range!\n";
        cout<<"Enter array size (5-20): ";
        cin>>_size;
    }

    for (int y=1 ; y<=_size ; y++)
    {
        cout<<"The "<<setw(2)<<y<<" element (0-100): ";
        cin>>element;
        arr[y] = element;
        while (element<0||element>100)//檢驗
        {
            cout<<"Out of range!\n";
            cout<<"The "<<y<<" element (0-100): ";
            cin>>element;
            arr[_size]= 0;
        }
    }

    cout<<"Before sort\n";//輸出
    for (int a=1; a<=_size; a++)
    {
        cout<<"The "<<setw(2)<<a<<" element (0-100): "<<setw(3)<<arr[a]<<endl;
    }
    bubblesort(arr, _size);//呼叫函式
    cout<<"Afer sort\n";//輸出
    for (int b=1; b<=_size; b++)
    {
        cout<<"The "<<setw(2)<<b<<" element (0-100): "<<setw(3)<<arr[b]<<endl;
    }

    return 0;
}

void bubblesort(int* arr , int _size)
{
    for (int i = _size; i > 0; --i)
        for (int j = 0; j < i; ++j)
            if (arr[j] > arr[j + 1])//比大小
                swap(arr[j], arr[j + 1]);//交換位置
}
