#include <iostream>
using namespace std;

void bubblesort(int*,int);

int main()
{
    int L; //arraysize

    do
    {
        cout << "Enter array size (5-20): ";
        cin >> L;
    }
    while ( (L<5 || L>20) && cout << "Out of range!\n" ); //要求輸入arraysize

    int a[L];
    int i=-1; //宣告陣列&i項

    do
    {
        i++;
        cout << "The " << i+1 << " element (0-100): ";
        cin >> a[i];
        if( a[i]<0 || a[i]>100 )
        {
            cout << "Out of range!" <<endl;
            i--;
        }
    }
    while ( i+1<L ); //儲存輸入值

    cout << "Before sort" << endl;

    for ( int j=0; j<L; j++)
    {
        cout << "The " << (j+1) << " element: " << a[j] << endl;
    } //輸出更改前值

    cout << "After sort" << endl;
    bubblesort(a , L); //bubble sort

    for(int i=0; i<L; i++)
        cout << "The " << (i+1) << " element: " << a[i] << endl; //輸出sort之後結果

    return 0;
}

void bubblesort(int* array , int size) //bubble sort
{
    int _size=size;
    for(int i=0; i<size-1; i++)
    {
        for(int j=0,k=j+1; j<_size-1; j++,k++)
        {
            if(array[j]>array[k])
            {
                int hold=array[j];
                array[j]=array[k];
                array[k]=hold;
            }
        }
        _size--;
    }
}
