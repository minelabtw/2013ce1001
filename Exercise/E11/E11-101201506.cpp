#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;

void bubblesort(int* A , int L) //排序
{
    int temp;

    for(int i=0; i<L; i++)
    {
        for(int j=0; j<L; j++)
        {
            if(A[j]>A[j+1])
            {
                temp=A[j];
                A[j]=A[j+1];
                A[j+1]=temp;
            }

        }
    }
}

int main()
{
    int L=0;//陣列大小

    do
    {
        cout<<"Enter array size (5-20): ";
        cin>>L;
        if(L<5 || L>20)
            cout<<"Out of range!"<<endl;
    }
    while(L<5 || L>20);

    int *A;//動態陣列
    A=new int[L];

    for(int i=0 ; i<L ; i++)
    {
        do//確認輸入的範圍
        {
            cout<<"The "<<i+1<<" element (0-100): ";
            cin>>A[i];
            if(A[i]<0 || A[i]>100)
                cout<<"Out of range!"<<endl;
        }
        while(A[i]<0 || A[i]>100);
    }

    cout<<"Before sort"<<endl;

    for(int k=0 ; k<L ; k++)//顯示出還沒排序過的
        cout<<"The "<<k+1<<" element: "<<A[k]<<endl;

    bubblesort(A,L);//傳到 FUNCTION: bubblesort

    cout<<"After sort"<<endl;

    for(int p=0 ; p<L ; p++)//顯示出排序過的
        cout<<"The "<<p+1<<" element: "<<A[p]<<endl;

    return 0;
}
