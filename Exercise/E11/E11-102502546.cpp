#include <iostream>

using namespace std;
void bubblesort(int*,int) ;//宣告函式
int main()
{
    int L ;
    int arr[21] = {} ;//宣告陣列


    cout << "Enter array size (5-20): " ;//確定陣列長度
    cin >> L ;
    while(L<5 || L>20)
    {
        cout << "Out of range!\nEnter array size (5-20): " ;
        cin >> L ;
    }


    for(int i=1 ; i<=L ; i++)//輸入陣列
    {
        cout << "The "<< i << " element (0-100): " ;
        cin >> arr[i] ;
        while(arr[i]<0 || arr[i]>100)
        {
            cout << "Out of range!\n" ;
            cout << "The "<< i << " element (0-100): "  ;
            cin >> arr[i] ;
        }
    }

    cout << "Before sort" << endl ;//顯示每一次輸入的數字
    for(int i=1 ; i<=L ; i++)
    {
        cout << "The "<< i << " element: " << arr[i] << endl ;
    }
    bubblesort(arr,L) ;//排列

    cout << "After sort" << endl ;//顯示排列後的數字
    for(int i=1 ; i<=L ; i++)
    {
        cout << "The "<< i << " element: " << arr[i] << endl ;
    }

    return 0 ;
}

void bubblesort(int *arr , int L)//排列函式
{
    int temp ;
    int a,b ;
    for(a=1 ; a<=L ; a++)
    {
        for(int b=1 ; b<L ; b++)
        {
            if(arr[b]>=arr[b+1])
            {
                temp=arr[b] ;
                arr[b]=arr[b+1] ;
                arr[b+1]=temp ;
            }
        }
    }
}
