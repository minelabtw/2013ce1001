#include <iostream>

using namespace std;

int bubble_sort(int* array , int size); //bubble sort
bool variable_check(double);            //check if input is integer or not

int main()
{
    int _array[20]= {};
    double ArraySize=0;
    do
    {
        cout << "Enter array size (5-20): ";
        cin >> ArraySize;
    }
    while ((variable_check(ArraySize)==1 || ArraySize<5 || ArraySize>20) && cout << "Out of range!" << endl);

    for (int i=0; i<ArraySize; i=i+1)
    {
        do
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> _array[i];
        }
        while ((variable_check(_array[i])==1 || _array[i]<0 || _array[i]>100) && cout << "Out of range!" << endl);
    }
    cout << "Before sort" << endl;
    for (int i=1; i<=ArraySize; i=i+1)
    {
        cout << "The " << i << " element: " << _array[i-1] << endl;
    }

    bubble_sort(_array, static_cast<int>(ArraySize));

    cout << "After sort" << endl;
    for (int i=1; i<=ArraySize; i=i+1)
    {
        cout << "The " << i << " element: " << _array[i-1] << endl;;
    }

    return 0;
}

bool variable_check(double input)
{
    int VariableCheck=input;
    return (static_cast<double>(VariableCheck)!=input);  //return 1 if input is not integer, return 0 if input is integer
}

int bubble_sort(int *_array , int _size)
{
    for (int i=_size-1; i>0; i=i-1)
    {
        for (int j=0; j<i; j=j+1)
        {
            if (_array[j]>_array[j+1])
            {
                int temp=_array[j];
                _array[j]=_array[j+1];
                _array[j+1]=temp;
            }
        }
    }
    return 0;
}
