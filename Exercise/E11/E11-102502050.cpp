#include<iostream>
using namespace std;
void bubblesort(int []  , int );
void outputarray(int []  , int );
int main()
{
    int arraysize;
    do
    {
        cout << "Enter array size (5-20): ";
        cin >> arraysize;
    }
    while((arraysize<5 || arraysize>20)&&cout << "Out of range!\n" );//輸入並檢查
    int number[arraysize];
    for(int i=0; i<arraysize; i++)
        do
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> number[i];
        }
        while((number[i]<0 || number[i]>100)&&cout << "Out of range!\n" );//輸入並檢查
    cout << "Before sort\n";
    outputarray(number,arraysize);
    bubblesort(number,arraysize);
    cout << "After sort\n";
    outputarray(number,arraysize);
    //輸出排序前後，陣列儲存的值
    return 0;
}
void bubblesort(int number[], int arraysize)
{
    int temp;
    for(int i=0; i<arraysize; i++)
    {
        for(int j=1; j<arraysize-i; j++)
            if(number[j-1]>number[j])
            {
                temp=number[j];
                number[j]=number[j-1];
                number[j-1]=temp;
                //將number[j-1]和number[j]的值互換
            }
    }
}//泡泡排序
void outputarray(int number[], int arraysize)
{
    for(int i=0; i<arraysize; i++)
        cout << "The " << i+1 << " element : " << number[i] << endl;
}//輸出陣列
