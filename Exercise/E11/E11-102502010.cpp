#include <iostream>
using namespace std;

void bubblesort(int* arr , int _size)
{
    int i,j;
    for(i=0; i<_size; i++)
        for(j=i+1; j<_size; j++)
            if(arr[i]>arr[j])
                swap(arr[i],arr[j]);
}

int main()
{
    int a[20],n,i;
    cout<<"Enter array size (5-20): ";
    cin>>n;
    while(n<5 || n>20)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter array size (5-20): ";
        cin>>n;
    }
    for(i=0; i<n; i++)
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        cin>>a[i];
        while(a[i]<0 || a[i]>100)
        {
            cout<<"Out of range!"<<endl;
            cout<<"The "<<i+1<<" element (0-100): ";
            cin>>a[i];
        }
    }
    cout<<"Before sort"<<endl;
    for(i=0; i<n; i++)
        cout<<"The "<<i+1<<" element: "<<a[i]<<endl;
    bubblesort(a,n);
    cout<<"After sort"<<endl;
    for(i=0; i<n; i++)
        cout<<"The "<<i+1<<" element: "<<a[i]<<endl;
    return 0;
}
