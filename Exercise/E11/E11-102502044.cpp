#include <stdio.h>
#include <string.h>

void bubblesort(int* array , int size);

int check_size(int x);
int check_num(int x);

void input(const char *str, int *tmp, int (*check)(int));

int main()
{
	int num[20];
	int size;

	input("Enter array size (5-20): ", &size, check_size);

	for(int i=0 ; i<size ; i++)
	{
		char str[30];
		sprintf(str,"The %d element (0-100): ", i+1);
		input(str, num+i, check_num);
	}

	puts("Before sort");
	for(int i=0 ; i<size ; i++)
		printf("The %d element: %d\n", i+1, num[i]);

	bubblesort(num, size);

	puts("After sort");
	for(int i=0 ; i<size ; i++)
		printf("The %d element: %d\n", i+1, num[i]);

	return 0;
}

int check_size(int x)
{
	if(5<=x && x<=20)
		return 1;
	return 0;
}

int check_num(int x)
{
	if(0<=x && x<=100)
		return 1;
	return 0;
}

void bubblesort(int* array , int size)
{
	int count[101];
	int a = 0;

	memset(count, 0, sizeof(count));

	for(int i=0 ; i<size ; i++)
		count[array[i]]++;

	for(int i=0 ; i<=100 ; i++)
		for(int j=0 ; j<count[i] ; j++)
			array[a++] = i;
}

void input(const char *str, int *tmp, int (*check)(int))
{
	for(;;)
	{
		printf(str);
		scanf("%d", tmp);
		if(check(*tmp))
			break;
		puts("Out of range!");

	}
}
