#include<iostream>
using namespace std;

void bubblesort(int* _array , int _size);

int main(void)
{
    int _size;
    int arr[20] = {};

    do    //輸入陣列大小
    {
        cout << "Enter array size (5-20): ";
        cin >> _size;
        if(_size<5 || _size>20)
            cout << "Out of range!" << endl;
    }
    while(_size<5 || _size>20);
    for(int i=0; i<_size; i++)           //輸入欲排列的數值
    {
        do
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> arr[i];
            if(arr[i]<0 || arr[i]>100)
                cout << "Out of range!" << endl;
        }while(arr[i]<0 || arr[i]>100);
    }
    cout << "Before sort" << endl;  //輸出未經 bubblesort的數值
    for(int j=0; j<_size; j++)
    {
        cout << "The "<< j+1 <<" element: " << arr[j] << endl;
    }
    bubblesort(arr,_size);
    cout << "After sort" << endl;     //輸出經過了 bubblesort的數值
    for(int j=0; j<_size; j++)
    {
        cout << "The "<< j+1 <<" element: " << arr[j] << endl;
    }
    return 0;
}

void bubblesort(int* _array , int _size)         //泡沫排列
{
    int i, j, temp;
    for (i = _size - 1; i > 0; i--)
    {
        for (j = 0; j <= i - 1; j++)
        {
            if (_array[j] > _array[j + 1])
            {
                temp = _array[j];
                _array[j] = _array[j + 1];
                _array[j + 1] = temp;
            }
        }
    }
}
