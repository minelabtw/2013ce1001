#include <iostream>
using namespace std;

void bubblesort(int* , int);

int main ()
{
    int Size;//陣列之大小
    int array[20];//宣告陣列,最大size為20

    cout << "Enter array size (5-20): ";
    cin >> Size;
    while (Size < 5 || Size > 20)
    {
        cout << "Out of range!" << endl;
        cout << "Enter array size (5-20): ";
        cin >> Size;
    }//輸入size若不再範圍內可重新輸入

    for (int i = 0; i < Size; i++)
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin >> array[i];
        while (array[i] < 0 || array[i] > 100)
        {
            cout << "Out of range!" << endl;
            cout << "The " << i+1 << " element (0-100): ";
            cin >> array[i];
        }
    }//輸入陣列元素

    cout << "Before sort" << endl;
    for (int i = 0; i < Size; i++)
    {
        cout << "The " << i+1 << " element (0-100): " << array[i] << endl;
    }//排列前先輸出

    cout << "After sort" << endl;
    bubblesort (array, Size);
   //泡沫排序後再輸出

    return 0;
}

void bubblesort(int* array , int _size)
{
    int temp;
    for(int i = 0; i < _size; i++)
    {
        for(int j = i; j < _size; j++)
        {
            if (array[i] > array[j+1]) //若前一個數字比後面數字大則交換
            {
                temp=array[i]; //以下三行為交換
                array[i]=array[j+1];
                array[j+1]=temp;
            }
        }
    }
     for (int i = 0; i <_size; i++)
    {
        cout << "The " << i+1 << " element (0-100): " << array[i] << endl;
    }
}
