#include <iostream>

using namespace std;

void Bubblesort(int* array , int size);  //泡泡排序法

int main(){
    int length;  //陣列長度
    int element[20] = {};  //數字陣列

    do{  //輸入陣列的長度
        cout << "Enter array size (5-20): ";
        cin >> length;
        if(length < 5 || length > 20){
            cout << "Out of range!" << endl;
        }
    }while(length < 5 || length > 20);  //超出範圍就重輸

    for(int i = 0 ; i < length ; i++){  //輸入數字並存入陣列
        do{
            cout << "The " << i + 1 << " element (0-100): ";
            cin >> element[i];
            if(element[i] < 0 || element[i] > 100){
                cout << "Out of range!" << endl;
            }
        }while(element[i] < 0 || element[i] > 100);  //不合條件就重新輸入
    }

    cout << "Before sort" << endl;  //輸出排序前

    for(int i = 0 ; i < length ; i++){  //輸出陣列裡的數字
        cout << "The " << i + 1 << "element (0-100): " << element[i] << endl;
    }

    Bubblesort(element , length);  //排序

    cout << "After sort" << endl;  //輸出排序後

    for(int i = 0 ; i < length ; i++){  //輸出排序後的陣列
        cout << "The " << i + 1 << "element (0-100): " << element[i] << endl;
    }

    return 0;
}

void Bubblesort(int* array , int size){  //泡泡排序法
    for(int i = size ; i > 0 ; i--){
        for(int j = i - 1 ; j >= 0  ; j--){
            if(array[j] > array[i - 1]){  //如果比目前大就交換
                int temp = array[j];
                array[j] = array[i - 1];
                array[i - 1] = temp;
            }
        }
    }
}
