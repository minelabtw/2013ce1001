#include <iostream>
using namespace std;
void bubblesort(int* array , int size)
{
    int temp;
    for(int h=size; h>1; h--)                   //迴圈 size-1次  (找最大size-1次)
    {
        for(int j=0; j<h-1; j++)                //找最大 從arr[0] 找到 arr[h] 比較h-1次  每次 比上一次比較少1次
        {
            if(array[j]>array[j+1])             //若比下一個大 則 替換
            {
                temp=array[j];
                array[j]=array[j+1];
                array[j+1]=temp;
            }
        }
    }
}
int main()
{
    int arrsize;
    do
    {
        cout<<"Enter array size (5-20): ";
        cin>>arrsize;
        if(arrsize<5 || arrsize>20)                 //超出範圍則重新輸入
            cout<<"Out of range!"<<endl;
    }
    while(arrsize<5 || arrsize>20);
    int arr[arrsize];                               //宣告陣列
    for(int j=0; j<arrsize; j++)                    //輸入陣列各項值
    {
        do
        {
            cout<<"The "<<j+1<<" element (0-100): ";
            cin>>arr[j];
            if(arr[j]>100 || arr[j]<0)              //超出範圍則重新輸入
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(arr[j]>100 || arr[j]<0);
    }
    cout<<"Before sort"<<endl;
    for(int j=0; j<arrsize; j++)                    //列印陣列
    {
        cout<<"The "<<j+1<<" element: "<<arr[j]<<endl;
    }
    cout<<"After sort"<<endl;
    bubblesort(arr,arrsize);                        //排列
    for(int j=0; j<arrsize; j++)                    //列印陣列
    {
        cout<<"The "<<j+1<<" element: "<<arr[j];
        if(j!=arrsize-1)
        {
            cout<<endl;
        }
    }
    return 0;
}
