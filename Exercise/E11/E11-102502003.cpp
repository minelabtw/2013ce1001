#include<iostream>
using namespace std;


void bubblesort(int* Array, int Size);

int main()
{
    int enter=0;
    int data[20];  //陣列大小為20

    while(enter>20||enter<5)  //限制輸入陣列大小
    {
        cout<<"Enter array size (5-20): ";
        cin>>enter;
        if(enter>20||enter<5)
            cout<<"Out of range!"<<endl;
    }

    for(int i=0; i<enter; i++)  //輸出需排序項目
    {
        do  //限制陣列中的數字範圍
        {
            cout<<"The "<<i+1<<" element (0-100): ";
            cin>>data[i];
            if(data[i]<0||data[i]>100)
                cout<<"Out of range!"<<endl;
        }
        while(data[i]<0||data[i]>100);

    }


    cout<<"Before sort"<<endl;

    for(int i=0; i<enter; i++)
    {
        cout<<"The "<<i+1<<" element: "<<data[i]<<endl;
    }

    cout<<"After sort"<<endl;

    bubblesort(data,enter);  //執行氣泡排序法

    for(int i=0; i<enter; i++)  //輸出結果
    {
        cout<<"The "<<i+1<<" element: "<<data[i]<<endl;
    }


    return 0;

}

void bubblesort(int* Array, int Size)  //按數字大小排序
{
    int i=0;
    int j=0;
    int temp=0;

    for(i=Size-1; i>0; i--)
    {
        for(j=0; j<Size; j++)
        {
            if(Array[j]>Array[j+1])  //若前項大於後項、交換
            {
                temp=Array[j];
                Array[j]=Array[j+1];
                Array[j+1]=temp;

            }
        }
    }

}
