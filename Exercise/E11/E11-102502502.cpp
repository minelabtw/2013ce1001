#include<iostream>

using namespace std;

void bubblesort(int* len, int L)
{
    int i,j,h;
    for (i = L; 0 < i; i--)                                               //i表示計算的次數
    {
        for (j = 1; j <= L; j++)                                          //j表示數字的位置
        {
            if (len[j] > len[j+1])                                        //若前面數字大於下一個數字，則前後互換
            {
                h = len[j];
                len[j] = len[j+1];
                len[j+1] = h;
            }
        }
    }
}

int main()
{
    int i,e,L;
    int len[21];

    cout << "Enter array size (5-20): ";
    while(cin >> L)
    {
        if (L < 5 || L > 20)
            cout << "Out of range!\n" << "Enter array size (5-20): ";
        else
            break;
    }
    for (i = 1; i <= L; i++)
    {
        cout << "The " << i << " element (0-100): ";
        while(cin >> e)
        {
            if (e < 0 || e > 100)
                cout << "Out of range!\n" << "The " << i << " element (0-100): ";
            else
                break;
        }
        len[i] = e;
    }
    cout << "Before sort\n";                                            //輸出排序前的樣子
    for (i = 1; i <= L; i++)
    {
        cout << "The " << i << " element (0-100): " << len[i] << endl;
    }
    cout << "After sort\n";                                             //輸出排序後的樣子
    bubblesort(len, L);
    for (i = 1; i <= L; i++)
    {
        cout << "The " << i << " element (0-100): " << len[i] << endl;
    }
    return 0;
}
