#include <iostream>
#include <cstdlib>
using namespace std;
void bubblesort(int* array , int size);
int main()
{
    int L=0;
    cout<<"Enter array size (5-20): ";
    cin >>L;
    while (L<5||L>20)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter array size (5-20): ";
        cin >>L;
    }
    int array[21]={};//陣列最多就21個
    int a,b,c;//a是輸入幾個數 b是輸入的數 c是印出幾個其值為a
    for (a=1 ; a<=L; a++)
    {
        cout<<"The "<<a<<" element (0-100): ";//a是計數器
        cin>>b;//b是存入輸入的數
        while (b<0||b>100)
        {
        cout<<"Out of range!"<<endl;
        cout<<"The "<<a<<" element (0-100): ";
        cin>>b;
        }
        array[a]=b;
    }
    cout<<"Before sort"<<endl;
    for (c=1; c<a; c++)//用<因為a假設輸入五個 最後離開for迴圈時a的5值是6
    {
    cout<<"The "<<c<<" element: "<<array[c]<<endl;//c是另一個計數器 負責讓它印出a個
    }
    cout<<"After sort"<<endl;
    bubblesort( array ,a);//array本身就是一個位址（自己試過）
    for (c=2; c<=a; c++)//用<因為a假設輸入五個 最後離開for迴圈時a的5值是6
    {
        cout<<"The "<<c-1<<" element: "<<array[c]<<endl;//c是另一個計數器 負責讓它印出a個
    }

    
}

  void bubblesort(int* array , int size)//size就是你原本輸入幾個
{
    for (int counter=0;counter<size; counter++)
    {
        for (int i=1; i<size; i++)
        {
            if (array[i]>array[i+1])
            {
                int s=array[i+1];//s是暫存
                array[i+1]=array[i];
                array[i]=s;
            }
        }
    }
}