#include<iostream>
using namespace std ;
void bubblesort(int*,int) ;//宣告函式
int main()
{
    int a=0 ;//宣告變數
    int b=0 ;
    const int arraysize = 20 ;//宣告陣列大小
    int n[arraysize] = {} ;

    cout << "Enter array size (5-20): " ;//輸出題目所需
    cin >> a ;
    while(a<5 or a>20)
    {
        cout << "Out of range!" << endl << "Enter array size (5-20): " ;
        cin >> a ;
    }
    for (int i=1; i<=a; i++)
    {
        cout << "The " <<  i << " element (0-100): " ;
        cin >> b ;
        while (b<0 or b>100)
        {
            cout << "Out of range!" << endl << "The " <<  i << " element (0-100): " ;
            cin >> b ;
        }
        n[i]=b ;//將輸入的數依序儲存在陣列裡
    }
    cout << "Before sort" << endl ;
    for (int i=1; i<=a; i++)
    {
        cout << "The " << i << " element: " << n[i] << endl ;
    }
    cout << "After sort" << endl ;
    bubblesort(n,a) ;//呼叫函式
    for (int i=1; i<=a; i++)
    {
        cout << "The " << i << " element: " << n[i] << endl ;
    }
    return 0 ;
}
void bubblesort(int*n1,int a1)//函式將所輸入的數依小到大重新排列後回傳
{
    int x=0 ;
    for(int j=1; j<a1; j++)
    {
        for(int i=1; i<a1; i++)
        {
            if (n1[i]>n1[i+1])
            {
                x=n1[i];
                n1[i]=n1[i+1];
                n1[i+1]=x;
            }
        }
    }
}

