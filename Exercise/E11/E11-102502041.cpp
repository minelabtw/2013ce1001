#include<iostream>
using namespace std;
void Bubblesort(int *input , int ArraySize)
{
    for(int i=ArraySize-1,sp; i>0; i--)
    {
        sp=1;
        for(int j=0,Temp; j<=i; j++)    //宣告一個變數Temp來當作排序過程中交換時暫存值得空間
            if(input[j]>input[j+1])
            {
                Temp=input[j];
                input[j]=input[j+1];
                input[j+1]=Temp;
                sp=0;
            }
        if(sp==1)break;
    }
}
void printarray(int *input, int ArraySize)
{   //將陣列中每個元素都輸出
    for(int i=0,j=1; i<ArraySize; i++,j++)cout<<"The "<<j<<" element: "<<input[i]<<endl;
}

int main()
{
    int ArraySize;
    while(1)
    {
        cout<<"Enter array size (5-20): ";
        cin>>ArraySize;
        if(ArraySize<5||ArraySize>20)cout<<"Out of range!"<<endl;
        else break;
    }
    int input[ArraySize];
    for(int i=0,j=1; i<ArraySize; i++,j++)
    {
        while(1)
        {
            cout<<"The "<<j<<" element (0-100): ";
            cin>>input[i];
            if(input[i]<0||input[i]>100)cout<<"Out of range!"<<endl;
            else break;
        }
    }
    cout<<"Before sort"<<endl;
    printarray(input,ArraySize);
    cout<<"After sort"<<endl;
    Bubblesort(input,ArraySize);
    printarray(input,ArraySize);
    return 0;
}

