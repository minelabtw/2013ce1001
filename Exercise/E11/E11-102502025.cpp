#include <iostream>
using namespace std;
void bubblesort(int * , int);       //令進行泡沫運算方程
int main()
{
    int Size=0;     //範圍
    int Array[20];      //集合
    int input=-1;       //輸入集合值
    while(Size<5 || Size>20)       //範圍迴圈
    {
        cout << "Enter array size (5-20): ";
        cin >> Size;
        if(Size<5 || Size>20)
        {
            cout << "Out of range!\n";
        }
    }
    for(int k=0; k<Size; k++)       //輸入與記錄子集
    {
        while(input<0 || input>100)
        {
            cout << "The " << k << " element (0-100): ";
            cin >> input;
            if(input<0 || input>100)
            {
                cout << "Out of range!\n";
            }
        }
        Array[k]=input;
        input=-1;
    }
    cout << "Before sort\n";
    for(int l=0; l<Size; l++)       //輸出原集合
    {
        cout << "The " << l+1 << " element: " << Array[l] << endl;
    }
    cout << "After sort\n";
    bubblesort(Array,Size);     //方程運算
    for(int b=0; b<Size; b++)       //輸出後集合
    {
        cout << "The " << b+1 << " element: " << Array[b] << endl;
    }
    return 0;
}
void bubblesort(int Array[],int Size)
{
    for(int m=Size; m>0; m--)
    {
        for(int n=0; n<Size-1; n++)
        {
            if(Array[n]>Array[n+1])
            {
                swap(Array[n],Array[n+1]);      //換位
            }
        }
    }
}
