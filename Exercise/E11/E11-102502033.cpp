#include<iostream>
using namespace std;
int sor(int[],int);//bubble prototype
int main()
{
    int L=0;//arraysize
    int element=0;//value of array


    cout << "Enter array size (5-20): ";
    cin  >> L;
    while(L<5 || L>20)//loop of out of range
    {
        cout << "Out of range!\n";
        cout << "Enter array size (5-20): ";
        cin  >> L;
    }
    int arr[L];//int array
    arr[L]={};//initialize (should seperate to int)

    for(int i=0; i<=L-1; i++)//save the value into array
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin  >> element;
        while(element<0 || element >100)
        {
            cout << "Out of range!\n";
            cout << "The " << i+1 << " element (0-100): ";
            cin  >> element;
        }
        arr[i]=element;
    }
    cout << "Before sort\n";
    for(int i=0; i<=L-1; i++)//show the original array
    {
        cout << "The " << i+1 << " element (0-100): " << arr[i] << "\n";
    }
    cout << "After sort\n";
    sor(arr,L);
    for(int i=0; i<=L-1; i++)//show the modified array
    {
        cout << "The " << i+1 << " element (0-100): " << arr[i] << "\n";
    }



    return 0;
}
int sor(int b[],int arraysize)
{
    int save=0;
    for(int i=1; i<arraysize; i++)//to make it do the "precise time" of the follow loop
    {
        for(int i=0; i<arraysize-1; i++)//From first to end. Compare all of them, two by two.
        {
            if(b[i]>b[i+1])
            {
                save=b[i+1];
                b[i+1]=b[i];
                b[i]=save;
            }
        }
    }
}

