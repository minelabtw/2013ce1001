#include <iostream>
using namespace std;
void bubblesort(int *,int );    //泡泡排序~
void output(int *,int );    //印出陣列~
int main()
{
    ios::sync_with_stdio(0);
    int L,arr[20];  //宣告變數~
    cout<<"Enter array size (5-20): ";
    while(cin>>L&&(L<5||L>21))
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter array size (5-20): ";
    }
    for(int i=0;i<L;++i)
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        while(cin>>arr[i]&&(arr[i]<0||arr[i]>101))
        {
            cout<<"Out of range!"<<endl;
            cout<<"The "<<i+1<<" element (0-100): ";
        }
    }
    cout<<"Before sort"<<endl;
    output(arr,L);
    bubblesort(arr,L);
    cout<<"After sort"<<endl;
    output(arr,L);
    return 0;
}
void bubblesort(int *arr,int siz)
{
    for(int i=0;i<siz;++i)
    {
        for(int j=i+1;j<siz;++j)
        {
            if(arr[i]>arr[j])
            {
                int tmp=arr[i];
                arr[i]=arr[j];
                arr[j]=tmp;
            }
        }
    }
}
void output(int *arr,int siz)
{
    for(int i=0;i<siz;++i)
    {
        cout<<"The "<<i+1<<" element: "<<arr[i]<<endl;
    }
}
