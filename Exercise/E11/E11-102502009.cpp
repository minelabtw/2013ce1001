#include<iostream>
using namespace std;
void bubblesort(int _array[20] , int _size)
{
    int tmp;
    cout<<"After sort"<<endl;
    for (int i=0; i<_size; i++)
    {
        for (int j=0; j<_size; j++)
        {
            if (_array[j]>_array[j+1])
            {
                tmp=_array[j];
                _array[j]=_array[j+1];
                _array[j+1]=tmp;
            }
        }
    }
    for(int i=0; i<_size; i++)
    {
        cout<<"The "<<i+1<<" element: "<<_array[i]<<endl;
    }
}
int main()
{
    int Size=0;
    int Array[20];
    do
    {
        cout<<"Enter array size (5-20): ";
        cin>>Size;
        if(Size<5 or Size>20)
            cout<<"Out of range!"<<endl;
    }
    while(Size<5 or Size>20);
    for(int i=0; i<Size; i++)
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        cin>>Array[i];
        if(Array[i]<0 or Array[i]>100)
        {
            cout<<"Out of range!"<<endl;
            i--;
            continue;
        }
    }
    cout<<"Before sort"<<endl;
    for(int i=0; i<Size; i++)
    {
        cout<<"The "<<i+1<<" element: "<<Array[i]<<endl;
    }
    bubblesort(Array ,Size);
    return 0;
}
