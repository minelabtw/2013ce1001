#include <iostream>

using namespace std ;

int bubblesort ( int* arr , int length ) ;  //宣告函式。

int main ()
{
    int length = 0 ;
    int number = 0 ;

    cout << "Enter array size (5-20): " ;
    cin >> length ;

    while ( length < 5 || length > 20 )
    {
        cout << "Out of range!" << endl << "Enter array size (5-20): " ;
        cin >> length ;
    }  //判斷輸入是否合理。

    int arr [ length ] ;

    for ( int i = 1 ; i <= length ; i ++ )
    {
        cout << "The " << i << " element (0-100): " ;
        cin >> number ;

        while ( number < 0 || number > 100 )
        {
            cout << "Out of range!" << endl << "The " << i << " element (0-100): " ;
            cin >> number ;
        }

        arr [i] = number ;
    }  //使輸入各項之數字，並判斷其合理性，再傳回陣列中。。

    cout << "Before sort" << endl ;

    for ( int i = 1 ; i <= length ; i ++ )
    {
        cout << "The " << i << " element: " << arr [ i ] << endl ;
    }  //輸出排列前順序。

    cout << "After sort" << endl ;

    bubblesort ( arr , length ) ;  //用函式作排列。

    for ( int i = 1 ; i <= length ; i ++ )
    {
        cout << "The " << i << " element: " << arr [ i ] << endl ;
    }  //輸出排列後結果。

    return 0 ;
}

int bubblesort ( int *arr , int length )
{
    for ( int i = length - 1 ; i > 0 ; i --  )
    {
        for ( int j = 1 ; j <= i ; j ++ )
        {
            if  ( arr [ j ] > arr [ j + 1 ] )
                swap ( arr  [ j ] , arr [ j + 1 ] ) ;
        }
    }
    return arr [ length ] ;
}  //定義函式：將陣列中各數值作由大到小之排列，再傳回原數列。
