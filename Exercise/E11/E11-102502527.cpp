#include <iostream>

using namespace std;

void bubblesort(int* array,int size);                                  //宣告函式

int main()
{
    int number = 0;
    int array[20];

    cout << "Enter array size (5-20): ";                        //輸入陣列大小，並判斷是否符合範圍
    while(cin >> number)
    {
        if ( number < 5 || number > 20 )
        {
            cout << "Out of range!" << endl;
            cout << "Enter array size (5-20): ";
        }
        else
            break;
    }

    for ( int i = 1 ; i <= number ; i++ )                                    //輸入數字進陣列，並判斷是否符合範圍
    {
        cout << "The " << i << " element (0-100): ";
        while( cin >> array[i] )
        {
            if ( array[i] < 0 || array[i] > 100 )
            {
                cout << "Out of range!" << endl;
                cout << "The " << i << " element (0-100): ";
            }
            else
                break;
        }
    }

    cout << "Before sort" << endl;                              //輸出排列前之陣列
    for ( int i = 1 ; i <= number ; i++ )
    {
        cout << "The " << i << " element: " << array[i] << endl;
    }

    bubblesort(array,number);//叫出泡泡函示

    cout << "After sort" << endl;                               //輸出排列後之陣列
    for ( int i = 1 ; i <= number ; i++ )
    {
        cout << "The " << i << " element: " << array[i] << endl;
    }

    return 0;
}

void bubblesort(int* array,int number)                                   //泡泡函示排序
{
    for (int j = 1 ; j <= number ; j++ )
    {
        for (int i = 1 ; i <= number ; i++ )
        {
            int a = 0;
            if ( array[i] > array[i+1] )
            {
                a = array[i];
                array[i] = array[i+1];
                array[i+1] = a;
            }

        }
    }
}
