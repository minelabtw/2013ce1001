#include <iostream>
using namespace std;
// bubblesort
void bubblesort(int *arr, int _size)
{
    for (int i=0;i<_size;i++)
        for (int j=1;j<_size-i;j++)
            if (arr[j-1] > arr[j])
            {
                int tmp = arr[j-1];
                arr[j-1] = arr[j];
                arr[j] = tmp;
            }
}
int main(int argc, char *argv[])
{
    int *arr = 0, _size = 0; // a pointer point to an array
    // input size
    do
    {
        cout << "nter array size (5-20): ";
        cin >> _size;
    } while ((_size<5 || _size>20) && cout << "Out of range!" << endl);

    arr = new int[_size]; // create array
    // input each element
    for (int i = 0; i != _size; i++)
        do
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> arr[i];
        } while ((arr[i]<0 || arr[i]>100) && cout << "Out of range!" << endl);

    cout << "Before sort" << endl;
    for (int i = 0; i != _size; i++)
        cout << "The " << i+1 << " element: " << arr[i] << endl;

    bubblesort(arr,_size); // call bubble sort

    cout << "After sort" << endl;
    for (int i = 0; i != _size; i++)
        cout << "The " << i+1 << " element: " << arr[i] << endl;
    // clean up
    delete[] arr;
    arr = 0;
    return 0;
}
