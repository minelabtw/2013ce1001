#include <iostream>
using namespace std;
void bubblesort(int* arr , int ArraySize);//由小排到大的函式
int main()
{
    int ArraySize;
    do//輸入陣列長度並判斷在範圍內
    {
        cout<<"Enter array size (5-20): ";
        cin>>ArraySize;
        if (ArraySize<5 or ArraySize>20)
            cout<<"Out of range!"<<endl;
    }
    while (ArraySize<5 or ArraySize>20);
    int arr[ArraySize];
    int i=0;
    do//輸入L個介於0到100的正整數並判斷在範圍內
    {
        cout<<"The 1 element (0-100): ";
        cin>>arr[i];
        if (arr[i]<0 or arr[i]>100)
            cout<<"Out of range!"<<endl;
        else i++;
    }
    while (i<ArraySize);

    cout<<"Before sort"<<endl;
    for(int a=0; a<ArraySize; a++)//輸出陣列內容
        cout<<"The "<<a+1<<" element: "<<arr[a]<<endl;
    cout<<"After sort"<<endl;
    for(int a=0; a<ArraySize; a++)//輸出排列後的陣列內容
    {
        bubblesort(arr,ArraySize);
        cout<<"The "<<a+1<<" element: "<<arr[a]<<endl;
    }
    return 0;
}
void bubblesort(int* arr , int ArraySize)
{
    for(int i=ArraySize; i>0; i--)//使用迴圈排列大小
    {
        if (arr[i]<arr[i-1])
        {
            int t = arr[i];
            arr[i] = arr[i-1];
            arr[i-1] = t;
        }
    }
}
