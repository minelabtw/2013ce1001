#include <iostream>

using namespace std;

void b(int * L,int as)   //bubblesort function
{
    int temp;  //暫時儲存變數用以交換L[i]、L[i+1]
    for(int bbb=1; bbb<=as; bbb++)//讓下面那個for重複跑as次
    {
        for(int blabla=1; blabla<=as; blabla++)
        {
            if (L[blabla]>L[blabla+1])
            {
                temp=L[blabla];
                L[blabla]=L[blabla+1];
                L[blabla+1]=temp;
            }
        }
    }
}
int main()
{
    int as;//arraysize
    int L[21];//陣列的長度
    int a;//輸入值
    cout<<"Enter array size(5-20): ";
    cin>>as;
    while(as<5||as>20)
    {
        cout<<"Out of range!"<<endl<<"Enter array size(5-20): ";
        cin>>as;
    }

    for(int i=1; i<=as; i++)
    {
        cout<<"The "<<i<<" element (0-100): ";
        cin>>a;
        while(a<0||a>100)
        {
            cout<<"Out of range!"<<endl<<"The"<<i<<" element (0-100): ";
            cin>>a;
        }
        L[i]=a;
    }

    cout<<"Before sort"<<endl; //照順序輸出element
    for(int i2=1; i2<=as; i2++)
    {
        cout<<"The "<<i2<<" element: "<<L[i2]<<endl;
    }

    b(L,as);

    cout<<"After sort"<<endl;  //照大小輸出element
    for(int i3=1; i3<=as; i3++)
    {
        cout<<"The "<<i3<<" element: "<<L[i3]<<endl;
    }
    return 0;
}
