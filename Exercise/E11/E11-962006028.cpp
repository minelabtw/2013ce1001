#include<iostream>

using namespace std ;

int main()
{
    int L ,num ;   //設定使用變數
    cout <<"Enter array size (5-20): " ;
    cin >> L ;
    while (L<5||L>20)    //判斷範圍
    {
        cout<<"Out of range!"<<endl;
        cout <<"Enter array size (5-20): " ;
        cin >> L ;
    }

    int array [L];   //設定陣列
    int x=0 ;
    do
    {
        cout<<"The "<< x+1 << " element (0-100): " ;
        cin >>num ;
        while(num<0||num>100)     //判斷範圍
        {
            cout<<"Out of range!"<<endl;
            cout<<"The "<< x+1 << " element (0-100): " ;
            cin >>num ;
        }
        array [x] = num ;    //放入陣列
        x++;
    }
    while (x<L);

    cout<<"Before sort"<<endl ;     //輸出數字順序
    for (int i=0 ; i<L ; i++)
    {
        cout <<"The " <<i+1 <<" element: "<< array[i] <<endl ;
    }

    cout <<"After sort"<<endl ;
    int y=0;
    for (int i=0 ; i<L-1 ; i++)    //排列數字大小
    {
        int k=0 ;
        while(1)
        {
            if (array[k]>array[k+1])
            {
                y=array[k+1];
                array[k+1]=array[k];
                array[k]=y;
            }
            if (k<L-i )
            {
                k++;
            }
            else if(k>=L-i )
                break ;
        }
    }

    for (int i=0 ; i<L ; i++)   //輸出變更後數字
    {
        cout <<"The " <<i+1 <<" element: "<< array[i] <<endl ;
    }

    return 0 ;
}
