#include<iostream>
using namespace std;

void bubblesort(int arr[] , int Size)//氣泡排序法排列輸入的數字
{
    int exchange=Size-1;
    while(exchange)
    {
        int bound=exchange;
        exchange=0;
        for(int i=0; i<bound; i++)
        {
            if(arr[i]>arr[i+1])
            {
                int a=0;
                a=arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=a;
                exchange=i;
            }
        }
    }
}
int main()
{
    int Size=0;//宣告型別為整數的Size並初始化其值為0
    do//讓使用者輸入介於5-20的陣列長度
    {
        cout << "Enter array size (5-20): ";
        cin >> Size;
    }
    while((Size<5 || Size>20)&&cout << "Out of range!\n");
    int arr[Size];//宣告長度為Size的陣列
    for(int i=0; i<Size; i++)//讓使用者依序輸入數字
    {
        do
        {
            cout << "The "<< i+1 << " element (0-100): ";
            cin >> arr[i];
        }
        while((arr[i]<0 || arr[i]>100)&&cout << "Out of range!\n");
    }
    cout << "Before sort\n";
    for(int i=0; i<Size; i++)//按照使用者輸入的順序輸出輸入的數字
    {
        cout << "The " << i+1 << " element: " << arr[i] << endl;
    }
    bubblesort(arr,Size);//排列數字
    cout << "After sort\n";
    for(int i=0; i<Size; i++)//輸出排列好的數字
    {
        cout << "The " << i+1 << " element: " << arr[i] << endl;
    }

    return 0;
}
