#include<iostream>

using std::cout;
using std::cin;
using std::endl;
void Print(int arr[],int _size)
{
    for (int i=0; i<_size; ++i)
    {
        cout << "The " << i+1 << " element: " << arr[i] << endl ;
    }
}
void bubblesort(int *arr , int _size)
{
    int temp ;
    for (int i=0; i<_size; ++i)
    {
        for (int j=0; j<_size; j++)
        {
            if (arr[j]>arr[j+1])
            {
                temp=arr[j] ;
                arr[j]=arr[j+1] ;
                arr[j+1]=temp ;
            }
        }
    }
}
int main()
{
    int _size ;
    do
    {
        cout << "Enter array size (5-20): " ;
        cin >> _size ;
        if (_size<5 || _size>20)
            cout << "Out of raqne!" ;
    }
    while (_size<5 || _size>100) ;
    int arr[20] ;
    for (int i=0; i<_size ; ++i)
    {
        do
        {
            cout << "The " << i+1 << " element (0-100): " ;
            cin >> arr[i] ;
            if (arr[i]<0 ||arr[i]>100)
                cout << "Out of range!" ;
        }
        while (arr[i]<0 || arr[i]>100) ;
    }
    cout << "Before sort" << endl ;
    Print(arr,_size);
    bubblesort(arr,_size) ;
    cout << "After sort" << endl ;
    Print(arr,_size) ;
    return 0;
}


