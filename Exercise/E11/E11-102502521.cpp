#include <iostream>

using namespace std;

int bubblesort(int* arr , int _size);    //prototype

int main()
{
    int arr[20]= {};    //設定變數和陣列
    int _size=0;
    int ele=0;
    int t=1;

    do    //判斷合理
    {
        cout<<"Enter array size (5-20): ";
        cin>>_size;
        if(_size<5||_size>20)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(_size<5||_size>20);

    while(_size>=t)    //輸入值
    {
        do
        {
            cout<<"The "<<t<<" element (0-100): ";
            cin>>ele;
            if(ele>100||ele<0)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(ele>100||ele<0);

        arr[t-1]=ele;

        t++;
    }

    cout<<"Before sort"<<endl;

    t=1;

    while(_size>=t)    //顯示值
    {
        cout<<"The "<<t<<" element: "<<arr[t-1]<<endl;
        t++;
    }

    bubblesort(arr,_size);

    t=1;

    cout<<"After sort"<<endl;

    while(_size>=t)    //顯示值
    {
        cout<<"The "<<t<<" element: "<<arr[t-1]<<endl;
        t++;
    }

    return 0;
}

int bubblesort(int* arr , int _size)
{
    int first=0;    //設定變數
    int num=1;
    int change=0;

    while(_size>num)    //重頭做交換
    {
        while(_size>first+1)    //每個檢查並交換
        {
            if(arr[first]>=arr[first+1])
            {
                change=arr[first];
                arr[first]=arr[first+1];
                arr[first+1]=change;
            }
            first++;
        }

        num++;
        first=0;    //恢復預設值
    }
}
