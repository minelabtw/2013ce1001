#include <iostream>

using namespace std;

int bubblesort(int*,int);
void output(int*,int);

int main()
{
    const int arraySize = 20; //宣告陣列大小
    int e[arraySize] = {}; // 宣告陣列
    int user_arraySize = 0;

    do //輸入使用者需求陣列
    {
        cout << "Enter array size (5-20): ";
        cin >> user_arraySize;
        if (user_arraySize<5 or user_arraySize>20)
            cout << "Out of range!" << endl;
    }
    while (user_arraySize<5 or user_arraySize>20);

    for (int i=1; i<=user_arraySize; i++) //輸入數字
    {
        do
        {
            cout << "The " << i << " element (0-100): ";
            cin >> e[i];
            if (e[i]<0 or e[i]>100)
                cout << "Out of range!" << endl;
        }
        while (e[i]<0 or e[i]>100);
    }

    cout << "Before sort" << endl;
    output(e,user_arraySize); //輸出排序前數字

    bubblesort(e,user_arraySize);

    cout << "After sort" << endl;
    output(e,user_arraySize); //輸出排序後數字

    return 0;
}

int bubblesort(int* array,int size) //由小到大排序
{
    int n = 0;
    int i = 1;

    for (int m=1; m<=size; m++)
    {
        n = i;
        do
        {
            if(array[n]>array[n+1])
            {
                array[n] += array[n+1];
                array[n+1] = array[n] - array[n+1];
                array[n] -= array[n+1];
            }
            n++;
        }
        while (n<size);
    }

    return array[size];
}

void output(int* array,int size) //輸出陣列內數字
{
    for (int p=1; p<=size; p++)
    {
        cout << "The " << p << " element: ";
        cout << array[p] << endl;
    }
}
