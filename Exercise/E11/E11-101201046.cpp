#include <iostream>
#include <algorithm>

using namespace std;

/*bubble sort*/
void bubblesort(int *arr, int _size) {
    for (int i = _size - 1; i > 0; i--)
        for (int j = 0; j < i; j++)
            if (arr[j] > arr[j+1])
                swap(arr[j], arr[j+1]);
    }

int main(void) {
    int *arr, len;

    do {
        cout << "Enter array size (5-20): ";
        cin >> len; //input the length
        } while((len < 5 || len > 20) && cout << "Out of range!\n");

    arr = new int[len]; // give arr memory

    //INPUT array
    for (int i = 0; i < len; i++)
        do {
            cout << "The " << i + 1 << " element (0-100): ";
            cin >> arr[i];
            } while((arr[i] < 0 || arr[i] > 100) && cout << "Out of range!\n");

    // OUTPUT
    cout << "Before sort\n";
    for (int i = 0; i < len; i++)
        cout << "The " << i + 1 << " element: " << arr[i] << endl;

    bubblesort(arr, len);

    cout << "After sort\n";
    for (int i = 0; i < len; i++)
        cout << "The " << i + 1 << " element: " << arr[i] << endl;

    delete [] arr;

    return 0;
    }
