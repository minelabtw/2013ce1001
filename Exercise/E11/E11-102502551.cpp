#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int a,b=0,number[20];

    cout<<"Enter array size (5-20): ";
    cin>>a;
    while(a<5 || a>20)
    {
        cout<<"Out of range!"<<endl<<"Enter array size (5-20): ";
        cin>>a;
    }
    while(b<a)
    {
        cout<<"The "<<b+1<<" element (0-100): ";
        cin>>number[b];
        while(number[b]<0 || number[b]>100)
        {
            cout<<"Out of range!"<<endl<<"The "<<b+1<<" element (0-100): ";
            cin>>number[b];
        }
        b++;
    }
    cout<<"Before sort"<<endl;
    for(b=0;b<a;b++)
    {
        cout<<"The "<<b+1<<" element: "<<number[b]<<endl;
    }
    cout<<"After sort";
    for(int i=0;i<a;i++)
    {
        for(int j=1;j<a-i;j++)
        {
            if(number[j]<number[j - 1])
            {
                swap(number[j],number[j-1]);
            }
        }
    }
    for(b=0;b<a;b++)
    {
        cout<<"The "<<b+1<<" element: "<<number[b]<<endl;
    }
}
