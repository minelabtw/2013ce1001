#include<iostream>
#include <cstdio>
#include <cstdlib>
using namespace::std;

void bubblesort(int* _array , int _size)//冒泡排序的函數
{
    int temp=0;
    //在此進行冒泡排序
    for(int i=0; i<_size; i++)
    {
        for(int j=0; j<_size; j++)
        {
            if(_array[j]>_array[j+1])
            {
                temp=_array[j];
                _array[j]=_array[j+1];
                _array[j+1]=temp;
            }

        }
    }
}

int main()
{
    int _size=0;//宣告陣列大小
    do//輸入陣列大小，若超出範圍則重新輸入
    {
        cout<<"Enter array size (5-20): ";
        cin>>_size;
        if(_size<5||20<_size)
            cout<<"Out of range!"<<endl;
    }
    while(_size<5||20<_size);

    int *_array;
    _array = new int[_size];//宣告動態陣列，並設定其大小

    for(int k=0; k<_size; k++)//請使用者輸入陣列的內容
    {
        do
        {
            cout<<"The "<<k+1<<" element (0-100): ";
            cin>>_array[k];
            if(_array[k]<0||100<_array[k])
                cout<<"Out of range!"<<endl;
        }
        while(_array[k]<0||100<_array[k]);
    }

    cout<<"Before sort"<<endl;

    for(int p=0;p<_size;p++)//輸出排序前的陣列內容
		cout<<"The "<<p+1<<" element: "<<_array[p]<<endl;

    bubblesort(_array,_size);

    cout<<"After sort"<<endl;

    for(int p=0;p<_size;p++)//輸出排序後的陣列內容
		cout<<"The "<<p+1<<" element: "<<_array[p]<<endl;

    return 0;
}
