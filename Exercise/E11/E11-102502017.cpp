#include<iostream>
using namespace std;

void bubblesort(int* _array , int _size)
{
    for(int j=0,k=_size,x=0; j<_size; j++)
    {
        x=0;
        for(int i=0; i<k-1; i++)
        {
            if(_array[i]>_array[i+1])               //將前後比較並判定是否交換
            {
                _array[i]+=_array[i+1];
                _array[i+1]=_array[i]-_array[i+1];
                _array[i]-=_array[i+1];
            }
            else x++;                               //紀錄交換次數
        }                                           //
        if(x==k-1)break;                            //如果都沒有交換，則排列完成-->Sort完成
        k--;                                        //每做完一次，下次次數-1
    }
}

int main(void)
{
    int _size;
    while(true)                                     //輸入array size
    {
        cout << "Enter array size (5-20): ";
        cin >> _size;
        if(_size>=5 && _size<=20)break;
        else cout << "Out of range!\n";
    }
    int *arr = new int[_size];                      //宣告array大小為_size
    while(true){
        static int i=0;                             //【成就：第一次Static】達成！
        while(true){
            cout << "The " << i+1 << " element (0-100): ";
            cin >> *(arr+i);
            if(*(arr+i)>=0 && *(arr+i)<=100)break;
            else cout << "Out of range!\n";
        }
        i++;
        if(i==_size)break;
    }
    cout << "Before sort\n";
    for(int i=0; i<_size; i++)
    {
        cout << "The " << i+1 << " element: " << *(arr+i) << endl;
    }
    bubblesort(arr,_size);
    cout << "After sort\n";
    for(int i=0; i<_size; i++)
    {
        cout << "The " << i+1 << " element: " << *(arr+i) << endl;
    }
    delete [] arr;                                  //歸還array空間給記憶體
    return 0;
}
