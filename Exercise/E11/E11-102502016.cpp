#include <iostream>

using namespace std;
void bubblesort(int* array , int size);
int main()
{
    int size;

    cout<<"Enter array size (5-20): ";
    cin>>size;
    while (size<5 || size>20)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter array size (5-20): ";
        cin>>size;
    }
    int array[size];//前面給了size值再宣告
    for (int i=0; i<size; i++)
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        cin>>array[i];
        while (array[i]<0 || array[i]>100)
        {
            cout<<"Out of range!"<<endl;
            cout<<"The "<<i+1<<" element (0-100): ";
            cin>>array[i];
        }
    }
    cout<<"Before sort"<<endl;
    for (int j=0; j<size; j++)
        cout<<"The "<<j+1<<" element: "<<array[j]<<endl;;
    cout<<"After sort"<<endl;
    bubblesort(array,size);
    for (int j=0; j<size; j++)
        cout<<"The "<<j+1<<" element: "<<array[j]<<endl;;
    return 0;
}
void bubblesort(int* array , int size)
{
    int change;
    for (int h=0; h<size; h++)//比完後再把剩下的比一次
    {
        for (int j=0; j<size; j++)//找最大放在最後一個
        {
            if (array[j]>array[j+1])
            {
                change = array[j];
                array[j]=array[j+1];
                array[j+1]=change;
            }

        }
    }
}
