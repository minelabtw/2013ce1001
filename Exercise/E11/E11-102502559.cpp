#include<iostream>
#include<cstdlib>
using namespace std;
int size=5;
void change(int *a, int *b)         //自訂交換函數
{
    int s=*a;
    *a=*b;
    *b=s;
}
void bubble_sort(int *x, int size)  //自訂排序函數
{
    for (int i=1 ; i<size ; i++)//限定次數
        for (int j=1 ; j<size ; j++)
            if (x[j]>x[j+1])
                change(x+j,x+j+1);
}
int main()
{

    int number;
    int counter1=1;
    int counter2=1;
    int counter3=1;
    cout<<"Enter array size (5-20): ";
    cin>>size;
    while(size<5 or size>20)
    {
        cout<<"Out of range !"<<endl;
        cout<<"Enter array size (5-20): ";
        cin>>size;
    }
    int x[size];
    while(counter1<=size)
    {
        cout<<"The "<<counter1<<" element (0-100): ";
        cin>>number;
        x[counter1]=number;
        counter1++;
    }
    cout<<"Before sort"<<endl;
    while(counter2<=size)
    {
        cout<<"The "<<counter2<<" element: "<<x[counter2]<<endl;
        counter2++;
    }
    cout<<"After sort"<<endl;
bubble_sort( x, size);
 while(counter3<=size)
 {
     cout<<"The "<<counter3<<" element: "<<x[counter3]<<endl;
     counter3++;
 }
}
