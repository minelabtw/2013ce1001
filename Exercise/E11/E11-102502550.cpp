#include <iostream>
using namespace std;
void bubblesort(int* , int );

int main()
{
    int array[20],s=0;                               //陣列宣告

    do                                               //使用者輸入長度
    {
        cout<<"Enter array size (5-20): ";
        cin>>s;
    }
    while((s<5 || s>20) && cout<<"Out of range!\n");

    for(int i=0; i<s; i++)                            //使用者輸入內容
    {
        do
        {
            cout<<"The "<<i+1<<" element: ";
            cin>>array[i];

        }
        while((array[i]<0 || array[i]>100) && cout<<"Out of range!\n");
    }

    cout<<"Before sort\n";                              //排序前
    for(int i=0; i<s; i++)
    {
        cout<<"The "<<i+1<<" element: "<<array[i]<<endl;

    }

    bubblesort(array,s);                               //泡沫排序

    cout<<"After sort\n";
    for(int i=0; i<s; i++)                             //排序後
    {
        cout<<"The "<<i+1<<" element: "<<array[i]<<endl;

    }
    return 0;
}
void bubblesort(int* array , int size)                  //泡沫排序法
{
    int tmp,count=0;
    for(int i=size-1; i>=1; i--)
    {
        for(int j=0; j<i; j++)
        {
            if(array[j] > array[j+1])
            {
                tmp=array[j];
                array[j]=array[j+1];
                array[j+1]=tmp;
            }
            count++;
        }
        if(!count) break;

    }

}
