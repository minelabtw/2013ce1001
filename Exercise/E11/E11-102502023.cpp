#include <iostream>
using namespace std;

int bubblesort(int* array , int size); // function prototype

int main()
{
    int arraySize = 20,number; // initialize ingeter arraySize to 20 and number
    int a[ arraySize ]; // initialize array a

    do
    {
        cout << "Enter array size (5-20): ";
        cin >> arraySize;
        if( arraySize < 5 || arraySize > 20 )
            cout << "Out of range!\n";
    }
    while ( arraySize < 5 || arraySize > 20 ); // do... while loop to check whether arraySize is on demand

    for ( int i = 1; i <= arraySize; i++ ) // for loop to prompt user to key in number
    {


        do
        {
            cout << "The " << i << " element (0-100): ";
            cin >> number;
            if ( number < 0 || number > 100 )
                cout << "Out of range!\n";
        }
        while ( number < 0 || number > 100 ); // do... while loop to check number is on demand

        a[ i - 1 ] = number; // sign a value to array
    }

    cout << "Before sort\n";
    for ( int j = 0; j < arraySize; j++ ) // for loop to output array
    {
        cout << "The " << j + 1 << " element: " << a[ j ] << endl;
    }

    bubblesort( a , arraySize );
    cout << "After sort\n";
    for ( int l = 0; l < arraySize; l++ ) // for loop to output array
    {
        cout << "The " << l + 1 << " element: " << a[ l ] << endl;
    }

    return 0;
}

int bubblesort(int* array , int size)
{
    for ( int k = size - 1; k > 0; k-- )
    {
        for ( int k1 = 0; k1 < size - 1; k1++ )
        {
            if ( array[ k1 ] > array[ k1 + 1 ] )
            {
                int t = array[ k1 ];
                array[ k1 ] = array[ k1 + 1 ];
                array[ k1 + 1 ] = t;
            }
        }
    }
}
