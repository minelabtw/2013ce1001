#include<iostream>
using namespace std;

void change(int *x, int *y)         //自訂交換函數
{
    int s=*x;
    *x=*y;
    *y=s;
}
void bubble_sort(int arr[], int n)  //自訂排序函數
{
    for (int i=1 ; i<n ; i++)
        for (int j=1 ; j<n ; j++)
            if (arr[j]>arr[j+1])
                change(arr+j,arr+j+1);
}
int main()
{
    int n=0;

    cout << "Enter array size (5-20): ";    //輸入要讓幾個數排列
    cin >> n;
    while(n<5||n>20)                        //判斷是否合乎要求
    {
        cout << "Out of range!" << endl;
        cout << "Enter array size (5-20): ";
        cin >> n;
    }
    int a[21]={};                           //宣告一陣列a
    for (int i=1 ; i<=n ; i++)
    {
        cout << "The " << i << " element (0-100): ";    //輸入數值給a陣列
        cin >> a[i];
        while(a[i]<0||a[i]>100)
        {
            cout << "Out of range!" << endl;
            cout << "The " << i << " element (0-100): ";
            cin >> a[i];
        }
    }
    cout << "Before sort" << endl;
    for(int i=1 ; i<=n ; i++)               //排序前的陣列
        cout << "The " << i << " element: " << a[i] << endl;

    bubble_sort(a,n);

    cout << "After sort" << endl;
    for(int i=1 ; i<=n ; i++)               //排序後的陣列
        cout << "The " << i << " element: " << a[i] << endl;

    return 0;
}
