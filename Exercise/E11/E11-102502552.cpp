#include<iostream>
using namespace std;

void bubblesort(int[],int);//呼叫排序函式

int main()
{
    int arr[20] = {};//宣告陣列
    int input,arraysize,counter = 0;//宣告輸入,元素,計數器變數

    do{
        cout << "Enter array size (5-20):";
        cin >> arraysize;
        if(arraysize < 5 || arraysize > 20)
            cout << "Out of range!" << endl;
    }while(arraysize < 5 || arraysize > 20);//檢查元素是否合規定

    do{
        cout << "The " << counter + 1 << " element (0-100):";
        cin >> input;
        if(input < 0 || input > 100)
        {
            cout << "Out of range!" << endl;
        }
        else
        {
            arr[counter] = input;
            counter++;
        }
    }while(counter < arraysize);//將輸入的數存入陣列

    cout << "Before sort" << endl;

    for(int counter1 = 0;counter1 < arraysize;counter1++)
    {
        cout << "The " << counter1 + 1 << " element:" << arr[counter1] << endl;
    }//輸出存入的數

    cout << "After resort" << endl;

    bubblesort(arr,arraysize);//排序

    for(int counter2=0;counter2 < arraysize;counter2++)
    {
        cout << "The " << counter2 + 1 << " element:" << arr[counter2] << endl;
    }//輸出排序後的數

return 0;
}

void bubblesort(int arr[],int arraysize)
{
    int key = arr[0];
    for(int i = 0;i < arraysize - 1;i++)//run幾次,-1是因為最後一次不用比
    {
       for(int j = 0;j < arraysize  -i - 1;j++)//每run一次,就會固定較大的數,所以-i,第一次run時-1就能夠固定最大數,不減會有垃圾
        {
            if(arr[j+1] < arr[j])//j與j+1活動相比
               {
                key = arr[j+1];
                arr[j+1] = arr[j];
                arr[j] = key;
               }
        }

    }
}//定義排序函式
