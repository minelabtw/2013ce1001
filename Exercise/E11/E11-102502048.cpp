#include<iostream>
#define swap(a,b){int t=a;a=b;b=t;}//ab交換
using namespace std;
void bubblesort(int*, int);
int main()
{
    int _size=0,ar[20];
    /*while(_size<5 || _size>20)
    {
        cout<<"Enter array size (5-20): ";
        cin>>_size;
        if(_size<5 || _size>20)
            cout<<"Out of range!\n";
    }*/
    while(cout<<"Enter array size (5-20): " && cin>>_size && (_size<5 || _size>20) && cout<<"Out of range!\n");//判斷加輸出
    for(int i=1,n=0; i<=_size; i++,n++)
    {
        do
        {
            cout<<"The "<<i<<" element (0-100): ";
            cin>>ar[n];
            if(ar[n]<0 || ar[n]>100)
                cout<<"Out of range!\n";
        }
        while(ar[n]<0 || ar[n]>100);
    }
    cout<<"Before sort\n";
    for(int i=1,n=0; i<=_size; i++,n++)
        cout<<"The "<<i<<" element: "<<ar[n]<<endl;
    bubblesort(ar,_size);
    cout<<"After sort\n";
    for(int i=1,n=0; i<=_size; i++,n++)
        cout<<"The "<<i<<" element: "<<ar[n]<<endl;
    return 0;
}
void bubblesort(int* a , int _size)
{
    /*for(int j=0; j<_size; j++) 選擇排序
    {
        int min=a[j],mink=j;
        for(int k=j; k<_size; k++)
        {
            if(a[k]<min)
            {
                min=a[k];
                mink=k;
            }
        }
        /*int temp=a[j];
        a[j]=a[mink];
        a[mink]=temp;
        swap(a[j],a[mink]);
    }*/
    for(;_size>0;_size--)
        for(int k=0;k<_size-1;k++)
            if(a[k]>a[k+1])
                swap(a[k],a[k+1]);
}
