#include<iostream>
using namespace std;

void bubblesort(int* a, int s) //宣告函式
//把陣列傳入bubblesort(int* array , int size)函式，因為指標只要只到陣列開頭位置就可，所以寫成int* a
{
    for(int i=0; i<=s-1; i++) //判斷s次
    {
        for(int j=0; j<=s-j+1; j++) //每次從陣列第一個開始往後判斷
        {
            int t=a[(j+1)];
            if(a[(j)]>a[(j+1)]) //如果後面一個比他大就交換
            {
                a[(j+1)]=a[(j)];
                a[(j)]=t;
            }
        }
    }
}

int main()
{
    int s,t;
    cout<<"Enter array size (5-20): ";
    cin>>s;
    while(s<5||s>20)
    {
        cout<<"Out of range!\n"<<"Enter array size (5-20): ";
        cin>>s;
    }
    int a[s-1]; //確認陣列大小後再宣告陣列
    for(int i=0; i<=s-1; i++)
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        cin>>t;
        while(t<0||t>100)
        {
            cout<<"Out of range!\n"<<"The "<<i+1<<" element (0-100): ";
            cin>>t;
        }
        a[i]=t; //確認陣列內容才輸入陣列
    }
    cout<<"Before sort\n";
    for(int i=0; i<=s-1; i++)
        cout<<"The "<<i+1<<" element: "<<a[i]<<endl;
    bubblesort(a , s);
    cout<<"After sort\n";
    for(int i=0; i<=s-1; i++)
        cout<<"The "<<i+1<<" element: "<<a[i]<<endl;

    return 0;
}
