#include <iostream>

using namespace std ;

//把數字從小排到大 以及輸出
void bubblesort(int* arr , int n)
{
    int k = n -1 ;
    int tmp ;

    while ( k!= 0 )
    {
        for ( int i = 0 ; i < k ; ++i )
        {
            if ( arr[i] > arr[i+1])
            {
                tmp = arr[i] ;
                arr[i] = arr[i+1] ;
                arr[i+1] =tmp ;
            }
        }
        --k ;
    }

    for ( int i =0 ; i < n ; ++i )
    {
        cout << "The " << i+1 << " element: " << arr[i] << endl ;
    }
}

int main ()
{
    int n , m ;
    cout << "Enter array size (5-20): " ;
    cin >> n ;

    while ( n < 5 || n >20 )
    {
        cout << "Out of range! " <<endl ;
        cout <<  "Enter array size (5-20): " ;
        cin >> n ;
    }
    //另一個指標陣列
    int *arr = new int[n] ;

//輸入每個數 然後存入指標
    for ( int i = 0 ; i <n ; ++i )
    {
        cout <<"The " << i+1 << " element (0-100): " ;
        cin >>m ;
        while ( m > 100 || m < 0 )
        {
            cout << "Out of range!" << endl ;
            cout <<"The " << i+1 << " element (0-100): " ;
            cin >>m ;
        }
        arr[i] = m ;
    }


    cout << "Before sort" <<endl ;

    for ( int i =0 ; i < n ; ++i )
    {
        cout << "The " << i+1 << " element: " << arr[i] << endl ;

    }
    cout << "After sort" <<endl ;
    bubblesort(arr , n) ;
    return 0 ;
}
