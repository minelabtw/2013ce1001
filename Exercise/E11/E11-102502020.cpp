#include <iostream>

using namespace std;

void bubblesort(int* array , int size);                       //宣告函數

int main()
{
    int _size=0;
    int number=0;                                             //宣告變數
    do
    {
        cout << "Enter array size (5-20): ";                  //輸入有多少個數字要進行排列
        cin >> _size;
        if(_size<5 or _size>20)
            cout << "Out of range!" << endl;
    }
    while(_size<5 or _size>20);
    int arr[_size];                                           //宣告陣列
    for(int i=0; i<_size; i++)
    {
        do
        {
            cout << "The " << i+1 << " element (0-100): ";    //將輸入數字存在陣列裡面
            cin >> number;
            if(number<0 or number>100)
                cout << "Out of range!" << endl;
        }
        while(number<0 or number>100);
        arr[i]=number;
    }
    cout << "Before sort" << endl;
    for(int j=0; j<_size; j++)                                //列出排列前的時候
    {
        cout << "The " << j+1 << " element: " << arr[j] << endl;
    }
    bubblesort(arr,_size);                                    //呼叫函式
    cout << "After sort" << endl;
    for(int k=0; k<_size; k++)                                //列出排列後的結果
    {
        cout << "The " << k+1 << " element: " << arr[k] << endl;
    }

    return 0;
}
void bubblesort(int* arr , int _size)                         //執行bubblesort
{
    int number=0;
    for(int i=1; i<_size; i++)
    {
        for(int j=0; j<_size-i; j++)
        {
            if(arr[j]>arr[j+1])
            {
                number=arr[j+1];
                arr[j+1]=arr[j];
                arr[j]=number;
            }
        }
    }
}
