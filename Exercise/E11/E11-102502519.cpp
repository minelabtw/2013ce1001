#include<iostream>
using namespace std;

void bubblesort(int* arr , int _size);

int main()
{
    int num1=0,num2[20]= {};
    do
    {
        cout << "Enter array size (5-20): ";
        cin >> num1;
        if(num1<5||num1>20)    //限定值在5~20之間
            cout << "Out of range!" << endl;
    }
    while(num1<5||num1>20);

    for(int i=0; i<num1; ++i)
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin >> num2[i];
        while(num2[i]<0||num2[i]>100)    //限定值位在0~100之間
        {
            cout << "Out of range!" << endl;
            cout << "The " << i+1 << " element (0-100): ";
            cin >> num2[i];
        }
    }

    cout << "Before sort" << endl;

    for(int j=0; j<num1; ++j)
        cout << "The " << j+1 << " element (0-100): " << num2[j] << endl;

    bubblesort(num2,num1);

    cout << "After sort" << endl;

    for(int k=0; k<num1; ++k)
        cout << "The " << k+1 << " element (0-100): " << num2[k] << endl;

    return 0;
}
void bubblesort(int* arr , int _size)
{
    int x=0;

    for(int i=0; i<_size; ++i)  //排序大小
    {
        for(int j=0; j<_size-1; ++j)
        {
            if(arr[j]>arr[j+1])
            {
                x=arr[j+1];
                arr[j+1]=arr[j];
                arr[j]=x;
            }
        }
    }
}
