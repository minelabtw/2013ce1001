#include<iostream>

using namespace std ;

void bubble(int * ,int );

int main()
{
    int arraysize=0;

    do                                                                      //judge arraysize
    {
        cout<<"Enter array size (5-20): ";
        cin>>arraysize;
        if(arraysize>=5&&arraysize<=20)
        {
            break;
        }
        else
            cout<<"Out of range!"<<endl;

    }
    while(1);
    const int s=arraysize ;
    int a[s];
    a[s]={};
    for(int i=0; i<arraysize; i++)                                  //輸入值
    {
        do
        {
            cout<<"The "<<i+1<<" element (0-100): ";
            cin>>a[i];
            if(a[i]<0||a[i]>100)
                cout<<"Out of range!"<<endl;
            else
                break;
        }
        while(1);

    }
    cout<<"Before sort"<<endl;
     for(int i=0; i<arraysize; i++)
    {
        cout<<"The "<<i+1<<" element: "<<a[i]<<endl;
    }
    bubble(a,s);

}

void bubble(int * a,int b)
{
    int exchange=b-1;

    while(exchange)
    {
        //#记录下发生数据交换的位置#%
        int bound = exchange;

        exchange = 0; //#假定本趟比较没有数据交换#%

        for(int i = 0; i < bound; i++)
        {
            if (a[i] >a[i + 1])
            {
                //#交换#%
                int t = a[i];
                a[i] = a[i + 1];
                a[i + 1] = t;
                exchange=i;
            }
        }
    }

    cout<<"After sort"<<endl;                                           //輸出sort後的
    for(int i=0; i<b; i++)
    {
        cout<<"The "<<i+1<<" element: "<<a[i]<<endl;
    }

}

