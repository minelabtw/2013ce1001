#include <iostream>

using namespace std;


void bubblesort(int* array , int size);

int main()
{
    int n_Size,arrayValue=0;

    //指定陣列大小
    do
    {
        cout << "Enter array size (5-20): " ;
        cin>> n_Size;
        if(n_Size<5 ||n_Size>20)
        {
            cout << "Out of range!" << endl;
        }
    }while(n_Size<5 ||n_Size>20);

    int arrays[n_Size];
    //輸入
    for(int i=1;i<=n_Size;i++)
    {
        do
        {
            cout << "The "<<i<<" element: " ;
            cin>> arrayValue;
            if(arrayValue<0 || arrayValue>100)
            {
                cout << "Out of range!" << endl;
            }
            else
            {
                arrays[i-1]=arrayValue;
            }
        }while(arrayValue<0 || arrayValue>100);
    }
    //印出排序前
    cout << "Before sort" << endl;
    for(int i=0;i<n_Size;i++)
    {
        cout << "The "<<i+1<<" element:" <<arrays[i] << endl;
    }
//使用泡沫排序法
    bubblesort(arrays,n_Size);
    //印出排序後
cout << "After sort" << endl;
    for(int i=0;i<n_Size;i++)
    {
        cout << "The "<<i+1<<" element:" << arrays[i] << endl;
    }
    return 0;
}

void bubblesort(int* array , int size)
{
    int temp,change;
    //取第一個到最後一個數
    //取出來的數依序與下一個做比較，若較大則互換，一值做到此陣列結束為止
    //3 2 1  2 3 1  2 1 3   2 4 1 3 = 2 1 4 3= 2 1 3 4
    //2 1 3  1 2 3                    1 2 3 4
    //執行次數共n次   1 3 2   1 3 2  1 2 3
    for(int i=0;i< size-1;i++)
    {
        for(int j=0;j<size-1;j++)
        {
            if(array[j]>array[j+1])
            {
                temp=array[j+1];
                array[j+1]=array[j];
                array[j]=temp;
            }
        }
    }
}
