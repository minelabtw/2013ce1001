#include <iostream>
using namespace std;
void bubblesort( int*, int );
main()
{
    int Size;  //設陣列大小

    do  //輸入陣列大小
    {
        cout << "Enter array size (5-20): ";
        cin >> Size;
    }
    while( Size<5 && cout << "Out of range!\n" || Size>20 && cout << "Out of range!\n" );

    int data[ Size ];
    for( int i=0; i<Size; i++ )  //輸入各個數值
    {
        do
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> data[ i ];
        }
        while( data[ i ]<0 && cout << "Out of range!\n" || data[ i ]>100 && cout << "Out of range!\n");
    }

    cout << "Before sort\n";
    for( int i=0; i<Size; i++ )  //排序前
    {
        cout << "The " << i+1 << " element: " << data[ i ] << endl;
    }

    bubblesort( data, Size );

    cout << "After sort\n";
    for( int i=0; i<Size; i++ )  //排序後
    {
        cout << "The " << i+1 << " element: " << data[ i ] << endl;
    }

    return 0;
}
void bubblesort( int* Array, int number )
{
    for( int i=0; i<number; i++ )  //多次執行
    {
        for( int j=1; j<number; j++ )  //比較大小
        {
            if( Array[ j ]<Array[ j-1 ] )  //交換
            {
                Array[ j ]+=Array[ j-1 ];
                Array[ j-1 ]=Array[ j ]-Array[ j-1 ];
                Array[ j ]-=Array[ j-1 ];
            }
        }
    }
}
