#include<iostream>

using namespace std;

int bubblesort(int *Array,int Size);  //prototype
int Swap(int *ele1ptr,int *ele2ptr);  //prototype

int main()
{
    int length=0,num=-1;  //宣告陣列長度、輸入值，並初始化其值於範圍外

    while (length<5||length>20)
    {
        cout << "Enter array size (5-20): ";
        cin >> length;
        if (length<5||length>20)
            cout << "Out of range !!" << endl;
    }

    int arr[length];  //宣告陣列，並使輸入值length為其長度

    for (int i=0;i<length;i++)  //for loop儲存每一輸入值於陣列中
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin >> num;
        if (num<0||num>100)
        {
            cout << "Out of range !!" << endl;
            i=i-1;
        }
        else
            arr[i]=num;
    }

    cout << "Before sort" << endl;
    for (int i=0;i<length;i++)  //印出未排列大小的陣列
        cout << "The " << i+1 << " element (0-100): " << arr[i] << endl;

    bubblesort(arr,length);  //呼叫排序函式，排列陣列大小

    cout << "After sort" << endl;
    for (int i=0;i<length;i++)  //印出排列後的陣列
        cout << "The " << i+1 << " element (0-100): " << arr[i] << endl;

    return 0;
}

int bubblesort(int *Array,int Size)
{
    int Min;  //宣告最小值
    for (int i=0;i<Size-1;i++)
    {
        Min=i;  //預設第i項最小
        for (int index=i+1;index<Size;index++)
            if(Array[index]<Array[Min])
                Min=index;  //若第i+1項小於第i項，則最小值等於i+1項
        Swap(&Array[i],&Array[Min]);  //呼叫swap函式互換其位置
    }
}
int Swap(int *ele1ptr,int *ele2ptr)
{
    int hold=*ele1ptr;
    *ele1ptr=*ele2ptr;
    *ele2ptr=hold;
}
