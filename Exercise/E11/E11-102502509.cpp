#include <iostream>
#include <iomanip>

using namespace std;
void bubblesort(int*, int);
int main()
{
    int range;
    int number;
    int arr[20]; // 上限

    cout << "Enter array size (5-20): ";
    cin >> range;

    while (range < 5 || range > 20) // 或者~~而非而且
    {
        cout << "Out of range!\n";
        cout << "Enter array size (5-20): ";
        cin >> range;
    }
    for (int g = 0; g < range; g++) // 才能儲存下一個數字
    {
        do
        {
            cout << "The " << g+1 << " element (0-100): ";
            cin >> arr[g];

            if (arr[g] < 0 || arr[g] >100)
                cout << "Out of range!\n";

        }
        while (arr[g] <0 || arr[g] >100);
    }

    cout << "Before sort\n" ;
    for (int i = 0; i < range; i++)
    {
        cout << "The " << i + 1 << " element : " << arr[i] << endl;
    }

    bubblesort(arr, range); // 請出函式

    cout << "After sort\n";
    for (int l = 0; l < range; l++)
        cout << "The " << l + 1 << " element : " << arr[l] << endl;

    return 0;
}
void bubblesort(int *sett, int f)
{
    for (int h = 0; h < f-1; h++) // 比幾次
    {
        for (int a = 0; a < f; a++) // 所有都要比
        {
            int hold;
            hold = sett[a+1]; // 拿出下一個
            if (sett[a] > sett[a+1])
            {
                // 換位置
                sett[a+1] = sett[a];
                sett[a] = hold;
            }
        }
    }
}
