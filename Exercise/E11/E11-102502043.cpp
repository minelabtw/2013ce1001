#include <iostream>

using namespace std;
void bubblesort(int* arr , int _size);
int main()
{
    int larray[20];
    int l;

    cout<<"Enter array size (5-20): ";
    cin>>l;
    while(l<5||l>20)                                                    //限制範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter array size (5-20): ";
        cin>>l;
    }
    for(int x=0;x<l;x++)                                                //僵直值入陣列
    {
        int i;
        cout<<"The "<<x+1<<" element (0-100): ";
        cin>>i;
        while(i<0||i>100)
        {
            cout<<"Out of range!"<<endl;
            cout<<"The "<<x+1<<" element (0-100): ";
            cin>>i;
        }
        larray[x]=i;
    }
    cout<<"Before sort"<<endl;
    for(int a=0;a<l;a++)                                            //顯示陣列未排序前的值
    {
        cout<<"The "<<a+1<<" element: "<<larray[a]<<endl;
    }
    bubblesort(larray,l);
    cout<<"After sort"<<endl;
    for(int b=0;b<l;b++)                                            //顯示排序後的陣列值
        {
        cout<<"The "<<b+1<<" element: "<<larray[b]<<endl;
        }
    return 0;
}
void bubblesort(int* arr , int _size)
{
    int a;
    int b;
    for(a=_size-1;a>=0;a--)                                         //排順序大小
    {
        for(b=0;b<a;b++)
        {
            if(arr[b]>arr[b+1])
            {
                int j;
                j=arr[b+1];
                arr[b+1]=arr[b];
                arr[b]=j;
            }
        }
    }

}
