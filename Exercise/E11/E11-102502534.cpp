#include<iostream>
using namespace std;

#define ArraySize 20
int record[ArraySize] = {};//宣告記錄的陣列
int bubblesort(int *arr,int _size);//function prototype
int main()
{
    int length=0;//宣告變數
    int input=0;

    cout<<"Enter array size (5-20): ";
    cin>>length;
    while(length<5 or length>20)//若length不在5~20之間顯示Out of range!並且重新輸入
    {
        cout<<"Out of range!"<<endl<<"Enter array size (5-20): ";
        cin>>length;
    }
    for(int x=0; x<length; x++)
    {
        cout<<"The "<<x+1<<" element (0-100): ";
        cin>>input;
        if(input<0 or input>100)
        {
            cout<<"Out of range!"<<endl<<"The "<<x+1<<" element (0-100): ";
            cin>>input;
        }
        record[x]=input;
    }
    cout<<"Before sort"<<endl;
    for(int x=0; x<length; x++)
    {
        cout<<"The "<<x+1<<" element: "<<record[x]<<endl;
    }
    cout<<"After sort"<<endl;
    bubblesort(record,length);//回傳排列好的陣列
    for(int x=0; x<length; x++)
    {
        cout<<"The "<<x+1<<" element: "<<record[x]<<endl;
    }
    return 0;
}
int bubblesort(int *arr,int _size)//function definition
{
    for(int i=0; i<_size-1; i++)
    {
        for ( int j=0 ; j<_size-1; j++ )
        {
            if  ( arr[ j ] > arr[ j + 1 ] )
                swap ( arr[ j ] , arr[ j + 1 ] ) ;
        }
    }

}

