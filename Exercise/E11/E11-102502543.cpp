#include <iostream>
using namespace std;
int bubblesort(int *, int ); //宣告函式
int main()
{
    int a=0; //宣告變數
    int c=0;
    do
    {
        cout <<"Enter array size (5-20): "; //輸出
        cin >> a; //輸入
        if (a<5 or a>20) //判斷
            cout <<"Out of range!"<<endl;
    }
    while (a<5 or a>20); //迴圈
    int b[a]; //宣告陣列
    do
    {
        cout <<"The "<<c+1<<" element (0-100): ";
        cin >>b[c];
        if (b[c]>=0 and b[c]<=100)
            c++;
        else
            cout <<"Out of range!"<<endl;
    }
    while (c<a);
    cout <<"Before sort"<<endl;
    for (int d=0; d<a; d++) //輸出陣列內容
    {
        cout <<"The "<<d+1<<" element: "<<b[d]<<endl;
    }
    cout <<"After sort";
    bubblesort(b,a); //進入函式
    for (int d=0; d<a; d++) //輸出陣列內容
    {
        cout <<endl<<"The "<<d+1<<" element: "<<b[d];
    }
    return 0;
}
int bubblesort(int* arr, int e) //函式
{
    for (int t=0; t<e-1 ; t++)
    {
        int n=t;
        int g=arr[t+1];
        if(g<arr[t])
            while (g<arr[n] and n>=0)
            {
                arr[n+1]=arr[n];
                n--;
            }
        arr[n+1]=g;
    }
}
