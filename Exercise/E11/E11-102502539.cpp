#include <iostream>

using namespace std;

void bubblesort( int* , int );

int main()
{
    int arraysize;

    do
    {
        cout << "Enter array size (5-20): " ;
        cin >> arraysize;
        if ( arraysize < 5 || arraysize > 20 )
            cout << "Out of range!" << endl;
    } while ( arraysize < 5 || arraysize > 20 ) ;

    int input[arraysize];

    for ( int i = 0 ; i < arraysize ; i++ )
    {
        do
        {
            cout << "The " << i + 1 << " element (0-100): " ;
            cin >> input[i];
            if ( input[i] < 0 || input[i] > 100 )
                cout << "Out of range!" << endl;
        } while ( input[i] < 0 || input[i] > 100 ) ;
    }

    cout << "Before sort" << endl;

    for ( int m = 0 ; m < arraysize ; m++ )
    {
        cout << "The " << m + 1 << " element (0-100): " << input[m] << endl;
    }

    cout << "After sort" << endl;

    bubblesort( input , arraysize );

    for ( int n = 0 ; n < arraysize ; n++ )
    {
            cout << "The " << n + 1 << " element (0-100): " << input[n] << endl;
    }

    return 0;
}

void bubblesort( int *input , int arraysize )      //比大小，作排列
{
    for ( int h = 0 ; h < arraysize ; h++ )
    {
        for ( int k = 0 ; k < arraysize ; k++ )
        {
            if ( input[k] > input[k+1] )
            {
                int hold = input[k];
                input[k] = input[k+1];
                input[k+1] = hold;
            }
        }
    }

}
