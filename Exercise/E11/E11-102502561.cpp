#include<iostream>
using namespace std;

int main()
{
    int lenght; //  array
    int enterL(int [],int );    //input number to each array
    int enterB(int [],int );    //arrange array
    cout << "Enter array size (5-20): ";
    cin >> lenght;
    while(lenght>20||lenght<5)  //array size
    {
        cout << "Out of range!\n";
        cout << "Enter array size (5-20): ";
        cin >> lenght;
    }
    int _array[lenght];         //int _array
    for(int i=0; i<lenght; i++)_array[i]=0;
    enterL(_array,lenght);
    cout << "Before sort\n";
    for(int i=0; i<lenght; i++) //output array before sorting
    {
        cout << "The " << i+1 << " element: " << _array[i] << "\n";
    }
    cout << "After sort\n";
    enterB(_array,lenght);
    for(int i=0; i<lenght; i++) //output array after sorting
    {
        cout << "The " << i+1 << " element: " << _array[i] << "\n";
    }
    return 0;
}

int enterL(int bubble[],int _size)
{
    int element;
    for(int i=0; i<_size; i++)
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin >> element;
        while(element<0||element>100)
        {
            cout << "Out of range!\n";
            cout << "The " << i+1 << " element (0-100): ";
            cin >> element;
        }
        bubble[i] = element;
    }
}

int enterB(int bubble[],int _size)
{
    for(int j=0; j<_size; j++)
    {
        for(int i=0; i<_size-1; i++)
        {
            if(bubble[i]>bubble[i+1])   //exchange
            {
                bubble[i]^= bubble[i+1];
                bubble[i+1]^= bubble[i];
                bubble[i]^= bubble[i+1];
                i=0;
            }
        }
    }
}
