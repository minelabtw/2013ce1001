#include<iostream>

#define swap(x,y) x^=y^=x^=y  //swap

void bubblesort(int* array,int size)   //Bubble Sort
{
   for(int i=size-1;i!=0;i--)
      for(int j=0;j!=i;j++)
         if(array[j]>array[j+1])
            swap(array[j],array[j+1]);
}

int main()
{
   int l,array[20];  //declaration

   do    //input
   {
      std::cout<<"Enter array size (5-20): ";
      std::cin>>l;
      if(l<5||l>20)
         std::cout<<"Out of range!\n";
   }while(l<5||l>20);
   for(int i=0;i!=l;i++)
   {
      std::cout<<"The "<<i+1<<" element (0-100): ";
      std::cin>>array[i];
      if(array[i]<0||array[i]>100)
      {
         std::cout<<"Out of range!\n";
         i--;
      }
   }

   std::cout<<"Before sort\n";   //before
   for(int i=0;i!=l;i++)
      std::cout<<"The "<<i+1<<" element: "<<array[i]<<'\n';

   bubblesort(array,l);    //Bubble Sort

   std::cout<<"After sort\n";    //After
   for(int i=0;i!=l;i++)
      std::cout<<"The "<<i+1<<" element: "<<array[i]<<'\n';

   return 0;   //exit
}
