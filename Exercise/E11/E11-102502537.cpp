#include<iostream>
using namespace std;
void bubblesort(int* AR , int AS)//函式 功能為排列
{
    int Q=0 ;
    int i , j ;
    for(i=1 ; i<AS ; i++ )
    {
        for(j=1; j<AS; j++)
        {
            if (AR[j]>AR[j+1])
            {
                Q=AR[j];
                AR[j]=AR[j+1];
                AR[j+1]=Q;
            }
        }
    }

}
int main()
{
    int AS=20 ;//宣告變數
    int i=0 ;
    int j=0 ,k=0,l=0;
    int AR[21];
    cout <<"Enter array size (5-20): ";//輸出輸入題目所需
    cin >> AS ;
    while(AS>20 or AS<5)
    {
        cout <<"Out of range!" <<endl;
        cout <<"Enter array size(5-20): ";
        cin >> AS ;
    }

    for (i=1 ; i<=AS; i++)//將輸入之值存入陣列
    {

        cout << "The "<< i <<" element (0-100):";
        cin >>AR[i] ;
        while(AR[i]<0 or AR[i]>100)
        {
            cout <<"Out of range!"<<endl;
            cout <<"The "<< i <<" element (0-100):" ;
            cin >> AR[i] ;
        }

    }
    cout <<"Before sort"<<endl;//排列前的數字
    for (j=1 ; j<=AS ; j++)
    {
        cout <<"The "<< j <<" element: " << AR[j]<<endl;
    }
    cout <<"After sort"<<endl;
    bubblesort(AR,AS);//叫出函式
    for (k=1; k<=AS ; k++)//排列後的數字
    {
        cout <<"The "<<k <<" element: " << AR[k]<<endl;
    }

    return 0;
}


