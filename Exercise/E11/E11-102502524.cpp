#include <iostream>
using namespace std;

void bubblesort(int* g,int l);                                  //宣告函式

int main()
{
    int a = 0;                                                  //設定變數
    int l = 0;
    int g[21];

    cout << "Enter array size (5-20): ";                        //輸入陣列大小，並判斷是否符合範圍
    while(cin >>l)
    {
        if (l<5 || l>20)
        {
            cout << "Out of range!" << endl;
            cout << "Enter array size (5-20): ";
        }
        else
            break;
    }

    for (int i=1; i<=l; i++)                                    //輸入數字進陣列，並判斷是否符合範圍
    {
        cout << "The " << i << " element (0-100): ";
        while(cin >> g[i])
        {
            if (g[i]<0 || g[i]>100)
            {
                cout << "Out of range!" << endl;
                cout << "The " << i << " element (0-100): ";
            }
            else
                break;
        }
    }

    cout << "Before sort" << endl;                              //輸出排列前之陣列
    for (int i=1; i<=l; i++)
    {
        cout << "The " << i << " element: " << g[i] << endl;
    }

    bubblesort(g,l);                                            //泡泡排序
    cout << "After sort" << endl;                               //輸出排列後之陣列
    for (int i=1; i<=l; i++)
    {
        cout << "The " << i << " element: " << g[i] << endl;
    }

    return 0;
}

void bubblesort(int* g,int l)                                   //函式－泡泡排序
{
    for (int j=1;j<=l;j++)
    {
        for (int i=1; i<=l; i++)
        {
            int temp = 0;
            if (g[i]>g[i+1])
            {
                temp = g[i];
                g[i] = g[i+1];
                g[i+1] = temp;
            }

        }
    }
}
