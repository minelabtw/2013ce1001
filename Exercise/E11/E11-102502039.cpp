#include<iostream>
using namespace std;

void bubblesort(int* array , int size);

int main()
{
    int number;//宣告變數
    int _size;

    cout<<"Enter array size (5-20): ";//進行輸出輸入與判斷
    cin>>_size;
    while(_size>20 || _size<5)
    {
        cout<<"Out of range!"<<endl<<"Enter array size (5-20): ";
        cin>>_size;
    }
    int arraysize[_size];
    for(int i=1,countN=0; i<=_size; i++,countN++)
    {
        do
        {
            cout<<"The "<<i<<" element (0-100): ";
            cin>>arraysize[countN];
            if(arraysize[countN]>100 || arraysize[countN]<0)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(arraysize[countN]>100 || arraysize[countN]<0);
    }
    cout<<"Before sort"<<endl;
    for(int k=0; k<_size; k++)
    {
        cout<<"The "<<k+1<<" element: "<<arraysize[k]<<endl;
    }
    bubblesort(arraysize,_size);
    cout<<"After sort"<<endl;
    for(int i=0;i<_size;i++){
        cout<<"The "<<i+1<<" element: "<<arraysize[i]<<endl;
    }
    return 0;
}
void bubblesort (int* array , int size)//函式
{

    for(int j=0;j<size;j++){
        int min=array[j];
        int mink=j;
        for(int k=j;k<size;k++){
            if(array[k]<min){
                min=array[k];
                mink=k;
            }
        }
        int temp=array[j];
        array[j]=array[mink];
        array[mink]=temp;
    }
}
