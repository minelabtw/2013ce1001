#include <iostream>
using namespace std;
int bubblesort(int* array , int size);  //宣告bubblesort函式,傳入陣列和陣列大小
int main()
{
    int L;
    int number;
    do  //判斷輸入陣列大小是否在範圍內
    {
        cout << "Enter array size (5-20): ";
        cin >> L;
        if (L>20 or L<5)
            cout << "Out of range!" << endl;
    }
    while (L>20 or L<5);
    int element[L];  //宣告陣列

    for (int i=0; i<L; i++)
    {
        do  //判斷輸入的值是否在範圍內
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> number;
            if (number>100 or number<0)
                cout << "Out of range!" << endl;
        }
        while (number>100 or number<0);
        element[i]=number;  //將值存入陣列
    }

    cout << "Before sort" << endl;
    for (int i=0; i<L; i++)  //將存入陣列的值輸出
    {
        cout << "The " << i+1 << " element (0-100): " << element[i] << endl;
    }
    bubblesort(element,L);

    return 0;
}

int bubblesort(int* array , int size)
{
    int a;
    for (int i=0; i<size-1; i++)
    {
        for (int i=0; i<size; i++)  //將陣列裡的值排列大小
        {
            if (array[i]>array[i+1])
            {
                a=array[i];
                array[i]=array[i+1];
                array[i+1]=a;
            }
        }
    }
    cout << "After sort";
    for (int i=0; i<size; i++)  //將排列完大小的陣列輸出
    {
        cout << endl << "The " << i+1 << " element (0-100): " << array[i];
    }
}
