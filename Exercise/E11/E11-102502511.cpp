#include <iostream>

void bubblesort(int* array , int size);

using namespace std;
int main()
{
    int a; //此變數為儲存要輸入幾次
    int c = 0; //此變數用來幫助陣列記下此數為第幾個數
    int d; //此數用來儲存輸出的數值
    const int e = 20; //固定陣列最多為20個數
    int f[e]; //設變數陣列

    cout << "Enter array size (5-20): ";
    cin >> a;
    while(a<5||a>20)
    {
        cout << "Out of range!" << endl;
        cout << "Enter array size (5-20): ";
        cin >> a;
    }

    for(int b = 1 ; b <= a ; b++) //重複次數為1變到a 及總輸入次數
    {
        cout << "The " << c+1 << " element (0-100): ";
        cin >> d;
        while(d<0||d>100)
        {
            cout << "Out of range!" << endl;
            cout << "The " << c+1 << " element (0-100): ";
            cin >> d;
        }
        f[c] = d; //儲存第幾次輸入的數到第n次的陣列中
        c++; //每次結束c++使陣列下次適存儲在第n+1個陣列中
    }

    cout << "Before sort" << endl;

    for(int k = 0 ; k < a ; k++)
    {
        cout << "The " << k+1 << " element (0-100): " << f[k];
        cout << endl;
    }

    cout << "After sort" << endl;

    bubblesort(f,a); //帶入函式

    return 0;
}

void bubblesort(int* array , int size)
{
    for(int  g = 1 ; g <= size ; g++) //每次步驟重複的次數為1~size及該次要輸入的數的數量
    {
        int i = 0;

        while(i < size-1) //當該數比後一個數大時，該數往後拉，繼續和後一個比，但該數大小不變，故用上面儲存的h代表該數
            //@size-1的目的在於陣列中若有5個數，該陣列最高只到第4個數,故size需先減1不然會跟陣列中第五個數比較到大小，該數為0，因此會影響到輸出結果，因為運作到6個數
        {
            int h = array[i]; //先儲存第i個數，用來與前一個數互換
            if(h > array[i+1])
            {
                array[i] = array[i+1];
                array[i+1] = h;
            }
            i++; //i++的目的在於持續地跟後一個數比較大小，直到不符合while迴圈
        }
    }

    for(int j = 0 ; j < size ; j++)
    {
        cout << "The " << j+1 << " element (0-100): " << array[j];
        cout << endl;
    }
}
