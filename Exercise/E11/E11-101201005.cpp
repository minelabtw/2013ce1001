//Bubble sort
#include<iostream>
using namespace std;

void bubblesort(int* array , int size) //bubblesort function
{
    for (int i=1; i<=size; i++) //由小排列到大
    {
        for (int j=size;j >i; j--)
        {
            if(array[j] < array[j-1])
            {
                int tmp = array[j-1];
                array[j-1] = array[j];
                array[j] = tmp;
            }
        }
    }

}


int main()
{
    int a;
    int Array[21];

    cout << "Enter array size (5-20): ";
    cin >> a;
    while (a<5 || a>20) //size(5-20)
    {
        cout << "Out of range!\n" ;
        cout << "Enter array size (5-20): ";
        cin >> a;
    }

    for (int i=1; i<=a; i++)
    {
        cout << "The " << i << " element (0-100): " ;
        cin >> Array[i];
        if ( Array[i]<0||Array[i]>100 ) //element(0-100)
        {
            cout << "Out of range!\n";
            cout << "The " << i << " element (0-100): " ;
            cin >> Array[i];
        }
    }

    cout << "Before sort\n";
    for (int i=1; i<=a; i++)
        cout << "The " << i << " element: " << Array[i] << endl;

    bubblesort(Array,a); //call function
    cout << "After sort\n";
     for (int i=1; i<=a; i++)
        cout << "The " << i << " element: " << Array[i] << endl;

    return 0;
}
