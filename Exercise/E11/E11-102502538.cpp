#include<iostream>

using namespace std;

int input[20] = {};
void bubblesort(int arr[] ,int _size)//排序輸入的數
{
    for(int t=0; t<_size-1; t++)
    {
        for(int i=0; i<_size-1; i++)//若後面較大就交換
        {
            int x;
            if(arr[i]>arr[i+1])
            {
                x=arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=x;
            }
        }
    }
}

int main()
{
    int L,element;

    cout<<"Enter array size (5-20): ";
    cin>>L;

    while(L>20||L<5)//判定L範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter array size (5-20): ";
        cin>>L;
    }

    for(int times=0; times<L; times++)
    {
        cout<<"The "<<times+1<<" element (0-100): ";
        cin>>element;
        while(element<0||element>100)//判定element範圍
        {
            cout<<"Out of range!"<<endl;
            cout<<"The "<<times<<" element (0-100): ";
            cin>>element;
        }
        input[times]=element;
    }

    cout<<"Before sort"<<endl;
    for(int times=0; times<L; times++)//輸出原始的陣列
    {
        cout<<"The "<<times+1<<" element: "<<input[times]<<endl;
    }

    bubblesort(input,L);//呼叫函數
    cout<<"After sort"<<endl;
    for(int times=0; times<L; times++)//輸出排列後的陣列
    {
        cout<<"The "<<times+1<<" element: "<<input[times]<<endl;
    }
    return 0;
}
