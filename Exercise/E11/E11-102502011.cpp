#include<iostream>

using namespace std;

void bubblesort(int* number , int L ) ;

int main()
{
    int L = 0 ;
    int number[20] = {} ; //當輸入20個 陣列才有空間可以交換

    do
    {
        cout << "Enter array size (5-20): " ;
        cin >> L ;
        if (L < 5 || L > 20 )
            cout << "Out of range!" << endl ;
    }
    while (L < 5 || L > 20 ) ;


    for (int x=0 ; x<L; x++)
    {
        do
        {
            cout <<  "The " << x+1 << " element (0-100): " ; //輸入要儲存的數
            cin >> number[x] ;
            if (number[x] > 100 || number[x] < 0 )
                cout << "Out of range!" << endl ;
        }
        while (number[x] > 100 || number[x] < 0 ) ;
    }

    cout << "Before sort" << endl ;

    for (int x=0 ; x<L; x++ ) //第一次輸出
        cout << "The " << x+1 << " element: " << number[x] << endl ;

    cout << "After sort" << endl ;

     bubblesort(number ,L ) ;

    return 0;
}

void bubblesort(int* number , int L ) //哥的凾式
{
    int hello = 0  , x = 0;

    for (int y=0 ; y<L; y++ )
    {
        for ( x=0 ; x<L-1; x++ )
        {
            if ( number[x] > number[x+1] ) //當第X的數大於第X+1的數 , 讓兩數互換
            {
                hello = number[x+1] ;
                number[x+1] = number[x] ;
                number[x] = hello ;
            }
        }
        x = 0 ;
    }

    for (int x=0 ; x<L; x++ )
        cout << "The " << x+1 << " element: " << number[x] << endl ;
}
