#include<iostream>
using namespace std;
int main()
{
    int L=0;
    int num=0;
    int array[20]= {};//宣告陣列並可以存入20個整數
    void bubblesort(int* array,int size);
    do
    {
        cout<<"Enter array size (5-20): ";
        cin>>L;
        if(L>20||L<5)
            cout<<"Out of range!"<<endl;
    }
    while(L>20||L<5);
    for(int input=0; input<L; input++)
    {
        do
        {
            cout<<"The "<<input+1<<" element (0-100): ";
            cin>>num;
            if(num>100||num<0)
                cout<<"Out of range!"<<endl;
        }
        while(num>100||num<0);
        array[input]=num;//輸入的值存入陣列
    }
    cout<<"Before sort"<<endl;
    for(int times=0; times<L; times++)//輸出未排列前的陣列
    {
        cout<<"The "<<times+1<<" element: "<<array[times]<<endl;
    }
    bubblesort(array,L);
    cout<<"After sort"<<endl;
    for(int times=0; times<L; times++)//輸出排列後的陣列
    {
        cout<<"The "<<times+1<<" element: "<<array[times]<<endl;
    }
    return 0;
}
void bubblesort(int* array,int size)
{
    for(int b=1; b<size; b++)
    {
        int memory=array[b];//存入陣列的值
        while((b>0)&&(array[b-1]>memory))//判斷是否需要排列
        {
            array[b]=array[b-1];
            b--;
        }
        array[b]=memory;
    }
}
