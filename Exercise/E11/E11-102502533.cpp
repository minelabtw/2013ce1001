#include<iostream>
using namespace std;

void bubblesort(int Array[] , int );//訂一個函式排列大小
int main()
{
    int s;//陣列的長度
    int a;//元素的名稱
    cout<<"Enter array size (5-20): ";//要求輸入陣列長度
    cin>>s;
    while(s<5 || s>20)//限制陣列長度
    {
        cout<<"Out of range!";
        cout<<"\nEnter array size (5-20):";
        cin>>s;
    }
    int Array[s];//依輸入直訂一個陣列
    for(int i=0; i<s; i++)//要求輸入陣列內的值
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        cin>>a;
        while(a<0 || a>100)
        {
            cout<<"Out of range!";
            cout<<"\nThe "<<i+1<<" element (0-100): ";
            cin>>a;
        }
        Array[i]=a;
    }
    cout<<"Before sort"<<endl;//先顯示未排序前的輸入值
    for(int j=0; j<s; j++)
    {
        cout<<"The "<<j+1<<" element : "<<Array[j]<<endl;
    }

    bubblesort( Array , s );//將輸入值導入函示排序

    cout<<"After sort"<<endl;//顯示排序後的值
    for(int b=0; b<s; b++)
    {
        cout<<"The "<<b+1<<" element : "<<Array[b]<<endl;
    }
}

void bubblesort(int Array[] , int i )
{
    int a;
    for(int next=1; next<i; next++)
    {
        a= Array[next];
        int movement=next;

        while((movement>0) && (Array[movement-1])>a)
        {
            Array[movement] =Array[movement-1];
            movement--;
        }
        Array[movement] =a;
    }

}
