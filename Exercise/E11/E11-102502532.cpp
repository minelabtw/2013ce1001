#include<iostream>
using namespace std;

int bubblesort(int [] , int );
int datas[20] = {};

int main()
{
    int Size =0;
    int data =0;

    cout<<"Enter array size (5-20): ";
    cin>>Size;
    while (Size<5 or Size>20)
    {
        cout<<"Out of range!\nEnter array size (5-20): ";
        cin>>Size;
    }
    for (int i =0; i<Size; i++)                  //從0開始 較好
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        cin>>data;
        while (data<0 or data>100)
        {
            cout<<"Out of range!\nThe "<<i+1<<" element (0-100): ";
            cin>>data;
        }
        datas[i] =data;
    }
    cout<<"Before sort";
    for (int i =0; i<Size; i++)
    {
        cout<<"\nThe "<<i<<" element: "<<datas[i];
    }
    cout<<"\nAfter sort";
    bubblesort(datas,Size);                //先表示 函式
    for (int i =0; i<Size; i++)
    {
        cout<<"\nThe "<<i<<" element: "<<datas[i];
    }

    return 0;
}

int bubblesort(int arr[] , int Size)              //[]裡面不用數字
{
    for(int round =0; round<Size-1; round++)               //round可少一次
    {
        for (int n =0; n<Size-1; n++)
        {
            if (arr[n] > arr[n+1])
                swap (arr[n],arr[n+1]);            //交換()裡兩數的位置
        }
    }

    /*int inSert =0;
        int moveitem =0;
        for (int n =2; n<=Size; n++)
        {
            inSert = datas[n];
            moveitem =n;
            while (moveitem>1 and datas[moveitem-1] >inSert)
            {
                datas[moveitem] = datas[moveitem-1];
                moveitem--;
            }
        }
        return datas[moveitem] = inSert;*/
}




