#include <iostream>
using namespace std;
void bubblesort(int x[],int y);

int main()
{
    int a=0;

    do
    {
        cout << "Enter array size (5-20): ";
        cin >> a;
        if(a<5 || a>20)
            cout << "Out of range!\n";
    }
    while(a<5 || a>20); //輸入陣列大小

    int b[20]= {0};

    for(int i=0; i<a; i++)
    {
        do
        {
            cout << "The "<< i+1  << " element (0-100): ";
            cin >> b[i];
            if(b[i]<1 || b[i]>99)
                cout << "Out of range!\n";
        }
        while(b[i]<1 || b[i]>99); //輸入陣列內的數
    }

    cout << "Before sort\n";
    for(int i=0; i<a; i++)
    {
        cout << "The "<< i+1  << " element: " << b[i] << endl; //輸出
    }

    bubblesort(b,a);
    cout << "After sort\n";
    for(int i=0; i<a; i++)
    {
        cout << "The "<< i+1  << " element: " << b[i] << endl; //輸出
    }

    return 0;
}

void bubblesort(int x[] , int y) //由小排到大
{
    for (int i=y-1; i>0; i--)
    {
        for (int j=0; j<i; j++)
        {
            if (x[j]>x[j+1])
                swap(x[j],x[j+1]);
        }
    }
}
