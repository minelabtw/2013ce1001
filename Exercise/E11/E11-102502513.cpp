//泡沫排序法
#include <iostream>
using namespace std;
int bubblesort(int* arr , int _size);

int main()
{
    int length=0;
    int value=0;
    int j=1;

    int arr[20]= {};

    while(length<5||length>20)
    {
        cout << "Enter array size (5-20): ";
        cin >> length;

        if(length<5||length>20)
            cout << "Out of range!\n";
    }

    for(int i=1; i<=length; i++)
    {
        do
        {

            cout << "The " << i << " element (0-100): ";
            cin >> value;

            if(value<0||value>100)
                cout << "Out of range!\n";
        }
        while(value<0||value>100);

        arr[j]=value;
        j++;
    }

    cout << "Before sort\n";
    for(int i=1; i<=length; i++)
    {
        cout << "The " << i << " element: " << arr[i] << "\n";
    }

    cout << "After sort\n";
    bubblesort(arr, length);  //呼叫函式
    for(int i=1; i<=length; i++)
    {
        cout << "The " << i << " element: " << arr[i] << "\n";
    }

    return 0;
}

int bubblesort(int* arr , int _size)  //在函式中 打入泡沫排序法
{
    for(int i=1; i<_size; i++)
    {
        for(int j=i+1; j<=_size; j++)
        {
            if(arr[i]>arr[j])
            {
                int x=arr[i];
                int y=arr[j];
                arr[i]=y;
                arr[j]=x;
            }
        }
    }
}
