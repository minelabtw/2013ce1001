//E11-102502026

#include<iostream>
using namespace std;
void bubblesort(int* ,int ); //use as prototype

int main()  //starts
{
    int as=0;   //define as
    int c=0;    //define counter
    int number=-1;  //define number
    int a[20];  //array

    while(as<5 or as>20)    //do it when as<5 or >20
    {
        cout<<"Enter array size (5-20): ";
        cin>>as;
        if (as<5 or as>20)
            cout<<"Out of range!\n";
    }
    while (c<as)    //do it when c<as
    {

        cout<<"The "<<c+1<<" element (0-100): ";
        cin>>number;
        while(number<0 or number>100)
        {
            cout<<"Out of range!\n";
            cout<<"The "<<c+1<<" element (0-100): ";
            cin>>number;
        }
        a[c]=number;
        c++;    // counter+1
    }
    cout<<"Before sort\n";
    for(int i=0; i<c; i++)
        cout<<"The "<<i+1<<" element: "<<a[i]<<endl;    //print numbers  bedore sort
    cout<<"After sort\n";
    bubblesort(a, as);
    for(int i=0; i<c; i++)
        cout<<"The "<<i+1<<" element: "<<a[i]<<endl;    //print numbers after sort
    return 0;
}

void bubblesort(int* a,int as)
{
    int tmp=0;
    for(int i=as-1; i>0; i--)
    {
        for(int j=0; j<as-1; j++)
        {
            if(a[j]>a[j+1])
            {
                tmp=a[j];
                a[j]=a[j+1];
                a[j+1]=tmp; //swap the numbers
            }
        }
    }
}
