#include <iostream>
using namespace std;

void bubblesort(int*,int);

int main()
{
    //call variables
    int arraysize=0;
    int number;
    int n=1; //counter

    //input and data validation for  array size
    do
    {
        cout<<"Enter array size (5-20): ";
        cin>>arraysize;
        if(arraysize<5||arraysize>20)
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(arraysize<5||arraysize>20);
    int a[arraysize];//call array

    //input and data validation for each elements
    do
    {
        do
        {
            cout<<"The "<<n<<" element (0-100): ";
            cin>>number;
            if(number<0||number>100)
            {
                cout<<"Out of range!"<<endl;
            }
        }
        while(number<0||number>100);
        a[n-1]=number; //save every input in array
        n++;
    }
    while(n<=arraysize);

    cout<<"Before sort"<<endl;
    for(int b=0; b<arraysize; b++)//display before sort
    {
        cout<<"The "<<b+1<<" element: "<<a[b]<<endl;
    }

    bubblesort(a,arraysize); //sorting

    cout<<"After sort"<<endl;
    for(int b=0; b<arraysize; b++)//display after sort
    {
        cout<<"The "<<b+1<<" element: "<<a[b]<<endl;
    }

    return 0;
}

void bubblesort(int* asort,int asize)//function for sorting
{
    while(asize>1)
    {
        for(int n=0; n<asize; n++)
        {
            int hold=asort[n];
            if(hold>asort[n+1])
            {
                asort[n]=asort[n+1];
                asort[n+1]=hold;
            }
        }
        asize--;
    }
}
