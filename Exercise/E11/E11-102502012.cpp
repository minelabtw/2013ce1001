#include<iostream>
using namespace std;
void bubblesort(int* array, int size){
	for (int i = 1; i <= size - 1; i++){
		for (int j = 1; j <= size - 1; j++){
			if (array[j] > array[j + 1]){ // sort by increasing order
				swap(array[j], array[j + 1]);
			}
		}
	}
}
int main(){
	int n;
	int S[21];
	do{
		cout << "Enter array size(5 - 20) : ";
		cin >> n;
	} while ((n > 20 || n < 5) && cout << "Out of range!" << endl);
	for (int i = 1; i <= n; i++){
		do{
			cout << "The " << i << " element(0 - 100) : ";
			cin >> S[i];
		} while ((S[i]>100 || S[i] < 0) && cout << "Out of range!" << endl);
	}
	cout << "Before sort" << endl;
	for (int i = 1; i <= n; i++){
		cout << "The " << i << " element: " << S[i] << endl;
	}
	bubblesort(S, n);
	cout << "After sort" << endl;
	for (int i = 1; i <= n; i++){
		cout << "The " << i << " element: " << S[i] << endl;
	}
	return 0;
}