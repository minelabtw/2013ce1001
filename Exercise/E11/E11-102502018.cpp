#include <iostream>
using namespace std;
int bubblesort(int* ,int );
int main()
{
    const int size0=20;
    int arr[size0]={};
    int size1=0,a=0,i=0;
    while(size1<5||size1>20)
    {
        cout<<"Enter array size (5-20): ";
        cin>>size1;
        if(size1<5||size1>20)
        {
            cout<<"Out of range!\n";
        }
    }
    while(i<size1)
    {
        cout<<"The "<<i+1<<" element (0-100): ";
        cin>>a;
        if(a<0||a>100)
        {
            cout<<"Out of range!\n";
        }
        else
        {
            arr[i]=a;
            i++;
        }
    }
    cout<<"Before sort\n";
    for(i=0;i<size1;i++)
    {
        cout<<"The "<<i+1<<" element (0-100): "<<arr[i]<<endl;
    }
    bubblesort(arr,size1);
    cout<<"After sort\n";
    for(i=0;i<size1;i++)
    {
        cout<<"The "<<i+1<<" element (0-100): "<<arr[i]<<endl;
    }
    return 0;
}
int bubblesort(int* arr,int size1)
{
    int p;
    for(int k=0;k<size1;k++)
    {
        for(int h=1;h<size1;h++)
        {
            p=arr[h-1];             //讓每一項帶入p
            if(p>arr[h])            //如果前一項大於後一項，前後交換
            {
             arr[h-1]=arr[h];
             arr[h]=p;
            }

            else;
        }
    }
}
