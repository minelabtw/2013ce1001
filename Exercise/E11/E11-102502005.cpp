#include<iostream>
using namespace std;

void bubblesort(int *arrayone, int showsize);       //函數prototype。
void showarray(int *arrayone, int showsize);

int main()
{
    const int _size = 21;                           //宣告20個陣列元素。
    int showsize;                                   //真正需要動到的陣列元素。
    int arrayone[_size] = {};

    cout << "Enter array size (5-20): " ;           //要求使用者輸入需要動到的陣列大小。
    cin >> showsize ;

    while(showsize<5 || showsize>20)                //若輸入數值不符範圍則要求重新輸入。
    {
        cout << "Out of range!" << endl << "Enter array size (5-20): ";
        cin >> showsize ;
    }

    for(int i=1; i<=showsize; i++)                  //利用for迴圈依序輸入每一個元素。
    {
        cout << "The " << i <<" element (0-100): ";
        cin >> arrayone[i];
        while(arrayone[i] < 0 || arrayone[i] > 100)
        {
            cout << "Out of range!" << endl << "The " << i <<" element (0-100): ";
            cin >> arrayone[i];
        }
    }

    cout << "Before sort" << endl ;
    showarray(arrayone, showsize);              //呼叫showarray函式印出每一個元素。

    bubblesort(arrayone, showsize);             //呼叫bubblesort函式排序。

    cout << "After sort" << endl;
    showarray(arrayone,showsize);               //再次呼叫showarray函式印出每一個元素。

    return 0;
}


void bubblesort(int *arrayone, int showsize)    //利用泡沫排序法排序。
{
    int _swap;
    for(int i=1; i<=showsize; i++)
    {
        for(int j=1; j<=showsize; j++)
        {
            if(arrayone[i]<arrayone[j])
            {
                _swap = arrayone[i];
                arrayone[i] = arrayone[j];
                arrayone[j] = _swap;
            }
        }
    }

}

void showarray(int *arrayone ,int showsize)   //利用for迴圈印出所有元素。
{
    for (int j=1; j<=showsize; j++)
    {
        cout << "The "<< j <<" element: " << arrayone[j] << endl ;
    }
}
