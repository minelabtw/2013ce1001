#include<iostream>
using namespace std;

void bubblesort(int* Array , int _size)   // bubblesort function
{
    int number=0;
    for(int i=0; i<_size; i++) // 跑行的部分
    {
        for(int j=0; j<_size; j++) // 一個一個比較
        {
            if(Array[j] > Array[j+1]) // 如果第一個比下一個大就換
            {
                number=Array[j];
                Array[j]=Array[j+1];
                Array[j+1]=number;
            }
        }
    }

}
int main()
{
    int ArraySize=0;
    int arr[100];       //宣告一個array
    int input=0;

    do  //輸入array的size
    {
        cout << "Enter array size (5-20): ";
        cin >> ArraySize;
        if (ArraySize<5 || ArraySize>20)  //如果超過範圍就重輸
            cout << "Out of range!" << endl;
    }
    while(ArraySize<5 || ArraySize>20);

    for(int i=0;i<ArraySize;i++)
    {
        do
        {
            cout << "The " << i+1 << " element (0-100): " ;
            cin >> input;
            if (input<0 || input>100)
                cout << "Out of range!" << endl;
        }
        while(input<0 || input>100);
        arr[i]=input;
    }

    cout << "Before sort" << endl;
    for(int j=0; j<ArraySize; j++)
    {
        cout << "The " << j+1 << " element: " << arr[j] << endl;
    }

    cout << "affter sort" << endl;

    bubblesort(arr, ArraySize);  //呼叫bubblesort的涵式

    for(int i=0;i<ArraySize;i++)
        cout << "The " << i+1 << " element: " << arr[i] << endl;  //由小到大排序出來


    return 0;
}
