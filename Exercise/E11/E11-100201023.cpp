#include <iostream>
#include <algorithm>

using namespace std;

void bubblesort(int* array , int size);

int main()
{
	int size , *number , count , temp;

	while(true) // input array size
	{
		cout << "Enter array size (5-20): ";
		cin >> size;
		if(size > 4 && size < 21)
			break;
		cout << "Out of range!" << endl;
	}

	number = new int[size];

	count = 0;
	while(count < size)
	{
		cout << "The " << count + 1 << " element (0-100): ";
		cin >> temp; // input value
		
		if(temp < 0 || temp > 100) // determine out of range
		{
			cout << "Out of range!" << endl;
			continue;
		}
		number[count] = temp; // store the value
		++count;
	}

	cout << "Before sort" << endl; // output element before sort
	for(int i = 0 ; i < size ; ++i)
		cout << "The " << i + 1 << " element: " << number[i] << endl;

	bubblesort(number , size); // call bubblesort function

	cout << "After sort" << endl; // output element after sort
	for(int i = 0 ; i < size ; ++i)
		cout << "The " << i + 1 << " element: " << number[i] << endl;

	return 0;
}

void bubblesort(int* array , int size) // implement bubblesort
{
	for(int i = size - 1 ; i > 0 ; --i)
		for(int j = 0 ; j < i ; ++j)
			if(array[j] > array[j + 1])
				swap(array[j] , array[j + 1]);
}
