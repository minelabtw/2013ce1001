#include <iostream>
using namespace std;

int arraysize;//定義陣列大小
int a[20];//定義陣列並使其大小最大值為20
int element;//定義陣列元素
int exchange1;//定義交換所需的兩個數
int exchange2;
int bubblesort(int [],int);//定義排列大小的函數

int main()
{
    cout << "Enter array size (5-20): ";//輸出字串
    cin >> arraysize;//輸入陣列大小
    while ( arraysize<5 or arraysize>20 )//利用迴圈來限制範圍
    {
        cout << "Out of range!" << endl;//輸出字串並重新輸入
        cout << "Enter array size (5-20): ";
        cin >> arraysize;
    }

    for(int i=1; i<=arraysize; i++)//輸入陣列元素
    {
        cout << "The " << i << " element (0-100): ";//輸出字串
        cin >> element;

        while ( element>100 or element<0 )//利用迴圈使元素在範圍內
        {
            cout << "Out of range!" << endl;
            cout << "The " << i << " element (0-100): ";
            cin >> element;
        }

        a[i]=element;//紀錄陣列
    }

    cout << "Before sort" << endl;//輸出字串及轉換前陣列
    for (int j=1; j<=arraysize; j++)
    {
        cout << "The " << j << " element: " << a[j] << endl;
    }

    bubblesort(a,arraysize);//呼叫函式

    cout << "After sort" << endl;//輸出字串及轉換後陣列
    for (int k=1; k<=arraysize; k++)
    {
        cout << "The " << k << " element: " << a[k] << endl;
    }

    return 0;//返回初始值

}

int bubblesort(int x[],int _size)//定義排列大小函示
{
    for (int l=_size; l>0; l--)//定義第一個迴圈
    {
        for (int m=1; m<=l-1; m++)//定義第二個迴圈
        {
            if (x[m]>x[m+1])//當前一個數大於後一個數時，互換
            {
                exchange1=x[m];
                exchange2=x[m+1];
                x[m]=exchange2;
                x[m+1]=exchange1;
            }
        }
    }
}
















