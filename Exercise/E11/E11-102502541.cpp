#include<iostream>
using namespace std;
void print(int arr[],int arraysize)//照順序印出
{
    for(int i=0; i<arraysize; i++)
    {
        cout << "The " << i+1 << " element: " << arr[i] << endl;
    }
}
void swap(int *const address1,int *const address2)//排列
{
    int temp= *address1;
    *address1= *address2;
    *address2= temp;
}
void bubblesort(int* _array , int _size)//比大小儲存
{
    int minium = 0;
    for(int i=0; i<_size-1; i++)
    {
        minium = i;
        for(int start=i+1; start<_size; start++)
        {
            if(_array[start]<_array[minium])
                minium=start;
        }
        swap(&_array[i] ,&_array[minium]);
    }
}
int main()
{
    int asize = 0;
    int i = 0;
    int number = 0;
    int save[20]={};
    do
    {
        cout << "Enter array size (5-20): ";
        cin >> asize;
        if(asize<5 || asize>20)
            cout << "Out of range!" << endl;
    }
    while(asize<5 || asize>20);
    do
    {
        cout << "The " << i+1 <<" element (0-100): ";
        cin >> number;
        if(number>=0 && number<=100)
        {
            save[i]=number;
            i++;
        }
        else
            cout << "Out of range!" << endl;
    }
    while(i<asize);
    cout << "Before sort" << endl;
    print(save,asize);//印出未排列數
    cout << "After sort" << endl;
    bubblesort(save,asize);//排列
    print(save,asize);//印出排列數
    return 0;
}
