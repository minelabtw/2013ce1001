#include <iostream>
using namespace std;

void bubblesort(int* array , int size);

int main()
{
    int size=0;

    cout<<"Enter array size (5-20): ";    //輸入字串
    cin>>size;

    while(size<5 or size>20)             //判斷範圍
    {
        cout<<"Out of range!"<<endl;
        cout<<"Enter array size (5-20): ";
        cin>>size;
    }


    int arr[size];                       //陣列初始化
    for(int i=0; i<size; i++)
        arr[i]=-1;

    for(int i=0; i<size; i++)
    {
        int num1;
        cout<<"The "<<i+1<<" element (0-100): ";     //判斷範圍
        cin>>num1;
        while(num1<0 or num1>100)
        {
            cout<<"Out of range!"<<endl;
            cout<<"The 1 element (0-100): ";
            cin>>num1;
        }
        arr[i]=num1;                    //儲存陣列
    }

    cout<<"Before sort"<<endl;          //顯示陣列
    for(int i=0; i<size; i++)
    {
        cout<<"The "<<i+1<<" element: "<<arr[i]<<endl;
    }

    cout<<"After sort"<<endl;
    bubblesort(arr,size);               //呼叫函式
    for(int i=0; i<size; i++)
    {
        cout<<"The "<<i+1<<" element: "<<arr[i]<<endl;
    }
    return 0;
}

void bubblesort(int* arr , int size)
{
    int c=0;
    for(int i=0; i<size; i++)
    {
        for(int i=0; i<size; i++)     //若前一項大於後一項即交換
        {
            c=arr[i];
            if(arr[i]<=arr[i+1]);
            else if(arr[i]>arr[i+1])
            {
                c=arr[i+1];
                arr[i+1]=arr[i];
                arr[i]=c;
            }
        }
    }
}

