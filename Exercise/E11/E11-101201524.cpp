#include<iostream>
using namespace std;

void bubblesort(int* array , int size)//宣告一涵式將陣列中的數字從第一項到倒數第i項跟自己的下一項比大小，大的排到右邊
{
    for(int i=size-1;i>0;i--)
    {
        for(int j=0;j<i;j++)
        {
            if(array[j]>array[j+1])
                swap(array[j],array[j+1]);
        }
    }
}

int main()
{
    int i,L,size,array[20]={};//宣告變數i用在for迴圈,L用來記錄輸入的數,size用來代表輸入的項數,和一個大小為20的陣列

    do//輸出字串要求輸入項數的大小若不符條件則輸出"Out of range!"並要求重新輸入
    {
        cout << "Enter array size (5-20): ";
        cin >> size;
        if(size>20 || size<5)
            cout << "Out of range!\n";
    }while(size>20 || size<5);
    for(i=0;i<size;i++)//按照順紀錄輸入的數字到陣列裡若不符條件則輸出"Out of range!"並要求重新輸入
    {
        do
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> L;
            if(L<0 || L>100)
                cout << "Out of range!\n";
        }while(L<0 || L>100);
        array[i]=L;
    }
    cout << "Before sort\n";//將輸入的紀錄輸出
    for(i=0;i<size;i++)
        cout << "The " << i+1 << " element: " << array[i] << "\n";

    bubblesort(array,size);//用涵式作排列

    cout << "After sort\n";//按照大小輸出數字
    for(i=0;i<size;i++)
        cout << "The " << i+1 << " element: " << array[i] << "\n";

    return 0;
}
