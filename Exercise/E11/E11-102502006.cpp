#include <iostream>

using namespace std;

void bsort(int b[],int _size)
{
    int tmp=0;
    for(int i=_size; i>0; i--)
    {
        for(int j=0; j<i-1; j++)
        {
            if(b[j]>b[j+1])
            {
                tmp=b[j];
                b[j]=b[j+1];
                b[j+1]=tmp;
            }
        }
    }
}

int main()
{

    int length = 0; //陣列大小
    int a[100] = {}; //陣列

    while(length<5 || length>20) // 輸入大小
    {
        cout << "Enter array size (5-20): ";
        cin >> length;
        if(length<5 || length>20)cout << "Out of range!\n";
    }

    for(int i=0; i<length; i++) // 輸入數字
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin >> a[i];
        if(a[i]<0 || a[i]>100)
        {
            cout << "Out of range!\n";
            i--;
        }
    }

    cout << "Before sort\n";
    for(int i=0; i<length; i++) // 排序前
    {
        cout << "The " << i+1 << " element: " << a[i] << endl;
    }

    bsort(a,length);

    cout << "After sort\n";
    for(int i=0; i<length; i++) // 排序後
    {
        cout << "The " << i+1 << " element: " << a[i] << endl;
    }

    return 0;
}
