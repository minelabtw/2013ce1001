//============================================================================
// Name        : E11-102502038.cpp
// Author      : catLee
// Version     : 0.1
// Description : NCU CE1001 E11
//============================================================================
#include <iostream>

using namespace std;

void bubblesort(int*,int);
void printArr(int*,int);

int main(void){
  int input,itemInput;
  while(true){
    cout << "Enter array size (5-20): ";
    cin >> input;
    if(input <= 20&&input >= 5){
      break;
    }
    cout << "Out of range!\n";
  }
  int arra[input];
  for(int i = 0;i<input;i++){
    while(true){
      cout << "The " << (i+1) << " element (0-100): ";
      cin >> itemInput;
      if(itemInput <= 100 && itemInput >=0){
	break;
      }
      cout << "Out of range!\n";
    }
    arra[i] = itemInput;
  }
  cout << "Before sort\n";
  printArr(arra,input);
  bubblesort(arra,input);
  cout << "Afteru sort\n";
  printArr(arra,input);
}
void bubblesort(int *arr,int length){
  int temp;
  for(int i=length;i>1;i--){
    for(int j = 0;j<i-1;j++){
      if(arr[j]>arr[j+1]){
	temp = arr[j];
	arr[j] = arr[j+1];
	arr[j+1] = temp;
      }
    }
  }
}
void printArr(int *arr,int length){
  for(int i =0;i<length;i++){
    cout << "The " << i+1 << " element: " << arr[i] << "\n";
  }
}
