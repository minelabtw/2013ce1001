#include <iostream>
using namespace std;

void bubblesort(int* _array , int _size);

int main()
{
    int arraysize;
    cout <<"Enter array size (5-20): ";
    cin >>arraysize;
    while (arraysize<5||arraysize>20)
    {
        cout <<"Our of range!\nEnter array size (5-20): ";
        cin >>arraysize;
    }
    int arr[arraysize];
    for (int i=0; i<arraysize; i++)
    {
        cout <<"The "<<i+1<<" element (0-100): ";
        cin >>arr[i];
        if (arr[i]<0||arr[i]>100)
        {
            cout <<"Out of range!\nThe "<<i+1<<" element (0-100): ";
            cin >>arr[i];
        }
    }
    cout <<"Before sort"<<endl;
    for (int j=0; j<arraysize; j++)
        cout <<"The "<<j+1<<" element: "<<arr[j]<<endl;

    bubblesort(arr,arraysize);

    cout <<"After sort"<<endl;
    for (int j=0; j<arraysize; j++)
        cout <<"The "<<j+1<<" element: "<<arr[j]<<endl;

    return 0;
}
void bubblesort(int* _array , int _size)
{
    int hold;
    for (int k=0; k<_size-1; k++)
    {
        for (int l=0; l<_size; l++)
        {
            if (_array[l]>_array[l+1])
            {
                hold=_array[l+1];
                _array[l+1]=_array[l];
                _array[l]=hold;
            }
        }
    }
}

