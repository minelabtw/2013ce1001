#include<iostream>

using namespace std;

int bubblesort(int *array,int ArraySize)
{
    int temp;
    for(int h=ArraySize; h>1; h--)    //總共找h-1次可把整個陣列依大小排列完整
    {
        for(int i=1; i<h;i++)         //找每一次的最大數
        {
            if(array[i]>array[i+1])   //若比下一個大 則替換
            {
                temp=array[i];
                array[i]=array[i+1];
                array[i+1]=temp;
            }
        }
    }
}

int main()
{
   int ArraySize=0;
   int array[ArraySize];                      //宣告陣列array存放值



   cout<<"Enter array size (5-20): ";         //array存放的數為5~20個之間
   cin>>ArraySize;
   while(ArraySize<5 || ArraySize>20)
   {
       cout<<"Out of range!"<<endl;
       cout<<"Enter array size (5-20): ";
       cin>>ArraySize;
   }
   for(int i=1;i<=ArraySize;i++)
   {
       cout<<"The "<<i<<" element (0-100): ";
       cin>>array[i];
       while(array[i]<0 || array[i]>100)     //arrary[i]的值為0~100之間
       {
           cout<<"Out of range!"<<endl;
           cout<<"The "<<i<<" element (0-100): ";
           cin>>array[i];
       }

   }
   cout<<"Before sort"<<endl;
   for(int i=1;i<=ArraySize;i++)
   {
       cout<<"The "<<i<<" element: "<<array[i]<<endl;
   }
   cout<<"After sort"<<endl;
   bubblesort(array,ArraySize);             //使用bubblesort的副程式
   for(int i=1;i<=ArraySize;i++)
   {
       cout<<"The "<<i<<" element: "<<array[i]<<endl;
   }

return 0;
}
