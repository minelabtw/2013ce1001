#include<iostream>


using namespace std;

void bubblesort(int Array[] , int Size)//用氣泡排序法排列輸入數字
{
    int exchange=Size-1;
    while(exchange)
    {
        int bound=exchange;
        exchange=0;
        for(int i=0; i<bound; i++)
        {
            if(Array[i]>Array[i+1])
            {
                int a=0;
                a=Array[i];
                Array[i]=Array[i+1];
                Array[i+1]=a;
                exchange=i;
            }
        }
    }
}
int main()
{
    int Size=0;
    cout << "Enter array size (5-20):" ;
    cin >> Size;
    while (Size<5 || Size>20)
    {
        cout << "Out of range!" << endl;
        cout << "Enter array size (5-20):" ;
        cin >> Size;
    }
    int Array[Size];
    for(int i=0; i<Size; i++)//讓使用者依序輸入數字
    {
        while(1)
        {
            cout << "The "<< i+1 << " element (0-100): ";
            cin >> Array[i];
            if(Array[i]>0 && Array[i]<100)
            {
                break;
            }
            cout << "Out of range!" << endl;
            cout << "The "<< i+1 << " element (0-100): ";
            cin >> Array[i];
        }
    }
    cout << "Before sort" <<endl;
    for(int i=0; i<Size; i++)//按照輸入的順序依序輸出輸入的數字
    {
        cout << "The " << i+1 << " element: " << Array[i] << endl;
    }
    bubblesort(Array,Size);//排列
    cout << "After sort" <<endl;
    for(int i=0; i<Size; i++)//輸出已排列的數字
    {
        cout << "The " << i+1 << " element: " << Array[i] << endl;
    }
}
