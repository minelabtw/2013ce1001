#include <iostream>
using namespace std;
void bubblesort(int* array1 , int sizeofarray)
{
    for(int i=0 ; i<sizeofarray ; i++)
        for( int j=0 ; j<sizeofarray-1 ; j++)
        {
            if(array1[j]>array1[j+1])
                swap(array1[j],array1[j+1]);
        }
}//set up a function to sort the array
int main()
{
    int array1[20]= {};//the array saves at most 20 elements
    int length;//the number of the element
    do
    {
        cout << "Enter array size (5-20): ";
        cin >> length;
        if(length<5 or length>20)
            cout << "Out of range!" << endl;
    }
    while(length<5 or length>20);//loop until the correct number is input
    for(int i=0 ; i<length ; i++)
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin >> array1[i];
        if(array1[i]<0 or array1[i]>100)
        {
            cout << "Out of range!" << endl;
            i--;
            continue;
        }
    }//let the user input the value and loop to assure that the value is in the range
    cout << "Before sort" << endl;
    for(int i=0 ; i<length ; i++)
        cout << "The " << i+1 << " element: " << array1[i] << endl;
    //print the array before the array have been sorted
    bubblesort(array1,length);//sort
    cout << "After sort" << endl;
    for(int i=0 ; i<length ; i++)
        cout << "The " << i+1 << " element: " << array1[i] << endl;
    //sort the array elements from small to big
    return 0;
}
