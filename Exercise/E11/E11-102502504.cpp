#include <iostream>
#define n 20
using namespace std;

void bubblesort(int bubble[n] , int a);

int main()
{
    int a; //array size
    int element; //輸入的數值
    int bubble[n];
    int j=0; //用來存放數值的變數

    cout << "Enter array size (5-20): ";
    cin >> a;

    while(a<5||a>20) //利用while使array size介於5~20之間
    {
        cout << "Out of range!" << endl;
        cout << "Enter array size (5-20): ";
        cin >> a;
    }

    for(int i=1; i<=a; i++) //用來依序顯示element
    {
        cout << "The " << i << " element (0-100): ";
        cin >> element;
        while(element<0||element>100) //使數值介於0~100之間
        {
            cout << "Out of range!" << endl;
            cout << "The " << i << " element (0-100): ";
            cin >> element;
        }
        bubble[j]=element; //將數值存放至bubble[]內
        j++;
    }

    cout << "Before sort" << endl;
    for(int k=1; k<=a; k++) //用來依序顯示排序之前的樣貌
    {
        cout << "The " << k << " element (0-100): " << bubble[k-1] << endl;
    }

    cout << "After sort" << endl;
    bubblesort(bubble,a);

    return 0;
}


void bubblesort(int bubble[n] , int a)
{
    int w; //用來暫存
    for(int i=1; i<a; i++) //第幾次排序
    {
        for(int x=0; x<=a-2; x++) //將數字兩兩比較大小，若左大於右則將數字對調
        {
            if(bubble[x]>bubble[x+1])
            {
                w=bubble[x];
                bubble[x]=bubble[x+1];
                bubble[x+1]=w;
            }
        }
    }
    for(int k=1; k<=a; k++) //顯示排序後的樣貌
    {
        cout << "The " << k << " element (0-100): " << bubble[k-1] << endl;
    }

}
