#include <iostream>
#include <cstdlib>
using namespace std ;
void print(int *,int) ; //輸出
void bubble_sort(int *,int) ; //氣泡排序法
int main()
{
    int length ;

    do
    {
        cout << "Enter array size (5-20): " ;
        cin >> length ;
        if(length<5 || length>20)
            cout << "Out of range!\n" ;
    }
    while(length<5 || length>20) ;
    // 輸入陣列長度
    int number[20]= {};
    for(int i=0 ; i<length ; i++)
    {
        cout << "The " << i+1 << " element (0-100): " ;
        cin >> number[i] ;
        if(number[i]<0 || number[i]>100)
        {
            cout << "Out of range!\n" ;
            i-- ;
        }
    }
    // 輸入陣列內容 *length 個

    cout << "Before sort\n" ;
    print(number,length) ; //輸出
    bubble_sort(number,length) ;
    cout << "After sort\n" ;
    print(number,length) ; //輸出
    return 0 ;
}
void print(int num[],int length)  //輸出
{
    for(int i=0 ; i<length ; i++)
        cout << "The " << i+1 <<" element: " << num[i] << endl ;
}
void bubble_sort(int num[],int length)  //氣泡排序法
{
    for(int i=length ; i>=0 ; i--)
    {
        for(int j=0 ; j<length-1 ; j++)
            if(num[j]>num[j+1])
                swap(num[j],num[j+1]) ;
    }
}
