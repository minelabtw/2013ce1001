#include <iostream>
using namespace std;

void bubblesort(int* array,int size){//bubblesort
    int i,j;//迴圈用變數
    for(i=0;i<size;i++){
        for(j=1;j<size-i;j++){
            if(array[j] < array[j-1])
                swap(array[j],array[j-1]);
        }
    }
}

int main(){
    int i,L,array[20];//宣告整數,i為迴圈用變數,L為陣列長度,array為儲存輸入數字的陣列
    do{
        cout << "Enter array size (5-20): ";
        cin >> L;
    }while((L<5 || L>20) && cout << "Out of range!\n");//處理輸入的陣列長度
    for(i=0;i<L;i++){
        do{
            cout << "The " << i+1 << " element (0-100): ";
            cin >> array[i];
        }while((array[i]<0 || array[i]>100) && cout << "Out of range!\n");//處理輸入的數字
    }
    cout << "Before sort\n";//輸出排序前
    for(i=0;i<L;i++){
        cout << "The " << i+1 << " element: " << array[i] << endl;
    }
    bubblesort(array,L);//丟入函式排序
    cout << "After sort\n";//輸出排序後
    for(i=0;i<L;i++){
        cout << "The " << i+1 << " element: " << array[i] << endl;
    }
    return 0;
}
