#include <iostream>

using namespace std ;

int bubblesort (int* array , int size) ;          //function prototype
int main ()
{
    int arraysize = 5 ;                           //declaration and intialization

    int element = 0 ;
    do                                            //輸入陣列數
    {
        cout << "Enter array size (5-20): " ;
        cin >> arraysize ;
    }
    while ((arraysize > 20 || arraysize < 5) && cout << "Out of range!" << endl) ;

    int array [arraysize] ;

    for (int a = 0 ; a < arraysize ; a ++)       //輸入陣列裡的元素
    {
        do
        {
            cout << "The " << a + 1 << " element (0-100): " ;
            cin >> element ;
        }
        while ((element < 0 || element > 100) && cout << "Out of range!" << endl) ;
        array [a] = element ;
    }

    cout << "Before sort" << endl ;              //輸出未排序前的陣列
    for (int b = 0 ; b < arraysize ; b ++)
    {
        cout << "The " << b + 1 << " element: " << array [b] << endl ;
    }

    cout << "After sort" << endl ;               //輸出排序後的陣列
    bubblesort (array,arraysize) ;
    for (int c = 0 ; c < arraysize ; c ++)
    {
        cout << "The " << c + 1 << " element: " << array [c] << endl ;
    }

    return 0 ;
}
int bubblesort (int* array , int size)           //函式內容(排序)
{



    for (int next = 1 ; next < size ; next ++)
    {
        int insertion = 0 ;
        int moveItem = 0 ;
        insertion = array [next] ;
        moveItem = next ;
        while ((moveItem > 0) && (array [moveItem - 1] > insertion))
        {
            array [moveItem] = array [moveItem - 1] ;
            moveItem -- ;
        }
        array [moveItem] = insertion ;
    }
}
