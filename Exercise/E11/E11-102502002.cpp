#include <iostream>
using namespace std;

void bubblesort(int* array , int size);

int main()
{
    int el=0;          //element
    int _size=0;       //陣列長度

    cout << "Enter array size (5-20): ";     //輸出字串
    cin >> _size;                            //輸入陣列的長度(5-20)
    while(_size>20 || _size<5)
    {
        cout << "Out of range!\n" << "Enter array size (5-20): ";
        cin >> _size;
    }

    int arr[_size];    //陣列
    for(int i=0; i<_size; i++)      //初始化
        arr[i]=-1;

    for(int i=0; i<_size; i++)
    {
        cout << "The " << i+1 << " element (0-100): ";               //輸入元素(0-100)
        cin >> el;
        while(el<=0 || el>100)
        {
            cout << "Out of range!\n" << "The " << i+1 << " element (0-100): ";
            cin >> el;
        }
        arr[i]=el;
    }

    cout << "Before sort\n";
    for(int i=0; i<_size; i++)
        cout << "The " << i+1 << " element: " << arr[i] << endl;            //輸出未排序前的結果

    bubblesort(arr,_size);                       //呼叫函式
    cout << "After sort\n";
    for(int i=0; i<_size; i++)
        cout << "The " << i+1 << " element: " << arr[i] << endl;            //輸出排序後的結果

    return 0;
}

void bubblesort(int* array , int size)           //比較元素大小並排序
{
    int a=0;
    for(int i=0; i<size; i++)
    {
        for(int i=0; i<size; i++)
        {
            if(array[i]<=array[i+1]);
            else if(array[i]>array[i+1])
            {
                a=array[i+1];
                array[i+1]=array[i];
                array[i]=a;
            }
        }
    }
}
