#include <iostream>
#include <vector>

using namespace std;
void bubblesort(int* array, int size)//a sort method
{
    for(int i=size; i>0; --i)
    {
        for(int j=0; j<i-1; ++j)
        {
            if(array[j]>array[j+1])
            {
                swap(array[j],array[j+1]);
            }
        }
    }
}
int main()
{
    int l=0;
    while(l<5||l>20)
    {
        cout << "Enter array size (5-20): ";
        cin >> l;
        if(l<5||l>20)
            cout << "Out of range!" << endl;
    }
    int arr[l];
    for(int i=0; i<l; ++i)
    {
        int temp=-1;
        while(temp<0||temp>100)
        {
            cout << "The " << i+1 << " element (0-100): ";
            cin >> temp;
            if(temp<0||temp>100)
                cout << "Out of range!" << endl;

        }
        arr[i]=temp;
    }
    cout << "Before sort" << endl;
    for(int i=0; i<l; ++i)
    {
        cout << "The " << i+1 << " element: " << arr[i] << endl;
    }
    bubblesort(arr,l);//sort this array
    cout << "After sort" << endl;
    for(int i=0; i<l; ++i)
    {
        cout << "The " << i+1 << " element: " << arr[i] << endl;
    }
    return 0;
}
