#include <iostream>

using namespace std;

int bubblesort(int* _array , int _size);//bubblesort function to arrange the inputs ascendantly.

int main()
{
    int L;

    do//ensure that the input in the arrangement.
    {
        cout << "Enter array size (5-20): " ;
        cin  >> L;
        if (L>20 || L<5)
        {
            cout << "Out of range!" << endl;
        }
    }
    while (L>20 || L<5);

    int arr[L];

    for (int i=0; i<L; i++)//use for to cin the numbers
    {
        cout << "The " << i+1 << " element (0-100): " ;
        cin  >> arr[i];
        while (arr[i]<0 || arr[i]>100)//use while to ensure the inputs in the arrangement
        {
            cout << "Out of range!" << endl;
            cout << "The " << i+1 << " element (0-100): " ;
            cin  >> arr[i];
        }
    }

    cout << "Before sort" << endl;

    for (int j=0; j<L; j++)//use for to cout all the inputs.
    {
        cout << "The " << j+1 << " element: " << arr[j] << endl;
    }

    bubblesort(arr,L);//call the bubblesort function.

    cout << "After sort" << endl;

    for (int m=0; m<L; m++)//use for to cout all the inputs after the arrangement.
    {
        cout << "The " << m+1 << " element: " << arr[m] << endl;
    }



    return 0;
}

int bubblesort(int* _array , int _size)
{
    int hold;
    for (int l=0; l<_size-1; l++)//perform size-1 time (at most)
    {
        for (int k=0; k<_size; k++)//第一個數字和後面的數字相比
        {
            if (_array[k]>_array[k+1])//如果前面數字大於後面，則對調
            {
                hold=_array[k+1];
                _array[k+1]=_array[k];
                _array[k]=hold;
            }
        }
    }
}

