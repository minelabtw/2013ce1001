#include<iostream>
#include<algorithm>

using namespace std;

void bubblesort(int *array , int &size);

void bubblesort(int *array , int &size)
{
    for(int j=0 ; j<size-1 ;j++)                             //將陣列從第一個比較，比較"size-1"次
    {
        int n=0;

        while(n<size-1)
        {
            if(array[n]>array[n+1])
            {
                swap(array[n],array[n+1]);
            }
            n++;
        }
    }
}

int main()
{
    int Size=0;
    int array[Size];

    cout << "Enter array size (5-20): ";
    cin >> Size;                                                                         //輸入陣列的大小
    while(Size<5 || Size>20)
    {
        cout << "Out of range!" << endl;
        cout << "Enter array size (5-20): ";
        cin >> Size;
    }

    for(int i=0; i<Size ; i++)
    {
        cout << "The " << i+1 << " element (0-100): ";
        cin >> array[i];
        while(array[i]<0 || array[i]>100)
        {
            cout << "Out of range!" << endl;
            cout << "The " << i+1 << " element (0-100): ";
            cin >> array[i];
        }
    }

    cout << "Before sort" << endl;
    for(int i=0; i<Size ; i++)                                                            //輸出輸入的陣列
    {
        cout << "The " << i+1 << " element (0-100): " << array[i] << endl;
    }

    bubblesort(array,Size);                                                               //將陣列由小到大重新排列

    cout << "After sort" << endl;
    for(int i=0; i<Size ; i++)                                                            //輸出重新排列的陣列
    {
        cout << "The " << i+1 << " element (0-100): " << array[i] << endl;
    }

    return 0;
}
