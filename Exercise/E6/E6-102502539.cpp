#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int i;
    int a1 = 1;
    int a2 = 0;
    int d;
    int r;
    int n;
    int sum;

    cout << "Calculate the progression.\n"  << "Choose the mode.\n"
         << "Arithmetic progression=1.\n"   << "Geometric progression=2.\n"
         << "Fibonacci sequence=3.\n"       << "Exit=-1.\n"
         << "Enter mode: " ;

    cin >> i;

    while ( i != -1 )
    {
        switch ( i )
        {
            case 1:
                cout << "Arithmetic progression" << endl;
                cout << "a1: " ;
                cin >> a1;
                while ( a1 < 1 )
                {
                    cout << "Out of range!\n" << "a1: " ;
                    cin >> a1;
                }
                cout << "d: ";
                cin >> d;
                while ( d < 1 )
                {
                    cout << "Out of range!\n" << "d: " ;
                    cin >> d;
                }
                cout << "n: ";
                cin >> n;
                while ( n < 1 )
                {
                    cout << "Out of range!\n" << "n: " ;
                    cin >> n;
                }
                for ( sum = 1 ; sum <= n ; sum++ )
                {
                    if ( sum == n )
                        cout << a1 << endl;
                    else
                        cout << a1 << ",";
                    a1 = a1 + d;
                }
                break;

            case 2:
                cout << "Geometric progression" << endl;
                cout << "a1: " ;
                cin >> a1;
                while ( a1 < 1 )
                {
                    cout << "Out of range!\n" << "a1: " ;
                    cin >> a1;
                }
                cout << "r: ";
                cin >> r;
                while ( r < 1 )
                {
                    cout << "Out of range!\n" << "r: " ;
                    cin >> r;
                }
                cout << "n: ";
                cin >> n;
                while ( n < 1 )
                {
                    cout << "Out of range!\n" << "n: " ;
                    cin >> n;
                }
                for ( sum = 1 ; sum <= n ; sum++ )
                {
                    if ( sum == n )
                        cout << a1 << endl;
                    else
                        cout << a1 << ",";
                    a1 = a1 * r;
                }
                break;

            case 3:
                cout << "Fibonacci sequence" << endl;
                cout << "n: ";
                cin >> n;
                while ( n < 1 )
                {
                    cout << "Out of range!\n" << "n: " ;
                    cin >> n;
                }
                a1 = 1;
                for ( int z = 1 ; z <= n ; z++ )
                {
                    if ( z != n )
                        cout << a1 << ",";
                    else
                        cout << a1 << endl ;
                    a1 = a1 + a2;
                    a2 = a1 - a2;
                }
            break;

        default:
            cout << "Illegal input!\n" ;
        }
        cout << "Enter mode: " ;
        cin >> i;
    }
    return 0;
}
