#include <iostream>
using namespace std;
int main()
{
    cout << "Calculate the progression."<<endl;
    cout << "Choose the mode."<<endl;
    cout << "Arithmetic progression=1."<<endl;
    cout << "Geometric progression=2."<<endl;
    cout << "Fibonacci sequence=3."<<endl;
    cout << "Exit=-1."<<endl;

    int entermode=1;
    int a1;
    int d;
    int r;
    int n;

    while(entermode!=-1)
    {
        cout << "Enter mode: ";
        cin>>entermode;

        switch(entermode)
        {
        case 1:
        {
            cout << "Arithmetic progression"<<endl;
            cout <<"a1: ";
            cin >> a1;
            while(a1<1)
            {
                cout << "Out of range!"<<endl;
                cout <<"a1: ";
                cin >> a1;
            }
            cout << "d: ";
            cin >> d;

            cout << "n: ";
            cin >> n;
            while(n<1)
            {
                cout << "Out of range!"<<endl;
                cout <<"n: ";
                cin >> n;
            }

            int a;
            int b=1;
            while(b<=n)
            {
                a=a1+d*(b-1);
                cout << a;
                if(b==n)
                {
                    break;
                }
                cout << ",";
                b++;
            }
            cout << endl;
            break;
        }

        case 2:
        {
            cout << "Geometric progression"<<endl;
            cout <<"a1: ";
            cin >> a1;
            while(a1<1)
            {
                cout << "Out of range!"<<endl;
                cout <<"a1: ";
                cin >> a1;
            }
            cout << "r: ";
            cin >> r;

            cout << "n: ";
            cin >> n;
            while(n<1)
            {
                cout << "Out of range!"<<endl;
                cout <<"n: ";
                cin >> n;
            }

            int c=2;
            int e=a1;

            cout << a1;
            if(n!=1)
            {
                cout<< ",";
            }
            while(c<=n)
            {
                e=e*r;
                cout <<e;
                if(c==n)
                {
                    break;
                }
                cout << ",";
                c++;
            }
            cout <<endl;
            break;
        }

        case 3:
        {
            cout << "Fibonacci sequence"<<endl;

            cout << "n: ";
            cin >> n;
            while(n<1)
            {
                cout << "Out of range!"<<endl;
                cout <<"n: ";
                cin >> n;
            }

            int x=1;
            int y=1;
            int z=2;
            int f=1;

            while(f<=n/3)
            {
                cout << x << ",";
                x=y+z;
                cout << y << ",";
                y=x+z;
                cout << z;
                if(f*3==n)
                {
                    break;
                }
                cout << ",";
                z=x+y;
                f++;
            }
            if(n%3==1||n%3==2)
            {
                x==y+z;
                cout<<x;
                if(n%3==2)
                {
                    cout << ",";
                }
            }
            if(n%3==2)
            {
                y==z+x;
                cout << y;
            }
            cout <<endl;
            break;
        }

        case -1:
        {
            break;
        }

        default:
            cout << "Illegal input!"<<endl;
        }
    }
}

