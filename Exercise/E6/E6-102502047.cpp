#include <iostream>

using namespace std;

int main()
{
    int a=0;
    int a1=0,d=0,n=0,r=0,a2=0,a3=1,k=0;
    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode: ";
    cin>>a;
    for(; a!=-1;)
    {
        for(; a!=1&&a!=2&&a!=3;)
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>a;
        }
        switch(a)
        {
        case 1:
            cout<<"Arithmetic progression"<<endl;
            cout<<"a1: ";
            cin>>a1;
            for(; a1<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            for(; d<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"d: ";
                cin>>d;
            }
            cout<<"n: ";
            cin>>n;
            for(; n<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            cout<<a1;
            for(int x=1; x<n; x++)
            {
                int y=a1+x*d;
                cout<<","<<y;
            }

            break;
        case 2:
            cout<<"Geometric progression"<<endl;
            cout<<"a1: ";
            cin>>a1;
            for(; a1<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"r: ";
            cin>>r;
            for(; r<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"r: ";
                cin>>r;
            }
            cout<<"n: ";
            cin>>n;
            for(; n<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            cout<<a1;
            for(int x=1; x<n; x++)
            {
                a1=a1*r;
                cout<<","<<a1;
            }
            break;
        case 3:
            cout<<"n: ";
            cin>>n;
            for(; n<=0;)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            cout<<"1";
            for(int x=1; x<n; x++)
            {
                k=a3;
                a3=a2+a3;
                a2=k;
                cout<<","<<a3;
            }
            break;
        }
        cout<<endl<<"Enter mode: ";
        cin>>a;
    }
    return 0;
}
