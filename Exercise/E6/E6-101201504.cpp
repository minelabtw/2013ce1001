#include<iostream>
using namespace std;
int main()
{
    int a ;
    int a1 ;
    int d ;
    int n ;
    int r ;

    cout <<"Calculate the progression.\nChoose the mode.\nArithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.\n" ;
    while(a!=-1)
    {
        cout <<"Enter mode: " ;
        cin >>a ;
        if(a==1)    //等差數列
        {
            cout <<"Arithmetic progression\n" ;
            cout <<"a1: " ;
            cin >>a1 ;
            while (a1<=0)
            {
                cout <<"Out of range!\n" ;
                cin >>a1 ;
            }
            cout<<"d: " ;
            cin >>d ;
            while (d<=0)
            {
                cout <<"Out of range!\n" ;
                cin >> d ;
            }
            cout <<"n: " ;
            cin   >> n ;
            while (n<=0)
            {
                cout <<"Out of range!\n" ;
                cin >> n ;
            }
            int i = 0 ;

            while(i<n)
            {
                int b =a1+i*d ;
                if(i!=n-1)
                    cout <<b<<"," ;
                else
                    cout <<b ;

                ++i ;
            }
            cout <<endl ;
        }
        else if(a ==2)   //等比數列
        {
            int c ;
            cout <<"Geometric progression\n" ;
            cout <<"a1: " ;
            cin >> a1 ;
            while (a1<=0)
            {

                cout <<"Out of range!\n" ;
                cin >>a1 ;

            }
            cout <<"r: " ;
            cin >> c ;
            while (c<=0)
            {

                cout <<"Out of range!\n" ;
                cin >>c ;
            }

            cout <<"n: " ;
            cin >> n ;
            while (n<=0)
            {
                cout <<"Out of range!\n" ;
                cin >> n ;
            }
            int j =0 ;
            while(j<n)   //次方
            {
                if(j!=n-1)
                    cout <<a1<<"," ;
                else
                    cout <<a1 ;
                a1*=c ;
                j++ ;

            }
            cout <<endl ;
        }
        else if(a==3)       //費氏數列
        {
            cout <<"Fibonacci sequence\n" ;
            cout <<"n: " ;
            cin >> n ;
            while(n<=0)
            {
                cout <<"Out of range!\n" ;
                cin >>n ;
            }
            int i = 1 ;
            int f1 =1 ;
            int f2 =1 ;
            int fn =0 ;

            while(i<=n)
            {
                if(i==1)
                    fn=f1;
                if(i==2)
                    fn=f2;
                if(i>2)
                {
                    fn=f1+f2;
                    f1=f2;
                    f2=fn;
                }

                if(i!=n)
                    cout <<fn<<"," ;
                else
                    cout <<fn ;
                i++;
            }
            cout <<endl ;
        }
        else if(a ==-1)
            break ;
        else
        {
            cout << "Illegal input!\n";
        }
    }

    return 0 ;
}

