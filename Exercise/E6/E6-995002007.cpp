#include<iostream>
#include <iomanip>
#include<cmath>
using namespace std;

int main()
{
    int a1=0,d=0,n=0,a=0,r=0,f1=1,f2=2,fn=0;
    int mode=0;

    cout << "Calculate the progression." << endl;
    cout << "Choose  the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;


    while (mode!=-1)        //當mode不等於-1時會一直迴圈
    {
        int a1=0,d=0,n=0,a=0,r=0,f1=1,f2=1,fn=0;
        cout << "Enter mode: " ;
        cin >> mode;
        if (mode!=1 && mode!=2 && mode!=3 && mode!=-1)
        {
            cout << "Illegal input!" << endl;
        }

        switch(mode)
        {
        case 1:
            cout << "Arithmetic progression" << endl;
            while(a1<=0)//輸入首項
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)
                    cout<<"Out of range!"<<endl;
            }

            while(d<=0)//輸入公差
            {
                cout<<"d: ";
                cin>>d;
                if(d<=0)
                    cout<<"Out of range!"<<endl;
            }

            while(n<=0)//輸入項數
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                    cout<<"Out of range!"<<endl;
            }

            for(int i=1; i<=n; i++)
            {
                a=a1+d*(i-1);
                if (i!=n)
                    cout << a << " ,";
                else
                    cout << a ;
            }
            cout << endl;
            break;

        case 2:
            cout << "Geometric progression" << endl;
            while(a1<=0)//輸入首項
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)
                    cout<<"Out of range!"<<endl;
            }

            while(r<=0)//輸入公差
            {
                cout<<"r: ";
                cin>>r;
                if(r<=0)
                    cout<<"Out of range!"<<endl;
            }

            while(n<=0)//輸入項數
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                    cout<<"Out of range!"<<endl;
            }

            for(int i=1; i<=n; i++)
            {
                a=a1*pow(r,i-1);
                if(i!=n)
                    cout << a << " ,";
                else
                    cout << a ;
            }
            cout << endl;
            break;

        case 3:
            cout << "Fibonacci sequence" << endl;
            while(n<=0)//輸入項數
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                    cout<<"Out of range!"<<endl;
            }

            for(int i=1; i<=n; i++)
            {
                if(i==1)//第1項
                    fn=f1;
                if(i==2)//第2項
                    fn=f2;
                if(i>2)//第3項後
                {
                    fn=f1+f2;//前2項相加
                    f1=f2;//進位
                    f2=fn;//進位
                }
                if(i!=n)
                    cout<<fn<<",";
                else
                    cout << fn ;
            }
            cout << endl;
            break;

        case -1:
            break;

        }
    }
    return 0;
}
