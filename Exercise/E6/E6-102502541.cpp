#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    cout << "Calculate the progression." << endl << "Choose  the mode." << endl << "Arithmetic progression=1." << endl << "Geometric progression=2." << endl << "Fibonacci sequence=3." << endl << "Exit=-1." << endl;
    int a = 0;//宣告整數a
    while(a!=-1)//當a=-1時結束
    {
        int b = 0;//宣告整數b
        int c = 0;//宣告整數c
        int d = 0;//宣告整數d
        cout << "Enter mode: ";//輸出Enter mode:
        cin >> a;//輸入a
        if(a!=1 && a!=2 && a!=3 && a!=-1)//當a不等於1 2 3 -1 迴圈
            cout << "Illegal input!" << endl;//輸出Illegal input! 換行
        switch(a)//判斷a
        {
        case 1://a=1
            cout << "Arithmetic progression" << endl;//輸出"Arithmetic progression 換行
            while(b<=0)//當b小於等於0迴圈
            {
                cout << "a1: ";//輸出a1:
                cin >> b;//輸入b
                if(b<=0)//當b小於等於0
                    cout << "Illegal input!" << endl;//輸出Illegal input! 換行
            }
            while(c<=0)//當c小於等於0 迴圈
            {
                cout << "d: ";//輸出d:
                cin >> c;//輸入c
                if(c<=0)//當c小於等於0
                    cout << "Illegal input!" << endl;//輸出Illegal input!
            }
            while(d<=0)//當d小於等於0 迴圈
            {
                cout << "n: ";//輸出n:
                cin >> d;//輸入d
                if(d<=0)//當d小於等於0
                    cout << "Illegal input!" << endl;//輸出Illegal input!
            }
            for(int e=1; e<=d; e++)//宣告整數e=1 e小於等於d e遞增
            {
                cout << b+(e-1)*c;//輸出 a1+(n-1)d,
                if(e<d)//當e=d
                    cout << ",";//輸出 a1+(n-1)d

            }
            cout << endl;//輸出換行
            break;//結束
        case 2://當a=2
            cout << "Geometric progression" << endl;//輸出Geometric progression 換行
            while(b<=0)//當b小於等於0迴圈
            {
                cout << "a1: ";//輸出a1:
                cin >> b;//輸入b
                if(b<=0)//當b小於等於0
                    cout << "Illegal input!" << endl;//輸出Illegal input!
            }
            while(c<=0)//當c小於等於0 迴圈
            {
                cout << "r: ";//輸出r:
                cin >> c;//輸入c
                if(c<=0)//當c小於等於0
                    cout << "Illegal input!" << endl;//輸出Illegal input!
            }
            while(d<=0)//當d小於等於0 迴圈
            {
                cout << "n: ";//輸出n:
                cin >> d;//輸入d
                if(d<=0)//當d小於等於0
                    cout << "Illegal input!" << endl;//輸出Illegal input!
            }
            for(int e=1; e<=d; e++)//宣告整數e=1 e小於等於d e遞增
            {
                cout << b*pow(c,e-1);//輸出a1*c,
                if(e<d)//當e=d
                    cout << ",";//輸出a1*c
            }
            cout << endl;//輸出換行
            break;//結束
        case 3://當a=3
            cout << "Fibonacci sequence" << endl;//輸出Fibonacci sequence 換行
            while(d<=0)//當d小於等於0 迴圈
            {
                cout << "n: ";//輸出n:
                cin >> d;//輸入d
                if(d<=0)//當d小於等於0
                    cout << "Illegal input!" << endl;//輸出Illegal input!
            }
            int number1 = 1;//宣告整數一=1
            int number2 = 1;//宣告整數二=2
            for(int e=1; e<=d; e++)//宣告整數e e小於等於d e遞增
            {
                if(e==1 || e==2)//當e=1,2
                cout << 1;//輸出1
                     else if(e%2==1)//當e為奇數
                {
                    number1 = number1+number2;//整數一=整數一+整數二
                    cout << number1;//輸出整數一
                }
                else//其餘
                {
                    number2 = number1+number2;//整數二=整數一+整數二
                    cout << number2 ;//輸出整數二
                }
                if(e<d)//當e小於d
                cout << ",";//輸出,
            }
            cout << endl;//輸出換行
            break;//結束
        }
    }
    return 0;
}
