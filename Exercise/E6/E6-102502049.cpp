#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int mode=0; //宣告整數變數作為模式選擇
    int a=1;
    int d=1;
    int n=1;
    int sumf=0;
    int num=0;//宣告整數變數作為首項.公差.公比.項數.費氏數列


    cout << "Calculate the progression." <<endl;
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.\n"; //顯示題目需求


    do
    {
        cout << "Enter mode: ";
        cin >> mode; //選擇模式編號
        while (mode!=1 &&mode!=2 &&mode!=3 &&mode!=-1) //當模式不為1.2.3.-1時顯示錯誤並要求重新輸入
        {
            cout << "Illegal input!\nEnter mode: ";
            cin >> mode;
        }
        switch (mode) //選擇模式
        {
        case 1:
            cout << "Arithmetic progression" << endl;
            cout << "a1: ";
            cin >> a;
            while (a<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "a1: ";
                cin >> a;
            }
            cout << "d: ";
            cin >> d;
            while (d<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "d: ";
                cin >> d;
            }
            cout << "n: ";
            cin >> n;
            while (n<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "n: ";
                cin >> n;
            } //要求輸入正整數的a1.d.n
            for (int x=0; x<n ; x++)
            {
                cout << a+x*d;
                if (x != n-1)
                {
                    cout << ",";
                }
            } //顯示出等差數列
            cout << endl;
            break;
        case 2:
            cout << "Geometric progression" << endl;
            cout << "a1: ";
            cin >> a;
            while (a<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "a1: ";
                cin >> a;
            }
            cout << "r: ";
            cin >> d;
            while (d<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "r: ";
                cin >> d;
            }
            cout << "n: ";
            cin >> n;
            while (n<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "n: ";
                cin >> n;
            } //要求輸入正整數的a1.r.n
            for (int x=0; x<n ; x++)
            {
                cout << a* pow(d,x);
                if (x != n-1)
                {
                    cout << ",";
                }
            } //輸出等比數列
            cout << endl;
            break;
        case 3:
            cout << "Fibonacci sequence" << endl;
            cout << "n: ";
            cin >> n;
            while (n<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "n: ";
                cin >> n; //要求輸入正整數的n
            }
            cout << "1";
            for (int x=0,sum=1,num=0; x<n-1 ; x++)
            {
                sumf = sum;
                cout << "," <<(sum += num);
                num = sumf;
            } //輸出費氏數列
            cout << endl;
            break;
        case -1:
            break; //跳出迴圈結束程式
        }
    }
    while (mode != -1 ); //除了輸入-1以外繼續執行程式


    return 0;
}
