#include <iostream>
using namespace std;
bool isNotPositive(int number);//檢查number是否為正

int main(){
    int mode = 1;//宣告型態為int的模式變數，並初始化為1
    int a1 = 1;//宣告型態為int的初項，並初始化為1
    int d = 1;//宣告型態為int的公差，並初始化為1
    int r = 1;//宣告型態為int的公比，並初始化為1
    int seqNumber = 1;//宣告型態為int的數列項數，並初始化為1

    cout << "Calculate the progression." << endl \
         << "Choose the mode." << endl \
         << "Arithmetic progression=1." << endl \
         << "Fibonacci sequence=3." << endl \
         << "Exit=-1." << endl;
    while(mode != -1){//當mode為-1表結束，跳出
        cout << "Enter mode: ";
        cin >> mode;
        switch(mode){
            case 1://產生等差數列
                cout << "Arithmetic progression" << endl;
                while(1){//輸入初項
                    cout << "a1: ";
                    cin >> a1;
                    if(isNotPositive(a1)){
                        cout << "Out of range!" << endl;
                    }
                    else{
                        break;
                    }
                }
                while(1){//輸入公差
                    cout << "d: ";
                    cin >> d;
                    if(isNotPositive(d)){
                        cout << "Out of range!" << endl;
                    }
                    else{
                        break;
                    }
                }
                while(1){//輸入項數
                    cout << "n: ";
                    cin >> seqNumber;
                    if(isNotPositive(seqNumber)){
                        cout << "Out of range!" << endl;
                    }
                    else{
                        break;
                    }
                }
                //因seqNumber必大於0，因此先輸出首項
                cout << a1 << ",";
                for(int i = 1; i < seqNumber; i++){
                    a1 += d;//加上公差
                    cout << a1;
                    if(i < (seqNumber - 1)){//如果不是最後一項輸出逗號
                        cout << ",";
                    }
                    else{//最後一項換行
                        cout << endl;
                    }
                }
                break;
            case 2://產生等比數列
                cout << "Geometric progression" << endl;
                while(1){//輸入初項
                    cout << "a1: ";
                    cin >> a1;
                    if(isNotPositive(a1)){
                        cout << "Out of range!" << endl;
                    }
                    else{
                        break;
                    }
                }
                while(1){//輸入公比
                    cout << "r: ";
                    cin >> r;
                    if(isNotPositive(r)){
                        cout << "Out of range!" << endl;
                    }
                    else{
                        break;
                    }
                }
                while(1){//輸入項數
                    cout << "n: ";
                    cin >> seqNumber;
                    if(isNotPositive(seqNumber)){
                        cout << "Out of range!" << endl;
                    }
                    else{
                        break;
                    }
                }
                //因seqNumber必大於0，因此先輸出首項
                cout << a1 << ",";
                for(int i = 1; i < seqNumber; i++){
                    a1 *= r;//乘上公比
                    cout << a1;
                    if(i < (seqNumber - 1)){//如果不是最後一項輸出逗號
                        cout << ",";
                    }
                    else{//最後一項換行
                        cout << endl;
                    }
                }
                break;
            case 3://產生費氏數列
                a1 = 1;//將首項設為1
                r = 1;//此處將r作為第二項並設為1
                cout << "Fibonacci sequence" << endl;
                while(1){//輸入項數
                    cout << "n: ";
                    cin >> seqNumber;
                    if(isNotPositive(seqNumber)){
                        cout << "Out of range!" << endl;
                    }
                    else{
                        break;
                    }
                }
                //因seqNumber必大於0，因此先輸出首項
                cout << a1 << ",";
                for(int i = 1; i < seqNumber; i++){
                    int nextSeq = 0;//區域變數，宣告為int的下一項，並初始化為0
                    nextSeq = a1 + r;//先計算下一項
                    cout << r;//輸出當前項
                    a1 = r;//將當前項的值複製到上一項
                    r = nextSeq;//將下一項的值複製到當前項
                    if(i < (seqNumber - 1)){//如果不是最後一項輸出逗號
                        cout << ",";
                    }
                    else{//最後一項換行
                        cout << endl;
                    }
                }
                break;
            case -1://結束
                //不做任何事情，且在迴圈開始時mode=-1，跳出。
                break;
            default:
                //不合法mode
                cout << "Illegal input!" << endl;
        }
    }
    return 0;
}

bool isNotPositive(int number){//檢查number是否為正
    if(number <= 0)
        return true;
    else
        return false;
}
