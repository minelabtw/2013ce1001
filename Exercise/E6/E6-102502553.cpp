#include<iostream>
#include<math.h>
using namespace std;
int main()
{
    int x = 0; //令一個整數變數初始化=0
    int a1 = 0;
    int d = 0;
    int n = 0;
    int r = 0;
    int c1 = 1;
    int c2 = 1;
    int cn = 0;
    int y = 1;

    cout << "Calculate the progression." << endl; //顯示字串，並換行
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;

    do
    {
        cout << "Enter mode: ";
        cin >> x; //輸入字串
        switch ( x ) //選擇狀況
        {
        case 1: //輸入為1的狀況，進入等差級數
            cout << "Arithmetic progression" << endl;
            do
            {
                cout << "a1: ";
                cin >> a1;
                if ( a1 == 0 )
                    cout << "Out of range!" << endl;
            }
            while ( a1 == 0 ); //當a1=0時找最近的do重做
            do
            {
                cout << "d: ";
                cin >> d;
                if ( d == 0 )
                    cout << "Out of range!" << endl;
            }
            while ( d == 0 );
            do
            {
                cout << "n: ";
                cin >> n;
                if ( n <= 0 )
                    cout << "Out of range!" << endl;
            }
            while ( n <= 0 );
            cout << a1;
            for ( int z = 2; z <= n; z++ ) //令變數z初始值=2，做到z=n，每次做z+1
            {
                cout << "," << a1 + ( z - 1 ) * d; //顯示等差級數
            }
            break; //跳出選擇

        case 2: //輸入為2的狀況，進入等比級數
            cout << "Geometric progression" << endl;
            do
            {
                cout << "a1: ";
                cin >> a1;
                if ( a1 <= 0 )
                    cout << "Out of range!" << endl;
            }
            while ( a1 <= 0 );
            do
            {
                cout << "r: ";
                cin >> r;
                if ( r == 0 )
                    cout << "Out of range!";
            }
            while ( r == 0 );
            do
            {
                cout << "n: ";
                cin >> n;
                if ( n <= 0 )
                    cout << "Out of range!" << endl;
            }
            while ( n <= 0 );
            cout << a1;
            for ( int z = 1; z <= n - 1; z++ )
            {
                cout << "," << pow ( r,z ) * a1; //顯示等比級數
            }
            break;

        case 3: //輸入為3的狀況，進入費式數列
            cout << "Fibonacci sequence" << endl;
            do
            {
                cout << "n: ";
                cin >> n;
                if ( n <= 1 )
                    cout << "Out of range!";
            }
            while ( n <= 0 );
            if ( n == 2 )
                cout <<  "1,1"; //n=2的話直接輸出1,1
            if ( n > 2 )
                cout << "1,1"; //先輸出1,1
            for ( int z = 2; z <= n - 1; z++ )
            {
                cn = c1 + c2;
                c2 = c1;
                c1 = cn;
                cout << "," << cn; //顯示費式數列
            }
            break;

        case -1: //輸入為-1的狀況，結束選擇
            y = -1;
            break;

        default:
            cout << "Illegal input!";
            break;
        }
        cout << endl;
    }
    while ( y != -1 );

    return 0; //結束
}
