#include <iostream>
using namespace std;

int main()
{
	//display infomation of mode
	cout << "Calculate the progression.\n"
	<< "Choose the mode.\n"
	<< "Arithmetic progression=1.\n"
	<< "Geometric progression=2.\n"
	<< "Fibonacci sequence=3.\n"
	<< "Exit=-1.\n";

	while(1){
		int mode;
		cout << "Enter mode: "; cin >> mode;
		if(mode==-1){break;}				//mode -1: exit program
		while(mode!=1 && mode!=2 && mode!=3){
			//warn the user and re-input if mode is illegal
			cout << "illegal input!\n";
			cout << "Enter mode: "; cin >> mode;
		}

		int a1, d, r, n;
		int last2=1, last1=0, last0;
		switch(mode)
		{
			case 1:			//mode 1: Arithmetic progression

			cout <<"Arithmetic progression\n";

			while(1){
				cout << "a1: "; cin >> a1;
				if(a1<=0){cout << "Out of range!\n"; continue;}
				break;
			}
			while(1){
				cout << "d: "; cin >> d;
				if(d<=0){cout << "Out of range!\n"; continue;}
				break;
			}
			while(1){
				cout << "n: "; cin >> n;
				if(n<=0){cout << "Out of range!\n"; continue;}
				break;
			}

			for(int i=1 ; i<=n ; i++){
				if(i>1){cout << ",";}
				//Calculate Arithmetic progression with a1, d for n
				cout << a1+(i-1)*d;
			}
			cout << "\n";

			break;

			case 2:			//mode 2: Geometric progression

			cout <<"Geometric progression\n";

			while(1){
				cout << "a1: "; cin >> a1;
				if(a1<=0){cout << "Out of range!\n"; continue;}
				break;
			}
			while(1){
				cout << "r: "; cin >> r;
				if(r<=0){cout << "Out of range!\n"; continue;}
				break;
			}
			while(1){
				cout << "n: "; cin >> n;
				if(n<=0){cout << "Out of range!\n"; continue;}
				break;
			}

			for(int i=1 ; i<=n ; i++){
				if(i>1){cout << ",";}
				//Calculate Geometric progression with a1, r for n
				int tmpnum=a1;
				for(int j=1 ; j<=i-1 ; j++){tmpnum*=r;}
				cout << tmpnum;
			}
			cout << "\n";

			break;

			case 3:			//mode 3: Fibonacci sequence

			cout <<"Fibonacci sequence\n";

			while(1){
				cout << "n: "; cin >> n;
				if(n<=0){cout << "Out of range!\n"; continue;}
				break;
			}

			for(int i=1 ; i<=n ; i++){
				if(i>1){cout << ",";}
				//Calculate Fibonacci sequence for n
				last0=last2+last1;
				cout << last0;
				last2=last1;
				last1=last0;
			}
			cout << "\n";

			break;

			default:

			;//break;
		}
	}
	return 0;
}
