#include<iostream>
using namespace std;

int main()
{
    int a1,a2,dr,n,mode;                                     //宣告變數a1(首項),a2(費氏數列運算用),dr(公差/比),n(項數)起始值皆為零,mode做為動作選項的變數

    cout << "Calculate the progression.\n";              //輸出字串講解程式的使用方法，並要求輸入變數i來做選擇
    cout << "Choose the mode.\n";
    cout << "Arithmetic progression=1.\n";
    cout << "Geometric progression=2.\n";
    cout << "Fibonacci sequence=3.\n";
    cout << "Exit=-1.\n";
    cout << "Enter mode: ";

    while(1)
    {
        a1=0,a2=1,dr=0,n=0;
        cin >> mode;
        if(mode==-1)
            break;
        if(mode==1 || mode==2)
        {
            if(mode==1)
                cout << "Arithmetic progression\n";
            else
                cout << "Geometric progression\n";
            while(a1<=0)
            {
                cout << "a1: ";
                cin >> a1;
                if(a1<=0)
                    cout << "Out of range!\n";
            }
            while(dr<=0)
            {
                if(mode==1)
                    cout << "d: ";
                else
                    cout << "r: ";
                cin >> dr;
                if(dr<=0)
                    cout << "Out of range!\n";
            }
            while(n<=0)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                    cout << "Out of range!\n";
            }
            cout << a1;
            for(int j=1;j<n;j++)
            {
                if(mode==1)
                    a1+=dr;
                else
                    a1*=dr;
                cout << "," << a1;
            }
            cout << "\nEnter mode: ";
            continue;
        }
        if(mode==3)
        {
            cout << "Fibonacci sequence\n";
            while(n<=0)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                    cout << "Out of range!\n";
            }
            a1=1;
            for(int j=1;j<n;j++)
            {
                if(j%2==1)
                {
                    cout << a1 << ",";
                    a1+=a2;
                }
                else
                {
                    cout << a2 << ",";
                    a2+=a1;
                }
            }
            if(n%2==1)
                cout << a1;
            else
                cout << a2;
            cout << "\nEnter mode: ";
            continue;
        }
        cout << "Illegal input!\n" << "Enter mode: ";
        continue;
    }

    return 0;
}
