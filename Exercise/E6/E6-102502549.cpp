#include <iostream>
using namespace std;

int main()
{
    int mode;//宣告模式
    int a1;//宣告首項
    int d;//宣告公差
    int r;//宣告公比
    int n;//宣告項數
    int temp1=1;//用來記錄Fibonacci sequence的前面一項數字
    int temp2=1;//用來記錄Fibonacci sequence的當前數字
    int temptemp;//用來記錄Fibonacci sequence的後一項數字

    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;

    //開始吧
    while(true)
    {
        cout<<"Enter mode: ";
        cin>>mode;

        //用switch來決定模式
        switch(mode)
        {

            //模式1
        case 1:
            cout<<"Arithmetic progression"<<endl;

            //檢查a1合法性
            while(true)
            {
                cout<<"a1: ";
                cin>>a1;

                if(a1<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            //檢查d合法性
            while(true)
            {
                cout<<"d: ";
                cin>>d;

                if(d<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            //檢查n合法性
            while(true)
            {
                cout<<"n: ";
                cin>>n;

                if(n<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            //印出結果
            for(int i=1; i<=n; i++)
            {
                if(i!=n)
                    cout<<a1<<",";
                else
                    cout<<a1;

                a1=a1+d;
            }

            cout<<endl;
            break;//模式1結束

            //模式2
        case 2:
            cout<<"Geometric progression"<<endl;

            //檢查a1合法性
            while(true)
            {
                cout<<"a1: ";
                cin>>a1;

                if(a1<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            //檢查r合法性
            while(true)
            {
                cout<<"r: ";
                cin>>r;

                if(r<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            //檢查n合法性
            while(true)
            {
                cout<<"n: ";
                cin>>n;

                if(n<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            //印出結果
            for(int i=1; i<=n; i++)
            {
                if(i!=n)
                    cout<<a1<<",";
                else
                    cout<<a1;

                a1=a1*r;
            }

            cout<<endl;
            break;//模式2結束

            //模式3
        case 3:
            cout<<"Fibonacci sequence"<<endl;

            //檢查n合法性
            while(true)
            {
                cout<<"n: ";
                cin>>n;

                if(n<=0)
                    cout<<"Out of range!"<<endl;
                else
                    break;
            }

            //印出結果
            if(n==1)
                cout<<1;
            else if(n==2)
                cout<<1<<","<<1;
            else
            {
                cout<<1<<","<<1<<",";

                for(int i=3; i<=n; i++)
                {
                    if(i!=n)
                        cout<<temp1+temp2<<",";
                    else
                        cout<<temp1+temp2;

                    //移項
                    temptemp=temp1+temp2;
                    temp1=temp2;
                    temp2=temptemp;
                }
            }

            //記得將temp1,temp2初始為1
            temp1=1;
            temp2=1;

            cout<<endl;
            break;//模式3結束

            //模式-1
        case -1:
            return 0;

            //非法模式
        default:
            cout<<"Illegal input!"<<endl;
        }
    }

















}
