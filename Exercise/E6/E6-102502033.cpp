#include<iostream>

using namespace std;

int main()
{
    int m=0;
    int aa1=0;
    int d=0;
    int an=0;
    int aan=0;
    int ga1=0;
    int r=0;
    int gn=0;
    int i=1;
    int n=0;


    cout << "Calculate the progression.\n";
    cout << "Choose the mode.\n";
    cout << "Arithmetic progression=1.\n";
    cout << "Geometric progression=2.\n";
    cout << "Fibonacci sequence=3.\n";
    cout << "Exit=-1.\n";
    cout << "Enter mode:";
    cin  >> m;

    while(m!=-1)
    {
        switch(m)
        {
        case 1:
            cout << "Arithmetic progression\n";

            cout << "a1: ";
            cin  >> aa1;
            while(aa1<=0)
            {
                cout << "Out of range!\n";
                cout << "a1: ";
                cin  >> aa1;
            }

            cout << "d: ";
            cin  >> d;
            while(d<=0)
            {
                cout << "Out of range!\n";
                cout << "d: ";
                cin  >> d;
            }

            cout << "n: ";
            cin  >> an;
            while(an<=0)
            {
                cout << "Out of range!\n";
                cout << "n: ";
                cin  >> an;
            }
            aan=aa1+(an-1)*d;

            while(aa1<aan)
            {
                cout << aa1 << ",";
                aa1=aa1+d;
            }
            cout << aan;
            break;

        case 2:
            cout << "Geometric progression\n";
            cout << "a1: ";
            cin  >> ga1;
            while(ga1<=0)
            {
                cout << "Out of range!\n";
                cout << "a1: ";
                cin  >> ga1;
            }

            cout << "r: ";
            cin  >> r;
            while(r<=0)
            {
                cout << "Out of range!\n";
                cout << "r: ";
                cin  >> r;
            }

            cout << "n: ";
            cin  >> gn;
            while(gn<=0)
            {
                cout << "Out of range!\n";
                cout << "n: ";
                cin  >> gn;
            }

            while(i<=gn)
            {
                if(i!=gn)
                {
                    cout << ga1 << ",";
                    ga1=ga1*r;
                    i++;
                }
                else
                {
                    cout << ga1;
                    i++;
                }
            }
            break;

        case 3:
            cout << "Fibonacci sequence\n";
            cout << "n: ";
            cin  >> n;
            while(n<=0)
            {
                cout << "Out of range!\n";
                cout << "n: ";
                cin  >> n;
            }
            break;



        default:
            cout << "Illegal input!";
            break;
        }
        cout << "\n";
        cout << "Enter mode:";
        cin  >> m;
    }

}
