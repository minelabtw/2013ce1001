#include<iostream>
#include<iomanip>
using namespace std;

int main(void)
{
    cout << "Calculate the progression." << endl;//輸出以下字元,顯示輸入什麼則進入什麼條件
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;

    int stop = 1;
    while(stop)//設定程式結束只在輸入-1的時候
    {
        int i=0;
        cout << "Enter mode: " ;//輸入使用算式
        cin >> i;
        int a=0;//首項
        int l=0;//公比或公差
        int q=0;//項數
        int x=0;//case1總和

        switch(i)
        {
        case 1:
            cout << "Arithmetic progression" << endl;
            cout << "a1: " ;//輸出a1=輸入a
            cin >> a;
            while(a<=0)//設定a為正整數
            {
                cout << "Out of range!" << endl;
                cout << "a1: " ;
                cin >> a;
            }
            cout << "d: " ;//輸出d=輸入l
            cin >> l;
            while(l<=0)//設定l為正整數
            {
                cout << "Out of range!" << endl;
                cout << "d: " ;
                cin >> l;
            }
            cout << "n: " ;//輸出n=輸入q
            cin >> q;
            while(q<=0)//設定q為正整數
            {
                cout << "Out of range!" << endl;
                cout << "n: " ;
                cin >> q;
            }

            for(int k=0; k<q; k++)
            {
                int x=a+l*k;
                cout << x << "," ;
            }
            cout <<endl;
            break;

        case 2:
            cout << "Geometric progression" << endl;
            cout << "a1: " ;
            cin >> a;
            while(a<=0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: " ;//設定首項
                cin >> a;
            }
            cout << "r: " ;//設定公比
            cin >> l;
            while(l<=0)
            {
                cout << "Out of range!" << endl;
                cout << "r: " ;
                cin >> l;
            }
            cout << "n: " ;//設定項數
            cin >> q;
            while(q<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: " ;
                cin >> q;
            }
            for(int i=0,y=a; i<q; i++)
            {
                cout << y << "," ;
                y=y*l;
            }
            cout <<endl;
            break;

        case 3:
        {
            int a1=1;
            int a2=1;
            int a3=1;
            cout << "Fibonacci sequence" << endl;//輸出費氏數列
            cin >> q;
            while(q<=0)//設定項數為正整數
            {
                cout << "Out of range!" << endl;
                cout << "n: " ;
                cin >> q;
            }
            cout << a3 << "," ;
            for(int t=1; t<q; t++)
            {
                cout << a3 << ",";
                a3=a1+a2;
                a1=a2;
                a2=a3;
            }
        }
        cout << endl;
        break;

        case -1:
            stop=0;//若是輸入-1,則程式結束
            break;

        default:
            cout << "Illegal input!" <<endl;//輸入設定字元以外的字元,顯示出字元Illegal input!
            break;
        }
    }

    return 0;
}
