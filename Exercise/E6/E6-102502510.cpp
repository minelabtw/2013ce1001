#include <iostream>

using namespace std;

int main()
{
    cout << "Calculate the progression." << endl;
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;
    int mode=0;
    while(true)
    {
        bool end=false;//judge end
        cout << "Enter mode: ";
        cin >> mode;
        switch(mode)
        {
        case 1:
        {
            cout << "Arithmetic progression" << endl;
            int a1=0;
            int d=0;
            int n=0;
            while(a1<=0)//judge range
            {
                cout << "a1: ";
                cin >> a1;
                if(a1<=0)
                    cout << "Out of range!" << endl;
            }
            while(d<=0)//judge range
            {
                cout << "d: ";
                cin >> d;
                if(d<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            while(n<=0)//judge range
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            cout << a1;//ouput
            for(int i=1; i<n; ++i)
            {
                cout << "," << a1+d*i;
            }
            cout << endl;
            break;
        }
        case 2:
        {
            cout << "Geometric progression" << endl;
            int a1=0;
            int r=0;
            int n=0;
            while(a1<=0)//judge range
            {
                cout << "a1: ";
                cin >> a1;
                if(a1<=0)
                    cout << "Out of range!" << endl;
            }
            while(r<=0)//judge range
            {
                cout << "r: ";
                cin >> r;
                if(r<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            while(n<=0)//judge range
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            cout << a1;//ouput
            for(int i=1; i<n; ++i)
            {
                a1*=r;
                cout << ',' << a1;
            }
            cout << endl;
            break;
        }
        case 3:
        {
            cout << "Fibonacci sequence" << endl;
            int fn1=0;
            int fn2=1;
            int n=0;
            int fn;
            while(n<=0)//judge range
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            cout << 1;//ouput
            for(int i=1; i<n; ++i)
            {
                fn=fn1+fn2;
                cout << "," << fn;
                fn1=fn2;
                fn2=fn;
            }
            cout << endl;
            break;
        }
        case -1:
            end=true;
            break;
        default:
            cout << "Illegal input!" << endl;
        }
        if(end==true)
        {
            break;
        }
    }
    return 0;
}
