#include<iostream>
#include <math.h>
using namespace std;

double Square(double a)//計算平方的函數 
{
    return pow(a,2);
}

double Cube(double a)//計算立方的函數 
{
    return pow(a,3);
}

double Factorial(double a)//計算階層的函數 
{
    int x;
    int product=1;
    for(x=1;x<=a;x++)
    {
        product=product*x;//利用迴圈計算階層並存放至product 
    }
    return product;
}

int main()
{
    double a;
    
    do//請使用者輸入大於零的數字 
    {
        cout<<"Please enter a number( >0 ): ";
        cin>>a;
        if(a<=0)
            cout<<"Out of range!"<<endl;
    }
    while(a<=0);
    
    cout<<"Square: "<<Square(a)<<endl;//輸出平方 
    cout<<"Cude: "<<Cube(a)<<endl;//輸出立方 
    cout<<"Factorial: "<<Factorial(a)<<endl;//輸出階層 
    return 0;
}
