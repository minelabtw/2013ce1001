#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    cout << "Calculate the progression.\n";
    cout << "Choose the mode.\n";
    cout << "Arithmetic progression=1.\n";
    cout << "Geometric progression=2.\n";
    cout << "Fibonacci sequence=3.\n";
    cout << "Exit=-1.\n";

    int mode=0;

    do
    {
        cout << "Enter mode: ";
        cin >> mode;

        switch(mode)
        {
        case 1:
        {
            int a1=0 , d=0 , n=0 , An=0;

            cout << "Arithmetic progression\n";

            while (a1<=0)
            {
                cout << "a1: ";
                cin >> a1;

                if (a1<=0)
                    cout << "Out of range!\n";
            }
            while (d<=0)
            {
                cout << "d: ";
                cin >> d;

                if (d<=0)
                    cout << "Out of range!\n";
            }
            while (n<=0)
            {
                cout << "n: ";
                cin >> n;

                if (n<=0)
                    cout << "Out of range!\n";
            }

            for(int x=1 ; x<n ; x++)  //建立等差數列
            {
                cout << a1 << ",";
                a1 +=d;
            }
            An = a1;
            cout << An << endl;
            break;
        }

        case 2:
        {
            int a1=0 , r=0 , n=0 , An=0;

            cout << "Geometric progression\n";

            while (a1<=0)
            {
                cout << "a1: ";
                cin >> a1;

                if (a1<=0)
                    cout << "Out of range!\n";
            }
            while (r<=0)
            {
                cout << "r: ";
                cin >> r;

                if (r<=0)
                    cout << "Out of range!\n";
            }
            while (n<=0)
            {
                cout << "n: ";
                cin >> n;

                if (n<=0)
                    cout << "Out of range!\n";
            }

            for(int x=1 ; x<n ; x++)  //建立等比數列
            {
                cout << a1 << ",";
                a1 *=r;
            }
            An =a1;
            cout << An << endl;
            break;
        }

        case 3:
        {
            cout << "Fibonacci sequence\n";

            int n=0;

            while (n<=0)
            {
                cout << "n: ";
                cin >> n;

                if (n<=0)
                    cout << "Out of range!\n";
            }

            int z1=1 , z2=1 , Z=1 ;

            cout << "1,";  //建立費氏數列
            for (int x=1 ; x<n-1 ; x++)
            {
                cout << Z << ",";
                Z=z1+z2;
                z1=z2;
                z2=Z;
            }
            cout << Z << endl;
            break;
        }

        case -1:
        {
            break;
        }

        default:
        {
            cout << "Illegal input!\n";
            break;
        }
        }
    }
    while(mode!=-1);

    return 0;
}
