#include<iostream>
using namespace std;

int main()
{
    int a,b,c,d,e=1,f,x=1,y=1,z=0,g=1;//宣告變數
    cout<<"Calculate the progression."<<endl<<"Choose  the mode."<<endl<<"Arithmetic progression=1."<<endl<<"Geometric progression=2."<<endl<<"Fibonacci sequence=3."<<endl<<"Exit=-1."<<endl<<"Enter mode:";
    while(g=1)//讓每次運算一定會進入while迴圈
    {
        cin>>a;
        switch(a)//以a的數值判斷
        {
        case 1://假如a=1
            cout<<"Arithmetic progression"<<endl<<"a1:";
            cin>>b;
            while(b<=0)
            {
                cout<<"Out of range!"<<endl<<"a1:";
                cin>>b;
            }
            cout<<"d:";
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl<<"d:";
                cin>>c;
            }
            cout<<"n:";
            cin>>d;
            while(d<=0)
            {
                cout<<"Out of range!"<<endl<<"n:";
                cin>>d;
            }
            for(e=1; e<=d; e++) //設e初始值為1,e不大於d,每次迴圈e+1
            {
                f=b+c*(e-1);
                cout<<f<<" ";
            }
            break;
        case 2://假如a=2,執行以下
            cout<<"Geometric progression"<<endl<<"a1:";
            cin>>b;
            while(b<=0)
            {
                cout<<"Out of range!"<<endl<<"a1:";
                cin>>b;
            }
            cout<<"r:";
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl<<"r:";
                cin>>c;
            }
            cout<<"n:";
            cin>>d;
            while(d<=0)
            {
                cout<<"Out of range!"<<endl<<"n:";
                cin>>d;
            }
            for(e=1; e<=d; e++)//設e初始值為1,e不大於d,每次迴圈e+1
            {
                cout<<b<<" ";
                b=b*c;
            }
            break;
        case 3:
            cout<<"Fibonacci sequence"<<endl<<"n:";
            cin>>b;
            while(b<=0)
            {
                cout<<"Out of range!"<<endl<<"n:";
                cin>>b;
            }
            for(e=1; e<=b; e++)//設e初始值為1,e不大於b,每次迴圈e+1
            {
                cout<<y<<" ";
                z=x+y;
                x=y;
                y=z;
            }
            break;
        default://除了a=1,2,3以外的情況,執行以下
            cout<<"Illegal input!";
        }
        cout<<endl<<"Enter mode:";//可以重複執行其他模式
    }
}
