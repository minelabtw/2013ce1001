#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int main()
{
	int a1 , d , n , mode; // declare input variable
	string trash;
	
	cout << "Calculate the progression." << endl;
	cout << "Choose the mode." << endl;
	cout << "Arithmetic progression=1." << endl;
	cout << "Geometric progression=2." << endl;
	cout << "Fibonacci sequence=3." << endl;
	cout << "Exit=-1." << endl;

	while(true)
	{
		cout << "Enter mode: ";
		if(!(cin >> mode)) // prevent not integer input
		{
			cin.clear();
			getline(cin , trash);
			cout << "Illegal input!" << endl;
			continue;
		}
		
		if(mode == 1) //Arithmetic progression
		{
			cout << "Arithmetic progression" << endl;

			while(true) // input 首項
			{
				cout << "a1: ";
				if(!(cin >> a1))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(a1 > 0)
					break;
				cout << "Out of range!" << endl;
			}

			while(true) // input 公差
			{
				cout << "d: ";
				if(!(cin >> d))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(d > 0)
					break;
				cout << "Out of range!" << endl;
			}

			while(true) // input 項數
			{
				cout << "n: ";
				if(!(cin >> n))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(n > 0)
					break;
				cout << "Out of range!" << endl;
			}

			for(int i = 0 ; i < n ; ++i) // output Arithmetic progression
			{
				cout << a1 + (d * i);
				if(i != n - 1)
					cout << ",";
				else
					cout << endl;
			}
		}
		else if(mode == 2) // Geometric progression
		{
			cout << "Geometric progression" << endl;

			while(true) // input 首項
			{
				cout << "a1: ";
				if(!(cin >> a1))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(a1 > 0)
					break;
				cout << "Out of range!" << endl;
			}

			while(true) // input 公差
			{
				cout << "r: ";
				if(!(cin >> d))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(d > 0)
					break;
				cout << "Out of range!" << endl;
			}

			while(true) // input 項數
			{
				cout << "n: ";
				if(!(cin >> n))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(n > 0)
					break;
				cout << "Out of range!" << endl;
			}

			for(int i = 0 ; i < n ; ++i) // output Geometric progression
			{
				cout << a1 * pow(d , i);
				if(i != n - 1)
					cout << ",";
				else
					cout << endl;
			}
		}
		else if(mode == 3) // Fibonacci sequence
		{
			cout << "Fibonacci sequence" << endl;

			while(true) // input 項數
			{
				cout << "n: ";
				if(!(cin >> n))
				{
					cin.clear();
					getline(cin , trash);
				}
				else if(n > 0)
					break;
				cout << "Out of range!" << endl;
			}

			int a2 = 1; // 宣告費式數列的第二項
			a1 = 1;
			for(int i = 0 ; i < n ; ++i) // output Fibonacci sequence
			{
				if(i < 2)
					cout << "1";
				else
				{
					cout << a1 + a2;
					if(i % 2)
						a2 = a1 + a2;
					else
						a1 = a1 + a2;
				}
				if(i != n - 1)
					cout << ",";
				else
					cout << endl;
			}
		}
		else if(mode == -1)
			break;
		else
		{
			cout << "Illegal input!" << endl;
			continue;
		}
	}

	return 0;
}
