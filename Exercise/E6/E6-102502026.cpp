//E6-102502026

#include<iostream>
using namespace std;

int main()  //start the program
{
    int a1=0;       //define a1
    int d=0;        //define d
    int n=0;        //define n
    int r=0;        //deifne r
    int n1=0;       //define n1
    int Count1=0;   //define Count1
    int total=0;    //define total
    int first=0;    //define first number for case 3
    int second=1;   //define seconde number for case 3

    cout<<"Calculate the progression.\n";
    cout<<"Choose the mode.\n";
    cout<<"Arithmetic progression=1.\n";
    cout<<"Geometric progression=2.\n";
    cout<<"Fibonacci sequence=3.\n";
    cout<<"Exit=-1.\n";
    cout<<"Enter mode: ";
    cin>>n1;                                //ask the mode

    while(n1!=-1)                           //do until n1 = -1
    {
        switch(n1)
        {
        case 1:                                 //if n1 = 1
            cout<<"Arithmetic progression\n";
            cout<<"a1: ";
            cin>>a1;                            //ask a1
            while(a1<=0)                        //do until a1>0
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"a1: ";
                cin>>a1;                        //ask again a1
            }
            cout<<"d: ";
            cin>>d;                             //ask d
            while(d<=0)                         //do until d>0
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"d: ";
                cin>>d;                         //ask again d
            }
            cout<<"n: ";
            cin>>n;                             //ask n
            while(n<=0)                         //do until n>0
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"n: ";
                cin>>n;                         //ask again n
            }
            for(Count1=1; Count1<=n; Count1++)  //do Count1=1 to n
            {
                cout<<a1;                       //print the result for case 1
                if(Count1<n)
                    cout<<",";
                a1=a1+d;                        //formula for case 1
            }
            cout<<"\n";
            cout<<"Enter mode: ";
            break;

        case 2:                                 //if n1 =2
            cout<<"Geometric progression\n";
            cout<<"a1: ";
            cin>>a1;                            //ask a1
            while(a1<=0)
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"a1: ";
                cin>>a1;                        //ask again a1
            }
            cout<<"r: ";
            cin>>r;                             //ask r
            while(r<=0)
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"r: ";
                cin>>r;                         //ask again r
            }
            cout<<"n: ";
            cin>>n;                             //ask n
            while(n<=0)
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"n: ";
                cin>>n;                         //ask again n
            }
            for(Count1=1; Count1<=n; Count1++)  //do Count1=1 to n
            {
                cout<<a1;                       //print the result for case 2
                if(Count1<n)
                    cout<<",";
                a1=a1*r;                        //formula for case 2
            }
            cout<<"\n";
            cout<<"Enter mode: ";
            break;

        case 3:                                 //if n1=3
            cout<<"Fibonacci sequence\n";
            cout<<"n: ";
            cin>>n;                             //ask n
            while(n<=0)
            {
                cout<<"Out of range!\n";        //wrong
                cout<<"n: ";
                cin>>n;                         //ask n
            }
            for(Count1=1; Count1<=n; Count1++)  //do Count1=1 to n
            {
                if(Count1<=1)
                    total= Count1;              //print the first number
                else
                {
                    total= first +second;       //formula for case 3
                    first= second;
                    second= total;
                }
                cout<< total;                   //print the result for case 3
                if(Count1<n)
                    cout<<",";
            }
            cout<<"\n";
            cout<<"Enter mode: ";
            break;

        case '\n':                              //if n1=\n
        case '\t':                              //if n1=\t
        case ' ':                               //if n1=" "
            break;

        default:                                //if is another number or letter
            cout<<"Illegal input!\n";           //print error
            cout<<"Enter mode: ";
            break;
        }
        cin>>n1;                                //all finish return to ask again n1
    }
    return 0;
}                                               //end program
