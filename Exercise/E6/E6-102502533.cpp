#include<iostream>
using namespace std;

int main()
{
    int Enter_code=0;//宣告整數
    int a1=0,d=0,r=0,n=0;//宣告整數

    cout<<"Calculate the progression.";
    cout<<"\nChoose the mode.";
    cout<<"\nArithmetic progression=1.";
    cout<<"\nGeometric progression=2.";
    cout<<"\nFibonacci sequence=3.";
    cout<<"\nExit=-1.";
    cout<<"\nEnter code: ";
    cin>>Enter_code;

    while( Enter_code!=-1)//限制輸入直
    {
        switch(Enter_code)
        {
        case 1 ://輸入1跑出等差數列
            cout<<"Enter mode: 1";
            cout<<"\nArithmetic progression";
            cout<<"\na1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!";
                cout<<"\na1: ";
                cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            while(d<=0)
            {
                cout<<"Out of range!";
                cout<<"\nd: ";
                cin>>d;
            }
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!";
                cout<<"\nn: ";
                cin>>n;
            }
            for(int x=0; x<=(n-1); x++)
                cout<<a1+(x*d)<<",";
            cout<<"\nEnter code: ";
            cin>>Enter_code;
            break;

        case 2://輸入2跑出等比數列

            cout<<"Enter mode: 2";
            cout<<"\nGeometric progression";
            cout<<"\na1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!";
                cout<<"\na1: ";
                cin>>a1;
            }
            cout<<"r: ";
            cin>>r;
            while(r<=0)
            {
                cout<<"Out of range!";
                cout<<"\nr: ";
                cin>>r;
            }
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!";
                cout<<"\nn: ";
                cin>>n;
            }
            cout<<a1<<",";
            for(int y=1; y<=n-1; y++)
            {
                a1=a1*r;
                cout<<a1<<",";
            }
            cout<<"\nEnter code: ";
            cin>>Enter_code;
            break;

        case 3://輸入3跑出費氏數列

            cout<<"Enter mode: 3";
            cout<<"\nFibonacci sequence";
            cout<<"\nn: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!";
                cout<<"\nn: ";
                cin>>n;
            }
            if(n==1)
            {
                cout<<"1,";
            }
            else if(n==2)
            {
                cout<<"1,1,";
            }
            else
            {
                cout<<"1,1,";
                int a1=1 , a2=1,a3=0 ,i=1;
                while(i<=n-2)
                {
                    a3=a1+a2;
                    a1=a2;
                    a2=a3;
                    cout<<a3<<",";
                    i++;
                }
            }
            cout<<"\nEnter code: ";
            cin>>Enter_code;

        case -1://輸入-1結束迴圈
            break;

        default://除1.2.3.-1之外,其他要求重新輸入
            cout<<"Illegal input!";
            cout<<"\nEnter code: ";
            cin>>Enter_code;
        }
    }
    return 0;
}





