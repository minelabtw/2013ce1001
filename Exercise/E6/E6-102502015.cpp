#include <iostream>
#include <math.h>
using namespace std;
int main()
{
    int mode=0;                                 //模式 初始0
    int a;                                      //a 初項 b 公差 c 項數 d 公比乘數 f 每一個項的值
    int b;
    int c;
    int d;
    int f;
    int j=1;                                    //費氏 一二項 = j l
    int l=1;
    int m=0;                                      // 費氏項數值(三項起)
    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;
    while (mode!=-1)
    {
        cout<<"Enter mode:";
        cin>>mode;
        switch(mode)                                    //判斷模式
        {
        case 1:                                         //當mode=1的時候
            cout<<"Arithmetic progression"<<endl;
            cout<<"a1: ";
            cin>>a;
            while(a<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"a1: ";
                cin>>a;
            }
            cout<<"d: ";
            cin>>b;
            while(b<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"d: ";
                cin>>b;
            }
            cout<<"n: ";
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"n: ";
                cin>>c;
            }
            for (int e=1; e<=c; e++)                    //迴圈 項數次
            {
                f = a+b*(e-1);                          //計算各項值
                cout<<f;
                if (e!=c)                               //最後一項不打","
                {
                    cout<<",";
                }
            }
            cout<<endl;
            break;
        case 2:                                         //當mode=2的時候
            cout<<"Geometric progression"<<endl;
            cout<<"a1: ";
            cin>>a;
            while(a<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"a1: ";
                cin>>a;
            }
            cout<<"r: ";
            cin>>b;
            while(b<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"r: ";
                cin>>b;
            }
            cout<<"n: ";
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl;            //超出範圍則重輸入
                cout<<"n: ";
                cin>>c;
            }
            for (int e=1; e<=c; e++)                    //迴圈 項數次
            {
                d = pow(b,e-1);                         //運算公比值
                f = a*d;                                //計算各項值
                cout<<f;
                if (e!=c)                               //最後一項不打","
                {
                    cout<<",";
                }
            }
            cout<<endl;
            break;
        case 3:                                         //當mode=3時
            cout<<"Finbonacci sequence"<<endl;
            cout<<"n: ";
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>c;
            }
            for (int n=1; n<=c; n++)                    //迴圈項數次
            {

                if(n==1 || n==2)                        //第一 第二項 輸出1
                {
                    cout<<"1";
                    if (n!=c)                           //最後一項不打","
                    {
                        cout<<",";
                    }
                }
                else
                {
                    m=j+l;                              //計算前兩項和
                    j=l;                                //給第一項第二項的值(給後一項的值)
                    l=m;                                //給第二項第三項的值(給後一項的值)
                    cout<<m;
                    if (n!=c)                           //最後一項不打","
                    {
                        cout<<",";
                    }
                }
            }
            j=1;                                        //初始化
            l=1;
            m=0;
            cout<<endl;
            break;
        case -1:
            break;
        default:                                        //1 2 3 -1 以外 顯示錯誤
            cout<<"Illegal input!"<<endl;
            break;
        }
    }
    return 0;
}
