#include<iostream>
using namespace std ;

int main()
{
    int type ;
    int a1 ;
    int r ;
    int n ;
    int a=1 ;
    int b=0 ;
    int counter ;
    cout<<"Calculate the progression."<<endl ;
    cout<<"Choose the mode."<<endl ;
    cout<<"Arithmetic progression=1."<<endl ;
    cout<<"Geometric progression=2."<<endl ;
    cout<<"Fibonacci sequence=3."<<endl ;
    cout<<"Exit=-1."<<endl ;
    do
    {
        cout<<"Enter mode:"<<" " ;
        cin>>type ;
        switch(type)
        {
        case 1 :
            cout<<"a1: " ;
            cin>>a1 ;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"a1: " ;
                cin>>a1 ;
            }
            cout<<"d: " ;
            cin>>r ;
            while(r<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"d: " ;
                cin>>r ;
            }
            cout<<"n: " ;
            cin>>n ;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"n: " ;
                cin>>n ;
            }
            for(counter=1; counter<=n; counter++)
            {
                if(counter==n)
                    cout<<a1 ;
                else
                {
                    cout<<a1<<"," ;
                    a1+=r ;
                }
            }
            cout<<endl ;
            break ;

        case 2 :
            cout<<"a1: " ;
            cin>>a1 ;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"a1: " ;
                cin>>a1 ;
            }
            cout<<"r: " ;
            cin>>r ;
            while(r<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"d: " ;
                cin>>r ;
            }
            cout<<"n: " ;
            cin>>n ;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"n: " ;
                cin>>n ;
            }
            for(counter=1; counter<=n; counter++)
            {
                if(counter==n)
                    cout<<a1 ;
                else
                {
                    cout<<a1<<"," ;
                    a1*=r ;
                }
            }
            cout<<endl ;
            break ;

        case 3 :
            cout<<"n: " ;
            cin>>n ;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"n: " ;
                cin>>n ;
            }

            for(counter=1; counter<=n; counter++)
            {
                if(counter==n&&counter%2!=0)
                {
                    a+=b ;
                    cout<<a ;
                }
                else if(counter==n&&counter%2==0)
                {
                    b+=a ;
                    cout<<b ;

                }
                else if(counter%2!=0)
                {
                    a+=b ;
                    cout<<a<<"," ;

                }
                else if(counter%2==0)
                {
                    b+=a ;
                    cout<<b<<"," ;

                }
            }
            cout<<endl ;
            break ;

        case -1 :
            break ;

        default :
            cout<<"Illegal input!"<<endl ;
            break ;
        }

    }
    while(type!=-1) ;

    return 0 ;
}
