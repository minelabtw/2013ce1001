#include <iostream>
using namespace std;

int main()
{
    int c = 1; // initialize c = 1
    int a = 0;
    int b = 0;
    int d = 0;
    int p = 0;

    cout << "Calculate the progression.\n"
         << "Choose the mode. \n"
         << "Artithmetic progression=1.\n"
         << "Geomatric progression=2.\n"
         << "Fibonacci sequence=3.\n"
         << "Exit=-1.\n";

    while (c != -1) // when c != 1 come into the while.
    {
        cout << "Enter mode: ";
        cin >> c;

        switch( c )
        {

        case 1:
        {
            cout <<"Artithmetic progression\n";
            while ( a <= 0 )
            {
                cout << "al: ";
                cin >> a;
                if ( a <= 0)
                    cout << "Out of range!\n";
            }

            while ( b <= 0)
            {
                cout << "b: ";
                cin >> b;
                if ( b <= 0)
                    cout << "Out of range! \n";
            }
            while ( d <= 0)
            {
                cout << "n: ";
                cin >> d;
                if ( d <= 0)
                    cout << "Out of range! \n";
            }

            int i = 0;

            while ( i <= (d - 1 ) )
            {
                int s = a + b * i;
                cout << s; // to let the final output without common.

                if (i == (d - 1) )
                {
                    break; // jump out of the case.
                }
                cout << ","; // output the common.
                i++;
            }
            cout << endl;
            a = 0; // to initialize the number.
            b = 0;
            d = 0;
            break;
        }


        case 2:
        {
            cout << "Geomatric progression\n";
            while ( a <= 0 )
            {
                cout << "al: ";
                cin >> a;
                if ( a <= 0)
                    cout << "Out of range!\n";
            }
            while ( d <= 0)
            {
                cout << "n: ";
                cin >> d;
                if ( d <= 0)
                    cout << "Out of range! \n";
            }
            while ( p <= 0)
            {
                cout <<"p: ";
                cin >> p;
                if ( p <= 0)
                    cout << "Out of range! \n";
            }

            int z = a;
            int x = p;
            int i = 0;

            while (i <= (d -1 ) )
            {
                z = z * x;
                cout << z;
                if (i == (d -1) )
                {
                    break;
                }
                cout << ",";
                i++;
            }
            z++;
            cout << endl;
            a = 0;
            d = 0;
            p = 0;
            break;
        }


        case 3:
        {
            cout << "Fibonacci sequence\n";
            while (d <= 0)
            {
                cout << "n: ";
                cin >> d;
                if ( d <= 0)
                    cout << "Out of range!\n";

                int q1 = 1;
                int q2 = 1;
                int q3 = 1;

                int r = 1;

                while (r <= d)
                {
                    if (r == 1)
                        q3 = q1;
                    if (r == 2)
                        q3 = q2;
                    if (r > 2) // to be a routine.
                    {
                        q3 = q1 + q2;
                        q1 = q2;
                        q2 = q3;
                    }

                    cout << q3;
                    if ( r == d)
                    {
                        break;
                    }
                    cout << ",";
                    r++;

                }
            }
            cout << endl;
            d = 0;
            break;

        }


        case -1:
        {
            cout << endl;
            break; // EOF
        }


        default:
        {
            cout << "Illigal input!\n";
            break;
        }


        }


    }
    return 0;
}

