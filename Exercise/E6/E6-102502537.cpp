#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int mode=0; // 宣告變數

    int a1=0;
    int an=0;

    int d=0;
    int n=0;
    int r=0;

    int f1=1;
    int f2=1;
    int fn=0;

    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;

    cout<<"Enter mode: "; // 輸入mode
    cin>>mode;
    while(mode!=-1 && mode!=1 && mode!=2 && mode!=3) // mode須為-1,1,2,3
    {
        cout<<"Illegal input!"<<endl;
        cout<<"Enter mode: ";
        cin>>mode;
    }

    while(mode!=-1)
    {
        switch(mode)
        {
        case 1:
            cout<<"Arithmetic progression"<<endl;

            cout<<"a1: "; // 輸入a1
            cin>>a1;
            while(a1<=0) // a1須>0
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }

            cout<<"d: "; // 輸入d
            cin>>d;

            cout<<"n: "; // 輸入n
            cin>>n;
            while(n<=0) // n須>0
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            cout<<a1; // 輸出等差數列
            an=a1;
            for(int i=1; i<n; i++)
            {
                an=an+d; // 每次運算加上公差
                cout<<","<<an;
            }
            cout<<endl;

            break;

        case 2:
            cout<<"Geometric progression"<<endl;

            cout<<"a1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }

            cout<<"r: ";
            cin>>r;
            while(r==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"r: ";
                cin>>r;
            }

            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            cout<<a1; // 輸出等比數列
            an=a1;
            for(int i=1; i<n; i++)
            {
                an=an*r; // 每次運算乘上公比
                cout<<","<<an;
            }
            cout<<endl;

            break;

        case 3:
            cout<<"Fibonacci sequence"<<endl;

            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            cout<<f1<<","<<f2; // 輸出費氏數列
            for(int i=2; i<n; i++)
            {
                fn=f1+f2; // 前兩項相加
                f1=f2; // 往後移一項
                f2=fn; // 往後移一項
                cout<<","<<fn;
            }
            cout<<endl;

            break;

        default:
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>mode;
        }

        cout<<"Enter mode: "; // 輸入mode
        cin>>mode;
        while(mode!=-1 && mode!=1 && mode!=2 && mode!=3) // mode須為-1,1,2,3
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>mode;
        }

    }
    return 0;
}
