#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

int main()
{
    int a = 0;                                                      //設定所需變數
    int a1 = 0; int d1 = 0; int n1 = 0;
    int a2 = 0; int r2 = 0; int n2 = 0;
    int n3 = 0; int f1 = 1; int f2 = 1; int f3 = 1; int fn = 0;

    cout << "Calculate the progression." << endl;
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;
    cout << "Enter mode: ";

    while(cin >> a)
    {
        while (a<=-2 || a==0 || a>3)                                //判斷輸入模式是否符合範圍
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> a;
        }

        switch(a)
        {
        case 1:                                                     //等差數列
            cout << "Arithmetic progression" << endl;
            cout << "a1: ";
            cin >> a1;
            while(a1<=0)                                            //判斷a1是否為非零正整數
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a1;
            }

            cout << "d: ";
            cin >> d1;
            while(d1<=0)                                            //判斷d是否為非零正整數
            {
                cout << "Out of range!" << endl;
                cout << "d: ";
                cin >> d1;
            }

            cout << "n: ";
            cin >> n1;
            while(n1<=0)                                            //判斷n是否為非零正整數
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n1;
            }

            for (int i=0; i<n1; i++)                                //輸出等差數列
            {
                cout << (a1+d1*i);

                if (i<n1-1)
                    cout << ",";
            }
            cout << endl;
            cout << "Enter mode: ";
            break;

        case 2:                                                     //等比數列
            cout << "Geometric progression" << endl;
            cout << "a1: ";
            cin >> a2;
            while(a2<=0)                                            //判斷a1是否為非零正整數
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a2;
            }

            cout << "r: ";
            cin >> r2;
            while(r2<=0)                                            //判斷r是否為非零正整數
            {
                cout << "Out of range!" << endl;
                cout << "r: ";
                cin >> r2;
            }

            cout << "n: ";
            cin >> n2;
            while(n2<=0)                                            //判斷n是否為非零正整數
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n2;
            }

            for (int i=0; i<n2; i++)                                //輸出等比數列
            {
                cout << (a2*pow(r2,i));

                if (i<n2-1)
                    cout << ",";
            }
            cout << endl;
            cout << "Enter mode: ";
            break;

        case 3:                                                     //費氏數列
            cout << "Fibonacci sequence" << endl;
            cout << "n: ";
            cin >> n3;
            while(n3<=0)                                            //判斷n是否為非零正整數
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n3;
            }
            while(f3<=n3)                                           //輸出費氏數列
            {
                if(f3==1)                                           //第1項
                    fn=f1;
                if(f3==2)                                           //第2項
                    fn=f2;
                if(f3>2)                                            //第3項以後
                {
                    fn=f1+f2;                                       //前2項相加
                    f1=f2;                                          //進位
                    f2=fn;                                          //進位
                }
                cout<<fn;
                if (f3<n3)
                    cout << ",";
                f3++;
            }
            cout << endl;
            cout << "Enter mode: ";
            break;
        case -1:                                                    //離開程式
            return 0;
        }
    }
    return 0;
}
