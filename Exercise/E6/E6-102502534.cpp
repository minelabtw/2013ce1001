#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int Entermode=0;//宣告變數
    int a1=0;
    int d=0;
    int n=0;
    int r=0;
    int x=1;
    int y=1;
    int f1=1;
    int f2=1;
    int fn=0;

    cout<<"Calculate the progression."<<endl<<"Choose the mode."<<endl<<"Arithmetic progression=1."<<endl<<"Geometric progression=2"<<endl<<"Fibonacci sequence=3."<<endl<<"Exit=-1."<<endl;
    while(Entermode!=-1)
    {
        cout<<"Enter mode: ";
        cin>>Entermode;
        switch(Entermode)//選擇模式
        {
        case 1://輸入的模式為1時
            cout<<"Arithmetic progression"<<endl;

            cout<<"a1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl<<"a1: ";
                cin>>a1;
            }

            cout<<"d: ";
            cin>>d;
            while(d<=0)
            {
                cout<<"Out of range!"<<endl<<"d: ";
                cin>>d;
            }

            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }

            while(x<=n)
            {
                cout<<a1+(x-1)*d;
                if(x==n)
                {
                    cout<<endl;
                    break;
                }
                else
                {
                    cout<<",";
                }
                x++;
            }
            break;

        case 2://輸入的模式為2時
            cout<<"Geometric progression"<<endl;

            cout<<"a1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl<<"a1: ";
                cin>>a1;
            }

            cout<<"r: ";
            cin>>r;
            while(r<=0)
            {
                cout<<"Out of range!"<<endl<<"r: ";
                cin>>r;
            }

            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }
            while(y<=n)
            {
                cout<<a1*pow(r,y-1);
                if(y==n)
                {
                    cout<<endl;
                    break;
                }
                else
                {
                    cout<<",";
                }
                y++;
            }
            break;

        case 3://輸入的模式為3時
            cout<<"Fibonacci sequence"<<endl;

            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }

            if(n==1)
            {
                cout<<"1";
            }
            else if(n==2)
            {
                cout<<"1,1";
            }
            else
            {
                cout<<"1,1,";
                for(int i=3;i<=n;i++)
                {
                    fn=f1+f2;
                    f1=f2;
                    f2=fn;
                    cout<<fn;
                    if(i==n)
                    {
                        cout<<endl;
                        break;
                    }
                    else
                    {
                        cout<<",";
                    }
                }
            }
            break;

        case -1://輸入的變數為-1時
            break;

        default://輸入的變數不為1&2&3&-1時
            cout<<"Illegal input!"<<endl;
        }
    }
    return 0;
}
