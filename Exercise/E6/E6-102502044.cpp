/*************************************************************************
  > File Name: E6-102502044.cpp
  > Author: rockwyc992
  > Mail: rockwyc992@gmail.com 
  > Created Time: 西元2013年10月30日 (週三) 16時17分12秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

/* function to check input */
inline int check_mode(int x);
inline int check_num(int x);

/* function to output squence */
inline void arith(int a1, int d, int n);
inline void geome(int a1, int r, int n);
inline void fibon(int n);

/* function to input number */
inline void input(const char *str, const char *error, int *tmp,int (*check)(int))

int main()
{
	/* print ui */
	puts("Calculate the progression.");
	puts("Choose the mode.");
	puts("Arithmetic progression=1.");
	puts("Geometric progression=2.");
	puts("Fibonacci sequence=3.");
	puts("Exit=-1.");

	/* declare all variables */
	int mode;
	int a1, n, r, d;
	
	for(;;)
	{
		/* input the mode */
		input("Enter mode: ", "Illegal input!", &mode, check_mode);

		/* check which mode */
		switch(mode)
		{
			case 1:
				/* print ui */
				puts("Arithmetic progression");

				/* input all variable */
				input("a1: ", "Out of range!", &a1, check_num);
				input("d: " , "Out of range!", &d , check_num);
				input("n: " , "Out of range!", &n , check_num);
				
				/* output the arithmetic sequence*/
				arith(a1, d, n);
				
				break;
			
			case 2:
				/* print ui */
				puts("Geometric progression");

				/* input all variable */
				input("a1: ", "Out of range!", &a1, check_num);
				input("r: " , "Out of range!", &r , check_num);
				input("n: " , "Out of range!", &n , check_num);
				
				/* output the geometric sequence*/
				geome(a1, r, n);

				break;
			
			case 3:
				/* print ui */
				puts("Fibonacci sequence");
				
				/* input all variable */
				input("n: " , "Out of range!", &n , check_num);

				/* output the fibonacci sequence*/
				fibon(n);

				break;

			case -1:
				/* exit the program */
				return 0;
		}
	}
	return 0;
}

/* x means the number will be check */
inline int check_mode(int x)
{
	/* check x is in set{1, 2, 3, -1} */
	switch(x)
	{
		case 1:
		case 2:
		case 3:
		case -1:
			return 1;

		default:
			return 0;
	}
}

/* x means the number will be check */
inline int check_num(int x)
{
	/* check x is in range (0, unlimit) */
	return x > 0;
}

/*******************************************/
/*   str means the ui message			   */
/* error means the error message		   */
/*  *tmp means the point of input		   */
/* check means the function to check input */
/*******************************************/
inline void input(const char *str, const char *error, int *tmp, int (*check)(int))
{
	for(;;)
	{
		printf("%s", str);	// print ui

		scanf("%d", tmp);	// input number

		if(check(*tmp))		// check the input is in range
			return;
		else
			puts(error);
	}
}

/*****************************************/
/* output the Arithmetic sequence        */
/* a1 means the first number of sequence */
/* an = a1 + d*n                         */
/*****************************************/
inline void arith(int a1, int d, int n)
{
	printf("%d", a1);
	
	/* use for loop to output all sequence */
	for(int i=1, tmp=d; i<n ; i++,tmp+=d)
		printf(",%d", a1 + tmp);

	putchar('\n');
}

/*****************************************/
/* output the Geometric sequence         */
/* a1 means the first number of sequence */
/* an = a1 * r^n                         */
/*****************************************/
inline void geome(int a1, int r, int n)
{
	printf("%d", a1);

	/* use for loop to output all sequence */
	for(int i=1, tmp=r; i<n ; i++,tmp*=r)
		printf(",%d", a1 * tmp);
	
	putchar('\n');
}

/*********************************/
/* output the Fibonacci sequence */
/* n means the lenth of sequence */
/*********************************/
inline void fibon(int n)
{
	/* f1 = an   */
	/* f2 = an+1 */
	int f1, f2;
	f1 = 1;
	f2 = 2;

	/* print first number */
	putchar('1');
	n--;

	while(n--)
	{
		/* output an */
		printf(",%d", f1);

		/* let f1 to an+2 = an + an+1 */
		f1 = f1+f2;
		
		/* swap f1 f2, then*/
		/* f1 = an+1 */
		/* f2 = an+2 */
		int t = f1;
		f1 = f2;
		f2 = t;
	}
	putchar('\n');
}


