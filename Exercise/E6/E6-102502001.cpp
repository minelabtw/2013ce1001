#include <iostream>
#include <cmath>
using namespace std;

double Square(double num1)
{
    return pow(num1,2);
}

double Cube(double num1)
{
    return pow(num1,3);
}

double Factorial(double num1)
{
    double x=1;
    double total=1;
    while(x<=num1)
    {
        total=total*x;
        x++;
    }
    return total;
}

int main()
{
    double num1;

    cout<<"Please enter a number( >0 ): ";
    cin>>num1;

    while(num1<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Please enter a number( >0 ): ";
        cin>>num1;
    }

    cout<<"Square: "<<Square(num1)<<endl;
    cout<<"Cube: "<<Cube(num1)<<endl;
    cout<<"Factorial: "<<Factorial(num1);

    return 0;
}
