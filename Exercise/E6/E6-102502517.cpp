#include <iostream>

using namespace std;

int main()
{
    int mode = 0 , a1 = 0 , d = 0 , n = 0 , r = 0 , n_min = 0 , k = 2 , a2 = 1 , a3 = 1; //宣告變數

    cout << "Calculate the progression." << endl;
    cout << "Choose  the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;

    while (mode==0)
    {
        cout << "Enter mode: ";
        cin >> mode;  //輸入模式

        switch (mode)  //使用switch迴圈
        {
        case 1:
            a1 = 0 , d = 0 , n = 0 , r = 0 , n_min = 0 , k = 2 , a2 = 1 , a3 = 1; //初始化變數

            cout << "Arithmetic progression" << endl;

            while (a1<=0)
            {
                cout << "a1: ";
                cin >> a1;
                if (a1<=0)
                    cout << "Out of range!" << endl;
            }

            while (d<=0)
            {
                cout << "d: ";
                cin >> d;
                if (d<=0)
                    cout << "Out of range!" << endl;
            }

            while (n<=0)
            {
                cout << "n: ";
                cin >> n;
                if (n<=0)
                    cout << "Out of range!" << endl;
            }

            while (n_min<n)
            {
                int a=a1+d*n_min;
                cout << a << ",";
                n_min++;
            }
            cout << endl;
            mode = 0;
            break;

        case 2:
            a1 = 0 , d = 0 , n = 0 , r = 0 , n_min = 0 , k = 2 , a2 = 1 , a3 = 1;  //初始化變數

            cout << "Geometric progression" << endl;

            while (a1<=0)
            {
                cout << "a1: ";
                cin >> a1;
                if (a1<=0)
                    cout << "Out of range!" << endl;
            }

            while (r<=0)
            {
                cout << "r: ";
                cin >> r;
                if (r<=0)
                    cout << "Out of range!" << endl;
            }

            while (n<=0)
            {
                cout << "n: ";
                cin >> n;
                if (n<=0)
                    cout << "Out of range!" << endl;
            }

            while (n_min<n)
            {
                cout << a1 << ",";
                a1=a1*r;
                n_min++;
            }
            cout << endl;
            mode = 0;
            break;

        case 3:
            a1 = 0 , d = 0 , n = 0 , r = 0 , n_min = 0 , k = 2 , a2 = 1 , a3 = 1;  //初始化變數

            cout << "Fibonacci sequence" << endl;

            while (n<=0)
            {
                cout << "n: ";
                cin >> n;
                if (n<=0)
                    cout << "Out of range!" << endl;
            }

            cout << "1,1,";

            while (k<n)
            {
                int f=a2+a3;
                cout << f << ",";
                a2 = a3;
                a3 = f;
                k++;
            }
            cout << endl;
            mode = 0;
            break;

        default:  //輸入其他值時跳回迴圈
            cout << "Illegal input!" << endl;
            mode = 0;
            break;

        case -1:  //結束迴圈
            break;
        }
    }

    return 0;
}
