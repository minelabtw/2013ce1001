#include <iostream>  //引入標頭檔

using namespace std;

int main()
{
    int mode=0,a1,n,d,r,x=1,y; //宣告變數

    cout << "Calculate the progression." << endl;
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;

    while (mode==0)
    {
        cout << "Enter mode: ";
        cin >> mode;
        while(mode!=1&&mode!=2&&mode!=3&&mode!=-1) //當mode不為-1、1、2、3時，顯示錯誤訊息，並要求重新輸入
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> mode;
        }

        switch(mode) //依據變數mode的值來判斷進入哪一個case
        {
        case 1:
            cout << "Arithmetic progression" << endl;

            cout << "a1: ";
            cin >> a1;
            while(a1<=0) //當a1不是自然數時，顯示錯誤訊息，並要求重新輸入
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a1;
            }

            cout << "d: ";
            cin >> d;
            while(d<=0) //當d不是自然數時，顯示錯誤訊息，並要求重新輸入
            {
                cout << "Out of range!" << endl;
                cout << "d: ";
                cin >> d;
            }

            cout << "n: ";
            cin >> n;
            while(n<=0) //當n不是自然數時，顯示錯誤訊息，並要求重新輸入
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }

            for(x=1; x<=n-1; x++) //顯示前n-1項數值及其期間之逗號
            {
                cout << a1+d*(x-1) << ",";

            }
            cout << a1+d*(n-1) << endl; //顯示第n項之數值
            break; //結束此case

        case 2:
            cout << "Geometric progression" << endl;

            cout << "a1: ";
            cin >> a1;
            while(a1<=0) //當a1不是自然數時，顯示錯誤訊息，並要求重新輸入
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a1;
            }

            cout << "r: ";
            cin >> r;
            while(r<=0) //當r不是自然數時，顯示錯誤訊息，並要求重新輸入
            {
                cout << "Out of range!" << endl;
                cout << "r: ";
                cin >> r;
            }

            cout << "n: ";
            cin >> n;
            while(n<=0) //當n不是自然數時，顯示錯誤訊息，並要求重新輸入
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }

            for(x=1; x<=n-1; x++) //顯示前n-1項數值及其期間之逗號
            {
                if(x>1)
                    a1=a1*r;
                cout << a1 << ",";

            }
            cout << a1*r << endl; //顯示第n項之數值
            break; //結束此case

        case 3:
            cout << "Fibonacci sequence" << endl;

            cout << "n: ";
            cin >> n;
            while(n<=0) //當n不是自然數時，顯示錯誤訊息，並要求重新輸入
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }

            int f1=1,f2=1; //宣告並設定費氏數列的第一及第二項為1
            int fn;

            for(x=1; x<=n-1; x++) //顯示前n-1項數值及其期間之逗號
            {
                if(x==1) //第一項時，顯示數值設定為1
                    fn=f1;
                if(x==2) //第二項時，顯示數值設定為1
                    fn=f2;
                if(x!=1&&x!=2) //第三項以上(包含)時，顯示數值設定為前兩項之總和
                {
                    fn=f1+f2;
                    f1=f2;
                    f2=fn;
                }
                cout << fn << "," ;
            }
            cout << f1+fn<< endl; //顯示第n項之數值
            break; //結束此case
        }
        if(mode==-1) //當mode為-1時，跳出while迴圈並結束程式
            break;
        mode=0;
    }

    return 0;
}
