#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    int mode=0;              //宣告變數：mode表示使用者欲運行的模式
    double a1,d,n;           //a1為首項、d為公差或公比、n為項數
    cout << "Calculate the progression." << endl <<
         "Choose the mode." << endl <<
         "Arithmetic progression=1." << endl <<
         "Geometric progression=2." << endl <<
         "Fibonacci sequence=3." << endl <<
         "Exit=-1." << endl;                             //印出本程式的功能以及操作方式
    while (!(mode==1 || mode==2 || mode==3 || mode==-1)) //先把mode設定為錯誤的情況，讓程式可以進入這個while迴圈
    {
        cout << "Enter mode: ";
        cin >> mode;
        if (!(mode==1 || mode==2 || mode==3 || mode==-1))
        {
            cout << "Illeagal input!" <<endl;
        }
        switch (mode)
        {
        case 1:                 //等差數列
            a1=0;
            d=0;
            n=0;                //把會用到的變數歸零
            cout <<"Arithmetic progression" << endl;
            while (a1<=0)       //判斷首項是否合法
            {
                cout << "a1: ";
                cin >> a1;
                if (a1<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            while (d<=0)         //判斷ˋ公差是否合法
            {
                cout << "d: ";
                cin >> d;
                if (d<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            while (n<=0)        //判斷項數是否合法
            {
                cout << "n: ";
                cin >> n;
                if (n<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            for (int counter=1 ; counter<=n-1 ; counter++)   //先計算前n-1項
            {
                cout << a1+d*(counter-1) << ",";
            }
            cout <<a1+d*(n-1) << endl;                      //最後一項獨立計算，不要加逗號
            break;
        case 2:                 //等比數列
            a1=0;
            d=0;
            n=0;                //把會用到的變數歸零
            cout <<"Geometric progression" <<endl;
            while (a1<=0)       //判斷首項是否合法
            {
                cout << "a1: ";
                cin >> a1;
                if (a1<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            while (d<=0)       //判斷公比是否合法
            {
                cout << "r: ";
                cin >> d;
                if (d<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            while (n<=0)       //判斷項數是否合法
            {
                cout << "n: ";
                cin >> n;
                if (n<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            for (int counter =1 ; counter <= n-1 ; counter++)
            {
                cout << a1*pow(d,(counter-1)) << ",";       //先計算前n-1項
            }
            cout <<a1*pow(d,(n-1)) << endl;                 //最後一項獨立計算，不要加逗號
            break;
        case 3:                 //費氏數列
        {
            cout << "Fibonacci sequence" << endl ;
            n=0;                //先將n歸零
            while (n<=0)        //判斷項數是否合法
            {
                cout << "n: ";
                cin >> n;
                if (n<=0)
                {
                    cout << "Out of range!" << endl;
                }
            }
            int left=1;         //left是該項左邊
            int leftleft=0;     //leftleft，該項左邊再左邊，也就是左邊數兩項
            int an;
            cout << "1,";       //由於至少有1項數列，所以先把左邊項=1，並直接印出1
            for (int counter =2 ; counter <= n-1 ; counter++)  //從第二項開始算，算到倒數到二項
            {
                an=left+leftleft;           //計算當前的項
                cout << an << ",";          //印出
                leftleft=left;              //左邊左邊等於左邊（它的右邊那項）
                left=an;                    //左邊那項等於當前的項
            }
            cout << left+leftleft <<endl;   //印出最後一項
            break;
        }
        case -1:                 //結束計算
            break;
        }
        if(mode!=-1)            //若不結束計算，就把mode歸零
            mode=0;
    }
    return 0;
}


