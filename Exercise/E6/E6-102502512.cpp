#include<iostream>
using namespace std;
int main()
{
    int x;
    cout<<"Calculate the progression.\n";
    cout<<"Choose the mode.\n";
    cout<<"Arithmetic progression=1.\n";
    cout<<"Geometric progression=2.\n";
    cout<<"Fibonacci sequence=3.\n";
    cout<<"Exit=-1.\n";
    do
    {
        int a1,d,r,n;
        int f1=1,f2=1,fn;
        do
        {
            cout<<"Enter mode: ";                                               //Line 16~23:To let user input the mode and make sure
            cin>>x;                                                             //that the number is correct
            if(x!=1&&x!=2&&x!=3&&x!=-1)
                cout<<"Illegal input!\n";
        }
        while(x!=1&&x!=2&&x!=3&&x!=-1);
        switch (x)
        {
        case 1:                                                                  //Line 26~62:When user input 1 then let user input the
        {                                                                        //arithmetic progression's a1, d, and n
            cout<<"Arithmetic progression\n";                                    //and then print it
        }
        do
        {
            cout<<"a1: ";
            cin>>a1;
            if(a1<=0)
                cout<<"Out of range!\n";
        }
        while(a1<=0);
        do
        {
            cout<<"d: ";
            cin>>d;
            if(d<=0)
                cout<<"Out of range!\n";
        }
        while(d<=0);
        do
        {
            cout<<"n: ";
            cin>>n;
            if(n<=0)
                cout<<"Out of range!\n";
        }
        while(n<=0);
        for(int i=0; i<n; i++)
        {
            cout<<a1;
            if(i!=n-1)
                cout<<",";
            else
                cout<<endl;
            a1=a1+d;
        }
        break;
        case 2:                                                                 //Line 63~100:when user input 2 and then let user
        {                                                                       //input geometric progression's a1, r, and n
            cout<<"Geometric progression \n";                                   //And print it out
        }
        do
        {
            cout<<"a1: ";
            cin>>a1;
            if(a1<=0)
                cout<<"Out of range!\n";
        }
        while(a1<=0);
        do
        {
            cout<<"r: ";
            cin>>r;
            if(r<=0)
                cout<<"Out of range!\n";
        }
        while(r<=0);
        do
        {
            cout<<"n: ";
            cin>>n;
            if(n<=0)
                cout<<"Out of range!\n";
        }
        while(n<=0);
        for(int i=0; i<n; i++)
        {
            cout<<a1;
            if(i!=n-1)
                cout<<",";
            else
                cout<<endl;
            a1*=r;
        }
        break;
        case 3:                                                         //Line 102~130:when user input 3 let user input
        {                                                               // fibonacci sequence's n and print it out
            cout<<"Fibonacci sequence\n";
        }
        do
        {
            cout<<"n: ";
            cin>>n;
            if(n<=0)
                cout<<"Out of range!\n";
        }
        while(n<=0);
        for(int j=0; j<n; j++)
        {
            if(j==0)                                                     //first number of fibonacci sequence
                fn=f1;
            if(j==1)                                                     //second number of fibonacci sequence
                fn=f2;
            if(j>1)
            {
                fn=f1+f2;                                                //make fibonacci sequence
                f1=f2;
                f2=fn;
            }
            cout<<fn;
            if(j!=n-1)
                cout<<",";
            else
                cout<<endl;
        }
        break;

        default:
            break;
        }

    }
    while(x==1||x==2||x==3);
    return 0;
}
