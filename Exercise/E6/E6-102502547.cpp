#include <iostream>
using namespace std;

main()
{
    int a=0;
    int x=0;
    int y=0;
    int z=0; //宣告4個數字a,x,y,z，值為0

    cout << "Calculate the progression.\nChoose the mode.\n";
    cout << "Arithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.\n";
    do
    {
        cout << "Enter mode: ";
        cin >> a;
        if(a!=-1) //如果a不等於-1時，執行
        {
            if(a==1) //如果a等於1，執行等差數列
            {

                cout << "Arithmetic progression\n";
                do
                {
                    cout << "a1: ";
                    cin >> x;
                    if(x<=0)
                        cout << "Out of range!\n";
                }
                while(x<=0); //x非正整數時重新輸入

                do
                {
                    cout << "d: ";
                    cin >> y;
                    if(y<=0)
                        cout << "Out of range!\n";
                }
                while(y<=0); //y非正整數時重新輸入

                do
                {
                    cout << "n: ";
                    cin >> z;
                    if(z<=0)
                        cout << "Out of range!\n";
                }
                while(z<=0); //z非正整數時重新輸入

                for(int i=1; i<=z; i++) //執行z次
                {
                    cout << x;
                    x=x+y; //x值變為x+y
                    if(i!=z)
                        cout << ",";
                    else
                        cout << endl;
                }
            }

            else if(a==2) //如果a等於2時，執行等比數列
            {
                cout << "Geometric progression\n";
                do
                {
                    cout << "a1: ";
                    cin >> x;
                    if(x<=0)
                        cout << "Out of range!\n";
                }
                while(x<=0); //x非正整數時重新輸入

                do
                {
                    cout << "r: ";
                    cin >> y;
                    if(y<=0)
                        cout << "Out of range!\n";
                }
                while(y<=0); //y非正整數時重新輸入

                do
                {
                    cout << "n: ";
                    cin >> z;
                    if(z<=0)
                        cout << "Out of range!\n";
                }
                while(z<=0); //z非正整數時重新輸入

                for(int i=1; i<=z; i++) //執行z次
                {
                    cout << x;
                    x=x*y; //x值變為x*y
                    if(i!=z)
                        cout << ",";
                    else
                        cout << endl;
                }
            }

            else if(a==3) //a等於3時，執行費氏數列
            {
                cout << "Fibonacci sequence\n";
                do
                {
                    cout << "n: ";
                    cin >> x;
                    if(x<=0)
                        cout << "Out of range!\n";
                }
                while(x<=0); //x非正整數時重新輸入

                int b[x]; //宣告1陣列b，有x項
                b[0]=1; //第一項為1
                b[1]=1; //第二項為1
                for(int i=0; i<x; i++) //執行x次
                {
                    if(i>=2) //i>=2時執行
                        b[i]=b[i-1]+b[i-2]; //第i+1項為前2項之和
                    cout << b[i];
                    if(i!=x-1)
                        cout << ",";
                    else
                        cout << endl;
                }
            }
            else //其他情況
                cout << "Illegal input!\n";
        }
    }
    while (a!=-1); //a不等於-1時執行迴圈

    return 0;
}
