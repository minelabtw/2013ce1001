#include<iostream>

void Arithmetic()   //等差
{
    int A[3];   //宣告
    char out[][5]={"a1: ","d: ","n: "};

    std::cout<<"Arithmetic progression\n";  //輸出敘述

    for(int i=0;i!=3;i++)   //輸入
    {
        std::cout<<out[i];
        std::cin>>A[i];
        if(A[i]<=0)
        {
            std::cout<<"Out of range!\n";
            i--;
        }
    }

    std::cout<<A[0];    //輸出結果
    for(int i=1;i!=A[2];i++)
        std::cout<<','<<A[0]+i*A[1];
}

int pow(int x, int k)   //計算次方
{
    if(k==1)
        return x;
    return pow(x*x,k/2)*(k&1?x:1);
}
void Geometric()    //等比
{
    int G[3];   //宣告
    char out[][5]={"a1: ","r: ","n: "};

    std::cout<<"Geometric progression\n";   //輸出敘述

    for(int i=0;i!=3;i++)   //輸入
    {
        std::cout<<out[i];
        std::cin>>G[i];
        if(G[i]<=0)
        {
            std::cout<<"Out of range!\n";
            i--;
        }
    }

    std::cout<<G[0];    //輸出結果
    for(int i=1;i!=G[2];i++)
        std::cout<<','<<G[0]*pow(G[1],i);
}

void Fibonacci()    //費式
{
    int n,f[2]={1,0};
    std::cout<<"Fibonacci sequence\n";

    do  //輸入
    {
        std::cout<<"n: ";
        std::cin>>n;
        if(n<=0)
            std::cout<<"Out of range!\n";
    }while(n<=0);

    std::cout<<'1';
    for(int i=1;i!=n;i++)   //輸出結果
    {
        f[i%2]=f[0]+f[1];
        std::cout<<','<<f[i%2];
    }
}

int main()
{
    int mode=0; //宣告模式

    std::cout<<"Calculate the progression.\n"<< //輸出敘述
               "Choose the mode.\nArithmetic progression=1.\n"<<
               "Geometric progression=2.\n"<<
               "Fibonacci sequence=3.\n"<<
               "Exit=-1.";

    do
    {
        std::cout<<"\nEnter mode: ";    //輸入
        std::cin>>mode;

        switch(mode)
        {
        case 1: //等差
            Arithmetic();
            break;
        case 2: //等比
            Geometric();
            break;
        case 3: //費式
            Fibonacci();
            break;
        case -1:    //結束程式
            return 0;
        default:    //輸入不符
            std::cout<<"Illegal input!";
        }
    }while(1);
}
