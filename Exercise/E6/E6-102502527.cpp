#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

int main()
{
    int mode = 0;
    int a1 = 0;
    int d = 0;
    int r = 0;
    int n = 0;
    int final1 = 0;
    int final2 = 0;
    int i = 1;
    int e = 1;
    int f = 1;
    int f1 = 1;
    int f2 = 2;
    int fn = 0;

    cout << "Calculate the progression." << endl;//輸出字彙
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;

    cout << "Enter mode: ";
    mode = cin.get();

    while ( mode != -1 )
    {
        switch ( mode )//mode的值做判斷
        {
        case '1'://當mode為1時做以下動作
            cout << "Arithmetic progression" <<endl;
            cout << "a1: ";
            cin >> a1;
            while ( a1 <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a1;
            }

            cout << "d: ";
            cin >> d;
            while ( d <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "d: ";
                cin >> d;
            }

            cout << "n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }

            final1 = a1 + d * ( n - 1 );//設定數列

            for ( int i = a1 ; i <= final1; i++ )
            {
                while ( e > 0 && e <= ( n - 1 ) && i == a1 + d * ( e - 1 ) )
                {
                    cout << i << ",";
                    e++;
                }
            }
            cout << final1 << endl;//輸出值後重新輸入
            cout << "Enter mode: ";
            cin >> mode;

        case '2'://當mode為2時做以下動作
            cout << "Geometric progression" << endl;
            cout << "a1: ";
            cin >> a1;
            while ( a1 <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a1;
            }

            cout << "r: ";
            cin >> r;
            while ( r <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "r: ";
                cin >> r;
            }

            cout << "n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }

            final2 = a1 * pow(r,n-1);//設定數列

            for ( int i = a1 ; i <= final2 ; i++ )
            {
                while ( f > 0 && f <= ( n - 1 ) && i == a1 * pow(r,f-1) )
                {
                    cout << i << ",";
                    f++;
                }
            }
            cout << final2 << endl;//輸出完後重新輸入
            cout << "Enter mode: ";
            cin >> mode;

        case '3'://當mode為3時做以下動作
            cout << "Fibonacci sequence" << endl;
            cout << "n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }

            cout << "1";
            for ( i = 1 ; i <= ( n - 1 ) ; i++ )
            {
                if ( i == 1 )
                    fn = f1;
                if ( i == 2 )
                    fn = f2;
                if ( i > 2 )
                {
                    fn = f1 + f2;
                    f1 = f2;
                    f2 = fn;
                }

                cout << "," << fn;
            }
            cout << endl;//輸出完後重新輸入
            cout << "Enter mode: ";
            cin >> mode;

        case '-1'://mode為-1時結束
            break;

        default://mode為其餘數值時重新輸入
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> mode;
        }
    }

    return 0;
}
