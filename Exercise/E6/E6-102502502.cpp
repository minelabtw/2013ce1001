#include<iostream>
#include<iomanip>
#include<cmath>
using namespace std;

int main()
{
    int w;
    int a1,d,n,r;
    int a;
    int i;
    int f1 = 1,f2 = 1;
    int fn;
    cout << "Calculate the progression."<< endl << "Choose the mode." << endl << "Arithmetic progression=1." << endl;

    cout <<  "Geometroc progression=2." << endl << "Fibonacci sequence=3." << endl << "Exit=-1." << endl << "Enter mode: ";
    while (cin >> w)
    {
        switch (w)
        {
        case 1:
            cout << "Arithmetic progression" << endl << "a1: ";
            cin >> a1;
            while (a1<1)                                        //輸入首項、公差及項數，如果其值小於等於0，則顯示超出範圍
            {
                cout << "Out of range!" << endl << "a1: ";
                cin >> a1;
            }
            cout << "d: ";
            cin >> d;
            while (d<1)
            {
                cout << "Out of range!" << endl << "d: ";
                cin >> d;
            }
            cout << "n: ";
            cin >> n;
            while (n<1)
            {
                cout << "Out of range!" << endl << "n: ";
                cin >> n;
            }
            for(i = 1; i <= n ; i++)
            {
                a = a1+ d*(i-1);
                if(i >= 1 && i <= n-1)
                {
                    cout << a << ",";
                }
                else
                {
                    cout << a << " ";
                }
            }
            break;

        case 2:
            double amount;
            double a1;
            double r;

            cout << "Geometric progression" << endl << "a1: ";
            cin >> a1;
            while (a1<1)                                        //輸入首項、公比及項數，如果其值小於等於0，則顯示超出範圍
            {
                cout << "Out of range!" << endl << "a1: ";
                cin >> a1;
            }
            cout << "r: ";
            cin >> r;
            while (r<1)
            {
                cout << "Out of range!" << endl << "r: ";
                cin >> r;
            }
            cout << "n: ";
            cin >> n;
            while (n<1)
            {
                cout << "Out of range!" << endl << "n: ";
                cin >> n;
            }
            for(i = 1; i <= n ; i++)
            {
                amount = a1*pow(0.+r,i);
                if(i >= 1 && i <= n-1)
                {
                    cout << amount << ",";
                }
                else
                {
                    cout << amount << " ";
                }
            }
            break;

        case 3:
            cout << "Fibonacci sequence" << endl << "n: ";
            cin >> n;                                       //輸入項數，如果其值小於等於0，則顯示超出範圍
            while (n<1)
            {
                cout << "Out of range!" << endl << "n: ";
                cin >> n;
            }
            for(i = 1; i <= n ; i++)
            {
                if(i==1)
                {
                    fn=f1;
                }
                if(i==2)
                {
                    fn=f2;
                }
                if(i > 2)
                {
                    fn = f1+f2;                     //前2項相加
                    f1 = f2;                       //進位
                    f2 = fn;                       //進位;
                }
                if(i >= 1 && i <= n-1)
                {
                    cout << fn << ",";
                }
                else
                {
                    cout << fn << " ";
                }
            }
            break;
        case -1:
            cout << "Exit\n";
            break;

        default:
            cout << "Illegal input!" << endl;
            break;
        }
        if (w==-1)
        {
            break;
        }
        cout << endl << "Enter mode: ";
    }
    return 0;
}
