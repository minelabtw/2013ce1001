#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int choosemode;

    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;

    while(1)
    {
        cout<<"Enter mode: ";
        cin>>choosemode;

        int a1,d,r,n,mount=0;                       //a1兜,dそ畉,rそゑ,n兜计,mount璸Ωノ

        switch(choosemode)
        {
        case 1:                                     //秈单畉计
        {
            cout<<"Arithmetic progression"<<endl;
            int gap;
            cout<<"a1: ";
            cin>>a1;
            while(a1==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            while(d==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"d: ";
                cin>>d;
            }
            cout<<"n: ";
            cin>>n;
            while(n==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            for(mount=1; mount<=n; ++mount)
            {
                gap=d-a1;
                cout<<d*mount-gap;
                if(mount==n)
                    break;
                cout<<",";
            }
            break;
        }
        case 2:                                     //秈碭计
        {
            cout<<"Geometric progression"<<endl;
            cout<<"a1: ";
            cin>>a1;
            while(a1==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"r: ";
            cin>>r;
            while(r==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"r: ";
                cin>>r;
            }
            cout<<"n: ";
            cin>>n;
            while(n==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            for(mount=0; mount<n; ++mount)
            {
                cout<<a1*pow(r,mount);
                if(mount==n-1)
                    break;
                cout<<",";
            }
            break;
        }
        case 3:                                     //秈禣ん计
        {
            cout<<"Fibonacci sequence"<<endl;
            cout<<"n: ";
            cin>>n;
            while(n==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            int a[n];
            a[0]=1,a[1]=1;
            for(mount=2; mount<n; ++mount)
            {
                a[mount]=a[mount-2]+a[mount-1];
            }
            for(mount=0; mount<n; ++mount)
            {
                cout<<a[mount];
                if(mount==n-1)
                    break;
                cout<<",";
            }
            break;
        }
        case -1:
        {
            break;
        }
        default:
        {
            cout<<"Illegal input!";
            break;
        }
        }
        cout<<endl;

        if(choosemode==-1)
            break;
        else
            continue;
    }
    return 0;
}
