#include<iostream>
using namespace std;

int main()
{
    int a = 0;  //宣告型別為 整數(int) 的九個變數，並初始化其數值，a為判斷值。
    int a1 = 0; //初始化首項為0。
    int d = 0;  //初始化公差為0
    int r = 0;  //初始化公比為0
    int n = 0;  //初始化項數為0
    int k = 0;  //k代表正在計算第k項，初始化其數值為0。
    int f1 = 1; //初始化費氏數列的第一項為1。
    int f2 = 1; //初始化費氏數列的第二項為1。
    int f3 = 0; //初始化費氏數列的第三項為0。
    cout << "Calculate the progression." << endl; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cout <<"Choose the mode." << endl;
    cout <<"Arithmetic progression=1."<< endl;
    cout <<"Geometric progression=2."<< endl;
    cout <<"Fibonacci sequence=3."<< endl;
    cout <<"Exit=-1."<< endl;
    while (1) //使用while迴圈，使判斷值(a)不為-1時，能重複做計算。
    {
        cout << "Enter mode: ";
        cin >> a;
        while (a != 1 && a != 2 && a != 3 && a != -1)
        //使用while迴圈，檢驗使用者所輸入的判斷值是否合乎標準，若不符，則要求其重新輸入。
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> a;
        }
        if (a == -1) //當判斷值(a)為-1時，結束程式。
        {
            break;
        }
        else
        {
            switch( a ) //使用switch迴圈，使其能針對不同的判斷值做不同的運算。
            {
            case 1:
                cout << "Arithmetic progression" << endl;
                cout << "a1: ";
                cin >> a1;
                while (a1 <= 0)
                {
                    cout << "Out of range!" << endl;
                    cout << "a1: ";
                    cin >> a1;
                }
                cout << "d: ";
                cin >> d;
                while (d <= 0)
                {
                    cout << "Out of range!" << endl;
                    cout << "d: ";
                    cin >> d;
                }
                cout << "n: " ;
                cin >> n;
                while (n <= 0)
                {
                    cout << "Out of range!" << endl;
                    cout << "n: ";
                    cin >> n;
                }
                for (k = 1; k <= n; k++) //使用for迴圈計算等差數列。
                {
                    if( k != 1 )
                    {
                        a1 += d;
                    }
                    cout << a1 ;
                    if ( k != n )
                    {
                        cout << "," ;
                    }
                }
                cout << endl;
                break;

            case 2:
                cout << "Geometric progression" << endl;
                cout << "a1: ";
                cin >> a1;
                while (a1 <= 0)
                {
                    cout << "Out of range!" << endl;
                    cout << "a1: ";
                    cin >> a1;
                }
                cout << "r: ";
                cin >> r;
                while (r <= 0)
                {
                    cout << "Out of range!" << endl;
                    cout << "r: ";
                    cin >> r;
                }
                cout << "n: ";
                cin >> n;
                while (n <= 0)
                {
                    cout << "Out of range!" << endl;
                    cout << "n: ";
                    cin >> n;
                }
                for (k = 1; k <= n; k++) //使用for迴圈計算等比數列。
                {
                    if( k != 1 )
                    {
                        a1 *= r;
                    }
                    cout << a1 ;
                    if ( k != n )
                    {
                        cout << "," ;
                    }

                }
                cout << endl;
                break;

            case 3:
                cout << "Fibonacci sequence" << endl;
                cout << "n: ";
                cin >> n;
                while (n <= 0)
                {
                    cout << "Out of range!" << endl;
                    cout << "n: ";
                    cin >> n;
                }
                cout << "1,1,";
                for (k = 3; k <= n; k++) //使用for迴圈計算費氏數列。
                {
                    f3 = f1 + f2;
                    cout << f3;
                    f1 = f2;
                    f2 = f3;
                    if ( k != n )
                    {
                        cout << "," ;
                    }
                }
                cout << endl;
                break;
            }
        }
    }
}
