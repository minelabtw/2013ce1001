#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int x,a,b,c,v,w,d,n,r; //設定九個變數
    cout<<"Calculate the progression."<<endl<<"Choose  the mode."<<endl<<"Arithmetic progression=1."<<endl<<"Geometric progression=2."<<endl<<"Fibonacci sequence=3."<<endl<<"Exit=-1.";
    //輸出說明
    for(v=0 ; v<=1 ; v++) //建立迴圈以能重複輸入
    {   cout<<endl<<"Enter mode: ";
        cin>>w; //輸入w
        switch(w) //判定w是否合格並且該進入何種運算
        {

        case 1: //等差運算
            cout<<"Arithmetic progression"<<endl;
            cout<<"a1: ";
            cin>>a;
            while(a<=0) //判斷數字是否超出範圍
            {
                cout<<"Out of range!"<<endl<<"a1: ";
                cin>>a;
            }
            cout<<"d: ";
            cin>>b;
            while(b<=0)
            {
                cout<<"Out of range!"<<endl<<"d: ";
                cin>>b;
            }
            cout<<"n: ";
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>c;
            }
            cout<<a;
            for(x=1 ; x<c ; x++) //輸出等差數列
            {   d=a+b;
                a=d;
                v=0;
                cout<<","<<a;
            }
            break;

        case 2: //等比運算
            cout<<"Geometric progression"<<endl;
            cout<<"a1: ";
            cin>>a;
            while(a<=0) //判斷數字是否合格
            {
                cout<<"Out of range!"<<endl<<"a1: ";
                cin>>a;
            }
            cout<<"r: ";
            cin>>b;
            while(b<=0)
            {
                cout<<"Out of range!"<<endl<<"r: ";
                cin>>b;
            }
            cout<<"n: ";
            cin>>c;
            while(c<=0)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>c;
            }
            cout<<a;
            for(x=1 ; x<c ; x++) //輸出等比數列
            {
                d=a*b;
                a=d;
                v=0;
                cout<<","<<a;
            }
            break;

        case 3: //費氏數列
            cout<<"Fibonacci sequence"<<endl;
            cout<<"n: ";
            cin>>n;
            while(n<=0) //檢查數字是否合格
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }
            a=1;
            b=1;
            cout<<a; //輸出費氏數列
            for(x=1 ; x<n ; x++)
            {

                c=a+b;
                a=b;
                b=c;
                v=0;
                cout<<","<<a;
            }
            break;


        case -1: //若輸入-1則結束運算
        v=1;
        break;



        default: //不合格輸出"Illegal input!"
        {
            cout<<"Illegal input!";
            v=0;
        }

        }
    }

    return 0;
}
