#include <iostream>

using namespace std;

int main()
{

    int a=0;
    int b=0;
    int c=0;
    int d=0;
    int e=0;
    int f=0;
    int g=0;
    int h=0;




    cout << "Calculate the progression."<<endl;
    cout << "Choose the mode."<<endl;
    cout << "Arithmetic progression=1."<<endl;
    cout << "Geometric progression=2."<<endl;
    cout << "Fibonacci sequence=3."<<endl;
    cout << "Exit=-1."<<endl;

    do
    {
        cout << "Enter mode: " ;
        cin >> a;
        if(a<-1||a>=4||a==0)
            cout << "Illegal input!";

        switch (a)
        {
        case 1 :
            cout<<"Arithmetic progression"<<endl;
            do
            {
                cout<<"a1: ";
                cin>>b;
                if(b<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(b<=0);

            do
            {
                cout<<"d: ";
                cin>>c;
                if(c<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(c<=0);

            do
            {
                cout<<"n: ";
                cin>>d;
                if(d<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(d<=0);

            for(int i=0,y=1; y<=d; y++)
            {
                i=b+y*c-c;
                if(y==d)
                    cout<<i;
                else if(y<=d)
                    cout<<i<<",";
            }
            a=4;
            break;

        case 2 :
            cout<<"Geometric progression"<<endl;
            do
            {
                cout<<"a1: ";
                cin>>e;
                if(e<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(e<=0);

            do
            {
                cout<<"r: ";
                cin>>f;
                if(f<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(f<=0);

            do
            {
                cout<<"n: ";
                cin>>g;
                if(g<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(g<=0);

            for(int i=e,y=1; y<=g; y++)
            {
                if(y==g)
                    cout<<i;
                else if(y<=g)
                    cout<<i<<",";
                i=i*f;

            }
            a=4;
            break;

        case 3 :
            cout<<"Fibonacci sequence"<<endl;
            do
            {
                cout<<"n: ";
                cin>>h;
                if(h<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(h<=0);

            for(int i=1,y=1,x=1,z=0; x<=h; x++)
            {
                if (x==1&&x==h)
                    cout<<i;
                else if (x==1)
                    cout<<i<<",";
                else if (x==2&&x==h)
                    cout<<y;
                else if (x==2)
                    cout<<y<<",";

                else if (x>=3&&x!=h)
                {
                    z=i+y;
                    i=y;
                    y=z;
                    cout<<z<<",";
                }
                else if (x==h)
                {
                    z=i+y;
                    i=y;
                    y=z;
                    cout<<z;
                }

            }
            a=4;
            break;

        default:
            break;


        }
        cout<<endl;




    }
    while(a<-1||a>=4||a==0);
    return 0;
}
