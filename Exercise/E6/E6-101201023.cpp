#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n=0;
    int i=0;
    int x=0;
    int y=0;
    int z=0;
    int j=0;
    int p=0,q=1;


    cout << "Calculate the progression." << endl;
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;


    while(i<1)                                             //當i小於1時，無限輪迴
    {
        cout << "Enter mode: ";
        cin >> n ;
        if(n==1)                                           //輸入數為1時，執行
        {
            cout << "Arithmetic progression" << endl;
            cout << "a1: ";
            cin >> x ;
            while(x<=0)                                    //判斷x是否符合範圍
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> x ;
            }
            cout << "d: ";
            cin >> y ;
            while(y<=0)
            {
                cout << "Out of range!" << endl;
                cout << "d: ";
                cin >> y ;
            }
            cout << "n: ";
            cin >> z ;
            while(z<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> z ;
            }
            cout << x ;
            for(int a=x+y; a<=x+y*z-y; a+=y)               //宣告變數a以及範圍、運算
            {
                cout << "," << a ;                         //a符合範圍時，輸出a
            }
            cout << endl;
        }


        else if(n==2)
        {
            cout << "Geometric progression" << endl;
            cout << "a1: ";
            cin >> x ;
            while(x<=0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> x ;
            }
            cout << "r: ";
            cin >> y ;
            while(y<=0)
            {
                cout << "Out of range!" << endl;
                cout << "r: ";
                cin >> y ;
            }
            cout << "n: ";
            cin >> z ;
            while(z<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> z ;
            }
            cout << x ;
            for(int a=x*y; a<=x*pow(y,z-1); a*=y)             //a需小於x乘y的z-1次方，定義新a=a*y
            {
                cout << "," << a ;
            }
            cout << endl;
        }


        else if(n==3)
        {
            cout << "Fibonacci sequence" << endl;
            cout << "n: ";
            cin >> z ;
            while(z<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> z ;
            }
            cout << q ;
            while(j<z-1)
            {
                cout << "," << p+q ;
                x=q;
                y=p+q;
                p=x;                                      //定義新p為x
                q=y;                                      //定義新q為y
                j++;
            }
            cout << endl;
        }


        else if(n==-1)
        {
            i++;                                         //結束計算
        }


        else
        {
            cout << "Illegal input!" << endl;
        }
    }


    return 0;
}
