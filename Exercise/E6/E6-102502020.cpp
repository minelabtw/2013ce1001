#include <iostream>

using namespace std;

int main()
{
    int a1=0;                                           //宣告變數
    int d=0;
    int c=0;
    int n=0;
    int r=0;
    int x=0;
    int i=0;


    cout << "Calculate the progression." << endl;       //輸出字串
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;

    while(c==0)                                         //用c判斷是否進行迴圈
    {
        cout << "Enter mode: ";
        cin >> i;
        switch(i)                                       //用i的值判斷進行某個動作
        {
        case 1:                                         //當i=1時
        {
            cout << "Arithmetic progression" << endl;
            while(c==0)
            {
                cout << "a1: ";
                cin >> a1;
                if(a1<=0)                               //如果a1<=0，則執行以下動作。
                    cout << "Out of range!" << endl;
                else c++;                               //否則c=1
            }
            while(c==1)
            {
                cout << "d: ";
                cin >> d;
                if(d<=0)
                    cout << "Out of range!" << endl;
                else c++;                               //否則c=2
            }
            while(c==2)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                    cout << "Out of range!" << endl;
                else c++;                               //否則c=3
            }
            for(int j=1; j<=n; j++)                     //等差數列
            {
                if(j<n)
                    cout << a1 << ",";
                else
                    cout << a1;
                a1=a1+d;
            }
            cout << endl;
            c=0;
            break;                                      //跳出switch
        }
        case 2:                                         //當i=2時
        {
            cout << "Geometric progression" << endl;
            while(c==0)
            {
                cout << "a1: ";
                cin >> a1;
                if(a1<=0)
                    cout << "Out of range!" << endl;
                else c++;
            }
            while(c==1)
            {
                cout << "r: ";
                cin >> r;
                if(r<=0)
                    cout << "Out of range!" << endl;
                else c++;
            }
            while(c==2)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                    cout << "Out of range!" << endl;
                else c++;
            }
            for(int j=1; j<=n; j++)                     //等比數列
            {
                if(j<n)
                    cout << a1 << ",";
                else
                    cout << a1;
                a1=a1*r;
            }
            cout << endl;
            c=0;
            break;
        }
        case 3:                                         //當i=3時
        {
            cout << "Fibonacci sequence" << endl;
            while(c==0)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                    cout << "Out of range!" << endl;
                else c++;
            }
            cout << "1,1,";
            for(int j=1,y=1,z=2; j<=n-2; j++)           //費式數列
            {
                if(j<n-2)
                    cout << z << ",";
                else
                    cout << z;
                x=y;
                y=z;
                z=x+y;
            }
            cout << endl;
            c=0;
            break;
        }
        case -1:                                        //當i=-1時
        {
            c=1;                                        //不用再執行while
            break;
        }
        default:                                        //當i等於其他值時
        {
            cout << "Illegal input!" << endl;
            c=0;
            break;
        }
        }
    }
    return 0;
}
