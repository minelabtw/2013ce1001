#include<iostream>
#include<cmath>
using namespace std;

int main()
{
    cout << "Calculate the progression.\nChoose the mode.\nArithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.";

    int number1=0;
    int a1=0,d1=0,n1=0;
    double a2=0,d2=0,n2=0;
    int a3=0,d3=0,n3=0;

    cout << "\nEnter mode: ";
    cin >> number1;

    while(number1 != 1 && number1 != 2 && number1 != 3 && number1 != -1)
    {
        cout << "Illegal input!\n";
        cout << "Enter mode: ";
        cin >> number1;
    }

    while(number1 != -1)
    {
        switch(number1)
        {
        case 1 :
            cout << "Arithmetic progression" << endl;

            while(a1 <= 0)
            {
                cout << "a1: ";
                cin >> a1;
                if(a1 <= 0)
                    cout << "Out of range!" << endl;
            }

            while(d1 <= 0)
            {
                cout << "d: ";
                cin >> d1;
                if(d1 <= 0)
                    cout << "Out of range!" << endl;
            }

            while(n1 <= 0)
            {
                cout << "n: ";
                cin >> n1;
                if(n1 <= 0)
                    cout << "Out of range!" << endl;
            }
            for(int i=1 ; i <= n1 ; ++i)
            {
                cout << a1+d1*(i-1) << ",";
            }
            cout << "\nEnter mode: ";
            cin >> number1;
            break;
        case 2 :
            cout << "Geometric progression" << endl;

            while(a2 <= 0)
            {
                cout << "a1: ";
                cin >> a2;
                if(a2 <= 0)
                    cout << "Out of range!" << endl;
            }

            while(d2 <= 0)
            {
                cout << "d: ";
                cin >> d2;
                if(d2 <= 0)
                    cout << "Out of range!" << endl;
            }

            while(n2 <= 0)
            {
                cout << "n: ";
                cin >> n2;
                if(n2 <= 0)
                    cout << "Out of range!" << endl;
            }
            for(int i=1 ; i <= n2 ; ++i)
            {
                cout << a2*pow(d2,i-1) << ",";
            }
            cout << "\nEnter mode: ";
            cin >> number1;
            break;
        case 3 :
            cout << "Fibonacci sequence" << endl;

            int f1=1,f2=1;
            int fn=0;
            int j=1;
            int n3=0;
            while(n3 <= 0)
            {
                cout << "n: ";
                cin >> n3;
                if(n3 <= 0)
                    cout << "Out of range!" << endl;
            }
            while(j <= n3)
            {
                if(j == 1)
                    fn=f1;
                if(j == 2)
                    fn=f2;
                if(j > 2)
                {
                    fn = f1 + f2;
                    f1 = f2;
                    f2 = fn;
                }
                cout << fn << ",";
                j++;
            }
            cout << "\nEnter mode: ";
            cin >> number1;
            break;
        }
    }
    return 0;
}
