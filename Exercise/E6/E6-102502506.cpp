#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    cout << "Calculate the progression." << endl;  //輸出所要輸入規定
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;

    int mode;  //設定變數
    int a1;
    int an;
    int d;
    int n;
    int r;

    int n1;
    int n2;
    int n3;
    cout << "Enter mode: ";
    cin >> mode;
    while ( mode != -1 && mode != 1 && mode != 2 && mode != 3 )  //若mode不等於-1,1,2,3就執行下列迴圈
    {
        cout << "Illegal input!" << endl;
        cout << "Enter mode: ";
        cin >> mode;
    }

    while ( mode != -1 )  //若mode不等於-1執行下列迴圈
    {
        switch ( mode )
        {
        case 1:  //判斷輸入如果等於1執行這段
            cout << "Arithmetic progression" << endl;
            cout << "a1: ";
            cin >> a1;
            while ( a1 <= 0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a1;
            }
            cout << "d: ";
            cin >> d;
            cout << "n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }
            cout << a1;  //等差數列輸出從a1開始輸出n減1次
            an = a1;
            for ( int i = 1; i < n; i++ )
            {
                an = an + d;
                cout << "," << an;
            }
            cout << endl;

            break;
        case 2:
            cout << "Geometric progression" << endl;
            cout << "a1: ";
            cin >> a1;
            while ( a1 <= 0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a1;
            }
            cout << "r: ";
            cin >> r;
            while ( r == 0 )
            {
                cout << "Out of range!" << endl;
                cout << "r: ";
                cin >> r;
            }
            cout << "n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }
            cout << a1;
            an = a1;
            for ( int i = 1; i < n; i++ )
            {
                an = an * r;
                cout << "," << an;
            }
            cout << endl;

            break;
        case 3:
            cout << "n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }
            n1 = 1;
            n2 = 1;
            cout << n1 << "," << n2;
            for ( int i = 2; i < n; i++ )
            {
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
                cout << "," << n3;
            }
            cout << endl;

            break;

        default:
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> mode;

            break;
        }
        cout << "Enter mode: ";
        cin >> mode;
        while ( mode != -1 && mode != 1 && mode != 2 && mode != 3 )
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";
            cin >> mode;
        }
    }
    return 0;
}
