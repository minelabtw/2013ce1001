#include <iostream>
#include <iomanip>
using namespace std;

int main()
{


    cout << "Calculate the progression." << endl ;
    cout << "Choose  the mode." << endl ;
    cout << "Arithmetic progression=1." << endl ;
    cout << "Geometric progression=2." << endl ;
    cout << "Fibonacci sequence=3." << endl ;
    cout << "Exit=-1." ;

    int enter = 0 , a1 = 1, a2 = 1 , d = 0 , n = 0 , x = 0 , y = 0 , an = 0 ;

    while(enter != -1 ) //做迴圈使Enter mode:能一直出現
    {
        cout << endl << "Enter mode: "  ;
        cin >> enter ;

        switch (enter)
        {
        case 1 : //等差數列
            cout << "Arithmetic progression" << endl ;

            cout << "a1: " ; //首相
            cin >> a1 ;

            while(a1 <= 0)
            {
                cout << "Out of range!" << endl ;
                cout << "a1: " ;
                cin >> a1 ;
            }

            cout << "d: " ; //公差
            cin >> d ;

            while(d <= 0)
            {
                cout << "Out of range!" << endl ;
                cout << "d: " ;
                cin >> d ;
            }

            cout << "n: " ; //項數
            cin >> n ;

            while(n <= 0)
            {
                cout << "Out of range!" << endl ;
                cout << "n: " ;
                cin >> n ;
            }

            an = a1 + d * (n-1) ; //等差數列公式

            for(x=a1; x<=an; x+=d) //輸出
            {
                cout << x ;
                if(x!=an) //防止末項還會有,的情況
                    cout << "," ;
            }

            break ;
        case 2 : //等比數列
            cout << "Geometric progression" << endl ;
            cout << "a1: " ;
            cin >> a1 ;

            while(a1 <= 0)
            {
                cout << "Out of range!" << endl ;
                cout << "a1: " ;
                cin >> a1 ;
            }

            cout << "r: " ; //公比
            cin >> d ;

            while(d <= 0)
            {
                cout << "Out of range!" << endl ;
                cout << "r: " ;
                cin >> d ;
            }

            cout << "n: " ;
            cin >> n ;

            while(n <= 0)
            {
                cout << "Out of range!" << endl ;
                cout << "n: " ;
                cin >> n ;
            }

            for(x=1; x<=n; x++) //等比數列的輸出
            {
                y=0;
                an=a1 ;
                while(y<x-1) //算次方
                {
                    an*=d ;
                    y++ ;
                }
                cout << an ;
                if(x!=n)
                    cout << "," ;
            }

            break ;
        case 3 : //費式數列
            cout << "Fibonacci sequence" << endl ;
            cout << "n: " ;
            cin >> n ;

            while(n <= 0)
            {
                cout << "Out of range!" << endl ;
                cout << "n: " ;
                cin >> n ;
            }

            for(x=1; x<=n; x++) //費式數列算法
            {
                if(x==1) //第一項為1
                    an = a1 ;
                else if(x==2) //第二項為1
                    an = a2 ;
                else if(x>2) //第三項之後的算法
                {
                    an = a1 + a2 ;
                    a1 = a2 ; //進位
                    a2 = an ; //進位
                }
                cout << an ;
                if (x!=n)
                    cout << "," ;
            }

            break ;
        case -1 : //當enter=-1 結束迴圈
            break ;

        default : //非上列數字的狀況
            cout << "Illegal input!" ;
            break ;
        }
    }

    return 0 ;
}
