#include <iostream>
using namespace std;

//distinguish if input is a natural number
int int_judge ( float input )
{
    //varible decalaration
    int x = 0;
    x = input;
    if ( input != static_cast <float> (x) or input <= 0 )
    {
        cout << "Illegal input!" << endl;
        return -1;
    }
    else
        return 0;
}
int main ()
{
    //varible decalaration
    float mode = 0;
    int data[3];
    for ( int i = 0; i <= 2; i ++ )
        data[i] = 0;
    int n1 = 1;
    int n2 = 1;

    //intro
    cout << "Calculate the progression.\n"
         << "Choose the mode.\n"
         << "Arithmetic progression=1.\n"
         << "Geometric progression=2.\n"
         << "Fibonacci sequence=3.\n"
         << "Exit=-1.\n";

    do
    {
        //ask output mode
        cout << "Enter mode: ";
        cin >> mode;

        //distinguish if input code is nonnegative int or -1
        if ( mode == -1)
            break;
        if ( int_judge(mode) == -1)
            continue;

        //output
        switch ( static_cast <int> (mode) )
        {
            //Arithmetic progression
        case 1:

            cout << "Arithmetic progression" << endl;

            //ask for data
            for (int i = 0; i <= 2; i ++ )
            {
                switch ( i )
                {
                case 0:
                    cout << "a1: ";
                    cin >> data[i];
                    if ( int_judge(data[i]) == -1 )
                    {
                        i--;
                        continue;
                    }
                    break;
                case 1:
                    cout << "d: ";
                    cin >> data[i];
                    if ( int_judge(data[i]) == -1 )
                    {
                        i--;
                        continue;
                    }
                    break;
                case 2:
                    cout << "n: ";
                    cin >> data[i];
                    if ( int_judge(data[i]) == -1 )
                    {
                        i--;
                        continue;
                    }
                    break;
                default:
                    break;
                }
            }

            //output
            for ( int i = 0; i < data[2]; i++ )
            {
                cout << data[0] + i * data[1];
                if ( i < data[2] - 1 )
                    cout << ",";
                else
                    cout << endl;
            }

            break;

            //Geometric progression
        case 2:

            cout << "Geometric progression" << endl;

            //ask for data
            for (int i = 0; i <= 2; i ++ )
            {
                switch ( i )
                {
                case 0:
                    cout << "a1: ";
                    cin >> data[i];
                    if ( int_judge(data[i]) == -1 )
                    {
                        i--;
                        continue;
                    }
                    break;
                case 1:
                    cout << "r: ";
                    cin >> data[i];
                    if ( int_judge(data[i]) == -1 )
                    {
                        i--;
                        continue;
                    }
                    break;
                case 2:
                    cout << "n: ";
                    cin >> data[i];
                    if ( int_judge(data[i]) == -1 )
                    {
                        i--;
                        continue;
                    }
                    break;
                default:
                    break;
                }
            }

            //output
            for ( int i = 0; i < data[2]; i++ )
            {
                cout << data[0];
                if ( i < data[2] - 1 )
                    cout << ",";
                else
                    cout << endl;
                data[0] *= data[1];
            }

            break;

            //Fibonacci sequence
        case 3:
            cout << "Fibonacci sequence" << endl;

            //initialize n1, n2
            n1 = 1;
            n2 = 1;
            //ask for data
            do
            {
                cout << "n: ";
                cin >> data[2];
            }
            while ( int_judge(data[2]) == -1 );

            //output
            for ( int i = 0; i < data[2]; i ++ )
            {
                if ( n1 >= n2 )
                {
                    cout << n1;
                    if ( i >= 1)
                        n2 += n1;
                }
                else
                {
                    cout << n2;
                    n1 += n2;
                }
                if ( i < data[2] - 1 )
                    cout << ",";
                else
                    cout << endl;
            }

            break;

        default:
            cout << "Illegal input!" << endl;
            break;
        }
    }
    while ( mode != -1);

    return 0;
}
