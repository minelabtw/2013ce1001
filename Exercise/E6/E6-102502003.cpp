#include<iostream>
#include<iomanip>
#include<cmath>
using namespace std;

int main()
{

    int m=0;
    int a1=0;
    int a1_G=0;
    int n=0;
    int n_G=0;
    int n_F=0;
    int d=0;
    int r=0;
    int an=0;
    int ar=0;
    int f1=1,f2=1;  //費式數列第一項和第二項
    int fn=0;
    int i=1;

    cout<<"Calculate the progression."<<endl<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl<<"Exit=-1."<<endl;

    while(m=-1) //mode=-1即停止
    {
        cout<<"Enter mode: ";
        cin>>m;
        if(m==-1)
        {
            break;
        }

        switch(m)  //選擇模式
        {
        case 1:
            cout<<"Arithmetic progression"<<endl;
            while(a1<=0)  //判定是否在範圍內
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)
                    cout<<"Out of range!"<<endl;

            }

            while(d<=0)
            {
                cout<<"d: ";
                cin>>d;
                if(d<=0)
                    cout<<"Out of range!"<<endl;
            }

            while(n<=0)
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                    cout<<"Out of range!"<<endl;

            }

            an=a1+(n-1)*d;  //等差數列
            for(int a=a1; a<an; a+=d)
            {
                cout<<a<<",";
            }

            cout<<an<<endl;

            break;


        case 2:
            cout<<"Geometric progression"<<endl;
            while(a1_G<=0)
            {
                cout<<"a1: ";
                cin>>a1_G;
                if(a1_G<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(r<=0)
            {
                cout<<"r: ";
                cin>>r;
                if(r<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(n_G<=0)
            {
                cout<<"n: ";
                cin>>n_G;
                if(n_G<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }


            ar=(a1_G)*pow(r,n_G-1);  //等比數列
            for(int a=a1; a<ar; a*=r)
            {
                cout<<a<<",";
            }
            cout<<ar<<endl;

            break;

        case 3:
            cout<<"Fibonacci sequence"<<endl;
            while(n_F<=0)
            {
                cout<<"n: ";
                cin>>n_F;
                if(n_F<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(i<=n_F)  //費式數列
            {
                if(i==1)
                    fn=f1;
                if(i==2)
                    fn=f2;
                if(i>2)
                {
                    fn=f1+f2;
                    f1=f2;
                    f2=fn;
                }
                cout<<fn<<",";
                i++;
            }
            cout<<endl;
            break;


        default:
            cout<<"Illegal input!"<<endl;
            break;

        }

    }


    return 0;

}
