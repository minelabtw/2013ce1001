#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int mode=0; // set variable

    int a1=0;
    int an=0;
    int d=0;
    int n=0;
    int r=0;

    int f1=1;
    int f2=1;
    int fn=0;

    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;

    cout<<"Enter mode: "; // input mode
    cin>>mode;
    while(mode!=-1 and mode!=1 and mode!=2 and mode!=3)
    {
        cout<<"Illegal input!"<<endl;
        cout<<"Enter mode: ";
        cin>>mode;
    }

    while(mode!=-1)
    {
        switch(mode)
        {
        case 1:
            cout<<"Arithmetic progression"<<endl;

            cout<<"a1: "; // intput a1
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }

            cout<<"d: "; // intput d
            cin>>d;

            cout<<"n: "; // intput n
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            cout<<a1; // output the Arithmetic progression
            an=a1;
            for(int i=1; i<n; i++)
            {
                an=an+d;
                cout<<","<<an;
            }
            cout<<endl;

            break;

        case 2:
            cout<<"Geometric progression"<<endl;

            cout<<"a1: "; // intput a1
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }

            cout<<"r: "; // intput r
            cin>>r;
            while(r==0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"r: ";
                cin>>r;
            }

            cout<<"n: "; // intput n
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            cout<<a1; // output the Geometric progression
            an=a1;
            for(int i=1; i<n; i++)
            {
                an=an*r;
                cout<<","<<an;
            }
            cout<<endl;

            break;

        case 3:
            cout<<"Fibonacci sequence"<<endl;

            cout<<"n: "; // intput n
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            cout<<f1<<","<<f2; // output the Fibonacci sequence
            for(int i=2; i<n; i++)
            {
                fn=f1+f2;
                f1=f2;
                f2=fn;
                cout<<","<<fn;
            }
            cout<<endl;

            break;

        default:
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>mode;
        }

        cout<<"Enter mode: "; // intput mode until mode=-1
        cin>>mode;
        while(mode!=-1 and mode!=1 and mode!=2 and mode!=3)
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>mode;
        }

    }
    return 0;
}
