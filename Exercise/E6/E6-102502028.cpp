#include <iostream>
#include <math.h>

using namespace std ;

int main ()

{
    cout << "Calculate the progression." << endl << "Choose the mode." << endl ;            //輸出題目要求
    cout << "Arithmetic progression=1." << endl << "Geometric progression=2." << endl ;
    cout << "Fibonacci sequence=3." << endl << "Exit=-1." << endl ;

    int a1 = 1 ;         //宣告一系列的整數變數和初始值
    int d = 1 ;
    int n1 = 1 ;
    int a2 = 1 ;
    int r  = 1;
    int n2 = 1;
    int a3 = 1;
    int r1 = 1 ;
    int n3 = 1 ;
    int mode = 0 ;
    int f1 = 1 ;
    int f2 = 1 ;
    int fn = 1 ;

    while ( mode != -1)    //當mode不等於-1時,繼續執行迴圈
    {
        cout << "Enter mode: " ;
        cin >> mode ;

        switch (mode)       //轉換mode
        {

        case 1 :                                              //當mode等於1的情況
            cout << "Arithmetic progression" << endl ;

            do                                                //輸入首項,公差,項數
            {
                cout << "a1:" ;
                cin >> a1 ;
            }
            while (a1 <= 0 && cout << "Out of range!") ;

            do
            {
                cout << "d:" ;
                cin >> d ;
            }
            while (d <= 0 && cout << "Out of range!") ;

            do
            {
                cout << "n:" ;
                cin >> n1 ;
            }
            while (n1 <= 0 && cout << "Out of range!") ;

            for (int n = 0 ; n <= (n1 - 2) ; n++)               //輸出等差級數
            {
                cout << a1 + n*d << "," ;
            }
            cout << a1 + (n1-1)*d << endl ;
            break ;                                             //結束mode等於1的情況

        case 2 :                                                //當mode等於2的情況
            cout << "Geometric progression" << endl ;

            do                                                  //輸入首項,公比,項數
            {
                cout << "a1:" ;
                cin >> a2 ;
            }
            while (a2 <= 0 && cout << "Out of range!") ;

            do
            {
                cout << "r:" ;
                cin >> r ;
            }
            while (r <= 0 && cout << "Out of range!") ;

            do
            {
                cout << "n:" ;
                cin >> n2 ;
            }
            while (n2 <= 0 && cout << "Out of range!") ;

            for(int nn = 1 ; nn <= (n2-1) ; nn++)                     //輸出等比級數
            {
                cout << a2*(pow(r,nn-1)) << "," ;
            }
            cout << a2*(pow(r,n2-1)) << endl ;
            break ;                                                   //結束mode等於2的情況

        case 3 :                                                  //mode等於3的情況
            cout << "Fibonacci sequence" << endl ;

            do                                                        //輸入項數
            {
                cout << "n:" ;
                cin >> n3 ;
            }
            while (n3 <= 0 && cout << "Out of range!") ;

            for (int nnn = 1 ; nnn <= n3 ; nnn++)           //輸出費式數列
            {
                if (nnn == 1)
                    cout << "1" << "," ;
                    else if (nnn == 2)
                        cout << "1"  ;
                else
                {
                    fn = f1 + f2 ;
                    cout << "," << fn ;
                    f1 = f2 ;
                    f2 = fn ;
                }
            }
            cout << endl ;
            break ;


        default :　　　　　　　　　　　　　　　　　　　　　　//其他情況
            cout << "Illegal input!" ;
            break ;

        }

    }

    return 0 ;
}
