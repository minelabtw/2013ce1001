#include <iostream>

using namespace std;

int main()
{

    cout << "Calculate the progression." << endl;
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;
    int mode ;

    do
    {
        int a1 ;
        int d ;
        int n ;
        int r ;
        int i = 1 ;
        int fig1 = 1 ;
        int fig2 = 1 ;
        int fign ;

        cout << "Enter mode: " ;
        cin  >> mode ;

        while (mode!=1 && mode!=2 && mode!=3 && mode!=-1)
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: " ;
            cin  >> mode ;
        }

        switch (mode)
        {
        case 1 :
            cout << "Arithmetic progression" << endl;
            cout << "a1: " ;
            cin  >> a1 ;
            while (a1<=0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: " ;
                cin  >> a1;
            }

            cout << "d: " ;
            cin  >> d ;
            while (d<=0)
            {
                cout << "Out of range!" << endl;
                cout << "d: " ;
                cin  >> d ;
            }

            cout << "n: " ;
            cin  >> n ;
            while (n<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: " ;
                cin  >> n ;
            }

            while (i<=n)
            {
                cout << a1+(i-1)*d ;
                if (i<=n-1)
                {
                    cout << "," ;
                }
                i++;
            }
            cout << endl;
            break;

        case 2 :
            cout << "Geometric progression" << endl;
            cout << "a1: " ;
            cin  >> a1 ;
            while (a1<=0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: " ;
                cin  >> a1;
            }

            cout << "r: " ;
            cin  >> r ;
            while (d<=0)
            {
                cout << "Out of range!" << endl;
                cout << "d: " ;
                cin  >> d ;
            }

            cout << "n: " ;
            cin  >> n ;
            while (n<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: " ;
                cin  >> n ;
            }


            while (i<=n)
            {
                cout << a1 ;
                a1 = a1 * r ;
                if (i<=n-1)
                {
                    cout << "," ;
                }
                i++ ;
            }
            cout << endl;
            break ;

        case 3 :
            cout << "Fibonacci sequence" << endl;
            cout << "n: ";
            cin  >> n;

            while (i<=n)
            {
                if (i==1)
                {
                    fign=fig1;
                }
                if (i==2)
                {
                    fign=fig2;
                }
                if (i>2)
                {
                    fign=fig1+fig2;
                    fig1=fig2;
                    fig2=fign;
                }
                cout << fign ;

                if (i<=n-1)
                {
                    cout << "," ;
                }
                i++;

            }
            cout << endl;
            break;







        }
    }
    while (mode != -1);
    return 0;
}
