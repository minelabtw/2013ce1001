#include <iostream>
#include <math.h>
using namespace std;
int main()
{
    int way = 0;//宣告方式的變數
    int a1 = 0;//宣告初始數的變數
    int d = 0;//宣告公差的變數
    int n = 0;//宣告項數的變數
    int r = 0;//宣告公比的變數
    int f1 =1;//宣告費式數列1號數的變數
    int f2 =1;//宣告費式數列2號數的變數
    int fn =1;//宣告費式數列每項數的變數

    cout << "Calculate the progression." << endl << "Choose the mode." << endl;//提示相應按鍵的內容
    cout << "Arithmetic progression=1." << endl << "Geometric progression=2." << endl << "Fibonacci sequence=3." << endl << "Exit=-1." << endl;//同上

    do
    {cout << "Enter mode:";
     cin >> way;//輸入方式變數
        switch( way )//將方式的變數帶入switch
        {
        case 1://輸入1進入等差數列的計算
            cout << "Arithmetic progression" << endl;
            do{
               cout << "a1:";
               cin >> a1;
               if( a1 <= 0 )
               cout << "Out of range!" << endl;
            }while( a1 <= 0);//輸入初始數並檢查
            do{
               cout << "d:";
               cin >> d;
               if( d <= 0 )
               cout << "Out of range!" << endl;
            }while( d <= 0);//輸入公差並檢查
            do{
               cout << "n:";
               cin >> n;
               if( n <= 0 )
               cout << "Out of range!" << endl;
            }while( n <= 0);//輸入項數並檢查
            cout << a1;//先印出初始數
            for( int x = 1; x < n; x++)
            {
                cout << "," << a1 +( x * d );
            }cout << endl;//印出等差數列
        break;//直接結束switch

        case 2:
            cout << "Geometric progression" << endl;//輸入2進入等比數列計算
            do{
               cout << "a1:";
               cin >> a1;
               if( a1 <= 0 )
               cout << "Out of range!" << endl;
            }while( a1 <= 0);//輸入初始數並檢查
            do{
               cout << "r:";
               cin >> r;
               if( r <= 0 )
               cout << "Out of range!" << endl;
            }while( r <= 0);//輸入公比並檢查
            do{
               cout << "n:";
               cin >> n;
               if( n <= 0 )
               cout << "Out of range!" << endl;
            }while( n <= 0);//輸入項數並檢查
            cout << a1;//先印出初始數
            for ( int y = 1; y < n; y++)
            {
                cout << "," << pow(r,y) * a1;
            }cout << endl;//印出等比數列
        break;//直接結束switch

        case 3:
            cout << "Fibonacci sequence" << endl;//輸入3進入費式數列
            do{
               cout << "n:";
               cin >> n;
               if( n <= 0 )
               cout << "Out of range!" << endl;
            }while( n <= 0);//輸入項數並檢查
            cout << "1";//先印出首項
            for(int z = 2; z <= n; z++)//設一個由跑動的項數z組成的迴圈
            {
                if (z == 2)
                    fn = f2;//第二項=第一項=1(放f2因為其初始值恰為1)
                if (z > 2)//項數>2
                    fn = f1 + f2;//每項數為前兩項和
                    f1 = f2;//隨項數增加,1號數成為後面的數(2號數)
                    f2 = fn;//隨項數增加,2號數成為後面的數(每項數)
                cout << "," << fn;//輸出費式數列
            }cout << endl;
        break;//直接結束switch

        case -1://-1為離開不限式任何值
        break;//直接結束switch

        default:
            cout << "Illegal input!" << endl;//輸入其他值,提示錯誤
            break;//直接結束switch
        }
    }while( way != -1 );//檢查方式變數,若為-1則跳出大迴圈
return 0;//回歸0
}
