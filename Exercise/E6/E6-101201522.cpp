#include <iostream>
using namespace std;

int main(){
    int mode,a1,a2,dr,n,i;//宣告整數,分別為輸入模式,首項,費氏數列用的第二項,公差或公比,項數,迴圈用變數 
    cout << "Calculate the progression.\n"//輸出說明 
         << "Choose the mode.\n"
         << "Arithmetic progression=1.\n"
         << "Geometric progression=2.\n"
         << "Fibonacci sequence=3.\n"
         << "Exit=-1.\n";
    while(1){
        cout << "Enter mode: ";
        cin >> mode;
        if(mode == -1)//如果輸入-1即跳出程式 
            break;
        else if(mode == 1 || mode == 2){//處理mode1,2的數列 
            cout << (mode==1 ? "Arithmetic progression\n" : "Geometric progression\n");
            while(1){//輸入首項,錯誤則重複要求重複輸入 
                cout << "a1: ";
                cin >> a1;
                if(a1>0)
                    break;
                else
                    cout << "Out of range!\n";
            }
            while(1){//輸入公差或公比,錯誤則重複要求重複輸入
                cout << (mode==1 ? "d: " : "r: ");
                cin >> dr;
                if(dr>0)
                    break;
                else
                    cout << "Out of range!\n";
            }
            while(1){//輸入項數,錯誤則重複要求重複輸入
                cout << "n: ";
                cin >> n;
                if(n>0)
                    break;
                else
                    cout << "Out of range!\n";
            }
            for(i=0;i<n;i++){//輸出等差或等比數列 
                i==0 ? cout << a1
                     : cout << ',' << a1;
                a1 = (mode==1 ? a1+dr : a1*dr);
            }
            cout <<endl;
        }
        else if(mode == 3){//處理mode3的費氏數列 
            a1 = 1;//首兩項為1 
            a2 = 1;
            cout << "Fibonacci sequence\n";
            while(1){//輸入項數,錯誤則重複要求重複輸入
                cout << "n: ";
                cin >> n;
                if(n>0)
                    break;
                else
                    cout << "Out of range!\n";
            }
            for(i=0;i<n;i++){//輸出費氏數列,利用a1,a2分別戶加 
                if(i%2){//奇數項輸出a1,偶數項輸出a2
                    cout << ',' << a2;
                    a2 += a1;
                }
                else{
                    i==0 ? cout << a1
                         : cout << ','<< a1;
                    a1 += a2;
                }
            }
            cout <<endl;
        }
        else//輸入錯誤,要求重新輸入模式 
            cout << "Illegal input!\n";
    }
    return 0;    
}
