#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int mode=0; //call variable for mode
    int a1; //call variable for first term
    int n; // call variable for last term
    int d; //common difference
    int nc=1; //call variable for n couter
    int tn; //output for each term
    int r; //common ratio

    cout<<"Calculate the progression."<<endl;
    cout<<"Choose  the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;

    do
    {
        //initialise variables into 0
        nc=1;
        a1=0;
        r=0;
        d=0;
        n=0;

        //display words and input
        cout<<"Enter mode: ";
        cin>>mode;

        //start the looping for choosing mode
        switch(mode)
        {
        case 1: //case for choosing 1
            cout<<"Arithmetic progression."<<endl;
            while(a1<=0)
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)//data validation for first term
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(d<=0)//data validation for common difference
            {
                cout<<"d: ";
                cin>>d;
                if(d<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(n<=0)//data validation for no. of terms
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            //calculation for the sequence
            while(nc<=n)
            {
                tn=a1+(nc-1)*d;
                if(nc<n)
                {
                    cout<<tn<<",";
                }
                else if(nc==n)
                {
                    cout<<tn<<endl;
                }
                nc++;
            }
            break;
            //for choosing mode 2
        case 2:
            cout<<"Geometric progression"<<endl;
            while(a1<=0) //data validation for first term
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(r<=0) //data validatio for common ratio
            {
                cout<<"r: ";
                cin>>r;
                if(r<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(n<=0) //data validation for no. of terms
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            //calculation for the sequence
            while(nc<=n)
            {
                tn=a1*pow(r,nc-1);
                if(nc<n)
                {
                    cout<<tn<<",";
                }
                else if(nc=n)
                {
                    cout<<tn<<endl;
                }
                nc++;
            }
            break;

        case 3:
            cout<<"Fibonacci sequence."<<endl;
            while(n<=0) //data validation for no. of terms
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            //calculation for the sequence
            a1=0;
            tn=1;
            while(nc<=n)
            {
                if(nc<n)
                {
                    cout<<tn<<",";
                }
                else if(nc=n)
                {
                    cout<<tn<<endl;
                }
                tn=a1+tn;
                a1=tn-a1;
                nc++;
            }
            break;

        case -1: //leave the program for input -1
            return 0;
            break;

        default: //for illegal input
            cout<<"Illegal input!"<<endl;
            break;
        }
    }
    while(mode!=-1); // continues on the program
}
