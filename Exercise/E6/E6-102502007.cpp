#include <iostream>
#include <cmath>
using namespace std;
int A1()
{
    int a1=0;
    do
    {
        cout<<"a1: ";
        cin>>a1;
        if(a1<=0)
            cout<<"Out of range!"<<endl;
    }
    while(a1<=0);//loop until the correct number is input
    return a1;
}//set up a function to replace all the a1 input
int D()
{
    int d=0;
    do
    {
        cout<<"d: ";
        cin>>d;
        if(d<=0)
            cout<<"Out of range!"<<endl;
    }//loop until the correct number is input
    while(d<=0);
    return d;
}//set up a function to replace all the d intput
int N()
{
    int n=0;
    do
    {
        cout<<"n: ";
        cin>>n;
        if(n<=0)
            cout<<"Out of range!"<<endl;
    }
    while(n<=0);//loop until the correct number is input
    return n;
}//set up a function to replace all the n input
int main()
{
    int a1=0,d=0,n=0;
    int mode=0;
    double r=0;//use double coz there are some system problems may cause output error.
    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;
    do
    {
        cout<<"Enter mode: ";
        cin>>mode;
        if(mode==-1)
            break;//when user input -1 , that means the program is ended.
        switch(mode)
        {
        case 1://do the following instruction if user input 1
        {
            cout<<"Arithmetic progression"<<endl;
            a1=A1();//call out the function to receive the data
            d=D();//above
            n=N();//above
            for(int i=a1; i<=a1+d*(n-1); i=i+d)
                if(i==a1+d*(n-1))
                    cout<<i;
                else//print the progression and don't print the comma behind the last number.
                    cout<<i<<",";
        }//
        cout<<endl;
        continue;//the progression is done and go back to choose a new mode.

        case 2://do the following instruction if user input 2
        {
            cout<<"Geometric progression"<<endl;
            a1=A1();//call out the function to receive the data
            do
            {
                cout<<"r: ";
                cin>>r;
                if(r<=0)
                    cout<<"Out of range!"<<endl;
            }
            while(r<=0);//loop until the correct number is input
            n=N();//call out the function to receive the data
            if(r!=1)
            {
                for(int i=a1; i<=a1*pow(r,n-1); i=i*r)
                {
                    if(i==a1*pow(r,n-1))
                        cout<<i;
                    else//print the progression and don't print the comma behind the last number.
                        cout<<i<<",";

                }
            }
            else
                for(int i=1; i<=n; i=i+1)
                {
                    if(i==n)
                        cout<<a1;
                    else
                        cout<<a1<<",";//for the r=1 case
                }
            cout<<endl;
            continue;//the progression is done and go back to choose a new mode.
        }

        case 3://do the following instruction if user input 3
        {
            cout<<"Fibonacci sequence"<<endl;
            n=N();//call out the function to receive the data
            int ary[n];
            ary[0]=1;//default the first and the second number of the array as 1 and 1.
            ary[1]=1;
            for(int i=0; i<=n-3; i=i+1)
                ary[i+2]=ary[i+1]+ary[i];//the third number is the sum of the formal 2 number in the array
            for(int i=0; i<=n-1; i=i+1)
            {
                if(n==1)
                    cout<<"1";
                else if(n==2)
                {
                    cout<<"1,1";
                    break;
                }
                else if(ary[i]==ary[n-1])
                    cout<<ary[i];//print the array , e.g. , the Fibonacci sequence
                else//print the progression and don't print the comma behind the last number.
                    cout<<ary[i]<<",";
            }
        }
        cout<<endl;
        continue;//the progression is done and go back to choose a new mode.

        default:
        {
            cout<<"Illegal input!"<<endl;
        }
        continue;//the input is illegal , go back and input again
        }
    }
    while(1);
    return 0;
}
