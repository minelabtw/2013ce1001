#include<iostream>
#include <iomanip>
using namespace std;

int main()
{
    int x;
    int a1,r,d,n;
    int n1=0,n2=0,n3=0;
    int a,b,c;


    cout <<"Calculate the progression."<<endl;
    cout <<"Choose the mode."<<endl;
    cout <<"Arithmetic progression=1."<<endl;
    cout <<"Geometric progression=2."<<endl;
    cout <<"Fibonacci sequence=3."<<endl;
    cout <<"Exit=-1."<<endl;

    while(x!=-1)
    {
        cout <<"Enter mode: ";
        cin >>x;

        switch (x)
        {
        case 1://設定輸入1後的輸出
            cout <<"Arithmetic progression"<<endl;
            cout <<"a1: ";
            cin >>a1;
            while (a1<=0)//判定a1是否為正確範圍
            {
                cout <<"Out of range!"<<endl;
                cout <<"a1: ";
                cin >>a1;
            }
            cout <<"d: ";
            cin >>d;
            while (d<=0)//判定d是否為正確範圍
            {
                cout <<"Out of range!"<<endl;
                cout <<"d: ";
                cin >>d;
            }
            cout <<"n: ";
            cin >>n;
            while (n<=0)//判定n是否為正確範圍
            {
                cout <<"Out of range!"<<endl;
                cout <<"n: ";
                cin >>n;
            }
            while (n1<=n)//輸出數列
            {
                if (n1<n-1)
                    cout <<a1+(n1*d)<<",";
                if (n1==n-1)
                    cout <<a1+n1*d;
                n1++;
            }
            n1=0;
            cout <<endl;
            break;
        case 2:
            cout <<"Geometric progression"<<endl;
            cout <<"a1: ";
            cin >>a1;
            while (a1<=0)//判定a1是否為正確範圍
            {
                cout <<"Out of range!"<<endl;
                cout <<"a1: ";
                cin >>a1;
            }
            cout <<"r: ";
            cin >>r;
            while (r<=0)//判定r是否為正確範圍
            {
                cout <<"Out of range!"<<endl;
                cout <<"r: ";
                cin >>r;
            }
            cout <<"n: ";
            cin >>n;
            while (n<=0)//判定n是否為正確範圍
            {
                cout <<"Out of range!"<<endl;
                cout <<"n: ";
                cin >>n;
            }
            while (n2<=n-1)//輸出數列
            {
                if (n2==0)
                    cout <<a1<<",";
                if (n2<n-1 &&n2!=0)
                    a1=a1*r;
                cout <<a1*r;

                if (n2==n-2)
                    break;
                else
                    cout <<",";

                n2++;
            }
            n2=0;
            cout <<endl;
            break;
        case 3:
            cout <<"Fibonacci sequence"<<endl;
            cout <<"n: ";
            cin >>n;
            while (n<=0)//判定n是否為正確範圍
            {
                cout <<"Out of range!"<<endl;
                cout <<"n: ";
                cin >>n;
            }
            a=1,b=1;
            cout <<a;
            while (n3<n-1)//輸出數列
            {
                c=b+a;
                a=b;
                b=c;
                cout <<","<<a;
                n3++;
            }
            n3=0;
            cout <<endl;
            break;
        case -1:
            break;
        default://判定錯誤輸入
            cout <<"Illegal input!"<<endl;
            break;
        }
    }
    return 0;
}
