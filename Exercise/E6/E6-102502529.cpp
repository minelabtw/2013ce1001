#include <iostream>

using namespace std ;

int main ()
{
    cout<<"Calculate the progression."<<endl;
    cout<<"Choose  the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl ;
    cout<<"Geometric progression=2."<<endl ;
    cout<<"Fibonacci sequence=3."<<endl ;
    cout<<"Exit=-1."<<endl;

    int input=0,a1=0,d=0,n=0,r=0,geout=1 ;
    cout<<"Enter mode: " ;
    while(cin>>input)                               //輸入input值時進入
    {
        if(input==-1)                               //=-1時跳出while迴圈
            break ;
        switch(input)
        {
        case 1:
            cout<<"Arithmetic progression\n";
            cout<<"a1: " ;
            cin>>a1;
            while(a1==0||a1<0)                      //a1=0 or <0 輸出錯誤訊息 且重新輸入
            {
                cout<<"Out of range!\n" ;
                cout<<"a1: " ;
                cin>>a1 ;
            }
            cout<<"d: ";
            cin>>d ;
            while(d==0||d<0)                        //d亦同
            {
                cout<<"Out of range!\n" ;
                cout<<"d: " ;
                cin>>d ;
            }
            cout<<"n: " ;
            cin>>n ;
            while(n==0||n<0)                       //n亦同
            {
                cout<<"Out of range!\n" ;
                cout<<"n: " ;
                cin>>n ;
            }
            cout<<a1 ;                            //先輸出首項
            for(int ari=1;ari!=n;ari++)           //for迴圈內輸出從第二項輸出至第n項
            {
                cout<<","<<a1+d*ari ;
            }
            cout<<endl ;
            cout<<"Enter mode: " ;
            break ;                              //跳出switch迴圈

        case 2:
            cout<<"Geometric progression\n";
            cout<<"a1: " ;
            cin>>a1;
            while(a1==0||a1<0)                  //若輸入a1值不符 輸出錯誤訊息
            {
                cout<<"Out of range!\n" ;
                cout<<"a1: " ;
                cin>>a1 ;
            }
            cout<<"r: ";
            cin>>r ;
            while(r==0||r<0)                    //r亦同
            {
                cout<<"Out of range!\n" ;
                cout<<"r: " ;
                cin>>r ;
            }
            cout<<"n: " ;
            cin>>n ;
            while(n==0||n<0)                //n亦同
            {
                cout<<"Out of range!\n" ;
                cout<<"n: " ;
                cin>>n ;
            }
            cout<<a1 ;
            geout=a1 ;                      //先輸出第1項
            for(int geo=1;geo!=n;geo++)     //從第二項開始輸出 an=an-1 * r
            {
                geout*=r ;
                cout<<","<<geout ;
            }
            cout<<endl ;
            cout<<"Enter mode: " ;
            break ;
        case 3 :
            {


            cout<<"Fibonacci sequence\n";
            cout<<"n: ";
            cin>>n ;
            while(n==0||n<0)                   //輸入的n值不符 輸出錯誤訊息
            {
                cout<<"Illegal input!\n" ;
                cout<<"n: " ;
                cin>>n ;
            }
            int f1=1,f2=1,i=2 ;
            int fn=0 ;
            cout<<f1 ;                        //先輸出第1項
            while(i<=n)
            {
                if(i>=3)                     //三項後
                {
                    fn=f1+f2 ;               //前兩項相加
                    f1=f2 ;                  //進位
                    f2=fn ;                  //進位
                }
                if(i==2)
                {
                    fn=f2 ;                 //用來輸出第2項
                }
                cout<<","<<fn ;
                i++ ;

            }
            cout<<endl ;
            cout<<"Enter mode: " ;
            }
            break ;                       //跳出switch
        default:
            cout<<"Illegal input!\n" ;
            cout<<"Enter mode: " ;

        }
    }
    return 0 ;
}
