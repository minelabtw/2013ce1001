#include<iostream>

using namespace std ;

int main ()
{
    int number=0, a=0, d=0, n=0 ,r=0 ,b=1 ,c=1 ,e=b+c ;//宣告變數

    cout << "Calculate the progression.\n" << "Choose  the mode.\n" << "Arithmetic progression=1.\n" ;

    cout << "Geometric progression=2.\n" << "Fibonacci sequence=3.\n" << "Exit=-1.\n" ;

    while (true)//迴圈
    {
        cout << "Enter mode: " ;

        cin >> number ;

        switch (number)//使用switch，判斷路徑
        {
        case 1 :
            cout << "Arithmetic progression\n" ;

            while (true)
            {
                cout << "a1: " ;

                cin >> a ;

                if (a<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            while (true)
            {
                cout << "d: " ;

                cin >> d ;

                if (d<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            while (true)
            {
                cout << "n: " ;

                cin >> n ;

                if (n<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }



            for (int x=1; x<=n; x++)//使用迴圈
            {
                cout << a ;

                a=a+d ;

                if(x<n)
                {
                    cout << "," ;
                }
            }

            cout << endl ;

            break ;

        case 2 :
            cout << "Geometric progression\n" ;

            while (true)
            {
                cout << "a1: " ;

                cin >> a ;

                if (a<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            while (true)
            {
                cout << "r: " ;

                cin >> r ;

                if (r<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            while (true)
            {
                cout << "n: " ;

                cin >> n ;

                if (n<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            for (int x=1; x<=n; x++)
            {
                cout << a ;

                a=a*r ;

                if (x<n)
                {
                    cout << "," ;
                }
            }

            cout << endl ;

            break ;

        case 3 :
            cout << "Fibonacci sequence\n" ;

            while (true)
            {
                cout << "n: " ;

                cin >> n ;

                if (n<=0)
                {
                    cout << "Out of range!\n" ;
                }
                else break ;
            }

            cout << c << "," << b << "," ;

            for (int x=1; x<n-1; x++)
            {
                cout << e ;

                b=c ;

                c=e ;

                e=b+c ;

                if (x<n-2)
                {
                    cout << "," ;
                }
            }

            cout << endl ;//換行

            break ;

        case -1 :
            break ;

        default :
            cout << "Illegal input!\n" ;
        }

        if (number==-1)
        {
            break ;//跳出迴圈
        }
    }
    return 0 ;
}
