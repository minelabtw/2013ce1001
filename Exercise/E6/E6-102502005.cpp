#include<iostream>
using namespace std;

int main ()
{
    int status=1,mode=0,i,m1a1=0,m1d=0,m1n=0,m2a1=0,m2r=0,m2n=0,m3n=0,a=1,b=1,c=1; //宣告變數。

    cout << "Calculate the progression." <<endl;     //輸出字串。
    cout << "Choose the mode." <<endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl ;
    cout << "Exit=-1." << endl;

    while (status==1)                                //當status為1時執行以下程式來判斷要輸出哪種數列，或是要改變status結束程式，預設status值為1讓第一次執行時可以進入。
    {
        cout << "Enter mode: ";                      //要求使用者輸入mode值來決定要進入哪個case。
        cin >> mode;

        while (mode!=-1 && mode!=1 && mode!=2 && mode!=3) //當mode值不符條件時要求使用者重新輸入。
        {
            cout << "Illegal input!" << endl ;
            cout << "Enter mode: " ;
            cin >> mode;

        }

        switch (mode)                                 //利用switch case來判斷要輸出哪種數列，或是直接更改status值結束程式。
        {

        case 1 :                                      //case1為等差數列。
            cout << "Arithmetic progression" << endl; //要求使用者輸入相關參數，若輸入錯誤則要求重新輸入。

            cout << "a1: " ;
            cin >> m1a1;
            while (m1a1<=0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> m1a1;
            }

            cout << "d: " ;
            cin >> m1d;
            while (m1d<=0)
            {
                cout << "Out of range!" << endl;
                cout << "d: ";
                cin >> m1d;
            }

            cout << "n: ";
            cin >> m1n;
            while (m1n<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "n: ";
                cin >> m1n;
            }

            for (i=1; i<=m1n-1; i=i+1)               //利用for迴圈輸出數列的第一項至倒數第二項。
            {
                cout << m1a1 <<",";
                m1a1=m1a1+m1d;
            }
            cout << m1a1 <<endl;                     //最後一項分開輸出,避免多出逗號。
            break;

        case 2 :
            cout << "Geometric progression" << endl; //要求使用者輸入相關參數，若輸入錯誤則要求重新輸入。

            cout << "a1: " ;
            cin >> m2a1;
            while (m2a1<=0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> m2a1;
            }

            cout << "r: " ;
            cin >> m2r;
            while (m2r<=0)
            {
                cout << "Out of range!" << endl;
                cout << "r: ";
                cin >> m2r;
            }

            cout << "n: ";
            cin >> m2n;
            while (m2n<=0)
            {
                cout << "Out of range!" <<endl;
                cout << "n: ";
                cin >> m2n;
            }

            for (i=1; i<=m2n-1; i=i+1)                //利用for迴圈輸出數列的第一項至倒數第二項。
            {
                cout << m2a1 <<",";
                m2a1=m2a1*m2r;
            }
            cout << m2a1 << endl;                     //最後一項分開輸出,避免多出逗號。
            break;

        case 3 :
            a=1;                                      //重新初始化變數值,避免上次輸出的費式數列影響此次輸出。
            b=1;
            c=1;
            cout << "Fibonacci sequence" <<endl;      //要求使用者輸入相關參數，若輸入錯誤則要求重新輸入。

            cout << "n: " ;
            cin >> m3n;
            while (m3n<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> m3n;
            }

            if (m3n==1)                               //當只要輸出一項時直接印出結果。
            {
                cout << "1" << endl;
            }
            else if (m3n==2)                          //當只要輸出兩項時直接印出結果。
            {
                cout << "1,1" << endl;
            }
            else
            {
                cout << "1,1,";
                for (i=3; i<m3n; i=i+1)               //利用for迴圈輸出數列的第一項至倒數第二項。
                {
                    c=a+b;
                    a=b;
                    b=c;
                    cout << c << "," ;
                }
                c=a+b;                                //最後一項分開輸出,避免多出逗號。
                a=b;
                b=c;
                cout << c << endl;
            }
            break;

        case -1 :                                     //若使用者要求退出程式，則修改status值跳出while迴圈並結束程式。
            status=-1;
            break;

        default:
            break;
        }
    }
    return 0;
}
