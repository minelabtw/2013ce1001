#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int x=0,a1=0,d=0,n=0,r=0,i=0;
    int num1=1;
    int num2=0;
    int num3=0;
    cout <<"Calculate the progression.\nChoose the mode.\n";//輸出變數
    cout <<"Arithmetic progression=1.\n";
    cout <<"Geometric progression=2.\n";
    cout <<"Fibonacci sequence=3.\n";
    cout <<"Exit=-1.\n";

    while(x!=-1)//當x不等於-1 則繼續跑 若等於-1 則結束
    {
        cout <<"Enter mode: ";
        cin >>x;
        while(x!=1 && x!=2 && x!=3 && x!=-1)//當X不屬於值 則重新輸入
        {
            cout <<"Illegal input!\nEnter mode: ";
            cin >>x;
        }


        switch(x)//CASE1.2.3.-1
        {
        case 1://等差數列
            cout <<"Arithmetic progression\n";

            cout <<"a1: ";
            cin >>a1;
            while(a1<=0)
            {
                cout <<"Out of range!\na1: ";
                cin >>a1;
            }

            cout <<"d: ";
            cin >>d;
            while(d<=0)
            {
                cout <<"Out of range!\nd: ";
                cin >>d;
            }

            cout <<"n: ";
            cin >>n;
            while(n<=0)
            {
                cout <<"Out of range!\nn: ";
                cin >>n;
            }

            cout <<a1;
            for(int i=1 ; i<n ; i++)
            {
                cout <<","<<a1+i*d;
            }
            cout <<endl;
            break;

        case 2://等比數列
            cout <<"Geometric progression\n";

            cout <<"a1: ";
            cin >>a1;
            while(a1<=0)
            {
                cout <<"Out of range!\na1: ";
                cin >>a1;
            }

            cout <<"r: ";
            cin >>r;
            while(r<=0)
            {
                cout <<"Out of range!\nr: ";
                cin >>r;
            }

            cout <<"n: ";
            cin >>n;
            while(n<=0)
            {
                cout <<"Out of range!\nn: ";
                cin >>n;
            }

            cout <<a1;
            for(int i=1 ; i<n ; i++)
            {
                cout <<","<<a1*pow(r,i);
            }
            cout <<endl;
            break;

        case 3://費氏
            cout <<"Fibonacci sequence";

            cout <<"n: ";
            cin >>n;
            while(n<=0)
            {
                cout <<"Out of range!\nn: ";
                cin >>n;
            }
            for(int i=1 ; i<=n ; i++)
            {
                if (i == n)
                    cout <<num1;
                else
                    cout <<num1 <<",";
                num1 = num1 + num2;
                num2 = num1 - num2;

            }
            cout <<endl;
            break;

        default :
            break ;
        }

    }


    return 0;
}
