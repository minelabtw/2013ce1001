#include<iostream>
using namespace std;

int main()
{
    int mode=0;//宣告變數並設定初始值
    int a1=0,d=0,r=0,n=0;
    int i;
    int b1=1,b2=1,b=0;

    cout << "Calculate the progression." << endl << "Choose the mode." << endl;//輸出字串
    cout << "Arithmetic progression=1." << endl << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl << "Exit=-1." << endl;
    cout << "Enter mode: ";
    cin  >> mode;//輸入並將值給變數

    while(mode!=1 and mode!=2 and mode!=3 and mode!=-1)//當mode不等於1,2,3,-1時進入迴圈
    {
        cout << "Out of range!" << cout << "Enter mode: ";
        cin  >> mode;
    }

    while(mode!=-1)//mode不等於-1時進入迴圈
    {
        switch(mode)//指定mode的方案
        {
            case 1://mode=1為等差
                cout << "Arithmetic progression" << endl << "a1: ";
                cin  >> a1;

                while(a1<=0)
                {
                    cout << "Out of range!" << endl << "a1: ";
                    cin  >> a1;
                }

                cout << "d: ";
                cin  >> d;

                while(d<=0)
                {
                    cout << "Out of range!" << endl << "d: ";
                    cin  >> d;
                }

                cout << "n: ";
                cin  >> n;

                while(n<=0)
                {
                    cout << "Out of range!" << endl << "n: ";
                    cin  >> n;
                }

                cout << a1;

                for(i=1;i<n;i++)
                {
                    cout << "," << a1+i*d;
                }

                cout << endl;
                break;

            case 2://mode=2為等比
                cout << "Geometric progression" << endl << "a1: ";
                cin  >> a1;

                while(a1<=0)
                {
                    cout << "Out of range!" << endl << "a1: ";
                    cin  >> a1;
                }

                cout << "r: ";
                cin  >> r;

                while(r<=0)
                {
                    cout << "Out of range!" << endl << "r: ";
                    cin  >> r;
                }

                cout << "n: ";
                cin  >> n;

                while(n<=0)
                {
                    cout << "Out of range!" << endl << "n: ";
                    cin  >> n;
                }

                cout << a1;

                for(i=1;i<n;i++)
                {
                    cout << "," << a1*r;
                    a1=a1*r;
                }

                cout << endl;
                break;

            case 3://mode=3為費氏
                cout << "Fibonacci sequence" << endl << "n: ";
                cin  >> n;

                while(n<=0)
                {
                    cout<<"Out of range!"<<endl;
                    cout<<"n: ";
                    cin>>n;
                }
                cout << "1,1";

                for(i=1;i<n;i++)
                {
                    b=b1+b2;
                    b1=b2;
                    b2=b;
                    cout << "," << b;
                }

                cout << endl;
                break;

            default://不符合條件
                cout << "Illegal input!" << endl << "Enter mode: ";
                cin  >> mode;
                break;
        }

        cout << "Enter mode: ";//再次進行迴圈
        cin  >> mode;

        while(mode!=1 and mode!=2 and mode!=3 and mode!=-1)
        {
            cout << "Illegal input!" << endl << "Enter mode: ";
            cin  >> mode;
        }

    }

return 0;
}
