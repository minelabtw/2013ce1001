#include<iostream>
using namespace std;
int main()
{
    int mode;
    cout<<"Calculate the progression."<<endl;
    cout<<"Choose  the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;

    while( true )
    {
        do
        {
            cout<<"Enter mode: ";
            cin>>mode;
        }
        while( !(mode==1||mode==2||mode==3||mode==-1)&&cout<<"Illegal input!"<<endl );

        int n,tmp; // general
        int a1,d; // for case 1 and 2
        int a,b; // for case 3
        switch( mode )
        {
        case 1:
            cout<<"Arithmetic progression"<<endl;
            do
            {
                cout<<"a1: ";
                cin>>a1;
            }
            while( a1<=0&&cout<<"Out of range!"<<endl );
            do
            {
                cout<<"d: ";
                cin>>d;
            }
            while( d<=0&&cout<<"Out of range!"<<endl );
            do
            {
                cout<<"n: ";
                cin>>n;
            }
            while( n<=0&&cout<<"Out of range!"<<endl );

            cout<<a1;
            for(int i=2; i<=n; i++) // calculate
                cout<<","<<a1+(i-1)*d;
            cout<<endl;
            break;
        case 2:
            cout<<"Geometric progression"<<endl;
            do
            {
                cout<<"a1: ";
                cin>>a1;
            }
            while( a1<=0&&cout<<"Out of range!"<<endl );
            do
            {
                cout<<"r: ";
                cin>>d;
            }
            while( d<=0&&cout<<"Out of range!"<<endl );
            do
            {
                cout<<"n: ";
                cin>>n;
            }
            while( n<=0&&cout<<"Out of range!"<<endl );

            cout<<a1;
            for(int i=2; i<=n; i++) // calculate
            {
                tmp=1;
                for(int j=1; j<=i-1; j++) // for r^j
                    tmp*=d;
                cout<<","<<a1*tmp;
            }
            cout<<endl;
            break;
        case 3:
            int a,b,n,tmp;
            cout<<"Fibonacci sequence"<<endl;
            do
            {
                cout<<"n: ";
                cin>>n;
            }
            while( n<=0&&cout<<"Out of range!"<<endl );
            a=b=1;
            cout<<a;
            for(int i=2; i<=n; i++)
            {
                tmp=a+b; // next fibonacci
                a=b; // change position
                b=tmp; // change position
                cout<<","<<a;
            }
            cout<<endl;
            break;
        case -1:
            break;
        }
        if( mode==-1 ) // exit application
            break;
    }
    return 0;
}
