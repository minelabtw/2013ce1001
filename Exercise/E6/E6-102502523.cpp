#include<iostream>
using namespace std;
int main()
{
    int a;//宣告變數
    cout<<"Calculate the progression.\n";
    cout<<"Choose the mode.\n";
    cout<<"Arithmetic progression=1.\n";
    cout<<"Geometric progression=2.\n";
    cout<<"Fibonacci sequence=3.\n";
    cout<<"Exit=-1.\n";
    cout<<"Enter mode: ";
    cin>>a;//輸入a的值
    while(1)
    {
        if(a!=-1)//若a非-1則執行以下程式
        {
            while(a!=1&&a!=2&&a!=3)//若a亦非1或2或3則進入迴圈
            {
                cout<<"Illegal input!\n";
                cout<<"Enter mode: ";
                cin>>a;//重新給定a的值
            }
            if(a==1)//若a等於1則執行下列程式
            {
                int a1;//宣告變數
                int d;
                int n;
                cout<<"Arithmetic progression\n";
                cout<<"a1: ";
                cin>>a1;//輸入a1
                while(a1<=0)//a1小於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"a1: ";
                    cin>>a1;//重新給定a1
                }
                cout<<"d: ";
                cin>>d;//輸入d
                while(d<=0)//若d小於等於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"d: ";
                    cin>>d;//重新給定d
                }
                cout<<"n: ";
                cin>>n;//輸入n
                while(n<=0)//若n小於等於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"n: ";
                    cin>>n;//重新給定n值
                }
                for(; n>0; n--)//進入迴圈
                {
                    if(n!=1)//若n不等於1則執行下列程式
                    {
                        cout<<a1<<",";
                    }
                    else
                    {
                        cout<<a1;
                    }
                    a1=a1+d;

                }

            }
            else if(a==2)//若a=2則執行下列程式
            {
                int a1;//宣告變數
                int r;
                int n;
                cout<<"Geometric progression\n";
                cout<<"a1: ";
                cin>>a1;//給定a1
                while(a1<=0)//若a1小於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"a1: ";
                    cin>>a1;//重新給定a1
                }
                cout<<"r: ";
                cin>>r;//給定r
                while(r<=0)//若r小於等於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"r: ";
                    cin>>r;//重新給定r值
                }
                cout<<"n: ";
                cin>>n;//給定n
                while(n<=0)//若n小於等於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"n: ";
                    cin>>n;//重新給定n值
                }
                for(; n>0; n--)//進入迴圈
                {
                    if(n!=1)//n不等於1則執行下列程式
                    {
                        cout<<a1<<",";
                    }
                    else
                    {
                        cout<<a1;
                    }
                    a1=a1*r;

                }

            }
            else
            {
                int n;//宣告變數
                cout<<"Fibonacci sequence\n";
                cout<<"n: ";
                cin>>n;//給定n
                while(n<=0)//n小於等於0則進入迴圈
                {
                    cout<<"Out of range!\n";
                    cout<<"n: ";
                    cin>>n;//重新給定n值
                }
                for(int x=1,y=0,z; n>0; n--)
                {
                    if(n!=1)//若n不等於1則執行下列程式
                    {
                        cout<<x<<",";
                    }
                    else
                    {
                        cout<<x;
                    }
                    z=x;
                    x=x+y;
                    y=z;
                }
            }

            cout<<"\nEnter mode: ";
            cin>>a;//重新給定a值判斷是否再次進入迴圈
        }
        else break;
    }
    return 0;
}
