#include<iostream>
using namespace std;

int main()
{
    int mode,a1,a2,d,r,n,i;
    cout << "Calculate the progression." << endl;
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;
    cout << "Enter mode: ";  //要求輸入模式代號mode(1,2,3,-1)
    cin >> mode;
    while (mode!=-1)  //只要不輸入-1就能重複計算，-1:結束計算
    {
        while (mode!=1 && mode!=2 && mode!=3)  //輸入非(1,2,3,-1)要輸出"Illegal input!"
        {
            cout << "Illegal input!" << endl;
            cout << "Enter mode: ";  //並重複輸入
            cin >> mode;
        }
        switch (mode)
        {
        case 1: //模式1:計算等差數列
            cout << "Arithmetic progression" << endl; //等差數列中會要輸入初項、公差、項數
            cout << "a1: ";
            cin >> a1;
            while (a1<=0) //初項、公差、公比、項數都要非零正整數
            {
                cout << "Out of range!" << endl; //否則要輸出"Out of range!"
                cout << "a1: "; //從輸入錯誤的該項重新輸入，提示也要再顯示一遍
                cin >> a1;
            }
            cout << "d: ";
            cin >> d;
            while (d<=0)
            {
                cout << "Out of range!" << endl;
                cout << "d: ";
                cin >> d;
            }
            cout << "n: ";
            cin >> n;
            while (n<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }
            cout << a1;
            for (i=1; i<=(n-1); i++)
            {
                a1 = a1 + i*d;
                cout << "," << a1;
            }
            break;
        case 2: //模式2:計算等比數列
            cout << "Geometric progression" << endl; //等比數列中會要輸入初項、公比、項數
            cout << "a1: ";
            cin >> a1;
            while (a1<=0)
            {
                cout << "Out of range!" << endl;
                cout << "a1: ";
                cin >> a1;
            }
            cout << "r: ";
            cin >> r;
            while (r<=0)
            {
                cout << "Out of range!" << endl;
                cout << "r: ";
                cin >> r;
            }
            cout << "n: ";
            cin >> n;
            while (n<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }
            cout << a1;
            for (i=1; i<=n-1; i++)
            {
                a1 = a1*r;
                cout << "," << a1;
            }
            break;
        case 3: //模式3:計算費氏數列
            cout << "Fibonacci sequence" << endl; //費氏數列中會要輸入項數
            a1 = 1;
            a2 = 1;
            cout << "n: ";
            cin >> n;
            while (n<=0)
            {
                cout << "Out of range!" << endl;
                cout << "n: ";
                cin >> n;
            }
            cout << a1;
            if (n>=2)
            cout << "," << a2;
            for (i=1; i<=n-2; i++)
            {
                a1 = a1 + a2;
                cout <<  "," << a1;
                i++;
                if(i<=n-2)
                {
                    a2 = a1 + a2;
                    cout << "," << a2;
                }
            }
            break;
        }
        cout << endl;
        cout << "Enter mode: ";
        cin >> mode;
    }
    return 0;
}
