#include<iostream>
using namespace std;

int main()
{
    int a=0;
    int a1=0;
    int d=0;
    int n=0;
    cout << "Calculate the progression." << endl;
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;
    cout << "Enter mode:" ;

    while ( cin >> a )      //當輸入a時,進入while迴圈
    {
        if ( a==-1 )        //當a=-1時,跳出迴圈
            break;

        switch ( a )
        {
        case 1:
            cout << "Arithmetic progression" << endl;   //case 1 等差數列
            cout << "a1: " ;
            cin >> a1 ;     //輸入首項值
            while (a1<=0)
            {
                cout << "Out of range!" ;
                cout << "a1: " ;
                cin >> a1 ;
            }

            cout << "d: ";  //輸入公差
            cin >> d ;
            while (d<=0)
            {
                cout << "Out of range!" ;
                cout << "d: " ;
                cin >> d ;
            }

            cout << "n: ";
            cin >> n ;      //輸入項數
            while (n<=0)
            {
                cout << "Out of range!" ;
                cout << "n: " ;
                cin >> n ;
            }
            cout << a1;
            for (int i=1 ; i<=n-1 ; i++ )
            {
                a1 = a1 + d ;
                cout << "," << a1 ;
            }
            cout << endl << "Enter mode:" ;
            break;

        case 2 :        //等比數列
            cout << "Geometric progression" << endl;
            cout << "a1: " ;
            cin >> a1 ;
            while (a1<=0)
            {
                cout << "Out of range!" ;
                cout << "a1: " ;
                cin >> a1 ;
            }

            cout << "r: ";
            cin >> d ;
            while (d<=0)
            {
                cout << "Out of range!" ;
                cout << "r: " ;
                cin >> d ;
            }

            cout << "n: ";
                 cin >> n ;
            while (n<=0)
            {
                cout << "Out of range!" ;
                cout << "n: " ;
                cin >> n ;
            }
            cout << a1;
            for (int i=1 ; i<=n-1 ; i++ )
            {
                a1 = a1 * d ;
                cout << "," << a1 ;
            }
            cout << endl << "Enter mode:" ;
            break;

        case 3:         //費氏數列
            cout << "Fibonacci sequence" << endl ;
            int b[10000];
            for (int j=1 ; j<=10000 ; j++)
                b[j]=-1;
            b[1] = 1;
            b[2] = 1;
            cout << "n: ";
            cin >> n ;
            while (n<=0)
            {
                cout << "Out of range!" ;
                cout << "n: " ;
                cin >> n ;
            }
            cout << "1" ;
            for (int v=3 ; v<=n ; v++ )
                b[v]=b[v-1]+b[v-2];
            for (int x=2 ; x<=n ; x++ )
                cout << "," << b[x];

            cout << endl << "Enter mode:" ;
            break;

        default :           //若輸入的a不符合要求,重新輸入
            cout << "Illegal input!" << endl;
            cout << "Enter mode:" ;
            break ;
        }
    }
return 0;
}
