#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int mode=0;//初始化各數值
    int a1=1;
    int d=0;
    int r,n;
    int an=0;
    int f1=1;
    int f2=1;
    int fn=0;
    int i=1;
    int x=0;

    cout<<"Calculate the progression."<<endl<<"Choose the mode."<<endl<<"Arithmetic progression=1."<<endl<<"Geometric progression=2."<<endl<<"Fibonacci sequence=3."<<endl<<"Exit=-1.";
    //輸出所需項目
    while (mode!=-1 )
    {
        cout<<endl<<"Enter mode: ";
        cin>>mode;

        switch ( mode )//輸出結果
        {
        case 1:
            cout<<"Arithmetic progression"<<endl<<"a1: ";
            cin>>a1;
            while (a1<=0){
            cout<<"Out of range!"<<endl<<"a1: ";
            cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            while (d<=0){
            cout<<"Out of range!"<<endl<<"d: ";
            cin>>d;
            }
            cout<<"n: ";
            cin>>n;
            while (n<=0){
                    cout<<"Out of range!"<<endl<<"n: ";
                    cin>>n;
            }
            x=a1+(n-1)*d;
            while (a1<=x){
                if (a1<x){
                    cout<<a1<<",";
                    a1=a1+d;
                }
                if (a1==x){
                    cout<<a1;
                    a1=a1+d;
                }
            }
            break;
        case 2:
            cout<<"Geometric progression"<<endl<<"a1: ";
            cin>>a1;
            while (a1<=0){
                cout<<"Out of range!"<<endl<<"a1: ";
                cin>>a1;
            }
            cout<<"r: ";
            cin>>r;
            while (r<=0){
                cout<<"Out of range!"<<endl<<"r: ";
                cin>>r;
            }
            cout<<"n: ";
            cin>>n;
            while (n<=0){
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }
            x=a1*pow(r,(n-1));
            while(a1<=x){
                if (a1<x){
                    cout<<a1<<",";
                    a1=a1*r;
                }
                if (a1==x){
                    cout<<a1;
                    a1=a1*r;
                }
            }
            break;

        case 3:
            cout<<"Fibonacci sequence"<<endl<<"n: ";
            cin>>n;
            while (n<=0){
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }
            while (i<=n){
                if (i==0){
                    cout<<"";

                }
                if (i==1){
                    fn=f1;
                    cout<<fn<<",";

                }
                if (i==2){
                    fn=f2;
                    cout<<fn<<",";

                }
                if (i>2 && i<n){
                        fn=f1+f2;
                        f1=f2;
                        f2=fn;
                        cout<<fn<<",";

                    }
                if (i==n){
                        fn=f1+f2;
                        f1=f2;
                        f2=fn;
                        cout<<fn;
                    }
                    i++;
                }
            break;
        case -1:
            break;

        default:
            cout<<"Illegal input!";
            break;

        }
    }
    return 0;
}
