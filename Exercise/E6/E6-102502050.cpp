#include<iostream>
using namespace std;
int checkinput(int number, int mode) //輸入首項 公比 公差 項數
{
    int input;
    do
    {
        if(number==2)                  //輸入項數  (mode1 & mode2 & mode3)
            cout << "n: ";
        else if(number==0)           //輸入首項  (mode1 & mode2)
            cout << "a1: ";
        else if(mode==1)             //輸入公差  (mode1)
            cout << "d: ";
        else                         //輸入公比  (mode2)
            cout << "r: ";
        cin >> input    ;
    }
    while(input<=0 && cout << "Out of range!\n");
    return input;
}
int main()
{
    int mode;
    int a1,dr,n;    //首項   公差/比  項數

    cout <<"Calculate the progression.\n";
    cout <<"Choose the mode.\n";
    cout <<"Arithmetic progression=1.\n";
    cout <<"Geometric progression=2.\n";
    cout <<"Fibonacci sequence=3.\n";
    cout <<"Exit=-1.\n";
    do
    {
        cout << "Enter mode: ";
        cin >> mode;
        switch (mode)
        {
        case 1:                                       //mode 1
            cout << "Arithmetic progression\n";
            a1=checkinput(0,1);                       //input
            dr=checkinput(1,1);
            n=checkinput(2,1);

            for(int i=1; i<n; i++, a1=a1+dr)          //output
                cout << a1 << ",";
            cout << a1 << endl;
            break;
        case 2:                                       //mode2
            cout << "Geometric progression\n";
            a1=checkinput(0,2);                       //input
            dr=checkinput(1,2);
            n=checkinput(2,2);

            for(int i=1; i<n; i++, a1=a1*dr)          //output
                cout << a1 << ",";
            cout << a1 << endl;
            break;
        case 3:                                       //mode3
            cout << "Fibonacci sequence\n";
            n=checkinput(2,3);                        //input
            a1=1;
            for(int i=1, temp1=0, temp2=0 ; i<n; i++, a1=temp1+temp2)
            {
                cout << a1 << ",";
                temp1=temp2;
                temp2=a1;
            }                                          //output
            cout << a1<< endl;
            break;
        case -1:
            break;
        default:                                       //illegal input
            cout << "Illegal input!\n";
        }
    }
    while( mode !=-1);

    return 0;
}
