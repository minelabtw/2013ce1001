#include <iostream>
using namespace std;

int main(){

    cout << "Calculate the progression." << endl << "Choose the mode." << endl << "Arithmetic progression=1." << endl << "Geometric progression=2." << endl << "Fibonacci sequence=3." << endl << "Exit=-1." << endl;

    int mode = 0;




    while(true){
        cout << "Enter mode: " ;
        cin >> mode;

        switch(mode){
            case 1 :
                int a1, d, n = 0;
                cout << "Arithmetic progression" << endl;
                while(a1 <= 0){
                    cout << "a1 : ";
                    cin >> a1;
                    if(a1 <= 0)
                        cout << "Out of range!" << endl;
                }
                while(d <= 0){
                    cout << "d : ";
                    cin >> d;
                    if(d <= 0)
                        cout << "Out of range!" << endl;
                }
                while(n <= 0){
                    cout << "n : ";
                    cin >> n;
                    if(n <= 0)
                        cout << "Out of range!" << endl;
                }
                cout << a1;
                for(int i=1; i<n; i++){
                    cout << "," << a1+(d*i) ;
                }
                break;

            case 2 :
                int a1, r, n = 0;
                cout << "Geometric progression" << endl;
                while(a1 <= 0){
                    cout << "a1 : ";
                    cin >> a1;
                    if(a1 <= 0)
                        cout << "Out of range!" << endl;
                }
                while(r <= 0){
                    cout << "r : ";
                    cin >> r;
                    if(r <= 0)
                        cout << "Out of range!" << endl;
                }
                while(n <= 0){
                    cout << "n : ";
                    cin >> n;
                    if(n <= 0)
                        cout << "Out of range!" << endl;
                }
                cout << a1;
                for(int i=1; i<n; i++){
                    cout << "," << a1*(r*i) ;
                }
                break;
            case 3 :
                int n = 0;
                cout << "Fibonacci sequence" << endl;

                while(n <= 0){
                    cout << "n : ";
                    cin >> n;
                    if(n <= 0)
                        cout << "Out of range!" << endl;
                }
                cout << 1;
                int x = 0, y = 1;
                for(int i=1; i<n; i++){
                    cout << "," << x+y ;
                    int temp = x;
                    x = y;
                    y += temp;
                }
                break;
            case -1 :
                return 0;

            default :
                cout << "Illegal input!" ;
                break;
        }





    }

}
