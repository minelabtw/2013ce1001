#include <iostream>
#include <cmath>

using namespace std ;

int main ()
{
    int mode = 0 ;
    int a1 = 0 ;
    int d = 0 ;
    int n = 0 ;
    int r = 0 ;
    int n1 = 1 ;
    int n2 = 1 ;

    cout << "Calculate the progression.\nChoose the mode.\nArithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1." ;
    while ( mode != -1 )
    {
        cout << endl <<  "Enter mode: " ;
        cin >> mode ;  //使操作者輸入mode。

        switch ( mode )  //對mode做判斷。
        {
        case 1 :
            cout << "Arithmetic progression\na1: " ;
            cin >> a1 ;  //輸入首項。
            while ( a1 <= 0 )
            {
                cout << "Out of range!\na1: " ;
                cin >> a1 ;
            }
            cout << "d: " ;
            cin >> d ;  //輸入公差。
            while ( d <= 0 )
            {
                cout << "Out of range!\nd: " ;
                cin >> d ;
            }
            cout << "n: " ;
            cin >> n ;  //輸入項數。
            while ( n <= 0 )
            {
                cout << "Out of range!\nn: " ;
                cin >> n ;
            }
            int ad ;
            ad = a1 + ( n - 1 ) * d ;
            for ( int x = 1 ; x < n ; x++ )
            {
                cout << a1 << "," ;
                a1 = a1 + d ;
            }
            cout << ad ;
            break ;


        case 2 :
            cout << "Geometric progression\na1: " ;
            cin >> a1 ;  //輸入首項。
            while ( a1 <= 0 )
            {
                cout << "Out of range!\na1: " ;
                cin >> a1 ;
            }
            cout << "r: " ;
            cin >> r ;  //輸入公比。
            while ( r <= 0 )
            {
                cout << "Out of range!\nr: " ;
                cin >> r ;
            }
            cout << "n: " ;
            cin >> n ;  //輸入項數。
            while ( n <= 0 )
            {
                cout << "Out of range!\nn: " ;
                cin >> n ;
            }
            int ar ;
            ar = a1 * pow ( r , n - 1 ) ;
            for ( int x = 1 ; x < n ; x++ )
            {
                cout << a1 << "," ;
                a1 = a1 * r ;
            }
            cout << ar ;
            break ;


        case 3 :
            cout << "Fibonacci sequence\nn: " ;
            cin >> n ;
            while ( n <= 0 )
            {
                cout << "Out of range!\nn: " ;
                cin >> n ;  //輸入項數。
            }
            cout << n1 << "," << n2 ;
            int n3 ;
            for ( int x = 2 ; x < n ; x++ )
            {
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
                cout << "," << n3;
            }
            break ;


        case -1 :
            break ;  //輸入-1時，就跳出判斷，結束程式。


        default :
            cout << "Illegal input!\nEnter mode: " ;
            cin >> mode ;
        }
    }
    return 0 ;
}
