#include<iostream>
#include<math.h>
using namespace std;
int main(void)
{
    cout << "Calculate the progression." << endl;
    cout << "Choose  the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;

    int mode=0;

    while(mode!=-1)
    {
        int one[3]= {};                                 //mode 1 的變數
        int two[3]= {};                                 //mode 2 的變數
        long long int three[3]= {1,1,0};                //mode 3 的變數
        int i=0;

        cout << "Enter mode: ";
        cin >> mode;

        switch(mode)
        {
        case -1:                        //mode=-1 程式結束
            break;

            /*------1111111111111111111111111--------*/
        case 1:                         //mode= 1 計算等差數列
            cout << "Arithmetic progression" << endl;

            while(i<3)
            {
                switch(i)
                {
                case 0:
                    cout << "a1: ";
                    break;
                case 1:
                    cout << "d: ";
                    break;
                case 2:
                    cout << "n: ";
                    break;
                }
                cin >> one[i];
                if(one[i]<=0)
                    cout << "Out of range!" << endl;
                else
                    i++;
            }
            for(int j=0; j<one[2]; j++)
            {
                cout << one[0] + one[1]*j;
                if(j!=one[2]-1)
                {
                    cout << ",";
                }
                else cout << endl;
            }
            break;

            /*-------2222222222222222222-------*/
        case 2:                         //mode= 2 計算等比數列
            cout << "Geometric progression" << endl;

            i=0;
            while(i<3)
            {
                switch(i)
                {
                case 0:
                    cout << "a1: ";
                    break;
                case 1:
                    cout << "r: ";
                    break;
                case 2:
                    cout << "n: ";
                    break;
                }
                cin >> two[i];
                if(two[i]<=0)
                {
                    cout << "Out of range!" << endl;
                }
                else i++;
            }
            for(int j=0; j<two[2]; j++)
            {
                cout << two[0]*(pow(two[1],j));
                if(j!=two[2]-1)
                    cout << ",";
                else
                    cout << endl;
            }
            break;

            /*------3333333333333333333333333------*/
        case 3:                         //算出費氏數列
            cout << "Fibonacci sequence" << endl;

            i=0;
            while(i<1)
            {
                cout << "n: ";
                cin >> three[2];
                if(three<=0)
                    cout << "Out of range!";
                else
                    i++;
            }
            for(int j=0; j<three[2]; j++)
            {
                if(j<2)
                    cout << "1";
                else
                {
                    switch(j%2)
                    {
                    case 0:
                        three[0]=three[0]+three[1];
                        cout << three[0];
                        break;
                    case 1:
                        three[1]=three[0]+three[1];
                        cout << three[1];
                        break;
                    }
                }
                if(j!=three[2]-1)
                    cout << ",";
                else
                    cout << endl;
            }
            break;

            /*--------------------------------------*/
        default:                            //mode= error 輸出"錯誤"
            cout << "Illegal input!" << endl;
            break;
        }
    }
    return 0;
}
