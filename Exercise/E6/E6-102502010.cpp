#include <iostream>
using namespace std;

int main()
{
    int mode;
    int a1;
    int d;
    int n;
    int r;
    int i;
    int f1,f2,f3;
    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode: ";
    while(cin>>mode,mode!=-1)
    {
        if(mode==1)
        {
            cout<<"Arithmetic progression"<<endl;
            cout<<"a1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            while(d<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"d: ";
                cin>>d;
            }
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            cout<<a1;
            for(i=1; i<n; i++)
            {
                a1+=d;
                cout<<','<<a1;
            }
            cout<<endl;
        }
        else if(mode==2)
        {
            cout<<"Geometric progression"<<endl;
            cout<<"a1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"r: ";
            cin>>r;
            while(r<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"r: ";
                cin>>r;
            }
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            cout<<a1;
            for(i=1; i<n; i++)
            {
                a1*=r;
                cout<<','<<a1;
            }
            cout<<endl;
        }
        else if(mode==3)
        {
            cout<<"Fibonacci sequence"<<endl;
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }
            f1=1;
            f2=1;
            f3=2;
            if(n==1)
                cout<<f1<<endl;
            else if(n==2)
                cout<<f1<<','<<f2<<endl;
            else if(n==3)
                cout<<f1<<','<<f2<<','<<f3<<endl;
            else
            {
                cout<<f1<<','<<f2<<','<<f3;
                for(i=3; i<n; i++)
                {
                    f1=f2;
                    f2=f3;
                    f3=f1+f2;
                    cout<<','<<f3;
                }
                cout<<endl;
            }
        }
        else
            cout<<"Illegal input!"<<endl;
        cout<<"Enter mode: ";
    }
    return 0;
}

