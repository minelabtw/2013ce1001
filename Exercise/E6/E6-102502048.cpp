#include<iostream>
using namespace std;
int main()
{
    int a1=0,d=0,n=0,r=0,N=0,a=0;//宣告變數
    int Arithmetic=0,Geometric=0,F[1000],f=0;//宣告變數
    cout<<"Calculate the progression."<<endl<<"Choose the mode."<<endl<<"Arithmetic progression=1."<<endl;//顯示字串
    cout<<"Geometric progression=2."<<endl<<"Fibonacci sequence=3."<<endl<<"Exit=-1."<<endl;//顯示字串
    do//先做後判斷迴圈
    {
        cout<<"Enter mode: ";//顯示字串
        cin>>a;//輸入
        while(a!=1 && a!=2 && a!=3 && a!=-1)
        {
            cout<<"Illegal input!";//顯示字串
            cin>>a;//輸入
        }
        switch(a)//判斷a值
        {
        case 1://等於1
        {
            cout<<"Arithmetic progression=1."<<endl;//顯示字串
            cout<<"a1: ";//顯示字串
            cin>>a1;//輸入
            while(a1<=0)//不讓值小於0
            {
                cout<<"Out of range!";//顯示字串
                cin>>a1;//輸入
            }
            cout<<"d: ";//顯示字串
            cin>>d;//輸入
            while(d<=0)//不讓值小於0
            {
                cout<<"Out of range!";//顯示字串
                cin>>d;//輸入
            }
            cout<<"n: ";//顯示字串
            cin>>n;//輸入
            while(n<=0)//不讓值小於0
            {
                cout<<"Out of range!";//顯示字串
                cin>>n;//輸入
            }
            cout<<a1;//顯示字串
            for(N=1; N<n; N++)//連續計算
            {
                Arithmetic=a1+d;//計算
                cout<<","<<Arithmetic;//顯示字串
                a1=Arithmetic;//計算
            }
            break;
        }
        case 2://等於2
        {
            cout<<"Geometric progression=2."<<endl;//顯示字串
            cout<<"a1: ";//顯示字串
            cin>>a1;//輸入
            while(a1<=0)//不讓值小於0
            {
                cout<<"Out of range!";//顯示字串
                cin>>a1;//輸入
            }
            cout<<"r: ";//顯示字串
            cin>>r;//輸入
            while(r<=0)//不讓值小於0
            {
                cout<<"Out of range!";//顯示字串
                cin>>r;//輸入
            }
            cout<<"n: ";//顯示字串
            cin>>n;//輸入
            while(n<=0)//不讓值小於0
            {
                cout<<"Out of range!";//顯示字串
                cin>>n;//輸入
            }
            cout<<a1;//顯示字串
            for(N=1; N<n; N++)//連續計算
            {
                Geometric=a1*r;//計算
                cout<<","<<Geometric;//顯示字串
                a1=Geometric;//計算
            }
            break;
        }
        case 3://等於3
        {
            cout<<"Fibonacci sequence"<<endl;//顯示字串
            cout<<"n: ";//顯示字串
            cin>>n;//輸入
            while(n<=0)//不讓值小於0
            {
                cout<<"Out of range!";//顯示字串
                cin>>n;//輸入
            }
            cout<<"1";//顯示字串
            for(N=3,f=2; f<=n; N++,f++)//連續計算
            {
                F[1]=1;//計算
                F[2]=1;//計算
                F[N]=F[N-1]+F[N-2];//計算
                cout<<","<<F[f];//顯示字串
            }
            break;
        }
        default://結束 等於-1
            break;
        }
        cout<<endl;//換行
    }
    while(a==1 || a==2 || a==3);//迴圈
    return 0;
}
