#include <iostream>

using namespace std;

int main()
{

    cout << "Calculate the progression.\nChoose the mode.\nArithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.\nEnter mode: ";

    int mode; // 模式

    while(cin >> mode && mode != -1) //判斷輸入模式
    {
        int a1 = 0; // 初項
        int d = 0; // 公差
        int r = 0; // 公比
        int n = 0; // 項數
        int num1=0; // 費氏數字1
        int num2=0; // 費氏數字2
        int num3=1; // 費氏數字3


        switch(mode)
        {

            // 等差數列 需要 a1,d,n
        case 1:
            cout << "Arithmetic progression\n";

            // 輸入首項
            cout << "a1: ";
            while(a1<1)
            {
                cin >> a1;
                if(a1<1)cout << "Out of range!\na1: ";
            }

            // 輸入公差
            cout << "d: ";
            while(d<1)
            {
                cin >> d;
                if(d<1)cout << "Out of range!\nd: ";
            }

            // 輸入項數
            cout << "n: ";
            while(n<1)
            {
                cin >> n;
                if(n<1)cout << "Out of range!\nn: ";
            }

            // 輸出等差數列
            for(int i=0; i<n; i++)
            {
                cout << a1;
                if(i != n-1)cout << ",";
                a1+=d;
            }

            cout << "\nEnter mode: ";

            break;

            // 等比數列 需要 a1,r,n
        case 2:

            cout << "Geometric progression\n";
            // 輸入首項
            cout << "a1: ";
            while(a1<1)
            {
                cin >> a1;
                if(a1<1)cout << "Out of range!\na1: ";
            }

            // 輸入公比
            cout << "r: ";
            while(r<1)
            {
                cin >> r;
                if(r<1)cout << "Out of range!\nr: ";
            }

            // 輸入項數
            cout << "n: ";
            while(n<1)
            {
                cin >> n;
                if(n<1)cout << "Out of range!\nn: ";
            }

            // 輸出等比數列
            for(int i=0; i<n; i++)
            {
                cout << a1;
                if(i != n-1)cout << ",";
                a1*=r;
            }

            cout << "\nEnter mode: ";

            break;

            // 費式數列 需要 n
        case 3:

            cout << "Fibonacci sequence\n";
            // 輸入項數
            cout << "n: ";
            while(n<1)
            {
                cin >> n;
                if(n<1)cout << "Out of range!\nn: ";
            }

            // 輸出費氏數列
            for (int i=0; i<n; i++)
            {
                num1 = num3 + num2;
                cout << num1;
                num3 = num2;
                num2 = num1;
                if(i != n-1)cout << ",";
            }
            cout << "\nEnter mode: ";
            break;

            // 非法輸入
        default:
            cout << "Illegal input!\nEnter mode: ";
            break;
        }
    }

    return 0;

}
