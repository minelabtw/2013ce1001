#include<iostream>
using namespace std;

int main()
{
    int n;      //兜计
    int d;      //そ畉
    int r;      //そゑ
    int a1;     //兜
    int mode;   //家Α
    int x = 1;  //そゑ纗ノ,穦家Α2ノ
    int i;      //癹伴ノ
    int num1,num2,num3;//穦家Α3ノ

    cout << "Calculate the progression.\n"; //肈ヘ
    cout << "Choose the mode.\n";
    cout << "Arithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.\n";

    cout << "Enter mode: "; //匡拒家Α
    cin >> mode;
    while(mode!=-1) //璶家Αぃ-1碞膥尿磅︽
    {
        if(mode<=0||mode>3) //家Α﹚-1,1,2,3贺,ㄤ逼埃
        {
            if(mode==-1)
            {
                break;
            }
            cout << "Illegal input!\n";
            cout << "Enter mode: ";
            cin >> mode;
        }

        if(mode==1) //家Α1单畉计
        {
            cout << "Arithmetic progression\n";
            cout << "a1: ";
            cin >> a1;
            while(a1<=0)    //絋玂﹍a1>=0
            {
                cout << "Out of range!\n";
                cout << "a1: ";
                cin >> a1;
            }
            cout << "d: ";
            cin >> d;
            while(d<=0)     //絋玂そ畉>0
            {
                cout << "Out of range!\n";
                cout << "d: ";
                cin >> d;
            }
            cout << "n: ";  //絋玂兜计>0
            cin >> n;
            while(n<=0)
            {
                cout << "Out of range!\n";
                cout << "n: ";
                cin >> n;
            }
            for(i=1; i<=n; i++) //磅︽单畉计挡狦
            {
                cout << a1+(i-1)*d;
                if(i<n)         //ヘňゎ程计ごΤ","
                {
                    cout << ",";
                }
            }
            cout << "\n";       //磅︽挡狦传︽块家Α
            cout << "Enter mode: ";
            cin >> mode;
        }

        if(mode==2)     //家Α2单ゑ计
        {
            cout << "Geometric progression\n";
            cout << "a1: ";
            cin >> a1;
            while(a1<=0)    //絋玂﹍a1>0
            {
                cout << "Out of range!\n";
                cout << "a1: ";
                cin >> a1;
            }
            cout << "r: ";  //絋玂そゑr>0
            cin >> r;
            while(r<=0)
            {
                cout << "Out of range!\n";
                cout << "r: ";
                cin >> r;
            }
            cout << "n: ";
            cin >> n;
            while(n<=0)     //絋玂兜计>0
            {
                cout << "Out of range!\n";
                cout << "n: ";
                cin >> n;
            }
            for(i=1,x=1; i<=n; i++) //磅︽单ゑ计挡狦
            {

                cout << a1*x;       //xノ材n兜计rΩよ磅︽
                x = x*r;
                if(i<n)
                {
                    cout << ",";
                }
            }
            cout << "\n";
            cout << "Enter mode: ";
            cin >> mode;
        }

        if(mode==3)     //家Α3禣ん计
        {
            cout << "Fibonacci sequence\n";
            cout << "n: ";
            cin >> n;
            while(n<=0) //絋玂兜计>0
            {
                cout << "Out of range!\n";
                cout << "n: ";
                cin >> n;
            }
            if(n<=2)    //狦兜计<=2兜碞钡块
            {
                cout << "1";
                if(n==2)
                {
                    cout << ",2";
                }
            }
            else        //讽n>=3甅ノ
            {
                num1 = num2 = 1;
                cout << num1 << "," << num2 << ",";
                for(i=1; i<=n-2; i++)
                {
                    num3 = num1+num2;   //num1+num2num3,盢num3块
                    cout << num3;
                    num1 = num2;        //块рnum3,num2常┕玡崩计num2-->num1,num3-->num2(计璶暗崩秈)
                    num2 = num3;
                    if(i<n-2)
                    {
                        cout << ",";    //ňゎ程计块临Τ","
                    }
                }
            }
            cout << "\n";
            cout << "Enter mode: "; //块家Α
            cin >> mode;
        }
    }
    return 0;
}
