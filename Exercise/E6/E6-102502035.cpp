#include <iostream>
#include <cmath>
using namespace std;
int main ()
{
    int mode =0;//宣告變數並=0
    int int1 =0;
    int int2 =0;
    int int3 =0;
    int loopcounter =0;
    cout << "Calculate the progression." << endl << "Choose the mode." << endl ;//輸出題目要求
    cout << "Arithmetic progression=1." << endl << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl << "Exit=-1." << endl;
    while (mode !=-1)//當模式輸入非-1程式必須不停迴圈
    {
        loopcounter =0;//實行另一迴圈時將數字歸零
        cout << "Enter mode: ";
        cin >> mode;
        switch (mode)//依模式選擇
        {
        case 1://模式1
            cout << "Arithmetic progression" << endl;//輸出題目要求
            cout << "a1: ";//提示輸入變數
            cin >> int1;//輸入變數
            while (int1<=0)//判斷是否符合條件
            {
                cout << "Out of range!" << endl;//提示不符合條件
                cout << "a1: ";//提示輸入變數
                cin >> int1;
            }
            cout << "d: ";//提示輸入變數
            cin >> int2;//輸入變數
            while (int2<=0)//判斷是否符合條件
            {
                cout << "Out of range!" << endl;//提示不符合條件
                cout << "d: ";//提示輸入變數
                cin >> int2;//輸入變數
            }
            cout << "n: ";//提示輸入變數
            cin >> int3;//輸入變數
            while (int3<=0)//判斷是否符合條件
            {
                cout << "Out of range!" << endl;//提示不符合條件
                cout << "n: ";//提示輸入變數
                cin >> int3;//輸入變數
            }
            while (loopcounter<int3-1)//執行項數次-1
            {
                cout << int1+int2*loopcounter << ",";
                loopcounter++;//下一項
            }
            cout << int1+int2*loopcounter << endl;//最後一次，結尾沒逗號
            break;//離開switch
        case 2:
            cout << "Geometric progression" << endl;//輸出題目要求
            cout << "a1: ";//提示輸入變數
            cin >> int1;//輸入變數
            while (int1<=0)//判斷是否符合條件
            {
                cout << "Out of range!" << endl;//提示不符合條件
                cout << "a1: ";//提示輸入變數
                cin >> int1;//輸入變數
            }
            cout << "r: ";//提示輸入變數
            cin >> int2;//輸入變數
            while (int2<=0)//判斷是否符合條件
            {
                cout << "Out of range!" << endl;//提示不符合條件
                cout << "r: ";//提示輸入變數
                cin >> int2;//輸入變數
            }
            cout << "n: ";//提示輸入變數
            cin >> int3;//輸入變數
            while (int3<=0)//判斷是否符合條件
            {
                cout << "Out of range!" << endl;//提示不符合條件
                cout << "n: ";//提示輸入變數
                cin >> int3;//輸入變數
            }
            while (loopcounter<int3-1)//執行項數次-1
            {
                cout << int1*(pow (int2,loopcounter)) << ",";
                loopcounter++;//下一項
            }
            cout << int1*(pow (int2,loopcounter)) << endl;//最後一次，結尾沒逗號
            break;
        case 3://離開switch
            cout << "Fibonacci sequence" << endl;//輸出題目要求
            cout << "n: ";//提示輸入變數
            cin >> int3;//輸入變數
            while (int3<=0)//判斷是否符合條件
            {
                cout << "Out of range!" << endl;//提示不符合條件
                cout << "n: ";//提示輸入變數
                cin >> int3;//輸入變數
            }
            int1 =1;
            int2 =0;
            while (loopcounter<int3-1)//執行項數次-1
            {
                if (loopcounter==0)//一開始1
                    cout << "1,";
                else if (loopcounter%2==0)//偶數
                {
                    int1=int1+int2;//提出奇數加上它
                    cout << int1 << ",";
                }
                else//奇數
                {
                    int2=int1+int2;//提出偶數加上它
                    cout << int2 << ",";
                }
                loopcounter++;//下一項
            }
            if (loopcounter%2==0)//偶數
            {
                int1=int1+int2;//提出奇數加上它
                cout << int1 << endl;//最後一次，結尾沒逗號
            }
            else//奇數
            {
                int2=int1+int2;//提出偶數加上它
                cout << int2 << endl;//最後一次，結尾沒逗號
            }
            break;//離開switch
        case -1://輸入-1結束程式
            break;//離開switch
        default://輸入其他數字重新輸入
                cout << "Illegal input!" << endl;
            break;//離開switch
        }
    }
    return 0;
}
