#include<iostream>
using namespace std;
int main()
{
    int x,y=0  ;  //設定變數
    int a1 , d , n  ,r;
    cout << "Calculate the progression." << endl << "Choose the mode." << endl ;   //提示使用者
    cout << "Arithmetic progression=1." <<endl <<"Geometric progression=2." <<endl ;
    cout << "Fibonacci sequence=3." << endl << "Exit=-1." <<endl ;
    do  //迴圈開始
    {
        cout << "Enter mode: " ;
        cin >> x ;
        while (x<-1 || x > 3 || x==0)    //如果不符合範圍就重新輸入
        {
            cout << "Illegal input!" <<endl ;
            cout << "Enter mode: " ;
            cin >> x ;
        }

        switch ( x )    //判斷要進行哪種計算
        {
        case 1 :   //計算等差數列
            cout << "Arithmetic progression" <<endl ;
            cout << "a1: " ;
            cin >> a1 ;

            cout << "d: " ;
            cin >>d ;

            cout << "n: " ;
            cin >>n ;
            while (n<=0)   //如果不符合範圍就重新輸入
            {
                cout << "Out of range!" <<endl ;
                cout << "n: " ;
                cin >>n ;
            }
            for (y=1; y<=n; y++)
            {
                cout << a1+ (y-1)*d << " " ;
            }
            cout << endl ;
            break;

        case 2 :  //計算等比數列
            cout << "Geometric progression" <<endl;
            cout << "a1: " ;
            cin >> a1 ;

            cout << "r: " ;
            cin >>r ;

            cout << "n: " ;
            cin >>n ;
            while (n<=0)  //如果不符合範圍就重新輸入
            {
                cout << "Out of range!" <<endl ;
                cout << "n: " ;
                cin >>n ;
            }
            cout << a1 <<" " ;
            for (y=1; y<n; y++)
            {
                cout << a1*y*r << " " ;
            }

            cout << endl ;
            break;

        case 3 :  //計算費氏數列
            cout <<"Fibonacci sequence" <<endl;
            cout << "n: " ;
            cin >>n ;
            a1=1 ;
            int a2=1;
            while (n<2)   //如果不符合範圍就重新輸入
            {
                cout << "Out of range!" <<endl ;
                cout << "n: " ;
                cin >>n ;
            }
            cout << a1 << " " << a2 <<" " ;
            for (y=2; y<n; y++)
            {
                int z ;
                z=a1+a2;
                cout << z <<" " ;
                a1=a2;
                a2=z;
            }
            cout << endl ;
            break;

        }
    }
    while (x!=-1) ;

    return 0 ;
}
