#include <iostream>
#include <cstdlib>

using namespace std;

/*Input and Out of Range check */
#define Orange(a) \
            do { \
                cout << #a << ": "; cin >> a;\
                } while(a <= 0 && (cout << "Out of range!\n"))

/*Illegal input for Enter mode*/
void Ill(void) {
    cout << "Illegal input!\n";
    }

/*functon for Arithmetic progression*/
void Ari(void) {
    int a1, d, n;

    cout << "Arithmetic progression\n";

    Orange(a1);
    Orange(d);
    Orange(n);

/*Ouput Arithmetic progression*/
    cout << a1;
    a1 += d;
    for (int i = 1; i < n; i++, a1 += d)
        cout << "," << a1;
    cout << "\n";

    }

/*functon for Geometric progression*/
void Geo(void) {
    int a1, r, n;

    cout << "Geometric progression\n";

    Orange(a1);
    Orange(r);
    Orange(n);

/*Output Geometric progression*/
    cout << a1;
    a1 *= r;
    for (int i = 1; i < n; i++, a1 *= r)
        cout << "," << a1;
    cout << "\n";

    }

/*functon for Fibonacci sequence*/
void Fib(void) {
    int n;
    int s[2] = {1, 1};

    Orange(n);

/*Output Fibonacci sequence*/
    cout << s[0];
    for (int i = 1; i < n; s[!(i%2)] += s[(i%2)],i++)
        cout << "," << s[(i%2)];
    cout << "\n";

    }

/*Exit*/
void Exit(void) {
    exit(EXIT_SUCCESS);
    }

void (*f[5])(void) = {Ill, Ari, Geo, Fib, Exit};


int main(void) {
    int n;

    cout <<"Calculate the progression.\n"
           "Choose the mode.\n"
           "Arithmetic progression=1.\n"
           "Geometric progression=2.\n"
           "Fibonacci sequence=3.\n"
           "Exit=-1.\n";

    while(1) {
        cout << "Enter mode: ";
        cin >> n;

/*MODE*/
        if (n == -1)
            f[4]();
        else if (n == 1)
            f[1]();
        else if (n == 2)
            f[2]();
        else if (n == 3)
            f[3]();
        else
            f[0]();

        }

    return 0;
    }
