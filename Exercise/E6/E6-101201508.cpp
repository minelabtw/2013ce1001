#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1 ;
    int a1 ;
    int d ;
    int n ;
    int next1 ;
    int next2 ;
    int next3 ;
    cout << "Calculate the progression.\nChoose the mode.\nArithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.\nEnter mode: " ;
    cin >> number1 ;
    while (number1 !=-1)                                        //重複，-1則跳出
    {
        if (number1==1)                                         //等差數列
        {
            cout << "Arithmetic progression" << endl << "a1: " ;//輸入
            cin >> a1 ;
            while (a1<1)
            {
                cout << "Out of range!" << endl << "a1: " ;
                cin >> a1 ;
            }
            cout << "d: " ;
            cin >> d ;
            while (d<1)
            {
                cout << "Out of range!" << endl << "d: " ;
                cin >> d ;
            }
            cout << "n: " ;
            cin >> n ;
            while (n<1)
            {
                cout << "Out of range!" << endl << "n: " ;
                cin >> n ;
            }

            for (int i=0; i<n; ++i)
            {
                cout << a1+i*d ;
                if (i!=n-1)
                    cout << "," ;
            }
            cout << endl << "Enter mode:" ;
            cin >> number1 ;
        }
        else if (number1==2)                                   //等比數列
        {
            cout << "Geometric progression" << endl << "a1: " ; //輸入
            cin >> a1 ;
            while (a1<1)
            {
                cout << "Out of range!" << endl << "a1: " ;
                cin >> a1 ;
            }
            cout << "r: " ;
            cin >> d ;
            while (d<1)
            {
                cout << "Out of range!" << endl << "r: ";
                cin >> d ;
            }
            cout << "n: " ;
            cin >> n ;
            while (n<1)
            {
                cout << "Out of range!" << endl << "n: " ;
                cin >> n ;
            }
            next1 =a1 ;
            cout << next1 ;
            if (n!=1)
                cout << "," ;
            for (int i=1; i<n; ++i)
            {
                next1=next1*d ;
                cout << next1 ;
                if (i!=n-1)
                    cout << "," ;
            }
            cout << endl << "Enter mode:" ;
            cin >> number1 ;
        }
        else if (number1==3)                                      //費氏數列
        {
            cout << "Fibonacci sequence" << endl << "n: " ;
            cin >> n ;
            while (n<1)
            {
                cout << "Out of range!" << endl << "n: " ;
                cin >> n ;
            }
            if (n==1)
            {
                cout << "1" ;
            }
            else if (n==2)
            {
                cout << "1,1" ;
            }
            else if (n>2)
            {
                cout << "1,1," ;
                next1=1 ;
                next2=1 ;
                for (int i=2; i<n; ++i)
                {
                    next3=next1+next2 ;
                    next1=next2 ;
                    next2=next3 ;
                    cout << next3 ;
                    if (i!=n-1)
                        cout << "," ;
                }
                cout << endl << "Enter mode:" ;
                cin >> number1 ;
            }
        }
        else if (number1==0 ||number1<-1 ||number1>3)                     //輸入錯誤就要求重新輸入
        {
            cout << "Illegal input!" <<endl << "Enter mode: ";
            cin >> number1 ;
        }
    }

    return 0;
}
