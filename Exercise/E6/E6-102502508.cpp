#include<iostream>
#include<cmath>
using namespace std ;
int main()
{   //本次的練習目標在於運用轉換目標系統,迴圈的概念和數學常識來建構一個能提供使用者查詢三種不同的數列
    int a1=0 ;
    int n=0 ;
    int d=0 ;
    int an=0 ;
    int c=0 ;
    int sn=1;
    int fn=0 ;
    int z=0 ;




    cout<<"Calculate the progression."<<endl ;
    cout<<"Choose the mode"<<endl ;
    cout<<"Arithmetic progression=1."<<endl ;
    cout<<"Geometric progression=2."<<endl ;
    cout<<"Fibonacci sequence=3."<<endl ;
    cout<<"Exit=-1."<<endl ;
    cout<<"Enter mode: " ;
    while(cin >>c and c!=-1)
    {


        switch(c)
        {
        case 1 :
            cout<<"Arithmetic progression"<<endl ;
            cout<<"a1: ";
            cin>>a1 ;
            if(a1<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"a1: ";
                cin >>a1 ;
            }
            cout<<"d: ";
            cin>>d ;
            if(d<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"d: " ;
                cin>>d ;

            }
            cout<<"n: " ;
            cin>>z ;
            if(z<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"n: ";
                cin>>z ;
            }

            for(n=1 ; n<=z ; n=n+1)
            {
                an=a1+(n-1)*d ;
                cout<<an ;
                if(n<z)
                    cout<<"," ;


            }

            break ;

        case 2:
            cout<<"Geometric progression"<<endl ;
            cout<<"a1: ";
            cin>>a1 ;
            if(a1<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"a1: ";
                cin >>a1 ;
            }
            cout<<"d: ";
            cin>>d ;
            if(d<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"d: " ;
                cin>>d ;

            }
            cout<<"n: " ;
            cin>>z ;
            if(z<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"n: ";
                cin>>z ;
            }
            for(n=1 ; n<=z ; n=n+1)
            {
                an=a1*pow(d,n-1) ;
                cout<<an;
                if(n<z)
                    cout<<"," ;



            }
            break ;
        case 3:
            cout<<"Fibonacci sequence"<<endl ;

            cout<<"n: " ;
            cin>>z ;
            if(z<=0)
            {
                cout<<"Out of range!"<<endl ;
                cout<<"n: ";
                cin>>z ;
            }
            for(n=1 ; n<=z ; n=n+1)
            {

                an=sn+fn ;
                sn=fn   ;           //運用迴圈概念來表示費波納契函數
                fn=an  ;
                cout<<an;
                if(n<z)
                    cout<<",";
            }
            break ;                   //達成目標後自動跳出

        default:
            cout<<"Illegal input!";
            break ;
        }
        cout<<endl ;
        cout<<"Enter mode: " ;      //讓使用者能夠再次的選擇運算的模式
        if(c==-1)
        {
            cout<<endl ;
        }




    }
    return 0 ;
}
