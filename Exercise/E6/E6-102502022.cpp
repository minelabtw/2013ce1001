#include<iostream>

using namespace std;

int main()

{
    int a=0;     //宣告名稱為a的變數

    cout<< "Calculate the progression.\n";
    cout<< "Choose  the mode.\n";
    cout<< "Arithmetic progression=1.\n";
    cout<< "Geometric progression=2.\n";
    cout<< "Fibonacci sequence=3.\n";
    cout<< "Exit=-1.\n";

    cout<< "Enter mode:";
    cin>> a;
    switch(a)    //讀取a的值
    {
    case 1:      //將1丟給a
        break;

    case 2:      //將2丟給a
        break;

    case 3:      //將3丟給a
        break;

    case -1:     //將-1丟給a
        break;

    default:      //其餘的值丟給a時
        while(a!=1 || a!=2 || a!=3 || a!=-1)    //迴圈  在a不等於1或不等於2或不等於3或不等於-1的情況下
        {
            cout<< "Out of range!\n";
            cout<< "Enter mode:";
            cin>> a;
        }

    }

    while(a==1)      //迴圈   在a等於1的情況下
    {
        int b=0;     //宣告名稱為b的變數
        int b1=0;    //宣告名稱為b1的變數
        int b2=0;    //宣告名稱為b2的變數

        cout<< "Arithmetic progression\n";
        cout<< "a1: ";
        cin>> b;
        while(b<=0)  //迴圈  在b小於等於0的情況下
        {
            cout<< "Out of range!\n";
            cout<< "a1: ";
            cin>> b;
        }
        cout<< "d: ";
        cin>> b1;
        while(b1<=0)  //迴圈  在b1小於等於0的情況下
        {
            cout<< "Out of range!\n";
            cout<< "d: ";
            cin>> b1;
        }
        cout<< "n: ";
        cin>> b2;
        while(b2<=0)  //迴圈   在b2小於等於0的情況下
        {
            cout<< "Out of range!\n";
            cout<< "n: ";
            cin>> b2;
        }
        for(int bn=0,i=1; i<=b2; i++)  //for迴圈  宣告名稱為bn以及i的變數  條件為i小於等於b2
        {
            bn=b+b1*(i-1);
            if(i<b2)
                cout<< bn << ",";
            else
                cout<< bn;

        }
        break;

    }




    while(a==2)     //迴圈   在a等於2的情況下
    {
        int c=0;   //宣告名稱為c的變數
        int c1=0;  //宣告名稱為c1的變數
        int c2=0;  //宣告名稱為c2的變數

        cout<< "Geometric progression\n";
        cout<< "a1: ";
        cin>> c;
        while(c<=0)   //迴圈  條件為c小於等於0
        {
            cout<< "Out of range!\n";
            cout<< "a1: ";
            cin>> c;
        }
        cout<< "r: ";
        cin>> c1;
        while(c1<=0)  //迴圈   條件為c1小於等於0
        {
            cout<< "Out of range!\n";
            cout<< "r: ";
            cin>> c1;
        }
        cout<< "n: ";
        cin>> c2;
        while(c2<=0)  //迴圈  條件為c2小於等於0
        {
            cout<< "Out of range!\n";
            cout<< "n: ";
            cin>> c2;
        }
        for(int x=0, x1=c, i=1; i<=c2; i++)   //for迴圈  宣告名稱為x,x1以及i的變數  條件為i小於等於c2
        {
            if(i==1)
                x=x1;
            if(i>1)
                x=x1*c1;
            x1=x;
            if(i<c2)
                cout<< x << ",";
            else
                cout<< x;

        }
        break;
    }





    while(a==3)      //迴圈   在a等於3的情況下
    {
        int n1=0;    //宣告名稱為n1的變數

        cout<< "Fibonacci sequence\n";
        cout<< "n: ";
        cin>> n1;
        while(n1<=0)  //迴圈  條件為n1小於等於0
        {
            cout<< "Out of range!\n";
            cout<< "n: ";
            cin>> n1;
        }

        for(int fn=1,f1=1,f2=1,i=1; i<=n1; i++)     //for迴圈  宣告名稱為fn,f1,f2以及i的變數  條件為i小於等於n1
        {
            if(i==1)
                fn==f1;
            if(i==2)
                fn==f2;
            if(i>2)
            {
                fn=f1+f2;
                f1=f2;
                f2=fn;
            }
            if(i<n1)
                cout<< fn << ",";
            else
                cout<< fn;

        }
        break;


    }


    return 0;

}
