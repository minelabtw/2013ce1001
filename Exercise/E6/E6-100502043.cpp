#include<iostream>
#include<math.h>  //使用pow函數

using namespace std;

int main()
{
    int a1=0,d=0,r=0,n=0;
    int an=0;
    int mode=0;

    cout << "Calculate the progression." << "\nChoose the mode.";
    cout << "\nArithmetic progression=1.";
    cout << "\nGeometric progression=2.";
    cout << "\nFibonacci sequence=3.";
    cout << "\nExit=-1." << endl;

    while (mode!=1&&mode!=2&&mode!=3&&mode!=-1)  //重覆詢問至輸入正確的模式
    {
    cout << "Enter mode: ";
    cin >> mode;
    switch (mode)  //選擇模式
    {
    case 1:
        {
            cout << "Arithmetic progression" << endl;
            while (a1<1)  //重覆輸入至初項在合理範圍
            {
                cout << "a1: ";
                cin >> a1;
                if (a1<1)
                    cout << "Out of range!" << endl;
            }
            while (d<1)  //重覆輸入至公差在合理範圍
            {
                cout << "d: ";
                cin >> d;
                if (d<1)
                    cout << "Out of range!" << endl;
            }
            while (n<1)  //重覆輸入至項數在合理範圍
            {
                cout << "n: ";
                cin >> n;
                if (n<1)
                    cout << "Out of range!" << endl;
            }
            for (an=a1;an<=d*n;an+=d)  //輸出等差數列
            {
                cout << an << ",";
            }
            break;

        }

    case 2:
        {
            cout << "Geometric progression" << endl;
            while (a1<1)
            {
                cout << "a1: ";
                cin >> a1;
                if (a1<1)
                    cout << "Out of range!" << endl;
            }
            while (r<1)  ////重覆輸入至公比在合理範圍
            {
                cout << "r: ";
                cin >> r;
                if (r<1)
                    cout << "Out of range!" << endl;
            }
            while (n<1)
            {
                cout << "n: ";
                cin >> n;
                if (n<1)
                    cout << "Out of range!" << endl;
            }
            for (an=a1;an<a1*pow(r,n);an*=r)  //輸出等比數列
            {
                cout << an << ",";
            }
            break;
        }

    case 3:
        {
            cout << "Fibonacci progression" << endl;
            while (n<1)
            {
                cout << "n: ";
                cin >> n;
                if (n<1)
                    cout << "Out of range!" << endl;
            }
            int n1=1,n2=1;
            int n3,i;

            if (n==1)  //區別n=1或>1之做法
                cout << n1;
            else
            {
                cout << n1 << "," << n2 << ",";  //先輸出費氏數列前兩項
                for (i=0;i<n-2;i++)  //用i做費氏數列遞增
                {
                    n3 = n1 + n2;
                    n1 = n2;
                    n2 = n3;
                    cout << n3 << ",";
                }
            }
            break;
        }

    case -1:
        break;

    default:
        cout << "Illegal input!" << endl;
    }
    }

    return 0;
}
