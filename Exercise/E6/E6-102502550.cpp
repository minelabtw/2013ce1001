#include <iostream>

using namespace std;
int F(int);
int main()
{
    int m=0,a1=0,d=0,r=0,n=0,flag=0;

    cout<<"Calculate the progression.\n";
    cout<<"Choose the mode.\n";
    cout<<"Arithmetic progression=1.\n";
    cout<<"Geometric progression=2.\n";
    cout<<"Fibonacci sequence=3.\n";
    cout<<"Exit=-1.\n";

    do
    {
       cout<<"Enter mode: ";
       cin>>m;
       switch(m)
       {
          case 1:
               do
               {
                   cout<<"a1: ";
                   cin>>a1;
                   if(a1<=0)
                      cout<<"Illegal input!\n";
               }while(a1<=0);
               do
               {
                   cout<<"d: ";
                   cin>>d;
                   if(d<=0)
                      cout<<"Illegal input!\n";
               }while(d<=0);
               do
               {
                   cout<<"n: ";
                   cin>>n;
                   if(n<=0)
                      cout<<"Illegal input!\n";
               }while(n<=0);
               for(int i=1;i<=n;i++,a1=a1+d)
               {
                   if(i==n)
                     cout<<a1;
                   else
                     cout<<a1<<",";

               }
               cout<<endl;
               break;

          case 2:
                do
               {
                   cout<<"a1: ";
                   cin>>a1;
                   if(a1<=0)
                      cout<<"Illegal input!\n";
               }while(a1<=0);
               do
               {
                   cout<<"r: ";
                   cin>>r;
                   if(r<=0)
                      cout<<"Illegal input!\n";
               }while(r<=0);
               do
               {
                   cout<<"n: ";
                   cin>>n;
                   if(n<=0)
                      cout<<"Illegal input!\n";
               }while(n<=0);
               for(int i=1;i<=n;i++,a1=a1*r)
               {
                   if(i==n)
                     cout<<a1;
                   else
                     cout<<a1<<",";

               }
               cout<<endl;
               break;
          case 3:
               do
               {
                   cout<<"n: ";
                   cin>>n;
                   if(n<=0)
                      cout<<"Illegal input!\n";
               }while(n<=0);
               for(int i=1;i<=n;i++)
               {
                   if(i==n)
                     cout<<F(i);
                   else
                     cout<<F(i)<<",";
               }
               cout<<endl;
               break;
          case -1:
             flag=1;
             break;
          default:
             cout<<"Illegal input!\n";
             break;
       }

    }while(!flag);
    return 0;
}
int F(int n)
{
    if(n==1 || n==2)
    {
        return 1;
    }
    else
    {

        return F(n-1)+F(n-2);
    }
}
