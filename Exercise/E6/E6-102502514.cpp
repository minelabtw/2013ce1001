#include <iostream>
using namespace std;

int main()
{
    cout <<"Calculate the progression.\n";
    cout <<"Choose the mode.\n";
    cout <<"Arithmetic progression=1.\n";
    cout <<"Geometric progression=2.\n";
    cout <<"Fibonacci sequence=3.\n";
    cout <<"Exit=-1.\n";

    int mode;

    while (mode!=-1) //跑完下面的case後可以再次回到迴圈開頭"Enter mode: "
    {
        cout <<"Enter mode: ";
        cin >>mode;
        while (mode!=1&&mode!=2&&mode!=3&&mode!=-1) //mode值必須為-1,1,2,3,否則請求重新輸入
        {
            cout <<"Illegal input!\nEnter mode: ";
            cin >>mode;
        }

        switch (mode)
        {
        case 1:
        {
            int a1,d,n;
            cout <<"Arithmetic progression\na1: "; //等差數列
            cin >>a1;
            while (a1<=0)
            {
                cout <<"Out of range!\na1: ";
                cin >>a1;
            }
            cout <<"d: ";
            cin >>d;
            while (d<=0)
            {
                cout <<"Out of range!\nd: ";
                cin >>d;
            }
            cout <<"n: ";
            cin >>n;
            while (n<=0)
            {
                cout <<"Out of range!\nn: ";
                cin >>n;
            }
            int x=1;
            while (x<n)
            {
                cout <<a1+(x-1)*d<<","; //等差數列第n項公式,印出直到第n-1項
                x++;
            }
            cout <<a1+(n-1)*d; //另外單獨印出等差數列第n項,這項後面並不能加逗號
            break;
        }
        case 2:
        {
            int a1,d,n;
            cout <<"Geometric progression\na1: "; //等比數列
            cin >>a1;
            while (a1<=0)
            {
                cout <<"Out of range!\na1: ";
                cin >>a1;
            }
            cout <<"d: ";
            cin >>d;
            while (d<=0)
            {
                cout <<"Out of range!\nd: ";
                cin >>d;
            }
            cout <<"n: ";
            cin >>n;
            while (n<=0)
            {
                cout <<"Out of range!\nn: ";
                cin >>n;
            }

            int i=1,y=1,x=1;

            while (y<=n-1)
            {
                cout <<(a1*i)<<","; //先印出第一項,之後讓i乘以公比,再把i乘以第一項形成第二項,依此繼續印出到n-1項
                i=i*d;
                y++;
            }
            cout <<a1*i<<endl; //因為最後一項後面不能加逗號,所以獨立印出第n項
            break;
        }
        case 3:
        {
            int n=0;
            int i=1;
            int x=1;
            int y=1;
            int z=0;
            cout <<"Fibonacci sequence\nn: "; //費式數列
            cin >>n;
            while (n<=0)
            {
                cout <<"Out of range!\nn: ";
                cin >>n;
            }

            while (i<=n)
            {
                if (i==1) //i=1時只有第一項x
                    cout <<x;
                else if (i==2) //i=2時有兩項,因此追加印出第2項y
                    cout <<","<<y;
                else
                {
                    z=x+y; //i>2時第三項z=x+y,
                    x=y; //把第一項x往後挪1項,變成y
                    y=z; //把第二項y往後挪1項,變成z
                    cout <<","<<z; //最後讓他跑迴圈形成一個費式數列
                }
                i++;
            }
            cout <<endl;
            break;
        }
        case -1: //-1表示離開
            break;
        }
    }
    return 0;
}
