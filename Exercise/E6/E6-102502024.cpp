#include<iostream>
using namespace std;
int main()
{
    int mode=0,a1=1,n=1,d=1,x=1,ax=1,y=1,z=1,az1=1,az2=1,az=2;
    cout<<"Calculate the progression.\n";
    cout<<"Choose  the mode.\n";
    cout<<"Arithmetic progression=1.\n";
    cout<<"Geometric progression=2.\n";
    cout<<"Fibonacci sequence=3.\n";
    cout<<"Exit=-1.\n";
    while(mode!=-1)
    {
        cout<<"Enter mode: ";
        cin>>mode;
        switch(mode)
        {
        case 1 :
            cout<<"Arithmetic progression\n";
            cout<<"a1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!\n";
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            while(d<=0)
            {
                cout<<"Out of range!\n";
                cout<<"d: ";
                cin>>d;
            }
            cout<<"n; ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!\n";
                cout<<"n: ";
                cin>>n;
            }
            while(x<=n)
            {
                ax=a1+(x-1)*d;
                cout<<ax<<",";
                x++;
            }
            cout<<endl;
            break;
        case 2 :
            cout<<"Geometric progression\n";
            cout<<"a1: ";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!\n";
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            while(d<=0)
            {
                cout<<"Out of range!\n";
                cout<<"d: ";
                cin>>d;
            }
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!\n";
                cout<<"n: ";
                cin>>n;
            }
            while(y<=n)
            {
                cout<<a1;
                if(y<n)
                {
                    cout<<",";
                }
                a1=a1*d;
                y++;
            }
            cout<<endl;
            break;
        case 3 :
            cout<<"Fibonacci sequence\n";
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!\n";
                cout<<"n: ";
                cin>>n;
            }
            while(z<=n)
            {

                if(z==1)
                {
                    az=az1;
                }
                if(z==2)
                {
                    az=az2;
                }
                if(z>2)
                {
                    az=az1+az2;
                    az1=az2;
                    az2=az;
                }
                cout<<az;
                if(z<n)
                {
                    cout<<",";
                }
                z++;
            }
            cout<<endl;
            break;
        default:
            cout<<"Illegal input!";
            break;
        }
    }
    return 0;
}
