#include <iostream>

using namespace std;

int main()
{
    int x;                                                           //宣告變數
    cout<<"Calculate the progression."<<endl;                        //顯示文字
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;
    while(1)                                                         //進入迴圈
    {
        cout<<"Enter mode: ";
        cin>>x;
        while(x!=-1&&x!=1&&x!=2&&x!=3)                               //限制模式數字
        {
            cout<<"Illegal input!"<<endl;
            cout<<"Enter mode: ";
            cin>>x;
        }
        if(x==1)                                                     //模式一
        {
            int a1;
            int d;
            int n;
            cout<<"Calculate the progression."<<endl;
            cout<<"a1: ";
            cin>>a1;
            cout<<"d: ";
            cin>>d;
            cout<<"n: ";
            cin>>n;
            int i;
            for(i=1; i<=n; i++)                                     //進入迴圈
            {
                if(i!=n)
                {
                    cout<<a1+(i-1)*d<<",";                          //計算值
                }
                else if(i==n)
                {
                    cout<<a1+(i-1)*d;
                }

            }
            cout<<endl;
        }


        if(x==2)                                                    //情況2
        {
            int a1;
            int r;
            int n;
            cout<<"Geometric progression."<<endl;
            cout<<"a1: ";
            cin>>a1;
            cout<<"r: ";
            cin>>r;
            cout<<"n: ";
            cin>>n;
            int i=1;
            int j=a1;
            while(i<=n)                                            //進入迴圈
            {
                if(i<n)
                {
                    cout<<j<<",";
                    j=j*r;                                         //進算數字
                }
                else
                {
                    cout<<j;
                }
                i++;
            }
            cout<<endl;
        }

        if(x==3)
        {
            int n;
            cout<<"Fibonacci sequence"<<endl;
            cout<<"n: ";
            cin>>n;
            int i;
            int j=1;
            int k=0;
            cout<<"1";
            for(i=2; i<=n; i++)                                   //進入迴圈
            {
                if(j>k)                                           //判斷數字大小
                {
                    k=j+k;
                    cout<<","<<j;                                 //顯示大的數字小的再拿去做運算
                }
                else
                {
                    j=j+k;                                        //判斷數字大小
                    cout<<","<<k;                                 //顯示大的數字小的再拿去做運算
                }
            }
        cout<<endl;
        }

        if(x==-1)                                                 //跳出回全條件
        {
            break;
        }
    }
    return 0;
}
