#include <iostream>

using namespace std;

int main()
{
    int a1=0,d=0,n1,a2=0,r=0,n2=0,n3=0,x=0,i=0;                    //宣告變數

    cout <<"Calculate the progression.\n";                         //顯示字元
    cout <<"Choose the mode.\n";
    cout <<"Arithmetic progression=1.\n";
    cout <<"Geometric progression=2.\n";
    cout <<"Fibonacci sequence=3.\n";
    cout <<"Exit=-1.\n";
    cout <<"Enter mode: ";
    cin >>x;
    while (x!=-1 && x!=1 &&x!=2 && x!=3)                          //while用來判斷輸入的數字是否可以執行動作
    {
        cout <<"Illegal input\n"<<"Enter mode: ";
        cin >>x;
    }
    while (x!=-1)                                                 //判斷是否繼續執行
    {
        switch ( x )                                              //執行不同動作
        {
        case 1:
        {
            cout <<"Arithmetic progression\n"<<"a1: ";
            cin >>a1;
            while (a1<0)
            {
                cout <<"Out of range!"<<endl<<"a1: ";
                cin >>a1;
            }
            cout <<"d: ";
            cin >>d;
            while (d<0)
            {
                cout <<"Out of range!"<<endl<<"d: ";
                cin >>d;
            }
            cout <<"n: ";
            cin >>n1;
            while (n1<0)
            {
                cout <<"Out of range!"<<endl<<"n: ";
                cin >>n1;
            }
            cout <<a1<<",";
            for(int number=1; number<=n1-2; number=number+1)
            {
                a1=a1+d;
                cout <<a1<<",";
            }
            cout <<a1+d<<endl;
            x=0;
            cout <<"Enter mode: ";
            cin >>x;
            while (x!=-1 && x!=1 &&x!=2 && x!=3)
            {
                cout <<"Illegal input\n"<<"Enter mode: ";
                cin >>x;
            }
        }
        break;
        case 2:
        {
            cout <<"Geometric progression\n"<<"a1: ";
            cin >>a2;
            while (a2<0)
            {
                cout <<"Out of range!"<<endl<<"a1: ";
                cin >>a2;
            }
            cout <<"r: ";
            cin >>r;
            while (r<0)
            {
                cout <<"Out of range!"<<endl<<"r: ";
                cin >>r;
            }
            cout <<"n: ";
            cin >>n2;
            while (n2<0)
            {
                cout <<"Out of range!"<<endl<<"n: ";
                cin >>n2;
            }
            cout <<a2<<",";
            for ( int number=1; number<=n2-2; number=number+1)
            {
                a2=a2*r;
                cout <<a2<<",";
            }
            cout <<a2*r<<endl;
            x=0;
            cout <<"Enter mode: ";
            cin >>x;
            while (x!=-1 && x!=1 &&x!=2 && x!=3)
            {
                cout <<"Illegal input\n"<<"Enter mode: ";
                cin >>x;
            }
        }
        break;
        case 3:
        {
            cout <<"Fibonacci sequence\n"<<"n: ";
            cin >>n3;
            while (n3<0)
            {
                cout <<"Out of range!"<<endl<<" n: ";
                cin >>n3;
            }
            int f1=1,f2=1;//第1,2項
            int fn=0;

            i=1;
            while(i<=n3-1)
            {
                if(i==1)//第1項
                    fn=f1;
                if(i==2)//第2項
                    fn=f2;
                if(i>2)//第3項後
                {
                    fn=f1+f2;//前2項相加
                    f1=f2;//進位
                    f2=fn;//進位
                }
                cout<<fn<<",";
                i++;
            }
            cout <<fn+f1<<endl<<"Enter mode: ";
            x=0;
            cin >>x;
            while (x!=-1 && x!=1 &&x!=2 && x!=3)
            {
                cout <<"Illegal input\n"<<"Enter mode: ";
                cin >>x;
            }
        }
        break;
        }

    }
    return 0;
}
