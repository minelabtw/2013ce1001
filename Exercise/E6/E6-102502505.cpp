#include <iostream>
#include <iomanip>

using namespace std;

int mode;//模式
int a1;//首項
int d;//公差
int n;//項數
int p=1;//費氏定理首項
int q=1;//費氏定理第二項
int r;//費氏定理第三項

int main()

{
    cout << "Calculate the progression." << endl;//輸出字串
    cout << "Choose the mode." << endl;
    cout << "Arithmetic progression=1." << endl;
    cout << "Geometric progression=2." << endl;
    cout << "Fibonacci sequence=3." << endl;
    cout << "Exit=-1." << endl;
    cout << "Enter mode: ";
    cin >> mode;//輸入模式

    while ( mode !=-1 )//設迴圈，使模式在等於-1以外皆不結束
    {
        while ( mode!=1 && mode!=2 && mode!=3 && mode!=-1 )//設迴圈，使模式條件符合
        {
            cout << "Illegal input!" << endl;//輸出字串
            cout << "Enter mode: ";
            cin >> mode;//重新輸入模式
        }

        switch (mode)//設定多重選擇
        {
        case 1://當模式=1時，輸出下列字串
            cout << "Arithmetic progression" << endl;
            cout << "a1: ";
            cin >> a1;
            while ( a1 <= 0 )//設迴圈使辯輸符合條件
            {
                cout << "Out of range!" <<endl;
                cout << "a1: ";
                cin >> a1;
            }

            cout << "d: ";
            cin >> d;
            while ( d <= 0)
            {
                cout << "Out of range!" <<endl;
                cout << "d: ";
                cin >> d;
            }

            cout << "n: ";
            cin >> n;
            while ( n <= 0)
            {
                cout << "Out of range!" <<endl;
                cout << "n: ";
                cin >> n;
            }

            cout << a1 << ",";
            for ( int x=0; x < n-2; x++ )//使用for迴圈，輸出等差數列直至倒數第二項
            {
                a1 = a1 + d;
                cout << a1 << ",";
            }
            cout << a1+d;//輸出最後一項
            break;
        case 2://當模式=2時，輸出下列字串
            cout << "Geometric progression" << endl;
            cout << "a1: ";
            cin >> a1;
            while ( a1 <= 0 )
            {
                cout << "Out of range!" <<endl;
                cout << "a1: ";
                cin >> a1;
            }

            cout << "r: ";
            cin >> d;
            while ( d <= 0)
            {
                cout << "Out of range!" <<endl;
                cout << "r: ";
                cin >> d;
            }

            cout << "n: ";
            cin >> n;
            while ( n <= 0)
            {
                cout << "Out of range!" <<endl;
                cout << "n: ";
                cin >> n;
            }

            cout << a1 << ",";
            for ( int x=1; x <= n-2; x++ )//使用for迴圈，輸出等比數列
            {
                a1 = a1*d;
                cout << a1 << ",";
            }
            a1 = a1*d^(n-1);//輸出最後一項
            cout << a1;
            break;
        case 3://模式為三時輸出下列字串
            cout << "Fibonacci sequence" << endl;
            cout << "n: ";
            cin >> n;
            while ( n <= 0)
            {
                cout << "Out of range!" <<endl;
                cout << "n: ";
                cin >> n;
            }

            cout << p << ",";//輸出第一項
            for ( int x=1; x <= n-2; x++ )//設定for迴圈
            {
                r = p+q;//第三項
                p = q;//第一項向下項推
                q = r;//第二項向第三項推
                cout << p << ",";//輸出一項為代表
            }
            cout << q;//輸出最後一項，不帶逗點
            break;
        case -1://當模式為-1時，不做任何事
            break;
        }
        cout << endl << "Enter mode: ";//結束一次計算後，再次跑出此字串，使程式可重複運算，直至模式=-1
        cin >> mode;
    }

    return 0;//返回初始值

}
