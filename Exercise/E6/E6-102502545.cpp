#include <iostream>
using namespace std;

int main()
{
    int a=0;                                                //宣告一變數a
    cout<<"Calculate the progression.\n";
    cout<<"Choose the mode.\n";
    cout<<"Arithmetic progression=1.\n";
    cout<<"Geometric progression=2.\n";
    cout<<"Fibonacci sequence=3.\n";
    cout<<"Exit=-1.\n";
    while(a!=-1)                                             //設置一迴圈來判定a不是-1的情況下
    {
        cout<<"Enter mode: ";
        cin>>a;
        while(a!=1&&a!=2&&a!=3&&a!=-1)                       //設置另一迴圈來判定a的範圍
        {
            cout<<"Illegal input!\n";
            cout<<"Enter mode: ";
            cin>>a;
        }
        switch(a)                                            //設置一迴圈來判定要執行何種程式
        {
        case 1:
        {
            int b,c,d;
            int z=0;
            cout<<"Arithmetic progression\n";
            cout<<"a1: ";
            cin>>b;
            while(b<1)                                       //設置迴圈來判定b的範圍
            {
                cout<<"Out of range!\n";
                cout<<"a1: ";
                cin>>b;
            }
            cout<<"d: ";
            cin>>c;
            while(c<1)                                         //設置迴圈來判定c的範圍
            {
                cout<<"Out of range!\n";
                cout<<"d ";
                cin>>c;
            }
            cout<<"n: ";
            cin>>d;
            while(d<1)                                           //設置迴圈來判定d的範圍
            {
                cout<<"Out of range!\n";
                cout<<"n: ";
                cin>>d;
            }
            while(z<=d-2)                                        //設置迴圈使其進行運算
            {
                cout<<b+z*c;
                cout<<",";
                z++;
            }
            cout<<b+(d-1)*c;
            cout<<endl;
            break;
        }
        case 2:
        {
            int e,f,g;
            cout<<"Geometric progression\n";
            cout<<"a1: ";
            cin>>e;
            while(e<1)                                           //設置迴圈來判定e的範圍
            {
                cout<<"Out of range!\n";
                cout<<"a1: ";
                cin>>e;
            }
            cout<<"r: ";
            cin>>f;
            while(f<1)                                            //設置迴圈來判定f的範圍
            {
                cout<<"Out of range!\n";
                cout<<"r: ";
                cin>>f;
            }
            cout<<"n: ";
            cin>>g;
            while(g<1)                                               //設置迴圈來判定g的範圍
            {
                cout<<"Out of range!\n";
                cout<<"n: ";
                cin>>g;
            }
            cout<<e;
            for(int y=0; y<g-1; y++)                                //設置迴圈使其進行運算
            {
                e=e*f;
                cout<<","<<e;
            }
            cout<<endl;
            break;
        }
        case 3:
        {
            int h;
            int x=2;
            cout<<"Fibonacci sequence\n";
            cout<<"n:";
            cin>>h;
            while(h<1)                                           //設置迴圈來判定h的範圍
            {
                cout<<"Out of range!\n";
                cout<<"n:";
                cin>>h;
            }
            int A=1,B=1;
            int S=1;
            cout<<S;
            while(x<=h)                                           //設置迴圈使其進行運算
            {
                if(x==2)
                    S=B;
                if(x>2)
                {
                    S=A+B;
                    A=B;
                    B=S;
                }
                cout<<","<<S;
                x++;

            }
            cout<<endl;
            break;
        }
        default:
            break;
        }

    }


    return 0;
}
