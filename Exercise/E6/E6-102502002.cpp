#include <iostream>
#include<cmath>
using namespace std;

int main()
{
    int mode=0;          //型態。
    int a1=0;            //首項。
    int d=0;             //公差。
    int r=0;             //公筆。
    int n=0;             //項數。
    int A=0;
    int A1=0;
    int A2=0;
    int F=0;
    int n1=0;
    int n2=1;

    cout << "Calculate the progression.\nChoose the mode.\nArithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.\n";  //輸出字串。
    while (mode!=-1)                //mode不等於-1
    {
        cout << "Enter mode: ";
        cin >> mode;                //輸入mode
        switch (mode)
        {
        case 1:
            cout << "Arithmetic progression" << endl << "a1: ";     //輸出字串
            cin >> a1;                                              //輸入a1
            for (int A1=a1; A1<=0;)                    //a1為正整數，否則輸出illegal input
            {
                cout << "Illegal input!" << endl << "a1: ";
                cin >> a1;
            }
            cout << "d: ";
            cin >> d;
            for (int D=d; D<=0;)                       //d為正整數，否則輸出illegal input
            {
                cout << "Illegal input!" << endl << "d :";
                cin >> d;
            }
            cout << "n: ";
            cin >> n;
            for (int N=n; N<=0;)                       //n為正整數，否則輸出illegal input
            {
                cout << "Illegal input!" << endl << "n :";
                cin >> n;
            }
            for(int A2=a1; A2<=(a1+d*(n-2)); A2+=d )       //對於A從a1到an，A加d。
                cout << A2 << ",";
            cout << a1+d*(n-1) << endl;                    //輸出數列
            break;

        case 2:
            cout << "Geometric progression" << endl << "a1: ";       //等差數列
            cin >> a1;                                               //輸入a1
            for (int A1=a1; A1<=0;)
            {                                                        //a1,r,n皆須為正整數，否則輸出illegal input
                cout << "Illegal input!" << endl << "a1: ";
                cin >> a1;
            }
            cout << "r: ";
            cin >> r;
            for (int R=r; r<=0;)
            {
                cout << "Illegal input!" << endl << "r :";
                cin >> r;
            }
            cout << "n: ";
            cin >> n;
            for (int N1=n; N1<=0;)
            {
                cout << "Illegal input!" << endl << "n :";
                cin >> n;
            }
            for (int N2=1; N2<n; N2++)                    //輸出等比數列
            {
                A2=(a1*pow(r,(N2-1)));
                cout << A2 << ",";
            }
            A=a1*pow(r,(n-1));
            cout << A << endl;
            break;

        case 3:
            cout << "Fibonacci sequence" << endl << "n: ";          //輸出字串
            cin >> n;
            for (int N1=n; N1<=0;)                                  //n需為正整數
            {
                cout << "Illegal input!" << endl << "n :";
                cin >> n;
            }
            for(int N2=1; N2<n; N2++)
            {
                if (N2<=1)
                    F=N2;
                else                                     //費式數列
                {
                    F=n1+n2;
                    n1=n2;
                    n2=F;
                }
                cout << F << ",";
            }
            cout << F+n2;
            cout << endl;
            break;

        default:                                       //其餘輸入，接輸出illegal input
            cout << "Illegal input!" << endl << "Enter mode: ";
            cin >> mode;
            break;
        }
    }
    return 0;
}
