#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    cout << "Calculate the progression.\n" ;
    cout << "Choose the mode.\n" ;
    cout << "Arithmetic progression=1.\n" ;
    cout << "Geometric progression=2.\n" ;
    cout << "Fibonacci sequence=3.\n" ;
    cout << "Exit=-1.\n" ;

    int mode=0;

    while(mode!=-1)
    {
        int a1=0,d=0,n=0,r=0,i=1;
        int f1=1,f2=1,fn=0;
        cout << "Enter mode: ";
        cin >> mode;
        switch(mode)
        {
        case 1:
            cout << "Arithmetic progression\n";
            while(a1<=0)
            {
                cout << "a1: ";
                cin >> a1;
                if(a1<=0)
                    cout << "Out of range!\n";
            }
            while(d<=0)
            {
                cout << "d: ";
                cin >> d;
                if(d<=0)
                    cout << "Out of range!\n";
            }
            while(n<=0)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                    cout << "Out of range!\n";
            }
            while(i<=n)
            {
                cout << a1+(i-1)*d;
                if(i!=n)
                    cout << ",";
                i++;
            }
            cout << "\n";
            break;
        case 2:
            cout << "Geometric progression\n";
            while(a1<=0)
            {
                cout << "a1: ";
                cin >> a1;
                if(a1<=0)
                    cout << "Out of range!\n";
            }
            while(r<=0)
            {
                cout << "r: ";
                cin >> r;
                if(r<=0)
                    cout << "Out of range!\n";
            }
            while(n<=0)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                    cout << "Out of range!\n";
            }
            while(i<=n)
            {
                cout << a1*pow(r,i-1);
                if(i!=n)
                    cout << ",";
                i++;
            }
            cout << "\n";
            break;
        case 3:
            cout << "Fibonacci sequence\n";
            while(n<=0)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                    cout << "Out of range!\n";
            }
            while(i<=n)
            {
                if(i==1)
                    fn=f1;
                if(i==2)
                    fn=f2;
                if(i>2)
                {
                    fn=f1+f2;
                    f1=f2;
                    f2=fn;
                }
                cout << fn;
                if(i!=n)
                    cout << ",";
                i++;
            }
            cout << "\n";
            break;
        case -1:
            break;
        default:
            cout << "Illegal input!\n";
            break;

        }
    }

    return 0;

}
