#include<iostream>
using namespace std;

int main()
{
    cout << "Calculate the progression.\nChoose the mode.\nArithmetic progression=1.\nGeometric progression=2.\nFibonacci sequence=3.\nExit=-1.\n";
    cout << "Enter mode: ";
    int mode;
    cin >> mode;
    while(mode != -1)   //-1時跳出循環
    {
        switch(mode)
        {
        case 1:        //輸入1時,計算等差數列
        {
            int a1=0,d=0,n1=0;
            cout << "a1: ";
            cin >> a1;
            while(a1<=0)
            {
                cout << "Illegal input!"<< endl << "a1: ";
                cin >> a1;
            }
            cout << "d: ";
            cin >> d;
            while(d<=0)
            {
                cout << "Illegal input!"<< endl << "d: ";
                cin >> d;
            }
            cout << "n: ";
            cin >> n1;
            while(n1<=0)
            {
                cout << "Illegal input!"<< endl << "n: ";
                cin >> n1;
            }
            int i;
            for(i=1; i<n1; i++)
            {
                cout << a1<<",";
                a1 = a1 + d;
            }
            cout << a1 << endl <<"Enter mode: ";
            cin >> mode;
        }
        break;
        case 2:             //輸入2時,計算等比數列
        {
            int a2=0,r=0,n2=0;
            cout << "a1: ";
            cin >> a2;
            while(a2<=0)
            {
                cout << "Illegal input!"<< endl << "a1: ";
                cin >> a2;

            }
            cout << "r: ";
            cin >> r;
            while(r<=0)
            {
                cout << "Illegal input!"<< endl << "r: ";
                cin >> r;

            }
            cout << "n: ";
            cin >> n2;
            while(n2<=0)
            {
                cout << "Illegal input!"<< endl << "n: ";
                cin >> n2;

            }
            int j;
            for(j=1; j<n2; j++)
            {
                cout << a2<<",";
                a2 = a2 * r;
            }
            cout <<a2<< endl <<"Enter mode: ";
            cin >> mode;
        }
        break;
        case 3:                   //輸入3時,計算費式數列
        {
            int a =0,b =1,n3=0,k;
            cout << "n: ";
            cin >> n3;
            while(n3<=0)
            {
                cout << "Illegal input!"<< endl << "n: ";
                cin >> n3;
            }
            for(k=1; k<n3; k++)
            {
                if(a<=b)
                {
                    cout << b << ",";
                    a = a + b;
                }
                else
                {
                    cout << a << ",";
                    b=b+a;
                }
            }
            if(a<=b)
                cout << b;
            else
                cout << a;
            cout << endl <<"Enter mode: ";
            cin >> mode;
        }
        break;
        case -1:
        {
        }
        break;
        default:
        {
            cout << "Illegal input!" << endl;   //輸入其他,顯示Illegal input!
        }
        break;
        }
    }
    return 0;
}
