#include <iostream>

using namespace std;

int main(){
    int enter = 0;  //變數輸入
    int a1;  //變數首項
    int d;  //變數公差
    int r;  //變數公比
    int n;  //變數項數
    int goon = 1;  //變數判定是否繼續
    int an1 = 1;  //變數費式數列N的前一項
    int an2 = 1;  //變數費式數列N的前二項
    int an = 0;  //變數第N項
    int gmax;  //變數公比最大項
    cout << "Calculate the progression." << endl;  //輸出計算
    cout << "Choose the mode." << endl;  //輸出選擇模式
    cout << "Arithmetic progression=1." << endl;  //輸出等差數列選一
    cout << "Geometric progression=2." <<endl;  //輸出等比數列選二
    cout << "Fibonacci sequence=3." << endl;  //輸出費式數列選三
    cout << "Exit=-1." << endl;  //輸出離開選負一
    while(goon != -1){  //判斷是否繼續執行
        cout << "Enter mode: ";  //請使用者輸入模式
        cin >> enter;  //輸入模式

        switch(enter){  //判斷為何模式
            case 1:  //等差模式
                cout << "Arithmetic progression" << endl;

                do{  //輸入首項並判斷有無超過範圍
                    cout << "a1: ";
                    cin >> a1;
                    if(a1 <= 0){
                        cout << "Out of range!";
                    }
                }while(a1 <= 0);
                do{  //輸入公差並判斷有無超過範圍
                    cout << "d: ";
                    cin >> d;
                    if(d <= 0){
                        cout << "Out of range!";
                    }
                }while(d <= 0);
                do{  //輸入項數並判斷有無超過範圍
                    cout << "n: ";
                    cin >> n;
                    if(n <= 0){
                        cout << "Out of range!";
                    }
                }while(n <= 0);

                for(int i = a1 ; i <= a1 + d * (n - 1) ; i += d ){  //輸出數列
                    cout << i;
                    if(i != a1 + d * (n - 1)){
                        cout << ",";
                    }
                }
                cout << endl;
                break;
            case 2:
                cout << "Geometric progression" << endl;

                do{  //輸入首項並判斷有無超過範圍
                    cout << "a1: ";
                    cin >> a1;
                    if(a1 <= 0){
                        cout << "Out of range!";
                    }
                }while(a1 <= 0);
                do{  //輸入公比並判斷有無超過範圍
                    cout << "r: ";
                    cin >> r;
                    if (r <= 0){
                        cout << "Out of range!";
                    }
                }while(r <= 0);
                do{  //輸入項數並判斷有無超過範圍
                    cout << "n: ";
                    cin >> n;
                    if(n <= 0){
                      cout << "Out of range!";
                    }
                }while(n <= 0);
                gmax = a1;
                for(int i = 1 ; i < n ; i++){  //找出數列最大項
                    gmax = gmax * r;
                }
                for(int i = a1 ; i <= gmax ; i = i * r){  //輸出數列
                    cout << i;
                    if(i != gmax){
                        cout << ",";
                    }
                }
                cout << endl;
                break;
            case 3:
                cout << "Fibonacci sequence" << endl;

                do{  //輸入項數並判斷有無超過範圍
                    cout << "n: ";
                    cin >> n;
                    if(n <= 0){
                        cout << "Out of range!";
                    }
                }while(n <= 0);
                int i;
                if(n <= 2){  //判斷是否小於二
                    if(n == 1){  //輸出數列
                        cout << an1;
                    }else{
                        cout << an1 << "," << an2;
                    }
                }else{  //輸出大於二的數列
                    cout << an1 << "," << an2 << ",";
                    for(i = 3 ; i <= n ; i++){
                        an = an1 + an2;
                        cout << an;
                        an2 = an1;
                        an1 = an;
                        if(i != n){
                            cout << ",";
                        }
                    }
                    an1 = 1;  //歸一
                    an2 = 1;  //歸一
                }
                cout << endl;
                break;
            case -1:  //跳出迴圈
                goon = -1;
                break;
            default:  //輸出不合理的輸入
                cout << "Illegal input!" << endl;
                break;
        }
    }
    return 0;
}
