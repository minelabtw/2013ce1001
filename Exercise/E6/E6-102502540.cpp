#include <iostream>
using namespace std;
int main()
{
    int mode = 0; //宣告變數其初始值為0
    int al = 0;
    int d = 0;
    int n = 0;
    int r = 0;
    int k = 1; //宣告變數其初始值為1
    int a = 0;
    int b = 1;
    int i = 0;

    cout << "Calculate the progression." << endl << "Choose the mode." << endl << "Arithmetic progression=1." << endl << "Geometric progression=2." << endl << "Fibonacci sequence=3." << endl << "Exit=-1." << endl; //顯示

    cout << "Enter mode: "; //顯示Enter mode:
    cin >> mode; //輸入值給mode

    while ( mode != -1 )  //當mode不等於1時進入以下迴圈
    {
        switch (mode)
        {
        case 1 : //等差數列
            cout << "Arithmetic progression" << endl << "a1: ";
            cin >> al;
            while ( al <= 0 )
            {
                cout << "Out of range!" << endl << "al: ";
                cin >> al;
            }
            cout << "d: ";
            cin >> d;
            while ( d <= 0 )
            {
                cout << "Out of range!" << endl << "d: ";
                cin >> d;
            }
            cout << "n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout << "Out of range!" << endl << "n: ";
                cin >> n;
            }

            cout << al;
            while ( k < n )
            {
                cout << ",";
                al = al + d;
                cout << al;
                k++;
            }
            k = 1; //初始k值
            cout << endl << "Enter mode: ";
            cin >> mode;
            break; //跳出

        case 2 : //等比數列
            cout << "Geometric progression" << endl << "al: ";
            cin >> al;
            while ( al <= 0 )
            {
                cout << "Out of range!" << endl << "al: ";
                cin >> al;
            }

            cout << "r: ";
            cin >> r;
            while ( r <= 0 )
            {
                cout << "Out of range!" << endl << "r: ";
                cin >> r;
            }

            cout << "n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout << "Out of range!" << endl << "n: ";
                cin >> n;
            }

            cout << al;
            while ( k < n )
            {
                cout << ",";
                al = al*r;
                cout << al;
                k++;
            }
            k = 1; //初始k值
            cout << endl << "Enter mode: ";
            cin >> mode;
            break; //跳出
        case 3 : //費氏數列
            cout << "Fibonacci sequence" <<endl<<"n: ";
            cin >> n;
            while ( n <= 0 )
            {
                cout<< "Out of range!" << endl << "n: ";
                cin >> n;
            }

            cout << 1;
            while ( k < n ) //輸出費氏數列
            {
                cout << ",";
                i = a + b;
                a = b;
                b = i;
                cout << i;
                k++; //使k值加1
            }
            k = 1; //初始k值
            cout << endl << "Enter mode: ";
            cin >> mode;
            break; //跳出
        case -1 :
            return 0; //結束

        default : //輸入其他
            cout << "Illegal input!" << endl << "Enter mode: "; //顯示
            cin >> mode;
        }
    }
    return 0;
}
