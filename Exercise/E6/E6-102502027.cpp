#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int a,a1,d,n,r;//宣告a,a1,d,n,r為整數
    int A1 =1;
    int A2 =1;
    int fn=1;
    cout<<"Calculate the progression."<<endl<<"Choose the mode."<<endl;//輸出Calculate the progression.
    cout<<"Arithmetic progression=1."<<endl<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl<<"Exit=-1."<<endl<<"Enter mode: ";

    while(cin>>a && a!=-1)//輸入a,當a=-1時跳出
    {
        switch(a)//switch statement nested in while
        {
        case 1: //當a=1
            cout<<"Arithmetic progression"<<endl<<"a1: ";
            cin>>a1;
            while(a1<=0) //當a1<=0
            {
                cout<<"Out of range!"<<endl<<"a1: ";
                cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            while(d<=0)
            {
                cout<<"Out of range!"<<endl<<"d: ";
                cin>>d;
            }
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }
            for(int i=1; i<=n-1; i++)//宣告i為整數且初始值為1<且i持續+1直到n-1
            {
                cout<<a1+(i-1)*d<<",";
            }
            cout<<a1+(n-1)*d<<endl;
            break; //跳出switch
        case 2:
            cout<<"Geometric progression"<<endl<<"a1 :";
            cin>>a1;
            while(a1<=0)
            {
                cout<<"Out of range!"<<endl<<"a1: ";
                cin>>a1;
            }
            cout<<"r: ";
            cin>>r;
            while(d<=0)
            {
                cout<<"Out of range!"<<endl<<"r: ";
                cin>>r;
            }
            cout<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }
            for(int j=1; j<=n-1; j++)
            {
                cout<<a1*pow(r,j)<<","; //r的j次方
            }
            cout<<a1*pow(r,n)<<endl;
            break;
        case 3:
            cout<<"Fibonacci sequence"<<endl<<"n: ";
            cin>>n;
            while(n<=0)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin>>n;
            }

            for(int k=2; k<=n; k++) //費氏數列
            {
                if (k==2)
                    cout<<"1";
                if(k>2)
                    fn =A1+A2;
                    A1=A2;
                    A2=fn;
                cout<<","<<fn;
            }
            cout<<endl;
            break;
        default://捕捉其他的元字
            cout<<"Illegal input!"<<endl;
            break;
        }
        cout<<"Enter mode: ";
    }
    return 0;
}
