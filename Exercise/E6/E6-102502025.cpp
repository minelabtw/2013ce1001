#include <iostream>
#include <iomanip>//用於setw.
using namespace std;

int main()
{
    int k=0;//令1/2/3/-1的選項變數整數。

    cout << "Calculate the program.\n";
    cout << "Choose the mode.\n";
    cout << "Arithmetic progression=1.\n";
    cout << "Geometric progression=2.\n";
    cout << "Fibonacci sequence=3.\n";
    cout << "Exit=-1.\n";

    cout << "Enter mode: ";
    cin >> k;

    while(k!=-1)//不是-1的話。
    {
        int a1=0;//首項
        int d=0;//等差/等比。
        int n=0;//幾項。
        int an=0;//答案
        int l=0;//迴圈值
        int next=0;//費氏多項式答案變數整數。
        int first=0;//費氏多項式的第一個變數整數。
        int second=1;//費氏多項式的第二個變數整數。


        switch(k)//選項
        {

        case 1://假如輸入1
            cout << "Arithmetic progression\n";
            cout << "a1: ";
            cin >> a1;

            while (a1<=0)
            {
                cout << "Out of range!\n";
                cout << "a1: ";
                cin >> a1;
            }

            cout << "d: ";
            cin >> d;

            while (d<=0)
            {
                cout << "Out of range!\n";
                cout << "d: ";
                cin >> d;
            }

            cout << "n: ";
            cin >> n;

            while (n<=0)
            {
                cout << "Out of range!\n";
                cout << "n: ";
                cin >> n;
            }

            while (l<n)
            {
                an=a1+d*l;//等差多項數公式
                cout << an;
                if(l==n-1)
                {
                    cout << endl;
                }
                else
                {
                    cout << ",";
                }
                l++;
            }

            break;

        case 2://選項2
            cout << "Geometric progression\n";
            while (a1<=0)
            {
                cout << "a1: ";
                cin >> a1;
                if (a1<=0)
                {
                    cout << "Out of range!\n";
                }
            }

            while (d<=0)
            {
                cout << "d: ";
                cin >> d;
                if (d<=0)
                {
                    cout << "Out of range!\n";
                }
            }

            while (n<=0)
            {
                cout << "n: ";
                cin >> n;
                if (n<=0)
                {
                    cout << "Out of range!\n";
                }
            }

            cout << a1 << ",";
            l=1;
            while (l<n)
            {
                an=a1*d;//等比多項式公式
                cout << an;
                a1=an;
                if(l==n-1)
                {
                    cout << endl;
                }
                else
                {
                    cout << ",";
                }
                l++;
            }

            break;

        case 3:
            cout << "Fibonacci sequence\n";

            while (n<=0)
            {
                cout << "n: ";
                cin >> n;
                if (n<=0)
                {
                    cout << "Out of range!\n";
                }
            }

            while (l<n)
            {
                next=first+second;//費氏多項式的計算方式
                cout << next;
                second=first;
                first=next;
                if(l==n-1)
                {
                    cout << endl;
                }
                else
                {
                    cout << ",";
                }
                l++;
            }

            break;

        default://假如不是1/2/3/-1的話
            cout << "Illegal input!" << endl;
        }

        cout << "Enter mode: ";
        cin >> k;


    }
}
