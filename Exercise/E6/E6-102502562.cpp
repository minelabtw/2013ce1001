#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int x=0,i=1;                                     //宣告型別為整數的x,i 並初始化其值為0和1
    int a=0,d=0,r=0,n=0;                             //宣告型別為整數的a,d,r,n 並初始化其值為0
    int b1=1,b2=1,b3=1;                              //宣告型別為整數的b1,b2,b3 並初始化其值為1

    cout << "Calculate the progression.\n";          //輸出"Calculate the progression.\n"到螢幕上
    cout << "Choose the mode.\n";                    //輸出"Choose the mode.\n"到螢幕上
    cout << "Arithmetic progression=1.\n";           //輸出"Arithmetic progression=1.\n"到螢幕上
    cout << "Geometric progression=2.\n";            //輸出"Geometric progression=2.\n"到螢幕上
    cout << "Fibonacci sequence=3.\n";               //輸出"Fibonacci sequence=3.\n"到螢幕上
    cout << "Exit=-1.\n";                            //輸出"Exit=-1.\n"到螢幕上

    while(x != -1)                                   //如果x不等於-1就進入迴圈
    {
        cout << "Enter mode: ";                      //輸出"Enter mode: "到螢幕上
        cin >> x;                                    //輸入一個值給x
        switch(x)                                    //判斷x的值
        {
        case 1:                                      //判斷x的值是否為1
            cout << "Arithmetic progression\n";      //輸出"Arithmetic progression\n"到螢幕上
            while(1)                                 //詢問a1的迴圈
            {
                cout << "a1: ";                      //輸出"a1: "到螢幕上
                cin >> a;                            //輸入一個值給a
                if(a<=0)                             //判斷a是否<=0
                {
                    cout << "Out of range!\n";       //輸出"Out of range!\n"到螢幕上
                }
                else
                {
                    break;                           //跳出迴圈
                }
            }
            while(1)
            {
                cout << "d: ";
                cin >> d;
                if(d<=0)
                {
                    cout << "Out of range!\n";
                }
                else
                {
                    break;
                }
            }
            while(1)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                {
                    cout << "Out of range!\n";
                }
                else
                {
                    break;
                }
            }
            cout << a;                              //輸出第1個a
            for (i=1; i<n; i++)                     //重複累加計算等差
            {
                cout << "," << a + i * d;           //輸出每次的等差級數
            }
            cout << endl;
            break;                                  //跳出switch到最下面

        case 2:                                     //判斷x的值是否為2
            cout << "Geometric progression\n";
            while(1)
            {
                cout << "a1: ";
                cin >> a;
                if(a<=0)
                {
                    cout << "Out of range!\n";
                }
                else
                {
                    break;
                }
            }
            while(1)
            {
                cout << "r: ";
                cin >> r;
                if(r<=0)
                {
                    cout << "Out of range!\n";
                }
                else
                {
                    break;
                }
            }
            while(1)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                {
                    cout << "Out of range!\n";
                }
                else
                {
                    break;
                }
            }
            cout << a;
            for(i=1; i<n; i++)                      //重複累加計算等比級數
            {
                a = a * r;
                cout << "," << a;                   //輸出每次的等比級數
            }
            cout << endl;
            break;

        case 3:                                     //判斷x的值是否為3
            cout << "Fibonacci sequence\n";
            while(1)
            {
                cout << "n: ";
                cin >> n;
                if(n<=0)
                {
                    cout << "Out of range!\n";
                }
                else
                {
                    break;
                }
            }
            cout << b1;
            for(i=1; i<n; i++)                      //重複累加計算費氏數列
            {
                cout << "," << b3;
                b3 = b1 + b2;
                b1 = b2;
                b2 = b3;
            }
            cout << endl;
            break;

        case -1:                                    //判斷x的值是否為-1
            break;

        default:                                    //判斷a是否為非上面的其他情形
            cout << "Illegal input!" << endl;
            break;
        }
    }
    return 0;
}
