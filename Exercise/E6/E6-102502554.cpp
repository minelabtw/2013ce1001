#include <iostream>
using namespace std;
int main ()
{
    int enter , a , d , r , n;
    int total = 0;
    int f1 = 1;
    int f2 = 1;
    int fn = 1;//宣告所需使用的變數

    cout << "Calculate the progression." << endl << "Choose the mode." << endl << "Arithmetic progression=1." << endl << "Geometric progression=2." << endl << "Fibonacci sequence=3." << endl << "Exit=-1." << endl;
    //輸出標題以及各種模式的代號
    do//執行以下指令當enter不等於-1時
    {
        cout << "Enter mode: ";//輸出Enter mode:
        cin >> enter;//輸入代號

        {
            switch ( enter )//設立switch判斷式
            {
            case 1://模式1
                cout << "Arithmetic progression" << endl << "a1: ";//輸出Arithmetic progression並於換行後輸出a1:
                cin >> a;//輸入a
                while ( a <= 0 )//當a<=0
                {
                    cout << "Out of range!" << endl << "a1: ";//輸出Out of range!並於換行後再輸出a1:
                    cin >> a;//再次輸入a
                }
                cout << "d: ";//輸出d:
                cin >> d;//輸入d
                while ( d <= 0 )//當d<=0
                {
                    cout << "Out of range!" << endl << "d: ";//輸出Out of range!並於換行後再輸出d:
                    cin >> d;//再次輸入d
                }
                cout << "n: ";//輸出n:
                cin >> n;//輸入n
                while ( n <= 0 )//當n<=0時
                {
                    cout << "Out of range!" << endl << "n: ";//輸出Out of range!並於換行後再輸出n:
                    cin >> n;//再次輸入n
                }
                cout << a;//先輸出第一項
                for ( int j = n ; j > 1 ; j-- )//設立for迴圈
                {
                    a = a + d ;//將a加上公差
                    cout << "," << a;//輸出,a
                }
                cout << endl ;//結束後換行
                break;//跳脫switch但因外面還有while迴圈所以能可繼續進行


            case 2://模式2
                cout << "Geometric progression" << endl << "a1: ";//輸出Geometric progression並於換行後再輸出a1:
                cin >> a;//輸入a
                while ( a <= 0 )//當a<=0
                {
                    cout << "Out of range!" << endl << "a1: ";//輸出Out of range!並於換行後再輸出a1:
                    cin >> a;//再次輸入a
                }
                cout << "r: ";//輸出r:
                cin >> r;//輸入r
                while ( r <= 0 )//當d<=0
                {
                    cout << "Out of range!" << endl << "r: ";//輸出Out of range!並於換行後再輸出d:
                    cin >> r;//再次輸入d
                }
                cout << "n: ";//輸出n:
                cin >> n;//輸入n
                while ( n <= 0 )//當n<=0時
                {
                    cout << "Out of range!" << endl << "n: ";//輸出Out of range!並於換行後再輸出n:
                    cin >> n;//再次輸入n
                }
                cout << a;//先輸出第一項
                for ( int j = n ; j > 1 ; j-- )//設立for迴圈
                {
                    a = a * r ;//將a乘上公比
                    cout << "," << a;//輸出,a
                }
                cout << endl ;//結束後換行
                break;//跳脫switch但因外面還有while迴圈所以能可繼續進行

            case 3://模式3
                cout << "Fibonacci sequence" << endl;//輸出Fibonacci sequence並換行
                cout << "n: ";//輸出n:
                cin >> n;//輸入n
                while ( n <= 0 )//當n<=0時
                {
                    cout << "Out of range!" << endl << "n: ";//輸出Out of range!並於換行後再輸出n:
                    cin >> n;//再次輸入n
                }
                cout << f1;//先輸出f1
                for ( int i = 2 ; i <= n ; i++ )//設立for迴圈
                {
                    if( i == 2 )//第2項
                        fn = f2;
                    if( i > 2 )//第3項後
                    {
                        fn = f1 + f2;//前2項相加
                        f1 = f2;//進位
                        f2 = fn;//進位
                    }
                    cout << "," << fn;//輸出,fn
                }
                cout << endl;//結束後換行
                break;//跳脫switch但因外面還有while迴圈所以能可繼續進行

            case -1://結束
                break;//跳脫switch且因為enter=-1所以會結束迴圈

            default://其他情況
                cout << "Illegal input!" << endl;//輸出Illegal input!並換行
                break;//跳脫switch但因外面還有while迴圈所以能可繼續進行
            }
        }
    }
    while ( enter != -1 );//設立大迴圈的條件

        return 0;
}
