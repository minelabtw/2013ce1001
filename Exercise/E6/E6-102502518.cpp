#include <iostream>
#include <cmath>

using namespace std;

int main()
{

    int a1=0;
    int d=0;
    int r=0;
    int n=0;
    int first_n=1;
    int f1=1;
    int f2=1;
    int fn=0;

    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode: ";

    int mode=0;

    while (mode!=-1)
    {
        cin>>mode;

        if (mode==-1)
            break;

        switch (mode)
        {

        case 1:

            cout<<"Arithmetic progression"<<endl;
            cout<<"a1: ";
            cin>>a1;

            while (a1<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }

            cout<<"d: ";
            cin>>d;

            while (d<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"d: ";
                cin>>d;
            }

            cout<<"n: ";
            cin>>n;

            while (n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            while(first_n<n)
            {
                cout<<(a1+d*(first_n-1))<<",";
                first_n++;
            }

            while(first_n==n)
            {
                cout<<(a1+d*(first_n-1))<<endl;
                first_n++;
                cout <<"Enter mode: ";
            }
            first_n=1;
            break;

        case 2:

            cout<<"Geometric progression"<<endl;
            cout<<"a1: ";
            cin>>a1;

            while (a1<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"a1: ";
                cin>>a1;
            }

            cout<<"r: ";
            cin>>r;

            while (r<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"r: ";
                cin>>r;
            }

            cout<<"n: ";
            cin>>n;

            while (n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            while(first_n<n)
            {
                cout<<(a1*pow(r,(first_n-1)))<<",";
                first_n++;
            }

            while(first_n==n)
            {
                cout<<(a1*pow(r,(first_n-1)))<<endl;
                first_n++;
                cout<<"Enter mode: ";
            }
            first_n=1;
            break;

        case 3:

            cout<<"Fibonacci sequence"<<endl;
            cout<<"n: ";
            cin>>n;

            while (n<=0)
            {
                cout<<"Out of range!"<<endl;
                cout<<"n: ";
                cin>>n;
            }

            while(first_n<n)
            {
                if(first_n==1)
                    fn=f1;
                if(first_n==2)
                    fn=f2;
                if(first_n>2)
                {
                    fn=f1+f2;
                    f1=f2;
                    f2=fn;
                }
                cout<<fn<<",";
                first_n++;

            }

            while(first_n==n)
            {
                fn=f1+f2;
                f1=f2;
                f2=fn;
                cout<<fn<<endl;
                first_n++;
                cout<<"Enter mode: ";
            }
            first_n=1;
            break;

        default:

            cout<< "Illegal input!"<<endl;
            cout<<"Enter mode: ";
            break;
        }

    }
    return 0;
}
