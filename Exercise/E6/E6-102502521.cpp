#include <iostream>

using namespace std;

int main()
{
    int mode;    //宣告變數

    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1"<<endl;

    while(mode!=-1)    //利用while來達到重複選擇模式的效果，在mode=-1時跳出迴圈
    {
        int a1=0;    //宣告變數，當迴圈回到這裡時重新設定變數數值
        int d=0;
        int n=0;
        int r=0;
        int m=0;
        int o=1;
        int p=0;
        int q=0;

        cout<<"Enter mode: ";
        cin>>mode;

        switch(mode)    //使用switch判斷使用模式
        {
        case 1:
            cout<<"Arithmetic progression"<<endl;

            while(a1<=0)    //使用while判斷數值是否合規定
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(d<=0)
            {
                cout<<"d: ";
                cin>>d;
                if(d<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(n<=0)
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(m<n)    //輸出數列
            {
                if(m==n-1)    //使用if判斷是否為最末項
                {
                    cout<<a1+d*m;
                }
                else
                {
                    cout<<a1+d*m<<",";
                }
                m++;    //遞增
            }

            cout<<endl;

            break;

        case 2:
            cout<<"Geometric progression"<<endl;

            while(a1<=0)
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(r<=0)
            {
                cout<<"r: ";
                cin>>r;
                if(r<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(n<=0)
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(m<n)
            {
                if(m==n-1)
                {
                    cout<<a1;
                    a1=a1*r;
                }
                else
                {
                    cout<<a1<<",";
                    a1=a1*r;
                }
                m++;
            }

            cout<<endl;

            break;

        case 3:
            cout<<"Fibonacci sequence"<<endl;

            while(n<=0)
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!"<<endl;
                }
            }

            while(m<n)
            {
                if(m==n-1)
                {
                    cout<<o;
                    p=o;    //將o之值傳給p
                    o=p+q;    //將o算出
                    q=p;    //將p之值傳給q
                }
                else
                {
                    cout<<o<<",";
                    p=o;
                    o=p+q;
                    q=p;
                }
                m++;
            }

            cout<<endl;

            break;

        default:
            cout<<"Illegal input!"<<endl;

            break;
        }
    }
    return 0;
}
