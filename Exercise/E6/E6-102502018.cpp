#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int x;
    int a1=0,d=0,n=0,r=0;
    int q=1;
    int f1=1,f2=1,fn=0;

    cout<<"Calculate the progression.\n";
    cout<<"Choose  the mode.\n";
    cout<<"Arithmetic progression=1.\n";
    cout<<"Geometric progression=2.\n";
    cout<<"Fibonacci sequence=3.\n";
    cout<<"Exit=-1.\n";
    cout<<"Enter mode: ";
    cin>>x;


    while(x!=-1)                                 //X等於-1跳出迴圈
    {
        switch(x)
        {
        case 1:                                  //
            cout<<"Arithmetic progression\n";
            while(a1<=0)
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            while(d<=0)
            {
                cout<<"d: ";
                cin>>d;
                if(d<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            while(n<=0)
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            while(q<=n)
            {
                cout<<a1;
                if(q<n)
                {
                    cout<<",";
                }
                a1=a1+d;               //前項加上公差等於後項
                q++;
            }
            break;
        case 2:
            cout<<"Geometric progression\n";
            while(a1<=0)
            {
                cout<<"a1: ";
                cin>>a1;
                if(a1<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            while(r<=0)
            {
                cout<<"r: ";
                cin>>r;
                if(r<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            while(n<=0)
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!\n";
                }
            }
            while(q<=n)
            {
                cout<<a1;
                if(q<n)
                {
                    cout<<",";
                }
                a1=a1*r;                  //前項乘公比等於後項
                q++;
            }
            break;
        case 3:
            cout<<"Fibonacci sequence\n";
            while(n<=0)
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                {
                    cout<<"Out of range!\n";
                }
            }

            while(q<=n)
            {
                if(q==1)
                    fn=f1;
                if(q==2)
                    fn=f2;
                if(q>2)
                {
                    fn=f1+f2;         //前兩項相加
                    f1=f2;            //進位
                    f2=fn;            //進位
                }
                cout<<fn;
                if(q<n)
                {
                    cout<<",";
                }
                q++;
            }
            break;

        default:
            cout<<"Illegal input!";

        }
        cout<<endl;
        cout<<"Enter mode: ";
        cin>>x;
        a1=0,d=0,n=0,r=0,q=1;           //使變數變回初始值
    }
    return 0;
}
