#include<iostream>
#include <cmath>
using namespace std;

int main()
{
    int a1=0,d=0,n=0, mode ; //設定數列的變數以及項數
    int i=0 ;
    int f1=1,f2=1;//第1,2項
    int fn=0;
    int a;
    cout<<"Calculate the progression."<<endl;
    cout<<"Choose the mode."<<endl;
    cout<<"Arithmetic progression=1."<<endl;
    cout<<"Geometric progression=2."<<endl;
    cout<<"Fibonacci sequence=3."<<endl;
    cout<<"Exit=-1."<<endl;
    cout<<"Enter mode:";
    cin>>mode ;
    while(mode!=1 &&mode!=2 && mode!=3 &&mode!=-1) //當MODE 不為1,2,3,-1時,重新輸入
    {
        cout<<" Illegal input!"<<endl<<"Enter mode:";
        cin>>mode ;
    }

    switch (mode)
    {
    default:
        cout<<" Illegal input!";
        break;
    case 1:
        cout<<"Arithmetic progression"<<endl;
        while(a1<=0)//輸入首項
        {
            cout<<"a1: ";
            cin>>a1;
            if(a1<=0)
                cout<<"Out of range!"<<endl;
        }
        while(d<=0)//輸入公差
        {
            cout<<"d: ";
            cin>>d;
            if(d<=0)
                cout<<"Out of range!"<<endl;
        }
        while(n<=0)//輸入項數
        {
            cout<<"n: ";
            cin>>n;
            if(n<=0)
                cout<<"Out of range!"<<endl;
        }

        while(i<n)//等差數列
        {
            int a=a1+d*i;
            cout<<a<<",";
            i++;
        }
        cout<<endl;
        cout<<"Enter mode:"; //重新進入判定
        cin>>mode ;
        while(mode!=1 &&mode!=2 && mode!=3 &&mode!=-1) //當MODE 不為1,2,3,-1時,重新輸入
        {
            cout<<" Illegal input!"<<endl<<"Enter mode:";
            cin>>mode ;
        }

    case 2:
        cout<<"Geometric progression."<<endl;
        while(a1<=0)//輸入首項
        {
            cout<<"a1: ";
            cin>>a1;
            if(a1<=0)
                cout<<"Out of range!"<<endl;
        }
        while(d<=0)//輸入公比
        {
            cout<<"d: ";
            cin>>d;
            if(d<=0)
                cout<<"Out of range!"<<endl;
        }
        while(n<=0)//輸入項數
        {
            cout<<"n: ";
            cin>>n;
            if(n<=0)
                cout<<"Out of range!"<<endl;
        }
        i=1;
        a=a1;
        while(i<n)//等比數列
        {

            cout<<a<<",";
            a=a*d;
            i++;
        }
        cout<<a<<endl;
        cout<<"Enter mode:"; //重新進入判定
        cin>>mode ;
        while(mode!=1 &&mode!=2 && mode!=3 &&mode!=-1) //當MODE 不為1,2,3,-1時,重新輸入
        {
            cout<<" Illegal input!"<<endl<<"Enter mode:";
            cin>>mode ;
        case 3:
            cout<<"Fibonacci sequence"<<endl;//費氏數列

            n=0;
            while(n<=0)//輸入項數
            {
                cout<<"n: ";
                cin>>n;
                if(n<=0)
                    cout<<"Out of range!"<<endl;
            }
            i=1;
            while(i<=n)
            {
                if(i==1)//第1項
                    fn=f1;
                if(i==2)//第2項
                    fn=f2;
                if(i>2)//第3項後
                {
                    fn=f1+f2;//前2項相加
                    f1=f2;//進位
                    f2=fn;//進位
                }

                cout<<fn<<",";
                i++;
            }
            cout<<endl;
            cout<<"Enter mode:"; //重新進入判定
            cin>>mode ;
            while(mode!=1 &&mode!=2 && mode!=3 &&mode!=-1) //當MODE 不為1,2,3,-1時,重新輸入
            {
                cout<<" Illegal input!"<<endl<<"Enter mode:";
                cin>>mode ;
            case -1:
                break;

            }
            return 0;
        }
    }
}
