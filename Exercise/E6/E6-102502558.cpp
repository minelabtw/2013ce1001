#include <iostream>

using namespace std;
/*
    輸入用的函數
    prompt 為提示使用者的文字
    error 為錯誤訊息
    n 是外部的變數
*/
void input(const char *prompt,const char *error, int &n)
{
    do
    {
        cout << prompt;
        cin >> n;
    }
    while(n <= 0 && cout << error << endl);
    return;
}
// 等差
void Arithmetic()
{
    cout << "Arithmetic progression" << endl;
    int a1 = 0,d = 0,n =0;
    input("a1: ","Out of range!",a1);
    input("d: ","Out of range!",d);
    input("n: ","Out of range!",n);
    cout << a1; // 先輸出第一個
    for (int i=1; i<n; i++)
    {
        cout << "," << a1+i*d; // 輸出 , an
    }
    cout << endl; // 最後換行
    return;
}
void Geometric()
{
    cout << "Geometric progression" << endl;
    int a1 = 0, r = 0, n = 0, an = 0;
    input("a1: ","Out of range!",a1);
    input("r: ","Out of range!",r);
    input("n: ","Out of range!",n);
    cout << a1; // 先輸出第一個
    an = a1;  //
    for (int i=1; i<n; i++)
    {
        an *= r; //把 an 累乘r
        cout << "," << an; // 輸出 , an
    }
    cout << endl;
    return;
}
void Fibonacci()
{
    cout << "Fibonacci sequence" << endl;
    int n = 0, a = 1, b = 1, c = 0;
    // a b c
    // 前兩項 = a,b
    // 下一項 = c = a+b
    input("n: ","Out of range!",n);
    cout << 1;
    while (--n)
    {
        cout << "," << b;
        // 算出c之後
        // a = b
        // b = c
        // 往前挪
        c = a+b;
        a = b;
        b = c;
    }
    cout << endl;
    return;
}
int main()
{
    int mode = 0; // 代表模式
    cout << "Calculate the progression." << endl
         << "Choose the mode." << endl
         << "Arithmetic progression=1." << endl
         << "Geometric progression=2." << endl
         << "Fibonacci sequence=3." << endl
         << "Exit=-1." << endl
         << "Enter mode: ";
    while (cin >> mode)
    {
        if (mode == -1) // 如果是-1就跳出
            break;
        switch(mode)
        {
        case 1:
            Arithmetic();
            break;
        case 2:
            Geometric();
            break;
        case 3:
            Fibonacci();
            break;
        default:
            cout << "Illegal input!" << endl; // 除了-1,1,2,3以外都是錯的
            break;
        }
        cout << "Enter mode: ";
    }
    return 0;
}
