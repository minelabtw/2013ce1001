#include <iostream>
using namespace std;
int main()
{
    int a=0; //宣告變數
    int b=0; //宣告變數
    int c=0; //宣告變數
    int d=0; //宣告變數
    int e=1; //宣告變數
    int f=0; //宣告變數
    cout <<"Calculate the progression."<<endl<<"Choose the mode."<<endl<<"Arithmetic progression=1."<<endl<<"Geometric progression=2."<<endl<<"Fibonacci sequence=3."<<endl<<"Exit=-1."<<endl<<"Enter mode: "; //輸出
    cin >>a; //輸入
    while (a!=-1) //進入迴圈
    {
        switch (a) //進入以下case
        {
        case 1: //等差數列
            cout <<"Arithmetic progression"<<endl<<"a1: ";
            cin >>b; //輸入
            while (b<1)
            {
                cout<<"Out of range!"<<endl<<"a1: ";
                cin >>b;
            }
            cout <<"d: ";
            cin >>c;
            while (c<1)
            {
                cout<<"Out of range!"<<endl<<"d: ";
                cin >>c;
            }
            cout <<"n: ";
            cin >>d;
            while (d<1)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin >>d;
            }
            cout <<b;
            while (e<d) //顯示等差數列
            {
                cout <<",";
                b=b+c;
                cout <<b;
                e++; //使e值加1
            }
            e=1; //初始e值
            cout <<endl<<"Enter mode: ";
            cin >>a;
            break; //跳出switch
        case 2: //等比數列
            cout<<"Geometric progression"<<endl<<"a1: ";
            cin >>b; //輸入
            while (b<1)
            {
                cout<<"Out of range!"<<endl<<"a1: ";
                cin >>b;
            }
            cout <<"r: ";
            cin >>c;
            while (c<1)
            {
                cout<<"Out of range!"<<endl<<"r: ";
                cin >>c;
            }
            cout <<"n: ";
            cin >>d;
            while (d<1)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin >>d;
            }
            cout <<b;
            while (e<d) //輸出等比數列
            {
                cout <<",";
                b=b*c;
                cout <<b;
                e++; //使e值加1
            }
            e=1; //初始e值
            cout <<endl<<"Enter mode: ";
            cin >>a;
            break; //跳出switch
        case 3: //費氏數列
            cout <<"Fibonacci sequence"<<endl<<"n: ";
            cin >>d;
            while (d<1)
            {
                cout<<"Out of range!"<<endl<<"n: ";
                cin >>d;
            }
            cout <<1;
            b=0; //使b值等於0
            c=1; //使c值等於1
            while (e<d) //輸出費氏數列
            {
                cout <<",";
                f=b+c;
                b=c;
                c=f;
                cout <<f;
                e++; //使e值加1
            }
            e=1; //初始e值
            cout <<endl<<"Enter mode: ";
            cin >>a;
            break; //跳出switch
        case -1:
            return 0; //結束
        default: //輸入其他
            cout <<"Illegal input!"<<endl<<"Enter mode: "; //輸出
            cin >>a; //輸入
        }
    }
    return 0; //結束
}
