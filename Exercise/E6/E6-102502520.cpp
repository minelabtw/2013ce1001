#include <iostream>
using namespace std;

int main()
{
    int mode;                                                       //定義一些整數
    int a1;
    int d;
    int r;
    int n;
    int rn=1;
    int f1=1;
    int f2=1;
    int fn=0;
    int an;

    cout<<"Calculate the progression.\n";                           //輸出
    cout<<"Choose the mode.\n";
    cout<<"Arithmetic progression=1.\n";
    cout<<"Geometric progression=2.\n";
    cout<<"Fibonacci sequence=3.\n";
    cout<<"Exit=-1.\n";

    while (mode!=-1)                                                //若mode!=-1,持續使用switch
    {
        cout<<"Enter mode: ";
        cin>>mode;
        switch (mode)
        {
        case 1:                                                     //case1: 等差數列
            cout<<"Arithmetic progression\n";
            cout<<"a1: ";
            cin>>a1;
            while (a1<=0)                                           //a1為非零正整數
            {
                cout<<"Out of range!\n";
                cout<<"a1: \n";
                cin>>a1;
            }
            cout<<"d: ";
            cin>>d;
            while (d<=0)                                            //d為非零正整數
            {
                cout<<"Out of range!\n";
                cout<<"d: ";
                cin>>d;
            }
            cout<<"n: ";
            cin>>n;
            while (n<=0)                                            //n為非零正整數
            {
                cout<<"Out of range!\n";
                cout<<"n: ";
                cin>>n;
            }
            for (int x=0 ; x<n ; x++)                               //等差數列
            {
                an=a1+x*d;
                cout<<an;
                if (x!=n-1)                                         //最後一位不加逗號
                {
                    cout<<",";
                }
            }
            cout<<endl;                                             //換行
            break;                                                  //結束switch

        case 2:                                                     //case2: 等比數列
            cout<<"Geometric progression\n";
            cout<<"a1: ";
            cin>>a1;
            while (a1<=0)                                           //a1為非零正整數
            {
                cout<<"Out of range!\n";
                cout<<"a1: ";
                cin>>a1;
            }
            cout<<"r: ";
            cin>>r;
            while (r<=0)                                           //r為非零正整數
            {
                cout<<"Out of range!\n";
                cout<<"r: ";
                cin>>r;
            }
            cout<<"n: ";
            cin>>n;
            while (n<=0)                                           //n為非零正整數
            {
                cout<<"Out of range!\n";
                cout<<"n: ";
                cin>>n;
            }
            for (int y=1 ; y<n ; y++)                             //等比數列
            {
                if (y==1)                                          //第1項
                {
                    cout<<a1<<",";
                }
                rn*=r;
                an=a1*rn;
                cout<<an;
                if (y!=n-1)                                          //最後一位不加逗號
                {
                    cout<<",";
                }
            }
            cout<<endl;                                             //換行
            break;                                                  //結束switch

        case 3:                                                     //case3: 費氏數列
            cout<<"Fibonacci sequence\n";
            cout<<"n: ";
            cin>>n;
            while (n<=0)                                           //n為非零正整數
            {
                cout<<"Out of range!\n";
                cout<<"n: ";
                cin>>n;
            }
            for (int z=1; z<=n; z++)                               //費氏數列
            {
                if (z==1)                                          //第1項
                {
                    fn=f1;
                }
                if (z==2)                                          //第2項
                {
                    fn=f2;
                }
                if (z>2)
                {
                    fn=f1+f2;
                    f1=f2;
                    f2=fn;
                }
                cout<<fn;
                if (z!=n)                                           //最後一位不加逗號
                {
                    cout<<",";
                }
            }
            cout<<endl;                                             //換行
            break;                                                  //結束switch

        default:
            cout<<"Illegal input!\n";                               //輸出
            break;                                                  //結束switch
        }
    }
    return 0;
}
