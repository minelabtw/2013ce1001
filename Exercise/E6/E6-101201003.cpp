#include<iostream>
using namespace std;

int main()
{
    int input=0;                                            
    int a1=0;
    int nMax=0;
    int d=0;
    int r=0;
    int number=0;
    int a4=1; 
    int a2=0;
    int a3=0;                                                           //定義型態為int的變數，並初始其值為0、1
    cout << "Calculate the progression." << endl
         << "Arithmetic progression=1." << endl
         << "Geometric progression=2." << endl
         << "Fibonacci sequence=3." << endl
         << "Exit=-1." << endl
         << "Enter mode:" ;                                             //顯示接下來所定義的模式 
    cin >> input;
    while(input!=-1)                                                    //當輸入不等於-1時，進入迴圈 
    {
        switch(input)                                                   //用switch判斷input變數 
        {   
            case 1:                                                     //當輸入值等於1時執行下列結果 
            {    
                cout << "Arithmetic progression" << endl << "a1: ";     //輸出字 
                cin >> a1;                                              //輸入a1    
                while(a1<=0)                                            //當a1小於0，進入迴圈，下列的d、nMax一樣 
                {
                    cout << "Out of range!" << endl << "a1: ";
                    cin >> a1;
                }
                cout << "d: ";
                cin >> d;
                while(d<=0)
                {
                    cout << "Out of range!" << endl << "d: ";
                    cin >> d;
                }
                cout << "n: ";
                cin >> nMax;
                while(nMax<=0)
                {
                    cout << "Out of range!" << endl << "n: ";
                    cin >> nMax;
                }
                for(int n=1;n<=nMax;n++)                                //當n<輸入的項數時 
                {
                    number=a1+n*d-d;                                    //等差公式 
                    cout << number ;
                    if(n!=nMax)
                        cout<<",";
                    else
                        cout<<endl;                                     //數跟數之間要有逗號，最後一項就換行 
                }
            }    
                break;                                                  //跳出迴圈 
            case 2:                                                     //當輸入值等於2時執行下列結果 
            {
                cout << "Arithmetic progression" << endl << "a1: ";
                cin >> a1;
                while(a1<=0)
                {
                    cout << "Out of range!" << endl << "a1: ";
                    cin >> a1;
                }
                cout << "r: ";
                cin >> r;
                while(r<=0)
                {
                    cout << "Out of range!" << endl << "r: ";
                    cin >> r;
                }
                cout << "n: ";
                cin >> nMax;
                while(nMax<=0)
                {
                    cout << "Out of range!" << endl << "n: ";
                    cin >> nMax;
                }
                for(int n=1;n<=nMax;n++)
                {
                    cout << a1;
                    a1 = a1*r;
                    
                    if(n!=nMax)
                        cout << ",";
                    else
                        cout << endl;
                }    
            }                                                           //同等差數列的做法，只是將公式換掉 
                break;
            case 3:                                                     //當輸入值等於3時執行下列結果 
            {    
                cout << "Fibonacci sequence" << endl << "n: ";          
                cin >> nMax;
                while(nMax<=0)
                {
                    cout << "Out of range!" << endl << "n: ";
                    cin >> nMax;
                }
                a4=1;
                a2=0;
                for(int n=1;n<=nMax;n++)
                {
                    
                    cout << a4;
                    a3 = a4;
                    a4 = a2 + a3;
                    a2 = a3;                                            //費式數列
                    
                    if(n!=nMax)
                        cout << ",";
                    else
                        cout << endl;
                }    
            }    
                break;
            default:                                                    //當輸入不1、2、3時 ，執行下列結果 
                cout << "Illegal input!" << endl << "Enter mode:";      //輸出 Illegal input!
                cin >> input;                                           //再次輸入input 
        }
    cout<< "Enter mode:" ;                                              //完成上面的迴圈後，再次輸入，可再次判斷數值 
    cin >> input;
    }
return 0;
}
    
