#include <iostream>
using namespace std;

int main()
{
    int flag=0, mode=0, input=0, a1=0, d=0, n=0, r=0;   //宣告變數為整數型態，標記、模式、判斷範圍用輸入、首項、公差、項數、公比，並設定初始值
    cout << "Calculate the progression.\n"  //輸出說明
         << "Choose the mode.\n"
         << "Arithmetic progression=1.\n"
         << "Geometric progression=2.\n"
         << "Fibonacci sequence=3.\n"
         << "Exit=-1.\n";
    while(1)    //使得可以重複選擇mode或quit
    {
        cout << "Enter mode: ";
        cin >> mode;
        if(mode==1 || mode==2)  //mode 1、2一起做，等差與等比數列
        {
            cout << (mode==1 ? "Arithmetic progression\n" : "Geometric progression\n");
            while(flag<3)   //要求輸入，超出範圍則發出警告並從新要求
            {
                if(flag==0)
                {
                    cout << "a1: ";
                    cin >> input, a1=input;
                }
                else if(flag==1)
                {
                    cout << (mode==1 ? "d: " : "r: ");
                    cin >> input, d=input, r=input;
                }
                else if(flag==2)
                {
                    cout << "n: ";
                    cin >> input, n=input;
                }
                if(input<=0)
                    cout << "Out of range!\n";
                else
                    flag++;
            }
            for(int i=1; i<=n; i++)
            {
                if(i==1)    //第一項分開輸出
                    cout << a1;
                else
                {
                    (mode==1 ? a1=a1+d : a1=a1*r);
                    cout << "," << a1;
                }
            }
            cout << endl;
        }
        else if(mode==3)    //費氏數列
        {
            cout << "Fibonacci sequence\n";
            while(1)    //要求輸入，超出範圍則發出警告並從新要求
            {
                    cout << "n: ";
                    cin >> n;
                if(n<=0)
                    cout << "Out of range!\n";
                else
                    break;
            }
            for(int i=1, f1=1, f2=1; i<=n; i++)
            {
                if(i==1)    //第一、二項分開輸出
                    cout << f1;
                else if(i==2)
                    cout << "," << f2;
                else if(i%2==1)     //奇數項輸出
                {
                    f1=f1+f2;
                    cout << "," << f1;
                }
                else    //偶數項輸出
                {
                    f2=f2+f1;
                    cout << "," << f2;
                }
            }
            cout << endl;
        }
        else if(mode==-1)
            break;
        else
            cout << "Illegal input!\n";
        flag=0;     //初始化flag
    }
    return 0;
}
