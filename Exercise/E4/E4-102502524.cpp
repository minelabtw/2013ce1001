#include <iostream>
using namespace std;

int main()
{
    int a = 0;                                              //設定輸入變數為a,b
    int b = 0;
    int x = 1;                                              //設定輸出變數為x,y
    int y = 1;

    cout << "Please enter first number( >0 ):";
    cin >> a;
    while (a<=0)                                            //判斷a是否符合所需範圍
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> a;
    }

    cout << "Please enter second number( >0 ):";
    cin >> b;
    while (b<=0)                                            //判斷b是否符合所需範圍
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> b;
    }

    while (x<=a)
    {
        while (y<=b)
        {
            cout << x << "*" << y << "=" << x*y << endl;    //輸出乘法表
            y++;
        }
        if (x<a)
            cout << endl;                                   //最後一組不空一行
        y=1;
        x++;
    }
    return 0;
}
