#include<iostream>

using namespace std;

int main()
{

    int integer1=0;                                   //輸入名稱為integer1的變數
    int integer2=0;                                   //輸入名稱為integer2的變數


    cout<< "Please enter first number( >0 ):";
    cin>> integer1;
    while(integer1<=0)                                //迴圈'條件為integer1小於或等於0
    {
        cout<< "First number is out of range!!\n""Please enter first number( >0 ):";
        cin>> integer1;
    }

    cout<< "Please enter second number( >0 ):";
    cin>> integer2;
    while(integer2<=0)                               //迴圈'條件為integer2小於或等於0                    //
    {
        cout<< "Second number is out of range!!\n""Please enter second number( >0 ):";
        cin>> integer2;
    }

    int x=1;                                 //輸入名稱為x的變數
    int y=1;                                 //輸入名稱為y的變數
    int answer=1;                            //輸入名稱為answer的變數

    while(x<=integer1)                       //迴圈  條件為x小於或等於integer1
    {
        while(y<=integer2)                   //迴圈 條件為y小於或等於integer2
        {
            answer=x*y;
            cout<< x<< "*"<< y<< "="<< answer<<"\n";
            y++;
        }
        cout<< endl;
        y=1;
        x++;
    }

    return 0;
}
