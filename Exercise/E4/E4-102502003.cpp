#include<iostream>
using namespace std;

int main()
{
    int number1 = 0;  //宣告變數number1表第一個數、其初始值為0。
    int number2 = 0;  //宣告變數number2表第二個數，其初始值為0。
    int sum = 0;
    int i = 1;  //宣告變數i，其初始值為1。
    int j = 1;  //宣告變數j，其初始值為1。

    while(number1<=0)  //輸入第一個數。
    {
        cout<<"Please enter first number( >0 ):";
        cin>>number1;
        if(number1<=0)
            cout<<"First number is out of range!!"<<endl;
    }

    while(number2<=0)  //輸入第二個數。
    {
        cout<<"Please enter second number( >0 ):";
        cin>>number2;
        if(number2<=0)
            cout<<"Second number is out of range!!"<<endl;
    }


    for(i=1; i<=number1; i++)  //i由初始值累加到number1的值始停止。
    {
        for(j=1; j<=number2; j++)  //j由初始值累加到number2的值。
        {
            sum = i * j;  //相乘
            cout<<i<<"*"<<j<<"="<<sum<<endl;  //輸出等式。
        }
        cout<<endl;  //使i改變時隔一行空白。


    }

    return 0;

}
