#include <iostream>
using namespace std;

int main()
{
	int firstnum;
	cout << "Please enter first number( >0 ):"; cin >> firstnum;

	while(firstnum<=0){
		//warn the user and re-input if firstnum is not in range
		cout << "First number is out of range!!\n";
		cout << "Please enter first number( >0 ):"; cin >> firstnum;
	}

	int secondnum;
	cout << "Please enter second number( >0 ):"; cin >> secondnum;

	while(secondnum<=0){
		//warn the user and re-input if secondnum is not in range
		cout << "Second number is out of range!!\n";
		cout << "Please enter second number( >0 ):"; cin >> secondnum;
	}

	for(int n1=1 ; n1<=firstnum ; n1++){							//for every first number
		for(int n2=1 ; n2<=secondnum ; n2++){						//for every second number
			cout << n1 << "*" << n2 << "=" << n1*n2 << "\n";		//print the multiplication table
		}
		cout << "\n";
	}

	return 0;
}
