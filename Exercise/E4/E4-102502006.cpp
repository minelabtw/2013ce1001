#include <iostream>

using namespace std;

int main()
{

    int num1=0; // 數字一
    int num2=0; // 數字二

    // 輸入數字一
    while(num1<1)
    {
        cout << "Please enter first number( >0 ):";
        cin >> num1;
        if(num1<1)cout << "First number is out of range!!\n";
    }

    // 輸入數字二
    while(num2<1)
    {
        cout << "Please enter second number( >0 ):";
        cin >> num2;
        if(num2<1)cout << "Second number is out of range!!\n";
    }

    // 印出乘法表
    for(int i=0; i<num1; i++)
    {
        for(int k=0; k<num2; k++)
        {
            cout << i+1 << "*" << k+1 << "=" << (i+1)*(k+1) << endl;
        }
        cout << endl;
    }

    return 0;
}
