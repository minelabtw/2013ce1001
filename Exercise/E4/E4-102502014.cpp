#include <iostream>
using namespace std;

int main()
{
    int a;                      //宣告3個變數 以int型別儲存
    int b;
    int v;

    while(1)                    //寫一個while迴圈 判斷輸入的第一個數是否大於0
    {
        cout<<"Please enter first number( >0 ):";
        cin>>a;
        if(a<=0)
        {
            cout<<"First number is out of range!!"<<endl;     //不大於0便在螢幕上列印出"First number is out of range!!"
        }
        else
        {
            break;
        }
    }
    while(1)                   //寫第二個while迴圈 判斷輸入的第二個數是否大於0
    {
        cout<<"Please enter second number( >0 ):";
        cin>>b;
        if(b<=0)
        {
            cout<<"Second number is out of range!!"<<endl;    //不大於0便在螢幕上列印出"First number is out of range!!"
        }
        else
        {
            break;
        }
    }
    for(int c=1; c<=a; c++)        //寫雙層的for迴圈列印出輸入數字對應的乘法表
    {
        for(int d=1; d<=b; d++)
        {
            v=c*d;
            cout<<c<<"*"<<d<<"="<<v;
            cout<<endl;
        }
        cout<<endl;
    }


    return 0;
}
