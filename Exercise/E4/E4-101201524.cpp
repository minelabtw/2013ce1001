#include<iostream>
using namespace std;

int main()
{
    int number1,number2,                                 //宣告變數number1,number2來接收之後輸入的變數
        i,j;                                             //宣告變數i,j來代表成法表裡不同次的運算

    cout << "Please enter first number( >0 ):";          //輸出字串"Please enter first number( >0 ):"
    cin >> number1;                                      //將輸入的變數宣告給number1
    while(number1<=0)
    {
        cout << "First number is out of range!!\n";      //當輸入的數字<=0時，輸出字串"First number is out of range!!\n"並要求重新輸入number1
        cout << "Please enter first number( >0 ):";
        cin >> number1;
    }

    cout << "Please enter second number( >0 ):";         //輸出字串"Please enter second number( >0 ):"
    cin >> number2;                                      //將輸入的變數宣告給number2
    while(number2<=0)
    {
        cout << "Second number is out of range!!\n";     //當輸入的數字<=0時，輸出字串"Second number is out of range!!\n"並要求重新輸入number2
        cout << "Please enter second number( >0 ):";
        cin >> number2;
    }

    for(i=1;i<=number1;i++)                              //令成法表為i*j的方式i從1到number1
    {
        for(j=1;j<=number2;j++)                          //令成法表為i*j的方式j從1到number2
        {
            cout << i << "*" << j << "=" << i*j << "\n"; //按照i和j的順序輸出字串"i*j=i*j"(後者為相成後的數字)
        }
        cout << "\n";                                    //每完成一組i成以1~number2的成法表後，空一行i=i+1，換下一個數字做1~number2的成法表直到i=number1
    }

    return 0;
}
