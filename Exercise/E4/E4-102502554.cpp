#include <iostream>
using namespace std ;

int main ()
{
    int a ;//宣告整數變數a
    int b ;//宣告整數變數b

    cout << "Please enter first number( >0 ):" ;//輸出Please enter first number( >0 ):
    cin >> a ;//輸入a

    while ( a < 0)//設定條件a<0
    {
        cout << "First number is out of range!!" << endl ;//輸出First number is out of range!!並換行
        cout << "Please enter first number( >0 ):" ;//再次輸入Please enter first number( >0 ):
        cin >> a ;//再次輸入a
    }//符合條件時執行括號之內容

    cout << "Please enter second number( >0 ):";//輸出Please enter second number( >0 ):
    cin >> b ;//輸入b

    while ( b < 0)//設定條件b<0
    {
        cout << "Second number is out of range!!" << endl ;//輸出Second number is out of range!!並換行
        cout << "Please enter second number( >0 ):";//再次輸出Please enter second number( >0 ):
        cin >> b ;//再次輸入b
    }//符合條件時執行括號之內容

    for ( int c = 1 ;  c <= a ; c++ )//建立一個迴圈及一個新的整數變數c,當c<a時會逐漸增加一加到a為止
    {
        for ( int d = 1 ; d <= b ; d++ )//建立一個迴圈及一個新的整數變數d,當d<b時會逐漸增加一加到b為止
        {
            int e = c*d;//設定一個新的整數變數e=c*d
            cout << c << "*" << d << "=" << e << endl ;//輸出c*d=e並換行
        }
        cout << endl ;//完成一個段落後再次換行
    }
    return 0 ;
}
