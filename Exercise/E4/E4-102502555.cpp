#include <iostream>

using namespace std;

int main()
{
    int first_number;  //宣告變數數字一
    int second_number;  //宣告變數數字二

    do{
        cout << "Please enter first number( >0 ):";  //請使用者輸入數字一
        cin >> first_number;  //輸入數字一
        if(first_number <= 0){  //判斷有無超出範圍
            cout << "First number is out of range!!" << endl;  //告知超出範圍
        }
    }while(first_number <= 0);  //當超出範圍再跑一次
    do{
        cout << "Please enter second number( <0 ):";  //請使用者輸入數字二
        cin >> second_number;  //輸入數字二
        if(second_number <= 0){  //判斷有無超出範圍
            cout << "Second number is out of range!!" << endl;  //告知超出範圍
        }
    }while(second_number <=0);  //當超出範圍再跑一次

    for(int i = 1 ; i <= first_number ; i++){  //判斷有多少段
        for(int j = 1 ; j <= second_number ; j++){  //判斷每段多少行
            cout << i << "*" << j << "=" << i*j << endl;  //輸出乘法表
            if(j == second_number){  //每段換行
                cout << endl;
            }
        }
    }

    return 0;

}
