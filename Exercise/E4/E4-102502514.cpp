//乘法表
//請寫一個程式可以供使用者輸入兩個整數，
//還要可以判別使用者輸入的數字大於0，
//若不滿足則要重新輸入，
//最後輸出的乘法表 (必須換行區隔如輸出表)。
#include <iostream>
using namespace std;
int main()
{
    int a,b;
    int x=1,y=1;

    cout <<"Please enter first number( >0 ):"; //輸入第一個數字
    cin >>a;
    while(a<=0)
    {
        cout <<"First number is out of range!!\n"<<"Please enter first number( >0 ):"; //判別第一個數字是否大於0,否則重新輸入
        cin >>a;
    }
    cout <<"Please enter second number( >0 ):"; //輸入第二個數字
    cin >>b;
    while(b<=0)
    {
        cout <<"Second number is out of range!!\n"<<"Please enter second number( >0 ):"; //判別第二個數字是否大於0,否則重新輸入
        cin >>b;
    }
    while (x<=a)
    {
        while (y<=b)
        {
            cout <<x<<"*"<<y<<"="<<x*y<<endl; //x先不變,y由1開始依序向下印出
            y++;
        }
        cout <<endl;
        y=1; //y跳回起始值
        x++; //x由1開始依序向下印出乘法表
    }

    return 0;
}
