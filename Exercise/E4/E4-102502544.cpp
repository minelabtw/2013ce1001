#include<iostream>
using namespace std;

int main()
{
    int a=0;      //宣告型別為整數,並初始化
    int b=0;      //宣告型別為整數,並初始化

    cout<<"Please enter first number(>0):";
    cin>>a;
    while(a<=0)        //重覆循環並設定範圍
    {
        cout<<"First number is out of range!!\nPlease enter first number(>0):";
        cin>>a;
    }
    cout<<"Please enter second number(>0):";
    cin>>b;
    while(b<=0)        //重覆循環並設定範圍
    {
        cout<<"Second number is out of range!!\nPlease enter second number(>0):";
        cin>>b;
    }
    int c=1;           //宣告型別為整數
    int d=1;           //宣告型別為整數
    while(c<=a)        //重覆循環並設定範圍
    {
        while(d<=b)    //重覆循環並設定範圍
        {
            cout<<c<<"*"<<d<<"="<<c*d<<endl;
            d++;
        }
        cout<<endl;
        d=1;
        c++;
    }
    return 0;
}
