#include<iostream>

using namespace std;

int main()
{
    int number1 = 0;//宣告整數一
    int number2 = 0;//宣告整數二
    cout << "Please enter first number( >0 ):";//輸出Please enter first number( >0 ):
    cin >> number1;//輸入整數一
    while(number1 <= 0)//迴圈 當整數一小於等於0
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";//輸出First number is out of range!! 換行 Please enter first number( >0 ):
        cin >> number1;//輸入整數一
    }
    cout << "Please enter second number( >0 ):";//輸出Please enter second number( >0 ):
    cin >> number2;//輸入整數二
    while(number2 <= 0)//迴圈 當整數二小於等於0
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):";//輸出Second number is out of range!! 換行 Please enter second number( >0 ):
        cin >> number2;//輸入整數二
    }
    for(int a = 1; a <= number1; a++)//迴圈 當a=1 a小於等於整數一 a遞增
    {
        for(int b = 1; b <= number2; b++)//迴圈 當b=1 a小於等於整數二 b遞增
        {
            cout << a << "*" << b << "=" << a*b << endl;//輸出a乘b等於積
            if(b == number2)//條件 當b=整數二
                cout << endl;//輸出 換行
        }
    }
    return 0;
}
