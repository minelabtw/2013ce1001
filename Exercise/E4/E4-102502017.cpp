#include<iostream>
using namespace std;

int main()
{
    int first=0,second=0;   //宣告型別為 整數(int) 的變數

    while(first<=0)         //輸入第一個數字
    {
        cout << "Please enter first number( >0 ):";
        cin >> first;
        if(first<=0) cout << "First number is out of range!!\n";
    }
    while(second<=0)        //輸入第二個數字
    {
        cout << "Please enter second number( >0 ):";
        cin >> second;
        if(second<=0) cout << "Second number is out of range!!\n";
    }

    for(int i=1; i<=first; i++)     //print出結果
    {
        for(int j=1; j<=second; j++)
        {
            cout << i << "*" << j << "=" << i*j << endl;
        }
        cout << endl;
    }
    return 0;
}
