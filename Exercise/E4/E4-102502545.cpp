#include <iostream>
using namespace std;


int main()
{
    int a=0;
    int b=0;

    cout<<"Please enter first number( >0 ):";
    cin>>a;

    while(a<=0)
    {
        cout<<"First number is out of range!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>a;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>b;

    while(b<=0)
    {
        cout<<"Second number is out of range!!\n";
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }

    int c=1;
    int d=1;

    while (c<=a)
    {
        while(d<=b)
        {
            cout<<c<<"*"<<d<<"="<<c*d<<endl;
            d++;

        }
        d=1;
        c++;
        cout<<endl;

    }

    return 0;
}
