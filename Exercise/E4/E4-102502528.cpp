#include<iostream>

using namespace std;

int main()
{
    int num1,num2;
    int i,j;

    cout << "Please enter first number( >0 ):" << endl;
    cin >> num1 ;
    while (num1 <= 0)
    {
        cout << "First number is out of range!!" << endl <<  "Please enter first number( >0 ):";     //確認輸入的值是否在範圍內
        cin >> num1;
    }
    cout << "Please enter second number( >0 ):" ;
    cin >> num2 ;
    while (num2 <= 0)
    {
        cout << "Second number is out of range!!" << endl <<  "Please enter second number( >0 ):";   //確認輸入的值是否在範圍內
        cin >> num2;
    }
    for(i=1; i<=num1; i++)                                                                           //輸出等於num1之值的次數
    {
        for(j=1; j<=num2; j++)                                                                       //輸出到等於num2之值的i*j
        {
            cout<<i<<"*"<<j<<"="<<i*j<<endl;
        }
        cout<<endl;                                                                                  //中間空一行
    }

    return 0;
}
