#include <iostream>

using namespace std;

int main()
{
    int integer1 = 0;                                     //宣告名稱為integer1整數變數
    int integer2 = 0;                                     //宣告名稱為integer2整數變數
    int x = 1;                                            //將變數x初始化為0
    int y = 1;                                            //將變數y初始化為0

    cout << "Please enter first number( >0 ):";           //輸出字串Please enter first number( >0 ):
    cin >> integer1;                                      //輸入integer1
    while (integer1<=0)                                   //當integer1<=0
    {
        cout << "First number is out of range!!" << endl; //輸出字串First number is out of range!!
        cout << "Please enter first number( >0 ):";       //輸出字串Please enter first number( >0 ):
        cin >> integer1;                                  //輸入integer1
    }

    cout << "Please enter second number( >0 ):";          //輸出字串Please enter second number( >0 ):
    cin >> integer2;                                      //輸入integer1
    while (integer2<=0)                                   //當integer2<=0
    {
        cout << "Second number is out of range!!" << endl;//輸出字串Second number is out of range!!
        cout << "Please enter second number( >0 ):";      //輸出字串Please enter second number( >0 ):
        cin >> integer2;                                  //輸入integer1
    }
    while (x<=integer1)                                   //當x<=integer1
    {
        while (y<=integer2)                               //當y<=integer2
        {
            cout << x << "*" << y << "=" << x * y << endl;//輸出字串x*y=
            y++;                                          //y+1
        }
        cout << endl << endl;                             //換行
        y=1;                                              //將y拉回1
        x++;                                              //x+1
    }
    return 0;
}
