#include<iostream>
using namespace std ;
int main()
{
    int number1=0; // set some values
    int number2=0;
    int i=0;
    int j=0;
    cout<<"Please enter first number( >0 ):"; // input number1
    cin>>number1;
    while(number1<=0) // if number1<=0, reset number1
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>number1;
    }
    cout<<"Please enter second number( >0 ):"; // input number2
    cin>>number2;
    while(number2<=0) // if number2<=0, reset number2
    {
        cout<<"Second number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>number2;
    }
    for(i=1; i<=number1; i++) // i from 1 to number1, loop number1 times
    {
        for(j=1; j<=number2; j++) // j from 1 to number2, loop number2 times
        {
            cout<<i<<"*"<<j<<"="<<i*j<<endl; // output i*j
        }
        cout<<endl;
    }
    return 0;
}
