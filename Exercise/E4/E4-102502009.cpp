#include<iostream>
using namespace std ;

int main()
{
    int number1=0; //宣告變數
    int number2=0;

    int timei=0;
    int timej=0;

    cout<<"Please enter first number( >0 ):"; //輸入number1
    cin>>number1;

    while(number1<=0) //若number1<=0,則重新輸入
    {
        cout<<"First number is out of range!!"<<endl;

        cout<<"Please enter first number( >0 ):";
        cin>>number1;
    }

    cout<<"Please enter second number( >0 ):"; //輸入number2
    cin>>number2;

    while(number2<=0) //若number2<=0,則重新輸入
    {
        cout<<"Second number is out of range!!"<<endl;

        cout<<"Please enter second number( >0 ):";
        cin>>number2;
    }

    for(timei=1; timei<=number1; timei++) //timei從1開始直到number1,迴圈共執行number1次
    {
        for(timej=1; timej<=number2; timej++) //timej從1開始直到number2,迴圈共執行number2次
        {
            cout<<timei<<"*"<<timej<<"="<<timei*timej<<endl; //輸出timei*timej
        }

        cout<<endl;
    }

    return 0;

}
