#include <iostream>

using namespace std;

int main()
{
    int one = -1;
    int two = -1;
    while(one <= 0)
    {
        cout << "Please enter first number( >0 ):";
        cin >> one;

        if(one <= 0)
            cout << "\aFirst number is out of range!\n";
    } // the restriction of the one
    while(two <= 0)
    {
        cout << "Please enter second number( >0 ):";
        cin >> two;

        if(two <= 0)
            cout << "\aSecond number is out of range!\n";
    } // the restriction of the two
    int x = 1, y = 1;
    while(x <= one) // let the number one replace from x
    {
        while(y <= two) // iet the number two replace from y
        {
            cout << x << "*" << y << "=" << x*y << "\n"; // print the answer of the one and the two
            y++;
        }
        cout << endl;
        y = 1; // y start from 1
        x++;

    }

    return 0;

}
