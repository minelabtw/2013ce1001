#include <iostream>

using namespace std;

int main()

{
    int integer1;//宣告變數integer1
    int integer2;//宣告變數integer2
    int x = 1;//宣告x
    int y = 1;//宣告y

    cout << "Please enter first number( >0 ):"; //輸出字串
    cin >> integer1 ;//輸入變數integer1

    while ( integer1 <= 0 )//加入條件-integer1要大於零
    {
        cout << "First number is out of range!!" << endl;//假如不符合條件，則跑出此字串
        cout << "Please enter first number( >0 ):";
        cin >> integer1;//重新輸入integer1
    }

    cout << "Please enter second number( >0 ):";
    cin >> integer2 ;

    while ( integer2 <= 0 )
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> integer2;
    }

    while ( x <= integer1 )//利用迴圈顯示字串
    {
        while ( y <= integer2 )//利用變數Y出現九九乘法的左邊列
        {
            cout << x << "*" << y << "=" << x*y << endl;//輸出x*y的字串
            y++;//Y+1，然後再回圈Y
        }
        y = 1;
        cout << endl;//換行
        x++;//X+1，然後再回迴圈X
    }


    return 0;
}
