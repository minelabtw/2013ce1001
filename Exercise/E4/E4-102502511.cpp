#include <iostream>
using namespace std;

int main()
{
    int firstnumber=1; //設firstnumber=1，使他為x的最小值，以做下列運算
    int secondnumber=1; //設secondnumber=1，使他為x的最小值，以做下列運算
    int x; //設一個變數為x
    int y; //設一個變數為y
    int z; //設一個變數為z

    cout << "Please enter first number( >0 ):";
    cin >> x;
    while(x<=0) //當打出來的x小於0時，做下列循環
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> x;
    }

    cout << "Please enter second number( >0 ):";
    cin >> y;
    while(y<=0) //當打出來的y小於0時，做下列循環
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> y;
    }

    while(firstnumber<=x) //當firstnumber<=x時，做下列動作
    {
        while(secondnumber<=y) //secondnumber<=y，做下列動作
        {
            z = firstnumber * secondnumber; //z為firstnumber乘於secondnumber的答案
            cout << firstnumber << "*" << secondnumber << "=" << z << endl;
            secondnumber++; //secondnumber不斷遞增，直到超過y為止
        }
        if(firstnumber==x) //當firstnumber=x時，跳出迴圈
            break;
        cout << "+++" << endl;
        secondnumber=1; //使secondnumber回到1，以跳回上面的迴圈當中，繼續做運算
        firstnumber++; //firstnumber不斷遞增，直到此數超過x為止
    }
    return 0;
}
