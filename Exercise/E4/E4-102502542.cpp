#include <iostream>
using namespace std ;

int main()
{
    int a=0 ; //宣告變數a
    int b=0 ; //宣告變數b
    int x=1 ; //宣告變數x
    int y=1 ; //宣告變數y

    cout << "Please enter first number( >0 ):" ; //輸出Please enter first number( >0 ):
    cin >> a ; //輸入a
    while ( a<=0 ) //當a<=0進入以下迴圈
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):" ;
        cin >> a ;
    }
    cout << "Please enter second number( >0 ):" ; //輸出"Please enter second number( >0 ):
    cin >> b ; //輸入b
    while ( b<=0 ) //當b<=0進入以下迴圈
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):" ;
        cin >> b ;
    }

    while (y<=a) //當y<=a進入以下迴圈
    {
        while (x<=b) //當x<=b進入以下迴圈
        {
            int c=y*x ; //宣告變數 c=y*x
            cout << y << "*" << x << "=" << c << endl ; //輸出c值
            x++ ; //x值加1
        }
        cout << endl ; //換行
        x=1 ; //重置x值讓x=1
        y++ ; //y加1
    }
    return 0 ;

}
