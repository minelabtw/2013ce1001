#include<iostream>

using namespace std;

int main()
{
    int a=1;//整數變數當第一個數字
    int b=1;//整數變數當第二個數字
    int ft=0;//第一個數字最大值
    int sd=0;//第二個數字最大值
    int p=0;//相乘之結果

    cout << "Please enter first number(  >0  ):";
    cin  >> ft;
    while(ft<=0)//如輸入小於等於零,則能再輸入一次的迴圈
    {
        cout << "First number is out of range!!" << "\n";
        cout << "Please enter first number(  >0  ):";
        cin  >> ft;
    }

    cout << "Please enter second number(  >0  ):";
    cin  >> sd;
    while(sd<=0)//如輸入小於等於零,則能再輸入一次的迴圈
    {
        cout << "Second number is out of range!!" << "\n";
        cout << "Please enter second number(  >0  ):";
        cin  >> sd;
    }

    while(a<=ft)//使第一個數字的迴圈 包 第二個數字的迴圈
    {
        while(b<=sd)//當第二個數字小於等於輸入值，讓ab相乘輸出出來的迴圈
        {
            p=a*b;
            cout << a << "*" << b << "=" << p << "\n";
            b++;
        }
        cout << "\n";//第一個字加一就換行，使他看起來一組組的
        a++;
        b=1;//b要歸零

    }


    return 0;

}
