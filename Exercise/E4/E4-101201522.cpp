#include <iostream>
using namespace std;

int main(){
    int num1,num2,i,j;//宣告變數為整數,num1為第一個數字,num2為第二個數字 
    
    while(1){//判斷輸入數字是否超出範圍,如果超出範圍,要求再輸入 
        cout << "Please enter first number( >0 ):";
        cin >> num1;
        if(num1>0)
            break;
        else
            cout << "First number is out of range!!\n";
    }
    
    while(1){//判斷輸入數字是否超出範圍,如果超出範圍,要求再輸入
        cout << "Please enter second number( >0 ):";
        cin >> num2;
        if(num2>0)
            break;
        else
            cout << "Second number is out of range!!\n";
    }
    
    for(i=1;i<=num1;i++){//輸出i*j乘法表,中間隔一個換行 
        for(j=1;j<=num2;j++){
            cout << i << "*" << j << "=" << i*j << endl;
        }
        cout << (i<num1 ? "\n" : "");
    }
    
    return 0;
}
