#include <iostream>
using namespace std;

int main()
{
    int first_number,second_number,sum;                     //宣告型別為整數的first_number,second_number,sum
    int a=1,b=1;                                            //宣告型別為整數的a,b

    while(1)                                                //詢問第一個數的迴圈
    {
        cout << "Please enter first number( >0 ):";         //輸出"Please enter first number( >0 ):"到螢幕上
        cin >> first_number;                                //輸入一個值給first_number
        if (first_number<=0)                                //判斷first_number是否符合範圍
        {
            cout << "First number is out of range!!\n";     //輸出"First number is out of range!!\n"到螢幕上
        }
        else
        {
            break;                                          //跳出迴圈
        }
    }
    while (1)                                               //詢問第二個數的迴圈
    {
        cout << "Please enter second number( >0 ):";        //輸出"Please enter second number( >0 ):"到螢幕上
        cin >> second_number;                               //輸入一個值給second_number
        if (second_number<=0)                               //判斷second_number是否符合範圍
        {
            cout << "Second number is out of range!!\n";    //輸出"Second number is out of range!!\n"到螢幕上
        }
        else
        {
            break;                                          //跳出迴圈
        }
    }
    for (a=1;a<=first_number;a++)                           //重覆詢問並累加a直到小於等於first_number
    {
        for (b=1;b<=second_number;b++)                      //重覆詢問並累加b直到小於等於second_number
        {
            sum = a * b;                                    //計算sum的值
            cout << a << "*" << b << "=" << sum;            //輸出a"*"b"="和結果到螢幕上
            cout << endl;                                   //輸出換行
        }
        cout << endl;                                       //輸出換行
    }

    return 0;
}
