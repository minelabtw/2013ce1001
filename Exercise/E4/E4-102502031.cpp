#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int category;
    cin >> category;

    //determine which category is enter
    switch(category)
    {
    case 1:                                                              //category is 1
        for (int x=1; x<=9; x=x+1)
        {
            for (int y=1; y<=9; y=y+1)
                cout << x << "*" << y << "=" << setw(2) << x*y << " ";   //print the result in one line as x is fixed
            cout << endl;                                                //make a newline as y is 9
        }
        break;                                                           //exit switch
    case 2:                                                              //category is 2
        for (int y=1; y<=9; y=y+1)
        {
            for (int x=1; x<=9; x=x+1)
                cout << x << "*" << y << "=" << setw(2) << x*y << " ";   //print the result in one line as y is fixd
            cout << endl;                                                //make a newline as x is 9
        }
        break;                                                           //exit switch
    default:                                                             //category is neither 1 nor 2
        cout << "Error!";                                                //print error massage
        break;                                                           //exit switch
    }
    return 0;
}
