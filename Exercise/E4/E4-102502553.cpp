#include<iostream>
using namespace std;
int main()
{
    int a = 0; //宣布一個整數變數，並初始化為0
    int b = 0; //宣布一個整數變數，並初始化為0

    do //先做括號裡的程式
    {
        cout << "Please enter first number( >0 ):"; //顯現字串
        cin >> a; //輸入數字
        if ( a <= 0 )
            cout << "First number is out of range!!" << endl;
    }
    while( a <= 0 ); //當a <= 0時，回去找最近的do重做一次

    do
    {
        cout << "Please enter second number( >0 ):";
        cin >> b;
        if ( b <= 0 )
            cout << "Second number is out of range!!" << endl;
    }
    while( b <= 0 );

    for ( int c = 1; c <= a; c++ ) //令初始值c = 1，執行到c <= a，每做一次c+1
    {
        for ( int d = 1; d <= b; d++ )
        {
            cout << c << "*" << d << "=" << c * d << endl;
        }
        cout << endl;
    }
    return 0; //結束
}
