#include<iostream>
using namespace std;

int main()
{
    int x=0,y=0,i=0,j=0;

    while(x<=0)                      //輸入數值
    {
        cout<<"Please enter first number( >0 ):";
        cin>>x;
        if(x<=0)
            cout<<"First number is out of range!!"<<endl;
    }


    while(y<=0)                      //輸入數值
    {
        cout<<"Please enter second number( >0 ):";
        cin>>y;
        if(y<=0)
            cout<<"Second number is out of range!!"<<endl;
    }

    for (i=1; i<=x; i=i+1)           //建立第一個迴圈，讓乘式分段
    {
        for (j=1; j<=y; j=j+1)       //建立第二個迴圈，一一寫出每一個乘式
            cout<<i<<"*"<<j<<"="<<i*j<<endl;
        cout<<endl;
    }

    return 0;
}
