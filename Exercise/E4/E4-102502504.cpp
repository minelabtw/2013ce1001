#include <iostream>

using namespace std;

int main()
{
    int n1,n2;    //宣告變數n1和n2
    int a=1,b=1 ; //宣告變數a和b
    int f;        //宣告變數f

    cout << "Please enter first number( >0 ):"; //顯示Please enter first number( >0 ):
    cin >> n1; //輸入第一個數字
    while (n1<=0) //利用while迴圈限制n1>0，若不符合則顯示"First number is out of range!!"，並要求重新輸入n1
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):" ;
        cin >> n1;

    }

    cout << "Please enter second number( >0 ):"; //顯示Please enter second number( >0 ):
    cin >> n2; //輸入第二個數字
    while (n2<=0) //利用while迴圈限制n2>0，若不符合則顯示"Second number is out of range!!"，並要求重新輸入n2
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):" ;
        cin >> n2;
    }

    while (a<=n1) //當a<=n1時，執行下方迴圈
    {
        while (b<=n2)  //當b<=n2時，執行下方迴圈
        {
            f=a*b;
            cout << a << "*" << b << "=" << f << endl ; //顯示a*b=f
            b++; //上方已定義b=1，因此為1<=b++<=n2的數字
        }
        cout << endl; //換行
        b=1; //使b值回復0
        a++; //上方已定義a=1，因此為1<=a++<=n1的數字
    }
    return 0;
}
