#include<iostream>

using namespace std;
main(){
    int a=0,b=0;    //a&b are the first and the second number.
    //input a
    while(a<=0){
        cout << "Please enter first number( >0 ):";
        cin >> a;
        if(a<=0)    //out of range
            cout << "First number is out of range!!" << endl;
    }
    //input b
    while(b<=0){
        cout << "Please enter second number( >0 ):";
        cin >> b;
        if(b<=0)    //out of range
            cout << "Second number is out of range!!" << endl;
    }
    //output
    for(int i=1;i<=a;i++){
        for(int j=1;j<=b;j++){
            cout << i << "*" << j << "=" << i*j << endl;
        }
        cout << endl;
    }



    return 0;
}
