#include <iostream>
using namespace std;

int main()
{
    int factor1; //call variable
    int factor2; //call variable
    int x; //call variable
    int y; //call variable

    do
    {
        cout << "Please enter first number( >0 ): "; //display words
        cin >> factor1; //input first factor

        if (factor1<=0) // data validation
        {
            cout << "First number is out of range!!" << endl; //return words for improper input
        }

    }
    while(factor1<=0);

    do
    {
        cout << "Please enter second number( >0 ):" ; // display words
        cin >> factor2; //input 2nd factor

        if(factor2<=0)//data validation
        {
            cout << "Second number is out of range!!" << endl; // display words for improper input
        }
    }
    while(factor2<=0);



    x=1; //set x=1 to count for displaying number of set of multiplication
    while(x<=factor1)
    {
        y=1; //set y=1 to count for number of times need to do in each set of multiplication
        while(y<=factor2)
        {
            cout << x <<"*"<< y << "=" << x*y << endl; //calculate for the product
            y++; //y+1 to see whether it continuesh for the looping
        }
        x++; //y+1 to see whether it continues for the looping
        cout << endl;
    }

    return 0;
}
