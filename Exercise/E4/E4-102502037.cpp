#include <iostream>

using namespace std;

int main()
{
    int w,h,x,y,z; //設定五個變數

    cout<<"Please enter first number( >0 ):";//輸出說明到螢幕上
    cin>>w; //輸入w.h
    while (w <= 0) //判別wh是否合格 否則重新輸入
    {
        cout << "First number is out of range!!" << endl <<  "Please enter first number( >0 ):";
        cin >> w;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>h;
    while (h <= 0)
    {
        cout << "Second number is out of range!!" << endl <<  "Please enter second number( >0 ):";
        cin >> h;
    }
    for(y=1 ; y<=w ; y++)  //y由1至w遞增
    {
        for(x=1 ; x<=h ; x++) //X由1至h遞增
        {
            z=x*y;
            cout<<y<<"*"<<x<<"="<<z<<endl; //輸出並且換行
        }
        cout<<endl; //當一列X輸出完之後換行
    }

    return 0;
}
