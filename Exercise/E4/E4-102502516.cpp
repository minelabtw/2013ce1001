#include <iostream>

using namespace std;

int main()
{
    int first_number,first_count=1;
    int second_number,second_count=1;
    //宣告用來儲存使用者定義的兩個數字的變數和迴圈的計數器

    cout << "Please enter first number. ( >0 ):" ;
    cin >> first_number;
    while (first_number <= 0 )
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number. ( >0 ):" ;
        cin >> first_number;
    }
    //請使用者輸入第一個數字，並判斷其值是否大於零

    cout << "Please enter second number. ( >0 ):" ;
    cin >> second_number;
    while (second_number <= 0 )
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter Second number. ( >0 ):" ;
        cin >> second_number;
    }
    cout << endl;
    //請使用者輸入第二個數字，並判斷其值是否大於零

    while (first_count <= first_number )
    {
        while (second_count <= second_number)
        {
            cout << first_count << "×" << second_count << "=" << first_count*second_count << endl;
            //輸出算式:第一個數字*第二個數字=其乘積
            second_count ++;                //每算完一行便將第二項加一
        }
        cout << endl<< endl;                //空兩行以分開段落
        first_count ++;                     //每完成一個段落便將第一個數字加一
        second_count = 1;                   //將第二個數字回復成1以利下次計算
    }

    return 0;
}
0
