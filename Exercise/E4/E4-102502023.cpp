#include <iostream>
using namespace std;

int main()
{
    int a,b; //initialize integer a and b
    cout << "Please enter first number( >0 ):" ;
    cin >> a;
    while ( a <= 0) // while loop to examine whether a is in demand
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):" ;
        cin >> a;
    }

    cout << "Please enter second number( >0 ):" ;
    cin >> b;
    while ( b <=0) // while loop to examine whether b is in demand
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):" ;
        cin >> b;
    }

    int x,y,z; //initialize integer a,b,and c
    for (x=1; x<=a; x++) // for loop to print first number
    {
        for(y=1; y<=b; y++) // for loop to print second number
        {
            z=x*y; // to calculate the first number and second number multiplication
            cout << x <<"*" << y << "=" << z << endl; // to print the result of above multiplication
        }
        cout << endl; //每一列結束後顯示+++並換行
    }
    return 0;
}
