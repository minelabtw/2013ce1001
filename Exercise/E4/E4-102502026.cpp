//E4-102502026
//table multiplication

#include <iostream>
using namespace std;

main()//start the program
{
    int a;//define a as the first number
    int b;//define b as the second number
    int c=1;//define c = 1 as a counter
    int d=1;//define d = 1 as a another counter
    int R;// for the result of the product


    cout<<"Please enter the first number ( >0 ):";// ask the first number
    cin>>a;

    while (a<=0)//do it until a >0
    {
        cout<<"First number is out of range!!!\n";//wrong
        cout<<"Please enter the first number ( >0 ):";//ask again the number
        cin>>a;
    }

    cout<<"Please enter the second number ( >0 ):";//ask the second number
    cin>>b;

    while (b<=0)//do it until b >0
    {
        cout<<"Second number is out of range!!!\n";//wrong
        cout<<"Please enter the second number ( >0 ):";//ask again the number
        cin>>b;
    }



    while (c<=a) //do it until c >a
    {
        while(d<=b)//do it until d > b
        {
            R= c*d; // formula for the product
            cout<<c<<"*"<<d<<"="<<R<<"\n";// print for the result
            d++; // d= d+1
        }
        cout<<"\n"; //jump a line
        d=1; //return d = 1 because if u dont use the second while cant fo it
        c++; // c= c+1
    }
    return 0;
}//end program
