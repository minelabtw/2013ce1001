#include<iostream>
using namespace std;
int main(void)
{
    int a=0,b=0;//設定所輸入之數字初始值=0
    cout << "Please enter first number( >0 ):" ;//顯示輸入字元與第一個輸入數字
    cin >> a;
    while(a<=0)//設定a若小於0,重新輸入
    {
        cout << "First number is out of range!!" <<endl;
        cout << "Please enter first number( >0 ):" ;
        cin >> a;
    }
    cout << "Please enter second number( >0 ):" ;//輸入字元與第二個輸入數字
    cin >> b;
    while(b<=0)//設定b若小於0,重新輸入
    {
        cout << "Second number is out of range!!" <<endl;
        cout << "Please enter second number( >0 ):" ;
        cin >> b;
    }
    int x=1,y=1;//設定x,y初始值=1
    while(x<=a)//設定執行完裡面的迴圈,才會讓x+1,繼續執行裡面的迴圈,直到x=輸入的第一個值
    {
        while(y<=b)//設定y會漸增到輸入的第二個數字之值,才會重新執行第一個迴圈
        {
            cout << x << "*" << y << "=" << x*y <<endl;
            y++;//y遞增
        }
        y=1;
        cout <<endl;
        x++;
    }

    return 0;
}
