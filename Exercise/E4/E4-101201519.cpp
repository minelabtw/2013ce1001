#include <iostream>
using namespace std;

int main()
{

    int number1=0,number2=0;//另變數number1,number2

    while(number1<=0)//判斷number1的範圍是否>0
    {
        cout << "Please enter first number( >0 ):";
        cin >> number1;
        if(number1<=0)
            cout << "First number is out of range!!" << endl;
    }
    while(number2<=0)//判斷number2的範圍是否>0
    {
        cout << "Please enter second number( >0 ):";
        cin >> number2;
        if(number2<=0)
            cout << "Second number is out of range!!" << endl;
    }
    int x=1,y=1;//迴圈變數
    while(x<=number1)
    {
        while(y<=number2)
        {
            cout << x << "*" << y << "=" << x*y << endl;//輸出乘法表
            y++;
        }
        cout << endl;//換行區隔乘法表
        y=1;//y回歸1在執行一次迴圈
        x++;
    }
    return 0;

}
