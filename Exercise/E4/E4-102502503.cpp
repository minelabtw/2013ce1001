#include <iostream>
using namespace std;

int main()
{
    int first;  //宣告名稱為first的整數變數
    int second;  //宣告名稱為second的整數變數
    int a = 1;  //宣告名稱為a的整數變數,將其值設為1
    int b = 1;  //宣告名稱為b的整數變數,將其值設為1

    cout << "Please enter first number( >0 ):";  //輸出字串
    cin >> first;  //將輸入的值給first
    while (first<=0)  //使用while迴圈，若first數值小於等於0就會一直重複該程式碼內的動作，否則跳出迴圈
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";
        cin >> first;
    }

    cout << "Please enter second number( >0 ):";
    cin >> second;
    while (second<=0)  //使用while迴圈，若second數值小於等於0就會一直重複該程式碼內的動作，否則跳出迴圈
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):";
        cin >> second;
    }

    while (a<=first)  //使用while迴圈，若a數值小於等於first就會一直重複該程式碼內的動作，否則跳出迴圈
    {
        while (b<=second)  //使用while迴圈，若b數值小於等於second就會一直重複該程式碼內的動作，否則跳出迴圈
        {
            cout << a << "*" << b << "=" << a*b << endl;  //輸出方程式並換行
            b++;  //b遞增
        }
        cout << endl;
        a++;  //a遞增
        b = 1;  //將b值重設為1
    }
    return 0;
}
