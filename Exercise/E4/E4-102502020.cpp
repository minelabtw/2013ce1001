#include <iostream>

using namespace std;

int main()
{
    int number1 = 0;      //宣告名稱為number1的整數，並初始化其數值為0。
    int number2 = 0;      //宣告名稱為number2的整數，並初始化其數值為0。
    int x = 1;            //宣告名稱為x的整數，並初始化其數值為1。
    int y = 1;            //宣告名稱為y的整數，並初始化其數值為1。

    cout << "Please enter first number( >0 ):";               //將"Please enter first number( >0 ):"輸出到螢幕上。
    cin >> number1;                                           //輸入整數number1。

    while(number1<=0)                                         //當number1小於等於0時，執行以下動作。
    {
        cout << "First number is out of range!!" << endl;     //將"First number is out of range!!"輸出到螢幕上，並換行。
        cout << "Please enter first number( >0 ):";
        cin >> number1;
    }

    cout << "Please enter second number( >0 ):";              //將"Please enter second number( >0 ):"輸出到螢幕上。
    cin >> number2;                                           //輸入整數number2。
    while(number2<=0)                                         //當number2小於等於0時，執行以下動作。
    {
        cout << "Second number is out of range!!" << endl;    //將"Secomd number is out of range!!"輸出到螢幕上，並換行。
        cout << "Please enter second number( >0 ):";
        cin >> number2;
    }

    while(x<=number1)                                         //當x小於等於0時，執行以下動作。
    {
        while(y<=number2)                                     //當y小於等於0時，執行以下動作。
        {
            cout << x << "*" << y << "=" << x*y << endl;      //將"x*y=x*y"輸出到螢幕上。
            y++;                                              //y從起始值開始逐1遞增。
        }
        cout << endl;                                         //換行
        x++;                                                  //x從起始值開始逐1遞增。
        y=1;                                                  //y回歸成起始值。
    }

    return 0;
}

