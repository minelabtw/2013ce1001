#include <iostream>
using namespace std;

int main()
{
    int a=0,b=0;
    cout<<"Please enter first number( >0 ):";
    cin>>a;//輸入a
    while(a<=0)
    {
        cout<<"First number is out of range!!\nPlease enter first number( >0 ):";
        cin>>a;
    }
    cout<<"Please enter second number( >0 ):";
    cin>>b;
    while(b<=0)
    {
        cout<<"Second number is out of range!!\nPlease enter second number( >0 ):";
        cin>>b;//輸入b
    }
    for(int c = 1; c <= a; c++)//讓C依序從1到a值
    {
        for(int d =1; d <= b; d++)//讓d依序從1到d值
        {
            int x= c*d;//依序相乘 從1*1到c*d
            cout<<c<< "*" <<d<< "="<<x<<endl;//
        }
        cout<<"\n";
    }
    return 0;
}
