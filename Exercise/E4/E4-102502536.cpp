#include<iostream>

using namespace std;

int main()
{
    int number1 , number2;//宣告變數number1,number2
    int a=1 , b=1;//宣告變數a,b 初始值為1

    cout << "Please enter first number( >0 ):";//顯示"Please enter first number( >0 ):"字串
    cin  >> number1;//輸入數字給number1

    while(number1<=0)//當number1小於等於0進入迴圈
    {
        cout << "First number is out of range!!" << endl;//顯示"First number is out of range!!"字串並換行
        cout << "Please enter first number( >0 ):";//顯示"Please enter first number( >0 ):"字串
        cin  >> number1;//輸入數字給number1
    }

    cout << "Please enter second number( >0 ):";//顯示"Please enter second number( >0 ):"字串
    cin  >> number2;//輸入數字給number2

    while(number2<=0)//當number2小於等於0進入迴圈
    {
        cout << "Second number is out of range!!" << endl;//顯示"Second number is out of range!!"字串並換行
        cout << "Please enter second number( >0 ):";//顯示"Please enter second number( >0 ):"字串
        cin  >> number2;//輸入數字給number2
    }

    while(b<=number2)//當b<=number2進入迴圈
    {
        while(a<=number1)//當a<=number1進入迴圈
        {
            cout << b << "*" << a << "=" << a*b << endl;//輸出b*a的運算結果並換行
            a++;//把a值加1重新進行運算
        }

        cout << endl;//a>number1跳出迴圈時換行
        a=1;//a值回到1
        b++;//把b值加1重新進入迴圈執行運算

    }

    return 0;

}
