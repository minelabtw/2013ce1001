#include <iostream>

using namespace std;

int main()
{
    int a,b;
    cout<<"Please enter first number( >0 ):";
    cin>>a;
    while(a<=0)
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>a;
    }
    cout<<"Please enter second number( >0 ):";
    cin>>b;
    while(b<=0)
    {
        cout<<"Second number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }
    int i,j;
    for(i=1;i<=a;i++)
    {
        for(j=1;j<=b;j++)
        {
            cout<<i<<'*'<<j<<'='<<i*j<<endl;
        }
        cout<<endl;
    }
    return 0;
}
