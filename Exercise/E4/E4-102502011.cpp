#include<iostream>
using namespace std;

int main()
{
    int number1 = 0 , number2 = 0 ;
    int mult = 0 ;

    cout << "Please enter first number( >0 ):"  ;
    cin >> number1 ;

    while(number1 <= 0 )
    {
        cout << "First number is out of range!!" << endl ;
        cout << "Please enter first number( >0 ):"  ;
        cin >> number1 ;
    }

    cout << "Please enter second number( >0 ):" ;
    cin >> number2 ;

    while(number2 <= 0)
    {
        cout <<"Second number is out of range!!" << endl ;
        cout << "Please enter second number( >0 ):" ;
        cin >> number2 ;
    }
    int x=1 ;
    int y=1 ;
    int xMax=number1 ;
    int yMax=number2 ;

    while (x <= xMax)  //先固定第一個數 讓第二個數做Y+1的迴圈
    {
        while(y <= yMax)
        {
            mult = x*y ; //因為執行完會讓Y+1 所以mult的結果會變
            cout << x << "*" << y << "=" << mult << endl ;
            y++; //讓y+1後再執行一次while
        }
        cout << endl  ;
        y = 1 ; //當y <= yMax 讓y重回1 x+1再次執行迴圈
        x++ ;
    }

    return 0;
}
