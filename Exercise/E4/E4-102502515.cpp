#include <iostream>

using namespace std;

int main()

{
    int number1 = 0;                                                        //宣告整數變數
    int number2 = 0;                                                        //宣告整數變數

    cout << "Please enter first number( >0 ):" ;                            //顯示出字串
    cin  >> number1 ;                                                       //將輸入的值給number1

    while (number1 <=0 )                                                    //設定條件(直到大於零的整數)，製造迴圈，當符合條件時，執行下列動作
    {
        cout << "First number is out of range!!" << endl;                   //顯示字串
        cout << "Please enter first number( >0 ):" ;                        //顯示字串
        cin  >> number1 ;                                                   //將輸入的值給number1，並確認條件，如果符合條件便重複此迴圈
    }

    cout << "Please enter second number( >0 ):" ;                           //顯示字串
    cin  >> number2 ;                                                       //將輸入的值給number2

    while (number2 <=0 )                                                    //製造迴圈，設定條件(直到大於零的整數)，當符合條件時，執行下列動作
    {
        cout << "Second number is out of range!!" << endl;                  //顯示字串
        cout << "Please enter second number( >0 ):" ;                       //顯示字串
        cin  >> number2 ;                                                   //將輸入的值給number2，並回到此迴圈條件，符合條件便重複執行規定的動作
    }


    int max1 = 1;                                                            //宣告整數變數，使迴圈可重複執行。並初始化其值為1
    int max2 = 1;                                                            //宣告整數變數，使迴圈可重複執行，並初始化其值為1

    while (max1 <= number1)                                                  //製造迴圈，設定條件(迴圈重複執行的次數)，符合條件時執行下列動作
    {
        while (max2 <= number2)                                              //製造迴圈，設定條件(迴圈重複執行的次數)，符合條件時執行下列動作
        {
            cout << max1 << "*" << max2 << "=" << max1 * max2 << endl;       //顯示字串並跳行
            max2++;                                                          //將宣告變數+1，並再次確認條件是否符合，符合情況下，重複執行下列動作
        }
        cout << endl;                                                        //跳行
        max2 = 1 ;                                                           //將宣告變數初始化到1，以利迴圈內的迴圈可以重複執行
        max1++ ;                                                             //宣告變數+1，並重複此while迴圈，直到條件不符
    }

    return 0 ;
}
