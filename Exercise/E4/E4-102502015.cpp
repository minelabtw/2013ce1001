#include <iostream>
using namespace std;

int main()
{
    int a;                                                  //定義變數 A = 第一個數 B= 第二個數 C,D=迴圈用計數  E=C*D
    int b;
    int e;
    cout<<"Please enter first number( >0 ):";
    cin>>a;
    while(a<=0)                                             //A小於等於0 則重輸
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>a;
    }
    cout<<"Please enter second number( >0 ):";
    cin>>b;
    while(b<=0)                                             //B小於等於0 則重輸
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }
    for(int c=1; c<a+1; c++)                                //從1開始迴圈 迴圈到a
    {
        for(int d=1; d<b+1; d++)                            //從1開始迴圈 迴圈到b
        {
            e=c*d;                                          //計算結果
            cout<<c<<"*"<<d<<"="<<e<<endl;
        }
        cout<<endl;
    }
    return 0;
}
