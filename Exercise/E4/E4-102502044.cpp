/*************************************************************************
    > File Name: E4-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年10月16日 (週三) 18時18分44秒
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>

/******************************************/
/*  str means an string for ui message	  */
/*  err means an string for error message */
/* *tmp means a point to save score		  */
/******************************************/
void input(const char *str, const char *err, int *tmp)
{
	for(;;)
	{
		printf(str); //input number
		scanf("%d", tmp);

		if(*tmp > 0) //check input is in range (0, unlimit)
			break;
		else
			puts(err);
	}
}

int main()
{
	/*****************************/
	/* x means the  first number */
	/* y means the second number */
	/*****************************/
	int x, y;

	/* input all variable */
	input("Please enter first number( >0 ):", "First number is out of range!!", &x);
	input("Please enter second number( >0 ):", "Second number is out of range!!", &y);

	/* print the  9*9 multi table*/
	for(int i=1 ; i<=x ; putchar('\n'), i++)
		for(int j=1 ; j<=y ; j++)
			printf("%d*%d=%d\n", i, j, i*j);
	
	return 0;
}

