#include <iostream>

using namespace std;

int main()
{
    int first_number=0;
    int second_number=0;

    cout<<"Please enter first number( >0 ):";
    cin>>first_number;

    while (first_number<=0)
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>first_number;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>second_number;

    while (second_number<=0)
    {
        cout<<"Second number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>second_number;
    }

    int x=1;
    int y=1;

    while (x<=first_number)
    {
        while (y<=second_number)
        {
            cout<<x <<"*"<<y<<"="<<x*y;
            cout<<endl;
            y++;
        }
        cout<<endl;
        cout<<endl;
        y=1;
        x++;
    }


    return 0;
}
