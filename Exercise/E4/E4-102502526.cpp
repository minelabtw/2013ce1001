#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a=0;     //宣告一個整數變數，初始化其值為零
    int b=0;     //宣告一個整數變數，初始化其值為零
    int x=1;     //宣告一個整數變數，初始化其值為一，做以計算之變數
    int y=1;     //宣告一個整數變數，初始化其值為一，做以計算之變數

    cout<<"Please enter first number( >0 ):";  //輸出文字至螢幕
    cin>>a;                                    //將所輸入之值代入變數a
    while(a<=0)                                //迴圈，若a小於等於0，則輸出First number is out of range!!至螢幕，並可重新輸入
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>a;
    }

    cout<<endl;
    cout<<"Please enter second number( >0 ):";
    cin>>b;
    while(b<=0)                                //作用同上一個迴圈
    {
        cout<<"Second number is out of range"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }

    while(x<=a)                              //當進行計算的變數x小於等於我們所輸入的變數a時
    {
        while(y<=b)                          //當進行計算的變數y小於等於我們所輸入的變數b時
        {
            cout<<x<<'*'<<y<<'='<<x*y<<endl; //輸出運算過程，且將a、b值代換成x、y值，輸出至螢幕上
            y++;                             //y從初始值跑迴圈，每次+1，直至y=b停止
        }
        x++;                                 //當上面的迴圈y>b時，跳過上述迴圈，然後x+1
        y = 1;                               //當上面的迴圈y>b時，y值回到1
        cout<<endl;
    }

    return 0;
}
