#include <iostream>
using namespace std;

int main()
{
    int a=0;
    int b=0;
    int c=1;
    int d=1;                                           //宣告變數
    cout<<"Please enter first number( >0 ):";
    cin>>a;
    while(a<=0)                                        //判斷a的範圍是否符合
    {
        cout<<"First number is out of range!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>a;
    }
    cout<<"Please enter second number( >0 ):";
    cin>>b;
    while(b<=0)                                        //判斷b的範圍是否符合
    {
        cout<<"Second number is out of range!!\n";
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }
    while(d<=b)                                        //迴圈做出表格
    {
        while(c<=a)
        {
            cout<<d<<"*"<<c<<"="<<c*d<<"\n";
            c++;
        }
        cout<<endl;
        c=1;
        d++;
    }
    return 0;
}
