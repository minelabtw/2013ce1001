#include<iostream>
using namespace std;
int main()
{
    int integer1=0;//宣告變數
    int integer2=0;
    int x=1;
    int y=1;
    cout <<"Please enter first number(>0):";//輸入第一個數
    cin >>integer1;
    while (integer1<=0)//當第一個數<=0時
    {
        cout<<"First number is out of range!!"<<endl<<"Please enter first number(>0):";//跑出警告,換行,再輸入一次
        cin>>integer1;
    }
    cout<<"Please enter second number(>0):";//輸入第二個數
    cin>>integer2;
    while (integer2<=0)//當第二個數<=0時
    {
        cout<<"Second number is out of range!!"<<endl<<"Please enter second number(>0):";//跑出警告,換行,再輸入一次
        cin>>integer2;
    }


    for(x=1; x<=integer1; x+=1)//進入迴圈,x初始值=1,不大於第一個數,每次加1
    {
        for(y=1; y<=integer2; y+=1)//y初始值=1,不大於第二個數,每次加1
        {
            cout <<x<<"*"<<y<<"="<<x*y;//跑出公式
            cout<<endl;//內迴圈跑完空行
        }

        cout<<endl;//外迴圈跑完空行
    }
    return 0;
}
