#include<iostream>
using namespace std;
int main()
{
    int x;//宣告一變數為第一數
    int y;//宣告一變數為第二數
    int line;//宣告一變數line為有幾個區塊
    int number;//宣告一變數number為要乘到幾
    cout <<"Please enter first number( >0 ):";
    cin >>x;
    while (x<=0)//做一迴圈讓第一數>0
    {
        cout <<"First number is out of range!!"<<endl;
        cout <<"Please enter first number( >0 ):";
        cin >>x;
    }
    cout <<"Please enter second number( >0 ):";
    cin >>y;
    while (y<=0)//做一迴圈讓第二數>0
    {
        cout <<"Second number is out of range!!"<<endl;
        cout <<"Please enter second number( >0 ):";
        cin >>y;
    }
    for(line=1; x>=line; line++)//做雙迴圈使得每一區塊間空一行，每一區塊跑到y就停止
    {
        for (number=1; y>=number; number++)
        {
            cout <<line<<"*"<<number<<"="<<line*number<<endl;
        }
        cout <<endl;
    }
    return 0;
}
