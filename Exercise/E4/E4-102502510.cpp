#include <iostream>

using namespace std;

int main()
{
    int firstint=0;
    int secondint=0;
    while(firstint<=0)//輸入第一個數
    {
        cout << "Please enter first number( >0 ):";
        cin >> firstint;
        if(firstint<=0)//判斷合理性
            cout << "First number is out of range!!" << endl;
    }
    while(secondint<=0)//輸入第二個數
    {
        cout << "Please enter second number( >0 ):";
        cin >> secondint;
        if(secondint<=0)//判斷合理性
            cout << "Second number is out of range!!" << endl;
    }
    for(int i=1;i<=firstint;++i)//輸出乘法表
    {
        for(int j=1;j<=secondint;++j)
        {
            cout << i << "*" << j << "=" << i*j << endl;
        }
        cout << endl;
    }
    return 0;
}
