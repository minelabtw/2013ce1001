#include <iostream>
using namespace std;

int main()
{
    int num1;//宣告外圈迴圈次數
    int num2;//宣告內圈迴圈次數

    //檢查num1值
    while(true)
    {
        cout<<"Please enter first number( >0 ):";
        cin>>num1;

        if(num1<=0)
            cout<<"First number is out of range!!"<<endl;
        else
            break;
    }

    //檢查num2值
    while(true)
    {
        cout<<"Please enter second number( >0 ):";
        cin>>num2;

        if(num2<=0)
            cout<<"Second number is out of range!!"<<endl;
        else
            break;
    }

    //輸出乘法表
    for(int i=1; i<=num1; i++)
    {
        for(int j=1; j<=num2; j++)
        {
            cout<<i<<"*"<<j<<"="<<i*j<<endl;
        }
        cout<<endl;
        cout<<endl;//沒空兩行一定要扣分啦!!!!!
    }

    return 0;
}
