#include<iostream>
using namespace std;
int main()
{
    int x,y;                                             //define the vairbles x and y
    int n=1,m=1;                                         //define the vairbles n and m, and give them to value 1
    do                                                   //Line 7~21:Let user input the value to x and y, and make sure that value don't smaller than 0
    {
        cout<<"Please enter first number( >0 ):";
        cin>>x;
        if(x<=0)
        {
            cout<<"First number is out of range!!\n";
        }
    }
    while(x<=0);
    do
    {
        cout<<"Please enter second number( >0 ):";
        cin>>y;
        if(y<=0)
        {
            cout<<"Second number is out of range!!\n";
        }
    }
    while(y<=0);
    while(n<=x)                                            //Line 23~35:print the Multiplication table
    {
        while(m<=y)
        {
            cout<<n<<"*"<<m<<"="<<n*m<<endl;
            m++;
        }
        if(n==x)
            break;                                           //when the Multiplication table show up, ober the loop before the string "+++" print again
        cout<<"+++"<<endl;
        m=1;
        n++;
    }
    return 0;

}
