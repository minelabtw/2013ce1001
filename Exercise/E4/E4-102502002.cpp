#include<iostream>
using namespace std;

int main()
{
    int num1=1;      //宣告一整數變數num1，並令其初始值為1。
    int num2=1;      //宣告一整數變數num2，並令其初始值為1。
    int no1=1;       //宣告一整數變數no1，並令其初始值為1。
    int no2=1;       //宣告一整數變數no2，並令其初始值為1。
    int mult=0;      //宣告一整數mult，並令其初始值為0，mult=no1*no2。

    cout << "Please enter first number( >0 ):";      //輸出"Please enter first number( >0 ):"於螢幕。
    cin >> num1;                                     //輸入num1的值。

    while (num1<=0)                      //當num1小於等於0。
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";  //輸出"First number is out of range!!"，換行後再重新輸入num1值。
        cin >> num1;                     //輸入num1值。
    }

    cout << "Please enter second number( >0 ):";      //輸出"Please enter second number( >0 ):"於螢幕。
    cin >> num2;                                      //輸入num2的值。

    while (num2<=0)                      //當num2小於等於0。
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):"; //輸出"Second number is out of range!!"，換行後再重新輸入num2值。
        cin >> num2;                     //輸入num2值。
    }

    while (no1<=num1)                    //當no1小於等於num1。
    {
        while (no2<=num2)                //當no2小於等於num2。
        {
            mult = no1*no2;              //令mult等於no1和no2的積。
            cout << no1 << "*" << no2 << "=" << mult << endl;  //輸出no1和no2的乘積等式。
            no2++;                       //no2遞增。
        }
        cout << endl;                    //換行。
        no2=1;                           //令no2初始值1。
        no1++;                           //no1遞增。
    }

    return 0;
}
