#include<iostream>

using namespace std;

int main()
{
    int integer1=0,x=0;                                           //宣告整數
    int integer2=0,y=0;
    int i=1;
    int r=1;


    cout << "Please enter first number( >0 ):";
    cin >> integer1 ;
    while(integer1<=0)                                            //無限循環，當integer1不大於零時
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> integer1 ;
    }
    x=integer1;                                                   //宣告x為輸入的第一個整數


    cout << "Please enter second number( >0 ):";
    cin >> integer2 ;
    while(integer2<=0)
    {
        cout << "second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> integer2 ;
    }
    y=integer2;


    while(i<=x)
    {
        while(r<=y)
        {
            cout << i << "*" << r << "=" << i*r <<endl;           //輸出i*r=其值後換行
            r++;                                                  //將r跳至r+1
        }
        cout <<endl;                                              //結束後換行(此即為空一行)
        r=1;                                                      //將r重置為1
        i++;
    }

    return 0;
}
