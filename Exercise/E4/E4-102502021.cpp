#include <iostream>

using namespace std;

int main ()
{
    int number1 = 0, number2 =0, sum = 0;
    cout << "Please enter first number (>0):" << endl;
    cin >> number1;
    while ( number1 < 0 )
    {
        cout << "First number is out of range!!";
        cin >> number1;
    }

    cout << "Please enter second number (>0):" << endl;
    cin >> number2;
    while ( number2 < 0 )
    {
        cout << "Second number is out of range!!";
        cin >> number2;
    }
    int x =1 , y=1;
    while ( x <= number1)
    {
        while ( y <= number2 )
        {
            sum = x*y ;
            cout << x << "*" << y << "=" << sum << endl;
            y++;
        }
        cout << endl;
        y=1;
        x++;
    }


}
