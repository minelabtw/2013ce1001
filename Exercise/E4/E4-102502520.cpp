#include <iostream>
using namespace std;

int main()
{
    int number1;                                    //定義一整數作為第一個輸出之數
    int number2;                                    //定義一整數作為第二個輸出之數
    int sum;                                        //定義一整數作為相乘之數

    cout<<"Please enter first number( >0 ):";       //輸出"Please enter first number( >0 ):"
    cin>>number1;                                   //輸入到number1的位置

    while (number1==0||number1<0)                   //若number1<=0，執行以下
    {
        cout<<"First number is out of range!!\n";   //輸出"First number is out of range!!"並換行
        cout<<"Please enter first number( >0 ):";   //輸出"Please enter first number( >0 ):"
        cin>>number1;                               //輸入到number1的位置
    }


    cout<<"Please enter second number( >0 ):";      //輸出"Please enter second number( >0 ):"
    cin>>number2;                                   //輸入到number2的位置

    while (number2==0||number2<0)                   //若number2<=0，執行以下
    {
        cout<<"Second number is out of range!!\n";  //輸出"Second number is out of range!!"並換行
        cout<<"Please enter second number( >0 ):";  //輸出"Please enter second number( >0 ):"
        cin>>number2;                               //輸入到number2的位置
    }



    for (int y=1;y<=number1;y++)                    //定義一整數y,初始值為1，一直加到y=number1
    {
        for(int x=1;x<=number2;x++)                 //定義一整數x,初始值為1，一直加到x=number2
        {
            sum=y*x;                                //用y*x取代sum的位置
            cout<<y<<"*"<<x<<"="<<sum<<endl;        //輸出
        }
        cout<<endl;                                 //換行
    }

    return 0;
}
