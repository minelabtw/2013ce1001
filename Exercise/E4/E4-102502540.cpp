#include <iostream>

using namespace std;

int main()
{
    int integer1,integer2; //宣告兩個整數變數

    cout << "Please enter first number( >0 ):" ; //顯示Please enter first number( >0 ):
    cin >> integer1 ; //從鍵盤輸入，並將輸入的值給integer1

    while ( integer1 <= 0 ) //迴圈當integer1小於等於0
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";//顯示First number is out of range!!並換行顯示Please enter first number( >0 ):
        cin >> integer1;
    }

    cout << "Please enter second number( >0 ):" ; //顯示Please enter second number( >0 ):
    cin >> integer2 ; //從鍵盤輸入，並將輸入的值給integer2

    while ( integer2 <= 0 ) //迴圈當integer2小於等於0
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):"; //顯示Second number is out of range!!並換行顯示Please enter second number( >0 ):
        cin >> integer2;
    }

    int a = 1; //宣告整數a其初始值為1
    int b = 1; //宣告整數b其初始值為1

    while ( a <= integer1 ) //迴圈當a小於等於integer1
    {
        b = 1; //使b值等於1
        while ( b <= integer2 ) //迴圈當b小於等於integer2
        {
            cout << a << "*" << b << "=" <<  a * b << endl; //顯示a*b=  並換行
            b++; //使b值+1
        }
        a++; //使a值+1
        cout << endl; //換行
    }

    return 0;

}
