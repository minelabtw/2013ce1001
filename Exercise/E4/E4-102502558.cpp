#include <iostream>
using namespace std;

/**************************************************/
/* *prompt means a string for information         */
/* *error means a string for shows error message  */
/* &n means to read a int without copy            */
/**************************************************/

void input(char *prompt, char *error,int &n)
{
    for (;;)
    {
        cout << prompt; // input an integer
        cin >> n;
        if (n > 0) // check input is valid
            return;
        else
            cout << error << endl;
    }
}

int main()
{
    int num_first,num_second;

    // print message and input number
    input("Please enter first number( >0 ):","First number is out of range!!",num_first);
    input("Please enter second number( >0 ):","Second number is out of range!!",num_second);

    // print table
    for (int i=1; i<=num_first; i++)
    {
        for (int j=1; j<=num_second; j++)
        {
            cout << i << "*" << j << "=" << i*j << endl;
        }
        cout << endl; // new line to seperate
    }
    return 0;
}
