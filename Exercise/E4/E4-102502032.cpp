#include <iostream>
using namespace std;

int main ()
{
    //varibles decalaration
    int x = 0;          //integer varible; initialize; used to save x (front one)
    int y = 0;          //integer varible; initialize; used to save y (later one)

    //ask for the first number and save to x
    do
    {
        cout << "Please enter first number( >0 ):";               //ask
        cin >> x;                                                 //save
        if ( x <= 0 )                                             //argu whether x is suitable or not
            cout << "First number is out of range!!" << endl;     //wrong message
    }
    while ( x <= 0 );

    //ask for the second number and save to y
    do
    {
        cout << "Please enter second number( >0 ):";               //ask
        cin >> y;                                                  //save
        if ( y <= 0 )                                              //argu whether y is suitable or not
            cout << "Second number is out of range!!" << endl;     //wrong message
    }
    while ( y <= 0 );

    //output results
    //output example ( x=2,y=3):
    /*
    1*1=1
    1*2=2
    1*3=3

    2*1=2
    2*2=4
    2*3=6

    */
    for ( int i = 1; i <= x; i ++ )                         //use i to control x's output
    {
        for ( int j = 1; j <= y; j ++)                      //use j to control y's output
            cout << i << "*" << j << "=" << i * j << endl;
        cout << endl;
    }

    return 0;
}

