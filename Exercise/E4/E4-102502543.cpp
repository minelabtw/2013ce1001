#include <iostream>
using namespace std;
int main()
{
    int a = 0; //宣告變數a
    int b = 0; //宣告變數b

    cout <<"Please enter first number( >0 ):"; //輸出
    cin >> a; //輸入變數a
    while (a <= 0 ) //重複判斷變數a是否小於等於零
    {
        cout <<"First number is out of range!!"<<endl<<"Please enter first number( >0 ):";
        cin >>a;
    }

    cout <<"Please enter second number( >0 ):";
    cin >> b;
    while ( b <= 0 ) //重複判斷變數b是否小於等於零
    {
        cout <<"Second number is out of range!!"<< endl <<"Please enter second number( >0 ):";
        cin >> b;
    }

    int c = 1; //宣告變數c等於1
    int d = 1; //宣告變數d等於1

    while (c <= a) //重複判斷c是否小於變數a
    {
        while (d <= b) //重複判斷d是否小於變數b
        {
            cout << c <<"*"<< d <<"="<< c * d << endl;
            d++; //使d值加1
        }
        cout << endl; //換行
        d = 1; //使d值回到1
        c++; //使c值加1
    }
    return 0;
}
