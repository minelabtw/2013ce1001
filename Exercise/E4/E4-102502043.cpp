#include <iostream>

using namespace std;

int main()
{
    int number1;                                      //宣告變數
    int number2;
    cout<<"Please enter first number( >0 ):";         //顯示文字
    cin>>number1;                                     //輸入數字
    while(number1<=0)                                 //數字小於等於0進入迴圈
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>number1;
    }
    cout<<"Please enter second number( >0 ):";        //顯示文字
    cin>>number2;                                     //輸入數字
    while(number2<=0)                                 //數字小於等於0進入迴圈
    {
        cout<<"Second number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>number2;
    }
    int x=1;                                           //宣告變數
    int y=1;

    while(x<=number1)                                   //黨X小於輸入數字進入迴圈
    {
        y=1;
        while(y<=number2)                               //當Y小於輸入數字進入迴圈
        {
            cout<<x<<"*"<<y<<"="<<x*y<<endl;            //顯示計算結果
            y++;                                        //Y累加
        }
        x++;                                            //X累加
        cout<<endl;                                     //換行
    }

    return 0;
}
