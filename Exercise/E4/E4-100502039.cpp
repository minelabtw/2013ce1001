#include <iostream>
using namespace std;

int main(){

    int x, y ,temp = 0;

    while(true){

        cout << "Please enter the first number( >0 ): ";
        cin >> temp;   //先將輸入存到暫存器

        if(temp<=0)    //如果輸入小於或等於0,則重新回到while迴圈
            cout << "First number is out of range!!" << endl;
        else{          //否則輸入正確,存入第一個變數,並中斷迴圈
            x = temp;
            break;
        }
    }

    while(true){       //同上

        cout << "Please enter the second number( >0 ): ";
        cin >> temp;

        if(temp<=0)
            cout << "Second number is out of range!!" << endl;
        else{
            y = temp;
            break;
        }
    }

    for(int i=1; i<=x; i++){
        for(int j=1; j<=y; j++){
            cout << i << "*" << j << "=" << i*j << endl;   //依序列出1 * 1~y 到 x * 1~y
        }
        cout << endl;
    }
}
