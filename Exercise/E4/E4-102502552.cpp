#include <iostream>
using namespace std;
int main()
{
    int x =1;//宣告跑動的一號變數
    int y =1;//宣告跑動的二號變數
    int first;//宣告輸入的一號變數
    int second;//宣告輸入的二號變數

    do{
        cout << "Please enter first number( >0 ):";//提示輸入第一個數
        cin >> first;
        if ( first <= 0)//判斷是否在規定範圍內
            cout << "First number is out of range!!" << endl;//提示數字超過範圍
    }while( first <= 0 );//迴圈檢查是否在範圍內

    do{
        cout << "Please enter second number( >0 ):";//提示輸入第二個數
        cin >> second;
        if ( second <= 0)//判斷是否在規定範圍內
            cout << "Second number is out of range!!" << endl;//提示數字超過範圍
    }while( second <= 0 );//迴圈檢查是否在範圍內

    for ( x = 1 ; x <= first; x++)//建立一個迴圈，跑動的一號變數向輸入的輸入的一號變數遞增
        {
        for ( y = 1 ; y <= second; y++)//建立一個迴圈，跑動的二號變數向輸入的輸入的二號變數遞增
            {
            cout << x << "*" << y << "=" << x * y << endl;//顯示出算式與答案并換行
            }cout << endl;//每當跑動一號的變數跑到輸入的一號變數後再換行
        }
return 0;//迴歸0
}//結束主程式
