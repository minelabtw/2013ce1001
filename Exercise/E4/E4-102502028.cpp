#include <iostream>

using namespace std;

int main()
{
    int a = 1 ;     //宣告a的整數變數且初始值=1
    int b = 1 ;     //宣告b的整數變數且初始值=1

    cout << "Please enter first number( >0 ):";     //輸出Please enter first number( >0 ):
    cin >> a ;                                      //輸入a
    while (a <= 0)                                  //當a是不合理範圍時輸出First number is out of range!!
    {
        cout << "First number is out of range!!" ;
        cin >> a ;
    }

    cout << "Please enter second number( >0 ):" ;      //輸出Please enter second number( >0 ):
    cin >> b ;                                         //輸入b
    while (b <= 0)                                     //當b在不合理的範圍時輸出Second number is out of range!!
    {
        cout << "Second number is out of range!!" ;
        cin >> b ;
    }



    int k = 1;                               //宣告整數變數k且初始值=1

    for (int i = 1; i <= a ; i++)            //2個迴圈形成乘法表//宣告變數給定範圍I+1
    {
        for ( int j = 1;j <= b ; j++)
        {
            k = i*j ;                                        //計算乘法
            cout << i << "*" << j << "=" << k << endl ;      //輸出結果
        }

     cout << endl; }         //讓每個區塊分隔



    return 0;
}
