#include<iostream>
using namespace std;
int main()
{
    int a;                                            //宣告變數a
    int b;                                            //宣告變數b
    cout<<"Please enter first number( >0 ):";         //將"Please enter first number( >0 ):"輸出到螢幕上
    cin>>a;                                           //將輸入的值丟給a
    while(a<1)                                        //在a小於1時進入迴圈
    {
        cout<<"First number is out of range!!\n";     //將"First number is out of range!!"輸出到螢幕上
        cout<<"Please enter first number( >0 ):";     //將"Please enter first number( >0 ):"輸出到螢幕上
        cin>>a;                                       //重新給定一個a的值
    }
    cout<<"Please enter second number( >0 ):";        //將"Please enter second number( >0 ):"輸出到螢幕上
    cin>>b;                                           //將輸入的值丟給b
    while(b<1)                                        //當b小於1進入迴圈
    {
        cout<<"second number is out of range!!\n";    //將"second number is out of range!!"輸出到螢幕上
        cout<<"Please enter second number( >0 ):";    //將"Please enter second number( >0 ):"輸出到螢幕上
        cin>>b;                                       //重新給定b的值
    }
    for(int c=1; c<=a; c++)                           //宣告變數c並把初始值訂為1在c小於等於a的情況下進入迴圈迴圈結束時c用c+1帶入
    {
        for(int d=1; d<=b; d++)                       //宣告變數d並把初始值訂為1在d小於等於b的情況下進入迴圈迴圈結束時d用d+1帶入
        {
            int e;                                    //宣告變數e
            e=c*d;                                    //將e的值定為c*d
            cout<<c<<"*"<<d<<"="<<e<<"\n";            //將c*d=e輸出到螢幕上
        }
        cout<<"\n";                                   //換行
    }
return 0;
}


