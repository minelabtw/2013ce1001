#include <iostream>
#include <cstdio>

int main()
{
    int multiplicand, multiplier ;  //宣告被乘數及乘數

    do
    {
        std::cout << "Please enter first number( >0 ):" ;   //輸出輸入說明
        std::cin >> multiplicand ;  //輸入被乘數
        if( multiplicand <= 0 ) //若被乘數不是正數
            std::cout << "First number is out of range!!\n" ;   //輸出警告
    }
    while( multiplicand <= 0 ) ;    //被乘數非正數時重新輸入

    do
    {
        std::cout << "Please enter second number( >0 ):" ;  //輸出輸入說明
        std::cin >> multiplier ;  //輸入乘數
        if( multiplier <= 0 )   //若乘數不是正數
            std::cout << "Second number is out of range!!\n" ;  //輸出警告
    }
    while( multiplier <= 0 ) ;  //乘數非正數時重新輸入

    for( int i = 1 ; i <= multiplicand ; puts(""),i++ ) //輸出結果
        for( int j = 1 ; j <= multiplier ; j++ )
            std::cout << i << '*' << j << '=' << i*j << '\n' ;
}
