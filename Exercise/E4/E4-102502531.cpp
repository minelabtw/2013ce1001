#include <iostream>

using namespace std;

int main ()
{
    int a=0; //宣告一個變數a,並初始化其數值為0。
    int b=0; //宣告一個變數b,並初始化其數值為0。
    int x=1; //宣告一個變數x,並初始化其數值為1。
    int y=1; //宣告一個變數y,並初始化其數值為1。


    do //進入此一迴圈
    {
        cout << "Please enter first number( >0 ):"; //將"Please enter first number( >0 ):"儲存於cout,並印在銀幕上。
        cin>>a;//輸入變數a的值。
        if (a<=0)
            cout << "First number is out of range!!"<<endl; //如果a<=0則將"illegal input!"印出螢幕。
    }
    while(a<=0);  //判斷是否還需不需繼續迴圈。

    do //進入此一迴圈。
    {
        cout << "Please enter second number( >0 ):"; //將"Please enter second number( >0 ):"儲存於cout,並印在銀幕上。
        cin>>b; //輸入變數b的值。
        if (b<=0)
            cout << "Second number is out of range!!"<<endl; //如果b<=0,則將"Second number is out of range!!"印出螢幕。
    }
    while(b<=0);   //判斷是否還需不需繼續迴圈。

    while(x<=a) //進入此一迴圈(第一層)。
    {
        while(y<=b) //進入此一迴圈(第二層)。
        {
            if(y<=b)
                cout <<x<<"*"<<y<<"="<<x*y<<endl; //算出九九乘法,使值輸出銀幕。
            y++; //每次y都向上加1。
        }
        cout<<endl; //換到下一行。
        y=1; //使y重新回到1。
        x++; //每次x都向上加1。

    }

    return 0;
}


