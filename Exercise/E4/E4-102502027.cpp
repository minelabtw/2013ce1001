#include <iostream>

using namespace std;

int main()
{
    int a ,b; //宣告兩個整數變數。
    int x= 1; //設x初始值為1
    int y = 1;//設y初始值為1
    int result=0 ; //宣告名稱為result的整數變數，並初始化其數值為0。


    cout<<"Please enter first number( >0 ):" ; //輸出"Please enter first number( >0 ):"
    cin>>a ; //輸入a

    while(a<0)
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):" ;
        cin>>a ;
    }

    cout<<"Please enter second number( >0 ):" ;
    cin>>b;

    while(b<0)
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):" ;
        cin>>b ;
    }

    while(x<=a) //在x<=a的範圍內執行
    {
        while(y<=b)//在y<=b的範圍內執行
        {
            cout<<x<<"*"<<y<<"=";
            result =a*b;
            cout<<result ;
            cout<<endl;
            y++; //y持續+1直到跳出迴圈
        }
        cout<<endl<<endl; //跳兩行
        x++; //x持續+1直到跳出迴圈
        y =1; //y回歸初始值1
    }
return 0;
}
