#include <iostream>
using namespace std;
int main()
{
    int a=0;  //first number
    int b=0;  //second number
    int mina=1;  //a start at 1
    int minb=1;  //b start at 1

    cout<<"Please enter first number( >0 ):";  //提示
    cin>>a;  //輸入前數上限
    while(a<=0)  //判定是否合理
    {
        cout<<"First number is out of range!!\n";
        cout<<"Please enter first number( >0 ):";
        cin>>a;
    }

    cout<<"Please enter second number( >0 ):";  //提示
    cin>>b;  //輸入後數上限
    while(b<=0)//判定是否合理
    {
        cout<<"Second number is out of range!!\n";
        cout<<"Please enter second number( >0 ):";
        cin>>b;
    }

    while(mina<=a)
    {
        while(minb<=b)
        {
            cout<<mina<<"*"<<minb<<"="<<mina*minb<<endl;  //輸出a*b從最小開始
            minb++;
        }
        minb=1;
        mina++;
        cout<<endl;  //換區

    }

    return 0;

}
