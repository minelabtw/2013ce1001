#include<iostream>
using namespace std;

int main()
{
    int number1=0,number2=0,x=1,y=1,result=1;  //宣告形式為整數的第一個數字跟第二數字跟結果

    cout<<"Please enter first number( >0 ):";  //輸出命令
    cin>>number1;
    while (number1<0)  //判斷是否符合條件
    {
        cout<<"First number is out of range!!\n";
        cin>>number1;
    }
    cout<<"Please enter second number( >0 ):";  //輸出命令
    cin>>number2;
    while (number2<0)  //判斷是否符合條件
    {
        cout<<"second number is out of range!!\n";
        cin>>number2;
    }
    while(x<=number1)  //輸出乘法表
    {
        while(y<=number2)
        {
            result=x*y;
            cout<<x<<"*"<<y<<"="<<result;
            y++;
            cout<<endl;
        }

        cout<<endl;
        y=1;
        x++;
    }
    return 0;
}
