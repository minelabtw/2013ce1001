#include<iostream>
using namespace std;
int main(){
	int a,b;
	bool first=true;
	cout<<"Please enter first number( >0 ):";
	while( cin>>a ){ // input a
		if( a>0 ) // check a
			break;
		cout<<"First number is out of range!!"<<endl;
		cout<<"Please enter first number( >0 ):";
	}
	cout<<"Please enter second number( >0 ):";
	while( cin>>b ){ // input b
		if( b>0 ) // check b
			break;
		cout<<"Second number is out of range!!"<<endl;
		cout<<"Please enter second number( >0 ):";
	}
	for(int i=1;i<=a;i++){ // print result
		for(int j=1;j<=b;j++)
			cout<<i<<"*"<<j<<"="<<i*j<<endl;
		if( i!=a )
			cout<<endl;
	}
	return 0;
}