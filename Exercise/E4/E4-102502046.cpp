#include <iostream>
using namespace std;

int main ()
{
    int a = 0;      //宣告整數a，且起始值為0
    int b = 0;      //宣告整數b，且起始值為0
    cout << "Please enter first number( >0 ):";
    cin >> a;       //輸入a值

    while (a<=0)    //當a小於等於0，進入while迴圈
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> a;       //再次輸入整數a
    }
    cout << "Please enter second number( >0 ):";
    cin >> b;

    while (b<=0)
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> b;       //再次輸入整數b
    }

    for ( int i=1 ; i <= a ; i++ )      //用for迴圈讓程式重複執行
    {
        for (int j=1 ; j <= b ; j++ )   //用for迴圈讓程式重複執行
        {
            cout << i << "*" << j << "=" << i*j <<endl;
        }
        cout << endl;   //換行
    }
    return 0;       //回傳整數0
}
