#include <iostream>

using namespace std;

int main()
{
    //輸入的值以及相乘結果
    int firstNum,secondNum,multipleSolution;
    //判斷值輸入是否符合條件
    int validInput1=0,validInput2=0;


    //判斷第一個輸入的值
    while(validInput1<1)
        {
            cout << "Please enter first number( >0 ):" ;
            cin >> firstNum;
            if(firstNum > 0 && firstNum < 10)
            {
                validInput1++;
            }
            else
            {
                cout << "First number is out of range!!" << endl;
            }
        }


    //判斷第二個輸入的值
    while(validInput2<1)
        {
            cout << "Please enter first number( >0 ):" ;
            cin >> secondNum;
            if(secondNum > 0 && secondNum < 10)
            {
                validInput2++;
            }
            else
            {
                cout << "Second  number is out of range!!" << endl;
            }
        }


    //印出乘法表
    for(int i=1;i<=firstNum;i++)
        {
            for(int j=1;j<=secondNum;j++)
            {
                multipleSolution=i*j;
                cout << i << "*" << j << "=" << multipleSolution << endl;


            }
            cout << " " << endl;
        }



    return 0;
}
