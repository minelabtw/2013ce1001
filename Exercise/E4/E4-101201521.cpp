#include <iostream>
using namespace std;

int main()
{
    int number1=0, number2=0, i=0, j=0; //宣告變數為整數型態並設定初始值為0。
    while(1)    //讓while永遠成立。
    {
        cout << "Please enter first number( >0 ):";     //要求輸入數字一。
        cin >> number1;
        //輸入錯誤則輸出警告並重新要求輸入，正確則跳出while。
        if (number1<=0)
            cout << "First number is out of range!!\n";
        else
            break;
    }
    while(1)
    {
        cout << "Please enter second number( >0 ):";    //要求輸入數字二。
        cin >> number2;
        //輸入錯誤則輸出警告並重新要求輸入，正確則跳出while。
        if (number2<=0)
            cout << "Second number is out of range!!\n";
        else
            break;
    }
    //外面的for跑number1，裡面的for跑number2。
    for(i=1; i<=number1; i++)
    {
        for(j=1; j<=number2; j++)
        {
            if(j == number2)    //每換一個i前跳一空行。
            {
                cout << i << "*" << j << "=" << i*j << endl;
                cout << "\n";
            }
            else
                cout << i << "*" << j << "=" << i*j << endl;
        }
    }
    return 0;
}
