#include<iostream>
using namespace std;

int main()
{
    int number1=0;     //宣告一個變數整數為first number的代數。
    int number2=0;     //宣告一個變數整數為second number的代數。

    cout << "Please enter first number (>0):";      //顯示輸入名
    cin >> number1;     //輸入後改變number1的值

    while (number1<=0)      //當它小於等於0時，進入回圈，並顯示通知其錯誤。
    {
        cout << "First number is out of range!!" << endl << "Please enter first number (>0):";
        cin >> number1;
    }

    cout << "Please enter second number (>0):";
    cin >> number2;

    while (number2<=0)
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number (>0):";
        cin >> number2;
    }

    int a=1;        //宣告乘法第一個數的變數整數值。
    int b=1;        //宣告乘法第二個數的變數整數值。
    int c=0;        //宣告一個變數整數為乘法後的值。

    while (a<=number1)      //進入乘法第一個數的迴圈，並設定小於等於輸入第一個值。
    {
        while (b<=number2)      //進入第二個數迴圈，並設定小於等於輸入的第二個值。
        {
            c=a*b;      //先計算第一數乘上第二數的值。
            cout << a << "*" << b << "=" << c << endl;      //顯示乘法

            b++;        //進入下一次的迴圈
        }

        cout << endl;
        b=1;        //設定為原本的值
        c=0;        //設定為原本的值
        a++;        //進入下一次的迴圈
    }
    return 0;
}
