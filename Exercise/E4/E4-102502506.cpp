#include<iostream>
using namespace std ;

int main()
{
    int number1 = 0;  //宣告變數
    int number2 = 0;
    int timeX = 0;  //for迴圈所用變數
    int timeY = 0;

    cout << "Please enter first number( >0 ):";  //輸入number1
    cin >> number1;

    while ( number1 <= 0 )  //若number1<=0 則執行下列迴圈至number1>0
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> number1;
    }
    cout << "Please enter second number( >0 ):";
    cin >> number2;

    while ( number2 <= 0 )  //若number2<=0 則執行下列迴圈至number2>0
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> number2;
    }
    for ( timeX = 1; timeX <= number1; timeX ++ )  //timeX from 1 to number1,loop number1 times
    {
        for ( timeY = 1; timeY <= number2; timeY ++)  //timeY from 1 to number2,loop number2 times
        {
            cout << timeX << "*" << timeY << "=" << timeX * timeY << endl;
        }
        cout << endl;
    }

    return 0;
}
