#include<iostream>

using namespace std ;

int main ()
{
// 宣告變數 a,b為整數
    int a , b ;
// 輸出與使用者輸入第一個數字
    cout << "Please enter first number( >0 ):";
    cin >> a ;
//判斷是否小於零 小於零者須重新輸入
    while ( a < 0 )
    {
        cout << "First number is out of range!! " << endl;
        cout << "Please enter first number( >0 ):";
        cin >> a ;
    }
//第二個數字
    cout << "Please enter second number( >0 ):";
    cin >> b ;

    while ( b < 0)
    {
        cout << "Second number is out of range!! " << endl;
        cout << "Please enter second number( >0 ):";
        cin >> b ;
    }
// 印出
    for ( int i = 1 ; i < a + 1 ; ++i)
    {
        for ( int j = 1 ; j < b + 1 ; ++j )
        {
            cout << i <<"*" << j << "=" <<i*j << endl ;
        }
        cout << "\n\n" ;
    }


    return 0 ;
}
