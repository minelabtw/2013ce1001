#include <iostream>
using namespace std;

int main()
{
    int first = 0;
    int second = 0; //宣告整數兩個變數，用以作為迴圈的界線
    int x = 1;
    int y = 1; //宣告兩個整數變數，作為迴圈的變數

    while (first<=0) //要求輸入一個大於0的第一整數
    {
        cout << "Please enter first number( >0 ):";
        cin >> first;
        if (first<=0)
        {
            cout << "First number is out of range!!" << endl;
        }
    }

    while (second<=0) //要求輸入一個大逾齡的第二整數
    {
        cout << "Please enter second number( >0 ):";
        cin >> second;
        if (second<=0)
        {
            cout << "Second number is out of range!!" << endl;
        }
    }

    while (x<first+1) //顯示乘法表
    {
        while (y<second+1)
        {
            cout << x << "*" << y << "=" << x*y << endl;
            y++;
        }
        cout << endl;
        y = 1;
        x++;
    }

    return 0;

}
