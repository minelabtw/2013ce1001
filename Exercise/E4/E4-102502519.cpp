#include<iostream>
using namespace std;

int main()
{
    int number1=0;    //宣告一變數number1，並使其值為0。
    int number2=0;    //宣告一變數number2，並使其值為0。

    cout << "Please enter first number( >0 ):";    //輸出Please enter first number( >0 ):至螢幕。
    cin >> number1;    //將值輸入至number1。

    while(number1<=0)    //while迴圈將範圍限制在number1<=0。
    {
        cout << "First number is out of range!!\n";    //輸出First number is out of range!!至螢幕。
        cout << "Please enter first number( >0 ):";    //輸出Please enter first number( >0 ):至螢幕。
        cin >> number1;    //將值輸入至number1。
    }

    cout << "Please enter second number( >0 ):";    //輸出Please enter second number( >0 ):至螢幕。
    cin >> number2;    //將值輸入至number2。

    while(number2<=0)    //while迴圈將範圍限制在number2<=0。
    {
        cout << "Second number is out of range!!\n";    //輸出Second number is out of range!!至螢幕。
        cout << "Please enter second number( >0 ):";    //輸出Please enter second number( >0 ):至螢幕。
        cin >> number2;    //將值輸入至number2。
    }

    int x=1;    //宣告一變數x，並使其值為0。
    int y=1;    //宣告一變數y，並使其值為0。

    while(x<=number1)    //while迴圈將範圍限制在x<=number1。
    {
        while(y<=number2)    //while迴圈將範圍限制在y<=number2。
        {
            cout << x << "*" << y << "=" << x * y << endl;    //輸出九九乘法表。
            y++;
        }
    cout << endl << endl;
    y=1;
    ++x;
    }










}
