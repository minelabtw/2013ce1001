#include <iostream>

using namespace std;

int main()
{
	int fnum , snum; // declare first number & second number

	while(true) // input first number
	{
		cout << "Please enter first number( >0 ):";
		cin >> fnum;
		if(fnum > 0)
			break;
		cout << "First number is out of range!!" << endl;
	}

	while(true) // intput second number
	{
		cout << "Please enter second number( >0 ):";
		cin >> snum;
		if(snum > 0)
			break;
		cout << "Second number is out of range!!" << endl;
	}

	for(int i = 1 ; i <= fnum ; ++i)
	{
		for(int j = 1 ; j <= snum ; ++j)
			cout << i << "*" << j << "=" << i * j << endl;
		cout << endl;
	}

	return 0;
}
