#include<iostream>
using namespace std;

int main()
{
    int x=1 ;//作為乘法表的第一個數
    int y=1 ;//作為乘法表的第二個數
    int xMax=0 ;//給使用者輸入第一個數的上界
    int yMax=0 ;//給使用者輸入第二個數的上界
    int product=0 ;//存放兩數相乘的積

    cout << "Please enter first number( >0 ):" ;
    cin >> xMax ;//要求輸入第一個上界'
    while (xMax < 0)
    {
        cout << "First number is out of range!!" << endl ;
        //判別第一個數的上界不能小於0
        cout << "Please enter first number( >0 ):" ;
        cin >> xMax ;
    }

    cout << "Please enter second number( >0 ):" ;
    cin >> yMax ;//要求輸入第二個上界
    while (yMax < 0)
    {
        cout << "Second number is out of range!!" << endl ;
        //判別第二個數的上界不能小於0
        cout << "Please enter second number( >0 ):" ;
        cin >> yMax ;
    }

    while (x<=xMax)
    {
        while (y<=yMax)
        {
            product=x*y ;//將x,y相乘的值輸入到product

            cout << x << "*" << y << "=" << product << endl ;//輸出乘法表

            if (y==yMax)
                cout << endl ;//當第一個數的乘法表結束後，空行

            y++;
        }
    x++;
    y=1;
    }

    return 0;
}

