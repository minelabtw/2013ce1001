#include <iostream>

using namespace std ;

int main()
{
    int first ;  //設定第一個變數為first。
    int second ;  //設定第二個變數為second。
    int f = 1 ;  //設定一對應first的變數為f，並將其初始值訂為1。
    int s = 1 ;  //設定一對應second的變數為s，並將其初始值訂為1。

    cout << "Please enter first number( >0 ):" ;
    cin >> first ;

    while ( first <= 0 ) //以while限制操作者的輸入範圍:若輸入的數字不大於0，即操作以下指示。
    {
        cout << "First number is out of range!!" << endl ;
        cout << "Please enter first number( >0 ):" ;
        cin >> first ;  //輸出"錯誤"，並使操作者輸入一新first值。
    }

    cout << "Please enter second number( >0 ):" ;
    cin >> second ;

    while ( second <= 0 ) //以while限制操作者的輸入範圍:若輸入的數字不大於0，即操作以下指示。
    {
        cout << "Second number is out of range!!" << endl ;
        cout << "Please enter second number( >0 ):" ;
        cin >> second ;  //輸出"錯誤"，並使操作者輸入一新second值。
    }


    while ( f <= first )  //當f小於等於first值時，操作以下指令。
    {
        while ( s <= second )  //當s小於等於second值時，操作以下指令。
        {
            cout << f << " * " << s << " = " << f * s ;  //輸出算式。
            s++ ;  //每執行完一次就加1。
            cout << endl ;
        }
        s = 1 ;  //使s值回復初始值1，
        f++ ;  //每執行完一次就加1。
        cout << endl ;
    }


    return 0 ;
}
