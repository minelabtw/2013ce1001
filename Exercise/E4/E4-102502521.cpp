#include <iostream>

using namespace std;

int main()
{
    int a=1;      //宣告名稱為a的整數變數,將其值設為1
    int b=1;      //宣告名稱為b的整數變數,將其值設為1
    int c;        //宣告名稱為c的整數變數
    int aMax;     //宣告名稱為aMax的整數變數
    int bMax;     //宣告名稱為bMax的整數變數

    cout<<"Please enter first number( >0 ):";   //將字串"Please enter first number( >0 ):"輸出到螢幕上
    cin>>aMax;                                  //從鍵盤輸入，並將輸入的值給aMax
    while(aMax<=0)   //使用while迴圈，若aMax數值小於等於0就會一直重複該程式碼內的動作
    {
        cout<<"First number is out of range!!"<<endl;  //將字串"First number is out of range!!"輸出到螢幕上並換行
        cout<<"Please enter first number( >0 ):";      //將字串"Please enter first number( >0 ):"輸出到螢幕上
        cin>>aMax;                                     //從鍵盤輸入，並將輸入的值給aMax
    }

    cout<<"Please enter second number( >0 ):";  //將字串"Please enter second number( >0 ):"輸出到螢幕上
    cin>>bMax;                                  //從鍵盤輸入，並將輸入的值給bMax
    while(bMax<=0)   //使用while迴圈，若bMax數值小於等於0就會一直重複該程式碼內的動作
    {
        cout<<"Second number is out of range!!"<<endl; //將字串"Second number is out of range!!"輸出到螢幕上並換行
        cout<<"Please enter second number( >0 ):";     //將字串"Please enter second number( >0 ):"輸出到螢幕上
        cin>>bMax;                                     //從鍵盤輸入，並將輸入的值給bMax
    }

    while(a<=aMax)      //使用while迴圈，若a小於等於aMax就會一直重複該程式碼內的動作
    {
        while(b<=bMax)  //使用while迴圈，若b小於等於bMax就會一直重複該程式碼內的動作
        {
            c=a*b;      //算式
            cout<<a<<"*"<<b<<"="<<c<<endl;   //將字串"a*b=c"輸出到螢幕上
            b++;                             //b遞增，公差為1
        }
        cout<<endl;     //換行
        b=1;            //重新設定b為1
        a++;            //a遞增，公差為1
    }

    return 0;
}
