#include<iostream>
using namespace std;
int main()
{
    int x;
    int y; //輸入的兩個整數
    int i=1;
    int j; //兩個index
    cout << "Please enter first number( >0 ):" ;
    cin >> x;
    while(x<=0)          //檢查輸入
    {
        cout << "First number is out of range!!\n" ;
        cout << "Please enter first number( >0 ):" ;
        cin >> x;
    }
    cout << "Please enter second number( >0 ):" ;
    cin >> y;
    while(y<=0)         //檢查輸入
    {
        cout << "Second number is out of range!!\n" ;
        cout << "Please enter second number( >0 ):" ;
        cin >> y;
    }
    for(; i<=x; i++)     //輸出乘法表
    {
        for(j=1; j<=y; j++)
            cout << i << "*" << j << "=" << i*j << endl;   //輸出 i * j = i*j
        cout << endl ;
    }
    return 0;
}
