#include<iostream>
using namespace std;

int main()
{
    int first_number,second_number,x,y; //宣告整數型態的first_number,second_number,x,y
    cout<<"Please enter first number( >0 ):";
    cin>>first_number;
    while(first_number<=0) //當first_number不符合條件，也就是小於等於0
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>first_number;
    }
    cout<<"Please enter second number( >0 ):";
    cin>>second_number;
    while(second_number<=0)//當second_number不符合條件，也就是小於等於0
    {
        cout<<"Second number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>second_number;
    }

    cout<<endl;
    for(y=1; y<=first_number; y++) //重覆輸出y次
    {
        for(x=1; x<=second_number; x++) //重覆輸出x次
        {
            cout<<y<<"*"<<x<<"="<<x*y<<endl; //輸出y和x相乘的值
        }
        cout<<endl;
    }
    return 0;
}
