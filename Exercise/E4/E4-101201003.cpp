#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int xMax=0;                                     //宣告型別為int的變數xMax，並初始化其數值為0
    int yMax=0;                                     //宣告型別為int的變數yMax，並初始化其數值為0


    cout << "Please enter first number( >0 ):" ;    //輸出Please enter first number( >0 ):
    cin >> xMax ;                                   //輸入 xMax
    while (xMax<0)
    {
        cout << "First number is out of range!!" << endl << "Please enter first number( >0 ):";
        cin >> xMax ;
    }                                               //當xMax<0時,顯示First number is out of range!! 換行並顯示Please enter first number( >0 ):

    cout << "Please enter second number( >0 ):" ;
    cin >> yMax ;
    while (yMax<0)
    {
        cout << "Second number is out of range!!" << endl << "Please enter second number( >0 ):";
        cin >> yMax ;
    }                                               //和xMax一樣

    int x=1 ;                                       //宣告型別為int的變數x，並初始化其數值為1
    int y=1 ;                                       //宣告型別為int的變數y，並初始化其數值為1
    int z=0 ;                                       //宣告型別為int的變數z，並初始化其數值為0

    while (x<=xMax)                                 //當x<xMax時進入迴圈
    {
        while(y<=yMax)                              //當y<yMax時進入迴圈
        {
            z=x*y ;                                 //z的值為x*y
            cout << x <<"*"<< y <<"="<< z <<endl ;  //輸出乘法表
            y++;                                    //每次跑完y的值+1
        }

        y=1 ;                                       //y的起始值為1
        x++ ;                                       //迴圈結束後，x的值要+1
        cout << endl ;                              //換行
    }

return 0 ;
}

