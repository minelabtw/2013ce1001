#include <iostream>
using namespace std;
int main()
{
    int integer1=-1 , integer2=-1;
    int x=1 , y=1;

    while(integer1<=0)
    {
        cout << "Please enter first number( >0 ):";
        cin >> integer1;
        if(integer1<=0)
            cout << "First number is out of range!!\n";
    }

    while(integer2<=0)
    {
        cout << "Please enter second number( >0 ):";
        cin >> integer2;
        if(integer2<=0)
            cout << "Second number is out of range!!\n";
    }

    while(x<=integer1)                                       //output the multiplication table
    {
        while(y<=integer2)
        {
            cout << x << "*" << y << "=" << x*y << endl;
            y++;
        }
        y=1;
        x++;
        cout << endl;
    }

    return 0;
}
