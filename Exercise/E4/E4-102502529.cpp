#include<iostream>

using namespace std ;

int main ()
{
    int a=0,b=0,t=0 ;                                           //a,b 分別為第一次與第二次輸入的數字 t用來while迴圈計次 皆設定初始值為0
    while(t<=1)
    {
        if(t==0)
        {
            cout<<"Please enter first number( >0 ):" ;         //先輸出提示
            cin>>a ;                                           //並輸入a值
            if(a>0)                                            //若符合條件 則t+1 所以跳出此if fuction
                t++;
            else
                cout<<"First number is out of range!!"<<endl; //錯誤則再一次
        }
        if(t==1)                                              //同上邏輯
        {
            cout<<"Please enter second number( >0 ):" ;
            cin>>b ;
            if(b>0)                                          //符合條件後t+1 則會跳出while 迴圈
                t++;
            else
                cout<<"Second number is out of range!!"<<endl;//不符則再一次
        }
    }
    for (int i1=1; i1<=a; i1++)                               //宣告i1用來輸出a並設定初始值 i1一直+1直到與a值相同 跳出for
    {
        for(int i2=1; i2<=b; i2++)                            //宣告i2用來輸出b並設定初始值 i2一直+1直到與b值相同 跳出for
        {
            cout<<i1<<"*"<<i2<<"="<<i1*i2<<endl ;             //輸出乘法表 先改變i2值 再換行改變i1
            if(i2==b)                                         //當輸出b次後
                cout<<endl ;                                  //用來換行
        }


    }
    return 0 ;
}
