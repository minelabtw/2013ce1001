#include <iostream>

using namespace std ;

int main ()
{
    int firstnumber=0 ,secondnumber=0 , x=1 ,y=1 ;//宣告變數firstnumber,secondnumber,x,y，並設定其初始值

    while (true)//使用迴圈
    {
       cout << " Please enter first number( >0 ):"  ;

       cin >> firstnumber  ;

       if (firstnumber<=0)
       {
           cout << "First number is out of range!!\n" ;//當輸入錯誤的值時，在螢幕上輸出"First number is out of range!!\n"
       }

       else break ;
    }

    while (true)
    {
        cout << "Please enter second number( >0 ):" ;

        cin >> secondnumber  ;

        if (secondnumber<=0)
        {
            cout << "Second number is out of range!!\n" ;

        }

        else break ;
    }

    while (x<=firstnumber)//使用迴圈
    {
        while (y<=secondnumber)
        {
            cout <<x<<"*"<<y<<"="<< x*y << "\n" ;//輸出乘法表

            y++ ;//用以限定迴圈的次數
        }
        cout << endl ;

        y=1  ;

        x++  ;

    }

    return 0 ;
}
