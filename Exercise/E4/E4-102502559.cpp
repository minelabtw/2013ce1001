#include<iostream>
using namespace std;

int main()
{
    int a,b,c,d ; //宣告4個變數
    cout <<"Please enter first number( >0 ):" ; //顯示Please enter first number( >0 ):
    cin>>a ; //將輸入的值配給a
    while(a<=0) //當a的值小於或等於0時,進入迴圈
    {
        cout << "First number is out of range!!"<<endl; //顯示"First number is out of range!!"
        cout << "Please enter first number( >0 ):" ; //顯示"Please enter first number( >0 ):"
        cin >>a ;
    }
    cout <<"Please enter second number( >0 ):" ;//顯示"Please enter second number( >0 ):"
    cin>>b;//將輸入的值配給b
    while(b<=0) //當b小於或等於0時,進入迴圈
    {
        cout << "Second number is out of range!!"<<endl;//顯示"Second number is out of range!!"
        cout << "Please enter second number( >0 ):" ;//顯示"Please enter second number( >0 ):"
        cin >>b ;//再將輸入的值配給b
    }
    for(c=1;c<=a;c++) //變數c的初始值是1,只要c小於或等於a,進入迴圈,每次c值+1
    {
        for(d=1;d<=b;d++) //變數d的初始值是1,只要d小於或等於b,進入迴圈,每次d值+1
        cout<<c<<"*"<<d<<"="<<c*d<<endl; //顯示c*d=,然後換行
    cout<<endl; //每個段落都要空一行
    }
    return 0;
}
