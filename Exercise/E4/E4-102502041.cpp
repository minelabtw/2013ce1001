#include<iostream>
using namespace std;
int main()
{
    int first_number;
    int second_number;

    cout<<"Please enter first number( >0 ):";
    cin>>first_number;
    while(first_number<=0)                              //當輸入的第一個數字不在我的範圍內時,執行這個while迴圈
    {
        cout<<"First number is out of range!!"<<endl;
        cout<<"Please enter first number( >0 ):";
        cin>>first_number;
    }

    cout<<"Please enter second number( >0 ):";
    cin>>second_number;
    while(second_number<=0)
    {
        cout<<"Second number is out of range!!"<<endl;
        cout<<"Please enter second number( >0 ):";
        cin>>second_number;
    }

    int i;                                              //輸出乘法表
    int j;
    for(i=1;i<=first_number;i++)
    {
        for(j=1;j<=second_number;j++)
        {
            cout<<i<<"*"<<j<<"="<<i*j<<endl;
        }
        cout<<endl;
    }
    return 0;

}
