#include<iostream>
using namespace std;

int main()
{
    int number1;    //宣告變數
    int number2;    //宣告變數
    int i;          //迴圈次數用
    int j;          //迴圈次數用

    cout << "Please enter first number( >0 ):";             //v取兩數字並確認在範圍內
    cin >> number1;

    while(number1<=0)
    {
        cout << "First number is out of range!!\n";
        cout << "Please enter first number( >0 ):";
        cin >> number1;
    }

    cout << "Please enter second number( >0 ):";
    cin >> number2;

    while(number2<=0)
    {
        cout << "Second number is out of range!!\n";
        cout << "Please enter second number( >0 ):";
        cin >> number2;
    }                                                       //^讀取兩數字並確認在範圍內

    for(i=1;i<=number1;i++)                                 //第一個迴圈是number1的值
    {
        for(j=1;j<=number2;j++)
        {
            cout << i << "*" << j << "=" << i*j << "\n";    //輸出number1*number2的方程式與其結果
        }
        cout << "\n";                                       //number2都乘完後換行然後換number1疊加
    }



    return 0;
}
