#include <iostream>
using namespace std;

int main ()
{
    int a = 0;      //宣告型別為 整數(int) 的第一個變數(a)，且初始化其數值為0。
    int b = 0;      //宣告型別為 整數(int) 的第二個變數(b)，且初始化其數值為0。
    cout << "Please enter first number( >0 ):"; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cin >> a;

    while (a<=0)    //使用while迴圈來檢驗使用者的輸入是否合乎標準，否則要求其重新輸入。
    {
        cout << "First number is out of range!!" << endl;
        cout << "Please enter first number( >0 ):";
        cin >> a;       //再次輸入整數a
    }
    cout << "Please enter second number( >0 ):";
    cin >> b;

    while (b<=0)
    {
        cout << "Second number is out of range!!" << endl;
        cout << "Please enter second number( >0 ):";
        cin >> b;       //再次輸入整數b
    }

    for ( int x=1 ; x <= a ; x++ )      //使用for迴圈使程式能重複計算。
    {
        for (int y=1 ; y <= b ; y++ )
        {
            cout << x << "*" << y << "=" << x*y << endl;
        }
        cout << endl;
    }
    return 0;
}
