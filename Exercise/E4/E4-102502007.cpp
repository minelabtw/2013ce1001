#include<iostream>
using namespace std;
int main()
{
    int number1=0;//declare a variable and initial it as 0
    int number2=0;//declare a varialbe and initial it as 0
    do
    {
        cout<<"Please enter first number( >0 ): ";
        cin>>number1;
        if (number1<=0)
            cout<<"First number is out of range!!"<<endl;
    }
    while(number1<=0);//loop until the correct number is input

    do
    {
        cout<<"Please enter second number( >0 ): ";
        cin>>number2;
        if (number2<=0)
            cout<<"Second number is out of range!!"<<endl;

    }
    while(number2<=0);//loop until the correct number is input

    int i=0;//declare a variable and initial it as 0 for the use of loop
    int j=0;//declare a variable and initial it as 0 for the use of loop
    for(i=1; i<=number1; i++) //i represent the the first number
    {
        for(j=1; j<=number2; j++) //j represent the second number
            cout<<i<<"*"<<j<<"="<<i*j<<endl;//print the multiple of first and second number
        cout<<endl;//the first round has completed and hence create a blank line
    }

    return 0;
}
