#include <iostream>

using namespace std;

int main(void) {
    int number_1, number_2; //declare two number

    /*input first number*/
    do {
        cout << "Please enter first number( >0 ):";
        cin >> number_1;
        cout << ((number_1 <= 0) ? "First number is out of range!!":"") << endl; //check inpput
    }while(number_1 <= 0);

    /*input second number*/
    do {
        cout << "Please enter second number( >0 ):";
        cin >> number_2;
        cout << ((number_2 <= 0) ? "Second number is out of range!!":"") << endl; //check input
    }while(number_2 <= 0);

    /*output*/
    for (int i = 1;i <= number_1; i++, cout << endl)
        for (int j = 1;j <= number_2; j++)
            cout << i << "*" << j << "=" << i*j << endl;

    return 0;
    }
