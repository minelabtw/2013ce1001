#include <iostream>
#include <string>
using namespace std;
int main()
{
    /* 優化I/O */
    ios::sync_with_stdio(false);
    /* 宣告兩整數變數 */
    int number1;
    int number2;
    /* 字串陣列str存放輸出要用的字串 */
    string str[] = {"Please enter ",
                    "first number( >0 ):",
                    "First number is out of range!!",
                    "second number( >0 ):",
                    "Second number is out of range!!"
                   };
    cout << str[0] << str[1];
    cin >> number1;
    /* 若超出範圍則進入迴圈 */
    while(number1 <= 0)
    {
        cout << str[2] << endl;
        cout << str[0] << str[1];
        cin >> number1;
    }
    cout << str[0] << str[3];
    cin >> number2;
    /* 若超出範圍則進入迴圈 */
    while(number2 <= 0)
    {
        cout << str[4] << endl;
        cout << str[0] << str[3];
        cin >> number2;
    }
    /* 兩層迴圈跑乘法表 */
    for(int i=1; i<=number1; ++i)
    {
        for(int j=1; j<=number2; ++j)
        {
            cout << i << '*' << j << '=' << i*j << endl;
        }
        cout << endl;
    }
    return 0;
}
