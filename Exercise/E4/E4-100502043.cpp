#include<iostream>

using namespace std;

int main()
{
    int num1=0,num2=0;
    int x=0,y=0;

    while (num1<=0)
    {
        cout << "Please enter first number( >0 ): ";
        cin >> num1;  //輸入數字1
        if (num1<=0)
            cout << "First number is out of range!!" << endl;
    }
    while (num2<=0)
    {
        cout << "Please enter second number( >0 ): ";
        cin >> num2;  //輸入數字2
        if (num2<=0)
            cout << "Second number is out of range!!" << endl;
    }

    for (x=1;x<=num1;x++)
    {
        for (y=1;y<=num2;y++)
        {
            cout << x << "*" << y << "=" << x*y << endl;  //輸出結果
        }
        cout << endl;  //區分區塊
    }

    return 0;
}
