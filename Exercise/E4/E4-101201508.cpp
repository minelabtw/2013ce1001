#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int first ;                                             //輸入第一個數
    int second ;                                            //輸入第二個數
    int i ;                                                 //紀錄現在做到第一個數的第幾個
    int j ;                                                 //紀錄現在做到第二個數的第幾個
    cout << "Please enter first number( >0 ):";             //再進去之前先輸出一次告訴使用這ˇ輸入
    cin >> first ;                                          //使用者第一次輸入
    while (first <1)                                        //如果使用者第一次輸入錯的話就在進去
    {
        cout << "First number is out of range!!" ;          //告訴使用這第一次他輸入錯了
        cout << "Please enter first number( >0 ):";
        cin >> first ;                                      //再輸入一次
    }
    cout << "Please enter second number( >0 ):";            //同上
    cin >> second ;
    while (second <1)
    {
        cout << "Second number is out of range!!" ;
        cout << "Please enter second number( >0 ):";
        cin >> second ;
    }
    for (i=1; i<=first; ++i)                                //做比較久才要開始重複的數
    {
        for (j=1; j<=second; ++j)                           //做比較快就開始重複的數
            cout << i <<"*" << j << "=" << i*j << endl ;
        cout << endl ;
    }
    return 0;
}
