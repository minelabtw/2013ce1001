#include<iostream>
using namespace std;

int main ()
{
    int right = 1;    //宣告right&left= 1
    int left = 1;
    int a = 0;        //輸入a*b
    int b = 0;
    int number = 0;    //a*b = number

    cout <<"Please enter first number( >0 ):";
    cin >> a;
    while ( a <= 0 )           //數字大於0
    {
        cout <<"First number is out of range!!\nPlease enter first number( >0 ):";
        cin >> a;
    }

    cout <<"Please enter second number( >0 ):";
    cin >> b;
    while ( b <= 0 )           //數字大於0
    {
        cout <<"Second number is out of range!!\nPlease enter second number( >0 ):";
        cin >> b;
    }

    while ( left <=a)
    {
        while ( right <=b )     //右邊要加1
        {
            number = left*right;
            cout << left <<"*"<< right<<"="<<number<<endl;     //輸出乘法表
            right++;
        }
        right = 1;         //右邊的要回到初始值
        cout <<"\n";       //換一行 區隔
        left++;
    }
    return 0;
}
