#include <iostream>
using namespace std ;

int main()
{
    int num1 ;   //宣告一整數變數
    int num2 ;   //宣告另一整數變數
    int x = 1 ;  //宣告整數變數x,其初始值為1
    int y = 1 ;  //宣告整數變數y,其初始值為1

    cout << "Please enter first number( >0 ):" ;            //將字串"Please enter first number( >0 ):"輸出至螢幕上
    cin >> num1 ;                                           //將num1輸入至電腦
    while (num1<=0)                                         //當num1小於等於0時
    {
        cout << "First number is out of range!!" << endl ;  //將字串"First number is out of range!!"輸出至螢幕上並換行
        cout << "Please enter first number( >0 ):" ;        //將字串"Please enter first number( >0 ):"輸出至螢幕上
        cin >> num1 ;                                       //將num1輸入至電腦
    }

    cout << "Please enter second number( >0 ):" ;           //將字串"Please enter second number( >0 ):"輸出至螢幕上
    cin >> num2 ;                                           //將num2輸入至電腦
    while (num2<=0)                                         //當num2小於等於0時
    {
        cout << "Second number is out of range!!" << endl ; //將字串"Second number is out of range!!"輸出至螢幕上並換行
        cout << "Please enter second number( >0 ):" ;       //將字串"Please enter second number( >0 ):"輸出至螢幕上
        cin >> num2 ;                                       //將num2輸入至電腦
    }

    while (x<=num1)                                                     //當x小於等於num1時
    {
        while (y<=num2)                                                 //當y小於等於num2時
        {
            cout << "" << x << "*" << "" << y << "=" << x*y << endl ;   //將x*y顯示於螢幕上
            y++;                                                        //x=x+1
        }
        cout <<endl;                                                    //顯示換行
        y=1;                                                            //y之初始值為1
        x++;                                                            //x=x+1
    }
    return 0 ;
}
