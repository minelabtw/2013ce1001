#include <iostream>

using namespace std;

int main()
{
    int number1;    //宣告變數
    int number2;
    int a = 1;
    int b = 1;

    cout << "Please enter first number( >0 ):" ;    //限制範圍
    cin >> number1;
    if ( number1 < 1 )
    {
        cout << "First number is out of range!!\n" << "Please enter first number( >0 ):" ;
        cin >> number1;
    }

    cout << "Please enter second number( >0 ):" ;   //限制範圍
    cin >> number2;
    if ( number2 < 1 )
    {
        cout << "Second number is out of range!!\n" << "Please enter second number( >0 ):" ;
        cin >> number2;
    }

    while ( a <= number1 )  //運算
    {
        while ( b <= number2 )
        {
            cout << a << "*" << b << "=" << a * b << endl;
            b++;
        }
        cout << endl;
        b = 1 ;
        a++;
    }

    return 0;
}
