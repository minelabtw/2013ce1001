/* 乘法表
請寫一個程式可以供使用者輸入兩個整數，
還要可以判別使用者輸入的數字大於0，
若不滿足則要重新輸入，
最後輸出的乘法表 */

#include<iostream>
using namespace std;

int main()
{
    int num1=0;                     //宣告一個整數並把0給他
    int num2=0;
    int sum=0;


    while(num1<=0)                  //first number 要大於0
    {
        cout << "please enter first number (>0) :  ";
        cin >> num1;

        if(num1<=0)                 //如果小於或等於0
            cout << "First number is out of range" << endl; //就輸出串字而且迴圈重輸入
    }

    while (num2<=0)
    {
        cout << "please enter sencond number (>0) : " ;
        cin >> num2;

        if(num2<=0)
            cout << "Sencond number is out of range" << endl;
    }

    for(int i=1; i<=num1; i++)              // 重複詢問並把i 遞增
    {
        for(int j=1; j<=num2; j++)
        {
            sum=i*j;                        //將兩個數字相加並把結果給sum
            cout << i << "*" << j << "=" << sum ;
            cout << endl;
        }
        cout << endl;
    }

    return 0;
}

