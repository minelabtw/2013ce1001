#include <iostream>

using namespace std;

int main()
{
    int firstnumber,secondnumber;//宣告整數firstnumber以及secondnumber
    int c = 1,d = 1;             //宣告c初始為1以及d初始為1

    cout << "Please enter first number( >0 ):";//顯示字彙並帶入firstnumber的值
    cin >> firstnumber;
    while ( firstnumber <= 0)//當firstnumber小於等於0時顯示字彙並換行重新輸入
    {
        cout << "First number is out of range!!" << endl;
        cin >> firstnumber;
    }


    cout << "Please enter second number( >0 ):";//顯示字彙並帶入secondnumber的值
    cin >> secondnumber;
    while ( secondnumber <= 0 )//當secondnumber小於等於0時顯示字彙並換行重新輸入
    {
        cout << "Second number is out of range!!" << endl;
        cin >> secondnumber;
    }


    while ( c <= firstnumber )//當c比firstnumber小
    {
        while ( d <= secondnumber )//當d比secondnumber小
        {
            cout << c << "*" << d << "=" << c * d << endl;//輸出字彙並換行且d的值做+1
            d++;
        }
        cout << endl;//換行且d的值回歸1,c的值做+1
        d = 1;
        c++;
    }

    return 0;
}
