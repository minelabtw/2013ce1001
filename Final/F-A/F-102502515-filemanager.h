#ifndef F-102502515-FILEMANAGER_H
#define F-102502515-FILEMANAGER_H
#define BUFFER_SIZE 200
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream out (content.c_str(), ios::out);//輸出檔案
    for (int j=0; j<_size;j++)
    {
        out << _buffer[j] << " ";
    }
}

void FileManager::Read()
{
    ifstream in(_filename.c_str(), ios::in);//輸入資料
    for (int i=0; i<200; i++)//存取資料
    {
        in >> _buffer[i];
        cout << _buffer[i] << " " ;
        _size=i;
    }
}

int FileManager::GetSize()
{
//finish me
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
