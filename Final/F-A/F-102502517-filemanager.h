#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    string Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream outClientFile ("_filename",ios::out);
    outClientFile << content;
}

string FileManager::Read()
{
    ifstream fin ("_filename",ifstream::in);

    fin >> _buffer[BUFFER_SIZE];

    return _buffer[BUFFER_SIZE];
}

int FileManager::GetSize()

{
    Read();
    _size = sizeof _buffer - 1;

    return _size;
}
//string* FileManager::GetData()
//{
    //Read();
    //inClientFile >> _buffer;

    //return _buffer;
//}


#endif // FILEMANAGER_H
