#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <string.h>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream outFile(content.c_str(), ios::out);
    int BUFFERSIZE=0;
    for (; BUFFERSIZE<_size; BUFFERSIZE++)
    {
        outFile <<_buffer[BUFFERSIZE]<<" ";
    }
    outFile.close();
//finish me
}
void FileManager::Read()
{
    ifstream inFile(_filename.c_str(), ios::in);
    int BUFFERSIZE=0;
    for (; BUFFERSIZE<GetSize(); BUFFERSIZE++)
    {
        inFile >>_buffer[BUFFERSIZE];
    }
    inFile.close();
//finish me
}
int FileManager::GetSize()
{
    _size=sizeof(_buffer);
    _size=_size/4;
    return _size;
//finish me
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
