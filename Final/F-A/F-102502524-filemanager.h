#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write();//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write()                   //out file to "output.txt"
{
    fstream fileout("output.txt",ios::out);
    for(int i=0;i<53;i++)
    {
        fileout << _buffer[i];
        if(i==52)
            break;
        fileout << " ";
    }
    fileout.close();
}

void FileManager::Read()                    //in file and put them on the screen
{
    fstream filein("data.txt",ios::in);
    for(int i=0;i<53;i++)
    {
        filein >> _buffer[i];
        cout << _buffer[i];
        if(i==52)
            break;
        cout << " ";
    }
    filein.close();
    Write();
}

int FileManager::GetSize()
{

}

string* FileManager::GetData()
{

}


#endif // FILEMANAGER_H
