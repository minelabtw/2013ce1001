#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream outFilename("output.txt", ios::out);
    for (; _size < '\n'; _size++)
    {
        cout << content[_size];
    }
}

void FileManager::Read() //
{
    ifstream inFilename("data.txt", ios::in);

}

int FileManager::GetSize()
{
    return _size;
}

string* FileManager::GetData()
{
    return _buffer;
}


#endif // FILEMANAGER_H
