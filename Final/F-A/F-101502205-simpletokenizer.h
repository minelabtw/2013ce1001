#ifndef SIMPLETOKENIZER_H
#define SIMPLETOKENIZER_H
#define FILEMANAGER_H
#define TOKEN_SIZE_MAX 150
#include <string>
using namespace std;

class SimpleTokenizer
{
public:
    SimpleTokenizer(FileManager* file);//constructor with file pointer as argument
    ~SimpleTokenizer();
    void Tokenize();//find the several tokens from the file data
    int GetTokenSize();//return token array size
    string GetToken(int index);//return token with specific index of the array
protected:
private:
    FileManager* _file;//pointer of file
    string _tokens[TOKEN_SIZE_MAX];//token array
    int _tokenSize;//token array size
    bool TokenContains(string str);//if str is already in token array
    void AddToken(string token);//add token to token array
};

SimpleTokenizer::SimpleTokenizer(FileManager* file)
{
    _file=file;
    _tokenSize=0;
}

SimpleTokenizer::~SimpleTokenizer()
{
}

void SimpleTokenizer::Tokenize()
{
    string *data = _file->GetData();    //the data
    int dataSize = _file->GetSize();    //get the size
    string str;
    for(int i=0; i<dataSize; i++){
        str=""; //initialize the string
        for(int j=0; j<data[i].length(); j++){
            //check the each letter in a word
            if(data[i][j]>='a' && data[i][j]<='z'){
                str += data[i][j];  //if its a~z, write it to the string
            }else if(data[i][j]>='A' && data[i][j]<='Z'){
                str += data[i][j]-'A'+'a';  //if its A~Z, change it into a~z
            }
            //else do nothing (it's not an alphabet!)
        }
        if(!TokenContains(str)){
            AddToken(str);  //add token if the token isn't in the _tokens
        }
    }
}

int SimpleTokenizer::GetTokenSize()
{
    return _tokenSize;  //return size
}

string SimpleTokenizer::GetToken(int index)
{
    return _tokens[index];  //return the token
}

bool SimpleTokenizer::TokenContains(string str)
{
    for(int i=0; i<_tokenSize; i++){
        if(_tokens[i]==str){
            return true;
            //if the token is in the _tokens, return true
        }
    }
    return false;   //else return false;
}

void SimpleTokenizer::AddToken(string token)
{
    _tokens[_tokenSize]=token;  //add a token
    _tokenSize++;   //size
}

#endif // SIMPLETOKENIZER_H
