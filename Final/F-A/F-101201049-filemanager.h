#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE]; //put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{

}

void FileManager::Read()
{

}

int FileManager::GetSize()
{
     int i=0;
     int countA=0;
     GetData();
     while(buffer[i]!='\0')
     {
        countA++;
     }

     return countA;


}

string* FileManager::GetData()
{
   string buffer[BUFSIZ];
   int i;
   ifstream input(filename.c_str());
   for(i=0;i<sizeof buffer;i++)
   {
       input >> buffer[i];
   }
   return buffer[i];

}


#endif // FILEMANAGER_H
