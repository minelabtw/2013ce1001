#ifndef F-102502502-FILEMANAGER_H_INCLUDED
#define F-102502502-FILEMANAGER_H_INCLUDED
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream outfile( "output.txt", ios::out);                                                 //輸出檔案
    for (int i = 0; i < _size; i++)
    {
        outfile << " " << _buffer[i]; //輸出
    }
}

void FileManager::Read()
{
    ifstream infile( "data.txt", ios::in);                                                  //讀取檔案
    for (int i = 0; i < _size; i++)
    {
        infile >> _buffer[i]; //讀取
    }
}

int FileManager::GetSize()                                                                  //傳回大小範圍
{
    for (int i = 0; i < _size; i++)
    {
        return i;
    }
}

string* FileManager::GetData()
{
    for (int i = 0; i < _size; i++)
    {
        return _buffer[i]; //輸出
    }
}


#endif // F-102502502-FILEMANAGER_H_INCLUDED
