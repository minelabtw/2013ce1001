#include<iostream>
#include "F-102502516-filemanager.h"
#include "F-102502516-simpletokenizer.h"
using namespace std;

int main()
{
    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename);//make a instance of FileManger

    inFile.Read();//read the file
    inFile.Write(outFilename);
    /*
        SimpleTokenizer tokenizer(&inFile);//make a tokenizer to tokenize file
        tokenizer.Tokenize();

        string output;//output content
        for(int i=0; i<tokenizer.GetTokenSize(); i++)
        {
            string msg="token: "+tokenizer.GetToken(i)+"\n";
            cout<<msg;
            output+=msg;  //累加到output （應該有超多東西）
        }
        FileManager outFile(outFilename);
        outFile.Write(output);//write to file 把output寫進 記事本

    */
    return 0;
}
