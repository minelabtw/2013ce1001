#ifndef SIMPLETOKENIZER_H
#define SIMPLETOKENIZER_H
#define FILEMANAGER_H
#define TOKEN_SIZE_MAX 150
#include <string>
#include "F-102502005-filemanager.h"

using namespace std;

class SimpleTokenizer
{
public:
    SimpleTokenizer(FileManager* file);//constructor with file pointer as argument
    ~SimpleTokenizer();
    void Tokenize(string b[], int a);//find the several tokens from the file data
    int GetTokenSize(int a);//return token array size
    string GetToken(int index);//return token with specific index of the array
    int howmanyword;
protected:
private:
    FileManager* _file;//pointer of file
    string _tokens[TOKEN_SIZE_MAX];//token array
    string _tokens2[TOKEN_SIZE_MAX];
    int _tokenSize;//token array size
    bool TokenContains(string str);//if str is already in token array
    void AddToken(string token);//add token to token array
};

SimpleTokenizer::SimpleTokenizer(FileManager* file)
{
    _file=file;
    _tokenSize=0;
}

SimpleTokenizer::~SimpleTokenizer()
{
}

void SimpleTokenizer::Tokenize(string b[], int a)
{
    howmanyword = 0;
    for(int i=0;i<a;i++)
    {
        _tokens[i] = b[i];
    }
    for(int j=0;j<a;j++)
    {
        int status=0;
        for(int k=0;k<TOKEN_SIZE_MAX;k++)
        {
           while(status!=1)
           {
            if (_tokens[j] != _tokens2[k])
            {
                howmanyword= howmanyword+1;
                _tokens2[k] = _tokens[j];
                status = 1;
            }
           }
        }
    }


//finish me
}

int SimpleTokenizer::GetTokenSize(int a)
{
    _tokenSize = a;
    return a;
//finish me
}

string SimpleTokenizer::GetToken(int index)
{
    return _tokens[index];
//finish me
}

bool SimpleTokenizer::TokenContains(string str)
{
//finish me
}

void SimpleTokenizer::AddToken(string token)
{
//finish me
}

#endif // SIMPLETOKENIZER_H
