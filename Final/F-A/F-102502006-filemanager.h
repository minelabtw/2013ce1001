#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{

    for(int i=0; i<=_size; i++)cout << _buffer[i] << " ";
    ofstream output("output.txt",ios::out);
    for(int i=0; i<=_size; i++)output << _buffer[i] << " ";
}

void FileManager::Read()
{
    ifstream input("data.txt",ios::in);
    for(int i=0; i<200; i++)input >> _buffer[i];


    FileManager::GetSize();
}

int FileManager::GetSize()
{
    for(int i=0; i<200; i++)
    {
        if(_buffer[i]=="BJ4")
        {
            _size = i;
            break;
        }
    }

}

string* FileManager::GetData()
{

}


#endif // FILEMANAGER_H
