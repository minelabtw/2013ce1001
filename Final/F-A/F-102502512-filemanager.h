#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
//finish me
    ofstream out(content.c_str());
    for(int j=0;j<BUFFER_SIZE;j++)
    {
        out<<_buffer[j]<<" ";
    }
    out.close();
}

void FileManager::Read()
{
//finish me
    ifstream in(_filename.c_str());
    for(int i=0;i<BUFFER_SIZE;i++)
    {
        in>>_buffer[i];
    }
    for(int i=0;i<BUFFER_SIZE;i++)
    {
        cout<<_buffer[i]<<" ";
    }
    in.close();
}

int FileManager::GetSize()
{
//finish me

}

string* FileManager::GetData()
{
//finish me

}


#endif // FILEMANAGER_H
