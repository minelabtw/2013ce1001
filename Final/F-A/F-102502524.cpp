#include<iostream>
#include "F-102502524-filemanager.h"
#include "F-102502524-simpletokenizer.h"
using namespace std;

int main()
{
    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename);//make a instance of FileManger
    inFile.Read();//read the infile and write to outfile

    return 0;
}
