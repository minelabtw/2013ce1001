#include<iostream>
#include "F-102502517-filemanager.h"
using namespace std;

int main()
{
    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename);//make a instance of FileManger

    //SimpleTokenizer tokenizer(&inFile);//make a tokenizer to tokenize file
    //tokenizer.Tokenize();

    //string output;//output content
    //for(int i=0; i<tokenizer.GetTokenSize(); i++)
    //{
        //string msg="token: "+tokenizer.GetToken(i)+"\n";
        //cout<<msg;
        //output+=msg;
    //}

    string output = inFile.Read();

    FileManager outFile(outFilename);
    outFile.Write(output);//write to file

    return 0;
}
