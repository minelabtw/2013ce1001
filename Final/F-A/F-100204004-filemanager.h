#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
//finish me
for(int i=0;i<_size;i++)
{
    cout<<_buffer[i]<<' ';
}
cout<<endl;
ofstream Output("output.txt",ios::out);
for(int i=0;i<_size;i++)
{
    Output<<_buffer[i];
}
}

void FileManager::Read()
{
//finish me
    ifstream Readfile("data.txt",ios::in);
    while(!Readfile.eof())
    {
        Readfile>>_buffer[_size];
        _size++;
    }
    Readfile.close();
    ofstream Output("output.txt",ios::out);
for(int i=0;i<_size;i++)
{
    Output<<_buffer[i]<<' ';
}
Output.close();
for(int i=0;i<_size;i++)
{
    cout<<_buffer[i]<<' ';
}
cout<<endl;
}



int FileManager::GetSize()
{
//finish me
return _size;
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
