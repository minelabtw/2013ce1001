#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string GetData(int a);//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
//finish me
    fstream file("output.txt",ios::out);
    file << content;

    file.close();

}

void FileManager::Read()
{
//finish me
    fstream file("data.txt",ios::in);
    while(file >> _buffer[_size]){
        _size++;
    }
    file.close();
}

int FileManager::GetSize()
{
//finish me
    return _size;
}

string FileManager::GetData(int a=0)
{
//finish me
    /*if(a)*/static int i=0;
    return _buffer[i++];
}


#endif // FILEMANAGER_H
