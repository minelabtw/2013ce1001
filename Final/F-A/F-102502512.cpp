#include<iostream>
#include "F-102502512-filemanager.h"
#include "F-102502512-simpletokenizer.h"
using namespace std;

int main()
{
    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename);//make a instance of FileManger
    inFile.Read();//read the file
    inFile.Write(outFilename);


    return 0;
}
