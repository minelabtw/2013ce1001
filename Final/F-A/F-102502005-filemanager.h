#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
    int _size;//data array size
    string _buffer[BUFFER_SIZE];//put data here
protected:
private:
    string _filename;//save filename


};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream outClientFile(_filename.c_str(),ios::out);
    outClientFile << content;
//finish me
}

void FileManager::Read()
{
    ifstream inClientFile(_filename.c_str(),ios::in);
    for(int i=0; i<BUFFER_SIZE; i++)
    {
        inClientFile >> _buffer[i];
    }
    for(int j=0; _buffer[j] != ""; j++)
    {
        _size=_size+1;
    }
//finish me
}

int FileManager::GetSize()
{
    return _size;
//finish me
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
