#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream writetxt("output.txt",ios::out);    //寫檔
    if(!writetxt)
    {
        cout<<"File could not be opened"<<endl;
        exit(1);
    }

    writetxt<<_buffer[0];
}

void FileManager::Read()
{
    ifstream readtxt("data.txt",ios::in);    //讀檔
    if(!readtxt)
    {
        cout<<"File could not be opened"<<endl;
        exit(1);
    }

    for(int i=0;i<200;i++)
    {
        readtxt>>_buffer[i];
    }

    for(int i=0;i<200;i++)
    {
        cout<<_buffer[i];
    }
}

int FileManager::GetSize()
{

}

string* FileManager::GetData()
{

}


#endif // FILEMANAGER_H
