#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
#include<iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ifstream  input("data.txt", ios::in);
    while(!input.eof())
    {
        input>>content;
    }
    ofstream output("output.txt",ios::out);
    if(output.is_open())
    {
        output<<content;
    }
          output.close();
    cout<<content;
}

void FileManager::Read()
{
//finish me
}

int FileManager::GetSize()
{
//finish me
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
