#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{

}

void FileManager::Read()
{
    char line[BUFFER_SIZE];
    int count;
    ifstream file("data.txt",ios::in);
    if (!file)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    while(file.getline(line,sizeof(line),' '))
    {
        string word(line);
    }
}

int FileManager::GetSize()
{
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
