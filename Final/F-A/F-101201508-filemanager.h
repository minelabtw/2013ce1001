#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
#include <iostream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream fileout(content,ios::out);

    for (int i=0;i<_size;i++)
    {
        fileout << _buffer[i] << " " ;
    }
    fileout.close() ;
}

void FileManager::Read()
{
    ofstream fileopen(_filename, ios::in);
    int i=0 ;
    while (!fileopen.eof())
    {
        fileopen << _buffer[i];
        i++ ;
    }
    _size=100 ;
    fileopen.close() ;
}

int FileManager::GetSize()
{
    return _size ;
}

string* FileManager::GetData()
{
    return &_buffer[0] ;
}


#endif // FILEMANAGER_H
