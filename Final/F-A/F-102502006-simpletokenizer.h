#ifndef SIMPLETOKENIZER_H
#define SIMPLETOKENIZER_H
#define FILEMANAGER_H
#define TOKEN_SIZE_MAX 150
#include <string>
using namespace std;

class SimpleTokenizer
{
public:
    SimpleTokenizer(FileManager* file);//constructor with file pointer as argument
    ~SimpleTokenizer();
    void Tokenize();//find the several tokens from the file data
    int GetTokenSize();//return token array size
    string GetToken(int index);//return token with specific index of the array
protected:
private:
    FileManager* _file;//pointer of file
    string _tokens[TOKEN_SIZE_MAX];//token array
    int _tokenSize;//token array size
    bool TokenContains(string str);//if str is already in token array
    void AddToken(string token);//add token to token array
};

SimpleTokenizer::SimpleTokenizer(FileManager* file)
{
    _file=file;
    _tokenSize=0;
}

SimpleTokenizer::~SimpleTokenizer()
{
}

void SimpleTokenizer::Tokenize()
{

}

int SimpleTokenizer::GetTokenSize()
{

}

string SimpleTokenizer::GetToken(int index)
{

}

bool SimpleTokenizer::TokenContains(string str)
{
//finish me
}

void SimpleTokenizer::AddToken(string token)
{
//finish me
}

#endif // SIMPLETOKENIZER_H
