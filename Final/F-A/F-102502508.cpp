#include<iostream>
#include "filemanager.h"
#include "simpletokenizer.h"
using namespace std;
_buffer[BUFFER_SIZE]
int main()
{
    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename,ios::in);//make a instance of FileManger
    inFile.Read();//read the file

    SimpleTokenizer tokenizer(&inFile);//make a tokenizer to tokenize file
    tokenizer.Tokenize();

    string output;//output content
    for(int i=0; i<tokenizer.GetTokenSize(); i++)
    {
        string msg="token: "+tokenizer.GetToken(i)+"\n";
        cout<<msg;
        output+=msg;
    }
    FileManager outFile(outFilename);
    outFile.Write(output);//write to file

    return 0;
}
