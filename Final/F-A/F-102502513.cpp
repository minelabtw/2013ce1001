#include<iostream>
#include "F-102502513-filemanager.h"
#include "F-102502513-simpletokenizer.h"
using namespace std;

int main()
{
    FileManager e;
    SimpleTokenizer g;
    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename);//make a instance of FileManger
    inFile.Read();//read the file

    SimpleTokenizer tokenizer(&inFile);//make a tokenizer to tokenize file
    tokenizer.Tokenize();

    string output;//output content
    for(int i=0; i<tokenizer.GetTokenSize(); i++)
    {
        string msg="token: "+tokenizer.GetToken(i)+"\n";
        cout<<msg;
        output+=msg;
    }
    FileManager outFile(outFilename);
    outFile.Write(output);//write to file

    return 0;
}
