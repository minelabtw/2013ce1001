#ifndef SIMPLETOKENIZER_H
#define SIMPLETOKENIZER_H
#define FILEMANAGER_H
#define TOKEN_SIZE_MAX 150
#include <string>
using namespace std;

class SimpleTokenizer
{
public:
    SimpleTokenizer(FileManager* file);//constructor with file pointer as argument
    ~SimpleTokenizer();
    void Tokenize();//find the several tokens from the file data
    int GetTokenSize();//return token array size
    string GetToken(int index);//return token with specific index of the array
protected:
private:
    FileManager* _file;//pointer of file
    string _tokens[TOKEN_SIZE_MAX];//token array
    int _tokenSize;//token array size
    bool TokenContains(string str);//if str is already in token array
    void AddToken(string token);//add token to token array
};

SimpleTokenizer::SimpleTokenizer(FileManager* file)
{
    _file=file;
    _tokenSize=0;
}

SimpleTokenizer::~SimpleTokenizer()
{
}

void SimpleTokenizer::Tokenize()
{
	for (int i = 0; i < (_file)->GetSize(); i++){
		string tmp1 = (_file)->GetData()[i]; // origin string
		string tmp2; // edit string
		for (int j = 0; tmp1[j]!= '\0'; j++){ // push all english alpha into tmp2
			if (tmp1[j] >= 'A' && tmp1[j] <= 'Z')
				tmp2 += (tmp1[j] + 32); // become a~z
			else if (tmp1[j] >= 'a'&&tmp1[j] <= 'z')
				tmp2 += tmp1[j];
		}
		if (!TokenContains(tmp2)){
			AddToken(tmp2);
		}
	}
}

int SimpleTokenizer::GetTokenSize()
{
	return _tokenSize;
}

string SimpleTokenizer::GetToken(int index)
{
	return _tokens[index];
}

bool SimpleTokenizer::TokenContains(string str)
{
	for (int i = 0; i < GetTokenSize(); i++){
		if (_tokens[i] == str)
			return true;
	}
	return false;
}

void SimpleTokenizer::AddToken(string token)
{
	_tokens[_tokenSize++] = token;
}

#endif // SIMPLETOKENIZER_H
