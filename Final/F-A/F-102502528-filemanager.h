#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string context);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string context)
{
    ofstream outputfile("output.txt",ios::out);
    outputfile << context;            //輸出context
}
void FileManager::Read()
{
    ifstream file1("data.txt", ios::in );
    FileManager::GetSize();
    for(int i=0; i<100; i++)       //將檔案中的文字拿出來
    {
        file1 >> _buffer[i];
    }
}

int FileManager::GetSize()
{
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
