#ifndef SIMPLETOKENIZER_H
#define SIMPLETOKENIZER_H
#define FILEMANAGER_H
#define TOKEN_SIZE_MAX 150
#include <string>
using namespace std;

class SimpleTokenizer
{
public:
    SimpleTokenizer(FileManager* file);//constructor with file pointer as argument
    ~SimpleTokenizer();
    void Tokenize();//find the several tokens from the file data
    int GetTokenSize();//return token array size
    string GetToken(int index);//return token with specific index of the array
protected:
private:
    FileManager* _file;//pointer of file
    string _tokens[TOKEN_SIZE_MAX];//token array
    int _tokenSize;//token array size
    bool TokenContains(string str);//if str is already in token array
    void AddToken(string token);//add token to token array
};

SimpleTokenizer::SimpleTokenizer(FileManager* file)
{
    _file=file;
    _tokenSize=0;
}

SimpleTokenizer::~SimpleTokenizer()
{
}

void SimpleTokenizer::Tokenize()
{
    string *num;
    num = strtok(str," ");
    while(num!=NULL)
    {
        for(int i=0; i<54; i++)
        {
            _tokens[i] = num;
            num = strtok(NULL," ");
        }
    }
}

int SimpleTokenizer::GetTokenSize()
{
    for(int j=0; _tokens[j]!='\0'; j++)
    {
        _tokenSize++;
    }
}

string SimpleTokenizer::GetToken(int index)
{
    _tokens[index];
}

bool SimpleTokenizer::TokenContains(string str)
{
//finish me
}

void SimpleTokenizer::AddToken(string token)
{
    string a[42];
    int b[42]= {};
    int _size=0;


    for(int j=0; j<TOKEN_SIZE_MAX; ++j)
    {
        int Find=0;

        for(int k=0; k<=_size; ++k)
        {
            if(a[j]==_tokens[k])
            {
                b[k]++;
                Find=1;
            }

        }
        if(Find==0)
        {
            a[_size]=_tokens[j];
            b[_size]++;
            _size++;
        }

    }
}

#endif // SIMPLETOKENIZER_H
