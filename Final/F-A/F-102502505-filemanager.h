#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
    int i=0;
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
//輸入和輸出合併
}

void FileManager::Read()
{
    ifstream read("data.txt",ios::in);//把data的資料輸入到字串中
    string str;
    while( read >> str )//讀到一個字串時
    {
        cout << str << " ";//輸出字串
        _buffer[i]=str;//並存字串
        i++;
    }

    cout << endl;

    ofstream write("output.txt",ios::out);//把_buffer中的字串輸出到output中
    for(int i=0; i<BUFFER_SIZE; i++)
    {
        write << _buffer[i] << " ";
    }
}

int FileManager::GetSize()
{
    _size=i;//把計算字串時使用到的變數即為字串數量
}

string* FileManager::GetData()
{

}


#endif // FILEMANAGER_H
