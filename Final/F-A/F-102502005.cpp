#include<iostream>
#include "F-102502005-filemanager.h"
#include "F-102502005-simpletokenizer.h"
using namespace std;

int main()
{
    int a;

    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename);//make a instance of FileManger
    inFile.Read();//read the file

    //for(int i=20; i<inFile._size; i++)
    //{
    //    cout << inFile._buffer[i] << endl;
    //}
    a = inFile._size;
    string b[a];

    for(int j=0;j<a;j++)
    {
        b[j] = inFile._buffer[j];
    }

    SimpleTokenizer tokenizer(&inFile);//make a tokenizer to tokenize file
    tokenizer.Tokenize(b,a);

    string output;//output content
    for(int i=0; i<tokenizer.GetTokenSize(a); i++)
    {
        string msg="token: "+tokenizer.GetToken(i)+"\n";
        cout<<msg;
        output+=msg;
    }
    FileManager outFile(outFilename);
    outFile.Write(output);//write to file

    return 0;
}
