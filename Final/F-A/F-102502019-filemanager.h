#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write();//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write()
{
    fstream p;
    p.open("output.txt",ios::out);
    for(int k=0; k<200; k++)
    {
        p <<_buffer[k];
    }
}

void FileManager::Read()
{
    fstream jj;
    jj.open("data.txt",ios::in);
    for(int i=0; i<200;i++)
    {
        jj>>_buffer[i];
    }
}

int FileManager::GetSize()
{
//finish me
}

string* FileManager::GetData()
{
     for(int y=0; y<200; y++)
    {
        cout <<_buffer[y];
    }
}


#endif // FILEMANAGER_H
