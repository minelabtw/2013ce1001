#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write();//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[200];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=200;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write()//將資料輸出至檔案
{
    int i;
    const char *ch=_filename.c_str();
    ofstream out(ch,ios::out);
    for(i = 0; i <_size; i++)
    {
        out << _buffer[i];
    }
    out.close();

}

void FileManager::Read()//將資料輸入至檔案
{
    int i;

    const char *ch=_filename.c_str();
    ifstream in(ch,ios::in);

    for(i = 0; i <_size; i++)
    {
        in >> _buffer[i];
        cout << _buffer[i] << " ";
    }
    in.close();

}

int FileManager::GetSize()
{
    const char *ch=_filename.c_str();
    ifstream in(ch, ios::in);

    _size = in.tellg();


    in.close();


    return _size;

}

string* FileManager::GetData()
{

}


#endif // FILEMANAGER_H
