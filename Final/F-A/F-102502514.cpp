#include<iostream>
#include "F-102502514-filemanager.h"
#include "F-102502514-simpletokenizer.h"
using namespace std;

int main()
{
    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename);//make a instance of FileManger
    inFile.Read();//read the file

    string output;//output content

    FileManager outFile(outFilename);
    outFile.Write(output);//write to file

    return 0;
}
