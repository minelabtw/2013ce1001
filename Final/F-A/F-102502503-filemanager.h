#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 53
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write();//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[53];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write()
{
    for (int i=0; i<53; i++)
        cout << _buffer[i] << " ";  //輸出字串
    ofstream data1 ("output.txt", ios::out);
    data1.is_open();  //開啟檔案
    for (int i=0; i<53; i++)
        data1 << _buffer[i] << " ";  //將字串輸出txt檔
    data1.close();  //關閉檔案
}

void FileManager::Read()
{
    ifstream data ("data.txt", ios::in);
    data.is_open();//開啟檔案
    for (int i=0; i<53; i++)
        data >> _buffer[i];  //將檔案存入陣列
    data.close();  //關閉檔案
}

#endif // FILEMANAGER_H
