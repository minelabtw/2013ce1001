#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream outFile(_filename.c_str(), ios::out);  //create an output file with filename _filename
    outFile << content; //write a word
    outFile.close();    //close file
}

void FileManager::Read()
{
    ifstream inFile(_filename.c_str(), ios::in);    //read a file
    int i;
    for(i=0; inFile >> _buffer[i]; i++);    //end loop when we get eof
    _size = i;  //the value of i is equal to number of words
    inFile.close(); //close file
}

int FileManager::GetSize()
{
    return _size;   //return value
}

string* FileManager::GetData()
{
    return _buffer; //return the pointer of buffer
}


#endif // FILEMANAGER_H
