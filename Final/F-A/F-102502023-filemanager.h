#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    ofstream outClientFile( content.c_str(), ios::out );//initialize outClientFile as ofstream type
    for( int i=0; i< BUFFER_SIZE ; i++ )//for loop to write the data
    {
        outClientFile << _buffer[i];
    }

}

void FileManager::Read()
{
    ifstream inClientFile( _filename.c_str(), ios::in );//create inClientFile as ifstream type
    while( !inClientFile.eof() )//while statement to check whether read to EOF
    {
       for( int i=0; i < BUFFER_SIZE; i++ )//for loop to dump the words
       {
           inClientFile >> _buffer[i];
           _buffer[i]+=" ";//add a space
       }
    }
    for(int i=0; i<BUFFER_SIZE; i++)//for loop to output the words to screen
    {
        cout << _buffer[i];
    }
}

int FileManager::GetSize()
{
//finish me
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
