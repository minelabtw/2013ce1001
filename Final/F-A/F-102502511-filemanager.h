#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
#include <vector>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    string b[0] = {};
    ofstream outfile(_filename.c_str(), ios::out);

    for(int i = 0; i < 100 ; i++)
    {
        if(_buffer[i] != b[0])
        {
            outfile << "token: " << _buffer[i] << endl;
        }
    }

}

void FileManager::Read()
{
    ifstream infile(_filename.c_str(), ios::in);

    for(int i = 0; i < 100 ; i++)
    {
        infile >> _buffer[i];
    }

    for(int i = 0; i < 53; i++)
    {
        cout << "token: " << _buffer[i] << endl;
    }
}

int FileManager::GetSize()
{
    string b[0] = {};
    int j =0;
    for(int i = 0; i < 100 ; i++)
    {
        if(_buffer[i] != b[0])
        {
            j++;
        }
    }

    _size = j;
}

string FileManager::GetData()
{
}


#endif // FILEMANAGER_H
