#include<iostream>
#include "F-102502015-filemanager.h"
#include "F-102502015-simpletokenizer.h"
using namespace std;

int main()
{
    string inFilename="data.txt";//input file
    string outFilename="output.txt";//output file

    FileManager inFile(inFilename);//make a instance of FileManger
    inFile.Read();//read the file
    string output;//output content
    for(int i=0; i<inFile.GetSize(); i++)
    {
        cout<<"token: "+inFile.GetData(i)+"\n";
    }
    inFile.Write();//write to file
    return 0;
}
