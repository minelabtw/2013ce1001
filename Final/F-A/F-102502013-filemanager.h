#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
#include <cstring>
#include <cstdio>

using namespace std;

class FileManager
{
public:
    FileManager(char  filename);
    ~FileManager();
    char * GetData();//return data array
    int GetSize();//return data array size
    void Write(char content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    char  _filename;//save filename
    char  _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(char  filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(char content)
{
    ofstream outfile("output.txt", ios::out);
    for(int i=0; i<_size; i++)
    {
        outfile << "token: "<<&_buffer[i]<<endl;
    }
}

void FileManager::Read()
{
    ifstream infile("data.txt", ios::in);
    for (_size=0;_size<200;_size++)
    {
        infile >> _buffer[_size];
    }
}

int FileManager::GetSize()
{
    return _size;
}

char * FileManager::GetData()
{
    char* delim=" ";
    char* p_buffer;
    p_buffer = strtok(p_buffer,NULL);
    while(*delim!=NULL)
    {
        &_buffer[_size]=p_buffer;
        p_buffer = strtok(NULL, *delim);
        _size++
        return &_buffer[_size];
    }
}


#endif // FILEMANAGER_H
