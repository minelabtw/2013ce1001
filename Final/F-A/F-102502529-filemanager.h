#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    void GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    string comp[200];
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)             //讀檔
{

    fstream out("output.txt",ios::out);
    for(int i=0; i<_size; i++)                                      //輸出結果
    {
        if(_buffer[i]!="tag")
            out<<"token: "<<_buffer[i]<<endl;
    }

}

void FileManager::Read()                            //讀檔
{
    int counter=0;
    fstream in("data.txt",ios::in);
    for(int i=0; !in.eof(); i++)
    {
        in>>_buffer[i];
        counter++;
    }
    _size=counter;

}

int FileManager::GetSize()
{
    return _size;
}

void FileManager::GetData()                                         //輸出
{
    for(int k1=0; k1<_size; k1++)
    {
        if(k1!=_size-1)                                     //最後一個不比
        {
            for(int k2=k1+1; k2<_size; k2++)
            {
                if(_buffer[k1]==_buffer[k2])                        //把後面相同的做標記
                {
                    _buffer[k2]="tag";
                }
            }
        }

    }
    for(int i=0; i<_size; i++)                                      //輸出結果
    {
        if(_buffer[i]!="tag")
            cout<<"token: "<<_buffer[i]<<endl;
    }

}


#endif // FILEMANAGER_H
