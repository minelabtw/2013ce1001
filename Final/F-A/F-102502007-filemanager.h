#ifndef F-102502007-FILEMANAGER_H
#define F-102502007-FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array 資料
    int GetSize();//return data array size 長度
    void Write(string content);//write data to file 寫入檔案的函式
    void Read();//read file to buffer; 讀入檔案
protected:
private:
    string _filename;//save filename //檔名
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size //資料大小
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
    fstream file("output.txt",ios::out);
    file.close();

}

void FileManager::Read()
{
    fstream file("data.txt",ios::in);
    file >> _buffer[200];
    file.close();
}

int FileManager::GetSize()
{
//finish me
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
