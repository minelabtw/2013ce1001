#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string GetData(int index);//return data array
    int GetSize();//return data array size
    void Write();//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write()
{
ofstream file2("output.txt");           //輸出測資
for(int i=0;i<_size;i++)
{
    file2<<_buffer[i];
    if(i!=_size)
    {
        file2<<" ";
    }
}
file2.close();                          //關檔案
}

void FileManager::Read()
{
    ifstream file("data.txt");          //讀檔案
    while(file)                         //讀資料
    {
        file>>_buffer[_size];
        _size++;
    }
    _size=_size-1;
    file.close();                       //關黨
}

int FileManager::GetSize()              //取得size
{
    return  _size;
}

string FileManager::GetData(int index)  //回傳資料
{
    return _buffer[index];
}


#endif // FILEMANAGER_H
