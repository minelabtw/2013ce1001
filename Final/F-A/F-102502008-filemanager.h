#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define txt_Size 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to txt;
protected:
private:
    string _filename;//save filename
    string txt[txt_Size];//put data here
    int Size;//data array size
};

FileManager::FileManager(string filename)
{
    Size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{

}

void FileManager::Read()
{
    ifstream intxt("data.txt",ios::in) ;
    ofstream outtxt("output.txt",ios::out) ;
    if (intxt.is_open())
    {
        while(!intxt.eof())
        {
            intxt >> txt[Size] ;
            Size ++ ;
        }
        for(int i=0 ; i<Size ; i++ )
            cout << txt[i] << " ";
    }
    else
    {
        cout << "Error!" <<endl ;
    }
    intxt.close() ;
    // 讀取資料
    if (outtxt.is_open())
    {
        int time =0 ;
        while(time<=Size)
        {
            outtxt << txt[time] << " ";
            time ++ ;
        }
        for(int i=0 ; i<Size ; i++ )
            cout << txt[i] << " ";
    }
    else
    {
        cout << "Error!" <<endl ;
    }
    outtxt.close();
    //寫入資料
}

int FileManager::GetSize()
{
//finish me
}

string* FileManager::GetData()
{
//finish me
}


#endif // FILEMANAGER_H
