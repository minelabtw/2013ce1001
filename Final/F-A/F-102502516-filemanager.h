#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define BUFFER_SIZE 200
#include <string>
#include <fstream>
using namespace std;

class FileManager
{
public:
    FileManager(string filename);
    ~FileManager();
    string* GetData();//return data array
    int GetSize();//return data array size
    void Write(string content);//write data to file
    void Read();//read file to buffer;
protected:
private:
    string _filename;//save filename
    string _buffer[BUFFER_SIZE];//put data here
    int _size;//data array size
};

FileManager::FileManager(string filename)
{
    _size=0;
    _filename=filename;
}

FileManager::~FileManager()
{
}

void FileManager::Write(string content)
{
//finish me
    ofstream outClientfile( content.c_str(), ios::out );

    for( int i=0; i<_size; i++)
        outClientfile << _buffer[i] << " ";//BUFFER存到新的檔案中

}

void FileManager::Read()
{
//finish me

    ifstream infile(_filename.c_str(),ios::in);
    for( int i=0; i<BUFFER_SIZE; i++)  //讀入_buffer中
        infile >> _buffer[i];

    for( int i=0; i<GetSize(); i++)
        cout << _buffer[i] << " " ;
}

int FileManager::GetSize()
{
//finish me

    for( int i=0; _buffer[i]!=""; i++)  //求出長度
        _size = i+1;

    return _size;
}

string* FileManager::GetData()
{
//finish me
    string data[GetSize()];
    string *dataPtr = &data[0];
    for( int i=0; i<GetSize(); i++)
        *(dataPtr+i) = _buffer[i];

    return dataPtr;
}


#endif // FILEMANAGER_H
