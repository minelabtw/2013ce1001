#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
    public:
        POSsystem()
        {
            string menuin[9]={"Fried chicken","Hamburger","Chicken nuggets","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
            int pricein[9]={50,60,40,30,20,40,30,20,30};//用來初始化
            total1=total2=total3=0;
            for(int i=0;i<9;i++)//將所有數值初始化
            {
                student[i]=0;
                teacher[i]=0;
                normal[i]=0;
                student_index[i]=0;
                teacher_index[i]=0;
                normal_index[i]=0;
                menu[i]=menuin[i];
                price[i]=pricein[i];
            }

        }
        void decode(char code[])
        {
            double sum=0;
            for(int i=1;code[i];i++)
            {
                if(code[i]<='9'&&code[i]>='0')
                {
                    sum=sum*10+code[i]-'0';//數字換算

                }
                else
                {
                    switch(code[0])
                    {
                    case '0'://老師
                        teacher[ code[i]-'A' ]=teacher[ code[i]-'A' ] +sum*price[code[i]-'A']*0.8;
                        sum=0;
                        break;
                    case '1'://學生
                        student[ code[i]-'A' ]=student[ code[i]-'A' ] +sum*price[code[i]-'A']*0.9;
                        sum=0;
                        break;
                    case '2':
                        normal[ code[i]-'A' ]+=sum*price[code[i]-'A'];
                        sum=0;
                        break;

                    }
                }
            }


        }
        void InputData()
        {
            ifstream file("fastfoodlist.txt",ios::in);
            char input[50];
            while(file.eof()==0)
            {
                file >> input;
                decode(input);
            }
        }

        void sortData(int number[],int temp[])//number :原始資料    temp: 排序後的index
        {
            int index=0;
            int numbertemp[9];
            int miniindex=-1;
            for(int i=0;i<9;i++)
            {
                numbertemp[i]=number[i];
            }

            for(int i=0;i<9;i++)//選擇排序
            {
                for(int j=0;j<9;j++)
                {
                   if((miniindex==-1&&numbertemp[j]!=-1) || (numbertemp[miniindex]>numbertemp[j] && numbertemp[j]!=-1) )
                        miniindex=j;
                }
                temp[i]=miniindex;
                numbertemp[miniindex]=-1;
                miniindex=-1;
            }

        }

        void OutputData()
        {
            sortData(teacher,teacher_index);
            sortData(student,student_index);
            sortData(normal,normal_index);
            ofstream file("102502050.txt",ios::out);


            file << "Teacher" << endl;//輸出老師點的餐

            file << "========================" << endl;
            for(int i=0;i<9;i++)
            {
                file << setw(15)<<menu[teacher_index[i]]<< " : "<<  setw(5)<<teacher[ teacher_index[i] ] << endl;
                total1+=teacher[ teacher_index[i] ];
            }
            file << "Total:" << total1 << endl;
            file << "------------------------" << endl;


            file << "Student" << endl;//輸出學生點的餐

            file << "========================" << endl;
            for(int i=0;i<9;i++)
            {
                file << setw(15)<<menu[student_index[i]]<< " : "<<  setw(5)<<student[ student_index[i] ] << endl;
                total2+=student[ student_index[i] ];
            }
            file << "Total:" << total2 << endl;
            file << "------------------------" << endl;


            file << "Normal" << endl;//輸出其他人點的餐

            file << "========================" << endl;
            for(int i=0;i<9;i++)
            {
                file << setw(15)<<menu[normal_index[i]]<< " : "<<  setw(5)<<normal[ normal_index[i] ] << endl;
                total3+=normal[ normal_index[i]];
            }
            file << "Total:" << total3 << endl;
            file << "------------------------" << endl;
            file << "Total Money:" <<(total1+total2+total3);

        }

    private:
        string menu[9];//餐點名
        int total1,total2,total3;//各種客人的總額
        int price[9];//餐點的價錢
        int student[9],teacher[9],normal[9];//在各種餐點上花的錢
        int student_index[9],teacher_index[9],normal_index[9];

};
