#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
    public:
        POSsystem()//初始化各個資料
        {
            total1=total2=total3=0;
            for(int i=0;i<9;i++)
            student[i]=teacher[i]=normal[i]=student_index[i]=teacher_index[i]=normal_index[i]=0;
            price[0]=50;
            price[1]=60;
            price[2]=price[5]=40;
            price[3]=price[6]=price[8]=30;
            price[4]=price[7]=20;
            menu[0]="Fried chicken :";
            menu[1]="Hamburger :";
            menu[2]="Chicken nuggets :";
            menu[3]="Cola :";
            menu[4]="Black tea :";
            menu[5]="Coffee :";
            menu[6]="French fries :";
            menu[7]="Tart :";
            menu[8]="Hot dog :";
        }
        void decode(char code[])
        {
            if(code[0]=='0')
                sortData(teacher,teacher_index);
            else if(code[0]=='1')
                sortData(student,student_index);
            else if(code[0]=='2')
                sortData(normal,normal_index);
        }
        void InputData()
        {
            ifstream fin ("fastfoodlist.txt" , ios::in);
            while(fin >> buy)
            {
                decode(buy);
            }
            for(int k=0;k<9;k++)
            {
                total1+=teacher[k]*price[k];
                total2+=student[k]*price[k];
                total3+=normal[k]*price[k];
            }
            fin.close();
        }

        void sortData(int number[],int temp[])
        {

        }

        void OutputData()
        {
            ofstream fout("101201524.txt" , ios::out);
            fout << "Student" << endl << "=======================";
            for(int j=0;j<9;j++)
                fout << endl << setw(17) << menu[j] << setw(5) << student[j];
            fout << endl << "Total:" << total2 << endl << "-----------------------" << endl;
            fout << "Teacher" << endl << "=======================";
            for(int j=0;j<9;j++)
                fout << endl << setw(17) << menu[j] << setw(5) << teacher[j];
            fout << endl << "Total:" << total1 << endl << "-----------------------" << endl;
            fout << "Normal" << endl << "=======================";
            for(int j=0;j<9;j++)
                fout << endl << setw(17) << menu[j] << setw(5) << normal[j];
            fout << endl << "Total:" << total3 << endl << "-----------------------" << endl;
            fout << "Total Money:" << total1+total2+total3;
            fout.close();
        }

    private:
        string menu[9];
        int total1,total2,total3;
        int price[9];
        int student[9],teacher[9],normal[9];
        int student_index[9],teacher_index[9],normal_index[9];
        char buy[100];
};

#endif // POS_SYSTEM_H_INCLUDED
