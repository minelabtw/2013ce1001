#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<string>
#include<fstream>
#include<iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
    };

    void decode()
    {
        string cutter;
        string cut[100];

        for(int i = 0; i < 9; i++)
        {
            total1 += student[i] * price[i] * 0.9;
        }//學生總數

        for(int i = 0; i < 9; i++)
        {
            total2 += teacher[i] * price[i] * 0.8;
        }//教師總數

        for(int i = 0; i < 9; i++)
        {
            total3 += normal[i] * price[i];
        }//其他總數

        totalmoney = total1 + total2 + total3;//總數
    }
    void InputData()
    {
        ifstream foodlist("fastfoodlist.txt",ios::in);//讀檔

        while(!foodlist.eof())
        {
            for(int i = 0; i < sizeof(code); i++)
            {
                foodlist >> code[i];//資料存入陣列
            }
        }
    }

    void sortData(int number[],int temp[])
    {
    }

    void OutputData()
    {
        ofstream thelist("102502552.txt",ios::out);//建新檔

        thelist << "Student" << endl << "============================" << endl;

        for(int i = 0; i < 9; i++)
        {
            thelist <<setw(15) << menu[i] << " : " << student[i] << endl;
        }//印出學生各項

        thelist << "Total:" << total1 << endl << "----------------------------" << endl << "Teacher" << endl << "============================" << endl;

        for(int i = 0; i < 9; i++)
        {
            thelist <<setw(15) << menu[i] << " : " << teacher[i] << endl;
        }//印出教師各項

        thelist << "Total:" << total2 << endl << "----------------------------" << endl << "Normal" << endl << "============================" << endl;

        for(int i = 0; i < 9; i++)
        {
            thelist <<setw(15) << menu[i] << " : " << normal[i] << endl;
        }//印出其他各項

        thelist << "Total:" << total3 << endl << "----------------------------" << endl << "Total Money:" << totalmoney;//總額
    }

private:
    char code[1681];
    string const menu[9] = {"Fried chicken","Hamburger","Chicken nuggets","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
    int total1,total2,total3,totalmoney = 0;
    int const price[9] = {50,60,40,30,20,40,30,20,30};
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];
};

#endif // POS_SYSTEM_H_INCLUDED
