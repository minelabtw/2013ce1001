#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem() //結構子，初始化各元素數值
    {
        menu[0] = "Fried chicken";
        menu[1] = "Hamburger";
        menu[2] = "Chicken nuggets";
        menu[3] = "Cola";
        menu[4] = "Black tea";
        menu[5] = "Coffee";
        menu[6] = "French fries";
        menu[7] = "Tart";
        menu[8] = "Hot dog";
        total1 = 0;
        total2 = 0;
        total3 = 0;
        price[0] = 50;
        price[1] = 60;
        price[2] = 40;
        price[3] = 30;
        price[4] = 20;
        price[5] = 40;
        price[6] = 30;
        price[7] = 20;
        price[8] = 30;
        for ( int i = 0 ; i < 9 ; i++ )
        {
            student[i] = 0;
            teacher[i] = 0;
            normal[i] = 0;
        }
    }
    void decode(char code[]) //解碼函數
    {
        char kind = 0;
        int amount = 0;
        int food = 0;
        int foodnum = 0;
        int i = 2;
        int midtotal = 0;
        kind = code[0];
        amount = code[1] - '0';
        switch ( kind )
        {
        case '0':
            while ( amount > 0 )
            {
                if ( code[i+1] == '0' || code[i+1] == '1' || code[i+1] == '2' || code[i+1] == '3' || code[i+1] == '4' || code[i+1] == '5' || code[i+1] == '6' || code[i+1] == '7' || code[i+1] == '8' || code[i+1] == '9' )
                {
                    foodnum = ( code[i] - '0' ) * 10;
                    i++;
                    foodnum += code[i] - '0';
                    i++;
                }
                else
                {
                    foodnum = code[i] - '0';
                    i++;
                }
                food = code[i] - 'A';
                i++;
                midtotal += price[food] * foodnum * 0.8;
                teacher[food] += foodnum;
                total2 += midtotal;
                amount--;
                midtotal = 0;
            }
            i = 2;
            break;
        case '1':
            while ( amount > 0 )
            {
                if ( code[i+1] == '0' || code[i+1] == '1' || code[i+1] == '2' || code[i+1] == '3' || code[i+1] == '4' || code[i+1] == '5' || code[i+1] == '6' || code[i+1] == '7' || code[i+1] == '8' || code[i+1] == '9' )
                {
                    foodnum = (code[i] - '0') * 10;
                    i++;
                    foodnum += code[i] - '0';
                    i++;
                }
                else
                {
                    foodnum = code[i] - '0';
                    i++;
                }
                food = code[i] - 'A';
                i++;
                midtotal += price[food] * foodnum * 0.9;
                student[food] += foodnum;
                total1 += midtotal;
                amount--;
                midtotal = 0;
            }
            i = 2;
            break;
        case '2':
            while ( amount > 0 )
            {
                if ( code[i+1] == '0' || code[i+1] == '1' || code[i+1] == '2' || code[i+1] == '3' || code[i+1] == '4' || code[i+1] == '5' || code[i+1] == '6' || code[i+1] == '7' || code[i+1] == '8' || code[i+1] == '9' )
                {
                    foodnum = (code[i] - '0') * 10;
                    i++;
                    foodnum += code[i] - '0';
                    i++;
                }
                else
                {
                    foodnum = code[i] - '0';
                    i++;
                }
                food = code[i] - 'A';
                i++;
                midtotal += price[food] * foodnum;
                normal[food] += foodnum;
                total3 += midtotal;
                amount--;
                midtotal = 0;
            }
            i = 2;
            break;
        }
    }
    void InputData() //讀入函數
    {
        ifstream fin ( "fastfoodlist.txt" );
        char code[40] = {};
        while ( fin >> code )
        {
            decode( code );
        }
        fin.close();
    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData() //輸出函數
    {
        ofstream fout ( "102502556.txt" );
        fout << "Student" << endl;
        fout << "================" << endl;
        for ( int i = 0 ; i < 9 ; i++ )
        {
            fout << setw(15) << menu[i] << " :  " << student[i] << endl;
        }
        fout << "Total:" << total1 << endl;
        fout << "----------------" << endl;
        fout << "Teacher" << endl;
        fout << "================" << endl;
        for ( int i = 0 ; i < 9 ; i++ )
        {
            fout << setw(15) << menu[i] << " :  " << teacher[i] << endl;
        }
        fout << "Total:" << total2 << endl;
        fout << "----------------" << endl;
        fout << "Normal" << endl;
        fout << "================" << endl;
        for ( int i = 0 ; i < 9 ; i++ )
        {
            fout << setw(15) << menu[i] << " :  " << normal[i] << endl;
        }
        fout << "Total:" << total3 << endl;
        fout << "----------------" << endl;
        fout << "Total Money:" << total1 + total2 + total3 << endl;
        fout.close();
    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
