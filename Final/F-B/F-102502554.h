#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
    public:
        POSsystem()
        {
            price[0] = 50;
            price[1] = 60;
            price[2] = 40;
            price[3] = 30;
            price[4] = 20;
            price[5] = 40;
            price[6] = 30;
            price[7] = 20;
            price[8] = 30;
            for ( int i = 0 ; i < 9 ; i++)
            {
                student[i] = 0;
                teacher[i] = 0;
                normal[i] = 0;
                student_index[i] = 0;
                teacher_index[i] = 0;
                normal_index[i] = 0;
            }
            total1 = 0;
            total2 = 0;
            total3 = 0;
        }
        void decode(char code[])
        {
            /*int i = 0;
            char de [1000];
            while ( code [] != '/0'; )
            {
                de [i] = code [i] '/0';
            }*/
        }
        void InputData()
        {
            ifstream fin ( "", ios::in);//讀取檔案
            char s[1000];//宣告陣列s
            while ( fin >> s)//將讀取之檔案存入s
            {
                decode (s);
            }
            fin.close();
        }

        void sortData(int number[],int temp[])
        {
        }

        void OutputData()
        {
            for (int i = 0 ; i < 9 ; i++)
            {
                total1 += price[i] * student [i] * 0.9;
            }
            for (int i = 0 ; i < 9 ; i++)
            {
                total2 += price[i] * teacher [i] * 0.8;
            }
            for (int i = 0 ; i < 9 ; i++)
            {
                total3 += price[i] * normal [i];
            }//計算總金額
            ofstream fout ("102502554.txt",ios::out);//輸出檔案
            fout << "Student" << endl << "_________________________" <<endl;
            for ( int i = 0 ; i < 9 ; i++)
            {
                fout << setw(15) << menu [i] << " :  " << setw(5) <<student_index[i] << endl;
            }
            fout << "Total:" << total1 << endl << "-------------------------" << endl;
            fout << "Teacher" << endl << "_________________________" <<endl;
            for ( int i = 0 ; i < 9 ; i++)
            {
                fout << setw(15) << menu [i] << " :  " << setw(5) << teacher_index[i]<< endl;
            }
            fout << "Total:" << total2 << endl << "-------------------------" << endl;
            fout << "Normal" << endl << "_________________________" <<endl;
            for ( int i = 0 ; i < 9 ; i++)
            {
                fout << setw(15) << menu [i] << " :  " << setw(5) << normal_index[i]<< endl;
            }
            fout << "Total:" << total3 << endl << "-------------------------" << endl;
            fout << "Total Money:" << total1 + total2 + total3;//輸出購買次數及總金額
            fout.close();
        }

    private:
        string menu[9] = {"Fried chicken","Hamburger","Chicken nuggets","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
        int total1,total2,total3;
        int price[9];
        int student[9],teacher[9],normal[9];
        int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
