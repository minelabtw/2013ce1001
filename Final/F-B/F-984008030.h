#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <fstream>
#include <string>
#include <cstdlib>
#include <iomanip>
using namespace std;

class POSsystem{
    public:
        POSsystem(){
            //將所有屬性初始化
            this->total1 = 0;
            this->total2 = 0;
            this->total3 = 0;
            for(int i = 0; i < 9; i++){
                this->student[i] = 0;
                this->teacher[i] = 0;
                this->normal[i] = 0;
                this->student_index[i] = i;
                this->teacher_index[i] = i;
                this->normal_index[i] = i;
            }
            this->menu[0] = "Fried chicken";
            this->menu[1] = "Hamburger";
            this->menu[2] = "Chicken nuggets";
            this->menu[3] = "Cola";
            this->menu[4] = "Black tea";
            this->menu[5] = "Coffee";
            this->menu[6] = "French fries";
            this->menu[7] = "Tart";
            this->menu[8] = "Hot dog";
        }
        void decode(char code[]){
            char* ptr = code;//宣告型態為char*的指標指向code的記憶體位置
            string numberStr = "";//宣告型態為字串的數字並初始化為空字串
            int number = 0;//宣告型態為int的單項總金額並初始化為0
            int doller = 0;//宣告型態為int的單項價格並初始化為0
            float discount = 1.0;//宣告型態為float的折扣並初始化為1.0
            int foodType = 0;//宣告型態為int的食物類型並初始化為0
            int type = (int)(*ptr++) - 48;//宣告型態為int的職業類別 0字元以整數表示為48，扣掉48同時ptr指向下一個
            int* total = &(this->total3);//宣告型態為int的總價格指標並指向其他的總價格
            int* numArrayPtr = this->normal;//宣告型態為int的項數陣列指標並指向其他的項數陣列

            if(type < 0 || type > 2){//如果不是0~2之間 跳出去
                cout << "Error type of cosumer." << endl;
                return;
            }
            //根據不同職業決定折扣和對應要相加的總價格和項數陣列
            if(type == 0){
                discount = 0.8;
                total = &(this->total1);
                numArrayPtr = this->teacher;
            }
            if(type == 1){
                discount = 0.9;
                total = &(this->total2);
                numArrayPtr = this->student;
            }
            for(; *ptr != '\0'; ptr++){
                if((int)(*ptr) < 65){//還是數字
                    numberStr = numberStr + *ptr;
                }
                else{
                    //決定食物項目
                    switch(*ptr){
                        case 'A':
                            foodType = 0;
                            doller = 50;
                            break;
                        case 'B':
                            foodType = 1;
                            doller = 60;
                            break;
                        case 'C':
                            foodType = 2;
                            doller = 40;
                            break;
                        case 'D':
                            foodType = 3;
                            doller = 30;
                            break;
                        case 'E':
                            foodType = 4;
                            doller = 20;
                            break;
                        case 'F':
                            foodType = 5;
                            doller = 40;
                            break;
                        case 'G':
                            foodType = 6;
                            doller = 30;
                            break;
                        case 'H':
                            foodType = 7;
                            doller = 20;
                            break;
                        case 'I':
                            foodType = 8;
                            doller = 30;
                            break;
                        default:
                            foodType = 9;
                            break;
                    }
                    if(foodType != 9){//如果是規範內食物項目才做
                        numArrayPtr[foodType] += atoi(numberStr.c_str());
                        *total += discount * doller * atoi(numberStr.c_str());
                    }
                    //clear numberStr
                    numberStr = "";
                }
            }
        }
        void InputData(){
            ifstream fin("fastfoodlist.txt");
            while(fin.good()){
                string line;
                getline(fin, line, '\n');
                this->decode((char*)(line.c_str()));
            }
            fin.close();
            this->sortData(this->teacher, this->teacher_index);
            this->sortData(this->student, this->student_index);
            this->sortData(this->normal, this->normal_index);
        }
        void sortData(int number[],int temp[]){
            for(int i = 8 ; i >= 0; i--){
                for(int j = 0; j < i; j++){
                    //交換的是temp內的植而不是number
                    if(number[temp[j]] > number[temp[j + 1]]){
                        int tempNum = temp[j];
                        temp[j] = temp[j + 1];
                        temp[j + 1] = tempNum;
                    }
                }
            }
        }
        void OutputData(){
            ofstream fout("984008030.txt");
            fout << "Student" << endl \
                 << "=======================" << endl;
            for(int i = 0; i < 9; i++){
                fout << setw(15) << this->menu[i] << " :" << setw(5) << this->student[this->student_index[i]] << endl;
            }
            fout << "Total:" << this->total2 << endl \
                 << "-----------------------" << endl;

            fout << "Techer" << endl \
                 << "=======================" << endl;
            for(int i = 0; i < 9; i++){
                fout << setw(15) << this->menu[i] << " :" << setw(5) << this->teacher[this->teacher_index[i]] << endl;
            }
            fout << "Total:" << this->total1 << endl \
                 << "-----------------------" << endl;

            fout << "Normal" << endl \
                 << "=======================" << endl;
            for(int i = 0; i < 9; i++){
                fout << setw(15) << this->menu[i] << " :" << setw(5) << this->normal[this->normal_index[i]] << endl;
            }
            fout << "Total:" << this->total3 << endl \
                 << "-----------------------" << endl;
            fout << "Total Money:" << this->total1 + this->total2 + this->total3;
            fout.close();
        }

    private:
        string menu[9];
        int total1,total2,total3;
        int price[9];
        int student[9],teacher[9],normal[9];
        int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
