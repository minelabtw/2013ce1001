#ifndef F-102502033_H_INCLUDED
#define F-102502033_H_INCLUDED
#include<fstream>
#include<iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
        for(int x=0; x<9; x++)
        {
            student[x]=0;
        }
        for(int x=0; x<9; x++)
        {
            teacher[x]=0;
        }
        for(int x=0; x<9; x++)
        {
            normal[x]=0;
        }


    }
    void decode(char code[])
    {

        if(code[0]==0)
        {
            for(int i=1; i<=code[1]; i++)
            {
                switch(code[1+2*i])
                {
                case 'A':
                    teacher[0]+=a;
                    break;
                case 'B':
                    teacher[1]+=a;
                    break;
                case 'C':
                    teacher[2]+=a;
                    break;
                case 'D':
                    teacher[3]+=a;
                    break;
                case 'E':
                    teacher[4]+=a;
                    break;
                case 'F':
                    teacher[5]+=a;
                    break;
                case 'G':
                    teacher[6]+=a;
                    break;
                case 'H':
                    teacher[7]+=a;
                    break;
                case 'I':
                    teacher[8]+=a;
                    break;
                default:
                    break;
                }
            }

        }
        else if(code[0]==1)
        {
            for(int i=1; i<=code[1]; i++)
            {
                switch(code[1+2*i])
                {
                case 'A':
                    student[0]+=b;
                    break;
                case 'B':
                    student[1]+=b;
                    break;
                case 'C':
                    student[2]+=b;
                    break;
                case 'D':
                    student[3]+=b;
                    break;
                case 'E':
                    student[4]+=b;
                    break;
                case 'F':
                    student[5]+=b;
                    break;
                case 'G':
                    student[6]+=b;
                    break;
                case 'H':
                    student[7]+=b;
                    break;
                case 'I':
                    student[8]+=b;
                    break;
                default:
                    break;
                }
            }

        }
        else if(code[0]==2)
        {
            for(int i=1; i<=code[1]; i++)
            {

                switch(code[1+2*i])
                {
                case 'A':
                    normal[0]+=c;
                    break;
                case 'B':
                    normal[1]+=c;
                    break;
                case 'C':
                    normal[2]+=c;
                    break;
                case 'D':
                    normal[3]+=c;
                    break;
                case 'E':
                    normal[4]+=c;
                    break;
                case 'F':
                    normal[5]+=c;
                    break;
                case 'G':
                    normal[6]+=c;
                    break;
                case 'H':
                    normal[7]+=c;
                    break;
                case 'I':
                    normal[8]+=c;
                    break;
                default:
                    break;
                }
            }

        }
    }
    void InputData()
    {
        ifstream op("fastfoodlist.txt", ios::in);
    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData()
    {
        total1=50*student[0]+60*student[1]+40*student[2]+30*student[3]+20*student[4]+40*student[5]+30*student[6]+20*student[7]+30*student[8];
        total2=50*teacher[0]+60*teacher[1]+40*teacher[2]+30*teacher[3]+20*teacher[4]+40*teacher[5]+30*teacher[6]+20*teacher[7]+30*teacher[8];
        total3=50*normal[0]+60*normal[1]+40*normal[2]+30*normal[3]+20*normal[4]+40*normal[5]+30*normal[6]+20*normal[7]+30*normal[8];

        ofstream op("102502033.txt", ios::out);
        op << "Student" << endl;
        op << "======================" << endl;
        op << setw(17) << "Fried chicken :" << setw(5) << student[0] << endl;
        op << setw(17) << "Hamburger :" << setw(5) << student[1] << endl;
        op << setw(17) << "Chicken nuggets :" << setw(5) << student[2] << endl;
        op << setw(17) << "Cola :" << setw(5) << student[3] << endl;
        op << setw(17) << "Black tea :" << setw(5) << student[4] << endl;
        op << setw(17) << "Coffee :" << setw(5) << student[5] << endl;
        op << setw(17) << "French fries :" << setw(5) << student[6] << endl;
        op << setw(17) << "Tart :" << setw(5) << student[7] << endl;
        op << setw(17) << "Hot dog :" << setw(5) << student[8] << endl;
        op << "Total:" << total1*0.9;
        op << "----------------------" << endl;
        op << "Teacher" << endl;
        op << "======================" << endl;
        op << setw(17) << "Fried chicken :" << setw(5) << teacher[0] << endl;
        op << setw(17) << "Hamburger :" << setw(5) << teacher[1] << endl;
        op << setw(17) << "Chicken nuggets :" << setw(5) << teacher[2] << endl;
        op << setw(17) << "Cola :" << setw(5) << teacher[3] << endl;
        op << setw(17) << "Black tea :" << setw(5) << teacher[4] << endl;
        op << setw(17) << "Coffee :" << setw(5) << teacher[5] << endl;
        op << setw(17) << "French fries :" << setw(5) << teacher[6] << endl;
        op << setw(17) << "Tart :" << setw(5) << teacher[7] << endl;
        op << setw(17) << "Hot dog :" << setw(5) << teacher[8] << endl;
        op << "Total:" << total2*0.8;
        op << "----------------------" << endl;
        op << "Normal" << endl;
        op << "======================" << endl;
        op << setw(17) << "Fried chicken :" << setw(5) << normal[0] << endl;
        op << setw(17) << "Hamburger :" << setw(5) << normal[1] << endl;
        op << setw(17) << "Chicken nuggets :" << setw(5) << normal[2] << endl;
        op << setw(17) << "Cola :" << setw(5) << normal[3] << endl;
        op << setw(17) << "Black tea :" << setw(5) << normal[4] << endl;
        op << setw(17) << "Coffee :" << setw(5) << normal[5] << endl;
        op << setw(17) << "French fries :" << setw(5) << normal[6] << endl;
        op << setw(17) << "Tart :" << setw(5) << normal[7] << endl;
        op << setw(17) << "Hot dog :" << setw(5) << normal[8] << endl;
        op << "Total:" << total3;
        op << "----------------------" << endl;
        op << "Total Money:" << total1+total2+total3;

    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
