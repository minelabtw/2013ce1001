#ifndef F-102502555_H_INCLUDED
#define F-102502555_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
    public:
        POSsystem()
        {

        }
        void decode(char code[])
        {
            int temp[9] = {};
            int i = 2;

            while(code[i] != '\0'){
                if(code[i + 2] - 65 >= 0 && code[i + 2] - 65 <= 8){
                        int food = code[i + 2] - 65;
                        temp[food] = ((int)(code[i]) - 48) * 10 + ((int)(code[i + 1]) - 48);
                        i += 3;

                }else if(code[i + 1] - 65 >= 0 && code[i + 1] - 65 <= 8){
                        int food = code[i + 1] - 65;
                        temp[food] = (int)(code[i]) - 48;
                        i += 2;
                }
            }

            switch(code[0]){
                case '0':
                    for(int i = 0 ; i < 9 ; i++){
                        teacher[i] += temp[i];
                    }
                    break;

                case '1':
                    for(int i = 0 ; i < 9 ; i++){
                        student[i] += temp[i];
                    }
                    break;

                case '2':
                    for(int i = 0 ; i < 9 ; i++){
                        normal[i] += temp[i];
                    }
                    break;

                default:
                    break;

            }
        }
        void InputData()
        {
            char input[31];
            ifstream fin("fastfoodlist.txt" , ios::in);

            while(!fin.eof()){
                fin >> input;
                decode(input);
            }

            for(int i = 0 ; i < 9 ; i++){
                total1 += price[i] * teacher[i];
                total2 += price[i] * student[i];
                total3 += price[i] * normal[i];
            }

            total1 *= 0.8;
            total2 *= 0.9;
        }

        void sortData(int number[],int temp[])
        {

        }

        void OutputData()
        {
            ofstream fout("102502555.txt" , ios::out);
            fout << "Student" << endl;
            for(int i = 0 ; i < 23 ; i++){
                fout << "=";
            }
            fout << endl;
            for(int i = 0 ; i < 9 ; i++){
                fout << setw(17) << menu[i] << setw(5) << student[i] << endl;
            }
            fout << "Total:" << total2 << endl;
            for(int i = 0 ; i < 23 ; i++){
                fout << "-";
            }
            fout << endl;
            fout << "Teacher" << endl;
            for(int i = 0 ; i < 23 ; i++){
                fout << "=";
            }
            fout << endl;
            for(int i = 0 ; i < 9 ; i++){
                fout << setw(17) << menu[i] << setw(5) << teacher[i] << endl;
            }
            fout << "Total:" << total1 << endl;
            for(int i = 0 ; i < 23 ; i++){
                fout << "-";
            }
            fout << endl;
            fout << "Normal" << endl;
            for(int i = 0 ; i < 23 ; i++){
                fout << "=";
            }
            fout << endl;
            for(int i = 0 ; i < 9 ; i++){
                fout << setw(17) << menu[i] << setw(5) << normal[i] << endl;
            }
            fout << "Total:" << total3 << endl;
            for(int i = 0 ; i < 23 ; i++){
                fout << "-";
            }
            fout << endl;
            fout << "Total Money:" << total1 + total2 + total3 << endl;
        }

    private:
        string menu[9] = {"Fried chicken :" , "Hamburger :" , "Chicken nuggets :" , "Cola :" , "Black tea :" , "Coffee :" , "French fries :" , "Tart :" , "Hot dog :"};
        int total1 = 0 , total2 = 0 , total3 = 0;
        int price[9] = {50 , 60 , 40 , 30 , 20 , 40 , 30 , 20 , 30};
        int student[9] = {} , teacher[9] = {} , normal[9] = {};
        int student_index[9],teacher_index[9],normal_index[9];

};

#endif // F-102502555_H_INCLUDED
