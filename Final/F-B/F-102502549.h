#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {

    }
    void decode(char code[])
    {
        switch(code[0])//利用switch辨別身分
        {
        case '0':
            for(int i=1; i<=code[1]-'0'; i++)
            {
                for(int j=0; j<code[i*2]-'0'; j++)
                {
                    teacher[code[i*2+1]-'A']++;
                    total1+=price[code[i*2+1]-'A']*0.8;
                }
            }
            break;

        case '1':
            for(int i=1; i<=code[1]-'0'; i++)
            {
                for(int j=0; j<code[i*2]-'0'; j++)
                {
                    student[code[i*2+1]-'A']++;
                    total2+=price[code[i*2+1]-'A']*0.9;
                }
            }
            break;

        case '2':
            for(int i=1; i<=code[1]-'0'; i++)
            {
                for(int j=0; j<code[i*2]-'0'; j++)
                {
                    normal[code[i*2+1]-'A']++;
                    total3+=price[code[i*2+1]-'A'];
                }
            }
            break;
        }

    }

    void InputData()
    {
        char temp[30];
        ifstream in("fastfoodlist.txt",ios::in);

        while(!in.eof())
        {
            in>>temp;
            decode(temp);
        }
    }

    void sortData(int number[],int temp[])
    {
        int numtemp,indextemp;

        for(int i=0; i<9; i++)
        {
            for(int j=i+1; j<9; j++)
            {
                if(number[i]>number[j])
                {
                    numtemp=number[i];
                    indextemp=j;

                    number[i]=number[j];
                    temp[i]=temp[j];

                    number[j]=numtemp;
                    temp[j]=indextemp;
                }
            }
        }
    }

    void OutputData()
    {
        ofstream out("102502549.txt",ios::out);

        out<<"Student"<<endl;
        out<<"===================="<<endl;

        for(int i=0; i<9; i++)
        {
            out<<setw(15)<<menu[i]<<" : "<<student[i]<<endl;
        }

        out<<"Total:"<<total2<<endl;
        out<<"--------------------"<<endl;
        out<<"Teacher"<<endl;
        out<<"===================="<<endl;

        for(int i=0; i<9; i++)
        {
            out<<setw(15)<<menu[i]<<" : "<<teacher[i]<<endl;
        }

        out<<"Total:"<<total1<<endl;
        out<<"--------------------"<<endl;
        out<<"Normal"<<endl;
        out<<"===================="<<endl;

        for(int i=0; i<9; i++)
        {
            out<<setw(15)<<menu[i]<<" : "<<normal[i]<<endl;
        }

        out<<"Total:"<<total3<<endl;
        out<<"--------------------"<<endl;
        out<<"Total Money:"<<total1+total2+total3;

    }

private:
    string menu[9]= {"Fried chicken","Hamburger","Chicken nuggets","Cola","Blake tea","Coffee","French fries","Tart","Hot dog"};
    int total1=0,total2=0,total3=0;//存各身分總共價錢
    int price[9]= {50,60,40,30,20,40,30,20,30};//食物價錢
    int student[9]= {0},teacher[9]= {0},normal[9]= {0};//存各身分各餐點次數
    int student_index[9]= {0,1,2,3,4,5,6,7,8},teacher_index[9]= {0,1,2,3,4,5,6,7,8},normal_index[9]= {0,1,2,3,4,5,6,7,8}; //紀錄索引值

};

#endif // POS_SYSTEM_H_INCLUDED
