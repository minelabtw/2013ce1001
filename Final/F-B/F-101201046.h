#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <fstream> //for read and write files
#include <iomanip> //for setw
#include <algorithm> //for swap
#include <cstdlib> //for atoi
#include <cstring> //for strpbrk
#include <string>
using namespace std;


class POSsystem
{
    public:
        POSsystem() : total1(0), total2(0), total3(0)
        {
            int i;
            for (i = 0; i < 9; i++)
                student_index[i] = teacher_index[i] = normal_index[i] = i; //initial the index
        }

        void decode(const char *code) //decode the input
        {
            int c;
            int *log[3] = {teacher, student, normal};
            int *total[3] = {&total2, &total1, &total3};

            for (const char *p = code + 2; *p != '\0' ; p++)
            {
                c = atoi(p);

                p = strpbrk(p, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
                if (*p >= 'J') continue;

                log[code[0] - '0'][*p - 'A'] += c;
                *total[code[0] - '0'] += price[*p - 'A'] * c * (8 + (code[0] - '0')) / 10;

                if (!(*p)) break;
            }
        }

        void InputData()
        {
            ifstream in("fastfoodlist.txt"); //open the fastfoodlist
            string str;

            while (getline(in, str)) //get one line
                decode(str.c_str()); //decode input and save the account and the number of food

            in.close();
        }

        void sortData(int number[],int temp[])
        {
            for (int i = 0; i < 9; i++) //selection sort
                for (int j = i + 1; j < 9; j++)
                    if (temp[number[i]] > temp[number[j]])
                        swap(number[i], number[j]);
        }

        void OutputData()
        {
            ofstream out("101201046.txt");
            int temp[9];

/*------------student part-----------*/
            sortData(student_index, student);

            out << "Student\n"
                << string(23, '=') << endl;
            for (int  i = 0; i < 9; i++)
                out << setw(15) << menu[student_index[i]] << " :"
                    << setw( 6) << student[student_index[i]] << endl;
            out << "Total:" << total1 << endl
                << string(23, '-') << endl;

/*------------teacher part-----------*/
            sortData(teacher_index, teacher);

            out << "Teacher\n"
                << string(23, '=') << endl;
            for (int  i = 0; i < 9; i++)
                out << setw(15) << menu[teacher_index[i]] << " :"
                    << setw( 6) << teacher[teacher_index[i]] << endl;
            out << "Total:" << total2 << endl
                << string(23, '-') << endl;

/*-------------normal part ----------*/
            sortData(normal_index, normal);

            out << "normal\n"
                << string(23, '=') << endl;
            for (int  i = 0; i < 9; i++)
                out << setw(15) << menu[normal_index[i]] << " :"
                    << setw( 6) << normal[normal_index[i]] << endl;
            out << "Total:" << total3 << endl
                << string(23, '-') << endl;

/*-------------total money----------*/
            out << "Total Money:" << total1 + total2 + total3;

            out.close();
        }

    private:
        string menu[9] = {"Fried chicken", "Hamburger", "Chicken nuggets",
                          "Cola", "Black tea", "Coffee",
                          "French fries", "Tart", "Hot dog"};
        int total1,total2,total3;
        int price[9] = {50, 60, 40, 30, 20, 40, 30, 20, 30};
        int student[9] = {0},teacher[9] = {0},normal[9] = {0};
        int student_index[9],teacher_index[9],normal_index[9];
};

#endif // POS_SYSTEM_H_INCLUDED
