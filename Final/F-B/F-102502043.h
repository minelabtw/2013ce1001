#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {

    }
    void decode(char code[])
    {
        while(!afile.eof)                                                   //確認讀完檔案
        {
            int num=65;
            if(code[0]=='0')                                                //確定身分
            {
                int i;
                for(int t=0; t<9; t++)
                {
                    teacher[t]=0;                                          //清空陣列
                }
                for(i=0; i<sizeof(afile); i++)
                {
                    if(code[i]==num)
                    {
                        int j=0;
                        teacher[j]=code[i-1]+teacher[j];                   //存值
                        num++;
                        j++;
                    }
                }
            }
            else if(code[0]=='1')
            {
                int i;
                for(int s=0; s<9; s++)
                {
                    student[s]=0;
                }
                for(i=0; i<sizeof(afile); i++)
                {
                    if(code[i]==num)
                    {
                        int j=0;
                        student[j]=code[i-1]+student[j];
                        num++;
                        j++;
                    }
                }
            }
            else
            {
                int i;
                for(int n=0; n<9; n++)
                {
                    normal[n]=0;
                }
                for(i=0; i<sizeof(afile); i++)
                {
                    if(code[i]==num)
                    {
                        int j;
                        normal[j]=code[i-1]+normal[j];
                        num++;
                        j++;
                    }
                }
            }
        }
    }
    void InputData()
    {
        ifstream afile("C://fastfoodlist.txt",ios::in);     //讀檔
    }

    void sortData(int number[],int temp[])
    {
        ofstream newfile("C://102502043.txt",ios::out);
        total1=(student[0]*50+student[1]*60+student[2]*40+student[3]*30+student[4]*20+student[5]*40+student[6]*30+student[7]*20+student[8]*30)*0.8;   //顯示部分
        total2=(teacher[0]*50+teacher[1]*60+teacher[2]*40+teacher[3]*30+teacher[4]*20+teacher[5]*40+teacher[6]*30+teacher[7]*20+teacher[8]*30)*0.9;
        total3=(normal[0]*50+normal[1]*60+normal[2]*40+normal[3]*30+normal[4]*20+normal[5]*40+normal[6]*30+normal[7]*20+normal[8]*30);
        newfile<<"Student"<<endl;
        newfile<<"======================"<<endl;
        newfile<<"Fried chicken"<<setw(15)<<" :"<<student[0]<<setw(5)<<endl;
        newfile<<"Hamber"<<setw(15)<<" :"<<student[1]<<setw(5)<<endl;
        newfile<<"Chicken nuggets"<<setw(15)<<" :"<<student[2]<<setw(5)<<endl;
        newfile<<"Cola"<<setw(15)<<" :"<<student[3]<<setw(5)<<endl;
        newfile<<"Black tea"<<setw(15)<<" :"<<student[4]<<setw(5)<<endl;
        newfile<<"Coffee"<<setw(15)<<" :"<<student[5]<<setw(5)<<endl;
        newfile<<"French fires"<<setw(15)<<" :"<<student[6]<<setw(5)<<endl;
        newfile<<"Tart"<<setw(15)<<" :"<<student[7]<<setw(5)<<endl;
        newfile<<"Hot dog"<<setw(15)<<" :"<<student[8]<<setw(5)<<endl;
        newfile<<"Tatol:"<<total1;
        newfile<<"----------------------"<<endl;
        newfile<<"Teacher"<<endl;
        newfile<<"======================"<<endl;
        newfile<<"Fried chicken"<<setw(15)<<" :"<<teacher[0]<<setw(5)<<endl;
        newfile<<"Hamber"<<setw(15)<<" :"<<teacher[1]<<setw(5)<<endl;
        newfile<<"Chicken nuggets"<<setw(15)<<" :"<<teacher[2]<<setw(5)<<endl;
        newfile<<"Cola"<<setw(15)<<" :"<<teacher[3]<<setw(5)<<endl;
        newfile<<"Black tea"<<setw(15)<<" :"<<teacher[4]<<setw(5)<<endl;
        newfile<<"Coffee"<<setw(15)<<" :"<<teacher[5]<<setw(5)<<endl;
        newfile<<"French fires"<<setw(15)<<" :"<<teacher[6]<<setw(5)<<endl;
        newfile<<"Tart"<<setw(15)<<" :"<<teacher[7]<<setw(5)<<endl;
        newfile<<"Hot dog"<<setw(15)<<" :"<<teacher[8]<<setw(5)<<endl;
        newfile<<"Tatol:"<<total2;
        newfile<<"----------------------"<<endl;
        newfile<<"Normal"<<endl;
        newfile<<"======================"<<endl;
        newfile<<"Fried chicken"<<setw(15)<<" :"<<normal[0]<<setw(5)<<endl;
        newfile<<"Hamber"<<setw(15)<<" :"<<normal[1]<<setw(5)<<endl;
        newfile<<"Chicken nuggets"<<setw(15)<<" :"<<normal[2]<<setw(5)<<endl;
        newfile<<"Cola"<<setw(15)<<" :"<<normal[3]<<setw(5)<<endl;
        newfile<<"Black tea"<<setw(15)<<" :"<<normal[4]<<setw(5)<<endl;
        newfile<<"Coffee"<<setw(15)<<" :"<<normal[5]<<setw(5)<<endl;
        newfile<<"French fires"<<setw(15)<<" :"<<normal[6]<<setw(5)<<endl;
        newfile<<"Tart"<<setw(15)<<" :"<<normal[7]<<setw(5)<<endl;
        newfile<<"Hot dog"<<setw(15)<<" :"<<normal[8]<<setw(5)<<endl;
        newfile<<"Tatol:"<<total3;
        newfile<<"----------------------"<<endl;
        newfile<<"Total Money:"<<(total1+total2+total3);
    }

    void OutputData()
    {
        ofstream newfile("C://102502043.txt",ios::out);   //寫入檔案
    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
