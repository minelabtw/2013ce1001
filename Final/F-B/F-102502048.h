#ifndef F-102502048_H_INCLUDED
#define F-102502048_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
        for(int i=0; i<9; i++)//初始化
        {
            student[i]=0;
            teacher[i]=0;
            normal[i]=0;
        }
    }
    int decode(char code)//解碼
    {
        int temp=0;
        temp=(int)code-65;
        return temp;
    }
    void InputData()
    {
        ifstream file("fastfoodlist.txt",ios::in);
        while(!file.eof())
        {
            file >> ID;//身分
            file >> fn;//種類多寡
            int _fn=(int)fn-48;
            for(int x=0; x<_fn; x++)
            {
                file >> num;//數量
                file >> foodcode;//食物種類
                food=decode(foodcode);
                switch (ID)
                {
                case '0' ://老師
                {
                    total2=price[food]*num+total2;//總金額
                    teacher[food]=teacher[food]+num;//數量
                    break;
                }
                case '1' ://學生
                {
                    total1=price[food]*num+total1;//總金額
                    student[food]=student[food]+num;//數量
                    break;
                }
                case '2' ://其他
                {
                    total3=price[food]*num+total3;//總金額
                    normal[food]=normal[food]+num;//數量
                    break;
                }
                }
            }
        }
    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData()//輸出
    {
        ofstream of("102502048.txt",ios::out);
        of<<"Student"<<endl<<"===================="<<endl;
        for(int k=0; k<9; k++)
            of<<setw(15)<<menu[k]<<" : "<<student[k]<<endl;
        of<<"Total:"<<total1<<endl<<"-------------------"<<endl;

        of<<"Teacher"<<endl<<"===================="<<endl;
        for(int k=0; k<9; k++)
            of<<setw(15)<<menu[k]<<" : "<<teacher[k]<<endl;
        of<<"Total:"<<total2<<endl<<"-------------------"<<endl;

        of<<"Normal"<<endl<<"===================="<<endl;
        for(int k=0; k<9; k++)
            of<<setw(15)<<menu[k]<<" : "<<normal[k]<<endl;
        of<<"Total:"<<total3<<endl<<"-------------------"<<endl;
    }

private:
    string menu[9]= {"Fried chicken","Hamburger","Chicken nuggets","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
    int total1=0,total2=0,total3=0;//總金額 學生 老師 其他
    int price[9]= {50,60,40,30,20,40,30,20,30};
    int student[9],teacher[9],normal[9];//食物種類
    int student_index[9],teacher_index[9],normal_index[9];
    char ID;
    char fn;
    int num=0;
    int food=0;
    char foodcode;
};

#endif // POS_SYSTEM_H_INCLUDED
