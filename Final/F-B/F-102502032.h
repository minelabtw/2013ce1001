#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
#include <iostream>
using namespace std;

class POSsystem
{
public:
    POSsystem()     //constructor
    {
        menu[0] = "Fried chicken";
        menu[1] = "Hamburger";
        menu[2] = "Chicken nuggets";
        menu[3] = "Cola";
        menu[4] = "Black tea";
        menu[5] = "Coffee";
        menu[6] = "French fries";
        menu[7] = "Tart";
        menu[8] = "Hot dog";
        total1 = 0;
        total2 = 0;
        total3 = 0;
        for (int i = 0; i < 9; i ++)
        {
            price[i] = 0;
            student[i] = 0;
            teacher[i] = 0;
            normal[i] = 0;
            student_index[i] = 0;
            teacher_index[i] = 0;
            normal_index[i] = 0;

        }
    }

    void decodestr(const char code[])
    {
        int temp = 0;
        string codestr = code;

        switch (codestr[0])
        {
        case '0':
            for (int i = 2; i < codestr.size(); i ++)
            {
                switch (codestr[i])
                {
                case 'A':
                    teacher[0] += temp;
                    total2 += 50 * temp;
                    temp = 0;
                    break;
                case 'B':
                    teacher[1] += temp;
                    total2 += 60 * temp;
                    temp = 0;
                    break;
                case 'C':
                    teacher[2] += temp;
                    total2 += 40 * temp;
                    temp = 0;
                    break;
                case 'D':
                    teacher[3] += temp;
                    total2 += 30 * temp;
                    temp = 0;
                    break;
                case 'E':
                    teacher[4] += temp;
                    total2 += 20 * temp;
                    temp = 0;
                    break;
                case 'F':
                    teacher[5] += temp;
                    total2 += 40 * temp;
                    temp = 0;
                    break;
                case 'G':
                    teacher[6] += temp;
                    total2 += 30 * temp;
                    temp = 0;
                    break;
                case 'H':
                    teacher[7] += temp;
                    total2 += 20 * temp;
                    temp = 0;
                    break;
                case 'I':
                    teacher[8] += temp;
                    total2 += 30 * temp;
                    temp = 0;
                    break;
                case 'J':
                    temp = 0;
                    break;
                default:
                    temp = temp * 10 + (static_cast <int> (codestr[i]) - 48);
                    break;
                };
            }

            break;
        case '1':
            for (int i = 2; i < codestr.size(); i ++)
            {
                switch (codestr[i])
                {
                case 'A':
                    student[0] += temp;
                    total1 += 50 * temp;
                    temp = 0;
                    break;
                case 'B':
                    student[1] += temp;
                    total1 += 60 * temp;
                    temp = 0;
                    break;
                case 'C':
                    student[2] += temp;
                    total1 += 40 * temp;
                    temp = 0;
                    break;
                case 'D':
                    student[3] += temp;
                    total1 += 30 * temp;
                    temp = 0;
                    break;
                case 'E':
                    student[4] += temp;
                    total1 += 20 * temp;
                    temp = 0;
                    break;
                case 'F':
                    student[5] += temp;
                    total1 += 40 * temp;
                    temp = 0;
                    break;
                case 'G':
                    student[6] += temp;
                    total1 += 30 * temp;
                    temp = 0;
                    break;
                case 'H':
                    student[7] += temp;
                    total1 += 20 * temp;
                    temp = 0;
                    break;
                case 'I':
                    student[8] += temp;
                    total1 += 30 * temp;
                    temp = 0;
                    break;
                case 'J':
                    temp = 0;
                    break;
                default:
                    temp = temp * 10 + (static_cast <int> (codestr[i]) - 48);
                    break;
                };
            }
            break;
        case '2':
            for (int i = 2; i < codestr.size(); i ++)
            {
                switch (codestr[i])
                {
                case 'A':
                    normal[0] += temp;
                    total3 += 50 * temp;
                    temp = 0;
                    break;
                case 'B':
                    normal[1] += temp;
                    total3 += 60 * temp;
                    temp = 0;
                    break;
                case 'C':
                    normal[2] += temp;
                    total3 += 40 * temp;
                    temp = 0;
                    break;
                case 'D':
                    normal[3] += temp;
                    total3 += 30 * temp;
                    temp = 0;
                    break;
                case 'E':
                    normal[4] += temp;
                    total3 += 20 * temp;
                    temp = 0;
                    break;
                case 'F':
                    normal[5] += temp;
                    total3 += 40 * temp;
                    temp = 0;
                    break;
                case 'G':
                    normal[6] += temp;
                    total3 += 30 * temp;
                    temp = 0;
                    break;
                case 'H':
                    normal[7] += temp;
                    total3 += 20 * temp;
                    temp = 0;
                    break;
                case 'I':
                    normal[8] += temp;
                    total3 += 30 * temp;
                    temp = 0;
                    break;
                case 'J':
                    temp = 0;
                    break;
                default:
                    temp = temp * 10 + (static_cast <int> (codestr[i]) - 48);
                    break;
                };
            }
            break;
        default:
            break;
        };
    }

    void InputData()    //load data
    {
        ifstream get("fastfoodlist.txt", ios::in);
        string temp;    //temporary

        while (get >> temp)
        {
            decodestr(temp.c_str());
        };
        get.close();
    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData()       //make a list to a file
    {
        //sortData(student, student_index);
        //sortData(teacher, teacher_index);
        //sortData(normal, normal_index);

        ofstream put("102502032.txt", ios::out);

        put << "Student" << endl
            << "======================" << endl;
        for (int i = 0; i < 9; i ++)
            put << setw(15) << menu[i] << " :" << setw(5) << student[i] << endl;
        put << "Total:" << total1 << endl
            << "----------------------" << endl;


        put << "Teacher" << endl
            << "======================" << endl;
        for (int i = 0; i < 9; i ++)
            put << setw(15) << menu[i] << " :" << setw(5) << teacher[i] << endl;
        put << "Total:" << total2 << endl
            << "----------------------" << endl;

        put << "Normal" << endl
            << "======================" << endl;
        for (int i = 0; i < 9; i ++)
            put << setw(15) << menu[i] << " :" << setw(5) << normal[i] << endl;
        put << "Total:" << total3 << endl
            << "----------------------" << endl;

        put << "Student" << endl
            << "======================" << endl;
        for (int i = 0; i < 9; i ++)
            put << setw(15) << menu[student_index[i]] << " :" << setw(5) << student[student_index[i]] << endl;
        put << "Total:" << total1 << endl
            << "----------------------" << endl;

        put << "Total Money:" << total1 + total2 + total3;

        /*
                put << "Teacher" << endl
                    << "======================" << endl;
                for (int i = 0; i < 9; i ++)
                    put << setw(15) << menu[teacher_index[i]] << " :" << setw(5) << teacher[teacher_index[i]] << endl;
                put << "Total:" << total2 << endl
                    << "----------------------" << endl;

                put << "Normal" << endl
                    << "======================" << endl;
                for (int i = 0; i < 9; i ++)
                    put << setw(15) << menu[normal_index[i]] << " :" << setw(5) << normal[normal_index[i]] << endl;
                put << "Total:" << total3 << endl
                    << "----------------------" << endl;
        */
        put.close();
    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];
};

#endif // POS_SYSTEM_H_INCLUDED
