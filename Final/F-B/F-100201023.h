#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <cmath>
#include <vector>
using namespace std;

class POSsystem
{
    public:
        POSsystem()
        {
            // initial menu
            menu[0] = "Fried chicken";
            menu[1] = "Hamburger";
            menu[2] = "Chickcen nuggets";
            menu[3] = "Cola";
            menu[4] = "Black tea";
            menu[5] = "Coffee";
            menu[6] = "French fries";
            menu[7] = "Tart";
            menu[8] = "Hot dog";

            // initial total
            total1 = total2 = total3 = 0;

            // initial price
            price[0] = 50;
            price[1] = 60;
            price[2] = 40;
            price[3] = 30;
            price[4] = 20;
            price[5] = 40;
            price[6] = 30;
            price[7] = 20;
            price[8] = 30;

            // inital others
            for(int i = 0 ; i < 9 ; ++i)
            {
                student[i] = teacher[i] = normal[i] = 0;
                student_index[i] = teacher_index[i] = normal_index[i] = i;
            }
        }
        void decode(char code[])
        {
            string type;
            int count , id , number , place , j;
            vector <int> temp;

            id = code[0]; // read cosume type
            id -= '0';
            number = code[1]; // read buy ratio
            number -= '0';

            j = 2;
            for(int i = 0 ; i < number ; ++i)
            {
                temp.clear();
                while(true)
                {
                    if(code[j] > '9' || code[j] < '0')
                        break;
                    temp.push_back(code[j] - '0');
                    ++j;
                }

                count = 0;
                for(int k = 0 ; k < temp.size() ; ++k)
                {
                    count += temp[k] * pow(10.0 , temp.size() - k - 1);
                }

                type = code[j]; // read food type
                ++j;
                place = type[0] - 'A'; // determine if 'J'
                if(place > 8)
                    continue;

                // store data
                // store data
                if(id == 0)
                {
                    teacher[place] += count;
                    total1 += count * price[place] * 0.8;
                }
                else if(id == 1)
                {
                    student[place] += count;
                    total2 += count * price[place] * 0.9;
                }
                else
                {
                    normal[place] += count;
                    total3 += count * price[place];
                }
            }
        }
        void InputData()
        {
            ifstream fin("fastfoodlist.txt"); // open file
            string data;
            char *cdata;

            while(getline(fin , data))
            {
                cdata = new char[data.size()];
                data.copy(cdata , data.size());
                decode(cdata);
                delete cdata;
            }
        }

        void sortData(int number[],int temp[])
        {
            for(int i = 0 ; i < 9 ; ++i)
            {
                for(int j = i + 1 ; j < 9 ; ++j)
                {
                    if(number[j] < number[i]) // detemine if change
                    {
                        swap(number[i] , number[j]);
                        swap(temp[i] , temp[j]);
                    }
                }
            }
        }

        void OutputData()
        {
            ofstream fout("100201023.txt");
            string line[2] = {"=========================" , "-------------------------"};

            // output student
            sortData(student , student_index);
            fout << "Student" << endl << line[0] << endl;
            for(int i = 0 ; i < 9 ; ++i)
                fout << setw(16) << menu[student_index[i]] << " : " << setw(4) << student[i] << endl;
            fout << "Total:" << total2 << endl;
            fout << line[1] << endl;

            // output teacher
            sortData(teacher , teacher_index);
            fout << "Teacher" << endl << line[0] << endl;
            for(int i = 0 ; i < 9 ; ++i)
                fout << setw(16) << menu[teacher_index[i]] << " : " << setw(4) << teacher[i] << endl;
            fout << "Total:" << total1 << endl;
            fout << line[1] << endl;

            // output normal
            sortData(normal , normal_index);
            fout << "Normal" << endl << line[0] << endl;
            for(int i = 0 ; i < 9 ; ++i)
                fout << setw(16) << menu[normal_index[i]] << " : " << setw(4) << normal[i] << endl;
            fout << "Total:" << total3 << endl;
            fout << line[1] << endl;

            // output total money
            fout << "Total Money:" << total1 + total2 + total3 << endl;
        }
public:
    //private:
        string menu[9];
        int total1,total2,total3;
        int price[9];
        int student[9],teacher[9],normal[9];
        int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
