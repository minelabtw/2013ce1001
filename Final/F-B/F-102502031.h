#ifndef F-102502031_H_INCLUDED
#define F-102502031_H_INCLUDED

#include <fstream>
#include <iomanip>

using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
        menu[0]="Fried chicken";
        menu[1]="Hamburger";
        menu[2]="Chicken nuggets";
        menu[3]="Cola";
        menu[4]="Black tea";
        menu[5]="Coffee";
        menu[6]="French fries";
        menu[7]="Tart";
        menu[8]="Hot dog";
        total1=0;
        total2=0;
        total3=0;
        price[0]=50;
        price[1]=60;
        price[2]=40;
        price[3]=30;
        price[4]=20;
        price[5]=40;
        price[6]=30;
        price[7]=20;
        price[8]=30;
        for (int i=0; i<9; i++)
        {
            student[i]=0;
            teacher[i]=0;
            normal[i]=0;
            student_index[i]=0;
            teacher_index[i]=0;
            normal_index[i]=0;
        }
        for (int i=0; i<38; i++)
        {
            a[i]='\0';
        }
    }

    void decode(char code[], int index[])
    {
        int temp=0;
        for (int i=2; code[i]!='\0'; i++)
        {
            switch (code[i])
            {
            case 'A':
                index[0]=index[0]+temp;
                temp=0;
                break;
            case 'B':
                index[1]=index[1]+temp;
                temp=0;
                break;
            case 'C':
                index[2]=index[2]+temp;
                temp=0;
                break;
            case 'D':
                index[3]=index[3]+temp;
                temp=0;
                break;
            case 'E':
                index[4]=index[4]+temp;
                temp=0;
                break;
            case 'F':
                index[5]=index[5]+temp;
                temp=0;
                break;
            case 'G':
                index[6]=index[6]+temp;
                temp=0;
                break;
            case 'H':
                index[7]=index[7]+temp;
                temp=0;
                break;
            case 'I':
                index[8]=index[8]+temp;
                temp=0;
                break;
            case '0':
                temp=temp*10+0;
                break;
            case '1':
                temp=temp*10+1;
                break;
            case '2':
                temp=temp*10+2;
                break;
            case '3':
                temp=temp*10+3;
                break;
            case '4':
                temp=temp*10+4;
                break;
            case '5':
                temp=temp*10+5;
                break;
            case '6':
                temp=temp*10+6;
                break;
            case '7':
                temp=temp*10+7;
                break;
            case '8':
                temp=temp*10+8;
                break;
            case '9':
                temp=temp*10+9;
                break;
            default:
                temp=0;
                break;
            }

        }
    }

    void InputData()
    {
        ifstream getcode("fastfoodlist.txt", ios::in);
        while (getcode >> a)
        {
            switch (a[0])
            {
            case '0':
                decode(a, teacher_index);
                break;
            case '1':
                decode(a, student_index);
                break;
            case '2':
                decode(a, normal_index);
                break;
            default:
                break;
            }
            for (int i=0; i<38; i++)
            {
                a[i]='\0';
            }
        }
        for (int i=0; i<9; i++)
            total1=total1+student_index[i]*price[i]*0.9;
        for (int i=0; i<9; i++)
            total2=total2+teacher_index[i]*price[i]*0.8;
        for (int i=0; i<9; i++)
            total3=total3+normal_index[i]*price[i];
    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData()
    {
        ofstream print("102502031.txt", ios::out);
        print << "Student" << endl;
        for (int i=0; i<23; i++)
            print << "=";
        print << endl;
        for (int i=0; i<9; i++)
        {
            print << setw(15) << menu[i] << " : " << setw(4) << student_index[i] << endl;
        }
        print << "Total:" << total1 << endl;
        for (int i=0; i<23; i++)
            print << "-";
        print << endl;
        print << "Teacher" << endl;
        for (int i=0; i<23; i++)
            print << "=";
        print << endl;
        for (int i=0; i<9; i++)
        {
            print << setw(15) << menu[i] << " : " << setw(4) << teacher_index[i] << endl;
        }
        print << "Total:" << total2 << endl;
        for (int i=0; i<23; i++)
            print << "-";
        print << endl;
        print << "Normal" << endl;
        for (int i=0; i<23; i++)
            print << "=";
        print << endl;
        for (int i=0; i<9; i++)
        {
            print << setw(15) << menu[i] << " : " << setw(4) << normal_index[i] << endl;
        }
        print << "Total:" << total3 << endl;
        for (int i=0; i<23; i++)
            print << "-";
        print << endl;
        print << "Total Money:" << total1+total2+total3 << endl;
    }

private:
    string menu[9];
    char a[38];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif    //F-102502031_H_INCLUDED
