#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
        menu[0]="Fried chicken : ";
        menu[1]="Hamburger : ";
        menu[2]="Chicken nuggets : ";
        menu[3]="Cola : ";
        menu[4]="Black tea : ";
        menu[5]="Coffee : ";
        menu[6]="French fries : ";
        menu[7]="Tart : ";
        menu[8]="Hot dog : ";
        price[0]=50;
        price[1]=60;
        price[2]=40;
        price[3]=30;
        price[4]=20;
        price[5]=40;
        price[6]=30;
        price[7]=20;
        price[8]=30;
        type[1]={};
    }
    void decode(char code[])
    {
        if(kind/1000==2 || kind/100==2)
        {
            normal[(int)(type[0])-65]+num;//計算次數
            total1=total1+price[(int)(type[0])-65]*num;//計算總和
        }
        else if(kind/1000==1 || kind/100==1)
        {
            student[(int)(type[0])-65]+num;
            total2=total2+price[(int)(type[0])-65]*num*0.9;
        }
        else
        {
            teacher[(int)(type[0])-65]+num;
            total3=total3+price[(int)(type[0])-65]*num*0.8;
        }
    }
    void InputData()
    {
        ifstream buylist("fastfoodlist.txt",ios::in);
        while(buylist.eof())//記錄資料
        {
            buylist >> kind >> type[0];
            if(kind>1000)
            {
                num = kind-(kind/100)*100;
                kind=(kind-(kind/1000)*1000)/100;
            }
            else if(kind>100)
            {
                num = kind-(kind/10)*10;
                kind=(kind-(kind/100)*100)/10;
            }
            else
            {
                num = kind-(kind/10)*10;
                kind= kind/10;
            }
            decode(type);
            for(int i=1; i<kind; i++)
            {
                buylist >> num >> type[0];
                decode(type);
            }
        }
    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData()
    {
        ofstream result("102502541.txt",ios::out);

        result << "Student" << endl << endl;
        for(int i=0; i<9; i++)
        {
            result << menu[i] << student[i] << endl;
        }
        result << "Total:" << total2 << endl << endl << "Teacher" << endl << endl;
        for(int i=0; i<9; i++)
        {
            result << menu[i] << teacher[i] << endl;
        }
        result << "Total:" << total3 << endl << endl << "Normal" << endl << endl;
        for(int i=0; i<9; i++)
        {
            result << menu[i] << normal[i] << endl;
        }
        result << "Total:" << total1 << endl << endl << "Total Money:" << total1+total2+total3;
    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];
    int kind;
    int num;
    char type[1];

};

#endif // POS_SYSTEM_H_INCLUDED
