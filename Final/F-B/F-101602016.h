#include<iostream>
#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {

    }
    void decode(char code[])
    {
        int note[10],n=1,number[3],number_1=0;
        for(int i=0; i<9; i++)
            note[i]=0;

        note[0]=2;

        for(int i=0; i<32; i++)
        {
            if(code[i]=='A'||code[i]=='B'||code[i]=='C'||code[i]=='D'||code[i]=='E'||code[i]=='F'||code[i]=='G'||code[i]=='H'||code[i]=='I')
            {
                note[n]=i+1;
                n++;
            }
        }
        /*for(int i=0;i<9;i++)
            cout<<note[i]<<" ";
        cout<<n<<endl;*/

        for(int j=0; j<n-1; j++)
        {
            if(note[j+1]-note[j]==2)
            {
                if(code[note[j]+1]=='0')
                    number[0]=0;
                else if(code[note[j]+1]=='1')
                    number[0]=1;
                else if(code[note[j]+1]=='2')
                    number[0]=2;
                else if(code[note[j]+1]=='3')
                    number[0]=3;
                else if(code[note[j]+1]=='4')
                    number[0]=4;
                else if(code[note[j]+1]=='5')
                    number[0]=5;
                else if(code[note[j]+1]=='6')
                    number[0]=6;
                else if(code[note[j]+1]=='7')
                    number[0]=7;
                else if(code[note[j]+1]=='8')
                    number[0]=8;
                else if(code[note[j]+1]=='9')
                    number[0]=9;
                number_1=number[0];
            }
            else if(note[j+1]-note[j]==3)
            {
                for(int k=0; k<2; k++)
                {
                    if(code[note[j]+1+k]=='0')
                        number[k]=0;
                    else if(code[note[j]+1+k]=='1')
                        number[k]=1;
                    else if(code[note[j]+1+k]=='2')
                        number[k]=2;
                    else if(code[note[j]+1+k]=='3')
                        number[k]=3;
                    else if(code[note[j]+1+k]=='4')
                        number[k]=4;
                    else if(code[note[j]+1+k]=='5')
                        number[k]=5;
                    else if(code[note[j]+1+k]=='6')
                        number[k]=6;
                    else if(code[note[j]+1+k]=='7')
                        number[k]=7;
                    else if(code[note[j]+1+k]=='8')
                        number[k]=8;
                    else if(code[note[j]+1+k]=='9')
                        number[k]=9;
                }
                number_1=number[0]*10+number[1];
            }
            else if(note[j+1]-note[j]==4)
            {
                for(int k=0; k<2; k++)
                {
                    if(code[note[j]+1+k]=='0')
                        number[k]=0;
                    else if(code[note[j]+1+k]=='1')
                        number[k]=1;
                    else if(code[note[j]+1+k]=='2')
                        number[k]=2;
                    else if(code[note[j]+1+k]=='3')
                        number[k]=3;
                    else if(code[note[j]+1+k]=='4')
                        number[k]=4;
                    else if(code[note[j]+1+k]=='5')
                        number[k]=5;
                    else if(code[note[j]+1+k]=='6')
                        number[k]=6;
                    else if(code[note[j]+1+k]=='7')
                        number[k]=7;
                    else if(code[note[j]+1+k]=='8')
                        number[k]=8;
                    else if(code[note[j]+1+k]=='9')
                        number[k]=9;
                }
                number_1=number[0]*100+number[1]*10+number[2];
            }


            if(code[0]=='0')
            {
                if(code[note[j+1]]=='A')
                    teacher[0]+=number_1;
                else if(code[note[j+1]]=='B')
                    teacher[1]+=number_1;
                else if(code[note[j+1]]=='C')
                    teacher[2]+=number_1;
                else if(code[note[j+1]]=='D')
                    teacher[3]+=number_1;
                else if(code[note[j+1]]=='E')
                    teacher[4]+=number_1;
                else if(code[note[j+1]]=='F')
                    teacher[5]+=number_1;
                else if(code[note[j+1]]=='G')
                    teacher[6]+=number_1;
                else if(code[note[j+1]]=='H')
                    teacher[7]+=number_1;
                else if(code[note[j+1]]=='I')
                    teacher[8]+=number_1;
            }
            else if(code[0]=='1')
            {
                if(code[note[j+1]]=='A')
                    student[0]+=number_1;
                else if(code[note[j+1]]=='B')
                    student[1]+=number_1;
                else if(code[note[j+1]]=='C')
                    student[2]+=number_1;
                else if(code[note[j+1]]=='D')
                    student[3]+=number_1;
                else if(code[note[j+1]]=='E')
                    student[4]+=number_1;
                else if(code[note[j+1]]=='F')
                    student[5]+=number_1;
                else if(code[note[j+1]]=='G')
                    student[6]+=number_1;
                else if(code[note[j+1]]=='H')
                    student[7]+=number_1;
                else if(code[note[j+1]]=='I')
                    student[8]+=number_1;
            }
            else if(code[0]=='2')
            {
                if(code[note[j+1]]=='A')
                    normal[0]+=number_1;
                else if(code[note[j+1]]=='B')
                    normal[1]+=number_1;
                else if(code[note[j+1]]=='C')
                    normal[2]+=number_1;
                else if(code[note[j+1]]=='D')
                    normal[3]+=number_1;
                else if(code[note[j+1]]=='E')
                    normal[4]+=number_1;
                else if(code[note[j+1]]=='F')
                    normal[5]+=number_1;
                else if(code[note[j+1]]=='G')
                    normal[6]+=number_1;
                else if(code[note[j+1]]=='H')
                    normal[7]+=number_1;
                else if(code[note[j+1]]=='I')
                    normal[8]+=number_1;
            }
        }
    }
    void InputData()
    {
        for(int i=0; i<9; i++)
            student[i]=0;
        for(int i=0; i<9; i++)
            teacher[i]=0;
        for(int i=0; i<9; i++)
            normal[i]=0;

        ifstream inputfile("fastfoodlist.txt");
        ofstream outputfile("101602016.txt");
        string code_;
        for(int i=0; i<100; i++)
        {
            inputfile>>code_;
            outputfile<<code_<<'Z'<<endl;
        }

        ifstream input("101602016.txt");
        char code[32];
        int n=-1;

        for(int i=0; i<100; i++)
        {
            for(int i=0; i<32; i++)
            code[i]='X';

            n=-1;
            do
            {
                n++;
                input>>code[n];
            }
            while(code[n]!='Z');

            decode(code);
        }

    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData()
    {
        ofstream output("101602016.txt");
        output<<"Sutdent"<<endl<<"========================="<<endl;
        output<<"  Fried chicken : "<<student[0]<<endl;
        output<<"      Hamburger : "<<student[1]<<endl;
        output<<"Chicken nuggets : "<<student[2]<<endl;
        output<<"           Cola : "<<student[3]<<endl;
        output<<"      Black tea : "<<student[4]<<endl;
        output<<"         Coffee : "<<student[5]<<endl;
        output<<"   French fries : "<<student[6]<<endl;
        output<<"           Tart : "<<student[7]<<endl;
        output<<"        Hot dog : "<<student[8]<<endl;
        for(int i=0; i<9; i++)
            total1+=student[i]*0.9;
        output<<"Total:"<<total1<<endl;
    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
