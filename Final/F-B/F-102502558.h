#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
    public:
        POSsystem()
        {
            // init
            total1 = total2 = total3 = 0;
            for (int i=0;i<9;i++)
            {
                student[i] = 0;
                teacher[i] = 0;
                normal[i] = 0;
                student_index[i] = 0;
                teacher_index[i] = 0;
                normal_index[i] = 0;
            }
        }
        void decode(char code[])
        {
            int type = code[0]-'0'; // check what type
            int num = 0; // store number
            int food; // food index
            string temp; // temp number in string
            int i = 2; // start from 2
            int c = code[1]-'0'; //  counter
            while (c--)
            {
                temp = "";
                while (code[i] >= '0' && code[i] <= '9') // put number to temp
                    temp += code[i++];

                num = str_to_int(temp); // convert string to int
                food = code[i]-'A'; // food type
                if (code[i] == 'J') // if J ignore
                {
                    i++;
                    continue;
                }
                if (type == 0)
                    teacher[food] += num;
                else if (type == 1)
                    student[food] += num;
                else
                    normal[food] += num;
                i++;
            }
        }
        void InputData()
        {
            fstream fin("fastfoodlist.txt",ios::in);
            char s[1000];
            while(fin >> s)
                if (s[0] != '\0' && s[0] != ' ' && s[0] != '\n' && s[0] != '\t') // if s not whitespaces
                    decode(s);
            fin.close();
        }

        void sortData(int number[],int temp[])
        {
            // sort data without changing original data
            // temp to store index
            for (int i=0;i<9;i++)
                temp[i] = i;
            for (int i=0;i<9;i++)
            {
                for (int j=i;j<9;j++)
                {
                    if (number[temp[i]] > number[temp[j]])
                    {
                        int t = temp[i];
                        temp[i] = temp[j];
                        temp[j] = t;
                    }
                }
            }
        }

        void OutputData()
        {
            // before output we sort the data
            sortData(student, student_index);
            sortData(teacher, teacher_index);
            sortData(normal, normal_index);
            fstream fout("102502558.txt",ios::out);
            fout << "Student" << endl;
            fout << "=======================" << endl;
            for (int i=0;i<9;i++)
            {
                fout << setw(15) << menu[student_index[i]] << " :" << setw(5) << student[student_index[i]] << endl; // use student_index to show sorted data
                total1 += student[i] * price[i];
            }
            fout << "Total : " << total1 * 0.9 << endl;
            fout << "-----------------------" << endl;

            fout << "Teacher" << endl;
            fout << "=======================" << endl;
            for (int i=0;i<9;i++)
            {
                fout << setw(15) << menu[teacher_index[i]] << " :" << setw(5) << teacher[teacher_index[i]] << endl;
                total2 += teacher[i] * price[i];
            }
            fout << "Total : " << total2 * 0.8 << endl;
            fout << "-----------------------" << endl;

            fout << "Normal" << endl;
            fout << "=======================" << endl;
            for (int i=0;i<9;i++)
            {
                fout << setw(15) << menu[normal_index[i]] << " :" << setw(5) << normal[normal_index[i]] << endl;
                total3 += normal[i] * price[i];
            }

            fout << "Total : " << total3 << endl;
            fout << "-----------------------" << endl;
            fout << "Total Money:" << total1 * 0.9 + total2 * 0.8 + total3 << endl;
            fout.close();
        }

    private:
        static string menu[9];
        int total1, total2, total3;
        static int price[9];
        int student[9],teacher[9],normal[9];
        int student_index[9],teacher_index[9],normal_index[9];

        int str_to_int(string s)
        {
            // convert string to int
            int n = 0;
            for (int i=0;i<s.length();i++)
                n = n * 10 + s[i] - '0';
            return n;
        }
};
string POSsystem::menu[9] = {"Fried chicken", "Hamburger", "Chicken nuggets", "Cola", "Black tea", "Coffee", "French fries", "Tart", "Hot dog"};
int POSsystem::price[9] = {50,60,40,30,20,40,30,20,30};

#endif // POS_SYSTEM_H_INCLUDED
