#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
        student[ 9 ]= {};
        teacher[ 9 ]= {};
        normal[ 9 ]= {};
        price[ 0 ]=50;
        price[ 1 ]=60;
        price[ 2 ]=40;
        price[ 3 ]=30;
        price[ 4 ]=20;
        price[ 5 ]=40;
        price[ 6 ]=30;
        price[ 7 ]=20;
        price[ 8 ]=30;
        menu[ 0 ]="Fried chicken";
        menu[ 1 ]="Hamburger";
        menu[ 2 ]="Chicken nuggets";
        menu[ 3 ]="Cola";
        menu[ 4 ]="Black tea";
        menu[ 5 ]="Coffee";
        menu[ 6 ]="French fries";
        menu[ 7 ]="Tart";
        menu[ 8 ]="Hot dog";
        student_index[ 9 ]= {};
        teacher_index[ 9 ] = {};
        normal_index[ 9 ] = {};
        total1=0;
        total2=0;
        total3=0;
    }
    void decode(char code[])
    {
        int num=0;
        int i;
        for( int j=2; j<29; j++)
        {
            if ( code[ j ]!=NULL)
            {
                if( code[ j ]=='A' )
                {
                    i=0;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else if( code[ j ]=='B' )
                {
                    i=1;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else if( code[ j ]=='C' )
                {
                    i=2;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else if( code[ j ]=='D' )
                {
                    i=3;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else if( code[ j ]=='E' )
                {
                    i=4;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else if( code[ j ]=='F' )
                {
                    i=5;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else if( code[ j ]=='G' )
                {
                    i=6;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else if( code[ j ]=='H' )
                {
                    i=7;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else if( code[ j ]=='I' )
                {
                    i=8;
                    num*=price[ i ];
                    switch( code[ 0 ] )
                    {
                    case '0':
                        teacher_index[ i ]+=num;
                        num*=0.8;
                        teacher[ i ]+=num;
                        break;
                    case '1':
                        student_index[ i ]+=num;
                        num*=0.9;
                        student[ i ]+=num;
                        break;
                    case '2':
                        normal_index[ i ]+=num;
                        normal[ i ]+=num;
                        break;
                    }
                }
                else
                {
                    num*=10;
                    num+=code[ j ];
                }
            }
        }
    }
    void InputData()
    {
        ifstream data( "fastfoodlist.txt", ios::in );
        char code[29]= {};
        while( !data.eof() )
        {
            for( int x=0; x<29; x++ )
            {
                while( data >> code[ x ] )
                {
                    decode( code );
                }
            }
        }
    }
    void sortData(int number[],int temp[])
    {

    }

    void OutputData()
    {
        ofstream result( "102502030.txt", ios::out );
        result << "Student" << endl;
        result << "=======================" << endl;
        for( int k=0; k<10; k++ )
        {
            result << setw(15) << menu[ k ] << " :" << setw(6) << student_index[ k ] << endl;
            total1+=student[ k ];
        }
        result << "Total:" << total1 << endl;
        result << "-----------------------" << endl;

        result << "Teacher" << endl;
        result << "=======================" << endl;
        for( int l=0; l<10; l++ )
        {
            result << setw(15) << menu[ l ] << " :" << setw(6) << teacher_index[ l ] << endl;
            total1+=teacher[ l ];
        }
        result << "Total:" << total1 << endl;
        result << "-----------------------" << endl;

        result << "Normal" << endl;
        result << "=======================" << endl;
        for( int m=0; m<10; m++ )
        {
            result << setw(15) << menu[ m ] << " :" << setw(6) << normal_index[ m ] << endl;
            total1+=normal[ m ];
        }
        result << "Total:" << total1 << endl;
        result << "-----------------------" << endl;

        int total=total1+total2+total3;
        result << "Total Money:" << total;
    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
