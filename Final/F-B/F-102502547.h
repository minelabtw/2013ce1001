#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
        total1=0;
        total2=0;
        total3=0;
        for(int i=0; i<9; i++)
        {
            student[i]=0;
        }
        for(int i=0; i<9; i++)
        {
            teacher[i]=0;
        }
        for(int i=0; i<9; i++)
        {
            normal[i]=0;
        }
    }
    void decode(char code[])
    {
        int b=(int)(code[1])-48;
        if(code[0]=='0')
        {
            for(int i=2; i<b*2+2; i=i+2)
            {
                int a=(int)(code[i+1])-65;
                int c=(int)(code[i])-48;
                teacher[a]=teacher[a]+c;
            }
        }

        if(code[0]=='1')
        {
            for(int i=2; i<b*2+2; i=i+2)
            {
                int a=(int)(code[i+1])-65;
                int c=(int)(code[i])-48;
                student[a]=student[a]+c;
            }
        }

        if(code[0]=='2')
        {
            for(int i=2; i<b*2+2; i=i+2)
            {
                int a=(int)(code[i+1])-65;
                int c=(int)(code[i])-48;
                normal[a]=normal[a]+c;
            }
        }

    }
    void InputData()
    {
        char a[20];
        ifstream in("fastfoodlist.txt", ios::in);

        while(!in.eof())
        {
            in >> a;
            decode(a);
        }
    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData()
    {

        ofstream out("102502547.txt",ios::out);
        out << "Student" << endl << "=======================" << endl;
        for(int i=0; i<9; i++)
        {
            out << setw(15) << menu[i] << " :" << setw(6) << student[i] << endl;
            total2=total2+student[i]*price[i];
        }
        out << "Total:" << total2*0.9 << endl << "-----------------------" << endl << "Teacher" << endl << "=======================" << endl;
        for(int i=0; i<9; i++)
        {
            out << setw(15) << menu[i] << " :" << setw(6) << teacher[i] << endl;
            total1=total1+student[i]*price[i];
        }
        out << "Total:" << total1*0.8 << endl << "-----------------------" << endl << "Normal" << endl << "=======================" << endl;
        for(int i=0; i<9; i++)
        {
            out << setw(15) << menu[i] << " :" << setw(6) << normal[i] << endl;
            total3=total3+student[i]*price[i];
        }
        out << "Total:" << total3 << endl << "-----------------------" << endl << "Total Money:" << total1+total2+total3;
    }

private:
    string menu[9]= {"Fried chicken","Hamburger","Chicken nuggets","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
    int total1,total2,total3;
    int price[9]= {50,60,40,30,20,40,30,20,30};
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
