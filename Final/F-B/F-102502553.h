#ifndef F-102502553_H_INCLUDED
#define F-102502553_H_INCLUDED
#include<fstream>
#include<iostream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
        char co[40]= {};
        int price[]= {50,60,40,30,20,40,30,20,30,0};
        int student_index[8]= {};
        int teacher_index[8]= {};
        int normal_index[8]= {};
        int student[8]= {};
        int teacher[8]= {};
        int normal[8]= {};
        string menu[]= {"Fried chicken","Hamburger","Chicken nuggests","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
    }

    void InputData()//傳入資料
    {
        ifstream file("fastfoodlistg.txt",ios::in);
        while(!file.eof())
        {
            file>>co;
            for(int f=0; f<40; f++)
            {
                if(co[f]='A')
                    co[f]-65;
                else if(co[f]='B')
                    co[f]-65;
                else if(co[f]='C')
                    co[f]-65;
                else if(co[f]='D')
                    co[f]-65;
                else if(co[f]='E')
                    co[f]-65;
                else if(co[f]='F')
                    co[f]-65;
                else if(co[f]='G')
                    co[f]-65;
                else if(co[f]='H')
                    co[f]-65;
                else if(co[f]='I')
                    co[f]-65;
                else if(co[f]='J')
                    co[f]-65;
            }
            for(int d=3; d<=co[3]*2+2; d+=2)
                sortData(co[0],co[d],co[d+1]);
        }
    }

    void sortData(int id,int number,int temp)//分類(錢和數量)並計算各類人的錢
    {
        switch(id)
        {
        case 0:
            total1=total1+number*price[temp]*0.8;
            teacher_index[temp]+=teacher_index[temp]+number;
            break;
        case 1:
            total2=total2+number*price[temp]*0.9;
            student_index[temp]+=student_index[temp]+number;
            break;
        case 2:
            total3=total3+number*price[temp];
            normal_index[temp]+=normal_index[temp]+number;
            break;
        }
    }

    void OutputData()//輸出資料
    {
        ofstream file("102502553.txt",ios::out);
        file<<"Student"<<endl<<"======================"<<endl;
        for(int e=0; e<9; e++)
        {
            file<<setw(16)<<menu[e]<<" :"<<setw(6)<<student_index[e]<<endl;
        }
        file<<"Total:"<<total1<<endl;
        file<<"----------------------"<<endl;
        file<<"Teacher"<<endl<<"======================"<<endl;
        for(int e=0; e<9; e++)
        {
            file<<setw(16)<<menu[e]<<" :"<<setw(6)<<teacher_index[e]<<endl;
        }
        file<<"Total:"<<total2<<endl;
        file<<"----------------------"<<endl;
        file<<"Normal"<<endl<<"======================"<<endl;
        for(int e=0; e<9; e++)
        {
            file<<setw(16)<<menu[e]<<" :"<<setw(6)<<normal_index[e]<<endl;
        }
        file<<"Total:"<<total3<<endl;
    }

private:
    string menu[8];
    int total1=0,total2=0,total3=0;
    int price[9];
    int student[8],teacher[8],normal[8];
    int student_index[8],teacher_index[9],normal_index[9];
    int id=0;
    char co[40];
    char codes[40]= {};
};




#endif // F-102502553_H_INCLUDED
