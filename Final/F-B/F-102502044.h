#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED

#include <stdio.h>
#include <string.h>

class POSsystem
{
    public:
		/* init all value */
        POSsystem()
        {
			/* fill zero to all member */
			memset(str, 0, sizeof(str));
			memset(menu, 0, sizeof(menu));
			memset(price, 0, sizeof(price));
			memset(total, 0, sizeof(total));
			memset(off, 0, sizeof(off));
			memset(name, 0, sizeof(name));
			memset(count, 0, sizeof(count));
			memset(index, 0, sizeof(index));
			money = 0;

			/* init the meny list */
			strcpy(menu[0], "  Fried chicken :");
			strcpy(menu[1], "      Hamburger :");
			strcpy(menu[2], "Chicken nuggets :");
			strcpy(menu[3], "           Cola :");
			strcpy(menu[4], "      Black tea :");
			strcpy(menu[5], "         Coffee :");
			strcpy(menu[6], "   French fries :");
			strcpy(menu[7], "           Tart :");
			strcpy(menu[8], "        Hot dog :");

			/* init all price on the menu */
			price[0] = 5;
			price[1] = 6;
			price[2] = 4;
			price[3] = 3;
			price[4] = 2;
			price[5] = 4;
			price[6] = 3;
			price[7] = 2;
			price[8] = 3;

			/* init all types of custom name */
			strcpy(name[0], "Student");
			strcpy(name[1], "Teacher");
			strcpy(name[2], "Normal");

			/* init all index */
			for(int i=0 ; i<3 ; i++)
				for(int j=0 ; j<9 ; j++)
					index[i][j] = j;
			
			/* init all price off */
			off[0] = 9;
			off[1] = 8;
			off[2] = 10;
		}

		/* decode one line */
		void decode(char *code)
        {
			/* type is the type of custom */
			/* student = 0 */
			/* teacher = 1 */
			/* normal  = 2 */
			int type = 0;

			/* num is the number of type the custom buy */
			int num = code[1] - '0';

			/* pro is the index of product */
			/* times is the number of product */
			int pro = 0;
			int times = 0; 
			
			/* init the type */
			if(code[0] == '0')
				type = 1;
			else if(code[0] == '2')
				type = 2;

			/* decoding..... complete :) */
			for(int i=2 ; code[i] != '\0' ; i++)
			{
				/* if code[i] is a number, then add into times */
				if('0' <= code[i] && code[i] <= '9')
					times = times*10 + code[i] - '0';

				/* if code[i] is a alpha, then add times into count */
				else if('A' <= code[i] && code[i] <= 'I')
				{
					pro = code[i] - 'A';
					count[type][pro] += times;
					/* init times */
					times = 0;
				}

				/* else, init times and product */
				else
					times = pro = 0;
			}
        }

		/* input all data */
        void InputData()
        {
			/* file point */
			FILE *fin = fopen("fastfoodlist.txt", "r+");
			
			/* input all date until EOF */
			while(fgets(str, 35, fin) != NULL)
				/* decode the input */
				decode(str);

			/* sort all count */
			sortData();

			/* close file */
			fclose(fin);
        }

		/* sort all count */
        void sortData()
        {
			/* sort 3 type of custom*/
			for(int i=0 ; i<3 ; i++)
				/* use bubble sort */
				for(int j=0 ; j<9 ; j++)
					for(int k=0 ; k<8 ; k++)
						/* if 2 item will swap */
						if(count[i][index[i][k]] > count[i][index[i][k+1]])
						{
							/* use three value method to swap them  */
							/* (TVM ? I don't know how to spell =P) */
							int tmp = index[i][k];
							index[i][k] = index[i][k+1];
							index[i][k+1] = tmp;
						}
        }

		/* output all data */
        void OutputData()
        {
			/* point of file */
			FILE *fout = fopen("102502044.txt", "w+");

			/* output all type of custom */
			for(int i=0 ; i<3 ; i++)
			{
				/* output the name */
				fprintf(fout, "%s\n", name[i]);
				/* ui message */
				fprintf(fout, "=======================\n");

				/* output all product */
				for(int j=0 ; j<9 ; j++)
				{
					/* output the count */
					fprintf(fout, "%s%5d\n", menu[index[i][j]], count[i][index[i][j]]);
					/* calc the total */
					total[i] += count[i][j] * price[j] * off[i]; 
				}
				/* output the total money */
				fprintf(fout, "Total:%d\n", total[i]);
				/* ui message */
				fprintf(fout, "-----------------------\n");
			}

			/* calc total money */
			money = total[0] + total[1] + total[2];

			/* output total money */
			fprintf(fout, "Total Money:%d\n", money);

			/* close the file */
			fclose(fout);
        }

    private:
		/* tmp string of input */
		char str[35];
        
		/* menu name */
		char menu[9][20];
		/* price on menu */
        int price[9];
        
		/* total of each type */
		int total[3];
		/* total money */
		int money;
		/* the price off */
		int off[3];
        
		/* name of each custom */
		char name[3][10];
		/* count of each product */
		int count[3][9];
		/* the index before sort */
		int index[3][9];
};

#endif // POS_SYSTEM_H_INCLUDED
