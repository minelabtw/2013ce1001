#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <fstream>
#include <iomanip>
#include <iostream>

using namespace std;

class POSsystem{
 public:
  POSsystem(){  //init
    menu[0] = "Fried chicken";
    menu[1] = "Hamuburger";
    menu[2] = "Chicken nuggets";
    menu[3] = "Cola";
    menu[4] = "Black tea";
    menu[5] = "Coffee";
    menu[6] = "French fries";
    menu[7] = "Tart";
    menu[8] = "Hot dog";
    price[0] = 50;
    price[1] = 60;
    price[2] = 40;
    price[3] = 30;
    price[4] = 20;
    price[5] = 40;
    price[6] = 30;
    price[7] = 20;
    price[8] = 30;
    for(int i = 0;i<9;i++){
      student[i] = 0;
    }
    for(int i = 0;i<9;i++){
      teacher[i] = 0;
    }
    for(int i = 0;i<9;i++){
      normal[i] = 0;
    }
    for(int i =0;i<9;i++){
      student_index[i] = i;
      teacher_index[i] = i;
      normal_index[i] = i;
    }
  }
  void decode(string _code){
    int temp[3];
    temp[0] = (_code[0] - 48);
    temp[1] = 0;
    for(int i = 2;i<_code.length();i++){
      if((_code[i] - 48) < 10 && (_code[i] - 48) > 0){ //fetch number
	temp[1] *= (temp[1] > 0?10:0);
	temp[1] += (_code[i] - 48);
      }else if((_code[i] + 0) < 74 && (_code[i] + 0) > 64){ //fetch food code
	temp[2] = (_code[i] - 65);
	insertData(temp[0],temp[1],temp[2]);
	temp[1] = 0;
      }else{
	temp[1] = 0;
      }
    }
  }
  void InputData(){
    string code;
    ifstream FILE("fastfoodlist.txt"); //read from file
    while(getline(FILE,code)){
      decode(code); //possess code
    }
    FILE.close(); //close file
  }
  void sortData(){ //sort
    int temp;
    for(int i = 8;i>0;i--){
      for(int j = 0;j<i;j++){
	if(student[student_index[j]] > student[student_index[j+j]]){
	  temp = student_index[j];
	  student_index[j] = student_index[j+1];
	  student_index[j+1] = temp;
	}
      }
    }
    for(int i = 8;i>0;i--){
      for(int j = 0;j<i;j++){
	if(teacher[teacher_index[j]] > teacher[teacher_index[j+1]]){
	  temp = teacher_index[j];
	  teacher_index[j] = teacher_index[j+1];
	  teacher_index[j+1] = temp;
	}
      }
    }
    for(int i = 8;i>0;i--){
      for(int j = 0;j<i;j++){
	if(normal[normal_index[j]] > normal[normal_index[j+1]]){
	  temp = normal_index[j];
	  normal_index[j] = normal_index[j+1];
	  normal_index[j+1] = temp;
	}
      }
    }
  }
  void OutputData(){
    sortData();
    ofstream FILE("1020502038.txt");  //open file
    int total;
    total = 0;
    //start write file
    FILE << "Student" << endl;
    FILE << "=======================" << endl;
    for(int i = 0;i<9;i++){
      FILE << setw(17) << menu[student_index[i]] + " :" << setw(5) << student[student_index[i]] << endl;
      total += (price[student_index[i]] * student[student_index[i]]);
    }
    FILE << "Total:" << (0.9 * total) << endl;
    FILE << "-----------------------" << endl;
    total = 0;
    FILE << "Teacher" << endl;
    FILE << "=======================" << endl;
    for(int i = 0;i<9;i++){
      FILE << setw(17) << menu[teacher_index[i]] + " :" << setw(5) << teacher[teacher_index[i]] << endl;
      total += (price[teacher_index[i]] * teacher[teacher_index[i]]);
    }
    FILE << "Total:" << (0.8 * total) << endl;
    FILE << "-----------------------" << endl;
    total = 0;
    FILE << "Normal" << endl;
    FILE << "=======================" << endl;
    for(int i = 0;i<9;i++){
      FILE << setw(17) << menu[normal_index[i]] + " :" << setw(5) << normal[normal_index[i]] << endl;
      total += (price[normal_index[i]] * normal[normal_index[i]]);
    }
    FILE << "Total:" << (0.9 * total) << endl;
    FILE << "-----------------------" << endl;
    FILE.close(); //close file
  }
  void insertData(int type,int num,int food){ //insert data into student[],teacher[],normal[]
    if(type == 0){
      teacher[food] += num;
    }else if(type == 1){
      student[food] += num;
    }else if(type == 2){
      normal[food] += num;
    }
  }
 private:
  string menu[9];
  int total1,total2,total3;
  int price[9];
  int student[9],teacher[9],normal[9];
  int student_index[9],teacher_index[9],normal_index[9];
  
};

#endif // POS_SYSTEM_H_INCLUDED
