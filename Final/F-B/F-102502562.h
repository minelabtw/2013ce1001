#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <fstream>
#include <iomanip>
#include <cstdlib>
using namespace std;

class POSsystem
{
    public:
        POSsystem()
        {

        }
        void decode(int a,int b,int c,char s1)
        {
            if(a==0)
            {
                for(int i=1;i<=b;i++)
                {
                    if((int)s1-65==0)
                    {
                        student[0]=50*c;
                    }
                    else if((int)s1-65==1)
                    {
                        student[1]=60*c;
                    }
                    else if((int)s1-65==2)
                    {
                        student[2]=40*c;
                    }
                    else if((int)s1-65==3)
                    {
                        student[3]=30*c;
                    }
                    else if((int)s1-65==4)
                    {
                        student[4]=20*c;
                    }
                    else if((int)s1-65==5)
                    {
                        student[5]=40*c;
                    }
                    else if((int)s1-65==6)
                    {
                        student[6]=30*c;
                    }
                    else if((int)s1-65==7)
                    {
                        student[7]=20*c;
                    }
                    else if((int)s1-65==8)
                    {
                        student[8]=30*c;
                    }
                }
                for(int j=0;j<9;j++)
                {
                    total1+=student[j];
                }
            }
            else if(a==1)
            {
                for(int i=1;i<=b;i++)
                {
                    if((int)s1-65==0)
                    {
                        teacher[0]=50*c;
                    }
                    else if((int)s1-65==1)
                    {
                        teacher[1]=60*c;
                    }
                    else if((int)s1-65==2)
                    {
                        teacher[2]=40*c;
                    }
                    else if((int)s1-65==3)
                    {
                        teacher[3]=30*c;
                    }
                    else if((int)s1-65==4)
                    {
                        teacher[4]=20*c;
                    }
                    else if((int)s1-65==5)
                    {
                        teacher[5]=40*c;
                    }
                    else if((int)s1-65==6)
                    {
                        teacher[6]=30*c;
                    }
                    else if((int)s1-65==7)
                    {
                        teacher[7]=20*c;
                    }
                    else if((int)s1-65==8)
                    {
                        teacher[8]=30*c;
                    }
                }
                for(int j=0;j<9;j++)
                {
                    total2+=teacher[j];
                }
            }
            else if(a==2)
            {
                for(int i=1;i<=b;i++)
                {
                    if((int)s1-65==0)
                    {
                        normal[0]=50*c;
                    }
                    else if((int)s1-65==1)
                    {
                        normal[1]=60*c;
                    }
                    else if((int)s1-65==2)
                    {
                        normal[2]=40*c;
                    }
                    else if((int)s1-65==3)
                    {
                        normal[3]=30*c;
                    }
                    else if((int)s1-65==4)
                    {
                        normal[4]=20*c;
                    }
                    else if((int)s1-65==5)
                    {
                        normal[5]=40*c;
                    }
                    else if((int)s1-65==6)
                    {
                        normal[6]=30*c;
                    }
                    else if((int)s1-65==7)
                    {
                        normal[7]=20*c;
                    }
                    else if((int)s1-65==8)
                    {
                        normal[8]=30*c;
                    }
                }
                for(int j=0;j<9;j++)
                {
                    total3+=normal[j];
                }
            }
        }
        void InputData()
        {
            int a,b,c,i=1;
            char s1;
            ifstream fin("fastfoodlist.txt",ios::in);
            while(!fin.eof())
            {
                fin >> a >> b;
                for(i;i<=b;i++)
                {
                    fin >> c >> s1;
                }
                POSsystem::decode(a,b,c,s1);
            }
        }

        void sortData(int number[],int temp[])
        {

        }

        void OutputData()
        {
            ofstream fout("102502562.txt",ios::out);
            fout << "Student" << endl << "======================" << endl;
            for(int i=0;i<9;i++)
            {
                fout << setw(15) << menu[i] << " : " << setw(5) << student[i] << endl;
            }
            fout << "Total:" << total1 << endl << "-----------------------" << endl;
            fout << "Teacher" << endl << "======================" << endl;
            for(int j=0;j<9;j++)
            {
                fout << setw(15) << menu[j] << " : " << setw(5) << teacher[j] << endl;
            }
            fout << "Total:" << total2 << endl << "-----------------------" << endl;
            fout << "Normal" << endl << "======================" << endl;
            for(int k=0;k<9;k++)
            {
                fout << setw(15) << menu[k] << " : " << setw(5) << normal[k] << endl;
            }
            fout << "Total:" << total3 << endl << "-----------------------" << endl;
            fout << "Total Money:" << total1+total2+total3;
        }

    private:
        string menu[9]={"Fried chicken","Hamburger","Chicken nuggets","Cola","Black tea","Coffee","French Fries","Tart","Hot Dog"};
        int total1,total2,total3;
        int price[9];
        int student[9],teacher[9],normal[9];
        int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
