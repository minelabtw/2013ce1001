#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
using namespace std;

class POSsystem
{
    public:
        POSsystem()//初始化
        {
            int i;
            for(i=0;i<9;i++)
            {
                student[i] = 0;
                teacher[i] = 0;
                normal[i] = 0;
                student_index[i] = i;
                teacher_index[i] = i;
                normal_index[i] = i;
            }
            menu[0] = "Fried chicken";
            menu[1] = "Hamburger";
            menu[2] = "Chicken nuggets";
            menu[3] = "Cola";
            menu[4] = "Black tea";
            menu[5] = "Coffee";
            menu[6] = "French fires";
            menu[7] = "Tart";
            menu[8] = "Hot dog";
            price[0] = 50;
            price[1] = 60;
            price[2] = 40;
            price[3] = 30;
            price[4] = 20;
            price[5] = 40;
            price[6] = 30;
            price[7] = 20;
            price[8] = 30;
            total1 = total2 = total3 = 0;
        }
        void decode(char code[])//分析字元陣列,將結果存入各陣列內
        {
            int i,num;
            num = 0;
                if(code[0] == '0')
                {
                    for(i=2;code[i]!='\0';i++)
                    {
                        if(code[i]>='A' && code[i]<='I')
                        {
                            teacher[code[i]-'A'] += num;
                            total1 += num*price[code[i]-'A']*8/10;
                            num = 0;
                        }
                        else if(code[i]>='J')
                        {
                            num = 0;
                        }
                        else
                        {
                            num = num*10 + code[i] - '0';
                        }
                    }
                }
                else if(code[0] == '1')
                {

                    for(i=2;code[i]!='\0';i++)
                    {
                        if(code[i]>='A' && code[i]<='I')
                        {
                            student[code[i]-'A'] += num;
                            total2 += num*price[code[i]-'A']*9/10;
                            num = 0;
                        }
                        else if(code[i]>='J')
                        {
                            num = 0;
                        }
                        else
                        {
                            num = num*10 + code[i] - '0';
                        }
                    }
                }
                else if(code[0] == '2')
                {
                    for(i=2;code[i]!='\0';i++)
                    {
                        if(code[i]>='A' && code[i]<='I')
                        {
                            normal[code[i]-'A'] += num;
                            total3 += num*price[code[i]-'A'];
                            num = 0;
                        }
                        else if(code[i]>='J')
                        {
                            num = 0;
                        }
                        else
                        {
                            num = num*10 + code[i] - '0';
                        }
                    }
                }
        }
        void InputData()//讀檔案並丟過去分析
        {
            ifstream fin("fastfoodlist.txt");
            string s;
            int i,length;
            char str[100];
            while(fin >> s)
            {
                length = s.size();
                for(i=0;i<length;i++)
                {
                    str[i] = s[i];
                }
                str[i] = '\0';
                decode(str);
            }
        }

        void sortData(int number[],int temp[])
        {
        }

        void OutputData()//輸出檔案
        {
            int i;
            ofstream fout("101201522.txt");
            fout << "Student" << endl;
            fout << "=======================" << endl;
            for(i=0;i<9;i++)
            {
                fout << setw(15) << menu[i] << " : " << setw(4) << student[i] << endl;
            }
            fout << "Total:" << total2 << endl;
            fout << "-----------------------" << endl;
            fout << "Teacher" << endl;
            fout << "=======================" << endl;
            for(i=0;i<9;i++)
            {
                fout << setw(15) << menu[i] << " : " << setw(4) << teacher[i] << endl;
            }
            fout << "Total:" << total1 << endl;
            fout << "-----------------------" << endl;
            fout << "Normal" << endl;
            fout << "=======================" << endl;
            for(i=0;i<9;i++)
            {
                fout << setw(15) << menu[i] << " : " << setw(4) << normal[i] << endl;
            }
            fout << "Total:" << total3 << endl;
            fout << "-----------------------" << endl;
            fout << "Total Money:" << total1+total2+total3;
            fout.close();
        }

    private:
        string menu[9];//菜單名稱
        int total1,total2,total3;//各職業總金額
        int price[9];//各菜色單價
        int student[9],teacher[9],normal[9];//各職業各菜色購買數量
        int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
