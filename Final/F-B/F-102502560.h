#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include <fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
	POSsystem()
	{
	}
	void decode(char code[])
	{
		int* ptr[]={student,teacher,normal};
		int person=code[0]-48;
		int datanum=code[1]-48;
		int nowidx=2;
		for(int i=0;i<datanum;i++){
			int num=code[nowidx++]-48;
			while(code[nowidx]>=48 && code[nowidx]<=57){(num*=10)+=code[nowidx++]-48;}
			int type=code[nowidx++]-65;
			if(type<0||type>8)continue;
			(ptr[person])[type]+=num;

			total[person]+=num*price[type]*rate[person]/10;
		}

	}
	void InputData()
	{
		ifstream IN("fastfoodlist.txt");
		char buf[20];
		while(IN >> buf)decode(buf);
	}

	void sortData(int number[],int index[])
	{
		int tmpnum[9];
		for(int i=0;i<9;i++){tmpnum[i]=number[i];}

		for(int i=8-1;i>=0;i--){
			for(int j=0;j<=i;j++){
				if(tmpnum[j]>tmpnum[j+1]){
					int tmpn,tmpi;
					tmpn=tmpnum[j];
					tmpnum[j]=tmpnum[j+1];
					tmpnum[j+1]=tmpn;

					tmpi=index[j];
					index[j]=index[j+1];
					index[j+1]=tmpi;
				}
			}
		}
	}

	void OutputData()
	{
		//sortData(student,student_index);
		//sortData(teacher,teacher_index);
		//sortData(normal,normal_index);
		//failed

		ofstream OUT("102502560.txt");
		int* ptr[]={student,teacher,normal};
		int personorder[3]={1,0,2};
		string none="";
		for(int o=0;o<3;o++){
			OUT << personname[personorder[o]] << endl;
			OUT << fixed << setw(23) << setfill('=') << none << setfill(' ') << endl;
			for(int i=0;i<9;i++){
				OUT << setw(15) << menu[student_index[i]] << " : " << setw(4) << (ptr[personorder[o]])[student_index[i]] << endl;
			}
			OUT << "Total:" << total[personorder[o]] << endl;
			OUT << fixed << setw(23) << setfill('-') << none << setfill(' ') << endl;
		}
		OUT << "Total Money:" << total[0]+total[1]+total[2] << endl;
	}

private:
	string menu[9]={"Fried chicken","Hamburger","Chicken nuggets","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
	int total[3]={};
	int price[9]={50,60,40,30,20,40,30,20,30};
	int student[9]={},teacher[9]={},normal[9]={};
	int student_index[9]={0,1,2,3,4,5,6,7,8},teacher_index[9]={0,1,2,3,4,5,6,7,8},normal_index[9]={0,1,2,3,4,5,6,7,8};
	string personname[3]={"Teacher","Student","Normal"};
	int rate[3]={8,9,10};
};

#endif // POS_SYSTEM_H_INCLUDED
