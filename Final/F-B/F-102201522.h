#ifndef F-102201522_H_INCLUDED
#define F-102201522_H_INCLUDED
#include<fstream>
#include <iomanip>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<string>
using namespace std;

class POSsystem
{
public:
    POSsystem() //建構式函數
    {
        for(int i=0; i<9; i++)
        {
            price[i]=0;
            student[i]=0,teacher[i]=0,normal[i]=0;
        }
        total1=0;
        total2=0;
        total3=0;
        menu[0]="Fried chicken :";
        menu[1]="Hamburger :";
        menu[2]="Chicken nuggets :";
        menu[3]="Cola :";
        menu[4]="Black tea :";
        menu[5]="Coffee :";
        menu[6]="French fries :";
        menu[7]="Tart :";
        menu[8]="Hot dog :";
    }
    void decode(char code[]) //分析讀進來的字元陣列，存入點餐內容
    {
        int temp[9];
        for (int i =0; i<9; i++)
            temp[i]=0;
        int type =0;
        for(int i=1; i<=code[1]; i++)
        {
            if(code[2*i+1]<9)
            {
                type= (int)code[2*i+2]-65;
                temp[type]+=code[2*i+1];
                cout <<type<<" : "<< temp[type] <<endl;
            }
            else
            {

                type= (int)code[2*i+3]-65 ;
                temp[type]+=10*code[2*i+1]+code[2*i+2];
                cout <<type<<" : "<< temp[type] <<endl;
            }
        }
        for(int j=0; j<9; j++)
            switch(code[0])
            {
            case '0':
                teacher[j]+=temp[j];
                break;
            case '1':
                student[j]+=temp[j];
                break;
            case '2':
                normal[j]+=temp[j];
                break;
            }

    }

    void InputData() //讀入檔案
    {
        ifstream filein ("fastfoodlist.txt",ios::in);
        while(!filein.eof())
        {
            char line[99];
            filein >> line;
            decode(line);
        }
    }

    void sortData(int number[],int temp[])
    {
        ;
    }

    void OutputData()
    {
        ofstream fileout ("102201522.txt",ios::out);
        fileout << "Student" << endl << "====================" << endl;
        for(int i=0; i<9; i++)
            fileout << setw(17) << menu[i] << setw(5) << student [i] << endl;
        fileout << "Total:" << total1 << endl << "--------------------" << endl
                << "Teacher" << endl << "====================" << endl;
        for(int i=0; i<9; i++)
            fileout << setw(17) << menu[i] << setw(5) << teacher [i] << endl;
        fileout << "Total:" << total2 << endl << "--------------------" << endl
                << "Normal" << endl << "====================" << endl;
        for(int i=0; i<9; i++)
            fileout << setw(17) << menu[i] << setw(5) << normal [i] << endl;
        fileout << "Total:" << total3 << endl << "--------------------" << endl
                << "Total Money:" << total1 + total2 + total3;

    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
