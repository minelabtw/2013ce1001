#ifndef F-102502036_H_INCLUDED
#define F-102502036_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
   public:
       POSsystem()
       {
       }
       void decode(char code[])
       {
           if ( (int)code[0] == 0 )
           {
               for ( int i = 0 ; i <= 50 ; i++ )
               {
                   if ( (int)code[i] > 64 && (int)code[i] < 74 )
                   {
                       teacher[(int)code[i] - 65] ++;
                   }
               }
           }

           else if ( (int)code[0] == 1 )
           {
               for ( int i = 0 ; i <= 50 ; i++ )
               {
                   if ( (int)code[i] > 64 && (int)code[i] < 74 )
                   {
                       int j = (int)code[i] - 65 ;
                       student[(int)code[i] - 65] ++;
                   }
               }
           }

           else if ( (int)code[0] == 2 )
           {
               for ( int i = 0 ; i <= 50 ; i++ )
               {
                   if ( (int)code[i] > 10 )
                   {
                       int j = (int)code[i] - 65 ;
                       normal[(int)code[i] - 65] ++;
                   }
               }
           }
       }
       void InputData()
       {
           char code[100];
           ifstream infile ( "fastfoodlist.txt" );
           while ( infile )
           {
               infile >> code;
               POSsystem::decode ( code );
           }
       }

       void sortData(int number[],int temp[])
       {

       }

       void OutputData()
       {
           ofstream outfile ( "102502036.txt" );

           for ( int i = 0 ; i < 9 ; i++ )
           {
               total1 += student[i] * price[i] * 0.9 ;
               total2 += teacher[i] * price[i] * 0.8 ;
               total3 += normal[i] * price[i] ;
           }

           outfile << "Student" << endl;
           for ( int i = 0 ; i < 23 ; i++ )
               outfile << "=" ;
           outfile << endl;
           for ( int i = 0 ; i < 9 ; i++ )
               outfile << setw(15) << menu[i] << " : " << setw(4) << student[i] << endl;
           outfile << "Total:" << total1 << endl;
           for ( int i = 0 ; i < 23 ; i++ )
               outfile << "-" ;

           outfile << endl << "Teacher" << endl;
           for ( int i = 0 ; i < 23 ; i++ )
               outfile << "=" ;
           outfile << endl;
           for ( int i = 0 ; i < 9 ; i++ )
               outfile << setw(15) << menu[i] << " : " << setw(4) << teacher[i] << endl;
           outfile << "Total:" << total2 << endl;
           for ( int i = 0 ; i < 23 ; i++ )
               outfile << "-" ;

           outfile << endl << "Normal" << endl;
           for ( int i = 0 ; i < 23 ; i++ )
               outfile << "=" ;
           outfile << endl;
           for ( int i = 0 ; i < 9 ; i++ )
               outfile << setw(15) << menu[i] << " : " << setw(4) << normal[i] << endl;
           outfile << "Total:" << total3;
       }

   private:
       string menu[9] = { "Fried chicken" , "Hamburger" , "Chicken nuggets" , "Cola" , "Black tea" , "Coffee" , "French fries" , "Tart" , "Hot dog" };
       int total1,total2,total3;
       int price[9] = { 50 , 60 , 40 , 30 , 20 , 40 , 30 , 20 , 30 };
       int student[9]={0} , teacher[9]={0} , normal[9]={0};
       int student_index[9],teacher_index[9],normal_index[9];

};

#endif // F-102502036_H_INCLUDED
