#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
#include <cstring>
#include <string>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
        //初始化!!
        menu[0]="Fried chicken";
        menu[1]="Hamburger";
        menu[2]="Chicken nuggets";
        menu[3]="Cola";
        menu[4]="Black tea";
        menu[5]="Coffee";
        menu[6]="French fries";
        menu[7]="Tart";
        menu[8]="Hot dog";
        price[0]=50;
        price[1]=60;
        price[2]=40;
        price[3]=30;
        price[4]=20;
        price[5]=40;
        price[6]=30;
        price[7]=20;
        price[8]=30;
        total1=total2=total3=0;
        memset(student,0,sizeof(student));
        memset(teacher,0,sizeof(teacher));
        memset(normal,0,sizeof(normal));
        for(int i=0;i<9;++i)    //索引值初始化
            student_index[i]=teacher_index[i]=normal_index[i]=i;
    }
    void decode(char code[])
    {
        int type=(code[0]-'0');
        //int num=(s[1]-'0');
        int sum=0;
        for(int i=2; code[i]!='\0'; ++i)
        {
            if(code[i]>='0'&&code[i]<='9')
            {
                sum=sum*10+(code[i]-'0');
            }
            else
            {
                int id=(code[i]-'A');

                if(id>8)
                {

                }
                else if(type==0)
                {
                    teacher[id]+=sum;
                    total2+=sum*price[id];
                }
                else if(type==1)
                {
                    student[id]+=sum;
                    total1+=sum*price[id];
                }
                else
                {
                    normal[id]+=sum;
                    total3+=sum*price[id];
                }
                sum=0;
            }
        }
    }
    void InputData()
    {
        //讀檔
        ifstream fin("fastfoodlist.txt");
        char s[500];
        while(fin>>s)
        {
            decode(s);
        }
        sortData(student,student_index);
        sortData(teacher,teacher_index);
        sortData(normal,normal_index);
    }

    void sortData(int number[],int temp[])
    {
        //排序
        for(int i=0;i<9;++i)
        {
            for(int j=i+1;j<9;++j)
            {
                if(number[i]>number[j])
                {
                    int tmp=number[i];
                    number[i]=number[j];
                    number[j]=tmp;
                    int tmp2=temp[i];
                    temp[i]=temp[j];
                    temp[j]=tmp2;
                }
            }
        }
    }

    void OutputData()
    {
        ofstream fout("102502042.txt");
        int t; //位數
        int tmp;
        //student的情形
        fout<<"Student"<<endl;
        fout<<"======================="<<endl;
        for(int i=0;i<9;++i)
        {
            for(int j=1;j<=15-menu[student_index[i]].size();++j)
                fout<<" ";
            fout<<menu[student_index[i]]<<" :";
            tmp=student[i];
            t=0;
            while(tmp>0)
            {
                tmp/=10;
                t++;
            }
            for(int j=1;j<=5-t;++j)
                fout<<" ";
            fout<<student[i]<<endl;
        }
        fout<<"Total:"<<total1/10*9<<endl;
        fout<<"-----------------------"<<endl;
        //Teacher的情形
        fout<<"Teacher"<<endl;
        fout<<"======================="<<endl;
        for(int i=0;i<9;++i)
        {
            for(int j=1;j<=15-menu[teacher_index[i]].size();++j)
                fout<<" ";
            fout<<menu[teacher_index[i]]<<" :";
            tmp=teacher[i];
            t=0;
            while(tmp>0)
            {
                tmp/=10;
                t++;
            }
            for(int j=1;j<=5-t;++j)
                fout<<" ";
            fout<<teacher[i]<<endl;
        }
        fout<<"Total:"<<total2/10*8<<endl;
        fout<<"-----------------------"<<endl;
        //Normal的情形
        fout<<"Normal"<<endl;
        fout<<"======================="<<endl;
        for(int i=0;i<9;++i)
        {
            for(int j=1;j<=15-menu[normal_index[i]].size();++j)
                fout<<" ";
            fout<<menu[normal_index[i]]<<" :";
            tmp=normal[i];
            t=0;
            while(tmp>0)
            {
                tmp/=10;
                t++;
            }
            for(int j=1;j<=5-t;++j)
                fout<<" ";
            fout<<normal[i]<<endl;
        }
        fout<<"Total:"<<total3<<endl;
        fout<<"-----------------------"<<endl;
        fout<<"Total Money:"<<total1/10*9+total2/10*8+total3<<endl;
    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
