#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;
class POSsystem
{
    public:
        POSsystem()
        {
            total1=0;
            total2=0;
            total3=0;
        }
        void decode(char code[])
        {

        }
        void InputData()
        {
            char context[50]={}; //儲存一行內容
            char mode[1]={}; //身分
            char number[3]={}; //種類數量
            int type; //餐點
            int typenum; //餐點數量
            int typere[9]={}; //各產品數量
            ifstream file ("fastfoodlist.txt",ios::in);
            while(!file.eof())
            {
                file >> context;
                mode[0]=context[0];

                int j=1; //位置
                for (; j<10; j++)
                {
                    if('context[j]'>64)
                        break;
                    number[j]=context[j];
                }


                int i=j;
                while(context[i]!='\0')
                {
                    if('context[i]'>=65) //判斷數量或餐點
                    {
                        type=('context[i]'-65);
                    }

                    else
                    {
                        typenum=('context[i]'-'0');
                        for(int k=0; k<typenum; k++)
                        {
                            typere[type]++;
                        }
                    }
                    i++;
                }

                switch('mode') //轉移內容
                {
                case '0': //student
                    for(int k=0; k<9; k++)
                    {
                        student[k]=typere[k];
                    }
                    break;
                case '1':
                    for(int k=0; k<9; k++)
                    {
                        teacher[k]=typere[k];
                    }
                    break;
                case '2':
                    for(int k=0; k<9; k++)
                    {
                        normal[k]=typere[k];
                    }
                    break;
                }
            }

            for (int k=0; k<9; k++) //計算總量
            {
                total1 += student[k]*price[k];
            }

            for (int k=0; k<9; k++)
            {
                total2 += teacher[k]*price[k];
            }

            for (int k=0; k<9; k++)
            {
                total3 += normal[k]*price[k];
            }

            file.close(); //關閉檔案
        }

        void sortData(int number[],int temp[])
        {

        }

        void OutputData() //輸出資料到TXT
        {
            ofstream file ("102502049.txt",ios::out);
            file << "Student" << endl;
            file << "========================================" << endl;
            for(int i=0; i<9; i++)
            {
                file << setw(15) << menu[i] << " :" << setw(6) << student[i]*price[i] << endl;
            }
            file << "Total:" << total1 << endl;
            file << "----------------------------------------" << endl;  //學生

            file << "Teacher" << endl;
            file << "========================================" << endl;
            for(int i=0; i<9; i++)
            {
                file << setw(15) << menu[i] << " :" << setw(6) << teacher[i]*price[i] << endl;
            }
            file << "Total:" << total2 << endl;
            file << "----------------------------------------" << endl;  //老師

            file << "Normal" << endl;
            file << "========================================" << endl;
            for(int i=0; i<9; i++)
            {
                file << setw(15) << menu[i] << " :" << setw(6) << normal[i]*price[i] << endl;
            }
            file << "Total:" << total3 << endl;
            file << "----------------------------------------" << endl;   //一般

            file << "Total Money:" << (total1+total2+total3) ;
            file.close();
        }

    private:
        string menu[9]={"Fried chichen","Hamberger","Chicken nuggets","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
        int total1,total2,total3;
        int price[9]={50,60,40,30,20,40,30,20,30};
        int student[9],teacher[9],normal[9];
        int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
