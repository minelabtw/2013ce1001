#ifndef F-102502535_H_INCLUDED
#define F-102502535_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {
    menu [ 0 ] = "Fried chicken" ;
    menu [ 1 ] = "Hamburger" ;
    menu [ 2 ] = "Chicken nuggets" ;
    menu [ 3 ] = "Cola" ;
    menu [ 4 ] = "Black tea" ;
    menu [ 5 ] = "Coffee" ;
    menu [ 6 ] = "French fries" ;
    menu [ 7 ] = "Tart" ;
    menu [ 8 ] = "Hot dog" ;

    price [ 0 ] = 50 ;
    price [ 1 ] = 60 ;
    price [ 2 ] = 40 ;
    price [ 3 ] = 30 ;
    price [ 4 ] = 20 ;
    price [ 5 ] = 40 ;
    price [ 6 ] = 30 ;
    price [ 7 ] = 20 ;
    price [ 8 ] = 30 ;

    total1 = ( student[0]*price[0] + student[1]*price[1] + student[2]*price[2] + student[3]*price[3] + student[4]*price[4] + student[5]*price[5] + student[6]*price[6] + student[7]*price[7] + student[8]*price[8] ) * 0.9 ;
    total2 = ( teacher[0]*price[0] + teacher[1]*price[1] + teacher[2]*price[2] + teacher[3]*price[3] + teacher[4]*price[4] + teacher[5]*price[5] + teacher[6]*price[6] + teacher[7]*price[7] + teacher[8]*price[8] ) * 0.8 ;
    total3 = normal[0]*price[0] + normal[1]*price[1] + normal[2]*price[2] + normal[3]*price[3] + normal[4]*price[4] + normal[5]*price[5] + normal[6]*price[6] + normal[7]*price[7] + normal[8]*price[8] ;

    }
    void decode(char code[])
    {

    }
    void InputData()
    {
        ifstream filein ( "fastfoodlist.txt" , ios::in ) ;
    }

    void sortData(int number[],int temp[])
    {

    }

    void OutputData()
    {
        ofstream outClientFile ( "102502535.txt" , ios::out ) ;

        outClientFile << "Student" << endl << "======================" << endl ;
        for ( int i = 0 ; i <= 8 ; i ++ )
        {
            outClientFile << setw ( 15 ) << menu [ i ] << " : " << setw ( 4 ) << student [ i ] << endl ;
        }
        outClientFile << "Total:" << total1 << endl << "----------------------" << endl ;

        outClientFile << "Teacher" << endl << "======================" << endl ;
        for ( int i = 0 ; i <= 8 ; i ++ )
        {
            outClientFile << setw ( 15 ) << menu [ i ] << " : " << setw ( 4 ) << teacher [ i ] << endl ;
        }
        outClientFile << "Total:" << total2 << endl << "----------------------" << endl ;

        outClientFile << "Normal" << endl << "======================" << endl ;
        for ( int i = 0 ; i <= 8 ; i ++ )
        {
            outClientFile << setw ( 15 ) << menu [ i ] << " : " << setw ( 4 ) << normal [ i ] << endl ;
        }
        outClientFile << "Total:" << total3 << endl ;
    }

private:
    string menu[9];
    int total1,total2,total3;
    int price[9];
    int student[9],teacher[9],normal[9];
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // POS_SYSTEM_H_INCLUDED
