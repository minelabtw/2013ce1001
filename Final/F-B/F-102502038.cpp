//============================================================================
// Name        : F-102502038.cpp                                   
// Author      : catLee                                              
// Version     : 0.1                                                 
// Description : NCU CE1001 Final                                      
//============================================================================

#include <iostream>
#include "F-102502038.h"

using namespace std;

int main()
{
    POSsystem system;
    system.InputData();
    system.OutputData();
    //char A = 'A';
    //cout << (A + 0);
    return 0;
}
