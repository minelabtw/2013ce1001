#ifndef F-102502028_H_INCLUDED
#define F-102502028_H_INCLUDED
#include<fstream>
#include <iomanip>
#include <cstdlib>
using namespace std;

class POSsystem
{
public:
    POSsystem()
    {

    }
    void decode(char code[])
    {
        int id = 0 ;
        id = (int)code[0] ;
        int kind = code[1] ;
        int amout = 0 ;
        char food ;

        while (id == 0)
        {

            for (int a = 0 ; a < kind ; a++)
            {
                amout = code[(a+1)*2] ;
                for (int b = 0 ; b < amout ; b++)
                {
                    teacher[(int)code[(a+1)*2+1]-65]++ ;
                }
            }
        }

        while (id == 1)
        {
            for (int a = 0 ; a < kind ; a++)
            {
                amout = code[(a+1)*2] ;
                for (int b = 0 ; b < amout ; b++)
                {
                    student[(int)code[(a+1)*2+1]-65]++ ;
                }
            }
        }

        while (id == 2)
        {
            for (int a = 0 ; a < kind ; a++)
            {
                amout = code[(a+1)*2] ;
                for (int b = 0 ; b < amout ; b++)
                {
                    normal[(int)code[(a+1)*2+1]-65]++ ;
                }
            }
        }

    }
    void InputData()
    {
        ifstream infile("fastfoodlist.txt",ios::in) ;
        char input[20] = {} ;

        while (!infile)
        {
            cout << "Error!" ;
            exit(1) ;
        }
        while (!infile.eof())
        {

                infile >> input ;
                //cout << input[0] << endl ;
                decode(input) ;

        }
    }

    void sortData(int number[],int temp[])
    {
    }

    void OutputData()
    {
        ofstream outfile("102502028.txt",ios::out) ;

        outfile << "Student" << endl << "========================" << endl ;
        for (int a = 0 ; a < 9 ; a++)
        {
            outfile << setw(15) << menu[a] << " : " << setw(4) << student[a] << endl ;
        }

        for (int a = 0 ; a < 9 ; a++)
        {
            total1 += student[a]*price[a] ;
        }
        outfile << "Total:" << total1 << endl << "------------------------" << endl ;


        outfile << "Teacher" << endl << "========================" << endl ;
        for (int a = 0 ; a < 9 ; a++)
        {
            outfile << setw(15) << menu[a] << " : " << setw(4) << teacher[a] << endl ;
        }

        for (int a = 0 ; a < 9 ; a++)
        {
            total2 += teacher[a]*price[a] ;
        }
        outfile << "Total:" << total2 << endl << "------------------------" << endl ;

        outfile << "Normal" << endl << "========================" << endl ;
        for (int a = 0 ; a < 9 ; a++)
        {
            outfile << setw(15) << menu[a] << " : " << setw(4) << normal[a] << endl ;
        }

        for (int a = 0 ; a < 9 ; a++)
        {
            total3 += normal[a]*price[a] ;
        }
        outfile << "Total:" << total3 << endl << "------------------------" << endl ;

        outfile << "Total Money:" << total1+total2+total3 ;
    }

private:
    string menu[9] = {"Fried chicken","Hamburger","Chicken nuggets","Cola","Black tea","Coffee","French fries","Tart","Hot dog"};
    int total1 = 0 ;
    int total2 = 0 ;
    int total3 = 0 ;
    int price[9] = {50,60,40,30,20,40,30,20,30};
    int student[9] = {} ;
    int teacher[9] = {} ;
    int normal[9] = {} ;
    int student_index[9],teacher_index[9],normal_index[9];

};

#endif // F-102502028_H_INCLUDED
