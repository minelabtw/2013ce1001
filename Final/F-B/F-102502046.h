#ifndef POS_SYSTEM_H_INCLUDED
#define POS_SYSTEM_H_INCLUDED
#include<fstream>
#include <iomanip>
using namespace std;

class POSsystem
{
    private:
        string menu[9];
        int total1,total2,total3;
        int price[9];
        int student[9],teacher[9],normal[9];
        int student_index[9],teacher_index[9],normal_index[9];
    public:
        POSsystem()                     //建構子
        {
            menu[0]="Fried chicken";
            menu[1]="Hamburger";
            menu[2]="Chicken nuggets";
            menu[3]="Cola";
            menu[4]="Black tea";
            menu[5]="Coffee";
            menu[6]="French fries";
            menu[7]="Tart";
            menu[8]="Hot dog";
            total1=0,total2=0,total3=0;
            for(int i=0 ; i<9 ; i++)
            {
                student[i]=0;
                teacher[i]=0;
                normal[i]=0;
            }
            price[0]=50;
            price[1]=60;
            price[2]=40;
            price[3]=30;
            price[4]=20;
            price[5]=40;
            price[6]=30;
            price[7]=20;
            price[8]=30;
        }
        void decode(char code[])    //解碼
        {
            char x[4]={};
            int k=0;
            if(code[0]=='0')        //是老師的話
            {
                for(int i=2 ; code[i]!='\0' ; i++)
                {
                    x[k]=code[i];
                    k++;
                    if((int)code[i]<=(int)'J'&&(int)code[i]>=(int)'A')
                    {
                        if(code[i]=='J')
                            k=0;
                        else if(k==2)
                        {
                            int n=(int)x[k-2]-'0';
                            teacher[(int)x[k-1]-(int)'A']+=n;
                        }
                        else
                        {
                            int n=(int)(x[k-3]-'0')*10+(int)(x[k-2]-'0');
                            teacher[(int)x[k-1]-(int)'A']+=n;
                        }
                        k=0;
                    }
                }
            }
            else if(code[0]=='1')       //是學生的話
            {
                for(int i=2 ; code[i]!='\0' ; i++)
                {
                    x[k]=code[i];
                    k++;
                    if((int)code[i]<=(int)'J'&&(int)code[i]>=(int)'A')
                    {
                        if(code[i]=='J')
                            k=0;
                        else if(k==2)
                        {
                            int n=(int)x[k-2]-'0';
                            student[(int)x[k-1]-(int)'A']+=n;
                        }
                        else
                        {
                            int n=(int)(x[k-3]-'0')*10+(int)(x[k-2]-'0');
                            student[(int)x[k-1]-(int)'A']+=n;
                        }
                        k=0;
                    }
                }
            }
            else                        //普通人
            {
                for(int i=2 ; code[i]!='\0' ; i++)
                {
                    x[k]=code[i];
                    k++;
                    if((int)code[i]<=(int)'J'&&(int)code[i]>=(int)'A')
                    {
                        if(code[i]=='J')
                            k=0;
                        else if(k==2)
                        {
                            int n=(int)x[k-2]-'0';
                            normal[(int)x[k-1]-(int)'A']+=n;
                        }
                        else
                        {
                            int n=(int)(x[k-3]-'0')*10+(int)(x[k-2]-'0');
                            normal[(int)x[k-1]-(int)'A']+=n;
                        }
                        k=0;
                    }
                }
            }
        }
        void InputData()        //毒入資料
        {
            char a[50]={};
            string filename="fastfoodlist.txt";
            fstream fin( filename.c_str(),ios::in );
            while(!fin.eof())
            {
                fin >> a;
                decode(a);
            }
        }
        void sortData(int number[],int temp[])
        {

        }
        void OutputData()
        {
            for(int i=0 ; i<9 ; i++)
            {
                total1+=(student[i])*price[i]*0.9;
                total2+=(teacher[i])*price[i]*0.8;
                total3+=(normal[i])*price[i];
            }
            string filename="10250246.txt";
            fstream fout( filename.c_str() , ios::out );
            fout << "Student" << endl;
            fout << "========================" << endl;
            for(int i=0 ; i<9 ; i++)
                fout << setw(15) << menu[i] << " :  " <<  student[i] << endl;
            fout << "Total:" << total1 << endl;
            fout << "------------------------" << endl;

            fout << "Teacher" << endl;
            fout << "========================" << endl;
            for(int i=0 ; i<9 ; i++)
                fout << setw(15) << menu[i] << " :  " <<  teacher[i] << endl;
            fout << "Total:" << total2 << endl;

            fout << "------------------------" << endl;
            fout << "Normal" << endl;
            fout << "========================" << endl;
            for(int i=0 ; i<9 ; i++)
                fout << setw(15) << menu[i] << " :  " <<  normal[i] << endl;
            fout << "Total:" << total3 << endl;
            fout << "------------------------" << endl;
            fout << "Total Money:" << total1+total2+total3;
        }
};
#endif // POS_SYSTEM_H_INCLUDED
