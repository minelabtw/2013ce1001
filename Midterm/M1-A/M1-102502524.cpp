#include <iostream>
using namespace std;

void MB(double&,double,int,double&);                            //宣告之後會用到的函式
void CPU(double&,double,int,double&);
void Buy(double,double&,double&);

int main()
{
    double budget = 0;                                          //設定變數
    double cost = 0;
    double check = 0;
    int card = 0;
    int choose = 0;
    int a = 0;

    cout << "Budget?: ";                                        //輸入並檢查預算是否大於0
    while(cin >> budget)
    {
        if (budget<=0)
        {
            cout << "Out of range!" << endl;
            cout << "Budget?: ";
        }
        else
            break;
    }

    cout << "Card?: ";                                          //輸入並判斷是否擁有會員卡
    while(cin >> card)
    {
        if (card==0 || card==1)
            break;
        else
        {
            cout << "Out of range!" << endl;
            cout << "Card?: ";
        }
    }

    cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
    while (cin >> choose)                                       //選擇所要購買的品項
    {
        if (choose==1 || choose==2 || choose==-1)
        {
            switch (choose)
            {
            case 1:                                             //跳至購買MB函式
                MB(budget,check,card,cost);
                cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
                break;
            case 2:                                             //跳至購買CPU函式
                CPU(budget,check,card,cost);
                cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
                break;
            case -1:                                            //選擇結帳，顯示總花費及剩餘金額
                cout << "Total cost $" << cost << endl;
                cout << "Total Remain $" << budget << endl;
                break;
            }
            if (choose==-1)                                     //如果選擇結帳，則跳離
                break;
        }
        else                                                    //如果輸入超出範圍，則重新輸入
        {
            cout << "Out of range!" << endl;
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        }
    }

    return 0;
}

void MB(double &budget,double check,int card,double &cost)      //購買MB函式
{
    int a = 0;
    cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    while(cin >> a)                                             //選擇所需品項
    {
        if (a==1 || a==2 || a==3)
            break;
        else
        {
            cout << "Out of range!" << endl;
            cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        }
    }
    switch (a)                                                  //計算花費
    {
    case 1:
        if(card==1)
            check=5000*0.85;
        else
            check=5000;
        break;
    case 2:
        if(card==1)
            check=4500*0.9;
        else
            check=4500;
        break;
    case 3:
        check=5000;
        break;
    }
    Buy(check,budget,cost);                                     //跳至檢查花費函式
}

void CPU(double &budget,double check,int card,double &cost)     //購買CPU函式
{
    int a = 0;
    cout << "1)AMD, 2)Intel?: ";
    while(cin >> a)                                             //選擇所需品項
    {
        if (a==1 || a==2)
            break;
        else
        {
            cout << "Out of range!" << endl;
            cout << "1)AMD, 2)Intel?: ";
        }
    }
    switch (a)                                                  //計算花費
    {
    case 1:
        if(card==1)
            check=5200*0.75;
        else
            check=5200;
        break;
    case 2:
        if(card==1)
            check=4400*0.8;
        else
            check=4400;
        break;
    }
    Buy(check,budget,cost);                                     //跳至檢查花費函式
}

void Buy(double check,double &budget,double &cost)              //檢查花費函式
{
    double test = 0;
    test=budget-check;
    if (test>=0)                                                //如果花費沒有超過預算，則計入統計值
    {                                                           //若超過統計值，顯示金額不足且取消上一筆購物
        budget = test;
        cost = cost+check;
        cout << "Remain $" << budget << endl;
    }
    else
    {
        cout << "Money not enough!" << endl;
        cout << "Remain $" << budget << endl;
    }
}
