#include <iostream>
using namespace std;

void Buy(double &,double cost);
void MB(double &,int card);
void CPU(double &,int card);

int main()
{
    double budget,originbudget; //預算,原預算
    int pay; //買甚麼
    int card;  //會員卡
    cout<<"Budget?: ";
    cin>>budget;
    while(budget<=0)  //判斷預算是否在範圍之內
    {
        cout<<"Out of range!"<<endl;
        cout<<"Budget?: ";
        cin>>budget;
    }
    originbudget=budget;  //存原來預算
    cout<<"Card?: ";
    cin>>card;
    while(card!=0 && card!=1) //判斷會員卡輸入是否在範圍之內
    {
        cout<<"Out of range!"<<endl;
        cout<<"Card?: ";
        cin>>card;
    }
    cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
    while(cin>>pay)
    {
        while(pay!=1 && pay!=2 && pay!=-1)  //判斷模式是否在範圍之內
        {
            cout<<"Out of range!"<<endl;
            cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin>>pay;
        }
        if(pay==1)
            MB(budget,card);
        else if(pay==2)
            CPU(budget,card);
        else
        {
            cout<<"Total cost $"<<originbudget-budget<<endl;  //輸出花費
            cout<<"Total Remain $"<<budget<<endl;
            break;
        }
        cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
    }
    return 0;
}

void Buy(double &budget,double cost)  //計算預算
{
    if(cost>budget)  //判斷想買價格是否超出預算
    {
        cout<<"Money not enough!"<<endl;
        cout<<"Remain $"<<budget<<endl;
    }
    else
    {
        budget-=cost;
        cout<<"Remain $"<<budget<<endl;
    }
}

void MB(double &budget,int card)  //想買主機板之類型
{
    int mode;
    cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin>>mode;
    while(mode!=1 && mode!=2 && mode!=3) //判斷模式是否在範圍之內
    {
        cout<<"Out of range!"<<endl;
        cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin>>mode;
    }
    if(card==1)  //判斷是否有會員卡
    {
        if(mode==1)
            Buy(budget,4250);
        else if(mode==2)
            Buy(budget,4050);
        else
            Buy(budget,5000);
    }
    else
    {
        if(mode==1)
            Buy(budget,5000);
        else if(mode==2)
            Buy(budget,4500);
        else
            Buy(budget,5000);
    }
}

void CPU(double &budget,int card)  //想買處理器之類型
{
    int mode;
    cout<<"1)AMD, 2)Intel?: ";
    cin>>mode;
    while(mode!=1 && mode!=2)  //判斷模式是否在範圍之內
    {
        cout<<"Out of range!"<<endl;
        cout<<"1)AMD, 2)Intel?: ";
        cin>>mode;
    }
    if(card==1) //判斷是否有會員卡
    {
        if(mode==1)
            Buy(budget,3900);
        else
            Buy(budget,3520);
    }
    else
    {
        if(mode==1)
            Buy(budget,5200);
        else
            Buy(budget,4400);
    }
}
