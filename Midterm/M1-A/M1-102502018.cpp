#include <iostream>
using namespace std;


double MB(double budget,int card);
double CPU(double budget,int card);

int main()
{
    int budget=0;
    int card=-1;
    int cost;
    int a=0,b,c,d,e;
    while(budget<=0)                  //判斷預算範圍
    {
        cout<<"Budget?: ";
        cin>>budget;
        if(budget<=0)
        {
            cout<<"Out of range!\n";
        }
    }
    while(card!=1&&card!=0)           //判斷是否有會員卡
    {
        cout<<"Card?: ";
        cin>>card;
        if(card!=1 && card!=0)
        {
            cout<<"Out of range!\n";
        }
    }
    while(a!=1&&a!=2&&a!=-1)
    {
        cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin>>a;
        if(a!=1&&a!=2&&a!=-1)
        {
            cout<<"Out of range!\n";
        }
    }
    if(a==1)
    {
        cout<<MB(budget,card);                 //呼叫函式
    }
    if(a==2)
    {
        cout<<CPU(budget,card);
    }
    return 0;
}
double MB(double budget,int card)
{
    int b;
    double asus=5000,msi=4500,gigabyte=5000;
    cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin>>b;
    if(card==0)                                 //判斷是否有會員卡
    {
        if(b==1)
        {
            if(budget<asus)                     //判斷錢是否足夠
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-asus;
        }
        if(b==2)
        {
            if(budget<msi)
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-msi;
        }
        if(b==3)
        {
            if(budget<gigabyte)
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-gigabyte;
        }
    }
    else if(card==1)
    {
        if(b==1)
        {
            if(budget<asus*0.85)
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-asus*0.85;
        }
        if(b==2)
        {
            if(budget<msi*0.9)
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-msi*0.9;
        }
        if(b==3)
        {
            if(budget<gigabyte)
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-gigabyte;
        }
    }
    else
        cout<<"Out of range!";
}

double CPU(double budget,int card)
{
    int a;
    double amd=5200,intel=4400;
    cout<<"1)AMD, 2)Intel?: ";
    cin>>a;
    if(card==0)                                 //判斷是否有會員卡
    {
        if(a==1)
        {
            if(budget<amd)                     //判斷錢是否足夠
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-amd;
        }
        if(a==2)
        {
            if(budget<intel)
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-intel;
        }
    }
    else if(card==1)
    {
        if(a==1)
        {
            if(budget<amd)
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-amd*0.75;
        }
        if(a==2)
        {
            if(budget<intel)
            {
                cout<<"Money not enough\n";
            }
            else
                cout<<"Remain $"<<budget-intel*0.9;
        }
    }
    else
        cout<<"Out of range!";
}
