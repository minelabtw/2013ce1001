#include<iostream>
#include<cmath>
using namespace std;

double Buy(double budget, double cost);
double MB(double budget, int card);
double CPU(double budget, int card);

int main()
{
    double budget=0;
    int card=0;
    int choose=0;
    double cost=0;

    while(budget<=0)
    {
        cout<<"Budget?: ";
        cin>>budget;
        if(budget<=0)
            cout<<"Out of range!"<<endl;
    }
    while(choose!=-1)


        do
        {
            cout<<"Card?: ";
            cin>>card;

            switch(card)
            {
            case 1:
                do
                {
                    cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
                    cin>>choose;

                    switch(choose)
                    {
                    case 1:
                        Buy(budget,MB(budget,card));
                        break;
                    case 2:
                        Buy(budget,CPU(budget,card));
                        break;
                    case -1:
                        cost=budget;
                        cout<<"Total cost $"<<cost<<endl<<"Total Remain $ ";
                        break;
                    default:
                        cout<<"Out of range!"<<endl;
                        break;
                    }


                }
                while(choose!=-1);
                break;

            case 0:
                do
                {
                    cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
                    cin>>choose;

                    switch(choose)
                    {
                    case 1:
                        while(choose!=-1)
                            Buy(budget,MB(budget,card));
                        break;
                    case 2:
                        Buy(budget,CPU(budget,card));
                        break;
                    case -1:
                        cost=budget;
                        cout<<"Total cost $"<<cost<<endl<<"Total Remain $ ";
                        break;
                    default:
                        cout<<"Out of range!"<<endl;
                        break;
                    }
                }
                while(choose!=-1);
                break;

            default:
                cout<<"Out of range!"<<endl;
                break;
            }


        }
        while(card!=1 and card!=0);

    return 0;

}

double Buy(double budget, double cost)
{
    double remain=0;
    remain=budget-cost;
    if(remain<0)
    {
        cout<<"Money not enough!"<<endl;
        cout<<"Remain $"<<budget<<endl;
    }
    else
        cout<<"Remain $"<<remain<<endl;
}

double MB(double budget, int card)
{
    int choose=0;
    int cost=0;


    do
    {
        cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin>>choose;
        switch(choose)
        {
        case 1:
            if(card==1)
                cost=5000*0.85;
            else
                cost=5000;

            return cost;

        case 2:
            if(card==1)
                cost=4500*0.9;
            else
                cost=4500;

            return cost;

        case 3:
            cost=5000;
            return cost;

        default:
            cout<<"Out of range!"<<endl;
        }
    }
    while(choose!=1 and choose!=2 and choose!=3);
}

double CPU(double budget, int card)
{
    int choose=0;
    int cost=0;


    do
    {
        cout<<"1)AMD, 2)Intel?: ";
        cin>>choose;
        switch(choose)
        {
        case 1:
            if(card==1)
                cost=5200*0.75;
            else
                cost=5200;

            return cost;

        case 2:
            if(card==1)
                cost=4400*0.8;
            else
                cost=4400;

            return cost;

        default:
            cout<<"Out of range!"<<endl;
        }
    }
    while(choose!=1 and choose!=2);
}
