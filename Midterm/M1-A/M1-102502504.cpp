#include <iostream>
#include <iomanip>

using namespace std;

double Buy(double budget,double cost); //宣告Buy函數
double MB(double budget, int card); //宣告MB函數
double CPU(double budget, int card); //宣告CPU函數

int main()
{
    double budget;
    int card;
    int thing;
    int cost;
    int x;

    cout << "Budget?: ";
    cin >> budget;
    while(budget<=0) //利用while迴圈使預算範圍>0  若無則顯示超出範圍
    {
        cout << "Out of range!" << endl;
        cout << "Budget?: ";
        cin >> budget;
    }

    cout << "Card?: ";
    cin >> card;
    while(card!=0&&card!=1) //判斷是否具有會員卡，只能輸入0和1否則顯示超出範圍
    {
        cout << "Out of range!" << endl;
        cout << "Card?: ";
        cin >> card;
    }

    cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: "; //判斷要購買的東西為何
    cin >> thing;
    while(thing!=1&&thing!=2&&thing!=-1)
    {
        cout << "Out of range!" << endl;
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> thing;
    }

    switch(thing) //利用switch 顯示欲購買物品之品牌
    {
    case 1:
        MB(budget,cost); //呼叫MB函數

        break;

    case 2:
        CPU(budget,card); //呼叫CPU函數

        break;
    }


    return 0;
}


double MB(double budget,int card) //MB函數
{
    int mb,cost;
    cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin >> mb;
    while(mb!=1&&mb!=2&&mb!=3) //利用while迴圈判斷所欲購買的品牌
    {
        cout << "Out of range!" << endl;
        cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> mb;
    }
    cout << "Remain: "  << budget - cost;
    return budget-cost;
}

double CPU(double budget,int card) //CPU函數
{
    int cpu,cost;
    cout << "1)AMD, 2)Intel?: ";
    cin >> cpu;
    while(cpu!=1&&cpu!=2) //利用while迴圈判斷所欲購買的品牌
    {
        cout << "Out of range!" << endl;
        cout << "1)AMD, 2)Intel?: ";
        cin >> cpu;
    }
    cout << "Remain: "  << budget - cost;
    return budget-cost;
}
