#include<iostream>
#include<math.h>
using namespace std;

double totalcost=0;

void Buy(double &budget,double cost) //宣告Buy涵式,計算餘額
{
    if(budget-cost<0)
    {
        cout<<"Money not enough!"<<endl;
        cout<<"Remain $"<<budget<<endl;
    }
    else
    {
        budget=budget-cost;
        totalcost=totalcost+cost;
        cout<<"Remain $"<<budget<<endl;
    }
}
void MB(double &budget,int card) //宣告MB涵式,讓使用者選擇購買的產品,並呼叫Buy涵式判斷餘額
{
    int ASUS=5000;
    int MSI=4500;
    int GIGABYTE=5000;
    int MBtype=0;
    if(card==1)
    {
        ASUS=4250;
        MSI=4050;
    }

    cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin>>MBtype;
    while(MBtype!=1 && MBtype!=2 && MBtype!=3)
    {
        cout<<"Out of range!"<<endl;
        cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin>>MBtype;
    }

    switch(MBtype)
    {
    case 1:
        Buy(budget,ASUS);
        break;

    case 2:
        Buy(budget,MSI);
        break;
    case 3:
        Buy(budget,GIGABYTE);
        break;
    }
}

void CPU(double &budget,int card) //宣告CPU涵式,讓使用者選擇購買的產品,並呼叫Buy涵式判斷餘額
{
    int AMD=5200;
    int Intel=4400;
    int CPUtype=0;
    if(card==1)
    {
        AMD=3900;
        Intel=3520;
    }

    cout<<"1) AMD, 2)Intel?: ";
    cin>>CPUtype;
    while(CPUtype!=1 && CPUtype!=2)
    {
        cout<<"Out of range!"<<endl;
        cout<<"1) AMD, 2)Intel?: ";
        cin>>CPUtype;
    }

    switch(CPUtype)
    {
    case 1:
        Buy(budget,AMD);
        break;

    case 2:
        Buy(budget,Intel);
        break;
    }
}

int main()
{
    double budget=0;
    int card=0;
    int mode=0;

    cout<<"Budget?: "; //輸入預算
    cin>>budget;
    while(budget<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Budget?: ";
        cin>>budget;
    }

    cout<<"Card?: "; //判斷是否為會員
    cin>>card;
    while(card!=1 && card!=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Card?: ";
        cin>>card;
    }

    cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: "; //判斷購買或結帳
    cin>>mode;
    while(mode!=1 && mode!=2 && mode!=-1)
    {
        cout<<"Out of range!"<<endl;
        cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin>>mode;
    }

    while(mode!=-1)
    {
        switch(mode)
        {
        case 1:
        {
            MB(budget,card);
            cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin>>mode;
        }
        break;
        case 2:
        {
            CPU(budget,card);
            cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin>>mode;
        }
        break;
        default:
        {
            cout<<"Out of range!"<<endl;
            cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin>>mode;
        }
        break;
        }
    }

    if(mode==-1) //結帳
    {
        cout<<"Total cost $"<<totalcost<<endl;
        cout<<"Total Remain $"<<budget;
    }

    return 0;
}

