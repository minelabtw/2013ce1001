#include <iostream>
using namespace std;

void Buy(double &budget, double &cost, int &buy)        //計算剩餘預算，計算價格，判斷錢夠不夠
{
    if (buy==1)
    {
        if(budget-cost<0)
            cout << "Money not enough!\n" << "Remain $" << budget << endl;
        else if (budget-cost>=0)
            cout << "Remain $" << budget-cost << endl;
    }
    else if (buy==2)
    {
        if(budget-cost<0)
            cout << "Money not enough!\n" << "Remain $" << budget << endl;
        else if (budget-cost>=0)
            cout << "Remain $" << budget-cost << endl;
    }
}

void MB(double &budget, int &card, int &buy)          //輸出MB商品名，判斷價格
{
    int mb=0;
    double cost=0;
    cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin >> mb;
    while (mb!=1 && mb!=2 && mb!=3)
    {
        cout << "Out of range!\n" << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> mb;
    }

    if (mb==1)
    {
        if (card==0)
            cost=5000;
        else if (card==1)
            cost=5000*0.85;
    }
    else if (mb==2)
    {
        if (card==0)
            cost=4500;
        else if (card==1)
            cost=4500*0.9;
    }
    else if (mb==3)
        cost=5000;
    Buy(budget,cost,buy);
}

void CPU(double &budget, int &card,int &buy)              //輸出CPU商品名，判斷價格
{
    int cpu=0;
    double cost=0;
    cout << "1)AMD, 2)Intel?: ";
    cin >> cpu;
    while (cpu!=1 && cpu!=2 && cpu!=3)
    {
        cout << "Out of range!\n" << "1)AMD, 2)Intel?: ";
        cin >> cpu;
    }
    if (cpu==1)
    {
        if (card==0)
            cost=5200;
        else if (card==1)
            cost=5200*0.75;
    }
    else if (cpu==2)
    {
        if (card==0)
            cost=4400;
        else if (card==1)
            cost=4400*0.8;
    }
    Buy(budget,cost,buy);
}




int main()
{
    double budget=0;       //預算
    int card=0;         //會員卡
    int buy=0;          //購買選項
    int mb=0;           //MB選項
    int cpu=0;          //CPU選項
    double cost=0;         //總價格

    cout << "Budget?: ";          //輸入預算
    cin >> budget;
    while (budget<=0)                //當預算少於等於0輸出Out of range!
    {
        cout << "Out of range!\n" << "Budget?: ";
        cin >> budget;
    }
    cout << "Card?: ";            //輸入是否有會員卡
    cin >> card;
    while (card!=1 && card!=0)
    {
        cout << "Out of range!\n" << "Card?: ";
        cin >> card;
    }
    cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
    cin >> buy;
    while (buy)
    {
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> buy;
        switch(buy)
        {
        case 1:
            MB(budget,card,buy);
            break;
        case 2:
            CPU(budget,card,buy);
            break;
        case -1:
            cout << "Total cost $" << cost << endl << "Total Remain $" << budget-cost;
            break;
        default:
            cout << "Out of range!\n";
            break;
        }
    }
    return 0;
}
