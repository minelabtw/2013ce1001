#include<iostream>
using namespace std ;
void Buy(int & ,int &,double &); //判斷剩下的預算是否足夠買下此次的物品 顯示剩餘預算
void MB(int &,int ,double &);//輸出廠牌名稱 可選擇並判斷有無會員卡將剩餘預算及則扣後的價錢傳入BUY()
void CPU(int &,int,double &);//輸出廠牌名稱 可選擇並判斷有無會員卡將剩餘預算及則扣後的價錢傳入BUY()
int main()
{
    int card=-1 ;//有無會員卡
    int budget ;//預算
    int mode ; //選擇
    double sum ; //總花費
    do //讀預算
    {
        cout <<"Budget?: " ;
        cin >> budget ;
        if(budget<=0)
            cout << "Out of range!\n";
    }
    while(budget<=0);

    while(1) //讀有卡無卡
    {
        cout << "Card?: ";
        cin >> card ;
        if(card==0)
            break ;
        if(card==1)
            break ;
        cout << "Out of range!\n" ;
    }
    while(1) //開始選擇購買
    {
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> mode ;
        if(mode==-1)
        {
            cout <<"Total cost $"<<sum<<endl<<"Total Remain $" <<budget <<endl ;
            break;
        }
        switch(mode)
        {
        case 1 :
            MB(budget,card,sum);
            break ;
        case 2:
            CPU(budget,card,sum);
            break ;
        default :
            cout << "Out of range!\n";
            break;
        }


    }
    return 0;
}




void Buy(int &budget,int &cost,double &sum)
{
    if(budget>=cost) //預算夠
    {
        budget-=cost;
        sum+=cost ;
        cout << "Remain $" <<budget <<endl ;
    }
    else
    {
        cout <<"Money not enough!\n" ;
        cout << "Remain $" <<budget <<endl ;
    }
};
void MB(int &budget,int card ,double &sum)
{
    int choose; //選擇
    int cost; //價錢
    int en=0; //結束計數器
    while(1)
    {
        if(en==1)
            break;
        cout <<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> choose ;
        switch(choose)
        {
        case 1 :
            if(card==1)
            {
                cost=0.85*5000;
                Buy(budget,cost,sum);
                en=1;
            }
            else
            {
                cost=1*5000;
                Buy(budget,cost,sum);
                en=1;
            }
            break ;
        case 2:
            if(card==1)
            {
                cost=0.9*4500;
                Buy(budget,cost,sum);
                en=1;
            }
            else
            {
                cost=1*4500;
                Buy(budget,cost,sum);
                en=1;
            }
            break ;
        case 3 :
            cost =1*5000 ;
            Buy(budget,cost,sum);
            en=1;
            break ;
        default :
            cout << "Out of range!\n";
            break;
        }
    }
}
void CPU(int &budget,int card ,double&sum)
{
    int choose; //選擇
    int cost;   //價錢
    int en=0;   //結束計數器
    while(1)
    {
        if(en==1)
            break ;
        cout <<"1)AMD, 2)Intel?: ";
        cin >> choose ;
        switch(choose)
        {
        case 1 :
            if(card==1)
            {
                cost=0.75*5200;
                Buy(budget,cost,sum);
                en=1;
            }
            else
            {
                cost=1*5200;
                Buy(budget,cost,sum);
                en=1;
            }
            break ;
        case 2:
            if(card==1)
            {
                cost=0.8*4400;
                Buy(budget,cost,sum);
                en=1;
            }
            else
            {
                cost=1*4400;
                Buy(budget,cost,sum);
                en=1;
            }
            break ;
        default :
            cout << "Out of range!\n";
            break;
        };
    }
}
