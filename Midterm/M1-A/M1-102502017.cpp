#include<iostream>
using namespace std;

void Buy(double &budget, double cost)
{
    if(budget-cost<0)cout << "Money not enough!\n";     //預算小於花費
    else budget-=cost;                                  //預算減掉花費
    cout << "Remain $" << budget << endl;               //輸出剩餘預算
}
void MB(double &budget,int card)                        //主機板
{
    int i=0;
    float j=1;                                          //會員折扣
    while(i<1 || i>3)
    {
        cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> i;
        if(i<1 || i>3)cout << "Out of range!\n";
    }
    switch(i)
    {
    case 1: //ASUS
        if(card==1)j=0.85;                              //有會員卡
        Buy(budget,5000*j);
        break;
    case 2: //MSI
        if(card==1)j=0.9;                               //有會員卡
        Buy(budget,4500*j);
        break;
    case 3: //GIGA
        Buy(budget,5000);
        break;
    }
}
void CPU(double &budget,int card)
{
    int i=0;
    float j=1;
    while(i<1 || i>2)
    {
        cout << "1)AMD, 2)Intel?: ";
        cin >> i;
        if(i<1 || i>2)cout << "Out of range!\n";
    }
    switch(i)
    {
    case 1://AMD
        if(card==1)j=0.75;                              //有會員卡
        Buy(budget,5200*j);
        break;
    case 2://Intel
        if(card==1)j=0.8;                               //有會員卡
        Buy(budget,4400*j);
        break;
    }
}

int main(void)
{
    double budget=0;
    int card=-1;
    while(budget <=0 )                                  //判斷關於預算的輸入
    {
        cout << "Budget?: ";
        cin >> budget;
        if(budget<=0)cout << "Out of range!\n";
    }

    double cost = budget;                               //紀錄一開始擁有的預算

    while(card<0 || card >1)                            //判斷關於會員卡的輸入
    {
        cout << "Card?: ";
        cin >> card;
        if(card<0 || card>1)cout << "Out of range!\n";
    }
    int correct=0;
    while(correct!=-1)
    {
        int mode=0;
        while(mode!=1 && mode!=2 && mode!=-1)
        {
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin >> mode;
            if(mode!=1 && mode!=2 && mode!=-1)cout << "Out of range!\n";
        }
        switch(mode)
        {
        case 1:
            MB(budget,card);
            break;
        case 2:
            CPU(budget,card);
            break;
        case -1:
            cout << "Total cost $" << cost-budget << endl;  //輸出花費金額
            cout << "Total Remain $" << budget;             //輸出剩餘預算
            correct = -1;                                   //結束程式
            break;
        }

    }

    return 0;
}
