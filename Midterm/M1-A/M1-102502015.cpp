#include <iostream>
using namespace std;
void Buy(double &budget, double cost)           //宣告 Buy 函數 輸入 (double型態 目前預算剩餘 參照位置,double 常數)
{
    if(budget<cost)                             //超出預算則顯示
    {
        cout<<"Money not enough!"<<endl;
        cout<<"Remain $"<<budget<<endl;         //剩餘
    }
    else
    {
        budget = budget-cost;                   //預算剩餘-消費
        cout<<"Remain $"<<budget<<endl;         //剩餘
    }
}
void MB(double &budget, int card)               //宣告 MB 函數 輸入 (double型態 目前預算剩餘 參照位置,有無卡片)
{
    int brand;                                  //判斷品牌
    cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin>>brand;
    while(brand!=1 && brand!=2 && brand!=3)     //超過範圍則重新輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin>>brand;
    }
    if (brand==1 && card==0)                    //判斷品牌 與有無卡片 再丟進buy 函數做運算與判斷
    {
        Buy(budget,5000);
    }
    if (brand==2 && card==0)
    {
        Buy(budget,4500);
    }
    if (brand==3)                               //有無卡片都依樣價錢
    {
        Buy(budget,5000);
    }
    if (brand==1 && card==1)
    {
        Buy(budget,5000*0.85);
    }
    if (brand==2 && card==1)
    {
        Buy(budget,4500*0.9);
    }
}
void CPU(double &budget, int card)              //宣告 CPU 函數 輸入 (double型態 目前預算剩餘 參照位置,有無卡片)
{
    int brand;                                  //宣告品牌
    cout<<"1)AMD, 2)Intel?: ";
    cin>>brand;
    while(brand!=1 && brand!=2)                 //超出範圍則重新輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"1)AMD, 2)Intel?: ";
        cin>>brand;
    }
    if (brand==1 && card==0)//判斷品牌 與有無卡片再丟進buy 函數做運算與判斷
    {
        Buy(budget,5200);
    }
    if (brand==2 && card==0)
    {
        Buy(budget,4400);
    }
    if (brand==1 && card==1)
    {
        Buy(budget,5200*0.75);
    }
    if (brand==2 && card==1)
    {
        Buy(budget,4400*0.8);
    }
}


int main()
{
    double budget;                              //預算=剩餘金額
    double cost;                                //總支出
    int card;                                   //卡片有無
    int mode=0;                                 //判斷買甚麼 還是結算 初始化為0
    cout<<"Budget?: ";
    cin>>budget;
    while(budget<=0)                            //超出範圍則重新輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Budget?: ";
        cin>>budget;
    }
    cout<<"Card?: ";
    cin>>card;
    while(card!=0 && card!=1)                   //超出範圍則重新輸入
    {
        cout<<"Out of range!"<<endl;
        cout<<"Card?: ";
        cin>>card;
    }
    cost=budget;                                //儲存預算
    while (mode !=-1)                           //迴圈直到 結帳
    {
        cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin>>mode;
        switch(mode)                            //判斷要買MB 還是 CPU 還是結算
        {
        case 1:
            MB(budget,card);                    //MB函數做運算
            break;

        case 2:
            CPU(budget,card);                   //CPU函數做運算
            break;
        case -1:
            break;                              //結算
        default:
            cout<<"Out of range!"<<endl;        //超出範圍則重新輸入
            break;
        }
    }
    cost=cost-budget;                           //支出= 預算-剩餘
    cout<<"Total cost $"<<cost<<endl;           //輸出支出
    cout<<"Total Remain $"<<budget;             //輸出剩餘
    return 0;
}
