#include <iostream>
using namespace std;

double MB(double budget, int card);  //宣告MB函式
double CPU(double budget, int card);
int main()
{
    double budget,cost,remain;
    int card,buy;
    cout << "Budget?: ";
    cin >> budget;
    while (budget<=0)  //使用while迴圈判斷預算是否大於0
    {
        cout << "Out of range!" << endl << "Budget?: ";
        cin >> budget;
    }
    cout << "Card?: ";
    cin >> card;
    while (card!=1 or card!=0)  //使用while迴圈判斷是否擁有會員卡
    {
        switch (card)
        {
        case 0:
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin >> buy;
            if (buy==1)
                MB(budget,card);  //呼叫MB函式
            cout << endl;
            if (buy==2)
                CPU(budget,card);  //呼叫CPU函式
            cout << endl;
            if (buy==-1)
                cout << "Total cost $" << cost << endl;
            cout << "Total Remain $" << remain;

            if (buy!=1 or buy!=2 or buy!=-1)
                cout << "Out of range!";
            break;
        case 1:
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin >> buy;
            if (buy==1)
                MB(budget,card);

            if (buy==2)
                CPU(budget,card);

            if (buy==-1)
                cout << "Total cost $" << cost << endl;
            cout << "Total Remain $" << remain;

            if (buy!=1 or buy!=2 or buy!=-1)
                cout << "Out of range!";
            break;
        default:
            cout << "Out of range!" << endl << "Card?: ";
            cin >> card;
            break;
        }
    }

}
double MB(double budget, int card)  //MB函式
{
    int MB;
    double cost;
    cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin >> MB;
    if (card==1&MB==1)
        cost=5000*0.85;
    budget=budget-5000*0.85;
    if (card==0&MB==1)
        cost=5000;
    budget=budget-5000;
    if (card==1&MB==2)
        cost=4500*0.9;
    budget=budget-4500*0.9;
    if (card==0&MB==2)
        cost=4500;
    budget=budget-4500;
    if (card==1&MB==3)
        cost=5000;
    budget=budget-5000;
    if (card==0&MB==3)
        cost=5000;
    budget=budget-5000;

    return budget,cost;
}
double CPU(double budget, int card)  //CPU函式
{
    int CPU;
    double cost;
    cout << "1)AMD, 2)Intel?: ";
    cin >> CPU;
    if (card==1&CPU==1)
        cost=5200*0.75;
    budget=budget-5200*0.75;
    if (card==0&CPU==1)
        cost=5200;
    budget=budget-5200;
    if (card==1&CPU==2)
        cost=4400*0.8;
    budget=budget-4400*0.8;
    if (card==0&CPU==2)
        cost=4400;
    budget=budget-4400;
    if (CPU!=1 or CPU!=2)
        cout << "Out of range!" << endl << "1)AMD, 2)Intel?: ";
    cin >> CPU;
    return budget,cost;
}
