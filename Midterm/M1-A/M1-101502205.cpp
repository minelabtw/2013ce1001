#include<iostream>

using namespace std;

//buy function
void Buy(double &budget, double cost) // double budget is passing by reference
{
    if(budget>=cost)
    {
        //enough money
        budget-=cost;//buy it
        cout << "Remain $" << budget << endl;;
    }
    else
    {
        //not enough money
        cout << "Money not enough!" << endl;
        cout << "Remain $" << budget << endl;;
    }
}

//MB function
void MB(double &budget, int card)
{
    double cost[3]= {5000,4500,5000}; //ASUS 5000, MSI 4500, GIGABYTE 5000
    double c[3]= {0.85,0.9,1}; //have card
    int option;
    //ask what to buy
    while(1)
    {
        cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> option;
        if(option>=1 && option<=3) // 1,2,3 are legal
            break;
        cout << "Out of range!" << endl; //error
    }
    if(card)
        Buy(budget, cost[option-1]*c[option-1]); //if have card then the cost is reduced
    else
        Buy(budget, cost[option-1]); //if not, just give original cost
}

void CPU(double &budget, int card)
{
    double cost[2]= {5200,4400}; //AMD 5200, Intel 4400
    double c[2]= {0.75,0.8}; //have card
    int option;
    //ask what to buy
    while(1)
    {
        cout << "1)AMD, 2)Intel?: ";
        cin >> option;
        if(option>=1 && option<=2) // 1,2 are legal
            break;
        cout << "Out of range!" << endl; //error
    }
    if(card)
        Buy(budget, cost[option-1]*c[option-1]); //if have card then the cost is reduced
    else
        Buy(budget, cost[option-1]); //if not, just give original cost
}

int main()
{
    double budget;
    int card;
    //ask budget
    while(1)
    {
        cout << "Budget?: ";
        cin >> budget;
        if(budget>0) //break if legal;
            break;
        cout << "Out of range!" << endl; //error
    }
    //ask card
    while(1)
    {
        cout << "Card?: ";
        cin >> card;
        if(card==1 || card==0) //1 and 0 are legal
            break;
        cout << "Out of range!" << endl; //error
    }
    bool checkout = false; //used to check if checking out
    int option;
    double total = budget; //initial budget
    while(!checkout)
    {
        //ask what to do
        while(1)
        {
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin >> option;
            if(option==1 || option==2 || option==-1) //1, 2, -1 are legal
                break;
            cout << "Out of range!" << endl; //error
        }
        switch(option)
        {
        case 1:
            //buy MB call function MB
            MB(budget, card);
            break;
        case 2:
            //buy CPU call function CPU
            CPU(budget, card);
            break;
        case -1:
            //checkout
            checkout = true;
            break;
        }
    }
    total -= budget; //total cost = initial - remain
    cout << "Total cost $" << total << endl; //output total cost
    cout << "Total Remain $" << budget << endl; //output total remain
    return 0;
}
