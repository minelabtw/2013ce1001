#include <iostream>

using namespace std;
void Buy(double &budget, double cost)
{
    if(budget-cost<0)
    {
        cout <<"Money not enough!" << endl;
    }
    else
    {
        budget-=cost;
    }
    cout << "Remain $" << budget << endl;
}
void MB(double &budget, int card)
{
    int type=0;
    cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    while(type!=1&&type!=2&&type!=3)
    {
        cin >> type;
        if(type!=1&&type!=2&&type!=3)//judge range
            cout << "Out of range!" << endl;
    }
    double MB[3]= {5000,4500,5000};
    double MB_off[3]= {4250,4050,5000};
    if(card==1)
    {
        Buy(budget,MB_off[type-1]);
    }
    else if(card==0)
    {
        Buy(budget,MB[type-1]);
    }
}
void CPU(double &budget, int card)
{
    int type=0;
    while(type!=1&&type!=2)
    {
        cout << "1)AMD, 2)Intel?: ";
        cin >> type;
        if(type!=1&&type!=2)//judge range
            cout << "Out of range!" << endl;
    }
    double CPU[2]= {5200,4400};
    double CPU_off[2]= {5200*0.75,4400*0.8};
    if(card==1)
    {
        Buy(budget,CPU_off[type-1]);
    }
    else if(card==0)
    {
        Buy(budget,CPU[type-1]);
    }
}
int main()
{
    double budget=0;
    int card=-1;
    while(budget<=0)
    {
        cout << "Budget?: ";
        cin >> budget;
        if(budget<=0)//judge range
            cout << "Out of range!" << endl;
    }
    double Total_budget=budget;
    while(card!=1&&card!=0)
    {
        cout << "Card?: ";
        cin >> card;
        if(card!=1&&card!=0)//judge range
            cout << "Out of range!" << endl;
    }
    int action=0;
    while(true)
    {
        while(action!=1&&action!=2&&action!=-1)
        {
            cout << "1)Buy MB, 2)Buy CPU, -1)Check out?: ";
            cin >> action;
            if(action!=1&&action!=2&&action!=-1)//judge range
                cout << "Out of range!" << endl;
        }
        if(action==1)
        {
            MB(budget,card);
        }
        else if(action==2)
        {
            CPU(budget,card);
        }
        else if(action==-1)
        {
            break;
        }
        action=0;
    }
    cout << "Total cost $" << Total_budget-budget << endl;
    cout << "Total Remain $" << budget << endl;
    return 0;
}
