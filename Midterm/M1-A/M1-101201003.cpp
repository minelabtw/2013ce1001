#include<iostream>
using namespace std;
double Buy(double,double);
double MB(double,int);
double CPU(double,int);

int budget=0,card=0,mode=0,cost=0,remain=0;
int main()
{
    cout<<"Budget?: ";                                  //問預算
    cin>>budget;
    while(budget<=0)                                    //要求要大於零
    {
        cout<<"Out of range!"<<endl<<"Budget?: ";
        cin>>budget;
    }
    cout<<"Card?: ";                                    //有無會員
    cin>>card;
    while(card!=0&&card!=1)                             //當輸入不等於1或0時要求重新輸入
    {
        cout<<"Out of range!"<<endl<<"Card?: ";
        cin>>card;
    }
    switch(card)                                        //判斷有無會員卡
    {
            case 1:                                     //有會員卡
            {
                cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";    //問要購買哪種商品
                cin>>mode;
                while(mode!=1&&mode!=2&&mode!=-1)       //輸入錯誤
                {
                    cout<<"Out of range!"<<endl<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
                    cin>>mode;
                }
                switch(mode)                            //判斷要買哪種商品
                {
                    case 1:                             //買筆電
                    {
                        MB(budget,1);                   //進入筆電的函式

                    }
                    break;
                    case 2:                             //買CPU
                    {
                        CPU(budget,1);                  //進入CPU的函式
                    }
                    break;
                    case -1:                            //結帳
                    {
                        cout<<"Total cost $"<<cost<<endl
                            <<"Total remain $"<<remain;
                    }
                    break;
                }

            }
            break;
            case 0:                                       //與case1相同
            {
                cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
                cin>>mode;
                while(mode!=1&&mode!=2&&mode!=-1)
                {
                    cout<<"Out of range!"<<endl<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
                    cin>>mode;
                }
                switch(mode)
                {
                    case 1:
                    {
                        MB(budget,0);
                    }
                    break;
                    case 2:
                    {
                        CPU(budget,0);
                    }
                    break;
                    case -1:
                    {
                        cout<<"Total cost $"<<cost<<endl
                            <<"Total remain $"<<remain;
                    }
                    break;
                }
            }
            break;
        cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";    //問要購買哪種商品
        cin>>mode;
    }

    return 0;
}

double Buy(double budget,double cost)                       //Buy函數
{
    int remain=0,budget1=0;
    budget1=budget-cost;
    if(budget1>=0)
    {
        remain=budget1;
        cout<<"Remain $"<<remain;
    }
    if(budget1<0)
    {
        remain=budget;
        cout<<"Money not enough!"<<endl<<"Remain $"<<remain;
    }
}
double MB(double budget,int card)                           //MB函數
{
    int mb=0,cost=0,buy=0;
    cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin>>mb;
    while(mb!=1&&mb!=2&&mb!=3)
    {
        cout<<"Out of range!"<<endl<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin>>mb;
    }
    buy=Buy(budget,cost);
    switch(card)
    {
        case 0:
        {
            if(mb=1)
            {
                cost=5000;
                Buy(budget,cost);
            }
           if(mb=2)
            {
                cost=4500;
                Buy(budget,cost);
            }
            if(mb=3)
            {
                cost=5000;
                Buy(budget,cost);
            }
        }
        break;
        case 1:
        {
            if(mb=1)
            {
                cost=5000*0.85;
                buy;
            }
            if(mb=2)
            {
                cost=4500*0.9;
                Buy(budget,cost);
            }
            if(mb=3)
            {
                cost=5000;
                Buy(budget,cost);
            }

        }
        break;
    }
}

double CPU(double budget,int card)                          //CPU函數
{
    int cpu=0,cost=0,buy=0;
    cout<<"1)AMD, 2)Intle?: ";
    cin>>cpu;
    while(cpu!=1&&cpu!=2)
    {
        cout<<"Out of range!"<<endl<<"1)AMD, 2)Intle?: ";
        cin>>cpu;
    }
    buy=Buy(budget,cost);
    switch(card)
    {
        case 0:
        {
            if(cpu=1)
            {
                cost=5200;
                Buy(budget,cost);
            }
            if(cpu=2)
            {
                cost=4400;
                Buy(budget,cost);
            }
        }
        break;
        case 1:
        {
            if(cpu=1)
            {
                cost=5200*0.75;
                Buy(budget,cost);
            }
            if(cpu=2)
            {
                cost=4400*0.8;
                Buy(budget,cost);
            }
        }
        break;
    }
}


