#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
void Buy(double &budget,double cost)         //判斷是否可以買，如果可以就買，不行就輸出現在剩多少錢
{
    if (budget<cost)                         //預算比要花的少，則不能買，就輸出不能買和現在剩多少錢
    {
        cout << "Money not enough!" <<endl;
        cout << "Remain $" << budget <<endl;
    }
    else                                     //其他則可以買，就買下來，看剩多少錢
    {
        budget=budget-cost;
        cout << "Remain $" << budget <<endl;
    }
}
void MB(double &budget,int card)             //詢問是要買哪一個，且判斷有沒有卡，決定要花的錢cost是多少，並傳到Buy做結帳，或判斷錢不夠
{
    int choose_MB ;                          //存使用者選擇哪項產品
    double cost ;                            //存要花多少錢
    do                                       //詢問使用者要哪一項產品，如果輸入數字不符合的話要求重新輸入
    {
        cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: " ;
        cin >>choose_MB ;
        if (choose_MB!=1 && choose_MB!=2 && choose_MB!=3)
            cout << "Out of range!" <<endl ;
    }
    while (choose_MB!=1 && choose_MB!=2 && choose_MB!=3);
    if (choose_MB==1 && card==0)             //各種產品和有沒有卡的情況下要付的錢
        cost=5000;
    else if (choose_MB==2 && card==0)
        cost=4500;
    else if (choose_MB==3)
        cost=5000;
    else if (choose_MB==1 && card==1)
        cost=4250;
    else if (choose_MB==2 && card==1)
        cost=4050;
    Buy(budget,cost);                        //把現在預算剩多少和現在要花多少傳給Buy做判斷可不可以買，可以的話就買下來
}
void CPU(double & budget,int card)           //詢問是要買哪一個，且判斷有沒有卡，決定要花的錢cost是多少，並傳到Buy做結帳，或判斷錢不夠
{
    int choose_CPU ;                         //存使用者選擇哪項產品
    double cost ;                            //存要花多少錢
    do                                       //詢問使用者要哪一項產品，如果輸入數字不符合的話要求重新輸入
    {
        cout << "1)AMD, 2)Intel?: " ;
        cin >>choose_CPU ;
        if (choose_CPU!=1 && choose_CPU!=2)
            cout << "Out of range!" << endl ;
    }
    while (choose_CPU!=1 && choose_CPU!=2);
    if (choose_CPU==1 && card==0)            //各種產品和有沒有卡的情況下要付的錢
        cost=5200;
    else if (choose_CPU==2 && card==0)
        cost=4400;
    else if (choose_CPU==1 && card==1)
        cost=3900;
    else if (choose_CPU==2 && card==1)
        cost=3520;
    Buy(budget,cost);
}
int main()
{
    double budget;                           //存剩下的預算多少
    double budget_O;                         //存原本的預算多少
    int card ;                               //存是否有會員卡
    int choose ;                             //存使用這要選擇的是MB或CPU，再進到各自的函式中
    do                                       //詢問使用者預算多少，如果輸入數字不符合的話要求重新輸入
    {
        cout << "Budget?: " ;
        cin >> budget ;
        if (budget <=0)
            cout << "Out of range!" << endl ;
    }
    while (budget <=0);
    budget_O=budget ;                        //把原本的預算先存起來
    do                                       //詢問使用者有沒有會員卡，如果輸入數字不符合的話要求重新輸入
    {
        cout << "Card?: " ;
        cin >>card ;
        if (card!=0 && card!=1)
            cout << "Out of range!" << endl ;
    }
    while (card!=0 && card!=1);
    do                                       //判斷是否結束，如果要結束的話(choose==-1)
    {
        do                                   //詢問使用者要哪一種產品，MB或CPU，如果輸入數字不符合的話要求重新輸入
        {
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: " ;
            cin >>choose ;
            if (choose!=1 && choose!=2 && choose!=-1)
                cout << "Out of range!" << endl ;
        }
        while (choose!=1 && choose!=2 && choose!=-1);
        if (choose==1)                       //如果使用者要的是MB，則進入MB的函式中
            MB(budget,card);
        else if (choose==2)                  //如果使用者要的是CPU，則進入CPU的函式中
            CPU(budget,card);
    }
    while (choose!=-1);
    cout << "Total cost $" << budget_O-budget <<endl ;//告訴使用者他總共花得多少錢
    cout << "Total Remain $" << budget ;              //告訴使用者他現在剩下多少錢

    return 0;
}
