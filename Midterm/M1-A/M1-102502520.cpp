#include <iostream>
#include <math.h>

using namespace std;

double Buy(double budget, double cost);//定式函式及其他要用到之數
double MB(double budget, int card);
double CPU(double budget, int card);
double budget;
double cost=0;
double remain;
int card;
int mode;
int main()
{
    cout<<"Budget?: ";
    cin>>budget;//輸入
    while(budget<0)//檢驗
    {
        cout<<"Out of range!\n";
        cout<<"Budget?: ";
        cin>>budget;
    }

    cout<<"Card?: ";
    cin>>card;//輸入
    while (card!=0&&card!=1)//檢驗
    {
        cout<<"Out of range!\n";
        cout<<"Card?: ";
        cin>>card;
    }


    while(mode!=-1)//模式不等於-1，持續進行
    {
        cout<<"1)Buy MB, 2)Buy CPU, -1)Chkeckout?: ";
        cin>>mode;
        switch (mode)
        {
        case 1://買MB
            MB(budget,card);//呼叫MB函式
            break;
        case 2 ://買CPU
            CPU(budget,card);//呼叫CPU函式
            break;
        case -1 ://結帳
            break;
        default://其他字元
            cout<<"Out of range!\n";
            break;
        }
    }
    cout<<"Total Cost: "<<cost<<endl;//總支出
    remain = budget - cost;
    cout<<"Total Remain: "<<remain<<endl;//剩餘金額

    return 0;
}
double Buy(double budget, double cost)
{
    if(cost>budget)//金額不足
    {
        cout<<"Money not enough!\n";
    }
}
double MB(double ,int )
{
    int MBn;
    cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin>>MBn;
    switch (MBn)
    {
    case 1://買AUSU
        if (card==1)//有折扣
        {
            cost=cost+5000*0.85;
        }
        cost=cost+5000;
        break;
    case 2 ://買MSI
        if (card==1)//有折扣
        {
            cost=cost+4500*0.9;
        }
        cost=cost+4500;
        break;
    case 3://買GIGABYTE
        cost=cost+5000;//無折扣
    default://其他字元
        cout<<"Out of range!\n";
        break;
    }
    Buy(budget,cost);//呼叫Buy函式
    if(budget>cost)
    {
        remain=budget-cost;
        cout<<"Remain: "<<remain<<endl;
    }
}
double CPU (double , int)
{
    int CPUn;
    cout<<"1)AMD, 2)Intel?: ";
    cin>>CPUn;
    switch (CPUn)
    {
    case 1://買AMD
        if(card==1)//有折扣
        {
            cost=cost + 5200*0.75;
        }
        cost=cost + 5200;
        break;
    case 2://買Intel
        if(card==1)//有折扣
        {
            cost=cost + 4400*0.8;
        }
        cost=cost + 4400;
        break;
    default://其他字元
        cout<<"Out of range!\n";
        break;
    }
    Buy(budget,cost);//呼叫Buy函式
    if (budget>cost)
    {
        remain=budget-cost;
        cout<<"Remain: "<<remain<<endl;
    }
}
