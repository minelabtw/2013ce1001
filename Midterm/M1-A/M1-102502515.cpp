#include <iostream>

using namespace std;

int main()
{
    double budget ;
    double cost =0;
    int card ;
    int choice ;
    int a;


    cout << "Budget?: " ;
    cin  >> budget ;

    while (budget <= 0)
    {
        cout << "Out of range!" << endl;
        cout << "Budget?: " ;
        cin  >> budget ;
    }

    cout << "Card?: " ;
    cin  >> card ;
    while (card != 1 && card!=0)
    {
        cout << "Out of range!" << endl ;
        cout << "Card?: " ;
        cin  >> card ;
    }
    do
    {
        cout << "1)Buy MB, 2)Buy CPU, -1)Chekout?: " ;
        cin  >> choice ;
        while (choice != 1 && choice!= 2 && choice != -1)
        {
            cout << "Out of range!" << endl ;
            cout << "1)Buy MB, 2)Buy CPU, -1)Chekout?: " ;
            cin  >> choice ;
        }
        if (choice != -1)
        {
            switch (card)                                                       //有會員卡採取的計算
            {
            case 1 :
                cout << "1)ASUS, 2)MSI, 3)GIGABYTE: " ;
                cin  >> a;
                while (a!=1 && a!=2 && a!=3)            //設定條件
                {
                    cout << "Out of range!" << endl;
                    cout << "1)ASUS, 2)MSI, 3)GIGABYTE: " ;
                    cin  >> a ;
                }

                if (a==1)
                {
                    if (budget<cost)
                    {
                        cout << "Money not enough!" << endl;
                    }
                    else
                        cost=cost+5000*0.85 ;
                }
                else if (a==2)
                {
                    if (budget<cost)
                    {
                        cout << "Money not enough!" << endl;
                    }
                    else
                        cost=cost+4500*0.9;
                }
                else if (a==3)
                {
                    if (budget<cost)
                    {
                        cout << "Money not enough!" << endl;
                    }
                    else
                        cost=cost+5000;
                }
                cout << "Remain $" << budget-cost << endl; //顯示出餘額
                break;//跳出switch

            case 0 :    //沒有會員卡的狀況
                cout << "1)ASUS, 2)MSI, 3)GIGABYTE: " ;
                cin  >> a ;
                while (a!=1 && a!=2 && a!=3)        //使輸入在選擇內
                {
                    cout << "Out of range!" << endl;
                    cout << "1)ASUS, 2)MSI, 3)GIGABYTE: " ;
                    cin  >> a ;
                }

                if (a==1)
                {
                    if (budget<cost)
                    {
                        cout << "Money not enough!" << endl;
                    }
                    else
                        cost=cost+5000 ;
                }
                else if (a==2)
                {
                    if (budget<cost)
                    {
                        cout << "Money not enough!" << endl;
                    }
                    else
                        cost=cost+4500 ;
                }
                else if (a==3)
                {
                    if (budget<cost)
                    {
                        cout << "Money not enough!" << endl;
                    }
                    else
                        cost=cost+5000 ;
                }
                cout << "Remain $" << budget-cost << endl; //顯示出餘額
                break;  //跳出switch
            }
        }

    }
    while (choice!=-1) ;//輸入為-1時跳出迴圈

    cout << "Total cost $" << cost << endl;
    cout << "Total Remain $" << budget-cost ;


    return 0;
}
