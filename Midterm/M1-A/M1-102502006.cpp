#include <iostream>

using namespace std;

void Buy(double &budg, double cost) // 判斷購買
{
    if(cost>budg)cout << "Money not enough!\nRemain $" << budg << endl;
    else
    {
        budg = budg - cost;
        cout << "Remain $" << budg << endl;
    }
}

void MB(double &budg, int card) // MB 類型
{
    int mode2=0; // 購買細節
    int ASUS=5000;
    int MSI=4500;
    int GIG=5000;

    while(mode2>3 || mode2<1)
    {
        cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> mode2;
        if(mode2>3 || mode2<1)cout << "Out of Range!\n";
    }

    if(card==1) // 會員
    {
        ASUS = ASUS * 0.85+1;
        MSI = MSI * 0.9;
    }

    switch(mode2)
    {
    case 1:
        Buy(budg,ASUS);
        break;
    case 2:
        Buy(budg,MSI);
        break;
    case 3:
        Buy(budg,GIG);
        break;
    default:
        break;
    }
}

void CPU(double &budg, int card)  // CPU 類型
{
    int mode2=0; // 購買細節
    int AMD=5200;
    int Intel=4400;

    while(mode2>2 || mode2<1)
    {
        cout << "1)AMD, 2)Intel?: ";
        cin >> mode2;
        if(mode2>2 || mode2<1)cout << "Out of Range!\n";
    }

    if(card==1) // 會員
    {
        AMD = AMD * 0.75;
        Intel = Intel * 0.8;
    }

    switch(mode2)
    {
    case 1:
        Buy(budg,AMD);
        break;
    case 2:
        Buy(budg,Intel);
        break;
    default:
        break;
    }
}

int main()
{
    double budg=0; // 本金
    int card=-1; // 會員
    int mode=0; // 購買什麼

    while(budg<1)
    {
        cout << "Budget?: ";
        cin >> budg;
        if(budg<1)cout << "Out of range!\n";
    }
    int money=0;
    money = budg; // 儲存本金

    while(card !=0 && card != 1)
    {
        cout << "Card?: ";
        cin >> card;
        if(card != 0 && card != 1)cout << "Out of range!\n";
    }

    cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
    while( cin >> mode && mode != -1)
    {
        switch(mode)
        {
        case 1:
            MB(budg,card); // 購買MB
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            break;
        case 2:
            CPU(budg,card); // 購買CPU
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            break;
        default:
            cout << "Out of range!\n";
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            break;
        }
    }

    cout << "Total cost $" << money - budg << endl; // 總花費
    cout << "Total Remain $" << budg; // 總剩餘

    return 0;
}
