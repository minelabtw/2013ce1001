#include<iostream>
using namespace std;
void Buy(double &budget,double inbudget)//buy function，inbudget為原budget
{
    if (budget<0)
    {
    cout <<"Money not enough!"<<endl;
    cout <<"Remain "<<inbudget<<endl;//使用原本存好的budget值
    }
    if (budget>=0)
    {
    cout <<"Remain "<<budget<<endl;
    }

}
void MB(double &budget,int card)//MB function
{
    int asus=5000,msi=4500,gigabyte=5000;
    int mb;
    int inbudget;
    inbudget=budget;//儲存原budget值，以免值被改掉
    switch(card)
    {
    case 0://當沒有會員時
    do{
    cout <<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin >>mb;
    if (mb<=0 || mb>3)
    cout <<"Out of range!"<<endl;
    }while(mb<=0 || mb>3);
    switch(mb)
    {
    case 1://asus
    budget=budget-asus;
    Buy(budget,inbudget);
    break;
    case 2://msi
    budget=budget-msi;
    Buy(budget,inbudget);
    break;
    case 3://gigabyte
    budget=budget-gigabyte;
    Buy(budget,inbudget);
    break;
    }
    break;
    case 1://當有會員時
    do{
    cout <<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin >>mb;
    if (mb<=0 || mb>3)
    cout <<"Out of range!"<<endl;
    }while(mb<=0 || mb>3);
    switch(mb)
    {
    case 1://asus加折扣
    budget=budget-asus*0.85;
    Buy(budget,inbudget);
    break;
    case 2://msi加折扣
    budget=budget-msi*0.9;
    Buy(budget,inbudget);
    break;
    case 3://gigabyte加折扣
    budget=budget-gigabyte;
    Buy(budget,inbudget);
    break;
    }
    break;
}
}
void CPU(double &budget,int card)//CPU function
{
    int amd=5200,intel=4400;
    int cpu;
    int inbudget;
    inbudget=budget;//儲存原budget值，以免值被改掉
    switch(card)
    {
    case 0://當沒有會員時
    do{
        cout <<"1)AMD, 2)Intel?: ";
        cin >>cpu;
        if (cpu<1 || cpu>2)
        cout <<"Out of range!"<<endl;
    }while(cpu<1 || cpu>2);
    switch(cpu)
    {
        case 1://amd
        budget=budget-amd;
        Buy(budget,inbudget);
        break;
        case 2://intel
        budget=budget-intel;
        Buy(budget,inbudget);
        break;
    }
    break;
    case 1://當有會員時
    do{
        cout <<"1)AMD, 2)Intel?: ";
        cin >>cpu;
        if (cpu<1 || cpu>2)
        cout <<"Out of range!"<<endl;
    }while(cpu<1 || cpu>2);
    switch(cpu)
    {
        case 1://amd
        budget=budget-amd*0.75;
        Buy(budget,inbudget);
        break;
        case 2://intel
        budget=budget-intel*0.8;
        Buy(budget,inbudget);
        break;
    }
    break;
    }
}
int main()
{
    double budget;
    int inbudget;//原budget值，怕被改掉
    int card;
    int mb;
    int cpu;
    int buy;

    do{
        cout <<"Budget?: ";
        cin >>budget;
        if (budget<=0)
        cout <<"Out of range!"<<endl;
    }while(budget<=0);
    inbudget=budget;
    do{
        cout <<"Card?: ";
        cin >>card;
        if (card>1 || card<0)
        cout <<"Out of range!"<<endl;
    }while(card>1 || card<0);
    do{
        cout <<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >>buy;
        if(buy<-1 || buy>2 || buy==0)
        cout <<"Out of range!"<<endl;
    }while(buy<-1 || buy>2 || buy==0);
    while (buy!=-1)
    {
    switch(buy)//看要做什麼
    {case 1:
    MB (budget,card);
    do{
        cout <<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >>buy;
        if(buy<-1 || buy>2 || buy==0)
        cout <<"Out of range!"<<endl;
    }while(buy<-1 || buy>2 || buy==0);
    break;
    case 2:
    CPU (budget,card);
    do{
        cout <<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >>buy;
        if(buy<-1 || buy>2 || buy==0)
        cout <<"Out of range!"<<endl;
    }while(buy<-1 || buy>2 || buy==0);
    break;
    cout <<"Total cost "<<inbudget-budget;
    cout <<"Total remain "<<budget;
    do{
        cout <<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >>buy;
        if(buy<-1 || buy>2 || buy==0)
        cout <<"Out of range!"<<endl;
    }while(buy<-1 || buy>2 || buy==0);
    break;
    }
    }
    cout <<"Total cost "<<inbudget-budget<<endl;//若為checkout，直接處理
    cout <<"Total remain "<<budget;
    return 0;
}

