#include<iostream>
#include <math.h>
using namespace std;

int main()
{

    double budget = 0 , card = 0 , cost = 0 , remain = 0 ;
    int buy1 = 0 , buy2 = 0 ;

    do
    {
        cout << "Budget?: " ;
        cin >> budget ;
        if ( budget <= 0 )
            cout << "Out of range!" << endl ;
    }
    while ( budget <= 0 ) ;

    do
    {
        cout << "Card?: " ;
        cin >> card ;
        if ( card != 1 && card != 0 )
            cout << "Out of range!" << endl ;
    }
    while ( card != 1 && card != 0 ) ;


    while ( buy1 != -1 )
    {
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: " ;
        cin >> buy1 ;

        switch(buy1)
        {
        case 1 :
            while ( buy2 != 1 && buy2 != 2 && buy2 != 3)
            {
                cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: " ;
                cin >> buy2 ;

                switch(buy2)
                {
                case 1 :

                    if (card == 1 )
                    {
                        remain = budget - 5000 * 0.85 ;
                        if ( remain < 0 )
                        {
                            cout << "Money not enough!" << endl ;
                            cout << "Remain $" << budget << endl ;
                        }
                        else
                            cout << "Remain $" << remain << endl ;
                    }
                    else if (card == 0 )
                    {
                        remain = budget - 5000 ;
                        if ( remain < 0 )
                        {
                            cout << "Money not enough!" << endl ;
                            cout << "Remain $" << budget << endl ;
                        }
                        else
                            cout << "Remain $" << remain << endl ;
                    }
                    break ;
                case 2 :

                    if (card == 1 )
                    {
                        remain = budget - 4500 * 0.9 ;
                        if ( remain < 0 )
                        {
                            cout << "Money not enough!" << endl ;
                            cout << "Remain $" << budget << endl ;
                        }
                        else
                            cout << "Remain $" << remain << endl ;
                    }

                    else if (card == 0 )
                    {
                        remain = budget - 4500 ;
                        if ( remain < 0 )
                        {
                            cout << "Money not enough!" << endl ;
                            cout << "Remain $" << budget << endl ;
                        }
                        else
                            cout << "Remain $" << remain << endl ;
                    }
                    break ;

                case 3 :
                    remain = budget -  5000 ;
                    if ( remain < 0 )
                    {
                        cout << "Money not enough!" << endl ;
                        cout << "Remain $" << budget << endl ;
                    }
                    else
                        cout << "Remain $" << remain << endl ;
                    break ;
                default :
                    cout << "Out of range!" << endl ;
                    break ;
                }
            }
            break ;
        case 2 :
            while ( buy2 != 1 && buy2 != 2 )
            {
                cout << "1)AMD, 2)Intel?: " ;
                cin >> buy2 ;

                switch(buy2)
                {
                case 1 :
                    if ( card == 1 )
                    {
                        remain = budget - 5200 * 0.75 ;

                        if ( remain < 0 )
                        {
                            cout << "Money not enough!" << endl ;
                            cout << "Remain $" << budget << endl ;
                        }
                        else
                            cout << "Remain $" << remain << endl ;
                    }
                    else if ( card == 0 )
                    {
                        remain = budget - 5200 ;

                        if ( remain < 0 )
                        {
                            cout << "Money not enough!" << endl ;
                            cout << "Remain $" << budget << endl ;
                        }
                        else
                            cout << "Remain $" << remain << endl ;
                    }
                    break ;
                case 2 :
                    if (card == 1 )
                    {
                        remain = budget - 4400 * 0.8 ;
                        if ( remain < 0 )
                        {
                            cout << "Money not enough!" << endl ;
                            cout << "Remain $" << budget << endl ;
                        }
                        else
                            cout << "Remain $" << remain << endl ;
                    }
                    else if (card == 0 )
                    {
                        remain = budget - 4400 ;
                        if ( remain < 0 )
                        {
                            cout << "Money not enough!" << endl ;
                            cout << "Remain $" << budget << endl ;
                        }
                        else
                            cout << "Remain $" << remain << endl ;
                    }
                    break ;
                default :
                    cout << "Out of range!" << endl ;
                    break ;
                }
            }
            break ;
        case -1 :
            break ;
        default :
            cout << "Out of range!" << endl ;
            break ;
        }
    }

    cost = budget - remain ;

    cout << "Total cost $" << cost << endl ;
    cout << "Total Remain $" << remain << endl ;

    return 0;
}

