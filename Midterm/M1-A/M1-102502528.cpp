#include<iostream>
using namespace std;

void Buy(double&budget,double cost);
void MB(double&budget,int card);
void CPU(double&budget,int card);

int main()
{
    double budget,budget2;
    int card,mode;
    cout << "Budget?: ";
    cin >>budget;
    while(budget<0)
    {
        cout <<"Out of range!"<<endl<<"Budget?: ";
        cin >> budget;
    }
    budget2 = budget;
    cout << "Card?: ";
    cin >> card;
    while(card!=1,0)
    {
        cout <<"Out of range!"<<endl<<"Card?: ";
        cin >> card;
    }

    do
    {
        cout <<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";       //問他要買啥
        cin >> mode;
        switch (mode)
        {
        case 1:
        {
            MB(budget,card);         //呼叫MB
        }
        break;
        case 2:
        {
            CPU(budget,card);           //呼叫CPU
        }
        break;
        case -1:
        {
        }
        break;
        default:
        {
            cout << "Out of range!"<<endl;
        }
        break;
        }
    }
    while(mode!=-1);                        //mode == -1 時,跳出循環
    cout << "Total Cost $"<<budget2 - budget<<endl;  //顯示總cost
    cout << "Total Remain $"<<budget<<endl;      //顯示剩餘
    return 0;
}

void Buy(double&budget,double cost)
{
    if(budget<cost)
    {
        cout << "Money not enough!"<<endl<<"Remain $"<<budget<<endl;  //錢不足時
    }
    else
    {
        budget = budget - cost;                     //錢足夠時,將budget減去cost
        cout << "Remain $"<< budget<<endl;
    }
}
void MB(double&budget,int card)
{
    int buymb;
    cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";         //問他要買啥
    cin >> buymb;
    switch(buymb)
    {
    case 1:                            //買ASUS時執行
    {
        if(card == 1)
        {
            Buy(budget,4250);
        }
        else
        {
            Buy(budget,5000);
        }
    }
    break;
    case 2:                          //買MSI時執行
    {
        if(card == 1)
        {
            Buy(budget,4050);
        }
        else
        {
            Buy(budget,4500);
        }
    }
    break;
    case 3:                      //買GIGABYTE時執行
    {
        Buy(budget,5000);
    }
    break;
    default:
    {
        cout << "Out of range!";
    }
    }
}
void CPU(double&budget,int card)
{
    int buycpu;
    cout << "1)AMD 2)Intel?: ";
    cin >> buycpu;
    switch(buycpu)
    {
    case 1:
    {
        if(card == 1)          //買AMD時執行
        {
            Buy(budget,3900);   //呼叫Buy
        }
        else
        {
            Buy(budget,5200);   //呼叫Buy
        }
    }
    break;
    case 2:              //買Intel時執行
    {
        if(card == 1)
        {
            Buy(budget,3520); //呼叫Buy
        }
        else
        {
            Buy(budget,4400);   //呼叫Buy
        }
    }
    break;
    default:
    {
        cout << "Out of range!";
    }
    }
}

