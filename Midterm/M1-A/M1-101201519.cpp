#include <iostream>
using namespace std;

double Buy(double budget,double cost)//判斷預算與價格哪個比較大
{
    int remain=budget-cost;
    if(budget<cost)
    {
        remain=budget;
        cout << "Money not enough!\n";
        cout << "Remain $" << remain << endl;
    }
    else
        cout << "Remain $" << remain << endl;
    return remain;
}
double MB(double budget,int card)//MB函式
{
    int input=0,cost=0;
    while(input!=1 && input!=2 && input!=3)//判斷input是否符合範圍
    {
        cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> input;
        if(input!=1 && input!=2 && input!=3)
            cout << "Out of range!\n";
    }
    switch(input)//input的種類
    {
    case 1:
    {
        if(card=0)
        {
            cost=5000;
        }
        if(card=1)
        {
            cost=5000*0.85;
        }
        break;
    }
    case 2:
    {
        if(card=0)
        {
            cost=4500;
        }
        if(card=1)
        {
            cost=4500*0.9;
        }
        break;
    }
    case 3:
    {
        cost=5000;
        break;
    }
    }
    double Money=Buy(budget,cost);
    return budget-cost;
}
double CPU(double budget,int card)//CPU函式
{
    {
        int input=0,cost=0;
        while(input!=1 || input!=2)//判斷input是否符合範圍
        {
            cout << "1)AMD, 2)Intel?: ";
            cin >> input;
            if(input!=1 || input!=2)
                cout << "Out of range!\n";
        }
        switch(input)
        {
        case 1:
        {
            if(card=0)
            {
                cost=5200;
                break;
            }
            if(card=1)
            {
                cost=5200*0.75;
                break;
            }
        }
        case 2:
        {
            if(card=0)
            {
                cost=4400;
                break;
            }
            if(card=1)
            {
                cost=4400*0.8;
                break;
            }
        }
        }
        double Money=Buy(budget,cost);
        return budget-cost;
    }
}
int main()//主函式
{
    int budget=0,card,input=0,remain;
    while(budget<=0)
    {
        cout << "Budget?: ";
        cin >> budget;
        if(budget<=0)
            cout << "Out of range!\n";
    }
    cout << "Card?: ";
    cin >> card;
    while(card!=1 && card!=0)
    {
        cout << "Out of range!\n";
        cout << "Card?: ";
        cin >> card;
    }
    while(input!=-1)
    {
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> input;
        while(input!=1 && input!=2 && input!=-1)
        {
            cout << "Out of range!\n";
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin >> input;
        }
        switch(input)//判斷input
        {
        case 1:
        {
            double BuyMB=MB(budget,card);//導入MB函式
            break;
        }
        case 2:
        {
            double BuyCPU=CPU(budget,card);//導入CPU函式
            break;
        }
        case -1:
            break;
        default:
            cout << "Out of range!\n";
        }
    }
    cout << "Total cost $";
    cout << "Total Remain $";

    return 0;

}
