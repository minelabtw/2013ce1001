#include <iostream>
using namespace std;
//prototypes and functions
void BUY(double &budget, double cost)//輸入目前餘額(budget)以及本次花費金額(cost)
{
    if(budget < cost)//餘額不足發出警告；最後輸出目前餘額
    {
        cout << "Money not enough!\n"
             << "Remain $" << budget << endl;
    }
    else
    {
        budget=budget-cost;
        cout << "Remain $" << budget << endl;
    }
}
void MB(double &budget, int card)
{
    double asus=5000, msi=4500, giga=5000, item=0, cost=0;
    //宣告變數並設定初始值，各項目初始值為其價目、item判斷為哪個商品、cost為此次消費金額
    while(1)//輸入購買項目
    {
        cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> item;
        if(item != 1 && item != 2 && item !=3)
            cout << "Out of range!\n";
        else
            break;
    }
    if(item==1)//判斷有無會員卡，並決定此次花費金額
        cost=(card == 1 ? asus*0.85 : asus);
    else if(item==2)
        cost=(card == 1 ? msi*0.9 : msi);
    else if(item==3)
        cost=giga;
    BUY(budget, cost);
}
void CPU(double &budget, int card)
{
    double amd=5200, intel=4400, item=0, cost=0;
    //宣告變數並設定初始值，各項目初始值為其價目、item判斷為哪個商品、cost為此次消費金額
    while(1)//輸入購買項目
    {
        cout << "1)AMD, 2)Intel?: ";
        cin >> item;
        if(item != 1 && item != 2)
            cout << "Out of range!\n";
        else
            break;
    }
    if(item==1)//判斷有無會員卡，並決定此次花費金額
        cost=(card == 1 ? amd*0.75 : amd);
    else if(item==2)
        cost=(card == 1 ? intel*0.8 : intel);
    BUY(budget, cost);
}
int main()
{
    double budget=0, card=0, check=0, ibudget=0, cost=0;
    //宣告變數並設定初始值，budget為目前金額、card會員卡、check判斷購買物品類型、ibudget初始金額、cost總花費
    while(1)//輸入總金額
    {
        cout << "Budget?: ";
        cin >> budget;
        if(budget <= 0)
            cout << "Out of range!\n";
        else
            break;
    }
    while(1)//輸入有無會員卡
    {
        cout << "Card?: ";
        cin >> card;
        if(card != 0 && card != 1)
            cout << "Out of range!\n";
        else
            break;
    }
    ibudget=budget;
    while(1)//持續進行購買
    {
        while(1)//輸入購買物品類型
        {
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin >> check;
            if(check != 1 && check != 2 && check != -1)
                cout << "Out of range!\n";
            else
                break;
        }
        if(check == -1)//結束購買
            break;
        else if(check == 1)//購買MB
            MB(budget, card);
        else if(check == 2)//購買CPU
            CPU(budget, card);
    }
    cost=ibudget-budget;//計算總花費
    cout << "Total cost $" << cost << endl;
    cout << "Total Remain $" << budget << endl;
    return 0;
}
