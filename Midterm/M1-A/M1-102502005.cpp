#include<iostream>
using namespace std;

void Buy(double & budget, double cost);         //函數prototype
void MB(double & budget, int card);
void CPU(double & budget, int card);

int main()
{
    int card,status=0,mode=0,mbbrand,cpubrand;  //宣告所需變數
    double budget,budgetroot;

    cout << "Budget?: ";                        //要求使用者輸入選項，若選項不符則重新輸入。
    cin >> budget;

    while (budget<=0)
    {
        cout <<"Out of range!"<<endl<<"Budget?: ";
        cin >> budget;
    }
    budgetroot=budget;

    cout << "Card?: ";
    cin >> card;

    while (card !=1 && card !=0)
    {
        cout <<"Out of range!"<<endl<<"Card?: ";
        cin >> card;
    }

    do
    {
        cout <<"1)Buy MB, 2)Buy CPU, -1)Checlout?: ";
        cin >> mode;

        while (mode!=1 && mode!=2 && mode!=-1)
        {
            cout <<"Out of range!"<<endl;
            cout <<"1)Buy MB, 2)Buy CPU, -1)Checlout?: ";
            cin >> mode;
        }

        switch (mode)                              //利用switch case來購買商品，若要結帳則改變status的值以退出do while迴圈。
        {

        case 1:
            MB (budget,card);
            break;

        case 2:
            CPU (budget,card);
            break;

        case -1:
            status=-1;
            break;

        default:
            break;
        }
    }
    while (status==0);

    cout <<"Total cost $"<<budgetroot-budget<<endl;              //利用一開始的預算減去最後的預算得知商品價值。
    cout <<"Total Remain $"<<budget<<endl;                       //印出最後剩下多少預算。

    return 0;
}

void Buy(double & budget, double cost)                          //Buy函式
{
    if (budget<cost)
    {
        cout <<"Money not enough!"<<endl;
        cout <<"Remain $"<<budget<<endl;
    }

    else if (budget>=cost)
    {
        budget=budget-cost;
        cout <<"Remain $"<<budget<<endl;
    }
}

void MB(double & budget, int card)                              //MB函式
{
    int mbbrand;
    double cost;

    cout <<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin >> mbbrand;

    while (mbbrand!=1 && mbbrand!=2 && mbbrand!=3)
    {
        cout <<"Out of range!"<<endl;
        cout <<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin >> mbbrand;
    }

    switch (mbbrand)
    {
    case 1:
        if(card==1)
        {
            cost=5000*0.85;
            Buy(budget,cost);
        }

        else if(card==0)
        {
            cost=5000;
            Buy(budget,cost);
        }
        break;

    case 2:
        if(card==1)
        {
            cost=4500*0.9;
            Buy(budget,cost);
        }

        else if(card==0)
        {
            cost=4500;
            Buy(budget,cost);
        }
        break;

    case 3:
        cost=5000;
        Buy(budget,cost);
        break;

    default:
        break;
    }
}

void CPU(double & budget, int card)                           //CPU函式
{
    int cpubrand;
    double cost;

    cout <<"1)AMD, 2)Intel?: ";
    cin >> cpubrand;

    while (cpubrand!=1 && cpubrand!=2)
    {
        cout <<"Out of range!"<<endl;
        cout <<"1)AMD, 2)Intel?: ";
        cin >> cpubrand;
    }

    switch (cpubrand)
    {
    case 1:
        if (card==1)
        {
            cost=5200*0.75;
            Buy (budget,cost);
        }

        else if (card==0)
        {
            cost=5200;
            Buy (budget,cost);
        }
        break;

    case 2:
        if (card==1)
        {
            cost=4400*0.8;
            Buy (budget,cost);
        }

        else if (card==0)
        {
            cost=4400;
            Buy (budget,cost);
        }
        break;
    default:
        break;
    }
}
