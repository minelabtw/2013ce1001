#include <iostream>

using namespace std;

double Buy(double &budget,double cost,int &card);
double MB(double &budget,int &card,int PC,double cost);
double CPU(double &budget,int &card,int C,double cost);

int main()
{
    double budget;
    double cost;
    int card;

    cout<<"Budget?: ";
    cin>>budget;
    while(budget<=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Budget?: ";
        cin>>budget;
    }

    cout<<"Card?: ";
    cin>>card;
    while(card!=1&&card!=0)
    {
        cout<<"Out of range!"<<endl;
        cout<<"Card?: ";
        cin>>card;
    }

    Buy(budget,cost,card);

    return 0;
}

double Buy(double &budget,double cost,int &card)
{
    double MB(budget);
    double CPU(budget);

    while(cost>budget)
    {
        cout<<"Money not enough!"<<endl;

        if(cost>budget)
        {
            cout<<"Remain $"<<budget;
        }
        else
        {
            cout<<"Remain $"<<budget-cost<<endl;
        }
    }
    return budget-cost;
}

double MB(double budget,int card,int PC,double cost)
{
    cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
    cin>>PC;

    if(card==0)
    {
        switch (PC)
        {
        case 1:
            cost=5000;
            break;
        case 2:
            cost=4500;
            break;
        case 3:
            cost=5000;
            break;
        default:
            break;
        }
    }

    else if(card==1)
    {
        switch (PC)
        {
        case 1:
            cost=4250;
            break;
        case 2:
            cost=4050;
            break;
        case 3:
            cost=5000;
            break;
        default:
            break;
        }

    }
    return budget-cost;
}

double CPU(double budget,int card,int C,double cost)
{
    cout<<"1)AMD, 2)Intel?: ";
    cin>>C;

    if(card==0)
    {
        switch (C)
        {
        case 1:
            cost=-5200;
            break;
        case 2:
            cost=-4400;
            break;
        default:
            break;
        }
    }

    else if(card==1)
    {
        switch (C)
        {
        case 1:
            cost=-3900;
            break;
        case 2:
            cost=-3520;
            break;
        default:
            break;
        }
    }
    return budget-cost;
}
