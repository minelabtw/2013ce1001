#include <iostream>
using namespace std;

int budget=0;//宣告變數
int money=0;
int total=0;
int card=0;
int buy=0;
int mb=0;
int cpu=0;
int MB();
int CPU();

int main()
{
cout << "Budget?: ";//輸出字串
cin >> budget;//輸入預算

while ( budget <= 0 )//設迴圈使預算大於零
{
    cout << "Out of range!" << endl;
    cout << "Budget?: ";
    cin >> budget;
}

cout << "Card?: ";
cin >> card;//輸入是否有卡

while ( card != 1 && card != 0 )//設迴圈使卡的變數為1或0
{
    cout << "Out of range!" << endl;
    cout << "Card?: ";
    cin >> card;
}

cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";//輸出要買的類別
cin >> buy;

while ( buy!=1 && buy!=2 && buy!=-1 )//設迴圈使範圍為1或2或-1
{
    cout << "Out of range!" << endl;
    cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
    cin >> buy;
}

while ( buy!=-1 )//使得輸入-1時會跳出迴圈
{
    switch(buy)
    {
    case 1://當類別為一時
    {
        cout << MB();//呼叫函式1
        break;
    }

    case 2:
    {
        cout << CPU();//呼叫函式2
        break;
    }

    case -1:
        break;

    default:
        break;
    }
}

cout << "Total cost $" << total << endl;//輸出總花費金額，並結束程式
cout << "Total Remain $" << budget;//輸出剩餘金額

return 0;//返回初始值
}

int MB()//定義函式1
{
    cout << "1)ASUS, 2)MSI, 3)GIGABITE?: ";
    cin >> mb;

    while ( mb!=1 && mb!=2 && mb!=3 )//設迴圈使範圍為1或2或3
    {
        cout << "Out of range!" << endl;
        cout << "1)ASUS, 2)MSI, 3)GIGABITE?: ";
        cin >> mb;
    }

    if ( mb==1 && card==1 )//利用if來計算花費金額
    {
        money = 5000*0.85;
    }
    else if ( mb==1 && card==0 )
    {
        money = 5000;
    }
    else if ( mb==2 && card==1 )
    {
        money = 4500*0.9;
    }
    else if ( mb==2 && card==0 )
    {
        money = 4500;
    }
    else if ( mb==3 )
    {
        money = 5000;
    }

    if ( budget < money )//利用if來判斷是否超出預算
    {
        cout << "Money not enough!" << endl;
        cout << "Remain $" << budget << endl;
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> buy;
    }
    else if ( budget >= money )
    {
        budget = budget-money;
        cout << "Remain $" << budget << endl;
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> buy;
        total = total + money;//當購買成功時，才會把金額加入總金額中
    }
}

int CPU()//定義函式2
{
    cout << "1)MDA, 2)Intel?: ";//輸出字串
    cin >> cpu;

    while ( cpu!=1 && cpu!=2 )
    {
        cout << "Out of range!" << endl;
        cout << "1)MDA, 2)Intel?: ";
        cin >> cpu;
    }

    if ( cpu==1 && card==1 )//利用if來計算花費金額
    {
        money = 5200*0.75;
    }
    if ( cpu==1 && card==0 )
    {
        money = 5200;
    }
    if ( cpu==2 && card==1 )
    {
        money = 4400*0.8;
    }
    if ( cpu==2 && card==0 )
    {
        money = 4400;
    }

    if ( budget < money )
    {

        cout << "Remain $" << budget << endl;
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> buy;
    }
    else if ( budget >= money )//假如花費金額在預算範圍內，才會從預算中扣除花費
    {
        budget = budget-money;
        cout << "Remain $" << budget << endl;
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> buy;
        total = total + money;//當購買成功時，才會把金額加入總金額中
    }
}

