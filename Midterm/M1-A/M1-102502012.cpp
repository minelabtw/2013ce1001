#include<iostream>
using namespace std;
void Buy(double &budget,double cost)
{
    if( budget<cost ) // determine if it is over budget
        cout<<"Money not enough!"<<endl;
    else
        budget-=cost;
    cout<<"Remain $"<<budget<<endl;
}
void MB(double &budget,int card)
{
    int num;
    do
    {
        cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
        cin>>num;
    }
    while( (num>3||num<1)&&cout<<"Out of range!"<<endl );
    switch( num )
    {
    case 1:
        Buy(budget,card==1?5000*0.85:5000);
        break;
    case 2:
        Buy(budget,card==1?4500*0.9:4500);
        break;
    case 3:
        Buy(budget,5000);
        break;
    }
}
void CPU(double &budget,int card)
{
    int num;
    do
    {
        cout<<"1)AMD, 2)Intel?: ";
        cin>>num;
    }
    while( (num>2||num<1)&&cout<<"Out of range!"<<endl );
    switch( num )
    {
    case 1:
        Buy(budget,card==1?5200*0.75:5200);
        break;
    case 2:
        Buy(budget,card==1?4400*0.8:4400);
        break;
    }
}
int main()
{
    double budget,tmp;
    int card;
    int mode;
    do
    {
        cout<<"Budget?: ";
        cin>>budget;
    }
    while( budget<=0&&cout<<"Out of range!"<<endl );
    do
    {
        cout<<"Card?: ";
        cin>>card;
    }
    while( (card<0||card>1)&&cout<<"Out of range!"<<endl );

    tmp=budget; // use tmp to calculate
    while( true )
    {
        do
        {
            cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin>>mode;
        }
        while( !(mode==1||mode==2||mode==-1)&&cout<<"Out of range!"<<endl );
        switch( mode )
        {
        case 1:
            MB(tmp,card);
            break;
        case 2:
            CPU(tmp,card);
            break;
        case -1:
            break;
        }
        if( mode==-1 ) // terminate
            break;
    }
    cout<<"Total cost $"<<budget-tmp<<endl; // total-left_budget
    cout<<"Total Remain $"<<tmp<<endl; // left_budget
    return 0;
}
