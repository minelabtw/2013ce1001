#include <iostream>

using namespace std;

double Buy(double budget,double cost);
double MB(double budget,int card);
double CPU(double budget,int card);

//判斷餘額是否足夠並回傳購買後餘額
double Buy(double budget,double cost)
{
    double remain=0;
    remain=budget-cost;

    if(remain<0)
    {
        remain= budget;
        cout<< "Money not enough!"<<endl;
    }

    return remain;
}
//印出MB廠牌及呼叫BUY並傳入價格
double MB(double budget,int card)
{
    double MB_ASUS_Price=5000,MB_MSI_Price=4500,MB_GIGABYTE_Price=5000;
    int MBType=0;
    double remain=0;
    //詢問購買MB類型
    do
    {
        cout << "1)ASUS, 2)MSI, 3)GIGABYTE: " ;
        cin>>MBType;
        if(MBType<=0||MBType>3)
            cout << "Out of range!" << endl;
    }
    while(MBType<=0||MBType>3);
    if(card==1)
    {
        if(MBType==1)
            remain=Buy(budget,0.85*MB_ASUS_Price);
        else if(MBType==2)
            remain=Buy(budget,0.9*MB_MSI_Price);
        else if(MBType==3)
            remain=Buy(budget,1*MB_GIGABYTE_Price);
    }
    else
    {
        if(MBType==1)
            remain=Buy(budget,MB_ASUS_Price);
        else if(MBType==2)
            remain=Buy(budget,MB_MSI_Price);
        else if(MBType==3)
            remain=Buy(budget,MB_GIGABYTE_Price);
    }
    cout<<"Remain $"<<remain<<endl;
    return remain;
}
//印出CPU廠牌及呼叫BUY並傳入價格
double CPU(double budget,int card)
{
    double CPU_AMD_Price=5200,CPU_Intel_Price=4400;
    int CPUType=0;
    double remain=0;
//詢問購買CPU類型
    do
    {
        cout << "1)AMD, 2)Intel?: " ;
        cin>>CPUType;
        if(CPUType<=0||CPUType>2)
            cout << "Out of range!" << endl;
    }
    while(CPUType<=0||CPUType>3);
    if(card==1)
    {
        if(CPUType==1)
            remain=Buy(budget,0.75*CPU_AMD_Price);
        else if(CPUType==2)
            remain=Buy(budget,0.8*CPU_Intel_Price);

    }
    else
    {
        if(CPUType==1)
            remain=Buy(budget,CPU_AMD_Price);
        else if(CPUType==2)
            remain=Buy(budget,CPU_Intel_Price);

    }
    cout<<"Remain $"<<remain<<endl;
    return remain;
}

int main()
{
    double budget=0,remain=0;
    int hasCard=0,buyWhat=0,checkout=0;

    //詢問預算
    do
    {
        cout << "Budget?: " ;
        cin>>budget;
        if(budget<=0)
            cout << "Out of range!" << endl;
    }
    while(budget<=0);
    remain=budget;
//詢問是否有會員卡
    do
    {
        cout << "Card?: " ;
        cin>>hasCard;
        if(hasCard<0||hasCard>=2)
            cout << "Out of range!" << endl;
    }
    while(hasCard<0||hasCard>=2);
//詢問購買類型
    while(checkout==0)
    {
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: " ;
        cin>> buyWhat;

        if(buyWhat==1)
        {
            remain=MB(remain,hasCard);
        }
        else if(buyWhat==2)
        {
            remain=CPU(remain,hasCard);
        }//印出總花費及餘額
        else if(buyWhat==-1)
        {
            cout << "Total cost $"<< budget-remain<<endl;
            cout << "Total Remain $" <<remain<<endl;
            checkout++;
        }
        else
        {
            cout << "Out of range!" << endl;
        }
    }

return 0;
}
