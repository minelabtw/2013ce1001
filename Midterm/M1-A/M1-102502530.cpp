#include<iostream>

void Buy(double &budget,double cost);  //函數原型
void MB(double &budget,int card);
void CPU(double &budget,int card);

int main()
{
   double budget,defaultBudget;  //宣告
   int card;

   do    //輸入預算
   {
      std::cout<<"Budget?: ";
      std::cin>>budget;
      if(budget<=0)
         std::cout<<"Out of range!\n";
   }while(budget<=0);
   defaultBudget=budget;

   do    //輸入有無會員卡
   {
      std::cout<<"Card?: ";
      std::cin>>card;
      if(card!=1&&card!=0)
         std::cout<<"Out of range!\n";
   }while(card!=1&&card!=0);

   for(int mode=0;mode!=-1;)  //如果Checkout則跳出迴圈
   {
      do    //輸入採購類別
      {
         std::cout<<"1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
         std::cin>>mode;
         if(mode!=1&&mode!=2&&mode!=-1)
            std::cout<<"Out of range!\n";
      }while(mode!=1&&mode!=2&&mode!=-1);

      if(mode==1)    //採購主機板
         MB(budget,card);
      else if(mode==2)  //採購中央處理器
         CPU(budget,card);
   }

   std::cout<<"Total cost $"<<defaultBudget-budget<<'\n';   //輸出總結
   std::cout<<"Total Remain $"<<budget<<'\n';
}
void Buy(double &budget,double cost)   //購買
{
   if(cost>budget)   //預算不足
      std::cout<<"Money not enough!\n";
   else
      budget-=cost;
   std::cout<<"Remain $"<<budget<<'\n';   //輸出餘額
}
void MB(double &budget,int card)    //採購主機板
{
   int num;    //宣告
   double cost[3]={5000,4500,5000};
   double discount[3]={0.85,0.9,1};

   do    //輸入購買廠牌
   {
      std::cout<<"1)ASUS, 2)MSI, 3)GIGABYTE?: ";
      std::cin>>num;
      if(num!=1&&num!=2&&num!=3)
         std::cout<<"Out of range!\n";
   }while(num!=1&&num!=2&&num!=3);

   Buy(budget,cost[num-1]*(card==1?discount[num-1]:1));  //購買
}
void CPU(double &budget,int card)   //採購中央處理器
{
   int num;    //宣告
   double cost[2]={5200,4400};
   double discount[2]={0.75,0.8};

   do    //輸入購買廠牌
   {
      std::cout<<"1)AMD, 2)Intel?: ";
      std::cin>>num;
      if(num!=1&&num!=2)
         std::cout<<"Out of range!\n";
   }while(num!=1&&num!=2);

   Buy(budget,cost[num-1]*(card==1?discount[num-1]:1));  //購買
}
