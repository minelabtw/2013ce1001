#include <iostream>

using namespace std;

int main()
{
    double budgetInput = 0; //使用者輸入的預算
    int cardOrNot;          //1表示使用者有黃色鬼屋會員卡，0表示他沒有
    int userBuy;            //表示使用者選擇買MB或是CPU或是結帳
    double eachCost , totalCost = 0; //eachCost表示每一筆消費；totalCost表示總共花了多少錢

    void Buy ( double & budget, double &cost);  //&budget表總預算（或是目前剩餘的錢），&cost表每一筆消費的金額（避免計算錯誤，這裡使用了refrence）
    void MB ( double & budget, int card);       //budget表目前剩餘的錢，card表有沒有卡
    void CPU ( double & budget, int card);      //budget表目前剩餘的錢，card表有沒有卡

    do
    {
        cout << "Budget?: ";
        cin >> budgetInput;
        if ( budgetInput <= 0)
        {
            cout << "Out of range!" << endl ;
        }
    }
    while ( budgetInput <= 0 );         //讓使用者輸入預算並判斷是否符合規則

    do
    {
        cout << "Card?: ";
        cin >> cardOrNot;
        if ( !( cardOrNot == 0 || cardOrNot == 1))
        {
            cout << "Out of range!" << endl ;
        }
    }
    while ( !( cardOrNot == 0 || cardOrNot == 1));//問使用者有沒有卡並判斷是否符合規則

    while ( true )      //無限迴圈
    {
        do
        {
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin >> userBuy;
            if (!(userBuy==1 || userBuy == 2 || userBuy == -1))
                cout << "Out of range!" << endl;
        }
        while (!(userBuy == 1 || userBuy == 2 || userBuy == -1));//每次都問使用者要買哪種東西，或是結帳。並判斷是否符合規則

        switch (userBuy)
        {
        case 1:
            MB( eachCost , cardOrNot);
            break;
        case 2:
            CPU( eachCost , cardOrNot);
            break;
        case -1:
            cout << "Total cost $" << totalCost << endl << "Total Remain $" << budgetInput; //印出總花費、剩餘
            return 0;       //並結束程式
        }
        Buy ( budgetInput,  eachCost);  //每次選完一個商品要買，便會呼叫Buy函式
        totalCost += eachCost;          //並把目前花費累加到總花費
    }
}

void Buy ( double & budget, double &cost)
{
    if (cost > budget)
    {
        cout << "Money not enough!" << endl ;   //首先判斷所剩的錢夠不夠買當前的商品
        cost = 0;                               //若不夠，則表示不夠，並把當前購買的金額歸零，以免統計錯誤，所以這裡要使用&cost（總花費）（參見第62行）
    }
    else
        budget -= cost;         //若夠，則從總預算中扣除目前花費
    cout << "Remain $" << budget << endl;   //不論夠不夠都要印出目前剩餘的金額
}

void MB ( double & budget, int card)    //買主機版
{
    int choose;
    do
    {
        cout << "1)ASUS, 2)MSI, 3)GIGBYTE?: ";
        cin >> choose;
        if (!(choose == 1 || choose == 2 || choose == 3 ))
            cout << "Out of range!" << endl ;
    }
    while (!(choose == 1 || choose == 2 || choose == 3 ));  //問使用者所欲購買主機版的品牌，並判斷輸入是否合理

    switch (choose)
    {
    case 1:
        if (card ==1)
            budget = 5000*0.85;
        else
            budget = 5000;        //有卡打折，沒卡沒折
        break;
    case 2:
        if (card ==1)
            budget = 4500*0.9;
        else
            budget = 4500;        //有卡打折，沒卡沒折
        break;
    case 3:
        budget =5000;             //就算有卡也不給折扣的技嘉主機版
        break;
    }
}

void CPU ( double & budget, int card)       //買CPU
{
    int choose;
    do
    {
        cout << "1)AMD, 2)Intel?: ";
        cin >> choose;
        if (!(choose == 1 || choose == 2))
            cout << "Out of range!" << endl ;
    }
    while (!(choose == 1 || choose == 2));  //看使用者要買哪牌的，並判斷輸入是否合理

    switch (choose)
    {
    case 1:
        if (card ==1)
            budget = 5200*0.75;
        else
            budget = 5200;        //有卡打折，沒卡沒折
        break;
    case 2:
        if (card ==1)
            budget = 4400*0.8;
        else
            budget = 4400;        //有卡打折，沒卡沒折
        break;
    }
}
