#include <iostream>

using namespace std;

double Buy(double budget,double cost)          //宣告函式Buy
{
    if(budget<cost)
        cout<< "Money not enough!"<<endl
            << "Remain $"<<budget<<endl;
    else
    {
        budget=budget-cost;
        cout<< "Remain $"<<budget<<endl;
    }

}
void MB(double  budget,int card)         //宣告函式MB
{
    int b=0;
    int money=budget;
    int cost=0;
    do
    {
        cout << "1)ASUS, 2)MSI, 3)GTGABYTE?: ";
        cin >> b;
        switch(b)
        {
        case 1:
        {
            if(card==0)
                cost=5000;
            else
                cost=4250;
            Buy(budget,cost);
            break;
        }
        case 2:
        {
            if(card==0)
                cost=4500;
            else
                cost=4050;
            Buy(budget,cost);
            break;
        }
        case 3:
        {
            cost=5000;
            Buy(budget,cost);
            break;
        }
        default:
        {
            cout << "Out of range!" << endl;
            break;
        }

        }
    }
    while(b!=1 &&b!=2 && b!=3);
}
void CPU(double  budget,int card)           //宣告函式CPU
{
    int c=0;
    int money=budget;
    int cost=0;
    do
    {
        cout << "1)AMD, 2)Intel?: ";
        cin >> c;
        switch(c)
        {
        case 1:
        {
            if(card==0)
                cost=5200;
            else
                cost=3900;
            Buy(budget,cost);
            break;
        }
        case 2:
        {
            if(card==0)
                cost=4400;
            else
                cost=3520;
            Buy(budget,cost);
            break;
        }
        default:
        {
            cout << "Out of range!" << endl;
            break;
        }
        }
    }
    while(c!=1 &&c!=2);
}


int main()
{
    int budget=0;
    int money=budget;
    int card=0;
    int i=0;
    do                                //do while迴圈
    {
        cout << "Budget?: ";
        cin >> budget;
        if(budget<=0)
            cout << "Out of range!" << endl;
    }
    while(budget<=0);
    do
    {
        cout << "Card?: ";
        cin >> card;
        if(card!=1 && card!=0)
            cout << "Out of range!" << endl;
    }
    while(card!=1 && card!=0);
    do
    {
        cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
        cin >> i;
        switch(i)                            //用switch來判斷用哪一個方案
        {
        case 1:
        {
            MB(budget,card);
            break;
        }
        case 2:
        {
            CPU( budget,card);
            break;
        }
        case -1:
        {
            cout << "Total cast $" << money << endl
                 << "Total Remain $" << budget;
            break;
        }
        default:
        {
            cout << "Out of range!" << endl;
            break;
        }

        }

    }
    while(i!=-1);

    return 0;
}

