#include <iostream>

using namespace std;

double Buy(double budget, double cost) //宣告函式
{

}

double MB(double budget, int card, int choose_MB) //宣告函式
{
    int cost = 0;
    {
        if (choose_MB==1 and card==0)
            cost = 5000;
        else if (choose_MB==1 and card==1)
            cost = 4250;
        else if (choose_MB==2 and card==0)
            cost = 4500;
        else if (choose_MB==2 and card==1)
            cost = 4250;
        else if (choose_MB==3)
            cost = 5000;
    }

    if (budget-cost<0)
        cout << "Money not enough!" << endl;
    else
        budget=budget-cost;

    return budget;
}

double CPU(double budget, int card, int choose_CPU) //宣告函式

{
    int cost = 0;

    {
        if (choose_CPU==1 and card==0)
            cost = 5200;
        else if (choose_CPU==1 and card==1)
            cost = 2900;
        else if (choose_CPU==2 and card==0)
            cost = 4400;
        else if (choose_CPU==2 and card==1)
            cost = 3520;
    }

    if (budget-cost<0)
        cout << "Money not enough!" << endl;
    else
        budget=budget-cost;

    return budget;
}


int main()
{
    int budget = 0;
    int card = 0;
    int choice = 0;
    int choose_MB = 0;
    int choose_CPU = 0;

    do
    {
        cout << "Budget?: ";
        cin >> budget;
        if (budget<=0)
            cout << "Out of range!" << endl;
    }
    while (budget<=0);

    do
    {
        cout << "Card?: ";
        cin >> card;
        if (card!=0 and card!=1)
            cout << "Out of range!" << endl;
    }
    while (card!=0 and card!=1);

    do
    {
        cout << "Remain $" << budget << endl;

        do
        {
            cout << "1)Buy MB, 2)Buy CPU, -1)Checkout?: ";
            cin >> choice;
            if (choice!=1 and choice!=2 and choice!=-1)
                cout << "Out of range!" << endl;
        }
        while (choice!=1 and choice!=2 and choice!=-1);

        switch (choice)
        {
        case 1:
            do
            {
                cout << "1)ASUS, 2)MSI, 3)GIGABYTE?: ";
                cin >> choose_MB;
                if (choose_MB!=1 and choose_MB!=2 and choose_MB!=3)
                    cout << "Out of range!" << endl;
            }
            while (choose_MB!=1 and choose_MB!=2 and choose_MB!=3);
            MB(budget,card,choose_MB);
            break;

        case 2:
            do
            {
                cout << "1)AMD, 2)Intel?: ";
                cin >> choose_CPU;
                if (choose_CPU!=1 and choose_CPU!=2)
                    cout << "Out of range!" << endl;
            }
            while (choose_CPU!=1 and choose_CPU!=2);
            CPU(budget,card,choose_CPU);
            break;
        }
    }
    while (choice==1 or choice==2);

    cout << "Total cost $" << budget << endl;
    cout << "Total Remain $" << budget;


    return 0;
}

