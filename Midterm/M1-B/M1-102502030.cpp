#include <iostream>
using namespace std;
void buy( int , int , int , int , double & , int );
main()
{
    int idcard;  //設變數
    int chopsticks;
    int buytype;
    int choose;
    double total;
    int money;
    int sum=0;

    do  //會員資格
    {
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> idcard;
    }
    while( idcard!=0 && idcard!=1 && cout << "Out of range!\n" );
    do  //測餐具
    {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chopsticks;
    }
    while( chopsticks!=0 && chopsticks!=1 && cout << "Out of range!\n" );

    do  //選-1跳出
    {
        do
        {
            cout << "1. Chicken boxed meal: 80\n";  //輸出菜單
            cout << "2. Pork boxed meal: 70\n";
            cout << "3. Vegetable boxed: 60\n";
            cout << "4. Meatball soup: 30\n";
            cout << "5. Fish soup: 20\n";
            cout << "Buy type: ";
            cin >> buytype;
        }
        while( buytype!=1 && buytype!=2 && buytype!=3 && buytype!=4 && buytype!=5 && buytype!=-1 && cout << "Out of range!\n");
        switch( buytype )
        {
        case 1:
            money=80;  //設價錢
            do
            {
                cout << "Number: ";
                cin >> choose;
            }
            while( choose<1 && cout << "Out of range!");
            buy ( idcard , chopsticks , money , choose , total , sum );  //計算金額
            cout << "Total: " << total << endl;
            sum=sum+money*choose;  //總計
            break;
        case 2:
            money=70;
            do
            {
                cout << "Number: ";
                cin >> choose;
            }
            while( choose<1 && cout << "Out of range!");
            buy ( idcard , chopsticks , money , choose , total , sum );
            cout << "Total: " << total << endl;
            sum=sum+money*choose;
            break;
        case 3:
            money=60;
            do
            {
                cout << "Number: ";
                cin >> choose;
            }
            while( choose<1 && cout << "Out of range!");
            buy ( idcard , chopsticks , money , choose , total , sum );
            cout << "Total: " << total << endl;
            sum=sum+money*choose;
            break;
        case 4:
            money=30;
            do
            {
                cout << "Number: ";
                cin >> choose;
            }
            while( choose<1 && cout << "Out of range!");
            buy ( idcard , chopsticks , money , choose , total , sum);
            cout << "Total: " << total << endl;
            sum=sum+money*choose;
            break;
        case 5:
            money=20;
            do
            {
                cout << "Number: ";
                cin >> choose;
            }
            while( choose<1 && cout << "Out of range!");
            buy ( idcard , chopsticks , money , choose , total , sum );
            cout << "Total: " << total << endl;
            sum=sum+money*choose;
            break;
        case -1:
            break;
        }
    }
    while( buytype!=-1 );
    cout << "Total money: " << total;  //輸出總金額

    return 0;
}
void buy( int id , int tool , int price , int num , double &total , int sum )  //計算金額用函數
{
    if( id==1 )  //有卡
        total=(price*num+sum)*9/10;
    else  //沒卡
        total=price*num+sum;
    if( tool==1 )  //有餐具
        total=total-5*num;
}
