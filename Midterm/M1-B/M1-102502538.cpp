#include<iostream>
using namespace std;

int main()
{
    int card,chopsticks;       //定義變數
    int type,number;
    int total=0;

    void buy(int card,int chopsticks,int type,int number,int &total);     //定義函式
    {
        if (type==1)
            total=card*0.9*(80*number)-chopsticks*5*number+total;
        if (type==2)
            total=card*0.9*(70*number)-chopsticks*5*number+total;
        if (type==3)
            total=card*0.9*(60*number)-chopsticks*5*number+total;
        if (type==4)
            total=card*0.9*(30*number)+0*chopsticks+total;
        else
            total=card*0.9*(20*number)+0*chopsticks+total;
    }

    cout <<"Enter if you have id card 0=no 1=yes : ";
    cin >>card;
    while (card<0||card>1)   //判定card
    {
        cout <<"Out of range!"<<endl;
        cout <<"Enter if you have id card 0=no 1=yes : ";
        cin >>card;
    }
    cout <<"Enter if you have chopsticks 0=no 1=yes : ";
    cin >>chopsticks;
    while (chopsticks<0||chopsticks>1) //判定chopsticks
    {
        cout <<"Out of range!"<<endl;
        cout <<"Enter if you have id chopsticks 0=no 1=yes : ";
        cin >>chopsticks;
    }
    while (type!=-1)         //設定-1為終止
    {
        cout <<"1. Chicken boxed meal: 80"<<endl;
        cout <<"2. Pork boxed meal: 70"<<endl;
        cout <<"3. Vegetable boxed meal: 60"<<endl;
        cout <<"4. Meatball soup: 30"<<endl;
        cout <<"5. Fish soup: 20"<<endl;
        cout <<"Buy type: ";
        cin >>type;
        while (type==0||type>5 ||type<-1)//判定type
        {
            cout <<"Out of range!"<<endl;
            cout <<"Buy type: ";
            cin >>type;
        }

        switch (type)       //使用switch case
        {
        case 1:
            cout <<"Number: ";   //判定number
            cin >>number;
            while (number<=0)
            {
                cout<<"Out of range!"<<endl;
                cout <<"Number: ";
                cin >>number;
            }
            cout <<"Total: "<<total<<endl;
            break;

        case 2:
            cout <<"Number: ";
            cin >>number;
            while (number<=0)
            {
                cout<<"Out of range!"<<endl;
                cout <<"Number: ";
                cin >>number;
            }

            cout <<"Total: "<<total<<endl;
            break;
        case 3:
            cout <<"Number: ";
            cin >>number;
            while (number<=0)
            {
                cout<<"Out of range!"<<endl;
                cout <<"Number: ";
                cin >>number;
            }

            cout <<"Total: "<<total<<endl;
            break;

        case 4:
            cout <<"Number: ";
            cin >>number;
            while (number<=0)
            {
                cout<<"Out of range!"<<endl;
                cout <<"Number: ";
                cin >>number;
            }

            cout <<"Total: "<<total<<endl;
            break;

        case 5:
            cout <<"Number: ";
            cin >>number;
            while (number<=0)
            {
                cout<<"Out of range!"<<endl;
                cout <<"Number: ";
                cin >>number;
            }

            cout <<"Total: "<<total<<endl;
            break;
        }
    }

    cout <<"Total money: "<<total;

    return 0;
}
