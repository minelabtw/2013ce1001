#include <iostream>
using namespace std;

int idcard, chopsticks;						//declare idcard and chopsticks as globals
int normalprice[]={80,70,60,30,20};			//the price table
void Buy(int,int,int&);						//prototype

int main()
{
	while(1){
		cout << "Enter if you have id card 0=no 1=yes : "; cin >> idcard;			//input idcard
		if(idcard!=0 && idcard!=1){cout << "Out of range!\n"; continue;}
		break;
	}

	while(1){
		cout << "Enter if you have chopsticks 0=no 1=yes : "; cin >> chopsticks;	//input chopsticks
		if(chopsticks!=0 && chopsticks!=1){cout << "Out of range!\n"; continue;}
		break;
	}

	int total=0;		//set initial total to 0
	while(1){
		//print the menu
		cout << "1. Chicken boxed meal: 80\n"
			<< "2. Pork boxed meal: 70\n"
			<< "3. Vegetable boxed meal: 60\n"
			<< "4. Meatball soup: 30\n"
			<< "5. Fish soup: 20\n";

		int buytype, number;

		while(1){
			cout << "Buy type: "; cin >> buytype;		//input buytype
			if(buytype!=-1 && (buytype<1 || buytype>5)){cout << "Out of range!\n"; continue;}
			break;
		}
		if(buytype==-1){break;}							//end loop if buytype==-1

		while(1){
			cout << "Number: "; cin >> number;			//input number
			if(number<1){cout << "Out of range!\n"; continue;}
			break;
		}

		Buy(buytype, number, total);					//Buy function: add to total
		cout << "Total: " << total << "\n";				//print total
	}

	cout << "Total money: " << total << "\n";			//print ALL total

	return 0;
}

void Buy(int type, int amount, int &total)		//call by reference
{
	int price=normalprice[type-1];				//get the price of type
	if(idcard){price*=0.9;}						//idcard discount
	if(chopsticks && type<4){price-=5;}			//chopsticks discount
	total+=price*amount;						//add to total
}
