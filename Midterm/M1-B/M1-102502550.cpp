#include <iostream>

using namespace std;

void Buy(int,int,int,int,int&);                                                        //宣告函數原形

int main()
{
    int type=0,typemoney=0,number=0,idcard=0,chopstick=0,sum=0,flag=1;                 //宣告變數

    do{
        cout<<"Enter if you have id card 0=no 1=yes : ";                               //持續要求使用者輸入直到在正確範圍內
        cin>>idcard;
    }while((idcard!=1 && idcard!=0) && cout<<"Out of range!\n");
    do{
        cout<<"Enter if you have chopsticks 0=no 1=yes : ";
        cin>>chopstick;
    }while((chopstick!=1 && chopstick!=0) && cout<<"Out of range!\n");
  while(flag)                                                                          //當flag=1繼續
  {
    cout<<"1. Chicken boxed meal: 80\n";
    cout<<"2. Pork boxed meal: 70\n";
    cout<<"3. Vegetable boxed meal: 60\n";
    cout<<"4. Meatball soup: 30\n";
    cout<<"5. Fish soup: 20\n";
    do{                                                                                //持續要求使用者輸入直到在正確範圍內
        cout<<"Buy type: ";
        cin>>type;
    }while((type==0 || (type<-1 || type>5)) && cout<<"Out of range!\n");
    if(type!=-1)                                                                       //持續要求使用者輸入直到在正確範圍內
    {
       do{
       cout<<"Number: ";
       cin>>number;
       }while((number<=0) && cout<<"Out of range!\n");
    }
    switch(type)                                                                       //判斷買了哪項
    {
       case 1:
          typemoney=80;                                                                //設定價錢
          Buy(typemoney,number,idcard,chopstick,sum);                                  //呼叫函數
          break;                                                                       //離開switch
       case 2:
          typemoney=70;
          Buy(typemoney,number,idcard,chopstick,sum);
          break;
       case 3:
          typemoney=60;
          Buy(typemoney,number,idcard,chopstick,sum);
          break;
       case 4:
          typemoney=30;
          Buy(typemoney,number,idcard,chopstick,sum);
          break;
       case 5:
          typemoney=20;
          Buy(typemoney,number,idcard,chopstick,sum);
          break;
       case -1:
          flag=0;                                                                      //設定flag=0將終止while
          break;

    }
    if(type==-1)
       cout<<"Total money: "<<sum<<endl;                                               //印出結果
    else
       cout<<"Total: "<<sum<<endl;
  }

    return 0;
}
void Buy(int typemoney,int number,int idcard,int chopstick,int &sum)
{
    int mon=0;
    mon=typemoney*number;                                                              //此項的原價
    if(idcard==1)
    {
        mon=mon*0.9;                                                                   //計算各種打折
    }
    if((typemoney==80 || typemoney==70 || typemoney==60) && chopstick==1)
    {
        mon=mon-5*number;
    }
    sum=sum+mon;                                                                       //累加至sum中
}
