#include <iostream>

using namespace std;

void Buy(int type , int number , int number2 , double idcard , int chopsticks , int &total);  //宣告函式

int main(){

    double idcard , idcard2 ;  //宣告學生證變數
    int chopsticks , chopsticks2 , number , number2 , type , type2;  //宣告有關價錢的變數
    int total = 0;  //宣告總和的變數

    do{  //詢問是否有學生證並打折
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> idcard;
        if(idcard > 1 || idcard < 0){
            cout << "Out of range!" << endl;
        }
        if(idcard == 1){
            idcard2 = 0.9;
        }
        if(idcard == 0){
            idcard2 = 1;
        }
    }while(idcard > 1 || idcard < 0);

    do{  //詢問是否有自備筷子並減價
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chopsticks;
        if(chopsticks > 1 || chopsticks < 0){
            cout << "Out of range!" << endl;
        }
        if(chopsticks == 1){
            chopsticks2 = -5;
        }
        if(chopsticks == 0){
            chopsticks2 = 0;
        }
    }while(chopsticks > 1 || chopsticks < 0);

    do{  //詢問要買哪種便當
        cout << "1. Chicken boxed meal: 80" << endl;
        cout << "2. Pork boxed meal: 70" << endl;
        cout << "3. Vegetable boxed meal: 60" << endl;
        cout << "4. Meatball soup: 30" << endl;
        cout << "5. Fish soup: 20" << endl;

        do{
            cout << "Buy type: ";
            cin >> type;
            switch (type){
                case 1:
                    type2 = 80;
                    break;

                case 2:
                    type2 = 70;
                    break;

                case 3:
                    type2 = 60;
                    break;

                case 4:
                    type2 = 30;
                    break;

                case 5:
                    type2 = 20;
                    break;

                case -1:
                    break;

                default:
                    cout << "Out of range!" << endl;
                    break;
            }
        }while(type > 5 || type < -1 || type == 0);

        if(type != -1){  //詢問要買多少數量
            do{
                cout << "Number: ";
                cin >> number;
                if(number < 1){
                    cout << "Out of range!" << endl;
                }
            }while(number < 1);

            switch(type){
                case 1:
                    number2 = number;
                    break;

                case 2:
                    number2 = number;
                    break;

                case 3:
                    number2 = number;
                    break;

                case 4:
                    number2 = 0;
                    break;

                case 5:
                    number2 = 0;
                    break;

                case -1:
                    break;

                default:
                    break;
            }

            Buy(type2 , number , number2 , idcard2 ,chopsticks2 , total);  //算總和

            cout << "Total: " << total << endl;  //輸出目前總和
        }

    }while(type != -1);


    cout << "Total money: " << total;  //輸出總和

    return 0;
}

void Buy(int type , int number , int number2 , double idcard , int chopsticks , int &total){  //買東西函式的詳細內容
    total = total + (type * number * idcard) + number2 * chopsticks;
}
