#include <iostream>
using namespace std;
bool IsBoolOutOfRange(int boolType);
int Buy(int type, int number, int haveID, int haveChopsticks);//計算單一種類的購買金額(回傳版本)
void Buy(int type, int number, int& totalMoney, int haveID, int haveChopsticks);//計算單一種類的購買金額(不回傳版本)

int main(){
    int haveID = 0;//宣告型態為int的是否有ID card，並初始化為0
    int haveChopsticks = 0;//宣告型態為int的是否自備 card，並初始化為0
    int totalMoney = 0;//宣告型態為int的總金額，並初始化為0
    int buyType = 0;//宣告型態為int的購買類型，並初始化為0
    int buyNumber = 0;//宣告型態為int的購買數量，並初始化為0

    while(1){//輸入是否有ID card
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> haveID;
        if(!IsBoolOutOfRange(haveID)){
            break;
        }
    }
    while(1){//輸入是否有自備筷子
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> haveChopsticks;
        if(!IsBoolOutOfRange(haveChopsticks)){
            break;
        }
    }
    //輸入購買類型和數量
    while(buyType != -1){//等於-1迴圈結束
        cout << "1. Chicken boxed meal: 80" << endl \
             << "2. Pork boxed meal: 70" << endl \
             << "3. Vegetable boxed meal: 60" << endl \
             << "4. Meatball soup: 30" << endl \
             << "5. Fish soup: 20" << endl;
        cout << "Buy type: ";
        cin >> buyType;
        switch(buyType){
            case 1://Chicken boxed meal
            case 2://Pork boxed meal
            case 3://Vegetable boxed meal
            case 4://Meatball soup
            case 5://Fish soup
                while(1){
                    cout << "Number: ";
                    cin >> buyNumber;
                    if(buyNumber < 1){//當buyNumber < 1則重新輸入
                        cout << "Out of range!" << endl;
                    }
                    else{
                        break;
                    }
                }
                //totalMoney += Buy(buyType, buyNumber, haveID, haveChopsticks);
                Buy(buyType, buyNumber, totalMoney, haveID, haveChopsticks);
                cout << "Total: " << totalMoney << endl;
                break;
            case -1://結束
                break;
            default:
                cout << "Out of range!" << endl;
                break;
        }
    }
    cout << "Total money: " << totalMoney << endl;

    return 0;
}

bool IsBoolOutOfRange(int boolType){
    if(boolType == 1 || boolType == 0){
        return false;
    }
    else{
        cout << "Out of range!" << endl;
        return true;
    }
}

int Buy(int type, int number, int haveID, int haveChopsticks){
    //以int的購買類型、int的購買數量、int的是否有ID card
    //int的是否自備筷子，計算單一種類的購買金額，並回傳
    //購買金額
    int result;
    switch(type){
        case 1://Chicken boxed meal
            result = number * 80;
            break;
        case 2://Pork boxed meal
            result = number * 70;
            break;
        case 3://Vegetable boxed meal
            result = number * 60;
            break;
        case 4://Meatball soup
            result = number * 30;
            break;
        case 5://Fish soup
            result = number * 20;
            break;
        default://因為在main迴圈已經篩過了所以此條件無用
            break;;
    }
    if(haveID){
        //有ID打9折
        result = (int)((float)result * 0.9);
    }
    if(haveChopsticks && type < 4){
        //有自備筷子每個便當折5塊
        result -= number * 5;
    }
    return result;
}

void Buy(int type, int number, int& totalMoney, int haveID, int haveChopsticks){
    //以int的購買類型、int的購買數量、int&的總金額、int的是
    //否自備筷子、int的是否有ID card為參數，計算單一種類的
    //購買金額，不回傳任何數值
    int result;
    switch(type){
        case 1://Chicken boxed meal
            result = number * 80;
            break;
        case 2://Pork boxed meal
            result = number * 70;
            break;
        case 3://Vegetable boxed meal
            result = number * 60;
            break;
        case 4://Meatball soup
            result = number * 30;
            break;
        case 5://Fish soup
            result = number * 20;
            break;
        default://因為在main迴圈已經篩過了所以此條件無用
            break;;
    }
    if(haveID){
        //有ID打9折
        result = (int)((float)result * 0.9);
    }
    if(haveChopsticks && type < 4){
        //有自備筷子每個便當折5塊
        result -= number * 5;
    }
    //加回總金額
    totalMoney += result;
}
