#include <iostream>

using namespace std;

void Buy(int typenumber,int number,double idcard,int chopsiticks,int &total)
{
    int tnum=0; //timimg
    tnum=typenumber*number;
    if(idcard==1)   //idcard
        tnum*=0.9;
    if(typenumber>=60 && chopsiticks==1)    //boxed and chopsiticks
        tnum-=number*5;
    total+=tnum;
    cout << "Total: "<< total << endl;

};

void cinnumber(int &n)  //input number of food;
{
    while(true)
    {
        cout << "Number: ";
        cin >> n;
        if(n > 0)
            break;
        else
            cout << "Out of range!" << endl;
    }
};

int main()
{
    //Fish soup > 20;
    int type=0,number=-1,total=0;    //food type, food number,total money;
    int peroff=-1,dd=-1;  //10per off & five dollar off
    //
    while(true)
    {
        //have idcard or not;
        cout << "Enter if you have id card 0=no 1=yes :" ;
        cin >> peroff;
        if(peroff!=0 && peroff!=1)
            cout << "Out of range!" << endl;
        else
            break;
    }
    while(true)
    {
        //have -5off or not,
        cout << "Enter if you have chopsticks 0=no 1=yes :" ;
        cin >> dd;
        if(dd!=0 && dd!=1)
            cout << "Out of range!" << endl;
        else
            break;
    }
    while(true)
    {
        cout << "1. Chicken boxed meal: 80\n2. Pork boxed meal: 70\n3. Vegetable boxed meal: 60\n4. Meatball soup: 30\n5. Fish soup: 20" << endl;
        cout << "Buy type: ";
        cin >> type;
        switch(type)    //Buy(int typenumber,int number,double idcard,int chopsiticks,int &total)
        {
            case 1: //80 dollar
                cinnumber(number);
                Buy(80,number,peroff,dd,total);
                break;
            case 2: //70
                cinnumber(number);
                Buy(70,number,peroff,dd,total);
                break;
            case 3: //60
                cinnumber(number);
                Buy(60,number,peroff,dd,total);
                break;
            case 4: //30
                cinnumber(number);
                Buy(30,number,peroff,dd,total);
                break;
            case 5: //20
                cinnumber(number);
                Buy(20,number,peroff,dd,total);
                break;
            case -1:    //exit
                cout << "Total: "<< total << endl;
                break;
            default :   //out of range,
                cout << "Out of range!" << endl;
                break;
        }
        if(type==-1)    //exit;
            break;
    }
    return 0;

}
