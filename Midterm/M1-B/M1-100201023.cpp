#include <iostream>

using namespace std;

void Buy(int type , int number , int student , int chopsticks , int& total);

int main()
{
    int student , chopsticks;
    int type , number , total = 0;

    while(true) // input if student card
    {
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> student;
        if(student == 0 || student == 1)
            break;
        cout << "Out of range!" << endl;
    }

    while(true) // input if chopsticks
    {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chopsticks;
        if(chopsticks == 0 || chopsticks == 1)
            break;
        cout << "Out of range!" << endl;
    }

    while(true)
    {
        cout << "1. Chicken boxed meal: 80" << endl; // output menu
        cout << "2. Pork boxed meal: 70" << endl;
        cout << "3. Vegetable boxed meal: 60" << endl;
        cout << "4. Meatball soup: 30" << endl;
        cout << "5. Fish soup: 20" << endl;

        while(true) // input type
        {
            cout << "Buy type: ";
            cin >> type;
            if((type <= 5 && type >= 1) || type == -1)
                break;
            cout << "Out of range!" << endl;
        }

        while(type != -1) // input number
        {
            cout << "Number: ";
            cin >> number;
            if(number > 0)
                break;
            cout << "Out of range!" << endl;
        }

        if(type != -1) // count money
        {
            Buy(type , number , student , chopsticks , total);
            cout << "Total: " << total << endl;
        }
        else // exit
        {
            cout << "Total money: " << total << endl;
            break;
        }
    }
}

void Buy(int type , int number , int student , int chopsticks , int& total)
{
    int money , temptotal , discount = 0;

    if(type == 1) // set money
        money = 80;
    else if(type == 2)
        money = 70;
    else if(type == 3)
        money = 60;
    else if(type == 4)
        money = 30;
    else
        money = 20;

    temptotal = money * number;

    if(student == 1) // student discount
        temptotal *= 0.9;

    if(type <= 3 && chopsticks == 1) // chopsticks discount
        discount = number * 5;

    temptotal -= discount; // total money

    total += temptotal;
}
