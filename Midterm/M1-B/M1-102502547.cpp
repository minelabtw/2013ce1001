#include <iostream>
using namespace std;
void buy(int v,int w,int x,int y,int &z); //宣告函式buy

void buy(int v,int w,int x,int y,int &z)
{
    int i=x*y; //宣告整數i，其值為x乘以y
    if(v==1)
        i=i*0.9; //如果v值為1，i乘以0.9
    if(w==1 && x>30)
        i=i-5*y; //如果w值為1且x大於30，i減5乘以y
    z=z+i;
    }

int main()
{
    int a=0;
    int b=0;
    int c=0;
    int d=0;
    int total=0; //宣告5個整數變數，初始值為0

    do
    {
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> a;
        if (a<0 || a>1)
            cout << "Out of range!\n";
    }
    while(a<0 || a>1); //輸入a值，如果a不為0或1時輸出"Out of range!"並重新輸入a值

    do
    {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> b;
        if (b<0 || b>1)
            cout << "Out of range!\n";
    }
    while(b<0 || b>1); //輸入b值，如果b不為0或1時輸出"Out of range!"並重新輸入b值

    do
    {
        do
        {
            cout << "1. Chicken boxed meal: 80\n2. Pork boxed meal: 70\n3.Vegetable boxed meal: 60\n4.Meatball soup : 30\n5.Fish soup: 20\nBuy type: ";
            cin >> c;
            if (c<-1 || c==0 || c>5)
                cout << "Out of range!\n";
        }
        while(c<-1 || c==0 || c>5); //輸入c值，如果c不為1~5或-1時輸出"Out of range!"並重新輸入c值

        if(c!=-1) //如果c不等於-1時執行
        {
        cout << "Number: ";
        cin >> d;
        if (d<1)
            cout << "Out of range!\n"; //輸入d值，如果d小於1時輸出"Out of range!"
        }

        if(d>0 && c!=-1) //d大於0且c不等於-1時執行
        {
            switch(c)
            {
            case 1: //c值為1
                buy(a,b,80,d,total); //執行函式buy
                break;
            case 2: //c值為2
                buy(a,b,70,d,total); //執行函式buy
                break;
            case 3: //c值為3
                buy(a,b,60,d,total); //執行函式buy
                break;
            case 4: //c值為4
                buy(a,b,30,d,total); //執行函式buy
                break;
            case 5: //c值為5
                buy(a,b,20,d,total); //執行函式buy
                break;
            default: //其他
                break;
            }
            cout << "Total: " << total << endl; //輸出字串及total值
        }
    }
    while(c!=-1); //c不等於-1時反覆執行
    cout << "Total money: " << total; //輸出字串及total值

    return 0;
}
