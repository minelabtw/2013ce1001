#include<iostream>
using namespace std;
void Buy(int,int ,int,int,int,int&);
int main()
{
    int total=0;//總價
    int type;//購買的商品
    int number;//購買數量
    int idcard;//判斷是否有學生證
    int chopsticks;//判斷是否有筷子
    do
    {
        //輸入是否有學生證，範圍: 0~1
        cout << ("Enter if you have id card 0=no 1=yes : ");
        cin >> idcard;
        if(idcard<0 || idcard>1)
            cout << "Out of range!\n";

    }
    while( idcard<0 || idcard>1 );
    do
    {
        //輸入是否有筷子， 範圍 0~1
        cout << ("Enter if you have chopsticks 0=no 1=yes : ");
        cin >> chopsticks;
        if(chopsticks<0 || chopsticks>1)
            cout << "Out of range!\n";

    }
    while( chopsticks<0 || chopsticks>1 );

    do
    {
        //輸出商品類別
        cout << "1. Chicken boxed meal: 80\n";
        cout << "2. Pork boxed meal: 70\n";
        cout << "3. Vegetable boxed meal: 60\n";
        cout << "4. Meatball soup: 30\n";
        cout << "5. Fish soup: 20\n";
        do
        {
            //輸入購買的商品種類， 範圍: 1~5
            cout << "Buy type: ";
            cin >> type;
            if((type>5 || type<1)&&type!=-1)
                cout << "Out of range!\n";
        }
        while( (type>5 || type<1)&&type!=-1 );

        if(type==-1)
            break;
        do
        {
            //輸入購買數量，大於零
            cout << "Number: ";
            cin >> number;
            if(number<1)
                cout << "Out of range!\n";
        }
        while(number<1);

        //依照購買的商品呼叫function
        switch(type)
        {
        case 1:
            Buy(1,80,number,idcard,chopsticks,total);
            cout << "Total: " << total << endl;
            break;
        case 2:
            Buy(2,70,number,idcard,chopsticks,total);
            cout << "Total: " << total << endl;
            break;
        case 3:
            Buy(3,60,number,idcard,chopsticks,total);
            cout << "Total: " << total << endl;
            break;
        case 4:
            Buy(4,30,number,idcard,chopsticks,total);
            cout << "Total: " << total << endl;
            break;
        case 5:
            Buy(5,20,number,idcard,chopsticks,total);
            cout << "Total: " << total << endl;
            break;
        }
    }
    while(type!=-1);
    //輸出總金額
    cout << "Total money: " << total << endl;
    return 0;
}
void Buy(int type,int typemoney,int number,int idcard,int chopsticks,int &total)
{
    /*
    type: 商品種類
    typemoney: 該商品的單價
    number: 購買的數量
    idcard: 是否有學生證
    chopsticks: 是否有筷子
    total: 目前的總金額
    */
    if(idcard==1)//有學生證打9折
        typemoney=typemoney*9/10;
    if(chopsticks==1 && type<4)//有筷子便當折扣5元
        typemoney-=5;

    //計算總金額
    total=total+typemoney*number;
}
