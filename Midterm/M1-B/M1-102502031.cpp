#include <iostream>

using namespace std;

//check function is used as a check for idCard and chopsticks
void check (string dataname, int &data)
{
    do
    {
        cout << "Enter if you have " << dataname << " 0=no 1=yes : ";
        cin >> data;
    }
    while (data!=0 && data!=1 && cout << "Out of range!" << endl);      //if data is neither 0 nor 1, prompt for data again
}

//Buy function prompt data ,calculate and print the result
void Buy (int type, int number, int idCard, int chopsticks, int price, int &total)
{
    //options
    cout << "1.Chicken boxed meal: 80" << endl;
    cout << "2.Pork boxed meal: 70" << endl;
    cout << "3.Vegetable boxed meal: 60" << endl;
    cout << "4.Meatball soup: 30" << endl;
    cout << "5.Fish soup: 20" << endl;

    while (type!=-1)   //leave while if type is -1
    {
        do
        {
            cout << "Buy type: ";
            cin >> type;
        }
        while (type!=1 && type!=2 && type!=3 && type!=4 && type!=5 && type!=-1 && cout << "Out of range!" << endl);

        if (type!=-1)  //run the program if type is not -1
        {
            do
            {
                cout << "Number: ";
                cin >> number;
            }
            while (number<=0 && cout << "Out of range!" << endl);  //number must be positive integer

            switch (type)
            {
            case 1:
                price=80;  //Chicken boxed meal
                break;
            case 2:
                price=70;  //Pork boxed meal
                break;
            case 3:
                price=60;  //Vegetable boxed meal
                break;
            case 4:
                price=30;  //Meatball soup
                break;
            case 5:
                price=20;  //Fish soup
                break;
            }

            if (idCard==1)
                price=price*0.9;  //have a ID card, so get 10% off

            if (chopsticks==1 && type <= 3)
                price=price-5;    //have chopsticks, so meal can get 5 dollars off

            total=total+price*number;
            cout << "Total: " << total << endl;
        }
    }
    cout << "Total money: " << total;  //print the result and end function
}

int main ()
{
    int idCard=0;
    int chopsticks=0;
    int BuyType=0;
    int number=0;
    int Total=0;
    int price=0;

    check ("id card", idCard);
    check ("chopsticks", chopsticks);

    Buy (BuyType, number, idCard, chopsticks, price, Total);

    return 0;
}
