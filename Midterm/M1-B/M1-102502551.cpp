#include <iostream>

using namespace std;

int main()
{
    int x=0,y=0,chicken=80,pork=70,vegetable=60,meatball=30,fish=20,type,number,a,total=0,f=0;

    cout<<"Enter if you have id card 0=no 1=yes : ";
    cin>>x;
    while(x!=0 && x!=1)                                                                                 //判斷是否在範圍內
    {
        cout<<"Out of range!"<<endl<<"Enter if you have id card 0=no 1=yes : ";
        cin>>x;
    }
    cout<<"Enter if you have chopstick 0=no 1=yes : ";
    cin>>y;
    while(y!=0 && y!=1)
    {
        cout<<"Out of range!"<<endl<<"Enter if you have chopstick 0=no 1=yes : ";                       //判斷是否在範圍內
        cin>>y;
    }
    if(x==1)                                                                                            //設定折價
    {
        chicken=72;
        pork=63;
        vegetable=54;
        meatball=27;
        fish=18;
    }
    if(y==1)
    {
        chicken=chicken-5;
        pork=pork-5;
        vegetable=vegetable-5;
    }
    while(f==0)
    {
        cout<<"1. Chicken boxed meal: 80"<<endl<<"2. Pork boxed meal: 70"<<endl<<"3. Vegetable boxed meal: 60"<<endl<<"4. Meatball soup: 30"<<endl<<"5. Fish soup: 20"<<endl<<"Buy type: ";
        cin>>type;
        while(type!=1 && type!=2 && type!=3 && type!=4 && type!=5 && type!=-1)                            //判斷是否在範圍內
        {
            cout<<"Out of range!"<<endl<<"Buy type: ";
            cin>>type;
        }
        if(type==-1)
        {
            cout<<"Total money: "<<total;
            f=1;
            break;
        }
        cout<<"Number: ";
        cin>>number;
        while(number<=0)
        {
            cout<<"Out of range!"<<endl<<"Number: ";
            cin>>number;
        }
        switch(type)                                                                                       //switch
        {
        case 1:
            a=chicken*number;
            total=total+a;
            cout<<"Total: "<<total<<endl;
            break;
        case 2:
            a=pork*number;
            total=total+a;
            cout<<"Total: "<<total<<endl;
            break;
        case 3:
            a=vegetable*number;
            total=total+a;
            cout<<"Total: "<<total<<endl;
            break;
        case 4:
            a=meatball*number;
            total=total+a;
            cout<<"Total: "<<total<<endl;
            break;
        case 5:
            a=fish*number;
            total=total+a;
            cout<<"Total: "<<total<<endl;
            break;
        }
    }
    return 0;
}
