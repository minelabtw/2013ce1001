#include<iostream>
#include <math.h>
using namespace std;
int idcard=0,chopstick=0,buytype=0,typemoney=0,typenumber=0,number=0,total=0;
//宣告各種變數

void buy(int buytype,int typemoney,int number,int idcard,int chopstick,int &total)//定義計算價錢的函數
{
    if(idcard!=0)//有學生證才可享九折
        typemoney*=0.9;

    if(chopstick!=0&&1<=buytype&&buytype<=3)//有帶環保筷則便當可少五元
        typemoney-=5;

    total+=typemoney*number;//計算總價
}

int main()
{
    do//詢問使用者有沒有學生證，且只能輸入0或1
    {
        cout<<"Enter if you have id card 0=no 1=yes : ";
        cin>>idcard;
        if(idcard<0||1<idcard)
            cout<<"Out of range!"<<endl;
    }
    while(idcard<0||1<idcard);

    do//詢問使用者有沒有自備環保筷，且只能輸入0或1
    {
        cout<<"Enter if you have chopstick 0=no 1=yes : ";
        cin>>chopstick;
        if(chopstick<0||1<chopstick)
            cout<<"Out of range!"<<endl;
    }
    while(chopstick<0||1<chopstick);

    cout<<"1. Chicken boxed meal: 80"<<endl
        <<"2. Pork boxed meal: 70"<<endl
        <<"3. Vegetable boxed meal: 60"<<endl
        <<"4. Meatball soup: 30"<<endl
        <<"5. Fish soup: 20"<<endl;
    //顯示出所有商品的定價

    cout<<"Buy type: ";//請使用者輸入要購買的物品
    cin>>buytype;

    while(buytype!=-1)//當輸入-1時程式結束
    {

        switch (buytype)
        {
        case 1:
            typemoney=80;//第一件商品價格為80元
            cout<<"Number: ";
            cin>>number;//輸入數量
            buy(buytype,typemoney,number,idcard,chopstick,total);//利用函數計算價錢
            cout<<"Total: "<<total<<endl;//輸出結果
            break;

        case 2:
            typemoney=70;//第一件商品價格為70元
            cout<<"Number: ";
            cin>>number;//輸入數量
            buy(buytype,typemoney,number,idcard,chopstick,total);//利用函數計算價錢
            cout<<"Total: "<<total<<endl;//輸出結果
            break;

        case 3:
            typemoney=60;//第一件商品價格為60元
            cout<<"Number: ";
            cin>>number;//輸入數量
            buy(buytype,typemoney,number,idcard,chopstick,total);//利用函數計算價錢
            cout<<"Total: "<<total<<endl;//輸出結果
            break;

        case 4:
            typemoney=30;//第一件商品價格為30元
            cout<<"Number: ";
            cin>>number;//輸入數量
            buy(buytype,typemoney,number,idcard,chopstick,total);//利用函數計算價錢
            cout<<"Total: "<<total<<endl;//輸出結果
            break;

        case 5:
            typemoney=20;//第一件商品價格為20元
            cout<<"Number: ";
            cin>>number;//輸入數量
            buy(buytype,typemoney,number,idcard,chopstick,total);//利用函數計算價錢
            cout<<"Total: "<<total<<endl;//輸出結果
            break;

        case -1:
            break;

        default:
            cout<<"Out of range!"<<endl;
            break;
        }
        cout<<"1. Chicken boxed meal: 80"<<endl
            <<"2. Pork boxed meal: 70"<<endl
            <<"3. Vegetable boxed meal: 60"<<endl
            <<"4. Meatball soup: 30"<<endl
            <<"5. Fish soup: 20"<<endl;
        cout<<"Buy type: ";
        cin>>buytype;
    }

    cout<<"Total money: "<<total;//最後再輸出總金額

    return 0;
}













