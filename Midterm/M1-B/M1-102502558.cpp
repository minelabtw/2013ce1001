#include <iostream>

using namespace std;

const double discount[2] {1.0,0.9}; // 儲存優惠打折

// 輸入學生證還有筷子用的
void input_first(const char *prompt,const char *error, int &n)
{
    cout << prompt;
    cin >> n;
    while (n != 0 && n != 1)
    {
        cout << error << endl << prompt;
        cin >> n;
    }
}

// 輸入buytype
void input_buytype(const char *prompt,const char *error, int &n)
{
    cout << prompt;
    cin >> n;
    while ((n < 1 || n > 5) && n != -1)
    {
        cout << error << endl << prompt;
        cin >> n;
    }
}

// 輸入數量
void input_num(const char *prompt,const char *error, int &n)
{
    cout << prompt;
    cin >> n;
    while (n <= 0)
    {
        cout << error << endl << prompt;
        cin >> n;
    }
}

// 印出菜單
void print_menu(void)
{
    cout << "1. Chicken boxed meal: 80" << endl;
    cout << "2. Pork boxed meal: 70" << endl;
    cout << "3. Vegetable boxed meal: 60" << endl;
    cout << "4. Meatball soup: 30" << endl;
    cout << "5. Fish soup: 20" << endl;
}

// 計算用的函數
void buy(int type,int typemoney, int number, int id, int chopsticks, double &total)
{
    switch(type)
    {
    // 如果 type 是 1 2 3 就計算便當
    case 1:
    case 2:
    case 3:
        total += typemoney * number * discount[id] - chopsticks * number * 5;
        break;
    // 如果 type 是 4 5 就計算湯
    case 4:
    case 5:
        total += typemoney * number * discount[id];
        break;
    }
    cout << "Total money: " << total << endl;
}

int main()
{

    int idcard = 0,chopsticks = 0,buytype = 0,num = 0;
    // 總金額 一開始為零
    double total = 0;
    // 價目表 陣列第一個為零 不會用到
    const int money[6] = {0,80,70,60,30,20};
    // 輸入 學生證還有筷子
    input_first("Enter if you have id card 0=no 1=yes : ","Out of range!",idcard);
    input_first("Enter if you have chopsticks 0=no 1=yes : ","Out of range!",chopsticks);

    // 一直讓使用者輸入
    while (true)
    {
        print_menu(); // 印出菜單
        input_buytype("Buy type: ","Out of range!",buytype);

        if (buytype == -1) // 如果 type  -1 就跳出
            break;

        input_num("Number: ","Out of range!",num);
        // 計算金額
        buy(buytype,money[buytype], num, idcard, chopsticks, total);
    }
    cout << "Total money: " << total << endl;
    return 0;
}
