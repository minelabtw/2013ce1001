#include<iostream>
using namespace std;
void Buy(int ,int ,int ,int ,int ,int ,int ,int&);        //function prototype

int main()
{
    int idcard =0;                      //宣告
    int chopsticks =0;
    int type =0;
    int n1 =0;
    int n2 =0;
    int n3 =0;
    int n4 =0;
    int n5 =0;
    int total =0;

    cout<<"Enter if you have id card 0=no 1=yes : ";
    cin>>idcard;
    while(idcard != 0 and idcard != 1)                    //只能輸入 0 1
    {
        cout<<"Out of range!\nEnter if you have id card 0=no 1=yes : ";
        cin>>idcard;
    }
    cout<<"Enter if you have chopsticks 0=no 1=yes : ";
    cin>>chopsticks;
    while(chopsticks != 0 and chopsticks != 1)                //只能輸入 0 1
    {
        cout<<"Out of range!\nEnter if you have chopsticks 0=no 1=yes : ";
        cin>>chopsticks;
    }

    while (type != -1)                                  //重複輸入
    {
        cout<<"1. Chicken boxed meal: 80\n2. Pork boxed meal: 70\n3. Vegetable boxed meal: 60\n4. Meatball soup: 30\n5. Fish soup: 20\nBuy type: ";
        cin>>type;
        switch(type)                        //判斷type
        {
        case 1:
            cout<<"Number: ";
            cin>>n1;
            while (n1 <= 0)                      //數量要>0
            {
                cout<<"Out of range!\nNumber: ";
                cin>>n1;
            }
            Buy(idcard , chopsticks , n1 , n2 , n3 , n4 , n5 , total);
            cout<<"Total: "<<total<<endl;                               //呼叫函式
            break;
        case 2:
            cout<<"Number: ";
            cin>>n2;
            while (n2 <= 0)                            //數量要>0
            {
                cout<<"Out of range!\nNumber: ";
                cin>>n2;
            }
            Buy(idcard , chopsticks , n1 , n2 , n3 , n4 , n5 , total);
            cout<<"Total: "<<total<<endl;                        //呼叫函式
            break;
        case 3:
            cout<<"Number: ";
            cin>>n3;
            while (n3 <= 0)                                 //數量要>0
            {
                cout<<"Out of range!\nNumber: ";
                cin>>n3;
            }
            Buy(idcard , chopsticks , n1 , n2 , n3 , n4 , n5 , total);
            cout<<"Total: "<<total<<endl;
            break;
        case 4:
            cout<<"Number: ";
            cin>>n4;
            while (n4 <= 0)
            {
                cout<<"Out of range!\nNumber: ";
                cin>>n4;
            }
            Buy(idcard , chopsticks , n1 , n2 , n3 , n4 , n5 , total);
            cout<<"Total: "<<total<<endl;
            break;
        case 5:
            cout<<"Number: ";
            cin>>n5;
            while (n5 <= 0)
            {
                cout<<"Out of range!\nNumber: ";
                cin>>n5;
            }
            Buy(idcard , chopsticks , n1 , n2 , n3 , n4 , n5 , total);
            cout<<"Total: "<<total<<endl;
            break;
        case -1:
            cout<<"Total money: "<<total<<endl;           //最後要有總金額
            break;
        default:
            cout<<"Out of range!\n";
            break;
        }
    }
    return 0;
}

void Buy( int idcard ,int chopsticks ,int n1 ,int n2 ,int n3 ,int n4 ,int n5 ,int &total )      //用,分開
{
    if (idcard == 1 and chopsticks == 1)
        total = (n1*80+n2*70+n3*60)*0.9-(n1+n2+n3)*5+(n4*30+n5*20)*0.9;

    else if (idcard != 1 and chopsticks == 1)
        total = (n1*80+n2*70+n3*60)-(n1+n2+n3)*5+(n4*30+n5*20);

    else if (idcard == 1 and chopsticks != 1)
        total = (n1*80+n2*70+n3*60)*0.9+(n4*30+n5*20)*0.9;

    else
        total = (n1*80+n2*70+n3*60)+(n4*30+n5*20);
}
