#include <iostream>
using namespace std;

//comput cummation
//data: 0:no use 1:id 2:chopsticks 3:typemoney 4:number
void buy( int data[], int &total)
{
    if ( data[3] >= 60 )
        if ( data[1] == 1 )
            total += ( 0.9 * data[3] - 5 * data[2] ) * data[4];
        else
            total += ( data[3] - 5 * data[2] ) * data[4];
    else
        if ( data[1] == 1 )
            total += 0.9 * data[3] * data[4];
        else
            total += data[3] * data[4];
}

int main()
{
    //decalaration and initialization
    int input[5];                    //0:no use 1:id 2:chopsticks 3:typemoney 4:number
    for ( int i = 0; i <= 4; i ++ )
        input[i] = 0;
    int sum = 0;
    int ctrl = 0;
    int temp = 0;

    //ask for id card
    do
    {
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> input[1];
        if ( input[1] == 0 or input[1] == 1 )
            break;
        else
            cout << "Out of range!\n";
    }
    while ( ctrl == 0 );

    // ask for chopsticks
    do
    {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> input[2];
        if ( input[2] == 0 or input[2] == 1 )
            break;
        else
            cout << "Out of range!\n";
    }
    while ( ctrl == 0 );

    //ask for type ane quanties
    do
    {
        //ask for type
        do
        {
            ctrl = 0;
            cout << "1. Chicken boxed meal: 80" << endl
                 << "2. Pork boxed meal: 70" << endl
                 << "3. Vegetable boxed meal: 60" << endl
                 << "4. Meatball soup: 30" << endl
                 << "5. Fish soup: 20" << endl;
            cout << "Buy type: ";
            cin >> temp;
            switch ( temp )
            {
            case 1:
                input[3] = 80;
                ctrl = 0;
                break;
            case 2:
                input[3] = 70;
                ctrl = 0;
                break;
            case 3:
                input[3] = 60;
                ctrl = 0;
                break;
            case 4:
                input[3] = 30;
                ctrl = 0;
                break;
            case 5:
                input[3] = 20;
                ctrl = 0;
                break;
            case -1:
                ctrl = -1;
                break;
            default:
                cout << "Out of range!\n";
                ctrl = 1;
                break;
            }
        }
        while ( ctrl == 1 );

        //if type input is -1, exit program
        if ( ctrl == -1 )
            break;

        //ask for number
        do
        {
            cout << "Number: ";
            cin >> input[4];
            if ( input[4] <= 0 )
                cout << "Out of range!\n";
        }
        while ( input[4] <= 0 );

        //reutrn result
        buy( input, sum );
        cout << "Total: " << sum << endl;
    }
    while ( ctrl == 0 );

    cout << "Total money: " << sum << endl;

    return 0;
}
