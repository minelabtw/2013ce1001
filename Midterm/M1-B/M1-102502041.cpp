#include<iostream>
using namespace std;
int total=0;
inline void menu()
{
    cout<<"1. Chicken boxed meal: 80"<<endl;
    cout<<"2. Pork boxed meal: 70"<<endl;
    cout<<"3. Vegetable boxed meal: 60"<<endl;
    cout<<"4. Meatball soup: 30"<<endl;
    cout<<"5. Fish soup: 20"<<endl;
}
inline void Buy(int typemoney,int number,int idcard,int chopsticks, int &total) //設計一個BUY函數來運算我的金額
{
    switch(idcard)                                                              //運算過程先用學生證來區隔開是否需要打九折
    {
    case 0:
    {
        if(chopsticks==1 and (typemoney==80 or typemoney==70 or typemoney==60))         //接著再看有沒有環保筷跟買便當
            total=total+(typemoney-5)*number;
        else                                                                            //若他買的不是便當,則有沒有環保筷都沒有差別
            total=total+typemoney*number;
        break;
    }
    case 1:
    {
        if(chopsticks==1 and (typemoney==80 or typemoney==70 or typemoney==60))
            total=total+(typemoney-5)*number*0.9;
        else
            total=total+typemoney*number*0.9;
        break;
    }
    }
}


int main()
{
    int idcard,chopsticks,buytype,typemoney,number;   //total為總金額,chopsticks為環保筷,buytype為購買項目,typemoney為購買項目之金額,number為購買數量
    while(1)
    {
        cout<<"Enter if you have id card 0=no 1=yes : ";
        cin>>idcard;
        if(idcard==0 or idcard==1)
            break;
        else
            cout<<"Out of range!"<<endl;
    }
    while(1)
    {
        cout<<"Enter if you have chopsticks 0=no 1=yes : ";
        cin>>chopsticks;
        if(chopsticks==0 or chopsticks==1)
            break;
        else
            cout<<"Out of range!"<<endl;
    }
    while(1)
    {
        menu();

        while(1)
        {
            cout<<"Buy type: ";
            cin>>buytype;
            switch(buytype)
            {
            case 1:
                typemoney=80;
                break;
            case 2:
                typemoney=70;
                break;
            case 3:
                typemoney=60;
                break;
            case 4:
                typemoney=30;
                break;
            case 5:
                typemoney=20;
                break;
            default:
                if(buytype==-1)
                    break;
                cout<<"Out of range!"<<endl;
                continue;
            }
            break;
        }
        if(buytype==-1)
            break;


        while(1)
        {
            cout<<"Number: ";
            cin>>number;
            if(number>0)
                break;
            else
                cout<<"Out of range!"<<endl;
        }
        Buy(typemoney,number,idcard,chopsticks,total);
        cout<<"Total money: "<<total<<endl;

    }
    cout<<"Total money: "<<total<<endl;
    return 0;
}
