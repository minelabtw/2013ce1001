#include <iostream>
using namespace std;

void buy(int typemoney,int number,double idcaed,int chopsticks,int &total)//計算函式
{
    if(idcaed==0)//沒學生證
    {
        if(chopsticks==0)//沒環保筷
        {
            total = typemoney * number + total;
        }
        else//有環保筷
        {
            if(typemoney==80 || typemoney==70 || typemoney==60)//判斷買的是否為便當
            {
                total = typemoney * number - 5 * number + total;
            }
            else
            {
                total = typemoney * number + total;
            }
        }
    }
    else//有學生證
    {
        if(chopsticks==0)//沒環保筷
        {
            total = typemoney * number * 0.9 + total;
        }
        else//有環保筷
        {
            if(typemoney==80 || typemoney==70 || typemoney==60)//判斷買的是否為便當
            {
                total = (typemoney * number * 0.9) - (5 * number) + total;
            }
            else
            {
                total = typemoney * number * 0.9 + total;
            }
        }
    }
}

int main()
{
    int idcaed,chopsticks;//宣告型別為整數的idcaed,chopsticks
    int buy_type,typemoney,number,total=0;//宣告型別為整數的buy_type,typemoney,number,total=0

    while(1)//詢問有無學生證
    {
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> idcaed;
        if(idcaed<0 || idcaed>1)//判斷輸入是否不為0或1
        {
            cout << "Out of range!\n";
        }
        else
        {
            break;
        }
    }
    while(1)//詢問有無環保筷
    {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chopsticks;
        if(chopsticks<0 || chopsticks>1)//判斷輸入是否不為0或1
        {
            cout << "Out of range!\n";
        }
        else
        {
            break;
        }
    }
    while(buy_type!=-1)//詢問餐點,數量,和輸出金額,並判斷是否輸入-1要離開
    {
        cout << "1. Chicken boxed meal: 80\n";//輸出菜單
        cout << "2. Pork boxed meal: 70\n";
        cout << "3. Vegetable boxed meal: 60\n";
        cout << "4. meatball soup: 30\n";
        cout << "5. Fish soup: 20\n";
        while(1)//詢問餐點
        {
            cout << "Buy type: ";
            cin >> buy_type;
            if(buy_type<-1 || buy_type==0)//判斷輸入的餐點是否不在範圍內
            {
                cout << "Out of range!\n";
            }
            else if(buy_type==1)//餐點1
            {
                typemoney=80;
                break;
            }
            else if(buy_type==2)//餐點2
            {
                typemoney=70;
                break;
            }
            else if(buy_type==3)//餐點3
            {
                typemoney=60;
                break;
            }
            else if(buy_type==4)//餐點4
            {
                typemoney=30;
                break;
            }
            else if(buy_type==5)//餐點5
            {
                typemoney=20;
                break;
            }
            else if(buy_type==-1)//不再購買
            {
                break;
            }
        }
        while(buy_type!=-1)//詢問數量,如果buy_type=-1則不進入
        {
            cout << "Number: ";
            cin >> number;
            if(number<0)//判斷數量是否<0
            {
                cout << "Out of range!\n";
            }
            else
            {
                break;
            }
        }
        while(buy_type!=-1)//計算價錢,如果buy_type=-1則不進入
        {
            buy(typemoney,number,idcaed,chopsticks,total);//帶入函式
            break;
        }
        cout << "Total: " << total << endl;//輸出價錢
    }

    return 0;
}
