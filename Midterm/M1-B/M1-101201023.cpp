#include <iostream>
using namespace std;

int Buy(int kind,int haveid,int havechop,int N,int m)
{
    if(kind==1)          //選擇不同購買類型的金額運算
    {
        if(haveid==1)              //判斷是否有學生證，並折扣
        {
            m=80*N*0.9-havechop*5*N+m;
        }

        else
        {
            m=80*N-havechop*5*N+m;
        }
    }
    else if(kind==2)
    {
        if(haveid==1)
        {
            m=70*N*0.9-havechop*5*N+m;
        }

        else
        {
            m=70*N-havechop*5*N+m;
        }
    }
    else if(kind==3)
    {
        if(haveid==1)
        {
            m=60*N*0.9-havechop*5*N+m;
        }

        else
        {
            m=60*N-havechop*5*N+m;
        }
    }
    else if(kind==4)
    {
        if(haveid==1)
        {
            m=30*N*0.9+m;
        }

        else
        {
            m=30*N+m;
        }
    }
    else if(kind==5)
    {
        if(haveid==1)
        {
            m=20*N*0.9+m;
        }

        else
        {
            m=20*N+m;
        }
    }
    return m;               //方程輸出的值
}

int main()
{
    int haveid=0;
    int havechop=0;
    int i=0;
    int kind=0;
    int N=0;
    int m=0;
    int Buy(int kind,int haveid,int havechop,int N,int m);           //宣告方程

    cout << "Enter if you have id card 0=no 1=yes : ";
    cin >> haveid;
    while(haveid!=0 && haveid!=1)                                      //當haveid不等於0 OR 1時執行
    {
        cout << "Out of range!" << endl;
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> haveid;
    }

    cout << "Enter if you have chopsticks 0=no 1=yes : ";
    cin >> havechop;
    while(havechop!=0 && havechop!=1)
    {
        cout << "Out of range!" << endl;
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> havechop;
    }

    while(i<1)
    {
        cout << "1. Chichen boxed meal: 80" << endl;
        cout << "2. Pork boxwd meal: 70" << endl;
        cout << "3. Vegetable boxed meal: 60" << endl;
        cout << "4. Meatball soup: 30" << endl;
        cout << "5. Fish soup: 20" << endl;

        cout << "Buy type: ";                    //輸出Buy type: ，並選擇購買類型
        cin >> kind;
        switch(kind)
        {
            case 1:
                cout << "Number: ";
                cin >> N;
                while(N<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Number: ";
                    cin >> N;
                }
                cout << "Total: ";
                cout << Buy(kind,haveid,havechop,N,m) << endl;         //將方程帶入，並輸出金額
                m=Buy(kind,haveid,havechop,N,m);
                break;

            case 2:
                cout << "Number: ";
                cout << "Number: ";
                cin >> N;
                while(N<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Number: ";
                    cin >> N;
                }
                cout << "Total: ";
                cout << Buy(kind,haveid,havechop,N,m) << endl;
                m=Buy(kind,haveid,havechop,N,m);
                break;

            case 3:
                cout << "Number: ";
                cin >> N;
                while(N<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Number: ";
                    cin >> N;
                }
                cout << "Total: ";
                cout << Buy(kind,haveid,havechop,N,m) << endl;
                m=Buy(kind,haveid,havechop,N,m);
                break;

            case 4:
                cout << "Number: ";
                cin >> N;
                while(N<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Number: ";
                    cin >> N;
                }
                cout << "Total: ";
                cout << Buy(kind,haveid,havechop,N,m) << endl;
                m=Buy(kind,haveid,havechop,N,m);
                break;

            case 5:
                cout << "Number: ";
                cin >> N;
                while(N<=0)
                {
                    cout << "Out of range!" << endl;
                    cout << "Number: ";
                    cin >> N;
                }
                cout << "Total: ";
                cout << Buy(kind,haveid,havechop,N,m) << endl;
                m=Buy(kind,haveid,havechop,N,m);
                break;

            case -1:                                    //結束購買，並輸出總金額
                cout << "Total money: " << Buy(kind,haveid,havechop,N,m) << endl;
                i++;
                break;

            default:
                cout << "Out of range!" << endl;
                break;
        }
    }

    return 0;
}

