#include<iostream>
using namespace std;
void menu ();                           //宣告函式
int money ( int ,int ,double ,int );
int typechange(int);
int innumber();
int typechange2( int );

int main ()
{
    double idcard=0;            //宣告變數
    int chopsticks=0;
    int type=0;
    int total=0;
    int Number=0;
    long double sum=0;
    int typemoney=0;


    cout << "Enter if you have id card 0=no 1=yes : ";          //輸入是否有idcard並判斷是否輸入錯誤
    cin >> idcard;
    while (idcard!=0&&idcard!=1)
    {
        cout << "Out of range!" << endl;
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> idcard;
    }

    cout << "Enter if you have chopsticks 0=no 1=yes : ";       //輸入是否有自備筷子並判斷是否輸入錯誤
    cin >> chopsticks;
    while (chopsticks!=0&&chopsticks!=1)
    {
        cout << "Out of range!" << endl;
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chopsticks;
    }

    if (idcard==0)          //轉換倍率
        idcard=1;
    else
        idcard=0.9;

    if (chopsticks==1)      //計算折扣
        chopsticks=-5;

    menu ();

    while (cin >> type)     //當輸入type 進入迴圈
    {
        if (type==-1)       //當type=0時，跳出迴圈
            break;

        switch(type)        //進入switch迴圈
        {
            case 1:
                Number = innumber();
                typemoney = typechange(type);
                total = money ( typemoney , Number , idcard , chopsticks );
                sum += total;
                cout << "Total: " << sum << endl ;
                menu ();
                break;

            case 2:
                Number = innumber();
                typemoney = typechange(type);
                total = money ( typemoney , Number , idcard , chopsticks );
                sum += total;
                cout << "Total: " << sum << endl ;
                menu ();
                break;

            case 3:
                Number = innumber();
                typemoney = typechange(type);
                total = money ( typemoney , Number , idcard , chopsticks );
                sum += total;
                cout << "Total: " << sum << endl ;
                menu ();
                break;

            case 4:
                Number = innumber();
                typemoney = typechange(type);
                chopsticks = typechange2(type);
                total = money ( typemoney , Number , idcard , chopsticks);
                sum += total;
                cout << "Total: " << sum << endl ;
                menu ();
                break;

            case 5:
                Number = innumber();
                typemoney = typechange(type);
                chopsticks = typechange2(type);
                total = money ( typemoney , Number , idcard , chopsticks);
                sum += total;
                cout << "Total: " << sum << endl ;
                menu ();
                break;

            default :       //輸入錯誤，重新輸入
                cout << "Out of range!" << endl;
                menu ();
                break;
        }
    }
    cout << "Total money: " << sum ;        //印出最後總額
    return 0;
}


int typechange(int x)       //轉換價格
{
    int typemoney=0;

    if (x==1)
        typemoney=80;
    else if (x==2)//輸入是否有idcard並判斷是否輸入錯誤
        typemoney=70;
    else if (x==3)
        typemoney=60;
    else if (x==4)
        typemoney=30;
    else if (x==5)
        typemoney=20;

    return typemoney;
}

int typechange2(int x)      //轉換折扣
{
    int chopsticks=0;
        chopsticks=0;

    return chopsticks;
}

int money (int a , int b , double c ,int d)     //計算本次交易花的錢
{
    int e=0;
    e = e + b*(a*c+d) ;
    return e;
}
int innumber()      //用來輸入買的數量
{
    int Number=0;
    cout << "Number: ";
    cin >> Number;
    while (Number<=0)
    {
        cout << "Out of range!" << endl;
        cout << "Number: ";
        cin >> Number;
    }
    return Number;
}

void menu ()        //菜單
{
    cout << "1. Chicken boxed meal: 80" << endl;
    cout << "2. Pork boxed meal: 70" << endl;
    cout << "3. Vegetable boxed meal: 60" << endl;
    cout << "4. Meatball soup: 30" << endl;
    cout << "5. Fish soup: 20" << endl;
    cout << "Buy type: ";
}
