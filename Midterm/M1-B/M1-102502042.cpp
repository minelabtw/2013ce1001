#include <iostream>
using namespace std;
void Buy(int *,int ,int ,int ,int ,int &);
void display_menu();
int main()
{
    ios::sync_with_stdio(0);
    /*宣告各種變數*/
    int have_idcard;
    int have_chopstick;
    int buy_type, number, total=0;
    int typemoney[5] = {80,70,60,30,20}; //各項菜色價錢
    cout << "Enter if you have id card 0=no 1=yes : ";
    cin >> have_idcard;
    while(have_idcard!=0&&have_idcard!=1)
    {
        cout << "Out of range!" << endl;
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> have_idcard;
    }
    cout << "Enter if you have chopsticks 0=no 1=yes : ";
    cin >> have_chopstick;
    while(have_chopstick!=0&&have_chopstick!=1)
    {
        cout << "Out of range!" << endl;
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> have_chopstick;
    }
    display_menu();
    while(cin >> buy_type&&buy_type!=-1)  //輸入類別與判斷是否結束迴圈
    {
        cout << "Number: ";
        cin >> number;
        while(number<=0)
        {
            cout << "Out of range!" << endl;
            cout << "Number: ";
            cin >> number;
        }
        Buy(typemoney,buy_type,number,have_idcard,have_chopstick,total);
        cout << "Total: " << total << endl;
        display_menu();
    }
    cout<<"Total: " << total << endl;
    return 0;
}
//購買函式
void Buy(int *typemoney,int type,int number,int idcard,int chopstick,int &total)
{
    int sum=(typemoney[type-1]*number);
    if(idcard)sum=sum*9/10;
    if(type<4&&chopstick)sum-=5*number;
    total+=sum;
}
//輸出菜單
void display_menu()
{
    cout << "1. Chicken boxed meal: 80" << endl;
    cout << "2. Pork boxed meal: 70" << endl;
    cout << "3. Vegetable boxed meal: 60"<< endl;
    cout << "4. Meatball soup: 30" << endl;
    cout << "5. Fish soup: 20" << endl;
    cout << "Buy type: ";
}
