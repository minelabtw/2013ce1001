#include <iostream>
#include <algorithm>

using namespace std;


//Buy function
void Buy(int type, int num, int id, int chop, int &total) {
    int sum = type * num;

    if (id) //id check
        sum = sum / 10 * 9;

    if (chop && type >= 60) //chop and boxed meal check
        sum -= 5*num;

    total += sum;
}

int main(void) {
    int id, chop;
    int btype, num;
    int flag = 1;
    int seal[5] = {80,70,60,30,20};
    int total = 0;

    //input id
    do {
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> id;
    } while ((id != 0 && id != 1) && cout << "Out of range!\n");

    //input chopsticks
    do {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chop;
    } while ((chop != 0 && chop != 1) && cout << "Out of range!\n");

    do {
        while (flag) {
            cout << "1. Chicken boxed meal: 80\n"
                    "2. Pork boxed meal: 70\n"
                    "3. Vegetable boxed meal: 60\n"
                    "4. Meatball soup: 30\n"
                    "5. Fish soup: 20\n";
            cout << "Buy type: ";
            cin >> btype;

            //option
            switch (btype) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5: {
                    cout << "Number: ";
                    cin >> num;
                    if (num > 0) {
                        flag = 0;
                        Buy(seal[btype -1], num, id, chop, total);
                        cout << "Total: " << total << endl;
                    }
                    break;
                }
                case -1:
                    flag = 0;
            }

            if (flag)
                cout << "Out of range!\n";
        }
        flag = 1;
    } while (btype != -1);

    cout << "Total money: " << total;

    return 0;
}
