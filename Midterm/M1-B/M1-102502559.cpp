#include<iostream>
using namespace std;
int buy(int&,int&,int&,int&,int&);
int total=0;
int typemoney,number,chopsticks,idcard;
int main()
{
    cout<<"Enter if you have id card 0=no 1=yes :";
    cin>>idcard;
    while(idcard!=0 and idcard !=1)//若此兩變數不等於1也不等於0,進入迴圈
    {
        cout<<"Out of range !"<<endl;
        cout<<"Enter if you have id card 0=no 1=yes :";
        cin>>idcard;
    }
    cout<<"Enter if you have chopsticks 0=no 1=yes :";
    cin>>chopsticks;
    while(chopsticks!=0 and chopsticks!=1)//若此兩變數不等於1也不等於0,進入迴圈
    {
        cout<<"Out of range !"<<endl;
        cout<<"Enter if you have chopsticks 0=no 1=yes :";
        cin>>chopsticks;
    }
    cout<<"1. Chicken boxed meal: 80"<<endl;
    cout<<"2. Pork boxed meal: 70"<<endl;
    cout<<"3. Vegetable boxed meal: 60"<<endl;
    cout<<"4. Meatball soup: 30"<<endl;
    cout<<"5. Fish soup: 20"<<endl;
    cout<<"Buy type: ";
    cin>>typemoney;//將輸入的值給變數
    while(typemoney!=1 && typemoney!=2 && typemoney!=3 && typemoney!=4 && typemoney!=5 && typemoney!=-1)//當此變數的值不等於1,2,3,4,5,-1時,進入迴圈
    {
        cout<<"Out of range !"<<endl;
        cout<<"Buy type: ";
        cin>>typemoney;
    }
    while(typemoney!=-1)
    {
        switch(typemoney)//依括號內變數的值做判斷
        {
        case 1://當值為1時,執行以下指令
            cout<<"Number: ";
            cin>>number;
            while(number<=0)
            {
                cout<<"Out of range !"<<endl;
                cout<<"Number: ";
                cin>>number;
            }
            switch(idcard)//依括號內變數的值做判斷
            {
            case 1:
                switch(chopsticks)
                {
                case 1:
                    total=total+number*(80-5)*0.9;
                    cout<<"Total: "<<total;
                    break;
                case 0:
                    total=total+number*80*0.9;
                    cout<<"Total: "<<total;
                    break;
                }
                break;
            case 0:
                switch(chopsticks)
                {
                case 1:
                    total=total+number*(80-5);
                    cout<<"Total: "<<total;
                    break;
                case 0:
                    total=total+number*80;
                    cout<<"Total: "<<total;
                    break;
                }
                break;
            }
            break;
        case 2:
            cout<<"Number: ";
            cin>>number;
            while(number<=0)
            {
                cout<<"Out of range !"<<endl;
                cout<<"Number: ";
                cin>>number;
            }
            switch(idcard)
            {
            case 1:
                switch(chopsticks)
                {
                case 1:
                    total=total+number*(70-5)*0.9;
                    cout<<"Total: "<<total;
                    break;
                case 0:
                    total=total+number*70*0.9;
                    cout<<"Total: "<<total;
                    break;
                }
                break;
            case 0:
                switch(chopsticks)
                {
                case 1:
                    total=total+number*(70-5);
                    cout<<"Total: "<<total;
                    break;
                case 0:
                    total=total+number*70;
                    cout<<"Total: "<<total;
                    break;
                }
                break;
            }
            break;
        case 3:
            cout<<"Number: ";
            cin>>number;
            while(number<=0)
            {
                cout<<"Out of range !"<<endl;
                cout<<"Number: ";
                cin>>number;
            }
            switch(idcard)
            {
            case 1:
                switch(chopsticks)
                {
                case 1:
                    total=total+number*(60-5)*0.9;
                    cout<<"Total: "<<total;
                    break;
                case 0:
                    total=total+number*60*0.9;
                    cout<<"Total: "<<total;
                    break;
                }
                break;
            case 0:
                switch(chopsticks)
                {
                case 1:
                    total=total+number*(60-5);
                    cout<<"Total: "<<total;
                    break;
                case 0:
                    total=total+number*60;
                    cout<<"Total: "<<total;
                    break;
                }
                break;
            }
            break;
        case 4:
            cout<<"Number: ";
            cin>>number;
            while(number<=0)
            {
                cout<<"Out of range !"<<endl;
                cout<<"Number: ";
                cin>>number;
            }
            switch(idcard)
            {
            case 1:
                total=total+number*30*0.9;
                cout<<"Total: "<<total;
                break;
            case 0:
                total=total+number*30;
                cout<<"Total: "<<total;
                break;
            }
            break;
        case 5:
            cout<<"Number: ";
            cin>>number;
            while(number<=0)
            {
                cout<<"Out of range !"<<endl;
                cout<<"Number: ";
                cin>>number;
            }
            switch(idcard)
            {
            case 1:
                total=total+number*20*0.9;
                cout<<"Total: "<<total;
                break;
            case 0:
                total=total+number*20;
                cout<<"Total: "<<total;
                break;
            }
            break;
        case -1:
            cout<<"Total :"<<total;
            break;
        }
        cout<<endl<<"1. Chicken boxed meal: 80"<<endl;
        cout<<"2. Pork boxed meal: 70"<<endl;
        cout<<"3. Vegetable boxed meal: 60"<<endl;
        cout<<"4. Meatball soup: 30"<<endl;
        cout<<"5. Fish soup: 20"<<endl;
        cout<<"Buy type: ";
        cin>>typemoney;
    }
    cout<<"Total :"<<total;//上面的while迴圈若因輸入typemoney=-1時會跳出,接下來顯示總金額
}


