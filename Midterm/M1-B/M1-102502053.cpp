#include <iostream>
#include <cmath>
using namespace std;

//function prototyper
void buy(double& total, int number, int idcard, int chopsticks, double cost, int type);

//start of the program
int main()
{
    //call variables
    int idcard;
    int chopsticks;
    int type;
    int number;
    double total=0;
    double cost;

    //choose for having a id card or not
    do
    {
        cout<<"Enter if you have id card 0=no 1=yes : ";
        cin>>idcard;
        if(idcard>1||idcard<0) //data validation
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(idcard>1||idcard<0);

    //choose for having chopsticks or not
    do
    {
        cout<<"Enter if you have chopsticks 0=no 1=yes : ";
        cin>>chopsticks;
        if(chopsticks>1||chopsticks<0)//data validation
        {
            cout<<"Out of range!"<<endl;
        }
    }
    while(chopsticks>1||chopsticks<0);

    do
    {
        //display list
        cout<<"1. Chicken boxed meal: 80"<<endl;
        cout<<"2. Pork boxed meal: 70"<<endl;
        cout<<"3. Vegetable boxed meal: 60"<<endl;
        cout<<"4. Meatball soup: 30"<<endl;
        cout<<"5. Fish soup: 20"<<endl;
        cout<<"Buy type: ";
        cin>>type;

        switch (type)
        {
        case 1: //select for Chicken boxed meal
            cost=80;
            do
            {
                cout<<"Number: ";
                cin>>number;
                if(number<=0)//data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(number<=0);
            buy(total, number, idcard, chopsticks, cost, type);//call the function
            cout<<"Total: "<<total<<endl;
            break;

        case 2: //select for Pork boxed meal
            cost=70;
            do
            {
                cout<<"Number: ";
                cin>>number;
                if(number<=0) //data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(number<=0);
            buy(total, number, idcard, chopsticks, cost, type);//call the function
            cout<<"Total: "<<total<<endl;
            break;

        case 3: //select for Vegetable boxed meal
            cost=60;
            do
            {
                cout<<"Number: ";
                cin>>number;
                if(number<=0) //data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(number<=0);
            buy(total, number, idcard, chopsticks, cost, type);//call the function
            cout<<"Total: "<<total<<endl;
            break;

        case 4://select for Meatball soup
            cost=30;
            do
            {
                cout<<"Number: ";
                cin>>number;
                if(number<=0)//data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(number<=0);
            buy(total, number, idcard, chopsticks, cost, type);//call the function
            cout<<"Total: "<<total<<endl;
            break;

        case 5: //select for Fish soup
            cost=20;
            do
            {
                cout<<"Number: ";
                cin>>number;
                if(number<=0)//data validation
                {
                    cout<<"Out of range!"<<endl;
                }
            }
            while(number<=0);
            buy(total, number, idcard, chopsticks, cost, type); //call the function
            cout<<"Total: "<<total<<endl;
            break;

        case -1: //exit
            cout<<"Total money: "<<total;
            break;

        default: // wrong input
            cout<<"Out of range!"<<endl;
            break;
        }


    }
    while(type!=-1);

    return 0;
}

//function for calculate the money
void buy(double& total,int number,int idcard,int chopsticks, double cost, int type)
{
    if(idcard==1)
    {
        cost=(cost*0.9);
    }
    if(type<4&&chopsticks==1)
    {
        cost=cost-5;
    }
    total=total+(cost*number);

}
