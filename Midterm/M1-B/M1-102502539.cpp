#include <iostream>

using namespace std;

int total = 0;
int buy ( int typemoney , int number , double idcard , int chopsticks );    //函式宣告

int main()
{
    double idcard;
    int chopsticks;
    int type = 0;
    int number = 0;

    cout << "Enter if you have id card 0=no 1=yes : " ;
    cin >> idcard;
    while ( idcard < 0 || idcard > 1 )
    {
        cout << "Out of range!\n" << "Enter if you have id card 0=no 1=yes : " ;
        cin >> idcard;
    }

    if ( idcard == 1 )
        idcard = 0.9 ;
    else
        idcard = 1 ;

    cout << "Enter if you have chopsticks 0=no 1=yes : " ;
    cin >> chopsticks;
    while ( chopsticks < 0 || chopsticks > 1 )
    {
        cout << "Out of range!\n" << "Enter if you have chopsticks 0=no 1=yes : " ;
        cin >> chopsticks;
    }

    if ( chopsticks == 1 )
        chopsticks = 5;
    else
        chopsticks = 0;

    while ( type != -1 )
    {
        cout << "1. Chicken boxed meal: 80" << endl << "2. Pork boxed meal: 70" << endl
             << "3. Vegetable boxed meal: 60" << endl << "4. Meatball soup: 30" << endl
             << "5. Fish soup: 20" << endl;
        cout << "Buy type: " ;
        cin >> type;

        switch ( type )
        {

            while ( type != 1 || type != 2 || type != 3 || type != 4 || type != 5 || type != -1 )
            {
                cout << "Out of range!\n" << "Buy type: " ;
                cin >> type;
            }
            if ( type == -1 )
                break;

        case 1: //Chicken boxed meal
            cout << "Number: " ;
            cin >> number;
            cout << "Total: " << buy( 80 , number , idcard , chopsticks ) << endl;
            cout << total;
            break;
        case 2: //Pork boxed meal
            cout << "Number: " ;
            cin >> number;
            cout << "Total: " << buy( 70 , number , idcard , chopsticks ) << endl;
            break;
        case 3: //Vegetable boxed meal
            cout << "Number: " ;
            cin >> number;
            cout << "Total: " << buy( 60 , number , idcard , chopsticks ) << endl;
            break;
        case 4: //Meatball soup
            cout << "Number: " ;
            cin >> number;
            cout << "Total: " << buy( 30 , number , idcard , 0 ) << endl;
            break;
        case 5: //Fish soup
            cout << "Number: " ;
            cin >> number;
            cout << "Total: " << buy( 20 , number , idcard , 0 ) << endl;
            break;
        case -1:
            cout << "Total money: " << total;
            break;
        default :
            cout << "Out of range!" << endl;
            break;

        }
    }

    return 0;
}

int buy ( int typemoney , int number , double idcard , int chopsticks )
{
    total = total + typemoney * number * idcard - chopsticks * number;
    return total;
}
