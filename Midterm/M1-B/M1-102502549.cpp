#include <iostream>
using namespace std;

//宣告並實作Buy函式
void buy(int typemoney,int number,int idcard,int chopsticks,int &total)
{
    int money[5]= {80,70,60,30,20};

    if(idcard==1)
    {
        if(chopsticks==1)
        {
            if(typemoney==1||typemoney==2||typemoney==3)
            {
                total=total+(money[typemoney-1]*0.9-5)*number;
            }
            else
            {
                total=total+(money[typemoney-1]*0.9)*number;
            }
        }
        else
        {
            total=total+(money[typemoney-1]*0.9)*number;
        }
    }
    else
    {
        if(chopsticks==1)
        {
            if(typemoney==1||typemoney==2||typemoney==3)
            {
                total=total+(money[typemoney-1]-5)*number;
            }
            else
            {
                total=total+(money[typemoney-1])*number;
            }
        }
        else
        {
            total=total+(money[typemoney-1])*number;
        }
    }
}


int main()
{
    //宣告Buy需要的參數(名稱淺顯易懂)
    int type;
    int num;
    int id;
    int chop;
    int total=0;

//檢查id合法性
    while(true)
    {
        cout<<"Enter if you have id card 0=no 1=yes : ";
        cin>>id;

        if(id!=0&&id!=1)
        {
            cout<<"Out of range!"<<endl;
        }
        else
            break;
    }

//檢查chop合法性
    while(true)
    {
        cout<<"Enter if you have chopsticks 0=no 1=yes : ";
        cin>>chop;

        if(chop!=0&&chop!=1)
        {
            cout<<"Out of range!"<<endl;
        }
        else
            break;
    }

//開始購買
    while(true)
    {
        cout<<"1. Chicken boxed meal: 80"<<endl;
        cout<<"2. Pork boxed meal: 70"<<endl;
        cout<<"3. Vegetable boxed meal: 60"<<endl;
        cout<<"4. Meatball soup: 30"<<endl;
        cout<<"5. Fish soup: 20"<<endl;

        cout<<"Buy type: ";
        cin>>type;

//用switch選擇
        switch(type)
        {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            while(true)
            {
                cout<<"Number: ";
                cin>>num;

                if(num<1)
                {
                    cout<<"Out of range!"<<endl;
                }
                else
                    break;
            }

            buy(type,num,id,chop,total);//呼叫Buy函式

            cout<<"Total: "<<total<<endl;
            break;

//-1為程式終止
        case -1:
            cout<<"Total money: "<<total;
            return 0;

        default:
            cout<<"Out of range!"<<endl;
            break;
        }
    }
}
