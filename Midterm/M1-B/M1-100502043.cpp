#include<iostream>

using namespace std;

void buy(int typemoney,int number,double idcard,int chopsticks,int &total)  //建構buy函式
{
    int total0=0;
    if (idcard==0&&chopsticks==0)  //用if約別有無學生證和筷子的4種作法
        total0 = typemoney * number;
    if (idcard==0&&chopsticks==1)
    {
        if (typemoney<4)
            total0 = (typemoney-5)*number;
        else
            total0 = typemoney*number;
    }
    if (idcard==1&&chopsticks==0)
        total0 = typemoney*number*0.9;
    if (idcard==1&&chopsticks==1)
    {
        if (typemoney<4)
        total0 = (typemoney-5)*number*0.9;
        else
            total0 = typemoney*number*0.9;
    }
    total = total0 + total;  //用total做y總金額
}

int main()
{
    int idcard,chopsticks;
    cout << "Enter if your have id card 0=no 1=yes : ";
    cin >> idcard;
    cout << "Enter if your have chopsticks 0=no 1=yes : ";
    cin >> chopsticks;

    int buytype=0,number,total=0,typemoney=0;

    while (buytype!=-1)
    {
        cout << "\n1. Chicken boxed meal:80" << endl << "2. Pork boxed meal: 70" << endl
         << "3. Vegetable boxed meal: 60" << endl << "4. Meatball soup: 30" << endl
         << "5. Fish soup: 20" << endl;

        cout << "Buy type: ";
        cin >> buytype;

        if (buytype==-1)
            break;

        switch(buytype)  //選擇類別
        {
        case 1:
            typemoney = 80;
            break;
        case 2:
            typemoney = 70;
            break;
        case 3:
            typemoney = 60;
            break;
        case 4:
            typemoney = 30;
            break;
        case 5:
            typemoney = 20;
            break;
        case -1:
            break;
        default :
            cout << "Out of range!" << endl;
            break;
        }

        cout << "Number: ";
        cin >> number;

        buy(typemoney,number,idcard,chopsticks,total);  //呼叫buy函式
        cout << "Total: " << total;  //輸出當前總金額
    }
    cout << "Total money: " << total;  //輸出最後總金額

    return 0;
}
