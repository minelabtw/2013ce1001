#include <iostream>
using namespace std;

int menu[] = {0,80,70,60,30,20};  //create a menu to record price of each type
class buycart{
public:
    buycart(int _id,int _chop){
	id = _id;
	chop = _chop;
    }
    int id;
    int chop;
    int type;
    int number;
    void buy(long int *total){
	*total += ((menu[type])*number*(id == 1?0.9:1)-(((chop == 1)&&(type-2 <=1))?5:0)*number);
    };
};
int main(void){
    long int total = 0;
    int input_id,input_chop;
    while(1){  //enter id loop
	cout << "Enter if you have id card 0=no 1=yes : ";
	cin >> input_id;
	if(input_id == 0||input_id == 1){
	    break;
	}else{
	    cout << "Out of range!!\n";
	}
    }
    while(1){  //enter chopsticks loop
	cout << "Enter if you have chopsticks d 0=no 1=yes : ";
	cin >> input_chop;
	if(input_chop == 0||input_chop == 1){
	    break;
	}else{
	    cout << "Out of range!!\n";
	}
    }
    int outerJump = 0;
    while(1){  //main loop,log things user ask
	buycart b1(input_id,input_chop);
	//dump menu.
	cout << "1. Chicken boxed meal: 80\n2. Porl boxed meal: 70\n3. Vegetable boxed meal: 60\n4. Meatball soup: 30\n5. Fish soup: 20\n";
	int innerJump = 0;
	while(!innerJump){
	    cout << "Buy type: ";
	    cin >> b1.type;
	    switch(b1.type){
	    case 1:
	    case 2:
	    case 3:
	    case 4:
	    case 5:
		innerJump = 1;
		break;
	    case -1:
		innerJump = 1;
		outerJump = 1;
		break;
	    default:
		cout << "Out of range!!\n";
		break;
	    };
	}
	if(outerJump){
	    break;
	}
	while(1){
	    cout << "Number: ";
	    cin >> b1.number;
	    if(b1.number > 0){
		break;
	    }else{
		cout << "Out of range!!\n";
	    }
	};
	b1.buy(&total);  //calc total
	cout << "Total: " << total << "\n";  //echo total present
    }
    cout << "Total money: "<< total << "\n";  //echo total
    return 0;
}
