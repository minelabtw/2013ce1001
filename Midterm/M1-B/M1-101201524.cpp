#include<iostream>
using namespace std;

int buy(double idcard,int chopsticks,int type,int number)//宣告涵式用來計算單次消費的支出
{
    int price;//宣告變數price代表消費物品的單價
    double sum;//宣告變數sum代表總金額

    if(type==1)//判斷選擇的品項決定不同的price
        price=80;
    if(type==2)
        price=70;
    if(type==3)
        price=60;
    if(type==4)
        price=30;
    if(type==5)
        price=20;

    if(type==4 || type==5)//若為湯品類則總價要計算自備餐具的折扣
        sum=price*number*idcard-number*chopsticks;
    else//若為便當類則只計算學生證的折扣
        sum=price*number*idcard;
    return sum;//回傳sum
}

int main()
{
    double idcard=-1,total=0;//宣告idcard代表學生證有無之後則代表他的折扣(1or0.9),宣告total代表每次消費加起來的總支出
    int chopsticks=-1;//宣告chopsticks代表餐具有無之後則代表他的折扣(無or扣五塊)

    while(idcard<0 || idcard>1)//當第一次輸入或idcard不符條件時
    {
        cout << "Enter if you have id card 0=no 1=yes : ";//輸出字串選擇1or0
        cin >> idcard;//將選項宣告給idcard
        if(idcard<0 || idcard>1)//若不符條件則輸出"Out of range!"並要求重新輸入
            cout << "Out of range!\n";
    }
    if(idcard==0)//判斷折扣有無(0則沒有1則打九折)
        idcard=1;
    else
        idcard=0.9;
    while(chopsticks<0 || chopsticks>1)//當第一次輸入或chopsticks不符條件時
    {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";//輸出字串選擇1or0
        cin >> chopsticks;//將選項宣告給chopsticks
        if(chopsticks<0 || chopsticks>1)//若不符條件則輸出"Out of range!"並要求重新輸入
            cout << "Out of range!\n";
    }
    if(chopsticks==0)//判斷折扣有無(0則沒有1則湯品少5元)
        chopsticks=0;
    else
        chopsticks=5;

    while(1)
    {
        int type=0,number=0;//宣告變數type代表選項,number代表購買數量
        cout << "1. Chicken boxed meal: 80\n";//輸出菜單
        cout << "2. Pork boxed meal: 70\n";
        cout << "3. Vegetable boxed meal: 60\n";
        cout << "4. Meatball soup: 30\n";
        cout << "5. Fish soup: 20\n";
        while((type<1 && type!=-1) || type>5)//當第一次輸入或type不符條件時
        {
            cout << "Buy type: ";//輸出字串選擇餐點
            cin >> type;//將選項宣告給type
            if((type<1 && type!=-1) || type>5)//若不符條件則輸出"Out of range!"並要求重新輸入
                cout << "Out of range!\n";
        }
        if(type==-1)//若輸入-1則終止消費
            break;
        while(number<=0)//當第一次輸入或number不符條件時
        {
            cout << "Number: ";//輸出字串選擇購買數量
            cin >> number;//將數量宣告給number
            if(number<=0)//若不符條件則輸出"Out of range!"並要求重新輸入
                cout << "Out of range!\n";
        }
        total=total+buy(idcard,chopsticks,type,number);//總消費金額=之前的總消費金額+這次的消費
        cout << "Total: " << total << "\n";//輸出字串和總金額並空行進行下一輪消費
    }

    return 0;
}
