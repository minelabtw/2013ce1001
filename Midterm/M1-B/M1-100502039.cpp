#include <iostream>
using namespace std;

double Buy(int type, int typemoney, int number, int idcard, int chopsticks);

int main(){

    int IdCard, Chopsticks = 0;       //學生證,環保筷,和總金額
    double Sum = 0;
    int BuyType, Number = 0;       //購買類別和數量

    do{                                                     //輸入有無學生證,0沒有,1有
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> IdCard;
        if(IdCard!=0 && IdCard!=1)
            cout << "Out of range!" << endl;
    }while(IdCard!=0 & IdCard!=1);

    do{                                                     //輸入有無環保筷,0沒有,1有
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> Chopsticks;
        if(Chopsticks!=0 && Chopsticks!=1)
            cout << "Out of range!" << endl;
    }while(Chopsticks!=0 & Chopsticks!=1);

    do{
        cout << "1. Chicken boxed meal: 80" << endl << "2. Pork boxed meal: 70" << endl
        << "3. Vegetable boxed meal: 60" << endl << "4. Meatball soup: 30" << endl
        << "5. Fish soup: 20" << endl;



        cout << "Buy type: ";
        cin >> BuyType;

        switch(BuyType){               //根據購買類別去做判斷
            case 1:
                cout << "Number: ";
                cin >> Number;
                Sum += Buy(BuyType, 80, Number, IdCard, Chopsticks);   //呼叫Buy函式計算此次購買金額,並加到總金額中
                cout << "Total: " << Sum << endl;
                break;
            case 2:
                cout << "Number: ";
                cin >> Number;
                Sum += Buy(BuyType, 70, Number, IdCard, Chopsticks);
                cout << "Total: " << Sum << endl;
                break;
            case 3:
                cout << "Number: ";
                cin >> Number;
                Sum += Buy(BuyType, 60, Number, IdCard, Chopsticks);
                cout << "Total: " << Sum << endl;
                break;
            case 4:
                cout << "Number: ";
                cin >> Number;
                Sum += Buy(BuyType, 30, Number, IdCard, Chopsticks);
                cout << "Total: " << Sum << endl;
                break;
            case 5:
                cout << "Number: ";
                cin >> Number;
                Sum += Buy(BuyType, 20, Number, IdCard, Chopsticks);
                cout << "Total: " << Sum << endl;
                break;
            case -1:                   //-1表示結帳
                cout << "Total money: " << Sum;
                break;
            default :                  //非-1或1~5表示輸入錯誤
                cout << "Out of range!" << endl;
                break;
        }

    }while(BuyType != -1);              //-1表示結帳,所以跳出do while

    return 0;
}

double Buy(int type, int typemoney, int number, int idcard, int chopsticks){
    double sum = 0;
    if(type <= 3){        //1~3項為便當,若自備環保筷一個便當可減5元
        switch(idcard){
            case 0:       //無學生證
                if(chopsticks=0)
                    sum = typemoney*number;
                else
                    sum = (typemoney-5)*number;
                break;
            case 1:       //有學生證,所有項目打九折
                if(chopsticks=0)
                    sum = typemoney*number*0.9;
                else
                    sum = (typemoney*0.9-5)*number;
                break;
            default :
                break;
        }
    }
    else{                 //非便當
        switch(idcard){
            case 0:
                sum = typemoney*number;
                break;
            case 1:
                sum = typemoney*number*0.9;
                break;
            default :
                break;
        }
    }
    return sum;
}
