#include<iostream>
using namespace std;

int main()
{
    void Buy(int&,int&,int&,int&,int&,int&,int&,int&);

    int idcard;             //判斷有無學生證
    int chopsticks;         //判斷筷子有無
    int type;               //商品編號
    int number=0;           //購買單一商品數量
    int totalnumber=0;      //商品總數量
    int total=0;            //所有花費總額
    int typeprice=0;          //單一商品價格
    int totalprice=0;       //最終價格
    cout << "Enter if you have id card 0=no 1=yes : ";          //有無學生證
    cin >> idcard;
    while(idcard!=0&&idcard!=1)
    {
        cout << "Out of range!\n";
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> idcard;
    }

    cout << "Enter if you have chopsticks 0=no 1=yes : ";       //有無自備筷子
    cin >> chopsticks;
    while(chopsticks!=0&&chopsticks!=1)
    {
        cout << "Out of range!\n";
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chopsticks;
    }

    cout << "1. Chicken boxed meal: 80\n";      //列出菜單
    cout << "2. Pork boxed meal: 70\n";
    cout << "3. Vegetable boxed meal: 60\n";
    cout << "4. Meatball soup: 30\n";
    cout << "5. Fish soup: 20\n";

    cout << "Buy type: ";
    cin >> type;
    while(type<=0||type>5)      //符合種類範圍
    {
        if(type==-1)
        {
            break;
        }
        cout << "Out of range!\n";
        cout << "Buy type: ";
        cin >> type;
    }
    switch(type)     //菜單 步驟2
    {
    case 1:
        typeprice = 80;
        Buy(type,idcard,chopsticks,typeprice,total,number,totalnumber,totalprice);
        if(type==-1)
        {
            cout << "Total money: " << totalprice;
            break;
        }

    case 2:
        typeprice = 70;
        Buy(type,idcard,chopsticks,typeprice,total,number,totalnumber,totalprice);
        if(type==-1)
        {
            cout << "Total money: " << totalprice;
            break;
        }

    case 3:
        typeprice = 60;
        Buy(type,idcard,chopsticks,typeprice,total,number,totalnumber,totalprice);
        if(type==-1)
        {
            cout << "Total money: " << totalprice;
            break;
        }

    case 4:
        typeprice = 30;
        Buy(type,idcard,chopsticks,typeprice,total,number,totalnumber,totalprice);
        if(type==-1)
        {
            cout << "Total money: " << totalprice;
            break;
        }

    case 5:
        typeprice = 20;
        Buy(type,idcard,chopsticks,typeprice,total,number,totalnumber,totalprice);
        if(type==-1)
        {
            cout << "Total money: " << totalprice;
            break;
        }

    }
    return 0;
}

void Buy(int &type,int &idcard,int &chopsticks,int &typeprice,int &total,int &number,int &totalnumber,int &totalprice)
{
    cout << "Number: ";         //商品數量符合
    cin >> number;
    while(number<=0)
    {
        cout << "Out of range!\n";
        cout << "Number: ";
        cin >> number;
    }

    total=number*typeprice;         //購買此項目所有花費

    if(idcard==1)                   //打折後
    {
        total=0.9*total;
    }
    if(chopsticks==1)
    {
        total=total-(number*5);
    }
    totalprice=totalprice+total;
    total=0;    //最終花費
    cout << "Total: " << totalprice << "\n";

    cout << "1. Chicken boxed meal: 80\n";      //列出菜單
    cout << "2. Pork boxed meal: 70\n";
    cout << "3. Vegetable boxed meal: 60\n";
    cout << "4. Meatball soup: 30\n";
    cout << "5. Fish soup: 20\n";

    cout << "Buy type: ";
    cin >> type;

}
