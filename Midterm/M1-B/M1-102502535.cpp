#include <iostream>

using namespace std ;

int buy ( int money , int  number , int type , int id , int chopsticks ) ;  //宣告函式。

int main()
{
    int money ;
    int number ;
    int id ;
    int chopsticks ;
    int type ;  //設定變數。

    cout << "Enter if you have id card 0=no 1=yes : " ;
    cin >> id ;
    while ( id != 1 && id != 0 )
    {
        cout << "Out of range!" << endl << "Enter if you have id card 0=no 1=yes : " ;
        cin >> id ;
    }  //判定輸入值是否合理，若不合，則重新輸入。

    cout << "Enter if you have chopsticks 0=no 1=yes : " ;
    cin >> chopsticks ;
    while ( chopsticks != 1 && chopsticks != 0 )
    {
        cout << "Out of range!" << endl << "Enter if you have chopsticks 0=no 1=yes : " ;
        cin >> chopsticks ;
    }  //判定輸入值是否合理，若不合，則重新輸入。


    while ( type != -1 )
    {
        cout << "1. Chicken boxed meal: 80" << endl << "2. Pork boxed meal: 70" << endl <<"3. Vegetable boxed meal: 60" << endl ;
        cout << "4. Meatball soup: 30" << endl <<"5. Fish soup: 20" << endl << "Buy type: " ;
        cin >> type ;
        while ( type != -1 && type < 1 || type > 5 )
        {
            cout << "Out of range!" << endl << "Buy type: " ;
            cin >> type ;
        }

        switch ( type )
        {
        default :
            cout << "Number: " ;
            cin >> number ;
            while ( number < 1 )
            {
                cout << "Out of range!" << endl << "Number: " ;
                cin >> number ;
            }
            break ;

        case -1 :
            break ;
        }

        cout << "Total: " << buy ( money , number, type , id, chopsticks ) << endl ;
    }  //判斷type的值，若為-1即跳出迴圈。

    return 0 ;
}

int buy ( int money , int  number , int type , int id , int chopsticks )
{
    if ( id == 1 )
    {

        if ( type == 1 )
        {
            money = 80 * number * 0.9 ;
        }
        else if ( type == 2 )
        {
            money = 70 * number * 0.9 ;
        }
        else if ( type == 3 )
        {
            money = 60 * number * 0.9 ;
        }
        else if ( type == 4 )
        {
            money = 30 * number * 0.9 ;
        }
        else if ( type == 5 )
        {
            money = 20 * number * 0.9 ;
        }
    }

    else if ( id == 0 )
    {
        if ( type == 1 )
        {
            money = 80 * number ;
        }
        else if ( type == 2 )
        {
            money = 70 * number ;
        }
        else if ( type == 3 )
        {
            money = 60 * number ;
        }
        else if ( type == 4 )
        {
            money = 30 * number ;
        }
        else if ( type == 5 )
        {
            money = 20 * number ;
        }
    }
    if ( chopsticks == 1 )
    {
        if ( type == 1 || type == 2 || type == 3 )
        {
            money = money - 5 * number ;
        }
        else
            money = money ;
    }
    else if ( chopsticks == 0 )
    {
        money = money ;
    }
    return money ;
}  //定義函式之計算方式。
