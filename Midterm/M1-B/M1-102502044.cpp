/*************************************************************************
    > File Name: M1-102502044.cpp
    > Author: rockwyc992
    > Mail: rockwyc992@gmail.com 
    > Created Time: 西元2013年11月13日 (週三) 16時19分15秒
 ************************************************************************/

#include <stdio.h>

/* function to check input number */
int check_bool(int x);
int check_type(int x);
int check_num(int x);

/* function to input */
void input(const char *str, int *tmp, int (*check)(int));

/* function to calc total money */
void buy(int type, int num, int card, int chop, int &sum);

int main()
{
	/* card=1 means have idcard						 */
	/* chop=1 means have chopsticks                  */
	/* type   means the type I want to buy           */
	/* num    means the number of meal or soup I buy */
	int card, chop;
	int type, num;

	/* sum is the total money */
	int sum = 0;

	/* input if I have card or chopsticks */
	input("Enter if you have id card 0=no 1=yes : ", &card, check_bool);
	input("Enter if you have chopsticks 0=no 1=yes : ", &chop, check_bool);
	
	/* repeat the buy meal loop, NO~~~~~~~~~ */
	while(1)
	{
		/* output the menu */
		puts("1. Chicken boxed meal: 80");
		puts("2. Pork boxed meal: 70");
		puts("3. Vegetable boxed meal: 60");
		puts("4. Meatball soup: 30");
		puts("5. Fish soup: 20");

		/* input type */
		input("Buy type: ", &type, check_type);
		
		/* exit the buy-meal-loop. Ya, I have a lots of meals */
		if(type == -1)
			break;	

		/* input the number of meal */
		input("Number: ", &num, check_num);
	
		/* calculate total money */
		buy(type, num, card, chop, sum);
		
		/* output total money */
		printf("Total: %d\n", sum);

	}

	/* output total money */
	printf("Total money: %d\n", sum);

	return 0;
}

/* x means the input */
int check_bool(int x)
{
	/* check x is in set{0, 1}*/
	switch(x)
	{
		case 0:
		case 1:
			return 1;
	
		default:
			return 0;
	}
}

/* x means the input */
int check_type(int x)
{
	/* check x is in set{1, 2, 3, 4, 5, -1}*/
	switch(x)
	{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case -1:
			return 1;

		default:
			return 0;
	}
}

/* x means the input */
int check_num(int x)
{
	/* check x is in range (0, unlimited)*/
	return x>0;
}

void input(const char *str, int *tmp, int (*check)(int))
{
	while(1)
	{
		/* output the UI */
		printf(str);

		/* input an integer into *tmp */
		scanf("%d", tmp);
		
		/* check input is in range */
		if(check(*tmp))
			break;
		else
			puts("Out of range!");
	}
}

/* function is used to calcule total money */
void buy(int type, int num, int card, int chop, int &sum)
{
	/* cost means the cost of one meal I will pay */
	int cost;

	/* calcule the cost of one meal */
	switch(type)
	{
		case 1:
			cost = 80 * (card ? 0.9 : 1.0) - (chop ? 5 : 0);
			break;
		case 2:
			cost = 70 * (card ? 0.9 : 1.0) - (chop ? 5 : 0);
			break;
		case 3:
			cost = 60 * (card ? 0.9 : 1.0) - (chop ? 5 : 0);
			break;
		case 4:
			cost = 30 * (card ? 0.9 : 1.0);
			break;
		case 5:
			cost = 20 * (card ? 0.9 : 1.0);
			break;
	}

	/* add cost to total money */
	sum += cost * num;
}
