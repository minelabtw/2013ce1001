#include<iostream>
using namespace std;

int Buy (int idcard, int chopsticks, int typemoney, int number, int buytype) //運算價格的函式
{
    int x=0;
    double y;
    if (idcard==1)
    {
        y=0.9;
    }
    else
    {
        y=1;
    }

    if (buytype==1 || buytype==2 || buytype==3)
    {
        if(chopsticks==1)
            typemoney=(typemoney*y-5)*number;
        else
            typemoney=(typemoney*y)*number;
    }
    else
    {
        typemoney=typemoney*y*number;
    }

    x=typemoney;

    return x;
}

int main()
{
    int idcard;
    int chopsticks;
    int buytype=1;
    int number;
    int total=0;
    int typemoney; //類別的價格
    int x; //運算後的值

    do
    {
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> idcard;
    }
    while(idcard != 1 && idcard != 0 && cout << "Out of range!" <<endl);

    do
    {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chopsticks;
    }
    while(chopsticks != 1 && chopsticks != 0 && cout << "Out of range!" <<endl); //顯示題目需求並給予限制

    do
    {
        cout << "1. Chicken boxed meal: 80" << endl;
        cout << "2. Pork boxed meal: 70" << endl;
        cout << "3. Vegetable boxed meal: 60" << endl;
        cout << "4. Meatball soup: 30" << endl;
        cout << "5. Fish soup: 20" << endl; //顯示價目表


        do
        {
            if (buytype <=0 || buytype>5)
            {
                cout << "Out of range!" << endl;
            }
            cout << "Buy type: ";
            cin >> buytype;
        }
        while (buytype!= -1 && buytype<=0 || buytype>5); //輸入購買類別並給予限制


        if (buytype!= -1) //當類別為1則結束
        {
            do
            {
                cout << "Number: ";
                cin >> number;
            }
            while (number<=0 && cout << "Out of range!" << endl);

            switch (buytype)
            {
            case 1:
                typemoney=80;
                break;
            case 2:
                typemoney=70;
                break;
            case 3:
                typemoney=60;
                break;
            case 4:
                typemoney=30;
                break;
            case 5:
                typemoney=20; //價目價格給typemoney
                break;
            }

            x=Buy (idcard, chopsticks, typemoney, number, buytype); //進入函式運算
            total += x; //累加總金額

            cout << "Total: " << total << endl; //顯示總金額
        }
        else
            cout << "Total money: " << total; //結束時輸出總金額
    }
    while (buytype != -1);
    return 0;
}
