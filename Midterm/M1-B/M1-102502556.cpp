#include <iostream>
using namespace std;

void buy ( int , int , int &, double , int ); //function prototype

int main ()
{
    double id = 0; //宣告型別為 double 的第一個變數(id)，並初始化其數值為0。
    int chop = 0; //宣告型別為 int(整數) 的第二-五個變數(chop,total,type,number)，並初始化其數值為0。
    int total = 0;
    int type = 0;
    int number = 0;
    cout << "Enter if you have id card 0=no 1=yes : "; //使用cout指令使欲顯示的文字輸出在螢幕上。
    cin >> id ;
    while ( id != 0 && id != 1 ) //用while迴圈檢驗使用者所輸入的數字是否合乎標準，否則要求其重新輸入。
    {
        cout << "Out of range!" << endl;
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> id ;
    }
    if ( id == 1 ) //用 if 和 esle 判斷是否有id card。
    {
        id = 0.9 ;
    }
    else
    {
        id = 1;
    }
    cout << "Enter if you have chopsticks 0=no 1=yes : ";
    cin >> chop ;
    while ( chop != 0 && chop != 1 )
    {
        cout << "Out of range!" << endl;
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chop ;
    }
    while ( 1 ) //使用while迴圈，使其能重複做購買的動作，若在type欄輸入-1，則購買結束，並輸出總金額。
    {
        cout << "1. Chicken boxed meal: 80" << endl;
        cout << "2. Pork boxed meal: 70" << endl;
        cout << "3. Vegetable boxed meal: 60" << endl;
        cout << "4. Meatball soup: 30" << endl;
        cout << "5. Fish soup: 20" << endl;
        cout << "Buy type: ";
        cin >> type;
        while ( type != 1 && type != 2 && type != 3 && type != 4 && type != 5 && type != -1 )
        {
            cout << "Out of range!" << endl ;
            cout << "Buy type: " ;
            cin >> type;
        }
        if ( chop == 1 )
        {
            if ( type == 1 || type == 2 || type == 3 )
            {
                chop = -5;
            }
            else
            {
                chop = 0;
            }
        }
        else
        {
            chop = 0;
        }
        if ( type == -1 )
        {
            break;
        }
        switch ( type ) //用switch迴圈來針對不同的type，將type改變為其項目所對應的單價。
        {
        case 1:
        {
            type = 80;
            break;
        }
        case 2:
        {
            type = 70;
            break;
        }
        case 3:
        {
            type = 60;
            break;
        }
        case 4:
        {
            type = 30;
            break;
        }
        case 5:
        {
            type = 20;
            break;
        }
        }
        cout << "Number: ";
        cin >> number;
        while ( number <= 0 )
        {
            cout << "Out of range!" << endl;
            cout << "Number: ";
            cin >> number;
        }
        buy( type , number , total , id , chop ); //呼叫buy函數。
    }
    cout << "Total money: " << total;
    return 0;
}

void buy ( int type , int number, int &total , double id , int chop ) //做金額的計算。
{
    total += type * number * id + chop * number ;
    cout << "Total: " << total << endl ;
}
