#include <iostream>
using namespace std;
double a=0; //宣告變數a
int b=0;    //宣告變數b
int c=0;    //宣告變數c
double d=0; //宣告變數d
int e=0;    //宣告變數e
void Buy (double a, int b,int c,double d,int &e); //宣告函式Buy
int main()
{
    cout <<"Enter if you have id card 0=no 1=yes : "; //輸出
    cin >>a; //輸入a
    while (a!=0 && a!=1) //進入迴圈
    {
        cout <<"Out of range!"<<endl<<"Enter if you have id card 0=no 1=yes : ";
        cin >>a;
    }
    cout <<"Enter if you have chopsticks 0=no 1=yes : ";
    cin >>b;
    while (b!=0 && b!=1) //進入迴圈
    {
        cout <<"Out of range!"<<endl<<"Enter if you have chopsticks 0=no 1=yes : ";
        cin >>b;
    }
    while (c!=-1) //進入迴圈
    {
        cout <<"1. Chicken boxed meal: 80"<<endl;
        cout <<"2. Pork boxed meal: 70"<<endl;
        cout <<"3. Vegetable boxed meal: 60"<<endl;
        cout <<"4. Meatball soup: 30"<<endl;
        cout <<"5. Fish soup: 20"<<endl;
        cout <<"Buy type: ";
        cin >>c;
        while (c<-1 || c==0 || c>5) //進入迴圈
        {
            cout <<"Out of range!"<<endl<<"Buy type: ";
            cin >>c;
        }
        if (c!=-1)
        {
            cout <<"Number: ";
            cin >>d;
            while (d<=0) //進入迴圈
            {
                cout <<"Out of range!"<<endl<<"Number: ";
                cin >>d;
            }
            Buy(a,b,c,d,e);
            cout <<"Total: "<<e<<endl;
        }
        else
        {
            Buy(a,b,c,d,e);
            cout <<"Total money: "<<e;
        }
    }
    return 0;
}
void Buy (double a, int b,int c,double d,int &e) //函式
{
    switch(c) //進入case
    {
    case 1:
        e=e-5*b*d; //計算
        d=d-0.1*a*d;
        e=e+80*d;
        break;
    case 2:
        e=e-5*b*d;
        d=d-0.1*a*d;
        e=e+70*d;
        break;
    case 3:
        e=e-5*b*d;
        d=d-0.1*a*d;
        e=e+60*d;
        break;
    case 4:
        d=d-0.1*a*d;
        e=e+30*d;
        break;
    case 5:
        d=d-0.1*a*d;
        e=e+20*d;
        break;
    case -1:
        e=e;
    }
}
