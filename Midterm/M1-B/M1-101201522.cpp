#include <iostream>
using namespace std;

void Buy(int price,int num,int id,int chopstick,int &total)//副函式,計算購買的總金額,利用參考的方式
{
    if(id)//如果有id卡,打9折
        price *= 0.9;
    if(price>=60)//如果有筷子,便當每個折五元
        total += num*(price-5*chopstick);
    else//計算湯類價格
        total += num*price;
}

int main()
{
    int id,chopstick,type,num,price[5]= {80,70,60,30,20},total=0;
    //宣告為整數,id為是否有id卡,chopstick為是否有筷子,type為購買類型,num為購買數量,price分別為1~5類型的價格,total為總金額
    do
    {
        cout << "Enter if you have id card 0=no 1=yes : ";
        cin >> id;
    }
    while((id!=1&&id)&&cout << "Out of range!\n");//處理輸入的id
    do
    {
        cout << "Enter if you have chopsticks 0=no 1=yes : ";
        cin >> chopstick;
    }
    while((chopstick!=1&&chopstick)&&cout << "Out of range!\n");//處理輸入的chopstick
    while(1)
    {
        cout << "1. Chicken boxed meal: 80\n"
             << "2. Pork boxed meal: 70\n"
             << "3. Vegetable boxed meal: 60\n"
             << "4. Meatball soup: 30\n"
             << "5. Fish soup: 20\n";//印出價目表
        do
        {
            cout << "Buy type: ";
            cin >> type;
        }
        while((type>5 || type<1)&&type!=-1 && cout << "Out of range!\n");//處理輸入購買的類型
        if(type==-1)//如果為-1就結帳
            break;
        else
        {
            do
            {
                cout << "Number: ";
                cin >> num;
            }
            while(num<=0 && cout << "Out of range!\n");
            Buy(price[type-1],num,id,chopstick,total);
            cout << "Total: " << total <<endl;//輸出目前總金額
        }//計算購買的金額
    }
    cout << "Total money: " << total <<endl;//輸出總金額
    return 0;
}
